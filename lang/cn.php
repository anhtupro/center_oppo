<?php

/**
 * @author: hero
 * Vietnamese language
 * description
 * 04-10-2016
 */
return array(
    'menu' => 'Menu principal',
    'menu-books' => 'Livres',
    'menu-electronics' => 'Electronics',
    'menu-apparel' => 'Vêtements',
    'menu-watches' => 'Montres',
    'good_morning' => 'Chào buổi sáng',
    'how_are_you' => 'Comment allez-vous?'
);
