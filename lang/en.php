<?php

/**
 * @author: hero
 * Vietnamese language
 * description
 * 04-10-2016
 */
return array(
	// key layout
	'layout_hide' => 'Hide',
	'layout_user_infomation' => 'User Infomation',
	'layout_change_password' => 'Change Password',
	'layout_clear_cache' => 'Clear Cache',
	'layout_log_out' => 'Log Out',

     // key for timing/index
    'timing_name' => 'Name',
    'timing_province' => 'Province',
    'timing_phone_number' => 'Phone number',
    'timing_email' => 'Mail',
    'timing_date' => 'Date',
    'timing_store' => 'Store',
    'timing_timing_at' => 'Timing at',
    'timing_update_at' => 'Update at',
    'timing_status' => 'Status',
    'timing_note' => 'Note',
    'timing_action' => 'Action',
    'timing_list_timing' => 'List timing',
    'timing_imei_expire' => 'Imei expired',
    'search' => 'Search',
   
    
    
    // key for all search
    'search' => 'Search',
    'search_name' => 'Name',
    'search_area' => 'Area',
    'search_email' => 'Email',
    'search_province' => 'Province',
    'search_dictrict' => 'District',
    'search_store' => 'Store',
    'search_approve' => 'Type Approve',
    'search_from_date' => 'From date',
    'search_to_date' => 'To date',
    'search_imei' => 'Imei',
     'search_reset' => 'Reset',
    'search_report' => 'Report',
	
	// key for appraisal office
	'ao_set_kpi_for_self' => 'Set KPI',
	'ao_performance_appraisal_for_self' => 'Appraise KPI',
	'ao_approve_kpi' => 'Approve KPI',
	'ao_view_results_for_self' => 'View result',
	'ao_performance_appraisal' => 'Appraise KPI',
	'ao_view_results' => 'View result',
	'ao_department_kpis' => 'Department KPIs',
	'ao_department_report' => 'Department Report',
    
	'ao_department_kpis_and_report' => 'Department KPIs & Report',
	'ao_setting_performance_appraisal' => 'Setting & Appraisal KPI ',
	'ao_setting_performance_appraisal_personal' => 'SETTING & APPRAISAL PERSONAL KPI',
	'ao_approve_kpi_performance_appraisal' => 'Approve & Appraisal KPI Employees',
	
	// key for appraisal office/pre-set-prd
	'ao_plan_id' => 'Time set KPI',
	'ao_title_page' => 'Set KPI',
	'ao_title_page_pre_star_prd' => 'Self - Appraisal (KPI)',
	'back_button' => 'Go back',
	'ao_title_page_appaisal_kpi' => 'Appraisal KPI',
	'ao_title_page_approving_kpi' => 'Approve KPI',

	'ao_appaisal_kpi_time' => 'Time',
	'ao_staff' => 'Staff',
	'approve' => 'Approve',
	'department' => 'Department',
	'ao_title' => 'Title',
	'team' => 'Team',
	'note' => 'Note',
	'ao_appaisal_pre_approve_time' => 'Time',
	'ao_view_result_head_kpi_result' => 'View Result',
	'ao_prd_result' => 'View KPI',

	"ao_status" =>"Status ",
	// appraisal-office/pre-star-prd
	"ao_status_1" =>"Status: ",

	"ao_rank_excellent" =>"Excellent",
	"ao_rank_good" =>"Good",
	"ao_rank_pretty" =>"Satisfication",
	"ao_rank_normal" =>"Fair",
	"ao_rank_failed" =>"Unuatisfication",

	"excellent_decription" => "<b>A+: Excellent</b> (Ratings > 110 points): High quality work that significantly exceeds expectations",
	"good_decription" => "<b>A: Good</b> (Rating = 100 points): Quality work that exceeds expectations.",
	"satisfactory_decription" => "<b>B: Satisfactory</b> (Ratings = 90 points): Achieve 90% of the requirement.",
	"fair_decription" => "<b>C: Fair</b> (Ratings = 70 points): Achieve 70% of the requirement.</b>",
	"unsatisfactory_decription" => "<b>D: Unsatisfactory </b> (Ratings < 70 points): Achieve under 70% of the requirement.",


	"kpi_decription" =>"<b>KPI</b><br>(Your work is good when reaching all the following index)",
	"duty_activity" =>"<b>Duties & Activities</b> <br>(Steps to complete your KPI)",
	"kpi_detail" =>"<b>Result detail</b>",
	"task" =>"<b>Tasks</b>",
	"ao_ratial" => "<b>Ratial</b><br>(Ratial of each must >= 10%)",
	"ao_ratial_detail" => "Ratial total must be 100%",



	// /appraisal-office/list-approve-prd?
	"list" =>"List",

	// common appraisal
	"are_you_sure" =>"Are you sure?",
	"can_not_rollback" =>"The action will not be roll back changes",
	"ratial"=>"Ratial",
	"appraisal"=>"<b>Appraisal</b>",


	





	
	

	




    
       // key for default
     'good_morning' => 'Good morning',
	 
	 
	 
	 
	 
	 	// Request-Office
    "rq_content" => "Request content",
    "rq_own" => "My (approved) request",
	"rq_is_year_contract" => "Year contract",
    "rq" => "Request Office",
    "rq_list" => "List Request Office",


    "rq_from_date" => "From date",
    "rq_to_date" => "To date",
    "rq_company" => "Company",
    "rq_status" => "Status",
    "rq_type" => "Request type",
    "rq_filter" => "Filter",
    "rq_unfilter" => "Unfilter",

    "rq_pr_name" => "PR Name",
    "rq_requestter" => "Requester",
    "rq_payment" => "Payment",
    "rq_amount_of_money" => "Estimated money",
	"rq_amount_of_actual" => "Real money spent",
    "rq_amount" => "Amount",
    "rq_payment_method" => "Payment methods",
    "rq_approved_by" => "Approved by",
    "rq_date" => "Date",
    "rq_number" => "Request Number",
	
	"rq_create" => " Create",
    "rq_update" => " Update",
    "rq_detail" => " Detail",
    "rq_advanced_request" => "Advanced Request Number",

     "rq_has_purchasing" => "Does Request has PR?",
    "rq_has_payment" => "Does Request has payment?",
    "rq_purchasing_info" => "Purchasing Infomation",
    "rq_purchasing_detail_info" => "Purchasing Detail Infomation",

    "rq_purchasing_number" => "PR number",
    "rq_purchasing_urgent_date" => "Urgent date",
    "rq_purchasing_type" => "PR type",
    "rq_purchasing_project" => "Project",
    "rq_purchasing_shipping_address" => "Shipping address",

    "rq_common_info" => "Request Common Information",
    "rq_detail_info" => "Request Detail Information",
    "rq_content_detail_info" => "Content",
    "rq_payee_detail_info" => "Payee",
    "rq_requester_detail_info" => "Requester",

    "rq_note_detail_info" => "Note",
    "rq_note_confirm" => "Putting your note",

    "rq_amount_detail_info" => "Amount",

    "rq_amount_advanced_detail_info" => "Advanced Amount",
    // "rq_pay_detail_info" => "Request Detail Information",
    // "rq_take_detail_info" => "Request Detail Information",
    "rq_money_change_detail_info" => "Number change",
    "rq_payment_method_detail_info" => "Payment methods",
    "rq_payment_cost_execute_detail_info" => "Execution amount",
    "rq_payment_attached_file" => "Attached Files",
    "rq_payment_department_detail" => "Payment for Department",
    "rq_payment_invoice" => "Invoice number",
    "rq_payment_date" => "Payment date",
    "rq_payment_invoice_date" => "Invoice date",
    "rq_payment_update" => "Update Payment",
    "rq_confirm" => "Confirm",
    "rq_finish_condition" => "Just only Finishing the Request after updating payment successfully",
    "rq_finish" => "Finishing the Request",
	
	"rq_payment_invoice_new" => "Invoice Number",
    "rq_payment_invoice_date_new" => "Invoice Date",

	"rq_is_coo" => "Is DIRECTOR confirm?",
	"rq_month_year_fee" => "Cost for the month",
	"rq_request_type_group" => "Category group",
	"rq_request_type" => "Category fee",
	"rq_budget" => "Budget",
	"rq_date_success" => "Estimated completion date",
	"rq_cost_before" => "The total amount of money expected",
	"rq_project" => "Project",
	"rq_contract_number" => "Contract number",
	"rq_contract_from" => "Contract start date",
	"rq_contract_to" => "Contract end date",
	"rq_total_budget" => "Total budget",
	"rq_total_use" => "Total use",
	"rq_total_remaining" => "Remaining Budget",
	"rq_finance_staff" => "Staff finance confirm?",
	
	"rq_request_id" => "Request Number",
	"finance_payment_date" => "Payment Date",
	"estimasted_liquidation_date" => "Estimasted Liquidation Date",
	 
	 //link: /staff-time/list-staff-approve

	'searchbox_search' => 'Search',
	'searchbox_fullname' => 'Full name',
	'searchbox_area' => 'Area',
	'searchbox_department' => 'Department',
	'searchbox_email' => 'Email',
	'searchbox_year' => 'Year',
	'searchbox_team' => 'Team',
	'searchbox_code' => 'Code',
	'searchbox_month' => 'Month',
	'searchbox_title' => 'Title',
	'searchbox_button_search' => 'Search',
	'searchbox_button_export' => 'Export',
	'searchbox_select_option_all' => 'All',
	'searchbox_select_option_choose' => 'Choose',


	'table_title_area' => 'Area',
	'table_title_code' => 'Code',
	'table_title_fullname' => 'Full Name',
	'table_title_title' => 'Title',

	'button_approval' => 'Approval',
	'button_approve' => 'Approve',
	'button_reject' => 'Reject',



	'staff_approve_day' => 'Day',
	'title-view-staff-leave'	=> 'Approve Time/ Leave',
	'button_list-staff-leave' => 'List Staff Leave',
	'button_list-staff-time' => 'List Staff Time',
	'pending-leaves' => 'Leaves pending for approval',
	'pending_leaves_arer' => 'Area',
	'pending_leaves_code' => 'Code',
	'pending_leaves_full-name' => 'Full Name',
	'pending_leaves_title' => 'Tittle',
	'pending_leaves_type-of-leaves' => 'Type of leave',
	'pending_leaves_form' => 'From',
	'pending_leaves_to' => 'To',
	'pending_leaves_due-date' => 'Due date',
	'pending_leaves_day-of-leave' => 'Days of leaves',
	'pending_leaves_reason' => 'Reason',
	'pending_leaves_approval' => "Manager's approval",
	'pending_leaves_approve' => 'Approve',
	'pending_leaves_reject' => 'Reject',
	'pending_leaves_hr' => "Hr's approval",
	'pending_leaves_feed-back' => 'Feedback',
	'pending_time' => 'Working days pending for approval',
	'pending_time_code' => 'Code',
	'pending_time_full-time' => 'Full name',
	'pending_time_title' => 'Title',
	'pending_time_working-day' => 'Working days pending for approval',
	'pending_time_attendance' => 'Attendance pending for approval',
	'pending_time_approval' => 'Approval',
	'pending_time_approve' => 'Approve',
	'pending_time_reject' => 'Reject',
	'pending_time_detail' => 'Details',
	'pending_time_pop-up-details_date' => 'Date',
	'pending_time_pop-up-details_check-in' => 'Check in',
	'pending_time_pop-up-details_check-out' => 'Check out',
	'pending_time_pop-up-details_working-day' => 'Working day',
	'pending_time_pop-up-details_approve-for' => 'Approval for',
	'pending_time_pop-up-details_reason' => 'Reason',
	'pending_time_pop-up-details_approval' => 'Approval',
	'pending_time_pop-up-details_approve' => 'Approve',
	'pending_time_pop-up-details_reject' => 'Reject',
	'pending_time-last-month' => 'Working days in last month pending for approval',
	'pending_time-last-month_code' => 'Code',
	'pending_time-last-month_full-name' => 'Full name',
	'pending_time-last-month_title' => 'Title',
	'pending_time-last-month_working-pending' => 'Working days pending for approval',
	'pending_time-last-month_approval' => 'Approval',
	'pending_time-last-month_details' => 'Details',

	'list_of_duty_day' => 'List of duty day',
	'table_title_area' => 'Area',
	'table_title_code' => 'Code',
	'table_title_fullname' => 'Full Name',
	'table_title_title' => 'Title',
	'list_of_duty_day_pending' => 'Duty days',
	'list_of_duty_day_detail' => 'Detail',

	'latitude_check_in' => 'Latitude checkin',
	'longitude_check_in' => 'Longitude checkin',
	'location_on_googlemaps' => 'Location on Google Maps',

	'time_checkin' => 'Time checkin',
	'location_checkin' => 'Location checkin',
	'office_checkin' => 'Office checkin',
	'time_checkout' => 'Time checkout',
	'location_checkout' => 'Location checkout',
	'office_checkout' => 'Office checkout',

        //link: /staff-time/lock-staff-time
        'title_lock_staff_time' => 'Lock time',
	 
	 
	 
	 
    
);
