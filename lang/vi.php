<?php

/**
 * @author: hero
 * Vietnamese language
 * description
 * 04-10-2016
 */
return array(
	// key layout
	'layout_hide' => 'Ẩn',
	'layout_user_infomation' => 'Tài khoản',
	'layout_change_password' => 'Đổi mật khẩu',
	'layout_clear_cache' => 'Xóa bộ nhớ đệm',
	'layout_log_out' => 'Đăng xuất',

    // key for timing/index
    'timing_name' => 'Tên',
    'timing_province' => 'Tỉnh',
    'timing_phone_number' => 'Số điện thoại',
    'timing_email' => 'Mail',
    'timing_date' => 'Ngày',
    'timing_store' => 'Cửa hàng',
    'timing_timing_at' => 'Thời gian',
    'timing_update_at' => 'Cập nhật',
    'timing_status' => 'Trạng thái',
    'timing_note' => 'Ghi chú',
    'timing_action' => 'Tác vụ',
    'timing_list_timing' => 'Danh sách báo số',
    'timing_imei_expire' => 'Danh sách Imei »',
    
    
    // key for all search
    'search' => 'Tìm kiếm',
    'search_name' => 'Tên',
    'search_area' => 'Khu vực',
    'search_email' => 'Email',
    'search_province' => 'Tỉnh',
    'search_dictrict' => 'Quận/Huyện',
    'search_store' => 'Cửa hàng',
    'search_approve' => 'Loại duyệt',
    'search_from_date' => 'Từ ngày',
    'search_to_date' => 'Đến ngày',
    'search_imei' => 'Số imei',
     'search_reset' => 'Đặt lại',
    'search_report' => 'Xuất báo cáo',
    
	// key for appraisal office
	'ao_set_kpi_for_self' => 'Thiết lập KPI',
	'ao_performance_appraisal_for_self' => 'Tự đánh giá kết quả công việc',
	'ao_approve_kpi' => 'Duyệt KPI',
	'ao_view_results_for_self' => 'Xem kết quả',
	'ao_performance_appraisal' => 'Đánh giá kết quả công việc',
	'ao_view_results' => 'Xem kết quả',
	'ao_department_kpis' => 'Danh sách thiết lập KPI',
	'ao_department_report' => 'Toàn bộ kết quả',
    
	'ao_department_kpis_and_report' => 'Danh sách KPI của toàn bộ phận',
	'ao_setting_performance_appraisal' => 'Thiết lập & đánh giá KPI cá nhân',
	'ao_setting_performance_appraisal_personal' => 'THIẾT LẬP & ĐÁNH GIÁ KPI CÁ NHÂN',
	'ao_title_page_approving_kpi' => 'Duyệt KPI',
	'ao_approve_kpi_performance_appraisal' => 'Duyệt & đánh giá KPI nhân viên',

	// key for appraisal office/pre-set-prd
	'ao_plan_id' => 'Chọn thời gian thiết lập:',
	'ao_title_page' => 'Thiết lập KPI',
	'ao_title_page_pre_star_prd' => 'Tự đánh giá kết quả công việc (KPI)',
	'back_button' => 'Quay lại',
	'ao_title_page_appaisal_kpi' => 'Duyệt KPI',
	'ao_appaisal_kpi_time' => 'Chọn thời gian duyệt KPI:',

	'ao_staff' => 'Nhân viên',
	'approve' => 'Duyệt',
	'team' => 'Bộ phận',
	'department' => 'Phòng ban',
	'ao_title' => 'Chức danh',
	'note' => 'Ghi chú',
	// key for appraisal office/view-result-staff

	'ao_prd_result' => 'Kết quả đánh giá',

	// rank
	"ao_rank_excellent" =>"Xuất sắc",
	"ao_rank_good" =>"Giỏi",
	"ao_rank_pretty" =>"Khá",
	"ao_rank_normal" =>"Trung Bình",
	"ao_rank_failed" =>"Không đạt",
	"ao_status" =>"Tình trạng ",
	// appraisal-office/pre-star-prd
	"ao_status_1" =>"Trạng thái: ",
	"ao_ratial" => "Tỷ trọng từng tiêu <br> chí phải >= 10%",
	"ao_ratial_detail" => "Tổng tỉ trọng phải bằng 100%",

	

	'ao_appaisal_pre_approve_time' => 'Chọn thời gian đánh giá KPI:',
	'ao_view_result_head_kpi_result' => 'Kết quả đánh giá',
	// /appraisal-office/list-approve-prd?
	"list" =>"Danh sách chờ duyệt",
	// https://center.opposhop.vn/appraisal-office/star-prd?plan_id=39
	"excellent_decription" => "<b>A+ : Xuất sắc</b> (Tỷ trọng đạt >110 điểm) : <b>Kết quả công việc đạt yêu cầu, trên mức mong đợi.</b>",
	"good_decription" => "<b>A : Giỏi</b> ( Tỷ trọng đạt = 100 điểm): <b>Kết quả công việc đạt đúng yêu cầu.</b>",
	"satisfactory_decription" => "<b>B : Khá</b> (Tỷ trọng đạt = 90 điểm): Kết quả công việc đạt 90% yêu cầu.",
	"fair_decription" => "<b>C : Trung bình <b> (Tỷ trọng đạt = 70 điểm): <b>Kết quả công việc đạt 70% yêu cầu.</b>",
	"unsatisfactory_decription" => "<b>D : Không đạt yêu cầu </b>(Tỷ trọng đạt < 70 điểm) : <b>Kết quả công việc đạt dưới 70% yêu cầu.</b>",

	"kpi_decription" =>"<b>KPI</b><br> Công việc của bạn thực hiện tốt khi đạt các chỉ số sau:",
	"duty_activity" =>"<b>Nhiệm vụ và hoạt động</b> <br>(Trình các bước thực hiện để hoàn thành KPI)",
	"kpi_detail" =>"<b>Chi tiết kết quả KPI/công việc đạt được<b>",
	"task" =>"<b>Các hạng mục công việc</b>",

	// common appraisal
	"are_you_sure" =>"Bạn chắc chắn xác nhận hoàn thành?",
	"can_not_rollback" =>"Sau khi gửi thông tin sẽ không thể điều chỉnh.!",
	"ratial"=>"Tỉ trọng",
	"appraisal"=>"Đánh giá",

	


	

	



	
    // key for default
    'good_morning' => 'Chào buổi sáng',
	

	// Request-Office
    "rq_content" => "Nội dung đề xuất",
    "rq_own" => "Đề xuất của tôi (đã duyệt)",
	"rq_is_year_contract" => "Hợp đồng năm",

    "rq" => "Đề xuất thanh toán",
    "rq_list" => "Danh sách đề xuất thanh toán",

    "rq_from_date" => "Từ ngày",
    "rq_to_date" => "Đến ngày",
    "rq_company" => "Công ty",
    "rq_status" => "Trạng thái",
    "rq_type" => "Loại đề xuât",
    "rq_filter" => "Tìm kiếm",
    "rq_unfilter" => "Xóa bộ lọc",
    "rq_pr_name" => "Tên PR",
    "rq_requestter" => "Người đề xuất",
    "rq_payment" => "Thanh toán",
    "rq_amount_of_money" => "Số tiền dự tính",
	"rq_amount_of_actual" => "Số tiền thực tế",
    "rq_amount" => "Số lượng",

    "rq_payment_method" => "Hình thức thanh toán",
    "rq_approved_by" => "Những người đã duyệt",
    "rq_date" => "Ngày yêu cầu",
    "rq_number" => "Mã đề xuất",
	
	"rq_create" => " Tạo mới",
    "rq_update" => " Cập nhật",
    "rq_detail" => " Chi tiết",
    "rq_advanced_request" => "Mã tạm ứng",
    "rq_has_purchasing" => "Đề xuất có Purchasing Request?",
    "rq_has_payment" => "Đề xuất cần thanh toán?",
    "rq_purchasing_info" => "Thông tin đơn hàng Purchasing",
    "rq_purchasing_detail_info" => "Thông tin chi tiết đơn hàng Purchasing",

    "rq_purchasing_number" => "Mã PR",
    "rq_purchasing_urgent_date" => "Ngày nhận hàng dự kiến",
    "rq_purchasing_type" => "Nhóm hàng",
    "rq_purchasing_project" => "Dự án",
    "rq_purchasing_shipping_address" => "Địa chỉ nhận đơn hàng",
    "rq_common_info" => "Thông tin đề xuất",
    "rq_detail_info" => "Nội dung đề xuất",
    "rq_content_detail_info" => "Nội dung đề xuất",

    "rq_payee_detail_info" => "Đề nghị trả cho",
    "rq_requester_detail_info" => "Người đề xuất",

    "rq_note_detail_info" => "Ghi chú",
    "rq_note_confirm" => "Để lại ghi chú của bạn",

    "rq_amount_detail_info" => "Số tiền",

    "rq_amount_advanced_detail_info" => "Số tiền đã tạm ứng",
    // "rq_pay_detail_info" => "Request Detail Information",
    // "rq_take_detail_info" => "Request Detail Information",
    "rq_money_change_detail_info" => "Chi phí chênh lệch",
    "rq_payment_method_detail_info" => "Phương thức thanh toán",
    "rq_payment_cost_execute_detail_info" => "Số thực thi",
    "rq_payment_attached_file" => "Tập tin đính kèm",
    "rq_payment_department_detail" => "Chi trả cho phòng ban",
    "rq_payment_invoice" => "Số chứng từ",
    "rq_payment_invoice_date" => "Ngày chứng từ",
	
	"rq_payment_invoice_new" => "Số hóa đơn",
    "rq_payment_invoice_date_new" => "Ngày hóa đơn",

    "rq_payment_date" => "Ngày thanh toán",
    "rq_payment_update" => "Cập nhật thanh toán",
    "rq_confirm" => "Duyệt",


    "rq_finish_condition" => "Chỉ khi cập nhật thanh toán thành công thì mới xác nhận hoàn tất đề xuất",
    "rq_finish" => "Xác nhận đề xuất hoàn tất",

	"rq_is_coo" => "Đề xuất qua DIRECTOR?",
	"rq_month_year_fee" => "Chi phí cho tháng",
	"rq_request_type_group" => "Nhóm hạng mục",
	"rq_request_type" => "Hạng mục",
	"rq_budget" => "Dự trù",
	"rq_date_success" => "Ngày dự trù hoàn thành",
	"rq_cost_before" => "Tổng tiền dự kiến thanh toán",
	"rq_project" => "Dự án",
	"rq_contract_number" => "Số hợp đồng",
	"rq_contract_from" => "Ngày bắt đầu hợp đồng",
	"rq_contract_to" => "Ngày kết thúc hợp đồng",
	"rq_total_budget" => "Tổng dự trù",
	"rq_total_use" => "Tổng đã dùng",
	"rq_total_remaining" => "Dự trù còn lại",
	"rq_finance_staff" => "Qua kế toán duyệt?",
	"rq_request_id" => "Mã đề xuất",
	"finance_payment_date" => "Ngày thanh toán",
	"estimasted_liquidation_date" => "Ngày dự tính hoàn trả",


	//link: /staff-time/list-staff-approve

	'searchbox_search' => 'Tìm kiếm',
	'searchbox_fullname' => 'Họ và tên',
	'searchbox_area' => 'Khu vực',
	'searchbox_department' => 'Phòng ban',
	'searchbox_email' => 'email',
	'searchbox_year' => 'Năm',
	'searchbox_team' => 'Bộ phận',
	'searchbox_code' => 'Mã nhân viên',
	'searchbox_month' => 'Tháng',
	'searchbox_title' => 'Chức danh',
	'searchbox_button_search' => 'Tìm kiếm',
	'searchbox_button_export' => 'Tải về',
	'searchbox_select_option_all' => 'Tất cả',
	'searchbox_select_option_choose' => 'Chọn',

	'table_title_area' => 'Khu vực',
	'table_title_code' => 'Mã nhân viên',
	'table_title_fullname' => 'Họ tên',
	'table_title_title' => 'Chức danh',


	'button_approval' => 'Xác nhận',
	'button_approve' => 'Đồng ý',
	'button_reject' => 'Không đồng ý',


	'staff_approve_day' => 'Ngày',
	'title-view-staff-leave'	=> 'Duyệt công/phép',
	'button_list-staff-leave' => 'DS Phép NV',
	'button_list-staff-time' => 'DS Công NV',
	'pending-leaves' => 'Xác nhận phép',
	'pending_leaves_arer' => 'Khu vực',
	'pending_leaves_code' => 'Mã nhân viên',
	'pending_leaves_full-name' => 'Họ tên',
	'pending_leaves_title' => 'Vị trí',
	'pending_leaves_type-of-leaves' => 'Loại phép',
	'pending_leaves_form' => 'Từ ngày',
	'pending_leaves_to' => 'Đến ngày',
	'pending_leaves_due-date' => 'Ngày dự sinh',
	'pending_leaves_day-of-leave' => 'Số ngày nghỉ theo đơn xin',
	'pending_leaves_reason' => 'Lý do xin nghỉ',
	'pending_leaves_approval' => 'Quản lý trực tiếp',
	'pending_leaves_approve' => 'Đồng ý',
	'pending_leaves_reject' => 'Không đồng ý',
	'pending_leaves_hr' => 'Nhân sự',
	'pending_leaves_feed-back' => 'Phản hồi',
	'pending_time' => 'Xác nhận công',
	'pending_time_code' => 'Mã nhân viên',
	'pending_time_full-time' => 'Họ và tên',
	'pending_time_title' => 'Chức vụ',
	'pending_time_working-day' => 'Những ngày cần xác nhận công (Không/Quên chấm công)',
	'pending_time_attendance' => 'Những ngày cần xác nhận đi trễ/về sớm',
	'pending_time_approval' => 'Xác nhận',
	'pending_time_approve' => 'Đồng ý',
	'pending_time_reject' => 'Không đồng ý',
	'pending_time_detail' => 'Chi tiết',
	'pending_time_pop-up-details_date' => 'Ngày',
	'pending_time_pop-up-details_check-in' => 'Giờ vào	',
	'pending_time_pop-up-details_check-out' => 'Giờ ra',
	'pending_time_pop-up-details_working-day' => 'Ngày công',
	'pending_time_pop-up-details_approve-for' => 'Nội dung',
	'pending_time_pop-up-details_reason' => 'Lý do',
	'pending_time_pop-up-details_approval' => 'Xác nhận',
	'pending_time_pop-up-details_approve' => 'Đồng ý',
	'pending_time_pop-up-details_reject' => 'Không đồng ý',
	'pending_time-last-month' => 'Xác nhận công tháng trước',
	'pending_time-last-month_code' => 'Mã nhân viên',
	'pending_time-last-month_full-name' => 'Họ tên',
	'pending_time-last-month_title' => 'Chức vụ',
	'pending_time-last-month_working-pending' => 'Ngày cần bổ sung công',
	'pending_time-last-month_approval' => 'Xác nhận',
	'pending_time-last-month_details' => 'Chi tiết',


	'list_of_duty_day' => 'Danh sách nhân viên đi công tác ngoài trong tháng',
	'table_title_area' => 'KHU VỰC',
	'table_title_code' => 'MÃ NHÂN VIÊN',
	'table_title_fullname' => 'HỌ TÊN',
	'table_title_title' => 'CHỨC VỤ',
	'list_of_duty_day_pending' => 'NHỮNG NGÀY ĐI CÔNG TÁC NGOÀI',
	'list_of_duty_day_detail' => 'CHI TIẾT',

	'latitude_check_in' => 'Vĩ độ checkin',
	'longitude_check_in' => 'Kinh độ checkin',
	'location_on_googlemaps' => 'Vị trí trên Google Maps',

	'time_checkin' => 'Thời gian checkin',
	'location_checkin' => 'Địa điểm checkin',
	'office_checkin' => 'Văn phòng checkin',
	'time_checkout' => 'Thời gian checkout',
	'location_checkout' => 'Địa điểm checkout',
	'office_checkout' => 'Văn phòng checkout',

    //link: /staff-time/lock-staff-time
    'title_lock_staff_time' => 'Khoá công',
	
	
	
	
	
	
	
	
	
);
