<?php

function _curl($url = null, $pars = array()) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0');
//	curl_setopt($curl, CURLOPT_POST, true);
//	curl_setopt($curl, CURLOPT_POSTFIELDS, $pars);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, false);
    $res  = curl_exec($curl);
    curl_close($curl);
    return $res;
}

function curlResponse($success = true, $message = '', $data = [])
{
    return array(
        'success'=> $success,
        'message'=> $message,
        'data'=> $data
    );
}

function httpRequest($url, $params, $isPost = false) {
    $ch = curl_init();
    if (!$isPost) {
        $url .= '?' . http_build_query($params);
        $params = '';
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    if ($isPost) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
    }
    $data = curl_exec($ch);

    if (curl_errno($ch)) {
        return curlResponse(false, 'erp error,Curl error: ' . curl_error($ch));
    }
    $ret = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json = json_decode($data, true);
    if ($ret != 200) {
        return curlResponse(false, 'fail with status code '.$ret, $json);
    }
    return curlResponse(true, 'get active iot successful', $json);
}

function bcdechex($dec) {
    $hex = '';
    do {
        $last = bcmod($dec, 16);
        $hex  = dechex($last) . $hex;
        $dec  = bcdiv(bcsub($dec, $last), 16);
    } while ($dec > 0);
    return $hex;
}

function _getSign($md5_string) {
    $string_after = '';

    $byte_arr = unpack('c*', md5($md5_string, true));
    var_dump($byte_arr);
    for ($m = 0; $m < count($byte_arr); $m++) {
        $val = bindec($byte_arr[$m]);
        var_dump($val);
        if ($val < 16) {
            $val = "0";
        }echo "<pre>";
        var_dump($val);
        $string_after.=bin2hex($val);
    }

    return $string_after;
}

date_default_timezone_set('Asia/Shanghai');
set_time_limit(0);

define('_ENABLE', true);
define('GLOBAL_OPPO_API_URL', 'https://api.myoas.com');
//define('GLOBAL_OPPO_API_SECRET_CODE', '44@ade$68%e*ad');
define('GLOBAL_OPPO_API_SECRET_CODE', '71e73562e7194e5aa59e4e70d521f912');
define('COUNTRY_CODE', 84);
define('APP_ID', 'ESA_Foreign');
// define('SSH_HOST', '171.244.18.74');
// define('SSH_USERNAME', 'cronbot');
// define('SSH_PASSWORD', 'c%6gJA6AXb');
// define('PATH_CENTER_CRONJOB', '/var/www/html/center/cronjob/');
// define('DEV_PATH_CENTER_CRONJOB', '/home/cronjob/');

define('DB_NAME_CENTER', 'hr.');
define('DB_NAME_WAREHOUSE', 'warehouse.');

$current   = !empty($_GET['current']) ? $_GET['current'] : null;
$from_date = !empty($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime('-1 day', time()));
$to_date   = !empty($_GET['to_date']) ? $_GET['to_date'] : date('Y-m-d', strtotime('-1 day', time()));

if ($current) {
    $from_date = date('Y-m-d');
    $to_date   = date('Y-m-d');
}

$expl_from_date = explode('-', $from_date);
$expl_to_date   = explode('-', $to_date);

if (!checkdate($expl_from_date[1], $expl_from_date[2], $expl_from_date[0]))
    exit('Wrong from date.');
if (!checkdate($expl_to_date[1], $expl_to_date[2], $expl_to_date[0]))
    exit('Wrong to date.');

$from_date = strtotime($from_date);
$to_date   = strtotime($to_date);
$data      = array();

if (_ENABLE) {
    for ($i = $from_date; $i <= $to_date; $i += 86400) {
        //$date   = date('Y/m/d', $i);
        $date   = date('Y/m/d', $i);
        $t      = time();
        $path   = '/oppo/esa/foreign_register_iot_data';
        $url    = GLOBAL_OPPO_API_URL;
        $str    = "";
        $params = array(
            'app_id'       => APP_ID,
            'country_code' => COUNTRY_CODE,
            'reg_date'     =>  $date ,
            'timestamp'    => $t
        );

        $str .= $path . "\n";
        ksort($params);
        foreach ($params as $k => $v) {
            $str .= $k."=" . trim($v) . "\n";
        }
        $str .= GLOBAL_OPPO_API_SECRET_CODE;
        $sign           = md5($str);
        $params['sign'] = $sign;
        $url .= $path;
//                        echo "<pre>";
//        var_dump($url. '?' . http_build_query($params));
//        die;
        $res = httpRequest($url, $params);

        if ($res['success']) {
            $lst_active = $res['data'];
            if (!empty($lst_active)) {
                foreach ($lst_active as $key => $value)
                    $data[] = sprintf('("%s", "%s", "%s")', $value['IMEI'], $value['REGISTERTIME'], $value['REGTIME_BJ']
                    );
            }
        } else
            break;
    }
    if ($data) {
        $file_name = 'iot_activation_tmp.sql';
        $from_date = date('Y-m-d', $from_date);
        $to_date   = date('Y-m-d', $to_date);
        $query = '
			INSERT INTO ' . DB_NAME_CENTER . 'iot_activation_tmp VALUES' . implode(',', $data) . '
			ON DUPLICATE KEY UPDATE
				register_time = VALUES(register_time)
				, regtime_bj = VALUES(regtime_bj);

			CALL SP_sync_iot_active("' . $from_date . '", "' . $to_date . '");
		';

        $f = fopen('iot_activation_tmp.sql', 'w');
        fwrite($f, $query);
        fclose($f);
    }
   exit("Có tất cả " . count($data) . " imei");
    $db   = $data = null;
    exit('Finished. ');
}
exit('Process is off.');
