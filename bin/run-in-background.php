<?php
$options = getopt("c:t:");

if (isset($options['c']) && $options['c']) {
    // echo "Start executing command\n";
    run_command($options['c']);
}

if (isset($options['t']) && $options['t']) {
    // echo "Start running file\n";
    exec_file($options['t']);
}

function exec_file($file_to_exec)
{
    $dir = dirname(__FILE__);

    if (!file_exists($file_to_exec))
        $file_to_exec = $dir.DIRECTORY_SEPARATOR.$file_to_exec;

    if (!file_exists($file_to_exec))
        exit("File '$file_to_exec' not exists.");

    $datetime = date('Y-m-d H-i-s');
    $command = "php -f $file_to_exec";

    shell_exec(sprintf("$command > \"$dir/log/log - %s.txt\" &", $datetime));
}

function run_command($command)
{
    echo $command;
    $dir = dirname(__FILE__);
    $datetime = date('Y-m-d H-i-s');
    shell_exec(sprintf("$command > \"$dir/log/log - %s.txt\" &", $datetime));
}
