<?php

/**
 * @author: hero
 * Up file lên s3 server
 * description
 * date
 */
require_once(__DIR__ . '/Aws_s3/autoload.php');

class Aws_s3 {

    /** AWS S3 Bucket Name */
    private $bucket_name = 'oppoerp';

    /** AWS S3 Bucket Access Key ID */
    private $access_key_id = 'AKIAW7FU4CEBALI2O5TY';

    /** AWS S3 Bucket Secret Access Key */
    private $secret = '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';
    private $s3;

    public function __construct() {
        /** Let's initialize our AWS Client for the file uploads */
        $this->s3 = new \Aws\S3\S3Client([
            /** Region you had selected, if don't know check in S3 listing */
            'region'      => 'ap-southeast-1',
            'version'     => 'latest',
            /** Your AWS S3 Credential will be added here */
            'credentials' => [
                'key'    => $this->access_key_id,
                'secret' => $this->secret,
            ]
        ]);
    }

    public function uploadS3($file_location, $detination_path, $destination_name) {
        /** Since the SDK throw exception if any error
         * I am adding in try, catch
         */
        /** With the following code I am fetching the MIME type of the file */
        $finfo     = new finfo(FILEINFO_MIME_TYPE);
        $file_mime = $finfo->file($file_location);
        try {
            $aws_object = $this->s3->putObject([
                /** You bucket name */
                'Bucket'      => $this->bucket_name,
                /** This is the upload file name, you can change above */
                'Key'         => $detination_path . $destination_name,
                /** Give the complete path from where it needs to upload the file */
                'SourceFile'  => $file_location,
                /** Keep It Public Unless You dont want someone to access it
                 * You can skip the following if you want to keep it private
                 */
                'ACL'         => 'public-read',
                /** Make sure to add the following line,
                 * else it will download once you use the end URL to render
                 */
                // 'ContentType'   => 'image/jpeg'
                'ContentType' => $file_mime
            ]);

            /**
             * Uncomment the following for debugging the whole object
             */
            // echo '<pre>';
            // print_r($aws_object);

            /** To get the uploaded path of the image you can do 2 ways */
            /** Type 1 - Uploaded AWS S3 Bucket URL */
//            echo $aws_object['@metadata']['effectiveUri'];

            /** Type 2 - Uploaded AWS S3 Bucket URL */
//            echo $aws_object['ObjectURL'];

            return [
                'status'  => 1,
                'url'     => $aws_object['@metadata']['effectiveUri'],
                'url1'    => $aws_object['ObjectURL'],
                'message' => "ok"
            ];
        } catch (Aws\Exception\AwsException $e) {
            /** Handle the error if any */
//            return 'Error: ' . $e->getAwsErrorMessage();

            return [
                'status'  => 0,
                'message' => 'Error: ' . $e->getAwsErrorMessage()
            ];
        }
    }

}
