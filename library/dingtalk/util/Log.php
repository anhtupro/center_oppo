<?php

namespace dingtalk;

class Log {

    public static function i($msg) {
        self::write('I', $msg);
    }

    public static function e($msg) {
        self::write('E', $msg);
    }

    private static function write($level, $msg) {
        $filename = APPLICATION_PATH . "/../" . "isv.log";
        chmod($filename, 0777);
        $logFile  = fopen($filename, "aw");
        fwrite($logFile, $level . "/" . date(" Y-m-d h:i:s") . "  " . $msg . "\n");
        fclose($logFile);
    }

}
