<?php
namespace dingtalk;
class User
{
    
      private $http;
    public function __construct() {
        $this->http = new Http();
    }   

    public function getUserInfo($accessToken, $code)
    {
        $response = $this->http->get("/user/getuserinfo", 
            array("access_token" => $accessToken, "code" => $code));
        return $response;
    }

    public function get($accessToken, $userId)
    {
        $response = $this->http->get("/user/get",
            array("access_token" => $accessToken, "userid" => $userId));
        return $response;
    }

    public function simplelist($accessToken,$deptId){
        $response = $this->http->get("/user/simplelist",
            array("access_token" => $accessToken,"department_id"=>$deptId));
        return $response;

    }
    
    public function testsmg($accessToken)
    {
        
//        $response = $this->http->get("/topapi/message/corpconversation/asyncsend_v2", 
//            array("access_token" => $accessToken, "agent_id" => 249574564, "msg" => "{ 'Msgtype': 'text', 'văn bản': { 'nội dung': '消息 内容'}}"));
//        return $response;
        
//        $response = $this->http->get("/topapi/message/corpconversation/asyncsend_v2", 
//            array("access_token" => $accessToken, array("agent_id" => 249574564, "userid_list" => "235568690422-2052823153", 
//                "dept_id_list" => "", "to_all_user" => false, "msg" => '{ "Msgtype": "text", "văn bản": { "nội dung": "ahihi"}}')));
//        return $response;
        
        $response = $this->http->post("/topapi/message/corpconversation/asyncsend_v2",
            array("access_token" => $accessToken), array("agent_id" => "249574564", "userid_list" => "235568690422-2052823153", 
                "dept_id_list" => "", "to_all_user" => false, "msg" => '{ "Msgtype": "text", "văn bản": { "nội dung": "ahihi"}}'));
        return $response;
    }
}
