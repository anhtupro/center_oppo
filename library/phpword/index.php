<?php
    require_once 'vendor/autoload.php';
    $template = 'template.docx';
    $phpWord = new \PhpOffice\PhpWord\PhpWord();
    $section = $phpWord->addSection();
    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template);
    
    $templateProcessor->setImg('areaImages',array('src' => 'approved.jpg','swh'=>'100'));
    $templateProcessor->setValue('hoten','Hồ Chí Tưởng');
    $templateProcessor->saveAs('template.docx');
?>
