<?php

/**
 * 
 */
class My_Image_Barcode {

    public static function render($code = '', $uploaded_dir = null, $type = 'code39') {
        if (!$uploaded_dir)
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                    . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo'
                    . DIRECTORY_SEPARATOR . 'barcode';

        $barcode_file = $uploaded_dir . DIRECTORY_SEPARATOR . $code . '.jpg';

        if (!file_exists($barcode_file)) {

            $barcodeOptions = array(
                'text' => $code,
                'barHeight' => 50,
                'factor' => 1,
                'font' => 5,
                'stretchText' => true,
            );

            // No required options
            $rendererOptions = array();

            // Draw the barcode in a new image,
            $imageResource = Zend_Barcode::factory(
                            $type, 'image', $barcodeOptions, $rendererOptions
            );

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $rc = $imageResource->draw();

            imagejpeg($rc, $barcode_file, 100);
            imagedestroy($rc);
        }
    }

    public static function renderNoCode($code = '', $uploaded_dir = null, $type = 'code39') {
        if (!$uploaded_dir)
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                    . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo'
                    . DIRECTORY_SEPARATOR . 'barcode2';

        $barcode_file = $uploaded_dir . DIRECTORY_SEPARATOR . $code . '.jpg';

        if (!file_exists($barcode_file)) {

            $barcodeOptions = array(
                'text' => $code,
                'barHeight' => 50,
                'factor' => 1,
                'font' => 5,
                'stretchText' => true,
                'drawText' => false
            );



            // No required options
            $rendererOptions = array();

            // Draw the barcode in a new image,
            $imageResource = Zend_Barcode::factory(
                            $type, 'image', $barcodeOptions, $rendererOptions
            );

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $rc = $imageResource->draw();

            imagejpeg($rc, $barcode_file, 100);
            imagedestroy($rc);
        }
    }

    public static function renderQR($data, $width = 200, $height = 200, $uploaded_dir = null) {
 
        $sn = isset($data['sn']) ? $data['sn'] : null;
        $url = 'https://chart.googleapis.com/chart?';
        $dataUrl = HOST . 'event/year-end-check-in?id=' . base64_encode($sn);
        $params = array(
            'cht' => 'qr',
            'chs' => (int) $width . 'x' . (int) $height,
            'chl' => $dataUrl
        );

        $url .= http_build_query($params);

        if (isset($url) and $url) {
            if (!$uploaded_dir)
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                        . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo'
                        . DIRECTORY_SEPARATOR . 'qrcode-2020-final';

            $barcode_file = $uploaded_dir . DIRECTORY_SEPARATOR . $sn . '.jpg';
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            if (!file_exists($barcode_file)) {
                My_Image_Barcode::saveFileFromTheWeb($url, $barcode_file);
            }
            
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            $name = $sn . '.jpg';
            $s3_lib->uploadS3($barcode_file, "photo/qrcode-2020-final/", $name);            
        }
        return $url;
    }

    public static function saveFileFromTheWeb($remoteFile, $localFile) {
     
        $fp = fopen($localFile, 'w+');
      
        $ch = curl_init($remoteFile);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// thanh add
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
        // curl_setopt($ch, CURLOPT_VERBOSE, true);   // disable debug
        curl_exec($ch);
        curl_close($ch);                              // closing curl handle
        fclose($fp);
    }

    public static function getQRCodeUrl($sn) {
        return HOST . 'photo/qrcode/' . $sn . '.jpg';
    }

}
