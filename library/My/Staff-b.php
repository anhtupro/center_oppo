<?php
/**
*
*/
class My_Staff
{
    /**
     * Khi nhân viên nghỉ (set ngày off_date), dùng phương thức này xóa hết các quyền liên qua khu vực, quyền cấp riêng
     * @param  [type] $staff_id [description]
     * @return [type]           [description]
     */
    public static function clear_all_roles($staff_id = null)
    {
        if (is_null($staff_id) || intval($staff_id) <= 0) throw new Exception("Invalid ID");

        $staff_id = intval($staff_id);

        $QStaff = new Application_Model_Staff();
        $staff_check = $QStaff->find($staff_id);
        $staff_check = $staff_check->current();

        if (!$staff_check) throw new Exception("Invalid staff ID");

        $QAsm = new Application_Model_Asm();
        $where = $QAsm->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $QAsm->delete($where);

        $QAsm_Standby = new Application_Model_AsmStandby();
        $where = $QAsm_Standby->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $QAsm_Standby->delete($where);

        $QSalesAdmin = new Application_Model_SalesAdmin();
        $where = $QSalesAdmin->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $QSalesAdmin->delete($where);

        $QStaffPriviledge = new Application_Model_StaffPriviledge();
        $where = $QStaffPriviledge->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $QStaffPriviledge->delete($where);
    }

    public static function getOwnStores($staff_id)
    {
        $QLog     = new Application_Model_StoreStaffLog();
        $page     = 1;
        $limit    = LIMITATION;
        $total    = 0;

        $params = array(
            'staff_id' => $staff_id,
            'from'     => date('Y-m-01'),
            'to'       => date('Y-m-d'),
        );

        $logs = $QLog->fetchPagination($page, $limit, $total, $params);
        $stores = array();

        foreach ($logs as $key => $item) {
            $stores[] = array(
                'store_name' => $item['store_name'],
                'store_id' => $item['store_id'],
                'from' => isset($item['joined_at']) ? date('d/m/Y', $item['joined_at']) : 'n/a',
                'to'   => isset($item['released_at']) ? date('d/m/Y', $item['released_at']) : '',
            );
        }

        return $stores;
    }

    /*
    * @thaisang.nguyen 
    * Function lấy danh sách store của nhân viên (chạy trong controller UserInformation)
    * input: staff_id
    * output: danh sách các store cùng thời gian đứng
    */
    public static function getOwnStoresUserInformation($staff_id)
    {
        $QLog     = new Application_Model_StoreStaffLog();
        $page     = 1;
        // $limit    = LIMITATION;
        $total    = 0;

        $params = array(
            'staff_id' => $staff_id,
        );

        $logs = $QLog->fetchPagination($page, $limit, $total, $params);
        $stores = array();

        foreach ($logs as $key => $item) {
            $stores[] = array(
                'store_name' => $item['store_name'],
                'store_id' => $item['store_id'],
                'from' => isset($item['joined_at']) ? date('d/m/Y', $item['joined_at']) : 'n/a',
                'to'   => isset($item['released_at']) ? date('d/m/Y', $item['released_at']) : '',
            );
        }

        return $stores;
    }

    public static function getImeiExpired($staff_id)
    {
        $QImei = new Application_Model_TimingSaleExpired();
        $result = $QImei->getAllImeiExpired($staff_id);
        return $result;
    }

    /**
     * Remove
     * @param  [type] $staff_id  [description]
     * @param  [type] $old_title [description]
     * @return [type]            [description]
     */
    public static function removeStoreForTransfer($staff_id, $old_title, $date)
    {
        if (in_array($old_title, array(SALES_TITLE, PGPB_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE,PB_SALES_TITLE)))
            self::_removeStoreSale($staff_id, $date);
        elseif ($old_title == LEADER_TITLE) {
            self::_removeStoreSale($staff_id, $date);
            self::_removeStoreLeader($staff_id, $date);
        }
    }

    /**
     * [removeStoreSale description]
     * @param  [type] $staff_id [description]
     * @param  [type] $time     [description]
     * @return [type]           [description]
     */
    private static function _removeStoreSale($staff_id, $date)
    {
        $QStoreStaff = new Application_Model_StoreStaff();
        $where = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', intval($staff_id));
        $QStoreStaff->delete($where);

        $QStoreStaffLog = new Application_Model_StoreStaffLog();
        $where = array();
        $where[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at = 0 OR released_at IS NULL', 1);
        $where[] = $QStoreStaffLog->getAdapter()->quoteInto('staff_id = ?', intval($staff_id));
        $data = array('released_at' => strtotime($date));
        $QStoreStaffLog->update($data, $where);
    }

    /**
     * [removeStoreLeader description]
     * @param  [type] $staff_id [description]
     * @param  [type] $time     [description]
     * @return [type]           [description]
     */
    private static function _removeStoreLeader($staff_id, $date)
    {
        $QStoreLeader = new Application_Model_StoreLeader();
        $where = $QStoreLeader->getAdapter()->quoteInto('staff_id = ?', intval($staff_id));
        $QStoreLeader->delete($where);

        $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
        $where = array();
        $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at = 0 OR released_at IS NULL', 1);
        $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('staff_id = ?', intval($staff_id));
        $data = array('released_at' => strtotime($date));
        $QStoreLeaderLog->update($data, $where);
    }

    public static function validateStaffForAsm($params)
    {
        $QStaff = new Application_Model_Staff();
        $QRegionalMarket = new Application_Model_RegionalMarket();
        $QAsm = new Application_Model_Asm();

        if (!isset($params['asm_id']) || !$params['asm_id'])
            throw new Exception("Invalid ASM ID", 6);

        if (isset($params['staff_id']) && intval($params['staff_id'])) {
            $staff_id = intval($params['staff_id']);
            $where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $staff_check = $QStaff->fetchRow($where);
            if (!$staff_check) throw new Exception("Invalid id", 1);

        } elseif (isset($params['email']) && $params['email']) {
            $where = $QStaff->getAdapter()->quoteInto('trim(BOTH FROM email) LIKE ?', str_replace(EMAIL_SUFFIX, '', trim($params['email'])).EMAIL_SUFFIX);
            $staff_check = $QStaff->fetchRow($where);
            if (!$staff_check) throw new Exception("Invalid email", 2);
            $staff_id = $staff_check['id'];

        } else {
            throw new Exception("Invalid params", 3);
        }

        if ($staff_check['group_id'] != PGPB_ID || !My_Staff_Title::isPg($staff_check['title']))
            throw new Exception("This staff is not a PG", 9);

        if (!isset($staff_check['regional_market']) || !$staff_check['regional_market'])
            throw new Exception("Invalid region", 4);

        $where = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $staff_check['regional_market']);
        $regional_market_check = $QRegionalMarket->fetchRow($where);
        if (!$regional_market_check) throw new Exception("Invaid region", 5);

        $asm_cache = $QAsm->get_cache();
        if (!isset($asm_cache[ $params['asm_id'] ])
            || !isset($asm_cache[ $params['asm_id'] ]['province'])
            || !is_array($asm_cache[ $params['asm_id'] ]['province'])
            || !count($asm_cache[ $params['asm_id'] ]['province']))
            throw new Exception("This user is not a valid ASM", 7);

        if (!in_array($staff_check['regional_market'], $asm_cache[ $params['asm_id'] ]['province']))
            throw new Exception("This staff does not belong to this ASM", 8);

        return true;
    }

    /**
     * kiểm tra nhân viên này có chấm công vào ngày chuyển công tác, nghỉ việc, gỡ shop không
     * @param  [type] $staff_id [description]
     * @param  [type] $date     [description]
     * @return boolean - true: có chấm công rồi; false: chưa có chấm công
     */
    public static function checkIfHaveTiming($staff_id, $date, $store_id = null)
    {
        $QTiming = new Application_Model_Timing();
        $where = array();
        $where[] = $QTiming->getAdapter()->quoteInto('DATE(`from`) = ?', $date);
        $where[] = $QTiming->getAdapter()->quoteInto('staff_id = ?', intval($staff_id));

        if ($store_id)
            $where[] = $QTiming->getAdapter()->quoteInto('store = ?', intval($store_id));

        $timing_check = $QTiming->fetchRow($where);
        if ($timing_check) return true;

        return false;
    }

    public static function isPgTitle($title = null)
    {
        return My_Staff_Title::isPg($title);
    }

    public static function pgTitleArray()
    {
        return My_Staff_Title::getPg();
    }
}
