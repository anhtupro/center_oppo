<?php
class My_Report_Hr
{
    public static function kpiPg($from, $to, array $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if($userStorage->id != SUPERADMIN_ID){
            // echo "Hệ thống đang cập nhật chức năng vui lòng quay lại sau";
            // exit;
        }

        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        $name            = isset($params['name']) ? $params['name'] : null;
        $regional_market = isset($params['regional_market']) ? $params['regional_market'] : null;
        $area            = isset($params['area']) ? $params['area'] : null;
        $district        = isset($params['district']) ? $params['district'] : null;
        $store           = isset($params['store']) ? $params['store'] : null;
        $distributor_id  = isset($params['distributor_id']) ? $params['distributor_id'] : null;
        $export          = isset($params['export']) ? $params['export'] : null;
        $kpi_area        = true;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $run_row = 1;

        // các model có đổi giá
        $QKpi = new Application_Model_GoodKpiLog();
        $list_area_kpi = $QKpi->get_list(date('Y-m-d', strtotime($from)), date('Y-m-d', strtotime($to)), My_Kpi_Object::Pg, $kpi_area);
        
        $QGood          = new Application_Model_Good();
        $products       = $QGood->get_cache();
        
        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();
        
        $QArea          = new Application_Model_Area();
        $area_cache     = $QArea->get_cache();

        $QRegion        = new Application_Model_RegionalMarket();
        $region_cache   = $QRegion->get_cache_all();

        $db = Zend_Registry::get('db');

        $QImeiKpi = new Application_Model_ImeiKpi($export);

        foreach ($list_area_kpi as $kpi_area_id => $list) {

            $heads = array(
                'KPI Khu vực',
                'STT',
                'Mã NV',
                'NV KV Phụ trách',
                'Ngày công',
                'Tên cửa hàng',
                'Tổng cộng',
            );

            $product_col = chr(ord('A') + count($heads));
            $product_color_list = array();

            foreach ($products as $key => $value)
            {
                // các model có đổi giá, tách thành nhiều cột
                if (isset($list[$key]))
                {
                    foreach ($list[$key] as $_color => $ranges)
                    {
                        if ($_color)
                        {
                            $product_color_list[$key][] = $_color;

                            foreach ($ranges as $range)
                                $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                    format('d') . '->' . (new DateTime($range['to']))->format('d');
                        } else
                        {
                            foreach ($ranges as $range)
                                $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                        }
                    }
                }
            }

            $heads[] = 'Cộng';
            $heads[] = 'Thưởng KPI';
            $heads[] = 'Hỗ trợ KPI';

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            $alpha = 'A';
            $index = $run_row;

            foreach ($heads as $key)
            {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }

            $index++;

            $alpha = $product_col;
            foreach ($products as $key => $value){
                // các model có đổi giá, tách thành nhiều cột
                if (isset($list[$key])){
                    foreach ($list[$key] as $_color => $ranges){
                        foreach ($ranges as $range){
                            $sheet->setCellValue($alpha++ . $index, $range['kpi']);
                        }
                    }    
                }
            }
                
            $index++;

            // danh sách tất cả NV PG được tính KPI
            // dựa vào danh sách và thứ tự trong danh sách này để điền chính xác vào file excel
            $sql = "SELECT DISTINCT s.id, s.`code`, s.regional_market, s.firstname, s.lastname, i.kpi_area
                FROM staff s INNER JOIN imei_kpi i ON s.id=i.pg_id AND i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id
                WHERE DATE(i.timing_date) >= ? AND DATE(i.timing_date) <= ?
                AND s.regional_market <> 0 AND s.regional_market IS NOT NULL";

            if ($name) $sql .= " AND CONCAT(TRIM(BOTH FROM s.firstname), ' ', TRIM(BOTH FROM s.lastname)) LIKE '%".$name."%' ";
            if ($store) $sql .= " AND i.store_id = " . intval($store) . " ";

            if ($distributor_id) {
                $store_sql = "SELECT id FROM store WHERE d_id = ?";
                $store_of_distributor = $db->query($store_sql, array(intval($distributor_id)));
                $store_of_distributor_arr = array();

                foreach ($store_of_distributor as $key => $value) {
                    $store_of_distributor_arr[] = $value['id'];
                }

                $store_of_distributor_str = implode(',', $store_of_distributor_arr);

                if (isset($store_of_distributor_str) && !empty($store_of_distributor_str))
                    $sql .= " AND i.store_id IN (".$store_of_distributor_str.") ";
            }

            if($kpi_area){
                $sql .= ' AND kpi_area = '. $kpi_area_id ;
            }


            $all_staff = $db->query($sql, array($from, $to));

            // dùng mảng này để lưu thứ tự các staff trong result set trên
            // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
            $staff_array = array();
            $staff_list = array();

            // điền danh sách staff ra file trước
            foreach ($all_staff as $key => $staff)
            {
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, isset($area_cache[$staff['kpi_area']]) ? $area_cache[$staff['kpi_area']] : 'CÔNG TY');
                $sheet->setCellValue($alpha++ . $index, isset($area_cache[$region_cache[$staff['regional_market']]['area_id']]) ?
                    $area_cache[$region_cache[$staff['regional_market']]['area_id']] : '');
                $sheet->setCellValue($alpha++ . $index, $staff['code']);
                $sheet->setCellValue($alpha++ . $index, $staff['firstname'] . ' ' . $staff['lastname']);

                $staff_array[$staff['id']] = $index; // lưu dòng ứng với staff id
                $staff_list[] = $staff['id']; // dùng để fetchGrid
                $index++;


                $run_row = $index;
            }

            
            $params = array(
                'kpi'            => 1,
                'pg'             => $staff_list,
                'from'           => $from,
                'to'             => $to,
                'area'           => $area,
                'province'       => $regional_market,
                'district'       => $district,
                'store'          => $store,
                'distributor_id' => $distributor_id,
                'kpi_area_id'    => $kpi_area_id   
            );

            $sell_out = $QImeiKpi->fetchGrid($params);
            $sell_out_list = array();

            // sắp xếp $sell_out theo staff, rồi theo good_id, color...
            foreach ($sell_out as $_key => $_so) {
                $sell_out_list[ $_so['staff_id'] ][ $_so['good_id'] ][ $_so['color_id'] ][ $_so['from_date'].'_'.$_so['to_date'] ] = array(
                    'sell_out'      => $_so['quantity'],
                    'activated'     => $_so['activated'],
                    'non_activated' => $_so['non_activated'],
                    'kpi'           => $_so['kpi'],
                );
            }

            // duyệt qua dãy model để tính KPI,
            // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
            $alpha = $product_col;
            $kpi_list = array();
            $quantity_list = array();

            foreach ($products as $_product_id => $value)
            {
                // các model có đổi giá, tách thành 2 cột
                if (isset($list[$_product_id])) {
                    foreach ($list[$_product_id] as $_color => $ranges) {
                        foreach ($ranges as $range) {
                            foreach ($sell_out_list as $_staff_id => $_data) {
                                // nếu có trong danh sách NV ở trên
                                // đưa vào đúng dòng luôn
                                if (isset($staff_array[$_staff_id])) {
                                    $sheet->setCellValue(
                                        $alpha . $staff_array[$_staff_id],
                                        isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                            ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                            : 0
                                    );

                                    if (!isset($kpi_list[ $_staff_id ]))
                                        $kpi_list[ $_staff_id ] = 0;

                                    $kpi_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi'])
                                        ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi']
                                        : 0;

                                    if (!isset($quantity_list[ $_staff_id ]))
                                        $quantity_list[ $_staff_id ] = 0;

                                    $quantity_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                        ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                        : 0;

                                }
                            }

                            $alpha++;
                        }
                    }
                }
            }

            foreach ($quantity_list as $_staff_id => $_qty) {
                if (isset($staff_array[$_staff_id])) {
                    $sheet->setCellValue(
                        $alpha . $staff_array[$_staff_id],
                        $_qty
                    );
                }
            }

            $alpha++;

            foreach ($kpi_list as $_staff_id => $_kpi) {
                if (isset($staff_array[$_staff_id])) {
                    $sheet->setCellValue(
                        $alpha . $staff_array[$_staff_id],
                        $_kpi
                    );
                }
            }

            // tính KPI hỗ trợ
            $params = array(
                'kpi'            => 2, // đánh dấu khác cái trên
                'pg'             => $staff_list,
                'from'           => $from,
                'to'             => $to,
                'area'           => $area,
                'province'       => $regional_market,
                'district'       => $district,
                'store'          => $store,
                'distributor_id' => $distributor_id,
            );

            $kpi_support = $QImeiKpi->fetchGrid($params);
            $kpi_support_list = array();

            // sắp xếp $sell_out theo staff, rồi theo good_id, color...
            foreach ($kpi_support as $_key => $_so)
                $kpi_support_list[ $_so['staff_id'] ] = $_so['kpi_support'];

            $alpha++;

            foreach ($kpi_support_list as $_staff_id => $_kpi) {
                if (isset($staff_array[$_staff_id])) {
                    $sheet->setCellValue(
                        $alpha . $staff_array[$_staff_id],
                        $_kpi
                    );
                }
            }

        }// End foreach kpi area
        


        $filename = 'Sell out - PG - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }
	
    public static function kpiStoreLeader($from, $to, array $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if($userStorage->id != SUPERADMIN_ID){
            // echo "Hệ thống đang cập nhật chức năng vui lòng quay lại sau";
            // exit;
        }

        $area = null;


        if($userStorage->title == SALES_ADMIN_TITLE){
            $QAsm = new Application_Model_Asm();
            $where = $QAsm->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $result = $QAsm->fetchAll($where);
            //
            $area_id = array();
            foreach ($result as $key => $value) {
                $area_id[] = $value['area_id'];
            }

            $area = implode(",",$area_id);
        }

        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);


        $QImeiKpi = new Application_Model_ImeiKpi2();
        $data = $QImeiKpi->getKPIByStoreLeader($from,$to,$area);
        // echo "<pre>";print_r($data);die;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Staff ID',
            'B' => 'Staff',
            'C' => 'Code',
            'D' => 'Area',
            'E' => 'Title',
            'F' => 'Team',
            'G' => 'Off Date',
            'H' => 'Store ID',
            'I' => 'Store Name',
            'J' => 'Model',
            'K' => 'Desc',
            'L' => 'Policy',
            'M' => 'Sellout',
            'N' => 'KPI',
            'O' => 'Total',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        $sheet->setCellValue($key.'1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(30);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(10);
        $sheet->getColumnDimension('N')->setWidth(10);
        $sheet->getColumnDimension('O')->setWidth(20);


        foreach($data as $key => $value){
            $sheet->setCellValue('A'.($key + 2), $value['staff_id']);
            $sheet->setCellValue('B'.($key + 2), $value['staff']);
            $sheet->setCellValue('C'.($key + 2), $value['code']);
            $sheet->setCellValue('D'.($key + 2), $value['area']);
            $sheet->setCellValue('E'.($key + 2), $value['title']);
            $sheet->setCellValue('F'.($key + 2), $value['team']);
            $sheet->setCellValue('G'.($key + 2),($value['off_date'] ? DateTime::createFromFormat('Y-m-d', $value['off_date'])->format('d-m-Y') : NULL));
            $sheet->setCellValue('H'.($key + 2), $value['store_id']);
            $sheet->setCellValue('I'.($key + 2), $value['store_name'] ? $value['store_name'] : null);
            $sheet->setCellValue('J'.($key + 2), $value['good'] ? $value['good'] : null);
            $sheet->setCellValue('K'.($key + 2), $value['desc'] ? $value['desc'] : null);
            $sheet->setCellValue('L'.($key + 2), $value['policy'] ? $value['policy'] : null);
            $sheet->setCellValue('M'.($key + 2), $value['sellout']);
            $sheet->setCellValue('N'.($key + 2), $value['kpi']);
            $sheet->setCellValue('O'.($key + 2), $value['total']);
        }

        

        $filename = 'Report Store Leader_'.date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        $objWriter->save('php://output');

        exit;

        // echo "<pre>";print_r($data);die;
    }

    public static function kpiPgNew($from, $to, array $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if($userStorage->id != SUPERADMIN_ID){
            // echo "Hệ thống đang cập nhật chức năng vui lòng quay lại sau";
            // exit;
        }

        $area = null;


        if($userStorage->title == SALES_ADMIN_TITLE){
            $QAsm = new Application_Model_Asm();
            $where = $QAsm->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $result = $QAsm->fetchAll($where);
            //
            $area_id = array();
            foreach ($result as $key => $value) {
                $area_id[] = $value['area_id'];
            }

            $area = implode(",",$area_id);
        }

        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);


        $QImeiKpi = new Application_Model_ImeiKpi2();
        $data = $QImeiKpi->getKPIByPg($from,$to,$area);


        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Staff ID',
            'B' => 'Staff',
            'C' => 'Code',
            'D' => 'Area',
            'E' => 'Title',
            'F' => 'Team',
            'G' => 'Off Date',
            'H' => 'Store ID',
            'I' => 'Store Name',
            'J' => 'Model',
            'K' => 'Desc',
            'L' => 'Policy',
            'M' => 'Sellout',
            'N' => 'KPI',
            'O' => 'Total',
            'P' => 'Store Staff',
            'Q' => 'From',
            'R' => 'To'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        $sheet->setCellValue($key.'1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(30);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(10);
        $sheet->getColumnDimension('N')->setWidth(10);
        $sheet->getColumnDimension('O')->setWidth(20);
        $sheet->getColumnDimension('P')->setWidth(30);
        $sheet->getColumnDimension('Q')->setWidth(15);
        $sheet->getColumnDimension('R')->setWidth(25);


        foreach($data as $key => $value){
            $sheet->setCellValue('A'.($key + 2), $value['staff_id']);
            $sheet->setCellValue('B'.($key + 2), $value['staff']);
            $sheet->setCellValue('C'.($key + 2), $value['code']);
            $sheet->setCellValue('D'.($key + 2), $value['area']);
            $sheet->setCellValue('E'.($key + 2), $value['title']);
            $sheet->setCellValue('F'.($key + 2), $value['team']);
            $sheet->setCellValue('G'.($key + 2), ($value['off_date'] ? DateTime::createFromFormat('Y-m-d', $value['off_date'])->format('d-m-Y') : NULL));
            $sheet->setCellValue('H'.($key + 2), $value['store_id']);
            $sheet->setCellValue('I'.($key + 2), $value['store_name'] ? $value['store_name'] : null);
            $sheet->setCellValue('J'.($key + 2), $value['good'] ? $value['good'] : null);
            $sheet->setCellValue('K'.($key + 2), $value['desc'] ? $value['desc'] : null);
            $sheet->setCellValue('L'.($key + 2), $value['policy'] ? $value['policy'] : null);
            $sheet->setCellValue('M'.($key + 2), $value['sellout']);
            $sheet->setCellValue('N'.($key + 2), $value['kpi']);
            $sheet->setCellValue('O'.($key + 2), $value['total']);
            $sheet->setCellValue('P'.($key + 2), $value['store_on']);
            $sheet->setCellValue('Q'.($key + 2), $value['joined_at']);
            $sheet->setCellValue('R'.($key + 2), $value['released_at']);
        }

        

        $filename = 'Report KPI_'.date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        $objWriter->save('php://output');

        exit;

        // echo "<pre>";print_r($data);die;
    }

	public static function kpiPgAccessories($from, $to, array $params)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);
        
        $name            = isset($params['name']) ? $params['name'] : null;
        $regional_market = isset($params['regional_market']) ? $params['regional_market'] : null;
        $area            = isset($params['area']) ? $params['area'] : null;
        $district        = isset($params['district']) ? $params['district'] : null;
        $store           = isset($params['store']) ? $params['store'] : null;
        $distributor_id  = isset($params['distributor_id']) ? $params['distributor_id'] : null;
        $export          = isset($params['export']) ? $params['export'] : null;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Mã NV',
            'NV KV Phụ trách',
        );

        // các model có đổi giá
        $QKpi = new Application_Model_GoodKpiLog();
        $list = $QKpi->get_list_access(date('Y-m-d', strtotime($from)), date('Y-m-d', strtotime($to)), My_Kpi_Object::Pg);

        $QGood = new Application_Model_Good();
        $products = $QGood->get_cache_access();
        
        $QGoodColor = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value)
        {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
            {
                foreach ($list[$key] as $_color => $ranges)
                {
                    if ($_color)
                    {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else
                    {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Cộng';
        $heads[] = 'Thưởng KPI';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['kpi']);
    
        $index++;

        $QArea = new Application_Model_Area();
        $area_cache = $QArea->get_cache();
        $QRegion = new Application_Model_RegionalMarket();
        $region_cache = $QRegion->get_cache_all();

        $db = Zend_Registry::get('db');

        // danh sách tất cả NV PG được tính KPI
        // dựa vào danh sách và thứ tự trong danh sách này để điền chính xác vào file excel
        
        /*Lấy tất cả PG có báo số(có trong bảng timing_accessories)
        $sql = "SELECT DISTINCT a.id, a.`code`, a.regional_market, a.firstname, a.lastname
                FROM staff as a
                INNER JOIN timing as b ON a.id = b.staff_id
                INNER JOIN timing_accessories as c ON b.id = c.timing_id 
                WHERE DATE(b.created_at) >= ? AND DATE(b.created_at) <= ? 
                AND a.regional_market <> 0 AND a.regional_market IS NOT NULL
				AND b.status_accessories = 1";
        */
        
        //Lấy tất cả PG, PB sale
        $sql = "SELECT DISTINCT a.id, a.`code`, a.regional_market, a.firstname, a.lastname
                FROM staff as a
				INNER JOIN timing as b ON a.id = b.staff_id
                INNER JOIN timing_accessories as c ON b.id = c.timing_id 
                WHERE a.title IN (312,182,293,419) AND off_date IS NULL
				ORDER BY c.id DESC";
        
        
        if ($name) $sql .= " AND CONCAT(TRIM(BOTH FROM a.firstname), ' ', TRIM(BOTH FROM a.lastname)) LIKE '%".$name."%' ";
        //$all_staff = $db->query($sql, array($from, $to));
        $all_staff = $db->query($sql);
        
        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $staff_array = array();
        $staff_list = array();

        // điền danh sách staff ra file trước
        foreach ($all_staff as $key => $staff)
        {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, isset($area_cache[$region_cache[$staff['regional_market']]['area_id']]) ?
                $area_cache[$region_cache[$staff['regional_market']]['area_id']] : '');
            $sheet->setCellValue($alpha++ . $index, $staff['code']);
            $sheet->setCellValue($alpha++ . $index, $staff['firstname'] . ' ' . $staff['lastname']);

            $staff_array[$staff['id']] = $index; // lưu dòng ứng với staff id
            $staff_list[] = $staff['id']; // dùng để fetchGrid
            $index++;
        }
        
        //Format Y-m-d
        $from_date = date('Y-m-d', strtotime($from));
        
        //START================================
        $sql = "SELECT staff_id,good_id,SUM(quantity) as total_qty,CASE WHEN from_date < ? THEN ? ELSE from_date END AS `from_date` 
                FROM(
                SELECT DISTINCT b.staff_id,a.good_id,a.quantity,b.created_at, c.from_date, c.to_date, c.id
                from timing_accessories AS a
                LEFT JOIN timing AS b ON a.timing_id = b.id
                LEFT JOIN good_kpi_log AS c ON a.good_id = c.good_id AND b.created_at >= DATE(c.from_date) AND ( c.to_date IS NULL OR  DATE(b.created_at) <= DATE(c.to_date) )
                WHERE b.status_accessories = 1 AND DATE(b.created_at) >= ? AND DATE(b.created_at) <= ?
                GROUP BY a.id) AS t 
                GROUP BY staff_id,good_id,from_date
                ";
        
        $sell_out = $db->query($sql, array($from_date,$from_date,$from, $to));
        $sell_out_list_by_staff = array();
        
        // sắp xếp $sell_out theo staff
        foreach ($sell_out as $key => $value){
            $sell_out_list_by_staff
                [ $value['staff_id'] ]
                [ $value['good_id'] ]
                [ $value['from_date'] ] = $value['total_qty'];
        }
        
        
        $kpi_list = array();
        $quantity_list = array();
        
        foreach($staff_list as $staff){
            $alpha = $product_col;
            foreach ($products as $key => $value){
                // các model có đổi giá, tách thành nhiều cột
                if (isset($list[$key]))
                    foreach ($list[$key] as $_color => $ranges)
                        foreach ($ranges as $range){
                            $sheet->setCellValue($alpha++ . $staff_array[$staff], isset($sell_out_list_by_staff[$staff][$key][$range['from']]) ? $sell_out_list_by_staff[$staff][$key][$range['from']] : 0);
                            
                            if (!isset($kpi_list[$staff]))
                                $kpi_list[$staff] = 0;

                            $kpi_list[$staff] += isset($sell_out_list_by_staff[$staff][$key][$range['from']]) ? ($sell_out_list_by_staff[$staff][$key][$range['from']])*($range['kpi']) : 0;

                            if (!isset($quantity_list[$staff]))
                                $quantity_list[$staff] = 0;

                            $quantity_list[$staff] += isset($sell_out_list_by_staff[$staff][$key][$range['from']]) ? $sell_out_list_by_staff[$staff][$key][$range['from']] : 0;
                        }
            }
        }
        
        foreach ($quantity_list as $_staff_id => $_qty) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_qty
                );
            }
        }

        $alpha++;

        foreach ($kpi_list as $_staff_id => $_kpi) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_kpi
                );
            }
        }
        
        //=====================================

        $filename = 'Sell out - PG - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function kpiSaleNew($from, $to, array $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if($userStorage->id != SUPERADMIN_ID){
            // echo "Hệ thống đang cập nhật chức năng vui lòng quay lại sau";
            // exit;
        }

        $area = null;


        if($userStorage->title == SALES_ADMIN_TITLE){
            $QAsm = new Application_Model_Asm();
            $where = $QAsm->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $result = $QAsm->fetchAll($where);
            //
            $area_id = array();
            foreach ($result as $key => $value) {
                $area_id[] = $value['area_id'];
            }

            $area = implode(",",$area_id);
        }

        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);


        $QImeiKpi = new Application_Model_ImeiKpi2();
        $data = $QImeiKpi->getKPIBySale($from,$to,$area);


        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Staff ID',
            'B' => 'Staff',
            'C' => 'Code',
            'D' => 'Area',
            'E' => 'Title',
            'F' => 'Team',
            'G' => 'Off Date',
            'H' => 'Store ID',
            'I' => 'Store Name',
            'J' => 'Model',
            'K' => 'Desc',
            'L' => 'Policy',
            'M' => 'Sellout',
            'N' => 'KPI',
            'O' => 'Total',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        $sheet->setCellValue($key.'1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(30);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(10);
        $sheet->getColumnDimension('N')->setWidth(10);
        $sheet->getColumnDimension('O')->setWidth(20);


        foreach($data as $key => $value){
            $sheet->setCellValue('A'.($key + 2), $value['staff_id']);
            $sheet->setCellValue('B'.($key + 2), $value['staff']);
            $sheet->setCellValue('C'.($key + 2), $value['code']);
            $sheet->setCellValue('D'.($key + 2), $value['area']);
            $sheet->setCellValue('E'.($key + 2), $value['title']);
            $sheet->setCellValue('F'.($key + 2), $value['team']);
            $sheet->setCellValue('G'.($key + 2), $value['off_date'] ? DateTime::createFromFormat('Y-m-d', $value['off_date'])->format('d-m-Y') : null);
            $sheet->setCellValue('H'.($key + 2), $value['store_id']);
            $sheet->setCellValue('I'.($key + 2), $value['store_name'] ? $value['store_name'] : null);
            $sheet->setCellValue('J'.($key + 2), $value['good'] ? $value['good'] : null);
            $sheet->setCellValue('K'.($key + 2), $value['desc'] ? $value['desc'] : null);
            $sheet->setCellValue('L'.($key + 2), $value['policy'] ? $value['policy'] : null);
            $sheet->setCellValue('M'.($key + 2), $value['sellout']);
            $sheet->setCellValue('N'.($key + 2), $value['kpi']);
            $sheet->setCellValue('O'.($key + 2), $value['total']);
        }

        

        $filename = 'Report KPI Sale -'.date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        $objWriter->save('php://output');

        exit;

        // echo "<pre>";print_r($data);die;
    }
    public static function kpiSale($from, $to, array $params)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        $name            = isset($params['name']) ? $params['name'] : null;
        $regional_market = isset($params['regional_market']) ? $params['regional_market'] : null;
        $area            = isset($params['area']) ? $params['area'] : null;
        $district        = isset($params['district']) ? $params['district'] : null;
        $store           = isset($params['store']) ? $params['store'] : null;
        $distributor_id  = isset($params['distributor_id']) ? $params['distributor_id'] : null;
        $export         = isset($params['export']) ? $params['export'] : null;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Mã NV',
            'NV KV Phụ trách',
            'Ngày công',
            'Tên cửa hàng',
            'Tổng cộng',
        );

        // các model có đổi giá
        $QKpi = new Application_Model_GoodKpiLog();
        $list = $QKpi->get_list(date('Y-m-d', strtotime($from)), date('Y-m-d', strtotime($to)), My_Kpi_Object::Sale);

        $QGood = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value)
        {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
            {
                foreach ($list[$key] as $_color => $ranges)
                {
                    if ($_color)
                    {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else
                    {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Cộng';
        $heads[] = 'Thưởng KPI';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['kpi']);

        $index++;

        $QArea = new Application_Model_Area();
        $area_cache = $QArea->get_cache();
        $QRegion = new Application_Model_RegionalMarket();
        $region_cache = $QRegion->get_cache_all();

        $db = Zend_Registry::get('db');

        // danh sách tất cả NV PG được tính KPI
        // dựa vào danh sách và thứ tự trong danh sách này để điền chính xác vào file excel
        $sql = "SELECT DISTINCT s.id, s.`code`, s.regional_market, s.firstname, s.lastname
            FROM staff s INNER JOIN imei_kpi i ON s.id=i.sale_id
            WHERE DATE(i.timing_date) >= ? AND DATE(i.timing_date) <= ?
            AND s.regional_market <> 0 AND s.regional_market IS NOT NULL";

        if ($name) $sql .= " AND CONCAT(TRIM(BOTH FROM s.firstname), ' ', TRIM(BOTH FROM s.lastname)) LIKE '%".$name."%' ";
        if ($store) $sql .= " AND i.store_id = " . intval($store) . " ";

        if ($distributor_id) {
            $store_sql = "SELECT id FROM store WHERE d_id = ?";
            $store_of_distributor = $db->query($store_sql, array(intval($distributor_id)));
            $store_of_distributor_arr = array();

            foreach ($store_of_distributor as $key => $value) {
                $store_of_distributor_arr[] = $value['id'];
            }

            $store_of_distributor_str = implode(',', $store_of_distributor_arr);

            if (isset($store_of_distributor_str) && !empty($store_of_distributor_str))
                $sql .= " AND i.store_id IN (".$store_of_distributor_str.") ";
        }

        $all_staff = $db->query($sql, array($from, $to));

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $staff_array = array();
        $staff_list = array();

        // điền danh sách staff ra file trước
        foreach ($all_staff as $key => $staff)
        {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, isset($area_cache[$region_cache[$staff['regional_market']]['area_id']]) ?
                $area_cache[$region_cache[$staff['regional_market']]['area_id']] : '');
            $sheet->setCellValue($alpha++ . $index, $staff['code']);
            $sheet->setCellValue($alpha++ . $index, $staff['firstname'] . ' ' . $staff['lastname']);

            $staff_array[$staff['id']] = $index; // lưu dòng ứng với staff id
            $staff_list[] = $staff['id']; // dùng để fetchGrid
            $index++;
        }

        $QImeiKpi = new Application_Model_ImeiKpi($export);
        $params = array(
            'kpi'            => 1,
            'sale'           => $staff_list,
            'from'           => $from,
            'to'             => $to,
            'area'           => $area,
            'province'       => $regional_market,
            'district'       => $district,
            'store'          => $store,
            'distributor_id' => $distributor_id,
        );

        $sell_out = $QImeiKpi->fetchGrid($params);
        $sell_out_list = array();

        // sắp xếp $sell_out theo staff, rồi theo good_id, color...
        foreach ($sell_out as $_key => $_so) {
            $sell_out_list[ $_so['staff_id'] ][ $_so['good_id'] ][ $_so['color_id'] ][ $_so['from_date'].'_'.$_so['to_date'] ] = array(
                'sell_out'      => $_so['quantity'],
                'activated'     => $_so['activated'],
                'non_activated' => $_so['non_activated'],
                'kpi'           => $_so['kpi'],
            );
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;
        $kpi_list = array();
        $quantity_list = array();

        foreach ($products as $_product_id => $value)
        {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            // đưa vào đúng dòng luôn
                            if (isset($staff_array[$_staff_id])) {
                                $sheet->setCellValue(
                                    $alpha . $staff_array[$_staff_id],
                                    isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                        ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                        : 0
                                );

                                if (!isset($kpi_list[ $_staff_id ]))
                                    $kpi_list[ $_staff_id ] = 0;

                                $kpi_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi']
                                    : 0;

                                if (!isset($quantity_list[ $_staff_id ]))
                                    $quantity_list[ $_staff_id ] = 0;

                                $quantity_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                    : 0;

                            }
                        }

                        $alpha++;
                    }
                }
            }
        }

        foreach ($quantity_list as $_staff_id => $_qty) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_qty
                );
            }
        }

        $alpha++;

        foreach ($kpi_list as $_staff_id => $_kpi) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_kpi
                );
            }
        }


        $filename = 'Sell out - Sale - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function kpiLeaderNew($from, $to, array $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if($userStorage->id != SUPERADMIN_ID){
            // echo "Hệ thống đang cập nhật chức năng vui lòng quay lại sau";
            // exit;
        }

        $area = null;


        if($userStorage->title == SALES_ADMIN_TITLE){
            $QAsm = new Application_Model_Asm();
            $where = $QAsm->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $result = $QAsm->fetchAll($where);
            //
            $area_id = array();
            foreach ($result as $key => $value) {
                $area_id[] = $value['area_id'];
            }

            $area = implode(",",$area_id);
        }

        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);


        $QImeiKpi = new Application_Model_ImeiKpi2();
        $data = $QImeiKpi->getKPIByLeader($from,$to,$area);


        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Staff ID',
            'B' => 'Staff',
            'C' => 'Code',
            'D' => 'Area',
            'E' => 'Title',
            'F' => 'Team',
            'G' => 'Off Date',
            'H' => 'Store ID',
            'I' => 'Store Name',
            'J' => 'Model',
            'K' => 'Desc',
            'L' => 'Policy',
            'M' => 'Sellout',
            'N' => 'KPI',
            'O' => 'Total',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        $sheet->setCellValue($key.'1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(30);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(10);
        $sheet->getColumnDimension('N')->setWidth(10);
        $sheet->getColumnDimension('O')->setWidth(20);


        foreach($data as $key => $value){
            $sheet->setCellValue('A'.($key + 2), $value['staff_id']);
            $sheet->setCellValue('B'.($key + 2), $value['staff']);
            $sheet->setCellValue('C'.($key + 2), $value['code']);
            $sheet->setCellValue('D'.($key + 2), $value['area']);
            $sheet->setCellValue('E'.($key + 2), $value['title']);
            $sheet->setCellValue('F'.($key + 2), $value['team']);
            $sheet->setCellValue('G'.($key + 2), $value['off_date'] ? DateTime::createFromFormat('Y-m-d', $value['off_date'])->format('d-m-Y') : null);
            $sheet->setCellValue('H'.($key + 2), $value['store_id']);
            $sheet->setCellValue('I'.($key + 2), $value['store_name'] ? $value['store_name'] : null);
            $sheet->setCellValue('J'.($key + 2), $value['good'] ? $value['good'] : null);
            $sheet->setCellValue('K'.($key + 2), $value['desc'] ? $value['desc'] : null);
            $sheet->setCellValue('L'.($key + 2), $value['policy'] ? $value['policy'] : null);
            $sheet->setCellValue('M'.($key + 2), $value['sellout']);
            $sheet->setCellValue('N'.($key + 2), $value['kpi']);
            $sheet->setCellValue('O'.($key + 2), $value['total']);
        }

        

        $filename = 'Report KPI Leader -'.date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        $objWriter->save('php://output');

        exit;

        // echo "<pre>";print_r($data);die;
    }

    public static function kpiLeader($from, $to, array $params)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        $name            = isset($params['name']) ? $params['name'] : null;
        $regional_market = isset($params['regional_market']) ? $params['regional_market'] : null;
        $area            = isset($params['area']) ? $params['area'] : null;
        $district        = isset($params['district']) ? $params['district'] : null;
        $store           = isset($params['store']) ? $params['store'] : null;
        $distributor_id  = isset($params['distributor_id']) ? $params['distributor_id'] : null;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Mã NV',
            'NV KV Phụ trách',
            'Ngày công',
            'Tên cửa hàng',
            'Tổng cộng',
        );

        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list(date('Y-m-d', strtotime($from)), date('Y-m-d', strtotime($to)), My_Kpi_Object::Pg);

        $QGood = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value)
        {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
            {
                foreach ($list[$key] as $_color => $ranges)
                {
                    if ($_color)
                    {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else
                    {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Cộng';
        $heads[] = 'Thưởng KPI';
        $heads[] = 'Doanh Thu';
        $heads[] = 'Từ ngày';
        $heads[] = 'Đến ngày';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        $QArea = new Application_Model_Area();
        $area_cache = $QArea->get_cache();
        $QRegion = new Application_Model_RegionalMarket();
        $region_cache = $QRegion->get_cache_all();

        $db = Zend_Registry::get('db');

        // danh sách tất cả NV PG được tính KPI
        // dựa vào danh sách và thứ tự trong danh sách này để điền chính xác vào file excel
        $sql = "SELECT DISTINCT s.id, s.`code`, s.regional_market, s.firstname, s.lastname
            FROM staff s INNER JOIN imei_kpi i ON s.id=i.leader_id
            WHERE DATE(i.timing_date) >= ? AND DATE(i.timing_date) <= ?
            AND s.regional_market <> 0 AND s.regional_market IS NOT NULL";

        if ($name) $sql .= " AND CONCAT(TRIM(BOTH FROM s.firstname), ' ', TRIM(BOTH FROM s.lastname)) LIKE '%".$name."%' ";
        if ($store) $sql .= " AND i.store_id = " . intval($store) . " ";

        if ($distributor_id) {
            $store_sql = "SELECT id FROM store WHERE d_id = ?";
            $store_of_distributor = $db->query($store_sql, array(intval($distributor_id)));
            $store_of_distributor_arr = array();

            foreach ($store_of_distributor as $key => $value) {
                $store_of_distributor_arr[] = $value['id'];
            }

            $store_of_distributor_str = implode(',', $store_of_distributor_arr);

            if (isset($store_of_distributor_str) && !empty($store_of_distributor_str))
                $sql .= " AND i.store_id IN (".$store_of_distributor_str.") ";
        }

        $all_staff = $db->query($sql, array($from, $to));

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $staff_array = array();
        $staff_list = array();

        // điền danh sách staff ra file trước
        foreach ($all_staff as $key => $staff)
        {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, isset($area_cache[$region_cache[$staff['regional_market']]['area_id']]) ?
                $area_cache[$region_cache[$staff['regional_market']]['area_id']] : '');
            $sheet->setCellValue($alpha++ . $index, $staff['code']);
            $sheet->setCellValue($alpha++ . $index, $staff['firstname'] . ' ' . $staff['lastname']);

            $staff_array[$staff['id']] = $index; // lưu dòng ứng với staff id
            $staff_list[] = $staff['id']; // dùng để fetchGrid
            $index++;
        }

        $QImeiKpi = new Application_Model_ImeiKpi();
        $params = array(
            'kpi'            => 1,
            'leader'         => $staff_list,
            'from'           => $from,
            'to'             => $to,
            'area'           => $area,
            'province'       => $regional_market,
            'district'       => $district,
            'store'          => $store,
            'distributor_id' => $distributor_id,
        );

        $sell_out = $QImeiKpi->fetchGrid($params);
        $sell_out_list = array();

        // sắp xếp $sell_out theo staff, rồi theo good_id, color...
        foreach ($sell_out as $_key => $_so) {
            $sell_out_list[ $_so['staff_id'] ][ $_so['good_id'] ][ $_so['color_id'] ][ $_so['from_date'].'_'.$_so['to_date'] ] = array(
                'sell_out'      => $_so['quantity'],
                'activated'     => $_so['activated'],
                'non_activated' => $_so['non_activated'],
                'kpi'           => $_so['kpi'],
                'value'         => $_so['total_value'],
            );
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;
        $kpi_list = array();
        $value_list = array(); // tổng doanh thu
        $quantity_list = array();

        foreach ($products as $_product_id => $value)
        {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            // đưa vào đúng dòng luôn
                            if (isset($staff_array[$_staff_id])) {
                                $sheet->setCellValue(
                                    $alpha . $staff_array[$_staff_id],
                                    isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                        ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                        : 0
                                );

                                if (!isset($kpi_list[ $_staff_id ]))
                                    $kpi_list[ $_staff_id ] = 0;

                                $kpi_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi']
                                    : 0;

                                if (!isset($quantity_list[ $_staff_id ]))
                                    $quantity_list[ $_staff_id ] = 0;

                                $quantity_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                    : 0;

                                if (!isset($value_list[ $_staff_id ]))
                                    $value_list[ $_staff_id ] = 0;

                                $value_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['value']
                                    : 0;
                            }
                        }

                        $alpha++;
                    }
                }
            }
        }

        foreach ($quantity_list as $_staff_id => $_qty) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_qty
                );
            }
        }

        $alpha++;

        foreach ($kpi_list as $_staff_id => $_kpi) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_kpi
                );
            }
        }

        $alpha++;

        foreach ($value_list as $_staff_id => $_vl) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_vl
                );
            }
        }


        $filename = 'Sell out - Leader - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function kpiAccessories($from, $to)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Mã NV',
            'Tên NV',
            'Email',
            'Khu vực',
            'Tỉnh',
        );
        $number_of_cols_before_products = count($heads);

        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list_accessories(date('Y-m-d', strtotime($from)), date('Y-m-d', strtotime($to)));

        $db = Zend_Registry::get('db');
        // // lấy danh sách sản phẩm
        $sql = "SELECT g.`id` as good_id, g.`name` as good_name
            FROM ".WAREHOUSE_DB.".good g
            WHERE g.cat_id = 12
            ORDER BY g.id";

        $product_list = $db->query($sql);
        $product_list_arr = array();
        $product_col = chr(ord('A') + count($heads));

        foreach ($product_list as $key => $value) {
            $product_list_arr[] = $value;
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$value['good_id']])) {
                foreach ($list[$value['good_id']] as $_color => $ranges) {
                    foreach ($ranges as $range)
                        $heads[] = $value['good_name'] . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                            DateTime($range['to']))->format('d');
                }
            }
        }
        
        
        $heads[] = 'Cộng';
        $heads[] = 'Thưởng KPI';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $excel = array();
        $excel[] = $heads;

        $row = array();

        for ($i=0; $i < $number_of_cols_before_products; $i++)
            $row[] = '';

        foreach ($product_list_arr as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$value['good_id']])) {
                foreach ($list[$value['good_id']] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        $row[] = $range['price'];
                    }
                }
            }
        }

        $excel[] = $row;

        $QArea = new Application_Model_Area();
        $area_cache = $QArea->get_cache();
        $QRegion = new Application_Model_RegionalMarket();
        $region_cache = $QRegion->get_cache_all();

        // $db = Zend_Registry::get('db');

        // danh sách tất cả NV được tính KPI
        // dựa vào danh sách và thứ tự trong danh sách này để điền chính xác vào file excel
        $sql = "SELECT s.id as staff_wh_id, st.id as staff_hr_id, st.email, st.regional_market, st.firstname, st.lastname, st.`code`
                FROM warehouse.staff s
                INNER JOIN hr.staff st
                    ON st.id=s.hr_id
                    AND (st.off_date IS NULL OR st.off_date >= ?)
                JOIN (
                    SELECT DISTINCT
                        s.id
                    FROM
                        staff s
                    LEFT JOIN (
                        SELECT DISTINCT
                            d.object,
                            d.old_value
                        FROM
                            staff_log_detail d
                        WHERE
                            (
                                (d.from_date >= ?
                                AND d.from_date <= ?)
                                OR
                                (d.to_date >= ?
                                AND d.to_date <= ?)
                            )
                        AND (d.old_value IN (183, 162, 164, 190, 274, 312))
                        AND d.info_type = ?
                    ) AS X ON s.id = X.object
                    WHERE
                        (
                            s.title IN (183, 162, 164, 190, 274, 312)
                            OR X.object IS NOT NULL
                        )
                ) Y ON st.id = Y.id";

        $all_staff = $db->query($sql, array($from, $from, $to, $from, $to, My_Staff_Info_Type::Title));
        $staff_array = array();

        foreach ($all_staff as $key => $value)
            $staff_array[ $value['staff_wh_id'] ] = $value;

        $sql = "SELECT DISTINCT
                    p.salesman,
                    p.good_id,
                    CASE WHEN g.from_date < ? THEN ? ELSE g.from_date END AS from_date,
                    IFNULL(g.to_date, DATE(?)) AS to_date,
                    SUM(p.num) AS `total_qty`
                    FROM
                        ".WAREHOUSE_DB.".`market` AS `p`
                    JOIN ".WAREHOUSE_DB.".good_price_log g ON p.good_id = g.good_id
                    AND DATE(p.add_time) >= g.from_date
                    AND (
                    DATE(p.add_time) <= g.to_date
                    OR g.to_date IS NULL
                    )
                    AND (p.`STATUS` = 1)
                    AND (p.isbacks = 0)
                    AND (p.cat_id = 12)
                    AND (p.pay_time IS NOT NULL)
                    AND (p.pay_time <> 0)
                    AND (p.add_time IS NOT NULL)
                    AND (p.add_time <> 0)
                    AND (
                        p.add_time >= ?
                    )
                    AND (
                        p.add_time <= ?
                    )
                    AND p.total != 0
                    AND p.total IS NOT NULL
                    GROUP BY
                    p.salesman,
                    p.good_id,
                    DATE(g.from_date);";

        $sell_out = $db->query($sql, array($from, $from, $to, $from, $to));

        $sell_out_list_by_staff = array();
        // sắp xếp $sell_out theo staff
        foreach ($sell_out as $key => $value) {
            $sell_out_list_by_staff
                [ $value['salesman'] ]
                [ $value['good_id'] ]
                [ $value['from_date'].'_'.$value['to_date'] ] = intval($value['total_qty']);
        }

        $i = 1;

        foreach ($sell_out_list_by_staff as $_salesman => $_product_list) {         
            
            
            if (!isset($staff_array[ $_salesman ])) continue;

            $region_tmp = isset($region_cache[ $staff_array[ $_salesman ]['regional_market'] ])
                ? $region_cache[ $staff_array[ $_salesman ]['regional_market'] ] : false;

            $area_tmp = isset($area_cache[ $region_tmp['area_id'] ]) ? $area_cache[ $region_tmp['area_id'] ] : '';

            $result = array(
                $i++,
                $staff_array[ $_salesman ]['code'],
                $staff_array[ $_salesman ]['firstname']. ' ' .$staff_array[ $_salesman ]['lastname'],
                $staff_array[ $_salesman ]['email'],
                $area_tmp,
                $region_tmp ? $region_tmp['name'] : '',
            );

            $total = 0;

            foreach ($product_list_arr as $value) {
                
                // các model có đổi giá, tách thành nhiều cột
                if (isset($list[$value['good_id']])) {
                    foreach ($list[$value['good_id']] as $_color => $ranges) {
                        foreach ($ranges as $range) {
                            $qty = isset($_product_list[ $value['good_id'] ][ $range['from'].'_'.$range['to'] ])
                                ? $_product_list[ $value['good_id'] ][ $range['from'].'_'.$range['to'] ]
                                : 0;
                            $result[] = $qty;
                            $total += $qty;
                        }
                    }
                }
            }
            

            $result[] = $total;
            $excel[] = $result;

            unset($total);
            unset($result);
            unset($region_tmp);
            unset($area_tmp);
            unset($qty);
            unset($_product_list);
        }

        
        $sheet->fromArray($excel);

        unset($excel);

        $filename = 'KPI - Accessories - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function kpiPgBK($from, $to, array $params)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        $name            = isset($params['name']) ? $params['name'] : null;
        $regional_market = isset($params['regional_market']) ? $params['regional_market'] : null;
        $area            = isset($params['area']) ? $params['area'] : null;
        $district        = isset($params['district']) ? $params['district'] : null;
        $store           = isset($params['store']) ? $params['store'] : null;
        $distributor_id  = isset($params['distributor_id']) ? $params['distributor_id'] : null;
        $export          = isset($params['export']) ? $params['export'] : null;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Mã NV',
            'NV KV Phụ trách',
            'Ngày công',
            'Tên cửa hàng',
            'Tổng cộng',
        );

        // các model có đổi giá
        $QKpi = new Application_Model_GoodKpiLog();
        $list = $QKpi->get_list(date('Y-m-d', strtotime($from)), date('Y-m-d', strtotime($to)), My_Kpi_Object::Pg);

        $QGood = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value)
        {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
            {
                foreach ($list[$key] as $_color => $ranges)
                {
                    if ($_color)
                    {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else
                    {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Cộng';
        $heads[] = 'Thưởng KPI';
        $heads[] = 'Hỗ trợ KPI';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['kpi']);

        $index++;

        $QArea = new Application_Model_Area();
        $area_cache = $QArea->get_cache();
        $QRegion = new Application_Model_RegionalMarket();
        $region_cache = $QRegion->get_cache_all();

        $db = Zend_Registry::get('db');

        // danh sách tất cả NV PG được tính KPI
        // dựa vào danh sách và thứ tự trong danh sách này để điền chính xác vào file excel
        $sql = "SELECT DISTINCT s.id, s.`code`, s.regional_market, s.firstname, s.lastname
            FROM staff s INNER JOIN imei_kpi i ON s.id=i.pg_id AND i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id
            WHERE DATE(i.timing_date) >= ? AND DATE(i.timing_date) <= ?
            AND s.regional_market <> 0 AND s.regional_market IS NOT NULL";

        if ($name) $sql .= " AND CONCAT(TRIM(BOTH FROM s.firstname), ' ', TRIM(BOTH FROM s.lastname)) LIKE '%".$name."%' ";
        if ($store) $sql .= " AND i.store_id = " . intval($store) . " ";

        if ($distributor_id) {
            $store_sql = "SELECT id FROM store WHERE d_id = ?";
            $store_of_distributor = $db->query($store_sql, array(intval($distributor_id)));
            $store_of_distributor_arr = array();

            foreach ($store_of_distributor as $key => $value) {
                $store_of_distributor_arr[] = $value['id'];
            }

            $store_of_distributor_str = implode(',', $store_of_distributor_arr);

            if (isset($store_of_distributor_str) && !empty($store_of_distributor_str))
                $sql .= " AND i.store_id IN (".$store_of_distributor_str.") ";
        }

        $all_staff = $db->query($sql, array($from, $to));

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $staff_array = array();
        $staff_list = array();

        // điền danh sách staff ra file trước
        foreach ($all_staff as $key => $staff)
        {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, isset($area_cache[$region_cache[$staff['regional_market']]['area_id']]) ?
                $area_cache[$region_cache[$staff['regional_market']]['area_id']] : '');
            $sheet->setCellValue($alpha++ . $index, $staff['code']);
            $sheet->setCellValue($alpha++ . $index, $staff['firstname'] . ' ' . $staff['lastname']);

            $staff_array[$staff['id']] = $index; // lưu dòng ứng với staff id
            $staff_list[] = $staff['id']; // dùng để fetchGrid
            $index++;
        }

        $QImeiKpi = new Application_Model_ImeiKpi($export);
        $params = array(
            'kpi'            => 1,
            'pg'             => $staff_list,
            'from'           => $from,
            'to'             => $to,
            'area'           => $area,
            'province'       => $regional_market,
            'district'       => $district,
            'store'          => $store,
            'distributor_id' => $distributor_id,
        );

        $sell_out = $QImeiKpi->fetchGrid($params);
        $sell_out_list = array();

        // sắp xếp $sell_out theo staff, rồi theo good_id, color...
        foreach ($sell_out as $_key => $_so) {
            $sell_out_list[ $_so['staff_id'] ][ $_so['good_id'] ][ $_so['color_id'] ][ $_so['from_date'].'_'.$_so['to_date'] ] = array(
                'sell_out'      => $_so['quantity'],
                'activated'     => $_so['activated'],
                'non_activated' => $_so['non_activated'],
                'kpi'           => $_so['kpi'],
            );
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;
        $kpi_list = array();
        $quantity_list = array();

        foreach ($products as $_product_id => $value)
        {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            // đưa vào đúng dòng luôn
                            if (isset($staff_array[$_staff_id])) {
                                $sheet->setCellValue(
                                    $alpha . $staff_array[$_staff_id],
                                    isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                        ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                        : 0
                                );

                                if (!isset($kpi_list[ $_staff_id ]))
                                    $kpi_list[ $_staff_id ] = 0;

                                $kpi_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi']
                                    : 0;

                                if (!isset($quantity_list[ $_staff_id ]))
                                    $quantity_list[ $_staff_id ] = 0;

                                $quantity_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                    : 0;

                            }
                        }

                        $alpha++;
                    }
                }
            }
        }

        foreach ($quantity_list as $_staff_id => $_qty) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_qty
                );
            }
        }

        $alpha++;

        foreach ($kpi_list as $_staff_id => $_kpi) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_kpi
                );
            }
        }

        // tính KPI hỗ trợ
        $params = array(
            'kpi'            => 2, // đánh dấu khác cái trên
            'pg'             => $staff_list,
            'from'           => $from,
            'to'             => $to,
            'area'           => $area,
            'province'       => $regional_market,
            'district'       => $district,
            'store'          => $store,
            'distributor_id' => $distributor_id,
        );

        $kpi_support = $QImeiKpi->fetchGrid($params);
        $kpi_support_list = array();

        // sắp xếp $sell_out theo staff, rồi theo good_id, color...
        foreach ($kpi_support as $_key => $_so)
            $kpi_support_list[ $_so['staff_id'] ] = $_so['kpi_support'];

        $alpha++;

        foreach ($kpi_support_list as $_staff_id => $_kpi) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_kpi
                );
            }
        }


        $filename = 'Sell out - PG - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function kpiPbSale($from, $to, array $params)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        $name            = isset($params['name']) ? $params['name'] : null;
        $regional_market = isset($params['regional_market']) ? $params['regional_market'] : null;
        $area            = isset($params['area']) ? $params['area'] : null;
        $district        = isset($params['district']) ? $params['district'] : null;
        $store           = isset($params['store']) ? $params['store'] : null;
        $distributor_id  = isset($params['distributor_id']) ? $params['distributor_id'] : null;
        $export          = isset($params['export']) ? $params['export'] : null;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Mã NV',
            'NV KV Phụ trách',
            'Ngày công',
            'Tên cửa hàng',
            'Tổng cộng',
        );

        // các model có đổi giá
        $QKpi           = new Application_Model_GoodKpiLog();
        $list           = $QKpi->get_list(date('Y-m-d', strtotime($from)), date('Y-m-d', strtotime($to)), My_Kpi_Object::Sale);
        
        $QGood          = new Application_Model_Good();
        $products       = $QGood->get_cache();
        
        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value)
        {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
            {
                foreach ($list[$key] as $_color => $ranges)
                {
                    if ($_color)
                    {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else
                    {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Cộng';
        $heads[] = 'Thưởng KPI';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['kpi']);

        $index++;

        $QArea        = new Application_Model_Area();
        $area_cache   = $QArea->get_cache();
        $QRegion      = new Application_Model_RegionalMarket();
        $region_cache = $QRegion->get_cache_all();

        $db = Zend_Registry::get('db');

        // danh sách tất cả NV PG được tính KPI
        // dựa vào danh sách và thứ tự trong danh sách này để điền chính xác vào file excel
        $sql = "SELECT DISTINCT s.id, s.`code`, s.regional_market, s.firstname, s.lastname
            FROM staff s INNER JOIN imei_kpi i ON s.id=i.pb_sale_id
            WHERE DATE(i.timing_date) >= ? AND DATE(i.timing_date) <= ?
            AND s.regional_market <> 0 AND s.regional_market IS NOT NULL";

        if ($name) $sql .= " AND CONCAT(TRIM(BOTH FROM s.firstname), ' ', TRIM(BOTH FROM s.lastname)) LIKE '%".$name."%' ";
        if ($store) $sql .= " AND i.store_id = " . intval($store) . " ";

        if ($distributor_id) {
            $store_sql = "SELECT id FROM store WHERE d_id = ?";
            $store_of_distributor = $db->query($store_sql, array(intval($distributor_id)));
            $store_of_distributor_arr = array();

            foreach ($store_of_distributor as $key => $value) {
                $store_of_distributor_arr[] = $value['id'];
            }

            $store_of_distributor_str = implode(',', $store_of_distributor_arr);

            if (isset($store_of_distributor_str) && !empty($store_of_distributor_str))
                $sql .= " AND i.store_id IN (".$store_of_distributor_str.") ";
        }

        $all_staff = $db->query($sql, array($from, $to));

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $staff_array = array();
        $staff_list = array();

        // điền danh sách staff ra file trước
        foreach ($all_staff as $key => $staff)
        {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, isset($area_cache[$region_cache[$staff['regional_market']]['area_id']]) ?
                $area_cache[$region_cache[$staff['regional_market']]['area_id']] : '');
            $sheet->setCellValue($alpha++ . $index, $staff['code']);
            $sheet->setCellValue($alpha++ . $index, $staff['firstname'] . ' ' . $staff['lastname']);

            $staff_array[$staff['id']] = $index; // lưu dòng ứng với staff id
            $staff_list[] = $staff['id']; // dùng để fetchGrid
            $index++;
        }

        $QImeiKpi = new Application_Model_ImeiKpi($export);
        $params = array(
            'kpi'            => 1,
            'pb_sale'           => $staff_list,
            'from'           => $from,
            'to'             => $to,
            'area'           => $area,
            'province'       => $regional_market,
            'district'       => $district,
            'store'          => $store,
            'distributor_id' => $distributor_id,
        );

        $sell_out = $QImeiKpi->fetchGrid($params);
        $sell_out_list = array();

        // sắp xếp $sell_out theo staff, rồi theo good_id, color...
        foreach ($sell_out as $_key => $_so) {
            $sell_out_list[ $_so['staff_id'] ][ $_so['good_id'] ][ $_so['color_id'] ][ $_so['from_date'].'_'.$_so['to_date'] ] = array(
                'sell_out'      => $_so['quantity'],
                'activated'     => $_so['activated'],
                'non_activated' => $_so['non_activated'],
                'kpi'           => $_so['kpi'],
            );
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;
        $kpi_list = array();
        $quantity_list = array();

        foreach ($products as $_product_id => $value)
        {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            // đưa vào đúng dòng luôn
                            if (isset($staff_array[$_staff_id])) {
                                $sheet->setCellValue(
                                    $alpha . $staff_array[$_staff_id],
                                    isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                        ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                        : 0
                                );

                                if (!isset($kpi_list[ $_staff_id ]))
                                    $kpi_list[ $_staff_id ] = 0;

                                $kpi_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['kpi']
                                    : 0;

                                if (!isset($quantity_list[ $_staff_id ]))
                                    $quantity_list[ $_staff_id ] = 0;

                                $quantity_list[ $_staff_id ] += isset($_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated'])
                                    ? $_data[ $_product_id ][ $_color ][ $range['from'].'_'.$range['to'] ]['activated']
                                    : 0;

                            }
                        }

                        $alpha++;
                    }
                }
            }
        }

        foreach ($quantity_list as $_staff_id => $_qty) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_qty
                );
            }
        }

        $alpha++;

        foreach ($kpi_list as $_staff_id => $_kpi) {
            if (isset($staff_array[$_staff_id])) {
                $sheet->setCellValue(
                    $alpha . $staff_array[$_staff_id],
                    $_kpi
                );
            }
        }


        $filename = 'Sell out - Pb Sale - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function pgImei($from, $to, array $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if($userStorage->id != SUPERADMIN_ID){
            // echo "Hệ thống đang cập nhật chức năng vui lòng quay lại sau";
            // exit;
        }

        $area = null;


        if($userStorage->title == SALES_ADMIN_TITLE){
            $QAsm = new Application_Model_Asm();
            $where = $QAsm->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $result = $QAsm->fetchAll($where);
            //
            $area_id = array();
            foreach ($result as $key => $value) {
                $area_id[] = $value['area_id'];
            }

            $area = implode(",",$area_id);
        }

        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);


        $QImeiKpi = new Application_Model_ImeiKpi2();
        $data = $QImeiKpi->getKPIByPgImei($from,$to,$area);


        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Imei',
            'B' => 'Timing Date',
            'C' => 'Staff ID',
            'D' => 'Staff',
            'E' => 'Code',
            'F' => 'Area',
            'G' => 'Title',
            'H' => 'Team',
            'I' => 'Off Date',
            'J' => 'Store ID',
            'K' => 'Store Name',
            'L' => 'Model',
            'M' => 'Desc',
            'N' => 'Policy',
            'O' => 'KPI',
            'P' => 'Customer',
            'Q' => 'Phone Number',
            'R' => 'Address'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        $sheet->setCellValue($key.'1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(30);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(10);
        $sheet->getColumnDimension('N')->setWidth(10);
        $sheet->getColumnDimension('O')->setWidth(25);
        $sheet->getColumnDimension('P')->setWidth(20);
        $sheet->getColumnDimension('Q')->setWidth(30);
        $sheet->getColumnDimension('R')->setWidth(30);

        foreach($data as $key => $value){
            $sheet->setCellValue('A'.($key + 2), $value['imei_sn']);
            $sheet->setCellValue('B'.($key + 2), $value['timing_date']);
            $sheet->setCellValue('C'.($key + 2), $value['staff_id']);
            $sheet->setCellValue('D'.($key + 2), $value['staff']);
            $sheet->setCellValue('E'.($key + 2), $value['code']);
            $sheet->setCellValue('F'.($key + 2), $value['area']);
            $sheet->setCellValue('G'.($key + 2), $value['title']);
            $sheet->setCellValue('H'.($key + 2), $value['team']); 
            $sheet->setCellValue('I'.($key + 2), ($value['off_date'] ? DateTime::createFromFormat('Y-m-d', $value['off_date'])->format('d-m-Y') : NULL));
            $sheet->setCellValue('J'.($key + 2), $value['store_id']);
            $sheet->setCellValue('K'.($key + 2), $value['store_name'] ? $value['store_name'] : null);
            $sheet->setCellValue('L'.($key + 2), $value['good'] ? $value['good'] : null);
            $sheet->setCellValue('M'.($key + 2), $value['desc'] ? $value['desc'] : null);
            $sheet->setCellValue('N'.($key + 2), $value['policy'] ? $value['policy'] : null);
            $sheet->setCellValue('O'.($key + 2), $value['kpi']);
            $sheet->setCellValue('P'.($key + 2), $value['customer_name']);
            $sheet->setCellValue('Q'.($key + 2), $value['phone_number']);
            $sheet->setCellValue('R'.($key + 2), $value['address']);

        }

        

        $filename = 'Report KPI_'.date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        $objWriter->save('php://output');

        exit;

        // echo "<pre>";print_r($data);die;
    }

    public static function reportHr($data){
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Year',
            'B' => 'Month',
            'C' => 'Code',
            'D' => 'Full name',
            'E' => 'Company',
            'F' => 'Department',
            'G' => 'Team',
            'H' => 'Title',
            'I' => 'Area',
            'J' => 'Province',
            'K'  => 'Phone number',
            'L'  => 'Email',
            'M'  => 'Contract signed at',
            'N'  => 'Contract term',
            'O' => 'Is officer',
            'P' => 'Group',
            'Q' => 'Joined at',
            'R' => 'Off date',
            'S' => 'Off date reason',
            'T' => 'Off date reason detail',
            'U' => 'Status',
            'V' => 'Đầu tháng',
            'W'  => 'Cuối tháng',
            'X' => 'New staff',
            'Y' => 'Transfer',
            'Z' => 'Total probation (at 1st of month)',
            'AA' => 'Total Permanent (at 1st of month)',
            'AB' => 'Total Headcount (at 1st of month)',
            'AC' => 'Total Headcount (end month)',
            'AD' => 'Total Off',
            'AE' => 'OFF từ dưới 3 tháng',
            'AF' => 'OFF từ 3 tháng đến dưới 6 tháng',
            'AG' => 'OFF từ 6 tháng đến dưới 1 năm',
            'AH' => 'OFF từ 1 năm đến dưới 2 năm',
            'AI' => 'OFF từ 2 năm đến dưới 4 năm',
            'AJ' => 'OFF từ trên 4 năm',
            'AK' => 'Involuntary',
            'AL' => 'Voluntary',
            'AM' => 'ON từ dưới 3 tháng',
            'AN' => 'ON từ 3 tháng đến dưới 6 tháng',
            'AO' => 'ON từ 6 tháng đến dưới 1 năm',
            'AP' => 'ON từ 1 năm đến dưới 2 năm',
            'AQ' => 'ON từ 2 năm đến dưới 4 năm',
            'AR' => 'ON từ trên 4 năm',
            

        );
    //     include 'PHPExcel/IOFactory.php';
    // $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
    //         DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
    //         DIRECTORY_SEPARATOR . 'hr-report' . DIRECTORY_SEPARATOR . 'template_report.xlsx';

    // $inputFileType = PHPExcel_IOFactory::identify($path);
    // $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
    // $objPHPExcel   = $objReader->load($path);
    // $sheet         = $objPHPExcel->getActiveSheet();
    // $index = 4;
    // $stt   = 1;

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        $sheet->setCellValue($key.'1', $value);
        $sheet->getStyle('A1:AR1')->applyFromArray(array('font' => array('bold' => true)));
        $sheet->getStyle('AE1:AJ1')->applyFromArray(array('fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'b3d9ff')
                                    )));
        $sheet->getStyle('AM1:AR1')->applyFromArray(array('fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'ffb3b3')
                                    )));
        // $sheet->getColumnDimension('A1:AM1')->setHeight(30);
        // $sheet->getStyle('Z:AG')->applyFromArray(array());
        // $sheet->getColumnDimension('A')->setWidth(10);
        // $sheet->getColumnDimension('B')->setWidth(30);
        // $sheet->getColumnDimension('C')->setWidth(15);
        // $sheet->getColumnDimension('D')->setWidth(20);
        // $sheet->getColumnDimension('E')->setWidth(25);
        // $sheet->getColumnDimension('F')->setWidth(25);
        // $sheet->getColumnDimension('G')->setWidth(15);
        // $sheet->getColumnDimension('H')->setWidth(10);
        // $sheet->getColumnDimension('I')->setWidth(30);
        // $sheet->getColumnDimension('J')->setWidth(15);
        // $sheet->getColumnDimension('K')->setWidth(15);
        // $sheet->getColumnDimension('L')->setWidth(30);
        // $sheet->getColumnDimension('M')->setWidth(10);
        // $sheet->getColumnDimension('N')->setWidth(10);
        // $sheet->getColumnDimension('O')->setWidth(25);
        // $sheet->getColumnDimension('P')->setWidth(20);
        // $sheet->getColumnDimension('Q')->setWidth(30);
        // $sheet->getColumnDimension('R')->setWidth(30);
        // echo "<pre>";print_r($data);die;
        foreach($data as $key => $value){
            $sheet->setCellValue('A'.($key + 2), $value['year']);
            $sheet->setCellValue('B'.($key + 2), $value['month']);
            $sheet->setCellValue('C'.($key + 2), $value['code']);
            $sheet->setCellValue('D'.($key + 2), $value['staff_name']);
            $sheet->setCellValue('E'.($key + 2), $value['company_name']);
            $sheet->setCellValue('F'.($key + 2), $value['department']);
            $sheet->setCellValue('G'.($key + 2), $value['team']);
            $sheet->setCellValue('H'.($key + 2), $value['title']);
            $sheet->setCellValue('I'.($key + 2), $value['area']);
            $sheet->setCellValue('J'.($key + 2), $value['province']);
            // $sheet->setCellValue('K'.($key + 2), "=\"" . $value['phone_number'] . "\"");
            $sheet->setCellValue('K'.($key + 2), $value['phone_number']);
            $sheet->setCellValue('L'.($key + 2), $value['email']);
            $sheet->setCellValue('M'.($key + 2), ($value['contract_signed_at'] ? DateTime::createFromFormat('Y-m-d', $value['contract_signed_at'])->format('d/m/Y') : NULL));
            $sheet->setCellValue('N'.($key + 2), $value['contract_name']);
            $sheet->setCellValue('O'.($key + 2), $value['is_officer']);
            $sheet->setCellValue('P'.($key + 2), $value['company_group_name']);
            $sheet->setCellValue('Q'.($key + 2), ($value['joined_at'] ? DateTime::createFromFormat('Y-m-d', $value['joined_at'])->format('d/m/Y') : NULL));
            $sheet->setCellValue('R'.($key + 2), ($value['off_date'] ? DateTime::createFromFormat('Y-m-d', $value['off_date'])->format('d/m/Y') : NULL));
            $sheet->setCellValue('S'.($key + 2), $value['off_date_reason']);
            $sheet->setCellValue('T'.($key + 2), $value['off_date_reason_detail']);
            $sheet->setCellValue('U'.($key + 2), $value['status']);
            $sheet->setCellValue('V'.($key + 2), $value['dau_thang']);
            $sheet->setCellValue('W'.($key + 2), $value['cuoi_thang']);
            $sheet->setCellValue('X'.($key + 2), $value['new_staff']);
            $sheet->setCellValue('Y'.($key + 2), $value['transfer']);
            $sheet->setCellValue('Z'.($key + 2), $value['thu_viec_dau_thang']);
            $sheet->setCellValue('AA'.($key + 2), $value['chinh_thuc_dau_thang']);
            $sheet->setCellValue('AB'.($key + 2), $value['tong_nhan_su_dau_thang']);
            $sheet->setCellValue('AC'.($key + 2), $value['tong_nhan_su_cuoi_thang']);
            $sheet->setCellValue('AD'.($key + 2), $value['total_off']);
            $sheet->setCellValue('AE'.($key + 2), $value['OFF < 3month']);
            $sheet->setCellValue('AF'.($key + 2), $value['OFF > 3month <= 6month']);
            $sheet->setCellValue('AG'.($key + 2), $value['OFF > 6month <= 12month']);
            $sheet->setCellValue('AH'.($key + 2), $value['OFF > 12month <= 24month']);
            $sheet->setCellValue('AI'.($key + 2), $value['OFF > 24month <= 48month']);
            $sheet->setCellValue('AJ'.($key + 2), $value['OFF > 48month']);
            $sheet->setCellValue('AK'.($key + 2), $value['involuntary']);
            $sheet->setCellValue('AL'.($key + 2), $value['voluntary']);
            $sheet->setCellValue('AM'.($key + 2), $value['ON < 3month']);
            $sheet->setCellValue('AN'.($key + 2), $value['ON > 3month <= 6month']);
            $sheet->setCellValue('AO'.($key + 2), $value['ON > 6month <= 12month']);
            $sheet->setCellValue('AP'.($key + 2), $value['ON > 12month <= 24month']);
            $sheet->setCellValue('AQ'.($key + 2), $value['ON > 24month <= 48month']);
            $sheet->setCellValue('AR'.($key + 2), $value['ON > 48month']);


        }

        

        $filename = 'Report Staff_'.date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        // header('Content-Type: text/csv; charset=utf-8');
        // header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        $objWriter->save('php://output');
        exit;
        // $this->_redirect(HOST.'hr-management/staff');
    }
}