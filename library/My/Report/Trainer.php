<?php
/**
 * Created by PhpStorm.
 * User: lphuctai
 * Date: 06/05/2019
 * Time: 16:59
 */

class My_Report_Trainer
{
    public static function appraisalSaleResult($data, $exportType)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', 0);
        error_reporting(~E_ALL);

        $QView = new Application_Model_AppraisalSaleView();
        $total = 0;
        $result = $QView->getResult($data, $total, 0, $exportType);
        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getPlanById($data['plan_id']);
        if($exportType == 1) {
            $surveyId = $plan['asp_survey_rsm_asm'];
        } elseif ($exportType == 2) {
            $surveyId = $plan['asp_survey_sale_leader'];
        } else {
            $surveyId = $plan['asp_survey_pg'];
        }

        $heads = [
            'Code Người đánh giá',
            'Người đánh giá',
            'Chức danh',
            'Khu vực Người đánh giá',
            'Code Người nhận đánh giá',
            'Người nhận đánh giá',
            'Chức danh',
            'Khu vực Người nhận đánh giá',
            'Ngày đánh giá',
        ];
        $QQuestion = new Application_Model_DynamicSurveyQuestion();
        $questions = $QQuestion->getQuestionBySurvey($surveyId);
        uasort($questions, function($a, $b) {
            return $a['dsq_id'] == $b['dsq_id'] ? 0 : ($a['dsq_id'] < $b['dsq_id'] ? -1 : 1);
        });
        foreach ($questions as $question) {
            $heads[] = $question['dsq_title'];
        }
        $buildQuestion = [];
        foreach ($questions as $question) {
            $buildQuestion[$question['dsq_id']] = $question['dsq_title'];
        }
        $heads[] = 'Điểm trung bình';

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $index = 1;
        $alpha = 'A';
        foreach($heads as $value) {
            $sheet->getColumnDimension($alpha)->setAutoSize(true);
            $sheet->setCellValue($alpha++ . $index, $value);
        }
        $sheet->getStyle('A1:' . $alpha . $index)->applyFromArray(array('font' => array('bold' => true)));

        $buildResult = [];
        foreach ($result as $item) {
            $alpha = sprintf('(%s,%s)', $item['from_staff'], $item['to_staff']);
            if(!array_key_exists($alpha, $buildResult)) {
                $buildResult[$alpha] = [
                    'from_staff_code' => $item['from_staff_code'],
                    'from_staff' => $item['from_staff'],
                    'from_staff_area' => $item['from_staff_area'],
                    'from_name' => $item['from_name'],
                    'from_title' => $item['from_title'],
                    'to_staff_code' => $item['to_staff_code'],
                    'to_staff' => $item['to_staff'],
                    'to_staff_area' => $item['to_staff_area'],
                    'to_name' => $item['to_name'],
                    'to_title' => $item['to_title'],
                    'asm_created_at' => $item['asm_created_at'],
                    'point' => $item['point'],
                    'question' => []
                ];
            }
            if(!array_key_exists($item['question_id'], $buildResult[$alpha]['question']) && array_key_exists($item['question_id'], $buildQuestion)) {
                $buildResult[$alpha]['question'][$item['question_id']] = $item['value'];
            }
        }
        $index ++;
        foreach($buildResult as $value){
            $alpha = 'A';
            $sheet->setCellValueExplicit($alpha++ .$index, $value['from_staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ .$index, $value['from_name']);
            $sheet->setCellValue($alpha++ .$index, self::getTitle($value['from_title']));
            $sheet->setCellValue($alpha++ .$index, $value['from_staff_area']);
            $sheet->setCellValueExplicit($alpha++ .$index, $value['to_staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ .$index, $value['to_name']);
            $sheet->setCellValue($alpha++ .$index, self::getTitle($value['to_title']));
            $sheet->setCellValue($alpha++ .$index, $value['to_staff_area']);
            $sheet->setCellValue($alpha++ .$index, $value['asm_created_at'] ? date('d/m/Y', strtotime($value['asm_created_at'])) : '-');
            foreach ($buildQuestion as $key => $title) {
                $sheet->setCellValue($alpha++ .$index, array_key_exists($key, $value['question']) ? $value['question'][$key] : '-');
            }
            $sheet->setCellValue($alpha++ .$index, $value['point'] ? $value['point'] : '-');
            $index ++;
        }
        $filename = 'AppraisalSaleResult-'.date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public static function getTitle($titleId)
    {
        switch (intval($titleId)) {
            case 462:
                return 'Sale Manager';
            case 308:
                return 'RSM';
            case 179:
                return 'ASM';
            case 190:
                return 'Sale Leader';
            case 183:
                return 'Sale';
            case 182:
                return 'PG';
        }
    }
}