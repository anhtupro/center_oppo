<?php

class My_Report_Sales {

    public static function timingDetail($data) {
        set_time_limit(0);
        error_reporting(E_ALL);
        ini_set('display_error', 1);
        ini_set('memory_limit', -1);

        $filename = 'Sell Out - Details - ' . date('d-m-Y H-i-s');

        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        $heads = array(
            'Date',
            'Province',
            'Store',
            'Address',
            'Title',
            'PG',
            'Sale',
            'Product',
            'Model',
            'IMEI',
            'Customer name',
            'Phone number',
            'Email',
            'Address',
        );

        fputcsv($output, $heads);

        $QGood           = new Application_Model_Good();
        $products_cached = $QGood->get_cache();

        $QGoodColor    = new Application_Model_GoodColor();
        $models_cached = $QGoodColor->get_cache();

        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        $data = preg_replace("/^(SELECT)(\s)+(SQL_CALC_FOUND_ROWS)?/", ' ', $data);
        $sql  = 'SELECT SQL_CALC_FOUND_ROWS ' . $data;

        $db     = Zend_Registry::get('db');
        $result = $db->query($sql);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        $limit  = LIMITATION * 1000;
        $page   = 0;

        do {
            $page++;
            $limit_string = sprintf(" LIMIT %d OFFSET %d ", $limit, $limit * ($page - 1));
            $sql          = " SELECT " . $data . $limit_string;

            $result = $db->query($sql);

            foreach ($result as $item) {
                $row = array();

                $row[] = $item['from'];
                $row[] = $item['regional_market'];
                $row[] = $item['store_name'];
                $row[] = $item['company_address'];
                $row[] = isset($teams[$item['title']]) ? $teams[$item['title']] : '';
                $row[] = $item['firstname'] . ' ' . $item['lastname'];
                $row[] = '';
                $row[] = (isset($products_cached[$item['product_id']]) ? $products_cached[$item['product_id']] :
                                '');
                $row[] = (isset($models_cached[$item['model_id']]) ? $models_cached[$item['model_id']] :
                                '');
                $row[] = "=\"" . $item['imei'] . "\"";
                $row[] = $item['customer_name'];
                $row[] = "=\"" . $item['phone_number'] . "\"";
                $row[] = $item['email'];
                $row[] = $item['address'];

                fputcsv($output, $row);
                unset($item);
                unset($row);
            }

            unset($sql);
            unset($result);
        } while ($page * $limit < $total);

        exit;
    }

    public static function store($from, $to, $result, $show_value = 0) {
        set_time_limit(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'Store ID',
            'Store Name',
            'Store Address',
            'Area',
            'Province',
            'District',
            'Sales Man',
            'Dealer ID',
            'Dealer Name',
            'Partner ID',
            'Channel',
            'Level',
        );

        $from = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $to)->format("Y-m-d");

        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list($from, $to);

        $QStoreStaffLog = new Application_Model_StoreStaffLog();
        $sale_list      = $QStoreStaffLog->getSaleManList($from, $to);

        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Total';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' Activated';
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' Activated';
                    }
                }
            }
        }

        $heads = array_merge($heads, array(
            'Total Activated',
            'Value',
            'Value Activated',
        ));

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;

        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        $all_store = array();

        $QDistributor      = new Application_Model_Distributor();
        $distributor_cache = $QDistributor->get_cache();

        $db     = Zend_Registry::get('db');
        $result = $db->query($result);
        // $stmt = $db->query($result);
        // $result = $stmt->fetchAll();

        foreach ($result as $_key => $_so) {

            $sell_out_list[$_so['store_id']][$_so['good_id']][$_so['color_id']][$_so['from_date'] . '_' . $_so['to_date']] = array(
                'total_quantity'        => $_so['total_quantity'],
                'total_activated'       => $_so['total_activated'],
                'total_value'           => $_so['total_value'],
                'total_value_activated' => $_so['total_value_activated'],
            );

            if (!isset($all_store[$_so['store_id']]))
                $all_store[$_so['store_id']] = array(
                    'store_id'        => $_so['store_id'],
                    'store_name'      => $_so['store_name'],
                    'company_address' => $_so['company_address'],
                    'store_district'  => $_so['store_district'],
                    'distributor_id'  => $_so['d_id'],
                    'store_chanel'    => $_so['store_chanel'],
                    'store_level'     => $_so['store_level'],
                    'partner_id'      => $_so['partner_id'],
                );
        }

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $store_array = array();

        // điền danh sách staff ra file trước
        foreach ($all_store as $key => $store) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $store['store_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_name']);
            $sheet->setCellValue($alpha++ . $index, $store['company_address']);
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Area) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Province) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::District) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($sale_list[$store['store_id']]) ? rtrim($sale_list[$store['store_id']], ", ") : '');
            $sheet->setCellValue($alpha++ . $index, isset($store['distributor_id']) ? $store['distributor_id'] : '#');
            $sheet->setCellValue($alpha++ . $index, isset($distributor_cache[$store['distributor_id']]['title']) ? $distributor_cache[$store['distributor_id']]['title'] : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['partner_id']) ? $store['partner_id'] : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_chanel']) ? $store['store_chanel'] : ' ');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_level']) ? $store['store_level'] : ' ');
            $alpha++;

            $store_array[$store['store_id']] = $index; // lưu dòng ứng với store id
            $index++;

            unset($store);
        }

        unset($sale_list);

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;

        $total_by_store           = array();
        $total_activated_by_store = array();
        $value_by_store           = array();
        $value_activated_by_store = array();

        unset($index);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {

                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id])) { // đưa vào đúng dòng luôn
                                if (isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) {

                                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                                    $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0
                                    );

                                    if (!isset($total_by_store[$_store_id]))
                                        $total_by_store[$_store_id] = 0;
                                    $total_by_store[$_store_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;

                                    if (!isset($total_activated_by_store[$_store_id]))
                                        $total_activated_by_store[$_store_id] = 0;
                                    $total_activated_by_store[$_store_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                                    if (!isset($value_by_store[$_store_id]))
                                        $value_by_store[$_store_id] = 0;
                                    $value_by_store[$_store_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;

                                    if (!isset($value_activated_by_store[$_store_id]))
                                        $value_activated_by_store[$_store_id] = 0;
                                    $value_activated_by_store[$_store_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                                    unset($tmp);
                                } // END inner IF
                            } // END outer IF

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach

                    unset($ranges);
                } // END outer foreach
            } // END big IF

            unset($value);
        } // END big foreach

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_by_store[$_store_id]);

        $alpha++;
        unset($total_by_store);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {

                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id]) && isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) { // đưa vào đúng dòng luôn
                                $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];
                                $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0
                                );

                                unset($tmp);
                            } // END if

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach
                    unset($ranges);
                } // END outer foreach
            } // end big id
        } // end big foreach
        unset($sell_out_list);
        unset($products);
        unset($list);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_activated_by_store[$_store_id]);

        $alpha++;
        unset($total_activated_by_store);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($value_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], ($show_value) ? $value_by_store[$_store_id] : "-");

        $alpha++;
        unset($value_by_store);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($value_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], ($show_value) ? $value_activated_by_store[$_store_id] : "-");

        unset($all_store);
        unset($alpha);
        unset($value_activated_by_store);
        unset($store_array);

        $filename  = 'Sell Out - Store - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function dealer($from, $to, $result) {
        set_time_limit(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'Distributor ID',
            'Distributor Name',
            'Address',
            'Area',
            'Province',
            'District',
            'Region',
            'Partner ID',
            'Channel',
            'Channel Details',
        );

        $from = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $to)->format("Y-m-d");

        $QImeiKpi          = new Application_Model_ImeiKpi();
        $distributor_store = $QImeiKpi->fetchStoreSellOut(array('from_date' => $from, 'to_date' => $to));

        $distributor_store_active = $QImeiKpi->fetchStoreActive();

        // echo "<pre>";print_r($distributor_store_active);die;
        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list($from, $to);

        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Total';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' Activated';
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' Activated';
                    }
                }
            }
        }

        $heads = array_merge($heads, array(
            'Total Activated',
            'Value',
            'Value Activated',
            'Store sell out number',
            'Store nummber'
        ));

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        $all_store = array();

        $QDistributor      = new Application_Model_Distributor();
        $distributor_cache = $QDistributor->get_cache();

        $db     = Zend_Registry::get('db');
        $result = $db->query($result);

        foreach ($result as $_key => $_so) {
            $sell_out_list[$_so['dealer_id']][$_so['good_id']][$_so['color_id']][$_so['from_date'] . '_' . $_so['to_date']] = array(
                'total_quantity'        => $_so['total_quantity'],
                'total_activated'       => $_so['total_activated'],
                'total_value'           => $_so['total_value'],
                'total_value_activated' => $_so['total_value_activated'],
            );

            if (!isset($all_store[$_so['dealer_id']]))
                $all_store[$_so['dealer_id']] = array(
                    'store_id'        => $_so['dealer_id'],
                    'store_name'      => $distributor_cache[$_so['dealer_id']]['title'],
                    'store_district'  => $distributor_cache[$_so['dealer_id']]['district'],
                    'company_address' => $distributor_cache[$_so['dealer_id']]['add'],
                    'partner_id'      => $distributor_cache[$_so['dealer_id']]['partner_id'],
                    'bigarea_name'    => $distributor_cache[$_so['dealer_id']]['bigarea_name'],
                    'store_chanel'    => $_so['store_chanel'],
                    'store_level'     => $_so['store_level'],
                );
        }

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $store_array = array();

        // điền danh sách staff ra file trước
        foreach ($all_store as $key => $store) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $store['store_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_name']);
            $sheet->setCellValue($alpha++ . $index, $store['company_address']);

            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Area) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Province) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::District) : '#');
            $sheet->setCellValue($alpha++ . $index, $store['bigarea_name']);
            
            $sheet->setCellValue($alpha++ . $index, $store['partner_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_chanel']);
            $sheet->setCellValue($alpha++ . $index, $store['store_level']);

            $alpha++;

            $store_array[$store['store_id']] = $index; // lưu dòng ứng với store id
            $index++;
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;

        $total_by_store           = array();
        $total_activated_by_store = array();
        $value_by_store           = array();
        $value_activated_by_store = array();
        $store_sellout_number     = array();
        $store_number             = array();

        unset($index);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id])) { // đưa vào đúng dòng luôn
                                if (isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) {
                                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                                    $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0
                                    );

                                    if (!isset($total_by_store[$_store_id]))
                                        $total_by_store[$_store_id] = 0;
                                    $total_by_store[$_store_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;

                                    if (!isset($total_activated_by_store[$_store_id]))
                                        $total_activated_by_store[$_store_id] = 0;
                                    $total_activated_by_store[$_store_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                                    if (!isset($value_by_store[$_store_id]))
                                        $value_by_store[$_store_id] = 0;
                                    $value_by_store[$_store_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;

                                    if (!isset($value_activated_by_store[$_store_id]))
                                        $value_activated_by_store[$_store_id] = 0;
                                    $value_activated_by_store[$_store_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                                    unset($tmp);
                                } // END inner IF
                            } // END outer IF

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach

                    unset($ranges);
                } // END outer foreach
            } // END big IF

            unset($value);
        } // END big foreach

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_by_store[$_store_id]);

        $alpha++;
        unset($total_by_store);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {

                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id]) && isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) { // đưa vào đúng dòng luôn
                                $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];
                                $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0
                                );

                                unset($tmp);
                            } // END if

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach
                    unset($ranges);
                } // END outer foreach
            } // end big id
        } // end big foreach
        unset($sell_out_list);
        unset($products);
        unset($list);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_activated_by_store[$_store_id]);

        $alpha++;
        unset($total_activated_by_store);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($value_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $value_by_store[$_store_id]);

        $alpha++;
        unset($value_by_store);

        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($value_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $value_activated_by_store[$_store_id]);
        }

        //store sellout number
        $alpha++;
        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($distributor_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $distributor_store[$_store_id]['store_sellout_number']);
            else {
                $sheet->setCellValue($alpha . $store_array[$_store_id], 0);
            }
        }

        $alpha++;
        //store number
        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($distributor_store_active[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $distributor_store_active[$_store_id]['store_number']);
            else {
                $sheet->setCellValue($alpha . $store_array[$_store_id], 0);
            }
        }


        unset($all_store);
        unset($alpha);
        unset($value_activated_by_store);
        unset($store_array);

        $filename  = 'Sell Out - Distributor - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function dealer_ka($from, $to, $result) {
	
        set_time_limit(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'Distributor ID',
            'Distributor Name',
            'Address',
            'Area',
            'Province',
            'District',
            'Partner ID',
            'Chanel',
            'Level',
        );

        $from = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $to)->format("Y-m-d");

        $QImeiKpi          = new Application_Model_ImeiKpi();
        $distributor_store = $QImeiKpi->fetchStoreSellOutKa(array('from_date' => $from, 'to_date' => $to));

        
        $distributor_store_active = $QImeiKpi->fetchStoreActiveKa();

        // echo "<pre>";print_r($distributor_store_active);die;
        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list($from, $to);
     
        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Total';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' Activated';
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' Activated';
                    }
                }
            }
        }

        $heads = array_merge($heads, array(
            'Total Activated',
            'Value',
            'Value Activated',
            'Store sell out number',
            'Store nummber'
        ));

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        $all_store = array();

        $QDistributor      = new Application_Model_Distributor();
        $distributor_cache = $QDistributor->get_cache();

        $db     = Zend_Registry::get('db');
        $result = $db->query($result);

        foreach ($result as $_key => $_so) {
            $sell_out_list[$_so['dealer_id']][$_so['good_id']][$_so['color_id']][$_so['from_date'] . '_' . $_so['to_date']] = array(
                'total_quantity'        => $_so['total_quantity'],
                'total_activated'       => $_so['total_activated'],
                'total_value'           => $_so['total_value'],
                'total_value_activated' => $_so['total_value_activated'],
            );

            if (!isset($all_store[$_so['dealer_id']]))
                $all_store[$_so['dealer_id']] = array(
                    'store_id'        => $_so['dealer_id'],
                    'store_name'      => ($_so['channel'] == 11) ? 'Điện máy xanh_KA' :  $distributor_cache[$_so['dealer_id']]['title'],
                    'store_district'  => $distributor_cache[$_so['dealer_id']]['district'],
                    'company_address' => $distributor_cache[$_so['dealer_id']]['add'],
                    'partner_id'      => $distributor_cache[$_so['dealer_id']]['partner_id'],
                    'store_chanel'    => $_so['store_chanel'],
                    'store_level'     => $_so['store_level'],
                );
        }
       
        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $store_array = array();

        // điền danh sách staff ra file trước
        foreach ($all_store as $key => $store) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $store['store_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_name']);
            $sheet->setCellValue($alpha++ . $index, $store['company_address']);

            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Area) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Province) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::District) : '#');

            $sheet->setCellValue($alpha++ . $index, $store['partner_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_chanel']);
            $sheet->setCellValue($alpha++ . $index, $store['store_level']);

            $alpha++;

            $store_array[$store['store_id']] = $index; // lưu dòng ứng với store id
            $index++;
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;

        $total_by_store           = array();
        $total_activated_by_store = array();
        $value_by_store           = array();
        $value_activated_by_store = array();
        $store_sellout_number     = array();
        $store_number             = array();

        unset($index);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id])) { // đưa vào đúng dòng luôn
                                if (isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) {
                                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                                    $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0
                                    );

                                    if (!isset($total_by_store[$_store_id]))
                                        $total_by_store[$_store_id] = 0;
                                    $total_by_store[$_store_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;

                                    if (!isset($total_activated_by_store[$_store_id]))
                                        $total_activated_by_store[$_store_id] = 0;
                                    $total_activated_by_store[$_store_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                                    if (!isset($value_by_store[$_store_id]))
                                        $value_by_store[$_store_id] = 0;
                                    $value_by_store[$_store_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;

                                    if (!isset($value_activated_by_store[$_store_id]))
                                        $value_activated_by_store[$_store_id] = 0;
                                    $value_activated_by_store[$_store_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                                    unset($tmp);
                                } // END inner IF
                            } // END outer IF

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach

                    unset($ranges);
                } // END outer foreach
            } // END big IF

            unset($value);
        } // END big foreach

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_by_store[$_store_id]);

        $alpha++;
        unset($total_by_store);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {

                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id]) && isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) { // đưa vào đúng dòng luôn
                                $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];
                                $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0
                                );

                                unset($tmp);
                            } // END if

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach
                    unset($ranges);
                } // END outer foreach
            } // end big id
        } // end big foreach
        unset($sell_out_list);
        unset($products);
        unset($list);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_activated_by_store[$_store_id]);

        $alpha++;
        unset($total_activated_by_store);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($value_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $value_by_store[$_store_id]);

        $alpha++;
        unset($value_by_store);

        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($value_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $value_activated_by_store[$_store_id]);
        }

        //store sellout number
        $alpha++;
        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($distributor_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $distributor_store[$_store_id]['store_sellout_number']);
            else {
                $sheet->setCellValue($alpha . $store_array[$_store_id], 0);
            }
        }

        $alpha++;
        //store number
        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($distributor_store_active[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $distributor_store_active[$_store_id]['store_number']);
            else {
                $sheet->setCellValue($alpha . $store_array[$_store_id], 0);
            }
        }


        unset($all_store);
        unset($alpha);
        unset($value_activated_by_store);
        unset($store_array);

        $filename  = 'Sell Out - Distributor - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public static function area($data, $total_sales, $point_list, $params) {
//        if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171', '171.244.18.76', '171.244.18.86'))) {
//            echo "<pre>";
//            print_r($data);
//            die;
//        }

        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', ~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array('Area',);

        // echo "<pre>";print_r($data);die;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list($from, $to);

        // echo "<pre>";print_r($list);die;
        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        $kpi_list = array();

        foreach ($data as $_key => $_value) {
            $kpi_list[$_value['area_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
                'total_quantity'            => $_value['total_quantity'],
                'total_activated'           => $_value['total_activated'],
                'total_value'               => $_value['total_value'],
                'total_value_activated'     => $_value['total_value_activated'],
                'total_value_IND'           => $_value['total_value_IND'],
                'total_value_activated_IND' => $_value['total_value_activated_IND'],
            );
        }

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                        }
                    }
                }
            }
        }

        $heads[] = 'Unit';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' activated';
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' activated';
                        }
                    }
                }
            }
        }

        $heads[] = 'Unit activated';
        $heads[] = 'Value (80%)';
        $heads[] = 'Value IND (80%)';
        $heads[] = 'Value KA (80%)';
//        $heads[] = 'Value A71k (80%)';
//        $heads[] = 'Value IND A71k (80%)';
//        $heads[] = 'Value KA A71k (80%)';
//        $heads[] = 'Value A3s16g (80%)';
//        $heads[] = 'Value IND A3s16g (80%)';
//        $heads[] = 'Value KA A3s16g (80%)';
        
        $heads[] = 'Value K3 (80%)';
        $heads[] = 'Value IND K3 (80%)';
        $heads[] = 'Value KA K3 (80%)';
        
        $heads[] = 'Value A1k SUBSIDI (80%)';
        $heads[] = 'Value IND A1k SUBSIDI (80%)';
        $heads[] = 'Value KA A1k SUBSIDI (80%)';
         
        $heads[] = 'Value A1k (80%)';
        $heads[] = 'Value IND A1k (80%)';
        $heads[] = 'Value KA A1k (80%)';
        
//        $heads[] = 'Value Reno2f (80%)';
//        $heads[] = 'Value IND Reno2f (80%)';
//        $heads[] = 'Value KA Reno2f (80%)';
//        
//        $heads[] = 'Value A5s (80%)';
//        $heads[] = 'Value IND A5s (80%)';
//        $heads[] = 'Value KA A5s (80%)';
        
        
        $heads[] = 'Value excluded special models (80%)';
        $heads[] = 'Value excluded special models IND (80%)';
        $heads[] = 'Value excluded special models KA (80%)';
        $heads[] = 'Value activated (80%)';
        $heads[] = 'Value activated IND (80%)';
        $heads[] = 'Value activated KA (80%)';
        $heads[] = 'Tỉ lệ active cả nước (%)';
        $heads[] = 'Tỉ lệ chưa active (%)';
        $heads[] = 'Tỉ lệ chưa active vượt (%)';
        $heads[] = 'Value tính KPI';
        $heads[] = 'Value tính KPI IND';
        $heads[] = 'Value tính KPI KA';
//        $heads[] = 'Value tính KPI A71k';
//        $heads[] = 'Value tính KPI A71k IND';
//        $heads[] = 'Value tính KPI A71k KA';
//        $heads[] = 'Value tính KPI A3s16g';
//        $heads[] = 'Value tính KPI A3s16g IND';
//        $heads[] = 'Value tính KPI A3s16g KA';
        $heads[] = 'Value tính KPI K3';
        $heads[] = 'Value tính KPI K3 IND';
        $heads[] = 'Value tính KPI K3 KA';
        $heads[] = 'Value tính KPI A1k Subsidi';
        $heads[] = 'Value tính KPI A1k Subsidi IND';
        $heads[] = 'Value tính KPI A1k Subsidi KA';
        $heads[] = 'Value tính KPI A1k';
        $heads[] = 'Value tính KPI A1k IND';
        $heads[] = 'Value tính KPI A1k KA';
//        $heads[] = 'Value tính KPI Reno2F';
//        $heads[] = 'Value tính KPI Reno2F IND';
//        $heads[] = 'Value tính KPI Reno2F KA';
        
//        $heads[] = 'Value tính KPI A5s';
//        $heads[] = 'Value tính KPI A5s IND';
//        $heads[] = 'Value tính KPI A5s KA';
        $heads[] = 'Value tính KPI Models còn lại';
        $heads[] = 'Value tính KPI Models còn lại IND';
        $heads[] = 'Value tính KPI Models còn lại KA';
        $heads[] = 'Region Share (%)';
        $heads[] = 'Point';
        // $heads[] = 'Point mới';

//        $heads[] = 'Chiết khấu A71k (%)';
//        $heads[] = 'Chiết khấu A3s16g (%)';
        $heads[] = 'Chiết khấu K3 (%)';
        $heads[] = 'Chiết khấu A1k Subsidy (%)';
        $heads[] = 'Chiết khấu A1k (%)';
//        $heads[] = 'Chiết khấu Reno2F IND (%)';
//        $heads[] = 'Chiết khấu Reno2F KA (%)';
//        $heads[] = 'Chiết khấu A5s (%)';

//        
//        $heads[] = 'Chiết khấu (%)';
        $heads[] = 'Chiết khấu IND (%)';
        $heads[] = 'Chiết khấu KA (%)';
//        $heads[] = 'Bonus';
        $heads[] = 'Bonus Special model & model còn lại';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        $QArea      = new Application_Model_Area();
        $area_cache = $QArea->get_cache_all_with_del();


        // dùng mảng này để lưu thứ tự các area trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $area_array = array();

        // điền danh sách area ra file trước
        $alpha = 'A';
        foreach ($area_cache as $_area_id => $_area_name) {
            $sheet->setCellValue($alpha . $index, $_area_name);
            $area_array[$_area_id] = $index++; // lưu dòng ứng với area id
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;

        $QAsm        = new Application_Model_Asm();
        $asm_cache   = $QAsm->get_cache();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $total_by_area                             = array();
        $total_activated_by_area                   = array();
        $total_value_activated_by_area             = array();
        $total_value_activated_by_area_IND         = array();
        $total_value_activated_by_area_KA          = array();
        $total_value_by_area                       = array();
        $total_value_by_area_IND                   = array();
        $total_value_by_area_KA                    = array();
        $total_value_exclude_special_model_by_area = array();
        $total_value_a71k_model_by_area            = array();
        $total_value_a71k_model_by_area_IND        = array();
        $total_value_a71k_model_by_area_KA         = array();
        $total_value_a3s16g_model_by_area          = array();
        $total_value_a3s16g_model_by_area_IND      = array();
        $total_value_a3s16g_model_by_area_KA       = array();
        $total_value_k3_model_by_area              = array();
        $total_value_k3_model_by_area_IND          = array();
        $total_value_k3_model_by_area_KA           = array();
        $total_value_a1k_subsidi_model_by_area          = array();
        $total_value_a1k_subsidi_model_by_area_IND      = array();
        $total_value_a1k_subsidi_model_by_area_KA       = array();
        $total_value_a1k_model_by_area          = array();
        $total_value_a1k_model_by_area_IND      = array();
        $total_value_a1k_model_by_area_KA       = array();
        $total_value_reno2f_model_by_area          = array();
        $total_value_reno2f_model_by_area_IND      = array();
        $total_value_reno2f_model_by_area_KA       = array();
        
        $total_value_a5s_model_by_area          = array();
        $total_value_a5s_model_by_area_IND      = array();
        $total_value_a5s_model_by_area_KA       = array();

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_area_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            if (isset($area_array[$_area_id]) && isset($asm_cache[$userStorage->id]['area']) && in_array($_area_id, $asm_cache[$userStorage->id]['area'])) {
                                // đưa vào đúng dòng luôn
                                if (!isset($total_by_area[$_area_id])) {
                                    $total_by_area[$_area_id] = 0;
                                }
                                if (!isset($total_value_by_area[$_area_id])) {
                                    $total_value_by_area[$_area_id]     = 0;
                                    $total_value_by_area_IND[$_area_id] = 0;
                                    $total_value_by_area_KA[$_area_id]  = 0;
                                }
                                if (!isset($total_value_activated_by_area[$_area_id])) {
                                    $total_value_activated_by_area[$_area_id]     = 0;
                                    $total_value_activated_by_area_IND[$_area_id] = 0;
                                    $total_value_activated_by_area_KA[$_area_id]  = 0;
                                }
                                $total_by_area[$_area_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                                $total_value_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                $total_value_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                $total_value_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                if ($_product_id == 597) {
//                                    if (!isset($total_value_a71k_model_by_area[$_area_id])) {
//                                        $total_value_a71k_model_by_area[$_area_id]     = 0;
//                                        $total_value_a71k_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a71k_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a71k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a71k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a71k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(622, 661))) {
//                                    if (!isset($total_value_a3s16g_model_by_area[$_area_id])) {
//                                        $total_value_a3s16g_model_by_area[$_area_id]     = 0;
//                                        $total_value_a3s16g_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a3s16g_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a3s16g_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a3s16g_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a3s16g_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
                                if ($_product_id == 750) {
                                    if (!isset($total_value_k3_model_by_area[$_area_id])) {
                                        $total_value_k3_model_by_area[$_area_id]     = 0;
                                        $total_value_k3_model_by_area_IND[$_area_id] = 0;
                                        $total_value_k3_model_by_area_KA[$_area_id]  = 0;
                                    }
                                    $total_value_k3_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                    $total_value_k3_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                    $total_value_k3_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                                }
                                
                                if (in_array($_product_id, array(808))) {
                                    if (!isset($total_value_a1k_model_by_area[$_area_id])) {
                                        $total_value_a1k_subsidi_model_by_area[$_area_id]     = 0;
                                        $total_value_a1k_subsidi_model_by_area_IND[$_area_id] = 0;
                                        $total_value_a1k_subsidi_model_by_area_KA[$_area_id]  = 0;
                                    }
                                    $total_value_a1k_subsidi_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                    $total_value_a1k_subsidi_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                    $total_value_a1k_subsidi_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                                }
                                if (in_array($_product_id, array(708))) {
                                    if (!isset($total_value_a1k_model_by_area[$_area_id])) {
                                        $total_value_a1k_model_by_area[$_area_id]     = 0;
                                        $total_value_a1k_model_by_area_IND[$_area_id] = 0;
                                        $total_value_a1k_model_by_area_KA[$_area_id]  = 0;
                                    }
                                    $total_value_a1k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                    $total_value_a1k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                    $total_value_a1k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                                }
//                                if (in_array($_product_id, array(747))) {
//                                    if (!isset($total_value_a1k_model_by_area[$_area_id])) {
//                                        $total_value_reno2f_model_by_area[$_area_id]     = 0;
//                                        $total_value_reno2f_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_reno2f_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_reno2f_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_reno2f_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_reno2f_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(703))) {
//                                    if (!isset($total_value_a5s_model_by_area[$_area_id])) {
//                                        $total_value_a5s_model_by_area[$_area_id]     = 0;
//                                        $total_value_a5s_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a5s_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a5s_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a5s_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a5s_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
                                
                                $total_value_activated_by_area[$_area_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;
                                $total_value_activated_by_area_IND[$_area_id] += isset($tmp['total_value_activated_IND']) ? $tmp['total_value_activated_IND'] : 0;
                                $total_value_activated_by_area_KA[$_area_id] += $tmp['total_value_activated'] - $tmp['total_value_activated_IND'];
                                $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0);
                            } elseif (!in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) ||
                                    My_Staff_Permission_Area::view_all($userStorage->id)) {
                                if (!isset($total_by_area[$_area_id]))
                                    $total_by_area[$_area_id]                 = 0;
                                if (!isset($total_value_by_area[$_area_id]))
                                    $total_value_by_area[$_area_id]           = 0;
                                if (!isset($total_value_activated_by_area[$_area_id]))
                                    $total_value_activated_by_area[$_area_id] = 0;

                                $total_by_area[$_area_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                                $total_value_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                $total_value_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                $total_value_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];

                                $total_value_activated_by_area[$_area_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;
                                $total_value_activated_by_area_IND[$_area_id] += isset($tmp['total_value_activated_IND']) ? $tmp['total_value_activated_IND'] : 0;
                                $total_value_activated_by_area_KA[$_area_id] += $tmp['total_value_activated'] - $tmp['total_value_activated_IND'];
                                
//                                if ($_product_id == 597) {
//                                    if (!isset($total_value_a71k_model_by_area[$_area_id])) {
//                                        $total_value_a71k_model_by_area[$_area_id]     = 0;
//                                        $total_value_a71k_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a71k_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a71k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a71k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a71k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(622, 661))) {
//                                    if (!isset($total_value_a3s16g_model_by_area[$_area_id])) {
//                                        $total_value_a3s16g_model_by_area[$_area_id]     = 0;
//                                        $total_value_a3s16g_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a3s16g_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a3s16g_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a3s16g_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a3s16g_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
                                if ($_product_id == 750) {
                                    if (!isset($total_value_k3_model_by_area[$_area_id])) {
                                        $total_value_k3_model_by_area[$_area_id]     = 0;
                                        $total_value_k3_model_by_area_IND[$_area_id] = 0;
                                        $total_value_k3_model_by_area_KA[$_area_id]  = 0;
                                    }
                                    $total_value_k3_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                    $total_value_k3_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                    $total_value_k3_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                                }
                                
                                if (in_array($_product_id, array(808))) {
                                    if (!isset($total_value_a1k_subsidi_model_by_area[$_area_id])) {
                                        $total_value_a1k_subsidi_model_by_area[$_area_id]     = 0;
                                        $total_value_a1k_subsidi_model_by_area_IND[$_area_id] = 0;
                                        $total_value_a1k_subsidi_model_by_area_KA[$_area_id]  = 0;
                                    }
                                    $total_value_a1k_subsidi_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                    $total_value_a1k_subsidi_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                    $total_value_a1k_subsidi_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                                }
                                if (in_array($_product_id, array(708))) {
                                    if (!isset($total_value_a1k_model_by_area[$_area_id])) {
                                        $total_value_a1k_model_by_area[$_area_id]     = 0;
                                        $total_value_a1k_model_by_area_IND[$_area_id] = 0;
                                        $total_value_a1k_model_by_area_KA[$_area_id]  = 0;
                                    }
                                    $total_value_a1k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                                    $total_value_a1k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                                    $total_value_a1k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                                }
//                                if (in_array($_product_id, array(747))) {
//                                    if (!isset($total_value_reno2f_model_by_area[$_area_id])) {
//                                        $total_value_reno2f_model_by_area[$_area_id]     = 0;
//                                        $total_value_reno2f_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_reno2f_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_reno2f_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_reno2f_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_reno2f_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(703))) {
//                                    if (!isset($total_value_a5s_model_by_area[$_area_id])) {
//                                        $total_value_a5s_model_by_area[$_area_id]     = 0;
//                                        $total_value_a5s_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a5s_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a5s_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a5s_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a5s_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }

                                $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0);
                            } else
                                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
                        }

                        $alpha++;
                    }
                }
            }
        }

        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_by_area[$_area_id])) {
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_by_area[$_area_id]);
                $total_all_area += $total_by_area[$_area_id];
            } else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_area_id => $_data) {
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            // nếu có trong danh sách NV ở trên
                            if (isset($area_array[$_area_id]) && isset($asm_cache[$userStorage->id]['area']) &&
                                    in_array($_area_id, $asm_cache[$userStorage->id]['area'])) {
                                // đưa vào đúng dòng luôn
                                if (!isset($total_activated_by_area[$_area_id]))
                                    $total_activated_by_area[$_area_id] = 0;
                                $total_activated_by_area[$_area_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                                $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0);
                            }
                            elseif (!in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) ||
                                    My_Staff_Permission_Area::view_all($userStorage->id)) {
                                if (!isset($total_activated_by_area[$_area_id]))
                                    $total_activated_by_area[$_area_id] = 0;
                                $total_activated_by_area[$_area_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                                $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0);
                            } else
                                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
                        }

                        $alpha++;
                    }
                }
            }
        }



        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_activated_by_area[$_area_id])) {
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_activated_by_area[$_area_id]);
                $total_activated_all_area += $total_activated_by_area[$_area_id];
            } else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

   
        $alpha++;

        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area[$_area_id]); //$heads[] = 'Value tính KPI';
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_IND[$_area_id]); // $heads[] = 'Value tính KPI IND';
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_KA[$_area_id]); // $heads[] = 'Value tính KPI KA';
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
        
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
        /**
         * @author: hero
         * mode c3
         * description
         * date
         */
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_k3_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_model_by_area[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_k3_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_model_by_area_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_k3_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_model_by_area_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        
        //a1k sub
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_subsidi_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_model_by_area[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_subsidi_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_model_by_area_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_subsidi_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_model_by_area_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        //End a1k sub
        
        //a1k
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_model_by_area[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_model_by_area_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_model_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_model_by_area_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        //End a1k
        
        //Reno2f
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
        //End Reno2f
        
        //a5s 
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
        //End a5s
        
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area[$_area_id] 
                       // - $total_value_a71k_model_by_area[$_area_id] 
//                        - $total_value_a3s16g_model_by_area[$_area_id] 
                         - $total_value_k3_model_by_area[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area[$_area_id] 
                        - $total_value_a1k_model_by_area[$_area_id] 
//                        - $total_value_reno2f_model_by_area[$_area_id] 
//                        - $total_value_a5s_model_by_area[$_area_id]
                        )
                    ;
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_IND[$_area_id] 
                        // - $total_value_a71k_model_by_area_IND[$_area_id] 
//                        - $total_value_a3s16g_model_by_area_IND[$_area_id] 
                         - $total_value_k3_model_by_area_IND[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area_IND[$_area_id] 
                        - $total_value_a1k_model_by_area_IND[$_area_id] 
//                        - $total_value_reno2f_model_by_area_IND[$_area_id] 
//                        - $total_value_a5s_model_by_area_IND[$_area_id]
                        );
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_KA[$_area_id] 
                        // - $total_value_a71k_model_by_area_KA[$_area_id] 
//                        - $total_value_a3s16g_model_by_area_KA[$_area_id] 
                         - $total_value_k3_model_by_area_KA[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area_KA[$_area_id] 
                        - $total_value_a1k_model_by_area_KA[$_area_id] 
//                        - $total_value_reno2f_model_by_area_KA[$_area_id] 
//                        - $total_value_a5s_model_by_area_KA[$_area_id]
                        );
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_activated_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_activated_by_area[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_activated_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_activated_by_area_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_activated_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_activated_by_area_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
// echo 111;
// exit();
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_activated_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1) . '%') ;
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;

        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_activated_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) . '%');
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;

        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_activated_by_area[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) - round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1) . '%');
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }

        $alpha++;
        $total_value_tinh_kpi                   = array();
        $total_value_tinh_kpi_IND               = array();
        $total_value_tinh_kpi_KA                = array();
//        $total_value_a71k                       = array();
//        $total_value_a71k_IND                   = array();
//        $total_value_a71k_KA                    = array();
        $total_value_a3s16g                     = array();
        $total_value_a3s16g_IND                 = array();
        $total_value_a3s16g_KA                  = array();
        $total_value_k3                         = array();
        $total_value_k3_IND                     = array();
        $total_value_k3_KA                      = array();
       
        
        $total_value_a1k_subsidi                     = array();
        $total_value_a1k_subsidi_IND                 = array();
        $total_value_a1k_subsidi_KA                  = array();
        $total_value_a1k                     = array();
        $total_value_a1k_IND                 = array();
        $total_value_a1k_KA                  = array();
        
        $total_value_reno2f                     = array();
        $total_value_reno2f_IND                 = array();
        $total_value_reno2f_KA                  = array();
        
        $total_value_a5s                     = array();
        $total_value_a5s_IND                 = array();
        $total_value_a5s_KA                  = array();
        $total_value_excluded_special_modes     = array();
        $total_value_excluded_special_modes_IND = array();
        $total_value_excluded_special_modes_KA  = array();
        foreach ($area_cache as $_area_id => $_area_name) {

            $active_vuot = round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) - round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1);
            if (isset($total_activated_by_area[$_area_id]) && $active_vuot <= 0) {
                $value_tinh_kpi = $total_value_by_area[$_area_id];

                $total_value_tinh_kpi[$_area_id]     = $value_tinh_kpi;
                $sheet->setCellValue($alpha . $area_array[$_area_id], $value_tinh_kpi);
                $total_value_tinh_kpi_IND[$_area_id] = $total_value_by_area_IND[$_area_id];
                $total_value_tinh_kpi_KA[$_area_id]  = $total_value_by_area_KA[$_area_id];
//                $total_value_a71k[$_area_id]         = $total_value_a71k_model_by_area[$_area_id];
//                $total_value_a71k_IND[$_area_id]     = $total_value_a71k_model_by_area_IND[$_area_id];
//                $total_value_a71k_KA[$_area_id]      = $total_value_a71k_model_by_area_KA[$_area_id];

//                $total_value_a3s16g[$_area_id]     = $total_value_a3s16g_model_by_area[$_area_id];
//                $total_value_a3s16g_IND[$_area_id] = $total_value_a3s16g_model_by_area_IND[$_area_id];
//                $total_value_a3s16g_KA[$_area_id]  = $total_value_a3s16g_model_by_area_KA[$_area_id];

                $total_value_k3[$_area_id]     = $total_value_k3_model_by_area[$_area_id];
                $total_value_k3_IND[$_area_id] = $total_value_k3_model_by_area_IND[$_area_id];
                $total_value_k3_KA[$_area_id]  = $total_value_k3_model_by_area_KA[$_area_id];

                $total_value_a1k_subsidi[$_area_id]     = $total_value_a1k_subsidi_model_by_area[$_area_id];
                $total_value_a1k_subsidi_IND[$_area_id] = $total_value_a1k_subsidi_model_by_area_IND[$_area_id];
                $total_value_a1k_subsidi_KA[$_area_id]  = $total_value_a1k_subsidi_model_by_area_KA[$_area_id];
                
                $total_value_a1k[$_area_id]     = $total_value_a1k_model_by_area[$_area_id];
                $total_value_a1k_IND[$_area_id] = $total_value_a1k_model_by_area_IND[$_area_id];
                $total_value_a1k_KA[$_area_id]  = $total_value_a1k_model_by_area_KA[$_area_id];
                
//                $total_value_reno2f[$_area_id]     = $total_value_reno2f_model_by_area[$_area_id];
//                $total_value_reno2f_IND[$_area_id] = $total_value_reno2f_model_by_area_IND[$_area_id];
//                $total_value_reno2f_KA[$_area_id]  = $total_value_reno2f_model_by_area_KA[$_area_id];
                
//                
//                $total_value_a5s[$_area_id]     = $total_value_a5s_model_by_area[$_area_id];
//                $total_value_a5s_IND[$_area_id] = $total_value_a5s_model_by_area_IND[$_area_id];
//                $total_value_a5s_KA[$_area_id]  = $total_value_a5s_model_by_area_KA[$_area_id];
                
                $total_value_excluded_special_modes[$_area_id]     = $total_value_by_area[$_area_id] 
                        // - $total_value_a71k_model_by_area[$_area_id] 
//                        - $total_value_a3s16g_model_by_area[$_area_id] 
                        - $total_value_k3_model_by_area[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area[$_area_id] 
                        - $total_value_a1k_model_by_area[$_area_id] 
//                        - $total_value_reno2f_model_by_area[$_area_id] 
//                        - $total_value_a5s_model_by_area[$_area_id]
                        ;
                $total_value_excluded_special_modes_IND[$_area_id] = $total_value_by_area_IND[$_area_id] 
                        // - $total_value_a71k_model_by_area_IND[$_area_id] 
//                        - $total_value_a3s16g_model_by_area_IND[$_area_id] 
                        - $total_value_k3_model_by_area_IND[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area_IND[$_area_id] 
                        - $total_value_a1k_model_by_area_IND[$_area_id] 
//                        - $total_value_reno2f_model_by_area_IND[$_area_id] 
//                        - $total_value_a5s_model_by_area_IND[$_area_id]
                        ;
                $total_value_excluded_special_modes_KA[$_area_id]  = $total_value_by_area_KA[$_area_id] 
                        // - $total_value_a71k_model_by_area_KA[$_area_id] 
//                        - $total_value_a3s16g_model_by_area_KA[$_area_id] 
                        - $total_value_k3_model_by_area_KA[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area_KA[$_area_id] 
                        - $total_value_a1k_model_by_area_KA[$_area_id] 
//                        - $total_value_reno2f_model_by_area_KA[$_area_id] 
//                        - $total_value_a5s_model_by_area_KA[$_area_id]
                        ;
            } else if (isset($total_activated_by_area[$_area_id]) && $active_vuot > 0) {
                $value_tinh_kpi = $total_value_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );

                $total_value_tinh_kpi[$_area_id]     = $value_tinh_kpi;
                $total_value_tinh_kpi_IND[$_area_id] = $total_value_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
                $total_value_tinh_kpi_KA[$_area_id]  = $value_tinh_kpi - $total_value_tinh_kpi_IND[$_area_id];

                $sheet->setCellValue($alpha . $area_array[$_area_id], $value_tinh_kpi);

//                $total_value_a71k[$_area_id]     = $total_value_a71k_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a71k_IND[$_area_id] = $total_value_a71k_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a71k_KA[$_area_id]  = $total_value_a71k_model_by_area_KA[$_area_id] * ( (100 - $active_vuot) / 100 );

//                $total_value_a3s16g[$_area_id]     = $total_value_a3s16g_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a3s16g_IND[$_area_id] = $total_value_a3s16g_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a3s16g_KA[$_area_id]  = $total_value_a3s16g[$_area_id] - $total_value_a3s16g_IND[$_area_id];

                $total_value_k3[$_area_id]     = $total_value_k3_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
                $total_value_k3_IND[$_area_id] = $total_value_k3_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
                $total_value_k3_KA[$_area_id]  = $total_value_k3_model_by_area_KA[$_area_id] * ( (100 - $active_vuot) / 100 );

                $total_value_a1k_subsidi[$_area_id]     = $total_value_a1k_subsidi_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
                $total_value_a1k_subsidi_IND[$_area_id] = $total_value_a1k_subsidi_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
                $total_value_a1k_subsidi_KA[$_area_id]  = $total_value_a1k_subsidi[$_area_id] - $total_value_a1k_subsidi_IND[$_area_id];
                
                $total_value_a1k[$_area_id]     = $total_value_a1k_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
                $total_value_a1k_IND[$_area_id] = $total_value_a1k_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
                $total_value_a1k_KA[$_area_id]  = $total_value_a1k[$_area_id] - $total_value_a1k_IND[$_area_id];
                
//                $total_value_reno2f[$_area_id]     = $total_value_reno2f_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_reno2f_IND[$_area_id] = $total_value_reno2f_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_reno2f_KA[$_area_id]  = $total_value_reno2f[$_area_id] - $total_value_reno2f_IND[$_area_id];
//                
//                $total_value_a5s[$_area_id]     = $total_value_a5s_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a5s_IND[$_area_id] = $total_value_a5s_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a5s_KA[$_area_id]  = $total_value_a5s[$_area_id] - $total_value_a5s_IND[$_area_id];
                
                $total_value_excluded_special_modes[$_area_id]     = ($total_value_by_area[$_area_id] 
//                        - $total_value_a71k_model_by_area[$_area_id] 
//                        - $total_value_a3s16g_model_by_area[$_area_id]
                        - $total_value_k3_model_by_area[$_area_id]  
                        - $total_value_a1k_subsidi_model_by_area[$_area_id] 
                        - $total_value_a1k_model_by_area[$_area_id] 
//                        - $total_value_reno2f_model_by_area[$_area_id] 
//                        - $total_value_a5s_model_by_area[$_area_id]
                        ) * ( (100 - $active_vuot) / 100 );
                $total_value_excluded_special_modes_IND[$_area_id] = ($total_value_by_area_IND[$_area_id] 
//                        - $total_value_a71k_model_by_area_IND[$_area_id] 
//                        - $total_value_a3s16g_model_by_area_IND[$_area_id]
                        - $total_value_k3_model_by_area_IND[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area_IND[$_area_id] 
                        - $total_value_a1k_model_by_area_IND[$_area_id] 
//                        - $total_value_reno2f_model_by_area_IND[$_area_id] 
//                        - $total_value_a5s_model_by_area_IND[$_area_id]
                        ) * ( (100 - $active_vuot) / 100 );
                $total_value_excluded_special_modes_KA[$_area_id]  = ($total_value_by_area_KA[$_area_id] 
//                        - $total_value_a71k_model_by_area_KA[$_area_id] 
//                        - $total_value_a3s16g_model_by_area_KA[$_area_id] 
                        - $total_value_k3_model_by_area_KA[$_area_id] 
                        - $total_value_a1k_subsidi_model_by_area_KA[$_area_id] 
                        - $total_value_a1k_model_by_area_KA[$_area_id] 
//                        - $total_value_reno2f_model_by_area_KA[$_area_id] 
//                        - $total_value_a5s_model_by_area_KA[$_area_id]
                        ) * ( (100 - $active_vuot) / 100 );
                
            } else {
                $total_value_tinh_kpi[$_area_id]                   = $total_value_tinh_kpi_IND[$_area_id]               = $total_value_tinh_kpi_KA[$_area_id]                = 0;
                $total_value_a71k[$_area_id]                       = $total_value_a71k_IND[$_area_id]                   = $total_value_a71k_KA[$_area_id]                    = 0;
                $total_value_a3s16g[$_area_id]                     = $total_value_a3s16g_KA[$_area_id]                  = $total_value_a3s16g_IND[$_area_id]                 = 0;
                $total_value_k3[$_area_id]                         = $total_value_k3_KA[$_area_id]                      = $total_value_k3_IND[$_area_id]                     = 0;
                $total_value_a1k_subsidi[$_area_id]                = $total_value_a1k_subsidi_KA[$_area_id]             = $total_value_a1k_subsidi_IND[$_area_id]            = 0;
                $total_value_a1k[$_area_id]                        = $total_value_a1k_KA[$_area_id]                     = $total_value_a1k_IND[$_area_id]                    = 0;
                $total_value_reno2f[$_area_id]                     = $total_value_reno2f_KA[$_area_id]                  = $total_value_reno2f_IND[$_area_id]                 = 0;
                $total_value_a5s[$_area_id]                        = $total_value_a5s_KA[$_area_id]                     = $total_value_a5s_IND[$_area_id]                    = 0;
                $total_value_excluded_special_modes[$_area_id]     = $total_value_excluded_special_modes_IND[$_area_id] = $total_value_excluded_special_modes_KA[$_area_id]  = 0;
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
            }
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi_IND[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_tinh_kpi_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi_KA[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_tinh_kpi_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
        /**
         * @author: hero
         * k3
         * description
         * date
     */
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_k3[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_k3[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_k3[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        
        $alpha++;
        
        //a1k sub
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_subsidi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_subsidi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k_subsidi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        //End a1k sub
        
        //a1k 
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_a1k[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        //End a1k 
        
        
        //reno2f 
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
        //End reno2f 
        
        //a5s 
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
        //End a5s 
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_excluded_special_modes[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_excluded_special_modes[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_excluded_special_modes[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_excluded_special_modes_IND[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_excluded_special_modes[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_excluded_special_modes_KA[$_area_id]);
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        // echo "<pre>";print_r($total_value_tinh_kpi);die;
        foreach ($total_value_tinh_kpi as $_key => $_v) {
            $total_value_tinh_kpi_finnaly += $_v;
        }


        $area_list        = $QArea->fetchAll();
        $area_regionshare = array();
        foreach ($area_list as $_key => $_area)
            if (isset($area_array[$_area['id']])) {
                $area_regionshare[$_area['id']] = $_area['region_share'];
                $sheet->setCellValue($alpha . $area_array[$_area['id']], $_area['region_share']);
            }

        $alpha++;

        // foreach ($point_list as $_area_id => $_point)
        //     if (isset($area_array[$_area_id]))
        //         $sheet->setCellValue($alpha . $area_array[$_area_id], $_point);
        // $alpha++;
        $ponit_moi_ = array();
        // echo "<pre>";print_r($area_cache);die;
        foreach ($area_cache as $_area_id => $_area_name) {
            foreach ($point_list as $_area_id => $_point) {
                $active_vuot = round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) - round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1);
                if (isset($total_activated_by_area[$_area_id])) {

                    $ponit_moi             = round(($total_value_tinh_kpi[$_area_id] / $total_value_tinh_kpi_finnaly) * 60 / ($area_regionshare[$_area_id] / 100), 2);
                    $ponit_moi_[$_area_id] = round(($total_value_tinh_kpi[$_area_id] / $total_value_tinh_kpi_finnaly) * 60 / ($area_regionshare[$_area_id] / 100), 2);
                    $sheet->setCellValue($alpha . $area_array[$_area_id], $ponit_moi);

                    // $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_tinh_kpi[$_area_id].'----'.$total_value_tinh_kpi_finnaly.'-----'.$area_regionshare[$_area_id]) ;
                } else {
                    $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
                }
            }
            // if (isset($area_array[$_area_id]))
        }
        // echo "<pre>";print_r($total_value_tinh_kpi);
        // echo "<pre>";print_r($ponit_moi_);die;
        $chiet_khau     = array();
        $chiet_khau_IND = array();
        $chiet_khau_KA  = array();
        foreach ($ponit_moi_ as $_key => $_v) {
            $array_hcm_hn   = array(10, 51, 32, 33, 24, 25, 26, 1, 34);
            $array_dn_ct_hp = array(7, 4, 13);
            if (in_array($_key, $array_dn_ct_hp)) {
                if ($_v >= 75)
                    $chiet_khau[$_key] = 6.9;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau[$_key] = 6.6;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau[$_key] = 6.3;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau[$_key] = 6;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau[$_key] = 5.85;
                elseif ($_v < 51)
                    $chiet_khau[$_key] = 5.7;



                if ($_v >= 75)
                    $chiet_khau_KA[$_key] = 6.1;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau_KA[$_key] = 5.9;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau_KA[$_key] = 5.7;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau_KA[$_key] = 5.5;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau_KA[$_key] = 5.3;
                elseif ($_v < 51)
                    $chiet_khau_KA[$_key] = 5.1;

                if ($_v >= 75)
                    $chiet_khau_IND[$_key] = 7.1;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau_IND[$_key] = 6.9;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau_IND[$_key] = 6.7;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau_IND[$_key] = 6.5;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau_IND[$_key] = 6.3;
                elseif ($_v < 51)
                    $chiet_khau_IND[$_key] = 6.1;
            }
            if (in_array($_key, $array_hcm_hn)) {
                if ($_v >= 75)
                    $chiet_khau[$_key] = 7.4;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau[$_key] = 7.1;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau[$_key] = 6.8;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau[$_key] = 6.5;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau[$_key] = 6.35;
                elseif ($_v < 51)
                    $chiet_khau[$_key] = 6.2;

                if ($_v >= 75)
                    $chiet_khau_KA[$_key] = 6.6;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau_KA[$_key] = 6.4;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau_KA[$_key] = 6.2;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau_KA[$_key] = 6;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau_KA[$_key] = 5.8;
                elseif ($_v < 51)
                    $chiet_khau_KA[$_key] = 5.6;

                if ($_v >= 75)
                    $chiet_khau_IND[$_key] = 7.6;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau_IND[$_key] = 7.4;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau_IND[$_key] = 7.2;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau_IND[$_key] = 7;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau_IND[$_key] = 6.8;
                elseif ($_v < 51)
                    $chiet_khau_IND[$_key] = 6.6;
            }
            if (!in_array($_key, $array_hcm_hn) && !in_array($_key, $array_dn_ct_hp)) {
                if ($_v >= 75)
                    $chiet_khau[$_key] = 6.4;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau[$_key] = 6.1;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau[$_key] = 5.8;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau[$_key] = 5.5;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau[$_key] = 5.35;
                elseif ($_v < 51)
                    $chiet_khau[$_key] = 5.2;

                if ($_v >= 75)
                    $chiet_khau_KA[$_key] = 5.6;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau_KA[$_key] = 5.4;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau_KA[$_key] = 5.2;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau_KA[$_key] = 5;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau_KA[$_key] = 4.8;
                elseif ($_v < 51)
                    $chiet_khau_KA[$_key] = 4.6;

                if ($_v >= 75)
                    $chiet_khau_IND[$_key] = 6.6;
                elseif ($_v >= 69 && $_v < 75)
                    $chiet_khau_IND[$_key] = 6.4;
                elseif ($_v >= 63 && $_v < 69)
                    $chiet_khau_IND[$_key] = 6.2;
                elseif ($_v >= 57 && $_v < 63)
                    $chiet_khau_IND[$_key] = 6;
                elseif ($_v >= 51 && $_v < 57)
                    $chiet_khau_IND[$_key] = 5.8;
                elseif ($_v < 51)
                    $chiet_khau_IND[$_key] = 5.6;
            }
        }
        // echo "<pre>";print_r($total_value_tinh_kpi);
        // echo "<pre>";print_r($chiet_khau);die;

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '4.5%');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '3.5%');
//        }
        $alpha++;
          foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], '0%');
        }
        $alpha++;
        //a1k sub
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], '4.5%');
        }
        $alpha++;
         //a1k
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], '4.5%');
        }
        $alpha++;
        //reno2f IND %
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], ($chiet_khau_IND[$_area_id] + 1). '%' );
//        }
//        $alpha++;
        //reno2f KA %
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], ($chiet_khau_KA[$_area_id] + 1). '%' );
//        }
//        $alpha++;
         //a5s
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '2%');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $chiet_khau[$_area_id] . '%');
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $chiet_khau_IND[$_area_id] . '%');
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;
        foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], $chiet_khau_KA[$_area_id] . '%');
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
        $alpha++;


//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], round(($total_value_tinh_kpi[$_area_id] * $chiet_khau[$_area_id]) / 100), 1);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;

//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], round(
//                                (($total_value_excluded_special_modes_IND[$_area_id] * $chiet_khau_IND[$_area_id]) / 100) + (($total_value_a71k_IND[$_area_id] * 4.5) / 100) + (($total_value_a3s16g_IND[$_area_id] * 3.5) / 100) + (($total_value_excluded_special_modes_KA[$_area_id] * $chiet_khau_KA[$_area_id]) / 100) + (($total_value_a71k_KA[$_area_id] * 4.5) / 100) + (($total_value_a3s16g_KA[$_area_id] * 3.5) / 100)
//                                , 1));
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
/*
         foreach ($area_cache as $_area_id => $_area_name) {
            if (isset($total_value_tinh_kpi[$_area_id]))
                $sheet->setCellValue($alpha . $area_array[$_area_id], round(
                ((($total_value_excluded_special_modes_IND[$_area_id]
//                    +$total_value_a71k_IND[$_area_id] 
//                    + $total_value_a3s16g_IND[$_area_id] 
//                    + $total_value_a5s_IND[$_area_id] 
                    + $total_value_a1k_IND[$_area_id]
//                    + $total_value_reno2f_IND[$_area_id]
//                    + $total_value_k3_IND[$_area_id]
                    + $total_value_a1k_subsidi_IND[$_area_id]
                    ) * $chiet_khau_IND[$_area_id]
                    ) / 100)  
                        
                   //  + (($total_value_reno2f_IND[$_area_id]) * ($chiet_khau_IND[$_area_id] + 1) / 100)    
                + ((($total_value_excluded_special_modes_KA[$_area_id]
//                    +$total_value_a71k_KA[$_area_id]
//                    +$total_value_a3s16g_KA[$_area_id]
//                    +  $total_value_a5s_KA[$_area_id] 
                    + $total_value_a1k_KA[$_area_id]
//                    + $total_value_reno2f_KA[$_area_id]
                    + $total_value_a1k_subsidi_KA[$_area_id]
//                    + $total_value_k3_KA[$_area_id]
                    ) * $chiet_khau_KA[$_area_id]) / 100) 
//                     + (($total_value_reno2f_KA[$_area_id]) * ($chiet_khau_KA[$_area_id] + 1) / 100)        
                        
                                ,1));
            else
                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
		*/


        // round ( ($value_tinh_kpi/$total_value_tinh_kpi_finnaly) * 60 / ($area_regionshare[$_area_id]/100), 2 )
        //
        // round ( ($value_tinh_kpi/$total_value_tinh_kpi_finnaly) * 60 / ($_area['region_share']/100), 2 )



        $filename  = 'Sell out - Area - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        exit;
    }

    public static function leader($params) {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', ~E_ALL);
        require_once 'PHPExcel.php';
        $PHPExcel    = new PHPExcel();
        $heads       = array(
            'Staff',
            'Code',
            'Title',
            'Email',
            'Province',
        );
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $db         = Zend_Registry::get('db');
        $from_f     = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d 00:00:00");
        $to_f       = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d 23:59:59");
        $sql_params = [
            $from_f,
            $to_f,
            '',
            -1
        ];

        $sql           = 'CALL sp_report_leader_asm(?,?,?,?,@total_value)';
        $stmt          = $db->query($sql, $sql_params);
        $list_data     = $stmt->fetchAll();
        $list_by_staff = My_Util::changeKey($list_data, "sale_region_staff_log_id");

        $stmt->closeCursor();

        // các model có đổi giá
        $QKpi     = new Application_Model_GoodPriceLog();
        $list     = $QKpi->get_list($from, $to);
        // echo "<pre>";print_r($list);die;
        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        // echo "<pre>";print_r($list);die;
        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        $QImeiKpi      = new Application_Model_ImeiKpi();
        $params['kpi'] = true;
        $data          = $QImeiKpi->fetchLeader($params);

        $data_asm = $QImeiKpi->fetchRegionShareAsm($params);

        // $data_asm           = $QImeiKpi->fetchRegionShareAsm($params);

        unset($params['kpi']);
        $params['get_total_sales'] = true;

        /** total sales phải lấy số của cả nước */
        $total_sales = $QImeiKpi->fetchArea($params);
//        echo "<pre>";print_r($data);die;
        // $total_money    = $total_sales['total_value'];die;
        // echo "<pre>";print_r($total_money);die;
        // if($userStorage->id == 5899){
        //     // echo date('t',strtotime($from));die;
        //     // echo "<pre>";print_r($total_money);die;
        // }
        unset($params['get_total_sales']);

        $point_list    = array();
        $data_area_sum = array(); // dùng để cộng các giá trị của leader
        $asm_array     = array(); // danh sách các asm sẽ nắm các vùng chưa có leader
        $sales         = array();
        $kpi_list      = array();
        $info_list     = array(); // thông tin liên quan leader

        foreach ($data as $_key => $_value) {

            $kpi_list[$_value['sales_region_staff_log_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
                'total_quantity'        => intval($_value['total_quantity']),
                'total_activated'       => intval($_value['total_activated']),
                'total_value'           => $_value['total_value'],
                'total_value_bonus'     => $_value['total_value_bonus'],
                'total_value_activated' => $_value['total_value_activated'],
                'total_sellout_sale'    => intval($_value['total_sellout_sale']),
            );

            $total_day_in_month = date('t', strtotime($from));
            // $dateFrom = null;
            if (strtotime($_value['staff_from_date']) <= strtotime($from)) {
                $dateFrom = $from;
            } else {
                $dateFrom = $_value['staff_from_date'];
            }

            if (empty($_value['staff_to_date'])) {
                $dateTo = date('Y-m-t', strtotime($from));
            } else {
                $dateTo = ($_value['staff_to_date']);
            }
            // $date_1   =  date_create_from_format("d/m/Y", $dateFrom)->format("Y-m-d");
            // $date_2   =  date_create_from_format("d/m/Y", $dateTo)->format("Y-m-d");
            $day_diff                                        = My_Date::date_diff($dateFrom, $dateTo);
            $info_list[$_value['sales_region_staff_log_id']] = array(
//                'staff_name'         => $_value['staff_name'],
//                'code'               => $_value['code'],
//                'email'              => $_value['email'],
//                'region_name'        => $_value['region_name'],
//                'region_shared'      => $_value['region_shared'],
//                'area_name'          => $_value['area_name'],
//                'area_shared'        => $_value['area_shared'],
//                'staff_title'        => $_value['staff_title'],
//                // 'joined_at' => date('d/m/Y', strtotime($_value['joined_at'])),
//                // 'off_date' => isset($_value['off_date']) ? $_value['off_date'] : null
//                'from_date'          => date('d/m/Y', strtotime($_value['staff_from_date'])),
//                // 'to_date' => isset($_value['staff_to_date']) ?  date('d/m/Y', strtotime($_value['staff_to_date'])) : null,
//                // 'from_date' => strtotime(date('d/m/Y', strtotime($_value['staff_from_date']))),
//                'to_date'            => isset($_value['staff_to_date']) ? date('d/m/Y', strtotime($_value['staff_to_date'])) : null,
//                // 'date_from' => $dateFrom,
//                // 'date_to' => $dateTo,
//                'date_from'          => date('d/m/Y', strtotime($dateFrom)),
//                'date_to'            => date('d/m/Y', strtotime($dateTo)),
//                'date_diff'          => $day_diff + 1,
//                'total_day_in_month' => $total_day_in_month,
//                'value_80_off'       => empty($_value['staff_to_date']) ? 999 : $QImeiKpi->getValue80(date('d/m/Y', strtotime($dateFrom)), date('d/m/Y', strtotime($dateTo)), null),
            );
        }

        // echo "<pre>";print_r($kpi_list);die;
//        foreach ($data_asm as $_value) {
//
//            $kpi_list[$_value['sales_region_staff_log_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
//                'total_quantity'        => intval($_value['total_quantity']),
//                'total_activated'       => intval($_value['total_activated']),
//                'total_value'           => $_value['total_value'],
//                'total_value_bonus'     => $_value['total_value_bonus'],
//                'total_value_activated' => $_value['total_value_activated'],
//            );
//
//            // $info_list[ $_value['sales_region_staff_log_id'] ] = array(
//            //     'from_date' => date('d/m/Y', strtotime($_value['from_date'])),
//            // );
//        }



        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                        }
                    }
                }
            }
        }
        // echo "<pre>";print_r($list);die;
        $heads[] = 'Unit';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' activated';
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' activated';
                        }
                    }
                }
            }
        }
        // echo "<pre>";print_r($product_color_list);die;
        $heads[] = 'Unit activated';
        $heads[] = 'Value (80%)';
        // $heads[] = 'KPI Sale';
        // $heads[] = 'Sellout Sale';
        $heads[] = 'Value activated (80%)';
        $heads[] = 'Area';
        $heads[] = 'Area Share';
        // $heads[] = 'Province';
        $heads[] = 'Province Share (%)';

        $heads[] = 'Point';
        // $heads[] = 'Point mới';
        $heads[] = '% Doanh Thu';
        $heads[] = 'KPI';
        $heads[] = 'From date';
        $heads[] = 'To date';
        $heads[] = 'Date 1';
        $heads[] = 'Date 2';
        $heads[] = 'Số ngày làm việc';
        $heads[] = 'Tổng số ngày trong tháng';
        $heads[] = 'Tổng số value';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        // echo "<pre>";print_r($heads);die;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;

        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        // echo $alpha;die;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        // dùng mảng này để lưu thứ tự các area trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $leader_array = array();



        // điền danh sách area ra file trước
        foreach ($info_list as $_staff_id => $_info) {
            $alpha                    = 'A';
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['staff_name']);
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['code']);
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['staff_title']);
            $sheet->setCellValue($alpha++ . $index, str_replace(EMAIL_SUFFIX, '', $list_by_staff[$_staff_id]['email']));
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['region_name']);
            $leader_array[$_staff_id] = $index++; // lưu dòng ứng với area id
        }

        // echo "<pre>";print_r($leader_array);die;
        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha             = $product_col;
        $arr_total         = array();
        $arr_active        = array();
        $arr_value         = array();
        // $arr_value_bonus           = array();
        $arr_value_active  = array();
        $arr_value_sellout = array();
        // echo "<pre>";print_r(count($list));
        // echo "<pre>";print_r($list);    die;
        // echo "<pre>";print_r($kpi_list);
        // die;

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            if (!isset($arr_total[$_staff_id])) {
                                $arr_total[$_staff_id] = 0;
                            }

                            if (!isset($arr_active[$_staff_id])) {
                                $arr_active[$_staff_id] = 0;
                            }

                            // if (!isset($arr_value[ $_staff_id ])){
                            //     $arr_value[ $_staff_id ] = 0;
                            // }
                            // if (!isset($arr_value_bonus[ $_staff_id ])){
                            //     $arr_value_bonus[ $_staff_id ] = 0;
                            // }

                            if (!isset($arr_value_active[$_staff_id])) {
                                $arr_value_active[$_staff_id] = 0;
                            }

                            if (!isset($arr_value_sellout[$_staff_id])) {
                                $arr_value_sellout[$_staff_id] = 0;
                            }

                            $arr_total[$_staff_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                            $arr_active[$_staff_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;


                            $arr_value[$_staff_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_ += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            // $arr_value_bonus[$_staff_id]  += isset($tmp['total_value_bonus']) ? $tmp['total_value_bonus'] : 0;
                            // $arr_value_sellout[$_staff_id]  += isset($tmp['total_sellout_sale']) ? $tmp['total_sellout_sale'] : 0;
                            $arr_value_active[$_staff_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                            // $sheet->setCellValue($alpha . $leader_array[$_staff_id],intval($tmp['total_quantity']));
                            if (isset($leader_array[$_staff_id])) {
                                $sheet->setCellValue($alpha . $leader_array[$_staff_id], intval($tmp['total_quantity']));
                            }

                            unset($tmp);
                        }
                        // echo "<pre>";print_r($arr_value);die;
                        $alpha++;
                    }
                }
            }
        }

        //unit
        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_total[$_staff_id])) {
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_quantity']);
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
        }

        $alpha++;

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            if (!isset($arr_total[$_staff_id])) {
                                $arr_total[$_staff_id] = 0;
                            }

                            if (!isset($arr_active[$_staff_id])) {
                                $arr_active[$_staff_id] = 0;
                            }

                            if (!isset($arr_value[$_staff_id])) {
                                $arr_value[$_staff_id] = 0;
                            }

                            if (!isset($arr_value_bonus[$_staff_id])) {
                                $arr_value_bonus[$_staff_id] = 0;
                            }

                            if (!isset($arr_value_active[$_staff_id])) {
                                $arr_value_active[$_staff_id] = 0;
                            }
                            if (!isset($arr_value_sellout[$_staff_id])) {
                                $arr_value_sellout[$_staff_id] = 0;
                            }


                            if (isset($leader_array[$_staff_id])) {
                                $sheet->setCellValue($alpha . $leader_array[$_staff_id], intval($tmp['total_activated']));
                            }
                            unset($tmp);
                        }

                        $alpha++;
                    }
                }
            }
        }

        //activated
        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_active[$_staff_id])) {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], intval($arr_active[$_staff_id]));
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_activated']);
        }

        $alpha++;

        //value
        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_value[$_staff_id])) {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $arr_value[$_staff_id]);
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_value']);
        }

        $alpha++;


        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_value_active[$_staff_id])) {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $arr_value_active[$_staff_id]);
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_value_activated']);
        }

        $alpha++;

        foreach ($leader_array as $_staff_id => $_info) {

            $tmp_alpha = $alpha;
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['area_name']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['area_shared']);
            // $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $info_list[$_staff_id]['region_name']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['province_share']);


            // $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $point);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['point']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['point_doanh_thu']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['value_kpi']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['staff_from_date']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['staff_to_date']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['final_from']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['final_to']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['day_diff']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['day_diff']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['sum_total_value']);
        }


        $filename  = 'Sell out - Leader - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public static function leader_bk($params) {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', ~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'Leader',
            'Code',
            'Email',
            'Province',
        );

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list($from, $to);

        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();



        $QImeiKpi      = new Application_Model_ImeiKpi();
        $params['kpi'] = true;
        $data          = $QImeiKpi->fetchLeader($params);

        unset($params['kpi']);

        $params['get_total_sales'] = true;
        //$total_sales = $QImeiKpi->fetchLeader($params);
        $total_sales               = $QImeiKpi->fetchArea($params);
        $total_money               = $total_sales['total_value'];
        unset($params['get_total_sales']);

        $point_list     = array();
        $data_for_point = $QImeiKpi->fetchLeader($params);
        $sales          = array();

        //tính point
        foreach ($data_for_point as $item) {
            $point_list[$item['staff_id']] = ( $total_money > 0 and ( $item ['region_shared'] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($item ['region_shared'] / 100), 2) : 0;
        }

        $kpi_list  = array();
        $info_list = array(); // thông tin liên quan leader

        foreach ($data as $_key => $_value) {
            $kpi_list[$_value['staff_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
                'total_quantity'        => $_value['total_quantity'],
                'total_activated'       => $_value['total_activated'],
                'total_value'           => $_value['total_value'],
                'total_value_activated' => $_value['total_value_activated'],
            );

            $info_list[$_value['staff_id']] = array(
                'staff_name'    => $_value['staff_name'],
                'code'          => $_value['code'],
                'email'         => $_value['email'],
                'region_name'   => $_value['region_name'],
                'region_shared' => $_value['region_shared'],
                'area_name'     => $_value['area_name'],
                'area_shared'   => $_value['area_shared'],
            );
        }

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                        }
                    }
                }
            }
        }

        $heads[] = 'Unit';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' activated';
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' activated';
                        }
                    }
                }
            }
        }

        $heads[] = 'Unit activated';
        $heads[] = 'Value (80%)';
        $heads[] = 'Value activated (80%)';
        $heads[] = 'Area';
        $heads[] = 'Area Share';
        $heads[] = 'Province';
        $heads[] = 'Province Share (%)';
        $heads[] = 'Point';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        // dùng mảng này để lưu thứ tự các area trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $leader_array = array();

        // điền danh sách area ra file trước

        foreach ($info_list as $_staff_id => $_info) {
            $alpha                    = 'A';
            $sheet->setCellValue($alpha++ . $index, $_info['staff_name']);
            $sheet->setCellValue($alpha++ . $index, $_info['code']);
            $sheet->setCellValue($alpha++ . $index, str_replace(EMAIL_SUFFIX, '', $_info['email']));
            $sheet->setCellValue($alpha++ . $index, $_info['region_name']);
            $leader_array[$_staff_id] = $index++; // lưu dòng ứng với area id
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;

        $total_by_leader           = array();
        $total_activated_by_leader = array();
        $total_value_by_leader     = array();
        $total_value               = 0;
        $total_unit                = 0;
        $total_value_activated     = 0;
        $total_unit_activated      = 0;

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            if (!isset($total_by_leader[$_staff_id]))
                                $total_by_leader[$_staff_id]               = 0;
                            if (!isset($total_value_by_leader[$_staff_id]))
                                $total_value_by_leader[$_staff_id]         = 0;
                            if (!isset($total_value_activated_by_area[$_staff_id]))
                                $total_value_activated_by_area[$_staff_id] = 0;

                            $total_by_leader[$_staff_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;

                            $total_value_by_leader[$_staff_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;

                            $total_value_activated_by_area[$_staff_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                            $total_value += isset($tmp['total_value']) ? $tmp['total_value'] : 0;

                            $total_value_activated += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                            $total_unit += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;

                            $sheet->setCellValue($alpha . $leader_array[$_staff_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0);

                            unset($_data);
                            unset($tmp);
                        }

                        $alpha++;
                    }
                }
            }
        }

        foreach ($leader_array as $_staff_id => $_value) {
            if (isset($total_by_leader[$_staff_id]))
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $total_by_leader[$_staff_id]);
            else
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], '-');
        }

        $alpha++;

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_area_id => $_data) {
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            if (!isset($total_activated_by_leader[$_area_id]))
                                $total_activated_by_leader[$_area_id] = 0;
                            $total_activated_by_leader[$_area_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                            $total_unit_activated += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                            $sheet->setCellValue($alpha . $leader_array[$_area_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0);

                            unset($_data);
                            unset($tmp);
                        }

                        $alpha++;
                    }
                }
            }
        }

        foreach ($leader_array as $_staff_id => $_value) {
            if (isset($total_activated_by_leader[$_staff_id]))
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $total_activated_by_leader[$_staff_id]);
            else
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], '-');
        }

        $alpha++;

        foreach ($leader_array as $_staff_id => $_value) {
            if (isset($total_value_by_leader[$_staff_id]))
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $total_value_by_leader[$_staff_id]);
            else
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], '-');
        }

        $alpha++;

        foreach ($leader_array as $_staff_id => $_value) {
            if (isset($total_value_activated_by_area[$_staff_id]))
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $total_value_activated_by_area[$_staff_id]);
            else
                $sheet->setCellValue($alpha . $leader_array[$_staff_id], '-');
        }

        $alpha++;

        foreach ($leader_array as $_staff_id => $_info) {
            $tmp_alpha = $alpha;
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $info_list[$_staff_id]['area_name']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $info_list[$_staff_id]['area_shared']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $info_list[$_staff_id]['region_name']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $info_list[$_staff_id]['region_shared']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $point_list[$_staff_id]);
        }

        $filename  = 'Sell out - Leader - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        exit;
    }

    public static function storeList($stores) {
        // echo "<pre>";print_r($stores);die;
        $db = Zend_Registry::get('db');
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'No.',
            'Store ID',
            'Store Name',
			'Store ID Realme',
			'Store Name Realme',
			'Store Realme Code',
			'Link Oppo-Realme',
            'Area',
            'Province',
            'District',
            'Address',
            'Shipping Address',
            'Phone Number',
            'Dealer ID',
            'Partner ID',
            'Dealer',
            'PGPB',
            'Code PGPB',
            'Sales Trực tiếp',
            'Code Sales Trực tiếp',
            'Sales Leader Gián tiếp',
            'Code Sales Leader Gián tiếp',
            'Store Leader',
            'Code Store Leader',
            'Status',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }


        $index = 2;

        $i          = 1;
        $QDealer    = new Application_Model_Distributor();
        $all_dealer = $QDealer->get_cache();

        ////////////////////////////////////////////////////
        /////////////////// Xuất
        ////////////////////////////////////////////////////
        $no = 1;



        foreach ($stores as $store) {



            $alpha = 'A';
			
			$oppoRealme = '';
			if($store['link_system'] == 1) {
				$oppoRealme = 'Oppo tạo';
			} elseif($store['link_system'] == 2) {
				$oppoRealme = 'Realme tạo';
			} else {
				$oppoRealme = '';
			}

            $sheet->getCell($alpha++ . $index)->setValueExplicit($no++, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['realme_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['realme_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['store_realme_code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($oppoRealme, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['regional_market_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['district_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['company_address'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['shipping_address'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($store['phone_number'], PHPExcel_Cell_DataType::TYPE_STRING);






            $sheet->getCell($alpha++ . $index)->setValueExplicit(!is_null($store['d_id']) && $store['d_id'] != 0 ? $store['d_id'] : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($all_dealer[$store['d_id']]['partner_id']) ? $all_dealer[$store['d_id']]['partner_id'] : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($all_dealer[$store['d_id']]['title']) ? $all_dealer[$store['d_id']]['title'] : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['name_pg']) ? ($store['name_pg']) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['code_pg']) ? $store['code_pg'] : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['firstname_sale_tructiep']) && isset($store['lastname_sale_tructiep']) ? ($store['firstname_sale_tructiep'] . ' ' . $store['lastname_sale_tructiep']) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['code_sale_tructiep']) ? $store['code_sale_tructiep'] : '', PHPExcel_Cell_DataType::TYPE_STRING);



            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['firstname_sale_giantiep']) && isset($store['lastname_sale_giantiep']) ? ($store['firstname_sale_giantiep'] . ' ' . $store['lastname_sale_giantiep']) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['code_sale_giantiep']) ? $store['code_sale_giantiep'] : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['firstname_store_leader']) && isset($store['lastname_store_leader']) ? ($store['firstname_store_leader'] . ' ' . $store['lastname_store_leader']) : '', PHPExcel_Cell_DataType::TYPE_STRING);




            $sheet->getCell($alpha++ . $index)->setValueExplicit(isset($store['code_store_leader']) ? $store['code_store_leader'] : '', PHPExcel_Cell_DataType::TYPE_STRING);
          $sheet->getCell($alpha++.$index)->setValueExplicit((isset($store['del']) AND $store['del']) ? 'Deleted' : '', PHPExcel_Cell_DataType::TYPE_STRING);




            $index++;
        }

        $filename  = 'Report_Store_List_' . date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;


        // echo "<pre>";print_r($stores);die;
    }

    public static function historicalDeduction($sql) {
        set_time_limit(0);
        error_reporting(0);
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);
        $filename = 'Sell in Deduction - ' . date('d-m-Y H-i-s');
        while (@ob_end_clean());
        ob_start();
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        // echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
        $output   = fopen('php://output', 'w');

        $head = array(
            'No.',
            'Dealer_id',
            'Ma_OP',
            'Ma_don_hang',
            'Invoice_Number',
            'Invoice_Time',
            'Ky_hieu_hoa_don',
            'Area_Name',
            'Province',
            'Dealer_Name',
            'Level_daily',
            'Model',
            'Quantity',
            'Doanh_thu_VAT',
            'Doanh_thu_chua_VAT',
        );

        fputcsv($output, $head);

        $QLoyaltyPlan = new Application_Model_LoyaltyPlan();
        $levels       = $QLoyaltyPlan->get_cache();

        $QGood = new Application_Model_Good();
        $goods = $QGood->get_cache();

        $db     = Zend_Registry::get('db');
        $result = $db->query($sql);
        $i      = 1;

        if ($result)
            foreach ($result as $key => $value) {
                $row   = array();
                $row[] = $i++;
                $row[] = $value['distributor_id'];
                $row[] = '="' . $value['store_code'] . '"';
                $row[] = '="' . $value['sales_sn'] . '"';
                $row[] = '="' . $value['invoice_number'] . '"';
                $row[] = $value['invoice_time'];
                $row[] = '="' . $value['invoice_sign'] . '"';
                $row[] = isset($value['district']) ? My_Region::getValue($value['district'], My_Region::Area) : '#';
                $row[] = isset($value['district']) ? My_Region::getValue($value['district'], My_Region::Province) : '#';
                $row[] = $value['title'];
                $row[] = isset($levels[$value['level']]) ? $levels[$value['level']] : '#';
                $row[] = isset($goods[$value['product_id']]) ? $goods[$value['product_id']] : '#';
                $row[] = $value['quantity'];
                $row[] = $value['value_vat'];
                $row[] = $value['value'];

                fputcsv($output, $row);

                unset($row);
                unset($value);
            }

        unset($result);
        unset($sql);

        exit;
    }

    /**
     * Description
     * @param type $sql
     * @return type
     */
    public static function historicalIncentive($sql) {
        set_time_limit(0);
        error_reporting(0);
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);
        $filename = 'Incentive - ' . date('d-m-Y H-i-s');
        while (@ob_end_clean()) {
            
        };
        ob_start();
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        // echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
        $output = fopen('php://output', 'w');

        $head = array(
            'No.',
            'Dealer ID',
            'Store Code',
            'Dealer Name',
            'Area',
            'Province',
            'Level',
            'Model',
            'Quantity',
            'Value per Unit',
            'Total',
        );

        fputcsv($output, $head);

        $QLoyaltyPlan = new Application_Model_LoyaltyPlan();
        $levels       = $QLoyaltyPlan->get_cache();

        $QGood = new Application_Model_Good();
        $goods = $QGood->get_cache();

        $db     = Zend_Registry::get('db');
        $result = $db->query($sql);
        $i      = 1;

        if ($result) {
            foreach ($result as $key => $value) {
                $row   = array();
                $row[] = $i++;
                $row[] = $value['distributor_id'];
                $row[] = '="' . $value['store_code'] . '"';
                $row[] = $value['title'];
                $row[] = isset($value['district']) ? My_Region::getValue($value['district'], My_Region::Area) : '#';
                $row[] = isset($value['district']) ? My_Region::getValue($value['district'], My_Region::Province) : '#';
                $row[] = isset($levels[$value['level']]) ? $levels[$value['level']] : '#';
                $row[] = isset($goods[$value['product_id']]) ? $goods[$value['product_id']] : '#';
                $row[] = $value['quantity'];
                $row[] = $value['value_unit'];
                $row[] = $value['total'];

                fputcsv($output, $row);
                unset($row);
                unset($value);
            }
        }

        unset($result);
        unset($sql);

        exit;
    }

    public static function preventTime($from_date, $to_date) {
        
    }
    
    public static function dealer_ka_New($from, $to, $result) {
	
        set_time_limit(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'Distributor ID',
            'Distributor Name',
            'Address',
            'Area',
            'Province',
            'District',
            'Partner ID',
            'Chanel',
            'Level',
        );

        $from = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $to)->format("Y-m-d");

        $QImeiKpi          = new Application_Model_ImeiKpi();
        $distributor_store = $QImeiKpi->fetchStoreSellOutKaNew(array('from_date' => $from, 'to_date' => $to));

        $distributor_store_active = $QImeiKpi->fetchStoreActiveKa();

        // echo "<pre>";print_r($distributor_store_active);die;
        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list($from, $to);
     
        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Total';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' Activated';
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' Activated';
                    }
                }
            }
        }

        $heads = array_merge($heads, array(
            'Total Activated',
            'Value',
            'Value Activated',
            'Store sell out number',
            'Store nummber'
        ));

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        $all_store = array();

        $QDistributor      = new Application_Model_Distributor();
        $distributor_cache = $QDistributor->get_cache();

        $db     = Zend_Registry::get('db');
      
        $result = $db->query($result);

        foreach ($result as $_key => $_so) {
          
            $sell_out_list[$_so['dealer_id']][$_so['good_id']][$_so['color_id']][$_so['from_date'] . '_' . $_so['to_date']] = array(
                'total_quantity'        => $_so['total_quantity'],
                'total_activated'       => $_so['total_activated'],
                'total_value'           => $_so['total_value'],
                'total_value_activated' => $_so['total_value_activated'],
            );

            if (!isset($all_store[$_so['dealer_id']]) ){
                $all_store[$_so['dealer_id']] = array(
                    'store_id'        => $_so['dealer_id'],
                    'store_name'      => ($_so['channel'] == 11) ? 'Điện máy xanh_KA' :  $distributor_cache[$_so['dealer_id']]['title'],
                    'store_district'  => $distributor_cache[$_so['dealer_id']]['district'],
                    'company_address' => $distributor_cache[$_so['dealer_id']]['add'],
                    'partner_id'      => $distributor_cache[$_so['dealer_id']]['partner_id'],
                    'store_chanel'    => $_so['store_chanel'],
                    'store_level'     => $_so['store_level'],
                );
            }
                
        }

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $store_array = array();

        // điền danh sách staff ra file trước
        foreach ($all_store as $key => $store) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $store['store_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_name']);
            $sheet->setCellValue($alpha++ . $index, $store['company_address']);

            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Area) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Province) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::District) : '#');

            $sheet->setCellValue($alpha++ . $index, $store['partner_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_chanel']);
            $sheet->setCellValue($alpha++ . $index, $store['store_level']);

            $alpha++;

            $store_array[$store['store_id']] = $index; // lưu dòng ứng với store id
            $index++;
        }

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;

        $total_by_store           = array();
        $total_activated_by_store = array();
        $value_by_store           = array();
        $value_activated_by_store = array();
        $store_sellout_number     = array();
        $store_number             = array();


        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id])) { // đưa vào đúng dòng luôn
                                if (isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) {
                                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                                    $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0
                                    );

                                    if (!isset($total_by_store[$_store_id]))
                                        $total_by_store[$_store_id] = 0;
                                    $total_by_store[$_store_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;

                                    if (!isset($total_activated_by_store[$_store_id]))
                                        $total_activated_by_store[$_store_id] = 0;
                                    $total_activated_by_store[$_store_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                                    if (!isset($value_by_store[$_store_id]))
                                        $value_by_store[$_store_id] = 0;
                                    $value_by_store[$_store_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;

                                    if (!isset($value_activated_by_store[$_store_id]))
                                        $value_activated_by_store[$_store_id] = 0;
                                    $value_activated_by_store[$_store_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                                    unset($tmp);
                                } // END inner IF
                            } // END outer IF

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach

                    unset($ranges);
                } // END outer foreach
            } // END big IF

            unset($value);
        } // END big foreach

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_by_store[$_store_id]);

        $alpha++;
        unset($total_by_store);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {

                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id]) && isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) { // đưa vào đúng dòng luôn
                                $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];
                                $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0
                                );

                                unset($tmp);
                            } // END if

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach
                    unset($ranges);
                } // END outer foreach
            } // end big id
        } // end big foreach
        unset($sell_out_list);
        unset($products);
        unset($list);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_activated_by_store[$_store_id]);

        $alpha++;
        unset($total_activated_by_store);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($value_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $value_by_store[$_store_id]);

        $alpha++;
        unset($value_by_store);

        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($value_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $value_activated_by_store[$_store_id]);
        }

        //store sellout number
        $alpha++;
        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($distributor_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $distributor_store[$_store_id]['store_sellout_number']);
            else {
                $sheet->setCellValue($alpha . $store_array[$_store_id], 0);
            }
        }

        $alpha++;
        //store number
        foreach ($all_store as $_store_id => $_data) {
            // nếu có trong danh sách NV ở trên
            if (isset($distributor_store_active[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $distributor_store_active[$_store_id]['store_number']);
            else {
                $sheet->setCellValue($alpha . $store_array[$_store_id], 0);
            }
        }


        unset($all_store);
        unset($alpha);
        unset($value_activated_by_store);
        unset($store_array);

        $filename  = 'Sell Out - Distributor - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

}
