<?php
/**
* @author buu.pham
*/
class My_Report_CallCenter
{
    public static function overal($params)
    {
        set_time_limit(0);
        error_reporting(E_ALL);
        ini_set('display_error', 1);
        ini_set('memory_limit', -1);

        $filename = 'Customer Info Checking - Report - ' . date('d-m-Y H-i-s');
        while(@ob_end_clean());
        ob_start();

        $db = Zend_Registry::get('db');

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $from = date_create_from_format('d/m/Y', $params['from'])->format('Y-m-d');
        $to = date_create_from_format('d/m/Y', $params['to'])->format('Y-m-d 23:59:59');

        $QStaff = new Application_Model_Staff();
        $staffs = $QStaff->get_all_cache();

        // get history
        $QHistory = new Application_Model_CustomerCheckCallHistory();
        $result = $QHistory->getSummary($from, $to);

        $sheet_number = 1;
        // loop through history
        foreach ($result as $_staff_id => $_weeks) {
            // create a sheet for each staff
            $sheet = $PHPExcel->createSheet($sheet_number++);

            if (isset($staffs[ $_staff_id ]['name']))
                $sheet->setTitle($staffs[ $_staff_id ]['name']);

            $index = 11;

            // loop through weeks
            $stt = 1;
            $total_ok = $total_not_ok = $total_not_confirmed = $total_resurvey = $total_total = 0;
            foreach ($_weeks as $_week => $_days) {
                if (!$_days || !count($_days)) continue;
                $sheet->getStyle('A'. $index)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFCC00')
                        )
                    )
                );
                $sheet->setCellValue('A'. $index++, 'W'.$_week);
                $alpha = 'A';

                // create table for each week
                $heads = array(
                    'NO.',
                    'TIME',
                    'AGENT RESULT',
                );

                $start_alpha = $start_alpha_0 = $alpha;
                $start_index = $index;

                foreach ($heads as $key)
                    $sheet->setCellValue($alpha++ . $index, $key);

                $alpha = 'C';
                $index++;

                $heads = array(
                    'OK',
                    'NOT OK',
                    'RESURVEY',
                    'NOT CONFIRMED'  ,
                    'TOTAL',
                );

                foreach ($heads as $key)
                {
                    $sheet->setCellValue($alpha++ . $index, $key);
                    $alpha++;
                }

                $index++;
                $alpha = 'C';

                $heads = array(
                    'QUANTITY',
                    'RATE',
                    'QUANTITY',
                    'RATE',
                    'QUANTITY',
                    'RATE',
                    'QUANTITY',
                    'RATE',
                );

                foreach ($heads as $key)
                    $sheet->setCellValue($alpha++ . $index, $key);

                $sheet->mergeCells($start_alpha.$start_index.':'.$start_alpha.($start_index+2));
                $start_alpha++;
                $sheet->mergeCells($start_alpha.$start_index.':'.$start_alpha.($start_index+2));
                $start_alpha++;
                $num = ord($start_alpha) - ord('A') + 9;
                $sheet->mergeCells($start_alpha.$start_index.':'.My_Excel_Column::getNameFromNumber($num).$start_index);

                $index--;
                $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
                $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
                $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
                $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
                $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);

                $start_alpha_1 = ord($start_alpha_0) - ord('A') + 11;
                $sheet->getStyle('A'. ($index-1).':'.My_Excel_Column::getNameFromNumber($start_alpha_1).($index+1))->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '55E0FF')
                        ),
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            ),
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        ),
                    )
                );

                $index += 2;
                $ok = $not_ok = $not_confirmed = $resurvey = $total = 0;

                foreach ($_days as $_date => $_item) {
                    // Các dòng thông tin từng ngày
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $stt++);
                    $sheet->setCellValue($alpha++ . $index, $_date && strtotime($_date) ? date('m/d/Y', strtotime($_date)) : '');

                    $sheet->setCellValue($alpha++ . $index, $_item['ok']);
                    $ok += $_item['ok'];
                    $total_ok += $_item['ok'];
                    $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                    $sheet->setCellValue($alpha++ . $index, $_item['ok'] / $_item['total']);
                    ////////////////////////////////////////////////////////////////

                    $sheet->setCellValue($alpha++ . $index, $_item['not_ok']);
                    $not_ok += $_item['not_ok'];
                    $total_not_ok += $_item['not_ok'];
                    $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                    $sheet->setCellValue($alpha++ . $index, $_item['not_ok'] / $_item['total']);
                    ////////////////////////////////////////////////////////////////

                    $sheet->setCellValue($alpha++ . $index, $_item['not_confirmed']);
                    $not_confirmed += $_item['not_confirmed'];
                    $total_not_confirmed += $_item['not_confirmed'];
                    $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                    $sheet->setCellValue($alpha++ . $index, $_item['not_confirmed'] / $_item['total']);
                    ////////////////////////////////////////////////////////////////

                    $sheet->setCellValue($alpha++ . $index, $_item['resurvey']);
                    $resurvey += $_item['resurvey'];
                    $total_resurvey += $_item['resurvey'];
                    $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                    $sheet->setCellValue($alpha++ . $index, $_item['resurvey'] / $_item['total']);
                    ////////////////////////////////////////////////////////////////

                    $sheet->setCellValue($alpha++ . $index, $_item['total']);
                    $total += $_item['total'];
                    $total_total += $_item['total'];
                    $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                    $sheet->setCellValue($alpha++ . $index, 1);

                    $index++;
                }

                // Dòng Total
                $alpha = 'A';
                $sheet->setCellValue($alpha.$index, 'Total');
                $num = ord($alpha) - ord('A') + 1;
                $sheet->getStyle($alpha.$index.':'.My_Excel_Column::getNameFromNumber($num).$index)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '55E0FF')
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        ),
                    )
                );
                $sheet->mergeCells($alpha++.$index.':'.$alpha++.$index);

                $sheet->setCellValue($alpha++ . $index, $ok);
                $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                $sheet->setCellValue($alpha++ . $index, 1);
                ////////////////////////////////////////////////////////////////

                $sheet->setCellValue($alpha++ . $index, $not_ok);
                $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                $sheet->setCellValue($alpha++ . $index, 1);
                ////////////////////////////////////////////////////////////////

                $sheet->setCellValue($alpha++ . $index, $not_confirmed);
                $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                $sheet->setCellValue($alpha++ . $index, 1);
                ////////////////////////////////////////////////////////////////

                $sheet->setCellValue($alpha++ . $index, $resurvey);
                $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                $sheet->setCellValue($alpha++ . $index, 1);
                ////////////////////////////////////////////////////////////////

                $sheet->setCellValue($alpha++ . $index, $total);
                $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
                $sheet->setCellValue($alpha++ . $index, 1);

                $index++;


                $index += 2;
            } // end week

            // summary //////////////////////////////////
            $index = 1;
            $sheet->getStyle('A'. $index)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'FFCC00')
                    )
                )
            );
            $sheet->setCellValue('A'. $index++, 'SUMMARY');
            $alpha = 'A';

            // create table
            $heads = array(
                'NO.',
                'STATUS',
                'AGENT RESULT',
            );

            $start_alpha = $start_alpha_0 = $alpha;
            $start_index = $index;

            foreach ($heads as $key)
                $sheet->setCellValue($alpha++ . $index, $key);

            $alpha = 'C';
            $index++;

            $heads = array(
                'OK',
            );

            foreach ($heads as $key)
            {
                $sheet->setCellValue($alpha++ . $index, $key);
                $alpha++;
            }

            $index++;
            $alpha = 'C';

            $heads = array(
                'QUANTITY',
                'RATE',
            );

            foreach ($heads as $key)
                $sheet->setCellValue($alpha++ . $index, $key);

            $sheet->mergeCells($start_alpha.$start_index.':'.$start_alpha.($start_index+2));
            $start_alpha++;
            $sheet->mergeCells($start_alpha.$start_index.':'.$start_alpha.($start_index+2));
            $start_alpha++;
            $num = ord($start_alpha) - ord('A') + 1;
            $sheet->mergeCells($start_alpha.$start_index.':'.My_Excel_Column::getNameFromNumber($num).$start_index);

            $index--;
            $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
            $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
            $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
            $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);
            $sheet->mergeCells($start_alpha++.$index.':'.$start_alpha++.$index);

            $start_alpha_1 = ord($start_alpha_0) - ord('A') + 3;
            $sheet->getStyle('A'. ($index-1).':'.My_Excel_Column::getNameFromNumber($start_alpha_1).($index+1))->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '55E0FF')
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        ),
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                )
            );

            $alpha = 'A';
            $index++;
            $index++;

            $sheet->setCellValue($alpha++ . $index, 1);
            $sheet->setCellValue($alpha++ . $index, 'OK');
            $sheet->setCellValue($alpha++ . $index, $total_ok);
            $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
            $sheet->setCellValue($alpha++ . $index, $total_ok / $total_total);

            $alpha = 'A';
            $index++;

            $sheet->setCellValue($alpha++ . $index, 2);
            $sheet->setCellValue($alpha++ . $index, 'NOT OK');
            $sheet->setCellValue($alpha++ . $index, $total_not_ok);
            $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
            $sheet->setCellValue($alpha++ . $index, $total_not_ok / $total_total);

            $alpha = 'A';
            $index++;

            $sheet->setCellValue($alpha++ . $index, 3);
            $sheet->setCellValue($alpha++ . $index, 'NOT CONFIRMED');
            $sheet->setCellValue($alpha++ . $index, $total_not_confirmed);
            $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
            $sheet->setCellValue($alpha++ . $index, $total_not_confirmed / $total_total);

            $alpha = 'A';
            $index++;

            $sheet->setCellValue($alpha++ . $index, 4);
            $sheet->setCellValue($alpha++ . $index, 'RESURVEY');
            $sheet->setCellValue($alpha++ . $index, $total_resurvey);
            $sheet->getStyle($alpha . $index)->getNumberFormat()->applyFromArray(array('code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE));
            $sheet->setCellValue($alpha++ . $index, $total_resurvey / $total_total);

            $alpha = 'A';
            $index++;

            $sheet->setCellValue($alpha.$index, "Total");
            $num = ord($alpha) - ord('A') + 1;
            $sheet->getStyle($alpha.$index.':'.My_Excel_Column::getNameFromNumber($num).$index)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '55E0FF')
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                )
            );
            $sheet->mergeCells($alpha++.$index.':'.$alpha++.$index);
            $sheet->setCellValue($alpha++ . $index, $total_total);
            $sheet->setCellValue($alpha++ . $index, 1);
        } // end sheet

        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    } // end func
}
