<?php
class My_Kpi_Object
{
    const Pg       = 1;
    const Sale     = 2;
    const Leader   = 3;
    const Pb_sale  = 4;
    const District = 5;
    const Province = 6;
    const Store    = 7;
    const Dealer   = 8;
    const Product  = 9;
    const Area     = 10;

    public static $name = array(
        self::Pg       => 'PG',
        self::Sale     => 'Sale',
        self::Leader   => 'Leader',
        self::District => 'District',
        self::Store    => 'Store',
        self::Dealer   => 'Dealer',
        self::Product  => 'Product',
        self::Pb_sale  => 'PB SALE'
    );
}
