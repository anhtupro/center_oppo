<?php

/**
 * 
 */
class My_Date {

    public static function normal_to_mysql($input) {
        $arr = explode('/', $input);

        if (!is_array($arr))
            return null;

        elseif (count($arr) == 2)
            return $arr[1] . '-' . $arr[0] . '-01';

        elseif (count($arr) == 3)
            return $arr[2] . '-' . $arr[1] . '-' . $arr[0];
        else
            return null;
    }

    public static function isMysqlDateFormat($date) {
        if (!$date)
            return false;
        $result = preg_match('/^([0-9]{4}-[0-9]{2}-[0-9]{2})$/', trim($date));
        if ($result) {
            return true;
        }
        return false;
    }

    public static function date_add($date, $add_num, $type = 'day') {
        $date = new DateTime($date);
        $add  = 'P' . $add_num;
        if ($type == 'day') {
            $add .= 'D';
        }

        if ($type == 'month') {
            $add .= 'M';
        }

        $date->add(new DateInterval($add));
        return $date->format('Y-m-d');
    }

    public static function date_sub($date, $sub_num, $typ = 'day') {
        $date = new DateTime($date);
        $sub  = 'P' . $sub_num;
        if ($type == 'day') {
            $sub .= 'D';
        }

        if ($type == 'month') {
            $sub .= 'M';
        }

        $date->sub(new DateInterval($sub));
        return $date->format('Y-m-d');
    }

    public static function date_diff($date_1, $date_2) {
        $dateTime_1 = new DateTime($date_1);
        $dateTime_2 = new DateTime($date_2);
        $interval   = $dateTime_1->diff($dateTime_2);
        return $interval->format('%a');
    }

    public static function get_months($date1, $date2) {
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my    = date('mY', $time2);

        $months = array(date('n', $time1));

        while ($time1 < $time2) {
            $time1    = strtotime(date('Y-m-d', $time1) . ' +1 month');
            if (date('mY', $time1) != $my && ($time1 < $time2))
                $months[] = date('n', $time1);
        }

        $months[] = date('n', $time2);
        return $months;
    }

}
