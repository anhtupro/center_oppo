<?php
    class My_ExportCsv
    {
        public function __construct()
        {

        }

        public static function _exportExel($params = array())
        {
             if(empty($params['query']) || empty($params['head']))
            {
                return;
            }
            
            $db = Zend_Registry::get('db');

            // Xử lý tên file
            $filename = empty($params['filename'])?'export':$params['filename'];

            header("Pragma: public");
            header('Content-Type: application/vnd.ms-excel; charset=utf-8');
            header('Content-Disposition: attachment; filename="MyFile.xls"');
            echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
            echo implode("\t", array_values($params['head'])) ."\n";

            $data = $db->query($params['query']);
            if (!$data) $data = array();

            foreach($data as $key => $item)
            {
                $row  = array();
                foreach ($params['head'] as $head_key => $head_value) {
                    $row[] = $item[$head_key];
                }
                
                echo implode("\t", array_values($row)) ."\n";
            }
            exit;

        }

        public static function _exportQuery($params = array())
        {
            if(empty($params['query']) || empty($params['head']))
            {
                return;
            }

            $db = Zend_Registry::get('db');

            // Xử lý tên file
            $filename = empty($params['filename'])?'export':$params['filename'];

            set_time_limit(0);
            error_reporting(~E_ALL);
            ini_set('display_error', 0);
            ini_set('memory_limit', -1);
            // output headers so that the file is downloaded rather than displayed
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$filename.'.csv');
            // echo "\xEF\xBB\xBF"; // UTF-8 BOM
            echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
            $output = fopen('php://output', 'w');

            $head_value = array();

            foreach($params['head'] as $key => $val)
            {
                $head_value[] = $val;
            }

            fputcsv($output, $head_value);

            $data = $db->query($params['query']);
            if (!$data) $data = array();

            foreach ($data as $item) 
            {
                $row = array();

                foreach($params['head'] as $key_head => $val_head)
                {
                    $row[$key_head] = ($item[$key_head])?$item[$key_head]:'';
                }
                fputcsv($output, $row);
                unset($item);
                unset($row);
            }
            exit;
        }
    }
