<?php
class My_AlphaExcel
{
    protected $_first;
    protected $_second;

    public function My_AlphaExcel()
    {
        $this->_first = '';
        $this->_second = 'A';
    }

    public function Show()
    {
        return $this->_first.$this->_second;
    }

    public function ShowAndUp()
    {
        $current = $this->Show();
        $this->Up();
        return $current;
    }

    public function Up()
    {
        if($this->_first == '')
        {
            if($this->_second == 'Z')
            {
                $this->_second = 'A';
                $this->_first = 'A';
            }
            else
            {
                $this->_second++;
            }
        }
        else
        {
            if($this->_second == 'Z')
            {
                $this->_second++;
                $this->_first = 'A';
            }
            else
            {
                $this->_second++;
            }
        }
    }
}