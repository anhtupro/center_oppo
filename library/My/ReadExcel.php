<?php

    class My_ReadExcel
    {
        protected $_fileName;

        public function __construct($fileName)
        {
            $this->_fileName = $fileName;
        }

        public function setFileName($fileName)
        {
            $this->_fileName = $fileName;
        }
        
        public function ConvertToArray($header = array())
        {
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();

            $inputFileType = PHPExcel_IOFactory::identify($this->_fileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            
            $objPHPExcel = $objReader->load($this->_fileName);

            $total_sheets=$objPHPExcel->getSheetCount();
 
            $allSheetName=$objPHPExcel->getSheetNames();
            $objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow    = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $arraydata = array();

            for ($row = 1; $row <= $highestRow;++$row)
            {
                for ($col = 0; $col <$highestColumnIndex;++$col)
                {
                    $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                    $arraydata[$row-1][!empty($header[$col+1])?$header[$col+1]:($col)]=$value;
                }
            }

            return $arraydata;
        }
    }