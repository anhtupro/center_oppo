<?php
class My_Controller_Plugin_Description {
    /**
     * encription and description 
     *
     * @param string $action:  'encrypt' or 'decrypt'
     * @param string $string:   content
     *
     * @return string
     */

    public static function encrypt_decrypt($action, $string , $secret_key) {
        $output = false;

        $encrypt_method = "AES-256-CBC";

        if(empty($secret_key))
              $secret_key = SECRET_KEY;

        $secret_iv = 'gaykhongphailacaitoi';
        // hash
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( isset($action) and  $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
            $output = md5($output);
        }
        else if( isset($action) and $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
}