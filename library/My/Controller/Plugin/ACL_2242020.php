<?php

class My_Controller_Plugin_ACL extends Zend_Controller_Plugin_Abstract {

    protected $_defaultRole = 'guest';

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

        if (My_Device_UserAgent::isCocCoc()) {
            $auth = Zend_Auth::getInstance();
            $auth->clearIdentity();
            session_destroy();

            if (My_Request::isXmlHttpRequest())
                exit;

            if ($request->getControllerName() != "user" || $request->getActionName() != "login")
                $r->gotoUrl(HOST . 'user/login')->redirectAndExit();
        }

        $auth = Zend_Auth::getInstance();
        $acl  = new Zend_Acl();

        $default = array(
            'default::test::rolling-game',
            'default::test::phone',
            'default::test::server',
            'default::test::reset',
            'default::test::get-result',
            'default::test::reset-add',
            'default::wss::index',
            'default::wsc::index',
            'default::wsc::set-imei-status',
            'default::ws::soap',
            'default::ws::add',
            'default::ws::wsdl',
            'default::error::error',
            'default::user::login',
            'default::user::login-trade',
            'default::user::login-erp',
            'default::user::logout-erp',
            'default::user::logout',
            'default::user::logout-trade',
            'default::user::auth-trade',
            'default::user::auth-erp',
            'default::user::auth',
            'default::user::reset',
            'default::user::new-pass',
            'default::user::noauth',
            'default::test::upload-retailer',
            'default::test::generate-code',
            'default::test::print-list',
            'default::user::lock',
            'default::test::upload-photo',
            'default::task-view::index',
            'default::user::login-training',
            'default::user::logout-training',
            'default::user::auth-training',
            'default::survey::random',
            'default::survey::save-random',
            'default::survey::do',
            'default::survey::save',
            'default::survey::dev',
            'default::user-information::index',
            'default::user-information::uploadphoto',
            'default::ajax::load-noti',
            'default::ajax::count-noti',
            'default::index::event-time',
            'default::bi::event-time',
            'default::manage::notification-mass-upload-save-new',
            'default::ajax::change-lang-default',
            'default::manage::inform',
            'default::manage::inform-view',
            'default::manage::list-guide',
            'default::manage::list-policy',
            'default::manage::list-organization-structure',
            'default::manage::list-brand',
            'default::manage::list-info',
            'default::manage::list-core-value',
            'default::leave::create',
            'default::leave::list-my-leave',
            'default::leave::list-leave-detail',
            'default::leave::list-leave-admin',
            'default::leave::ajax-load-child-leave-type',
            'default::leave::ajax-load-image',
            'default::leave::edit-image',
            'default::leave::detail-leave',
            'default::leave::ajax-load-detail-leave',
            'default::leave::view-staff-leave',
            'default::staff-time::staff-view',
            'default::staff-time::staff-view-temp',
            'default::staff-time::list-staff-check-in',
            'default::staff-time::get-check-in-detail',
            'default::staff-time::ajax-get-temp-time',
            'default::staff-time::get-view-detail',
            'default::staff-time::view-staff-time',
            'default::staff-time::upload-img',
            'default::staff-time::get-view-detail-approve-last',
            'default::staff-time::get-view-detail-last',
            'default::staff-time::add-time-late',
            'default::staff-time::add-time-late-staff',
            'default::staff-time::export-over-time',
            'default::time-machine::luoi',
            'default::time-machine::department',
            'default::time-machine::by-pass-late',
            'default::time-machine::cancel-day-late',
			'default::time-machine::send-email-manager-deparment',
			'default::staff-time::get-reason-time-late-by-type',
			'default::staff-time::get-reason-time-tmp-time',
			'default::staff-time::get-reason-time-late-by-id',
			'default::staff-time::get-reason-tmp-time',
			'default::staff-time::add-online-time',
			'default::staff-time::delete-online-time',
			'default::staff-time::show-online-time',
            'default::test::test-mail',
            'default::test::update-location',
            // 'default::pay-slip::view-pay',
            // 'default::time::pg-time-create',
            // 'default::time::pg-time-save',
            // 'default::time::time-upload',
            // 'default::time::time-upload',
            // 'default::time::pg-check-out',
            'default::staff-time::test-area',
            'default::staff-time::remove-staff-time',
            'default::staff-time::add-staff-time',
            'default::staff-time::add-staff-leave',
            'default::staff-time::list-staff-approve',
            'default::leave::approve-leave-detail',
            'default::staff-time::get-view-detail-approve',
			'default::staff-time::test-curl',
            'default::utilities-tool::convert-excel',
            'default::event::team-building',
            'default::event::oppoer',
            'default::event::oppoer-play',
            'default::event::oppoer-gift',
            'default::event::oppoer-result',
            'default::event::lunar-new-year2018',
			'default::collection::lunar-new-year2020',
            'default::event::culture-day',
             'default::event::company-trip',
            'default::ajax::get-district-by-province-code',
            'default::office::load-office',
            'default::staff::update-device',
            'default::staff::update-shift',
            'default::staff::update-locked-photo',
            'default::download::inform',
            'default::react-native::bod',
            'default::react-native::list-order',
            'default::react-native::trade-local',
            'default::react-native::asm',
            'default::react-native::asm-details',
            'default::react-native::asm-province',
            'default::react-native::asm-district',
            'default::react-native::list-bod',
            'default::react-native::order',
            'default::react-native::order-details',
            'default::ajax::get-maxim',
            'default::ajax::get-maxim-mobi',
            'default::user::auth-trade-cordova',
            'default::trade::permission-app',
            'default::trade::permission-app-oppo',
			'default::trade::check-link-notification',
            'default::payment::check-link-notification',
            'default::tracking-location::checkin',
            'default::ajax::pass-by-noti',
            'default::ajax::load-team',
            'default::insure::my-insurance',
            //TUONG
            'default::iamoppers::index',
            'default::iamoppers::play',
            'default::iamoppers::result',
            'default::iamoppers::reward',
            'default::iamoppers::get-question',
            'default::iamoppers::submit-question',
            'default::minigame::index',
            'default::minigame::play',
            'default::minigame::result',
            'default::minigame::reward',
            'default::minigame::get-question',
            'default::minigame::submit-question',
            'default::minigame::list-reward',
            'default::minigame::list-answer',
            'default::minigame::rotation',
            'default::minigame::rotation-reward',
            'default::event::lunar-new-year2019',
            'default::ajax::get-ward-delivery',
            //J&T
            'default::form-print::list-jt',
            'default::form-print::edit-jt',
            'default::form-print::copy-jt',
            'default::form-print::savecode-jt',
            'default::form-print::import-jt',
            'default::form-print::delete-jt',
            'default::form-print::multiple-jt',
            'default::form-print::start-jt',
            'default::form-print::save-jt',
            'default::form-print::create-jt',
            'default::form-print::preview-jt',
            //Survey
            'default::survey::form-builder-submit',
            'default::survey::form-builder-end',
            'default::survey::form-submit-save',
            'default::trade::air-list',
            'default::trade::air-create',
            'default::trade::air-edit',
            'default::trade::save-air-edit',
            'default::trade::get-info-shop',
            //ChatBot
            'default::api::get-latest-promo',
            'default::api::get-product-info',
            'default::api::get-price',
            'default::api::get-nearest-service-center',
            'default::api::check-genuine',
            'default::api::check-status-device',
            'default::api::get-service-center',
			'default::api::crm-check-warranty-status',
			'default::api::crm-check-genuine',
			'default::api::crm-get-all-mobile',
			'default::api::crm-get-component-by-phoneid',
			'default::api::crm-get-service-center',
			'default::api::crm-get-service-center-by-keyword',
			'default::api::crm-get-category',
			'default::api::crm-get-question-by-category',
			'default::api::get-purchase',
			'default::api::check-imei-oppo',
            // Appraisal Office Test
//            'default::appraisal-office::set-prd',
//            'default::appraisal-office::get-plan',
//            'default::appraisal-office::save-plan',
//            'default::appraisal-office::get-prd',
//            'default::appraisal-office::save-task',
//            'default::appraisal-office::delete-task',
//            'default::appraisal-office::save-cross-review',
//            'default::appraisal-office::delete-cross-review',
//            'default::appraisal-office::save-field',
//            'default::appraisal-office::delete-field',
//            'default::appraisal-office::staff-approved-prd',
//            'default::appraisal-office::cross-review-approved-prd',
//            'default::appraisal-office::set-prd-multiple',
//            'default::appraisal-office::get-prd-multiple',
//            'default::appraisal-office::get-plan-detail',
//            'default::appraisal-office::view-staff',
//            'default::appraisal-office::view-manager',
//            'default::appraisal-office::skill',
//            'default::appraisal-office::save-skill',
//            'default::appraisal-office::delete-skill',
//            'default::appraisal-office::get-level',
//            'default::appraisal-office::save-level',
//            'default::appraisal-office::delete-level',
//            'default::appraisal-office::create-survey',
//            'default::appraisal-office::get-quarter',
//            'default::appraisal-office::get-capacity',
//            'default::appraisal-office::get-quarter-manager',
//            'default::appraisal-office::get-capacity-manager',
            'default::appraisal-office::save-field',
            'default::appraisal-office::save-task',
            'default::appraisal-office::delete-task',
            'default::appraisal-office::delete-field',
            'default::dynamic-survey::list',
            'default::dynamic-survey::preview',
            'default::dynamic-survey::submit',
            'default::dynamic-survey::save-submit',
            'default::dynamic-survey::create',
            'default::dynamic-survey::save',
            'default::dynamic-survey::preview-live',
            'default::ajax::load-team-title',
            'default::ajax::search-staff',
            'default::user-information::save-change-infor',
            'default::manage::list-type-inform',
            'default::appraisal-sale::test',
			'default::insure::create-staff-contract-file',
			'default::insure::save-staff-contract-file',
			'default::insure::delete-staff-contract-file',
            'default::payment::createabc',
            'default::payment::index',
            'default::payment::login-trade',
            'default::payment::login-center',
            'default::payment::dashboard-system',
			'default::capacity::lock-capacity',		

            'default::collection::status-gift',

            'default::trade::get-standard-image',
            
//            'default::request-office::get-info-payment',
//            'default::request-office::get-list-budget',
//            'default::request-office::get-purchasing-request',

            'default::trade::check-posm-get-statistic-category-sale',
            'default::trade::check-posm-get-statistic-store-sale',
            'default::trade::get-detail-checkshop',
            
            'default::survey::image-survey',


        );

        foreach ($default as $access) {
//            var_dump($access);exit;
            $acl->add(new Zend_Acl_Resource($access));
        }

        if ($auth->hasIdentity()) {


            $user = $auth->getIdentity();

            if (!( @$user->id == SUPERADMIN_ID || @$user->group_id == ADMINISTRATOR_ID )) {
                
                
                
                $user_role     = @$user->role;
                $user_accesses = @$user->accesses;
                if (!$user_role)
                    $user_role     = $this->_defaultRole;

                if (!$user_accesses)
                    $user_accesses = $default;

                $acl->addRole(new Zend_Acl_Role($user_role));
                foreach ($default as $access)
                    $acl->allow($user_role, $access);
                
                
                if ($user_accesses) {
                    foreach ($user_accesses as $access) {
                        if (!$acl->has($access))
                            $acl->add(new Zend_Acl_Resource($access));
                        $acl->allow($user_role, $access);
                    }


                    if (!$acl->has($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()))
                        $acl->add(new Zend_Acl_Resource($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()));

                    if (!$acl->isAllowed($user_role, $request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName())) {
                        if (My_Request::isXmlHttpRequest())
                            exit(json_encode(array('error' => 'No permission.')));

                        $r->gotoUrl('/user/noauth')->redirectAndExit();
                    }
                } else {
                    if (My_Request::isXmlHttpRequest())
                        exit(json_encode(array('error' => 'No permission.')));

                    $r->gotoUrl('/user/noauth')->redirectAndExit();
                }
            }

            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
            if (null === $viewRenderer->view) {
                $viewRenderer->initView();
            }
            $view       = $viewRenderer->view;
            $view->name = @$user->lastname;

            if (@$user->group_id != TRAINING_STAFF_GROUP_ID) {
                // kiểm tra nếu bị bắt đổi pass mà còn đi lung tung thì hốt nó về trang đổi pass
                // - trừ trainning(nhân viên chưa có tài khoản center)
                // if ( My_Staff_Password::force(
                //         $request->getControllerName(),
                //         $request->getActionName()) )
                //     $r->gotoUrl(HOST.'user/change-pass')->redirectAndExit();
                //kiểm tra bắt làm survey
                if (!in_array($request->getControllerName(), array('survey', 'error', 'training', 'trade', 'ajax')) && ($survey_id = My_Survey::run()) && My_Survey::force(
                                $request->getControllerName(), $request->getActionName()
                        )) {
                    if (in_array($survey_id, array(28, 29))) {//dùng để khảo sát. thì làm hết
                        $r->gotoUrl(HOST . 'survey/do?survey_id=' . $survey_id)->redirectAndExit();
                    } else {
                        $r->gotoUrl(HOST . 'survey/random?survey_id=' . $survey_id)->redirectAndExit();
                    }
                }
                //Begin check approve

                if (in_array(@$user->title, array(183))) {
                    if (!in_array($request->getControllerName(), array('staff-time', 'survey', 'error', 'training', 'user-information', 'user')) && My_Approve::run()
                    ) {

                        $r->gotoUrl(HOST . 'staff-time/list-staff-approve')->redirectAndExit();
                    }
                }
                //End


                if (!in_array($request->getControllerName(), array('survey', 'error', 'training', 'user-information', 'user', 'staff-time')) && My_Builder::run()) {
                    $survey_id = My_Builder::run();
                    $r->gotoUrl(HOST . 'survey/form-builder-submit?survey_id=' . $survey_id[0])->redirectAndExit();
                }
                $QStaff = new Application_Model_Staff();
                if (date('H:i:s') >= '08:00:00' && $QStaff->isPgSumupSales(@$user->id)) {
                    if (!in_array($request->getControllerName(), array('survey', 'error', 'training', 'user-information', 'user', 'sumup-sales', 'staff-time', 'leave')) && My_Approve::needToDoDailyReport()) {
                        $r->gotoUrl(HOST . "sumup-sales/create?force=true")->redirectAndExit();
                    }
                }
            }
        } else {



            $acl->addRole(new Zend_Acl_Role($this->_defaultRole));

            if (!$acl->has($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()))
                $acl->add(new Zend_Acl_Resource($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()));

            foreach ($default as $access) {

                $acl->allow($this->_defaultRole, $access);
            }

            if (!$acl->isAllowed($this->_defaultRole, $request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName())) {

                $redirect_url = '/user/login';

                if ($request->getControllerName() != 'index')
                    $redirect_url .= '?b=' . urlencode($request->getRequestUri());

                if (My_Request::isXmlHttpRequest())
                    exit(json_encode(array('error' => 'No permission.')));

                $r->gotoUrl($redirect_url)->redirectAndExit();
            }
        }
    }

}
