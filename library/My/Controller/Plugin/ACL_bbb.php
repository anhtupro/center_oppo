<?php

class My_Controller_Plugin_ACL extends Zend_Controller_Plugin_Abstract {

    protected $_defaultRole = 'guest';

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

        if (My_Device_UserAgent::isCocCoc()) {
            $auth = Zend_Auth::getInstance();
            $auth->clearIdentity();
            session_destroy();

            if (My_Request::isXmlHttpRequest())
                exit;

            if ($request->getControllerName() != "user" || $request->getActionName() != "login")
                $r->gotoUrl(HOST . 'user/login')->redirectAndExit();
        }

        $auth = Zend_Auth::getInstance();
        $acl = new Zend_Acl();

        $default = array(
            'default::test::rolling-game',
            'default::test::phone',
            'default::test::server',
            'default::test::reset',
            'default::test::get-result',
            'default::test::reset-add',
            'default::wss::index',
            'default::wsc::index',
            'default::wsc::set-imei-status',
            'default::ws::soap',
            'default::ws::add',
            'default::ws::wsdl',
            'default::error::error',
            'default::user::login',
            'default::user::logout',
            'default::user::auth',
            'default::user::reset',
            'default::user::new-pass',
            'default::user::noauth',
            'default::test::upload-retailer',
            'default::test::generate-code',
            'default::test::print-list',
            'default::user::lock',
            'default::test::upload-photo',
            'default::task-view::index',
            'default::user::login-training',
            'default::user::logout-training',
            'default::user::auth-training',
            'default::survey::random',
            'default::survey::save-random',
            'default::survey::do',
            'default::survey::save',
            'default::user-information::index',
            'default::user-information::uploadphoto',
            'default::ajax::load-noti',
            'default::ajax::count-noti',
            'default::index::event-time',
            'default::bi::event-time',
            'default::manage::notification-mass-upload-save-new',
            'default::ajax::change-lang-default',
            'default::manage::inform',
            'default::manage::inform-view',
            'default::manage::list-guide',
            'default::manage::list-policy',
            'default::manage::list-organization-structure',
            'default::manage::list-brand',
            'default::manage::list-info',
            'default::manage::list-core-value',
            'default::leave::create',
            'default::leave::list-my-leave',
            'default::leave::list-leave-detail',
            'default::leave::list-leave-admin',
            'default::leave::ajax-load-child-leave-type',
            'default::leave::ajax-load-image',
            'default::leave::edit-image',
            'default::leave::detail-leave',
            'default::leave::ajax-load-detail-leave',
            'default::staff-time::staff-view',
            'default::staff-time::staff-view-temp',
            'default::staff-time::list-staff-check-in',
            'default::staff-time::get-check-in-detail',
            'default::staff-time::ajax-get-temp-time',
            'default::staff-time::get-view-detail',
            'default::staff-time::view-staff-time',
            'default::time-machine::luoi',
            'default::time-machine::department',
            'default::test::test-mail',
				// 'default::pay-slip::view-pay',
                // 'default::time::pg-time-create',
                // 'default::time::pg-time-save',
                // 'default::time::time-upload',
                // 'default::time::time-upload',
                // 'default::time::pg-check-out',

            'default::staff-time::list-staff-approve',
            'default::leave::approve-leave-detail',
            'default::staff-time::get-view-detail-approve',
            'default::utilities-tool::convert-excel'
        );

        foreach ($default as $access) {
//            var_dump($access);exit;
            $acl->add(new Zend_Acl_Resource($access));
        }

        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();

            if (!( @$user->id == SUPERADMIN_ID || @$user->group_id == ADMINISTRATOR_ID )) {
                $user_role = @$user->role;
                $user_accesses = @$user->accesses;
                if (!$user_role)
                    $user_role = $this->_defaultRole;

                if (!$user_accesses)
                    $user_accesses = $default;

                $acl->addRole(new Zend_Acl_Role($user_role));
                foreach ($default as $access)
                    $acl->allow($user_role, $access);

                if ($user_accesses) {
                    foreach ($user_accesses as $access) {
                        if (!$acl->has($access))
                            $acl->add(new Zend_Acl_Resource($access));
                        $acl->allow($user_role, $access);
                    }


                    if (!$acl->has($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()))
                        $acl->add(new Zend_Acl_Resource($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()));

                    if (!$acl->isAllowed($user_role, $request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName())) {
                        if (My_Request::isXmlHttpRequest())
                            exit(json_encode(array('error' => 'No permission.')));

                        $r->gotoUrl('/user/noauth')->redirectAndExit();
                    }
                } else {
                    if (My_Request::isXmlHttpRequest())
                        exit(json_encode(array('error' => 'No permission.')));

                    $r->gotoUrl('/user/noauth')->redirectAndExit();
                }
            }

            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
            if (null === $viewRenderer->view) {
                $viewRenderer->initView();
            }
            $view = $viewRenderer->view;
            $view->name = @$user->lastname;

            if (@$user->group_id != TRAINING_STAFF_GROUP_ID) {
                // kiểm tra nếu bị bắt đổi pass mà còn đi lung tung thì hốt nó về trang đổi pass
                // - trừ trainning(nhân viên chưa có tài khoản center)
                // if ( My_Staff_Password::force(
                //         $request->getControllerName(),
                //         $request->getActionName()) )
                //     $r->gotoUrl(HOST.'user/change-pass')->redirectAndExit();
                //kiểm tra bắt làm survey
                if ( !in_array($request->getControllerName(),array('survey','error','training'))
                     && ($survey_id = My_Survey::run())
                     && My_Survey::force(
                         $request->getControllerName(),
                         $request->getActionName()
                     ) ){
                     if($survey_id == 12){
                         $r->gotoUrl(HOST.'survey/random?survey_id='.$survey_id)->redirectAndExit();
                     }else{
                         $r->gotoUrl(HOST.'survey/do?survey_id='.$survey_id)->redirectAndExit();
                     }
                 }
            }
        } else {

            $acl->addRole(new Zend_Acl_Role($this->_defaultRole));

            if (!$acl->has($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()))
                $acl->add(new Zend_Acl_Resource($request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName()));

            foreach ($default as $access) {

                $acl->allow($this->_defaultRole, $access);
            }

            if (!$acl->isAllowed($this->_defaultRole, $request->getModuleName() . '::' . $request->getControllerName() . '::' . $request->getActionName())) {

                $redirect_url = '/user/login';

                if ($request->getControllerName() != 'index')
                    $redirect_url .= '?b=' . urlencode($request->getRequestUri());

                if (My_Request::isXmlHttpRequest())
                    exit(json_encode(array('error' => 'No permission.')));

                $r->gotoUrl($redirect_url)->redirectAndExit();
            }
        }
    }

}
