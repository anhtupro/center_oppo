<?php

class My_Controller_Plugin_Login extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
    // echo 'Hệ thống đang nâng cấp. Vui lòng quay lại vào ngày mai'; die;
        if(($request->getControllerName() != "user" && $request->getActionName() != "login-trade") && ($request->getControllerName() != "user" && $request->getActionName() != "login-erp")) {
            $r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

            if (My_Device_UserAgent::isCocCoc()) {
                $auth = Zend_Auth::getInstance();
                $auth->clearIdentity();
                session_destroy();
    
                if (My_Request::isXmlHttpRequest())
                    exit;
    
                if ($request->getControllerName() != "user" || $request->getActionName() != "login")
                    $r->gotoUrl(HOST . 'user/login')->redirectAndExit();
            }
    
            $auth = Zend_Auth::getInstance();
    
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
    
                //Check khong co token thi clear Session -> logout
                if(!$_COOKIE[auth_token]){
                    $auth->clearIdentity();
                    session_destroy();
                    $c = $request->getParam('c');
                    $g = $request->getParam('g');
                    $j = $request->getParam('j');
                    if($c)
                        $r->gotoUrl('/user/login?c='.$c)->redirectAndExit();
                    else if($g)
                        $r->gotoUrl('/user/login?g='.$g)->redirectAndExit();
                    else if($j)
                        $r->gotoUrl('/user/login?j='.$j)->redirectAndExit();
                    else{
                        $redirect_url = '/user/login';
    
                        if ($request->getControllerName() != 'index')
                            $redirect_url .= '?b=' . urlencode($request->getRequestUri());
                            
                        $r->gotoUrl($redirect_url)->redirectAndExit();
                    }
                } else{
                    //Check user token vs user Session
                    $jwt = $_COOKIE[auth_token];
                    $tokenParts = explode('.', $jwt);
                    $payload = json_decode(base64_decode($tokenParts[1]));
                    $staff_id = base64_decode($payload->s);
    
                    if($staff_id !== $user->id) {
                        $auth->clearIdentity();
                        session_destroy();
                        $redirect_url = '';
                        if ($request->getControllerName() != 'index')
                            $redirect_url .= '?b=' . urlencode($request->getRequestUri());
                            
                        $r->gotoUrl($redirect_url)->redirectAndExit();
                    } else{
                        //Reload token
                        setcookie(auth_token, $_COOKIE[auth_token], time() + (1440), "/", ".opposhop.vn");
                        setcookie('p', $_COOKIE['p'], time() + (1440), "/", ".opposhop.vn");
                    }
                }
    
            } else {
    
                if($_COOKIE[auth_token]) {
                    $jwt = $_COOKIE[auth_token];
                    //Get token and check
                    $secretKey = md5('0p0pVj3tNam.?;Taem2030');
                    // split the token
                    $tokenParts = explode('.', $jwt);
                    $header = base64_decode($tokenParts[0]);
                    $payload = json_decode(base64_decode($tokenParts[1]));
                    $signatureProvided = $tokenParts[2];
                    //Check signature
                    $signature2 = hash_hmac('sha256', $tokenParts[0] . "." . $tokenParts[1]  . "." . $_COOKIE['PHPSESSID'], $secretKey, true);
                    $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature2));
            
                    if($signatureProvided !== $base64UrlSignature) {
                        //Wrong token
                        unset($_COOKIE[auth_token]);
                        setcookie(auth_token, null, -1, '/', '.opposhop.vn'); 
                    }
            
                    $staff_id = base64_decode($payload->s);
    
                    $db    = Zend_Registry::get('db');
    
                    $select = $db->select()
                            ->from(array('p' => 'staff'), array('p.*'));
                    $select->where('p.id = ?', $staff_id);
                    $resultStaff = $db->fetchRow($select);
    
                    if(!$resultStaff) {
                        echo 'Staff cannot go to Center!';die;
                        throw new Exception("Staff cannot go to Center!");
                    }
    
                    $request = $this->getRequest();
                    $auth    = Zend_Auth::getInstance();
    
                    try {
                        $uname = $resultStaff['email'];
                        $md5_pass = $resultStaff['password'];
                        $flag = 0;
    
                        $db    = Zend_Registry::get('db');
    
                        $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                        $authAdapter->setTableName('staff')
                                ->setIdentityColumn('email')
                                ->setCredentialColumn('password');
    
                        // Set the input credential values
    
    
                        $authAdapter->setIdentity($uname);
    
                        $authAdapter->setCredential($md5_pass);
    
                        // Perform the authentication query, saving the result
                        $result = $auth->authenticate($authAdapter);
    
                        if (!$result->isValid())
                            throw new Exception("Email or password is invalid!");
    
                        $data = $authAdapter->getResultRowObject(null, 'password');
                        //check This account of Realme support
                        if($data->company_id == 5){
                            $auth = Zend_Auth::getInstance();
                            $auth->clearIdentity();
                            throw new Exception("This account cannot login center!");
                        }
                        /*
                        if ( $data->off_date )
                        {
                        throw new Exception("This account was disabled!");
                        }
                        */
                        if ($data->status == 0) {
                            $auth = Zend_Auth::getInstance();
                            $auth->clearIdentity();
                            throw new Exception("This account was disabled!");
                        }
    
                        //Mapping access data
                        $QGroupLevel2 = new Application_Model_GroupLevel2();
    
                        $permission_access_staff_id = $QGroupLevel2->getAccessByStaffId($data->id);
    
                        $permission_access_title_id = $QGroupLevel2->getAccessByTitleId($data->title);
    
                        $result_access = array();
                        $result_menu = array();
    
                        foreach ($permission_access_staff_id as $key => $value) {
                            $result_access = array_merge($result_access, json_decode($value['access']));
                            $result_menu = array_merge($result_menu, explode(',', $value['menu']));
                        }
    
                        foreach ($permission_access_title_id as $key => $value) {
                            $result_access = array_merge($result_access, json_decode($value['access']));
                            $result_menu = array_merge($result_menu, explode(',', $value['menu']));
                        }
    
                        $QRegionalMarket          = new Application_Model_RegionalMarket();
                        $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
                        $data->regional_market_id = $area_id;
                        $QTeam                    = new Application_Model_Team();
                        $title                    = $QTeam->find($data->title)->current();
                        $QGroup                   = new Application_Model_Group();
                        
                        
                        if($uname == 'hiep.nguyen@oppo-aed.vn'){
                                $group      = $QGroup->find(8)->current();
                        }
                        else{
                                $group      = $QGroup->find($title->access_group)->current();
                        }
                        
                        
                        $data->group_id=$title->access_group;
                        $data->role = $group->name;
                        
                        $menu           = array_unique($result_menu);
                        
                        $QMenu   = new Application_Model_Menu();
                        $where   = array();
                        $where[] = $QMenu->getAdapter()->quoteInto('group_id = ? and hidden is null', 1);
    
                        if ($menu)
                            $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
                        else
                            $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);
    
                        if($data->id == '5899'){
                            $where = $QMenu->getAdapter()->quoteInto('group_id = ? and hidden is null', 1);
                        }
    
                        $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));
    
                        $data->menu = $menus;
                        $data->accesses = array_unique($result_access);

                        $d = $request->getParam('d');
                        if($d && $d == 'trade') {
                            $data->login_from_trade = 1;

                            //set last login
                            $QStaff = new Application_Model_Staff();
                            $where  = $QStaff->getAdapter()->quoteInto('id = ?', $data->id);

                            if(!empty($token) and $token != ''){
                                $QStaff->update(array('last_login' => date('Y-m-d H:i:s'), 'dingtalk_id' => $token), $where);
                            }
                            else{
                                $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);
                            }

                            $QLogSystemTrade = new Application_Model_LogSystemTrade();
                            $action_link = $this->getRequest()->getActionName();

                            $log_system_trade = [
                                'staff_id' => $data->id,
                                'action'   => $action_link,
                                'created_at' => date('Y-m-d H:i:s')
                            ];
                            $QLogSystemTrade->insert($log_system_trade);
                        }

                        $f = $request->getParam('f');
                        if($f && $f == 'erp') {
                            $data->login_from_erp = 1;

                            //get personal access
                            $QStaffPriveledge = new Application_Model_StaffPriviledgeErp();

                            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

                            $priviledge = $QStaffPriveledge->fetchRow($where);

                            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

                            $QRegionalMarket          = new Application_Model_RegionalMarket();
                            $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
                            $data->regional_market_id = $area_id;
                            $QTeam                    = new Application_Model_Team();
                            $title                    = $QTeam->find($data->title)->current();
                            $QGroup                   = new Application_Model_GroupTrade();

                            
                            $group      = $QGroup->find($title->access_group)->current();
                            $data->group_id=$title->access_group;
                            $data->role = $group->name;

                            if ($personal_accesses) { 
                                $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                                $menu = explode(',', $personal_accesses['menu']);
                            } else {

                                $data->accesses = json_decode($group->access);

                                $menu = $group->menu ? explode(',', $group->menu) : null;
                            }
                            

                            $QMenu   = new Application_Model_MenuErp();
                            $where   = array();
                            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

                            if ($menu)
                                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
                            else
                                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

                            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

                            $data->menu = $menus;

                            $auth->getStorage()->write($data);


                            //set last login
                            $QStaff = new Application_Model_Staff();
                            $where  = $QStaff->getAdapter()->quoteInto('id = ?', $data->id);
                            
                            if(!empty($token) and $token != ''){
                                $QStaff->update(array('last_login' => date('Y-m-d H:i:s'), 'token_erp_app' => $token), $where);
                            }
                            else{
                                $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);
                            }
                        }
                        
                        //Cấp user cho guest indo
                        if($uname == 'thehien.hoang@oppo-aed.vn'){
                        //$data->email = 'guest.indo@oppomobile.vn';
                        //$data->firstname = 'GUEST';
                        //$data->lastname = 'INDO';
                        //$data->photo = 'INDO.jpg';
    
                        }
                        //Cấp user cho user it.hq
                        if($flag == 2){
                        $data->email = 'jevis@sharedcenter.cn';
                        $data->firstname = 'jevis';
                        $data->lastname = 'jevis';
                        $data->photo = 'INDO.jpg';
                        }
    
                        if($flag == 3){
                        $data->email = 'jameswong@sharedcenter.cn';
                        $data->firstname = 'jameswong';
                        $data->lastname = 'jameswong';
                        $data->photo = 'INDO.jpg';
                        }
                        // Toan
                        $date = new DateTime('now');
                        $active_date =(date("d",$date->modify('last day of this month') ) == date("d") OR date("d")==1)  ? 1:0;
                        if($active_date){
                            if( (date("H:s") > date("H:s",strtotime("8:30")) AND  date("H:s") < date("H:s",strtotime("9:30")))
                                OR 
                                date("H:s") > date("H:s",strtotime("16:00")) AND  date("H:s") < date("H:s",strtotime("17:00"))
                            ){
                                $data->show_notification=1;
                            }
                        }
    
    
                        $auth->getStorage()->write($data);
    
                        $QLog = new Application_Model_Log();
                        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
                        $info = "USER - Login (" . $data->id . ")";
                        //todo log
                        $QLog->insert(array(
                            'info'       => $info,
                            'user_id'    => $data->id,
                            'ip_address' => $ip,
                            'time'       => date('Y-m-d H:i:s'),
                        ));
                        
                        $b = $request->getParam('b');
                        
                        $QNotificationAccess = new Application_Model_NotificationAccess();
                        $QNotificationAccess->addNotifyForSaleAppraisal($data->id, $data->title);
                        $QNotificationAccess->addNotifyForOfficeCapacityAppraisal($data->id, $data->title);
                        $QNotificationAccess->addNotifyForOfficePrdAppraisal($data->id, $data->title);
                        
                        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
                        // Check gift lunar
                        $QLunar=new Application_Model_Lunar2020();
                        $where=null;
                        $where=$QLunar->getAdapter()->quoteInto("code = ? ",$userStorage->code);
                        
                        $res=$QLunar->fetchRow($where);
                        if(!empty($res)){
                            $userStorage->flatAlert=1;
                        };

                        //Redirect
                        $d = $request->getParam('d');
                        $e = $request->getParam('e');
                        $f = $request->getParam('f');

                        if($d && $d == 'trade') {
                            //Redirect to Trade Marketing
                            $data->login_from_trade = 1;
                            $redirect_url = HOST . 'view-menu?id=501';
                        } else if($e && $e == 'training') {
                            //Redirect to Training
                            $redirect_url = HOST . 'training';
                        } else if($f && $f == 'erp') {
                            //Redirect to Training
                            $data->login_from_erp = 1;
                            $redirect_url = HOST . 'payment/createabc';
                        } else{
                            $redirect_url = $b ? $b : HOST;
                        }
    
                    } catch (Exception $e) {
                        $flashMessenger = $this->_helper->flashMessenger;
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    
                        $QLog = new Application_Model_Log();
                        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
                        $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
                        //todo log
                        $QLog->insert(array(
                            'info'       => $info,
                            'user_id'    => 0,
                            'ip_address' => $ip,
                            'time'       => date('Y-m-d H:i:s'),
                        ));
                        
                    }
                    
                    $r->gotoUrl($redirect_url)->redirectAndExit();
                }
            }
        }
    }

}
