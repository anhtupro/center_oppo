<?php
class My_Controller_Action extends Zend_Controller_Action
{

    public function preDispatch() {
        // sanitize data
        // var_dump(in_array($controller, array('user', 'user-information')));die;
        foreach ($this->getRequest()->getParams() as $key => $value)
            $this->getRequest()->setParam($key, $this->sanitize($value));

        // $auth_check_photo = Zend_Auth::getInstance()->getStorage()->read();
        // $controller = $this->getRequest()->getControllerName();
        // $action = $this->getRequest()->getActionName();
        // if(!empty($auth_check_photo->id) && !in_array($controller, array('user', 'user-information')))
        // {
        //     $flashMessenger = $this->_helper->flashMessenger;

        //     $url_file = APPLICATION_PATH . '/../public/photo/staff/' . $auth_check_photo->id;
            
        //     $flag_photo = 0;

        //     if(empty($auth_check_photo->photo) || !file_exists($url_file . '/' . $auth_check_photo->photo))
        //     {
        //         $flashMessenger->setNamespace('error')->addMessage("Xin vui lòng bổ sung ảnh nhân viên");
        //         $flag_photo = 1;
        //     }

        //     if(empty($auth_check_photo->id_photo) || !file_exists($url_file . '/ID_Front/' . $auth_check_photo->id_photo))
        //     {
        //         $flashMessenger->setNamespace('error')->addMessage("Xin vui lòng bổ sung ảnh CMND mặt trước");
        //         $flag_photo = 1;
        //     }

        //     if(empty($auth_check_photo->id_photo_back) || !file_exists($url_file . '/ID_Back/' . $auth_check_photo->id_photo_back))
        //     {
        //         $flashMessenger->setNamespace('error')->addMessage("Xin vui lòng bổ sung ảnh CMND mặt sau");
        //         $flag_photo = 1;
        //     }

        //     if($flag_photo == 1)
        //     {
        //         // $this->_helper->layout->disableLayout();
        //         // $this->_helper->viewRenderer->setNoRender();
        //         $this->_redirect("/user-information");
        //     }
        // }
    }

    public function checkPhoto()
    {

    }

    private function sanitize($value)
    {
        return $this->UnicodeToHop2DungSan($value);
    }

    private function UnicodeToHop2DungSan($str)
    {
        if ( !is_string( $str) )
            return $str;

        $dungsan=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");

        $tohop=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","ỷ","Ỹ",
            "Đ","ê","ù","à");

        return str_replace($tohop,$dungsan,$str);
    }
}