<?php
class My_Controller_Action extends Zend_Controller_Action
{
//    public function __construct(\Zend_Controller_Request_Abstract $request, \Zend_Controller_Response_Abstract $response, array $invokeArgs = array()) {
//          $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//          $list_lang = My_Lang::getLangList();
//          if(!empty($userStorage)){      
//                $user_lang = $userStorage->defaut_language;
//                $current_lang=$list_lang[$user_lang];
//          }else{
//              $current_lang='vi';
//          }
//          $this->view->current_lang=$current_lang;
//    }
    
    public function preDispatch() {
        // sanitize data
        foreach ($this->getRequest()->getParams() as $key => $value)
            $this->getRequest()->setParam($key, $this->sanitize($value));

        $auth_check_photo = Zend_Auth::getInstance()->getStorage()->read();
        // var_dump($this->getRequest()->getControllerName());die;
        $controller = $this->getRequest()->getControllerName();
        //$action = $this->getRequest()->getActionName();
        $auth_check = Zend_Auth::getInstance()->getStorage()->read();
      
         $list_lang = My_Lang::getLangList();
          if(!empty($auth_check_photo) && isset($auth_check_photo->defaut_language)){   
            
                $user_lang = $auth_check_photo->defaut_language;
                $current_lang=$list_lang[$user_lang];
             
          }else{
              $current_lang='vi';
          }
          $this->view->current_lang=$current_lang;
          $this->view->translate_adapter= Zend_Registry::get('translate');
        $array_must_upload_photo = array(162,164,179,181,182,183,190,191,274,293,295,308,312,178,299,186,373,163,291,372,296,316,319,168,169,171,173,268,307,349,174,175,176,279,281,286,317,256,257,258,259,260,261,262,270,218,219,220,234,235,236,237,238,240,241,242,243,244,271,278,282,292,298,300,301,305,306,309,314,341,343,345,347);
        
        if(!empty($auth_check->id) && !in_array($controller, array('user', 'user-information')) && in_array($auth_check->title, $array_must_upload_photo) && $auth_check->group_id != 27)
        {
            $flashMessenger = $this->_helper->flashMessenger;

            $url_file = APPLICATION_PATH . '/../public/photo/staff/' . $auth_check_photo->id;

            $QStaff = new Application_Model_Staff();

            $StaffRowSet = $QStaff->find($auth_check_photo->id);
            $auth_check_photo = $StaffRowSet->current();
            
            $flag_photo = 0;
            $flag_id_photo = 0;
            $flag_id_photo_back = 0;

            $array_error = array();

            if(empty($auth_check_photo->photo) || !file_exists($url_file . '/' . $auth_check_photo->photo))
            {
                $array_error[] = "Xin vui lòng bổ sung ảnh nhân viên";
                
                $flag_photo = 1;
            }

            if(empty($auth_check_photo->id_photo) || !file_exists($url_file . '/ID_Front/' . $auth_check_photo->id_photo))
            {
                $array_error[] = "Xin vui lòng bổ sung ảnh CMND mặt trước";
                $flag_id_photo = 1;
            }

            if(empty($auth_check_photo->id_photo_back) || !file_exists($url_file . '/ID_Back/' . $auth_check_photo->id_photo_back))
            {
                $array_error[] = "Xin vui lòng bổ sung ảnh CMND mặt sau";
                $flag_id_photo_back = 1;
            }

            if($flag_photo == 1 || ($flag_id_photo_back == 1 && $flag_id_photo == 1))
            {
                // foreach($array_error as $value)
                // {
                //     $flashMessenger->setNamespace('error')->addMessage($value);
                // }
                 $flashMessenger->setNamespace('error')->addMessage("Bạn vui lòng upload hình nhân viên và hình CMND vào hệ thống nhé");
                $this->_redirect("/user-information");
            }
        }
		
		$is_layout = $this->_helper->layout->getLayout();
		if($is_layout == 'layout'){
			if($auth_check->group_id == HR_ID or $auth_check->group_id == PGPB_ID or $auth_check->group_id == SALES_ID or $auth_check->group_id == ASM_ID or $auth_check->group_id == LEADER_ID or $auth_check->group_id == ASMSTANDBY_ID or in_array($auth_check->id, array(103,765,240,241,5968,341))){
				$this->_helper->layout->setLayout('layout_new');
			}
			//$this->_helper->layout->setLayout('layout_new');
		}

    }

    private function sanitize($value)
    {
        return $this->UnicodeToHop2DungSan($value);
    }

    private function UnicodeToHop2DungSan($str)
    {
        if ( !is_string( $str) )
            return $str;

        $dungsan=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");

        $tohop=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","ỷ","Ỹ",
            "Đ","ê","ù","à");

        return str_replace($tohop,$dungsan,$str);
    }
    
    public static function insertAllrow($params,$db){
    	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
    	$config = $config->toArray();
    	$db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
    			'host'     => $config['resources']['db']['params']['host'],
    			'username' => $config['resources']['db']['params']['username'],
    			'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
    			'dbname'   => $config['resources']['db']['params']['dbname']
    	));
    
    	$temp = $params[0];
    
    	$arrkey = array_keys($temp);
    
    	$str_insert = '';
    	foreach ($params as $k => $param){
    		$str_insert .= "('".implode("', '", $param)."')" . ',';
    	}
    
    	$str_rows = rtrim($str_insert,',');
    
    	$sql  = "INSERT INTO $db ";
    
    	$sql .= " (`".implode("`, `", array_keys($temp))."`)";
    
    	$sql .= " VALUES $str_rows ";
    
    	$db_log->query($sql);
    }
}