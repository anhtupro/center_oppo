<?php

class My_Controller_Action extends Zend_Controller_Action {

//    public function __construct(\Zend_Controller_Request_Abstract $request, \Zend_Controller_Response_Abstract $response, array $invokeArgs = array()) {
//          $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//          $list_lang = My_Lang::getLangList();
//          if(!empty($userStorage)){      
//                $user_lang = $userStorage->defaut_language;
//                $current_lang=$list_lang[$user_lang];
//          }else{
//              $current_lang='vi';
//          }
//          $this->view->current_lang=$current_lang;
//    }

    public function preDispatch() {
        $auth_check                  = Zend_Auth::getInstance()->getStorage()->read();
        $controller                  = $this->getRequest()->getControllerName();
        $this->view->controller_name = $controller;
        $action                      = $this->getRequest()->getActionName();
        $this->view->action_name     = $action;
        $auth_check_photo            = Zend_Auth::getInstance()->getStorage()->read();

        $actual_link                 = My_Util::escape_string($_SERVER[REQUEST_URI]);

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();		
        
        if($_COOKIE['defaut_lang']) {
            $portal_lang = $_COOKIE['defaut_lang'];

            if($portal_lang !== $userStorage->defaut_language) {
                $userStorage->defaut_language = $portal_lang;
            }
        }
        
        $lang = $userStorage->defaut_language;
        $this->view->lang = $lang;

        if (isset($userStorage->menu) && $userStorage->menu) {
            $menu = $userStorage->menu;
        } else {
            $QMenu = new Application_Model_Menu();
            $where = $QMenu->getAdapter()->quoteInto('group_id = ?', $group_id);
            $menu = $QMenu->fetchAll($where, array('parent_id', 'position'));
        }

        $array_menu_parent = array();
        foreach ($menu as $row) {
            if($row['parent_id'] == 0){
                array_push($array_menu_parent, $row);
            }
        }

        $this->view->total_number_menu = count($array_menu_parent);

        $QMenu2 = new Application_Model_Menu2();
        $menu_permisson = $QMenu2->getMenuPermission();

        $arr_breadcrumb = array();

        $QMenu = new Application_Model_Menu();
        $action_name = Zend_Controller_Front::getInstance()->getRequest()->getActionName() == 'index' ? '' : '/' . Zend_Controller_Front::getInstance()->getRequest()->getActionName();
        $controller_action_name = '/' . Zend_Controller_Front::getInstance()->getRequest()->getControllerName() . $action_name;

        if($controller_action_name == '/view-menu'){
            $title_menu = $QMenu->getTitleByUrl($actual_link, $lang);
        } else {
            $title_menu = $QMenu->getTitleByUrl($controller_action_name, $lang);
            if($title_menu[0]['is_hidden'] == '1') {
                $parent_ca_name = $title_menu[0]['url_father'];
                $title_menu = $QMenu->getTitleByUrl($parent_ca_name, $lang);
            }
        }

        if(mb_strlen($title_menu[0]["title_name"]) > 9) {
            $title_menu_mobile = mb_substr($title_menu[0]["title_name"], 0, 9, 'UTF-8');
            $title_menu_mobile .= ' ...';
        } else {
            $title_menu_mobile = $title_menu[0]["title_name"];
        }
        $this->view->title_menu = $title_menu_mobile;
        if($title_menu) {
            if($_SESSION["breadcrumb"]) {
                unset($_SESSION['breadcrumb']);
            }
            $_SESSION["breadcrumb"] = $title_menu[0];
        }
        $this->view->breadcrumb = $_SESSION["breadcrumb"];
         


        if (!in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171', '171.244.18.76', '171.244.18.86'))) {
//            echo "<pre>";
//            print_r("Hệ thống đang tạm khóa .Vui lòng quay lại sau ít phút nữa");
//            die;
        }

        if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
            if (!in_array($controller, array("ajax", "user", "pay-slip"))) {
                $server_path = $this->getRequest()->getServer('HTTP_HOST');
                $full_path   = $this->getRequest()->getRequestUri();
                $user_id     = 0;
                $user_name   = "NA";
                if (!empty($auth_check)) {
                    $user_id   = $auth_check->id;
                    $user_name = $auth_check->email;
                }
                $log_model  = new Application_Model_LogExcutionAction();
                $data_array = [
                    'controller_name' => $controller,
                    'action_name'     => $action,
                    'full_path'       => $server_path . $full_path,
                    'start_time'      => APP_START_TIME,
                    'created_at'      => date('Y-m-d H:i:s'),
                    'excution_user'   => $user_id,
                    'user_name'       => $user_name,
                    'unique_id'       => UNIQUE_ID
                ];

                $log_model->insert($data_array);
            }
        }
//        if (!in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171','171.244.18.76','171.244.18.86'))) {
//            echo "<pre>";
//            print_r("Hệ thống đang tạm khóa để chạy số tháng 7.Vui lòng quay lại sau ít phút nữa");
//            die;
//        }

        foreach ($this->getRequest()->getParams() as $key => $value)
            $this->getRequest()->setParam($key, $this->sanitize($value));



        $QTeam            = new Application_Model_Team();
        $staff_title_info = $auth_check->title;
        $team_info        = $QTeam->find($staff_title_info);
        $team_info        = $team_info->current();
        $group_id         = $team_info['access_group'];

        $list_lang = My_Lang::getLangList();
        if (!empty($auth_check_photo) && isset($auth_check_photo->defaut_language)) {

            $user_lang    = $auth_check_photo->defaut_language;
            $current_lang = $list_lang[$user_lang];
        } else {
            $current_lang = 'vi';
        }
        $this->view->current_lang      = $current_lang;
        $this->view->translate_adapter = Zend_Registry::get('translate');
        $array_must_upload_photo       = array(162, 164, 179, 181, 182, 183, 190, 191, 274, 293, 295, 308, 312, 178, 299, 186, 373, 163, 291, 372, 296, 316, 319, 168, 169, 171, 173, 268, 307, 349, 174, 175, 176, 279, 281, 286, 317, 256, 257, 258, 259, 260, 261, 262, 270, 218, 219, 220, 234, 235, 236, 237, 238, 240, 241, 242, 243, 244, 271, 278, 282, 292, 298, 300, 301, 305, 306, 309, 314, 341, 343, 345, 347);

        if (!empty($auth_check->id) && !in_array($controller, array('user', 'user-information', 'survey', 'error', 'training', 'trade')) && in_array($auth_check->title, $array_must_upload_photo) && $auth_check->group_id != 27) {
            $flashMessenger = $this->_helper->flashMessenger;

            $url_file = APPLICATION_PATH . '/../public/photo/staff/' . $auth_check_photo->id;



            $QStaff = new Application_Model_Staff();

            $StaffRowSet      = $QStaff->find($auth_check_photo->id);
            $auth_check_photo = $StaffRowSet->current();


            $flag_photo         = 0;
            $flag_id_photo      = 0;
            $flag_id_photo_back = 0;

            $array_error = array();

            if (empty($auth_check_photo->photo) || !file_exists($url_file . '/' . $auth_check_photo->photo)) {
                $array_error[] = "Xin vui lòng bổ sung ảnh nhân viên";
                $flag_photo    = 1;
            }

            if (empty($auth_check_photo->id_photo) || !file_exists($url_file . '/ID_Front/' . $auth_check_photo->id_photo)) {
                $array_error[] = "Xin vui lòng bổ sung ảnh CMND mặt trước";
//                echo 'bbbbb';
                $flag_id_photo = 1;
            }

            if (empty($auth_check_photo->id_photo_back) || !file_exists($url_file . '/ID_Back/' . $auth_check_photo->id_photo_back)) {
                $array_error[] = "Xin vui lòng bổ sung ảnh CMND mặt sau";

                $flag_id_photo_back = 1;
            }

            //  if ($flag_photo == 1 || ($flag_id_photo_back == 1 && $flag_id_photo == 1)) {
            //  $flashMessenger->setNamespace('error')->addMessage("Bạn vui lòng upload hình nhân viên/ hình CMND còn thiếu vào hệ thống nhé");
            //   $this->_redirect("/user-information");
            // }
        }

        //Set cookie 30 days for layout mode - Anh Tam
        $change_color = $this->getRequest()->getParam('btn-change-mode');
        $cookie_name = "layout_mode";
        if($change_color == "1") {
            if($_COOKIE['layout_mode']){
                unset($_COOKIE['layout_mode']);
                setcookie($cookie_name, null, -1, '/', '.opposhop.vn'); 
            }
            $cookie_value = "1";
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/", ".opposhop.vn"); // 86400 = 1 day
            if($userStorage->login_from_trade)
                $this->redirect('view-menu?id=501');
            $this->redirect(HOST);
        } else if($change_color == "2") {
            if($_COOKIE['layout_mode']){
                unset($_COOKIE['layout_mode']);
                setcookie($cookie_name, null, -1, '/', '.opposhop.vn'); 
            }
            $cookie_value = "2";
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/", ".opposhop.vn"); // 86400 = 1 day
            if($userStorage->login_from_trade)
                $this->redirect('view-menu?id=501');
            $this->redirect(HOST);
        }

        if(isset($_COOKIE['layout_mode']) && $_COOKIE['layout_mode'] == "1"){
            $_SESSION["light_template"] = 1;
        } else if(isset($_COOKIE['layout_mode']) && $_COOKIE['layout_mode'] == "2"){
            unset($_SESSION['light_template']);
        }

        $is_layout = $this->_helper->layout->getLayout();
        if ($is_layout == 'layout') {

            if ($auth_check->team == SALES_TEAM
                    or $auth_check->group_id == HR_ID
                    or $auth_check->group_id == PGPB_ID
                    or $auth_check->group_id == SALES_ID
                    or $auth_check->group_id == ASM_ID
                    or $auth_check->group_id == LEADER_ID
                    or $auth_check->group_id == ASMSTANDBY_ID
                    or in_array($auth_check->id, array(103, 765, 240, 241, 5968, 341, 24))
                    or $auth_check->group_id == TRAINING_TEAM_ID
                    or $auth_check->group_id == TRAINING_LEADER_ID
                    or $auth_check->group_id == BOARD_ID
                    or $auth_check->group_id == CALL_CENTER_ID
                    or $auth_check->group_id == 31
                    or $auth_check->group_id == 11
                    or $auth_check->id == 5899
                    or $auth_check->id == 23776

                    or in_array($group_id, array(6, 21, 22, 20))
            ) {

                if ($auth_check->login_from_erp == 1) {
                    $this->_helper->layout->setLayout('layout_payment');
                } else {
                    $this->_helper->layout->setLayout('layout_metronic2020');
                }


            } else{
                $this->_helper->layout->setLayout('layout_metronic2020');
            }
        }
    }

    private function sanitize($value) {
        return $this->UnicodeToHop2DungSan($value);
    }

    private function UnicodeToHop2DungSan($str) {
        if (!is_string($str))
            return $str;

        $dungsan = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ"
            , "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ",
            "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ"
            , "ờ", "ớ", "ợ", "ở", "ỡ",
            "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
            "ỳ", "ý", "ỵ", "ỷ", "ỹ",
            "đ",
            "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă"
            , "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
            "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
            "Ì", "Í", "Ị", "Ỉ", "Ĩ",
            "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ"
            , "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
            "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
            "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
            "Đ", "ê", "ù", "à");

        $tohop = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ"
            , "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ",
            "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ"
            , "ờ", "ớ", "ợ", "ở", "ỡ",
            "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
            "ỳ", "ý", "ỵ", "ỷ", "ỹ",
            "đ",
            "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă"
            , "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
            "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
            "Ì", "Í", "Ị", "Ỉ", "Ĩ",
            "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ"
            , "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
            "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
            "Ỳ", "Ý", "Ỵ", "ỷ", "Ỹ",
            "Đ", "ê", "ù", "à");

        return str_replace($tohop, $dungsan, $str);
    }

    public static function insertAllrow($params, $db) {

        try {
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
            $config = $config->toArray();
            $db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
                'host'     => $config['resources']['db']['params']['host'],
                'username' => $config['resources']['db']['params']['username'],
                'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
                'dbname'   => $config['resources']['db']['params']['dbname'],
                'charset'  => $config['resources']['db']['params']['charset']
            ));

            $temp = $params[0];

            $arrkey = array_keys($temp);

            $str_insert = '';
            foreach ($params as $k => $param) {
                $param_data = [];
                foreach ($param as $key => $value){
                    $param_data[$key] = str_replace("'", "\'", $value);
                }
                $str_insert .= "('" . implode("', '", $param_data) . "')" . ',';
            }
            
            $str_rows = rtrim($str_insert, ',');

            $sql = "INSERT INTO `$db` ";

            $sql .= " (`" . implode("`, `", array_keys($temp)) . "`)";

            $sql .= " VALUES $str_rows ";
//            if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
//                echo "<pre>";
//                print_r($sql);
//                die; 
//            }
            $test = $db_log->query($sql);
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }
        
    public static function distance($lat1, $lon1, $lat2, $lon2) {
        $pi80 = M_PI / 180;
        $lat1 *= $pi80;
        $lon1 *= $pi80;
        $lat2 *= $pi80;
        $lon2 *= $pi80;
        $r    = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;
        $a    = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        $c    = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $km   = $r * $c;
        return round($km * 1000, 1);
    }

    public static function saveNotificationByApprove($content) {
        $QNotification       = new Application_Model_Notification();
        $QNotificationAccess = new Application_Model_NotificationAccess();
        $userStorage         = Zend_Auth::getInstance()->getStorage()->read();

//     	$db = Zend_Registry::get('db');
//     	$db->beginTransaction();
        try {
            $date = date('Y-m-d');

            $data_new = array(
                'title'       => 'Thông báo approve',
                'content'     => $content,
                'category_id' => 8,
                'status'      => 1,
                'created_at'  => date('Y-m-d H:i:s'),
                'created_by'  => $userStorage->id,
                'type'        => 1,
            );


            $where_access   = array();
            $where_access[] = $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $userStorage->id);
            $where_access[] = $QNotificationAccess->getAdapter()->quoteInto('DATE(created_date) = ?', $date);
            $result_access  = $QNotificationAccess->fetchRow($where_access);

            if (!empty($result_access)) {
                $notification_id = $result_access->toArray()['notification_id'];
                $id_access       = $result_access->toArray()['id'];

                $where_new   = array();
                $where_new[] = $QNotification->getAdapter()->quoteInto('id = ?', $notification_id);
                $where_new[] = $QNotification->getAdapter()->quoteInto('category_id = ?', 8);
                $result_new  = $QNotification->fetchRow($where_new);
                if (!empty($result_new)) {
//Update tbl notificaton_new
                    $data_new_update  = array(
                        'content' => $content,
                    );
                    $where_new_update = $QNotification->getAdapter()->quoteInto('id = ?', $notification_id);
                    $QNotification->update($data_new_update, $where_new_update);

//Update tbl notificaton_access
                    $data_access_update  = array(
                        'notification_status' => 0,
                    );
                    $where_access_update = $QNotificationAccess->getAdapter()->quoteInto('id = ?', $id_access);
                    $QNotificationAccess->update($data_access_update, $where_access_update);
                } else {
                    $id_new = $QNotification->insert($data_new);

                    $data_access = array(
                        'user_id'             => $userStorage->id,
                        'notification_id'     => $id_new,
                        'notification_status' => 0,
                        'created_date'        => date('Y-m-d H:i:s'),
                    );

                    $QNotificationAccess->insert($data_access);
                }
            } else {
                $id_new = $QNotification->insert($data_new);

                $data_access = array(
                    'user_id'             => $userStorage->id,
                    'notification_id'     => $id_new,
                    'notification_status' => 0,
                    'created_date'        => date('Y-m-d H:i:s'),
                );

                $QNotificationAccess->insert($data_access);

//commit
//$db->commit();
            }
        } catch (exception $e) {
//	$db->rollback();
            echo - 1;
            exit;
        }
    }

    public function rule_penalti($time) {
        if ($time <= 120) {
            $x = 0;
        } elseif ($time > 120 && $time <= 180) {
            $x = 0.5;
        } else {
            $phat   = round(($time * 2) / 480, 2);
            $nguyen = floor($phat);
            $x      = 0;
            $phat   = round($phat - $nguyen,2);
            if ($phat >= 0 && $phat <= 0.26) {
                $x = $nguyen;
            }
            if ($phat >= 0.27 && $phat <= 0.75) {
                $x = $nguyen + 0.5;
            }
            if ($phat >= 0.76 && $phat <= 1) {
                $x = $nguyen + 1;
            }
        }
    
        return $x;
    }

    public function postDispatch() {

//        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//        $user_id     = 0;
//        $user_name   = "NA";
//        if (!empty($userStorage)) {
//            $user_id = $userStorage->id;
//        }
//
//
//        $excute_time = microtime(true);
//        $total_time  = $excute_time - APP_START_TIME;
//
//        $log_model  = new Application_Model_LogExcutionAction();
//        $data_array = [
//            'excution_time' => round($total_time, 2),
//            'finished'      => 1
//        ];
//        $where[]    = $log_model->getAdapter()->quoteInto('unique_id like ?', UNIQUE_ID);
//        $where[]    = $log_model->getAdapter()->quoteInto('excution_user = ?', $user_id);
//        $log_model->update($data_array, $where);
    }

    public static function insertAllrowDB($params, $db,$db_log) {

        try {
            $temp = $params[0];

            $arrkey = array_keys($temp);

            $str_insert = '';
            foreach ($params as $k => $param) {
                $param_data = [];
                foreach ($param as $key => $value){
                    $param_data[$key] = str_replace("'", "\'", $value);
                }
                $str_insert .= "('" . implode("', '", $param_data) . "')" . ',';
            }
            
            $str_rows = rtrim($str_insert, ',');

            $sql = "INSERT INTO `$db` ";

            $sql .= " (`" . implode("`, `", array_keys($temp)) . "`)";

            $sql .= " VALUES $str_rows ";
            $test = $db_log->query($sql);
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }
}
