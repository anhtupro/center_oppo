<?php

class My_Approve extends Zend_Controller_Plugin_Abstract
{
    public static function run()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        //Sale, sale leader approve staff time
        $db = Zend_Registry::get('db');
        $staff_id = $userStorage->id;
		/*
        if($userStorage->title == 190){
            $stmt = $db->prepare("CALL PR_get_unconfirmed_list_sale_leader(:staff_id)");
            $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            if(!empty($data)){
                return 1;
            }
             
        }*/
		if ($userStorage->title == 183){

            $stmt1 = $db->prepare("CALL PR_get_unconfirmed_list_sale_fix(:staff_id)");
            $stmt1->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
            $stmt1->execute();
            $data1 = $stmt1->fetchAll();
				
            $stmt1->closeCursor();
		
            if(!empty($data1)){
                return 1;
            }
			
        }
        //End

        return false;
    }
    public static function needToDoDailyReport()
    {
        $auth = Zend_Auth::getInstance()->getStorage()->read();
        //check 7 ngày gần nhất
        $date = [
            date('Y-m-d',strtotime("-1 days")),
            date('Y-m-d',strtotime("-2 days")),
            date('Y-m-d',strtotime("-3 days")),
            date('Y-m-d',strtotime("-4 days")),
            date('Y-m-d',strtotime("-5 days")),
            date('Y-m-d',strtotime("-6 days")),
            date('Y-m-d',strtotime("-7 days"))
        ];
        //check báo số chi tiết
        $QSumupSalesDaily = new Application_Model_SumupSalesDaily();
        $remainDetailReportBrand = $QSumupSalesDaily->getListNeedToReport($auth->id, $date);

        //check báo số tổng
        $QSumupSalesDailyTotal = new Application_Model_SumupSalesDailyTotal();
        $remainTotalReportBrand = $QSumupSalesDailyTotal->getListNeedToReport($auth->id, $date);

        if ($remainDetailReportBrand || $remainTotalReportBrand) {
            return true;
        }

        return false;
    }
   
}
