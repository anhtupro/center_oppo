<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class My_StaffPermissions
{
    const TYPE_AREA_ID = 1;
    const TYPE_PROVINCE_ID = 2;
    const TYPE_DISTRICT_ID = 3;
    const TYPE_DEPARTMENT_ID = 4;
    const TYPE_TEAM_ID = 5;
    const TYPE_TITLE_ID = 6;
    const TYPE_OFFICE_ID = 7;
    const TYPE_STAFF_ID = 8;
    const TYPE_FILTER_AREA_ID = 9;
}