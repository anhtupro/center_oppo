<?php
/**
* @author buu.pham
* @create 2015-10-27T10:43:09+07:00
* Danh sách các dashboard và điều kiện cần để đưa staff vào dashboard này
*/
class My_Dashboard_Dashboards
{
    private $_dashboardList = array();
    const EMAIL_CREATE = 6;

    public function __construct()
    {
        $email_create = new My_Dashboard('email_create', array(
            'off_date'        => array('function' => 'empty', 'condition' => ''),
            'code'            => array('function' => 'empty', 'condition' => 'not'),
            'firstname'       => array('function' => 'empty', 'condition' => 'not'),
            'lastname'        => array('function' => 'empty', 'condition' => 'not'),
            'department'      => array('function' => 'empty', 'condition' => 'not'),
            'team'            => array('function' => 'empty', 'condition' => 'not'),
            'title'           => array('function' => 'equal', 'condition' => 'not', 'value' => PGPB_TITLE),
            'joined_at'       => array('function' => 'empty', 'condition' => 'not'),
            'group_id'        => array('function' => 'empty', 'condition' => 'not'),
            'regional_market' => array('function' => 'empty', 'condition' => 'not'),
            // 'email'        => array('function' => 'empty', 'condition' => ''),
            'has_email'       => array('function' => 'equal', 'condition' => '', 'value' => 0),
        ));

        $this->_dashboardList[self::EMAIL_CREATE] = $email_create;
    }

    public function getDashboards()
    {
        return $this->_dashboardList;
    }

    public function validate($staff, My_Dashboard $dashboard)
    {
        $conditions = $dashboard->getConditions();

        foreach ($conditions as $key => $condition) {
            if (isset($staff[ $key ])
                && !$this->_check($condition, $staff[$key])) {
                // PC::db($condition);
                // PC::db($staff[$key]);
                return false;
            }
        }

        return true;
    }

    private function _check($condition, $value)
    {
        switch ($condition['function']) {
            case 'empty':
                if ('not' == $condition['condition'])
                    return !empty($value);
                return empty($value);
                break;
            case 'hasvalue':
                if ('not' == $condition['condition'])
                    return (empty($value) || is_null($value));
                return !(empty($value) || is_null($value));
                break;
            case 'equal':
                if ('not' == $condition['condition'])
                    return !(empty($value) || $value == $condition['value']);
                return empty($value) || $value == $condition['value'];
                break;

            default:
                break;
        }
    }
}
