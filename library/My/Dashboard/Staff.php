<?php
/**
* @author buu.pham
* @create 2015-10-27T10:38:41+07:00
*/
class My_Dashboard_Staff
{
    /**
     * [add description]
     * @param [type] $staff_id     [description]
     * @param [type] $dashboard_id [description]
     */
    public static function add($staff_id, $dashboard_id)
    {
        try {
            $QDashboardStaff = new Application_Model_DashboardStaff();
            $time = time();
            $data = array(
                'staff_id'      => $staff_id,
                'dashboard_id'  => $dashboard_id,
                'add_time'      => date('Y-m-d H:i:s', $time),
                'add_time_unix' => $time,
            );
            $QDashboardStaff->insert($data);
        } catch (Exception $e) {}
    }

    /**
     * [remove description]
     * @param  [type] $staff_id     [description]
     * @param  [type] $dashboard_id [description]
     * @return [type]               [description]
     */
    public static function remove($staff_id, $dashboard_id)
    {
        try {
            $QDashboardStaff = new Application_Model_DashboardStaff();
            $where = array();
            $where[] = $QDashboardStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where[] = $QDashboardStaff->getAdapter()->quoteInto('dashboard_id = ?', $dashboard_id);
            $QDashboardStaff->delete($where);
        } catch (Exception $e) {}
    }

    /**
     * [checkDashboardCondition description]
     * @param  [type] $staff [description]
     * @return [type]        [description]
     */
    public static function checkDashboardCondition($staff)
    {
        $dashboard_manager = new My_Dashboard_Dashboards();
        $dashboard_list = $dashboard_manager->getDashboards();

        foreach ($dashboard_list as $_id => $_dashboard) {
            if ($dashboard_manager->validate($staff, $_dashboard))
                self::add($staff['id'], $_id);
            else
                self::remove($staff['id'], $_id);
        }
    }
}
