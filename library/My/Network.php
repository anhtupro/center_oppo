<?php 
/**
* @author buu.pham
* Các loại network khách hàng dùng trên điện thoại
*/
class My_Network
{
    const WIFI = 1;
    const MOBILE_3G = 2;
    const WIFI_MOBILE_3G = 3;
    const NONE = 4;

    public static $name = array(
        self::WIFI => 'Wifi',
        self::MOBILE_3G => '3G',
        self::WIFI_MOBILE_3G => 'Wifi + 3G',
        self::NONE => 'None',
    );
}
