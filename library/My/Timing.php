<?php
/**
* 
*/
class My_Timing
{

    const SIGN_TIME_CHECKIN_SUNDAY_FULLDAY  = 'C';
    const SIGN_TIME_CHECKIN_SUNDAY_HALFDAY  = 'D';
    const SIGN_TIME_CHECKIN_WEEKDAY_HALFDAY = 'H';
    const SIGN_TIME_CHECKIN_WEEKDAY_FULLDAY = 'X';
    const SIGN_TIME_CHECKIN_SPECIAL_DAY     = 'L';
    const SIGN_TIME_CHECKIN_OFF_DAY         = 'P';
    const SIGN_TIME_CHECKIN_TRAINING        = 'T';
    const SIGN_TIME_CHECKIN_GAY             = 'G';
    const SIGN_TIME_CHECKIN_CHILDBEARING    = 'TS';

    public static function sign( $checkin , $shift , $off)
    {
        $saturday = $sunday = array();

        if(empty($off))
        {
            $date = date('Y-m-d', strtotime($checkin));
            $week = date('l', strtotime($date));

        }
        else
        {
            // cÃ¡c cháº¥m cÃ´ng off

        }

    }

    public  function  transfer($staff_id , $from ,  $to)
    {


        $result = array(
            'before' => '',
            'after'  => '',
            'noted'  => ''
        );





        if(!$to)
            return $result;

        try
        {
            $db = Zend_Registry::get('db');

            //load view staff transfer

            $select = $db->select()
                ->from('v_staff_transfer' , array( '*' , new Zend_Db_Expr("MAX(`v_staff_transfer`.from_date) AS maxDate")))
                ->where('`v_staff_transfer`.staff_id = ?' , $staff_id)
                ->where('`v_staff_transfer`.from_date >= ?' , $from);



                    //  echo $select;exit;


            /* $select = $db->select()
                ->from("staff_transfer", array(new Zend_Db_Expr("MAX(created_at) AS maxDate")))
                ->where("staff_id = ?" , $staff_id)
                ->where("created_at >= ?" , $from)
                ->where("created_at <= ?" , $to);
           */

            $result_transfer = $db->fetchRow($select);

            if(empty($result_transfer))
                return $result;

            $next_arrow = '-->';
            $QTeam = new Application_Model_Team();
            $title  = $QTeam->get_cache();

            $QCompany = new Application_Model_Company();
            $company  = $QCompany->get_cache();

            $QArea = new Application_Model_Area();
            $areas = $QArea->get_cache();

            $QRegional_market = new Application_Model_RegionalMarket();
            $regional_market  = $QRegional_market->get_cache();



            if($result_transfer)
            {
                $select_from = $db->select()
                    ->from("time", array(new Zend_Db_Expr("COUNT(id) AS totalTime")))
                    ->where("staff_id = ?" , $staff_id)
                    ->where("DATE(created_at) >= ?" , $from)
                    ->where("DATE(created_at) < ?" , $result_transfer['maxDate'])
                    ->where("approved_at is not null")
                ;

                $select_to = $db->select()
                    ->from("time", array(new Zend_Db_Expr("COUNT(id) AS totalTime")))
                    ->where("staff_id = ?" , $staff_id)
                    ->where("DATE(created_at) >= ?" , $result_transfer['maxDate'])
                    ->where("DATE(created_at) < ?" , $to)
                    ->where("approved_at is not null")
                ;


                $log_transfer = array_filter(array(
                     $areas[$result_transfer['old_area']] !=  $areas[$result_transfer['area']] ?     'Area: ' . $areas[$result_transfer['old_area']] . $next_arrow . $areas[$result_transfer['area']] : '' ,
                     $result_transfer['old_team'] != $result_transfer['team'] ? 'Team: ' . $title[$result_transfer['old_team']] . $next_arrow . $title[$result_transfer['team']] : '' ,
                     $result_transfer['old_regional_market'] != $result_transfer['regional_market'] ? 'Regional Market: ' . $regional_market[$result_transfer['old_regional_market']] . $next_arrow . $regional_market[$result_transfer['old_regional_market']] : '',
                     $result_transfer['old_title'] != $result_transfer['title'] ? 'Title: ' . $title[$result_transfer['old_title']] . $next_arrow . $title[$result_transfer['title']] : '',
                     $result_transfer['old_company_id'] != $result_transfer['company_id'] ? 'Company: ' . $company[$result_transfer['old_company_id']]. $next_arrow . $company[$result_transfer['company_id']] : '',

                ));



                $result_from = $db->fetchOne($select_from);
                $result_to   = $db->fetchOne($select_to);
                $result = array_filter(array(
                    'before' => $result_from ? $result_from : 0,
                    'after'  => $result_to ? $result_to : 0,
                    'transfer_date' => $result_transfer['maxDate'],
                    'noted'  => implode(', ' , $log_transfer)
                ));


            }
            return $result;
        }

        catch(exception $e)
        {
            return $result;
        }





    }
}