<?php 
/**
* 
*/
class My_Excel_Column
{
    /**
     * Chuyển số thứ tự cột dạng số tự nhiên sang dạng chữ như trong Excel
     *     (A, B, C... AA, AB, AC...)
     * @param  int $num - thứ tự cột
     * @return char
     */
    public static function getNameFromNumber($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        
        if ($num2 > 0)
            return self::getNameFromNumber($num2 - 1) . $letter;
        else
            return $letter;
    }
}