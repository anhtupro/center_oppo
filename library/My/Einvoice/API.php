<?php
/**
 * Created by PhpStorm.
 * User: danhnh
 * Date: 27/05/2019
 * Time: 10:15
 */


class My_Einvoice_API extends My_Einvoice_ObjectBuilder
{
    protected $api_type;
    protected $service;
    protected $http_header;
    protected $method;
    protected $url;
    protected $params;
    /**
     * GetStatus constructor.
     */

    public function __construct()
    {

    }

    public function getApiType()
    {
        return $this->api_type;
    }

    public function setApiType($api_type)
    {
        $this->api_type = $api_type;
    }

    public function getService()
    {
        return $this->service;
    }

    public function setService($service)
    {
        $this->service = $service;
    }

    public function getHttpHeader()
    {
        return $this->http_header;
    }

    public function setHttpHeader($http_header)
    {
        $this->http_header = $http_header;
    }

    public function callAPI($method = '', $params = array(), $http_header = '', $url = ''){
//    set_time_limit(0);
        $this->setListParam(array(
            'method' => $method,
            'params' => $params,
            'http_header' => $http_header,
            'url' => $url
        ));
//        print "<pre>".print_r($this->url)."</pre>";
//        print "<pre>".print_r($this->method)."</pre>";
//        print "<pre>".print_r($this->params)."</pre>";
//        print "<pre>".print_r($this->http_header)."</pre>";die;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($curl, CURLOPT_PORT, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->method);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->params);
        curl_setopt($curl, CURLOPT_HTTPHEADER , $this->http_header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 0);
        curl_setopt($curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, [
//            'Content-Type: text/xml',
//            'SOAPAction: http://tempuri.org/'.$func,
//            'Charset: utf-8'
//        ]);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $data = curl_exec($curl);
        
        $error = curl_errno($curl);
        $http_status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $res = array('data' => json_decode($data, true), 'is_error' => $error, 'http_status_code' => $http_status_code);
        return $res;
    }

    public function getCurrentEinvoiceService()
    {
        return array(
            'WAREHOUSE' => 1,
        );
    }

}
