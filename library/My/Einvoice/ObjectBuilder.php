<?php
/**
 * Created by PhpStorm.
 * User: lphuctai
 * Date: 09/11/2018
 * Time: 16:58
 */

abstract class My_Einvoice_ObjectBuilder
{
    /**
     * Constructor can quickly set variable for object
     * @param $data
     */
    public function __construct($data = [])
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
        return $this;
    }

    public function setParam($key, $value)
    {
        if(!empty($value))
            $this->{$key} = $value;
    }

    public function setListParam($data = [])
    {
        foreach ($data as $key => $value) {
            if(!empty($value))
                $this->{$key} = $value;
        }
    }
}