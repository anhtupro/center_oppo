<?php
/**
 * Created by PhpStorm.
 * User: danhnh
 * Date: 27/05/2019
 * Time: 10:15
 */


class My_Einvoice_Payment_API extends My_Einvoice_API
{
    protected $sell_tax;
    protected $url;
    protected $ws_url;
    protected $token;

    /**
     * GetStatus constructor.
     */

    public function __construct($sell_tax = null)
    {
        $this->service = 'Payment';
        $this->api_type = 'Restful';
        $this->ws_url = 'http://payment.opposhop.vn/';
        
        $this->sell_tax = $sell_tax;

    }

    public function getSellTax()
    {
        return $this->sell_tax;
    }

    public function getFormatSellTax()
    {
        return str_replace('-','',$this->sell_tax);
    }

    public function setSellTax($sell_tax)
    {
        $this->sell_tax = $sell_tax;
    }

    public function getControllerActionPayment()
    {
        $func = $this->ws_url . 'wss/get-controller-payment';
        $header = array(
            'Content-Type: application/json'
        );

        $pass = md5('0p0pVj3tNam.?;Taem2030');
        $func = $func . '?password='.$pass;

        $call_api_result = $this->callAPI('GET', '', $header, $func);

        return $call_api_result;

    }
}