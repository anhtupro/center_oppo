<?php
/**
 * Created by PhpStorm.
 * User: danhnh
 * Date: 27/05/2019
 * Time: 10:15
 */


class My_Einvoice_Warehouse_API extends My_Einvoice_API
{
    protected $sell_tax;
    protected $url;
    protected $ws_url;
    protected $token;

    /**
     * GetStatus constructor.
     */

    public function __construct($sell_tax = null)
    {
        $this->service = 'Warehouse';
        $this->api_type = 'Restful';
        $this->ws_url = 'http://warehouse.opposhop.vn/';
        
//        $this->ws_url = 'http://demows.cyberbill.vn/api/services/hddtws/';
        $this->sell_tax = $sell_tax;

    }

    public function getSellTax()
    {
        return $this->sell_tax;
    }

    public function getFormatSellTax()
    {
        return str_replace('-','',$this->sell_tax);
    }

    public function setSellTax($sell_tax)
    {
        $this->sell_tax = $sell_tax;
    }

    public function getControllerActionWarehouse()
    {
        $func = $this->ws_url . 'wss/get-controller-wh';
        $header = array(
            'Content-Type: application/json'
        );

        $pass = md5('0p0pVj3tNam.?;Taem2030');
        $func = $func . '?password='.$pass;

        $call_api_result = $this->callAPI('GET', '', $header, $func);

        return $call_api_result;

    }
}