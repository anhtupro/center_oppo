<?php
/**
*
*/
class My_Imei_Tool
{
    public static function autobotChecking($staff_id)
    {
        return false;
        $check_30 = $check_1 = 0;
        $db = Zend_Registry::get('db');

        // $sql = "SELECT COUNT( lg.imei ) AS total
        //     FROM  `check_imei_log` lg
        //     WHERE lg.result <> 0
        //     AND lg.time >=  DATE_SUB( DATE( NOW( ) ) , INTERVAL 30 DAY )
        //     AND lg.user_id=?";

        // $result = $db->query($sql, array(intval($staff_id)));
        // $check_30 = $result->fetch();
        // $check_30 = $check_30 && isset($check_30['total']) ? $check_30['total'] : 0;

        $sql = "SELECT COUNT( lg.imei ) AS total
            FROM  `check_imei_log` lg
            WHERE lg.result <> 0
            AND DATE(lg.time) =  DATE(NOW())
            AND lg.user_id=?";

        $result = $db->query($sql, array(intval($staff_id)));
        $check_1 = $result->fetch();
        $check_1 = $check_1 && isset($check_1['total']) ? $check_1['total'] : 0;

        if ($check_1 > IMEI_CHECKING_FAILED_IN_1_DAYS || $check_30 > IMEI_CHECKING_FAILED_IN_30_DAYS)
            return true;

        return false;
    }
}