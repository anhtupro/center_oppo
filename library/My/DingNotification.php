<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author Hero
 */
class My_DingNotification {

    private $appId;
    private $appSecret;
    private $appKey;
    private $accessToken;

    public function __construct($appId, $appSecrect, $appKey) {
        $this->appId     = $appId;
        $this->appSecret = $appSecrect;
        $this->appKey    = $appKey;
        $this->getAccesstoken();
    }

    private function getAccesstoken() {
        $url      = "https://oapi.dingtalk.com/gettoken?appkey=$this->appKey&appsecret=$this->appSecret";
        $response = file_get_contents($url);
        $response = json_decode($response,true);
       
        if ($response['errcode'] == 0) {
            $this->accessToken = $response['access_token'];
        } else {
            echo "<pre>";
            var_dump($result['errmsg']);
            die;
        }
    }

    public function sendTextNotification($list_uid, $message) {
 
        $message_overide = ['msgtype' => 'text', 'text' => ['content' => $message]];
        $url             = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2?access_token=$this->accessToken";
        $params          = array(
            'agent_id'    => $this->appId,
            'userid_list' => $list_uid,
            'msg'         => json_encode($message_overide)
        );
       
        $result=$this->_curl($url,$params);
        echo "<pre>";
        var_dump($result);
        die;
    }
    
      public function sendTextNotificationDept($list_dept, $message) {
 
        $message_overide = ['msgtype' => 'text', 'text' => ['content' => $message]];
        $url             = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2?access_token=$this->accessToken";
        $params          = array(
            'agent_id'    => $this->appId,
            'dept_id_list' => $list_dept,
            'msg'         => json_encode($message_overide)
        );
       
        $result=$this->_curl($url,$params);
        echo "<pre>";
        var_dump($result);
        die;
    }

    private function _curl($url = null, $pars = array()) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $pars);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, false);
        $res  = curl_exec($curl);
        curl_close($curl);
        return $res;
    }

}
