<?php
class My_Message
{
    
    public static function palert($msg = '')
    {
        exit('<script>parent.palert("'.$msg.'");</script>');
    }

    public static function jsAlert($msg,$option = 'success',$exit = 0){
    	if($option == 'success'){
    		echo '<script>window.parent.document.getElementById("palert").innerHTML = "<div class=\'alert alert-success\'>Success!</div>";</script>';
    			
    	}else{
    		echo '<script>window.parent.document.getElementById("palert").innerHTML = "<div class=\'alert alert-error\'>Failed - '.$msg.'</div>";</script>';
    	}

    	if($exit){
    		exit;
    	}
    }

    /**
     * [blockUI description: 1:clock, 2: unclock]
     * @param  integer $clock [description]
     * @return [type]         [description]
     */
    public static function blockUI($clock = 1){

    }

    public static function redirect($url = null)
    {
        if (!$url) exit;

        exit('<script>parent.window.location="'.$url.'"</script>');
    }
}