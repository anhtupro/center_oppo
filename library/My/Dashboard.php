<?php
/**
* @author buu.pham
* @create 2015-10-27T10:44:18+07:00
* Định nghĩa Dashboard
*/
class My_Dashboard
{
    private $_name;
    private $_conditions;

    function __construct($name, array $conditions) {
        $this->_name = $name;
        $this->_conditions = $conditions;
    }

    public function getConditions()
    {
        if (!isset($this->_conditions))
            return null;

        return $this->_conditions;
    }

    public function setConditions(array $conditions)
    {
        $this->_conditions = $conditions;
    }

    public function getName()
    {
        if (!isset($this->_name))
            return null;

        return $this->_name;
    }

    public function setName(array $name)
    {
        $this->_name = $name;
    }
}
