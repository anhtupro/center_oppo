<?php
/**
*
*/
class My_Request
{
    public static function isXmlHttpRequest()
    {
        if ((isset($_SERVER['X_REQUESTED_WITH']) && $_SERVER['X_REQUESTED_WITH'] == 'XMLHttpRequest')
            || (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
        )
            return true;

        return false;
    }

    public static function sync_table($options)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        try {
            $id     = isset($options['id']) ? $options['id'] : 0;
            $type   = isset($options['type']) ? $options['type'] : 'all';
            $table  = isset($options['table']) ? $options['table'] : null;

            $options = array(
                'type'  => $type,
                'table' => $table,
            );

            $db = Zend_Registry::get('db');

            $QWS = new Application_Model_WS();

            if ($id != 0)
            {
                $select = $db->select()->from($table)->where('id = ' . $id);
                $stmt   = $db->query($select);
                $result = $stmt->fetchAll();

                $response_ws = $QWS->syncTable($result[0], $options);
            }
            else
            {
                $select = $db->select()->from($table);
                $stmt   = $db->query($select);
                $result = $stmt->fetchAll();

                $QWS->truncateTable($table);

                foreach ($result as $k => $v)
                    $response_ws = $QWS->syncTable($v, $options);
            }
        } catch (Exception $e) {}
    }
}