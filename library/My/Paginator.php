<?php
class My_Paginator{
    public function createPaginator($totalItems,$arrPaginator,$options =null){
        $adapter  = new Zend_Paginator_Adapter_Null($totalItems);
        $panigator = new Zend_Paginator($adapter);
        $panigator->setItemCountPerPage($arrPaginator['itemCountPerPage']);
        $panigator->setPageRange($arrPaginator['pageRange']);
        $panigator->setCurrentPageNumber($arrPaginator['currentPage']);
        return $panigator;
    }
   
}