<?php
class My_Builder extends Zend_Controller_Plugin_Abstract
{
    public static function run()
    {
       $userStorage = Zend_Auth::getInstance()->getStorage()->read();
       $staff_id = $userStorage->id;


       $db = Zend_Registry::get('db');
       //get survey_ids
       $sql = " SELECT survey_id
                FROM survey_access
                WHERE survey_id IN ( 
                                     SELECT id
                                     FROM survey_form
                                     WHERE (status = 1 ) 
                                     AND required = 1
                                   )
                AND status = 0  AND staff_id = $staff_id";

        $stmt = $db->prepare($sql);    
        $stmt->execute();
        $result = $stmt->fetchAll();  
        $stmt->closeCursor();

        foreach ($result as $key => $value) {
            $survey_id [] = $value['survey_id'];
        }

       if (empty($survey_id)) {
            return false;
       }

       return $survey_id;
    }

   
}
