<?php

/**
 * @author buu.pham
 * @create 2015-11-06T09:43:44+07:00
 * Load survey, bắt nó làm survey
 */
class My_Survey extends Zend_Controller_Plugin_Abstract {

    public static function run() {

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QSurveyObject = new Application_Model_SurveyObject();
        $QSurveyStaff = new Application_Model_SurveyStaff();
        $QStaffMainNew=new Application_Model_StaffMainNew();


            $result_staff = $QSurveyStaff->check($userStorage->id);

        if (isset($result_staff) && is_numeric($result_staff) && $result_staff) {
            return $result_staff;
        } else {

                $params['staff_id']=$userStorage->id;
                $staffChannel=$QStaffMainNew->getListChannelByPgsId($params);

            if( !empty($staffChannel) AND in_array($userStorage->title, array(SALE_SALE_PGPB,CHUYEN_VIEN_BAN_HANG_TITLE))){ //2 title channel dividing

                $result = $QSurveyObject->checkNew($userStorage->id,$staffChannel);
                // $result = $QSurveyObject->check($userStorage->id);


            }else{
                $result = $QSurveyObject->check($userStorage->id);
                // if($userStorage->id == 13672){
                //     echo $result;exit;
                // }
            }

            if (isset($result) && is_numeric($result) && $result) {

                //ngoai le
                if ($result == 8 and in_array($userStorage->id, array(341, 5968))) {
                    return false;
                }
                if ($result == 11 and ( $userStorage->joined_at > '2015-11-28' || !empty($userStorage->off_date))) {
                    return false;
                }

                return $result;
            }
        }
        return false;
    }

    public static function force($controller_name, $action_name) {
        if (!in_array($controller_name, array('survey', 'user')))
            return true;

        return false;
    }

}
