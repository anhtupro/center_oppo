<?php
/**
* @author buu.pham
*/
class My_Staff_Relative extends My_Type_Enum
{
    const Wife    = 1;
    const Husband = 2;
    const Father  = 3;
    const Mother  = 4;
    const Brother = 5;
    const Sister  = 6;
    const Child   = 7;
	const Grandparent = 8;
    const Other   = 9;
	
}