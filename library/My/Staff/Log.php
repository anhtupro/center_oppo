<?php

/**
 * @author buu.pham
 */
class My_Staff_Log {

    public static function getValue($input = 0, $type = null) {
        $output = '';

        if ($input && $type) {

            switch ($type) {
                case My_Staff_Info_Type::Area:
                    $QModel = new Application_Model_Area();
                    $cache  = $QModel->get_cache();
                    if (isset($cache[$input]))
                        $output = $cache[$input];
                    break;
                case My_Staff_Info_Type::Region:
                    $QModel = new Application_Model_RegionalMarket();
                    $cache  = $QModel->get_cache();
                    if (isset($cache[$input]))
                        $output = $cache[$input];
                    break;
                case My_Staff_Info_Type::Group:
                    $QModel = new Application_Model_Group();
                    $cache  = $QModel->get_cache();
                    if (isset($cache[$input]))
                        $output = $cache[$input];
                    break;
                case My_Staff_Info_Type::Team:
                    $QModel = new Application_Model_Team();
                    $cache  = $QModel->get_cache();
                    if (isset($cache[$input]))
                        $output = $cache[$input];
                    break;
                case My_Staff_Info_Type::Department:
                    $QModel = new Application_Model_Department();
                    $cache  = $QModel->get_cache();
                    if (isset($cache[$input]))
                        $output = $cache[$input];
                    break;
                case My_Staff_Info_Type::Company:
                    $QModel = new Application_Model_Company();
                    $cache  = $QModel->get_cache();
                    if (isset($cache[$input]))
                        $output = $cache[$input];
                    break;
                case My_Staff_Info_Type::Status:
                    if (My_Staff_Status::get($input))
                        $output = My_Staff_Status::get($input);
                    break;

                default:
                    break;
            } // END switch
        } // END big if


        return $output;
    }

    /**
     * Log bình thường, mỗi khi edit staff
     * @param  [type] $staff_id [description]
     * @param  array  $data     [description]
     * @return [type]           [description]
     */
    public static function write($staff_id, array $data) {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if (!$userStorage)
            throw new Exception("Invalid user. Must login to use this action.");

        $ip = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];

        if (empty($ip))
            throw new Exception("Invalid IP.");

        $before = isset($data['before']) ? $data['before'] : array();
        $after  = isset($data['after']) ? $data['after'] : array();

        if ($after || !count($after))
            throw new Exception("New data may be wrong, cannot blank, please check again.");

        $QLog = new Application_Model_StaffLog();

        $log_id = $QLog->insert(array(
            'object'     => $staff_id,
            'before'     => serialize($before),
            'after'      => serialize($after),
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));

        return $log_id;
    }

    /**
     * Transfer of work - lưu theo trường
     * @param  [type] $staff_id [description]
     * @param  array  $data     [description]
     * @return [type]           [description]
     */
    public static function transfer($staff_id, $transfer_data, $option = 'add', $new_title) {
        $userStorage     = Zend_Auth::getInstance()->getStorage()->read();
        $db              = Zend_Registry::get('db');
        $QStaff          = new Application_Model_Staff();
        $QStaffLogDetail = new Application_Model_StaffLogDetail();
        $QStaffTransfer  = new Application_Model_StaffTransfer();
        $QStaffTransferTmp = new Application_Model_StaffTransferTmp();
        if (!$userStorage) {
            throw new Exception("Invalid user. Must login to use this action.");
        }
       
        if (isset($transfer_data['from_date']) && strtotime($transfer_data['from_date'])) {
            $from_date = $transfer_data['from_date'];
        } else {
            throw new Exception("Invalid date.");
        }
        $transfer_tmp_id = $transfer_data['transfer_tmp_id'];
        // Data for new transfer
        $data   = array(
            'staff_id'   => $staff_id,
            'transfer_tmp_id'   => $transfer_tmp_id,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id,
            'from_date'  => date('Y-m-d', strtotime($from_date)),
        );
        $time   = strtotime($from_date);
        $object = $staff_id;
        $info   = $transfer_data['info']; //info_type => value

        $db->beginTransaction();
        try {

            if ($option == 'update') {

                // update to_date time for old transfer
                $selectOldTransfer = $QStaffTransfer->select()
                        ->where('staff_id = ?', $staff_id)
                        ->where('to_date IS NOT NULL')
                        ->order('created_at DESC')
                        ->limit(1);
                ;
                $rowOld            = $QStaffTransfer->fetchRow($selectOldTransfer);
                if ($rowOld) {
                    $rowOld->to_date = $from_date;
                    $rowOld->save();
                }

                //update current transfer
                $whereUpdateTransfer = $QStaffTransfer->getAdapter()->quoteInto('id = ?', $transfer_data['transfer_id']);
                $data                = array('from_date' => date('Y-m-d', strtotime($from_date)));
                $QStaffTransfer->update($data, $whereUpdateTransfer);
                $transfer_id         = $transfer_data['transfer_id'];

                // tạm thời không check duplicate trong trường hợp update
                foreach ($info as $key => $value) {
                    $where              = $QStaffLogDetail->select()
                            ->where('object = ?', $object)
                            ->where('info_type = ?', $key)
                            ->where('transfer_id = ?', $transfer_id);
                    $row                = $QStaffLogDetail->fetchRow($where);
                    $row->current_value = ($key == My_Staff_Info_Type::Title) ? trim($value) : intval($value);
                    $row->save();
                }
                $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                $user_id     = $userStorage->id;

                //Update staff table
                if ($row->to_date == NULL) {//nếu là transfer mới nhất
                    $dataStaff = array(
                        'company_id'      => $info[My_Staff_Info_Type::Company],
                        'department'      => $info[My_Staff_Info_Type::Department],
                        'team'            => $info[My_Staff_Info_Type::Team],
                        'regional_market' => $info[My_Staff_Info_Type::Region],
                        'title'           => $info[My_Staff_Info_Type::Title],
                        'group_id'        => $info[My_Staff_Info_Type::Group],
                        'updated_at'      => date('Y-m-d H:i:s'),
                        'updated_by'      => $userStorage->id,
                    );

                    if ($info[My_Staff_Info_Type::Title] == PGPB_TITLE)
                        $dataStaff['has_email'] = 0;

                    self::updateStaffTable($dataStaff, $staff_id);

                    $QStaffContract = new Application_Model_StaffContract();
                    $check_contract = $QStaffContract->checkTransferToContract($transfer_id);
                    if (!empty($check_contract)) {
                        throw new Exception("Tranfer này đã phát sinh hợp đồng, vui lòng rollback lại các hợp đồng phát sinh từ transfer này.");
                    }
                    $result_transfer = My_Staff_Transfer::transfer_to_contract($transfer_id);
                    if (!$result_transfer['status']) {
                        throw new Exception($result_transfer['message']);
                    }
                }
            } else {

                //Kiểm tra lần đầu :D
                $selectNew              = $QStaffTransfer->select()
                        ->where('staff_id = ?', $staff_id);
                $new                    = $QStaffTransfer->fetchAll($selectNew);

                $selectCurrentStaffInfo = $db->select()
                        ->from(array('s' => 'staff'), array(
                            's.company_id',
                            's.group_id',
                            's.department',
                            's.team',
                            'region' => 'r.id',
                            'area'   => 'r.area_id',
                            's.title',
                            'joined_at'
                        ))
                        ->join(array('r' => 'regional_market'), 's.regional_market = r.id', array())
                        ->where('s.id = ?', $staff_id)
                ;
                $currentStaffInfo       = $db->fetchRow($selectCurrentStaffInfo);
                if (!count($new)) {


                    $basicTransferData = array(
                        'note'       => 'initFirstData',
                        'staff_id'   => $staff_id,
                        'created_at' => $from_date,
                        'created_by' => $userStorage->id,
                        'from_date'  => $currentStaffInfo['joined_at'],
                        'transfer_tmp_id'   => $transfer_tmp_id,
                        'to_date'    => NULL
                    );
                    $basicTransferId   = $QStaffTransfer->insert($basicTransferData);

                    $arrBasicInfoType = array(
                        My_Staff_Info_Type::Company    => $currentStaffInfo['company_id'],
                        My_Staff_Info_Type::Department => $currentStaffInfo['department'],
                        My_Staff_Info_Type::Team       => $currentStaffInfo['team'],
                        My_Staff_Info_Type::Group      => $currentStaffInfo['group_id'],
                        My_Staff_Info_Type::Area       => $currentStaffInfo['area'],
                        My_Staff_Info_Type::Region     => $currentStaffInfo['region'],
                        My_Staff_Info_Type::Title      => $currentStaffInfo['title']
                    );

                    foreach ($arrBasicInfoType as $key => $value) {
                        $arrTmp = array(
                            'object'        => $staff_id,
                            'info_type'     => $key,
                            'current_value' => $value,
                            'transfer_id'   => $basicTransferId,
                            'from_date'     => strtotime($currentStaffInfo['joined_at']),
                            'to_date'       => NULL,
                        );
                        $QStaffLogDetail->insert($arrTmp);
                    }
                }// End check first time
                //update  date for old transfer
                $selectOldTransfer = $QStaffTransfer->select()
                        ->where('staff_id = ?', $staff_id)
                        ->where('to_date IS NULL', null)
                ;
                $rowOld            = $QStaffTransfer->fetchRow($selectOldTransfer);
                if ($rowOld) {
                    $rowOld->to_date = date('Y-m-d', strtotime($from_date));
                    $rowOld->save();
                }

                // Add new transfer
                $transfer_id    = $QStaffTransfer->insert($data);
                $checkDuplicate = true;
                $message_log    = '';
                // echo "<pre>";print_r($info);die;
                $rowOld_id = $rowOld->id;
                foreach ($info as $key => $value) {
                    //update old staff log detail
                    $where     = $QStaffLogDetail->select()
                            ->where('object = ?', $object)
                            ->where('info_type = ?', $key)
//                            ->where('transfer_id = ?', $rowOld->id)
                            ->where('transfer_id = ?', $rowOld_id)
                    ;
                    $row       = $QStaffLogDetail->fetchRow($where); //old staff log detail
                    // echo "<pre>";print_r($row);die;
                    $old_value = null;
                    if ($row) {//nếu có giá trị cũ
                        $row->to_date = $time;
                        $row->save();
                        $old_value    = $row->current_value;
                    }

                    // data for new row
                    $data = array(
                        'transfer_id'   => $transfer_id,
                        'object'        => $object,
                        'info_type'     => $key,
                        'old_value'     => ($key == My_Staff_Info_Type::Title) ? trim($old_value) : intval($old_value),
                        'current_value' => ($key == My_Staff_Info_Type::Title) ? trim($value) : intval($value),
                        'from_date'     => $time,
                    );
                   
                    //$message_log .= '<br/>key:'.$key.'- value'.json_encode($data);
                    if ($old_value != $value) {
                        $checkDuplicate = false;
                    }

                    if ($row) {
                        if (!self::_check_transfer_time($object, $key, $time)) {
                            exit(
                                    json_encode(array('status' => 0, 'message' => 'Time transfer is wrong'))
                            );
                        }
                    }

                    $QStaffLogDetail->insert($data);

//                    if ($key == My_Staff_Info_Type::Title) {
//                        if (in_array($old_value, array(PGPB_TITLE, SALES_TITLE, LEADER_TITLE, PGPB_2_TITLE))) {
//                            My_Staff::removeStoreForTransfer($object, $old_value, date('Y-m-d', $time), $new_title);
//                        }
//                    }
                }//End foreach

                if ($checkDuplicate == true) {
                    exit(
                            json_encode(array('status' => 0, 'message' => 'New transfer must be different: ' . $message_log))
                    );
                }

                //data staff table and log
                $dataStaff = array(
                    'company_id'      => $info[My_Staff_Info_Type::Company],
                    'department'      => $info[My_Staff_Info_Type::Department],
                    'team'            => $info[My_Staff_Info_Type::Team],
                    'regional_market' => $info[My_Staff_Info_Type::Region],
                    'title'           => $info[My_Staff_Info_Type::Title],
                    'group_id'        => $info[My_Staff_Info_Type::Group],
                    'updated_at'      => date('Y-m-d H:i:s'),
                    'updated_by'      => $userStorage->id,
                );
               
                if ($info[My_Staff_Info_Type::Title] == PGPB_TITLE)
                    $dataStaff['has_email'] = 0;

                self::updateStaffTable($dataStaff, $staff_id);
//                self::transfer_to_contract($transfer_id);
                // check gan shop sale, pg, storeleader, pg leader
                $QstoreStaffLog         = new Application_Model_StoreStaffLog();
                
                $result_store_staff_log = $QstoreStaffLog->getStoreByStaffAndTitle($staff_id, $currentStaffInfo['title'], $info[My_Staff_Info_Type::Title], $currentStaffInfo['region'],$info[My_Staff_Info_Type::Region]);
                if (!empty($result_store_staff_log)) {
                    throw new Exception("Nhân viên này đang được gán shop với thông tin cũ.Vui lòng gỡ shop của nhân viên trước khi transfer");
                }
                // end check gan shop sale, pg, storeleader, pg leader
                // check gan shop sale leader
                $QstoreLeaderLog         = new Application_Model_StoreLeaderLog();
                $result_store_leader_log = $QstoreLeaderLog->getStoreByStaffAndTitle($staff_id, $currentStaffInfo['title'], $info[My_Staff_Info_Type::Title], $currentStaffInfo['region'],$info[My_Staff_Info_Type::Region]);
                if (!empty($result_store_leader_log)) {
                    throw new Exception("Nhân viên này đang được gán shop với thông tin leader cũ.Vui lòng gỡ shop leader của nhân viên trước khi transfer");
                }
                // end check gan shop sale leader
                $QStaffContract = new Application_Model_StaffContract();
                $check_contract = $QStaffContract->checkTransferToContract($transfer_id);
                if (!empty($check_contract)) {
                    throw new Exception("Tranfer này đã phát sinh hợp đồng, vui lòng rollback lại các hợp đồng phát sinh từ transfer này.");
                }
                $result_transfer = My_Staff_Transfer::transfer_to_contract($transfer_id);
                if (!$result_transfer['status']) {
                    throw new Exception($result_transfer['message']);
                }
            }
            if(!empty($transfer_tmp_id)) {
                $data_tmp = array(
                 'finish'       => 1
                );
                $where_tmp            = $QStaffTransferTmp->getAdapter()->quoteInto('id = ?', $transfer_tmp_id);
                $QStaffTransferTmp->update($data_tmp, $where_tmp);
            }
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            exit(
                    json_encode(array('status' => 0, 'message' => $e->getMessage()))
            );
        }
        return true;
    }

    /**
     * Check xem thời gian transfer có bị chồng lấn hay không
     * @param  [type] $staff_id [description]
     * @param  [type] $type     [description]
     * @param  [type] $from     [description]
     * @return [type]           [description]
     */
    private static function _check_transfer_time($staff_id, $type, $from) {
        $QStaffLogDetail = new Application_Model_StaffLogDetail();
        $where           = array();
        $where[]         = $QStaffLogDetail->getAdapter()->quoteInto('object = ?', $staff_id);
        $where[]         = $QStaffLogDetail->getAdapter()->quoteInto('info_type = ?', $type);
        $where[]         = $QStaffLogDetail->getAdapter()->quoteInto('from_date <= ?', $from);
        $where[]         = $QStaffLogDetail->getAdapter()->quoteInto('to_date IS NOT NULL AND to_date > ?', $from);
        $log             = $QStaffLogDetail->fetchRow($where);
        if ($log)
            return false;
        return true;
    }

    private static function updateStaffTable($dataStaff, $staff_id) {
        $QStaff = new Application_Model_Staff();

        //get old data
        $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
        $s          = $QStaff->fetchRow($whereStaff);
        $QStaff->update($dataStaff, $whereStaff);
        //get new data;
        $s_after    = $QStaff->fetchRow($whereStaff);

        // check and add staff to dashboard
        My_Dashboard_Staff::checkDashboardCondition($s_after);

        Log::w($s->toArray(), $s_after->toArray(), $staff_id, LogGroup::Staff, LogType::Update);
    }

    public static function delete($transfer_id) {
        if (!$transfer_id) {
            $arrResult = array(
                'code'    => 1,
                'message' => 'Error: Please select transfer to delete'
            );
            return $arrResult;
        }
        $db              = Zend_Registry::get('db');
        $QStaffTransfer  = new Application_Model_StaffTransfer();
        $QStaffLogDetail = new Application_Model_StaffLogDetail();
        $db->beginTransaction();
        try {
            $QStaffContract = new Application_Model_StaffContract();
            $check_contract = $QStaffContract->checkTransferToContract($transfer_id);
            if (!empty($check_contract)) {
                throw new Exception("Tranfer này đã phát sinh hợp đồng, vui lòng rollback lại các hợp đồng phát sinh từ transfer này.");
            }
            $row           = $QStaffTransfer->find($transfer_id)->current();
            $staff_id      = $row->staff_id;
            $row->delete();
            $whereStaffLog = $QStaffLogDetail->getAdapter()->quoteInto('transfer_id = ?', $transfer_id);
            $QStaffLogDetail->delete($whereStaffLog);

            //update to_date NULL
            $select = $QStaffTransfer->select()
                    ->where('staff_id = ?', $staff_id)
                    ->order('created_at DESC')
                    ->limit(1);
            $row    = $QStaffTransfer->fetchRow($select);
            if ($row) {
                $row->to_date = NULL;
                $row->save();
            }
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            $arrResult = array(
                'code'    => 1,
                'message' => $e->getMessage()
            );
            return $arrResult;
        }
        $arrResult = array(
            'code'    => 2,
            'message' => 'Done'
        );
        return $arrResult;
    }

    public static function transfer_to_contract($transfer_id) {
        $transfer_id  = intval($transfer_id);
        $db           = Zend_Registry::get('db');
        $QAsmContract = new Application_Model_AsmContract();
        $QStaff       = new Application_Model_Staff();
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();

        $arr_pg          = My_Staff_Title::getPg();
        $arr_sale        = array(183, 190, 162, 164);
        $arr_none_office = array(182, 183, 190, 162, 164, 179, 181, 293, 312);
        $arr_pb_sale     = My_Staff_Title::getPbSale();

        $auto_note  = 'INSERT FROM TRANSFER';
        $created_at = date('Y-m-d H:i:s');
        $created_by = $userStorage->id;

        $flg_update_staff = 0;
        $add_contract_2   = 0;

        //transfer data
        $select_transfer = $db->select()
                ->from(array('a' => 'v_staff_transfer'), array('*'))
                ->where('a.transfer_id = ?', $transfer_id);
        $row_transfer    = $db->fetchRow($select_transfer);

        //Không xét những trường hợp tạo transfer đầu tiên
        if (!$row_transfer['old_title']) {
            return null;
        }

        $staff_id = $row_transfer['staff_id'];

        try {

            //HD HIỆN TẠI
            $select_current_contract = $db->select()
                    ->from(array('a' => 'asm_contract'), array('a.*'))
                    ->where('a.print_type = ?', 1)
                    ->where('a.locked IS NULL OR a.locked = ?', 0)
                    ->where('a.staff_id = ?', $staff_id)
                    ->order(array('a.from_date DESC', 'a.id DESC'))
                    ->limit(1);
            $current_contract        = $db->fetchRow($select_current_contract);

            if (empty($current_contract)) {
                return null;
            }

            //HD MỚI
            $contractTerm  = new Application_Model_ContractTerm();
            $next_contract = $contractTerm->get_next_contract($current_contract['new_contract']);

            // tạo HD mới nếu HĐ đã hết hạn và transfer trước ngày xác nhận HD mới
            if (strtotime($row_transfer['from_date']) > strtotime($current_contract['to_date'])) {

                $flg_update_staff = 1;

                //kiểm tra hd nếu có phụ lục
                $select_sub_contract = $db->select()
                        ->from(array('a' => 'asm_contract'), array('*'))
                        ->where('print_type = ?', 2)
                        ->where('parent_id = ?', $current_contract['id'])
                        ->order('to_date DESC')
                        ->limit(1)
                ;
                $sub_contract        = $db->fetchRow($select_sub_contract);

                $salary       = 0;
                $total_salary = 0;
                $allowance_1  = NULL;
                $allowance_2  = NULL;
                $allowance_3  = NULL;
                $work_cost    = NULL;

                if ($sub_contract) {
                    $salary       = $sub_contract['salary'];
                    $total_salary = $sub_contract['total_salary'];
                    $allowance_1  = $sub_contract['allowance_1'];
                    $allowance_2  = $sub_contract['allowance_2'];
                    $allowance_3  = $sub_contract['allowance_3'];
                    $work_cost    = $sub_contract['work_cost'];
                }


                $from_date = self::get_add_day($current_contract['title'], $next_contract['level'], $current_contract['new_contract'], 1, $current_contract['to_date'], 1, $next_contract['nadd_day']);
                $to_date   = self::get_add_day($current_contract['title'], $next_contract['level'], $current_contract['new_contract'], 1, $current_contract['to_date'], 2, $next_contract['nadd_day']);

                $data_new = array(
                    'staff_id'        => $current_contract['staff_id'],
                    'new_contract'    => $next_contract['nid'],
                    'from_date'       => $from_date,
                    'to_date'         => $to_date,
                    'hospital_id'     => $current_contract['hospital_id'],
                    'status'          => 0,
                    'title'           => $current_contract['title'],
                    'regional_market' => $current_contract['regional_market'],
                    'print_type'      => 1,
                    'company_id'      => $current_contract['company_id'],
                    'auto_note'       => $auto_note,
                    'salary'          => $salary,
                    'total_salary'    => $total_salary,
                    'work_cost'       => $work_cost,
                    'allowance_1'     => $allowance_1,
                    'allowance_2'     => $allowance_2,
                    'allowance_3'     => $allowance_3,
                );

                // fix salary leader
                if (in_array($data_new['title'], array(LEADER_TITLE))) {
                    $slr                      = $db->query('SELECT * FROM salary_leader WHERE province_id = ' . $data_new['regional_market'])->fetch();
                    $data_new['salary']       = $slr['base_salary'];
                    $data_new['total_salary'] = $slr['bonus_salary'];
                    $data_new['kpi_text']     = $slr['kpi'];
                }

                $QAsmContract->insert($data_new);

                // Update HĐ cũ
                $where       = $QAsmContract->getAdapter()->quoteInto('id = ?', $current_contract['id']);
                $data_update = array(
                    'status'    => 2, 'auto_note' => $current_contract['auto_note'] . ';' . date('Y-m-d') . ' update status = 1');
                $QAsmContract->update($data_update, $where);

                //Lấy lại hd hiện tại
                $select_current_contract = $db->select()
                        ->from(array('a' => 'asm_contract'), array('a.*'))
                        ->where('a.print_type = ?', 1)
                        ->where('a.locked IS NULL OR a.locked = ?', 0)
                        ->where('a.staff_id = ?', $staff_id)
                        ->order(array('a.from_date DESC', 'a.id DESC'))
                        ->limit(1);
                $current_contract        = $db->fetchRow($select_current_contract);
            }

            $sql = "
            SELECT  a.old_title, a.title, a.old_company_id, a.company_id, e.`level`, a.from_date, h.new_contract,h.from_date, h.to_date,
                    a.regional_market, a.old_regional_market,
                    CASE WHEN k.base_salary IS NOT NULL THEN k.base_salary WHEN b.base_salary IS NOT NULL THEN b.base_salary WHEN c.base_salary IS NOT NULL THEN c.base_salary ELSE 0 END  AS 'kv_base_salary',
                    CASE WHEN k.base_salary IS NOT NULL THEN k.bonus_salary WHEN b.base_salary IS NOT NULL THEN b.bonus_salary WHEN c.base_salary IS NOT NULL THEN c.bonus_salary ELSE 0 END as 'kv_bonus_salary',
                    CASE WHEN k.base_salary IS NOT NULL THEN k.allowance_1 WHEN b.base_salary IS NOT NULL THEN b.allowance_1 WHEN c.base_salary IS NOT NULL THEN c.allowance_1 ELSE 0 END as 'kv_allowance_1',
                    CASE WHEN k.base_salary IS NOT NULL THEN k.allowance_2 WHEN b.base_salary IS NOT NULL THEN b.allowance_2 WHEN c.base_salary IS NOT NULL THEN c.allowance_2 ELSE 0 END as 'kv_allowance_2',
                    CASE WHEN k.base_salary IS NOT NULL THEN k.allowance_3 WHEN b.base_salary IS NOT NULL THEN b.allowance_3 WHEN c.base_salary IS NOT NULL THEN c.allowance_3 ELSE 0 END as 'kv_allowance_3',
                    h.salary, h.total_salary, h.allowance_1, h.allowance_2, h.allowance_3,h.hospital_id
            FROM v_staff_transfer AS a
            LEFT JOIN salary_pg b ON a.title IN (" . implode(',', $arr_pg) . ") AND a.regional_market = b.province_id
            LEFT JOIN salary_sales c ON a.title IN (" . implode(',', $arr_sale) . ") AND a.regional_market = c.province_id
            LEFT JOIN staff d ON a.staff_id = d.id
            LEFT JOIN contract_term e ON d.contract_term = e.id
            LEFT JOIN regional_market f ON a.regional_market = f.id
            LEFT JOIN regional_market g ON a.old_regional_market = g.id
            INNER JOIN asm_contract h ON h.staff_id = a.staff_id
            LEFT JOIN salary_log k ON k.province_id = a.regional_market AND  a.title IN (" . implode(',', $arr_pb_sale) . ")
            WHERE ( a.title != a.old_title OR f.province_id != g.province_id OR a.company_id != a.old_company_id )
                AND a.transfer_id = " . $transfer_id . " AND h.id = " . $current_contract['id'];

            $stmt   = $db->query($sql);
            $result = $stmt->fetch();

            if (!$result) {
                return 1;
            }

            if (in_array($result['old_title'], $arr_none_office) AND ! in_array($result['title'], $arr_none_office)) {
                if ($result['level'] == 1 OR $result['old_company_id'] != $result['company_id']) {
                    //Nếu ở level 1 mà khác công ty thì tạo HD mới
                    $new_contract     = 2;
                    $print_type       = 1;
                    $status           = 0;
                    $to_date          = My_Date::date_add($row_transfer['from_date'], 59);
                    $flg_update_staff = 1;
                } elseif ($result['level'] != 1 OR $result['old_company_id'] == $result['company_id']) {
                    //Nếu cùng công ty mà level cao hơn thì tạo phụ lục thử việc và phụ lục tg còn lại
                    $new_contract   = $result['new_contract'];
                    $to_date        = My_Date::date_add($row_transfer['from_date'], 59);
                    $print_type     = 2;
                    $status         = 2;
                    $add_contract_2 = 1;
                } else {
                    //còn lại thì tạo 1 phụ lục HĐ thử việc và 1 HD còn lại
                    $new_contract   = $result['new_contract'];
                    $to_date        = My_Date::date_add($row_transfer['from_date'], 59);
                    $print_type     = 2;
                    $status         = 2;
                    $add_contract_2 = 1;
                }
            } elseif (in_array($result['old_title'], $arr_none_office) AND in_array($result['title'], $arr_none_office)) {

                if (!in_array($result['old_title'], $arr_pg) AND in_array($result['title'], $arr_pg) AND $result['old_title'] != $result['title']) {
                    $new_contract     = 3;
                    $to_date          = My_Date::date_add($row_transfer['from_date'], 84);
                    $print_type       = 1;
                    $status           = 0;
                    $flg_update_staff = 1;
                } elseif (!in_array($result['old_title'], $arr_pb_sale) AND in_array($result['title'], $arr_pb_sale) AND $result['old_title'] != $result['title']) {
                    //pb sale
                    $new_contract     = $result['new_contract'];
                    $to_date          = My_Date::date_add($row_transfer['from_date'], 54);
                    $print_type       = 1;
                    $status           = 0;
                    $flg_update_staff = 1;
                } elseif ($result['old_title'] != $result['title'] AND $result['level'] == 1) {
                    $new_contract     = 2;
                    $to_date          = My_Date::date_add($row_transfer['from_date'], 59);
                    $print_type       = 1;
                    $status           = 0;
                    $flg_update_staff = 1;
                } else {
                    // còn lại thường do chuyển vùng
                    $new_contract = $result['new_contract'];
                    $to_date      = $result['to_date'];
                    $print_type   = 2;
                    $status       = 2;
                }
            } elseif ($result['old_regional_market'] != $result['old_regional_market']) {
                $new_contract = $result['new_contract'];
                $to_date      = $result['to_date'];
                $print_type   = 2;
                $status       = 2;
            } else {
                $new_contract = $result['new_contract'];
                $to_date      = $result['to_date'];
                $print_type   = 2;
                $status       = 2;
            }

            //Mặc định phụ lục đã xác nhận, đối với phục lục ko có ảnh hưởng nhìu
            $parent_id = NULL;
            if ($print_type == 2) {
                $status    = 2;
                $parent_id = $current_contract['id'];
            }

            //nếu transfer thì lấy lương của khu vực hiện tại, ( có thể điều chỉnh lại sau theo yêu cầu của HR )
            $salary        = intval($result['kv_base_salary']);
            $data_contract = array(
                'staff_id'        => $staff_id,
                'new_contract'    => $new_contract,
                'from_date'       => $row_transfer['from_date'],
                'to_date'         => $to_date,
                'status'          => $status,
                'print_type'      => $print_type,
                'salary'          => $salary,
                'created_at'      => $created_at,
                'created_by'      => $created_by,
                'regional_market' => $result['regional_market'],
                'title'           => $result['title'],
                'hospital_id'     => $result['hospital_id'],
                'auto_note'       => $auto_note,
                'company_id'      => $result['company_id'],
                'parent_id'       => $parent_id
            );

            // fix salary leader
            if (in_array($data_contract['title'], array(LEADER_TITLE))) {
                $slr                           = $db->query('SELECT * FROM salary_leader WHERE province_id = ' . $data_contract['regional_market'])->fetch();
                $data_contract['salary']       = $slr['base_salary'];
                $data_contract['total_salary'] = $slr['bonus_salary'];
                $data_contract['kpi_text']     = $slr['kpi'];
            }

            $QAsmContract->insert($data_contract);

            if ($add_contract_2 == 1 AND strtotime($to_date) < strtotime($result['to_date'])) {
                $data_contract = array(
                    'staff_id'        => $staff_id,
                    'new_contract'    => $new_contract,
                    'from_date'       => My_Date::date_add($to_date, 1),
                    'to_date'         => $result['to_date'],
                    'status'          => $status,
                    'print_type'      => $print_type,
                    'salary'          => $salary,
                    'created_at'      => $created_at,
                    'created_by'      => $created_by,
                    'regional_market' => $result['regional_market'],
                    'title'           => $result['title'],
                    'hospital_id'     => $result['hospital_id'],
                    'auto_note'       => $auto_note,
                    'company_id'      => $result['company_id'],
                    'parent_id'       => $parent_id
                );

                // fix salary leader
                if (in_array($data_contract['title'], array(LEADER_TITLE))) {
                    $slr                           = $db->query('SELECT * FROM salary_leader WHERE province_id = ' . $data_contract['regional_market'])->fetch();
                    $data_contract['salary']       = $slr['base_salary'];
                    $data_contract['total_salary'] = $slr['bonus_salary'];
                    $data_contract['kpi_text']     = $slr['kpi'];
                }

                $QAsmContract->insert($data_contract);
            }

            if ($flg_update_staff == 1) {
                $data_staff = array(
                    'contract_term'       => $new_contract,
                    'contract_signed_at'  => $data_contract['from_date'],
                    'contract_expired_at' => $data_contract['to_date']
                );
                $where      = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $QStaff->update($data_staff, $where);
            }

            //Lấy lại hd hiện tại
            $select_current_contract = $db->select()
                    ->from(array('a' => 'asm_contract'), array('a.*'))
                    ->where('a.print_type = ?', 1)
                    ->where('a.locked IS NULL OR a.locked = ?', 0)
                    ->where('a.staff_id = ?', $staff_id)
                    ->order(array('a.from_date DESC', 'a.id DESC'))
                    ->limit(1);
            $current_contract        = $db->fetchRow($select_current_contract);

            //update hd cũ đã được xác nhận
            if ($current_contract) {
                $sql  = "UPDATE asm_contract
                SET `status` = 2
                WHERE print_type = 1 and locked = 0 AND IFNULL(`status`,0) <= 1 AND id != ? AND staff_id = ?";
                $stmt = $db->query($sql, array($current_contract['id'], $staff_id));
                $stmt->execute();
            }

            return 1;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public static function get_add_day($title, $level, $contract_id, $asm_proposal, $to_date, $type, $nadd_day) {
        $arr_pg      = My_Staff_Title::getPg();
        $arr_sale    = array(183, 190, 162, 164);
        $arr_pb_sale = My_Staff_Title::getPbSale();
        $num_add     = 0;
        if ($type == 1) {
            if (in_array($title, $arr_pg) and $level == 1) {
                $num_add = 5;
            } elseif (in_array($title, $arr_sale) AND $contract_id == 2 AND $asm_proposal == 3) {
                $num_add = 5;
            } elseif (in_array($title, $arr_sale) AND $contract_id == 4 AND $asm_proposal == 1) {
                $num_add = 5;
            } elseif (in_array($title, $arr_pb_sale) AND $level == 1) {
                $num_add = 5;
            } else {
                $num_add = 1;
            }
        } else {
            if (in_array($title, $arr_pg) and $level == 1 and $asm_proposal == 1) {
                $num_add = 5 + $nadd_day;
            } elseif (in_array($title, $arr_pg) and $level == 1 and $asm_proposal == 3) {
                $num_add = 34;
            } elseif (in_array($title, $arr_sale) AND $contract_id == 2 AND $asm_proposal == 3) {
                $num_add = 34;
            } elseif (in_array($title, $arr_sale) AND $contract_id == 4 AND $asm_proposal == 1) {
                $num_add = 5 + $nadd_day;
            } elseif (in_array($title, $arr_pb_sale) AND $level == 1) {
                $num_add = 5 + 54;
            } else {
                if ($asm_proposal == 3) {
                    $num_add = 29;
                } else {
                    $num_add = 1 + $nadd_day;
                }
            }
        }

        if (!$nadd_day) {
            return null;
        }

        $date = new DateTime($to_date);
        $date->add(new DateInterval('P' . $num_add . 'D'));
        return $date->format('Y-m-d');
    }

    public static function mass_upload($data) {
        if (!is_array($data) || count($data) == 0) {
            return array('code' => 1, 'message' => 'required data');
        }
    }

}
