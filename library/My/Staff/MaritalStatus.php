<?php
/**
* @author buu.pham
*/
class My_Staff_MaritalStatus extends My_Type_Enum
{
    const Single = 1;
    const Married = 2;
    const Divorced = 3;
}