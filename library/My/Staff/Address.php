<?php
/**
* @author buu.pham
*/
class My_Staff_Address extends My_Type_Enum
{
    const Birth_Certificate = 1;
    const Temporary = 2;
    const Permanent = 3;
    const ID_Card = 4;
}