<?php 
/**
* @author buu.pham
*/
class My_Staff_Title
{
    
    private static $_sales_title_list = array(
        PGPB_TITLE,
        SALES_TITLE,
        LEADER_TITLE,
        CHUYEN_VIEN_BAN_HANG_TITLE,
        PB_SALES_TITLE,
        SALES_TRAINEE_TITLE,
    );

    private static $_trainee = array(SALES_TRAINEE_TITLE);

    private static $_pg_title      = array(PGPB_TITLE, PGPB_2_TITLE,PG_BRANDSHOP,SENIOR_PROMOTER_BRANDSHOP);
    
    private static $_sale_title    = array(SALES_TITLE);
    
    private static $_leader_title  = array(LEADER_TITLE);
    
    private static $_pb_sale_title = array(PB_SALES_TITLE);

    public static function getSalesman()
    {
        return self::$_sales_title_list;
    }

    public static function isPg($title = null)
    {
        if (is_null($title)) return false;

        return in_array($title, self::getPg());
    }

    public static function getPg()
    {
        return self::$_pg_title;
    }

    public static function getSale()
    {
        return self::$_sale_title;
    }

    public static function getLeader(){
        return self::$_leader_title;
    }

    public static function getPbSale()
    {
        return self::$_pb_sale_title;
    }

    public static function getTrainee()
    {
        return self::$_trainee;
    }
}
