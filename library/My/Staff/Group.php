<?php
/**
* @author buu.pham
*/
class My_Staff_Group extends My_Type_Enum
{
    const ASM         = 5;
    const ASM_STANDBY = 16;
    const LEADER      = 14;
    const SALES       = 9;
    const PG          = 4;
    const SALES_ADMIN = 12;
    const TRAINING    = 17;
    const BOARD       = 8;
    const ACCESSORIES = 18;
	const MODULE_SALE = 28;
    const TRAINING_LEADER = TRAINING_LEADER_ID;

    public static $allow_in_area_view = array(
        My_Staff_Group::ASM,
        My_Staff_Group::ASM_STANDBY,
        My_Staff_Group::SALES_ADMIN,
        My_Staff_Group::TRAINING,
        My_Staff_Group::BOARD,
        My_Staff_Group::ACCESSORIES,
        My_Staff_Group::TRAINING_LEADER,
		My_Staff_Group::MODULE_SALE,
    );

    public static $training_allow_area_view = array(
        My_Staff_Group::TRAINING,
        My_Staff_Group::TRAINING_LEADER,
        My_Staff_Group::BOARD,
    );

    public static $targetName = array(
        self::PG => 'PG',
        self::SALES => 'Sale',
    );
}