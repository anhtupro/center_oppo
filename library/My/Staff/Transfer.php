<?php

/**
 * @author: hero
 * Transfer Class
 * Các hàm trong qui trinh transfer
 * 27-05-2017
 */
class My_Staff_Transfer {

    public static function transfer_to_contract($transfer_id) {
        $response = array('status' => 0, 'message' => 'Tạo hợp đồng không thành công');
        $db = Zend_Registry::get('db');
        $arr_pg = My_Staff_Title::getPg();
        $arr_none_office = array(182, 183, 190, 162, 164, 293, 312, 295, 730, 732, 732, 424, 417, 731,798, 577);
        // get transfer data
        $select_transfer = $db->select()
                ->from(array('a' => 'v_staff_transfer_fix'), array('*'))
                ->where('a.transfer_id = ?', $transfer_id);
        $row_transfer = $db->fetchRow($select_transfer);

        //Không xét những trường hợp tạo transfer đầu tiên

        if (!$row_transfer['old_title']) {
            return $response = array('status' => 1, 'message' => 'Tạo hợp đồng thành công');
        }
        $staff_id = $row_transfer['staff_id'];

        //save log transfer
        //self::saveLogContract($staff_id);
        // get current contract
        $QStaffSalary = new Application_Model_StaffSalary();
        $change_work_cost = 1;
        try {
            $select_contract = $db->select()
                    ->from(array('p' => 'staff_contract'), array('p.*'))
                    ->where('p.staff_id = ?', $staff_id)
                    ->where('p.is_expired <> 1 and p.is_disable <> 1')
                    ->where('p.is_next =2')
                    ->where('p.from_date <= ?', $row_transfer['from_date'])
                    ->where('(p.to_date >= ?', $row_transfer['from_date'])
                    ->orWhere('p.to_date IS NULL )');
            $row_contract = $db->fetchRow($select_contract);
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            if (!empty($row_contract)) {

                //get current salary
                $where_staff_salary_old = $QStaffSalary->getAdapter()->quoteInto('id = ?', $row_contract['salary_id']);
                $old_salary = $QStaffSalary->fetchRow($where_staff_salary_old);
                // check co giu work cost hay khong
                if (in_array($row_transfer['title'], $arr_pg) && in_array($row_transfer['old_title'], $arr_pg)) {
                    $change_work_cost = 0;
                }
                $QCompanyGroup = new Application_Model_CompanyGroup();
                $old_group = $QCompanyGroup->getGroupParentByTitle($row_transfer['old_title']);
                $new_group = $QCompanyGroup->getGroupParentByTitle($row_transfer['title']);

                $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                $user_id = $userStorage->id;
                /*
                  ContractInsert : Tao hop dong
                  AppendixInsert : phu luc binh thuong
                  AppendixInsertProbation : phu luc thu viec
                 */
                $params = array();

                $isChangeCompany = (!empty($row_transfer['old_company_id']) AND $row_transfer['old_company_id'] != $row_transfer['company_id'] ) ? 1 : 0;
                if ($isChangeCompany) { // khac cong ty
                    // exit(json_encode(array('status' => 0, 'message' => 'Khac cong ty, tao hop dong')));
                    $result = self::contractInsert($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer);

                    if ($result['status'] == 1) {
                        $response['status'] = $result['status'];
                        $response['message'] = $result['Thành Công'];
                    } else {
                        throw new Exception($result['message']);
                    }
                    return $response;
                } else { //Cung cong ty
                    $sameTitle = ($row_transfer['old_title'] == $row_transfer['title'] AND $row_transfer['regional_market'] != $row_transfer['old_regional_market']) ? 1 : 0;
                    if ($sameTitle) {
                        $result = self::AppendixInsert($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer);
                        if ($result['status'] == 1) {
                            $response['status'] = $result['status'];
                            $response['message'] = $result['Thành Công'];
                        } else {
                            throw new Exception($result['message']);
                        }
                    }else {
                         //Local-Local => special case
                        $isProbativeStaff = in_array($row_contract['contract_term'], array(2, 17)) ? 1 : 0;
                        $isGroupLocalCurrent = (self::getGroupByTitle($row_transfer['old_title']) == 3) ? 1 : 0;
                        // tao phu luc
                        if ($isProbativeStaff) { //Hop dong thu viec, dao tao -> tao hop dong
                            $isProbativeStaffAndSpecialCaseTransfer = self::isProbativeStaffAndSpecialCaseTransfer($row_transfer['old_title'], $row_transfer['title']);
                            if ($isProbativeStaffAndSpecialCaseTransfer) {//Truong hop dac biet cua hop dong thu viec, dao tao -> tao phu luc
                                $result = self::AppendixInsert($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer);
                                if ($result['status'] != 1) {
                                    throw new Exception($result['message']);
                                }
                            } else {

                                // exit(json_encode(array('status' => 0, 'message' => 'Cung cong ty,truong hop binh thuong => tao hop dong')));

                                $result = self::contractInsert($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer);
                                if ($result['status'] != 1) {
                                    throw new Exception($result['message']);
                                }
                            }
                            // return $response;
                        } elseif (!$isProbativeStaff) { //Hop dong chinh thuc
                            $groupNewIsLocal = (self::getGroupByTitle($row_transfer['title']) == 3) ? 1 : 0;
                            $caseSpecial = ($row_transfer['title'] == 577) ? 1 : 0; //577 PGs Sup

                            $nextToDate = date('Y-m-d', strtotime("+" . 59 . " days", strtotime($row_transfer['from_date'])));
                            $endDayOfNewContract = date('Y-m-d', strtotime("+" . 1 . " days", strtotime($nextToDate)));

                            if ($isGroupLocalCurrent) {

                                if ($groupNewIsLocal) {

                                    if ($caseSpecial) { //Truong hop dac biet, chuc danh moi PGs Sup -> tao phu luc thu viec
                                        $result = self::contractInsertProbationTwoMonth($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer); //HDTV
                                        $row_contract_new = $db->fetchRow($select_contract);

                                        if ($result['status'] != 1) {
                                            throw new Exception($result['message']);
                                        }
                                        if (!empty($row_contract['to_date']) AND $row_contract['to_date'] < $nextToDate) {
                                            //Sinh hợp đồng tái ký:
                                            $result = self::contractInsertRenewable($staff_id, $row_contract['contract_term']); //Renewable
                                            $result['status'] = 1;
                                            if ($result['status'] != 1) {
                                                throw new Exception($result['message']);
                                            }
                                        } else {
                                            $result = self::AppendixInsert($staff_id, $endDayOfNewContract, $old_salary, $row_contract, $change_work_cost, $row_transfer);
                                            if ($result['status'] != 1) {
                                                throw new Exception($result['message']);
                                            }
                                        }
                                    } else {
                                        $result = self::AppendixInsert($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer);
                                        if ($result['status'] == 1) {
                                            $response['status'] = $result['status'];
                                            $response['message'] = $result['Thành Công'];
                                        } else {
                                            throw new Exception($result['message']);
                                        }
                                    }
                                } else {
                                    $result = self::contractInsertProbationTwoMonth($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer); //HDTV
                                    $row_contract_new = $db->fetchRow($select_contract);
                                    if ($result['status'] != 1) {
                                        throw new Exception($result['message']);
                                    }
                                    if (!empty($row_contract['to_date']) AND $row_contract['to_date'] < $nextToDate) {
                                        //Sinh hợp đồng tái ký:
                                        $result = self::contractInsertRenewable($staff_id, $row_contract['contract_term']); //Renewable
                                        $result['status'] = 1;
                                        if ($result['status'] != 1) {
                                            throw new Exception($result['message']);
                                        }
                                    } else {
                                        $result = self::AppendixInsert($staff_id, $endDayOfNewContract, $old_salary, $row_contract, $change_work_cost, $row_transfer);
                                        if ($result['status'] != 1) {
                                            throw new Exception($result['message']);
                                        }
                                    }
                                }
                            } else { //va group khac local -> PL binh thuong
                                // exit(json_encode(array('status' => 0, 'message' => 'Cung cong ty,Hop dong chinh thuc,title khac local -> tao phu luc')));
                                $result = self::AppendixInsert($staff_id, $row_transfer['from_date'], $old_salary, $row_contract, $change_work_cost, $row_transfer);
                                if ($result['status'] == 1) {
                                    $response['status'] = $result['status'];
                                    $response['message'] = $result['Thành Công'];
                                } else {
                                    throw new Exception($result['message']);
                                }
                            }
                        }
                    }
                    if ($result['status'] == 1) {
                        $response['status'] = $result['status'];
                        $response['message'] = $result['Thành Công'];
                    } else {
                        throw new Exception($result['message']);
                    }
                }
            } else {
                throw new Exception('Không có hợp đồng hiện tại');
            }
        } catch (Exception $ex) {
            // $db->rollback();
            $response['status'] = 0;
            $response['message'] = $ex->getMessage();
        }

        return $response;
    }

    public function checkConfirmDistrict($current_transfer) {
        $QStoreStaffLog = new Application_Model_StoreStaffLog();
        if (in_array($current_transfer['title'], array_diff(TITLE_ASSIGN_SHOP, [SALES_TITLE,PGS_SUPERVISOR,LEADER_TITLE])) && ($current_transfer['regional_market'] != $current_transfer['old_regional_market'] || $current_transfer['title'] != $current_transfer['old_title'] || $QStoreStaffLog->checkReleasedStoreByStaffId($current_transfer['staff_id']) == 1)) {
            return 1;
        }
        return 0;
    }

    public static function AppendixInsert($staff_id, $from_date, $old_salary, $current_contract_array, $change_work_cost, $current_transfer) {

        $QStaff = new Application_Model_Staff();
        $QStaffContract = new Application_Model_StaffContract();
        $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
        $QStaffSalary = new Application_Model_StaffSalary();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QRegional_market = new Application_Model_RegionalMarket;
        $db = Zend_Registry::get('db');
        $arr_pg = My_Staff_Title::getPg();
        $arr_sale = array(183, 190, 162, 164);
        $arr_none_office = array(182, 183, 190, 162, 164, 293, 312, 295, 730, 732, 732, 424, 417, 731,798, 577);
        $created_at = date('Y-m-d H:i:s');
        $created_by = $userStorage->id;
        $response = array('status' => 0, 'message' => 'Lỗi tạo phụ lục');
        try {
            $where_staff = array();
            $where_staff[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $Staff_Row = $QStaff->fetchRow($where_staff);
            // tat ca cac truong hop deu phai update luong cu
            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('to_date is NULL', null);
            $data_update_salary = array('to_date' => $from_date);
            $QStaffSalary->update($data_update_salary, $where_staff_salary);


            $work_cost = 0;
            $default_district = 0;

            //- Kế thừa thông tin hợp đồng, không cập nhật lại quận huyện khi: Transfer trong cùng tỉnh + (cùng chức danh/từ PGPB => Senior Promoter)
            if ($current_transfer['regional_market'] == $current_transfer['old_regional_market'] && ($current_transfer['title'] == $current_transfer['old_title'] || (($current_transfer['old_title'] == 182 && $current_transfer['title'] == 293) || ($current_transfer['old_title'] == 730 && $current_transfer['title'] == 732)))) {
                $default_district = $Staff_Row['district'];
            } else { //($title,$region_market,$staff_id)
                $default_district = self::getDistrictId($current_transfer['title'], $current_transfer['regional_market'], $staff_id, $current_transfer['area']);

                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $data_update = array(
                    "district" => $default_district
                );
                $QStaff->update($data_update, $where_staff);
            }
            if (empty($default_district) || $default_district == -1 || $default_district == 0) {
                throw new Exception('Kiểm tra lại quận huyện');
            }

            if (!$change_work_cost) {
                // neu ko change work cost thi lay work cost cu
                $work_cost = $old_salary['work_cost_salary'];
            }
            if (in_array($current_transfer['title'], array(182, 293, 730, 732))) {
                // new transfer ve PG va SNP thi tim lai work cost cu
                $max_wk = $QStaffSalary->getRecentWorkCost($staff_id);

                if (!empty($max_wk)) {
                    $work_cost = $max_wk['work_cost_salary'];
                }
            }
            if (in_array($current_transfer['title'], $arr_none_office)) {
                // luong moi theo chinh sach 
                if (in_array($current_transfer['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER  (chính sách PG)
                    $salary_policy_data = $QStaffContract->getSalaryDetailByTitleDistrict(182, $default_district);

                    if (empty($salary_policy_data)) {
                        throw new Exception('Không tìm thấy chính sách lương PG cho nhân viên này');
                    }
                    $salary_policy_filter = $salary_policy_data['salary_policy_id'];
                    $PolicyDetail_Row = $salary_policy_data;
                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);



                    if (in_array($salary_policy_filter, array(5))) {
                        $allowance = $PolicyDetail_Row['allowance'] + $work_cost;
                        $salary_kpi = $salary_basic;
                        $salary_total = $salary_basic + $work_cost;
                    } else {
                        if ($salary_insurance_salary >= ($salary_basic + $work_cost)) {
                            $allowance = 0;
                        } else {
                            $allowance = $salary_basic + $work_cost - $salary_insurance_salary;
                        }
                        if (($salary_basic + $work_cost) >= $salary_insurance_salary) {
                            $salary_kpi = 0;
                        } else {
                            $salary_kpi = $salary_insurance_salary - $salary_basic - $work_cost;
                        }
                        $salary_total = $salary_insurance_salary + $allowance;
                    }
                } elseif (in_array($current_transfer['title'], array(183, 164, 162))) { //Sale, Sale Access, Sale Leader Access ( chính sách sale)
                    $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(183, $default_district);

                    $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;
                    if (!$policy_detail_id) {
                        throw new Exception('Không tìm thấy chính sách lương SALE cho nhân viên này');
                    }

                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                    $salary_kpi = intval($PolicyDetail_Row['salary_kpi']);
                    $allowance = intval($PolicyDetail_Row['allowance']);
                    $salary_total = $PolicyDetail_Row['total_company_salary'];
                } elseif (in_array($current_transfer['title'], array(190))) { //Sale leader (chính sách sale leader)
                    $salary_insurance_salary = 6000000;
                    $salary_kpi = 6000000;
                    $salary_basic = 6000000;
                    $allowance = 0;
                    $salary_total = 6000000;
                } elseif (in_array($current_transfer['title'], array(417))) { //PG leader (chính sách PG leader)
                    $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict($current_transfer['title'], $default_district);
                    $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;
                    if (!$policy_detail_id) {
                        throw new Exception('Không tìm thấy chính sách lương PG LEADER cho nhân viên này');
                    }

                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                    $salary_kpi = intval($PolicyDetail_Row['salary_kpi']);
                    $allowance = intval($PolicyDetail_Row['allowance']);
                    $salary_total = $PolicyDetail_Row['total_company_salary'];
                } elseif (in_array($current_transfer['title'], array(732, 731,798, 577 ,725, 799))) {

                    $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict($current_transfer['title'], $default_district);

                    $policy_detail_id = intval($PolicyDetail_Row['id']);
                    if (!$policy_detail_id) {
                        throw new Exception('Không tìm thấy chính sách lương STORE LEADER cho nhân viên này');
                    }

                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                    $salary_kpi = intval($PolicyDetail_Row['salary_kpi']);
                    $allowance = intval($PolicyDetail_Row['allowance']);
                    $salary_total = $PolicyDetail_Row['total_company_salary'];
                } elseif (in_array($current_transfer['title'], array(424))) { // sale leader BS
                    $where_PolicyDetail = array();
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(424));
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $current_transfer['regional_market']);
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                    $PolicyDetail_Row = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                    $user_id = $userStorage->id;

                    $policy_detail_id = intval($PolicyDetail_Row['id']);
                    if (!$policy_detail_id) {
                        throw new Exception('Không tìm thấy chính sách lương SALE LEADER cho nhân viên này');
                    }
                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                    $salary_kpi = intval($PolicyDetail_Row['salary_kpi']);
                    $allowance = intval($PolicyDetail_Row['allowance']);
                    $salary_total = $PolicyDetail_Row['total_company_salary'];
                }
                //tao luong moi
                $data_salary = array(
                    'staff_id' => $staff_id,
                    'insurance_salary' => $salary_insurance_salary,
                    'work_cost_salary' => $work_cost,
                    'from_date' => $from_date,
                    'note' => 'create from transfer id ' . $current_transfer['transfer_id'],
                    'created_at' => $created_at,
                    'created_by' => $created_by
                );

                $salary_id = $QStaffSalary->insert($data_salary);
            } else {
                // luong khoi none sale cuar Thi
                //  luong moi theo luong cu
                //tao luong moi
                $max_salary = $QStaffSalary->getMaxInsuranceSalary($staff_id);
                $data_salary = array(
                    'staff_id' => $staff_id,
                    'insurance_salary' => $max_salary['max_insurance_salary'],
                    'work_cost_salary' => $work_cost,
                    'from_date' => $from_date,
                    'note' => 'create from transfer id' . $current_transfer['transfer_id'],
                    'created_at' => $created_at,
                    'created_by' => $created_by
                );
                $salary_id = $QStaffSalary->insert($data_salary);

                $salary_kpi = 0;
                $salary_basic = 0;
                $allowance = 0;
                $salary_total = $salary_basic + $allowance;
            }
            // tao phu luc 
            //
            $where_append = array();
            $where_append[] = $QStaffContract->getAdapter()->quoteInto('print_type = ?', 2);
            $where_append[] = $QStaffContract->getAdapter()->quoteInto('print_status = ?', 0);
            $where_append[] = $QStaffContract->getAdapter()->quoteInto('transfer_id = ?', $current_transfer['transfer_id']);

            $append_data_update = array(
                'is_disable' => 1,
                'print_status' => 1,
                'note' => 'disable by update transfer,update by :' . $created_by . ' at: ' . $created_at
            );
            $QStaffContract->update($append_data_update, $where_append);
            $data_appendix = array(
                'staff_id' => $staff_id,
                'from_date' => $from_date,
                'contract_term' => $current_contract_array['contract_term'],
                'to_date' => $current_contract_array['to_date'],
                'status' => 2,
                'print_type' => 2,
                'created_at' => $created_at,
                'created_by' => $created_by,
                'regional_market' => $current_transfer['regional_market'],
                'title' => $current_transfer['title'],
                'hospital_id' => $current_contract_array['hospital_id'],
                'system_note' => 'create from transfer id ' . $current_transfer['transfer_id'],
                'company_id' => $current_transfer['company_id'],
                'parent_id' => $current_contract_array['id'],
                'salary_total' => $salary_total,
                'salary_kpi' => $salary_kpi,
                'salary_bonus' => $allowance,
                'salary_id' => $salary_id,
                'transfer_id' => $current_transfer['transfer_id'],
                'salary_policy_detail_id' => $policy_detail_id,
            );
            if (!empty($PolicyDetail_Row)) {
                $data_appendix['salary_policy_detail_id'] = $PolicyDetail_Row['id'];
            }
            $data_appendix['district_id'] = $default_district;

            if (self::checkConfirmDistrict($current_transfer) == 1) {
                $data_appendix['district_confirm'] = 0;
            }

            $QStaffContract->insert($data_appendix);
            $response['status'] = 1;
            $response['message'] = "Thành công";
            //update next contract
            $QStaffContract->UpdateNextContractByStaff($staff_id);
        } catch (Exception $ex) {

            $response['message'] = $ex->getMessage();
        }
        return $response;
    }

    public static function contractInsert($staff_id, $from_date, $old_salary, $current_contract_array, $change_work_cost, $current_transfer) {
        $QStaff = new Application_Model_Staff();
        $QStaffContract = new Application_Model_StaffContract();
        $QContractType = new Application_Model_ContractTypes();
        $QContractProcess = new Application_Model_ContractProcess();
        $QTeam = new Application_Model_Team();
        $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
        $QStaffSalary = new Application_Model_StaffSalary();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $arr_none_office = array(182, 183, 190, 162, 164, 293, 312, 295, 730, 732, 732, 424, 417, 731,798, 577);
        $db = Zend_Registry::get('db');
        $created_at = date('Y-m-d H:i:s');
        $created_by = $userStorage->id;
        $response = array('status' => 0, 'message' => 'Lỗi tạo hợp đồng từ transfer');
        $QRegional_market = new Application_Model_RegionalMarket;
        try {
            $where_staff = array();
            $where_staff[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $Staff_Row = $QStaff->fetchRow($where_staff);


            if (empty($from_date)) {
                $from_date = $Staff_Row['joined_at'];
            }
            //het han hop dong sinh ra do transfer
            $data_update = array(
                'is_next' => 0,
                'note' => 'expired by update transfer,update by :' . $created_by . ' at: ' . $created_at
            );
            $where_staff_contract = $QStaffContract->getAdapter()->quoteInto('id = ?', $current_contract_array['id']);
            $QStaffContract->update($data_update, $where_staff_contract);

            $where_CProcess = [];
            $where_CProcess[] = $QTeam->getAdapter()->quoteInto('id = ?', $Staff_Row['title']);
            $where_CProcess[] = $QTeam->getAdapter()->quoteInto('del = ?', 0);
            $QContractProcess_Row = $QTeam->fetchRow($where_CProcess);

            if (empty($QContractProcess_Row['contract_term'])) {
                throw new Exception('Khu vực hoặc title chưa có chính sách trong bảng contract_process.!');
            }
            $work_cost = 0;
            if (!$change_work_cost) {
                // neu ko change work cost thi lay work cost cu
                $work_cost = $old_salary['work_cost_salary'];
            }

            if (in_array($current_transfer['title'], array(182, 293, 730, 732))) {
                // new transfer ve PG va SNP thi tim lai work cost cu
                $max_wk = $QStaffSalary->getRecentWorkCost($staff_id);
                if (!empty($max_wk)) {
                    $work_cost = $old_salary['work_cost_salary'];
                }
            }
            /// lấy lương  fn_salary_TH_old  fn_salary_TH_new  fn_salary_company
            $policy_detail_id = $salary = $salary_basic = $salary_basic1 = $salary_bonus = $salary_min = $salary_kpi = 0;
            $allowance = $allowance1 = $salary_total = $salary_total1 = 0;
            $salary_insurance_salary = 0;
            $default_district = 0;

            //- Kế thừa thông tin hợp đồng, không cập nhật lại quận huyện khi: Transfer trong cùng tỉnh + (cùng chức danh/từ PGPB => Senior Promoter)
            if ($current_transfer['regional_market'] == $current_transfer['old_regional_market'] && ($current_transfer['title'] == $current_transfer['old_title'] || (($current_transfer['old_title'] == 182 && $current_transfer['title'] == 293) || ($current_transfer['old_title'] == 730 && $current_transfer['title'] == 732)))) {
                $default_district = $Staff_Row['district'];
            } else { //($title,$region_market,$staff_id)
                $default_district = self::getDistrictId($current_transfer['title'], $current_transfer['regional_market'], $staff_id, $current_transfer['area']);

                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $data_update = array(
                    "district" => $default_district
                );
                $QStaff->update($data_update, $where_staff);
            }
            if (empty($default_district) || $default_district == -1 || $default_district == 0) {
                throw new Exception('Kiểm tra lại quận huyện');
            }

            if (in_array($current_transfer['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER  (chính sách PG)
                $salary_policy_data = $QStaffContract->getSalaryDetailByTitleDistrict(182, $default_district);

                if (empty($salary_policy_data)) {
                    throw new Exception('Không tìm thấy chính sách lương PG cho nhân viên này');
                }
                $PolicyDetail_Row = $salary_policy_data;
                $policy_detail_id = $salary_policy_data['salary_policy_detail_id'];
                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_basic1 = intval($PolicyDetail_Row['basic_salary1']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
            } elseif (in_array($current_transfer['title'], array(183, 164, 162))) { //Sale, Sale Access, Sale Leader Access ( chính sách sale)
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(183, $default_district);

                $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;

                if ($policy_detail_id == 0) {
                    throw new Exception('Không tìm thấy chính sách lương SALE cho nhân viên này');
                }

                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            } elseif (in_array($current_transfer['title'], array(190))) { //Sale leader (chính sách sale leader)
                $salary_kpi = 6000000;
                $salary_basic = 6000000;
                $salary_min = $allowance1 = $allowance = 0;
                $salary_total1 = $salary_basic + $allowance1;
                $salary_total = $salary_basic + $allowance;
            } elseif (in_array($Staff_Row['title'], array(417))) { //PG leader (chính sách PG leader)
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(417, $default_district);

                $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;

                if ($policy_detail_id == 0) {
                    throw new Exception('Không tìm thấy chính sách lương PG LEADER cho nhân viên này');
                }

                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            } elseif (in_array($current_transfer['title'], array(732, 731,798, 577,725, 799))) { //Store leader (chính sách store leader )  // lương KPI=6tr, tổng lương 6tr
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict($current_transfer['title'], $default_district);

                $policy_detail_id = intval($PolicyDetail_Row['id']);
                if (!$policy_detail_id) {
                    throw new Exception('Không tìm thấy chính sách lương STORE LEADER cho nhân viên này');
                }
                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            } elseif (in_array($current_transfer['title'], array(424))) { //Sale leader BS 
                $where_PolicyDetail = array();
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(424));
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $current_transfer['regional_market']);
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                $PolicyDetail_Row = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);

                $policy_detail_id = intval($PolicyDetail_Row['id']);
                if (!$policy_detail_id) {
                    throw new Exception('Không tìm thấy chính sách lương SALE LEADER cho nhân viên này');
                }
                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            }
            $StaffContract_id = $StaffSalary_id = 0;
            // update het luong cu neu co
            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('staff_id = ?', $Staff_Row['id']);
            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('to_date is NULL');
            $data_update_salary = array('to_date' => $from_date);
            $QStaffSalary->update($data_update_salary, $where_staff_salary);
            ///// insert table:  staff_salary
            $data_staff_salary = array(
                'staff_id' => $Staff_Row['id'],
                'insurance_salary' => $salary_insurance_salary,
                'work_cost_salary' => $work_cost,
                'from_date' => $from_date,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'note' => 'create from transfer id ' . $current_transfer['transfer_id'],
            );

            $StaffSalary_id = $QStaffSalary->insert($data_staff_salary);

            /////////// Row1
            $where_CType1 = array();
            $where_CType1[] = $QContractType->getAdapter()->quoteInto('id = ?', $QContractProcess_Row['contract_term']);
            $QContractType_Row1 = $QContractType->fetchRow($where_CType1)->toArray();

            $addDay1 = (isset($QContractType_Row1['term']) and $QContractType_Row1['term']) ? $QContractType_Row1['term'] : 0;

            $to_date1 = date('Y-m-d', strtotime("+" . $addDay1 . " days", strtotime($from_date)));
            $data_contract1 = array(
                'company_id' => $current_transfer['company_id'],
                'staff_id' => $Staff_Row['id'],
                'from_date' => $from_date,
                'to_date' => $to_date1,
                'system_note' => 'create from transfer id ' . $current_transfer['transfer_id'],
                'title' => $current_transfer['title'],
                'regional_market' => $current_transfer['regional_market'],
                'contract_term' => $QContractProcess_Row['contract_term'],
                'status' => 2, // 0: chưa approve, 1: Khu vực approve, 2: HR approve
                'print_type' => 1, // 1: Hợp đồng, 2: phụ lục
                'created_at' => $created_at,
                'created_by' => $created_by,
                'salary_policy_detail_id' => $policy_detail_id,
                'salary_id' => $StaffSalary_id,
                'salary_total' => $salary_total1,
                'salary_bonus' => $allowance1,
                'salary_kpi' => $salary_kpi,
                'salary_min' => $salary_min,
                'is_next' => 2, // hop dong hien tai
                'parent_id' => $current_contract_array['id'],
                'transfer_id' => $current_transfer['transfer_id'],
                'hospital_id' => $current_contract_array['hospital_id'],
                'district_id' => $default_district
            );

            if (self::checkConfirmDistrict($current_transfer) == 1) {
                $data_contract1['district_confirm'] = 0;
            }
            $StaffContract_id = $QStaffContract->insert($data_contract1);

            //Tracking
            if ($current_transfer['title'] == 183) {

                $QLogTracking = new Application_Model_LogTracking();
                $data_tracking = array(
                    'name' => 'save_transfer',
                    'note' => 'contractInsert transfer_id: ' . $current_transfer['transfer_id'] . ' contract_id ' . $StaffContract_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $created_by
                );
                $QLogTracking->insert($data_tracking);
            }

            /// Cập nhật thời gian hợp đồng hiện tại cho bảng staff
            $data_update_staff = array(
                'contract_signed_at' => $from_date,
                'contract_expired_at' => $to_date1,
                'contract_term' => $QContractProcess_Row['contract_term']
            );

            $QStaff->update($data_update_staff, $where_staff);
            //todo cần log lại chỗ này
            //
            // het han next cu
            $data_update2 = array(
                'is_expired' => 1
            );
            $where_staff_contract2 = array();
            $where_staff_contract2[] = $QStaffContract->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where_staff_contract2[] = $QStaffContract->getAdapter()->quoteInto('is_next = 1');
            $QStaffContract->update($data_update2, $where_staff_contract2);
            // insert hop dong next moi
            $QStaffContract->createNextContractByStaff($staff_id);
            $update_next = $QStaffContract->UpdateNextContractByStaff($staff_id);

            if (empty($StaffContract_id) || empty($StaffSalary_id) || empty($update_next)) {
                throw new Exception('Insert Contract không thành công.!');
            } else {
                $response['status'] = 1;
                $response['message'] = "Thành công";
            }
        } catch (exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function contractInsertRenewable($staff_id, $contract_term) {
        $QStaffContract = new Application_Model_StaffContract();
        try {
            // het han next cu
            $data_update2 = array(
                'is_expired' => 1
            );
            $where_staff_contract2 = array();
            $where_staff_contract2[] = $QStaffContract->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where_staff_contract2[] = $QStaffContract->getAdapter()->quoteInto('is_next = 1');
            $QStaffContract->update($data_update2, $where_staff_contract2);

            // insert hop dong next moi
            $QStaffContract->createNextContractByStaffLocal($staff_id, $contract_term);
            $update_next = $QStaffContract->UpdateNextContractByStaff($staff_id);

            if (empty($StaffContract_id) || empty($StaffSalary_id) || empty($update_next)) {
                throw new Exception('Insert Contract không thành công.!');
            } else {
                $response['status'] = 1;
                $response['message'] = "Thành công";
            }
        } catch (exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function contractInsertProbationTwoMonth($staff_id, $from_date, $old_salary, $current_contract_array, $change_work_cost, $current_transfer) {
        $QStaff = new Application_Model_Staff();
        $QStaffContract = new Application_Model_StaffContract();
        $QContractType = new Application_Model_ContractTypes();
        $QContractProcess = new Application_Model_ContractProcess();
        $QTeam = new Application_Model_Team();
        $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
        $QStaffSalary = new Application_Model_StaffSalary();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $arr_none_office = array(182, 183, 190, 162, 164, 293, 312, 295, 730, 732, 732, 424, 417, 731, 798, 577);
        $db = Zend_Registry::get('db');
        $created_at = date('Y-m-d H:i:s');
        $created_by = $userStorage->id;
        $response = array('status' => 0, 'message' => 'Lỗi tạo hợp đồng từ transfer');
        $QRegional_market = new Application_Model_RegionalMarket;
        $contract_type = 2; // Thu viec 2 thang
        try {
            $where_staff = array();
            $where_staff[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $Staff_Row = $QStaff->fetchRow($where_staff);


            if (empty($from_date)) {
                $from_date = $Staff_Row['joined_at'];
            }
            //het han hop dong sinh ra do transfer
            $data_update = array(
                'is_next' => 0,
                'note' => 'expired by update transfer,update by :' . $created_by . ' at: ' . $created_at
            );
            $where_staff_contract = $QStaffContract->getAdapter()->quoteInto('id = ?', $current_contract_array['id']);
            $QStaffContract->update($data_update, $where_staff_contract);
            $work_cost = 0;
            if (!$change_work_cost) {
                // neu ko change work cost thi lay work cost cu
                $work_cost = $old_salary['work_cost_salary'];
            }

            if (in_array($current_transfer['title'], array(182, 293, 730, 732))) {
                // new transfer ve PG va SNP thi tim lai work cost cu
                $max_wk = $QStaffSalary->getRecentWorkCost($staff_id);
                if (!empty($max_wk)) {
                    $work_cost = $old_salary['work_cost_salary'];
                }
            }
            /// lấy lương  fn_salary_TH_old  fn_salary_TH_new  fn_salary_company
            $policy_detail_id = $salary = $salary_basic = $salary_basic1 = $salary_bonus = $salary_min = $salary_kpi = 0;
            $allowance = $allowance1 = $salary_total = $salary_total1 = 0;
            $salary_insurance_salary = 0;
            $default_district = 0;

            //- Kế thừa thông tin hợp đồng, không cập nhật lại quận huyện khi: Transfer trong cùng tỉnh + (cùng chức danh/từ PGPB => Senior Promoter)
            if ($current_transfer['regional_market'] == $current_transfer['old_regional_market'] && ($current_transfer['title'] == $current_transfer['old_title'] || (($current_transfer['old_title'] == 182 && $current_transfer['title'] == 293) || ($current_transfer['old_title'] == 730 && $current_transfer['title'] == 732)))) {
                $default_district = $Staff_Row['district'];
            } else {
                $default_district = self::getDistrictId($current_transfer['title'], $current_transfer['regional_market'], $staff_id, $current_transfer['area']);

                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $data_update = array(
                    "district" => $default_district
                );
                $QStaff->update($data_update, $where_staff);
            }
            if (empty($default_district) || $default_district == -1 || $default_district == 0) {
                throw new Exception('Kiểm tra lại quận huyện');
            }

            if (in_array($current_transfer['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER  (chính sách PG)
                $salary_policy_data = $QStaffContract->getSalaryDetailByTitleDistrict(182, $default_district);

                if (empty($salary_policy_data)) {
                    throw new Exception('Không tìm thấy chính sách lương PG cho nhân viên này');
                }
                $PolicyDetail_Row = $salary_policy_data;
                $policy_detail_id = $salary_policy_data['salary_policy_detail_id'];
                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_basic1 = intval($PolicyDetail_Row['basic_salary1']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
            } elseif (in_array($current_transfer['title'], array(183, 164, 162))) { //Sale, Sale Access, Sale Leader Access ( chính sách sale)
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(183, $default_district);

                $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;

                if ($policy_detail_id == 0) {
                    throw new Exception('Không tìm thấy chính sách lương SALE cho nhân viên này');
                }

                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            } elseif (in_array($current_transfer['title'], array(190))) { //Sale leader (chính sách sale leader)
                $salary_kpi = 6000000;
                $salary_basic = 6000000;
                $salary_min = $allowance1 = $allowance = 0;
                $salary_total1 = $salary_basic + $allowance1;
                $salary_total = $salary_basic + $allowance;
            } elseif (in_array($Staff_Row['title'], array(417))) { //PG leader (chính sách PG leader)
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(417, $default_district);

                $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;

                if ($policy_detail_id == 0) {
                    throw new Exception('Không tìm thấy chính sách lương PG LEADER cho nhân viên này');
                }

                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            } elseif (in_array($current_transfer['title'], array(732, 731, 798, 577,725, 799))) { //Store leader (chính sách store leader )  // lương KPI=6tr, tổng lương 6tr
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict($current_transfer['title'], $default_district);

                $policy_detail_id = intval($PolicyDetail_Row['id']);
                if (!$policy_detail_id) {
                    throw new Exception('Không tìm thấy chính sách lương STORE LEADER cho nhân viên này');
                }
                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            } elseif (in_array($current_transfer['title'], array(424))) { //Sale leader BS 
                $where_PolicyDetail = array();
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(424));
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $current_transfer['regional_market']);
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                $PolicyDetail_Row = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);

                $policy_detail_id = intval($PolicyDetail_Row['id']);
                if (!$policy_detail_id) {
                    throw new Exception('Không tìm thấy chính sách lương SALE LEADER cho nhân viên này');
                }
                $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $salary_total = intval($PolicyDetail_Row['total_company_salary']);
            }
            $StaffContract_id = $StaffSalary_id = 0;
            // update het luong cu neu co
            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('staff_id = ?', $Staff_Row['id']);
            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('to_date is NULL');
            $data_update_salary = array('to_date' => $from_date);
            $QStaffSalary->update($data_update_salary, $where_staff_salary);
            $data_staff_salary = array(
                'staff_id' => $Staff_Row['id'],
                'insurance_salary' => $salary_insurance_salary,
                'work_cost_salary' => $work_cost,
                'from_date' => $from_date,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'note' => 'create from transfer id ' . $current_transfer['transfer_id'],
            );

            $StaffSalary_id = $QStaffSalary->insert($data_staff_salary);

            /////////// Row1
            $where_CType1 = array();
            $where_CType1[] = $QContractType->getAdapter()->quoteInto('id = ?', $contract_type);
            $QContractType_Row1 = $QContractType->fetchRow($where_CType1)->toArray();

            $addDay1 = (isset($QContractType_Row1['term']) and $QContractType_Row1['term']) ? $QContractType_Row1['term'] : 0;

            $to_date1 = date('Y-m-d', strtotime("+" . $addDay1 . " days", strtotime($from_date)));
            $data_contract1 = array(
                'company_id' => $current_transfer['company_id'],
                'staff_id' => $Staff_Row['id'],
                'from_date' => $from_date,
                'to_date' => $to_date1,
                'system_note' => 'create from transfer id ' . $current_transfer['transfer_id'],
                'title' => $current_transfer['title'],
                'regional_market' => $current_transfer['regional_market'],
                'contract_term' => $contract_type,
                'status' => 2, // 0: chưa approve, 1: Khu vực approve, 2: HR approve
                'print_type' => 1, // 1: Hợp đồng, 2: phụ lục
                'created_at' => $created_at,
                'created_by' => $created_by,
                'salary_policy_detail_id' => $policy_detail_id,
                'salary_id' => $StaffSalary_id,
                'salary_total' => $salary_total1,
                'salary_bonus' => $allowance1,
                'salary_kpi' => $salary_kpi,
                'salary_min' => $salary_min,
                'is_next' => 2, // hop dong hien tai
                'parent_id' => $current_contract_array['id'],
                'transfer_id' => $current_transfer['transfer_id'],
                'hospital_id' => $current_contract_array['hospital_id'],
                'district_id' => $default_district
            );

            if (self::checkConfirmDistrict($current_transfer) == 1) {
                $data_contract1['district_confirm'] = 0;
            }
            $StaffContract_id = $QStaffContract->insert($data_contract1);

            //Tracking
            if ($current_transfer['title'] == 183) {

                $QLogTracking = new Application_Model_LogTracking();
                $data_tracking = array(
                    'name' => 'save_transfer',
                    'note' => 'contractInsert transfer_id: ' . $current_transfer['transfer_id'] . ' contract_id ' . $StaffContract_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $created_by
                );
                $QLogTracking->insert($data_tracking);
            }

            /// Cập nhật thời gian hợp đồng hiện tại cho bảng staff
            $data_update_staff = array(
                'contract_signed_at' => $from_date,
                'contract_expired_at' => $to_date1,
                'contract_term' => $contract_type
            );

            $QStaff->update($data_update_staff, $where_staff);
            $update_next = $QStaffContract->UpdateNextContractByStaff($staff_id);
            if (empty($StaffContract_id) || empty($StaffSalary_id) || empty($update_next)) {
                throw new Exception('Insert Contract không thành công.!');
            } else {
                $response['status'] = 1;
                $response['message'] = "Thành công";
            }
        } catch (exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function updateDistrict($staff_id, $district_id) {

        $QStaff = new Application_Model_Staff();
        $db = Zend_Registry::get('db');
        $QStaffSalary = new Application_Model_StaffSalary();
        $QStaffContract = new Application_Model_StaffContract();

        $response = array('status' => 0, 'message' => 'lỗi tạo hợp đồng');

        try {


            $select_contract = $db->select()
                    ->from(array('p' => 'staff_contract'), array('p.*'))
                    ->where('p.staff_id = ?', $staff_id)
                    ->where('p.print_status = 0')
                    ->where('p.district_confirm = 0');
            $row_contract = $db->fetchRow($select_contract);

            if (!empty($row_contract) && !in_array($row_contract['title'], array(SALES_TITLE,PGS_SUPERVISOR,LEADER_TITLE))) {
                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $data_update = array(
                    "district" => $district_id
                );
                $QStaff->update($data_update, $where_staff);
                if (in_array($row_contract['title'], array(182, 293, 730, 732))) {
                    $work_cost_arr = $QStaffContract->getMaxSeniority($staff_id);
                    $work_cost = $work_cost_arr['max_work_cost'];
                }

                //update hop dong
                if (in_array($row_contract['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER  (chính sách PG)
//              
                    $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(182, $district_id);


                    if (empty($PolicyDetail_Row)) {
                        throw new Exception('Không tìm thấy chính sách lương PG cho nhân viên này');
                    }
                    $policy_detail_id = $PolicyDetail_Row['id'];


                    $salary_basic1 = intval($PolicyDetail_Row['basic_salary1']);
                    $salary_min = intval($PolicyDetail_Row['min_salary']);
                    $allowance1 = intval($PolicyDetail_Row['allowance1']);
                    $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                    $salary_kpi1 = intval($PolicyDetail_Row['salary_kpi1']);

                    $salary_policy_filter = $PolicyDetail_Row['salary_policy_id'];
                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                    if (in_array($salary_policy_filter, array(5))) {
                        $allowance = $PolicyDetail_Row['allowance'] + $work_cost;
                        $salary_kpi = $salary_basic;
                        $salary_total = $salary_basic + $work_cost;
                    } else {
                        if ($salary_insurance_salary >= ($salary_basic + $work_cost)) {
                            $allowance = 0;
                        } else {
                            $allowance = $salary_basic + $work_cost - $salary_insurance_salary;
                        }
                        if (($salary_basic + $work_cost) >= $salary_insurance_salary) {
                            $salary_kpi = 0;
                        } else {
                            $salary_kpi = $salary_insurance_salary - $salary_basic - $work_cost;
                        }
                        $salary_total = $salary_insurance_salary + $allowance;
                    }
                } elseif (in_array($row_contract['title'], array(183, 164, 162))) { //Sale, Sale Access, Sale Leader Access ( chính sách sale)
                    $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(183, $district_id);

                    $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;

                    if ($policy_detail_id == 0) {
                        throw new Exception('Không tìm thấy chính sách lương SALE cho nhân viên này');
                    }

                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_kpi = intval($PolicyDetail_Row['salary_kpi']);
                    $salary_kpi1 = intval($PolicyDetail_Row['salary_kpi1']);
                    $salary_min = intval($PolicyDetail_Row['min_salary']);
                    $allowance1 = intval($PolicyDetail_Row['allowance1']);
                    $allowance = intval($PolicyDetail_Row['allowance']);
                    $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                    $salary_total = intval($PolicyDetail_Row['total_company_salary']);
                } elseif (in_array($row_contract['title'], array(417, 731, 798, 577))) { //PG leader (chính sách PG leader)
                    $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict($row_contract['title'], $district_id);

                    $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;

                    if ($policy_detail_id == 0) {
                        throw new Exception('Không tìm thấy chính sách lương PG LEADER cho nhân viên này');
                    }
                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_kpi1 = intval($PolicyDetail_Row['salary_kpi1']);
                    $salary_kpi = intval($PolicyDetail_Row['salary_kpi']);
                    $salary_min = intval($PolicyDetail_Row['min_salary']);
                    $allowance1 = intval($PolicyDetail_Row['allowance1']);
                    $allowance = intval($PolicyDetail_Row['allowance']);
                    $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                    $salary_total = intval($PolicyDetail_Row['total_company_salary']);
                } elseif (in_array($row_contract['title'], array(732,725 , 799))) { //Store leader (chính sách store leader )  // lương KPI=6tr, tổng lương 6tr
                    $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict($row_contract['title'], $district_id);
                    $policy_detail_id = intval($PolicyDetail_Row['id']);
                    if (!$policy_detail_id) {
                        throw new Exception('Không tìm thấy chính sách lương STORE LEADER cho nhân viên này');
                    }
                    $salary_basic = intval($PolicyDetail_Row['basic_company_salary']);
                    $salary_kpi = intval($PolicyDetail_Row['salary_kpi1']);
                    $allowance1 = intval($PolicyDetail_Row['allowance1']);
                    $allowance = intval($PolicyDetail_Row['allowance']);
                    $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                    $salary_total = intval($PolicyDetail_Row['total_company_salary']);
                    $salary_kpi1 = intval($PolicyDetail_Row['salary_kpi1']);
                }
                if (in_array($row_contract['contract_term'], array(2, 3, 17))) {
                    // luong thu viec

                    $data_update = array(
                        'salary_total' => $salary_total1,
                        'salary_bonus' => $allowance1,
                        'salary_min' => $salary_min,
                        'salary_kpi' => $salary_kpi1,
                        'salary_policy_detail_id' => $PolicyDetail_Row['id'],
                        'district_id' => $district_id,
                        'is_disable' => 0,
                        'district_confirm' => 1,
                    );
                    $data_salary = array(
                        'insurance_salary' => 0,
                        'work_cost_salary' => 0,
                    );
                } else {
                    // luong chinh thuc
                    $data_salary = array(
                        'insurance_salary' => $PolicyDetail_Row['basic_insurance_salary'],
                        'work_cost_salary' => $work_cost,
                    );
                    $data_update = array(
                        'salary_total' => $salary_total,
                        'salary_bonus' => $allowance,
                        'salary_min' => $salary_min,
                        'salary_kpi' => $salary_kpi,
                        'salary_policy_detail_id' => $PolicyDetail_Row['id'],
                        'district_id' => $district_id,
                        'is_disable' => 0,
                        'district_confirm' => 1,
                    );
                }

                $where_salary = $QStaffSalary->getAdapter()->quoteInto('id = ?', $row_contract['salary_id']);
                $QStaffSalary->update($data_salary, $where_salary);
                $where_staff_contract = $QStaffContract->getAdapter()->quoteInto('id = ?', $row_contract['id']);
                $QStaffContract->update($data_update, $where_staff_contract);
                $QStaffContract->UpdateNextContractByStaff($row_contract['staff_id']);
                $QLog = new Application_Model_Log();
                $userStorage = Zend_Auth::getInstance()->getStorage()->read();

                $ip = $_SERVER['REMOTE_ADDR'];
                $info = "update staff " . $staff_id . " district " . $district_id;
                //todo log
                $QLog->insert(array(
                    'info' => $info,
                    'user_id' => $userStorage->id,
                    'ip_address' => $ip,
                    'time' => date('Y-m-d H:i:s'),
                ));

                $response['status'] = 1;
                $response['message'] = "Thành công";
            } else {
                $response['status'] = 1;
                $response['message'] = "Không tìm thấy hợp đồng hiện tại chưa gán quận huyện";
            }
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function updateOfficeWhenTransfer($staff_id, $area_id, $title_id) {
        if (empty($area_id) || empty($title_id) || empty($staff_id)) {
            return -1;
        }
        $db = Zend_Registry::get('db');
        $QTeam = new Application_Model_Team();
        $QStaff = new Application_Model_Staff();
        $titleRowSet = $QTeam->find($title_id);
        $office_type = $titleRowSet->current()['office_type'];

        if (!empty($office_type)) {
            $whereLocation = $db->select()
                    ->from(array('a' => 'office'), array('a.id', 'a.district'))
                    ->where('a.area_id = ?', $area_id)
                    ->where('a.office_type = ?', $office_type)
                    ->where('a.del <> ?', 1);
            $locationRow = $db->fetchRow($whereLocation);

            if (!empty($locationRow)) {

                $data_update = array(
                    'office_id' => $locationRow['id'],
                    'district' => $locationRow['district']
                );
                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $QStaff->update($data_update, $where_staff);
            }
            return $locationRow['district'];
        } else {
            return -1;
        }
    }

    //function append
    public static function getGroupByTitle($title) {
        $QTeam = new Application_Model_Team();
        $res = $QTeam->fetchRow(['id = ?' => $title]);
        if (!empty($res['company_group'])) {
            return $res['company_group'];
        }
        return null;
    }

    //end function append
    public static function isProbativeStaffAndSpecialCaseTransfer($titleCurrent, $titleNext) {
        $group1 = [182, 730, 731, 798];  //PGs (Sale/Brandshop), Product Consultant
        $group2 = [417, 577];      //PGs Sup, PGs Leader
        if (
                (in_array($titleCurrent, $group1) AND in_array($titleNext, $group1))
                OR ( in_array($titleCurrent, $group2) AND in_array($titleNext, $group2))
        // OR
        // ($titleCurrent ==$titleNext)
        ) {
            return 1;
        }
        return 0;
    }

    // get district theo rule transfer
    public function getDistrictId($title, $region_market, $staff_id, $area) {
        $QRegional_market = new Application_Model_RegionalMarket();
        /**
         * @author: hero
         * title
         * từ 27-10-2020 tat ca đeu lay theo district default 
         * date
         */
//        if(in_array($title, array(730,182,577,731,183,399,424,190,732,293,732,417))){
        $where_regional_market = array();
        $where_regional_market[] = $QRegional_market->getAdapter()->quoteInto('parent = ?', $region_market);
        $where_regional_market[] = $QRegional_market->getAdapter()->quoteInto('default_contract_district = ?', 1);
        $where_regional_market[] = $QRegional_market->getAdapter()->quoteInto('del = ?', 0);
        $regional_market_district = $QRegional_market->fetchRow($where_regional_market);
        $default_district = $regional_market_district['id'];
        if (empty($default_district) || $QRegional_market->checkDistrictExits($default_district) == 0) {
            return -1;
        }
        return $default_district;
    }

}
