<?php
/**
* @author buu.pham
*/
class My_Staff_Info_Type extends My_Type_Enum
{
    const Area       = 1;
    const Region     = 2;
    const Group      = 3;
    const Team       = 4;
    const Department = 5;
    const Status     = 6;
    const Company    = 7;
    const Title      = 8;
}