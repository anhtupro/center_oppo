<?php
/**
*  
*/
class My_Staff_Permission_Area
{
    
    public static function view_all($staff_id)
    {
        if (is_null($staff_id))
            return false;

        $QAll = new Application_Model_AllArea();
        $all = $QAll->get_cache();

        if (!$all || !is_array($all) || !count($all) || !in_array($staff_id, $all))
            return false;

        return true;
    }

    public static function view_bi($staff_id)
    {
        if (is_null($staff_id))
            return false;

        $QBiArea = new Application_Model_BiArea();
        $where = array();
        $where[] = $QBiArea->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where[] = $QBiArea->getAdapter()->quoteInto('type = ?', 1);
        $bi_area = $QBiArea->fetchRow($where);
       
        if (!$bi_area || !count($bi_area))
            return false;

        return true;
    }

    public static function view_bi_all($staff_id)
    {
        if (is_null($staff_id))
            return false;

        $QBiArea = new Application_Model_BiArea();
        $where = array();
        $where[] = $QBiArea->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where[] = $QBiArea->getAdapter()->quoteInto('type = ?', 2);
        $bi_area = $QBiArea->fetchRow($where);

        if (!$bi_area || !count($bi_area))
            return false;

        return true;
    }
}