<?php
/**
* @author buu.pham
*/
class My_Survey_Info
{
    const CUSTOMER_NAME = 0;
    const MODEL_COLOR   = 1;
    const PRICE         = 2;
    const SELL_OUT_DATE = 3;
    const STORE         = 4;
    const IMEI          = 5;
}
