<?php 
/**
* @author buu.pham
* Các kết quả khảo sát
*/
class My_Survey_Result
{
    const OK = 1;
    const NOT_OK = 2;
    const NOT_CONFIRMED = 3;
    const RESURVEY = 4;

    public static $name = array(
        self::OK => 'OK',
        self::NOT_OK => 'Not OK',
        self::NOT_CONFIRMED => 'Not confirmed',
    );

    public static $color = array(
        self::OK => 'success',
        self::NOT_OK => 'important',
        self::NOT_CONFIRMED => 'default',
    );
}
