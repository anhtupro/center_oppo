<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ErrorHandler
 *
 * @author Hero
 */
class My_ErrorHandler {

    public static function shutdownHandler() {

        $e = error_get_last();
        if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $user_id     = 0;
            $user_name   = "NA";
            if (!empty($userStorage)) {
                $user_id = $userStorage->id;
            }


            $excute_time = microtime(true);
            $total_time  = $excute_time - APP_START_TIME;

            $log_model  = new Application_Model_LogExcutionAction();
            $data_array = [
                'excution_time' => round($total_time, 2),
                'finished'      => 1
            ];
            $where[]    = $log_model->getAdapter()->quoteInto('unique_id like ?', UNIQUE_ID);
            $where[]    = $log_model->getAdapter()->quoteInto('excution_user = ?', $user_id);
            $log_model->update($data_array, $where);
        }
        if (!is_null($e) && $e['type'] == E_ERROR) { //fatal error
//            //log
//            Zend_Registry::get('log')->err($e['message']);
//
//            //Redirect to error page
//            $redirector = new Zend_Controller_Action_Helper_Redirector();
//            $redirector->gotoSimple('error', 'error', null, array('error_handler' => 'fatal'));
        }
    }

}
