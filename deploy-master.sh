ssh bitbucket@3.1.208.199 /bin/bash <<EOT
 cd /var/www/html/center/
 echo "Backup source..."
 sudo rsync -az --exclude=public --exclude=logs --exclude=bin/log --delete /var/www/html/center /tmp/backup/
 echo "Backup success!"
 echo "Pull..."
 sudo git pull origin master
 echo "Pull success!"
 sudo chown -R apache.apache /var/www/html/center/
 sudo chown -R bitbucket.bitbucket bitbucket-pipelines.yml deploy-develop.sh deploy-master.sh .git .gitignore
 echo "Done!"
EOT
