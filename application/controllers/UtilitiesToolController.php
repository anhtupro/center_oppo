<?php

class UtilitiesToolController extends My_Controller_Action
{
	public function indexAction(){

		$area = new Application_Model_Area();
		$area_data = $area->select()->order('name ASC');
		$this->view->area_data = $area->fetchAll($area_data);

		$QRegion = new Application_Model_RegionalMarket();

		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest())
		{
			$this->_helper->layout->disableLayout();
		    
			$area_id	= $this->getRequest()->getParam('area_id');
			

			$db			= Zend_Registry::get('db');
			$sql		= ' SELECT id,name FROM regional_market WHERE area_id = :area_id ';
			$stmt		= $db->prepare($sql);
			$stmt->bindParam("area_id", $area_id, PDO::PARAM_INT);
			$stmt->execute();
			$list		= $stmt->fetchAll();
			$stmt->closeCursor();
			$db			= $stmt = null;
				
			$this->view->QRegion_data = $list;
			$this->render('province');
			
			// exit();
		}
		

	}
	public function remapAction(){

		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest())
		{
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$region_id	= $this->getRequest()->getParam('region_id');

			$region = new Application_Model_UtilitiesTool();
			$update_region = $region->UpdateRegionalMarket($region_id);
			// var_dump($update_region);die;
			if($update_region)
				echo json_encode(array('status' => true));
			else
				echo json_encode(array('status' => false));

			
		}
	}

	public function convertExcelAction(){
		if($_FILES){
			set_time_limit(0);
			$file = !empty($_FILES['file']) ? $_FILES['file'] : null;
			if(!$file) exit('No file to convert.');

			include 'PHPExcel/IOFactory.php';
			$objReader = PHPExcel_IOFactory::createReader('CSV');
			$objReader->setDelimiter(",");
			$objReader->setInputEncoding('UTF-8');
			$objPHPExcel = $objReader->load($file['tmp_name']);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			$filename = pathinfo($file['name']);
			$filename = $filename['filename'];
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;
		}
	}

}