<?php

class KpisettingController extends My_Controller_Action{

	public function init(){
		$this->from_date = '2016-09-01 00:00';
		$this->to_date = '2016-09-30 23:59';
	}

	public function indexAction(){

	}

	public function ssghAction(){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('
			SELECT *, SUM(total_sell_out) AS total_sell_out, SUM(total_sell_out * kpi) AS total FROM (
				SELECT
					a.name AS area_name,
					st.code AS staff_code,
					CONCAT(st.firstname, " ", st.lastname) AS staff_name,
					COUNT(*) AS total_sell_out,
					i.kpi_pg AS kpi,
					MIN(DATE(i.timing_date)) AS from_date,
					MAX(DATE(i.timing_date)) AS to_date
				FROM imei_kpi i
				LEFT JOIN area a ON i.area_id = a.id
				JOIN staff st ON i.pg_id = st.id
				JOIN warehouse.good g ON i.good_id = g.id

				WHERE
					i.kpi_pg_bk <> i.kpi_pg
					AND i.kpi_pg_bk > 0
					AND i.timing_date >= "'.$this->from_date.'"
					AND i.timing_date <= "'.$this->to_date.'"
				GROUP BY staff_code, kpi
			) AS tbl1
			GROUP BY staff_code
		');
		$stmt->execute();
		$data = $stmt->fetchAll();
		$stmt->closeCursor();
		$db = $stmt = null;

		require_once 'PHPExcel.php';
		$PHPExcel = new PHPExcel();
		$heads = array(
			'A' => 'Area',
			'B' => 'Staff Code',
			'C' => 'Staff Name',
			'D' => 'Sell Out',
			'E' => 'Total',
			'F' => 'From Date',
			'G' => 'To Date'
		);
		$PHPExcel->setActiveSheetIndex(0);
		$sheet = $PHPExcel->getActiveSheet();

		foreach($heads as $key => $value)
			$sheet->setCellValue($key.'1', $value);

		$sheet->getStyle('A1:I1')->applyFromArray(array('font' => array('bold' => true)));

		$sheet->getColumnDimension('A')->setWidth(10);
		$sheet->getColumnDimension('B')->setWidth(10);
		$sheet->getColumnDimension('C')->setWidth(30);
		$sheet->getColumnDimension('D')->setWidth(10);
		$sheet->getColumnDimension('E')->setWidth(10);
		$sheet->getColumnDimension('F')->setWidth(15);
		$sheet->getColumnDimension('G')->setWidth(15);

		foreach($data as $key => $value){
			$sheet->setCellValue('A'.($key + 2), $value['area_name']);
			$sheet->setCellValue('B'.($key + 2), $value['staff_code']);
			$sheet->setCellValue('C'.($key + 2), $value['staff_name']);
			$sheet->setCellValue('D'.($key + 2), $value['total_sell_out']);
			$sheet->setCellValue('E'.($key + 2), $value['total']);
			$sheet->setCellValue('F'.($key + 2), date('d/m/Y', strtotime($value['from_date'])));
			$sheet->setCellValue('G'.($key + 2), date('d/m/Y', strtotime($value['to_date'])));
		}

		$filename = 'Sell out - VTA - ' . date('Y-m-d H-i-s');
		$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		$objWriter->save('php://output');
		exit;
	}

	public function ssghdAction(){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('
			SELECT
				a.name AS area_name,
				st.code AS staff_code,
				CONCAT(st.firstname, " ", st.lastname) AS staff_name,
				g.desc AS product_name,
				gc.name AS color,
				COUNT(*) AS total_sell_out,
				i.kpi_pg AS kpi,
				(COUNT(*) * i.kpi_pg) AS total,
				MIN(DATE(i.timing_date)) AS from_date,
				MAX(DATE(i.timing_date)) AS to_date
			FROM imei_kpi i
			LEFT JOIN area a ON i.area_id = a.id
			JOIN staff st ON i.pg_id = st.id
			JOIN warehouse.good g ON i.good_id = g.id
			JOIN warehouse.good_color gc ON i.color_id = gc.id

			WHERE
				i.kpi_pg_bk <> i.kpi_pg
				AND i.kpi_pg_bk > 0
				AND i.timing_date >= "'.$this->from_date.'"
				AND i.timing_date <= "'.$this->to_date.'"
			GROUP BY staff_code, product_name, color, kpi
		');
		$stmt->execute();
		$data = $stmt->fetchAll();
		$stmt->closeCursor();
		$db = $stmt = null;

		require_once 'PHPExcel.php';
		$PHPExcel = new PHPExcel();
		$heads = array(
			'A' => 'Area',
			'B' => 'Staff Code',
			'C' => 'Staff Name',
			'D' => 'Product',
			'E' => 'Color',
			'F' => 'Sell Out',
			'G' => 'KPI',
			'H' => 'Total',
			'I' => 'From Date',
			'J' => 'To Date'
		);
		$PHPExcel->setActiveSheetIndex(0);
		$sheet = $PHPExcel->getActiveSheet();

		foreach($heads as $key => $value)
			$sheet->setCellValue($key.'1', $value);

		$sheet->getStyle('A1:I1')->applyFromArray(array('font' => array('bold' => true)));

		$sheet->getColumnDimension('A')->setWidth(10);
		$sheet->getColumnDimension('B')->setWidth(10);
		$sheet->getColumnDimension('C')->setWidth(30);
		$sheet->getColumnDimension('D')->setWidth(10);
		$sheet->getColumnDimension('E')->setWidth(10);
		$sheet->getColumnDimension('F')->setWidth(10);
		$sheet->getColumnDimension('G')->setWidth(10);
		$sheet->getColumnDimension('H')->setWidth(10);
		$sheet->getColumnDimension('I')->setWidth(15);
		$sheet->getColumnDimension('J')->setWidth(15);

		foreach($data as $key => $value){
			$sheet->setCellValue('A'.($key + 2), $value['area_name']);
			$sheet->setCellValue('B'.($key + 2), $value['staff_code']);
			$sheet->setCellValue('C'.($key + 2), $value['staff_name']);
			$sheet->setCellValue('D'.($key + 2), $value['product_name']);
			$sheet->setCellValue('E'.($key + 2), $value['color']);
			$sheet->setCellValue('F'.($key + 2), $value['total_sell_out']);
			$sheet->setCellValue('G'.($key + 2), $value['kpi']);
			$sheet->setCellValue('H'.($key + 2), $value['total']);
			$sheet->setCellValue('I'.($key + 2), date('d/m/Y', strtotime($value['from_date'])));
			$sheet->setCellValue('J'.($key + 2), date('d/m/Y', strtotime($value['to_date'])));
		}

		$filename = 'Sell out - VTA - ' . date('Y-m-d H-i-s');
		$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		$objWriter->save('php://output');
		exit;
	}

	public function ssghsaleAction(){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('
			SELECT
				CONCAT(s.firstname, " ", s.lastname) AS staff_name,
				ik.pb_sale_id pb_sale_id,
				ik.area_id area_id,
				ik.kpi_pb_sale kpi,
				g.id good_id,
				g.desc good_name,
				COUNT(g.id) sell_out,
				(COUNT(g.id) * ik.kpi_pb_sale) total,
				MIN(ik.timing_date) from_date,
				MAX(ik.timing_date) to_date
			FROM (
				SELECT pb_sale_id, area_id, kpi_pb_sale, good_id, timing_date
				FROM hr.imei_kpi WHERE timing_date BETWEEN "2016-08-01 00:00" AND "2016-08-31 23:59" AND pb_sale_id > 0 AND kpi_pb_sale > 0
			) ik
			JOIN warehouse.good g ON ik.good_id = g.id
			JOIN hr.staff s ON pb_sale_id = s.id
			GROUP BY staff_name, good_name, ik.kpi_pb_sale
		');
		$stmt->execute();
		$data = $stmt->fetchAll();
		$stmt->closeCursor();
		$db = $stmt = null;

		require_once 'PHPExcel.php';
		$PHPExcel = new PHPExcel();
		$heads = array(
			'A' => 'Area',
			'B' => 'Staff Code',
			'C' => 'Staff Name',
			'D' => 'Product',
			'E' => 'Color',
			'F' => 'Sell Out',
			'G' => 'KPI',
			'H' => 'Total',
			'I' => 'From Date',
			'J' => 'To Date'
		);
		$PHPExcel->setActiveSheetIndex(0);
		$sheet = $PHPExcel->getActiveSheet();

		foreach($heads as $key => $value)
			$sheet->setCellValue($key.'1', $value);

		$sheet->getStyle('A1:I1')->applyFromArray(array('font' => array('bold' => true)));

		$sheet->getColumnDimension('A')->setWidth(10);
		$sheet->getColumnDimension('B')->setWidth(10);
		$sheet->getColumnDimension('C')->setWidth(30);
		$sheet->getColumnDimension('D')->setWidth(10);
		$sheet->getColumnDimension('E')->setWidth(10);
		$sheet->getColumnDimension('F')->setWidth(10);
		$sheet->getColumnDimension('G')->setWidth(10);
		$sheet->getColumnDimension('H')->setWidth(10);
		$sheet->getColumnDimension('I')->setWidth(15);
		$sheet->getColumnDimension('J')->setWidth(15);

		foreach($data as $key => $value){
			$sheet->setCellValue('A'.($key + 2), $value['area_name']);
			$sheet->setCellValue('B'.($key + 2), $value['staff_code']);
			$sheet->setCellValue('C'.($key + 2), $value['staff_name']);
			$sheet->setCellValue('D'.($key + 2), $value['product_name']);
			$sheet->setCellValue('E'.($key + 2), $value['color']);
			$sheet->setCellValue('F'.($key + 2), $value['total_sell_out']);
			$sheet->setCellValue('G'.($key + 2), $value['kpi']);
			$sheet->setCellValue('H'.($key + 2), $value['total']);
			$sheet->setCellValue('I'.($key + 2), date('d/m/Y', strtotime($value['from_date'])));
			$sheet->setCellValue('J'.($key + 2), date('d/m/Y', strtotime($value['to_date'])));
		}

		$filename = 'Sell out - VTA - ' . date('Y-m-d H-i-s');
		$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		$objWriter->save('php://output');
		exit;
	}

}