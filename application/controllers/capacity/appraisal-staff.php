<?php

    $staff_id = $this->getRequest()->getParam('staff_id');
    $plan_id = $this->getRequest()->getParam('plan_id');
    
    $QCapacityPlan = new Application_Model_CapacityPlan();
    $QCapacityDictionaryLevel = new Application_Model_CapacityDictionaryLevel();
    $QCapacity = new Application_Model_Capacity();
    $QStaffCapacity = new Application_Model_StaffCapacity();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $dictionary_level_all = $QCapacityDictionaryLevel->fetchAll(null, 'level DESC')->toArray();
    $dictionary_level = [];
    foreach($dictionary_level_all as $key=>$value){
        $dictionary_level[$value['dictionary_id']][$value['level']] = $value['title'];
    }
    
    
    //Kiểm tra có thiết lập capacity riêng chưa
    $capacity = $QCapacity->getCapacityByStaffDetails($staff_id);
    $capacity_info = $QCapacity->getInfoCapacityStaff($staff_id);

    if(!$capacity){
        $capacity = $QCapacity->getCapacityByStaff($staff_id);
        $capacity_info = $QCapacity->getInfoCapacity($capacity['capacity_id']);
    }
    //END Kiểm tra có thiết lập capacity riêng chưa

    if($staff_capacity){
        //$capacity_level = $QStaffCapacity->getListLevel($staff_capacity['id']);

        $capacity_level = $QStaffCapacity->getListLevelGroupBy($staff_capacity['id']);

    }
    else{
        //$capacity_level = $QCapacity->getListLevel($capacity['capacity_id']);

        $capacity_level = $QCapacity->getListLevelGroupBy($capacity['capacity_id']);

    }

    if($staff_capacity){
        $capacity_science = $QStaffCapacity->getListScience($staff_capacity['id']);
    }
    else{
        $capacity_science = $QCapacity->getListScience($capacity['capacity_id']);
    }

    $dictionary_science = $QCapacity->getDictionaryScience();
    
    $this->view->capacity = $capacity;
    $this->view->capacity_info = $capacity_info;
    $this->view->capacity_level = $capacity_level;
    $this->view->dictionary_level = $dictionary_level;
    $this->view->capacity_science = $capacity_science;
    $this->view->dictionary_science = $dictionary_science;
    
    
//    echo json_encode([
//        'status' => 0,
//        'capacity' => $capacity,
//        'capacity_info' => $capacity_info,
//        'capacity_level' => $capacity_level,
//        'dictionary_level_all' => $dictionary_level_all,
//        'capacity_science' => $capacity_science,
//        'capacity_plan' => $capacity_plan,
//        'dictionary_science' => $dictionary_science
//    ]);
    
    $capacity_plan = $QCapacityPlan->fetchRow(['id = ?' => $plan_id]);
    $this->view->capacity_plan = $capacity_plan;
    
    