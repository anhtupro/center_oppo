<?php

    $id = $this->getRequest()->getParam('id');

    $params = array(
        'id' => $id
    );

    $QCapacityDictionary = new Application_Model_CapacityDictionary();
    $QCapacityDictionaryLevel = new Application_Model_CapacityDictionaryLevel();
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    
    if($id){
        $dictionary = $QCapacityDictionary->fetchRow(['id = ?' => $id]);
        $dictionary_level = $QCapacityDictionaryLevel->fetchAll(['dictionary_id = ?' => $id]);

        $level = [];
        foreach($dictionary_level as $key=>$value){
            $level[$value['level']] = $value['title'];
        }
    }
    
    
    if($this->getRequest()->isPost())
    {
        
        $db = Zend_Registry::get('db');
        try {
            $db->beginTransaction();

            $id = $this->getRequest()->getParam('id');
            $title = $this->getRequest()->getParam('title');
            $type = $this->getRequest()->getParam('type');
            $desc = $this->getRequest()->getParam('desc');
            $level = $this->getRequest()->getParam('level');
            $delete = $this->getRequest()->getParam('delete');

            $data = [
                'title' => $title,
                'type' => $type,
                'desc' => $desc,
                'is_deleted' => $delete
            ];

            if($id){
                $QCapacityDictionary->update($data, ['id = ?' => $id]);
                
                foreach($level as $key=>$value){
                    $QCapacityDictionaryLevel->update(['title' => $value], ['dictionary_id = ?' => $id, 'level = ?' => $key]);
                }
                
            }
            else{
                
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_by'] = $userStorage->id;
                
                $id = $QCapacityDictionary->insert($data);
                foreach($level as $key=>$value){
                    $QCapacityDictionaryLevel->insert([
                        'dictionary_id' => $id, 
                        'level' => $key, 
                        'title' => $value
                    ]);
                }
            }
            
            $db->commit();
            
            $back_url = '/capacity/dictionary';
            $this->_redirect($back_url);
            
        } catch (Exception $e) {

            $db->rollback();

            echo 'Caught exception: ', $e->getMessage(), "\n";exit;
        }
        
        
    }
    
    $this->view->level = $level;
    $this->view->params = $params;
    $this->view->dictionary = $dictionary;
    
    $this->view->level_storage = $this->storage['level'];
    
    // Messages
    $flashMessenger = $this->_helper->flashMessenger;

    $messages = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages;

    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

