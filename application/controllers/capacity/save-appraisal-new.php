<?php

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    /** @var Zend_Controller_Request_Http $request */
    $request = $this->getRequest();
    if (!$request->isPost()) {
        echo json_encode([
            'status' => 1,
            'message' => 'Wrong method submitted!'
        ]);
        return;
    }

    $params = $this->getRequest()->getParams();

    $data_send = $params['dictionary'];
    $staff_id = $params['staff_id'];
    $capacity_id = $params['capacity_id'];
    $plan_id = $params['plan_id'];
    $recommend = $params['recommend'];
    $note_capacity = $params['note_capacity'];
    
    $data_appraisal = [];
    foreach($data_send as $key=>$value){
        $data_appraisal[$value['type_id']][$value['dictionary_id']] = $value['level_select'];
    }
    
    try {

        $QStaff = new Application_Model_Staff();
        $QCapacity = new Application_Model_Capacity();
        $QCapacityField = new Application_Model_CapacityField();
        $QCapacityFieldDictionary = new Application_Model_CapacityFieldDictionary();
        $QCapacityScience = new Application_Model_CapacityScience();
        
        $QStaffCapacity = new Application_Model_StaffCapacity();
        $QStaffCapacityField = new Application_Model_StaffCapacityField();
        $QStaffCapacityFieldDictionary = new Application_Model_StaffCapacityFieldDictionary();
        $QStaffCapacityScience = new Application_Model_StaffCapacityScience();
        $QCapacityRecommend = new Application_Model_CapacityRecommend();
        $CapacityRecommendDetails = new Application_Model_CapacityRecommendDetails();
        
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $where_capacity = [];
        $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('plan_id = ?', $plan_id);
        $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('is_head_appraisal = 1');
        $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('is_deleted = 0');
        
        $staff_capacity = $QStaffCapacity->update(['is_deleted' => 1], $where_capacity);
       
        if(!empty($staff_id)){

            $capacity = $QCapacity->getCapacityByStaffDetails($staff_id);
            if(!$capacity){
                $capacity = $QCapacity->getCapacityByStaff($staff_id);
            }

            $capacity_id = $capacity['capacity_id'];

            $capacity_field = $QCapacityField->fetchAll(['capacity_id = ?'=>$capacity_id, 'is_deleted = ?'=>0]);

            $data_capacity = [
                'staff_id' => $staff_id,
                'plan_id' => $plan_id,
                'title_id' => $capacity['title_id'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'is_head_appraisal' => 1,
                'note' => $note_capacity
            ];

            $staff_capacity_id = $QStaffCapacity->insert($data_capacity);
            
            //Insert vào capacity_recommend
            $data_recommend = [
                'staff_capacity_id' => $staff_capacity_id,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'is_head_appraisal' => 1,
            ];
            $capacity_recommend_id = $QCapacityRecommend->insert($data_recommend);
            foreach($recommend as $key=>$value){
                $data_recommend_details = [
                    'recommend_id' => $capacity_recommend_id,
                    'dictionary_id' => $value,
                ];
                $CapacityRecommendDetails->insert($data_recommend_details);
            }
            

            foreach ($capacity_field as $key=>$value){
                $data_field = [
                    'capacity_id' => $staff_capacity_id,
                    'title' => $value['title'],
                    'desc' => $value['desc'],
                ];

                $staff_capacity_field_id = $QStaffCapacityField->insert($data_field);

                $capacity_field_dictionary = $QCapacityFieldDictionary->fetchAll(['field_id = ?'=>$value['id'], 'is_deleted = ?'=>0]);
                $capacity_science = $QCapacityScience->fetchAll(['capacity_field_id = ?'=>$value['id'], 'is_deleted = ?'=>0]);

                
                foreach ($capacity_field_dictionary as $key=>$v){
                    
                    if(!empty($data_appraisal[1][$v['dictionary_id']])){
                        $data_field_dictionary = [
                            'field_id' => $staff_capacity_field_id,
                            'dictionary_id' => $v['dictionary_id'],
                            'level' => $v['level'],
                            'share' => $v['share'],
                            'appraisal_head_level' => $data_appraisal[1][$v['dictionary_id']]
                        ];
                        $QStaffCapacityFieldDictionary->insert($data_field_dictionary);
                    }
                    
                    
                }

                foreach ($capacity_science as $k=>$v){
                    $data_field_dictionary = [
                        'capacity_field_id' => $staff_capacity_field_id,
                        'desc' => $v['desc'],
                        'share' => $v['share'],
                        'level' => $v['level'],
                        'appraisal_head_level' => $data_appraisal[2][15]
                    ];
                    $QStaffCapacityScience->insert($data_field_dictionary);
                }

            }

            echo json_encode([
                'status' => 0,
                'capacity' => $capacity,
            ]);

        }
        else{
            echo json_encode([
                'status' => 1,
                'message' => 'Empty staff_id'
            ]);
        }



    } catch (\Exception $ex) {
        echo json_encode([
            'status' => 1,
            'message' => $ex->getMessage()
        ]);
    }

