<?php

    $staff_id = $this->getRequest()->getParam('staff_id');
    
    $QStaff = new Application_Model_Staff();
    $QTeam = new Application_Model_Team();
    
    $staff = $QStaff->fetchRow(['id = ?' => $staff_id]);
    
    $this->view->staff = $staff;

