<?php

    $params = $this->getRequest()->getParams();
    $capacity_id = $params['capacity_id'];
    
    $QCapacity = new Application_Model_Capacity();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $capacity = $QCapacity->getInfoCapacity($capacity_id);
    
    //Kiểm tra có thiết lập riêng hay ko
    if($capacity['staff_id']){
        $capacity = $QCapacity->getInfoCapacityStaff($capacity['staff_id']);
    }

    $capacity_level = $QCapacity->getListLevelGroupBy($capacity_id);

    $capacity_science = $QCapacity->getListScience($capacity_id);
    
    $get_dictionary_science = $QCapacity->getDictionaryScience();
    $dictionary_science = [];
    foreach($get_dictionary_science as $key=>$value){
        $dictionary_science[$value['level']] = $value['title'];
    }
    
    $this->view->capacity = $capacity;
    $this->view->capacity_id = $capacity_id;
    $this->view->capacity_level = $capacity_level;
    $this->view->capacity_science = $capacity_science;
    $this->view->dictionary_science = $dictionary_science;

