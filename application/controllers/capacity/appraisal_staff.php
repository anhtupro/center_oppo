<?php

    $QCapacityPlan = new Application_Model_CapacityPlan();

    $capacity_plan = $QCapacityPlan->fetchAll(['is_deleted = 0']);
    $this->view->capacity_plan = $capacity_plan;
    