<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $QTeam = new Application_Model_Team();
    $QCapacityPlan = new Application_Model_CapacityPlan();
    $QCapacity = new Application_Model_Capacity();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    
    $plan = $QCapacityPlan->fetchAll(['is_deleted = 0']);
    
    $plan_year = $QCapacityPlan->fetchAll(['is_year = 1']);
    
    $level = $this->storage['level'];
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
        'department_id' => $userStorage->department
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $page = $this->getRequest()->getParam('page', 1);
    $limit = 20;
    $total = 0;
    
    
    $params['level'] = 3;
    $result = $QCapacity->fetchPaginationResult($page, $limit, $total, $params);
    
    $list_staff = [];
    $data = [];
    
    foreach($result as $key=>$value){
        $list_staff[] = $value['staff_id'];
        $data['is_staff_appraisal'][$value['plan_id']][$value['is_staff_appraisal']] = $value['staff_capacity_id'];
        $data['is_head_appraisal'][$value['plan_id']][$value['is_head_appraisal']] = $value['staff_capacity_id'];
        
        $data['title_name'] = $value['title_name'];
    }
    
    
    $params['list_staff'] = $list_staff;
    
    $this->view->plan = $plan;
    
    $this->view->is_admin = $this->is_admin;
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->dictionary_capacity = $dictionary_capacity;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->data = $data;
    
    
    $this->view->plan_year = $plan_year;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'capacity/list-capacity' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    
    