<?php

    $capacity_id = $this->getRequest()->getParam('id');
    $recomment_id = $this->getRequest()->getParam('re');
    $QCapacity = new Application_Model_Capacity();
    $QStaffCapacity = new Application_Model_StaffCapacity();
    $QCapacityDictionary = new Application_Model_CapacityDictionary();
    $QCapacityRecommend = new Application_Model_CapacityRecommend();
    $QHrCourse = new Application_Model_HrCourse();
    
    $capacity_info = $QStaffCapacity->getInfoCapacity($capacity_id);
    $staff = $QCapacity->getInfoStaff($capacity_info['staff_id']);
    $dictionary_list = $QStaffCapacity->getDictionaryStaff($capacity_id);
    $dictionary_list_group = $QStaffCapacity->getDictionaryStaffGroup($capacity_id);
    
    $science_list = $QStaffCapacity->getScienceStaff($capacity_id);
    
    $dictionary_science = $QCapacity->getDictionaryScience();
    
    
    $hr_course = $QHrCourse->getListCourse($capacity_info['staff_id'], 0);
    
    $science_name = [];
    foreach($dictionary_science as $key=>$value){
        $science_name[$value['level']] = $value['title'];
    }
    
    
    $total_point = 0;
    $total_share = 0;
    foreach($dictionary_list as $key=>$value){
        $total_share = $total_share + $value['share'];
    }
    foreach($science_list as $key=>$value){
        $total_share = $total_share + $value['share'];
    }
    
    $dictionary_chart = [];
    foreach($dictionary_list_group as $key=>$value){
        $dictionary_chart[$value['type']][] = [
            'dictionary_id' => $value['dictionary_id'],
            'title_name' => trim($value['title_name']),
            'level' => $value['level'],
            'appraisal_head_level' => $value['appraisal_head_level'],
            'appraisal_staff_level' => $value['appraisal_staff_level']
        ];
    }
    
    foreach($dictionary_list as $key=>$value){
        //$total_point = $total_point + (($value['appraisal_head_level']/$value['level'])*$value['share']);
        $total_point = $total_point + ((min($value['appraisal_head_level'], $value['level'])/$value['level'])*$value['share']);
        //echo $value['appraisal_head_level']."__".$value['level']."__".$value['share'].":::".(($value['appraisal_head_level']/$value['level'])*$value['share'])."<br>";
    }
    
    
    $total_level = 0;
    $total_appraisal_head_level = 0;
    $total_appraisal_staff_level = 0;
    foreach($science_list as $key=>$value){
        $total_level = $total_level + $value['level'];
        $total_appraisal_head_level = $total_appraisal_head_level + $value['appraisal_head_level'];
        $total_appraisal_staff_level = $total_appraisal_staff_level + $value['appraisal_staff_level'];
        
        //$total_point = $total_point + ($value['appraisal_head_level']/$value['level'])*$value['share'];
        $total_point = $total_point + (min($value['appraisal_head_level'], $value['level'])/$value['level'])*$value['share'];
        //echo $value['appraisal_head_level']."__".$value['level']."__".$value['share'].":::".($value['appraisal_head_level']/$value['level'])*$value['share']."<br>";
    }
    
    $science_chart = [
        'title_name' => 'Kiến thức chuyên môn',
        'level' => (int)($total_level/count($science_list)),
        'appraisal_head_level' => (int)($total_appraisal_head_level/count($science_list)),
        'appraisal_staff_level' => (int)($total_appraisal_staff_level/count($science_list)),
    ];
    
    
    $all_dictionary = $QCapacityDictionary->getDictionaryLevel();
    
    $dictionary = [];
    foreach($all_dictionary as $key=>$value){
        $dictionary[$value['id']][] = $value;
    }
    
    //Lấy kết quả nhân viên tự đánh giá
    $where_capacity = [];
    $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('staff_id = ?', $capacity_info['staff_id']);
    $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('plan_id = ?', $capacity_info['plan_id']);
    $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('is_staff_appraisal = 1');
    $where_capacity[] = $QStaffCapacity->getAdapter()->quoteInto('is_deleted = 0');

    $staff_capacity = $QStaffCapacity->fetchRow($where_capacity);
    if($staff_capacity){
        $dictionary_list_group_staff = $QStaffCapacity->getDictionaryStaffGroup($staff_capacity['id']);
        
        $dictionary_chart_staff = [];
        foreach($dictionary_list_group_staff as $key=>$value){
            $dictionary_chart_staff[$value['type']][$value['dictionary_id']] = [
                'title_name' => trim($value['title_name']),
                'level' => $value['level'],
                'appraisal_staff_level' => $value['appraisal_staff_level']
            ];
        }
        
        $science_list_staff = $QStaffCapacity->getScienceStaff($staff_capacity['id']);
        
        $total_level = 0;
        $total_appraisal_head_level = 0;
        $total_appraisal_staff_level = 0;
        foreach($science_list_staff as $key=>$value){
            $total_level = $total_level + $value['level'];
            $total_appraisal_head_level = $total_appraisal_head_level + $value['appraisal_head_level'];
            $total_appraisal_staff_level = $total_appraisal_staff_level + $value['appraisal_staff_level'];

            //$total_point = $total_point + ($value['appraisal_head_level']/$value['level'])*$value['share'];
            $total_point = $total_point + (min($value['appraisal_head_level'], $value['level'])/$value['level'])*$value['share'];
            //echo $value['appraisal_head_level']."__".$value['level']."__".$value['share'].":::".($value['appraisal_head_level']/$value['level'])*$value['share']."<br>";
        }
        
        $science_chart_staff = [
            'title_name' => 'Kiến thức chuyên môn',
            'level' => (int)($total_level/count($science_list_staff)),
            'appraisal_head_level' => (int)($total_appraisal_head_level/count($science_list_staff)),
            'appraisal_staff_level' => (int)($total_appraisal_staff_level/count($science_list_staff)),
        ];
        
        $this->view->dictionary_chart_staff = $dictionary_chart_staff;
        $this->view->science_chart_staff = $science_chart_staff;
        
    }
    
    
    
    //END - Kết quả nhân viên tự đánh giá
    
    $directory_recommed = $QCapacityRecommend->getRecommend($capacity_id);
    $directory_recommed_staff = $QCapacityRecommend->getRecommendStaff($recomment_id);
    
    $this->view->capacity_info = $capacity_info;
    $this->view->staff = $staff;
    $this->view->dictionary_list = $dictionary_list;
    $this->view->science_list = $science_list;
    $this->view->science_chart = $science_chart;
    $this->view->total_point = $total_point/$total_share*100;
    $this->view->total_share = $total_share;
    $this->view->science_name = $science_name;
    $this->view->dictionary_chart = $dictionary_chart;
    $this->view->dictionary = $dictionary;
    $this->view->directory_recommed_staff = $directory_recommed_staff;
    $this->view->directory_recommed = $directory_recommed;
    $this->view->hr_course = $hr_course;
    
    
