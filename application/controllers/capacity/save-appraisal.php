<?php

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    /** @var Zend_Controller_Request_Http $request */
    $request = $this->getRequest();
    if (!$request->isPost()) {
        echo json_encode([
            'status' => 1,
            'message' => 'Wrong method submitted!'
        ]);
        return;
    }

    $params = $this->getRequest()->getParams();

    $data_send = $params['data_send'];
    $staff_id = $params['staff_id'];
    $capacity_id = $params['capacity_id'];
    $plan_id = $params['plan_id'];
    
    $data_appraisal = [];
    foreach($data_send as $key=>$value){
        $data_appraisal[$value['type_id']][$value['field_id']][$value['dictionary_id']] = $value['level_select'];
    }
    
    try {

        $QStaff = new Application_Model_Staff();
        $QCapacity = new Application_Model_Capacity();
        $QCapacityField = new Application_Model_CapacityField();
        $QCapacityFieldDictionary = new Application_Model_CapacityFieldDictionary();
        $QCapacityScience = new Application_Model_CapacityScience();
        
        
        $QStaffCapacity = new Application_Model_StaffCapacity();
        $QStaffCapacityField = new Application_Model_StaffCapacityField();
        $QStaffCapacityFieldDictionary = new Application_Model_StaffCapacityFieldDictionary();
        $QStaffCapacityScience = new Application_Model_StaffCapacityScience();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $staff_capacity = $QStaffCapacity->fetchRow(['staff_id = ?' => $staff_id, 'plan_id = ?' => $plan_id, 'is_deleted = ?' => 0]);
        if($staff_capacity['is_head_appraisal'] == 1){
            echo json_encode([
                'status' => 2,
                'message' => 'Đã đánh giá'
            ]);
            exit;
        }
        
        if($staff_capacity){
            
            $QStaffCapacity->update(['is_head_appraisal' => 1], ['id = ?' => $staff_capacity['id']]);
            
            foreach($data_send as $key=>$value){
                if($value['type_id'] == 1){
                    $data_up = [
                        'appraisal_head_level' => $value['level_select']
                    ];
                    $QStaffCapacityFieldDictionary->update($data_up, ['id = ?' => $value['staff_capacity_field_dictionary_id']]);
                }
                elseif($value['type_id'] == 2){
                    $data_up = [
                        'appraisal_head_level' => $value['level_select']
                    ];
                    $QStaffCapacityScience->update($data_up, ['id = ?' => $value['capacity_science_id']]);
                }
                
            }

            echo json_encode([
                'status' => 0,
                'capacity' => $capacity,
            ]);
            
        }
        else{
            if(!empty($staff_id)){

                $capacity = $QCapacity->getCapacityByStaffDetails($staff_id);
                if(!$capacity){
                    $capacity = $QCapacity->getCapacityByStaff($staff_id);
                }
                
                $capacity_id = $capacity['capacity_id'];

                $capacity_field = $QCapacityField->fetchAll(['capacity_id = ?'=>$capacity_id, 'is_deleted = ?'=>0]);

                $data_capacity = [
                    'staff_id' => $staff_id,
                    'plan_id' => $plan_id,
                    'title_id' => $capacity['title_id'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id,
                    'is_head_appraisal' => 1
                ];

                $staff_capacity_id = $QStaffCapacity->insert($data_capacity);

                foreach ($capacity_field as $key=>$value){
                    $data_field = [
                        'capacity_id' => $staff_capacity_id,
                        'title' => $value['title'],
                        'desc' => $value['desc'],
                    ];

                    $staff_capacity_field_id = $QStaffCapacityField->insert($data_field);

                    $capacity_field_dictionary = $QCapacityFieldDictionary->fetchAll(['field_id = ?'=>$value['id'], 'is_deleted = ?'=>0]);
                    $capacity_science = $QCapacityScience->fetchAll(['capacity_field_id = ?'=>$value['id'], 'is_deleted = ?'=>0]);


                    foreach ($capacity_field_dictionary as $key=>$v){
                        $data_field_dictionary = [
                            'field_id' => $staff_capacity_field_id,
                            'dictionary_id' => $v['dictionary_id'],
                            'level' => $v['level'],
                            'share' => $v['share'],
                            'appraisal_head_level' => $data_appraisal[1][$value['id']][$v['dictionary_id']]
                        ];
                        $QStaffCapacityFieldDictionary->insert($data_field_dictionary);
                    }

                    foreach ($capacity_science as $k=>$v){
                        $data_field_dictionary = [
                            'capacity_field_id' => $staff_capacity_field_id,
                            'desc' => $v['desc'],
                            'share' => $v['share'],
                            'level' => $v['level'],
                            'appraisal_head_level' => $data_appraisal[2][$value['id']][$v['id']]
                        ];
                        $QStaffCapacityScience->insert($data_field_dictionary);
                    }

                }

                echo json_encode([
                    'status' => 0,
                    'capacity' => $capacity,
                ]);

            }
            else{
                echo json_encode([
                    'status' => 1,
                    'message' => 'Empty staff_id'
                ]);
            }
        }//$staff_capacity



    } catch (\Exception $ex) {
        echo json_encode([
            'status' => 1,
            'message' => $ex->getMessage()
        ]);
    }

