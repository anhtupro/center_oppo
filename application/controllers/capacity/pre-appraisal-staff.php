<?php
    
    $staff_id = $this->getRequest()->getParam('staff_id');
    
    $QCapacity = new Application_Model_Capacity();
    $QCapacityPlan = new Application_Model_CapacityPlan();
    $QStaff = new Application_Model_Staff();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    if ($userStorage->id != $staff_id) {
        $this->_redirect(HOST.'capacity');
    }
    
    $capacity = $QCapacity->fetchRow(['staff_id = ?' => $staff_id]);
    
    if(!$capacity){
        $capacity = $QCapacity->fetchRow(['title_id = ?' => $userStorage->title, 'is_lock = ?' => 1]);
    }
    
    $capacity_plan = $QCapacityPlan->fetchAll(['is_deleted = 0']);
    $this->view->capacity_plan = $capacity_plan;
    $this->view->capacity = $capacity;