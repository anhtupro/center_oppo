<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');
    $appraisal = $this->getRequest()->getParam('appraisal');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $QTeam = new Application_Model_Team();
    $QCapacity = new Application_Model_Capacity();
    $QStaffCapacity = new Application_Model_StaffCapacity();
    $QCapacityPlan = new Application_Model_CapacityPlan();
    
    $plan = $QCapacityPlan->fetchAll(['is_deleted = 0']);

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    
    $level = $this->storage['level'];
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
        'department_id' => $userStorage->department,
        'appraisal' => $appraisal,
        'export' => $export
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $page = $this->getRequest()->getParam('page', 1);
    $limit = 20;
    $total = 0;
    $result = $QCapacity->fetchPaginationStaff($page, $limit, $total, $params);
    
    $list_staff = [];
    foreach($result as $key=>$value){
        $list_staff[] = $value['id'];
    }
    
    $params['list_staff'] = $list_staff;
    $staff_capacity = $QCapacity->getCapacityByListStaff($params);
    
    $staff_appraisal = [];
    $head_appraisal = [];
    foreach($staff_capacity as $key=>$value){
        if($value['is_staff_appraisal'] == 1){
            $staff_appraisal[$value['staff_id']][$value['plan_id']] = $value['id'];
        }
        
        if($value['is_head_appraisal'] == 1){
            $head_appraisal[$value['staff_id']][$value['plan_id']] = $value['id'];
        }
    }

    $this->view->plan = $plan;
    
    $this->view->appraisal = $appraisal;
    $this->view->staff_appraisal = $staff_appraisal;
    $this->view->head_appraisal = $head_appraisal;
    
    $this->view->is_admin = $this->is_admin;
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'capacity/list-staff' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    
    