<?php

    $title_id = $this->getRequest()->getParam('title_id');
    
    $QTeam = new Application_Model_Team();
    
    $title = $QTeam->fetchRow(['id = ?' => $title_id]);
    
    $department = $QTeam->get_list_department();
    
    $this->view->title = $title;
    $this->view->level = $this->storage['level'];

    $this->view->department = $department;