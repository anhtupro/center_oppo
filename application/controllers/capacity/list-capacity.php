<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');
    $export_title = $this->getRequest()->getParam('export_title');
    $export_staff= $this->getRequest()->getParam('export_staff');
    
    $export_title_setup = $this->getRequest()->getParam('export_title_setup');
    $export_staff_setup = $this->getRequest()->getParam('export_staff_setup');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $QTeam = new Application_Model_Team();
    $QCapacity = new Application_Model_Capacity();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    
    $level = $this->storage['level'];
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
        'department_id' => $userStorage->department,
        'export' => $export
    );
    

    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $page = $this->getRequest()->getParam('page', 1);
    $limit = 20;
    $total = 0;
    $result = $QCapacity->fetchPagination($page, $limit, $total, $params);

    if ($export_title) {
        $QCapacity->exportTitle($params);
    }

    if ($export_staff) {
        $QCapacity->exportStaff($params);
    }
    
    if ($export_title_setup) {
        $QCapacity->exportTitleSetup($params);
    }

    if ($export_staff_setup) {
        $QCapacity->exportStaffSetup($params);
    }
    

    $this->view->plan = $plan;
    
    $this->view->is_admin = $this->is_admin;
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'capacity/list-capacity' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    
    