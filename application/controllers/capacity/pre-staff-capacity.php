<?php
    
    $title_id = $this->getRequest()->getParam('title_id');
    
    $QCapacity = new Application_Model_Capacity();
    $QCapacityPlan = new Application_Model_CapacityPlan();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $staff_list = $QCapacity->getStaffList($title_id);
    
    $info_title = $QCapacity->getInfoTitle($title_id);
    
    $capacity_plan = $QCapacityPlan->fetchAll(['is_deleted = 0']);
    
    $this->view->capacity_plan = $capacity_plan;
    $this->view->staff_list = $staff_list;
    $this->view->info_title = $info_title;