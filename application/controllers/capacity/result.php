<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $QTeam = new Application_Model_Team();
    $QCapacityPlan = new Application_Model_CapacityPlan();
    $QCapacity = new Application_Model_Capacity();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    
    $plan = $QCapacityPlan->fetchAll(['is_deleted = 0']);
    
    $level = $this->storage['level'];
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
        'department_id' => $userStorage->department
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $page = $this->getRequest()->getParam('page', 1);
    $limit = 30;
    $total = 0;
    $result = $QCapacity->fetchPaginationResult($page, $limit, $total, $params);
    
    $total_point = 0;
    $index_point = 0;
    foreach ($result as $key=>$value){
        $total_point = $total_point + $value['point'];
        $index_point = $index_point + 1;
    }
    $avg_point = number_format($total_point/$index_point, 2);
    
    $list_staff = [];
    foreach($result as $key=>$value){
        $list_staff[] = $value['staff_id'];
    }
    
    $params['list_staff'] = $list_staff;
    
    $dictionary = $QCapacity->getDictionaryStaff($params);
    
    $dictionary_capacity = [];
    $dictionary_list = [];
    foreach($dictionary as $key=>$value){
        $dictionary_capacity[$value['capacity_id']][$value['dictionary_id']] = $value;
        $dictionary_list[$value['dictionary_id']] = $value['dictionary_name'];
    }
    
    $this->view->plan = $plan;
    
    $this->view->is_admin = $this->is_admin;
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->dictionary_capacity = $dictionary_capacity;
    $this->view->dictionary_list = $dictionary_list;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->avg_point = $avg_point;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'capacity/list-capacity' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    
    