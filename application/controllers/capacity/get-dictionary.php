<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    /** @var Zend_Controller_Request_Http $request */
    $request = $this->getRequest();
    if (!$request->isPost()) {
        echo json_encode([
            'status' => 1,
            'message' => 'Wrong method submitted!'
        ]);
        return;
    }

    $params = $this->getRequest()->getParams();
    $staff_id = $params['staff_id'];
    $plan_id = $params['plan_id'];

    try {

        $QStaff = new Application_Model_Staff();
        $QCapacity = new Application_Model_Capacity();
        $QStaffCapacity = new Application_Model_StaffCapacity();
        $QCapacityPlan = new Application_Model_CapacityPlan();
        $QCapacityDictionaryLevel = new Application_Model_CapacityDictionaryLevel();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $dictionary_level_all = $QCapacityDictionaryLevel->fetchAll(null, 'level DESC')->toArray();
        
        $staff_capacity = $QStaffCapacity->fetchRow(['staff_id = ?' => $staff_id, 'plan_id = ?' => $plan_id, 'is_deleted = ?' => 0]);
        
        if(!empty($staff_id)){

            $capacity_plan = $QCapacityPlan->fetchAll(['is_deleted = 0'])->toArray();
            
            //Kiểm tra có thiết lập capacity riêng chưa
            $capacity = $QCapacity->getCapacityByStaffDetails($staff_id);
            $capacity_info = $QCapacity->getInfoCapacityStaff($staff_id);
            
            if(!$capacity){
                $capacity = $QCapacity->getCapacityByStaff($staff_id);
                $capacity_info = $QCapacity->getInfoCapacity($capacity['capacity_id']);
            }
            //END Kiểm tra có thiết lập capacity riêng chưa
            
            if($staff_capacity){
                //$capacity_level = $QStaffCapacity->getListLevel($staff_capacity['id']);
                
                $capacity_level = $QStaffCapacity->getListLevelGroupBy($staff_capacity['id']);
                
            }
            else{
                //$capacity_level = $QCapacity->getListLevel($capacity['capacity_id']);
                
                $capacity_level = $QCapacity->getListLevelGroupBy($capacity['capacity_id']);
                
            }
            
            if($staff_capacity){
                $capacity_science = $QStaffCapacity->getListScience($staff_capacity['id']);
            }
            else{
                $capacity_science = $QCapacity->getListScience($capacity['capacity_id']);
            }
            
            $dictionary_science = $QCapacity->getDictionaryScience();

            echo json_encode([
                'status' => 0,
                'capacity' => $capacity,
                'capacity_info' => $capacity_info,
                'capacity_level' => $capacity_level,
                'dictionary_level_all' => $dictionary_level_all,
                'capacity_science' => $capacity_science,
                'capacity_plan' => $capacity_plan,
                'dictionary_science' => $dictionary_science
            ]);

        }
        else{
            echo json_encode([
                'status' => 1,
                'message' => 'Empty staff_id'
            ]);
        }



    } catch (\Exception $ex) {
        echo json_encode([
            'status' => 1,
            'message' => $ex->getMessage()
        ]);
    }
    
    
    