<?php

    $capacity_id = $this->getRequest()->getParam('staff_capacity_id');
    
    $QCapacity = new Application_Model_Capacity();
    $QStaffCapacity = new Application_Model_StaffCapacity();
    $QCapacityDictionary = new Application_Model_CapacityDictionary();
    $QCapacityRecommend = new Application_Model_CapacityRecommend();
    
    $capacity_info = $QStaffCapacity->getInfoCapacity($capacity_id);
    $staff = $QCapacity->getInfoStaff($capacity_info['staff_id']);
    $dictionary_list = $QStaffCapacity->getDictionaryStaff($capacity_id);
    $dictionary_list_group = $QStaffCapacity->getDictionaryStaffGroup($capacity_id);
    
    $science_list = $QStaffCapacity->getScienceStaff($capacity_id);
    
    $dictionary_science = $QCapacity->getDictionaryScience();
    
    
    $science_name = [];
    foreach($dictionary_science as $key=>$value){
        $science_name[$value['level']] = $value['title'];
    }
    
    
    $total_point = 0;
    $total_share = 0;
    foreach($dictionary_list as $key=>$value){
        $total_share = $total_share + $value['share'];
    }
    foreach($science_list as $key=>$value){
        $total_share = $total_share + $value['share'];
    }
    
    $dictionary_chart = [];
    foreach($dictionary_list_group as $key=>$value){
        $dictionary_chart[$value['type']][] = [
            'title_name' => trim($value['title_name']),
            'level' => $value['level'],
            'appraisal_staff_level' => $value['appraisal_staff_level']
        ];        
        
        
    }
    
    foreach($dictionary_list as $key=>$value){
        //$total_point = $total_point + (($value['appraisal_staff_level']/$value['level'])*$value['share']);
        $total_point = $total_point + ((min($value['appraisal_staff_level'], $value['level'])/$value['level'])*$value['share']);
        //echo $value['appraisal_staff_level']."__".$value['level']."__".$value['share'].":::".(($value['appraisal_staff_level']/$value['level'])*$value['share'])."<br>";
    }
    
    
    $total_level = 0;
    $total_appraisal_staff_level = 0;
    foreach($science_list as $key=>$value){
        $total_level = $total_level + $value['level'];
        $total_appraisal_staff_level = $total_appraisal_staff_level + $value['appraisal_staff_level'];
        
        //$total_point = $total_point + ($value['appraisal_staff_level']/$value['level'])*$value['share'];
        $total_point = $total_point + (min($value['appraisal_staff_level'], $value['level'])/$value['level'])*$value['share'];
        //echo $value['appraisal_staff_level']."__".$value['level']."__".$value['share'].":::".($value['appraisal_staff_level']/$value['level'])*$value['share']."<br>";
    }
    
    $science_chart = [
        'title_name' => 'Kiến thức chuyên môn',
        'level' => (int)($total_level/count($science_list)),
        'appraisal_staff_level' => (int)($total_appraisal_staff_level/count($science_list)),
    ];
    
    
    $all_dictionary = $QCapacityDictionary->getDictionaryLevel();
    
    $dictionary = [];
    foreach($all_dictionary as $key=>$value){
        $dictionary[$value['id']][] = $value;
    }
    
    $directory_recommed = $QCapacityRecommend->getRecommend($capacity_id);

    foreach ($dictionary_list as $kd => $vd) {
        if ($vd['level'] * $vd['appraisal_staff_level'] == 0) unset($dictionary_list[$kd]);
    }

    $this->view->capacity_info = $capacity_info;
    $this->view->staff = $staff;
    $this->view->dictionary_list = $dictionary_list;
    $this->view->science_list = $science_list;
    $this->view->science_chart = $science_chart;
    $this->view->total_point = $total_point/$total_share*100;
    $this->view->total_share = $total_share;
    $this->view->science_name = $science_name;
    $this->view->dictionary_chart = $dictionary_chart;
    $this->view->dictionary = $dictionary;
    $this->view->directory_recommed = $directory_recommed;
    
