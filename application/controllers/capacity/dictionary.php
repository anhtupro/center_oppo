<?php

    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $sn = $this->getRequest()->getParam('sn');
    $from_name = $this->getRequest()->getParam('from_name');
    $from_code = $this->getRequest()->getParam('from_code');
    $to_name = $this->getRequest()->getParam('to_name');
    $to_code = $this->getRequest()->getParam('to_code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $type = $this->getRequest()->getParam('type');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;

    $QTeam = new Application_Model_Team();
    $QCapacityDictionary = new Application_Model_CapacityDictionary();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();


    $params = array(
        'sn' => $sn,
        'from_name' => $from_name,
        'to_name' => $to_name,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'staff_head_id' => $userStorage->id,
        'is_admin' => $is_admin,
        'from_code' => $from_code,
        'to_code'   => $to_code,
        'type' => $type
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;


    $QMember = new Application_Model_AppraisalOfficeMemberRoot();
    $page = $this->getRequest()->getParam('page', 1);
    $limit = 100;
    $total = 0;
    $result = $QCapacityDictionary->fetchPagination($page, $limit, $total, $params);


    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;
    $this->view->dictionary = $result;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'capacity/dictionary' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    $this->view->level = $this->storage['level'];
    
