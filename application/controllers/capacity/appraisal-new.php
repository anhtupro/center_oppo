<?php
    
    $staff_id = $this->getRequest()->getParam('staff_id');
    $plan_id = $this->getRequest()->getParam('plan_id');
    
    $QCapacityPlan = new Application_Model_CapacityPlan();
    $QCapacityDictionaryLevel = new Application_Model_CapacityDictionaryLevel();
    $QCapacity = new Application_Model_Capacity();
    $QStaffCapacity = new Application_Model_StaffCapacity();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $dictionary_level_all = $QCapacityDictionaryLevel->fetchAll(null, 'level DESC')->toArray();
    $dictionary_level = [];
    foreach($dictionary_level_all as $key=>$value){
        $dictionary_level[$value['dictionary_id']][$value['level']] = $value['title'];
    }
    
    $params = [
        'staff_id' => $staff_id,
        'plan_id'  => $plan_id
    ];
    
    //Kiểm tra có thiết lập capacity riêng chưa
    $capacity = $QCapacity->getCapacityByStaffDetails($staff_id);
    $capacity_info = $QCapacity->getInfoCapacityStaff($staff_id);
    
    $link_view_capacity = '';
    if($capacity){
        $link_view_capacity = '/capacity/view-capacity?capacity_id='.$capacity['capacity_id'];
    }
    else{
        $capacity = $QCapacity->getCapacityByStaff($staff_id);
        $capacity_info = $QCapacity->getInfoCapacity($capacity['capacity_id']);
        
        $link_view_capacity = '/capacity/view-capacity?capacity_id='.$capacity['capacity_id'];
    }
    //END Kiểm tra có thiết lập capacity riêng chưa
    
    $capacity_level = $QCapacity->getListLevelGroupBy($capacity['capacity_id']);
    $capacity_science = $QCapacity->getListScience($capacity['capacity_id']);
    $dictionary_science = $QCapacity->getDictionaryScience();
    
    //Get staff_capacity
    $staff_capacity = $QCapacity->getCapacity($params);
    
    if($staff_capacity){
        $dictionary_list_group = $QStaffCapacity->getDictionaryStaffGroup($staff_capacity['id']);
        $dictionary_chart = [];
        foreach($dictionary_list_group as $key=>$value){
            $dictionary_chart[$value['type']][$value['dictionary_id']] = $value['appraisal_staff_level'];
        }
        
        //Get science
        $total_science_point = 0;
        $science_list = $QStaffCapacity->getScienceStaff($staff_capacity['id']);
        foreach($science_list as $key=>$value){
            $total_science_point = $total_science_point + $value['appraisal_staff_level'];
        }
        
        $this->view->dictionary_chart = $dictionary_chart;
        $this->view->total_science_point = (int)($total_science_point/count($science_list));
        
    }
    
    
    
    $this->view->capacity = $capacity;
    $this->view->capacity_info = $capacity_info;
    $this->view->capacity_level = $capacity_level;
    $this->view->dictionary_level = $dictionary_level;
    $this->view->capacity_science = $capacity_science;
    $this->view->dictionary_science = $dictionary_science;
    $this->view->link_view_capacity = $link_view_capacity;
    
    
    $capacity_plan = $QCapacityPlan->fetchRow(['id = ?' => $plan_id]);
    $this->view->capacity_plan = $capacity_plan;
    
    