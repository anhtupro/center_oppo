<?php
    
    $staff_id = $this->getRequest()->getParam('staff_id');
    $plan_id = $this->getRequest()->getParam('plan_id');
    
    $QCapacityPlan = new Application_Model_CapacityPlan();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    

    $capacity_plan = $QCapacityPlan->fetchRow(['id = ?' => $plan_id]);
    $this->view->capacity_plan = $capacity_plan;
    