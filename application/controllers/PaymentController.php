<?php

class PaymentController extends My_Controller_Action
{
    public function init()
    {
        
        $list_status = [
            1 => 'Chờ confirm',
            2 => 'Đã được confirm',
        ];
        
        $this->view->list_status = $list_status;
        
        $this->_helper->layout->setLayout('layout_payment');
    }
    
    public function indexAction()
    {
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if ($userStorage->email == 'khoinguyen.tran@oppomobile.vn') {
            echo "<pre>";
            var_dump($userStorage);
            exit;
        }
        
        $this->_helper->viewRenderer->setNoRender();
    }
    
    public function listPaymentAction()
    {

        $fullname            = $this->getRequest()->getParam('fullname');
        $content             = $this->getRequest()->getParam('content');
        

        $QPayment = new Application_Model_Payment();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $flashMessenger       = $this->_helper->flashMessenger;

        $limit = LIMITATION;
        $total = 0;
        $params = [
                'fullname' => $fullname,
                'content'  => $content,
        ];


        $result = $QPayment->fetchPagination($page, $limit, $total, $params);

        $this->view->params=$params;
        $this->view->list= $result;
        $this->view->desc = $desc;
        $this->view->sort = $sort;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'payment' . ($params ? '?' . http_build_query($params) .
                '&' : '?');
        $this->view->offset = $limit * ($page - 1);


        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
    }
    
    
    public function createAction()
    {
        $id                  = $this->getRequest()->getParam('id');
        $staff_id            = $this->getRequest()->getParam('staff_id');
        $department_id       = $this->getRequest()->getParam('department_id');
        $content             = $this->getRequest()->getParam('content');
        $price               = $this->getRequest()->getParam('price');
        $invoice_number      = $this->getRequest()->getParam('invoice_number');
        $update_status       = $this->getRequest()->getParam('update_status');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        
        $QTeam = new Application_Model_Team();
        $QPayment = new Application_Model_Payment();
        $QAppNoti = new Application_Model_AppNoti();
        
        $flashMessenger = $this->_helper->flashMessenger;
        $department = $QTeam->getTitleTeamDepartment($userStorage->title);
        
        if($id){
            $payment = $QPayment->getDetail($id);
            $this->view->payment = $payment;
        }
        
        if($this->getRequest()->getMethod()=='POST')
        {
            $data = [
                'department_id'  => $department_id,
                'title_id'       => $department['title'],
                'team_id'        => $department['team'],
                'content'        => $content,
                'price'          => $price,
                'status'         => 1,
                'invoice_number' => $invoice_number,
                'created_by'     => $userStorage->id,
                'created_at'     => date('Y-m-d H:i:s'),
            ];
            
            if(empty($id)){
                $id_payment = $QPayment->insert($data);
                
                $data_noti = [
                    'title' => "Thông báo từ OPPO",
                    'message' => "Bạn có 1 đề nghị thanh toán cần xác nhận",
                    'link' => "/payment/create?id=".$id_payment,
                    'staff_id' => 5899
                ];
                $id_notification = $QAppNoti->insert($data_noti);
                $send_notification = $QAppNoti->sendNotification($id_notification);
                
                
            }
            
            
            if(!empty($id) AND !empty($update_status)){
                $QPayment->update(['status' => 2], ['id = ?' => $id]);
                
                $data_noti = [
                    'title' => "Thông báo từ OPPO",
                    'message' => "Đề nghị thanh toán số ".$id." đã được xác nhận.",
                    'link' => "/payment/create?id=".$id,
                    'staff_id' => 7564
                ];
                $id_notification = $QAppNoti->insert($data_noti);
                $send_notification = $QAppNoti->sendNotification($id_notification);
                
            }
            
            
            $flashMessenger->setNamespace('success')->addMessage('Done!');
            $this->redirect('/payment/create?id='.$id_payment);
            
        }
        
        $this->view->full_name = $userStorage->firstname.' '.$userStorage->lastname;
        $this->view->staff_id = $userStorage->id;
        $this->view->department = $department;
    }
    
    public function createabcAction()
    {
        $this->redirect('/request-office/list-request');
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->setLayout('layout_dashboard');
    }
    
    public function loginTradeAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();
        $QTeam = new Application_Model_Team();
        $QGroup = new Application_Model_GroupTrade();
        $QMenu = new Application_Model_MenuTrade();
        
        $title = $QTeam->find($userStorage->title)->current();
        $group = $QGroup->find($title->access_group)->current();
        
        $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
        $priviledge = $QStaffPriveledge->fetchRow($where);
        $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;
        
        if ($personal_accesses) {
            $userStorage->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
            $menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
        } else {
            $userStorage->accesses = json_decode($group->access);
            $menu = $group->menu ? explode(',', $group->menu) : null;
        }
        
        $where = array();
        $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

        if ($menu)
            $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
        else
            $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

        $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

        $userStorage->menu = $menus;
        
        $this->_redirect('/trade/air-list');
        
    }
    
    public function loginCenterAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $QStaffPriveledge = new Application_Model_StaffPriviledge();
        $QTeam = new Application_Model_Team();
        $QGroup = new Application_Model_Group();
        $QMenu = new Application_Model_Menu();
        
        $title = $QTeam->find($userStorage->title)->current();
        $group = $QGroup->find($title->access_group)->current();
        
        $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
        $priviledge = $QStaffPriveledge->fetchRow($where);
        $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;
        
        if ($personal_accesses) {
            $userStorage->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
            $menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
        } else {
            $userStorage->accesses = json_decode($group->access);
            $menu = $group->menu ? explode(',', $group->menu) : null;
        }
        
        $where = array();
        $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);
        $where[] = $QMenu->getAdapter()->quoteInto('is_app = ?', 1);

        if ($menu)
            $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
        else
            $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

        $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

        $userStorage->menu = $menus;
        
        $this->_redirect('/index');
        
    }
    
    public function dashboardSystemAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->setLayout('layout_dashboard');
    }
    
    public function checkLinkNotificationAction()
    {
        $token = $this->getRequest()->getParam('token');
	$id_notification = $this->getRequest()->getParam('id_notification');
	
	$auth = Zend_Auth::getInstance();
	$QAppNoti = new Application_Model_AppNoti();
	
	$notification = $QAppNoti->fetchRow(['id = ?' => $id_notification]);
	
	if(!empty($notification)){
		//Check token auto login
		if(!empty($token) AND !$auth->hasIdentity()){
			
			$db = Zend_Registry::get('db');
			$auth = Zend_Auth::getInstance();
			$authAdapter = new Zend_Auth_Adapter_DbTable($db);
			$authAdapter->setTableName('staff')
					->setIdentityColumn('email')
					->setCredentialColumn('password');
			$select = $db->select()
					->from(array('p' => 'staff'), array('p.*'));
            $select->where('p.dingtalk_id = ?', $token);
            $select->orWhere('p.token_erp_app = ?', $token);
			$resultStaff = $db->fetchRow($select);

			if (!empty($resultStaff)) {
                            
				$md5_pass = $resultStaff['password'];
				$authAdapter->setIdentity($resultStaff['email']);
				$authAdapter->setCredential($md5_pass);
				$result = $auth->authenticate($authAdapter);
                                
				if ($result->isValid()) {
                                    
					$data = $authAdapter->getResultRowObject(null, 'password');
					$QStaffPriveledge = new Application_Model_StaffPriviledgeErp();

					$where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

					$priviledge = $QStaffPriveledge->fetchRow($where);

					$personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

					$QRegionalMarket = new Application_Model_RegionalMarket();
					$area_id = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
					$data->regional_market_id = $area_id;
                                        
                                        
                                        
					$QTeam = new Application_Model_Team();
					$title = $QTeam->find($data->title)->current();
					$QGroup = new Application_Model_GroupErp();
					$group = $QGroup->find($title->access_group)->current();
					$data->group_id = $title->access_group;
					$data->role = $group->name;

					if ($personal_accesses) {
						// Thanh edit merge   
						$data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
						//$menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
						$menu = explode(',', $personal_accesses['menu']);
					} else {

						$data->accesses = json_decode($group->access);

						$menu = $group->menu ? explode(',', $group->menu) : null;
					}

					$QMenu = new Application_Model_MenuErp();
					$where = array();
					$where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

					if ($menu)
						$where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
					else
						$where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

					$menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

					$data->menu = $menus;
                                        $data->login_from_trade = 1;
                                        
					$auth->getStorage()->write($data);
					if (!empty($resultStaff)) {
						$response['status'] = 1;
						$response['data'] = $resultStaff;
					}
                                        
				}
			}
		}
		//END Check token auto login

		if ($auth->hasIdentity()) {
			$this->_redirect(HOST . $notification['link']);
		}
	}
	
	
	
	echo "Done";exit;
    }
    
}
