<?php
/**
 * Các report của phòng Call Center
 */
class CallCenterReportController extends My_Controller_Action
{
    public function inactiveAction()
    {
        $from        = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to          = $this->getRequest()->getParam('to', date('d/m/Y'));
        $area_id     = $this->getRequest()->getParam('area_id');
        $imei        = $this->getRequest()->getParam('imei');
        $status      = $this->getRequest()->getParam('status');
        $imei_status = $this->getRequest()->getParam('imei_status');

        $page    = 1;
        $limit   = null;
        $total   = 0;
        $params  = array(
            'from'        => $from,
            'to'          => $to,
            'area_id'     => $area_id,
            'imei'        => $imei,
            'status'      => $status,
            'imei_status' => $imei_status,
        );

        $params['inactive'] = 1;
        $params['export'] = 1;

        $QImeiKpi = new Application_Model_ImeiKpi();
        $imeis = $QImeiKpi->fetchDetailPagination($page, $limit, $total, $params);
        unset($params['inactive']);
        unset($params['export']);
        My_Report_CallCenter::inactive($imeis);
    }
}
