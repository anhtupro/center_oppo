<?php

$page         = $this->getRequest()->getParam('page', 1);
$imei         = $this->getRequest()->getParam('imei', null);
$warehouse_id = $this->getRequest()->getParam('warehouse_id');
$type         = $this->getRequest()->getParam('type');
$export       = $this->getRequest()->getParam('export');

$bill_date_from  = $this->getRequest()->getParam('bill_date_from', date('m/d/Y', strtotime('-1 month')));
$bill_date_to    = $this->getRequest()->getParam('bill_date_to');
$created_at_from = $this->getRequest()->getParam('created_at_from');
$created_at_to   = $this->getRequest()->getParam('created_at_to');
$good_id         = $this->getRequest()->getParam('good_id');

$area_id = $this->getRequest()->getParam('area_id');
if ($bill_date_from) {
    $bill_date_from = date("Y-m-d 00:00:00", strtotime($bill_date_from));
}

if ($bill_date_to) {
    $bill_date_to = date("Y-m-d 23:59:59", strtotime($bill_date_to));
}

if ($created_at_from) {
    $created_at_from = date("Y-m-d 00:00", strtotime($created_at_from));
}

if ($created_at_to) {
    $created_at_to = date("Y-m-d 23:59:59", strtotime($created_at_to));
}

$QSelloutPartner = new Application_Model_SelloutPartner();
$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$QArea           = new Application_Model_Area();
$area_cache      = $QArea->get_cache();
$QAsm            = new Application_Model_Asm();
$asm_cache       = $QAsm->get_cache();
$area_list       = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();

if (!empty($area_list)) {
    if (!empty($area_id)) {
        $filter_area = array_intersect($area_list, $area_id);
    } else {
        $filter_area = $area_list;
    }
} else {
    if (!empty($area_id)) {
        $filter_area = $area_id;
    } else {
        $filter_area = $area_list;
    }
}


$params = array(
    'imei'            => $imei,
    'warehouse_id'    => $warehouse_id,
    'bill_date_from'  => $bill_date_from,
    'bill_date_to'    => $bill_date_to,
    'created_at_from' => $created_at_from,
    'created_at_to'   => $created_at_to,
    'export'          => $export,
    'area_id'         => $filter_area,
    'type'            => $type,
    'good_id'         => $good_id
);


if (empty($area_list)) {
    $list_area = $area_cache;
} else {
    foreach ($area_list as $key => $value) {
        $list_area[$value] = $area_cache[$value];
    }
}

$this->view->list_area = $list_area;
$page                  = $this->getRequest()->getParam('page', 1);
$limit                 = LIMITATION;
$total                 = 0;
if ($export) {
    $limit = $page  = 0;
}
$data = $QSelloutPartner->fetchPagination($page, $limit, $total, $params);

if ($export and $export == 1) {
    $this->reportSelloutPartner($data);
}
$QGood             = new Application_Model_Good();
$goods             = $QGood->get_cache_all(11);
$this->view->goods = $goods;

$this->view->params = $params;
$this->view->data   = $data;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->url    = HOST . 'sellout-partner' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset = $limit * ($page - 1);

