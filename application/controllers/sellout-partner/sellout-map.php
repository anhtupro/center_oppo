<?php

$page   = $this->getRequest()->getParam('page', 1);
$min    = $this->getRequest()->getParam('min', 0);
$export = $this->getRequest()->getParam('export');

$sell_out_from   = $this->getRequest()->getParam('sell_out_from', date('m/d/Y', strtotime('-3 month')));
$sell_out_to     = $this->getRequest()->getParam('sell_out_to', date('m/d/Y', strtotime('now')));
$created_at_from = $this->getRequest()->getParam('created_at_from');
$created_at_to   = $this->getRequest()->getParam('created_at_to');

$area_id = $this->getRequest()->getParam('area_id');
if ($sell_out_from) {
    $sell_out_from = date("Y-m-d 00:00:00", strtotime($sell_out_from));
}

if ($sell_out_to) {
    $sell_out_to = date("Y-m-d 23:59:59", strtotime($sell_out_to));
}
$QSelloutPartner = new Application_Model_SelloutPartner();
$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$QArea           = new Application_Model_Area();
$area_cache      = $QArea->get_cache();
$QAsm            = new Application_Model_Asm();
$asm_cache       = $QAsm->get_cache();
$area_list       = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();

if (!empty($area_list)) {
    if (!empty($area_id)) {
        $filter_area = array_intersect($area_list, $area_id);
    } else {
        $filter_area = $area_list;
    }
} else {
    if (!empty($area_id)) {
        $filter_area = $area_id;
    } else {
        $filter_area = $area_list;
    }
}


$params = array(
    'min'           => $min,
    'sell_out_from' => $sell_out_from,
    'sell_out_to'   => $sell_out_to,
    'export'        => $export,
    'area_id'       => $filter_area
);


if (empty($area_list)) {
    $list_area = $area_cache;
} else {
    foreach ($area_list as $key => $value) {
        $list_area[$value] = $area_cache[$value];
    }
}

$this->view->list_area = $list_area;
$page                  = $this->getRequest()->getParam('page', 1);
$limit                 = LIMITATION;
$total                 = 0;
if ($export) {
    $limit = $page  = 0;
}

if (!empty($params['area_id'])) {
    $data = $QSelloutPartner->fetchMap($params);
} else {
    $data = [];
}
$params['sell_out_from']= date("m/d/Y", strtotime($sell_out_from));
$params['sell_out_to']= date("m/d/Y", strtotime($sell_out_to));
//
//if ($export and $export == 1) {
//    $this->reportSelloutPartner($data);
//}

$this->view->params = $params;
$this->view->data   = $data;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->url    = HOST . 'sellout-partner/sellout-map/' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset = $limit * ($page - 1);

