<?php

class StaffTimeController extends My_Controller_Action
{
    public function init()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if(empty($userStorage))
        {
            $this->_redirect("/user/login");
        }

        $QTimeStaffExpired = new Application_Model_TimeStaffExpired();
        $where = array();
        $where[] = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ?' , $userStorage->id );
        $where[] = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null' , null);
        $result  = $QTimeStaffExpired->fetchRow($where);
        if($result)
         {
             $this->redirect('user/lock');
         }
    }
	function daysBetween($dt1, $dt2) {
		return date_diff(
			date_create($dt2),  
			date_create($dt1)
		)->format('%a');
	}
	public function getReasonTmpTimeAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $reason_id           = $this->getRequest()->getParam('reason_id');
        $QReasonTempTime = new Application_Model_ReasonTempTime();
        $where = $QReasonTempTime->getAdapter()->quoteInto('id = ?', $reason_id);
        echo json_encode($QReasonTempTime->fetchRow($where)->toArray());

    }

    public function getReasonTimeLateByTypeAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $type_reason_id           = $this->getRequest()->getParam('type_reason_id');
        $QReasonTimeLate = new Application_Model_ReasonTimeLate();
        $where = $QReasonTimeLate->getAdapter()->quoteInto('type = ?', $type_reason_id);
        echo json_encode($QReasonTimeLate->fetchAll($where)->toArray());

    }
    public function getReasonTimeLateByIdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $id           = $this->getRequest()->getParam('id');
        $QReasonTimeLate = new Application_Model_ReasonTimeLate();
        $where = $QReasonTimeLate->getAdapter()->quoteInto('id = ?', $id);
        echo json_encode($QReasonTimeLate->fetchRow($where)->toArray());

    }
	   
	public function findLocationStaffAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $area_id = 1;
        $title_id = 453;
        $db           = Zend_Registry::get('db');
        $QTeam = new Application_Model_Team();
        $titleRowSet = $QTeam->find($title_id);
        $office_type = $titleRowSet->current()['office_type'];
		        
        if(!empty($office_type)){
          $whereLocation  = $db->select()
                                ->from(array('a'=>'office'),array('a.id', 'a.district'))
                                ->where('a.area_id = ?',$area_id)
                                ->where('a.office_type = ?',$office_type)
				;
                echo $whereLocation->__toString();
	
            $locationRow    = $db->fetchRow($whereLocation);
            echo "<pre>";
            print_r($locationRow);
            echo "</pre>";
        }
    }
	public function hrRemoveTimeAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'hr-remove-time.php';
    }
    public function checkPermissionAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'check-permission.php';
    }
    private function exportPermission($data){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-list-staff-permisson.php';
    }
    public function exportOverTimeWarrantyAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-over-time-warranty.php';
    }
	  public function exportInboundAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-inbound.php';
    }
	 public function infoStaffAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'info-staff.php';
    }
    
    public function getInfoStaffAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'get-info-staff.php';
    }
    
	 public function updateGpsAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'update-gps.php';
    }
	
    public function testGpsAction(){
         $this->_helper->layout->disableLayout();
    }
    
    public function pushAction(){
         $this->_helper->layout->disableLayout();
             $this->_helper->viewRenderer->setNoRender();
    }
    public function pushNotiAction(){
         $this->_helper->layout->disableLayout();
         
    }
    public function testAreaAction(){
		// $distance_office1 = My_DistanceGps::getDistance(10.7443317, 106.7294735, 10.7495406, 106.6805595, "K");
		
		$distance_office2 = My_DistanceGps::getDistance(14.14706738,107.6879421,14.696060,107.667309, "K");
		
      //  echo $distance_office1;
		echo $distance_office2;
		
		/*       
        $db = Zend_Registry::get('db');

	   $select_area = "SELECT  `latitude` ,  `longitude`  FROM time_gps WHERE check_in_day =  '2018-1-2' LIMIT 0,3000";
				
		$select_area = "SELECT  `latitude` ,  `longitude` 
        FROM time_gps tg INNER JOIN staff st ON st.id = tg.staff_id
        WHERE tg.check_in_day =  '2018-1-2' AND st.team = 131";

        $stmt_area = $db->prepare($select_area);
        $stmt_area->execute();
        $result_area = $stmt_area->fetchAll();
     
		echo "<pre>";
        print_r($result_area);
        echo "</pre>";
		
        $this->view->area  = $result_area;
        */
        $this->_helper->layout->disableLayout();
	
    }
//	public function checkAreaAction(){
//        $db = Zend_Registry::get('db');
//        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender();
//        
//        $select_area = "SELECT st.`id`,
//                            st.`code`, 
//                            CONCAT(st.firstname, ' ',st.lastname) as 'name',
//							st.`email`,
//                             t.`name` AS 'title',
//                             str.`latitude`,
//                             str.`longitude`,
//							str.`shipping_address`,
//                             tg.`latitude` AS 'latitude_check_in',
//                             tg.`longitude` AS 'longitude_check_in'
//                               
//                            FROM staff st
//                            INNER JOIN store_staff_log sst ON st.id = sst.`staff_id` AND sst.released_at is null AND sst.is_leader IN (0,3) 
//                            INNER JOIN store str ON sst.`store_id` = str.`id` 
//                            INNER JOIN team t ON st.`title` = t.id
//                            LEFT JOIN time_gps tg ON tg.`staff_id` = st.id AND tg.`check_in_day` = '2018-1-8'
//                            
//                            WHERE
//                            st.`off_date` IS NULL
//							AND tg.staff_id IS NOT NULL
//							AND st.regional_market = 4187
//							ORDER BY st.id
//							";
//                $stmt_area = $db->prepare($select_area);
//                $stmt_area->execute();
//                $result_area = $stmt_area->fetchAll();
//         $stt = 0;
//        foreach ($result_area as $k => $val){
//            if(!empty($val['latitude']) && $val['latitude'] != ""){
//				$distance = $this->distanceGPS($val['latitude'], $val['longitude'], $val['latitude_check_in'], $val['longitude_check_in'], "K");
//                
//                if($distance > 2){
//                    $stt++;
//                    $data = array(
//                        'stt'  => $stt,
//                        'code' => $val['code'],
//                        'name' => $val['name'],
//                        'title' => $val['title'],
//                        'email' => $val['email'],
//                        'latitude' => $val['latitude'],
//                        'longitude' => $val['longitude'],
//                        'shipping_address' =>  $val['shipping_address'],
//                        'latitude_check_in' => $val['latitude_check_in'],
//                        'longitude_check_in' => $val['longitude_check_in'],
//                        'distance'   => $distance
//                    );
//                    echo "<pre>";
//                    print_r($data);
//                    echo "</pre>";
//                }
//                
//            }
//        }
//
//
//    }
	
	public function distanceGPS($lat1, $lon1, $lat2, $lon2, $unit) {
    
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
    
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
    
    public function exportDateAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $db           = Zend_Registry::get('db');
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
        $sql          = "SELECT p.name, p.code, b.ten_quan_huyen, b.ma_quan_huyen, b.ten_tinh_tp, ma_tinh_tp
FROM backup.area_2017 b
LEFT JOIN  `regional_market` p ON p.name = b.ten_quan_huyen
GROUP BY b.ma_quan_huyen";
        
        $stmt = $db->prepare($sql);
        
        $stmt->execute();
        $data_export = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        require_once 'PHPExcel.php';
        
        $alphaExcel = new My_AlphaExcel();
        
        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Tên cũ',
            $alphaExcel->ShowAndUp() => 'Code cũ',
            $alphaExcel->ShowAndUp() => 'Tên mới',
            $alphaExcel->ShowAndUp() => 'Code mới',
            $alphaExcel->ShowAndUp() => 'Tên TP mới',
            $alphaExcel->ShowAndUp() => 'Mã TP mới',
            
        );
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }
        $index = 1;
        foreach($data_export as $key => $val)
        {
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $val['name'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $val['code'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $val['ten_quan_huyen'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $val['ma_quan_huyen'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $val['ten_tinh_tp'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $val['ma_tinh_tp'],PHPExcel_Cell_DataType::TYPE_STRING);
            
            $index ++;
        }
        
        $filename = 'data - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
	    public function testAction(){
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		 $db   = Zend_Registry::get('db');
              $QStore = new Application_Model_Store();
        $list_store = $QStore->get_pg_brand_shop(2875); 
		echo "<pre>";
        print_r($list_store);
        echo "</pre>";
        
    }	
		
	public function getViewDetailApproveLastAction()
    {
        $month = $this->getRequest()->getParam('month');
        $staff_id = $this->getRequest()->getParam('staff_id');
        $year = $this->getRequest()->getParam('year');
        $work_day_string = $this->getRequest()->getParam('work_day_string');
                                
	$p_month_required = $year . "-" . $month . "-01";        
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
                                

        $db   = Zend_Registry::get('db');
        $stmt   = $db->prepare("CALL `get_list_last_month_detail`(:p_month_required, :p_staff_id, :p_work_day_string)");
        $stmt->bindParam('p_month_required', $p_month_required , PDO::PARAM_INT);
        $stmt->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->bindParam('p_work_day_string', My_Util::escape_string($work_day_string), PDO::PARAM_STR);        
        $stmt->execute();
        $data_last = $stmt->fetchAll();
        $stmt->closeCursor(); 
        $stmt = $db = null;
                                
        $html = "<input type='hidden' name='staff_approve' value='" . $data_last[0]['staff_id'] . "'>";
        $html .= "<table class='table'><thead>";
        $html .= "<th>Ngày</th><th>Ngày công</th><th>Nội dung</th><th>Lý do</th><th>Xác nhận</th></thead>";
        $html .= '<tbody>';
        foreach($data_last as $key => $value)
        { 
            $html_tr = '<tr>';
            $html_tr .= "<td>" . date('d/m/Y', strtotime($value['work_day'])) . '</td>';
            
            if(!empty($value['office_time']))
            {
                $html_tr .= "<td>" . $value['office_time'] . "  ngày" . "</td>";
            }
            else
            {
                $shift_name = "";
                switch ($value['shift']) {
                    case '1':
                        $shift_name = "Công sáng";
                        break;
                    case '2':
                        $shift_name = "Công gãy";
                        break;
                    case '3':
                        $shift_name = "Công chiều";
                        break;
                    case '6':
                        $shift_name = "Công hành chính";
                        break;
                    case '9':
                        $shift_name = "Nửa ngày";
                        break;
                    default:
                        # code...
                        break;
                }
                $html_tr .= "<td>" . $shift_name . "</td>";
            } 
            
            $html_tr .= "<td>Xác nhận có đi làm</td>";
            $html_tr .= "<td>" .$value['reason']. "</td>";
            $html_tr .= "<td>
                        <button name='date-approve-last' style='width: 100%' value='" . $value['work_day'] . "'>Đồng ý</button>
                        <button name='date-none-approve-last' style='width: 100%' value='" . $value['work_day'] . "'>Không đồng ý</button>
                        </td>";
            $html_tr .= '</tr>';
            $html .= $html_tr;
        }

        $html .= '</tbody></table>';
        echo $html;
        die;
    }
	 public function getViewDetailLastAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('year');
        $staff_id = $this->getRequest()->getParam('staff_id');
        
        $this->_helper->viewRenderer->setNoRender();
	    $p_month_required = $year . "-" . $month . "-01";          
        
        $db   = Zend_Registry::get('db');
        $stmt   = $db->prepare("CALL `get_view_detail_last`(:p_month_required, :p_staff_id)");
        $stmt->bindParam('p_month_required', My_Util::escape_string($p_month_required) , PDO::PARAM_STR);
        $stmt->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);    
        $stmt->execute();
        $data_last = $stmt->fetchAll();
        $stmt->closeCursor(); 
        $stmt = $db = null;
                                
        $html = "<input type='hidden' name='staff_approve' value='" . $data_last[0]['staff_id'] . "'>";
        $html .= "<table class='table'><thead>";
        $html .= "<th>Ngày</th><th>Ngày công</th><th>Nội dung</th><th>Lý do</th><th>Trạng thái</th></thead>";
        $html .= '<tbody>';
        foreach($data_last as $key => $value)
        { 
            $html_tr = '<tr>';
            $html_tr .= "<td>" . date('d/m/Y', strtotime($value['work_day'])) . '</td>';
            
            if(!empty($value['office_time']))
            {
                $html_tr .= "<td>" . $value['office_time'] . "  ngày" . "</td>";
            }
            else
            {
                $shift_name = "";
                switch ($value['shift']) {
                    case '1':
                        $shift_name = "Công sáng";
                        break;
                    case '2':
                        $shift_name = "Công gãy";
                        break;
                    case '3':
                        $shift_name = "Công chiều";
                        break;
                    case '6':
                        $shift_name = "Công hành chính";
                        break;
                    case '9':
                        $shift_name = "Nửa ngày";
                        break;
                    default:
                        # code...
                        break;
                }
                $html_tr .= "<td>" . $shift_name . "</td>";
            } 
            
            $html_tr .= "<td>Xác nhận có đi làm</td>";
            $html_tr .= "<td>" .$value['reason']. "</td>";
            $html_tr .= "<td>" .$value['status']. "</td>";
            $html_tr .= '</tr>';
            $html .= $html_tr;
        }

        $html .= '</tbody></table>';
        echo $html;
        die;
    }

	
	public function uploadImgAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'upload-img.php';
    }
	public function addTimeLateAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'add-time-late.php';
    }
	
	public function addTimeLateStaffAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'add-time-late-staff.php';
    }
	
	public function addStaffTimeAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'add-staff-time.php';
    }
	public function addTimeLastMonthAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'add-time-last-month.php';
    }
	
    public function addStaffLeaveAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'add-staff-leave.php';
    }
	
	public function removeStaffTimeAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'remove-staff-time.php';
    }
	
	public function exportStaffTimeNew(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-staff-time-new.php';
    }
     public function exportTotalStaffTimeNewAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-total-staff-time-new.php';
    }
	
	  public function exportListStaffTimeNewAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-list-staff-time-new.php';
    }
	
	public function exportOverTimeAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-over-time.php';
    }
    
    public function exportCheckInDetailAction(){
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'export-check-in-detail.php';
    }
	
    public function overTimeAction()
    {
        // $db = Zend_Registry::get('db');

        // $sql = "
        //         SELECT
        //             CONCAT(st.firstname, ' ', st.lastname) AS `staff_name`,
        //             st.code AS `code`,
        //             cid.check_in_day as `check_in_day`,
        //             cid.check_in_at as `check_in_at`,
        //             cid.check_out_at as `check_out_at`,
        //             cid.check_in_late_time as `late`,
        //             cid.check_out_soon_time as `soon`
        //         FROM `check_in_detail` cid
        //         JOIN `staff` st ON cid.staff_code = st.code
        //         WHERE (cid.check_in_day >= '2017-04-01 00:00:00' AND cid.check_in_day <= '2017-04-30 23:59:59')
        //             AND st.department = 150
        //             AND (cid.check_in_late_time IS NOT NULL OR cid.check_out_soon_time IS NOT NULL)";

        // $stmt = $db->prepare($sql);
        // $stmt->execute();
        // $data  = $stmt->fetchAll();

        // require_once 'PHPExcel.php';

        // $alphaExcel = new My_AlphaExcel();

        // $PHPExcel = new PHPExcel();
        // $heads = array(
        //     $alphaExcel->ShowAndUp() => 'Tên nhân viên',
        //     $alphaExcel->ShowAndUp() => 'Mã nhân viên',
        //     $alphaExcel->ShowAndUp() => 'Ngày',
        //     $alphaExcel->ShowAndUp() => 'Giờ vào',
        //     $alphaExcel->ShowAndUp() => 'Giờ ra',
        //     $alphaExcel->ShowAndUp() => 'Đi trễ',
        //     $alphaExcel->ShowAndUp() => 'Về sớm',
        // );
        
        // $PHPExcel->setActiveSheetIndex(0);
        // $sheet = $PHPExcel->getActiveSheet();

        // foreach($heads as $key => $value)
        // {
        //     $sheet->setCellValue($key.'1', $value);
        // }

        // foreach($data as $key => $val)
        // {
        //     $alphaExcel = new My_AlphaExcel();
        //     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['staff_name']);
        //     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['code']);
        //     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($val['check_in_day'])));
        //     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('H:i:s', strtotime($val['check_in_at'])));
        //     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['check_out_at'])?date('H:i:s', strtotime($val['check_out_at'])):'' );
        //     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['late'])?date('H:i:s', strtotime($val['late'])):'');
        //     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['soon'])?date('H:i:s', strtotime($val['soon'])):'');
        // }

        // $filename = 'Export Time - ' . date('Y-m-d H-i-s');
        // $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        // $objWriter->save('php://output');
        // exit;


        // exit;

        $db = Zend_Registry::get('db');

        $sql = "
               SELECT 
 cid.staff_code AS 'staff_code',
        CONCAT(st.firstname,' ',st.lastname) AS 'staff_name',
      --   DATE_ADD(cid.check_in_day, INTERVAL - 1 DAY)  AS 'check_in_date',
				cid.check_in_day AS 'check_in_date',
        cid.check_in_at AS 'check_in_at',
       cid.check_out_at AS 'check_out_at',
				cid.check_in_late_time AS 'late',
			cid.check_out_soon_time AS 'soon',
--        SEC_TO_TIME(TIME_TO_SEC('05:30:00') + TIME_TO_SEC(TIME(cid.check_in_at))  ) AS 'overtime',
				SEC_TO_TIME(TIME_TO_SEC(IF(cid.check_out_at IS NOT NULL ,cid.check_out_at,cid.check_in_at)) - TIME_TO_SEC('18:30:00')) AS 'overtime'
 
      -- SEC_TO_TIME(TIME_TO_SEC(IF(cid.check_out_at IS NOT NULL ,cid.check_out_at,cid.check_in_at)) - TIME_TO_SEC('18:30:00'))/3600 AS 'overtime_int',
      
 FROM check_in_detail cid 
 INNER JOIN staff st ON cid.staff_code = st.`code`
 INNER JOIN check_in_shift  cis ON st.`code` = cis.staff_code AND cis.date IS NULL
WHERE st.department = 150
AND cid.check_in_day BETWEEN '2017-6-1 00:00:00' AND '2017-6-30 23:59:59'
 AND TIME(IF(cid.check_out_at IS NOT NULL ,cid.check_out_at,cid.check_in_at) ) > '18:30:00'
 -- AND TIME(cid.check_in_at) < '06:00:00'
                ";
        
		
		$sql = "SELECT 
 cid.staff_code AS 'staff_code',
        CONCAT(st.firstname,' ',st.lastname) AS 'staff_name',
         DATE_ADD(cid.check_in_day, INTERVAL - 1 DAY)  AS 'check_in_date',
       cid2.check_in_at AS 'check_in_at',
        cid.check_in_at AS 'check_out_at',
       SEC_TO_TIME(TIME_TO_SEC('05:30:00') + TIME_TO_SEC(TIME(cid.check_in_at))  ) AS 'overtime',
       (TIME_TO_SEC('05:30:00') + TIME_TO_SEC(TIME(cid.check_in_at))  )/3600 AS 'overtime_int'
      
 FROM check_in_detail cid 
 INNER JOIN staff st ON cid.staff_code = st.`code`
 INNER JOIN check_in_shift  cis ON st.`code` = cis.staff_code AND cis.date IS NULL
 LEFT JOIN `check_in_detail` cid2 
    ON cid2.staff_code = cid.staff_code 
        AND cid2.check_in_day = DATE_ADD(cid.check_in_day, INTERVAL - 1 DAY)
WHERE st.department = 150
AND cid.check_in_day BETWEEN '2017-6-1 00:00:00' AND '2017-6-30 23:59:59'
 AND TIME(cid.check_in_at) < '06:00:00'
";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data  = $stmt->fetchAll();

        require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Tên nhân viên',
            $alphaExcel->ShowAndUp() => 'Mã nhân viên',
            $alphaExcel->ShowAndUp() => 'Ngày',
            $alphaExcel->ShowAndUp() => 'Giờ vào',
            $alphaExcel->ShowAndUp() => 'Giờ ra',
            $alphaExcel->ShowAndUp() => 'Đi trễ',
            $alphaExcel->ShowAndUp() => 'Về sớm',
            $alphaExcel->ShowAndUp() => 'Thời gian làm thêm (chưa trừ đi trễ)',
            $alphaExcel->ShowAndUp() => 'Thời gian làm thêm (trừ trừ đi trễ)',
            $alphaExcel->ShowAndUp() => 'Quên check out'
        );
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        foreach($data as $key => $val)
        {
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['staff_name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['staff_code']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($val['check_in_date'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('H:i:s', strtotime($val['check_in_at'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['check_out_at'])?date('H:i:s', strtotime($val['check_out_at'])):'' );
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['late'])?$val['late']:'');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['soon'])?$val['soon']:'');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['overtime_not_late']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['overtime']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['quen_check_out']);

        }

        $filename = 'Export Time - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function exportAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '5120M');
        $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('year');
        $QTime2 = new Application_Model_Time2();
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
        $from_date = $year . '-' . $month . '-01';
        $to_date = $year . '-' . $month . '-' . $number_day_of_month;

        $params = array(
            'month' => $month,
            'year' => $year,
            'limit' => null,
            'offset' => null,
            'from_date' => $from_date,
            'to_date' => $to_date
        );

        $data_training = $QTime2->getListTrainingAll($params);

        $params_special = array(
            'from_date' => $from_date,
            'to_date' => $to_date
        );

        $data_special = $QTime2->get_special_day($params_special);
        $special = array();
        foreach($data_special as $key => $val)
        {
            $data_date = $val;
            $data_date['list_group'] = json_decode($val['group'], true);
            $special[intval(date('d', strtotime($val['date'])))] = $data_date;
        }

        $db = Zend_Registry::get('db');
        $sql = "SELECT st.id, st.firstname, st.lastname, st.email, 
                    st.code,
                    st.workingdate,
                    ar.name AS `area`,
                    cp.name AS `company_name`,
                    st.department,
                    st.title,
                    st.team,
                    st.ID_number AS `cmnd`,
                    st.is_officer,
                    st.joined_at AS `joined_at`,
                    st.off_date AS `off_date`,
                    std.number AS `number`, ada.`id` AS holiday_id,
                    dc.code as `holiday_special`
                    , t.*, lv.`leave_type`, lv.`is_half_day`, cgm.company_group as `company_group`
                FROM
                (
                SELECT st.id, st.company_id, st.regional_market, st.title, st.joined_at, st.department, st.team, st.off_date, st.is_officer, st.ID_number, st.code, st.firstname, st.lastname, st.email, st.status, CONCAT(ad.`date`, ' 00:00:00') fromtime,  CONCAT(ad.`date`, ' 23:59:59') totime, ad.`date` AS workingdate
                FROM `staff` AS `st`
                CROSS JOIN (SELECT * FROM `all_date` WHERE MONTH(`date`) = $month 
                        AND YEAR(`date`) = $year) AS `ad` 
                ) st
                LEFT JOIN (SELECT id as `time_id`, staff_id, created_at, shift, `status`, approve_time, real_time FROM `time` 
                        WHERE MONTH(`created_at`) = $month AND YEAR(`created_at`) = $year AND off <> 1  ) 
                    AS `t` ON t.staff_id = st.id 
                        AND (t.created_at >= st.fromtime AND t.created_at <= st.totime)
                        AND (t.created_at >= st.joined_at AND (st.off_date IS NULL OR t.created_at <= st.off_date) )
                LEFT JOIN 
                (SELECT staff_id, from_date, to_date, `leave_type`, `is_half_day` 
                    FROM `leave_detail` 
                    WHERE ( (from_date >=  '$from_date' AND from_date <=  '$to_date') 
                            OR (to_date >=  '$from_date' AND to_date <=  '$to_date') ) 
                            AND `status` = 1) 
                lv ON st.id = lv.staff_id and st.workingdate between lv.from_date and lv.to_date
                INNER JOIN `company` AS `cp` ON st.company_id = cp.id
                INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
                LEFT JOIN `all_date_advanced` AS `ada` ON workingdate = ada.date
                                                AND ada.is_off = 1
                                                AND (
                                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                                )
                                                AND ada.category_date IS NOT NULL
                                                AND ada.category_date = 2
                LEFT JOIN `date_category` AS `dc` ON dc.id = ada.category_date
                                            AND ada.category_date IS NOT NULL
                LEFT JOIN `saturday` AS `std` ON std.month = 3 
                                            AND std.year = 2017
                                            AND std.type = 4 AND std.type_id = cgm.company_group 
                WHERE ( st.off_date IS NULL OR st.off_date >= '$from_date' )
                    AND st.joined_at <= '$to_date'
                    AND st.title <> 375

                ";
        
		
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        $data_export = array();

        foreach($data as $value)
        {
            if(empty($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]))
            {
                $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
            }
            else
            {
                if($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'] == ''
                || 
                $value['approve_time'] > $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'])
                {
                    $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
                }
            }
        }
        require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
        );

        $number_day_of_month = 30;

        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $heads[$alphaExcel->ShowAndUp()] = $i;
        }
        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        $heads[$alphaExcel->ShowAndUp()] = 'Ngày nghỉ hưởng lương';
        $heads[$alphaExcel->ShowAndUp()] = 'Công thứ 7 (x2)';
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        // print_r($recursiveDeparmentTeamTitle); die;
       
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $info = $value['1'];
            $array_number = array(
                'number_work' => 0,
                'number_gay' => 0,
                'number_training' => 0,
                'number_leave' => 0,
                'number_sunday' => 0,
                'number_le' => 0,
                'number_nghi_huong_luong' => 0,
                'sat' => 0
            );
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['company_name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['code']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['firstname'] . ' ' . $value['1']['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['children'][$info['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($info['joined_at']) ));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($info['off_date'])):'' );
            $data_staff_train = array();
            if(!empty($info['id']) && !empty($data_training[$info['id']]))
            {
                $data_staff_train = $data_training[$info['id']];
            }
            elseif(!empty($info['cmnd']) && !empty($data_training[trim($info['cmnd'])])  )
            {
                $data_staff_train = $data_training[trim($info['cmnd'])];
            }
            if(empty($data_staff_train))
            {
                $data_staff_train = array();
            }

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                
                $number_leave = 0;
                $data_day = $value[$i];
                $text_day = '-';

                if(in_array($i, $data_staff_train))
                {
                    $text_day = 'T';
                }
                else
                {
                    if(
                        strtotime($year . '-' . $month . '-' . $i) >= strtotime($info['joined_at']) 
                        && (
                            strtotime($year . '-' . $month . '-' . $i) <= strtotime($info['off_date']) 
                            || empty($info['off_date'])
                        )
                    )
                    {
                        if(!empty($data_day['time_id']))
                        {   
                            if(in_array($data_day['shift'], array(7, 8)))
                            {
                                $text_day = 'T';
                            }
                            elseif($data_day['approve_time'] != '')
                            {
                                switch ($data_day['approve_time']) {
                                    case 1:
                                        $text_day = 'X';
                                        break;
                                    case 0.5:
                                        $text_day = 'H';
                                        break;
                                }
                            }
                            elseif($data_day['status'] == 1)
                            {
                                if($info['department'] == 159 && in_array($data_day['shift'], array(1,3)))
                                {
                                    $text_day = 'H';
                                }
                                else
                                {
                                    switch ($data_day['shift']) {
                                        case 1:
                                            $text_day = 'X';
                                            break;
                                        case 0:
                                            $text_day = 'X';
                                            break;
                                        case 3:
                                            $text_day = 'X';
                                            break;
                                        case 6:
                                            $text_day = 'X';
                                            break;
                                        case 2:
                                            $text_day = 'G';
                                            break;
                                    }
                                }
                            }
                        }
                        elseif(!empty($data_day['leave_type'])  && empty($data_day['holiday_id']) && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            $text_day = 'P';
                        }
                        
                        if(!empty($data_day['leave_type'])  && empty($data_day['holiday_id'])  && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            if(!empty($data_day['is_half_day']) || $text_day == 'H')
                            {
                                $number_leave = 0.5;
                            }
                            elseif($text_day != 'X')
                            {
                                $number_leave = 1;
                            }
                        }

                        if(!empty($data_day['holiday_id']))
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'C';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'D';
                            }
                        }

                        if(in_array($info['company_group'], $special[$i]['list_group']) && $data_day['time_id'] != 2332159)
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'L';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'K';
                            }

                            if($text_day == '-')
                            {
                                $text_day = 'X';
                                $array_number['number_nghi_huong_luong'] ++;
                            }
                        }
                        elseif(!empty($special[$i]))
                        {
                            if($text_day != 'G')
                            {
                                $text_day = 'X';
                            }
                            $number_leave = 0;
                            $array_number['number_nghi_huong_luong'] ++;
                        }

                        if($info['company_group'] == 6 && !in_array($i, array(7, 14, 21, 28,1, 2)) )
                        {
                            $text_day = 'X';
                        }
                    }
                }

                

                if($text_day == 'X' || $text_day == 'G')
                {
                    $number_leave = 0;
                }
                
                if(date('D', strtotime($year . '-' . $month . '-' . $i )) == 'Sat' 
                    && in_array($info['company_group'], array(6, 7, 8, 9, 12)) && in_array($text_day, array('H', 'X')))
                {
                    if($text_day == 'X')
                    {
                        $array_number['sat']++;
                    }
                    
                    if($text_day == 'H')
                    {
                        $array_number['sat'] += 0.5;
                    }
                }

                

                switch ($text_day) {
                    case 'X':
                        $array_number['number_work'] ++;
                        break;
                    case 'H':
                        $array_number['number_work'] += 0.5;
                        break;
                    case 'G':
                        $array_number['number_work'] ++;
                        $array_number['number_gay'] ++;
                        break;
                    case 'T':
                        $array_number['number_training'] ++;
                        break;
                    case 'C':
                        $array_number['number_sunday'] ++;
                        break;
                    case 'D':
                        $array_number['number_sunday'] += 0.5;
                        break;
                    case 'L':
                        $array_number['number_le'] ++;
                        break;
                    case 'K':
                        $array_number['number_le'] += 0.5;
                        break;
                }
                $array_number['number_leave'] += $number_leave;
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }

            $array_number['sat'] = ($array_number['sat'] > 3)?($array_number['sat'] - 3):0;

            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_work'] - $array_number['number_nghi_huong_luong'] - $array_number['sat']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_gay']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_training']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_leave']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_sunday']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_le']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_nghi_huong_luong']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['sat']);
            $index++;
        }

        $data_staff_new = $QTime2->get_new_staff_time(array('month' => 4, 
                                                        'year' => 2017, 
                                                        'code' => null,
                                                        'name' => null,
                                                        'area' => null));
        $array_ddtm = array(
                24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
        );
        foreach($data_staff_new as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();
            $company_name = (in_array($value['area_id'], $array_ddtm))?'Di Động Thông Minh':'OPPO';
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $company_name);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['code']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['firstname'] . ' ' . $value['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['children'][$value['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $data_staff_train = array();
            $data_training_day = json_decode($value['list_day'], true);
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                if(in_array($i, $data_training_day))
                {
                    $text_day = 'T';
                }
                else
                {
                    $text_day = '-';
                }
                
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), count($data_training_day));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $index++;
        }

        
        $filename = 'Export Time - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
        echo '<pre>';
        // print_r($data_export);
        echo 'done';
        exit;
    }

    public function exportStaffTime($month, $year)
    {
    
        set_time_limit(0);
        ini_set('memory_limit', '5120M');
        $QTime2 = new Application_Model_Time2();
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
        $from_date = $year . '-' . $month . '-01';
        $to_date = $year . '-' . $month . '-' . $number_day_of_month;

        $params = array(
            'month' => $month,
            'year' => $year,
            'limit' => null,
            'offset' => null
        );

        $data_training = $QTime2->getListTrainingAll($params);

        $params_special = array(
            'from_date' => $from_date,
            'to_date' => $to_date
        );

        $data_special = $QTime2->get_special_day($params_special);
        $special = array();
        foreach($data_special as $key => $val)
        {
            $data_date = $val;
            $data_date['list_group'] = json_decode($val['group'], true);
            $special[intval(date('d', strtotime($val['date'])))] = $data_date;
        }

        $db = Zend_Registry::get('db');
        $sql = "SELECT st.id, st.firstname, st.lastname, st.email, 
                    st.code,
                    st.workingdate,
                    ar.name AS `area`,
                    cp.name AS `company_name`,
                    st.department,
                    st.title,
                    st.team,
                    st.ID_number AS `cmnd`,
                    st.is_officer,
                    st.joined_at AS `joined_at`,
                    st.off_date AS `off_date`,
                    std.number AS `number`, ada.`id` AS holiday_id,
                    dc.code as `holiday_special`
                    , t.*, lv.`leave_type`, lv.`is_half_day`, cgm.company_group as `company_group`
                FROM
                (
                SELECT st.id, st.company_id, st.regional_market, st.title, st.joined_at, st.department, st.team, st.off_date, st.is_officer, st.ID_number, st.code, st.firstname, st.lastname, st.email, st.status, CONCAT(ad.`date`, ' 00:00:00') fromtime,  CONCAT(ad.`date`, ' 23:59:59') totime, ad.`date` AS workingdate
                FROM `staff` AS `st`
                CROSS JOIN (SELECT * FROM `all_date` WHERE MONTH(`date`) = $month 
                        AND YEAR(`date`) = $year) AS `ad` 
                ) st
                LEFT JOIN (SELECT id as `time_id`, staff_id, created_at, shift, `status`, approve_time, real_time FROM `time` 
                        WHERE MONTH(`created_at`) = $month AND YEAR(`created_at`) = $year AND off <> 1  ) 
                    AS `t` ON t.staff_id = st.id 
                        AND (t.created_at >= st.fromtime AND t.created_at <= st.totime)
                        AND (t.created_at >= st.joined_at AND (st.off_date IS NULL OR t.created_at <= st.off_date) )
                LEFT JOIN 
                (SELECT staff_id, from_date, to_date, `leave_type`, `is_half_day` 
                    FROM `leave_detail` 
                    WHERE ( (from_date >=  '$from_date' AND from_date <=  '$to_date') 
                            OR (to_date >=  '$from_date' AND to_date <=  '$to_date') ) 
                            AND `status` = 1) lv ON st.id = lv.staff_id and st.workingdate between lv.from_date and lv.to_date
                INNER JOIN `company` AS `cp` ON st.company_id = cp.id
                INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
                LEFT JOIN `all_date_advanced` AS `ada` ON workingdate = ada.date
                                                AND ada.is_off = 1
                                                AND (
                                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                                )
                                                AND ada.category_date IS NOT NULL
                                                AND ada.category_date = 2
                LEFT JOIN `date_category` AS `dc` ON dc.id = ada.category_date
                                            AND ada.category_date IS NOT NULL
                LEFT JOIN `saturday` AS `std` ON std.month = 3 
                                            AND std.year = 2017
                                            AND std.type = 4 AND std.type_id = cgm.company_group 
                WHERE ( st.off_date IS NULL OR st.off_date >= '$from_date' )
                    AND st.joined_at <= '$to_date'
                    AND st.title <> 375
                ";
        if(!empty($_GET['dev'])){
            echo $sql;
            exit();
        }
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        $data_export = array();

        $data_return = array();

        foreach($data as $value)
        {
            if(empty($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]))
            {
                $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
            }
            else
            {
                if($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'] == ''
                || 
                $value['approve_time'] > $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'])
                {
                    $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
                }
            }
        }
        require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
        );

        $number_day_of_month = 31;

        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $heads[$alphaExcel->ShowAndUp()] = $i;
        }

        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        // print_r($recursiveDeparmentTeamTitle); die;
       
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $staff_return = array();
            $info = $value['1'];
            $staff_return['info'] = $info;
            $array_number = array(
                'number_work' => 0,
                'number_gay' => 0,
                'number_training' => 0,
                'number_leave' => 0,
                'number_sunday' => 0,
                'number_le' => 0,
            );
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['company_name']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['firstname'] . ' ' . $value['1']['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['children'][$info['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($info['joined_at']) ));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($info['off_date'])):'' );
            
            $staff_return['info']['department_name'] = $recursiveDeparmentTeamTitle[$info['department']]['name'];
            $staff_return['info']['team_name'] = $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['name'];
            $staff_return['info']['title_name'] = $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['children'][$info['title']]['name'];
            
            $data_staff_train = array();
            $data_cmnd_train = array();
            if(!empty($info['id']) && !empty($data_training[$info['id']]))
            {
                $data_staff_train = $data_training[$info['id']];
            }
            
            if(!empty($info['cmnd']) && !empty($data_training[trim($info['cmnd'])])  )
            {
                $data_cmnd_train = $data_training[trim($info['cmnd'])];
            }

            if(empty($data_staff_train))
            {
                $data_staff_train = array();
            }

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                
                $number_leave = 0;
                $data_day = $value[$i];
                $text_day = '-';


                if(in_array($i, $data_staff_train) || in_array($i, $data_cmnd_train))
                {
                    $text_day = 'T';
                }
                else
                {
                    if(
                        strtotime($year . '-' . $month . '-' . $i) >= strtotime($info['joined_at']) 
                        && (
                            strtotime($year . '-' . $month . '-' . $i) <= strtotime($info['off_date']) 
                            || empty($info['off_date'])
                        )
                    )
                    {
                        if(!empty($data_day['time_id']))
                        {   
                            if(in_array($data_day['shift'], array(7, 8)))
                            {
                                $text_day = 'T';
                            }
                            elseif($data_day['approve_time'] != '')
                            {
                                switch ($data_day['approve_time']) {
                                    case 1:
                                        $text_day = 'X';
                                        break;
                                    case 0.5:
                                        $text_day = 'H';
                                        break;
                                }
                            }
                            elseif($data_day['status'] == 1)
                            {
                                if($info['department'] == 159 && in_array($data_day['shift'], array(1,3)))
                                {
                                    $text_day = 'H';
                                }
                                else
                                {
                                    switch ($data_day['shift']) {
                                        case 1:
                                            $text_day = 'X';
                                            break;
                                        case 0:
                                            $text_day = 'X';
                                            break;
                                        case 3:
                                            $text_day = 'X';
                                            break;
                                        case 6:
                                            $text_day = 'X';
                                            break;
                                        case 2:
                                            $text_day = 'G';
                                            break;
                                    }
                                }
                            }
                        }
                        elseif(!empty($data_day['leave_type'])  && empty($data_day['holiday_id']) && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            $text_day = 'P';
                        }
                        
                        if(!empty($data_day['leave_type'])  && empty($data_day['holiday_id'])  && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            if(!empty($data_day['is_half_day']) || $text_day == 'H')
                            {
                                $number_leave = 0.5;
                            }
                            elseif($text_day != 'X')
                            {
                                $number_leave = 1;
                            }
                        }

                        if(!empty($data_day['holiday_id']))
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'C';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'D';
                            }
                        }

                        if(in_array($info['company_group'], $special[$i]['list_group']) && $data_day['time_id'] != 2332159)
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'L';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'K';
                            }

                            if($text_day == '-')
                            {
                                // $text_day = 'X';
                            }
                        }
                        elseif(!empty($special[$i]))
                        {
                            // if(!empty($GET['check'])){
                                
                            // }else{
                            //     $text_day = 'X';
                            //     $number_leave = 0;
                            // }
                            
                        }

                        if($info['company_group'] == 6 && !in_array($i, array(7, 14, 21, 28,1, 2)) && empty($data_day['leave_type']) )
                        {
                            $text_day = 'X';
                        }
                        elseif($info['company_group'] == 6 && in_array($i, array(7, 14, 21, 28,1, 2)))
                        {
                            $text_day = '-';
                        }
                    }
                }

                

                if($text_day == 'X')
                {
                    $number_leave = 0;
                }
                switch ($text_day) {
                    case 'X':
                        $array_number['number_work'] ++;
                        break;
                    case 'H':
                        $array_number['number_work'] += 0.5;
                        break;
                    case 'G':
                        $array_number['number_work'] ++;
                        $array_number['number_gay'] ++;
                        break;
                    case 'T':
                        $array_number['number_training'] ++;
                        break;
                    case 'C':
                        $array_number['number_sunday'] ++;
                        break;
                    case 'D':
                        $array_number['number_sunday'] += 0.5;
                        break;
                    case 'L':
                        $array_number['number_le'] ++;
                        break;
                    case 'K':
                        $array_number['number_le'] += 0.5;
                        break;
                }
                $array_number['number_leave'] += $number_leave;
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_work']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_gay']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_training']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_leave']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_sunday']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_le']);
            $staff_return['number'] = $array_number;
            $data_return[] = $staff_return;
            $index++;
        }

        if($return == true)
        {
            return $data_return;
        }

        $data_staff_new = $QTime2->get_new_staff_time(array('month' => $month, 
                                                        'year' => $year, 
                                                        'code' => null,
                                                        'name' => null,
                                                        'area' => null));
        $array_ddtm = array(
                24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
        );

        foreach($data_staff_new as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();
            $company_name = (in_array($value['area_id'], $array_ddtm))?'Di Động Thông Minh':'OPPO';
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $company_name);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['code']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['firstname'] . ' ' . $value['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['children'][$value['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $data_staff_train = array();
            $data_training_day = json_decode($value['list_day'], true);
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                if(in_array($i, $data_training_day))
                {
                    $text_day = 'T';
                }
                else
                {
                    $text_day = '-';
                }
                
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), count($data_training_day));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $index++;
        }
        
        $filename = 'Export Time - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
        echo '<pre>';
        // print_r($data_export);
        echo 'done';
        exit;
    }

    public function exportStaffTimeAction()
    {
        
        set_time_limit(0);
        ini_set('memory_limit', '5120M');
        $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('year');
        $QTime2 = new Application_Model_Time2();
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
        $from_date = $year . '-' . $month . '-01';
        $to_date = $year . '-' . $month . '-' . $number_day_of_month;

        $params = array(
            'month' => $month,
            'year' => $year,
            'limit' => null,
            'offset' => null
        );

        $data_training = $QTime2->getListTrainingAll($params);

        $params_special = array(
            'from_date' => $from_date,
            'to_date' => $to_date
        );

        $data_special = $QTime2->get_special_day($params_special);
        $special = array();
        foreach($data_special as $key => $val)
        {
            $data_date = $val;
            $data_date['list_group'] = json_decode($val['group'], true);
            $special[intval(date('d', strtotime($val['date'])))] = $data_date;
        }

        $db = Zend_Registry::get('db');
        $sql = "SELECT st.id, st.firstname, st.lastname, st.email, 
                    st.code,
                    st.workingdate,
                    ar.name AS `area`,
                    cp.name AS `company_name`,
                    st.department,
                    st.title,
                    st.team,
                    st.ID_number AS `cmnd`,
                    st.is_officer,
                    st.joined_at AS `joined_at`,
                    st.off_date AS `off_date`,
                    std.number AS `number`, ada.`id` AS holiday_id,
                    dc.code as `holiday_special`
                    , t.*, lv.`leave_type`, lv.`is_half_day`, cgm.company_group as `company_group`
                FROM
                (
                SELECT st.id, st.company_id, st.regional_market, st.title, st.joined_at, st.department, st.team, st.off_date, st.is_officer, st.ID_number, st.code, st.firstname, st.lastname, st.email, st.status, CONCAT(ad.`date`, ' 00:00:00') fromtime,  CONCAT(ad.`date`, ' 23:59:59') totime, ad.`date` AS workingdate
                FROM `staff` AS `st`
                CROSS JOIN (SELECT * FROM `all_date` WHERE MONTH(`date`) = $month 
                        AND YEAR(`date`) = $year) AS `ad` 
                ) st
                LEFT JOIN (SELECT id as `time_id`, staff_id, created_at, shift, `status`, approve_time, real_time FROM `time` 
                        WHERE MONTH(`created_at`) = $month AND YEAR(`created_at`) = $year AND off <> 1  ) 
                    AS `t` ON t.staff_id = st.id 
                        AND (t.created_at >= st.fromtime AND t.created_at <= st.totime)
                        AND (t.created_at >= st.joined_at AND (st.off_date IS NULL OR t.created_at <= st.off_date) )
                LEFT JOIN 
                (SELECT staff_id, from_date, to_date, `leave_type`, `is_half_day` 
                    FROM `leave_detail` 
                    WHERE ( (from_date >=  '$from_date' AND from_date <=  '$to_date') 
                            OR (to_date >=  '$from_date' AND to_date <=  '$to_date') ) 
                            AND `status` = 1) lv ON st.id = lv.staff_id and st.workingdate between lv.from_date and lv.to_date
                INNER JOIN `company` AS `cp` ON st.company_id = cp.id
                INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
                LEFT JOIN `all_date_advanced` AS `ada` ON workingdate = ada.date
                                                AND ada.is_off = 1
                                                AND (
                                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                                )
                                                AND ada.category_date IS NOT NULL
                                                AND ada.category_date = 2
                LEFT JOIN `date_category` AS `dc` ON dc.id = ada.category_date
                                            AND ada.category_date IS NOT NULL
                LEFT JOIN `saturday` AS `std` ON std.month = 3 
                                            AND std.year = 2017
                                            AND std.type = 4 AND std.type_id = cgm.company_group 
                WHERE ( st.off_date IS NULL OR st.off_date >= '$from_date' )
                    AND st.joined_at <= '$to_date'
                ";
       
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        $data_export = array();

        foreach($data as $value)
        {
            if(empty($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]))
            {
                $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
            }
            else
            {
                if($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'] == ''
                || 
                $value['approve_time'] > $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'])
                {
                    $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
                }
            }
        }
        require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
        );

        $number_day_of_month = 31;

        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $heads[$alphaExcel->ShowAndUp()] = $i;
        }

        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        // print_r($recursiveDeparmentTeamTitle); die;
       
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $info = $value['1'];
            $array_number = array(
                'number_work' => 0,
                'number_gay' => 0,
                'number_training' => 0,
                'number_leave' => 0,
                'number_sunday' => 0,
                'number_le' => 0,
            );
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['company_name']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['firstname'] . ' ' . $value['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['children'][$info['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($info['joined_at']) ));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($info['off_date'])):'' );
            $data_staff_train = array();
            if(!empty($info['id']) && !empty($data_training[$info['id']]))
            {
                $data_staff_train = $data_training[$info['id']];
            }
            elseif(!empty($info['cmnd']) && !empty($data_training[trim($info['cmnd'])])  )
            {
                $data_staff_train = $data_training[trim($info['cmnd'])];
            }
            if(empty($data_staff_train))
            {
                $data_staff_train = array();
            }

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                
                $number_leave = 0;
                $data_day = $value[$i];
                $text_day = '-';


                if(in_array($i, $data_staff_train))
                {
                    $text_day = 'T';
                }
                else
                {


                    if(
                        strtotime($year . '-' . $month . '-' . $i) >= strtotime($info['joined_at']) 
                        && (
                            strtotime($year . '-' . $month . '-' . $i) <= strtotime($info['off_date']) 
                            || empty($info['off_date'])
                        )
                    )
                    {

                        if(!empty($data_day['time_id']))
                        {   
                            if(in_array($data_day['shift'], array(7, 8)))
                            {
                                $text_day = 'T';
                            }
                            elseif($data_day['approve_time'] != '')
                            {
                                switch ($data_day['approve_time']) {
                                    case 1:
                                        $text_day = 'X';
                                        break;
                                    case 0.5:
                                        $text_day = 'H';
                                        break;
                                }
                            }
                            elseif($data_day['status'] == 1)
                            {
                                if($info['department'] == 159 && in_array($data_day['shift'], array(1,3)))
                                {
                                    $text_day = 'H';
                                }
                                else
                                {
                                    switch ($data_day['shift']) {
                                        case 1:
                                            $text_day = 'X';
                                            break;
                                        case 0:
                                            $text_day = 'X';
                                            break;
                                        case 3:
                                            $text_day = 'X';
                                            break;
                                        case 6:
                                            $text_day = 'X';
                                            break;
                                        case 2:
                                            $text_day = 'G';
                                            break;
                                    }
                                }
                            }
                        }elseif(!empty($data_day['leave_type'])  && empty($data_day['holiday_id']) && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            $text_day = 'P';
                        }
                        
                        if(!empty($data_day['leave_type'])  && empty($data_day['holiday_id'])  && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            if(!empty($data_day['is_half_day']) || $text_day == 'H')
                            {
                                $number_leave = 0.5;
                            }
                            elseif($text_day != 'X')
                            {
                                $number_leave = 1;
                            }
                        }

                        if(!empty($data_day['holiday_id']))
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'C';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'D';
                            }
                        }

                        if(in_array($info['company_group'], $special[$i]['list_group']) && $data_day['time_id'] != 2332159)
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'L';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'K';
                            }

                            if($text_day == '-')
                            {
                                // $text_day = 'X';
                            }
                        }
                        elseif(!empty($special[$i]))
                        {
                            $text_day = 'X';
                            $number_leave = 0;
                        }

                        if($info['company_group'] == 6 && !in_array($i, array(7, 14, 21, 28,1, 2)) && empty($data_day['leave_type']) )
                        {
                            $text_day = 'X';
                        }
                    }
                }

                

                if($text_day == 'X')
                {
                    $number_leave = 0;
                }

                

                switch ($text_day) {
                    case 'X':
                        $array_number['number_work'] ++;
                        break;
                    case 'H':
                        $array_number['number_work'] += 0.5;
                        break;
                    case 'G':
                        $array_number['number_work'] ++;
                        $array_number['number_gay'] ++;
                        break;
                    case 'T':
                        $array_number['number_training'] ++;
                        break;
                    case 'C':
                        $array_number['number_sunday'] ++;
                        break;
                    case 'D':
                        $array_number['number_sunday'] += 0.5;
                        break;
                    case 'L':
                        $array_number['number_le'] ++;
                        break;
                    case 'K':
                        $array_number['number_le'] += 0.5;
                        break;
                }
                $array_number['number_leave'] += $number_leave;
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_work']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_gay']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_training']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_leave']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_sunday']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_le']);
            $index++;
        }

        $data_staff_new = $QTime2->get_new_staff_time(array('month' => 4, 
                                                        'year' => 2017, 
                                                        'code' => null,
                                                        'name' => null,
                                                        'area' => null));
        $array_ddtm = array(
                24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
        );
        
        
        foreach($data_staff_new as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();
            $company_name = (in_array($value['area_id'], $array_ddtm))?'Di Động Thông Minh':'OPPO';
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $company_name);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['code']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['firstname'] . ' ' . $value['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['children'][$value['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $data_staff_train = array();
            $data_training_day = json_decode($value['list_day'], true);
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                if(in_array($i, $data_training_day))
                {
                    $text_day = 'T';
                }
                else
                {
                    $text_day = '-';
                }
                
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), count($data_training_day));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $index++;
        }
        
        
        $filename = 'Export Time - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
        echo '<pre>';
        // print_r($data_export);
        echo 'done';
        exit;
    }

    public function exportTotal($month, $year)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5120M');
        $QTime2 = new Application_Model_Time2();
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
        $from_date = $year . '-' . $month . '-01';
        $to_date = $year . '-' . $month . '-' . $number_day_of_month;

        $params = array(
            'month' => $month,
            'year' => $year,
            'limit' => null,
            'offset' => null
        );

        $data_training = $QTime2->getListTrainingAll($params);

        $params_special = array(
            'from_date' => $from_date,
            'to_date' => $to_date
        );

        $data_special = $QTime2->get_special_day($params_special);
        $special = array();
        foreach($data_special as $key => $val)
        {
            $data_date = $val;
            $data_date['list_group'] = json_decode($val['group'], true);
            $special[intval(date('d', strtotime($val['date'])))] = $data_date;
        }

        $db = Zend_Registry::get('db');
        $sql = "SELECT st.id, st.firstname, st.lastname, st.email, 
                    st.code,
                    st.workingdate,
                    ar.name AS `area`,
                    rm.name AS `rm_name` ,
                    cp.name AS `company_name`,
                    st.department,
                    st.title,
                    st.team,
                    st.email,
                    st.ID_number AS `cmnd`,
                    st.is_officer,
                    st.joined_at AS `joined_at`,
                    st.off_date AS `off_date`,
                    std.number AS `number`, ada.`id` AS holiday_id,
                    dc.code as `holiday_special`
                    , t.*, lv.`leave_type`, lv.`is_half_day`, cgm.company_group as `company_group`
                    , v.from_date as `v_from_date` , v.to_date as `v_to_date`
                    , v.title as `v_title`
                    , v.team as `v_team`
                    , v.department as `v_department`
                FROM
                (
                SELECT st.id, st.company_id, st.email, st.regional_market, st.title, st.joined_at, st.department, st.team, st.off_date, st.is_officer, st.ID_number, st.code, st.firstname, st.lastname, st.status, CONCAT(ad.`date`, ' 00:00:00') fromtime,  CONCAT(ad.`date`, ' 23:59:59') totime, ad.`date` AS workingdate
                FROM `staff` AS `st`
                CROSS JOIN (SELECT * FROM `all_date` WHERE MONTH(`date`) = $month 
                        AND YEAR(`date`) = $year) AS `ad`
                ) st
                LEFT JOIN (SELECT id as `time_id`, staff_id, created_at, shift, `status`, approve_time, real_time FROM `time` 
                        WHERE MONTH(`created_at`) = $month AND YEAR(`created_at`) = $year AND off <> 1  ) 
                    AS `t` ON t.staff_id = st.id 
                        AND (t.created_at >= st.fromtime AND t.created_at <= st.totime)
                        AND (t.created_at >= st.joined_at AND (st.off_date IS NULL OR t.created_at <= st.off_date) )
                LEFT JOIN 
                
                (SELECT staff_id, from_date, to_date, `leave_type`, `is_half_day` 
                    FROM `leave_detail` 
                    WHERE ( (from_date >=  '$from_date' AND from_date <=  '$to_date') 
                            OR (to_date >=  '$from_date' AND to_date <=  '$to_date') ) 
                            AND `status` = 1
             
                ) 
                
                lv ON st.id = lv.staff_id and st.workingdate between lv.from_date and lv.to_date
                LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
                LEFT JOIN `all_date_advanced` AS `ada` ON workingdate = ada.date
                                                AND ada.is_off = 1
                                                AND (
                                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                                )
                                                AND ada.category_date IS NOT NULL
                                                AND ada.category_date = 2
                LEFT JOIN `date_category` AS `dc` ON dc.id = ada.category_date
                                            AND ada.category_date IS NOT NULL
                LEFT JOIN `saturday` AS `std` ON std.month = 3 
                                            AND std.year = 2017
                                            AND std.type = 4 AND std.type_id = cgm.company_group 
                LEFT JOIN 
                    `v_staff_transfer_fix` v ON st.id = v.staff_id 
                    AND ( (st.workingdate BETWEEN v.from_date and DATE_ADD(v.to_date, INTERVAL - 1 DAY))
                        OR (st.workingdate >= v.from_date and v.to_date IS NULL)
                    )
                INNER JOIN `company` AS `cp` ON st.company_id = cp.id
                INNER JOIN `regional_market` AS `rm` 
                    ON  (v.regional_market IS NULL AND st.regional_market = rm.id)
                        OR (v.regional_market IS NOT NULL AND v.regional_market = rm.id)
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                
                -- INNER JOIN (SELECT `staff_id`
                  --               FROM  v_staff_transfer_fix
                    --             WHERE from_date >= '$from_date' AND from_date <= '$to_date'
                      --           AND DATE(from_date) <> '$from_date'
                    --             GROUP BY `staff_id`) vc ON st.id = vc.staff_id
               -- WHERE ( st.off_date IS NULL OR st.off_date >= '$from_date' )
                 --   AND st.title <> 375
                    
                 WHERE ( st.off_date IS NULL OR st.off_date >= '2017-5-01' ) AND st.joined_at <= '2017-5-31' AND st.title <> 375    
           
           --     ORDER BY st.id, v.from_date
        
        ";

        if(!empty($_GET['dev'])){
            echo $sql;
            exit();
        }
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        $data_export = array();

        foreach($data as $value)
        {
            if(empty($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]))
            {
                $data_export[intval($value['id']).'-'.$value['v_from_date']][intval(date('d', strtotime($value['workingdate'])))] = $value;
            }
            else
            {
                if($data_export[intval($value['id']).'-'.$value['v_from_date']][intval(date('d', strtotime($value['workingdate'])))]['approve_time'] == ''
                    || $value['approve_time'] > $data_export[intval($value['id']).'-'.$value['v_from_date']][intval(date('d', strtotime($value['workingdate'])))]['approve_time'])
                {
                    $data_export[intval($value['id']).'-'.$value['v_from_date']][intval(date('d', strtotime($value['workingdate'])))] = $value;
                }
            }
        }

        // echo '<pre>';
        // print_r($data_export); die;

        require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Regional market',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
            $alphaExcel->ShowAndUp() => 'Transfer date',
            $alphaExcel->ShowAndUp() => 'Lần transfer'
        );

        $number_day_of_month = 31;

        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $heads[$alphaExcel->ShowAndUp()] = $i;
        }

        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        $heads[$alphaExcel->ShowAndUp()] = 'Ngày nghỉ hưởng lương';
        $heads[$alphaExcel->ShowAndUp()] = 'Công thứ 7 (x2)';
        $heads[$alphaExcel->ShowAndUp()] = 'Email';
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        // print_r($recursiveDeparmentTeamTitle); die;

        $index = 1;

        $array_number_transfer = array();

        foreach($data_export as $key => $value)
        {
            $first_key = key($value);
            $info = $value[$first_key];
            if(empty($array_number_transfer[$info['id']]))
            {
                $array_number_transfer[$info['id']] = 1;
            }
            else
            {
                $array_number_transfer[$info['id']]++;
            }

            $array_number = array(
                'number_work' => 0,
                'number_gay' => 0,
                'number_training' => 0,
                'number_leave' => 0,
                'number_sunday' => 0,
            );

            $department = !empty($info['v_department'])?$info['v_department']:$info['department'];
            $team = !empty($info['v_team'])?$info['v_team']:$info['team'];
            $title = !empty($info['v_title'])?$info['v_title']:$info['title'];


            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['company_name']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['code'], PHPExcel_Cell_DataType::TYPE_STRING);
//             $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['cmnd']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['firstname'] . ' ' . $info['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$department]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$department]['children'][$team]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$department]['children'][$team]['children'][$title]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['rm_name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($info['joined_at']) ));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($info['off_date'])):'' );
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), (empty($info['v_from_date']) || strtotime($info['v_from_date']) <= strtotime($from_date) )?'':date('d/m/Y', strtotime($info['v_from_date'])) );
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number_transfer[$info['id']]);
            
            $data_staff_train = array();
            $data_cmnd_train = array();
            if(!empty($info['id']) && !empty($data_training[$info['id']]))
            {
                $data_staff_train = $data_training[$info['id']];
            }
            
            if(!empty($info['cmnd']) && !empty($data_training[trim($info['cmnd'])])  )
            {
                $data_cmnd_train = $data_training[trim($info['cmnd'])];
            }

            if(empty($data_staff_train))
            {
                $data_staff_train = array();
            }

            $number_saturday = 0;
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                $number_leave = 0;
                $data_day = $value[$i];
                $text_day = '-';
                $date_current = strtotime($year . '-' . $month . '-' . $i);

                if(empty($value[$i]))
                {

                }
                else
                {
                    // if($info['company_group'] == 6 && date('D', strtotime(date('2017-03-' . $i))) != 'Sun')
                    // {
                    //     $text_day = 'X';
                    //     $array_number['number_work'] ++;
                    // }
                    // else
                    {
                        if( $date_current >= strtotime($info['v_from_date']) 
                        && ($date_current < strtotime($info['v_to_date']) || empty(strtotime($info['v_to_date'])) ) )
                        {
                            // if(in_array($i, $data_staff_train))
                            // {
                            //     $text_day = 'T';
                            // }
                            // else
                            // {
                            //     if(!empty($data_day['time_id']))
                            //     {
                            //         if(in_array($data_day['shift'], array(7, 8)))
                            //         {
                            //             $text_day = 'T';
                            //         }
                            //         elseif($data_day['approve_time'] != '')
                            //         {
                            //             switch ($data_day['approve_time']) {
                            //                 case 1:
                            //                     $text_day = 'X';
                            //                     break;
                            //                 case 0.5:
                            //                     $text_day = 'H';
                            //                     break;
                            //             }
                            //         }
                            //         elseif($data_day['status'] == 1)
                            //         {
                            //             switch ($data_day['shift']) {
                            //                 case 1:
                            //                     $text_day = 'X';
                            //                     break;
                            //                 case 0:
                            //                     $text_day = 'X';
                            //                     break;
                            //                 case 3:
                            //                     $text_day = 'X';
                            //                     break;
                            //                 case 6:
                            //                     $text_day = 'X';
                            //                     break;
                            //                 case 2:
                            //                     $text_day = 'G';
                            //                     break;
                            //             }
                            //         }
                            //     }
                            //     elseif(!empty($data_day['leave_type']) && empty($data_day['holiday_id']))
                            //     {
                            //         $text_day = 'P';
                            //     }

                            //     if(!empty($data_day['leave_type']) && empty($data_day['holiday_id']))
                            //     {
                            //         if(!empty($data_day['is_half_day']))
                            //         {
                            //             $number_leave = 0.5;
                            //         }
                            //         else
                            //         {
                            //             $number_leave = 1;
                            //         }
                            //     }

                            //     if(!empty($data_day['holiday_id']))
                            //     {
                            //         if($text_day == 'X')
                            //         {
                            //             $text_day = 'C';
                            //         }
                                    
                            //         if($text_day == 'H')
                            //         {
                            //             $text_day = 'D';
                            //         }
                            //     }
                            // }

                            // switch ($text_day) {
                            //     case 'X':
                            //         $array_number['number_work'] ++;
                            //         break;
                            //     case 'H':
                            //         $array_number['number_work'] += 0.5;
                            //         break;
                            //     case 'G':
                            //         $array_number['number_work'] ++;
                            //         $array_number['number_gay'] ++;
                            //         break;
                            //     case 'T':
                            //         $array_number['number_training'] ++;
                            //         break;
                            //     case 'C':
                            //         $array_number['number_sunday'] ++;
                            //         break;
                            //     case 'D':
                            //         $array_number['number_sunday'] += 0.5;
                            //         break;
                            // }

                            // $array_number['number_leave'] += $number_leave;


                            if(in_array($i, $data_staff_train) || in_array($i, $data_cmnd_train))
                            {
                                $text_day = 'T';
                            }
                            else
                            {
                                if(
                                    strtotime($year . '-' . $month . '-' . $i) >= strtotime($info['joined_at']) 
                                    && (
                                        strtotime($year . '-' . $month . '-' . $i) <= strtotime($info['off_date']) 
                                        || empty($info['off_date'])
                                    )
                                )
                                {
                                    if(!empty($data_day['time_id']))
                                    {   
                                        if(in_array($data_day['shift'], array(7, 8)))
                                        {
                                            $text_day = 'T';
                                        }
                                        elseif($data_day['approve_time'] != '')
                                        {
                                            switch ($data_day['approve_time']) {
                                                case 1:
                                                    $text_day = 'X';
                                                    break;
                                                case 0.5:
                                                    $text_day = 'H';
                                                    break;
                                            }
                                        }
                                        elseif($data_day['status'] == 1)
                                        {
                                            if($info['department'] == 159 && in_array($data_day['shift'], array(1,3)))
                                            {
                                                $text_day = 'H';
                                            }
                                            else
                                            {
                                                switch ($data_day['shift']) {
                                                    case 1:
                                                        $text_day = 'X';
                                                        break;
                                                    case 0:
                                                        $text_day = 'X';
                                                        break;
                                                    case 3:
                                                        $text_day = 'X';
                                                        break;
                                                    case 6:
                                                        $text_day = 'X';
                                                        break;
                                                    case 2:
                                                        $text_day = 'G';
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    elseif(!empty($data_day['leave_type'])  && empty($data_day['holiday_id']) && !in_array($info['company_group'], $special[$i]['list_group']))
                                    {
                                        $text_day = 'P';
                                    }
                                    
                                    if(!empty($data_day['leave_type'])  && empty($data_day['holiday_id'])  && !in_array($info['company_group'], $special[$i]['list_group']))
                                    {
                                        if(!empty($data_day['is_half_day']) || $text_day == 'H')
                                        {
                                            $number_leave = 0.5;
                                        }
                                        elseif($text_day != 'X')
                                        {
                                            $number_leave = 1;
                                        }
                                    }

                                    if(date('D', strtotime($year . '-' . $month . '-' . $i )) == 'Sat' 
                                        && in_array($info['company_group'], array(6, 7, 8, 9, 12)))
                                    {

                                    }

                                    if(!empty($data_day['holiday_id']))
                                    {
                                        if($text_day == 'X')
                                        {
                                            $text_day = 'C';
                                        }
                                        
                                        if($text_day == 'H')
                                        {
                                            $text_day = 'D';
                                        }
                                    }

                                    if(in_array($info['company_group'], $special[$i]['list_group']) && $data_day['time_id'] != 2332159)
                                    {
                                        if($text_day == 'X')
                                        {
                                            $text_day = 'L';
                                        }
                                        
                                        if($text_day == 'H')
                                        {
                                            $text_day = 'K';
                                        }

                                        if($text_day == '-')
                                        {
                                            $text_day = 'X';
                                            $array_number['number_nghi_huong_luong'] ++;
                                        }
                                    }
                                    elseif(!empty($special[$i]))
                                    {
                                        $text_day = 'X';
                                        $number_leave = 0;
                                        $array_number['number_nghi_huong_luong'] ++;
                                    }

                                    if($info['company_group'] == 6 && !in_array($i, array(7, 14, 21, 28,1, 2)) && empty($value['leave_type']) )
                                    {
                                        $text_day = 'X';
                                    }
                                }
                            }

                            

                            if($text_day == 'X')
                            {
                                $number_leave = 0;
                            }
                            
                        }
                    }
                }
                
                switch ($text_day) {
                    case 'X':
                        $array_number['number_work'] ++;
                        break;
                    case 'H':
                        $array_number['number_work'] += 0.5;
                        break;
                    case 'G':
                        $array_number['number_work'] ++;
                        $array_number['number_gay'] ++;
                        break;
                    case 'T':
                        $array_number['number_training'] ++;
                        break;
                    case 'C':
                        $array_number['number_sunday'] ++;
                        break;
                    case 'D':
                        $array_number['number_sunday'] += 0.5;
                        break;
                    case 'L':
                        $array_number['number_le'] ++;
                        break;
                    case 'K':
                        $array_number['number_le'] += 0.5;
                        break;
                }
                $array_number['number_leave'] += $number_leave;
               
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_work'] - $array_number['number_nghi_huong_luong']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_gay']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_training']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_leave']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_sunday']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_le']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_nghi_huong_luong']);

            if(in_array($info['company_group'], array(7, 8)))
            {
                if($number_saturday > 2)
                {
                    $number_saturday -= 2;
                }
                else
                {
                    $number_saturday = 0;
                }
            }
            else
            {
                $number_saturday = 0;
            }

            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $number_saturday);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['email']);

            $index++;
        }
        

        $data_staff_new = $QTime2->get_new_staff_time(array('month' => $month, 
                                                        'year' => $year, 
                                                        'code' => null,
                                                        'name' => null,
                                                        'area' => null));
        $array_ddtm = array(
                24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
        );
        foreach($data_staff_new as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();
            $company_name = (in_array($value['area_id'], $array_ddtm))?'Di Động Thông Minh':'OPPO';
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $company_name);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['code']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['firstname'] . ' ' . $value['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$value['department']]['children'][$value['team']]['children'][$value['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '');
            $data_staff_train = array();
            $data_training_day = json_decode($value['list_day'], true);
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                if(in_array($i, $data_training_day))
                {
                    $text_day = 'T';
                }
                else
                {
                    $text_day = '-';
                }
                
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), count($data_training_day));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 0);
            $index++;
        }
        
        // echo '<pre>';
        // print_r($data_staff_new); die;
        


        $filename = 'Export Time Total - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
        echo '<pre>';
        // print_r($data_export);
        echo 'done';
        exit;
    }

    public function exportFile($month, $year, $list_staff_id, $return = false)
    {
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
        $from_date = $year . '-' . $month . '-01';
        $to_date = $year . '-' . $month . '-' . $number_day_of_month;

        $QTime2 = new Application_Model_Time2();

        $params_special = array(
            'from_date' => $from_date,
            'to_date' => $to_date
        );

        $data_special = $QTime2->get_special_day($params_special);
        $special = array();
        foreach($data_special as $key => $val)
        {
            $data_date = $val;
            $data_date['list_group'] = json_decode($val['group'], true);
            $special[intval(date('d', strtotime($val['date'])))] = $data_date;
        }

        $params = array(
            'month' => $month,
            'year' => $year,
            'limit' => null,
            'offset' => null
        );

        $data_training = $QTime2->getListTrainingAll($params);

        $db = Zend_Registry::get('db');
        $sql = "SELECT st.id, st.firstname, st.lastname, st.email, 
                    st.code,
                    st.workingdate,
                    ar.name AS `area`,
                    cp.name AS `company_name`,
                    st.department,
                    st.title,
                    st.team,
                    st.ID_number AS `cmnd`,
                    st.is_officer,
                    st.joined_at AS `joined_at`,
                    st.off_date AS `off_date`,
                    std.number AS `number`, ada.`id` AS holiday_id,
                    dc.code as `holiday_special`
                    , t.*, lv.`leave_type`, lv.`is_half_day`
                    , cgm.company_group as `company_group`
                FROM
                (
                SELECT st.id, st.company_id, st.regional_market, st.title, st.joined_at, st.department, st.team, st.off_date, st.is_officer, st.ID_number, st.code, st.firstname, st.lastname, st.email, st.status, CONCAT(ad.`date`, ' 00:00:00') fromtime,  CONCAT(ad.`date`, ' 23:59:59') totime, ad.`date` AS workingdate
                FROM `staff` AS `st`
                CROSS JOIN (SELECT * FROM `all_date` WHERE MONTH(`date`) = $month 
                        AND YEAR(`date`) = $year) AS `ad` 
                WHERE
                     ( '" . $list_staff_id . "' IS NULL OR 
                        FIND_IN_SET( st.id, '" . $list_staff_id . "') )
                ) st
                LEFT JOIN (SELECT id as `time_id`, staff_id, created_at, shift, `status`, approve_time, real_time FROM `time` 
                        WHERE 
                            MONTH(`created_at`) = $month AND YEAR(`created_at`) = $year AND off <> 1) 
                    AS `t` ON t.staff_id = st.id 
                        AND (t.created_at >= st.fromtime AND t.created_at <= st.totime)
                LEFT JOIN 
                (SELECT staff_id, from_date, to_date, `leave_type`, `is_half_day` 
                    FROM `leave_detail` 
                    WHERE ( (from_date >=  '$from_date' AND from_date <=  '$to_date') 
                            OR (to_date >=  '$from_date' AND to_date <=  '$to_date') ) 
                            AND `status` = 1) lv ON st.id = lv.staff_id and st.workingdate between lv.from_date and lv.to_date
                INNER JOIN `company` AS `cp` ON st.company_id = cp.id
                INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
                LEFT JOIN `all_date_advanced` AS `ada` ON workingdate = ada.date
                                                AND ada.is_off = 1
                                                AND (
                                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                                )
                                                AND ada.category_date IS NOT NULL 
                                                AND ada.category_date = 2
                LEFT JOIN `date_category` AS `dc` ON dc.id = ada.category_date
                                            AND ada.category_date IS NOT NULL
                LEFT JOIN `saturday` AS `std` ON std.month = $month 
                                            AND std.year = $year
                                            AND std.type = 4 AND std.type_id = cgm.company_group 
                WHERE (st.off_date IS NULL OR st.off_date >= '$from_date')
                ORDER BY st.id
                ";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        $data_export = array();

        $data_return = array();

        foreach($data as $value)
        {
            if(empty($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]))
            {
                $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
            }
            else
            {
                if($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'] == ''
                || 
                $value['approve_time'] > $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'])
                {
                    $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
                }
            }
        }
        require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
        );

        $number_day_of_month = 31;

        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $heads[$alphaExcel->ShowAndUp()] = $i;
        }

        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

       if(!empty($_GET['hns']))
       {
           echo '<pre>';
           print_r($data_export); die;
       }
       
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $staff_return = array();
            $info = $value['1'];
            $staff_return['info'] = $info;
            $array_number = array(
                'number_work' => 0,
                'number_gay' => 0,
                'number_training' => 0,
                'number_leave' => 0,
                'number_sunday' => 0,
                'number_le' => 0,
            );
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['company_name']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['firstname'] . ' ' . $value['1']['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['children'][$info['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($info['joined_at']) ));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($info['off_date'])):'' );
            
            $staff_return['info']['department_name'] = $recursiveDeparmentTeamTitle[$info['department']]['name'];
            $staff_return['info']['team_name'] = $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['name'];
            $staff_return['info']['title_name'] = $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['children'][$info['title']]['name'];
            
            $data_staff_train = array();
            if(!empty($info['id']) && !empty($data_training[$info['id']]))
            {
                $data_staff_train = $data_training[$info['id']];
            }
            elseif(!empty($info['cmnd']) && !empty($data_training[trim($info['cmnd'])])  )
            {
                $data_staff_train = $data_training[trim($info['cmnd'])];
            }
            if(empty($data_staff_train))
            {
                $data_staff_train = array();
            }

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                
                $number_leave = 0;
                $data_day = $value[$i];
                $text_day = '-';


                if(in_array($i, $data_staff_train))
                {
                    $text_day = 'T';
                }
                else
                {
                    if(
                        strtotime($year . '-' . $month . '-' . $i) >= strtotime($info['joined_at']) 
                        && (
                            strtotime($year . '-' . $month . '-' . $i) <= strtotime($info['off_date']) 
                            || empty($info['off_date'])
                        )
                    )
                    {
                        if(!empty($data_day['time_id']))
                        {   
                            if(in_array($data_day['shift'], array(7, 8)))
                            {
                                $text_day = 'T';
                            }
                            elseif($data_day['approve_time'] != '')
                            {
                                switch ($data_day['approve_time']) {
                                    case 1:
                                        $text_day = 'X';
                                        break;
                                    case 0.5:
                                        $text_day = 'H';
                                        break;
                                }
                            }
                            elseif($data_day['status'] == 1)
                            {
                                if($info['department'] == 159 && in_array($data_day['shift'], array(1,3)))
                                {
                                    $text_day = 'H';
                                }
                                else
                                {
                                    switch ($data_day['shift']) {
                                        case 1:
                                            $text_day = 'X';
                                            break;
                                        case 0:
                                            $text_day = 'X';
                                            break;
                                        case 3:
                                            $text_day = 'X';
                                            break;
                                        case 6:
                                            $text_day = 'X';
                                            break;
                                        case 2:
                                            $text_day = 'G';
                                            break;
                                    }
                                }
                            }
                        }elseif(!empty($data_day['leave_type'])  && empty($data_day['holiday_id']) && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            $text_day = 'P';
                        }
                        
                        if(!empty($data_day['leave_type'])  && empty($data_day['holiday_id'])  && !in_array($info['company_group'], $special[$i]['list_group']))
                        {
                            if(!empty($data_day['is_half_day']) || $text_day == 'H')
                            {
                                $number_leave = 0.5;
                            }
                            elseif($text_day != 'X')
                            {
                                $number_leave = 1;
                            }
                        }

                        if(!empty($data_day['holiday_id']))
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'C';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'D';
                            }
                        }

                        if(in_array($info['company_group'], $special[$i]['list_group']) && $data_day['time_id'] != 2332159)
                        {
                            if($text_day == 'X')
                            {
                                $text_day = 'L';
                            }
                            
                            if($text_day == 'H')
                            {
                                $text_day = 'K';
                            }

                            if($text_day == '-')
                            {
                                // $text_day = 'X';
                            }
                        }
                        elseif(!empty($special[$i]))
                        {
                            // if(!empty($GET['check'])){
                                
                            // }else{
                            //     $text_day = 'X';
                            //     $number_leave = 0;
                            // }
                            
                        }

                        if($info['company_group'] == 6 && !in_array($i, array(7, 14, 21, 28,1, 2)) && empty($data_day['leave_type']) )
                        {
                            $text_day = 'X';
                        }
                        elseif($info['company_group'] == 6 && in_array($i, array(7, 14, 21, 28,1, 2)))
                        {
                            $text_day = '-';
                        }
                    }
                }

                // if($info['company_group'] == 6 && !in_array($i, array(7, 14, 21, 28,1, 2)) )
                // {
                //     $text_day = 'X';
                // }

                if($text_day == 'X')
                {
                    $number_leave = 0;
                }

                

                switch ($text_day) {
                    case 'X':
                        $array_number['number_work'] ++;
                        break;
                    case 'H':
                        $array_number['number_work'] += 0.5;
                        break;
                    case 'G':
                        $array_number['number_work'] ++;
                        $array_number['number_gay'] ++;
                        break;
                    case 'T':
                        $array_number['number_training'] ++;
                        break;
                    case 'C':
                        $array_number['number_sunday'] ++;
                        break;
                    case 'D':
                        $array_number['number_sunday'] += 0.5;
                        break;
                    case 'L':
                        $array_number['number_le'] ++;
                        break;
                    case 'K':
                        $array_number['number_le'] += 0.5;
                        break;
                }
                $array_number['number_leave'] += $number_leave;
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_work']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_gay']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_training']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_leave']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_sunday']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $array_number['number_le']);
            $staff_return['number'] = $array_number;
            $data_return[] = $staff_return;
            $index++;
        }

        if($return == true)
        {
            return $data_return;
        }

        
        $filename = 'Export Time - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
        echo '<pre>';
        // print_r($data_export);
        echo 'done';
        exit;
    }

    public function viewCheckIn($month, $year, $list_staff_id)
    {
        $month = intval($month);
        $year = intval($year);

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
        $from_date = $year . '-' . $month . '-01';
        $to_date = $year . '-' . $month . '-' . $number_day_of_month;

        $QTime2 = new Application_Model_Time2();

        $params_special = array(
            'from_date' => $from_date,
            'to_date' => $to_date
        );

        $data_special = $QTime2->get_special_day($params_special);
        $special = array();
        foreach($data_special as $key => $val)
        {
            $data_date = $val;
            $data_date['list_group'] = json_decode($val['group'], true);
            $special[intval(date('d', strtotime($val['date'])))] = $data_date;
        }

        $params = array(
            'month' => $month,
            'year' => $year,
            'limit' => null,
            'offset' => null
        );

        $data_training = $QTime2->getListTrainingAll($params);

        $db = Zend_Registry::get('db');
        $sql = "SELECT st.id, st.firstname, st.lastname, st.email, 
                    st.code,
                    st.workingdate,
                    ar.name AS `area`,
                    cp.name AS `company_name`,
                    st.department,
                    st.title,
                    st.team,
                    st.ID_number AS `cmnd`,
                    st.is_officer,
                    st.joined_at AS `joined_at`,
                    st.off_date AS `off_date`,
                    std.number AS `number`, ada.`id` AS holiday_id,
                    dc.code as `holiday_special`
                    , t.*, lv.`leave_type`, lv.`is_half_day`
                    , cgm.company_group as `company_group`
                FROM
                (
                SELECT st.id, st.company_id, st.regional_market, st.title, st.joined_at, st.department, st.team, st.off_date, st.is_officer, st.ID_number, st.code, st.firstname, st.lastname, st.email, st.status, CONCAT(ad.`date`, ' 00:00:00') fromtime,  CONCAT(ad.`date`, ' 23:59:59') totime, ad.`date` AS workingdate
                FROM `staff` AS `st`
                CROSS JOIN (SELECT * FROM `all_date` WHERE MONTH(`date`) = $month 
                        AND YEAR(`date`) = $year) AS `ad` 
                WHERE ( '" . $list_staff_id . "' IS NULL OR 
                        FIND_IN_SET( st.id, '" . $list_staff_id . "')  )
                ) st
                LEFT JOIN (SELECT id as `time_id`, staff_id, created_at, shift, `status`, approve_time, real_time FROM `time` 
                        WHERE 
                            MONTH(`created_at`) = $month AND YEAR(`created_at`) = $year AND off <> 1) 
                    AS `t` ON t.staff_id = st.id 
                        AND (t.created_at >= st.fromtime AND t.created_at <= st.totime)
                LEFT JOIN 
                (SELECT staff_id, from_date, to_date, `leave_type`, `is_half_day` 
                    FROM `leave_detail` 
                    WHERE ( (from_date >=  '$from_date' AND from_date <=  '$to_date') 
                            OR (to_date >=  '$from_date' AND to_date <=  '$to_date') ) 
                            AND `status` = 1) lv ON st.id = lv.staff_id and st.workingdate between lv.from_date and lv.to_date
                INNER JOIN `company` AS `cp` ON st.company_id = cp.id
                INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
                LEFT JOIN `all_date_advanced` AS `ada` ON workingdate = ada.date
                                                AND ada.is_off = 1
                                                AND (
                                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                                )
                                                AND ada.category_date IS NOT NULL 
                                                AND ada.category_date = 2
                LEFT JOIN `date_category` AS `dc` ON dc.id = ada.category_date
                                            AND ada.category_date IS NOT NULL
                LEFT JOIN `saturday` AS `std` ON std.month = $month 
                                            AND std.year = $year
                                            AND std.type = 4 AND std.type_id = cgm.company_group 
                WHERE (st.off_date IS NULL OR st.off_date >= '$from_date')
                ORDER BY st.id
                ";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        $data_export = array();
        foreach($data as $value)
        {
            if(empty($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]))
            {
                $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
            }
            else
            {
                if($data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'] == ''
                || $value['approve_time'] > $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))]['approve_time'])
                {
                    $data_export[intval($value['id'])][intval(date('d', strtotime($value['workingdate'])))] = $value;
                }
            }
        }

        require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
        );

        // $number_day_of_month = 31;

        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $heads[$alphaExcel->ShowAndUp()] = $i;
        }

        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        // print_r($recursiveDeparmentTeamTitle); die;

        $index = 1;
        foreach($data_export as $key => &$value)
        {
            $info = $value['1'];
            $array_number = array(
                'number_work' => 0,
                'number_gay' => 0,
                'number_training' => 0,
                'number_leave' => 0,
                'number_sunday' => 0,
            );
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['company_name']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $info['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['firstname'] . ' ' . $value['1']['lastname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $recursiveDeparmentTeamTitle[$info['department']]['children'][$info['team']]['children'][$info['title']]['name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $info['area']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($info['joined_at']) ));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($info['off_date'])):'' );
            $data_staff_train = array();
            if(!empty($info['id']) && !empty($data_training[$info['id']]))
            {
                $data_staff_train = $data_training[$info['id']];
            }
            if(empty($data_staff_train))
            {
                $data_staff_train = array();
            }
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                
                $number_leave = 0;
                $data_day = $value[$i];
                $text_day = '-';
                if(in_array($i, $data_staff_train))
                {
                    $text_day = 'T';
                }
                else
                {
                    if(!empty($data_day['time_id']))
                    {

                        if(in_array($data_day['shift'], array(7, 8)))
                        {
                            $text_day = 'T';
                        }
                        elseif($data_day['approve_time'] != '')
                        {
                            switch ($data_day['approve_time']) {
                                case 1:
                                    $text_day = 'X';
                                    break;
                                case 0.5:
                                    $text_day = 'H';
                                    break;
                            }
                        }
                        elseif($data_day['status'] == 1)
                        {
                            if($info['department'] == 159 && in_array($data_day['shift'], array(1,3)))
                            {
                                $text_day = 'H';
                            }
                            else
                            {
                                switch ($data_day['shift']) {
                                    case 1:
                                        $text_day = 'X';
                                        break;
                                    case 0:
                                        $text_day = 'X';
                                        break;
                                    case 3:
                                        $text_day = 'X';
                                        break;
                                    case 6:
                                        $text_day = 'X';
                                        break;
                                    case 2:
                                        $text_day = 'G';
                                        break;
                                }
                            }
                        }
                    }elseif(!empty($data_day['leave_type'])  && empty($data_day['holiday_id']) && !in_array($info['company_group'], $special[$i]['list_group']))
                    {
                        $text_day = 'P';
                    }

                    if(!empty($data_day['leave_type'])  && empty($data_day['holiday_id'])  && !in_array($info['company_group'], $special[$i]['list_group']))
                    {
                        if(!empty($data_day['is_half_day']) || $text_day == 'H')
                        {
                            
                            $number_leave = 0.5;
                        }
                        elseif($text_day != 'X')
                        {
                            $number_leave = 1;
                        }
                    }

                    if(!empty($data_day['holiday_id']))
                    {
                        if($text_day == 'X')
                        {
                            $text_day = 'C';
                        }
                        
                        if($text_day == 'H')
                        {
                            $text_day = 'D';
                        }
                    }

                    if(in_array($info['company_group'], $special[$i]['list_group']))
                    {
                        if($text_day == 'X')
                        {
                            $text_day = 'L';
                        }
                        
                        if($text_day == 'H')
                        {
                            $text_day = 'K';
                        }

                        if($text_day == '-')
                        {
                            // $text_day = 'X';
                        }
                    }
                }

                if($text_day == 'X')
                {
                    $number_leave = 0;
                }

                

                switch ($text_day) {
                    case 'X':
                        $array_number['number_work'] ++;
                        break;
                    case 'H':
                        $array_number['number_work'] += 0.5;
                        break;
                    case 'G':
                        $array_number['number_work'] ++;
                        $array_number['number_gay'] ++;
                        break;
                    case 'T':
                        $array_number['number_training'] ++;
                        break;
                    case 'C':
                        $array_number['number_sunday'] ++;
                        break;
                    case 'D':
                        $array_number['number_sunday'] += 0.5;
                        break;
                }
                $array_number['number_leave'] += $number_leave;
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $text_day);
            }
            $value['number_work'] = $array_number['number_work'];
            $value['number_gay'] = $array_number['number_gay'];
            $value['number_training'] = $array_number['number_training'];
            $value['number_leave'] = $array_number['number_leave'];
            $value['number_sunday'] = $array_number['number_sunday'];
            $index++;
        }
        return $data_export;
    }
	
	public function exportDetailAction(){
		
        set_time_limit(0);
        ini_set('memory_limit', '5120M');
        
        $db = Zend_Registry::get('db');
        
        $sql = "SELECT 
			tm.staff_id as `staff_id`, tm.	check_in_day as `check_in_day`, 
			CONCAT(HOUR(tm.check_in_at), ':', MINUTE(tm.check_in_at)) as `check_in_time`,
			CONCAT(HOUR(tm.check_out_at), ':', MINUTE(tm.check_out_at)) as `check_out_time`,
			st.code `staff_code`,
			dp.name dep, 
			te.name team, 
			ti.name title,
			CONCAT(st.firstname, ' ', st.lastname) `name`,
			st.ID_Number `cmnd`
FROM  `time_machine` tm
JOIN `staff` st ON tm.staff_id = st.id
LEFT JOIN team ti ON st.title = ti.id
LEFT JOIN team dp ON st.department = dp.id
LEFT JOIN team te ON te.id = st.team

WHERE  `check_in_day` 
BETWEEN  '2017-6-1'
AND  '2017-6-30'
ORDER BY tm.staff_id ASC ";
        
        $stmt = $db->prepare($sql);
        
        $stmt->execute();
        $data_export = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        
        require_once 'PHPExcel.php';
        
        $alphaExcel = new My_AlphaExcel();
        
        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
			$alphaExcel->ShowAndUp() => 'Ngày',
            $alphaExcel->ShowAndUp() => 'Check In',
            $alphaExcel->ShowAndUp() => 'Check Out',
        );
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }
        
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();
        
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($value['check_in_day'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['check_in_time'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['check_out_time'],PHPExcel_Cell_DataType::TYPE_STRING);	
            $index++;
        
        }
        $filename = 'Export Detail Time - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        
        exit;
 
    }

    public function listStaffCheckInAction()
    {
        if(empty($_GET['dev'])){
            require_once 'staff-time' . DIRECTORY_SEPARATOR . 'list-staff-check-in.php';
        }else{
            $not_approve_self = array(LEADER_ID,SALES_ID, PB_SALES_ID, PGPB_ID);
            $page  = $this->getRequest()->getParam('page', 1);
            $name  = $this->getRequest()->getParam('name');
            $code  = $this->getRequest()->getParam('code');
            $email  = $this->getRequest()->getParam('email');
            $export = $this->getRequest()->getParam('export');
            $only_training = $this->getRequest()->getParam('only_training', '0');
            $month = $this->getRequest()->getParam('month', date('m'));
            $year = $this->getRequest()->getParam('year', date('Y'));
            $area = $this->getRequest()->getParam('area');
            $department = $this->getRequest()->getParam('department');
            $team = $this->getRequest()->getParam('team');
            $title = $this->getRequest()->getParam('title');
            $export_hr = $this->getRequest()->getParam('export_hr', 0);
            $export_hr_total = $this->getRequest()->getParam('export_hr_total', 0);
            $limit = 20;
            $QShift  = new Application_Model_Shift();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QTime = new Application_Model_Time2();
            $note = $this->getRequest()->getParam('note', null);
            $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
            $from_date    = $year . '-' . $month . '-01';
            $to_date      = $year . '-' . $month . '-' . $number_day_of_month;
            
            if(!empty($export_hr))
            {
            
                set_time_limit(0);
                ini_set('memory_limit', '5120M');
            
                $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
            
                $from_date    = $year . '-' . $month . '-01';
                $to_date      = $year . '-' . $month . '-' . $number_day_of_month;
            
                $db           = Zend_Registry::get('db');
                $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
                $sql          = "CALL `PR_Export_Staff_Time_New`(:from_date, :to_date)";
            
                $stmt = $db->prepare($sql);
                $stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
                $stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
            
                $stmt->execute();
                $data_export = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;
            
                require_once 'PHPExcel.php';
            
                $alphaExcel = new My_AlphaExcel();
            
                $PHPExcel = new PHPExcel();
                $heads = array(
                    $alphaExcel->ShowAndUp() => 'Company',
                    $alphaExcel->ShowAndUp() => 'Code',
                    $alphaExcel->ShowAndUp() => 'CMND',
                    $alphaExcel->ShowAndUp() => 'Name',
                    $alphaExcel->ShowAndUp() => 'Department',
                    $alphaExcel->ShowAndUp() => 'Team',
                    $alphaExcel->ShowAndUp() => 'Title',
                    $alphaExcel->ShowAndUp() => 'Area',
                    $alphaExcel->ShowAndUp() => 'Join date',
                    $alphaExcel->ShowAndUp() => 'Off date',
                );
            
                for($i = 1; $i <= $number_day_of_month; $i++)
                {
                    $heads[$alphaExcel->ShowAndUp()] = $i;
                }
            
                $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
                $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
                $heads[$alphaExcel->ShowAndUp()] = 'Công training';
                $heads[$alphaExcel->ShowAndUp()] = 'Phép';
                $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
                $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
            
                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();
            
                foreach($heads as $key => $value)
                {
                    $sheet->setCellValue($key.'1', $value);
                }
            
                $index = 1;
                foreach($data_export as $key => $value)
                {
                    $alphaExcel = new My_AlphaExcel();
            
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['com_name']);
                    //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);
                    //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'');
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
            
                    for($i = 1; $i <= $number_day_of_month; $i++)
                    {
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value[$i]);
                    }
            
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congthuong']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['conggay']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congtraining']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phep']+$value['phepcty']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congchunhat']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congle']);
                    	
                    $index++;
            
                }
                $filename = 'Export Time - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            
            
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
            
                exit;
            
            }
            
            if(!empty($export_hr_total))
            {
                //$this->exportTotal($month, $year);
                set_time_limit(0);
                ini_set('memory_limit', '5120M');
            
            
                $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
            
                $from_date    = $year . '-' . $month . '-01';
                $to_date      = $year . '-' . $month . '-' . $number_day_of_month;
            
                $db           = Zend_Registry::get('db');
                $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
                $sql          = "CALL `PR_Export_Total_Staff_Time_New`(:from_date, :to_date)";
            
                $stmt = $db->prepare($sql);
                $stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
                $stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
            
                $stmt->execute();
                $data_export = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;
            
                require_once 'PHPExcel.php';
            
                $alphaExcel = new My_AlphaExcel();
            
                $PHPExcel = new PHPExcel();
                	
                $heads_month = array(
                    'K' => '1',
                    'L' => '2',
                    'M' => '3',
                    'N' => '4',
                    'O' => '5',
                    'P' => '6',
                    'Q' => '7',
                    'R' => '8',
                    'S' => '9',
                    'T' => '10',
                    'U' => '11',
                    'V' => '12',
                    'W' => '13',
                    'X' => '14',
                    'Y' => '15',
                    'Z' => '16',
                    'AA' => '17',
                    'AB' => '18',
                    'AC' => '19',
                    'AD' => '20',
                    'AE' => '21',
                    'AF' => '22',
                    'AG' => '23',
                    'AH' => '24',
                    'AI' => '25',
                    'AJ' => '26',
                    'AK' => '27',
                    'AL' => '28',
                    'AM' => '29',
                    'AN' => '30',
                    'AO' => '31'
                );
            
            
                $heads_mid = array_slice($heads_month,0,$number_day_of_month);
                $heads_first = array(
                    'A' => 'Company',
                    'B' => 'Code',
                    'C' => 'CMND',
                    'D' => 'Name',
                    'E' => 'Department',
                    'F' => 'Team',
                    'G' => 'Title',
                    'H' => 'Area',
                    'I' => 'Join date',
                    'J' => 'Off date'
                );
            
                $heads_last = array(
                    'AP' => 'Công thường',
                    'AQ' => 'Công gãy',
                    'AR' => 'Công training',
                    'AS' => 'Công chủ nhật',
                    'AU' => 'Ngày lễ X2',
                    'AV' => 'Ngày lễ X3',
                    'AW' => 'Ngày lễ X4',
                    'AX' => 'Thứ 7 X2',
                    'AY' => 'Phép năm sử dụng trong tháng',
                    'AZ' => 'Ứng phép/Hoàn phép',
                    'BA' => 'Nghỉ việc riêng được hưởng lương',
                    'BB' => 'Tổng giờ tăng ca trong tháng',
                    'BC' => 'Ngày nghỉ được hưởng lương',
                );
                $heads = array_merge($heads_first,$heads_mid,$heads_last);
            
            
                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();
            
                /*
                 $heads = array(
                 $alphaExcel->ShowAndUp() => 'Company',
                 $alphaExcel->ShowAndUp() => 'Code',
                 $alphaExcel->ShowAndUp() => 'CMND',
                 $alphaExcel->ShowAndUp() => 'Name',
                 $alphaExcel->ShowAndUp() => 'Department',
                 $alphaExcel->ShowAndUp() => 'Team',
                 $alphaExcel->ShowAndUp() => 'Title',
                 $alphaExcel->ShowAndUp() => 'Area',
                 $alphaExcel->ShowAndUp() => 'Join date',
                 $alphaExcel->ShowAndUp() => 'Off date',
                 );
                 	
                 $heads = array(
                 'Company',
                 'Code',
                 'CMND',
                 'Name',
                 'Department',
                 'Team',
                 'Title',
                 'Area',
                 'Join date',
                 'Off date',
                 );
            
                 	
                 for($i = 1; $i <= $number_day_of_month; $i++)
                 {
                 //$heads[$alphaExcel->ShowAndUp()] = $i;
                 $heads[] = $i;
                 }
                 */
                foreach($heads as $key => $value)
                    $sheet->setCellValue($key.'1', $value);
                    /*
                     $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
                     $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
                     $heads[$alphaExcel->ShowAndUp()] = 'Công training';
                     $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
                     $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
                     $heads[$alphaExcel->ShowAndUp()] = 'Ngày lễ X2';
                     $heads[$alphaExcel->ShowAndUp()] = 'Ngày lễ X3';
                     $heads[$alphaExcel->ShowAndUp()] = 'Ngày lễ X4';
                     $heads[$alphaExcel->ShowAndUp()] = 'Thứ 7 X2';
                     $heads[$alphaExcel->ShowAndUp()] = 'Phép năm sử dụng trong tháng';
                     $heads[$alphaExcel->ShowAndUp()] = 'Ứng phép/Hoàn phép';
                     $heads[$alphaExcel->ShowAndUp()] = 'Nghỉ việc riêng được hưởng lương';
                     $heads[$alphaExcel->ShowAndUp()] = 'Tổng giờ tăng ca trong tháng';
                     $heads[$alphaExcel->ShowAndUp()] = 'Ngày nghỉ được hưởng lương';
            
                     */
                    	
            
                    	
                    $PHPExcel->setActiveSheetIndex(0);
                    $sheet = $PHPExcel->getActiveSheet();
                    /*
                     foreach($heads as $key => $value)
                     {
                     $sheet->setCellValue($key.'1', $value);
                     //	$sheet->setCellValue($key.'1', $value);
                     }
                     */
                    $index = 1;
                    foreach($data_export as $key => $value)
                    {
                        $alphaExcel = new My_AlphaExcel();
            
                        $sheet->setCellValue('A' . ($index+1), $value['com_name']);
                        //$sheet->setCellValue('B' . ($index+1), $value['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                        //$sheet->setCellValue('C' . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValueExplicit('B' . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValueExplicit('C' . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);
            
                        $sheet->setCellValue('D' . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue('E' . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue('F' . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue('G' . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue('H' . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue('I' . ($index+1), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'');
                        $sheet->setCellValue('J' . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
            
            
                        for($i = 1; $i <= $number_day_of_month; $i++)
                        {
                            $sheet->setCellValue(array_search($i,$heads_mid) . ($index+1), $value[$i]);
                        }
            
                        $sheet->setCellValue('AP' . ($index+1), $value['congthuong']);
                        $sheet->setCellValue('AQ' . ($index+1), $value['conggay']);
                        $sheet->setCellValue('AR' . ($index+1), $value['congtraining']);
                        $sheet->setCellValue('AS' . ($index+1), $value['congchunhat']);
                        $sheet->setCellValue('AU' . ($index+1), $value['holidayX2']);
                        $sheet->setCellValue('AV' . ($index+1), $value['holidayX3']);
                        $sheet->setCellValue('AW' . ($index+1), $value['holidayX4']);
                        $sheet->setCellValue('AX' . ($index+1), $value['saturdayX2']);
                        $sheet->setCellValue('AY' . ($index+1), $value['phepnam']);
                        $sheet->setCellValue('AZ' . ($index+1), $value['tamungphep']);
                        $sheet->setCellValue('BA' . ($index+1), $value['phepcty']);
                        $sheet->setCellValue('BB' . ($index+1), $value['overtime']);
                        $sheet->setCellValue('BC' . ($index+1), $value['lehuongluong']);
            
                        	
                        $index++;
            
                    }
                    	
                    /*
                     foreach($data_export as $key => $value)
                     {
                     $alphaExcel = new My_AlphaExcel();
            
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['com_name']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($value['joined_at']) ));
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
            
                     for($i = 1; $i <= $number_day_of_month; $i++)
                     {
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value[$i]);
                     }
            
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congthuong']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['conggay']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congtraining']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congchunhat']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congle']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['holidayX2']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['holidayX3']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['holidayX4']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['saturdayX2']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phepnam']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['tamungphep']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phepcty']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['overtime']);
                     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['lehuongluong']);
            
                     	
                     $index++;
            
                     }
                     	
                     */
                    $filename = 'Export Time Total - ' . date('Y-m-d H-i-s');
                    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            
            
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                    $objWriter->save('php://output');
            
                    exit;
            }
            
            if(in_array($userStorage->group_id, $not_approve_self))
            {
                $this->view->not_approve_self = 1;
            }
            else
            {
                $this->view->not_approve_self = 0;
            }
            
            $approve_all_by_staff = $this->getRequest()->getParam('approve_all_by_staff');
            $deapprove_all_by_staff = $this->getRequest()->getParam('deapprove_all_by_staff');
            $date_approve = $this->getRequest()->getParam('date-approve');
            $date_none_approve = $this->getRequest()->getParam('date-none-approve');
            
            if(!empty($date_approve))
            {
                $staff_approve = $this->getRequest()->getParam('staff_approve');
                $params_approve_date = array(
                    'staff_id' => $staff_approve,
                    'date' => $date_approve,
                    'admin' => $userStorage->id,
                    'note' => ''
                );
                $QTime->approveAllTempTimeByStaffDate($params_approve_date);
            }
            
            
            
            if(!empty($approve_all_by_staff))
            {
                $month_edit = $this->getRequest()->getParam('month_edit');
                $year_edit = $this->getRequest()->getParam('year_edit');
            
                $params_approve_all = array(
                    'staff_id' => $approve_all_by_staff,
                    'month' => $month_edit,
                    'year' => $year_edit,
                    'admin' => $userStorage->id,
                    'note' => ''
                );
                $QTime->approveAllTempTimeByStaff($params_approve_all);
            
            }
            
            if($this->getRequest()->getParam('approve-none-office') )
            {
                $staff_id = $this->getRequest()->getParam('staff_id');
                $date = $this->getRequest()->getParam('date');
                $QStaff = new Application_Model_Staff();
                $currentStaff = $QStaff->find($staff_id);
                $currentStaff = $currentStaff[0];
            
                $db  = Zend_Registry::get('db');
                $sql = "
                UPDATE time t
                    JOIN staff s ON t.staff_id = s.id
                SET t.status = 1,
                    t.updated_at = '" . date('Y-m-d h:i:s') . "' ,
                    t.approved_at = '" . date('Y-m-d h:i:s') . "' ,
                    t.updated_by = '" . $userStorage->id . "',
                    t.approved_by = '" . $userStorage->id . "',
                t.regional_market = ". $currentStaff->regional_market . "
                WHERE date(t.created_at) = '" . $date ."'
                    AND t.staff_id = " . $staff_id ."
                    AND s.joined_at <= '". $date .
                                "' AND (s.off_date IS NULL OR s.off_date > '" . $date . "')";
                $db->query($sql);
            }
            
            if($this->getRequest()->getParam('unapprove-none-office'))
            {
                $staff_id = $this->getRequest()->getParam('staff_id');
                $date = $this->getRequest()->getParam('date');
            
                $sql = "UPDATE `time`
                    SET status = 0, approve_time = 0, approved_by = NULL
                    WHERE DATE(created_at) = :date
                        AND staff_id = :staff_id";
                $db  = Zend_Registry::get('db');
            
                $stmt = $db->prepare($sql);
                $stmt->bindParam("date", $date, PDO::PARAM_STR);
                $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
            
                $stmt->execute();
                $stmt->closeCursor();
                $stmt = $db = null;
            }
            
            if($this->getRequest()->getParam('approve-office'))
            {
                $staff_id = $this->getRequest()->getParam('staff_id');
                $date = $this->getRequest()->getParam('date');
                $QTime->approveOffice($staff_id, $date, $userStorage->id);
            }
            
            if($this->getRequest()->getParam('approve-office'))
            {
                $staff_id = $this->getRequest()->getParam('staff_id');
                $date = $this->getRequest()->getParam('date');
                $QTime->approveOffice($staff_id, $date, $userStorage->id);
            }
            
            if($this->getRequest()->getParam('hr-approve-office'))
            {
                $hr_time = $this->getRequest()->getParam('hr_time');
                $staff_id = $this->getRequest()->getParam('staff_id');
                $date = $this->getRequest()->getParam('date_detail');
                $QTime->approveOfficeHR($staff_id, $date, $hr_time, $userStorage->id);
            }
            
            if($this->getRequest()->getParam('approve-temp-time'))
            {
                $id_update = $this->getRequest()->getParam('approve-temp-time');
                $QTime->approveTempTime($id_update, $note);
            }
            
            if($this->getRequest()->getParam('btn_reject_temp'))
            {
                $temp_id_reject = $this->getRequest()->getParam('id_temp_reject');
                $reason_reject = $this->getRequest()->getParam('reason_reject');
                $QTime->rejectTempTime($temp_id_reject, $reason_reject);
            }
            
            if($this->getRequest()->getParam('approve-all-temp-time'))
            {
            
                $month_approve_all = $this->getRequest()->getParam('month_approve_all');
                $year_approve_all = $this->getRequest()->getParam('year_approve_all');
                $user_approve_all = $this->getRequest()->getParam('user_approve_all');
            
                $params_approve_all = array(
                    'month' => $month_approve_all,
                    'year' => $year_approve_all,
                    'staff_id' => $user_approve_all,
                    'note' => $note,
                );
            
                $QTime->approveAllTempTimeByStaff($params_approve_all);
            }
            
            if(!empty($this->getRequest()->getParam('submit_approve_all')))
            {
                $QLeave = new Application_Model_LeaveDetail();
                $json_temp_id = json_decode($this->getRequest()->getParam('list_temp_id'), true);
                $json_remove_time = json_decode($this->getRequest()->getParam('list_remove_time'), true);
                $json_leave_id = array_unique(json_decode($this->getRequest()->getParam('list_leave_id'), true));
                $json_time_id = array_unique(json_decode($this->getRequest()->getParam('list_time_id'), true));
                $staff_id = $this->getRequest()->getParam('staff_id_approve');
            
                $params_approve_select = array(
                    'temp_id' => $json_temp_id,
                    'leave_id' => $json_leave_id,
                    'remove_time' => $json_remove_time,
                    'staff_id' => $staff_id,
                    'admin' => $userStorage->id
                );
                $QTime->approveSelect($params_approve_select);
            
                $db = Zend_Registry::get('db');
                if(!empty($json_time_id))
                {
                    $sql = "UPDATE time set status = 1 where id IN (" . implode(",", $json_time_id) . ")";
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                    $stmt->closeCursor();
                    $stmt = null;
                }
            
                if(!empty($json_remove_time))
                {
                    $sql = "DELETE FROM time where staff_id = " . $staff_id . " AND id IN (" . implode(",", $json_remove_time) . ")";
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                    $stmt->closeCursor();
                    $stmt = null;
            
                    // $sql = "DELETE FROM time_image_location where  time_id IN (" . implode(",", $json_remove_time) . ")";
                    // $stmt = $db->prepare($sql);
                    // $stmt->execute();
                    // $stmt->closeCursor();
                    // $stmt = null;
                }
            
                foreach($json_leave_id as $val_leave)
                {
                    $QLeave->updateStatus(array('status'=>1,'id'=>$val_leave));
                }
                $db = null;
            }
            
            if($this->getRequest()->getParam('update-none-office')) // Nếu HR hoặc ASM cập nhật công
            {
                $QTime = new Application_Model_Time2();
            
                $staff_id = $this->getRequest()->getParam('staff_id');
                $date = $this->getRequest()->getParam('date_detail');
                $shift= $this->getRequest()->getParam('shift');
                $QStaff = new Application_Model_Staff();
                $currentStaff = $QStaff->find($staff_id);
                $currentStaff = $currentStaff[0];
                $db  = Zend_Registry::get('db');
                $select = $db->select()
                ->from(array('t' => 'time'))
                ->where('date(t.created_at) = ?', $date)
                ->where('t.staff_id = ?', $staff_id);
                $check_data = $db->fetchAll($select);
                if(count($check_data) > 0)
                {
                    $sql = "
                    UPDATE time t
                    JOIN staff s ON t.staff_id = s.id
                    SET t.shift = $shift,
                    t.updated_at = '" . date('Y-m-d h:i:s') . "' ,
                        t.approved_at = '" . date('Y-m-d h:i:s') . "' ,
                        t.updated_by = '" . $userStorage->id . "',
                        t.approved_by = '" . $userStorage->id . "',
                        t.status = 1,
                    t.regional_market = ". $currentStaff->regional_market . "
                    WHERE date(t.created_at) = '" . $date ."'
                        AND t.staff_id = " . $staff_id ."
                        AND s.joined_at <= '". $date .
                                    "' AND (s.off_date IS NULL OR s.off_date > '" . $date . "')";
            
                    $db->query($sql);
                }
                else
                {
            
                    if(strtotime($currentStaff->joined_at) <= strtotime($date) && (strtotime($currentStaff->off_date) > strtotime($date) || empty($currentStaff->off_date)))
                    {
                        $data['shift'] = $shift;
                        $data['created_at'] = date($date .' h:i:s');
                        $data['approved_at'] = date('Y-m-d h:i:s');
                        $data['created_by'] = $userStorage->id;
                        $data['approved_by'] = $userStorage->id;
                        $data['staff_id'] = $staff_id;
                        $data['status'] = 1;
                        $QTime->insert($data);
                    }
                    else
                    {
                    }
                }
            }
            $QStaffPermisson = new Application_Model_StaffPermission();
            // if($QStaffPermisson->checkPermission($userStorage->code) || in_array($userStorage->title, array(SALE_SALE_ASM, SALES_ADMIN_TITLE, SALES_TITLE, SALES_LEADER_TITLE)) || in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID)))
            {
                $QTime = new Application_Model_Time2();
                $list_allowance = $QTime->getAllowance();
                $params = array(
                    'off' => 1,
                    'name' => $name,
                    'code' => $code,
                    'email' => $email,
                    'only_training' => $only_training,
                    'month' => $month,
                    'year' => $year,
                    'area' => $area,
                    'department' => $department,
                    'team' => $team,
                    'title' => $title,
                );
            
                $user_id = $userStorage->id;
                $group_id = $userStorage->group_id;
            
                if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
                    ADMINISTRATOR_ID,
                    HR_ID,
                    HR_EXT_ID,
                    BOARD_ID)))
                {
                }
                elseif ($group_id == PGPB_ID){
                    $params['staff_id'] = $user_id;
                }elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID){
                    $params['asm'] = $user_id;
                }elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID,TRAINING_LEADER_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID))){
                    $params['other'] = $user_id;
                }elseif($group_id == LEADER_ID){
                    $params['leader'] = $user_id;
                }
            
                //set quyen cho hong nhung trainer
                elseif ($user_id == 7278 || $user_id == 240){
                    $params['other'] = $user_id;
                }
                //chi van ha noi
                elseif ($user_id == 95){
                    $params['other'] = $user_id;
                }
                elseif ($group_id == 22)
                $params['other'] = $user_id;
                elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915)
                $params['asm'] = $user_id;
                else{
                    $params['sale'] = $user_id;
                }
            
                $public_holyday = $QTime->getPublicHolyday($params);
                if($this->getRequest()->getParam('approve-all') )
                {
                    $QTime->approveAll($params);
                }
            
            
                $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
            
                $QTeam = new Application_Model_Team();
                $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
                $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
                $array_ddtm = array(
                    24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
                );
            
                $params_day = array(
                    'from' => $params['year'] . '-' . $params['month'] . '-01',
                    'to' => $params['year'] . '-' . $params['month'] . $number_day_of_month,
                );
                $QCheckIn = new Application_Model_CheckIn();
                $off_date = $QCheckIn->getDateInfo($params_day);
                	
                if(!empty($export)){
                    $list_export_staff_id = $QTime->getListStaffById($params);
                    $db           = Zend_Registry::get('db');
                    $sql          = "CALL `PR_List_Staff_Time_New`('$from_date', '$to_date', '$list_export_staff_id')";
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                    $data_export = $stmt->fetchAll();
                    $stmt->closeCursor();
                    $stmt = $db = null;
                     
            
                    set_time_limit(0);
                    ini_set('memory_limit', '5120M');
            
                    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
            
                    require_once 'PHPExcel.php';
            
                    $alphaExcel = new My_AlphaExcel();
            
                    $PHPExcel = new PHPExcel();
                    $heads = array(
                        $alphaExcel->ShowAndUp() => 'Company',
                        $alphaExcel->ShowAndUp() => 'Code',
                        $alphaExcel->ShowAndUp() => 'CMND',
                        $alphaExcel->ShowAndUp() => 'Name',
                        $alphaExcel->ShowAndUp() => 'Department',
                        $alphaExcel->ShowAndUp() => 'Team',
                        $alphaExcel->ShowAndUp() => 'Title',
                        $alphaExcel->ShowAndUp() => 'Area',
                        $alphaExcel->ShowAndUp() => 'Join date',
                        $alphaExcel->ShowAndUp() => 'Off date',
                    );
            
                    for($i = 1; $i <= $number_day_of_month; $i++)
                    {
                        $heads[$alphaExcel->ShowAndUp()] = $i;
                    }
            
                    $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
                    $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
                    $heads[$alphaExcel->ShowAndUp()] = 'Công training';
                    $heads[$alphaExcel->ShowAndUp()] = 'Phép';
                    $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
                    $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
            
                    $PHPExcel->setActiveSheetIndex(0);
                    $sheet = $PHPExcel->getActiveSheet();
            
                    foreach($heads as $key => $value)
                    {
                        $sheet->setCellValue($key.'1', $value);
                    }
            
                    $index = 1;
                    foreach($data_export as $key => $value)
                    {
                        $alphaExcel = new My_AlphaExcel();
                        	
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['com_name']);
                        //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code']);
                        //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '`'.$staff_code,PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);
                        // $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'');
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
            
                        for($i = 1; $i <= $number_day_of_month; $i++)
                        {
                            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value[$i]);
                        }
            
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congthuong']);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['conggay']);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congtraining']);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phep']+$value['phepcty']);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congchunhat']);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congle']);
                         
                        $index++;
            
                    }
                    $filename = 'Staff Check In - ' . date('Y-m-d H-i-s');
                    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            
            
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                    $objWriter->save('php://output');
            
                    exit();
                     
                }
                	
                if(!empty($export))
                {
                    $list_export_staff_id = $QTime->get_time_list_export_file(null, 0, $params);
                    $this->exportFile($month, $year, $list_export_staff_id);
            
                    exit;
                    $alphaExcel = new alphaExcel();
                    require_once 'PHPExcel.php';
                    $PHPExcel = new PHPExcel();
                    $heads = array(
                        $alphaExcel->ShowAndUp() => 'Company',
                        $alphaExcel->ShowAndUp() => 'Code',
                        $alphaExcel->ShowAndUp() => 'CMND',
                        $alphaExcel->ShowAndUp() => 'Name',
                        $alphaExcel->ShowAndUp() => 'Department',
                        $alphaExcel->ShowAndUp() => 'Team',
                        $alphaExcel->ShowAndUp() => 'Title',
                        $alphaExcel->ShowAndUp() => 'Join date',
                        $alphaExcel->ShowAndUp() => 'Off date',
                        $alphaExcel->ShowAndUp() => 'Area',
                    );
            
            
                    $array_a_to_z = range("A", "Z");
                    $array_head = array();
            
                    for($i = 1; $i <= $number_day_of_month; $i++)
                    {
                        // if($i > 17)
                        // {
                        //     $array_head[$i] .= "A" . $array_a_to_z[$i-18];
                        // }
                        // else
                        // {
                        //     $array_head[$i] = $array_a_to_z[$i+8];
                        // }
                        $heads[$alphaExcel->ShowAndUp()] = $i;
                    }
                    foreach($array_head as $key => $value)
                    {
                        // $heads[$value] = $key;
                    }
                    $number_after = 18;
                    $heads[$alphaExcel->ShowAndUp()] = "Công thường";
                    $heads[$alphaExcel->ShowAndUp()] = "Công gãy";
                    $heads[$alphaExcel->ShowAndUp()] = "Công training";
                    $heads[$alphaExcel->ShowAndUp()] = "Công chủ nhật";
                    $heads[$alphaExcel->ShowAndUp()] = "Công lễ (x2)";
                    $heads[$alphaExcel->ShowAndUp()] = "Công lễ (x3)";
                    $heads[$alphaExcel->ShowAndUp()] = "Công lễ (x4)";
                    $heads[$alphaExcel->ShowAndUp()] = "Công thứ bảy nhân 2";
                    $heads[$alphaExcel->ShowAndUp()] = "Tổng giờ tăng ca";
            
                    foreach($list_allowance as $key_allowance => $value_allowance)
                    {
                        if($value_allowance['id'] == 1)
                        {
                            $heads[$alphaExcel->ShowAndUp()] = $value_allowance['name'];
                        }
                    }
                    $PHPExcel->setActiveSheetIndex(0);
                    $sheet = $PHPExcel->getActiveSheet();
            
                    foreach($heads as $key => $value)
                        $sheet->setCellValue($key.'1', $value);
            
                        $sheet->getStyle('A1:' . "A" . $array_a_to_z[$number_day_of_month-19+5] . '1')->applyFromArray(array('font' => array('bold' => true)));
                        $sheet->getColumnDimension('A')->setWidth(15);
                        $sheet->getColumnDimension('B')->setWidth(15);
                        $sheet->getColumnDimension('C')->setWidth(30);
                        $sheet->getColumnDimension('D')->setWidth(20);
                        $sheet->getColumnDimension('E')->setWidth(20);
                        $sheet->getColumnDimension('F')->setWidth(25);
                        $sheet->getColumnDimension('G')->setWidth(15);
                        $sheet->getColumnDimension('H')->setWidth(15);
                        $sheet->getColumnDimension('I')->setWidth(20);
            
                        foreach($array_head as $key => $value)
                        {
                            $sheet->getColumnDimension($value)->setWidth(5);
                        }
            
                        $sheet->getColumnDimension("A" . $array_a_to_z[$number_day_of_month-19+1])->setWidth(17);
                        $sheet->getColumnDimension("A" . $array_a_to_z[$number_day_of_month-19+2])->setWidth(17);
                        $sheet->getColumnDimension("A" . $array_a_to_z[$number_day_of_month-19+3])->setWidth(17);
                        $sheet->getColumnDimension("A" . $array_a_to_z[$number_day_of_month-19+4])->setWidth(17);
            
            
                        // if(!empty($_GET['dev']))
                        // {
                        //     $data_export = $QTime->get_time_list_export_test(null, 0, $params);
                        // }
                        $data_export = $QTime->get_time_list_export_file(null, 0, $params);
            
                        $array_staff_id = array();
                        foreach ($data_export['data'] as $key => $value) {
                            $array_staff_id[] = $value['id'];
                            $array_cmnd[] = $value['cmnd'];
                        }
            
                        if(!empty($array_cmnd) && !empty($array_staff_id))
                        {
                            $params['list_cmnd'] = "('" . implode("','", $array_cmnd) . "')";
                            $params['list_staff_id'] = "(" . implode(",", $array_staff_id) . ")";
                        }
            
                        if(!empty($params['list_cmnd']) && !empty($params['list_staff_id']))
                        {
                            $data_training = $QTime->getListTrainingByStaffId($params);
                            $data_allowance = $QTime->getListAllowanceByStaffId($params);
                        }
                        set_time_limit(300);
            
                        foreach ($data_export['data'] as $key => $staff)
                        {
                            $alphaExcel = new alphaExcel();
                            try
                            {
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff['company_name']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff['code']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff['cmnd']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff['firstname'].' '.$staff['lastname']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['name']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['name']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['children'][$staff['title']]['name']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($staff['joined_at'])));
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($staff['off_date'])?date('d/m/Y', strtotime($staff['off_date'])):'');
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff['area']);
                                $array_total_day = array(
                                    'normal' => 0,
                                    'gay' => 0,
                                    'sunday' => 0,
                                    'special_2' => 0,
                                    'special_3' => 0,
                                    'special_4' => 0,
                                    'saturday' => 0,
                                );
                                $number_saturday = 0;
            
            
                                for($key_day = 1; $key_day <= $number_day_of_month; $key_day++)
                                {
                                    $value = $alphaExcel->ShowAndUp();
                                    $array_day = json_decode($staff['list'], true);
            
                                    $current_day_value = strtotime($year . '-' . $month . '-' . $key_day);
                                    $off_date_value = !empty($staff['off_date'])? strtotime($staff['off_date']):null;
                                    if(strtotime($staff['joined_at']) > $current_day_value || ($off_date_value != null && $current_day_value >= $off_date_value) )
                                    {
                                        // nothing
                                    }
                                    elseif(!empty($array_day[$key_day]['hr_approved']))
                                    {
                                        if(in_array($key_day, $public_holyday) && $array_day[$key_day]['type'] == "-")
                                        {
                                            $array_day[$key_day]['type'] = 'X';
                                        }
                                        $sheet->setCellValue($value . ($key+2), $array_day[$key_day]['type']);
                                    }
                                    elseif(!empty($data_training[$staff['id']]) && in_array($i, $data_training[$staff['id']]))
                                    {
                                        $array_day[$key_day]['type'] = 'T';
                                        $sheet->setCellValue($value . ($key+2), $array_day[$key_day]['type']);
                                    }
                                    elseif(!empty($data_leave[$staff['id']][$key_day]))
                                    {
                                        $array_day[$key_day]['type'] = 'P';
                                        if($data_leave[$staff['id']][$key_day]['sub_stock'] == 1)
                                        {
                                            if($data_leave[$staff['id']][$key_day]['is_half_day'] == 1)
                                            {
                                                $array_total_day['leave']+= 0.5;
                                            }
                                            else
                                            {
                                                $array_total_day['leave']++;
                                            }
                                        }
                                        $sheet->setCellValue($value . ($key+2), $array_day[$key_day]['type']);
                                    }
                                    elseif( (!empty($array_day[$key_day]) && $array_day[$key_day]['status'] == 1) || in_array($key_day, $public_holyday))
                                    {
                                        if(!in_array($array_day[$key_day]['type'], array('L', 'K')) && in_array($key_day, $public_holyday))
                                        {
                                            $array_day[$key_day]['type'] = 'X';
                                        }
            
                                        if($array_day[$key_day]['type'] == 'S')
                                        {
                                            $array_day[$key_day]['type'] = 'X';
                                        }
                                        elseif($array_day[$key_day]['type'] == 'Y')
                                        {
                                            $array_day[$key_day]['type'] = 'H';
                                        }
            
                                        if(date('D', strtotime(date($params['year'] . '-' . $params['month'] . '-' . $i))) == 'Sat')
                                        {
                                            $number_saturday++;
                                        }
            
                                        if($array_day[$key_day]['status']==1 || in_array($key_day, $public_holyday))
                                        {
                                            $array_day[$key_day]['status'] = 1;
                                        }
                                        $sheet->setCellValue($value . ($key+2), $array_day[$key_day]['type']);
                                    }
                                    else
                                    {
            
                                    }
            
                                    if(strtotime($staff['joined_at']) > $current_day_value || ($off_date_value != null && $current_day_value >= $off_date_value) )
                                    {
                                        // nothing
                                    }
                                    elseif($array_day[$key_day]['status'] == 1 && !(!empty($data_training[$staff['id']]) && in_array($key_day, $data_training[$staff['id']])) )
                                    {
                                        switch ($array_day[$key_day]['type'])
                                        {
                                            case 'G':
                                                $array_total_day['gay']++;
                                                break;
                                            case 'H':
                                                $array_total_day['normal']+=0.5;
                                                break;
                                            case 'X':
            
                                                $array_total_day['normal']++;
                                                break;
                                            case 'L':
                                                if($array_day[$i]['multiple'] == 2)
                                                {
                                                    $array_total_day['special_2']++;
                                                }
                                                elseif($array_day[$i]['multiple'] == 3)
                                                {
                                                    $array_total_day['special_3']++;
                                                }
                                                elseif($array_day[$i]['multiple'] == 4)
                                                {
                                                    $array_total_day['special_4']++;
                                                }
                                                break;
                                            case 'K':
                                                if($array_day[$i]['multiple'] == 2)
                                                {
                                                    $array_total_day['special_2']+= 0.5;
                                                }
                                                elseif($array_day[$i]['multiple'] == 3)
                                                {
                                                    $array_total_day['special_3']+= 0.5;
                                                }
                                                elseif($array_day[$i]['multiple'] == 4)
                                                {
                                                    $array_total_day['special_4']+= 0.5;
                                                }
                                                break;
                                            case 'C':
                                                $array_total_day['sunday']++;
                                                break;
                                            case 'D':
                                                $array_total_day['sunday']+=0.5;
                                                break;
                                            case 'S':
                                                $array_total_day['saturday']++;
                                                break;
                                            case 'Y':
                                                $array_total_day['saturday']+=0.5;
                                                break;
                                        }
                                        if($array_day[$key_day]['type'] == 'S')
                                        {
                                            $sheet->setCellValue($value . ($key+2), 'X');
                                        }
                                        elseif($array_day[$key_day]['type'] == 'Y')
                                        {
                                            $sheet->setCellValue($value . ($key+2), 'H');
                                        }
                                        else
                                        {
                                            $sheet->setCellValue($value . ($key+2), $array_day[$key_day]['type']);
                                        }
                                        if($array_day[$key_day]['type'] != 'P' && date('D', strtotime(date($params['year'] . '-' . $params['month'] . '-' . $key_day))) == 'Sat')
                                        {
                                            $number_saturday++;
                                        }
                                    }
                                    elseif(!empty($data_training[$staff['id']]) && in_array($key_day, $data_training[$staff['id']]))
                                    {
                                        $sheet->setCellValue($value . ($key+2), 'T');
                                    }
            
                                }
                                if(empty($staff['number']))
                                {
                                    // $number_saturday;
                                    // NUMBER SATURDAY
                                }
                                elseif($number_saturday > $staff['number'])
                                {
                                    $number_saturday = $number_saturday - $staff['number'];
                                }
                                else
                                {
                                    $number_saturday = 0;
                                }
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), $array_total_day['normal'] + $array_total_day['gay']);
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), $array_total_day['gay']);
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), empty($data_training[$staff['id']])?'0':count($data_training[$staff['id']]) );
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), $array_total_day['sunday']);
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), $array_total_day['special_2']);
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), $array_total_day['special_3']);
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), $array_total_day['special_4']);
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), $number_saturday);
                                $sheet->setCellValue($value = $alphaExcel->ShowAndUp() . ($key+2), empty($staff['overtime'])?0:date('H', strtotime($staff['overtime'])));
            
                                // foreach($list_allowance as $key_allowance => $value_allowance)
                                // {
                                //     if($value_allowance['id'] == 1)
                                //     {
                                //         $sheet->setCellValue("A" . $array_a_to_z[$number_day_of_month-19+7+$key_allowance+1] . ($key+2), !empty($data_allowance[$staff['id']][$value_allowance['id']])?$data_allowance[$staff['id']][$value_allowance['id']]:0);
                                //     }
                                // }
                            }
                            catch(Exception $e)
                            {
                            }
                        }
            
                        $data_new_staff = $QTime->get_new_staff_time_new($params);
            
                        if(!empty($data_new_staff))
                        {
                            $sum_key = count($data_export['data']);
                            foreach ($data_new_staff as $key => $staff)
                            {
                                $alphaExcel = new alphaExcel();
                                $key = $sum_key + $key;
                                $company_name = (in_array($staff['area_id'], $array_ddtm))?'Di Động Thông Minh':'OPPO';
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $company_name);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff['cmnd']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff['firstname'].' '.$staff['lastname']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['name']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['name']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['children'][$staff['title']]['name']);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), '');
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($staff['off_date'])?date('d/m/Y', strtotime($staff['off_date'])):'');
            
                                $array_day_training = json_decode($staff['list_day'], true);
                                for($key_day = 1; $key_day <= $number_day_of_month; $key_day++)
                                {
                                    $value = $alphaExcel->ShowAndUp();
                                    if(in_array($key_day, $array_day_training))
                                    {
                                        $sheet->setCellValue($value . ($key+2), 'T');
                                    }
            
                                }
            
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), 0);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), 0);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), count($array_day_training) );
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), 0);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), 0);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), 0);
                                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), empty($staff['overtime'])?0:date('H', strtotime($staff['overtime'])));
            
                                foreach($list_allowance as $key_allowance => $value_allowance)
                                {
                                    if($value_allowance['id'] == 1)
                                    {
                                        $value = $alphaExcel->ShowAndUp();
                                        $sheet->setCellValue("A" . $value . ($key+2), !empty($data_allowance[$staff['id']][$value_allowance['id']])?$data_allowance[$staff['id']][$value_allowance['id']]:0);
                                    }
                                }
            
                            }
            
                        }
            
                        $filename = 'Staff Check In - ' . date('Y-m-d H-i-s');
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                        header('Cache-Control: max-age=0');
                        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                        ob_end_clean();
                        $objWriter->save('php://output');
                        exit;
                }
            
                $data = $QTime->get_time_list($limit, $page, $params);
                $data_from_export = $this->exportFile($params['month'], $params['year'], $data['list_staff_id'], true);
                $array_staff_id = array();
                $array_cmnd = array();
            
                $list_staff_id = $QTime->get_time_list_export_file($limit, $page, $params);
                $data_check_in = $this->viewCheckIn($month, $year, $list_staff_id);
            
                $max_page = ceil($data['total'] / $limit);
                $page_trainer = $page - $max_page;
                $sum_offset = ($limit * $max_page) - $data['total'];
                $limit_trainer = (($limit - count($data['data'])) > 0 && count($data['data'] > 0))?($limit - count($data['data'])):$limit;
                foreach ($data['data'] as $key => $value) {
                    $array_staff_id[] = $value['id'];
                    $array_cmnd[] = $value['cmnd'];
                }
                // $data_new_staff_trainer = $QTime->get_staff_new_time($limit_trainer, $page_trainer, $sum_offset, $params);
            
                if($page < $max_page)
                {
                    $data_new_staff_trainer['data'] = null;
                }
            
                if(!empty(implode(",", $array_staff_id)))
                {
                    $params['list_staff_id'] = "(" . implode(",", $array_staff_id) . ")";
                    $params['list_cmnd'] = "('" . implode("','", $array_cmnd) . "')";
                    $data_training = $QTime->getListTrainingByStaffId($params);
                    $data_allowance = $QTime->getListAllowanceByStaffId($params);
                    $data_leave = $QTime->getListLeaveByStaffId($params);
            
                    $list_training_check_in = $QTime->getListTrainingCheckIn($params);
            
                    $this->view->data_training = $data_training;
                    $this->view->data_allowance = $data_allowance;
                    $this->view->data_leave = $data_leave;
                }
                $this->view->data = $data_from_export;
                $this->view->staffs = $data['data'];
                $this->view->staffs_trainer = $data_new_staff_trainer['data'];
            
                $this->view->limit = $limit;
                $this->view->total = $data['total'] + $data_new_staff_trainer['total'];
                $this->view->number_day_of_month = $number_day_of_month;
                $this->view->url = HOST . 'staff-time/list-staff-check-in' . ($params ? '?' . http_build_query($params) .
                    '&' : '?');
            
                $this->view->offset = $limit * ($page - 1);
            }
            // else
            {
                // $this->_redirect('/user/noauth');
            }
            unset($params['list_staff_id']);
            unset($params['list_cmnd']);
            $this->view->public_holyday = $public_holyday;
            $this->view->shift = $QShift->getShiftByTitle($userStorage->title);
            $this->view->current_user  = $userStorage;
            $QArea = new Application_Model_Area();
            $all_area =  $QArea->fetchAll(null, 'name');
            $this->view->areas = $all_area;
            $this->view->params = $params;
            $this->view->list_allowance = $list_allowance;
            $this->view->array_ddtm = $array_ddtm;
            $this->view->off_date = $off_date;
        }
        
    }

    public function listStaffApproveAction()
    {
		  if(empty($_GET['dev'])){
            require_once 'staff-time' . DIRECTORY_SEPARATOR . 'list-staff-approve.php';
        }else{
			$not_approve_self = array(LEADER_ID,SALES_ID, PB_SALES_ID, PGPB_ID);
        $page  = $this->getRequest()->getParam('page', 1);
        $name  = $this->getRequest()->getParam('name');
        $code  = $this->getRequest()->getParam('code');
        $email  = $this->getRequest()->getParam('email');
        $export = $this->getRequest()->getParam('export');
        $only_training = $this->getRequest()->getParam('only_training', '0');
        $month = $this->getRequest()->getParam('month', date('m'));
        $year = $this->getRequest()->getParam('year', date('Y'));
        $area = $this->getRequest()->getParam('area');
        $department = $this->getRequest()->getParam('department');
        $team = $this->getRequest()->getParam('team');
        $title = $this->getRequest()->getParam('title');
        $note = $this->getRequest()->getParam('note');
        $limit = 20;
        $QShift  = new Application_Model_Shift();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QTime = new Application_Model_Time2();
        $note = $this->getRequest()->getParam('note', null);
        $QLeaveDetail = new Application_Model_LeaveDetail();

        $export = $this->getRequest()->getParam('export');
        $approved = $this->getRequest()->getParam('approved', 0);
        $notapproved = $this->getRequest()->getParam('notapproved', 0);
        $hr_approved = $this->getRequest()->getParam('hr_approved', 0);
        $delete = $this->getRequest()->getParam('delete');
        $this->view->auth_id = $userStorage->id;
        if(in_array($userStorage->group_id, $not_approve_self))
        {
            $this->view->not_approve_self = 1;
        }
        else
        {
            $this->view->not_approve_self = 0;
        }

        $this->view->auth_id = $userStorage->id;

        $approve_all_by_staff = $this->getRequest()->getParam('approve_all_by_staff');
        $deapprove_all_by_staff = $this->getRequest()->getParam('deapprove_all_by_staff');
        $date_approve = $this->getRequest()->getParam('date-approve'); 
        $date_none_approve = $this->getRequest()->getParam('date-none-approve'); 
        if(!empty($date_approve))
        {
            $staff_approve = $this->getRequest()->getParam('staff_approve');
            $params_approve_date = array(
                'staff_id' => $staff_approve,
                'date' => $date_approve,
                'admin' => $userStorage->id,
                'note' => ''
            );
            $QTime->approveAllTempTimeByStaffDate($params_approve_date);
        }

        if(!empty($date_none_approve))
        {
            
            $staff_approve = $this->getRequest()->getParam('staff_approve');
            $params_approve_date = array(
                'staff_id' => $staff_approve,
                'date' => $date_none_approve,
                'admin' => $userStorage->id,
                'note' => ''
            );
            $QTime->nonapproveAllTempTimeByStaffDate($params_approve_date);
        }

        if(!empty($deapprove_all_by_staff))
        {
            $month_edit = $this->getRequest()->getParam('month_edit');
            $year_edit = $this->getRequest()->getParam('year_edit');

            $params_approve_all = array(
                'staff_id' => $deapprove_all_by_staff,
                'month' => $month_edit,
                'year' => $year_edit,
                'admin' => $userStorage->id,
                'note' => ''
            );
            $QTime->deapproveAllTempTimeByStaff($params_approve_all);
        }

        if(!empty($approved))
        {
            $params_approved = array(
                'status' => 1,
                'id' => $approved,
                'note' => $note,
            );
            $QLeaveDetail->updateStatus($params_approved);
        }

        if(!empty($notapproved))
        {
            $params_notapproved = array(
                'status' => 2,
                'id' => $notapproved,
                'note' => $note,
            );
            $QLeaveDetail->updateStatus($params_notapproved);
        }

        if(!empty($hr_approved))
        {
            $params_hr_approved = array(
                'status' => 1,
                'id' => $hr_approved,
            );
            $QLeaveDetail->updateHrStatus($params_hr_approved);
            // $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
        }

        if(!empty($approve_all_by_staff))
        {
            $month_edit = $this->getRequest()->getParam('month_edit');
            $year_edit = $this->getRequest()->getParam('year_edit');

            $params_approve_all = array(
                'staff_id' => $approve_all_by_staff,
                'month' => $month_edit,
                'year' => $year_edit,
                'admin' => $userStorage->id,
                'note' => ''
            );
            $QTime->approveAllTempTimeByStaff($params_approve_all);
            $QTime->approveAllLateTimeByStaff($params_approve_all);
        }

        $params_leave = array(
                'from' => $params['year'] . '-' . $params['month'] . '-01',
                'to' => $params['year'] . '-' . $params['month'] . $number_day_of_month,
            );

        // $this->view->leave = $QLeaveDetail->_selectAdmin(0, 0, $params_leave);
        
        $QStaffPermisson = new Application_Model_StaffPermission();
        // if($QStaffPermisson->checkPermission($userStorage->code) || in_array($userStorage->title, array(SALE_SALE_ASM, SALES_ADMIN_TITLE, SALES_TITLE, SALES_LEADER_TITLE)) || in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID)))
        {
            $QTime = new Application_Model_Time2();
            $list_allowance = $QTime->getAllowance();
            $params = array(
                    'off' => 1, 
                    'name' => $name,
                    'code' => $code,
                    'email' => $email,
                    'only_training' => $only_training,
                    'month' => $month,
                    'year' => $year,
                    'area' => $area,
                    'department' => $department,
                    'team' => $team,
                    'title' => $title,
            );

            $user_id = $userStorage->id;
            $group_id = $userStorage->group_id;

            if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
                ADMINISTRATOR_ID,
                HR_ID,
                HR_EXT_ID,
                BOARD_ID)))
            {
            }
            elseif ($group_id == PGPB_ID){
                $params['staff_id'] = $user_id;
            }elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID){
                $params['asm'] = $user_id;
            }elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID,TRAINING_LEADER_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID))){
                $params['other'] = $user_id;
            }elseif($group_id == LEADER_ID){
                $params['leader'] = $user_id;
            }

            //set quyen cho hong nhung trainer
            elseif ($user_id == 7278 || $user_id == 240){
                $params['other'] = $user_id;
            }
            //chi van ha noi
            elseif ($user_id == 95){
                $params['other'] = $user_id;
            }
            elseif ($group_id == 22)
                $params['other'] = $user_id;
            elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915)
                $params['asm'] = $user_id;
            else{
                $params['sale'] = $user_id;
            }
            
            $public_holyday = $QTime->getPublicHolyday($params);
            if($this->getRequest()->getParam('approve-all') )
            {
                $QTime->approveAll($params);
            }

            
            $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
            
            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
            $array_ddtm = array(
                    24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
            );

            $params_day = array(
                'from' => $params['year'] . '-' . $params['month'] . '-01',
                'to' => $params['year'] . '-' . $params['month'] . $number_day_of_month,
            );
            $QCheckIn = new Application_Model_CheckIn();
            $off_date = $QCheckIn->getDateInfo($params_day);

            $data = $QTime->get_time_list_new(null, $page, $params);
            $array_staff_id = array();
            $array_cmnd = array();

            $max_page = ceil($data['total'] / $limit);
            $page_trainer = $page - $max_page;
            $sum_offset = ($limit * $max_page) - $data['total'];
            $limit_trainer = (($limit - count($data['data'])) > 0 && count($data['data'] > 0))?($limit - count($data['data'])):$limit;
            foreach ($data['data'] as $key => $value) {
                $array_staff_id[] = $value['id'];
                $array_cmnd[] = $value['cmnd'];
            }
            // $data_new_staff_trainer = $QTime->get_staff_new_time($limit_trainer, $page_trainer, $sum_offset, $params);

            if($page < $max_page)
            {
                $data_new_staff_trainer['data'] = null;
            }
            
            if(!empty(implode(",", $array_staff_id)))
            {
                $params['list_staff_id'] = "(" . implode(",", $array_staff_id) . ")";
                $params['list_cmnd'] = "('" . implode("','", $array_cmnd) . "')";
                $data_training = $QTime->getListTrainingByStaffId($params);
                $data_allowance = $QTime->getListAllowanceByStaffId($params);
                $data_leave = $QTime->getListLeaveByStaffId($params);

                $list_training_check_in = $QTime->getListTrainingCheckIn($params);

                $this->view->data_training = $data_training;
                $this->view->data_allowance = $data_allowance;
                $this->view->data_leave = $data_leave;
            }

			if(!empty($export)){
                require_once 'PHPExcel.php';
                
                $alphaExcel = new My_AlphaExcel();
                
                $PHPExcel = new PHPExcel();
                $heads = array(
                    $alphaExcel->ShowAndUp() => 'Mã nhân viên',
                    $alphaExcel->ShowAndUp() => 'Họ tên',
                    $alphaExcel->ShowAndUp() => 'Chức vụ',
                    $alphaExcel->ShowAndUp() => 'Những ngày cần xác nhận công',
                    $alphaExcel->ShowAndUp() => 'Những ngày cần xác nhận đi trễ về sớm do công tác ngoài',
                );
                
                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();
                
                foreach($heads as $key => $value)
                {
                    $sheet->setCellValue($key.'1', $value);
                }
                
                $index = 1;
                foreach($data['data'] as $key => $value)
                {
                    $alphaExcel = new My_AlphaExcel();
                
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['temp_time'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['late_time'],PHPExcel_Cell_DataType::TYPE_STRING);
                
                    $index++;
                
                }
                $filename = 'Export Pending Approve - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                
                
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                
                exit;
            }


            $this->view->staffs = $data['data'];
            $this->view->staffs_trainer = $data_new_staff_trainer['data'];

            $this->view->limit = $limit;
            $this->view->total = $data['total'] + $data_new_staff_trainer['total'];
            $this->view->number_day_of_month = $number_day_of_month;
            $this->view->url = HOST . 'staff-time/list-staff-check-in' . ($params ? '?' . http_build_query($params) .
                '&' : '?');

            $this->view->offset = $limit * ($page - 1);
        }
        // else
        {
            // $this->_redirect('/user/noauth');   
        }
        unset($params['list_staff_id']);
        unset($params['list_cmnd']);
        $this->view->public_holyday = $public_holyday;
        $this->view->shift = $QShift->getShiftByTitle($userStorage->title);
        $this->view->current_user  = $userStorage;
        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;
        $this->view->params = $params;
        $this->view->list_allowance = $list_allowance;
        $this->view->array_ddtm = $array_ddtm;
        $this->view->off_date = $off_date;


        // $params_leave = array(
            
        // );

        $params_leave = $params;


        $params_leave['from'] = $year . '-' . $month . '-01';
        $params_leave['to'] = $year . '-' . $month . '-' . $number_day_of_month;
        $params_leave['status'] = 0;

        $QLeaveDetail = new Application_Model_LeaveDetail();
        $this->view->leave = $QLeaveDetail->_selectAdmin(null, $page, $params_leave);
		}
	   

    }

    public function getViewDetailAction()
    {
        $date = $this->getRequest()->getParam('date');
        $staff_id = $this->getRequest()->getParam('staff_id');
        $temp_id = $this->getRequest()->getParam('temp_id');
        $leave_id = $this->getRequest()->getParam('leave_id');
        $late_id = $this->getRequest()->getParam('late_id');
        
        if(!empty($late_id)){
            echo '<h3>Xác nhận vào trễ/ về sớm ngày '.$date.'</h3>';
        }
        
        $db  = Zend_Registry::get('db');
        $html = "";
        if(!empty($temp_id))
        {
            $sql = "
                SELECT
                    tt.*,
                    CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                    s.name as `shift_name`
                FROM `temp_time` tt
                JOIN `staff` st ON tt.staff_id = st.id
                LEFT JOIN shift s ON tt.shift = s.id
                WHERE tt.id = :temp_id
            ";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('temp_id', $temp_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch();
            $html .= "<h3>Nhân viên yêu cầu cập nhật</h3>";
            $html .= "<table class='table'>";
            $html .= "<tr><td>Nhân viên</td><td>" . $data['staff_name'] . "</td></tr>";
            $html .= "<tr><td>Ngày</td><td>" . date('d/m/Y', strtotime($data['date'])) . "</td></tr>";
            if(!empty($data['shift']))
            {
                $html .= "<tr><td>Ca</td><td>" . $data['shift_name'] . "</td></tr>";
            }
            else
            {
                $html .= "<tr><td>Số ngày</td><td>" . $data['office_time'] . "</td></tr>";
            }
            $html .= "<tr><td>Lý do</td><td>" . $data['reason'] . "</td></tr>";
            $html .= "<tr><td>Yêu cầu lúc</td><td>" . date('H:i:s d/m/Y', strtotime($data['created_at'])) ."</td></tr>";
            $html .= "<tr><td>Loại Yêu cầu</td><td>Xác nhận công</td></tr>";
            $html .= "</table>";
            $html .= "<div>";
            // $html .= "<form method='post' action=''>";
            // $html .= "<div>
            //     Note:
            // </div>";
            // $html .= "<textarea rows='' name='reason_reject' cols=''></textarea>";
            // $html .= "<input type='hidden' name='id_temp_reject' value='" . $temp_id . "'>";
            // $html .= "<button type='submit' class='btn btn-warning' value='1' name='btn_reject_temp'>Reject</button>";
            // $html .= "</form>";
            $html .= "</div>";
            echo $html; 
            exit;
        }
        elseif(!empty($leave_id) && is_numeric($leave_id))
        {
            
            $sql = "
                SELECT
                    ld.*,
                    CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                    lt.note as `leave_type_name`
                FROM `leave_detail` ld
                JOIN `staff` st ON ld.staff_id = st.id
                LEFT JOIN `leave_type` lt ON lt.id = ld.leave_type
                WHERE ld.id = :leave_id
            ";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('leave_id', $leave_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch();
            $html .= "<h3>Phép nhân viên</h3>";
            $html .= "<table class='table'>";
            $html .= "<tr><td>Nhân viên</td><td>" . $data['staff_name'] . "</td></tr>";
            if(empty($data['is_half_day']))
            {
                $html .= "<tr><td>Từ ngày</td><td>" . date('d/m/Y', strtotime($data['from_date'])) . "</td></tr>";
                $html .= "<tr><td>Đến ngày </td><td>" . date('d/m/Y', strtotime($data['to_date'])) . "</td></tr>";
            }
            else
            {
                $html .= "<tr><td>Ngày</td><td>" . date('d/m/Y', strtotime($data['from_date'])) . "</td></tr>";
            }
            
            $html .= "<tr><td>Lý do</td><td>" . $data['reason'] . "</td></tr>";
            $html .= "<tr><td>Loại phép</td><td>" . $data['leave_type_name'] . "</td></tr>";
            if($data['status'] == 1)
            {
                $html .= "<tr><td></td><td>Approved</td></tr>";
            }
            $html .= "<tr><td>Loại Yêu cầu</td><td>Đăng ký phép</td></tr>";
            
            $html .= "</table>";
            echo $html; 
            exit;

        }
        elseif(!empty($date) && !empty($staff_id))
        {
            $sql = "SELECT 
                        DATE(t.created_at) as `check_in_day`,
                        CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                        s.name as `shift_name`,
                        t.status as `status`,
                        t.shift as `shift`,
                        cid.check_in_at as `check_in_at`,
                        cid.check_out_at  as `check_out_at`,
                        t.approve_time as `approve_time`,
                        t.hr_time as `hr_time`,
                        t.hr_approved as `hr_approved`
                    FROM `time` t
                    LEFT JOIN shift s ON t.shift = s.id
                    JOIN `staff` st ON st.id = t.staff_id
                    LEFT JOIN check_in_detail cid ON cid.staff_code = st.code AND date(t.created_at) = cid.check_in_day
                    WHERE t.staff_id = :staff_id AND DATE(t.created_at) = '" . $date . "'";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch();
            $html = "";
            if(!empty($data))
            {
                $html .= "<h3>Thông tin công</h3>";
                $html .= "<table class='table'>";
                $html .= "<tr><td>Nhân viên</td><td>" . $data['staff_name'] . "</td></tr>";
                if(!empty($data['check_in_at']))
                {
                    $html .= "<tr><td>Check in</td><td>" . date('d/m/Y H:i:s', strtotime($data['check_in_at'])) . "</td></tr>";
                }
                if(!empty($data['check_out_at']))
                {
                    $html .= "<tr><td>Check out</td><td>" . date('d/m/Y H:i:s', strtotime($data['check_out_at'])) . "</td></tr>";
                }
                if(!empty($data['approve_time']))
                {
                    $html .= "<tr><td>Đi làm</td><td>" . (!empty($data['hr_approved'])?$data['hr_time']:$data['approve_time']) . " ngày</td></tr>";
                }
                elseif(!empty($data['shift_name']))
                {
                    $html .= "<tr><td>Ca</td><td>" . $data['shift_name'] . "</td></tr>";
                }
                elseif($data['shift'] == 0)
                {
                    $html .= "<tr><td>1 </td><td>ngày công</td></tr>";
                }
            }
            echo $html; die;
        }
        die;
    }

    public function getViewDetailApproveAction()
    {
        $month = $this->getRequest()->getParam('month');
        $staff_id = $this->getRequest()->getParam('staff_id');
        $year = $this->getRequest()->getParam('year');

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);

        $from_date = $year . '-' . $month . '-01 00:00:00';
        $to_date = $year . '-' . $month . '-' . $number_day_of_month . ' 23:59:59';

        $db  = Zend_Registry::get('db');
       // $sql = "CALL PR_get_staff_temp_time(:staff_id, :from_date, :to_date)";
		$sql = "CALL PR_List_Staff_Time_Approve_By_Id(:staff_id, :p_month, :p_year)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        //$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
        //$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
		$stmt->bindParam('p_month', $month, PDO::PARAM_INT);
        $stmt->bindParam('p_year', $year, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
        $QTeam  = new Application_Model_Team();
        $staff_title_info = $userStorage->title;

        $html = "<input type='hidden' name='staff_approve' value='" . $data[0]['staff_id'] . "'>";
        $html .= "<table class='table'><thead>";
        $html .= "<th>Ngày</th><th>Giờ vào</th><th>Giờ ra</th><th>Ngày công</th><th>Nội dung</th><th>Lý do</th><th>Xác nhận</th></thead>";
        $html .= '<tbody>';
        foreach($data as $key => $value)
        {
//            check phai ngi vi corona hay khong
            if($value['add_reason']==7) $class="online_time_detail";
            else $class="";
            $html_tr = '<tr class="'.$class.'">';
            $html_tr .= "<td>" . date('d/m/Y', strtotime($value['date'])) . '</td>';
            $html_tr .= "<td>" . (!empty($value['check_in_at'])?date('H:i', strtotime($value['check_in_at'])):'') . '</td>';
            $html_tr .= "<td>" . (!empty($value['check_out_at'])?date('H:i', strtotime($value['check_out_at'])):'') . '</td>';
            
            if(!empty($value['temp_time_id']))
            {
                $html_tr .= "<td>" . $value['temp_office_time'] . "  ngày" . "</td>";
            }
            else
            {
                $shift_name = "";
                switch ($value['shift']) {
                    case '1':
                        $shift_name = "Công sáng";
                        break;
                    case '2':
                        $shift_name = "Công gãy";
                        break;
                    case '3':
                        $shift_name = "Công chiều";
                        break;
                    case '6':
                        $shift_name = "1 ngày công";
                        break;
                    default:
                        # code...
                        break;
                }
                $html_tr .= "<td>" . $shift_name . "</td>";
            }
            if(!empty($value['time_late_id']))
            {
                $type = "Xác nhận không đi trễ / về sớm";
            }
            else
            {
                $type = "Xác nhận có đi làm";
            }
            $html_tr .= "<td>$type</td>";
            $html_tr .= "<td>" . (!empty($value['temp_reason'])?$value['temp_reason']:(!empty($value['time_late_reason'])?$value['time_late_reason']:$value['time_reason'])) . "</td>";
            if($staff_title_info != SALES_ADMIN_TITLE){
                $html_tr .= "<td>
                        <button name='date-approve' style='width: 100%' value='" . date('Y-m-d', strtotime($value['date'])) . "'>Đồng ý</button>
                        <button name='date-none-approve' style='width: 100%' value='" . date('Y-m-d', strtotime($value['date'])) . "'>Không đồng ý</button>
                        </td>";
            }else{
                $html_tr .= "<td>
                        </td>";
            } 
            
            $html_tr .= '</tr>';
            $html .= $html_tr;
        }

        $html .= '</tbody></table>';
        echo $html;
        die;
    }

    public function totalAction()
    {
        $department = $this->getRequest()->getParam('department');
        $team = $this->getRequest()->getParam('team');
        $title = $this->getRequest()->getParam('title');
        $area = $this->getRequest()->getParam('area');
        $month = $this->getRequest()->getParam('month', date('m'));
        $year = $this->getRequest()->getParam('year', date('Y'));
        $code = $this->getRequest()->getParam('code');
        $search = $this->getRequest()->getParam('search', 0);
        $page = $this->getRequest()->getParam('page', 1);
        $export = $this->getRequest()->getParam('export', 0);
        $name = $this->getRequest()->getParam('name');
        $department = $this->getRequest()->getParam('department');
        $team = $this->getRequest()->getParam('team');
        $title = $this->getRequest()->getParam('title');
        $limit = 20;
        $QShift  = new Application_Model_Shift();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        

        // $QStaffPermisson = new Application_Model_StaffPermission();
        // if($QStaffPermisson->checkPermission($userStorage->code) || in_array($userStorage->title, array(SALE_SALE_ASM, SALES_ADMIN_TITLE, SALES_TITLE, SALES_LEADER_TITLE)) || $userStorage->group_id == HR_ID)
        {
            
            $QTime = new Application_Model_Time2();
            $list_allowance = $QTime->getAllowance();
            $params = array(
                    'off' => 1, 
                    'name' => $name,
                    'code' => $code,
                    'email' => $email,
                    'only_training' => $only_training,
                    'month' => $month,
                    'year' => $year,
                    'area' => $area,
                    'department' => $department,
                    'team' => $team,
                    'title' => $title,
            );
            $public_holyday = $QTime->getPublicHolyday($params);
            if($this->getRequest()->getParam('approve-all') )
            {
                $QTime->approveAll($params);
            }

            
            $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
            $ngay_dinh_muc = $number_day_of_month;
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                if(date('D', strtotime(date($year . '-' . $month . '-' . $i))) == 'Sun')
                {
                    $ngay_dinh_muc--;
                }
            }
            
            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
            
            if(!empty($export))
            {
                require_once 'PHPExcel.php';
                $PHPExcel = new PHPExcel();
                $heads = array(
                    'Company',
                    'Code',
                    'Name',
                    'Department',
                    'Team',
                    'Title',
                    'Area',
                    'From date',
                    'Contract type',
                    'Join date',
                    'Off date',
                    'Công thường',
                    'Công gãy',
                    'Công training',
                    'Công chủ nhật',
                    'Công thứ bảy nhân hai',
                    'Công lễ (x2)',
                    'Công lễ (x3)',
                    'Công lễ (x4)',
                    'Tổng phép',
                    'Tổng số tăng ca',
                    'Cơm',
                );

                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();

                $alpha = 'A';
                foreach($heads as $key => $value)
                {
                    $sheet->setCellValue($alpha.'1', $value);
                    $alpha++;
                }
                $limit = null;
                $data = $QTime->get_time_list_total_export($limit, $page, $params);
                $array_staff_id = array();
                foreach ($data['data'] as $key => $value) {
                    $array_staff_id[] = $value['id'];
                    $array_cmnd[] = $value['cmnd'];
                }
                if(!empty(implode(",", $array_staff_id)))
                {
                    $params['list_cmnd'] = "('" . implode("','", $array_cmnd) . "')";
                    $params['list_staff_id'] = "(" . implode(",", $array_staff_id) . ")";
                    $data_training = $QTime->getListTrainingByStaffId($params);
                    $data_allowance = $QTime->getListAllowanceByStaffId($params);
                    $data_leave = $QTime->getListLeaveByStaffId($params);

                    $list_training_check_in = $QTime->getListTrainingCheckIn($params);
                }
                $printTitle = new printTitle();
                $printTitle->setTeam($recursiveDeparmentTeamTitle);
                set_time_limit(300);

                $sheet->getColumnDimension('C')->setWidth(35);
                $sheet->getColumnDimension('F')->setWidth(20);
                $sheet->getColumnDimension('G')->setWidth(20);
                $sheet->getColumnDimension('H')->setWidth(17);
                $sheet->getColumnDimension('I')->setWidth(17);

                foreach ($data['data'] as $key => $staff) 
                {
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . ($key+2), $staff['company_name']);
                    $sheet->setCellValue($alpha++ . ($key+2), $staff['code']);
                    $sheet->setCellValue($alpha++ . ($key+2), $staff['firstname'].' '.$staff['lastname']);

                    $arrayTeam = $printTitle->GetTeamName($staff['contract_title']);
                    if(empty($arrayTeam['title']))
                    {
                        $department = $recursiveDeparmentTeamTitle[$staff['department']]['name'];
                        $team = $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['name'];
                        $title = $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['children'][$staff['title']]['name'];
                    }
                    else
                    {
                        $title = $arrayTeam['title'];
                        $team = $arrayTeam['team'];
                        $department = $arrayTeam['department'];
                    }

                    $sheet->setCellValue($alpha++ . ($key+2), $department);
                    $sheet->setCellValue($alpha++ . ($key+2), $team);
                    $sheet->setCellValue($alpha++ . ($key+2), $title);
                    $sheet->setCellValue($alpha++ . ($key+2), $staff['area']);
                    $sheet->setCellValue($alpha++ . ($key+2), !empty($staff['contract_from_date'])? date('d/m/Y', strtotime($staff['contract_from_date'])): '' );
                    $sheet->setCellValue($alpha++ . ($key+2), $staff['contract_term']);
                    $sheet->setCellValue($alpha++ . ($key+2), date('d/m/Y', strtotime($staff['joined_at'])));
                    $sheet->setCellValue($alpha++ . ($key+2), !empty($staff['off_date'])?date('d/m/Y', strtotime($staff['off_date'])):'');

                    $array_day = json_decode($staff['list'], true);
                    $array_total_day = array(
                        'normal' => 0,
                        'gay' => 0,
                        'sunday' => 0,
                        'special' => 0,
                        'special_2' => 0,
                        'special_3' => 0,
                        'special_4' => 0,
                        'leave' => 0,
                    );

                    $array_total_day_contract = array(
                        'normal' => 0,
                        'gay' => 0,
                        'sunday' => 0,
                        'special_2' => 0,
                        'special_3' => 0,
                        'special_4' => 0,
                        'saturday' => 0,
                        'leave' => 0,
                    );
                    $number_saturday = 0;
                    
                    if(strtotime($staff['contract_from_date']) < strtotime(date($params['year'] . '-' . $params['month'] . '-1')))
                    {
                        $from_day = 0;
                    }
                    else
                    {
                        $from_day = intval(date('d', strtotime($staff['contract_from_date'])));
                    }
                    $to_day = empty($staff['contract_to_date'])?'':intval(date('d', strtotime($staff['contract_to_date'])));
                    $array_sunday_total = array();
                    $array_sunday = array();
                    $index_sunday = 1;
                    for($i = 1; $i <= $number_day_of_month; $i++)
                    {
                        if(date('D', strtotime(date($params['year'] . '-' . $this->params['month'] . '-' . $i))) == 'Sun')
                        {
                            $array_sunday_total[$i] = $index_sunday;
                            $index_sunday++;
                        }

                        $class = '';
                        if(!empty($array_day[$i]['mc']))
                        {
                            if($array_day[$i]['approve'] == 0 && $array_day[$i]['real'] < 1 && empty($array_day[$i]['hr_approved']))
                            {
                                $class = 'not-approve';
                            }
                        }
                        if(!empty($array_day[$i]['hr_approved']))
                        {
                        }
                        elseif(!empty($data_training[$staff['id']]) && in_array($i, $data_training[$staff['id']]))
                        {
                        }
                        elseif(!empty($data_leave[$staff['id']][$i]))
                        {
                            if($data_leave[$staff['id']][$i]['sub_stock'] == 1)
                            {
                                if($data_leave[$staff['id']][$i]['is_half_day'] == 1)
                                {
                                    $array_total_day['leave']+= 0.5;
                                }
                                else
                                {
                                    $array_total_day['leave']++;
                                }
                            }
                        }
                        elseif(!empty($array_day[$i]) || in_array($i, $public_holyday))
                        {
                            if(!in_array($array_day[$i]['type'], array('L', 'K')) && in_array($i, $public_holyday))
                            {
                                $array_day[$i]['type'] = 'X';
                            }

                            if($array_day[$i]['type'] == 'S')
                            {
                            }
                            elseif($array_day[$i]['type'] == 'Y')
                            {
                            }
                            else
                            {
                            }
                            if($array_day[$key_day]['type'] != 'P' && date('D', strtotime(date($params['year'] . '-' . $params['month'] . '-' . $i))) == 'Sat')
                            {
                                $number_saturday++;
                            }

                            $array_day[$i]['status']=1;
                        }
                            
                        
                        if($i >= $from_day && ($i < $to_day || empty($to_day)))
                        {
                            if(date('D', strtotime(date($params['year'] . '-' . $params['month'] . '-' . $i))) == 'Sun')
                            {
                                $array_sunday[$i] = $i;
                            }

                            if($array_day[$i]['status']==1 && !(!empty($data_training[$staff['id']]) && in_array($i, $data_training[$staff['id']])))
                            {
                                switch ($array_day[$i]['type']) {
                                    case 'G':
                                        $array_total_day_contract['gay']++;
                                        break;
                                    case 'H':
                                        $array_total_day_contract['normal']+=0.5;
                                        break;
                                    case 'X':
                                        $array_total_day_contract['normal']++;
                                        break;
                                    case 'L':
                                        if($array_day[$i]['multiple'] == 2)
                                        {
                                            $array_total_day_contract['special_2']++;
                                        }
                                        elseif($array_day[$i]['multiple'] == 3)
                                        {
                                            $array_total_day_contract['special_3']++;
                                        }
                                        elseif($array_day[$i]['multiple'] == 4)
                                        {
                                            $array_total_day_contract['special_4']++;
                                        }
                                        break;
                                    case 'K':
                                        if($array_day[$i]['multiple'] == 2)
                                        {
                                            $array_total_day_contract['special_2']+= 0.5;
                                        }
                                        elseif($array_day[$i]['multiple'] == 3)
                                        {
                                            $array_total_day_contract['special_3']+= 0.5;
                                        }
                                        elseif($array_day[$i]['multiple'] == 4)
                                        {
                                            $array_total_day_contract['special_4']+= 0.5;
                                        }
                                        break;
                                    case 'C':
                                        $array_total_day_contract['sunday']++;
                                        break;
                                    case 'D':
                                        $array_total_day_contract['sunday']+=0.5;
                                        break;
                                    case 'S':
                                        $array_total_day_contract['saturday']++;
                                        break;
                                    case 'Y':
                                        $array_total_day_contract['saturday']+=0.5;
                                        break;
                                    case 'S':
                                        $array_total_day_contract['saturday']++;
                                        break;
                                    case 'Y':
                                        $array_total_day_contract['saturday']+=0.5;
                                        break;
                                }
                            }
                        }
                        if($array_day[$i]['status']==1 && !(!empty($data_training[$staff['id']]) && in_array($i, $data_training[$staff['id']])))
                        {
                            switch ($array_day[$i]['type']) {
                                case 'G':
                                    $array_total_day['gay']++;
                                    break;
                                case 'H':
                                    $array_total_day['normal']+=0.5;
                                    break;
                                case 'X':
                                    $array_total_day['normal']++;
                                    break;
                                case 'L':
                                    if($array_day[$i]['multiple'] == 2)
                                    {
                                        $array_total_day['special_2']++;
                                    }
                                    elseif($array_day[$i]['multiple'] == 3)
                                    {
                                        $array_total_day['special_3']++;
                                    }
                                    elseif($array_day[$i]['multiple'] == 4)
                                    {
                                        $array_total_day['special_4']++;
                                    }
                                    break;
                                case 'K':
                                    if($array_day[$i]['multiple'] == 2)
                                    {
                                        $array_total_day['special_2']+= 0.5;
                                    }
                                    elseif($array_day[$i]['multiple'] == 3)
                                    {
                                        $array_total_day['special_3']+= 0.5;
                                    }
                                    elseif($array_day[$i]['multiple'] == 4)
                                    {
                                        $array_total_day['special_4']+= 0.5;
                                    }
                                    break;
                                case 'C':
                                    $array_total_day['sunday']++;
                                    break;
                                case 'D':
                                    $array_total_day['sunday']+=0.5;
                                    break;
                                case 'S':
                                    $array_total_day['saturday']++;
                                    break;
                                case 'Y':
                                    $array_total_day['saturday']+=0.5;
                                    break;
                                case 'S':
                                    $array_total_day['saturday']++;
                                    break;
                                case 'Y':
                                    $array_total_day['saturday']+=0.5;
                                    break;
                            }
                        }
                    }
                    $tong_ngay_cong_thuong = $array_total_day['normal'] + $array_total_day_contract['gay'];
                    $so_ngay_du = ($tong_ngay_cong_thuong > $ngay_dinh_muc)?($tong_ngay_cong_thuong - $ngay_dinh_muc):0;
                    $so_ngay_tru = 0;
                    if($so_ngay_du > 0)
                    {
                        foreach ($array_sunday as $key_sunday => $value_sunday) 
                        {
                            if($array_sunday_total[$value_sunday] <= $so_ngay_du)
                            {
                                $so_ngay_tru++;
                            }
                        }
                    }

                    $sheet->setCellValue($alpha++ . ($key+2), $array_total_day_contract['normal'] + $array_total_day_contract['gay'] - $so_ngay_tru);
                    $sheet->setCellValue($alpha++ . ($key+2), $array_total_day_contract['gay']);
                    $sheet->setCellValue($alpha++ . ($key+2), empty($data_training[$staff['id']])?'0':count($data_training[$staff['id']]));
                    $sheet->setCellValue($alpha++ . ($key+2), $array_total_day_contract['sunday']);

                    if(empty($to_day))
                    {
                        if(empty($staff['number']))
                        {
                            $suturday_export = $number_saturday;
                        }
                        elseif($number_saturday > $staff['number'])
                        {
                            $suturday_export = $number_saturday - $staff['number'];
                        }
                        else
                        {
                            $suturday_export = '0';
                        }
                    }
                    else
                    {
                        $suturday_export = "0";
                    }

                    $sheet->setCellValue($alpha++ . ($key+2), $suturday_export);
                    $sheet->setCellValue($alpha++ . ($key+2), $array_total_day['special_2']);
                    $sheet->setCellValue($alpha++ . ($key+2), $array_total_day['special_3']);
                    $sheet->setCellValue($alpha++ . ($key+2), $array_total_day['special_4']);
                    $sheet->setCellValue($alpha++ . ($key+2), $array_total_day['leave']);
                    $sheet->setCellValue($alpha++ . ($key+2), empty($staff['overtime'])?0:date('H', strtotime($staff['overtime'])));

                    if(empty($to_day))
                    {
                        foreach ($this->list_allowance as $key_allowance => $value_allowance) 
                        {
                            if($value_allowance['id'] == 1)
                            {
                                $sheet->setCellValue($alpha++ . ($key+2), empty($this->data_allowance[$staff['id']][$value_allowance['id']])?0:$this->data_allowance[$staff['id']][$value_allowance['id']]);
                            }
                        }
                    }
                    else
                    {
                        $sheet->setCellValue($alpha++ . ($key+2), 0);;
                    }
                    if($staff['transfer'] == 1)
                    {
                        for($a = 'A'; $a < $alpha; $a++)
                        {
                            $sheet->getStyle($a . ($key+2))->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'FF0000')
                                    )
                                )
                            );
                        }
                    }
                    
                    $sum_key = $key+1;
                }
                
                $data_new_staff = $QTime->get_new_staff_time($params);
                $array_ddtm = array(
                    24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
                );
                foreach ($data_new_staff as $key => $staff) 
                {
                    $company_name = (in_array($staff['area_id'], $array_ddtm))?'Di Động Thông Minh':'OPPO';
                    $key = $sum_key + $key;
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . ($key+2), $company_name);
                    $sheet->setCellValueExplicit($alpha++ . ($key+2), $staff['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alpha++ . ($key+2), $staff['firstname'] . ' ' . $staff['lastname']);

                    $arrayTeam = $printTitle->GetTeamName($staff['title']);
                    
                    $sheet->setCellValue($alpha++ . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['name']);
                    $sheet->setCellValue($alpha++ . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['name']);
                    $sheet->setCellValue($alpha++ . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['children'][$staff['title']]['name']);
                    $sheet->setCellValue($alpha++ . ($key+2), $staff['area']);
                    $sheet->setCellValue($alpha++ . ($key+2), '');
                    $sheet->setCellValue($alpha++ . ($key+2), '');
                    $sheet->setCellValue($alpha++ . ($key+2), '');
                    $sheet->setCellValue($alpha++ . ($key+2), !empty($staff['off_date'])?date('d/m/Y', strtotime($staff['off_date'])):'');
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                    $sheet->setCellValue($alpha++ . ($key+2), count(json_decode($staff['list_day'], true)));
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                    if($staff['current_title'] == $staff['title'])
                    {
                        $sheet->setCellValue($alpha++ . ($key+2), !empty($data_allowance[$staff['staff_id']][1])?$data_allowance[$staff['staff_id']][1]:0);
                    }
                    else
                    {
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                    }
                    $sheet->setCellValue($alpha++ . ($key+2),empty($staff['overtime'])?0:date('H', strtotime($staff['overtime'])));
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                    $sheet->setCellValue($alpha++ . ($key+2), 0);
                }

                $filename = 'Total - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;
            }
            $data = $QTime->get_time_list_total($limit, $page, $params);
            
            $array_staff_id = array();
            $max_page = ceil($data['total'] / $limit);
            $page_trainer = $page - $max_page;
            $sum_offset = ($limit * $max_page) - $data['total'];
            $limit_trainer = (($limit - count($data['data'])) > 0 && count($data['data'] > 0))?($limit - count($data['data'])):$limit;
            
            $data_new_staff_trainer = $QTime->get_staff_new_time($limit_trainer, $page_trainer, $sum_offset, $params);
            if($page < $max_page)
            {
                $data_new_staff_trainer['data'] = null;
            }
            foreach ($data['data'] as $key => $value) {
                $array_staff_id[] = $value['id'];
                $array_cmnd[] = $value['cmnd'];
            }
            if(!empty(implode(",", $array_staff_id)))
            {
                $params['list_staff_id'] = "(" . implode(",", $array_staff_id) . ")";
                $params['list_cmnd'] = "('" . implode("','", $array_cmnd) . "')";
                $data_training = $QTime->getListTrainingByStaffId($params);
                $data_allowance = $QTime->getListAllowanceByStaffId($params);
                $data_leave = $QTime->getListLeaveByStaffId($params);

                $list_training_check_in = $QTime->getListTrainingCheckIn($params);

                $this->view->data_training = $data_training;
                $this->view->data_allowance = $data_allowance;
                $this->view->data_leave = $data_leave;
            }
            unset($params['list_staff_id']);
            unset($params['list_cmnd']);
            $this->view->staffs = $data['data'];
            $this->view->staffs_trainer = $data_new_staff_trainer['data'];
            $this->view->limit = $limit;
            $this->view->total = $data['total'] + $data_new_staff_trainer['total'];
            $this->view->number_day_of_month = $number_day_of_month;
            $this->view->url = HOST . 'staff-time/total' . ($params ? '?' . http_build_query($params) .
                '&' : '?');

            $this->view->offset = $limit * ($page - 1);
        }
        
        $printTitle = new printTitle();
        $printTitle->setTeam($recursiveDeparmentTeamTitle);
        $this->view->printTitle = $printTitle;
        $this->view->public_holyday = $public_holyday;
        $this->view->shift = $QShift->getShiftByTitle($userStorage->title);
        $this->view->current_user  = $userStorage;
        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;
        $this->view->params = $params;
        $this->view->list_allowance = $list_allowance;
    }

    public function trainingListDetailAction()
    {
        $month = $this->getRequest()->getParam('month', date('m'));
        $year = $this->getRequest()->getParam('year', date('Y'));
        $code = $this->getRequest()->getParam('code');
        $email = $this->getRequest()->getParam('email');
        $name = $this->getRequest()->getParam('name');
        $export = $this->getRequest()->getParam('export', 0);
        $page = $this->getRequest()->getParam('page', 1);

        $params = array(
            'code' => $code,
            'month' => $month,
            'name' => $name,
            'year' => $year,
            'email' => $email,
        );
        $QTime = new Application_Model_Time2();
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
        if(!empty($export))
        {
            $params['limit'] = null;
            $params['offset'] = null;
            $data_export = $QTime->getListTraining($params);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'A' => 'Code',
                'B' => 'Name',
            );

            $array_a_to_z = range("A", "Z");
            $array_head = array();

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                if($i > 24)
                {
                    $array_head[$i] .= "A" . $array_a_to_z[$i-25];
                }
                else
                {
                    $array_head[$i] = $array_a_to_z[$i+1];
                }
            }
            foreach($array_head as $key => $value)
            {
                $heads[$value] = $key;
            }
            

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);

            $sheet->getStyle('A1:' . $array_head[count($array_head)-1] . '1')->applyFromArray(array('font' => array('bold' => true)));
            $sheet->getColumnDimension('A')->setWidth(15);
            $sheet->getColumnDimension('B')->setWidth(30);
            foreach($array_head as $key => $value)
            {
                $sheet->getColumnDimension($value)->setWidth(5);
            }

            foreach ($data_export['data'] as $key => $staff) 
            {
                if(!empty($staff['code']))
                {
                    $sheet->setCellValue('A' . ($key+2), $staff['code']);
                }
                else
                {
                    $sheet->setCellValue('A' . ($key+2), $staff['cmnd']);
                }

                if(!empty($staff['staff_name']))
                {
                    $sheet->setCellValue('B' . ($key+2), $staff['staff_name']);
                }
                else
                {
                    $sheet->setCellValue('B' . ($key+2), $staff['new_staff_name']);
                }
                $array_day_training = json_decode($staff['list_day']); 
                foreach ($array_head as $key_day => $value) 
                {
                    if(in_array($key_day, $array_day_training))
                    {
                        $sheet->setCellValue($value . ($key+2), "T");
                    }
                }
            }
         

            $filename = 'Staff Training - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            
            exit;
        }

        $params['limit'] = 10;
        $params['page'] = $page;
        $params['offset'] = $params['limit']*($params['page'] - 1);
        
        $data = $QTime->getListTraining($params);

        $this->view->data = $data['data'];
        $this->view->params = $params;
        $this->view->total = $data['total'];
        unset($params['page']);
        $this->view->url = HOST . 'staff-time/training-list-detail' . ($params ? '?' . http_build_query($params) .
                '&' : '?');

        
        $this->view->number_day_of_month = $number_day_of_month;
    }

    public function getCheckInDetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->isXmlHttpRequest())
        {
            $date = $this->getRequest()->getParam('date');
            $code = $this->getRequest()->getParam('code');

            $QCheckInDetail = new Application_Model_CheckInDetail();
            $data = $QCheckInDetail->getDetailByDate($date, $code);
            if(!empty($data))
            {
                $data_ajax = array();
                if(!empty($data['check_in_at']))
                {
                    $data_ajax['check_in_at'] = date('H:i d/m/Y', strtotime($data['check_in_at']));
                }

                if(!empty($data['check_out_at']))
                {
                    $data_ajax['check_out_at'] = date('H:i d/m/Y', strtotime($data['check_out_at']));
                }

                if(!empty($data['created_at']))
                {
                    $data_ajax['created_at'] = date('H:i d/m/Y', strtotime($data['created_at']));
                }
                

                $data_ajax['check_in_late_time'] = $data['check_in_late_time'];
                $data_ajax['check_out_soon_time'] = $data['check_out_soon_time'];
            }

            echo json_encode($data_ajax); die;
        }
    }

    public function totalzAction()
    {
        $department = $this->getRequest()->getParam('department');
        $team = $this->getRequest()->getParam('team');
        $title = $this->getRequest()->getParam('title');
        $area = $this->getRequest()->getParam('area');
        $month = $this->getRequest()->getParam('month', date('m'));
        $year = $this->getRequest()->getParam('year', date('Y'));
        $code = $this->getRequest()->getParam('code');
        $search = $this->getRequest()->getParam('search', 0);
        $page = $this->getRequest()->getParam('page', 1);
        $export = $this->getRequest()->getParam('export', 0);

        $params = array(
            'department' => $department,
            'team' => $team,
            'title' => $title,
            'area' => $area,
            'month' => $month,
            'year' => $year,
            'code' => $code,
            'limit' => 30,
        );

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $printTitle = new printTitle();
        $printTitle->setTeam($recursiveDeparmentTeamTitle);
        $this->view->printTitle = $printTitle;

        if(!empty($export))
        {
            $params['limit'] = null;
            $params['offset'] = ($page - 1) * $params['limit'];

            $QTime2 = new Application_Model_Time2();
            $data = $QTime2->totalTime($params);

            $staff_ids = array_unique(array_map(create_function('$o', 'return $o["staff_id"];'), $data['data']));
            
            if(!empty($staff_ids))
            {
                $params_training = array(
                    'month' => $month,
                    'year' => $year,
                    'list_staff_id' => "(" . implode(",", $staff_ids) . ")",
                );

                $data_training = $QTime2->getDataTraining($params_training);
                $data_allowance = $QTime2->getListAllowanceByStaffId($params_training);
            }

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'A' => 'Company',
                'B' => 'Code',
                'C' => 'Name',
                'D' => 'Department',
                'E' => 'Team',
                'F' => 'Title',
                'G' => 'Join date',
                'H' => 'Off date',
                'I' => 'Công thường',
                'J' => 'Công chủ nhật',
                'K' => 'Công lễ (x2)',
                'L' => 'Công gãy',
                'M' => 'Công training',
                'N' => 'Công thứ 7',
                'O' => 'Tiền cơm (ngày)',
                'P' => 'Tổng giờ tăng ca',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);

            $sheet->getStyle('A1:R1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(15);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(30);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(20);
            $sheet->getColumnDimension('F')->setWidth(20);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('H')->setWidth(20);
            $sheet->getColumnDimension('I')->setWidth(20);
            $sheet->getColumnDimension('J')->setWidth(20);
            $sheet->getColumnDimension('K')->setWidth(20);
            $sheet->getColumnDimension('L')->setWidth(20);
            $sheet->getColumnDimension('M')->setWidth(20);
            $sheet->getColumnDimension('N')->setWidth(20);
            $sheet->getColumnDimension('Q')->setWidth(25);
            $sheet->getColumnDimension('O')->setWidth(25);
            $sheet->getColumnDimension('P')->setWidth(25);
            $sheet->getColumnDimension('R')->setWidth(25);

            foreach ($data['data'] as $key => $staff) 
            {
                 $sheet->setCellValue('A' . ($key+2), $staff['company_name']);
                 $sheet->setCellValue('B' . ($key+2), $staff['code']);
                 $sheet->setCellValue('C' . ($key+2), $staff['staff_name']);

                 $arrayTeam = $printTitle->GetTeamName($staff['title']);

                 $sheet->setCellValue('D' . ($key+2), $arrayTeam['department']);
                 $sheet->setCellValue('E' . ($key+2), $arrayTeam['team']);
                 $sheet->setCellValue('F' . ($key+2), $arrayTeam['title']);
                 $sheet->setCellValue('G' . ($key+2), date('d/m/Y', strtotime($staff['joined_at'])));
                 $sheet->setCellValue('H' . ($key+2), !empty($staff['off_date'])?date('d/m/Y', strtotime($staff['off_date'])):'');
                 $sheet->setCellValue('I' . ($key+2), $staff['workday']);
                 $sheet->setCellValue('J' . ($key+2), $staff['sunday']);
                 $sheet->setCellValue('K' . ($key+2), $staff['special']);
                 $sheet->setCellValue('L' . ($key+2), $staff['gay']);
                 if(empty( $this->data_training[$staff['staff_id']][$staff['title']] ))
                 {
                    $sheet->setCellValue('M' . ($key+2), 0);
                 }
                 else
                 {
                    $sheet->setCellValue('M' . ($key+2), $data_training[$staff['staff_id']][$staff['title']]);
                 }
                $number = empty($staff['number'])?0:$staff['number'];
                $saturday_day = empty($staff['saturday_day'])?0:$staff['saturday_day'];
                 $sheet->setCellValue('N' . ($key+2), $saturday_day - $number);
                 if($staff['current_title'] == $staff['title'])
                 {
                    $sheet->setCellValue('O' . ($key+2), !empty($data_allowance[$staff['staff_id']][1])?$data_allowance[$staff['staff_id']][1]:0);
                }
                 else
                 {
                    $sheet->setCellValue('O' . ($key+2), 0);
                 }
                 $sheet->setCellValue('P' . ($key+2),empty($staff['overtime'])?0:date('H', strtotime($staff['overtime'])));
            }
                        
            $data_new_staff = $QTime2->get_new_staff_time($params);
            $sum_key = count($data_export['data']);
            $array_ddtm = array(
                24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
            );
            foreach ($data_new_staff as $key => $staff) 
            {
                $company_name = (in_array($staff['area_id'], $array_ddtm))?'Di Động Thông Minh':'OPPO';
                $key = $sum_key + $key;
                $sheet->setCellValue('A' . ($key+2), $company_name);
                $sheet->setCellValue('B' . ($key+2), $staff['code']);
                $sheet->setCellValue('C' . ($key+2), $staff['firstname'] . ' ' . $staff['lastname']);

                $arrayTeam = $printTitle->GetTeamName($staff['title']);
                
                $sheet->setCellValue('D' . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['name']);
                $sheet->setCellValue('E' . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['name']);
                $sheet->setCellValue('F' . ($key+2), $recursiveDeparmentTeamTitle[$staff['department']]['children'][$staff['team']]['children'][$staff['title']]['name']);
                $sheet->setCellValue('G' . ($key+2), '');
                $sheet->setCellValue('H' . ($key+2), !empty($staff['off_date'])?date('d/m/Y', strtotime($staff['off_date'])):'');
                $sheet->setCellValue('I' . ($key+2), 0);
                $sheet->setCellValue('J' . ($key+2), 0);
                $sheet->setCellValue('K' . ($key+2), 0);
                $sheet->setCellValue('L' . ($key+2), 0);
                $sheet->setCellValue('M' . ($key+2), count(json_decode($staff['list_day'], true)));
                $sheet->setCellValue('N' . ($key+2), 0);
                if($staff['current_title'] == $staff['title'])
                {
                    $sheet->setCellValue('O' . ($key+2), !empty($data_allowance[$staff['staff_id']][1])?$data_allowance[$staff['staff_id']][1]:0);
                }
                else
                {
                $sheet->setCellValue('O' . ($key+2), 0);
                }
                $sheet->setCellValue('P' . ($key+2),empty($staff['overtime'])?0:date('H', strtotime($staff['overtime'])));
            }

            $filename = 'Total Time - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
            
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }

        if(!empty($search))
        {
            $params['offset'] = ($page - 1) * $params['limit'];
            $params['search'] = 1;

            $QTime2 = new Application_Model_Time2();
            $data = $QTime2->totalTime($params);
            
            $staff_ids = array_unique(array_map(create_function('$o', 'return $o["staff_id"];'), $data['data']));
            
            if(!empty($staff_ids))
            {
                $params_training = array(
                    'month' => $month,
                    'year' => $year,
                    'list_staff_id' => "(" . implode(",", $staff_ids) . ")",
                );

                $data_training = $QTime2->getDataTraining($params_training);
                $this->view->data_training = $data_training;

                $data_allowance = $QTime2->getListAllowanceByStaffId($params_training);
                $this->view->data_allowance = $data_allowance;
            }

            
            $list_allowance = $QTime2->getAllowance();
            $this->view->list_allowance = $list_allowance;

            $this->view->data = $data['data'];
            $this->view->limit = $params['limit'];
            $this->view->offset = $params['offset'];
            $this->view->total = $data['total'];
            $this->view->url = HOST . 'staff-time/total' . ($params ? '?' . http_build_query($params) .
                '&' : '?');
        }
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->current_user = $userStorage;

        if(in_array($userStorage->group_id, array(HR_ID)))
        {
            $this->view->show_columns = 1;
        }
        else
        {
            $this->view->show_columns = 0;
        }

        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;

        $this->view->params = $params;

    }

    public function staffViewAction()
    {
    	require_once 'staff-time' . DIRECTORY_SEPARATOR . 'staff-view.php';
    }
    //dang ki ngay cong online cho nhan vien mua dich
    public function addOnlineTimeAction()
    {
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'add-online-time.php';
    }

    public function deleteOnlineTimeAction()
    {
        require_once 'staff-time' . DIRECTORY_SEPARATOR . 'delete-online-time.php';
    }

    // public function staffViewAction()
    // {
    //     $flashMessenger = $this->_helper->flashMessenger;
    //     $month = $this->getRequest()->getParam('month', date('m'));
    //     $year = $this->getRequest()->getParam('year', date('Y'));
    //     $shift = $this->getRequest()->getParam('shift');
    //     $office_time = $this->getRequest()->getParam('office_time');
    //     $date = $this->getRequest()->getParam('date');
    //     $reason = $this->getRequest()->getParam('reason');
    //     $update_time = $this->getRequest()->getParam('update-time', 0);
    //     $create_leave = $this->getRequest()->getParam('create_leave');

    //     $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    //     $staff_id = $userStorage->id;
    //     $QTime2 = new Application_Model_Time2();

    //     // leave

    //     $QLeaveDetail = new Application_Model_LeaveDetail();
    //     if(!empty($create_leave))
    //     {
    //         $is_leave_half = $this->getRequest()->getParam("is-leave-half");
    //         $from = $this->converDate($this->getRequest()->getParam("from"));
    //         $to = $this->converDate($this->getRequest()->getParam("to"));
    //         $date_leave = $this->converDate($this->getRequest()->getParam("date-leave-half"));
    //         $leave_type = $this->getRequest()->getParam("leave-type");
    //         $reason_leave = $this->getRequest()->getParam("reason");
    //         $auth = Zend_Auth::getInstance()->getStorage()->read();
    //         $is_office = $auth->is_officer;
    //         $staff_id = $auth->id;
            
    //         $upload = new Zend_File_Transfer_Adapter_Http();
    //         $uniqid = uniqid('', true);
    //         $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    //         $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
    //                 . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'
    //                 . DIRECTORY_SEPARATOR . 'leave'
    //                 . DIRECTORY_SEPARATOR . $userStorage->id;
            
    //         if (!is_dir($uploaded_dir))
    //                 @mkdir($uploaded_dir, 0777, true);

    //         $upload->setDestination($uploaded_dir);

    //         $array_img = array();
    //         $files = $upload->getFileInfo();
    //         foreach($files as $file => $fileInfo) 
    //         {
    //             if(!empty($fileInfo['name']))
    //             {
    //                 $old_name = $fileInfo['name'];

    //                 $tExplode = explode('.', $old_name);
    //                 $extension = strtolower(end($tExplode));
    //                 $new_name = md5(uniqid('', true)) . '.' . $extension;
    //                 $upload->receive($file);
    //                 if (is_file($uploaded_dir . DIRECTORY_SEPARATOR . $old_name))
    //                 {
    //                     rename($uploaded_dir . DIRECTORY_SEPARATOR . $old_name, $uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
    //                 }
    //                 $array_img[] = $new_name;
    //             }
    //         }

    //         if($is_leave_half == 1)
    //         {
    //             if(strtotime($from) > strtotime($to))
    //             {
    //                 $flashMessenger->setNamespace('error')->addMessage("Ngày bắt đầu nghỉ phép không được lơn hơn ngày kết thúc");
    //                 $this->_redirect("/leave/create");
    //             }
    //             else
    //             {
    //                 $params = array(
    //                     'from' => $from,
    //                     'to' => $to,
    //                     'leave_type' => $leave_type,
    //                     'is_officer' => $is_office,
    //                     'staff_id' => $staff_id,
    //                     'reason' => $reason_leave,
    //                     'image' => json_encode($array_img),
    //                 );
    //                 $QLeaveDetail = new Application_Model_LeaveDetail();
    //                 $data = $QLeaveDetail->insertLeave($params);
    //             }
    //         }
    //         else
    //         {
    //             $params = array(
    //                 'date' => $date_leave,
    //                 'leave_type' => $leave_type,
    //                 'is_officer' => $is_office,
    //                 'staff_id' => $staff_id,
    //                 'reason' => $reason_leave,
    //                 'image' => json_encode($array_img),
    //             );
    //             $QLeaveDetail = new Application_Model_LeaveDetail();
    //             $data = $QLeaveDetail->insertLeaveHalf($params);
    //         }
    //         if($data['status'] == 1)
    //         {
    //             $flashMessenger->setNamespace('success')->addMessage('Thêm phép thành công.');
    //             $this->_redirect("/leave/list-my-leave");
    //         }
    //         else
    //         {
    //             $flashMessenger->setNamespace('error')->addMessage($data['message']);
    //             $this->_redirect("/leave/create");
    //         }
    //     }
        
    //     $QLeavInfo = new Application_Model_LeaveInfo();

    //     $this->view->checkHalfLeave = $QLeaveDetail->checkHalfLeave();
    //     $this->view->stock = $QLeavInfo->getStockById(Zend_Auth::getInstance()->getStorage()->read()->id);
    //     $QLeaveType = new Application_Model_LeaveType();
    //     $this->view->parent_type = $QLeaveType->getParent(); 

    //     // end leave
    //     if(!empty($update_time))
    //     {
    //         if(empty($userStorage->is_officer))
    //         {
    //             $office_time = null;
    //         }
    //         $params_update = array(
    //             'date' => $date,
    //             'shift' => $shift,
    //             'office_time' => $office_time,
    //             'staff_id' => $staff_id,
    //             'reason' => $reason,
    //         );


    //         $QTime2->insertTempTime($params_update);
    //         $flashMessenger->setNamespace('success')->addMessage("Đã yêu cầu thành công.");
    //     }

    //     $params_training = $params = array(
    //         'month' => $month,
    //         'year' => $year,
    //         'staff_id' => $staff_id,
    //         'code' => $userStorage->code,
    //     );

    //     $params_training['list_staff_id'] = "(" . $staff_id . ")";
    //     $params_training['list_cmnd'] = "(-1)";

    //     $this->view->params = $params;
    //     $this->view->auth = $userStorage;
        
    //     $data = $QTime2->getStaffTime($params);
    //     $data_map = array_reduce($data, 
    //                     function(&$result, $item)
    //                     {
    //                         $result[intval(date('d', strtotime($item['date'])))] = $item;
    //                         return $result;
    //                     }, array()
    //                 );
        
    //     $data_training = $QTime2->getListTrainingByStaffId($params_training);
    //     $data_allowance = $QTime2->getListAllowanceByStaffId($params_training);
    //     $data_leave = $QTime2->getListLeaveByStaffId($params_training);
    //     $this->view->data_leave = $data_leave[$staff_id];
    //     $this->view->data_training = $data_training[$staff_id];
    //     $this->view->data_allowance = $data_allowance[$staff_id];

    //     $this->view->data = $data_map;
    //     $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);

    //     $this->view->number_day_of_month = $number_day_of_month;

    //     $QArea = new Application_Model_Area();
    //     $all_area = $QArea->get_cache();

    //     $this->view->areas = $all_area;

    //     $QTeam = new Application_Model_Team();
    //     $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    //     $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

    //     $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
    //     $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
    // }

    public function viewStaffTimeAction()
    {
	require_once 'staff-time' . DIRECTORY_SEPARATOR . 'view-staff-time.php';	
    }

    public function staffViewTempAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        $month = $this->getRequest()->getParam('month', date('m'));
        $year = $this->getRequest()->getParam('year', date('Y'));
        $shift = $this->getRequest()->getParam('shift');
        $office_time = $this->getRequest()->getParam('office_time');
        $date = $this->getRequest()->getParam('date');
        $reason = $this->getRequest()->getParam('reason');
        $update_time = $this->getRequest()->getParam('update-time', 0);

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id = $userStorage->id;
        $QTime2 = new Application_Model_Time2();

        $params = array(
            'month' => $month,
            'year' => $year,
            'staff_id' => $userStorage->id,
        );

        $this->view->params = $params;
        $this->view->auth = $userStorage;

        $data = $QTime2->getStaffView($params);
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);

        $this->view->number_day_of_month = $number_day_of_month;

        $QArea = new Application_Model_Area();
        $all_area = $QArea->get_cache();

        $this->view->areas = $all_area;

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
    }

    public function setStaffAllowanceAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        $month = $this->getRequest()->getParam('month', date('m'));
        $year = $this->getRequest()->getParam('year', date('Y'));
        $staff_id = $this->getRequest()->getParam('staff_id');
        $allowance_id = $this->getRequest()->getParam('allowance_id');
        $quantity = $this->getRequest()->getParam('quantity');
        $submit = $this->getRequest()->getParam('submit', 0);
        $id = $this->getRequest()->getParam('id');
        
        $params = array(
            'month' => $month,
            'year' => $year,
            'staff_id' => $staff_id,
            'quantity' => $quantity,
            'allowance_id' => $allowance_id,
        );

        $QTime2 = new Application_Model_Time2();
        if(!empty($submit))
        {
            $QTime2->setStaffAllowance($params);
            $flashMessenger->setNamespace('success')->addMessage("Done.");
            $this->redirect('staff-time/list-staff-allowance');
        }
        else
        {
            if(!empty($id))
            {
                $params_by_id = $QTime2->getStaffAllowanceById($id);
                $this->view->params = $params_by_id;
            }
            else
            {
                $this->view->params = $params;
            }
        }
        
        $list_allowance = $QTime2->getAllowance();
        $this->view->list_allowance = $list_allowance;
    }

    public function listStaffAllowanceAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        $month = $this->getRequest()->getParam('month', date('m'));
        $year = $this->getRequest()->getParam('year', date('Y'));
        $name = $this->getRequest()->getParam('name');
        $code = $this->getRequest()->getParam('code');
        $page = $this->getRequest()->getParam('page', 1);

        $params = array(
            'month' => $month,
            'year' => $year,
            'code' => $code,
            'name' => $name,
            'department' => $department,
            'team' => $team,
            'title' => $title,
            'limit' => 30,
        );

        $params['offset'] = ($page - 1) * $params['limit'];

        $QTime2 = new Application_Model_Time2();
        $data = $QTime2->fetchPaginationAllowance($params);

        $this->view->params = $params;
        $this->view->list_allowance = $list_allowance;
        $this->view->data = $data['data'];
        $this->view->total = $params['total'];
        $this->view->limit = $params['limit'];
        $this->view->total = $data['total'];
        $this->view->url =  HOST . 'staff-time/list-staff-allowance' . ($params ? '?' . http_build_query($params) .
                '&' : '?');
        
        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
    }

    public function ajaxGetRequestAction()
    {
        $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('month');
        $staff_id = $this->getRequest()->getParam('staff_id');


    }

    public function ajaxGetTempTimeAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->isXmlHttpRequest())
        {
            $date = $this->getRequest()->getParam('date');
            $staff_id = $this->getRequest()->getParam('staff_id');
            $params = array (
                'date' => $date,
                'staff_id' => $staff_id,
            );

            $QTime2 = new Application_Model_Time2();
            $data = $QTime2->getDetailTime($params);
            if(!empty($data))
            {
                $data_time = $data[0];
                if(!empty($data_time['check_in_at']))
                {
                    $data_time['check_in_at'] = date('H:i d/m/Y', strtotime($data_time['check_in_at']));
                }

                if(!empty($data_time['check_out_at']))
                {
                    $data_time['check_out_at'] = date('H:i d/m/Y', strtotime($data_time['check_out_at']));
                }

                if(!empty($data['created_at']))
                {
                    $data_time['created_at'] = date('H:i d/m/Y', strtotime($data_time['created_at']));
                }

                echo json_encode($data_time);
            }
            else
            {
                echo json_encode(array());
            }
        }
    }

    function converDate($date = '')
    {
        $array_date = explode("/", $date);

        if(count($array_date) == 3)
        {
            return $array_date[2] . '-' . $array_date[1] . '-' . $array_date[0];
        }
        return;
    }

    public function exportStaffsPermission($data)
    {
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'CODE',
            'NAME',
            'DEPARTMENT',
            'TEAM',
            'TITLE',
            'PROVINCE'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        $stt = 1;
        foreach($data as $key => $value) {
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( $stt++, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['fullname']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['department']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['team']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['province_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        }

        $filename = 'staffs_permission_' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
    public function createImportTotalAllAction()
    {

    }

    public function importTotalAllSaveAction()
    {
            require_once 'staff-time' . DIRECTORY_SEPARATOR . 'import-total-all-save.php';
    }
    public function listStaffTimeCheckInOnlineAction(){
            require_once 'staff-time' . DIRECTORY_SEPARATOR . 'list-staff-time-check-in-online.php';    
}
//hien thi ngày di lam online Covid 19 với manager
    public function showOnlineTimeAction(){
        require_once 'staff-time'.DIRECTORY_SEPARATOR. 'show-online-time.php';
    }
    //lay ngay thang trong tuan de hien tra cho manager
    function getStartAndEndDate($week, $year) {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');
        return $ret;
    }


    //end hien thi ngày di lam online Covid 19 với manager
}

class alphaExcel
{
    protected $_first;
    protected $_second;

    public function alphaExcel()
    {
        $this->_first = '';
        $this->_second = 'A';
    }

    public function Show()
    {
        return $this->_first.$this->_second;
    }

    public function ShowAndUp()
    {
        $current = $this->Show();
        $this->Up();
        return $current;
    }

    public function Up()
    {
        if($this->_first == '')
        {
            if($this->_second == 'Z')
            {
                $this->_second = 'A';
                $this->_first = 'A';
            }
            else
            {
                $this->_second++;
            }
        }
        else
        {
            if($this->_second == 'Z')
            {
                $this->_second++;
                $this->_first = 'A';
            }
            else
            {
                $this->_second++;
            }
        }
    }
}


class printTitle
{
    protected $_team = array();

    public function setTeam($team = array())
    {
        $this->_team = $team;
    }

    public function GetTeamName($title)
    {
        $array_team = array();

        foreach($this->_team as $key_department => $value_department)
        {
            $array_team['department'] = $value_department['name'];
            foreach ($value_department['children'] as $key_team => $value_team) {
                $array_team['team'] = $value_team['name'];
                foreach ($value_team['children'] as $key_title => $value_title) {
                    if($key_title == $title)
                    {
                        $array_team['title'] = $value_title['name'];
                        return $array_team;
                    }
                }
                
            }
        }
    }

}

