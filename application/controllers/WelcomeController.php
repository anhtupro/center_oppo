<?php
class WelcomeController extends My_Controller_Action
{

    public function indexAction(){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (isset($userStorage->menu) && $userStorage->menu) {
            $menu = $userStorage->menu;
        } else {

            $QMenu = new Application_Model_Menu();
            $where = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);
            $menu = $QMenu->fetchAll($where, array('parent_id', 'position'));

        }
        $this->view->menu = $menu;

        //$this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();
    }

    public function testAction(){
        
    }

}
