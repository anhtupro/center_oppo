<?php

class StaffController extends My_Controller_Action {

    private $list_teams, $data, $list_titles;

    public function init() {
        $this->list_teams = array(
            SALES_TEAM,
            SALES_ADMIN_TEAM,
            TRAINING_TEAM,
        );
    }

    /* - configure PG salary action */

    public function configPgAction() {
        $sort = $this->getRequest()->getParam('sort', '');
        $desc = $this->getRequest()->getParam('desc', 1);
        $page = $this->getRequest()->getParam('page', 1);

        $limit = LIMITATION;

        $total = 0;

        $params['sort']          = $sort;
        $params['desc']          = $desc;
        $params['get_salary_pg'] = 1;

        $QRegionalMarket = new Application_Model_RegionalMarket();

        $provinces = $QRegionalMarket->fetchPagination($page, $limit, $total, $params);

        $this->view->desc      = $desc;
        $this->view->sort      = $sort;
        $this->view->provinces = $provinces;
        $this->view->params    = $params;
        $this->view->limit     = $limit;
        $this->view->total     = $total;
        $this->view->url       = HOST . 'staff/config-pg' . ($params ? '?' . http_build_query
                        ($params) . '&' : '?');

        $this->view->offset = $limit * ($page - 1);

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
    }

    public function configPgEditAction() {
        $province_id = $this->getRequest()->getParam('province_id');
        $QModel      = new Application_Model_SalaryPg();

        if ($province_id) {

            $where = $QModel->getAdapter()->quoteInto('province_id = ?', $province_id);

            $item = $QModel->fetchRow($where);

            $this->view->item = $item;

            //get province
            $QRegionalMarket = new Application_Model_RegionalMarket();

            $where = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $province_id);

            $province = $QRegionalMarket->fetchRow($where);

            $this->view->province = $province;
        }

        if ($this->getRequest()->getMethod() == 'POST') {

            $base_salary      = $this->getRequest()->getParam('base_salary');
            $probation_salary = $this->getRequest()->getParam('probation_salary');
            $bonus_salary     = $this->getRequest()->getParam('bonus_salary');
            $allowance_1      = $this->getRequest()->getParam('allowance_1');
            $allowance_2      = $this->getRequest()->getParam('allowance_2');
            $allowance_3      = $this->getRequest()->getParam('allowance_3');
            $kpi              = trim($this->getRequest()->getParam('kpi'));
            $kpi_1            = trim($this->getRequest()->getParam('kpi_1'));
            $work_cost        = $this->getRequest()->getParam('work_cost');

            $data = array(
                'base_salary'      => intval($base_salary),
                'bonus_salary'     => intval($bonus_salary),
                'allowance_1'      => intval($allowance_1),
                'allowance_2'      => intval($allowance_2),
                'allowance_3'      => intval($allowance_3),
                'probation_salary' => intval($probation_salary),
                'kpi'              => $kpi,
                'kpi_1'            => $kpi_1,
                'work_cost'        => intval($work_cost)
            );

            if (isset($item) and $item) //neu co record nay roi
                $QModel->update($data, 'province_id = ' . $province_id);

            else {
                $data['province_id'] = $province_id;
                $QModel->insert($data);
            }

            $back_url = $this->getRequest()->getParam('back_url');

            $this->_redirect($back_url ? $back_url : HOST . 'staff/config-pg');
        }

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        //back url
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
    }

    /* - configure sale salary action  */

    public function configSalesAction() {
        $sort = $this->getRequest()->getParam('sort', '');
        $desc = $this->getRequest()->getParam('desc', 1);
        $page = $this->getRequest()->getParam('page', 1);

        $limit = LIMITATION;

        $total = 0;

        $params['sort']             = $sort;
        $params['desc']             = $desc;
        $params['get_salary_sales'] = 1;

        $QRegionalMarket = new Application_Model_RegionalMarket();

        $provinces = $QRegionalMarket->fetchPagination($page, $limit, $total, $params);

        $this->view->desc      = $desc;
        $this->view->sort      = $sort;
        $this->view->provinces = $provinces;
        $this->view->params    = $params;
        $this->view->limit     = $limit;
        $this->view->total     = $total;
        $this->view->url       = HOST . 'staff/config-sales' . ($params ? '?' .
                http_build_query($params) . '&' : '?');

        $this->view->offset = $limit * ($page - 1);

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
    }

    /* configure sale edit action */

    public function configSalesEditAction() {
        $province_id = $this->getRequest()->getParam('province_id');
        $QModel      = new Application_Model_SalarySales();

        if ($province_id) {

            $where = $QModel->getAdapter()->quoteInto('province_id = ?', $province_id);

            $item = $QModel->fetchRow($where);

            $this->view->item = $item;

            //get province
            $QRegionalMarket = new Application_Model_RegionalMarket();

            $where = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $province_id);

            $province = $QRegionalMarket->fetchRow($where);

            $this->view->province = $province;
        }

        if ($this->getRequest()->getMethod() == 'POST') {

            $base_salary           = $this->getRequest()->getParam('base_salary');
            $bonus_salary          = $this->getRequest()->getParam('bonus_salary');
            $allowance_1           = $this->getRequest()->getParam('allowance_1');
            $allowance_2           = $this->getRequest()->getParam('allowance_2');
            $allowance_3           = $this->getRequest()->getParam('allowance_3');
            $probation_salary      = $this->getRequest()->getParam('probation_salary');
            $kpi                   = $this->getRequest()->getParam('kpi');
            $work_cost             = $this->getRequest()->getParam('work_cost');
            $base_probation_salary = $this->getRequest()->getParam('base_probation_salary');

            $data = array(
                'base_salary'           => intval($base_salary),
                'bonus_salary'          => intval($bonus_salary),
                'allowance_1'           => intval($allowance_1),
                'allowance_2'           => intval($allowance_2),
                'allowance_3'           => intval($allowance_3),
                'probation_salary'      => intval($probation_salary),
                'kpi'                   => $kpi,
                'work_cost'             => intval($work_cost),
                'base_probation_salary' => intval($base_probation_salary),
            );

            if (isset($item) and $item)
                $QModel->update($data, 'province_id = ' . $province_id);

            else {
                $data['province_id'] = $province_id;
                $QModel->insert($data);
            }

            $back_url = $this->getRequest()->getParam('back_url');

            $this->_redirect($back_url ? $back_url : HOST . 'staff/config-sales');
        }

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        //back url
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
    }

    public function contractTermAction() {
        $sort            = $this->getRequest()->getParam('sort', '');
        $desc            = $this->getRequest()->getParam('desc', 1);
        $page            = $this->getRequest()->getParam('page', 1);
        $name            = $this->getRequest()->getParam('name');
        $department      = $this->getRequest()->getParam('department');
        $off             = $this->getRequest()->getParam('off', 1);
        $team            = $this->getRequest()->getParam('team');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $area_id         = $this->getRequest()->getParam('area_id');
        $note            = $this->getRequest()->getParam('note');
        $sname           = $this->getRequest()->getParam('sname', 0);
        $code            = $this->getRequest()->getParam('code');
        $s_assign        = $this->getRequest()->getParam('s_assign');
        $ood             = $this->getRequest()->getParam('ood');
        $email           = $this->getRequest()->getParam('email');
        $contract_term   = $this->getRequest()->getParam('contract_term');
        $is_officer      = $this->getRequest()->getParam('is_officer');
        $ready_print     = $this->getRequest()->getParam('ready_print');
        $date            = $this->getRequest()->getParam('date');
        $month           = $this->getRequest()->getParam('month');
        $year            = $this->getRequest()->getParam('year');
        $tags            = $this->getRequest()->getParam('tags');
        $title           = $this->getRequest()->getParam('title');
        $company_id      = $this->getRequest()->getParam('company_id');
        $no_print        = $this->getRequest()->getParam('no_print');
        $from            = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to              = $this->getRequest()->getParam('to', date('d/m/Y'));
        $QStaff          = new Application_Model_Staff();
        if ($tags and is_array($tags))
            $tags            = $tags;
        else
            $tags            = null;

        //check if export
        $export           = $this->getRequest()->getParam('export', 0);
        $expired_contract = $this->getRequest()->getParam('expired_contract', 0);

        if (!$sname)
            $limit = LIMITATION;
        else
            $limit = null;

        $total = 0;

        $params = array_filter(array(
            'name'            => $name,
            'department'      => $department,
            'off'             => $off,
            'team'            => $team,
            'regional_market' => $regional_market,
            'district'        => $district,
            'area_id'         => $area_id,
            'note'            => $note,
            'code'            => $code,
            's_assign'        => $s_assign,
            'ood'             => $ood,
            'email'           => $email,
            's_assign'        => $s_assign,
            'ready_print'     => $ready_print,
            'ood'             => $ood,
            'email'           => $email,
            'date'            => $date,
            'month'           => $month,
            'year'            => $year,
            'is_officer'      => $is_officer,
            'sname'           => $sname,
            'tags'            => $tags,
            'title'           => $title,
            'company_id'      => $company_id,
            'export'          => $export,
            'contract_term'   => $contract_term,
            'no_print'        => $no_print,
            'from'            => $from,
            'to'              => $to,
        ));

        if ($export == 1) {
            $this->_exportContractPrintingLog($params);
            exit;
        }


        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $QArea             = new Application_Model_Area();
        $this->view->areas = $QArea->fetchAll(null, 'name');

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $QSalarySales    = new Application_Model_SalarySales();
        $QSalaryPG       = new Application_Model_SalaryPg();

        $this->view->regional_markets = $QRegionalMarket->get_cache();

        if ($regional_market) {
            if (is_array($regional_market) && count($regional_market))
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

            $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
        }


        //get salary sales
        $params['salary_sales'] = 1;

        $staffs = $QStaff->fetchPagination($page, $limit, $total, $params);

        if ($export == 2) {
            $staffs = $QStaff->fetchPagination($page, null, $total, $params);
            $this->_exportCsv($staffs);
        }
        $luong = array();

        foreach ($staffs as $k => $v) {
            //sale team
            if ($v['title'] == SALES_TITLE || $v['team'] == SALES_ACCESSORIES_TITLE) {
                $where           = array();
                $where[]         = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $v['regional_market']);
                $luong[$v['id']] = $QSalarySales->fetchRow($where);
            }

            //pg team
            if (My_Staff::isPgTitle($v['title'])) {
                $where1          = array();
                $where1[]        = $QSalaryPG->getAdapter()->quoteInto('province_id = ?', $v['regional_market']);
                $luong[$v['id']] = $QSalaryPG->fetchRow($where1);
            }
        }

        // var_dump($luong);exit;
        //get department
        $QDepartment             = new Application_Model_Department();
        $this->view->departments = $QDepartment->get_cache();

        //get teams
        $QTeam = new Application_Model_Team();


        $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;


        $this->view->luong = $luong;

        $this->view->team_all = $QTeam->get_cache();

        $QCompany              = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $QModel                     = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();
        $this->view->list_contract  = $QModel->get_cache();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $this->view->userStorage = $userStorage;

        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->staffs = $staffs;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;

        $this->view->url = HOST . 'staff/contract-term' . ($params ? '?' .
                http_build_query($params) . '&' : '?');

        $this->view->offset = $limit * ($page - 1);

        $flashMessenger               = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->
                getMessages();
        $this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();

            if ($sname) {
                $QRegion          = new Application_Model_RegionalMarket();
                $regional_markets = $QRegion->fetchAll();

                $rm = array();
                foreach ($regional_markets as $key => $value) {
                    $rm[$value['id']] = $value['name'];
                }
                $this->view->rm_list = $rm;

                $this->_helper->viewRenderer->setRender('partials/searchname');
            } elseif ($s_assign) {
                $this->_helper->viewRenderer->setRender('partials/staff');
            } else
                $this->_helper->viewRenderer->setRender('partials/list');
        }
    }

    public function contractTermOfficeAction() {
        $sort            = $this->getRequest()->getParam('sort', '');
        $desc            = $this->getRequest()->getParam('desc', 1);
        $page            = $this->getRequest()->getParam('page', 1);
        $name            = $this->getRequest()->getParam('name');
        $department      = $this->getRequest()->getParam('department');
        $off             = $this->getRequest()->getParam('off', 1);
        $team            = $this->getRequest()->getParam('team');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $area_id         = $this->getRequest()->getParam('area_id');
        $note            = $this->getRequest()->getParam('note');
        $sname           = $this->getRequest()->getParam('sname', 0);
        $code            = $this->getRequest()->getParam('code');
        $s_assign        = $this->getRequest()->getParam('s_assign');
        $ood             = $this->getRequest()->getParam('ood');
        $email           = $this->getRequest()->getParam('email');
        $contract_term   = $this->getRequest()->getParam('contract_term');
        $is_officer      = $this->getRequest()->getParam('is_officer');
        $ready_print     = $this->getRequest()->getParam('ready_print');
        $date            = $this->getRequest()->getParam('date');
        $month           = $this->getRequest()->getParam('month');
        $year            = $this->getRequest()->getParam('year');
        $tags            = $this->getRequest()->getParam('tags');
        $title           = $this->getRequest()->getParam('title');
        $company_id      = $this->getRequest()->getParam('company_id');
        $no_print        = $this->getRequest()->getParam('no_print');
        $from            = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to              = $this->getRequest()->getParam('to', date('d/m/Y'));

        if ($tags and is_array($tags))
            $tags = $tags;
        else
            $tags = null;

        //check if export
        $export           = $this->getRequest()->getParam('export', 0);
        $expired_contract = $this->getRequest()->getParam('expired_contract', 0);

        if (!$sname)
            $limit = LIMITATION;
        else
            $limit = null;

        $total = 0;

        $params = array_filter(array(
            'name'            => $name,
            'department'      => $department,
            'off'             => $off,
            'team'            => $team,
            'regional_market' => $regional_market,
            'district'        => $district,
            'area_id'         => $area_id,
            'note'            => $note,
            'code'            => $code,
            's_assign'        => $s_assign,
            'ood'             => $ood,
            'email'           => $email,
            's_assign'        => $s_assign,
            'ready_print'     => $ready_print,
            'ood'             => $ood,
            'email'           => $email,
            'date'            => $date,
            'month'           => $month,
            'year'            => $year,
            'is_officer'      => $is_officer,
            'sname'           => $sname,
            'tags'            => $tags,
            'title'           => $title,
            'company_id'      => $company_id,
            'export'          => $export,
            'contract_term'   => $contract_term,
            'no_print'        => $no_print,
            'from'            => $from,
            'to'              => $to,
        ));

        if ($export == 1) {
            $this->_exportContractPrintingLog($params);
            exit;
        }

        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $QArea             = new Application_Model_Area();
        $this->view->areas = $QArea->fetchAll(null, 'name');

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $QSalarySales    = new Application_Model_SalarySales();
        $QSalaryPG       = new Application_Model_SalaryPg();


        $this->view->regional_markets = $QRegionalMarket->get_cache();


        if ($regional_market) {
            if (is_array($regional_market) && count($regional_market))
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

            $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
        }

        $QStaff = new Application_Model_Staff();

        //get salary sales
        $params['is_officer'] = 1;

        $staffs = $QStaff->fetchPagination($page, $limit, $total, $params);

        $luong = array();

        foreach ($staffs as $k => $v) {
            //sale team
            if ($v['is_officer'] == 1) {
                $where           = array();
                $where[]         = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $v['regional_market']);
                $luong[$v['id']] = $QSalarySales->fetchRow($where);
            }
        }



        //get department
        //get department
        $QDepartment             = new Application_Model_Department();
        $this->view->departments = $QDepartment->get_cache();

        //get teams
        $QTeam = new Application_Model_Team();


        $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $this->view->luong = $luong;

        $this->view->team_all = $QTeam->get_cache();

        $QCompany              = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $QModel                     = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();
        $this->view->list_contract  = $QModel->get_cache();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $this->view->userStorage = $userStorage;

        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->staffs = $staffs;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;

        $this->view->url = HOST . 'staff/contract-term-office' . ($params ? '?' .
                http_build_query($params) . '&' : '?');

        $this->view->offset = $limit * ($page - 1);

        $flashMessenger               = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->
                getMessages();
        $this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();

            if ($sname) {
                $QRegion          = new Application_Model_RegionalMarket();
                $regional_markets = $QRegion->fetchAll();

                $rm = array();
                foreach ($regional_markets as $key => $value) {
                    $rm[$value['id']] = $value['name'];
                }
                $this->view->rm_list = $rm;

                $this->_helper->viewRenderer->setRender('partials/searchname');
            } elseif ($s_assign) {
                $this->_helper->viewRenderer->setRender('partials/staff');
            } else
                $this->_helper->viewRenderer->setRender('partials/list');
        }
    }

    /* update contract function */

    public function updateContractAction() {
        $this->_helper->layout->disableLayout();
        $back_url             = $this->getRequest()->getParam('back_url');
        $this->view->back_url = $back_url;
        $ids                  = $this->getRequest()->getParam('id');
        $contract_type        = $this->getRequest()->getParam('contract_type');
        $print_time           = $this->getRequest()->getParam('print_time');
        $QArea                = new Application_Model_Area();
        $QSalarySales         = new Application_Model_SalarySales();
        $QSalaryPG            = new Application_Model_SalaryPg();
        $QLog                 = new Application_Model_StaffPrintLog();

        $this->view->areas = $QArea->fetchAll();
        $staffs            = array();
        $contract_name     = array();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        try {
            if (is_array($ids) && $ids) {

                foreach ($ids as $key => $v) {

                    $QStaff      = new Application_Model_Staff();
                    $staffRowset = $QStaff->find($v);
                    $staff       = $staffRowset->current();

                    //update ngay het han hop dong va ngay bat dau hop dong

                    if (empty($staff['contract_signed_at'])) {
                        $staff['contract_signed_at'] = $staff['joined_at'];
                    }

                    if (isset($contract_type) and $contract_type == '') {
                        $data               = array();
                        $data['print_time'] = intval($print_time);
                        $where              = array();
                        $where[]            = $QStaff->getAdapter()->quoteInto('id = ?', $staff['id']);
                        $QStaff->update($data, $where);
                    } else {
                        $start = $staff['contract_signed_at'];

                        if ($staff['print_time'] != 0) {
                            $end   = date("Y-m-d", strtotime($staff['contract_expired_at'] . "+ 1 days"));
                            $start = $end;
                        }

                        //hop dong thu viec
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_LABOUR) {
                            if (empty($end))
                                $end = $staff['contract_signed_at'];

                            if ($staff['department'] == DEPARTMENT_WARRANTY_CENTER)
                                $end = date("Y-m-d", strtotime($end . "+ 29 days"));
                            else
                                $end = date("Y-m-d", strtotime($end . "+ 59 days"));

                            $contract_name[$staff['id']] = "Thử việc";
                        }

                        //hop dong 1 nam
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_12_MONTH) {
                            if (isset($staff['title']) and My_Staff::isPgTitle($staff['title'])) {
                                $start = date("Y-m-d", strtotime($start . "+ 4 days"));
                                $end   = date("Y-m-d", strtotime($end . "+ 5 days"));
                            } else if (isset($staff['title']) and $staff['title'] == SALES_TITLE and $staff['contract_term'] == CONTRACT_TERM_SEASONAL_CHALLENGE) {
                                $start = date("Y-m-d", strtotime($start . "+ 4 days"));
                                $end   = date("Y-m-d", strtotime($end . "+ 5 days"));
                            }



                            $end                         = date("Y-m-d", strtotime($end . "+ 364 days"));
                            $contract_name[$staff['id']] = "12 Tháng";
                        }

                        //hop dong th?i v?
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_SEASONAL) {
                            $end                         = date("Y-m-d", strtotime($end . "+ 84 days"));
                            $contract_name[$staff['id']] = "Th?i v?";
                        }

                        //hop dong thoi vu thu thach cho sale
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_SEASONAL_CHALLENGE and
                                $staff['title'] == SALES_TITLE) {

                            $end                         = date("Y-m-d", strtotime($end . "+ 29 days"));
                            $contract_name[$staff['id']] = "Th?i v?";
                        }

                        //hop dong thoi vu thu thach cho pg
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_SEASONAL_CHALLENGE and
                                My_Staff::isPgTitle($staff['title'])) {
                            $start                       = date("Y-m-d", strtotime($start . "+ 4 days"));
                            $end                         = date("Y-m-d", strtotime($end . "+ 5 days"));
                            $end                         = date("Y-m-d", strtotime($end . "+ 29 days"));
                            $contract_name[$staff['id']] = "Thử việc";
                        }

                        //chua in hop dong
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_NOT_YET) {
                            $end                         = '';
                            $start                       = '';
                            $contract_name[$staff['id']] = "";
                        }

                        //hop dong 3 nam
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_36_MONTH) {
                            $end                         = date("Y-m-d", strtotime($end . "+ 1094 days"));
                            $contract_name[$staff['id']] = "36 tháng";
                        }

                        //hop dong khong thoi han
                        if (isset($contract_type) and $contract_type == CONTRACT_TERM_UNLIMITED) {
                            $contract_name[$staff['id']] = "Không xác định thời hạn";
                        }




                        $data = array(
                            'contract_signed_at'  => $start,
                            'contract_expired_at' => $end
                        );


                        if (isset($contract_type) and $contract_type)
                            $data['contract_term'] = $contract_type;

                        if (isset($contract_type) and $contract_type)
                            $data['print_time'] = 0;
                        else {
                            $data['print_time'] = intval($print_time);
                        }


                        $staff['contract_signed_at']  = $start;
                        $staff['contract_expired_at'] = $end;


                        $where   = array();
                        $where[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff['id']);

                        $QStaff->update($data, $where);


                        //luu log nhan vien

                        $ip = $this->getRequest()->getServer('REMOTE_ADDR');

                        $info = 'Update contract term type = ' . $contract_name[$staff['id']] . ' for: ' .
                                $staff['firstname'] . ' ' . $staff['lastname'] . ' <br/> Form : ' . $staff['contract_signed_at'] .
                                ' To :' . $staff['contract_expired_at'];


                        //sale team
                        if ($staff['title'] == SALES_TITLE || $staff['title'] == SALES_ACCESSORIES_TITLE) {
                            $where   = array();
                            $where[] = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                            $luong   = $QSalarySales->fetchRow($where);
                        }

                        //pg team
                        if (My_Staff::isPgTitle($staff['title'])) {
                            $where   = array();
                            $where[] = $QSalaryPG->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                            $luong   = $QSalaryPG->fetchRow($where);
                        }

                        $db          = Zend_Registry::get('db');
                        $selectTitle = $db->select()->from(array('p' => 'team'), array('name'))->where('id = ?', $staff['title']);
                        $title       = $db->fetchOne($selectTitle);

                        $QLog->insert(array(
                            'info'             => $info,
                            'user_id'          => $userStorage->id,
                            'object'           => $staff['id'],
                            'ip_address'       => $ip,
                            'time'             => date('Y-m-d H:i:s'),
                            'contract_term'    => $contract_type,
                            'log_type'         => STAFF_PRINT_LOG_UPDATE,
                            'from_date'        => $staff['contract_signed_at'],
                            'to_date'          => $staff['contract_expired_at'],
                            'title'            => $title,
                            'regional_market'  => $staff['regional_market'],
                            'base_salary'      => $luong['base_salary'] ? $luong['base_salary'] : 0,
                            'bonus_salary'     => $luong['bonus_salary'] ? $luong['bonus_salary'] : 0,
                            'allowance_1'      => $luong['allowance_1'] ? $luong['allowance_1'] : 0,
                            'allowance_2'      => $luong['allowance_2'] ? $luong['allowance_2'] : 0,
                            'allowance_3'      => $luong['allowance_3'] ? $luong['allowance_3'] : 0,
                            'probation_salary' => $luong['probation_salary'] ? $luong['probation_salary'] : 0,
                            'kpi'              => $luong['kpi'] ? $luong['kpi'] : '',
                        ));
                        $staffs[] = $staff;
                    }
                }
            }
        } catch (exception $e) {
            var_dump($e);
            exit;
        }

        echo '1';
        exit;
    }

    /* update contract function */

    public function updateContractOfficeAction() {
        $this->_helper->layout->disableLayout();
        $back_url      = $this->getRequest()->getParam('back_url');
        $ids           = $this->getRequest()->getParam('id');
        $contract_type = $this->getRequest()->getParam('contract_type');
        $print_time    = $this->getRequest()->getParam('print_time');
        $QArea         = new Application_Model_Area();
        $QSalarySales  = new Application_Model_SalarySales();
        $QSalaryPG     = new Application_Model_SalaryPg();
        $QLog          = new Application_Model_StaffPrintLog();

        $this->view->back_url = $back_url;
        $this->view->areas    = $QArea->fetchAll();
        $staffs               = array();
        $contract_name        = array();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        try {
            if (is_array($ids) && $ids) {

                foreach ($ids as $key => $v) {

                    $QStaff      = new Application_Model_Staff();
                    $staffRowset = $QStaff->find($v);
                    $staff       = $staffRowset->current();

                    //update ngay het han hop dong va ngay bat dau hop dong

                    if (!isset($staff['contract_signed_at'])) {
                        $staff['contract_signed_at'] = $staff['join_date'];
                    }
                    if (isset($contract_type) and $contract_type == '') {
                        $data               = array();
                        $data['print_time'] = intval($print_time);
                        $where              = array();
                        $where[]            = $QStaff->getAdapter()->quoteInto('id = ?', $staff['id']);
                        $QStaff->update($data, $where);
                    } else {

                        $start = $staff['contract_signed_at'];
                        if ($staff['print_time'] != 0)
                            $end   = date("Y-m-d", strtotime($staff['contract_expired_at'] . "+ 1 days"));
                        $start = $end;

                        //hop dong thu viec
                        if (isset($contract_type) and $contract_type == '2') {
                            $end                         = date("Y-m-d", strtotime($end . "+ 57 days"));
                            $contract_name[$staff['id']] = "Thử việc";
                        }

                        //hop dong 1 nam
                        if (isset($contract_type) and $contract_type == '1') {
                            if (isset($staff['is_officer']) and $staff['is_officer']) {
                                $start = date("Y-m-d", strtotime($start . "+ 4 days"));
                                $end   = date("Y-m-d", strtotime($end . "+ 5 days"));
                            }

                            $end                         = date("Y-m-d", strtotime($end . "+ 364 days"));
                            $contract_name[$staff['id']] = "12 tháng";
                        }

                        //chua in hop dong
                        if (isset($contract_type) and $contract_type == '5') {
                            $end                         = '';
                            $start                       = '';
                            $contract_name[$staff['id']] = "";
                        }

                        //hop dong 3 nam
                        if (isset($contract_type) and $contract_type == '6') {
                            $end                         = date("Y-m-d", strtotime($end . "+ 1094 days"));
                            $contract_name[$staff['id']] = "36 tháng";
                        }

                        //hop dong khong thoi han
                        if (isset($contract_type) and $contract_type == '7') {
                            $contract_name[$staff['id']] = "Không xác định thời hạn";
                        }


                        $data = array(
                            'contract_signed_at'  => $start,
                            'contract_expired_at' => $end,
                        );

                        if (isset($contract_type) and $contract_type)
                            $data['contract_term'] = $contract_type;

                        if (isset($contract_type) and $contract_type)
                            $data['print_time'] = 0;
                        else {
                            $data['print_time'] = intval($print_time);
                        }


                        $staff['contract_signed_at']  = $start;
                        $staff['contract_expired_at'] = $end;

                        $where   = array();
                        $where[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff['id']);


                        $QStaff->update($data, $where);

                        //luu log nhan vien

                        $ip = $this->getRequest()->getServer('REMOTE_ADDR');

                        $info = 'Update contract term type = ' . $contract_name[$staff['id']] . ' for: ' .
                                $staff['firstname'] . ' ' . $staff['lastname'] . ' <br/> Form : ' . $staff['contract_signed_at'] .
                                ' To :' . $staff['contract_expired_at'];

                        $QLog->insert(array(
                            'info'       => $info,
                            'user_id'    => $userStorage->id,
                            'object'     => $staff['id'],
                            'ip_address' => $ip,
                            'time'       => date('Y-m-d H:i:s'),
                        ));

                        $staffs[] = $staff;
                    }
                }
            }
        } catch (exception $e) {
            echo '-1';
            exit;
        }

        echo '1';
        exit;
    }

    public function printAction() {
        $this->_helper->layout->disableLayout();
        $back_url             = $this->getRequest()->getParam('back_url');
        $this->view->back_url = $back_url;
        $ids                  = $this->getRequest()->getParam('id');
        $contract_type        = $this->getRequest()->getParam('contract_type');
        $QArea                = new Application_Model_Area();
        $QSalarySales         = new Application_Model_SalarySales();
        $QSalaryPG            = new Application_Model_SalaryPg();
        $QLog                 = new Application_Model_StaffPrintLog();
        $QContract            = new Application_Model_ContractTerm();
        $QStaffAddress        = new Application_Model_StaffAddress();
        $QRegionalMarket      = new Application_Model_RegionalMarket();
        $areas                = $QArea->get_cache();
        $regional_markets     = $QRegionalMarket->get_cache_all();


        $contract_name = $QContract->get_cache();

        $this->view->areas = $QArea->fetchAll();
        $staffs            = array();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if (is_array($ids) && $ids) {

            foreach ($ids as $key => $v) {

                $QStaff      = new Application_Model_Staff();
                $staffRowset = $QStaff->find($v);
                $staff       = $staffRowset->current();

                //update ngay het han hop dong va ngay bat dau hop dong

                if (isset($staff['contract_signed_at']) and $staff['contract_signed_at'] == '') {
                    $staff['contract_signed_at'] = $staff['joined_at'];
                }

                if (!$staff['birth_place']) {
                    $where                = array();
                    $where[]              = $QStaffAddress->getAdapter()->quoteInto('staff_id = ? ', $staff['id']);
                    $where[]              = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', 4);
                    $staff_adress         = $QStaffAddress->fetchRow($where);
                    $address              = $staff_adress['address'] . ' , ' . $staff_adress['ward'];
                    $regional_market      = $staff_adress['district'];
                    $Area_result          = $QRegionalMarket->find($regional_market);
                    $result_set           = $Area_result->current();
                    $area                 = $result_set['parent'];
                    $regional_cache       = $QRegionalMarket->get_district_cache($area);
                    $regional_market      = $regional_cache[$regional_market];
                    $area                 = $regional_markets[$area];
                    $staff['birth_place'] = $area['name'];
                }

                if (!$staff['address']) {
                    $where        = array();
                    $where[]      = $QStaffAddress->getAdapter()->quoteInto('staff_id = ? ', $staff['id']);
                    $where[]      = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', 4);
                    $staff_adress = $QStaffAddress->fetchRow($where);

                    $address         = $staff_adress['address'] . ' , ' . $staff_adress['ward'];
                    $regional_market = $staff_adress['district'];

                    $Area_result = $QRegionalMarket->find($regional_market);
                    $result_set  = $Area_result->current();
                    $area        = $result_set['parent'];

                    $regional_cache = $QRegionalMarket->get_district_cache($area);


                    $regional_market = $regional_cache[$regional_market];


                    $area    = $regional_markets[$area];
                    if (isset($regional_market['name']))
                        $address = $address . ' , ' . $regional_market['name'];

                    if (isset($area))
                        $address = $address . ' , ' . $area['name'];

                    $staff['address'] = $address;
                }


                $start = $staff['contract_signed_at'];
                $end   = $staff['contract_expired_at'];
                $start = $end;

                // $print_time = intval($staff['print_time']) + 1;
                //$data = array('print_time' => $print_time, );
                //$where = array();
                //$where[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff['id']);
                ///$QStaff->update($data, $where);
                //luu log nhan vien

                $ip = $this->getRequest()->getServer('REMOTE_ADDR');


                //pg team
                if (My_Staff::isPgTitle($staff['title'])) {
                    $where   = array();
                    $where[] = $QSalaryPG->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                    $luong   = $QSalaryPG->fetchRow($where);
                } else {
                    $where   = array();
                    $where[] = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                    $luong   = $QSalarySales->fetchRow($where);
                }


                $info = 'Print contract term type = ' . $contract_name[$staff['contract_term']] .
                        ' <br/>for: ' . $staff['firstname'] . '  ' . $staff['lastname'] .
                        ' <br/> Form : ' . $staff['contract_signed_at'] . '<br/> To :' . $staff['contract_expired_at'];

                $QLog->insert(array(
                    'info'             => $info,
                    'user_id'          => $userStorage->id,
                    'object'           => $staff['id'],
                    'ip_address'       => $ip,
                    'time'             => date('Y-m-d H:i:s'),
                    'contract_term'    => $staff['contract_term'],
                    'log_type'         => STAFF_PRINT_LOG_PRINT,
                    'from_date'        => $staff['contract_signed_at'],
                    'to_date'          => $staff['contract_signed_at'],
                    'title'            => $staff['title'],
                    'regional_market'  => $staff['regional_market'],
                    'base_salary'      => $luong['base_salary'],
                    'bonus_salary'     => $luong['bonus_salary'],
                    'allowance_1'      => $luong['allowance_1'],
                    'allowance_2'      => $luong['allowance_2'],
                    'allowance_3'      => $luong['allowance_3'],
                    'probation_salary' => $luong['probation_salary'],
                    'kpi'              => $luong['kpi']));

                $staffs[] = $staff;
            }
        }

        $luong = array();

        foreach ($staffs as $k => $v) {
            //sale team
            //pg team
            if (My_Staff::isPgTitle($v['title'])) {
                $where1          = array();
                $where1[]        = $QSalaryPG->getAdapter()->quoteInto('province_id = ?', $v['regional_market']);
                $luong[$v['id']] = $QSalaryPG->fetchRow($where1);
            } else {
                $where           = array();
                $where[]         = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $v['regional_market']);
                $luong[$v['id']] = $QSalarySales->fetchRow($where);
            }
        }

        $this->view->contract_name = $contract_name;
        $this->view->luong         = $luong;
        $this->view->staff         = $staffs;

        $QGroup             = new Application_Model_Group();
        $this->view->groups = $QGroup->fetchAll();

        $QModel                     = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel                     = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel                  = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        $QRegionalMarket = new Application_Model_RegionalMarket();

        $this->view->regional_markets = $QRegionalMarket->get_cache();


        $QModel                  = new Application_Model_Team();
        $this->view->teams       = $QModel->fetchAll();
        $this->view->teams_cache = $QModel->get_cache();

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
    }

    public function printLogAction() {
        $object = $this->getRequest()->getParam('object');

        if ($object) {
            $QStaffLog       = new Application_Model_StaffPrintLog();
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $regional        = $QRegionalMarket->get_cache();

            $QContractTerm = new Application_Model_ContractTerm();
            $contract_term = $QContractTerm->get_cache();

            $from  = $this->getRequest()->getParam('from');
            $to    = $this->getRequest()->getParam('to');
            $page  = $this->getRequest()->getParam('page', 1);
            $limit = LIMITATION;
            $total = 0;

            $params = array(
                'object' => $object,
                'from'   => $from,
                'to'     => $to,
            );

            $log_res = $QStaffLog->fetchPagination($page, $limit, $total, $params);
            $logs    = array();

            foreach ($log_res as $k => $log) {
                $logs[] = array(
                    'print_log_id'     => $log['id'],
                    'time'             => $log['time'],
                    'user_id'          => $log['user_id'],
                    'ip'               => $log['ip_address'],
                    'info'             => $log['info'],
                    'object'           => $log['object'],
                    'base_salary'      => $log['base_salary'],
                    'bonus_salary'     => $log['bonus_salary'],
                    'allowance_1'      => $log['allowance_1'],
                    'allowance_2'      => $log['allowance_2'],
                    'allowance_3'      => $log['allowance_3'],
                    'contract_term'    => isset($contract_term[$log['contract_term']]) ? $contract_term[$log['contract_term']] :
                    '',
                    'regional_market'  => isset($regional[$log['regional_market']]) ? $regional[$log['regional_market']] :
                    '',
                    'probation_salary' => $log['probation_salary'],
                    'from_date'        => $log['from_date'],
                    'to_date'          => $log['to_date'],
                    'kpi'              => $log['kpi'],
                    'title'            => $log['title'],
                );
            }

            $n = count($logs);


            $QStaff          = new Application_Model_Staff();
            $QGroup          = new Application_Model_Group();
            $QContractTerm   = new Application_Model_ContractTerm();
            $QContractType   = new Application_Model_ContractType();
            $QRegionalMarket = new Application_Model_RegionalMarket();

            $this->view->staffs         = $QStaff->get_cache();
            $this->view->regionalMarket = $QRegionalMarket->get_cache();
            $this->view->group          = $QGroup->get_cache();
            $this->view->contractTerm   = $QContractTerm->get_cache();
            $this->view->contractType   = $QContractType->get_cache();
            $this->view->object         = $object;

            $this->view->logs   = $logs;
            $this->view->params = $params;
            $this->view->limit  = $limit;
            $this->view->total  = $total;
            $this->view->url    = HOST . 'staff/print-log' . ($params ? '?' . http_build_query
                            ($params) . '&' : '?');

            $this->view->offset = $limit * ($page - 1);

            $flashMessenger       = $this->_helper->flashMessenger;
            $messages             = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $messages;

            $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
            $this->view->messages_success = $messages_success;
        } else {
            $this->_redirect(HOST . 'staff/contract-term');
        }
    }

    public function indexAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'index.php';
    }

    public function quickEditAction() {
        $back_url             = $this->getRequest()->getParam('back_url');
        $this->view->back_url = $back_url;
        $id                   = $this->getRequest()->getParam('id');

        $QArea             = new Application_Model_Area();
        $this->view->areas = $QArea->fetchAll();

        if ($id) {
            $QStaff      = new Application_Model_Staff();
            $staffRowset = $QStaff->find($id);
            $staff       = $staffRowset->current();

            $this->view->staff = $staff;

            //get area & province
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $rowset          = $QRegionalMarket->find($staff->regional_market);

            if ($rowset) {
                $this->view->regional_market = $regional_market             = $rowset->current();
                $where                       = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);

                $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

                $rowset           = $QArea->find($regional_market['area_id']);
                $this->view->area = $rowset->current();
            }
        }

        $QGroup             = new Application_Model_Group();
        $this->view->groups = $QGroup->fetchAll();

        $QModel                     = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel                     = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel                  = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        $QModel            = new Application_Model_Team();
        $this->view->teams = $QModel->fetchAll();

        $QModel                = new Application_Model_Religion();
        $this->view->religions = $QModel->fetchAll();

        $QModel                    = new Application_Model_Nationality();
        $this->view->nationalities = $QModel->fetchAll();

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setRender('quick_edit');
        //back url
    }

    public function listBasicAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-basic.php';
    }

    public function listOffdatePurposeAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-offdate-purpose.php';
    }

    public function deleteOffdatePurposeAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $back_url = $this->getRequest()->getServer('HTTP_REFERER') ? $this->getRequest()->getServer('HTTP_REFERER') : '/staff/list-offdate-purpose';

        $id = $this->getRequest()->getParam('id');

        $flashMessenger = $this->_helper->flashMessenger;
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $QStaff         = new Application_Model_Staff();
        $QStaffTemp     = new Application_Model_StaffTemp();

        try {
            $whereStaffTemp = $QStaffTemp->getAdapter()->quoteInto('id = ?', $id);
            $QStaffTemp->delete($whereStaffTemp);

            $flashMessenger->setNamespace('success')->addMessage('Done');
        } catch (Exception $e) {
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->redirect($back_url);
    }

    public function listBasicRecordAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-basic-record.php';
    }

    public function createBasicAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'create-basic.php';
    }

    public function saveBasicAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'save-basic.php';
    }

    public function approveBasicAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'approve-basic.php';
    }

    public function viewBasicAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'view-basic.php';
    }

    public function loadTableTransferViewAction() {
        require_once 'ajax' . DIRECTORY_SEPARATOR . 'load-table-transfer-view.php';
    }

    public function viewTAction() {
        //view staff
        $userStorage              = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->user_current = $userStorage->id;
        $id                       = $this->getRequest()->getParam('id');
        // $id = $userStorage->id;
        //12- SALES ADMIN, 5-ASM, 7-HR
        /*
          if(in_array($userStorage->group_id,array(12,5,7,1)) ){
          $id = $this->getRequest()->getParam('id');
          }
         */
        $QStaff                   = new Application_Model_Staff();
        $QArea                    = new Application_Model_Area();
        $QDependentPersonStaff    = new Application_Model_DependentPersonStaff();
        $this->view->areas        = $QArea->get_cache();

        $QRegionalMarket                = new Application_Model_RegionalMarket();
        $this->view->all_province_cache = $QRegionalMarket->get_cache();
        $QOffice                        = new Application_Model_Office();
        $offices                        = $QOffice->get_all();
        $this->view->offices            = $offices;


        $QStoreStaffLog = new Application_Model_StoreStaffLog();

        $store = $QStoreStaffLog->getStore($id);

        $this->view->store = $store;

        $QWard                   = new Application_Model_Ward();
        // --get Level Staff
        $QLevelStaff             = new Application_Model_LevelStaff();
        $level_staff             = $QLevelStaff->fetchAll();
        $this->view->level_staff = $level_staff;
        if ($id) {

            $staffRowset       = $QStaff->find($id);
            $staff             = $staffRowset->current();
            $this->view->staff = $staff;

            $QCheckIn    = new Application_Model_CheckIn();
            $postmachine = $this->getRequest()->getParam('postmachine');

            $QcheckInShift = new Application_Model_CheckInShift();
            $where_shift   = array();
            $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('staff_code = ?', $staff->code);
            // $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('date IS NULL');
            $res_shift     = $QcheckInShift->fetchRow($where_shift);
            if (!empty($res_shift)) {
                $this->view->staff_shift = $res_shift;
            }

            if (!empty($postmachine)) {
                $ip_machine     = $this->getRequest()->getParam('ip_machine');
                $enroll_number  = $this->getRequest()->getParam('enroll_number');
                $status_machine = $this->getRequest()->getParam('status_machine');

                $array_data_machine = array();
                foreach ($enroll_number as $key => $value) {
                    if (!empty($value)) {
                        $data                 = array();
                        $data                 = array(
                            'enroll_number' => $value,
                            'ip_machine'    => $ip_machine[$key]
                        );
                        $data['status']       = ($status_machine[$key] == -1) ? 0 : $status_machine[$key];
                        $array_data_machine[] = $data;
                    }
                }
                $params_machine = array('data' => $array_data_machine, 'code' => $staff->code);
                $QCheckIn->insertData($params_machine);
            }

            if (!empty($staff->office_id)) {
                $QOffice      = new Application_Model_Office();
                $officeRowset = $QOffice->find($staff->office_id);
                $office       = $officeRowset->current();

                $this->view->office = $office;
            }

            // lấy district insurance
            if (!empty($staff->district)) {
                $where_district[]           = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $staff->district);
                $where_district[]           = $QRegionalMarket->getAdapter()->quoteInto('del is null or del = 0', null);
                $this->view->name_insurance = $QRegionalMarket->fetchRow($where_district, 'name_insurance');
            }

            //kiem tra xem no co bi lock k

            $QTimeStaffExpired = new Application_Model_TimeStaffExpired();
            $where             = array();
            $where[]           = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
            $where[]           = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null', null);
            $result            = $QTimeStaffExpired->fetchRow($where);

            if ($result) {

                $this->view->lock = 1;
            }

            //get area & province
            $rowset = $QRegionalMarket->find($staff->regional_market);

            if ($rowset) {
                $this->view->regional_market = $regional_market             = $rowset->current();
                $where                       = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);

                $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

                $rowset           = $QArea->find($regional_market['area_id']);
                $this->view->area = $rowset->current();
            }

            // get tags
            $QTag       = new Application_Model_Tag();
            $QTagObject = new Application_Model_TagObject();

            $where   = array();
            $where[] = $QTagObject->getAdapter()->quoteInto('object_id = ?', $id);
            $where[] = $QTagObject->getAdapter()->quoteInto('type = ?', TAG_STAFF);

            $a_tags = array();

            $tags_object = $QTagObject->fetchAll($where);
            if ($tags_object)
                foreach ($tags_object as $to) {
                    $where    = $QTag->getAdapter()->quoteInto('id = ?', $to['tag_id']);
                    $tag      = $QTag->fetchRow($where);
                    if ($tag)
                        $a_tags[] = $tag['name'];
                }

            $this->view->a_tags = $a_tags;

            // get addresses
            $QStaffAddress              = new Application_Model_StaffAddress();
            $district_cache             = $QRegionalMarket->get_district_cache();
            $this->view->district_cache = $district_cache;

            // ------------------------- get permanent address
            $where                         = array();
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Permanent);
            $permanent_address             = $QStaffAddress->fetchRow($where);
            $this->view->permanent_address = $permanent_address;

            if (isset($permanent_address['district']) && isset($district_cache[$permanent_address['district']])) {
                $this->view->permanent_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$permanent_address['district']]['parent']);
                $list_permanent_ward                     = $QWard->getWardByDistrict($permanent_address['district']);
                $this->view->list_permanent_ward         = $list_permanent_ward;
            }

            // ------------------------- get temporary address
            $where                         = array();
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Temporary);
            $temporary_address             = $QStaffAddress->fetchRow($where);
            $this->view->temporary_address = $temporary_address;

            if (isset($temporary_address['district']) && isset($district_cache[$temporary_address['district']])) {
                $this->view->temporary_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$temporary_address['district']]['parent']);
                $list_temporary_ward                     = $QWard->getWardByDistrict($temporary_address['district']);
                $this->view->list_temporary_ward         = $list_temporary_ward;
            }


            // ------------------------- get birth certificate address
            $where                     = array();
            $where[]                   = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                   = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Birth_Certificate);
            $birth_address             = $QStaffAddress->fetchRow($where);
            $this->view->birth_address = $birth_address;

            if (isset($birth_address['district']) && isset($district_cache[$birth_address['district']])) {
                $this->view->birth_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$birth_address['district']]['parent']);
                $list_birth_ward                     = $QWard->getWardByDistrict($birth_address['district']);
                $this->view->list_birth_ward         = $list_birth_ward;
            }


            // ------------------------- get ID card address
            $where                       = array();
            $where[]                     = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                     = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::ID_Card);
            $id_card_address             = $QStaffAddress->fetchRow($where);
            $this->view->id_card_address = $id_card_address;

            if (isset($id_card_address['district']) && isset($district_cache[$id_card_address['district']])) {
                $this->view->id_card_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$id_card_address['district']]['parent']);

                //danh sách ward theo district
                $list_id_card_ward             = $QWard->getWardByDistrict($id_card_address['district']);
                $this->view->list_id_card_ward = $list_id_card_ward;
                //PC::debug($list_id_card_ward);
            }

            $data_ip                   = $QCheckIn->getIpMachine();
            $data_staff_ip             = $QCheckIn->getStaffIpMachine($staff->code);
            $this->view->data_ip       = $data_ip;
            $this->view->data_staff_ip = $data_staff_ip;

            // ------------------------- Get other logs
            $QStaffEducation       = new Application_Model_StaffEducation();
            $where                 = $QStaffEducation->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->education = $QStaffEducation->fetchAll($where);

            $QStaffExperience       = new Application_Model_StaffExperience();
            $where                  = $QStaffExperience->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->experience = $QStaffExperience->fetchAll($where);

            $QStaffRelative         = new Application_Model_StaffRelative();
            $where                  = $QStaffRelative->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->relative   = $QStaffRelative->fetchAll($where);
            // ------------------------- Get transfer logs
            $QStaffLogDetail        = new Application_Model_StaffLogDetail();
            $where                  = $QStaffLogDetail->getAdapter()->quoteInto('object = ?', $id);
            $this->view->log_detail = $QStaffLogDetail->fetchAll($where, 'from_date DESC');

            // ------------------------- Get change log - need approval
            // $wf = new Application_Model_Workflow();
            // $this->view->change_log = $wf->get('staff', array($id));
            // get Transfer
            // ------------------------- Get bank
            $QStaffBank       = new Application_Model_StaffBank();
            $where            = $QStaffBank->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->bank = $QStaffBank->fetchRow($where);


            // ------------------------- Get dependent
            $QStaffDependent       = new Application_Model_StaffDependent();
            $where                 = $QStaffDependent->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->dependent = $QStaffDependent->fetchAll($where);

            $this->view->staff_id = $id;
        }

        $QCompany              = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $QGroup             = new Application_Model_Group();
        $this->view->groups = $QGroup->get_cache();

        $QModel                     = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel                     = new Application_Model_ContractTypes();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel                  = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        //get teams
        $QTeam                       = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->teamsCached                 = $QTeam->get_cache();

        $QModel                = new Application_Model_Religion();
        $this->view->religions = $QModel->fetchAll();

        $QModel                    = new Application_Model_Nationality();
        $this->view->nationalities = $QModel->fetchAll();

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all2();
        $this->view->provinces = $provinces;

        // ---- Insurance
        if (isset($staff) && $staff) {

            $QHospital        = new Application_Model_Hospital();
            $provinceHospital = 0;
            if (isset($staff['hospital_id']) AND $staff['hospital_id']) {
                $rowHospital      = $QHospital->find($staff['hospital_id'])->current();
                $provinceHospital = $rowHospital['province_id'];
            }
            $currentHospitals             = $QHospital->get_full(array('province_id' => $provinceHospital));
            $this->view->currentHospitals = $currentHospitals;
            $this->view->provinceHospital = $provinceHospital;
        }
        $QUnitCode             = new Application_Model_UnitCode();
        $unitCodes             = $QUnitCode->get_cache();
        $this->view->unitCodes = $unitCodes;

        $QStaffOffdateReason              = new Application_Model_StaffDateoffReason();
        $this->view->staff_dateoff_reason = $QStaffOffdateReason->get_cache();

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $back_url             = $this->getRequest()->getParam('back_url');
        //back url
        $this->view->back_url = $back_url ? $back_url : ($this->getRequest()->getServer
                        ('HTTP_REFERER') ? $this->getRequest()->getServer('HTTP_REFERER') : '/staff');

        $userStorage              = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->user_current = $userStorage->id;
        if (in_array($userStorage->group_id, array(ADMINISTRATOR_ID, HR_ID)) || $userStorage->id == 7) {
            $this->view->show_tab = true;
        } else {
            $this->view->show_tab = false;
        }

        $QCompanyGroup = new Application_Model_CompanyGroup();
        try {
            $this->view->LineManager        = $QCompanyGroup->getLineManager($staff);
            $this->view->Manager            = $QCompanyGroup->getManager($staff);
            $this->view->companyGroup       = $QCompanyGroup->getGroupByTitle($staff->title);
            $groups_cache                   = $QGroup->get_cache();
            $accessGroup                    = $QCompanyGroup->getCenterGroupByTitle($staff->title);
            $this->view->accessGroup        = $groups_cache[$accessGroup];
            $this->view->parentCompanyGroup = $QCompanyGroup->getGroupParentByTitle($staff->title);
        } catch (Exception $e) {
            
        }
        $QCompanyGroup = new Application_Model_CompanyGroup();
        try {
            $this->view->companyGroup       = $QCompanyGroup->getGroupByTitle($staff->title);
            $this->view->LineManager        = $QCompanyGroup->getLineManager($staff);
            $this->view->Manager            = $QCompanyGroup->getManager($staff);
            $this->view->parentCompanyGroup = $QCompanyGroup->getGroupParentByTitle($staff->title);
        } catch (Exception $e) {
            
        }

        $staff_id       = $id;
        $m_asm_contract = new Application_Model_AsmContract;
        $v_asm_contract = $m_asm_contract->getCurrentContract($staff_id);

        $this->view->v_asm_contract = $v_asm_contract;



        $staff_insurance      = $QStaff->getInsurance($id);
        $list_contract        = $QStaff->getDataListContract($id);
        $list_dependent_staff = $QDependentPersonStaff->getList($staff['code']);
        $data_bank            = $QStaff->getDataBank($staff['code']);
        $pti                  = $QStaff->getDataPti($staff['code']);

        $this->view->data_bank            = $data_bank;
        $this->view->pti                  = $pti;
        $this->view->group_id             = $userStorage->group_id;
        $this->view->list_contract        = $list_contract;
        $this->view->dem                  = count($list_contract);
        $this->view->list_dependent_staff = $list_dependent_staff;
        $this->view->staff_insurance      = $staff_insurance;
    }

    public function createAction() {
        //edit staff
        $id = $this->getRequest()->getParam('id');

        $QArea             = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $QRegionalMarket                = new Application_Model_RegionalMarket();
        $this->view->all_province_cache = $QRegionalMarket->get_cache();
        $QOffice                        = new Application_Model_Office();
        $offices                        = $QOffice->get_all();
        $this->view->offices            = $offices;


        $QStoreStaffLog = new Application_Model_StoreStaffLog();

        $store = $QStoreStaffLog->getStore($id);

        $this->view->store = $store;

        $QWard = new Application_Model_Ward();


        // --get Level Staff
        $QLevelStaff             = new Application_Model_LevelStaff();
        $level_staff             = $QLevelStaff->fetchAll();
        $this->view->level_staff = $level_staff;

        if ($id) {

            $QStaff            = new Application_Model_Staff();
            $staffRowset       = $QStaff->find($id);
            $staff             = $staffRowset->current();
            $this->view->staff = $staff;
            $QCheckIn          = new Application_Model_CheckIn();
            $postmachine       = $this->getRequest()->getParam('postmachine');

            //TUONG
            $QDependentPersonStaff            = new Application_Model_DependentPersonStaff();
            $data_bank                        = $QStaff->getDataBank($staff['code']);
            $pti                              = $QStaff->getDataPti($staff['code']);
            $list_dependent_staff             = $QDependentPersonStaff->getList($staff['code']);
            $this->view->data_bank            = $data_bank;
            $this->view->pti                  = $pti;
            $this->view->list_dependent_staff = $list_dependent_staff;

            $QStaffContract      = new Application_Model_StaffContract();
            $this->view->v_asm_contract = $QStaffContract->getCurrentContract($id);
            
            $QcheckInShift = new Application_Model_CheckInShift();
            $where_shift   = array();
            $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('staff_code = ?', $staff->code);
            // $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('date IS NULL');
            $res_shift     = $QcheckInShift->fetchRow($where_shift);
            if (!empty($res_shift)) {
                $this->view->staff_shift = $res_shift;
            }

            if (!empty($postmachine)) {
                $ip_machine     = $this->getRequest()->getParam('ip_machine');
                $enroll_number  = $this->getRequest()->getParam('enroll_number');
                $status_machine = $this->getRequest()->getParam('status_machine');

                $array_data_machine = array();
                foreach ($enroll_number as $key => $value) {
                    if (!empty($value)) {
                        $data                 = array();
                        $data                 = array(
                            'enroll_number' => $value,
                            'ip_machine'    => $ip_machine[$key]
                        );
                        $data['status']       = ($status_machine[$key] == -1) ? 0 : $status_machine[$key];
                        $array_data_machine[] = $data;
                    }
                }
                $params_machine = array('data' => $array_data_machine, 'code' => $staff->code);
                $QCheckIn->insertData($params_machine);
            }

            if (!empty($staff->office_id)) {
                $QOffice      = new Application_Model_Office();
                $officeRowset = $QOffice->find($staff->office_id);
                $office       = $officeRowset->current();

                $this->view->office = $office;
            }

            // lấy district insurance
            if (!empty($staff->district)) {
                $where_district[]           = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $staff->district);
                $where_district[]           = $QRegionalMarket->getAdapter()->quoteInto('del is null or del = 0', null);
                $this->view->name_insurance = $QRegionalMarket->fetchRow($where_district, 'name_insurance');
            } else {
                $where_district[]           = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $staff->regional_market);
                $where_district[]           = $QRegionalMarket->getAdapter()->quoteInto('default_contract_district = 1', null);
                $this->view->name_insurance = $QRegionalMarket->fetchRow($where_district, 'name_insurance');
            }

            //kiem tra xem no co bi lock k

            $QTimeStaffExpired = new Application_Model_TimeStaffExpired();
            $where             = array();
            $where[]           = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
            $where[]           = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null', null);
            $result            = $QTimeStaffExpired->fetchRow($where);

            if ($result) {

                $this->view->lock = 1;
            }

            //get area & province
            $rowset = $QRegionalMarket->find($staff->regional_market);

            if ($rowset) {
                $this->view->regional_market = $regional_market             = $rowset->current();
                $where                       = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);

                $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

                $rowset           = $QArea->find($regional_market['area_id']);
                $this->view->area = $rowset->current();
            }

            // get tags
            $QTag       = new Application_Model_Tag();
            $QTagObject = new Application_Model_TagObject();

            $where   = array();
            $where[] = $QTagObject->getAdapter()->quoteInto('object_id = ?', $id);
            $where[] = $QTagObject->getAdapter()->quoteInto('type = ?', TAG_STAFF);

            $a_tags = array();

            $tags_object = $QTagObject->fetchAll($where);
            if ($tags_object)
                foreach ($tags_object as $to) {
                    $where    = $QTag->getAdapter()->quoteInto('id = ?', $to['tag_id']);
                    $tag      = $QTag->fetchRow($where);
                    if ($tag)
                        $a_tags[] = $tag['name'];
                }

            $this->view->a_tags = $a_tags;

            // get addresses
            $QStaffAddress              = new Application_Model_StaffAddress();
            $district_cache             = $QRegionalMarket->get_district_cache();
            $this->view->district_cache = $district_cache;

            // ------------------------- get permanent address
            $where                         = array();
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Permanent);
            $permanent_address             = $QStaffAddress->fetchRow($where);
            $this->view->permanent_address = $permanent_address;

            if (isset($permanent_address['district']) && isset($district_cache[$permanent_address['district']])) {
                $this->view->permanent_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$permanent_address['district']]['parent']);
                $list_permanent_ward                     = $QWard->getWardByDistrict($permanent_address['district']);
                $this->view->list_permanent_ward         = $list_permanent_ward;
            }

            // ------------------------- get temporary address
            $where                         = array();
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                       = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Temporary);
            $temporary_address             = $QStaffAddress->fetchRow($where);
            $this->view->temporary_address = $temporary_address;

            if (isset($temporary_address['district']) && isset($district_cache[$temporary_address['district']])) {
                $this->view->temporary_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$temporary_address['district']]['parent']);
                $list_temporary_ward                     = $QWard->getWardByDistrict($temporary_address['district']);
                $this->view->list_temporary_ward         = $list_temporary_ward;
            }


            // ------------------------- get birth certificate address
            $where                     = array();
            $where[]                   = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                   = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Birth_Certificate);
            $birth_address             = $QStaffAddress->fetchRow($where);
            $this->view->birth_address = $birth_address;

            if (isset($birth_address['district']) && isset($district_cache[$birth_address['district']])) {
                $this->view->birth_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$birth_address['district']]['parent']);
                $list_birth_ward                     = $QWard->getWardByDistrict($birth_address['district']);
                $this->view->list_birth_ward         = $list_birth_ward;
            }


            // ------------------------- get ID card address
            $where                       = array();
            $where[]                     = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[]                     = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::ID_Card);
            $id_card_address             = $QStaffAddress->fetchRow($where);
            $this->view->id_card_address = $id_card_address;

            if (isset($id_card_address['district']) && isset($district_cache[$id_card_address['district']])) {
                $this->view->id_card_address_districts = $QRegionalMarket->
                        get_district_by_province_cache($district_cache[$id_card_address['district']]['parent']);

                //danh sách ward theo district
                $list_id_card_ward             = $QWard->getWardByDistrict($id_card_address['district']);
                $this->view->list_id_card_ward = $list_id_card_ward;
                //PC::debug($list_id_card_ward);
            }

            $data_ip                   = $QCheckIn->getIpMachine();
            $data_staff_ip             = $QCheckIn->getStaffIpMachine($staff->code);
            $this->view->data_ip       = $data_ip;
            $this->view->data_staff_ip = $data_staff_ip;

            // ------------------------- Get other logs
            $QStaffEducation       = new Application_Model_StaffEducation();
            $where                 = $QStaffEducation->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->education = $QStaffEducation->fetchAll($where);

            $QStaffExperience       = new Application_Model_StaffExperience();
            $where                  = $QStaffExperience->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->experience = $QStaffExperience->fetchAll($where);

            $QStaffRelative         = new Application_Model_StaffRelative();
            $where                  = $QStaffRelative->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->relative   = $QStaffRelative->fetchAll($where);
            // ------------------------- Get transfer logs
            $QStaffLogDetail        = new Application_Model_StaffLogDetail();
            $where                  = $QStaffLogDetail->getAdapter()->quoteInto('object = ?', $id);
            $this->view->log_detail = $QStaffLogDetail->fetchAll($where, 'from_date DESC');

            // ------------------------- Get change log - need approval
            // $wf = new Application_Model_Workflow();
            // $this->view->change_log = $wf->get('staff', array($id));
            // get Transfer
            // ------------------------- Get bank
            $QStaffBank       = new Application_Model_StaffBank();
            $where            = $QStaffBank->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->bank = $QStaffBank->fetchRow($where);


            // ------------------------- Get dependent
            $QStaffDependent       = new Application_Model_StaffDependent();
            $where                 = $QStaffDependent->getAdapter()->quoteInto('staff_id = ?', $id);
            $this->view->dependent = $QStaffDependent->fetchAll($where);

            $this->view->staff_id = $id;
        }

        $QCompany              = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $QGroup             = new Application_Model_Group();
        $this->view->groups = $QGroup->get_cache();

        $QModel                     = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel                     = new Application_Model_ContractTypes();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel                  = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        //get teams
        $QTeam                       = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->teamsCached                 = $QTeam->get_cache();

        $QModel                = new Application_Model_Religion();
        $this->view->religions = $QModel->fetchAll();

        $QModel                    = new Application_Model_Nationality();
        $this->view->nationalities = $QModel->fetchAll();

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all2();
        $this->view->provinces = $provinces;

        // ---- Insurance
        if (isset($staff) && $staff) {

            $QHospital        = new Application_Model_Hospital();
            $provinceHospital = 0;
            if (isset($staff['hospital_id']) AND $staff['hospital_id']) {
                $rowHospital      = $QHospital->find($staff['hospital_id'])->current();
                $provinceHospital = $rowHospital['province_id'];
            }
            $currentHospitals             = $QHospital->get_all(array('province_id' => $provinceHospital));
            $this->view->currentHospitals = $currentHospitals;
            $this->view->provinceHospital = $provinceHospital;
        }
        $QUnitCode             = new Application_Model_UnitCode();
        $unitCodes             = $QUnitCode->get_cache();
        $this->view->unitCodes = $unitCodes;

        $QStaffOffdateReason              = new Application_Model_StaffDateoffReason();
        $this->view->staff_dateoff_reason = $QStaffOffdateReason->get_cache();

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $back_url             = $this->getRequest()->getParam('back_url');
        //back url
        $this->view->back_url = $back_url ? $back_url : ($this->getRequest()->getServer
                        ('HTTP_REFERER') ? $this->getRequest()->getServer('HTTP_REFERER') : '/staff');

        $userStorage              = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->user_current = $userStorage->id;
        if (in_array($userStorage->group_id, array(ADMINISTRATOR_ID, HR_ID)) || $userStorage->id == 7) {
            $this->view->show_tab = true;
        } else {
            $this->view->show_tab = false;
        }

        $QCompanyGroup = new Application_Model_CompanyGroup();
        try {
            $this->view->LineManager        = $QCompanyGroup->getLineManager($staff);
            $this->view->Manager            = $QCompanyGroup->getManager($staff);
            $this->view->companyGroup       = $QCompanyGroup->getGroupByTitle($staff->title);
            $groups_cache                   = $QGroup->get_cache();
            $accessGroup                    = $QCompanyGroup->getCenterGroupByTitle($staff->title);
            $this->view->accessGroup        = $groups_cache[$accessGroup];
            $this->view->parentCompanyGroup = $QCompanyGroup->getGroupParentByTitle($staff->title);
        } catch (Exception $e) {
            
        }
        $QCompanyGroup = new Application_Model_CompanyGroup();
        try {
            $this->view->companyGroup       = $QCompanyGroup->getGroupByTitle($staff->title);
            $this->view->LineManager        = $QCompanyGroup->getLineManager($staff);
            $this->view->Manager            = $QCompanyGroup->getManager($staff);
            $this->view->parentCompanyGroup = $QCompanyGroup->getGroupParentByTitle($staff->title);
        } catch (Exception $e) {
            
        }

//        $staff_id       = $id;
//        $m_asm_contract = new Application_Model_AsmContract;
//        $v_asm_contract = $m_asm_contract->getCurrentContract($staff_id);
//
//
//        $this->view->v_asm_contract = $v_asm_contract;
    }

    public function saveAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'save.php';
    }

    public function saveFileContractAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'save-file-contract.php';
    }

    /*
     * In phu luc hop dong
     * */

    public function printAppendixAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'print-appendix.php';
    }

    public function jobTitleAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'job-title.php';
    }

    public function getTeamAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getRequest()->getMethod() == 'POST') {

            $departmentID = $this->getRequest()->getParam('department_id');
            $QTeam        = new Application_Model_Team();
            $whereTeam    = array();
            $whereTeam[]  = $QTeam->getAdapter()->quoteInto('parent_id = ?', $departmentID);
            $whereTeam[]  = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
            $Teams        = $QTeam->fetchAll($whereTeam);
            if ($Teams) {
                echo json_encode($Teams->toArray());
            }
        }
    }

    public function getJobTitleAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getRequest()->getMethod() == 'POST') {

            $teamID         = $this->getRequest()->getParam('team_id');
            $QTeam          = new Application_Model_Team();
            $whereJobTile   = array();
            $whereJobTile[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $teamID);
            $whereJobTile[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
            $jobTitle       = $QTeam->fetchAll($whereJobTile);
            if ($jobTitle) {
                echo json_encode($jobTitle->toArray());
            }
        }
    }

    public function delAction() {
        $id = $this->getRequest()->getParam('id');

        $staff = new Application_Model_Staff();
        $where = $staff->getAdapter()->quoteInto('id = ?', $id);
        $staff->delete($where);
        My_Request::sync_table(array('table' => 'staff', 'id' => $id, 'type' => 'delete'));

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QLog        = new Application_Model_Log();
        $ip          = $this->getRequest()->getServer('REMOTE_ADDR');
        $info        = "STAFF - Delete (" . $id . ")";
        //todo log
        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));

        $back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
    }

    public function disableAction() {
        $id = $this->getRequest()->getParam('id');

        $staff = new Application_Model_Staff();
        $where = $staff->getAdapter()->quoteInto('id = ?', $id);

        $data = array('status' => 0);
        $staff->update($data, $where);
        My_Request::sync_table(array('table' => 'staff', 'id' => $id, 'type' => 'update'));

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QLog        = new Application_Model_Log();
        $ip          = $this->getRequest()->getServer('REMOTE_ADDR');
        $info        = "STAFF - Disable (" . $id . ")";
        //todo log
        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));

        $back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
    }

    public function enableAction() {
        $id = $this->getRequest()->getParam('id');

        $staff = new Application_Model_Staff();
        $where = $staff->getAdapter()->quoteInto('id = ?', $id);

        $data = array('status' => 1);
        $staff->update($data, $where);
        My_Request::sync_table(array('table' => 'staff', 'id' => $id, 'type' => 'update'));

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QLog        = new Application_Model_Log();
        $ip          = $this->getRequest()->getServer('REMOTE_ADDR');
        $info        = "STAFF - Enable (" . $id . ")";
        //todo log
        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));

        $back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
    }

    public function searchAction() {
        $term       = $this->getRequest()->getParam('term');
        $department = $this->getRequest()->getParam('department');
        $team       = $this->getRequest()->getParam('team');
        $date       = $this->getRequest()->getParams('date');
        $where      = array();
        $QStaff     = new Application_Model_Staff();
        $where[]    = $QStaff->getAdapter()->quoteInto('CONCAT(firstname, " ",lastname) LIKE ?', '%' . $term . '%');
        if ($department)
            $where[]    = $QStaff->getAdapter()->quoteInto('department = ?', $department);

        if ($team)
            $where[] = $QStaff->getAdapter()->quoteInto('team = ?', $team);
        if ($date)
            $where[] = $QStaff->getAdapter()->quoteInto('date = ?', $date);
        $staffs  = $QStaff->fetchAll($where);

        $data = array();

        if ($staffs)
            foreach ($staffs as $staff) {
                $data[] = array(
                    'id'    => $staff->id,
                    'value' => $staff->firstname . ' ' . $staff->lastname,
                );
            }

        echo json_encode($data);
        exit;
    }

    private function _exportCsv($data) {
        ////////////////////////////////////////////////////
        /////////////////// KHỞI TẠO ĐỂ XUẤT CSV
        ////////////////////////////////////////////////////
        $db       = Zend_Registry::get('db');
        set_time_limit(0);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);
        $filename = 'Staff List - ' . date('d-m-Y H-i-s');
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        // echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
        $output   = fopen('php://output', 'w');

        ////////////////////////////////////////////////////
        /////////////////// TỔNG HỢP DỮ LIỆU
        ////////////////////////////////////////////////////

        $heads = array(
            'Code',
            'Full Name',
            // 'Last name',
            'Company',
            'Department',
            'Team',
            'Title',
            'Title Id',
            'Area',
            'Province',
            'District',
            'District code',
            'Contract type',
            'Contract signed at',
            'Contract term',
            'Contract expired at',
            'Joined at',
            'Off date',
            'Off date reason',
            'Off date reason detail',
            'Off type',
            'Gender',
            'Date of birth',
            'Level',
//            'Level new',
            'Certificate',
            'ID Card Address Street',
            'ID Card Address Ward',
            'ID Card Address District',
            'ID Card Address Province',
            'Temporary Address Street',
            'Temporary Address Ward',
            'Temporary Address District',
            'Temporary Address Province',
            'Permanent address Street',
            'Permanent address Ward',
            'Permanent address District',
            'Permanent address Province',
            'Birth place Ward',
            'Birth place District',
            'Birth place Province',
            'ID number',
            'ID place',
            'ID date',
            'Nationality',
            'Religion',
            'Phone number',
            'Email',
            'Status',
            'Note',
            'Tags',
            'Group Center',
            'Policy Group',
            'Group',
            'Office Location',
//            'Title Id',
            'Province Id',
            //TUONG
            // 'Pit_Name',
            // 'Pit_Dob',
            // 'Pit_Mst',
            // 'Pit_relative',
            // 'Pit_Frommonth',
            // 'Pit_Tomonth',
            'Bank_Number',
            'Bank_At',
            'Bank_Branch',
            'Bank_Province',
            'PTI',
                //TUONG
        );

        fputcsv($output, $heads);

        //get department
        $QDepartment      = new Application_Model_Department();
        $departments      = $QDepartment->get_cache();
        //get teams
        $QTeam            = new Application_Model_Team();
        $teams            = $QTeam->get_cache();
        $QArea            = new Application_Model_Area();
        $areas            = $QArea->get_cache();
        //get regional markets
        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache_all();
        //get contract type
        $QContractType    = new Application_Model_ContractType();
        $contract_types   = $QContractType->get_cache();
        //get contract term
        $QContractTerm    = new Application_Model_ContractTypes();
        $contract_terms   = $QContractTerm->get_cache();
        //get contract term
        $QNationality     = new Application_Model_Nationality();
        $nationalities    = $QNationality->get_cache();
        //get contract term
        $QReligion        = new Application_Model_Religion();
        $religions        = $QReligion->get_cache();
        $QCompany         = new Application_Model_Company();
        $companies        = $QCompany->get_cache();
        $QTag             = new Application_Model_Tag();
        $QTagObject       = new Application_Model_TagObject();
        $QOff             = new Application_Model_StaffDateoffReason();
        $off_cache        = $QOff->get_cache();
        $QOffice          = new Application_Model_Office();
        $office_cache     = $QOffice->get_cache();
        $QGroup           = new Application_Model_Group();
        $groups_cache     = $QGroup->get_cache();

        //Province District
        $QProvinceDistrict      = new Application_Model_ProvinceDistrict();
        $provinceDistrict_cache = $QProvinceDistrict->get_cache();
        $staff_list             = $db->query($data);
        if (!$staff_list)
            $staff_list             = array();

        $i = 1;

        ////////////////////////////////////////////////////
        /////////////////// XUẤT RA CSV
        ////////////////////////////////////////////////////
        foreach ($staff_list as $item) {
            $row = array();

            $row[] = "=\"" . $item['code'] . "\"";
            $row[] = $item['firstname'] . ' ' . $item['lastname'];
            // $row[] = $item['lastname'];
            $row[] = isset($companies[$item['company_id']]) ? $companies[$item['company_id']] : '';
            $row[] = isset($teams[$item['department']]) ? $teams[$item['department']] : '';
            $row[] = isset($teams[$item['team']]) ? $teams[$item['team']] : '';
            $row[] = isset($teams[$item['title']]) ? $teams[$item['title']] : '';
            $row[] = isset($item['title']) ? $item['title'] : '';

            $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]) && isset($regional_markets[$item['regional_market']]['area_id']) && isset($areas[$regional_markets[$item['regional_market']]['area_id']]) ? $areas[$regional_markets[$item['regional_market']]['area_id']] : '';
            $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]['name']) ? $regional_markets[$item['regional_market']]['name'] : '';
            $row[] = isset($item['district_name_insurance']) ? trim($item['district_name_insurance']) : '';
            $row[] = isset($item['district_code']) ? "=\"" . $item['district_code'] . "\"" : '';
            //$row[] = $item['is_officer'] ? 'X' : '';
            $row[] = isset($contract_types[$item['contract_type']]) ? ("=\"" . $contract_types[$item['contract_type']] . "\"") : '';
            $row[] = $item['contract_signed_at'] ? ("=\"" . date('d/m/Y', strtotime($item['contract_signed_at'])) . "\"") : '';
            $row[] = isset($contract_terms[$item['contract_term']]) ? $contract_terms[$item['contract_term']] : '';
            $row[] = $item['contract_expired_at'] ? ("=\"" . date('d/m/Y', strtotime($item['contract_expired_at'])) . "\"") : '';
            $row[] = $item['joined_at'] ? ("=\"" . date('d/m/Y', strtotime($item['joined_at'])) . "\"") : '';
            $row[] = $item['off_date'] ? ("=\"" . date('d/m/Y', strtotime($item['off_date'])) . "\"") : '';
            $row[] = isset($off_cache[$item['date_off_purpose_reason']]) ? $off_cache[$item['date_off_purpose_reason']] : '';
            $row[] = $item['date_off_purpose_detail'];
            $row[] = isset(My_Staff_Status_Off::$name[$item['off_type']]) ? My_Staff_Status_Off::$name[$item['off_type']] : '';
            $row[] = ($item['gender'] == 1 ? 'Nam' : 'Nữ');

            if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? ("=\"" . date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y") . "\"") : "";
            } else {
                $dob = $item['dob'];
            }

            $row[] = $dob;

//            $row[] = "=\"" . $item['d_level'] . "\"";
            $row[] = "=\"" . $item['level_name'] . "\"";
            $row[] = $item['d_certificate'];

            // SELECT ADDRESS
            $select        = $db->select()
                    ->from(array('s' => 'staff_address'), array(
                        'type'          => 's.address_type',
//                        'province_code' => 'IFNULL(s.province_code,r2.code_insurance)',
                        'province_code' => 's.province_code',
                        'address'       => 's.address',
                        'ward'          => 's.ward',
                        'district'      => 'r.name',
                        'province'      => 'r2.name',
                        'ward_name'     => 'w.name',
                        'ward_id',
                    ))
                    ->join(array('r' => 'regional_market'), 'r.id = s.district', array())
                    ->join(array('r2' => 'regional_market'), 'r.parent = r2.id', array())
                    ->joinLeft(array('w' => 'ward'), 'w.id = s.ward_id', array())
                    ->where('s.staff_id = ?', $item['id'])
                    ->order('s.address_type ASC');
            $arrAddress    = $db->fetchAll($select);
            $arrTmpAddress = array();
            foreach ($arrAddress as $add):
                $arrTmpAddress[$add['type']] = $add;
            endforeach;
            unset($arrAddress);
            unset($select);
            unset($add);

            if (isset($arrTmpAddress[My_Staff_Address::ID_Card])) {
                $province_code = $arrTmpAddress[My_Staff_Address::ID_Card]['province_code'];
                $row[]         = "=\"" . $arrTmpAddress[My_Staff_Address::ID_Card]['address'] . "\"";
                $row[]         = ($arrTmpAddress[My_Staff_Address::ID_Card]['ward_id']) ? $arrTmpAddress[My_Staff_Address::ID_Card]['ward_name'] : $arrTmpAddress[My_Staff_Address::ID_Card]['ward'];
                $row[]         = $arrTmpAddress[My_Staff_Address::ID_Card]['district'];
                $row[]         = $provinceDistrict_cache[$province_code];
                // $row[]         = $item['id_place_province_name'];
            } else {
                $row[] = "";
                $row[] = "";
                $row[] = "";
                $row[] = "";
            }

            if (isset($arrTmpAddress[My_Staff_Address::Temporary])) {
                $province_code = $arrTmpAddress[My_Staff_Address::Temporary]['province_code'];
                $row[]         = "=\"" . $arrTmpAddress[My_Staff_Address::Temporary]['address'] . "\"";
                $row[]         = ($arrTmpAddress[My_Staff_Address::Temporary]['ward_id']) ? $arrTmpAddress[My_Staff_Address::Temporary]['ward_name'] : $arrTmpAddress[My_Staff_Address::Temporary]['ward'];
                $row[]         = $arrTmpAddress[My_Staff_Address::Temporary]['district'];
                $row[]         = $provinceDistrict_cache[$province_code];
            } else {
                $row[] = "";
                $row[] = "";
                $row[] = "";
                $row[] = "";
            }

            if (isset($arrTmpAddress[My_Staff_Address::Permanent])) {
                $province_code = $arrTmpAddress[My_Staff_Address::Permanent]['province_code'];
                $row[]         = "=\"" . $arrTmpAddress[My_Staff_Address::Permanent]['address'] . "\"";
                $row[]         = ($arrTmpAddress[My_Staff_Address::Permanent]['ward_id']) ? $arrTmpAddress[My_Staff_Address::Permanent]['ward_name'] : $arrTmpAddress[My_Staff_Address::Permanent]['ward'];
                $row[]         = $arrTmpAddress[My_Staff_Address::Permanent]['district'];
                $row[]         = $provinceDistrict_cache[$province_code];
            } else {
                $row[] = "";
                $row[] = "";
                $row[] = "";
                $row[] = "";
            }

            if (isset($arrTmpAddress[My_Staff_Address::Birth_Certificate])) {
                $province_code = $arrTmpAddress[My_Staff_Address::Birth_Certificate]['province_code'];
                $row[]         = ($arrTmpAddress[My_Staff_Address::Birth_Certificate]['ward_id']) ? $arrTmpAddress[My_Staff_Address::Birth_Certificate]['ward_name'] : $arrTmpAddress[My_Staff_Address::Birth_Certificate]['ward'];
                $row[]         = $arrTmpAddress[My_Staff_Address::Birth_Certificate]['district'];
                $row[]         = $provinceDistrict_cache[$province_code];
            } else {
                $row[] = "";
                $row[] = "";
                $row[] = "";
            }

            unset($arrTmpAddress);
            $row[] = "=\"" . $item['ID_number'] . "\"";
            $row[] = $item['id_place_province_name'];
            $row[] = $item['ID_date'] ? ("=\"" . date('d/m/Y', strtotime($item['ID_date'])) . "\"") : '';
            $row[] = isset($nationalities[$item['nationality']]) ? $nationalities[$item['nationality']] : '';
            $row[] = isset($religions[$item['religion']]) ? $religions[$item['religion']] : '';
            $row[] = "=\"" . $item['phone_number'] . "\"";
            $row[] = $item['email'];
            $row[] = My_Staff_Status::get($item['status']);
            // print_r(str_replace("\n", " ", $item['note'])); die;
            $row[] = str_replace(array("\r\n", "\n\r", "\n", "\r"), " ", $item['note']);

            // get tags
            $where   = array();
            $where[] = $QTagObject->getAdapter()->quoteInto('type = ?', TAG_STAFF);
            $where[] = $QTagObject->getAdapter()->quoteInto('object_id = ?', $item['id']);

            $tags     = $QTagObject->fetchAll($where);
            $tags_arr = array();

            foreach ($tags as $_key => $_value)
                $tags_arr[] = $_value['tag_id'];

            if (is_array($tags_arr) && count($tags_arr))
                $where = $QTag->getAdapter()->quoteInto('id IN (?)', $tags_arr);
            else
                $where = $QTag->getAdapter()->quoteInto('1=0', 1);

            $tags     = $QTag->fetchAll($where);
            $tags_str = '';
            foreach ($tags as $_key => $_value)
                $tags_str .= $_value['name'];

            $tags_str = str_replace("\n", " ", $tags_str);
            $row[]    = $tags_str;

            $group_id = $item['access_group'];
            $row[]    = $groups_cache[$group_id];

            $row[] = $item['policy_group_name'];

            $row[] = $item['company_group_name'];

            $office_id = $item['office_id'];

            if (!empty($item['brand_shop_address'])) {
                $row[] = $item['brand_shop_address'];
            } else {
                if (!empty($office_id)) {
                    $row[] = $office_cache[$office_id];
                } else {
                    $row[] = NULL;
                }
            }
//            $row[] = $item['title'];
            $row[] = $item['regional_market'];


            //TUONG
            // $row[] = $item['name_pit'];
            // $row[] = $item['dob_pit'];
            // $row[] = $item['mst_pit'];
            // $row[] = $item['relative'];
            // $row[] = $item['from_month'];
            // $row[] = $item['to_month'];
            $row[] = "=\"" . $item['bank_number'] . "\"";
            $row[] = $item['at_bank'];
            $row[] = $item['branch'];
            $row[] = $item['province_city'];
            $row[] = "=\"" . $item['tax'] . "\"";

            //TUONG

            unset($tags);
            unset($tags_str);
            unset($tags_arr);
            unset($where);

            fputcsv($output, $row);
            unset($item);
            unset($row);
        }

        exit;
    }

    private function _exportCsvASM($data) {
        $db       = Zend_Registry::get('db');
        set_time_limit(0);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);
        $filename = 'Staff List for ASM - ' . date('d-m-Y H-i-s');
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        // echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
        $output   = fopen('php://output', 'w');

        ////////////////////////////////////////////////////
        /////////////////// TỔNG HỢP DỮ LIỆU ///////////////
        ////////////////////////////////////////////////////

        $heads = array(
            'Code',
            'First name',
            'Last name',
            'Company',
            'Department',
            'Team',
            'Title',
            'Area',
            'Province',
            //  'District',
            'ID number',
            'ID place',
            'ID date',
            'Joined at',
            'Date of birth',
            'Off date',
            'Off type',
            'Gender',
            // 'Certificate',
            // 'ID',
            'Office Location',
                // 'Contract signed at',
                // 'Contract term',
                // 'Contract expired at',
        );

        fputcsv($output, $heads);

        //get department
        $QDepartment = new Application_Model_Department();
        $departments = $QDepartment->get_cache();

        //get teams
        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        //get titles
        /* $QTitle         = new Application_Model_Title();
          $titles           = $QTitle->get_cache(); */

        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();

        //get regional markets
        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache_all();

        //get contract type
        $QContractType  = new Application_Model_ContractType();
        $contract_types = $QContractType->get_cache();

        $QContractTerm  = new Application_Model_ContractTerm();
        $contract_terms = $QContractTerm->get_cache();

        //get contract term
        $QNationality  = new Application_Model_Nationality();
        $nationalities = $QNationality->get_cache();

        //get contract term
        $QReligion = new Application_Model_Religion();
        $religions = $QReligion->get_cache();

        $QCompany  = new Application_Model_Company();
        $companies = $QCompany->get_cache();

        $QOffice      = new Application_Model_Office();
        $office_cache = $QOffice->get_cache();

        $staff_list = $db->query($data);

        if (!$staff_list)
            $staff_list = array();

        $i = 1;


        foreach ($staff_list as $item) {
            $row = array();

            $row[] = "=\"" . $item['code'] . "\"";
            $row[] = $item['firstname'];
            $row[] = $item['lastname'];
            $row[] = isset($companies[$item['company_id']]) ? $companies[$item['company_id']] : '';
            $row[] = isset($teams[$item['department']]) ? $teams[$item['department']] : '';
            $row[] = isset($teams[$item['team']]) ? $teams[$item['team']] : '';
            $row[] = isset($teams[$item['title']]) ? $teams[$item['title']] : '';

            $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]) && isset($regional_markets[$item['regional_market']]['area_id']) && isset($areas[$regional_markets[$item['regional_market']]['area_id']]) ? $areas[$regional_markets[$item['regional_market']]['area_id']] : '';
            $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]['name']) ? $regional_markets[$item['regional_market']]['name'] : '';
            //  $row[] = isset($item['district_name_insurance']) ? trim($item['district_name_insurance']) : '';
            $row[] = "=\"" . $item['ID_number'] . "\"";
            $row[] = $item['id_place_province_name'];
            $row[] = $item['ID_date'] ? ("=\"" . date('d/m/Y', strtotime($item['ID_date'])) . "\"") : '';
            $row[] = $item['joined_at'] ? ("=\"" . date('d/m/Y', strtotime($item['joined_at'])) . "\"") : '';

            if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? ("=\"" . date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y") . "\"") : "";
            } else {
                $dob = $item['dob'];
            }

            $row[] = $dob;
            $row[] = $item['off_date'] ? ("=\"" . date('d/m/Y', strtotime($item['off_date'])) . "\"") : '';
            $row[] = isset(My_Staff_Status_Off::$name[$item['off_type']]) ? My_Staff_Status_Off::$name[$item['off_type']] : '';
            $row[] = ($item['gender'] == 1 ? 'Nam' : 'Nữ');

            if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? ("=\"" . date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y") . "\"") : "";
            } else {
                $dob = $item['dob'];
            }

            // $row[] = $dob;

            $office_id = $item['office_id'];

            if (!empty($item['brand_shop_address'])) {
                $row[] = $item['brand_shop_address'];
            } else {
                if (!empty($office_id)) {
                    $row[] = $office_cache[$office_id];
                } else {
                    $row[] = NULL;
                }
            }



            // $row[] = $item['d_level'];
            // $row[] = $item['d_certificate'];
            /*
              $row[] = "=\"" . $item['ID_number'] . "\"";
              $row[] = $item['contract_signed_at'] ? ("=\"" . date('d/m/Y', strtotime($item['contract_signed_at'])) . "\"") : '';
              $row[] = isset($contract_terms[$item['contract_term']]) ? $contract_terms[$item['contract_term']] : '';
              $row[] = $item['contract_expired_at'] ? ("=\"" . date('d/m/Y', strtotime($item['contract_expired_at'])) . "\"") : '';
             */
            //  $row[] = "=\"".$item['phone_number']."\"";
            // $row[] = $item['email'];

            fputcsv($output, $row);
            unset($item);
            unset($row);
        }

        exit;
    }

    private function _exportXlsxASM($data) {
        $db = Zend_Registry::get('db');

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        set_time_limit(0);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);
        $filename = 'Staff List for ASM - ' . date('d-m-Y H-i-s');



        ////////////////////////////////////////////////////
        /////////////////// TỔNG HỢP DỮ LIỆU ///////////////
        ////////////////////////////////////////////////////

        $heads = array(
            'A' => 'Code',
            'B' => 'First name',
            'C' => 'Last name',
            'D' => 'Company',
            'E' => 'Department',
            'F' => 'Team',
            'G' => 'Title',
            'H' => 'Area',
            'I' => 'Province',
            'J' => 'Joined at',
            'K' => 'Off date',
            'L' => 'Off type',
            'M' => 'Gender',
            'N' => 'Certificate',
            'O' => 'ID'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach ($heads as $key => $value)
            $sheet->setCellValue($key . '1', $value);

        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(12);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->getColumnDimension('F')->setWidth(15);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(15);
        $sheet->getColumnDimension('I')->setWidth(20);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(10);
        $sheet->getColumnDimension('M')->setWidth(15);
        $sheet->getColumnDimension('N')->setWidth(15);
        $sheet->getColumnDimension('O')->setWidth(10);

        //get department
        $QDepartment = new Application_Model_Department();
        $departments = $QDepartment->get_cache();

        //get teams
        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        //get titles
        /* $QTitle         = new Application_Model_Title();
          $titles           = $QTitle->get_cache(); */

        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();

        //get regional markets
        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache_all();

        //get contract type
        $QContractType  = new Application_Model_ContractType();
        $contract_types = $QContractType->get_cache();



        //get contract term
        $QNationality  = new Application_Model_Nationality();
        $nationalities = $QNationality->get_cache();

        //get contract term
        $QReligion = new Application_Model_Religion();
        $religions = $QReligion->get_cache();

        $QCompany  = new Application_Model_Company();
        $companies = $QCompany->get_cache();


        $staff_list = $db->query($data);

        if (!$staff_list)
            $staff_list = array();

        $i = 1;


        foreach ($staff_list as $main_key => $item) {
            $sheet->setCellValue('A' . ($main_key + 2), $item['code']);
            $sheet->setCellValue('B' . ($main_key + 2), $row[] = $item['firstname']);
            $sheet->setCellValue('C' . ($main_key + 2), $item['lastname']);
            $sheet->setCellValue('D' . ($main_key + 2), isset($companies[$item['company_id']]) ? $companies[$item['company_id']] : '');
            $sheet->setCellValue('E' . ($main_key + 2), isset($teams[$item['department']]) ? $teams[$item['department']] : '');
            $sheet->setCellValue('F' . ($main_key + 2), isset($teams[$item['team']]) ? $teams[$item['team']] : '');
            $sheet->setCellValue('G' . ($main_key + 2), isset($teams[$item['title']]) ? $teams[$item['title']] : '');

            $sheet->setCellValue('H' . ($main_key + 2), isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]) && isset($regional_markets[$item['regional_market']]['area_id']) && isset($areas[$regional_markets[$item['regional_market']]['area_id']]) ? $areas[$regional_markets[$item['regional_market']]['area_id']] : '');
            $sheet->setCellValue('I' . ($main_key + 2), isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]['name']) ? $regional_markets[$item['regional_market']]['name'] : '');

            $sheet->setCellValue('J' . ($main_key + 2), $item['joined_at'] ? ("=\"" . date('d/m/Y', strtotime($item['joined_at'])) . "\"") : '');
            $sheet->setCellValue('K' . ($main_key + 2), $item['off_date'] ? ("=\"" . date('d/m/Y', strtotime($item['off_date'])) . "\"") : '');
            $sheet->setCellValue('L' . ($main_key + 2), isset(My_Staff_Status_Off::$name[$item['off_type']]) ? My_Staff_Status_Off::$name[$item['off_type']] : '');
            $sheet->setCellValue('M' . ($main_key + 2), ($item['gender'] == 1 ? 'Nam' : 'Nữ'));

            if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? ("=\"" . date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y") . "\"") : "";
            } else {
                $dob = $item['dob'];
            }

            $sheet->setCellValue('N' . ($main_key + 2), $dob);

            $sheet->setCellValue('O' . ($main_key + 2), $item['id']);
        }

        $filename  = 'Staff Photo - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');


        exit;
    }

    private function _formatDate($date) {
        if (!$date)
            return null;

        $date = trim($date);

        $temp         = explode('/', $date);
        $formatedDate = (isset($temp[2]) ? $temp[2] : '0000') . '-' . (isset($temp[1]) ?
                $temp[1] : '01') . '-' . (isset($temp[0]) ? $temp[0] : '01');
        return $formatedDate;
    }

    public function analyticsAction() {
        $region = $this->getRequest()->getParam('regional_market', '');
        $QStaff = new Application_Model_Staff();

        if ($region != '') { // hiÃƒÂ¡Ã‚Â»Ã†â€™n thÃƒÂ¡Ã‚Â»Ã¢â‚¬Â¹ theo vÃƒÆ’Ã‚Â¹ng
            $limit = 20;
            $total = 0;
            $page  = $this->getRequest()->getParam('page', 1);

            $department              = $this->getRequest()->getParam('department', -1);
            $sort                    = $this->getRequest()->getParam('sort', '');
            $desc                    = $this->getRequest()->getParam('desc', 1);
            $this->view->desc        = $desc;
            $this->view->current_col = $sort;

            $params = array(
                'off'             => 1,
                'regional_market' => $region,
                'department'      => $department,
                'sort'            => $sort,
                'desc'            => $desc,
            );

            $QStaff = new Application_Model_Staff();

            $QTitle             = new Application_Model_Title();
            $this->view->titles = $QTitle->get_cache();

            //get regional market
            $QRegion             = new Application_Model_RegionalMarket();
            $regions             = $QRegion->get_cache();
            $this->view->regions = $regions;

            $staffs = $QStaff->fetchPagination($page, $limit, $total, $params);

            $this->view->staffs = $staffs;
            $this->view->region = $region;
            $this->view->limit  = $limit;
            $this->view->total  = $total;
            $this->view->url    = HOST . 'staff/analytics' . ($params ? '?' . http_build_query
                            ($params) . '&' : '?');
            $this->view->offset = $limit * ($page - 1);

            // thÃƒÂ¡Ã‚Â»Ã¢â‚¬Ëœng kÃƒÆ’Ã‚Âª theo phÃƒÆ’Ã‚Â²ng ban
            $data_by_region = $QStaff->analytics_by_region($region);
            $new_data       = array();
            foreach ($data_by_region as $key => $value) {
                $tmp = array();

                $tmp[] = $departments[$value['department']];
                $tmp[] = intval($value['department']);
                $tmp[] = intval($value['qty']);

                $new_data[] = $tmp;
            }

            if ($department != -1) {
                $this->view->department = $department;
            }
            $this->view->data = json_encode($new_data);

            if ($this->getRequest()->isXmlHttpRequest()) {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setRender('partials/analytics');
            } else {
                $this->_helper->viewRenderer->setRender('analytics-region');
            }
        } else { // HiÃƒÂ¡Ã‚Â»Ã†â€™n thÃƒÂ¡Ã‚Â»Ã¢â‚¬Â¹ tÃƒÂ¡Ã‚Â»Ã¢â‚¬Â¢ng thÃƒÂ¡Ã‚Â»Ã†â€™
            $data  = $QStaff->analytics();
            $data2 = $QStaff->analytics_by_area();

            //get regional markets
            $QRegionalMarket  = new Application_Model_RegionalMarket();
            $regional_markets = $QRegionalMarket->get_cache();

            $QArea = new Application_Model_Area();
            $areas = $QArea->get_cache();

            $this->view->regional_markets = $regional_markets;
            $this->view->areas            = $areas;

            $sum      = 0;
            $new_data = array();
            foreach ($data as $key => $value) {
                $tmp = array();

                $tmp[] = isset($regional_markets[$value['regional_market']]) ? $regional_markets[$value['regional_market']] :
                        'UNDEFINED';
                $tmp[] = intval($value['regional_market']);
                $tmp[] = intval($value['qty']);

                $sum        += $value['qty'];
                $new_data[] = $tmp;
            }

            $new_data2 = array();
            foreach ($data2 as $key => $value) {
                $tmp = array();

                $tmp[] = isset($areas[$value['id']]) ? trim($areas[$value['id']]) : 'UNDEFINED';
                $tmp[] = intval($value['id']);
                $tmp[] = intval($value['qty']);

                $new_data2[] = $tmp;
            }

            $this->view->sum   = $sum;
            $this->view->data  = json_encode($new_data);
            $this->view->data2 = json_encode($new_data2);
        }
    }

    public function analyticsByAreaAction() {
        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();

        $QTeam = new Application_Model_Team();

        $where = $QTeam->getAdapter()->quoteInto('parent_id IN (?)', (explode(',', implode(',', $this->list_teams))));

        $teams      = $QTeam->fetchAll($where);
        $team_arr   = array();
        $list_title = array();

        foreach ($teams as $key => $value) {
            $list_title[]           = $value['id'];
            $team_arr[$value['id']] = $value['name'];
        }

        $this->list_titles = $list_title;

        $this->view->areas = $areas;
        $this->view->teams = $team_arr;

        $staffs = array();

        foreach ($areas as $k => $area) {
            $staffs[$k] = $this->get_sales_staff_area($k);
        }

        $out = array();

        $tmp   = array();
        $tmp[] = "Area";
        foreach ($this->list_titles as $key => $value) {
            if (!isset($team_arr[$value])) {
                continue;
            }
            $tmp[] = $team_arr[$value];
        }
        $out[] = $tmp;

        foreach ($areas as $key => $area) {
            $tmp   = array();
            $tmp[] = $area;

            foreach ($this->list_titles as $team) {
                $tmp[] = isset($staffs[$key][$team]) ? intval($staffs[$key][$team]) : 0;
            }

            $out[] = $tmp;
        }

        $this->view->staffs     = $staffs;
        $this->view->out        = json_encode($out);
        $this->view->list_teams = $this->list_titles;
    }

    private function contractAdd($staff_id, $joined_at) {
        $QStaff        = new Application_Model_Staff();
        $QSalaryPG     = new Application_Model_SalaryPg();
        $QSalarySales  = new Application_Model_SalarySales();
        $QSalaryLog    = new Application_Model_SalaryLog();
        $QContractTerm = new Application_Model_ContractTerm();
        $QAsmContract  = new Application_Model_AsmContract();
        $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
        $staff_rowset  = $QStaff->find($staff_id);
        $staff         = $staff_rowset->current();
        $title         = $staff['title'];
        $data          = array();

        $contract_term_cache = $QContractTerm->get_cache_all();
        try {

            if (My_Staff::isPgTitle($title)) {
                $data['contract_term']       = CONTRACT_TERM_SEASONAL;
                $data['contract_signed_at']  = $joined_at;
                $data['contract_expired_at'] = My_Date::date_add(date('Y-m-d'), $contract_term_cache[CONTRACT_TERM_SEASONAL]['addday']);
            } elseif ($title == PB_SALES_TITLE) {
                $data['contract_term']       = CONTRACT_TERM_SEASONAL_PB_SALE;
                $data['contract_signed_at']  = $joined_at;
                $data['contract_expired_at'] = My_Date::date_add(date('Y-m-d'), $contract_term_cache[CONTRACT_TERM_SEASONAL_PB_SALE]['addday']);
            } elseif (in_array($title, array(231, 288, 325, 329))) {
                $data['contract_term']       = 15;
                $data['contract_signed_at']  = $staff['joined_at'];
                $data['contract_expired_at'] = My_Date::date_add(date('Y-m-d'), $contract_term_cache[15]['addday']);
            } else {
                $data['contract_term']      = CONTRACT_TERM_LABOUR;
                $data['contract_signed_at'] = $joined_at;
                if ($title == NHAN_VIEN_KIEM_HANG_TITLE) {
                    $data['contract_expired_at'] = My_Date::date_add(date('Y-m-d'), 29);
                } else {
                    $data['contract_expired_at'] = date('Y-m-d', strtotime('+59 days', strtotime($joined_at)));
                }
            }
            //luu log contract
            $where   = array();
            $where[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $QStaff->update($data, $where);

            if ($staff['title'] == SALES_TITLE || $staff['title'] == SALES_ACCESSORIES_TITLE) {
                $where   = array();
                $where[] = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                $luong   = $QSalarySales->fetchRow($where);
            }

            //pg team
            if (My_Staff::isPgTitle($staff['title'])) {
                $where   = array();
                $where[] = $QSalaryPG->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                $luong   = $QSalaryPG->fetchRow($where);
            }

            if ($staff['title'] == PB_SALES_TITLE) {
                $luong = $QSalaryLog->getSalaryLogTitle($staff['title'], $data['contract_signed_at'], $staff['regional_market']);
            }

            //Thêm vào bảng hợp đồng

            $data_contract = array(
                'company_id'      => $staff['company_id'],
                'staff_id'        => $staff['id'],
                'from_date'       => $data['contract_signed_at'],
                'to_date'         => $data['contract_expired_at'],
                'title'           => $staff['title'],
                'regional_market' => $staff['regional_market'],
                'new_contract'    => $data['contract_term'],
                'salary'          => (isset($luong['base_salary']) and $luong['base_salary']) ? $luong['base_salary'] : 0,
                'print_type'      => 1,
                'created_at'      => date('Y-m-d H:i:s'),
                'created_by'      => $userStorage->id
            );

            $QAsmContract->save($data_contract);

            //successful
            return array('code' => 1, 'message' => 'success');
        } catch (exception $e) {
            return array('code' => -1, 'message' => $e->getMessage());
        }
    }

    public function analyticsGeneralAction() {
        $staffs = $this->get_staff_general();
        $out    = array();
        $tmp    = array();
        $tmp[]  = "Department";
        $tmp[]  = "Staff";
        $out[]  = $tmp;
        foreach ($staffs as $key => $v) {
            $tmp   = array();
            $tmp[] = $v['department_name'];
            $tmp[] = intval($v['total']);
            $out[] = $tmp;
        }

        $this->view->out = json_encode($out);

        $staff      = array();
        $department = array();
        $sum        = 0;

        foreach ($staffs as $k => $v) {
            $staff[]      = $v['total'];
            $sum          += isset($v['total']) ? $v['total'] : 0;
            $department[] = $v['department_name'];
        }

        $tmp = array($department, $staff);

        $this->view->staffs = $tmp;
        $this->view->sum    = $sum;
    }

    public function checkAction() {

        $id    = $this->getRequest()->getParam('id');
        $code  = $this->getRequest()->getParam('code');
        $email = $this->getRequest()->getParam('email');

        $where  = array();
        $QStaff = new Application_Model_Staff();

        if ($code)
            $where[] = $QStaff->getAdapter()->quoteInto('code = ?', $code);

        if ($email)
            $where[] = $QStaff->getAdapter()->quoteInto('email = ?', $email);

        if (sizeof($where) == 0) {
            echo "-1";
            exit;
        }

        /* $where[] = $QStaff->getAdapter()->quoteInto('off_date IS NULL ', null); */

        $where[] = $QStaff->getAdapter()->quoteInto('id != ?', intval($id));

        $staffs = $QStaff->fetchAll($where);

        if (isset($staffs[0]))
            echo '1';
        else
            echo '0';

        exit;
    }

    public function loadProvinceAction() {
        $area_id         = $this->getRequest()->getParam('area_id');
        $where           = array();
        $QRegionalMarket = new Application_Model_RegionalMarket();
        if (is_array($area_id) && $area_id) {
            $where[] = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
            $where[] = $QRegionalMarket->getAdapter()->quoteInto('del <> 1', NULL);
        } else {
            $where[] = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
            $where[] = $QRegionalMarket->getAdapter()->quoteInto('del <> 1', NULL);
        }

        echo json_encode($QRegionalMarket->fetchAll($where, 'name')->toArray());
        exit;
    }

    public function loadSalesTeamAction() {
        $district        = $this->getRequest()->getParam('district');
        $regional_market = $this->getRequest()->getParam('regional_market');

        $QStore = new Application_Model_Store();
        /*
          $where = array();
          if (is_array($district) && count($district) > 0)
          $where[] = $QStore->getAdapter()->quoteInto('district IN (?)', $district);
          else
          $where[] = $QStore->getAdapter()->quoteInto('district = ?', $district);

          $where[] = $QStore->getAdapter()->quoteInto('(del IS NULL OR del = 0)', 1);
          $stores = $QStore->fetchAll($where, 'name');
          echo json_encode(array('stores' => $stores->toArray()));
         */

        $db     = Zend_Registry::get('db');
        $select = $db->select()->from(array('a' => 'store'), array('a.*'))
                ->joinLeft(array('b' => 'store_area_chanel'), 'b.store_id = a.id', array());

        if (is_array($district) && count($district) > 0) {
            $select->where('a.district IN (?)', $district);
        } else {
            $select->where('a.district = ?', $district);
        }

        if (isset($regional_market)) {
            if (is_array($regional_market) and count($regional_market)) {
                
            }
        }
        $stores = $db->fetchAll($select);
        echo json_encode(array('stores' => $stores));

        exit;
    }

    public function assignAction() {
        $id              = $this->getRequest()->getParam('id', 0);
        $regional_market = $this->getRequest()->getParam('regional_market', 0);

        if ($id > 0 && $regional_market > 0) {
            $staff = new Application_Model_Staff();
            $where = $staff->getAdapter()->quoteInto('id = ?', $id);
            $data  = array('regional_market' => $regional_market);
            $n     = $staff->update($data, $where);

            if (!empty($n) && $n > 0) {
                echo json_encode(array('result' => '1'));
                exit;
            }
        }
        echo json_encode(array('result' => '0'));
        exit;
    }

    public function unassignAction() {
        $id = $this->getRequest()->getParam('id', 0);

        if ($id > 0) {
            $staff = new Application_Model_Staff();
            $where = $staff->getAdapter()->quoteInto('id = ?', $id);
            $data  = array('regional_market' => null);
            $n     = $staff->update($data, $where);
        }

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;

        $this->_redirect(HOST . 'manage/assign');
    }

    public function logAction() {
        $object = $this->getRequest()->getParam('object');

        if ($object) {
            $QStaffLog = new Application_Model_StaffLog();

            $from  = $this->getRequest()->getParam('from');
            $to    = $this->getRequest()->getParam('to');
            $page  = $this->getRequest()->getParam('page', 1);
            $limit = LIMITATION;
            $total = 0;

            $params = array(
                'object' => $object,
                'from'   => $from,
                'to'     => $to,
            );

            $log_res = $QStaffLog->fetchPagination($page, $limit, $total, $params);

            if ($_GET['hien'] == 1) {
                $limit   = 1000;
                $log_res = $QStaffLog->fetchPagination($page, $limit, $total, $params);

                foreach ($log_res as $key => $value) {
                    $before = unserialize($value['before']);
                    $after  = unserialize($value['after']);

                    $diff = array_diff_assoc($before, $after);

                    if ($diff['ID_number'] AND $value['type'] == 2) {
                        $QLessonScores = new Application_Model_LessonScores();
                        $where__       = $QLessonScores->getAdapter()->quoteInto('staff_cmnd = ?', $before['ID_number']);
                        $QLessonScores->update(['staff_cmnd' => $after['ID_number']], $where__);
                    }
                }
            }


            $logs = array();

            foreach ($log_res as $k => $log) {
                $logs[] = array(
                    'time'    => $log['time'],
                    'user_id' => $log['user_id'],
                    'ip'      => $log['ip_address'],
                    'before'  => unserialize($log['before']),
                    'after'   => unserialize($log['after']),
                    'diffs'   => array(),
                    'type'    => $log['type'],
                );
            }

            $n = count($logs);

            for ($i = 0; $i < $n; $i++) {
                $logs[$i]['diffs'] = array_diff_assoc($logs[$i]['after'], $logs[$i]['before']);
            }

            $QStaff          = new Application_Model_Staff();
            $QReligion       = new Application_Model_Religion();
            $QDepartment     = new Application_Model_Department();
            $QTeam           = new Application_Model_Team();
            $QNationality    = new Application_Model_Nationality();
            $QArea           = new Application_Model_Area();
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $QGroup          = new Application_Model_Group();
            $QContractTerm   = new Application_Model_ContractTerm();
            $QContractType   = new Application_Model_ContractType();
            $QProvince       = new Application_Model_Province();
            $QCompany        = new Application_Model_Company();

            $this->view->companys       = $QCompany->get_cache();
            $this->view->staffs         = $QStaff->get_cache();
            $this->view->religion       = $QReligion->get_cache();
            $this->view->department     = $QDepartment->get_cache();
            $this->view->team           = $QTeam->get_cache();
            $this->view->nationality    = $QNationality->get_cache();
            $this->view->area           = $QArea->get_cache();
            $this->view->regionalMarket = $QRegionalMarket->get_cache();
            $this->view->group          = $QGroup->get_cache();
            $this->view->contractTerm   = $QContractTerm->get_cache();
            $this->view->contractType   = $QContractType->get_cache();
            $this->view->provinces      = $QProvince->get_all2();
            $this->view->labels         = array(
                'code'                         => 'Code',
                'contract_type'                => 'Contract Type',
                'contract_signed_at'           => 'Contract Signed At',
                'contract_term'                => 'Contract Term',
                'contract_expired_at'          => 'Contract Expired At',
                'company'                      => 'Company',
                'department'                   => 'Department',
                'team'                         => 'Team',
                'firstname'                    => 'Firstname',
                'lastname'                     => 'Lastname',
                'title'                        => 'Title',
                'joined_at'                    => 'Joined At',
                'off_date'                     => 'Off Date',
                'gender'                       => 'Gender',
                'dob'                          => 'DOB',
                'certificate'                  => 'Certificate',
                'level'                        => 'Level',
                'temporary_address'            => 'Temporary Address',
                'address'                      => 'Address',
                'regional_market'              => 'Regional Market',
                'district'                     => 'District',
                'office'                       => 'Office',
                'permanent_address'            => 'Permanent Address',
                'birth_place'                  => 'Birth Place',
                'native_place'                 => 'Native Place',
                'ID_number'                    => 'ID Number',
                'ID_place'                     => 'ID Place',
                'ID_date'                      => 'ID Date',
                'nationality'                  => 'Nationality',
                'religion'                     => 'Religion',
                'phone_number'                 => 'Phone Number',
                'note'                         => 'Note',
                'additional_info'              => 'Additional Info',
                'email'                        => 'Email',
                'group_id'                     => 'Group',
                'social_insurance_time'        => 'Social Insurance Time',
                'social_insurance_number'      => 'Social Insurance Number',
                'personal_tax'                 => 'Personal Tax',
                'family_allowances_registered' => 'Family Allowances Registered',
                'created_at'                   => 'Created At',
                'created_by'                   => 'Created By',
                'photo'                        => 'Photo',
                'last_login'                   => 'Last Login',
                'is_officer'                   => 'Is Head Office',
                'id_place_province'            => 'ID Place Province'
            );

            $this->view->logs   = $logs;
            $this->view->params = $params;
            $this->view->limit  = $limit;
            $this->view->total  = $total;
            $this->view->url    = HOST . 'staff/log' . ($params ? '?' . http_build_query($params) .
                    '&' : '?');

            $this->view->offset = $limit * ($page - 1);
        } else {
            $this->_redirect(HOST . 'staff');
        }
    }

    public function allLogAction() {
        $id        = $this->getRequest()->getParam('id');
        $QStaffLog = new Application_Model_StaffLog();
        $QStaff    = new Application_Model_Staff();

        $this->view->staffs = $QStaff->get_cache();

        // hiÃƒÂ¡Ã‚Â»Ã†â€™n thÃƒÂ¡Ã‚Â»Ã¢â‚¬Â¹ chi tiÃƒÂ¡Ã‚ÂºÃ‚Â¿t 1 log
        if ($id) {
            $log = $QStaffLog->find($id);
            $log = $log->current();

            if ($log) {
                $this->view->log = array(
                    'id'      => $log['id'],
                    'before'  => unserialize($log['before']),
                    'after'   => unserialize($log['after']),
                    'ip'      => $log['ip'],
                    'time'    => $log['time'],
                    'user_id' => $log['user_id'],
                    'object'  => $log['object'],
                    'type'    => $log['type'],
                );
            } else {
                $this->_redirect(HOST . 'staff/all-log');
            }

            // hiÃƒÂ¡Ã‚Â»Ã†â€™n thÃƒÂ¡Ã‚Â»Ã¢â‚¬Â¹ danh sÃƒÆ’Ã‚Â¡ch
        } else {

            $from  = $this->getRequest()->getParam('from');
            $to    = $this->getRequest()->getParam('to');
            $page  = $this->getRequest()->getParam('page', 1);
            $limit = 1;
            $total = 0;

            $params = array(
                'id'   => $id,
                'from' => $from,
                'to'   => $to,
            );

            $log_res = $QStaffLog->fetchPagination($page, $limit, $total, $params);

            $this->view->logs   = $logs;
            $this->view->params = $params;
            $this->view->limit  = $limit;
            $this->view->total  = $total;
            $this->view->url    = HOST . 'staff/all-log' . ($params ? '?' . http_build_query($params) .
                    '&' : '?');

            $this->view->offset = $limit * ($page - 1);
        }
    }

    public function photoAction() {
        
    }

    public function photoLoadAction() {
        $this->_helper->layout->disableLayout();

        $QStaff          = new Application_Model_Staff();
        $QRegionalMarket = new Application_Model_RegionalMarket();
        $page            = $this->getRequest()->getParam('page', 1);

        $limit = LIMITATION * 2;
        $total = 0;

        $params = array(
            'page'      => $page,
            'has_photo' => 1,
        );


        $this->view->staffs  = $QStaff->fetchPagination($page, $limit, $total, $params);
        $this->view->regions = $QRegionalMarket->get_cache();
    }

    public function companyAction() {
        $page  = $this->getRequest()->getParam('page', 1);
        $name  = $this->getRequest()->getParam('name');
        $limit = LIMITATION;
        $total = 0;

        $params = array('name' => $name,);

        $QCompany              = new Application_Model_Company();
        $this->view->companies = $QCompany->fetchPagination($page, $limit, $total, $params);
        $this->view->limit     = $limit;
        $this->view->total     = $total;
        $this->view->url       = HOST . 'staff/company/' . ($params ? '?' . http_build_query($params) .
                '&' : '?');
        $this->view->offset    = $limit * ($page - 1);
    }

    public function companyEditAction() {
        $id = $this->getRequest()->getParam('id');

        $QCompany = new Application_Model_Company();
        $company  = $QCompany->find($id);
        $company  = $company->current();

        if (!$company) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('Invalid ID.');
            $this->_redirect(HOST . 'staff/company');
        }

        $this->view->company = $company;

        //get department
        $QDepartment             = new Application_Model_Department();
        $this->view->departments = $QDepartment->get_cache();

        //get teams
        $QTeam             = new Application_Model_Team();
        $this->view->teams = $QTeam->get_cache();

        $QArea             = new Application_Model_Area();
        $this->view->areas = $QArea->fetchAll(null, 'name');
    }

    public function companySearchAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setRender('partials/list-company');
        } else {
            exit;
        }

        $sort = $this->getRequest()->getParam('sort', '');
        $desc = $this->getRequest()->getParam('desc', 1);

        $page            = $this->getRequest()->getParam('page', 1);
        $name            = $this->getRequest()->getParam('name');
        $department      = $this->getRequest()->getParam('department');
        $off             = $this->getRequest()->getParam('off', 1);
        $team            = $this->getRequest()->getParam('team');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $area_id         = $this->getRequest()->getParam('area_id');
        $email           = $this->getRequest()->getParam('email');
        $is_officer      = $this->getRequest()->getParam('is_officer');
        $tags            = $this->getRequest()->getParam('tags');

        if ($tags and is_array($tags))
            $tags = $tags;
        else
            $tags = null;

        $limit = LIMITATION;

        $total = 0;

        $params = array_filter(array(
            'name'            => $name,
            'department'      => $department,
            'off'             => $off,
            'team'            => $team,
            'regional_market' => $regional_market,
            'area_id'         => $area_id,
            'email'           => $email,
            'is_officer'      => $is_officer,
        ));

        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $QStaff = new Application_Model_Staff();
        $staffs = $QStaff->fetchPagination($page, $limit, $total, $params);

        $userStorage             = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->userStorage = $userStorage;

        //get department
        $QDepartment             = new Application_Model_Department();
        $this->view->departments = $QDepartment->get_cache();

        //get teams
        $QTeam             = new Application_Model_Team();
        $this->view->teams = $QTeam->get_cache();

        $QCompany              = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $QArea             = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $QRegionalMarket     = new Application_Model_RegionalMarket();
        $this->view->regions = $QRegionalMarket->get_cache_all();

        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->staffs = $staffs;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST . 'staff/company-search' . ($params ? '?' .
                http_build_query($params) . '&' : '?');

        $this->view->offset      = $limit * ($page - 1);
        $this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');
    }

    public function casualWorkerAction() {
        $sort    = $this->getRequest()->getParam('sort', '');
        $desc    = $this->getRequest()->getParam('desc', 1);
        $page    = $this->getRequest()->getParam('page', 1);
        $name    = $this->getRequest()->getParam('name');
        $area_id = $this->getRequest()->getParam('area_id');

        $limit = LIMITATION;
        $total = 0;

        $params = array(
            'sort'    => $sort,
            'desc'    => $desc,
            'name'    => $name,
            'area_id' => $area_id,
        );

        $QCasual = new Application_Model_CasualWorker();
        $staffs  = $QCasual->fetchPagination($page, $limit, $total, $params);

        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->staffs = $staffs;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST . 'staff/casual-worker' . ($params ? '?' .
                http_build_query($params) . '&' : '?');

        $QArea             = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $QRegion             = new Application_Model_RegionalMarket();
        $this->view->regions = $QRegion->get_cache_all();

        $this->view->offset = $limit * ($page - 1);
    }

    private function get_sales_staff_area($area_id = 0) {
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('s' => 'staff'), array('total' =>
                            'COUNT(s.id)'))->join(array('r' => 'regional_market'), 's.regional_market = r.id', array('province_name' => 'r.name'))->join(array('a' =>
                            'area'), 'r.area_id = a.id', array('area_name' => 'a.name'))->join(array('t' =>
                            'team'), 't.id = s.title', array('title_name' => 't.name', 'title_id' => 't.id'))->
                        join(array('tm' => 'team'), 'tm.id = t.parent_id', array('team_name' =>
                            'tm.name', 'team_id'   => 'tm.id'))->where('s.off_date = 0 OR s.off_date IS NULL OR s.off_date = \'\'')->
                        group('t.id')->having('t.id IN (?)', $this->list_titles);

        if (isset($area_id) && $area_id)
            $select->where('a.id = ?', $area_id);

        $result = $db->fetchAll($select);

        $staffs = array();
        foreach ($result as $k => $v) {
            $staffs[$v['title_id']] = $v['total'];
        }
        return $staffs;
    }

    private function get_staff_general() {
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('s' => 'staff'), array('total' =>
                    'COUNT(s.id)'))->join(array('t' => 'team'), 't.id=s.title', array())->join(array
                    ('tm' => 'team'), 'tm.id=t.parent_id', array())->join(array('d' => 'team'), 'd.id=tm.parent_id', array('department_name' => 'd.name'))->where('s.off_date = 0 OR s.off_date IS NULL OR s.off_date = \'\' ')->
                group('d.id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function priviledgeAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'priviledge.php';
    }

    public function priviledgeCsAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'priviledge-cs.php';
    }

    public function priviledgeTradeAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'priviledge-trade.php';
    }
    
    public function priviledgeErpAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'priviledge-erp.php';
    }

    public function priviledgeSaveAction() {

        if ($this->getRequest()->getMethod() == 'POST') {


            $id           = $this->getRequest()->getParam('id');
            $staff_id     = $this->getRequest()->getParam('staff_id');
            $default_page = $this->getRequest()->getParam('default_page');
            $menus        = $this->getRequest()->getParam('menus');

            $raw_access = $this->getRequest()->getParam('access');
            $access     = array();
            if (is_array($raw_access)) {
                foreach ($raw_access as $module => $item)
                    foreach ($item as $controller => $item_2)
                        foreach ($item_2 as $action => $item_3) {
                            if ($item_3)
                                $access[] = $module . '::' . $controller . '::' . $action;
                        }
            }


            $QStaffPriveledge = new Application_Model_StaffPriviledge();

            $data = array(
                'staff_id'     => $staff_id,
                'default_page' => ($default_page ? $default_page : null),
                'menu'         => (is_array($menus) ? implode(',', $menus) : null),
                'access'       => json_encode($access),
            );

            if ($id) {
                $where = $QStaffPriveledge->getAdapter()->quoteInto('id = ?', $id);

                $QStaffPriveledge->update($data, $where);
            } else {
                $QStaffPriveledge->insert($data);
            }

            //remove cache
            $cache = Zend_Registry::get('cache');
            $cache->remove('staff_priviledge_cache');

            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('Done!');
        }

        $back_url = $this->getRequest()->getParam('back_url');

        $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
    }

    public function priviledgeTradeSaveAction() {

        if ($this->getRequest()->getMethod() == 'POST') {


            $id           = $this->getRequest()->getParam('id');
            $staff_id     = $this->getRequest()->getParam('staff_id');
            $default_page = $this->getRequest()->getParam('default_page');
            $menus        = $this->getRequest()->getParam('menus');

            $raw_access = $this->getRequest()->getParam('access');
            $access     = array();
            if (is_array($raw_access)) {
                foreach ($raw_access as $module => $item)
                    foreach ($item as $controller => $item_2)
                        foreach ($item_2 as $action => $item_3) {
                            if ($item_3)
                                $access[] = $module . '::' . $controller . '::' . $action;
                        }
            }


            $QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();

            $data = array(
                'staff_id'     => $staff_id,
                'default_page' => ($default_page ? $default_page : null),
                'menu'         => (is_array($menus) ? implode(',', $menus) : null),
                'access'       => json_encode($access),
            );

            if ($id) {
                $where = $QStaffPriveledge->getAdapter()->quoteInto('id = ?', $id);

                $QStaffPriveledge->update($data, $where);
            } else {
                $QStaffPriveledge->insert($data);
            }

            //remove cache
            $cache = Zend_Registry::get('cache');
            $cache->remove('staff_priviledge_cache');

            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('Done!');
        }

        $back_url = $this->getRequest()->getParam('back_url');

        $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
    }
    
    public function priviledgeErpSaveAction() {

        if ($this->getRequest()->getMethod() == 'POST') {


            $id           = $this->getRequest()->getParam('id');
            $staff_id     = $this->getRequest()->getParam('staff_id');
            $default_page = $this->getRequest()->getParam('default_page');
            $menus        = $this->getRequest()->getParam('menus');

            $raw_access = $this->getRequest()->getParam('access');
            $access     = array();
            if (is_array($raw_access)) {
                foreach ($raw_access as $module => $item)
                    foreach ($item as $controller => $item_2)
                        foreach ($item_2 as $action => $item_3) {
                            if ($item_3)
                                $access[] = $module . '::' . $controller . '::' . $action;
                        }
            }


            $QStaffPriveledge = new Application_Model_StaffPriviledgeErp();

            $data = array(
                'staff_id'     => $staff_id,
                'default_page' => ($default_page ? $default_page : null),
                'menu'         => (is_array($menus) ? implode(',', $menus) : null),
                'access'       => json_encode($access),
            );

            if ($id) {
                $where = $QStaffPriveledge->getAdapter()->quoteInto('id = ?', $id);

                $QStaffPriveledge->update($data, $where);
            } else {
                $QStaffPriveledge->insert($data);
            }

            //remove cache
            $cache = Zend_Registry::get('cache');
            $cache->remove('staff_priviledge_cache');

            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('Done!');
        }

        $back_url = $this->getRequest()->getParam('back_url');

        $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
    }

    private function generate_list($group_menus) {
        return $this->ul(0, '', $group_menus);
    }

    function ul($parent = 0, $attr = '', $group_menus = null) {
        static $i = 1;
        $indent   = str_repeat("\t\t", $i);
        if (isset($this->data[$parent])) {
            if ($attr) {
                $attr = ' ' . $attr;
            }
            $html = "\n$indent";
            $html .= "<ul$attr>";
            $i++;
            foreach ($this->data[$parent] as $row) {
                $child = $this->ul($row['id'], '', $group_menus);
                $html  .= "\n\t$indent";
                $html  .= '<li>';
                $html  .= '<input value="' . $row['id'] . '" ' . (($group_menus and in_array($row['id'], $group_menus)) ? 'checked' : '') . ' type="checkbox" id="menus_' . $row['id'] .
                        '" name="menus[]"><label>' . $row['label'] . '</label>';
                if ($child) {
                    $i--;
                    $html .= $child;
                    $html .= "\n\t$indent";
                }
                $html .= '</li>';
            }
            $html .= "\n$indent</ul>";
            return $html;
        } else {
            return false;
        }
    }

    private function add_row($id, $parent, $label) {
        $this->data[$parent][] = array('id' => $id, 'label' => $label);
    }

    private function get_address($staff_id = null) {
        if (is_null($staff_id))
            return false;

        // get addresses
        $QStaffAddress              = new Application_Model_StaffAddress();
        $district_cache             = $QRegionalMarket->get_district_cache();
        $this->view->district_cache = $district_cache;

        // ------------------------- get permanent address
        $where                         = array();
        $where[]                       = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
        $where[]                       = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Permanent);
        $permanent_address             = $QStaffAddress->fetchRow($where);
        $this->view->permanent_address = $permanent_address;

        if (isset($permanent_address['district']) && isset($district_cache[$permanent_address['district']]))
            $this->view->permanent_address_districts = $QRegionalMarket->
                    get_district_by_province_cache($district_cache[$permanent_address['district']]['parent']);
        // ------------------------- get temporary address
        $where                                   = array();
        $where[]                                 = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
        $where[]                                 = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Temporary);
        $temporary_address                       = $QStaffAddress->fetchRow($where);
        $this->view->temporary_address           = $temporary_address;

        if (isset($temporary_address['district']) && isset($district_cache[$temporary_address['district']]))
            $this->view->temporary_address_districts = $QRegionalMarket->
                    get_district_by_province_cache($district_cache[$temporary_address['district']]['parent']);

        // ------------------------- get birth certificate address
        $where                     = array();
        $where[]                   = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
        $where[]                   = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Birth_Certificate);
        $birth_address             = $QStaffAddress->fetchRow($where);
        $this->view->birth_address = $birth_address;

        if (isset($birth_address['district']) && isset($district_cache[$birth_address['district']]))
            $this->view->birth_address_districts = $QRegionalMarket->
                    get_district_by_province_cache($district_cache[$birth_address['district']]['parent']);

        // ------------------------- get ID card address
        $where                       = array();
        $where[]                     = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
        $where[]                     = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::ID_Card);
        $id_card_address             = $QStaffAddress->fetchRow($where);
        $this->view->id_card_address = $id_card_address;

        if (isset($id_card_address['district']) && isset($district_cache[$id_card_address['district']]))
            $this->view->id_card_address_districts = $QRegionalMarket->
                    get_district_by_province_cache($district_cache[$id_card_address['district']]['parent']);

        // ------------------------- Get other logs
        $QStaffEducation       = new Application_Model_StaffEducation();
        $where                 = $QStaffEducation->getAdapter()->quoteInto('staff_id = ?', $id);
        $this->view->education = $QStaffEducation->fetchAll($where);

        $QStaffExperience       = new Application_Model_StaffExperience();
        $where                  = $QStaffExperience->getAdapter()->quoteInto('staff_id = ?', $id);
        $this->view->experience = $QStaffExperience->fetchAll($where);

        $QStaffRelative       = new Application_Model_StaffRelative();
        $where                = $QStaffRelative->getAdapter()->quoteInto('staff_id = ?', $id);
        $this->view->relative = $QStaffRelative->fetchAll($where);
    }

    public function saveTransferAction() {
        $this->_helper->layout()->disableLayout(true);

        $staff_id        = $this->getRequest()->getParam('staff_id');
        $area_id         = $this->getRequest()->getParam('area_id');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $company         = $this->getRequest()->getParam('company');
        $status          = $this->getRequest()->getParam('status');
        $department      = $this->getRequest()->getParam('department');
        $team            = $this->getRequest()->getParam('team');
        $group           = $this->getRequest()->getParam('group');
        $title           = trim($this->getRequest()->getParam('title'));
        $from_date       = $this->getRequest()->getParam('from_date');

        $transfer_id = $this->getRequest()->getParam('transfer_id');
        $transfer_tmp_id = $this->getRequest()->getParam('transfer_tmp_id', 0);

        // echo "<pre>";print_r($title);die;
        $QStaffTransferTemp = new Application_Model_StaffTransferTmp();

        if (!$company) {
            exit(json_encode(array('status' => 0, 'message' => 'Company is required')));
        }

        if (!$area_id) {
            exit(json_encode(array('status' => 0, 'message' => 'Area is required')));
        }

        if (!$regional_market) {
            exit(json_encode(array('status' => 0, 'message' => 'Province is required')));
        }


        if (!$department) {
            exit(json_encode(array('status' => 0, 'message' => 'Department is required')));
        }


        if (!$team) {
            exit(json_encode(array('status' => 0, 'message' => 'Team is required')));
        }


        if (!$title) {
            exit(json_encode(array('status' => 0, 'message' => 'Title is required')));
        }

        if (!$from_date) {
            exit(json_encode(array('status' => 0, 'message' => 'From date is required')));
        }
		
		if($title == 183){
			
            $QRegionalMarket = new Application_Model_RegionalMarket();
				
			
            $where_province      = array();
            $where_province[]    = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
            $where_province[]    = $QRegionalMarket->getAdapter()->quoteInto('default_province = ?', 1);
            $province   = $QRegionalMarket->fetchRow($where_province);
		
            if(!empty($province)){
                $default_province  = $province->toArray()['id'];
				if($regional_market != $default_province){
                    exit(json_encode(array('status' => 0, 'message' => 'Wrong default province')));
                }
            }
        }

        $selectTmpTransfer = $QStaffTransferTemp->select()
                    ->where('staff_id = ?', $staff_id)
                    ->where('finish = 0')
                    ->limit(1);

        $rowTmpTransfer            = $QStaffTransferTemp->fetchRow($selectTmpTransfer);
        if ($rowTmpTransfer) {
            exit(json_encode(array(
                'status'  => 0,
                'message' => 'Please check Transfer By Admin Pendding',
            )));
        }    
        
        $transfer_data                = array();
        $transfer_data['staff_id']    = $staff_id;
        $transfer_data['from_date']   = $from_date;
        $transfer_data['transfer_id'] = $transfer_id;
        $transfer_data['transfer_tmp_id'] = !empty($transfer_tmp_id) ? $transfer_tmp_id : 0;
        $transfer_data['info']        = array(
            My_Staff_Info_Type::Area       => $area_id,
            My_Staff_Info_Type::Region     => $regional_market,
            My_Staff_Info_Type::Company    => $company,
            My_Staff_Info_Type::Department => $department,
            My_Staff_Info_Type::Team       => $team,
//             My_Staff_Info_Type::Group      => $group,
            My_Staff_Info_Type::Title      => $title);

        $option = 'add';
        if ($transfer_id) {
            $option = 'update';
        }
        // var_dump($option);
        $result = My_Staff_Log::transfer($staff_id, $transfer_data, $option, $title);

        if ($result) {
            exit(json_encode(array(
                'status'  => 1,
                'message' => 'Success1',
            )));
        } else {
            exit(json_encode(array('status'  => 0, 'message' =>
                'Can not update, please try again!')));
        }
    }

    public function saveTransferBkAction() {
        $this->_helper->layout()->disableLayout(true);

        $staff_id        = $this->getRequest()->getParam('staff_id');
        $area_id         = $this->getRequest()->getParam('area_id');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $company         = $this->getRequest()->getParam('company');
        $status          = $this->getRequest()->getParam('status');
        $department      = $this->getRequest()->getParam('department');
        $team            = $this->getRequest()->getParam('team');
        $group           = $this->getRequest()->getParam('group');
        $title           = trim($this->getRequest()->getParam('title'));
        $from_date       = $this->getRequest()->getParam('from_date');

        $transfer_id = $this->getRequest()->getParam('transfer_id', 0);




        if (!$company) {
            exit(json_encode(array('status' => 0, 'message' => 'Company is required')));
        }

        if (!$area_id) {
            exit(json_encode(array('status' => 0, 'message' => 'Area is required')));
        }

        if (!$regional_market) {
            exit(json_encode(array('status' => 0, 'message' => 'Province is required')));
        }


        if (!$department) {
            exit(json_encode(array('status' => 0, 'message' => 'Department is required')));
        }


        if (!$team) {
            exit(json_encode(array('status' => 0, 'message' => 'Team is required')));
        }


//         if (!$group) {
//             exit(json_encode(array('status' => 0, 'message' => 'Group is required')));
//         }


        if (!$title) {
            exit(json_encode(array('status' => 0, 'message' => 'Title is required')));
        }

        if (!$from_date) {
            exit(json_encode(array('status' => 0, 'message' => 'From date is required')));
        }


        $transfer_data                = array();
        $transfer_data['staff_id']    = $staff_id;
        $transfer_data['from_date']   = $from_date;
        $transfer_data['transfer_id'] = $transfer_id;
        $transfer_data['info']        = array(
            My_Staff_Info_Type::Area       => $area_id,
            My_Staff_Info_Type::Region     => $regional_market,
            My_Staff_Info_Type::Company    => $company,
            My_Staff_Info_Type::Department => $department,
            My_Staff_Info_Type::Team       => $team,
//             My_Staff_Info_Type::Group      => $group,
            My_Staff_Info_Type::Title      => $title);

        $option = 'add';
        if ($transfer_id) {
            $option = 'update';
        }
        // var_dump($option);
        $result = My_Staff_Log::transfer($staff_id, $transfer_data, $option, $title);


        if ($result) {
            exit(json_encode(array(
                'status'  => 1,
                'message' => 'Success',
            )));
        } else {
            exit(json_encode(array('status'  => 0, 'message' =>
                'Can not update, please try again!')));
        }
    }

    public function delTransferAction() {
        $transfer_id = $this->getRequest()->getParam('transfer_id');
        exit(json_encode(My_Staff_Log::delete($transfer_id)));
    }

    public function updateTransferDataAction() {
        /**
         * @for: admin
         * @description: init data transfer
         */
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        $QStaffTransfer  = new Application_Model_StaffTransfer();
        $QStaffLogDetail = new Application_Model_StaffLogDetail();
        $db              = Zend_Registry::get('db');
        $userStorage     = Zend_Auth::getInstance()->getStorage()->read();
        $select          = $db->select()->from(array('s' => 'staff'), array('s.*'));
        $result          = $db->fetchAll($select);

        $db->beginTransaction();
        $date = date('Y-m-d');
        try {
            foreach ($result as $item) {
                $dataTransfer = array(
                    'staff_id'   => $item['id'],
                    'from_date'  => $item['joined_at'],
                    'created_at' => $date,
                    'created_by' => $userStorage->id,
                    'note'       => 'initFirstData',
                );
                $transfer_id  = $QStaffTransfer->insert($dataTransfer);

                $selectRegion = $db->select()->from(array('r' => 'regional_market'), 'r.*')->
                        where('r.id = ?', intval($item['regional_market']));

                $rowRegion = $db->fetchRow($selectRegion);

                $arrInfoType = array(
                    My_Staff_Info_Type::Company    => $item['company_id'],
                    My_Staff_Info_Type::Area       => $rowRegion['area_id'],
                    My_Staff_Info_Type::Region     => $item['regional_market'],
                    My_Staff_Info_Type::Department => $item['department'],
                    My_Staff_Info_Type::Team       => $item['team'],
                    My_Staff_Info_Type::Group      => $item['group_id'],
                    My_Staff_Info_Type::Title      => $item['title'],
                );

                //update staff log detail
                foreach ($arrInfoType as $key => $value) {
                    $dataLog = array(
                        'transfer_id'   => $transfer_id,
                        'object'        => $item['id'],
                        'from_date'     => (strtotime($item['joined_at'])) ? strtotime($item['joined_at']) : null,
                        'info_type'     => $key,
                        'current_value' => ($key == My_Staff_Info_Type::Title) ? trim($value) : intval($value),
                    );
                    $QStaffLogDetail->insert($dataLog);
                }
            }
            $db->commit();
        } catch (exception $e) {
            $db->rollBack();
            exit(json_encode(array('code' => 1, 'message' => $e->getMessage())));
        }
        exit(json_encode(array('code' => 2, 'message' => 'Success')));
    }

    public function transferReportAction() {
        $from   = $this->getRequest()->getParam('from');
        $to     = $this->getRequest()->getParam('to');
        $page   = $this->getRequest()->getParam('page', 1);
        $export = $this->getRequest()->getParam('export');
        $QStaff = new Application_Model_Staff();
        $limit  = LIMITATION;
        $total  = 0;
        $params = array('from' => $from, 'to' => $to);

        if ($export) {
            $transfers = $QStaff->fetchPaginationTransfer($page, null, $total, $params);
            $this->_exportTransfer($transfers);
        }
        $transfers = $QStaff->fetchPaginationTransfer($page, $limit, $total, $params);

        $this->view->transfers = $transfers;
        $this->view->limit     = $limit;
        $this->view->total     = $total;
        $this->view->url       = HOST . 'group/' . ($params ? '?' . http_build_query($params) .
                '&' : '?');
        $this->view->offset    = $limit * ($page - 1);
    }

    private function _exportTransfer($data) {
        $db       = Zend_Registry::get('db');
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'STT',
            'Staff Code',
            'Staff Name',
            'Old company',
            'Old Department',
            'Old Team',
            'Old Title',
            'Old Area',
            'Old Province',
//             'Old Group',
            'Transfer Time',
            'company',
            'Department',
            'Team',
            'Title',
            'Area',
            'Province',
//             'Group'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index    = 2;
        $intCount = 1;
        $color    = 'FF0000';
        try {
            foreach ($data as $item) {
                $alpha      = 'A';
                $arrCurrent = array_combine(explode(',', $item['info_types']), explode(',', $item['current_values']));
                $arrOld     = array_combine(explode(',', $item['info_types']), explode(',', $item['old_values']));
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['fullname'], PHPExcel_Cell_DataType::TYPE_STRING);

                // old value
                $select       = $db->select()->from(array('c' => 'company'), array('name'))->where('id = ?', $arrOld[My_Staff_Info_Type::Company]);
                $company_name = $db->fetchOne($select);
                $sheet->getCell($alpha . $index)->setValueExplicit($company_name, PHPExcel_Cell_DataType::TYPE_STRING); //company
                if ($arrOld[My_Staff_Info_Type::Company] != $arrCurrent[My_Staff_Info_Type::
                        Company]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }
                $alpha++;
                $select = $db->select()->from(array('t1' => 'team'), array('title' => 't1.name'))->
                                join(array('t2' => 'team'), 't1.parent_id = t2.id', array('team' => 't2.name'))->
                                join(array('t3' => 'team'), 't2.parent_id = t3.id', array('department' =>
                                    't3.name'))->where('t1.id = ?', $arrOld[My_Staff_Info_Type::Title]);
                $team   = $db->fetchRow($select);
                $sheet->getCell($alpha . $index)->setValueExplicit($team['department'], PHPExcel_Cell_DataType::TYPE_STRING); //department
                if ($arrOld[My_Staff_Info_Type::Department] != $arrCurrent[My_Staff_Info_Type::
                        Department]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }

                $alpha++;
                $sheet->getCell($alpha . $index)->setValueExplicit($team['team'], PHPExcel_Cell_DataType::TYPE_STRING); //team
                if ($arrOld[My_Staff_Info_Type::Team] != $arrCurrent[My_Staff_Info_Type::Team]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }

                $alpha++;
                $sheet->getCell($alpha . $index)->setValueExplicit($team['title'], PHPExcel_Cell_DataType::TYPE_STRING); //title
                if ($arrOld[My_Staff_Info_Type::Title] != $arrCurrent[My_Staff_Info_Type::Title]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }

                $alpha++;
                $select   = $db->select()->from(array('r' => 'regional_market'), array('regional_name' =>
                            'r.name', 'area_name'     => 'a.name'))->join(array('a' => 'area'), 'a.id = r.area_id', array())->where('r.id = ?', $arrOld[My_Staff_Info_Type::
                        Region]);
                $regional = $db->fetchRow($select);
                $sheet->getCell($alpha . $index)->setValueExplicit($regional['area_name'], PHPExcel_Cell_DataType::TYPE_STRING); //area
                if ($arrOld[My_Staff_Info_Type::Area] != $arrCurrent[My_Staff_Info_Type::Area]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }

                $alpha++;
                $sheet->getCell($alpha . $index)->setValueExplicit($regional['regional_name'], PHPExcel_Cell_DataType::TYPE_STRING); //region
                if ($arrOld[My_Staff_Info_Type::Region] != $arrCurrent[My_Staff_Info_Type::
                        Region]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }
//                 $alpha++;
//                 $select     = $db->select()->from(array('g' => 'group'), array('name'))->where('g.id = ?', $arrOld[My_Staff_Info_Type::Group]);
//                 $group_name = $db->fetchOne($select);
//                 $sheet->getCell($alpha . $index)->setValueExplicit($group_name, PHPExcel_Cell_DataType::TYPE_STRING); //group
//                 if ($arrOld[My_Staff_Info_Type::Group] != $arrCurrent[My_Staff_Info_Type::Group]) {
//                     $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
//                             PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
//                 }

                $alpha++;
                $sheet->getCell($alpha . $index)->setValueExplicit(date('d/m/Y', strtotime($item['transfer_time'])), PHPExcel_Cell_DataType::TYPE_STRING); //time transfer
                $alpha++;
                //current transfer
                $select       = $db->select()->from(array('c' => 'company'), array('name'))->where('c.id = ?', $arrCurrent[My_Staff_Info_Type::Company]);
                $company_name = $db->fetchOne($select);
                $sheet->getCell($alpha . $index)->setValueExplicit($company_name, PHPExcel_Cell_DataType::TYPE_STRING); //company
                if ($arrOld[My_Staff_Info_Type::Company] != $arrCurrent[My_Staff_Info_Type::
                        Company]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }
                $alpha++;
                $select = $db->select()->from(array('t1' => 'team'), array('title' => 't1.name'))->
                                join(array('t2' => 'team'), 't1.parent_id = t2.id', array('team' => 't2.name'))->
                                join(array('t3' => 'team'), 't2.parent_id = t3.id', array('department' =>
                                    't3.name'))->where('t1.id = ?', $arrCurrent[My_Staff_Info_Type::Title]);
                $team   = $db->fetchRow($select);
                $sheet->getCell($alpha . $index)->setValueExplicit($team['department'], PHPExcel_Cell_DataType::TYPE_STRING); //department
                if ($arrOld[My_Staff_Info_Type::Department] != $arrCurrent[My_Staff_Info_Type::
                        Department]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }
                $alpha++;
                $sheet->getCell($alpha . $index)->setValueExplicit($team['team'], PHPExcel_Cell_DataType::TYPE_STRING); //team
                if ($arrOld[My_Staff_Info_Type::Team] != $arrCurrent[My_Staff_Info_Type::Team]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }

                $alpha++;
                $sheet->getCell($alpha . $index)->setValueExplicit($team['title'], PHPExcel_Cell_DataType::TYPE_STRING); //title
                if ($arrOld[My_Staff_Info_Type::Title] != $arrCurrent[My_Staff_Info_Type::Title]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }
                $alpha++;
                $select   = $db->select()->from(array('r' => 'regional_market'), array('regional_name' =>
                            'r.name', 'area_name'     => 'a.name'))->join(array('a' => 'area'), 'a.id = r.area_id', array())->where('r.id = ?', $arrCurrent[My_Staff_Info_Type::
                        Region]);
                $regional = $db->fetchRow($select);
                $sheet->getCell($alpha . $index)->setValueExplicit($regional['area_name'], PHPExcel_Cell_DataType::TYPE_STRING); //area
                if ($arrOld[My_Staff_Info_Type::Area] != $arrCurrent[My_Staff_Info_Type::Area]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }
                $alpha++;
                $sheet->getCell($alpha . $index)->setValueExplicit($regional['regional_name'], PHPExcel_Cell_DataType::TYPE_STRING); //region
                if ($arrOld[My_Staff_Info_Type::Region] != $arrCurrent[My_Staff_Info_Type::
                        Region]) {
                    $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
                }
//                 $alpha++;
//                 $select     = $db->select()->from(array('g' => 'group'), array('name'))->where('g.id = ?', $arrCurrent[My_Staff_Info_Type::Group]);
//                 $group_name = $db->fetchOne($select);
//                 $sheet->getCell($alpha . $index)->setValueExplicit($group_name, PHPExcel_Cell_DataType::TYPE_STRING); //group
//                 if ($arrOld[My_Staff_Info_Type::Group] != $arrCurrent[My_Staff_Info_Type::Group]) {
//                     $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
//                             PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
//                 }
                $alpha++;
                $index++;
            }
        } catch (exception $e) {
            echo $e->getMessage();
            exit;
        }
        $filename  = 'Transfer_Report_' . date('d_m_Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    private function _exportPictureAdd($params) {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'STT',
            'Staff Code',
            'Firstname',
            'Lastname',
            'Email',
            'Team',
            'Title',
            'Area');
        $sheet    = $PHPExcel->getActiveSheet();
        $alpha    = 'A';
        $index    = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $db     = Zend_Registry::get('db');
        $select = $db->select()->from(array('s' => 'staff'), array(
                    's.id',
                    's.code',
                    's.email',
                    's.firstname',
                    's.lastname',
                    's.contract_expired_at',
                    's.regional_market',
                    's.title',
                    's.department',
                    's.team',
                    's.contract_term',
                ))->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array('r.area_id'))->where('s.photo IS NULL')->where('s.status = 1', null);

        if (isset($params['name']) && $params['name'])
            $select->where('CONCAT(s.firstname, \' \', s.lastname) LIKE ?', '%' . $params['name'] .
                    '%');


        if (isset($params['email']) && $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']) .
                    EMAIL_SUFFIX);



        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('s.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (empty($params['area_id'])) {
            $this->_redirect(HOST . 'staff/list-basic-record');
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('s.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('s.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        $staffs = $db->fetchAll($select);

        $QDepartment = new Application_Model_Department();
        $departments = $QDepartment->get_cache();

        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache();

        $QContractTerm  = new Application_Model_ContractTerm();
        $contract_terms = $QContractTerm->get_cache();

        $QArea = new Application_Model_Area();
        $area  = $QArea->get_cache();

        $index    = 2;
        $intCount = 1;

        try {
            if ($staffs) {
                foreach ($staffs as $_key => $item) {
                    $alpha = 'A';

                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($item['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alpha++ . $index, $item['firstname']);
                    $sheet->setCellValue($alpha++ . $index, $item['lastname']);
                    $sheet->setCellValue($alpha++ . $index, $item['email']);
                    $sheet->setCellValue($alpha++ . $index, isset($teams[$item['team']]) ? $teams[$item['team']] :
                                    '');
                    $sheet->setCellValue($alpha++ . $index, isset($teams[$item['title']]) ? $teams[$item['title']] :
                                    '');
                    $sheet->setCellValue($alpha++ . $index, isset($area[$item['area_id']]) ? $area[$item['area_id']] :
                                    '');

                    $sheet->setCellValue($alpha++ . $index, isset($regional_markets[$item['regional_market']]) ?
                                    $regional_markets[$item['regional_market']] : '');


                    $index++;
                }
            }
        } catch (exception $e) {
            
        }

        $filename  = ' Expired Additional Pictures ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    private function _exportContractPrintingLog($params) {
        require_once 'PHPExcel.php';
        $staff_print_manager = 3246;

        $PHPExcel = new PHPExcel();
        $heads    = array(
            'STT',
            'Staff Code',
            'Firstname',
            'Lastname',
            'Email',
            'Department',
            'Team',
            'Title',
            'Province',
            'Base Salary',
            'Bonus Salary',
            'Food Salary',
            'Fuel Salary',
            'Phone Salary',
            'Probation Salary',
            'Contract',
            'From',
            'To',
            'Time Update',
            'KPI');

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $db     = Zend_Registry::get('db');
        $select = $db->select()->from(array('p' => 'staff_print_log'), array('p.*'))->
                join(array('s' => 'staff'), 's.id=p.object', array(
            's.firstname',
            's.lastname',
            's.email',
            's.code',
            's.team',
            's.department'));

        if (isset($params['name']) && $params['name'])
            $select->where('CONCAT(s.firstname, \' \', s.lastname) LIKE ?', '%' . $params['name'] .
                    '%');

        if (isset($params['email']) && $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']) .
                    EMAIL_SUFFIX);

        if (isset($params['from']) && $params['from'])
            $select->where('p.time >= ?', DateTime::createFromFormat('d/m/Y', $params['from'])->
                            format('Y-m-d 00:00:00'));

        if (isset($params['to']) && $params['to'])
            $select->where('p.time <= ?', DateTime::createFromFormat('d/m/Y', $params['to'])->
                            format('Y-m-d 23:59:59'));

        //  $select->where('s.title in (?)' , array(PGPB_TITLE PGPB_2_TITLE, SALES_LEADER_TILE , SALES_ACCESSORIES_TITLE ,SALES_TITLE));
        $select->where('p.user_id = ? ', $staff_print_manager);

        $data = $db->fetchAll($select);

        $index    = 2;
        $intCount = 1;

        $QDepartment = new Application_Model_Department();
        $departments = $QDepartment->get_cache();

        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache();

        $QContractTerm  = new Application_Model_ContractTerm();
        $contract_terms = $QContractTerm->get_cache();

        try {
            if ($data)
                foreach ($data as $item) {
                    $alpha = 'A';

                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($item['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alpha++ . $index, $item['firstname']);
                    $sheet->setCellValue($alpha++ . $index, $item['lastname']);
                    $sheet->setCellValue($alpha++ . $index, $item['email']);
                    $sheet->setCellValue($alpha++ . $index, isset($departments[$item['department']]) ?
                                    $departments[$item['department']] : '');
                    $sheet->setCellValue($alpha++ . $index, isset($teams[$item['team']]) ? $teams[$item['team']] :
                                    '');
                    $sheet->setCellValue($alpha++ . $index, $item['title']);
                    $sheet->setCellValue($alpha++ . $index, isset($regional_markets[$item['regional_market']]) ?
                                    $regional_markets[$item['regional_market']] : '');
                    $sheet->setCellValue($alpha++ . $index, $item['base_salary']);
                    $sheet->setCellValue($alpha++ . $index, $item['bonus_salary']);
                    $sheet->setCellValue($alpha++ . $index, $item['allowance_1']);
                    $sheet->setCellValue($alpha++ . $index, $item['allowance_2']);
                    $sheet->setCellValue($alpha++ . $index, $item['allowance_3']);
                    $sheet->setCellValue($alpha++ . $index, $item['probation_salary']);
                    $sheet->setCellValue($alpha++ . $index, isset($contract_terms[$item['contract_term']]) ?
                                    $contract_terms[$item['contract_term']] : '');
                    $sheet->setCellValue($alpha++ . $index, isset($item['from_date']) && strtotime($item['from_date']) ?
                                    date('m/d/Y', strtotime($item['from_date'])) : '');
                    $sheet->setCellValue($alpha++ . $index, isset($item['to_date']) && strtotime($item['to_date']) ?
                                    date('m/d/Y', strtotime($item['to_date'])) : '');
                    $sheet->setCellValue($alpha++ . $index, isset($item['time']) && strtotime($item['time']) ?
                                    date('m/d/Y', strtotime($item['time'])) : '');
                    $sheet->setCellValue($alpha++ . $index, $item['kpi']);
                    $index++;
                }
        } catch (exception $e) {
            
        }

        $filename  = 'Contract Printing Report ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function contractRevisionListAction() {
        $id             = $this->getRequest()->getParam('object');
        $flashMessenger = $this->_helper->flashMessenger;

        try {
            if (!$id || !intval($id))
                throw new Exception("Invalid ID", 1);

            $QStaff      = new Application_Model_Staff();
            $where       = $QStaff->getAdapter()->quoteInto('id = ?', $id);
            $staff_check = $QStaff->fetchRow($where);

            if (!$staff_check)
                throw new Exception("Invalid staff", 2);

            $QStaffPrintLog = new Application_Model_StaffPrintLog();

            $page               = $this->getRequest()->getParam('page', 1);
            $total              = 0;
            $limit              = LIMITATION;
            $params             = array('object' => intval($id));
            $params['revision'] = true;

            $this->view->logs   = $QStaffPrintLog->fetchPagination($page, $limit, $total, $params);
            $this->view->params = $params;
            $this->view->limit  = $limit;
            $this->view->total  = $total;

            unset($params['revision']);

            $this->view->url    = HOST . 'staff/contract-revision-list' . ($params ? '?' .
                    http_build_query($params) . '&' : '?');
            $this->view->offset = $limit * ($page - 1);

            $this->view->staff = $staff_check;

            $this->view->back_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] :
                    'staff/contract-term';
        } catch (exception $e) {
            $flashMessenger->setNamespace('error')->addMessage(sprintf("[%d] %s", $e->
                                    getCode(), $e->getMessage()));
            $this->_redirect(HOST . 'staff/contract-term');
        }
    }

    public function contractRevisionAction() {
        $id = $this->getRequest()->getParam('id');

        $flashMessenger = $this->_helper->flashMessenger;

        try {
            if (!$id)
                throw new Exception("Invalid ID", 1);

            $QStaffPrintLog = new Application_Model_StaffPrintLog();
            $where          = $QStaffPrintLog->getAdapter()->quoteInto('id = ?', $id);
            $log            = $QStaffPrintLog->fetchRow($where);

            if (!$log)
                throw new Exception("Invalid revision", 2);

            $QSalarySales     = new Application_Model_SalarySales();
            $QSalaryPG        = new Application_Model_SalaryPg();
            $QContract        = new Application_Model_ContractTerm();
            $QStaffAddress    = new Application_Model_StaffAddress();
            $QRegionalMarket  = new Application_Model_RegionalMarket();
            $regional_markets = $QRegionalMarket->get_cache_all();
            $QStaff           = new Application_Model_Staff();

            $where = $QStaff->getAdapter()->quoteInto('id = ?', $log['object']);
            $staff = $QStaff->fetchRow($where);

            if (!$staff)
                throw new Exception("Invalid staff", 3);

            $contract_name = $QContract->get_cache();

            if (isset($staff['contract_signed_at']) and $staff['contract_signed_at'] == '') {
                $staff['contract_signed_at'] = $staff['joined_at'];
            }

            if (!$staff['birth_place']) {
                $where                = array();
                $where[]              = $QStaffAddress->getAdapter()->quoteInto('staff_id = ? ', $staff['id']);
                $where[]              = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', 4);
                $staff_adress         = $QStaffAddress->fetchRow($where);
                $address              = $staff_adress['address'] . ' , ' . $staff_adress['ward'];
                $regional_market      = $staff_adress['district'];
                $Area_result          = $QRegionalMarket->find($regional_market);
                $result_set           = $Area_result->current();
                $area                 = $result_set['parent'];
                $regional_cache       = $QRegionalMarket->get_district_cache($area);
                $regional_market      = $regional_cache[$regional_market];
                $area                 = $regional_markets[$area];
                $staff['birth_place'] = $area['name'];
            }

            if (!$staff['address']) {
                $where        = array();
                $where[]      = $QStaffAddress->getAdapter()->quoteInto('staff_id = ? ', $staff['id']);
                $where[]      = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', 4);
                $staff_adress = $QStaffAddress->fetchRow($where);

                $address         = $staff_adress['address'] . ' , ' . $staff_adress['ward'];
                $regional_market = $staff_adress['district'];

                $Area_result = $QRegionalMarket->find($regional_market);
                $result_set  = $Area_result->current();
                $area        = $result_set['parent'];

                $regional_cache = $QRegionalMarket->get_district_cache($area);


                $regional_market = $regional_cache[$regional_market];


                $area    = $regional_markets[$area];
                if (isset($regional_market['name']))
                    $address = $address . ' , ' . $regional_market['name'];

                if (isset($area))
                    $address = $address . ' , ' . $area['name'];

                $staff['address'] = $address;
            }

            if ($staff['title'] == 'SALE LEADER') {
                $staff['title'] = 'Nhân viên kinh doanh';
            }

            $start = $staff['contract_signed_at'];
            $end   = $staff['contract_expired_at'];
            $start = $end;

            $luong = array();

            //sale team
            if ($staff['team'] == '75' || $staff['team'] == 147) {
                $where               = array();
                $where[]             = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                $luong[$staff['id']] = $QSalarySales->fetchRow($where);
            }

            //pg team
            if ($staff['team'] == '16') {
                $where1              = array();
                $where1[]            = $QSalaryPG->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
                $luong[$staff['id']] = $QSalaryPG->fetchRow($where1);
            }

            $this->view->contract_name = $contract_name;
            $this->view->luong         = $luong;
            $staffs                    = array($staff);
            $this->view->staff         = $staffs;

            $QGroup             = new Application_Model_Group();
            $this->view->groups = $QGroup->fetchAll();

            $QModel                     = new Application_Model_ContractType();
            $this->view->contract_types = $QModel->fetchAll();

            $QModel                     = new Application_Model_ContractTerm();
            $this->view->contract_terms = $QModel->fetchAll();

            $QModel                  = new Application_Model_Department();
            $this->view->departments = $QModel->fetchAll();

            $QRegionalMarket = new Application_Model_RegionalMarket();

            $this->view->regional_markets = $QRegionalMarket->get_cache();

            $QModel            = new Application_Model_Team();
            $this->view->teams = $QModel->fetchAll();

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setRender('print');
        } catch (exception $e) {
            $flashMessenger->setNamespace('error')->addMessage(sprintf("[%d] %s", $e->
                                    getCode(), $e->getMessage()));
            $this->_redirect(HOST . 'staff/contract-term');
        }
    }

    private function _exportExpiredContract($params) {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'STT',
            'Staff Code',
            'Firstname',
            'Lastname',
            'Email',
            'Department',
            'Team',
            'Title',
            'Province',
            'Contract Type',
            'Expired',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $db     = Zend_Registry::get('db');
        $select = $db->select()->from(array('s' => 'staff'), array(
                    's.id',
                    's.code',
                    's.email',
                    's.firstname',
                    's.lastname',
                    's.contract_expired_at',
                    's.regional_market',
                    's.title',
                    's.department',
                    's.team',
                    's.contract_term'))->where('s.off_date IS NULL');

        if (isset($params['name']) && $params['name'])
            $select->where('CONCAT(s.firstname, \' \', s.lastname) LIKE ?', '%' . $params['name'] .
                    '%');

        if (isset($params['email']) && $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']) .
                    EMAIL_SUFFIX);

        if (isset($params['from']) && $params['from'])
            $select->where('s.contract_expired_at >= ?', DateTime::createFromFormat('d/m/Y', $params['from'])->format('Y-m-d 00:00:00'));

        if (isset($params['to']) && $params['to'])
            $select->where('s.contract_expired_at <= ?', DateTime::createFromFormat('d/m/Y', $params['to'])->format('Y-m-d 23:59:59'));

        $staffs = $db->fetchAll($select);

        $QDepartment = new Application_Model_Department();
        $departments = $QDepartment->get_cache();

        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache();

        $QContractTerm  = new Application_Model_ContractTerm();
        $contract_terms = $QContractTerm->get_cache();

        $index    = 2;
        $intCount = 1;

        try {
            if ($staffs) {
                foreach ($staffs as $_key => $item) {
                    $alpha = 'A';

                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($item['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alpha++ . $index, $item['firstname']);
                    $sheet->setCellValue($alpha++ . $index, $item['lastname']);
                    $sheet->setCellValue($alpha++ . $index, $item['email']);
                    $sheet->setCellValue($alpha++ . $index, isset($departments[$item['department']]) ?
                                    $departments[$item['department']] : '');
                    $sheet->setCellValue($alpha++ . $index, isset($teams[$item['team']]) ? $teams[$item['team']] :
                                    '');
                    $sheet->setCellValue($alpha++ . $index, $item['title']);
                    $sheet->setCellValue($alpha++ . $index, isset($regional_markets[$item['regional_market']]) ?
                                    $regional_markets[$item['regional_market']] : '');
                    $sheet->setCellValue($alpha++ . $index, isset($contract_terms[$item['contract_term']]) ?
                                    $contract_terms[$item['contract_term']] : '');
                    $sheet->setCellValue($alpha++ . $index, isset($item['contract_expired_at']) &&
                            strtotime($item['contract_expired_at']) ? date('m/d/Y', strtotime($item['contract_expired_at'])) :
                                    '');

                    $index++;
                }
            }
        } catch (exception $e) {
            
        }

        $filename  = ' Expired Contract Report ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function delPrintLogAction() {
        $id             = $this->getRequest()->getParam('id');
        $object         = $this->getRequest()->getParam('object');
        $flashMessenger = $this->_helper->flashMessenger;
        $QStaffPrintLog = new Application_Model_StaffPrintLog();
        $db             = Zend_Registry::get('db');
        $row            = $QStaffPrintLog->find($id)->current();

        if ($row) {
            $db->beginTransaction();
            try {
                $row->delete();
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Done!');
                $this->_redirect(HOST . 'staff/print-log?object=' . $object);
            } catch (exception $e) {
                $db->rollBack();
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                $this->_redirect(HOST . 'staff/print-log?object=' . $object);
            }
        } else {
            $flashMessenger->setNamespace('error')->addMessage('Print Log not existed!');
            $this->_redirect(HOST . 'staff/print-log?object=' . $object);
        }
    }

    public function uploadPhotoAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'upload-photo.php';
    }

    public function resetPasswordAction() {
        $flashMessenger               = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();
    }

    public function savePasswordAction() {
        $email          = $this->getRequest()->getParam('email');
        $password1      = $this->getRequest()->getParam('password1');
        $password2      = $this->getRequest()->getParam('password2');
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $flashMessenger = $this->_helper->flashMessenger;

        try {
            if (!$email || empty($email))
                throw new Exception("Invalid email", 1);
            if (!$userStorage || !isset($userStorage->id))
                throw new Exception("Invalid user", 2);

            if ($password1 != $password2)
                throw new Exception("Password does not macth", 1);

            $params = array(
                'email' => $email
            );

            if (in_array($userStorage->group_id, array(ASM_ID, ASMSTANDBY_ID, SALES_ADMIN_ID))) {
                $params['asm_id'] = $userStorage->id;
                My_Staff::validateStaffForAsm($params);
            }

            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('email LIKE ?', str_replace(EMAIL_SUFFIX, '', $email) . EMAIL_SUFFIX);
            $data   = array('password' => md5($password1));
            $QStaff->update($data, $where);

            $flashMessenger->setNamespace('success')->addMessage('Success');
        } catch (Exception $e) {
            $flashMessenger->setNamespace('error')->addMessage(sprintf("[%s] %s", $e->getCode(), $e->getMessage()));
        }

        $this->_redirect(HOST . 'staff/reset-password');
    }

    private function _exportDateOffCsv($data) {
        ////////////////////////////////////////////////////
        /////////////////// KHỞI TẠO ĐỂ XUẤT CSV
        ////////////////////////////////////////////////////
        $db       = Zend_Registry::get('db');
        set_time_limit(0);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);
        $filename = 'List Off Date Purpose - ' . date('d-m-Y H-i-s');
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        // echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
        $output   = fopen('php://output', 'w');

        ////////////////////////////////////////////////////
        /////////////////// TỔNG HỢP DỮ LIỆU
        ////////////////////////////////////////////////////

        $heads = array(
            'Code',
            'Họ tên',
            'Khu vực',
            'Ngày nghỉ',
            'Lý do nghỉ',
            'Loại',
            'Ngày update',
            'Người update',
            'Note',
        );

        fputcsv($output, $heads);

        $staff_list = $db->query($data);

        if (!$staff_list)
            $staff_list = array();

        $i = 1;

        ////////////////////////////////////////////////////
        /////////////////// XUẤT RA CSV
        ////////////////////////////////////////////////////

        $arrApproved = array(
            -1 => 'Rejected',
            0  => 'Pending',
            1  => 'Approved',
        );

        $QRegionalMarket     = new Application_Model_RegionalMarket();
        $QArea               = new Application_Model_Area();
        $QStaffOffdateReason = new Application_Model_StaffDateoffReason();
        $QStaff              = new Application_Model_Staff();

        $regional_markets_all = $QRegionalMarket->get_cache_all();
        $areas_all            = $QArea->get_cache();

        $staff_dateoff_reason = $QStaffOffdateReason->get_cache();

        $staffs = array();

        foreach ($staff_list as $k => $v) {
            $v['date_off_purpose_update_by']    = ($v['date_off_purpose_update_by'] != '') ? $v['date_off_purpose_update_by'] : $v['staff_temp_created_by'];
            $where_staff_update_offdate_purpose = $QStaff->getAdapter()->quoteInto('id = ?', $v['date_off_purpose_update_by']);
            $staff_update_offdate_purpose       = $staff                              = $QStaff->fetchRow($where_staff_update_offdate_purpose);

            $staffs[$k]                                    = $v;
            $staffs[$k]['date_off_purpose_update_by_name'] = $staff_update_offdate_purpose['email'];
        }

        foreach ($staffs as $staff) {

            $date_off_purpose_update_at = '';
            $late                       = false;
            $note                       = '';
            $action                     = '';

            $date_off                   = ($staff['off_date'] != '') ? $staff['off_date'] : $staff['staff_temp_date_off_purpose'];
            $date_off_reason            = ($staff['date_off_purpose_reason'] != '') ? $staff['date_off_purpose_reason'] : $staff['staff_temp_date_off_purpose_reason'];
            $date_off_detail            = ($staff['date_off_purpose_detail'] != '') ? $staff['date_off_purpose_detail'] : $staff['staff_temp_date_off_purpose_detail'];
            $off_type                   = ($staff['off_type'] != '' && $staff['off_type'] != 0) ? $staff['off_type'] : $staff['staff_temp_off_type'];
            $date_off_purpose_update_at = ($staff['date_off_purpose_update_at'] != '') ? $staff['date_off_purpose_update_at'] : $staff['staff_temp_created_at'];
            if ($date_off_purpose_update_at != '') {
                $date_off_purpose_update_at_ex = explode(' ', $date_off_purpose_update_at);
                $date_off_purpose_update_at    = date('d/m/Y', strtotime($date_off_purpose_update_at_ex[0]));

                $datetime1 = new DateTime($date_off_purpose_update_at_ex[0]);
                $datetime2 = new DateTime($date_off);
                $interval  = $datetime1->diff($datetime2);

                if (preg_match('/\-/', $interval->format('%R%a days')) && $interval->format('%a') > 4)
                    $late = true;
            }

            if ($staff['off_date'] == '') {
                if (isset($staff['staff_temp_is_approved'])) {
                    $approve_name = (isset($arrApproved[$staff['staff_temp_is_approved']]) ? $arrApproved[$staff['staff_temp_is_approved']] : '');
                    $note         .= $approve_name . '.';
                }
            }

            if ($late == true)
                $note .= ' Cập nhật trễ';

            $row = array();

            $row[] = "=\"" . $staff['code'] . "\"";
            $row[] = $staff['firstname'] . ' ' . $staff['lastname'];
            $row[] = (isset($regional_markets_all[$staff['regional_market']]) && isset($areas_all[$regional_markets_all[$staff['regional_market']]['area_id']])) ? $areas_all[$regional_markets_all[$staff['regional_market']]['area_id']] : '';
            $row[] = "=\"" . date('d/m/Y', strtotime($date_off)) . "\"";
            $row[] = ((isset($staff_dateoff_reason[$date_off_reason])) ? $staff_dateoff_reason[$date_off_reason] : '') . '. ' . $date_off_detail;
            $row[] = isset(My_Staff_Status_Off::$name[$off_type]) ? My_Staff_Status_Off::$name[$off_type] : '';
            $row[] = "=\"" . $date_off_purpose_update_at . "\"";
            $row[] = $staff['date_off_purpose_update_by_name'];
            $row[] = $note;

            fputcsv($output, $row);
            unset($item);
            unset($row);
        }

        exit;
    }

    //Toan create 03062016
    public function _exportExcelScores($data) {

        ini_set("memory_limit", -1);
        ini_set("display_error", 0);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'No.',
            'AREA',
            'STAFF CODE',
            'STAFF NAME',
            'TITLE',
            'ATTENDANCE',
            'TOTAL OT',
            'KPI',
            'WARNING',
            'COURSE PASS',
            'COURSE FAIL',
            'TOTAL',
            'RESULT',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;


        $i = 1;

        foreach ($data as $item) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $i++);
            $sheet->setCellValue($alpha++ . $index, $item['area']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_code']);
            $sheet->setCellValue($alpha++ . $index, $item['firstname'] . ' ' . $item['lastname']);
            $sheet->setCellValue($alpha++ . $index, $item['title']);
            $sheet->setCellValue($alpha++ . $index, $item['total_attendance']);
            $sheet->setCellValue($alpha++ . $index, $item['total_ot']);
            $sheet->setCellValue($alpha++ . $index, $item['total_kpi']);
            $sheet->setCellValue($alpha++ . $index, $item['total_warning']);
            $sheet->setCellValue($alpha++ . $index, $item['total_pass']);
            $sheet->setCellValue($alpha++ . $index, $item['total_fail']);
            $sheet->setCellValue($alpha++ . $index, $item['total']);
            $sheet->setCellValue($alpha++ . $index, $item['result']);

            $index++;
        }

        $filename  = 'scores-list_' . date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

     public function lockUnlockUploadBankAreaAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'lock-unlock-upload-bank-area.php';
    }
    public function resetUploadBankAreaAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'reset-upload-bank-area.php';
    }
    // Toan create 03062016
    public function staffScoresAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'staff-scores.php';
    }

    public function staffScoresViewAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'staff-scores-view.php';
    }

    public function staffScoresDetailAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'staff-scores-detail.php';
    }

    public function timeKeepingAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'time-keeping.php';
    }

    public function listPhotoAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'listphoto.php';
    }

    public function uploadPhotoAjaxAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'upload-photo-ajax.php';
    }

    public function deletePhotoAjaxAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'delete-photo-ajax.php';
    }

    public function downloadZipAjaxAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'download-zip-ajax.php';
    }

    public function ajaxLoadOfficeAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'ajax-load-office.php';
    }

    public function printcardmassuploadAction() {
        if ($this->_request->isPost()) {
            $staff_code = $this->_request->getParam('staff_code');
            $staff_code = trim($staff_code);
            if ($staff_code) {
                $staff_code          = preg_replace('/[\s\s]+/', ' ', $staff_code);
                $staff_code          = preg_replace('/\s/', ',', $staff_code);
                $ms                  = new Application_Model_Staff;
                $res                 = $ms->UpdatePrintCardStatus($staff_code);
                if ($res)
                    $this->view->success = true;
                else
                    $this->view->error   = true;
            } else
                $this->view->error = true;
        }
    }

    private function contractInsert($staff_id) {
        $QStaff              = new Application_Model_Staff();
        $QStaffContract      = new Application_Model_StaffContract();
        $QContractType       = new Application_Model_ContractTypes();
        $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
        $QStaffSalary        = new Application_Model_StaffSalary();
        $QRegional_market    = new Application_Model_RegionalMarket;
        $Qteam               = new Application_Model_Team();


        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db          = Zend_Registry::get('db');

        try {
//            $db->beginTransaction();
            $where_staff   = array();
            $where_staff[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $Staff_Row     = $QStaff->fetchRow($where_staff);
            ///// Trường hợp PGPB PART TIME không tạo hợp đồng
            if ($Staff_Row['title'] == 375) {
                return array('code' => 1, 'message' => 'success');
            }
            $where_CProcess = array();

            $where_CProcess[]     = $Qteam->getAdapter()->quoteInto('id = ?', $Staff_Row['title']);
            $where_CProcess[]     = $Qteam->getAdapter()->quoteInto('del = 0');
            $QContractProcess_Row = $Qteam->fetchRow($where_CProcess);

            if (empty($QContractProcess_Row['contract_term'])) {
                return array('code' => -2, 'message' => "Title này chưa được khởi tạo hợp đồng.!");
            }
            $default_district = 4246; // default quan hoan kiem
            if (in_array($Staff_Row['title'], array(182, 293, 419, 420, 183, 417, 403, 545,577))) {
                $where_regional_market    = array();
                $where_regional_market[]  = $QRegional_market->getAdapter()->quoteInto('parent = ?', $Staff_Row['regional_market']);
                $where_regional_market[]  = $QRegional_market->getAdapter()->quoteInto('default_contract_district = ?', 1);
                $regional_market_district = $QRegional_market->fetchRow($where_regional_market);
                $default_district         = $regional_market_district['id'];


                // update staff_district
                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $data_update = array(
                    "district" => $default_district
                );
                $QStaff->update($data_update, $where_staff);
            }

            $policy_detail_id        = $salary                  = $salary_basic            = $salary_bonus            = $allowance               = $salary_min              = 0;
            $kpi1                    = $kpi                     = 0;
            $salary_total1           = $salary_total            = $allowance1              = $salary_insurance_salary = 0;
            $print_status            = 0;

            if (in_array($Staff_Row['title'], array(182, 293, 419, 420))) { //PGPB & SENIOR PROMOTER , PGPG Brandshop
//                $salary_policy_data = $QStaffContract->getSalaryDetailForPGByNew($staff_id);
                $salary_policy_data = $QStaffContract->getSalaryDetailByTitleDistrict(182, $default_district);
                if (empty($salary_policy_data)) {
                    return array('code' => -2, 'message' => "Không tìm thấy Policy của hợp đồng cho PG này.!");
                }
                $policy_detail_id = intval($salary_policy_data['id']);
                $PolicyDetail_Row = $salary_policy_data;

                $salary_basic = intval($PolicyDetail_Row['basic_insurance_salary']);

                $allowance     = intval($PolicyDetail_Row['allowance']);
                $allowance1    = intval($PolicyDetail_Row['allowance1']);
                $kpi1          = intval($PolicyDetail_Row['salary_kpi1']);
                $kpi           = intval($PolicyDetail_Row['salary_kpi']);
                $salary_min    = intval($PolicyDetail_Row['min_salary']);
                $salary_total  = intval($PolicyDetail_Row['total_company_salary']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
            } elseif (in_array($Staff_Row['title'], array(183, 164))) { //183, 164, 403, 162, 190, 312
//                $where_PolicyDetail   = array();
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(183));
////                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $Staff_Row['regional_market']);
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('district_id = ?', $default_district);
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
//                $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(183, $default_district);

                $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;
                $salary_total     = intval($PolicyDetail_Row['total_company_salary']);
                $salary_total1    = intval($PolicyDetail_Row['total_salary1']);
                $salary_basic     = intval($PolicyDetail_Row['basic_insurance_salary']);

                $allowance  = intval($PolicyDetail_Row['allowance']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $kpi1       = intval($PolicyDetail_Row['salary_kpi1']);
                $kpi        = intval($PolicyDetail_Row['salary_kpi']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                if ($policy_detail_id == 0) {
                    return array('code' => -2, 'message' => "Không tìm thấy Policy của hợp đồng cho SALE này.!");
                }
            } elseif (in_array($Staff_Row['title'], array(190))) { //Sale leader (chính sách sale leader)
                $salary_insurance_salary = 6000000;
                $kpi                     = 6000000;
                $kpi1                    = 6000000;
                $salary_basic            = 6000000;
                $salary_total1           = 6000000;
                $salary_total            = 6000000;
                $salary_min              = 0;
                $allowance               = 0;
                $allowance1              = 0;
            } elseif (in_array($Staff_Row['title'], array(417))) { //PG leader (chính sách PG leader)
//                $where_PolicyDetail   = array();
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(417));
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
////              $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $Staff_Row['regional_market']);
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('district_id = ?', $default_district);
//                $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(417, $default_district);

                $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;
                $salary_total     = intval($PolicyDetail_Row['total_company_salary']);
                $salary_total1    = intval($PolicyDetail_Row['total_salary1']);
                $salary_basic     = intval($PolicyDetail_Row['basic_insurance_salary']);

                $allowance  = intval($PolicyDetail_Row['allowance']);
                $allowance1 = intval($PolicyDetail_Row['allowance1']);
                $kpi1       = intval($PolicyDetail_Row['salary_kpi1']);
                $kpi        = intval($PolicyDetail_Row['salary_kpi']);
                $salary_min = intval($PolicyDetail_Row['min_salary']);
                if ($policy_detail_id == 0) {
                    return array('code' => -2, 'message' => "Không tìm thấy Policy của hợp đồng cho PG LEADER này.!");
                }
            } elseif (in_array($Staff_Row['title'], array(403))) { //Store leader (chính sách store leader )  // lương KPI=6tr, tổng lương 6tr
//                $where_PolicyDetail   = array();
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(403));
////                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $Staff_Row['regional_market']);
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('district_id = ?', $default_district);
//                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
//                $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict(403, $default_district);

                $policy_detail_id = intval($PolicyDetail_Row['id']);
                if (!$policy_detail_id) {
                    throw new Exception('Không tìm thấy chính sách lương STORE LEADER cho nhân viên này');
                }
                $salary_basic  = intval($PolicyDetail_Row['basic_insurance_salary']);
                $kpi1          = intval($PolicyDetail_Row['salary_kpi1']);
                $kpi           = intval($PolicyDetail_Row['salary_kpi']);
                $salary_min    = intval($PolicyDetail_Row['min_salary']);
                $salary_total  = intval($PolicyDetail_Row['total_company_salary']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $allowance     = intval($PolicyDetail_Row['allowance']);
                $allowance1    = intval($PolicyDetail_Row['allowance1']);
            } elseif (in_array($Staff_Row['title'], array(545,577))) { //PRODUCT CONSULTANT (chính sách PRODUCT CONSULTANT )  // lương KPI=6tr, tổng lương 6tr

                $PolicyDetail_Row = $QStaffContract->getSalaryDetailByTitleDistrict($Staff_Row['title'], $default_district);

                $policy_detail_id = intval($PolicyDetail_Row['id']);
                if (!$policy_detail_id) {
                    throw new Exception('Không tìm thấy chính sách lương PRODUCT CONSULTANT  cho nhân viên này');
                }
                $salary_basic  = intval($PolicyDetail_Row['basic_insurance_salary']);
                $kpi1          = intval($PolicyDetail_Row['salary_kpi1']);
                $kpi           = intval($PolicyDetail_Row['salary_kpi']);
                $salary_min    = intval($PolicyDetail_Row['min_salary']);
                $salary_total  = intval($PolicyDetail_Row['total_company_salary']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $allowance     = intval($PolicyDetail_Row['allowance']);
                $allowance1    = intval($PolicyDetail_Row['allowance1']);
            } elseif (in_array($Staff_Row['title'], array(424))) { //Sale leader (chính sách Sale leader )  // 
                $where_PolicyDetail   = array();
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(424));
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $Staff_Row['regional_market']);
                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);

                $policy_detail_id = intval($PolicyDetail_Row['id']);
                if (!$policy_detail_id) {
                    throw new Exception('Không tìm thấy chính sách lương SALE LEADER BS cho nhân viên này');
                }
                $salary_basic  = intval($PolicyDetail_Row['basic_insurance_salary']);
                $kpi1          = intval($PolicyDetail_Row['salary_kpi1']);
                $kpi           = intval($PolicyDetail_Row['salary_kpi']);
                $salary_min    = intval($PolicyDetail_Row['min_salary']);
                $salary_total  = intval($PolicyDetail_Row['total_company_salary']);
                $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                $allowance     = intval($PolicyDetail_Row['allowance']);
                $allowance1    = intval($PolicyDetail_Row['allowance1']);
            } else {
                $print_status = 1;
            }


            $StaffContract_id   = $StaffContract_id_2 = $StaffSalary_id     = 0;


            ///// insert staff_salary
            $data_staff_salary = array(
                'staff_id'         => $Staff_Row['id'],
                'insurance_salary' => 0,
                'work_cost_salary' => 0,
                'from_date'        => $Staff_Row['joined_at'],
                'created_at'       => date('Y-m-d H:i:s'),
                'created_by'       => $userStorage->id,
                'note'             => "Insert from create new"
            );

            $StaffSalary_id = $QStaffSalary->insert($data_staff_salary);

            /////////// Row1
            $where_CType1       = array();
            $where_CType1[]     = $QContractType->getAdapter()->quoteInto('id = ?', $QContractProcess_Row['contract_term']);
            $QContractType_Row1 = $QContractType->fetchRow($where_CType1)->toArray();

            $addDay1  = (isset($QContractType_Row1['term']) and $QContractType_Row1['term']) ? $QContractType_Row1['term'] : 0;
            $to_date1 = date('Y-m-d', strtotime("+" . $addDay1 . " days", strtotime($Staff_Row['joined_at'])));

            $data_contract1 = array(
                'company_id'              => $Staff_Row['company_id'],
                'staff_id'                => $Staff_Row['id'],
                'from_date'               => $Staff_Row['joined_at'],
                'to_date'                 => $to_date1,
                'system_note'             => "Insert Auto Row1",
                'title'                   => $Staff_Row['title'],
                'regional_market'         => $Staff_Row['regional_market'],
                'contract_term'           => $QContractProcess_Row['contract_term'],
                'status'                  => 2, // 0: chưa approve, 1: Khu vực approve, 2: HR approve
                'print_type'              => 1, // 1: Hợp đồng, 2: phụ lục
                'created_at'              => date('Y-m-d H:i:s'),
                'created_by'              => $userStorage->id,
                'salary_id'               => $StaffSalary_id,
                'salary_total'            => $salary_total1,
                'salary_bonus'            => $allowance1,
                'salary_min'              => $salary_min,
                'salary_kpi'              => $kpi1,
                'is_next'                 => 2,
                'salary_policy_detail_id' => $policy_detail_id,
                'print_status'            => $print_status
            );
            // if (in_array($Staff_Row['title'], array(182, 293, 419, 420, 183, 417, 403,545))) {
			if (in_array($Staff_Row['title'], array(182, 293, 419, 420, 417, 403,545))) { // Update chuc danh sale set district_confirm = 1
				
                $data_contract1['district_id']      = $default_district;
                $data_contract1['district_confirm'] = 0;
//                $data_contract1['is_disable']       = 1;
            }
            $StaffContract_id = $QStaffContract->insert($data_contract1);


            // update staff
            $data_staff                        = [];
            $data_staff['contract_term']       = $QContractProcess_Row['contract_term'];
            $data_staff['contract_signed_at']  = $Staff_Row['joined_at'];
            $data_staff['contract_expired_at'] = $to_date1;

            $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $QStaff->update($data_staff, $where_staff);


            /////////// Row2
            $where_CType2       = array();
            $where_CType2[]     = $QContractType->getAdapter()->quoteInto('id = 1'); // default 1 nam
            $QContractType_Row2 = $QContractType->fetchRow($where_CType2)->toArray();

            $addDay2from = (isset($QContractType_Row2['start_date']) and $QContractType_Row2['start_date']) ? $QContractType_Row2['start_date'] : 0;
            if (in_array($QContractProcess_Row['contract_term'], array(3))) {
                $addDay2from = 5;
            }
            $from_date2        = date('Y-m-d', strtotime("+" . $addDay2from . " days", strtotime($to_date1)));
            $from_date_payment = date('Y-m-d', strtotime("+1 days", strtotime($to_date1)));

            $addDay2to = (isset($QContractType_Row2['term']) and $QContractType_Row2['term']) ? $QContractType_Row2['term'] : 0;
            $to_date2  = date('Y-m-d', strtotime("+" . $addDay2to . " days", strtotime($from_date2)));

            $data_contract2     = array(
                'company_id'              => $Staff_Row['company_id'],
                'staff_id'                => $Staff_Row['id'],
                'from_date'               => $from_date2,
                'to_date'                 => $to_date2,
                'system_note'             => "Insert Auto Row2",
                'title'                   => $Staff_Row['title'],
                'regional_market'         => $Staff_Row['regional_market'],
                'contract_term'           => 1,
                'status'                  => 0, // 0: chưa approve, 1: Khu vực approve, 2: HR approve
                'print_type'              => 1, // 1: Hợp đồng, 2: phụ lục
                'created_at'              => date('Y-m-d H:i:s'),
                'created_by'              => $userStorage->id,
                'salary_policy_detail_id' => $policy_detail_id,
                'salary_id'               => $StaffSalary_id,
                'salary_total'            => $salary_total,
                'salary_bonus'            => $allowance,
                'salary_kpi'              => $kpi,
                'is_next'                 => 1, // đánh dấu HĐ tiếp theo
                'salary_insurance_temp'   => $salary_basic,
                'old_contract_id'         => $StaffContract_id,
                'from_date_payment'       => $from_date_payment,
            );
            $StaffContract_id_2 = $QStaffContract->insert($data_contract2);


            if (empty($StaffContract_id) || empty($StaffContract_id_2) || empty($StaffSalary_id)) {
                return array('code' => -2, 'message' => "Insert Contract không thành công.!");
            }
//            $db->commit();
            //successful
            return array('code' => 1, 'message' => 'success');
        } catch (exception $e) {
//            $db->rollback();
            return array('code' => -1, 'message' => $e->getMessage());
        }
    }

    public function updateDeviceAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'update-device.php';
    }

    public function updateShiftAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'update-shift.php';
    }

    public function updateLockedPhotoAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'update-locked-photo.php';
    }

    public function refreshTimeAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'refresh-time.php';
    }

    public function delBasicAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'del-basic.php';
    }

    public function createTransferAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'create-transfer.php';
    }

    public function checkStatusTransferAction() {
        $staff_id       = $this->getRequest()->getParam('staff_id');
        $QStaffTransfer = new Application_Model_StaffTransfer();
        $where          = $QStaffTransfer->getAdapter()->quoteInto('status = 1 and staff_id = ?', $staff_id);
        $rs             = $QStaff->fetchAll($where);

        echo "<pre>";
        print_r($rs);
        die;
    }

    public function listCmndAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-cmnd.php';
    }

    public function listQuitAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-quit.php';
    }

    public function exportListQuit($data) {
        $db       = Zend_Registry::get('db');
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Code',
            'B' => 'First name',
            'C' => 'Last name',
            'D' => 'Company',
            'E' => 'Department',
            'F' => 'Team',
            'G' => 'Title',
            'H' => 'Area',
            'I' => 'Province',
            'J' => 'Joined at',
            'K' => 'Off date',
            'L' => 'Off type',
            'M' => 'Gender',
            'N' => 'Certificate',
            'O' => 'ID'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach ($heads as $key => $value) {
            $sheet->setCellValue($key . '1', $value);
        }

        if (gettype($data) == 'string') {
            $data_arr = array();
            $data_arr = $db->fetchAll($data);
        } else {
            $data_arr = $data;
        }

        //get department
        $QDepartment = new Application_Model_Department();
        $departments = $QDepartment->get_cache();

        //get teams
        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();

        //get regional markets
        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache_all();

        //get contract type
        $QContractType  = new Application_Model_ContractType();
        $contract_types = $QContractType->get_cache();

        //get contract term
        $QNationality  = new Application_Model_Nationality();
        $nationalities = $QNationality->get_cache();

        //get contract term
        $QReligion = new Application_Model_Religion();
        $religions = $QReligion->get_cache();

        $QCompany  = new Application_Model_Company();
        $companies = $QCompany->get_cache();

        foreach ($data_arr as $key => $item) {
            $sheet->setCellValue('A' . ($key + 2), $item['code']);
            $sheet->setCellValue('B' . ($key + 2), $row[] = $item['firstname']);
            $sheet->setCellValue('C' . ($key + 2), $item['lastname']);
            $sheet->setCellValue('D' . ($key + 2), isset($companies[$item['company_id']]) ? $companies[$item['company_id']] : '');
            $sheet->setCellValue('E' . ($key + 2), isset($teams[$item['department']]) ? $teams[$item['department']] : '');
            $sheet->setCellValue('F' . ($key + 2), isset($teams[$item['team']]) ? $teams[$item['team']] : '');
            $sheet->setCellValue('G' . ($key + 2), isset($teams[$item['title']]) ? $teams[$item['title']] : '');

            $sheet->setCellValue('H' . ($key + 2), isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]) && isset($regional_markets[$item['regional_market']]['area_id']) && isset($areas[$regional_markets[$item['regional_market']]['area_id']]) ? $areas[$regional_markets[$item['regional_market']]['area_id']] : '');
            $sheet->setCellValue('I' . ($key + 2), isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]['name']) ? $regional_markets[$item['regional_market']]['name'] : '');

            $sheet->setCellValue('J' . ($key + 2), $item['joined_at'] ? ("=\"" . date('d/m/Y', strtotime($item['joined_at'])) . "\"") : '');
            $sheet->setCellValue('K' . ($key + 2), $item['off_date'] ? ("\"" . date('d/m/Y', strtotime($item['off_date'])) . "\"") : '');
            $sheet->setCellValue('L' . ($key + 2), isset(My_Staff_Status_Off::$name[$item['off_type']]) ? My_Staff_Status_Off::$name[$item['off_type']] : '');
            $sheet->setCellValue('M' . ($key + 2), ($item['gender'] == 1 ? 'Nam' : 'Nữ'));

            if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? ("=\"" . date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y") . "\"") : "";
            } else {
                $dob = $item['dob'];
            }

            $sheet->setCellValue('N' . ($key + 2), $dob);

            $sheet->setCellValue('O' . ($key + 2), $item['id']);
        }

        $filename  = 'List Quit - ' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        exit;
    }

    public function delDeviceAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'del-device.php';
    }

    public function getProviceCodeById($id) {
        $province_code   = NULL;
        $QRegionalMarket = new Application_Model_RegionalMarket();
        $rowSet          = $QRegionalMarket->find($id);
        $regionalMarket  = $rowSet->current();
        $province_code   = $regionalMarket['code_insurance'];

        return $province_code;
    }

    //TUONG import file excel data staff 1.bank,2.mst,3.dependent person
    public function uploadDataStaffAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'upload-data-staff.php';
    }

    public function uploadBankAreaAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'upload-bank-area.php';
    }

    public function saveUploadBankAreaAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'save-upload-bank-area.php';
    }

    public function viewBankAreaAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'view-bank-area.php';
    }
    public function viewPtiAreaAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'view-pti-area.php';
    }

    public function timeMachineAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'time-machine.php';
    }

    public function createLogAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'create-log.php';
    }

    public function updateBodyInfoAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'update-body-info.php';
    }

    public function listUpdateInforAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-update-infor.php';
    }

    public function listUpdateDetailAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-update-detail.php';
    }

    public function listUpdateApproveAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'list-update-approve.php';
    }

    public function uploadFileAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'upload-file.php';
    }

    public function saveUploadFileAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'save-upload-file.php';
    }

    public function ptiDeleteTempAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'pti-delete-temp.php';
    }
    private function _exportxlsxASMNew($data) {
        $db = Zend_Registry::get('db');

        error_reporting(~E_ALL);
        ini_set('display_error', 0);


        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', '5120M');
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        ////////////////////////////////////////////////////
        /////////////////// T?NG H?P D? LI?U ///////////////
        ////////////////////////////////////////////////////
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        //get department
        $QDepartment = new Application_Model_Department();
        $departments = $QDepartment->get_cache();

        //get teams
        $QTeam = new Application_Model_Team();
        $teams = $QTeam->get_cache();

        //get titles
        /* $QTitle         = new Application_Model_Title();
          $titles           = $QTitle->get_cache(); */

        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();

        //get regional markets
        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache_all();

        //get contract type
        $QContractType  = new Application_Model_ContractType();
        $contract_types = $QContractType->get_cache();

        $QContractTerm  = new Application_Model_ContractTerm();
        $contract_terms = $QContractTerm->get_cache();

        //get contract term
        $QNationality  = new Application_Model_Nationality();
        $nationalities = $QNationality->get_cache();

        //get contract term
        $QReligion = new Application_Model_Religion();
        $religions = $QReligion->get_cache();

        $QCompany  = new Application_Model_Company();
        $companies = $QCompany->get_cache();

        $QOffice      = new Application_Model_Office();
        $office_cache = $QOffice->get_cache();

        $staff_list = $db->query($data);
        if (!$staff_list)
            $staff_list = array();

        $i      = 1;
        $result = array();

        foreach ($staff_list as $item) {
            $row = array();

            $row[] = $item['code'];
            $row[] = $item['firstname'] . ' ' .$item['lastname'];
          //  $row[] = $item['lastname'];
            $row[] = isset($companies[$item['company_id']]) ? $companies[$item['company_id']] : '';
            $row[] = isset($teams[$item['department']]) ? $teams[$item['department']] : '';
            $row[] = isset($teams[$item['team']]) ? $teams[$item['team']] : '';
            $row[] = isset($teams[$item['title']]) ? $teams[$item['title']] : '';

            $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]) && isset($regional_markets[$item['regional_market']]['area_id']) && isset($areas[$regional_markets[$item['regional_market']]['area_id']]) ? $areas[$regional_markets[$item['regional_market']]['area_id']] : '';
            $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]['name']) ? $regional_markets[$item['regional_market']]['name'] : '';
            //  $row[] = isset($item['district_name_insurance']) ? trim($item['district_name_insurance']) : '';
            $row[] = $item['ID_number'];
            $row[] = $item['id_place_province_name'];
            $row[] = $item['ID_date'] ? (date('d/m/Y', strtotime($item['ID_date']))) : '';
            $row[] = $item['joined_at'] ? (date('d/m/Y', strtotime($item['joined_at']))) : '';

            if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? (date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y")) : "";
            } else {
                $dob = $item['dob'];
            }

            $row[] = $dob;
            $row[] = $item['off_date'] ? ( date('d/m/Y', strtotime($item['off_date']))) : '';
            $row[] = isset(My_Staff_Status_Off::$name[$item['off_type']]) ? My_Staff_Status_Off::$name[$item['off_type']] : '';
            $row[] = ($item['gender'] == 1 ? 'Nam' : 'Nữ');

            if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? ("=\"" . date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y") . "\"") : "";
            } else {
                $dob = $item['dob'];
            }

            // $row[] = $dob;

            $office_id = $item['office_id'];

            if (!empty($item['brand_shop_address'])) {
                $row[] = $item['brand_shop_address'];
            } else {
                if (!empty($office_id)) {
                    $row[] = $office_cache[$office_id];
                } else {
                    $row[] = NULL;
                }
            }
            $row[] =   My_Staff_Status::get($item['status']);  
            $row[] =   $item['bank_number'];  
            $row[] =   $item['at_bank'];  
            $row[] =   $item['branch'];  
            $row[] =   $item['province_city'];  
           
            if (in_array($userStorage->team, array(131, 133)) OR $userStorage->id == 150) { // add $userStorage->id == 150 (tam.tran) at 2/3/2020
                $row[] = $item['phone_number'];
                $row[] = $item['email'];
            }
            $row[] = $item['tax'];
            array_push($result, $row);
        }
//        debug($result); return;
        $heads = array(
            'Code',
            'Full name',
            'Company',
            'Department',
            'Team',
            'Title',
            'Area',
            'Province',
            //  'District',
            'ID number',
            'ID place',
            'ID date',
            'Joined at',
            'Date of birth',
            'Off date',
            'Off type',
            'Gender',
            // 'Certificate',
            // 'ID',
            'Office Location',
            'Status',
            'Bank_Number',
            'Bank_At',
            'Bank_Branch',
            'Bank_Province',
                // 'Contract signed at',
                // 'Contract term',
                // 'Contract expired at',
        );

        if (in_array($userStorage->team, array(131, 133)) OR $userStorage->id == 150) { // add $userStorage->id == 150 (tam.tran) at 2/3/2020
            array_push($heads, "phone number", "email");
        }
		array_push($heads, "PIT");
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        foreach ($result as $key => $value):
            $alpha = 'A';

            for ($x = 0; $x <= count($value); $x++) {

//                debug($value);exit;
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value[$x], PHPExcel_Cell_DataType::TYPE_STRING);
            }

            $index++;
        endforeach;

        $filename  = 'Staff List for ASM - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $filename . '.xlsx');
        $objWriter->save('php://output');

        exit;
    }

    public function ajaxGetLastCheckInAction() {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'ajax-get-last-check-in.php';
    }

    function convertViToEn($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", "e", $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", "i", $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $str);
        $str = preg_replace("/(đ)/", "d", $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "A", $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "I", $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "O", $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "U", $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "Y", $str);
        $str = preg_replace("/(Đ)/", "D", $str);
        //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return $str;
    }

    private function _exportxlsxNew($data) {
       
        $db       = Zend_Registry::get('db');
        set_time_limit(0);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();



        ////////////////////////////////////////////////////
        /////////////////// TỔNG HỢP DỮ LIỆU
        ////////////////////////////////////////////////////
        //get department
        $QDepartment      = new Application_Model_Department();
        $departments      = $QDepartment->get_cache();
        //get teams
        $QTeam            = new Application_Model_Team();
        $teams            = $QTeam->get_cache();
        $QArea            = new Application_Model_Area();
        $areas            = $QArea->get_cache();
        //get regional markets
        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache_all();
        $districts        = $QRegionalMarket->get_district_cache();
        // get wards
        $Qward            = new Application_Model_Ward;
        $wards            = $Qward->get_cache2();
        //get contract type
        $QContractType    = new Application_Model_ContractType();
        $contract_types   = $QContractType->get_cache();
        //get contract term
        $QContractTerm    = new Application_Model_ContractTypes();
        $contract_terms   = $QContractTerm->get_cache();
        
        //get contract term
        $QNationality     = new Application_Model_Nationality();
        $nationalities    = $QNationality->get_cache();
        //get contract term
        $QReligion        = new Application_Model_Religion();
        $religions        = $QReligion->get_cache();
        $QCompany         = new Application_Model_Company();
        $companies        = $QCompany->get_cache();

        $QTag         = new Application_Model_Tag();
        $QTagObject   = new Application_Model_TagObject();
        $QOff         = new Application_Model_StaffDateoffReason();
        $off_cache    = $QOff->get_cache();
        $QOffice      = new Application_Model_Office();
        $office_cache = $QOffice->get_cache();
        $QGroup       = new Application_Model_Group();
        $groups_cache = $QGroup->get_cache();

        //Province District
        $QProvinceDistrict      = new Application_Model_ProvinceDistrict();
        $provinceDistrict_cache = $QProvinceDistrict->get_cache();

        $stmt       = $db->query($data);
        $staff_list = $stmt->fetchAll();
        $stmt->closeCursor();

        $i     = 1;
//        debug($staff_list); exit;
        $heads = array(
            'Code',
            'Full Name',
            // 'Last name',
            'Company',
            'Department',
            'Team',
            'Title',
            'Title Id',
            'Area',
            'Province',
            'District',
            'District code',
            'Contract type',
            'Contract signed at',
            'Contract term',
            'Contract expired at',
            'Joined at',
            'Off date',
            'Off date reason',
            'Off date reason detail',
            'Off type',
            'Gender',
            'Date of birth',
            'Level',
//            'Level new',
            'Certificate',
            'ID Card Address Street',
            'ID Card Address Ward',
            'ID Card Address District',
            'ID Card Address Province',
            'Temporary Address Street',
            'Temporary Address Ward',
            'Temporary Address District',
            'Temporary Address Province',
            'Permanent address Street',
            'Permanent address Ward',
            'Permanent address District',
            'Permanent address Province',
            'Birth place Ward',
            'Birth place District',
            'Birth place Province',
            'ID number',
            'ID place',
            'ID date',
            'Nationality',
            'Religion',
            'Phone number',
            'Email',
            'Status',
            'Note',
            'Tags',
            'Group Center',
            'Policy Group',
            'Group',
            'Office Location',
//            'Title Id',
            'Province Id',
            //TUONG
            // 'Pit_Name',
            // 'Pit_Dob',
            // 'Pit_Mst',
            // 'Pit_relative',
            // 'Pit_Frommonth',
            // 'Pit_Tomonth',
            'Bank_Number',
            'Bank_At',
            'Bank_Branch',
            'Bank_Province',
            'PTI',
                //TUONG
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

//        foreach ($result as $key => $value):
//            $alpha = 'A';
//            for ($x = 0; $x <= count($value); $x++) {
////                debug($value);exit;
//                $sheet->getCell($alpha++ . $index)->setValueExplicit($value[$x], PHPExcel_Cell_DataType::TYPE_STRING);
//            }
//
//            $index++;
//        endforeach;
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    if($userStorage->id == 5899){
    /*             echo "<pre>";
        print_r($staff_list);
        die;
     */
	 }

        if (!empty($staff_list)) {

            foreach ($staff_list as $item) {
                $alpha = 'A';
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['firstname'] . ' ' . $item['lastname'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($companies[$item['company_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($teams[$item['department']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($teams[$item['team']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($teams[$item['title']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['title'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($areas[$regional_markets[$item['regional_market']]['area_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($regional_markets[$item['regional_market']]['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($item['district_name_insurance']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['district_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($contract_types[$item['contract_type']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['contract_from'] ? (date('d/m/Y', strtotime($item['contract_from']))) : '', PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($contract_terms[$item['staff_contract_term']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['contract_to'] ? (date('d/m/Y', strtotime($item['contract_to']))) : '', PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['joined_at'] ? (date('d/m/Y', strtotime($item['joined_at']))) : '', PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['off_date'] ? (date('d/m/Y', strtotime($item['off_date']))) : '', PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($off_cache[$item['date_off_purpose_reason']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['date_off_purpose_detail'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(My_Staff_Status_Off::$name[$item['off_type']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(($item['gender'] == 1 ? 'Nam' : 'Nữ'), PHPExcel_Cell_DataType::TYPE_STRING);
                if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
                    $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? (date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y")) : "";
                } else {
                    $dob = $item['dob'];
                }
                $sheet->getCell($alpha++ . $index)->setValueExplicit($dob, PHPExcel_Cell_DataType::TYPE_STRING);
//                $sheet->getCell($alpha++ . $index)->setValueExplicit(1, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['level_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['d_certificate'], PHPExcel_Cell_DataType::TYPE_STRING);


                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['id_c_address'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($wards[$item['id_c_ward']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($districts[$item['id_c_district']]['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                // $sheet->getCell($alpha++ . $index)->setValueExplicit($provinceDistrict_cache[$item['id_c_province_code']], PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getCell($alpha++ . $index)->setValueExplicit($item['id_c_province_name'], PHPExcel_Cell_DataType::TYPE_STRING);


                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['t_address'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($wards[$item['t_ward']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($districts[$item['t_district']]['name'], PHPExcel_Cell_DataType::TYPE_STRING);
               // $sheet->getCell($alpha++ . $index)->setValueExplicit($provinceDistrict_cache[$item['t_province_name']], PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getCell($alpha++ . $index)->setValueExplicit($item['t_province_name'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['p_address'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($wards[$item['p_ward']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($districts[$item['p_district']]['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                //$sheet->getCell($alpha++ . $index)->setValueExplicit($provinceDistrict_cache[$item['p_province_code']], PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getCell($alpha++ . $index)->setValueExplicit($item['p_province_name'], PHPExcel_Cell_DataType::TYPE_STRING);



//                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['bd_address'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($wards[$item['bd_ward']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($districts[$item['bd_district']]['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                // $sheet->getCell($alpha++ . $index)->setValueExplicit($provinceDistrict_cache[$item['bd_province_code']], PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->getCell($alpha++ . $index)->setValueExplicit($item['bd_province_name'], PHPExcel_Cell_DataType::TYPE_STRING);


                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['ID_number'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['id_place_province_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['ID_date'] ? (date('d/m/Y', strtotime($item['ID_date']))) : '', PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($nationalities[$item['nationality']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($religions[$item['religion']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['phone_number'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['email'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(My_Staff_Status::get($item['status']), PHPExcel_Cell_DataType::TYPE_STRING);

                // print_r(str_replace("\n", " ", $item['note'])); die;
                $sheet->getCell($alpha++ . $index)->setValueExplicit(str_replace(array("\r\n", "\n\r", "\n", "\r"), " ", $item['note']), PHPExcel_Cell_DataType::TYPE_STRING);


                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['tag_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($groups_cache[$item['access_group']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['policy_group_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['company_group_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                if (!empty($item['brand_shop_address'])) {
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($item['brand_shop_address'], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                    if (!empty($item['office_id'])) {
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($office_cache[$item['office_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                    } else {
                        $row[] = NULL;
                        $sheet->getCell($alpha++ . $index)->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);
                    }
                }
//            $row[] = $item['title'];
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['regional_market'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['bank_number'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['at_bank'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['branch'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['province_city'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['tax'], PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
        }
//        $result = array();
//        if (!empty($staff_list)) {
//            foreach ($staff_list as $item) {
//                $row   = array();
//                $row[] = $item['code'];
//                $row[] = $item['firstname'] . ' ' . $item['lastname'];
//                // $row[] =
//                // $item['lastname'];
//                $row[] = isset($companies[$item['company_id']]) ? $companies[$item['company_id']] : '';
//                $row[] = isset($teams[$item['department']]) ? $teams[$item['department']] : '';
//                $row[] = isset($teams[$item['team']]) ? $teams[$item['team']] : '';
//                $row[] = isset($teams[$item['title']]) ? $teams[$item['title']] : '';
//                $row[] = isset($item['title']) ? $item['title'] : '';
//
//                $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]) && isset($regional_markets[$item['regional_market']]['area_id']) && isset($areas[$regional_markets[$item['regional_market']]['area_id']]) ? $areas[$regional_markets[$item['regional_market']]['area_id']] : '';
//                $row[] = isset($item['regional_market']) && isset($regional_markets[$item['regional_market']]['name']) ? $regional_markets[$item['regional_market']]['name'] : '';
//                $row[] = isset($item['district_name_insurance']) ? trim($item['district_name_insurance']) : '';
//                $row[] = isset($item['district_code']) ? $item['district_code'] : '';
//                //$row[] = $item['is_officer'] ? 'X' : '';
//                $row[] = isset($contract_types[$item['contract_type']]) ? ($contract_types[$item['contract_type']]) : '';
//                $row[] = $item['contract_signed_at'] ? (date('d/m/Y', strtotime($item['contract_signed_at']))) : '';
//                $row[] = isset($contract_terms[$item['contract_term']]) ? $contract_terms[$item['contract_term']] : '';
//                $row[] = $item['contract_expired_at'] ? (date('d/m/Y', strtotime($item['contract_expired_at']))) : '';
//                $row[] = $item['joined_at'] ? (date('d/m/Y', strtotime($item['joined_at']))) : '';
//                $row[] = $item['off_date'] ? (date('d/m/Y', strtotime($item['off_date']))) : '';
//                $row[] = isset($off_cache[$item['date_off_purpose_reason']]) ? $off_cache[$item['date_off_purpose_reason']] : '';
//                $row[] = $item['date_off_purpose_detail'];
//                $row[] = isset(My_Staff_Status_Off::$name[$item['off_type']]) ? My_Staff_Status_Off::$name[$item['off_type']] : '';
//                $row[] = ($item['gender'] == 1 ? 'Nam' : 'Nữ');
//
//                if ($item['dob'] AND preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', trim($item['dob']))) {
//                    $dob = $item['dob'] && date_create_from_format('d/m/Y', $item['dob']) ? (date_create_from_format('d/m/Y', $item['dob'])->format("d/m/Y")) : "";
//                } else {
//                    $dob = $item['dob'];
//                }
//
//                $row[] = $dob;
//
////            $row[] = "=\"" . $item['d_level'] . "\"";
//                $row[] = $item['level_name'];
//                $row[] = $item['d_certificate'];
//
//                // SELECT ADDRESS
////            $select        = $db->select()
////                ->from(array('s' => 'staff_address'), array(
////                    'type'          => 's.address_type',
//////                        'province_code' => 'IFNULL(s.province_code,r2.code_insurance)',
////                    'province_code' => 's.province_code',
////                    'address'       => 's.address',
////                    'ward'          => 's.ward',
////                    'district'      => 'r.name',
////                    'province'      => 'r2.name',
////                    'ward_name'     => 'w.name',
////                    'ward_id',
////                ))
////                ->join(array('r' => 'regional_market'), 'r.id = s.district', array())
////                ->join(array('r2' => 'regional_market'), 'r.parent = r2.id', array())
////                ->joinLeft(array('w' => 'ward'), 'w.id = s.ward_id', array())
////                ->where('s.staff_id = ?', $item['id'])
////                ->order('s.address_type ASC');
////            $arrAddress    = $db->fetchAll($select);
////            $arrTmpAddress = array();
////            foreach ($arrAddress as $add):
////                $arrTmpAddress[$add['type']] = $add;
////            endforeach;
////            unset($arrAddress);
////            unset($select);
////            unset($add);
////            if (isset($arrTmpAddress[My_Staff_Address::ID_Card])) {
////                $province_code = $arrTmpAddress[My_Staff_Address::ID_Card]['province_code'];
////                $row[]         = $arrTmpAddress[My_Staff_Address::ID_Card]['address'];
////                $row[]         = ($arrTmpAddress[My_Staff_Address::ID_Card]['ward_id']) ? $arrTmpAddress[My_Staff_Address::ID_Card]['ward_name'] : $arrTmpAddress[My_Staff_Address::ID_Card]['ward'];
////                $row[]         = $arrTmpAddress[My_Staff_Address::ID_Card]['district'];
////                $row[]         = $provinceDistrict_cache[$province_code];
////                // $row[]         = $item['id_place_province_name'];
////            } else {
////                $row[] = "";
////                $row[] = "";
////                $row[] = "";
////                $row[] = "";
////            }
//                $province_code   = $item['id_c_province_code'];
//                $row[]           = $item['id_c_address'];
//                $row[]           = ($item['id_c_ward_id']) ? $item['id_c_ward_name'] : $item['id_c_ward'];
//                $row[]           = $item['id_c_district'];
//                $row[]           = $provinceDistrict_cache[$province_code];
//                // $row[]         = $item['id_place_province_name'];
////            if (isset($arrTmpAddress[My_Staff_Address::Temporary])) {
////                $province_code = $arrTmpAddress[My_Staff_Address::Temporary]['province_code'];
////                $row[]         = $arrTmpAddress[My_Staff_Address::Temporary]['address'];
////                $row[]         = ($arrTmpAddress[My_Staff_Address::Temporary]['ward_id']) ? $arrTmpAddress[My_Staff_Address::Temporary]['ward_name'] : $arrTmpAddress[My_Staff_Address::Temporary]['ward'];
////                $row[]         = $arrTmpAddress[My_Staff_Address::Temporary]['district'];
////                $row[]         = $provinceDistrict_cache[$province_code];
////            } else {
////                $row[] = "";
////                $row[] = "";
////                $row[] = "";
////                $row[] = "";
////            }
//                $province_code_t = $item['t_province_code'];
//                $row[]           = $item['t_address'];
//                $row[]           = ($item['t_ward_id']) ? $item['t_ward_name'] : $item['t_ward'];
//                $row[]           = $item['t_district'];
//                $row[]           = $provinceDistrict_cache[$province_code_t];
//
////            if (isset($arrTmpAddress[My_Staff_Address::Permanent])) {
////                $province_code = $arrTmpAddress[My_Staff_Address::Permanent]['province_code'];
////                $row[]         = $arrTmpAddress[My_Staff_Address::Permanent]['address'];
////                $row[]         = ($arrTmpAddress[My_Staff_Address::Permanent]['ward_id']) ? $arrTmpAddress[My_Staff_Address::Permanent]['ward_name'] : $arrTmpAddress[My_Staff_Address::Permanent]['ward'];
////                $row[]         = $arrTmpAddress[My_Staff_Address::Permanent]['district'];
////                $row[]         = $provinceDistrict_cache[$province_code];
////            } else {
////                $row[] = "";
////                $row[] = "";
////                $row[] = "";
////                $row[] = "";
////            }
//
//                $province_code_p = $item['p_province_code'];
//                $row[]           = $item['p_address'];
//                $row[]           = ($item['p_ward_id']) ? $item['p_ward_name'] : $item['p_ward'];
//                $row[]           = $item['p_district'];
//                $row[]           = $provinceDistrict_cache[$province_code_p];
//
////            if (isset($arrTmpAddress[My_Staff_Address::Birth_Certificate])) {
////                $province_code = $arrTmpAddress[My_Staff_Address::Birth_Certificate]['province_code'];
////                $row[]         = ($arrTmpAddress[My_Staff_Address::Birth_Certificate]['ward_id']) ? $arrTmpAddress[My_Staff_Address::Birth_Certificate]['ward_name'] : $arrTmpAddress[My_Staff_Address::Birth_Certificate]['ward'];
////                $row[]         = $arrTmpAddress[My_Staff_Address::Birth_Certificate]['district'];
////                $row[]         = $provinceDistrict_cache[$province_code];
////            } else {
////                $row[] = "";
////                $row[] = "";
////                $row[] = "";
////            }
//
//                $province_code_bd = $item['bd_province_code'];
//                $row[]            = $item['bd_address'];
//                $row[]            = ($item['bd_ward_id']) ? $item['bd_ward_name'] : $item['bd_ward'];
//                $row[]            = $item['bd_district'];
//                $row[]            = $provinceDistrict_cache[$province_code_bd];
//
////            unset($arrTmpAddress);
//                $row[] = $item['ID_number'];
//                $row[] = $item['id_place_province_name'];
//                $row[] = $item['ID_date'] ? (date('d/m/Y', strtotime($item['ID_date']))) : '';
//                $row[] = isset($nationalities[$item['nationality']]) ? $nationalities[$item['nationality']] : '';
//                $row[] = isset($religions[$item['religion']]) ? $religions[$item['religion']] : '';
//                $row[] = $item['phone_number'];
//                $row[] = $item['email'];
//                $row[] = My_Staff_Status::get($item['status']);
//                // print_r(str_replace("\n", " ", $item['note'])); die;
//                $row[] = str_replace(array("\r\n", "\n\r", "\n", "\r"), " ", $item['note']);
//
//                // get tags
////            $where   = array();
////            $where[] = $QTagObject->getAdapter()->quoteInto('type = ?', TAG_STAFF);
////            $where[] = $QTagObject->getAdapter()->quoteInto('object_id = ?', $item['id']);
////
////            $tags     = $QTagObject->fetchAll($where);
////            $tags_arr = array();
////
////            foreach ($tags as $_key => $_value)
////                $tags_arr[] = $_value['tag_id'];
////
////            if (is_array($tags_arr) && count($tags_arr))
////                $where = $QTag->getAdapter()->quoteInto('id IN (?)', $tags_arr);
////            else
////                $where = $QTag->getAdapter()->quoteInto('1=0', 1);
////
////            $tags     = $QTag->fetchAll($where);
////            $tags_str = '';
////            foreach ($tags as $_key => $_value)
////                $tags_str .= $_value['name'];
////
////            $tags_str = str_replace("\n", " ", $tags_str);
////            $row[]    = $tags_str;
//                $row[]    = $item['tag_name'];
//                $group_id = $item['access_group'];
//                $row[]    = $groups_cache[$group_id];
//
//                $row[] = $item['policy_group_name'];
//
//                $row[] = $item['company_group_name'];
//
//                $office_id = $item['office_id'];
//
//                if (!empty($item['brand_shop_address'])) {
//                    $row[] = $item['brand_shop_address'];
//                } else {
//                    if (!empty($office_id)) {
//                        $row[] = $office_cache[$office_id];
//                    } else {
//                        $row[] = NULL;
//                    }
//                }
////            $row[] = $item['title'];
//                $row[] = $item['regional_market'];
//
//
//                //TUONG
//                // $row[] = $item['name_pit'];
//                // $row[] = $item['dob_pit'];
//                // $row[] = $item['mst_pit'];
//                // $row[] = $item['relative'];
//                // $row[] = $item['from_month'];
//                // $row[] = $item['to_month'];
//                $row[] = $item['bank_number'];
//                $row[] = $item['at_bank'];
//                $row[] = $item['branch'];
//                $row[] = $item['province_city'];
//                $row[] = $item['tax'];
//
////                array_push($result, $row);
//            }
//        }
//        $heads = array(
//            'Code',
//            'Full Name',
//            // 'Last name',
//            'Company',
//            'Department',
//            'Team',
//            'Title',
//            'Title Id',
//            'Area',
//            'Province',
//            'District',
//            'District code',
//            'Contract type',
//            'Contract signed at',
//            'Contract term',
//            'Contract expired at',
//            'Joined at',
//            'Off date',
//            'Off date reason',
//            'Off date reason detail',
//            'Off type',
//            'Gender',
//            'Date of birth',
//            'Level',
////            'Level new',
//            'Certificate',
//            'ID Card Address Street',
//            'ID Card Address Ward',
//            'ID Card Address District',
//            'ID Card Address Province',
//            'Temporary Address Street',
//            'Temporary Address Ward',
//            'Temporary Address District',
//            'Temporary Address Province',
//            'Permanent address Street',
//            'Permanent address Ward',
//            'Permanent address District',
//            'Permanent address Province',
//            'Birth place Ward',
//            'Birth place District',
//            'Birth place Province',
//            'ID number',
//            'ID place',
//            'ID date',
//            'Nationality',
//            'Religion',
//            'Phone number',
//            'Email',
//            'Status',
//            'Note',
//            'Tags',
//            'Group Center',
//            'Policy Group',
//            'Group',
//            'Office Location',
////            'Title Id',
//            'Province Id',
//            //TUONG
//            // 'Pit_Name',
//            // 'Pit_Dob',
//            // 'Pit_Mst',
//            // 'Pit_relative',
//            // 'Pit_Frommonth',
//            // 'Pit_Tomonth',
//            'Bank_Number',
//            'Bank_At',
//            'Bank_Branch',
//            'Bank_Province',
//            'PTI',
//            //TUONG
//        );
//        $PHPExcel->setActiveSheetIndex(0);
//        $sheet = $PHPExcel->getActiveSheet();
//        $alpha = 'A';
//        $index = 1;
//
//        foreach ($heads as $key) {
//            $sheet->setCellValue($alpha . $index, $key);
//            $alpha++;
//        }
//        $index = 2;
//
//        foreach ($result as $key => $value):
//            $alpha = 'A';
//            for ($x = 0; $x <= count($value); $x++) {
////                debug($value);exit;
//                $sheet->getCell($alpha++ . $index)->setValueExplicit($value[$x], PHPExcel_Cell_DataType::TYPE_STRING);
//            }
//
//            $index++;
//        endforeach;

        $filename  = 'Staff List - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.xlsx');
        $objWriter->save('php://output');
        exit;
    }

    private function sendmail($to, $subject, $maildata, $image = '', $ccStaff) {
        require_once 'staff' . DIRECTORY_SEPARATOR . 'send-email.php';
    }
    public function exportDependentPersonAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $QDependentPersonStaff=new Application_Model_DependentPersonStaff();
      
        $listExport=$QDependentPersonStaff->getListExport();

        if(!empty($listExport)){

    include 'PHPExcel/IOFactory.php';
        // no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
        
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            '#',
            'Staff Code',
            'Staff Name',
            'Department',
            'Team',
            'Title',
            'Relative',
            'Relative Person',
            'PIT',

        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $intCount = 1;       


        
        $db = Zend_Registry::get('db');

        try
        {

                foreach ($listExport as $key => $value)
                {

                    $alpha = 'A';

                    $sheet->setCellValue($alpha++ . $index, $intCount++); 
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["staff_code"]) ? $value["staff_code"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["staff_name"]) ? $value["staff_name"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["department_name"]) ? $value["department_name"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["team_name"]) ? $value["team_name"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["title_name"]) ? $value["title_name"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["name"]) ? $value["name"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["relative"]) ? $value["relative"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value["mst"]) ? $value["mst"]:"" , PHPExcel_Cell_DataType::TYPE_STRING);

                    $index++;                    
                }
        }
        catch (exception $e)
        {
            exit;
        }

        $filename = 'DependentPerson_' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');

        $objWriter->save('php://output');
        exit;
        $this->_redirect($back_url ? $back_url : HOST . 'staff/upload-data-staff');
        }
    }

}
