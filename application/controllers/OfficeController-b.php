<?php
class OfficeController extends My_Controller_Action
{
	public function indexAction(){


		$office_name = $this->getRequest()->getParam('office_name');
		$area_id      = $this->getRequest()->getParam('area_id');
		$page         = $this->getRequest()->getParam('page',1);

		$sort = $this->getRequest()->getParam('sort','created_at');
		$desc = $this->getRequest()->getParam('desc',1);
		$params  = array(
				'office_name' => $office_name,
				'area_id'      => $area_id,
				'sort'         => $sort,
				'desc'         => $desc
			);

		$total   = 0;
		$limit   = LIMITATION;
		$QOffice = new Application_Model_Office();
		$list = $QOffice->fetchPagination($page, $limit, $total, $params);
		$this->view->list = $list;

		$this->view->sort = $sort;
		$this->view->desc = $desc;
		$this->view->params = $params;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'office' . ($params ? '?' . http_build_query($params) .
            '&' : '?');
        $this->view->offset = $limit * ($page - 1);

        $QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

		$flashMessenger             = $this->_helper->flashMessenger;
		$messages                   = $flashMessenger->setNamespace('success')->getMessages();
		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages       = $messages;
		$this->view->messages_error = $messages_error;
	}

	public function createAction(){
		$QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();
        $id = $this->getRequest()->getParam('id');

        if($id){
			$QOffice = new Application_Model_Office();
			$office = $QOffice->find($id)->current();
			$this->view->office = $office;
        }
	}

	public function saveAction(){
		$office_name    = $this->getRequest()->getParam('office_name');
		$area_id        = $this->getRequest()->getParam('area_id');
		$id             = $this->getRequest()->getParam('id');
		$flashMessenger = $this->_helper->flashMessenger;

		if(trim($office_name) == ''){
			$flashMessenger->setNamespace('error')->addMessage('offince name is require');
			$this->_redirect('/office/');
		}

		if(!intval($area_id)){
			$flashMessenger->setNamespace('error')->addMessage('area is require');
			$this->_redirect('/office/');
		}

		$params = array(
			'office_name' => $office_name,
			'area_id'     => $area_id
		);
		$QOffice = new Application_Model_Office();
		$result = $QOffice->save($params,$id);
		if($result['code'] <= 0){
			$flashMessenger->setNamespace('error')->addMessage($result['message']);
		}else{
			$flashMessenger->setNamespace('success')->addMessage($result['message']);
		}
		$this->_redirect('/office/');
	}

	public function delAction(){
		$id = $this->getRequest()->getParam('id');
		$id = intval($id);
		$flashMessenger  = $this->_helper->flashMessenger;
		if($id){
			$QOffice = new Application_Model_Office();
			$result = $QOffice->save(array('del'=>1),$id);

			if($result['code'] <= 0){
				$flashMessenger->setNamespace('error')->addMessage($result['message']);
				$this->_redirect('/office');
			}else{
				$flashMessenger->setNamespace('success')->addMessage($result['message']);
				$this->_redirect('/office');
			}
		}else{
			$flashMessenger->setNamespace('error')->addMessage('Please select item to delete');
			$this->_redirect('/office');
		}
	}
}