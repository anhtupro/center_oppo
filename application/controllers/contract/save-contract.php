<?php
if($this->getRequest()->isPost()){

    $id                    = $this->getRequest()->getParam('id');
    $staff_id              = $this->getRequest()->getParam('staff_id');
    $company_id            = $this->getRequest()->getParam('company_id');
    $regional_market       = $this->getRequest()->getParam('regional_market');
    $title                 = $this->getRequest()->getParam('title');

    $new_contract          = $this->getRequest()->getParam('new_contract');
    $asm_proposal          = $this->getRequest()->getParam('asm_proposal');
    $status                = $this->getRequest()->getParam('status');
    $parent_id             = $this->getRequest()->getParam('parent_id');
    $hospital_id           = $this->getRequest()->getParam('hospital_id');
    $from_date             = $this->getRequest()->getParam('from_date');
    $to_date               = $this->getRequest()->getParam('to_date');
    $salary                = $this->getRequest()->getParam('salary');
    $total_salary          = $this->getRequest()->getParam('total_salary');
    $allowance_1           = $this->getRequest()->getParam('allowance_1');
    $allowance_2           = $this->getRequest()->getParam('allowance_2');
    $allowance_3           = $this->getRequest()->getParam('allowance_3');
    $probation_salary      = $this->getRequest()->getParam('probation_salary');
    $base_probation_salary = $this->getRequest()->getParam('base_probation_salary');
    $send_letter           = $this->getRequest()->getParam('send_letter');
    $return_letter         = $this->getRequest()->getParam('return_letter');
    $print_type            = $this->getRequest()->getParam('print_type');
    $print_status          = $this->getRequest()->getParam('print_status');
    $kpi_text              = $this->getRequest()->getParam('kpi_text');
    $kpi_text_probation    = $this->getRequest()->getParam('kpi_text_probation');
    $locked                = $this->getRequest()->getParam('locked',0);
    $work_cost             = $this->getRequest()->getParam('work_cost');
    $note                  = $this->getRequest()->getParam('note');

    $back_url = $this->getRequest()->getParam('back_url',HOST.'contract/');

    $params = array(
        'id'                    => $id,
        'staff_id'              => $staff_id,
        'new_contract'          => $new_contract,
        'from_date'             => $from_date,
        'to_date'               => $to_date,
        'asm_proposal'          => $asm_proposal,
        'note'                  => $note,
        'hospital_id'           => $hospital_id,
        'status'                => $status,
        'title'                 => $title,
        'regional_market'       => $regional_market,
        'send_letter'           => $send_letter,
        'return_letter'         => $return_letter,
        'print_type'            => $print_type,
        'print_status'          => $print_status,
        'salary'                => $salary,
        'total_salary'          => $total_salary,
        'kpi_text'              => $kpi_text,
        'kpi_text_probation'    => $kpi_text_probation,
        'allowance_1'           => $allowance_1,
        'allowance_2'           => $allowance_2,
        'allowance_3'           => $allowance_3,
        'locked'                => $locked,
        'work_cost'             => $work_cost,
        'probation_salary'      => $probation_salary,
        'base_probation_salary' => $base_probation_salary,
        'parent_id'             => $parent_id,
        'company_id'            => $company_id
    );

    $QContract = new Application_Model_AsmContract();
    $db = Zend_Registry::get('db');
    $messages_success = array();
    $messages_error = array();
    $db->beginTransaction();
    try{
        $result = $QContract->save_contract($params,$id);
        $db->commit();
        if($result['code'] == 1){
            $messages_success[] = 'Done';
            $this->view->result = 'success';
        }else{
            $messages_error[] = $result['message'];
        }

    }catch (Exception $e){
        $db->rollBack();
        $messages_error[] = $e->getMessage();
    }

    $this->back_url = $back_url;
    $this->view->messages_error = $messages_error;
    $this->view->messages_success = $messages_success;
}
$this->_helper->layout()->disableLayout(true);
?>
