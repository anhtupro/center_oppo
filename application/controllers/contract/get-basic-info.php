<?php
$this->_helper->layout()->disableLayout(true);
$db = Zend_Registry::get('db');
$staff_id = $this->getRequest()->getParam('staff_id');
$QStaff = new Application_Model_Staff();
$staff = $QStaff->find($staff_id)->current();
$this->view->contract = $staff;


$QCompany = new Application_Model_Company();
$companies  = $QCompany->get_cache();
$this->view->companies = $companies;

$QArea = new Application_Model_Area();
$area_list = $QArea->get_cache();
$this->view->area_list = $area_list;

$QTeam = new Application_Model_Team();
$department_list = $QTeam->get_department();
$this->view->department_list = $department_list;

$QRegionalMarket = new Application_Model_RegionalMarket();

if($staff){
    if($staff['title']){
        $arr_title = $QTeam->getTitleTeamDepartment($staff['title']);
        $this->view->arr_title = $arr_title;

        if($arr_title){
            $team_list = $QTeam->getTeamByDepartment($arr_title['department']);
            $this->view->team_list = $team_list;

            $title_list = $QTeam->getTeamByDepartment($arr_title['team']);
            $this->view->title_list = $title_list;
        }
    }

    if($staff['regional_market']){
        $selectAreaId = $db->select()
            ->from(array('a'=>'regional_market'),array('area_id'))
            ->where('id = ?',$staff['regional_market']);
        $area_id = $db->fetchOne($selectAreaId);

        $regional_market_list = $QRegionalMarket->get_regional_market_by_area($area_id);
        $this->view->regional_market_list = $regional_market_list;

        $arr_regional_area = array(
            'area' => $area_id,
            'regional_market' => $staff['regional_market']
        );
        $this->view->arr_regional_area = $arr_regional_area;


    }
}