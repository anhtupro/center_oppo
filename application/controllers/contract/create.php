<?php

$id = $this->getRequest()->getParam('id');
$db        = Zend_Registry::get('db');
$select = $db->select()
    ->from(array('s' => 'staff'), array('id', 'staff_name' =>'CONCAT(s.firstname," ",s.lastname," - ",s.code," - ",t.name)'))
    ->join(array('t' =>'team'), 't.id = s.team', array())
    ->order('s.firstname');
$staffs = $db->fetchAll($select);
$this->view->staffs = $staffs;

$QCompany = new Application_Model_Company();
$companies  = $QCompany->get_cache();
$this->view->companies = $companies;

$QArea = new Application_Model_Area();
$area_list = $QArea->get_cache();
$this->view->area_list = $area_list;

$QTeam = new Application_Model_Team();
$department_list = $QTeam->get_department();
$this->view->department_list = $department_list;

$QContractTerm = new Application_Model_ContractTerm();
$contract_list = $QContractTerm->get_cache();
$this->view->contract_list = $contract_list;

$print_type_list = array(
    1 => 'Hợp đồng',
    2 => 'Phụ lục'
);
$this->view->print_type_list = $print_type_list;

$status_list = array(
    1 => 'ASM xác nhận',
    2 => 'HR xác nhận'
);
$this->view->status_list = $status_list;


if($id){
    $QAsmContract = new Application_Model_AsmContract();
    $contract = $QAsmContract->find($id)->current()->toArray();
    $this->view->contract = $contract;

    if($contract['title']){
        $arr_title = $QTeam->getTitleTeamDepartment($contract['title']);
        $this->view->arr_title = $arr_title;

        $title_list = $QTeam->getTeamByDepartment($arr_title['team']);
        $this->view->title_list = $title_list;

    }

    if($contract['regional_market']){
        $selectAreaId = $db->select()
            ->from(array('a'=>'regional_market'),array('area_id'))
            ->where('id = ?',$staff['regional_market']);
        $area_id = $db->fetchOne($selectAreaId);

        $regional_market_list = $QRegionalMarket->get_regional_market_by_area($area_id);
        $this->view->regional_market_list = $regional_market_list;

        $arr_regional_area = array(
            'area' => $area_id,
            'regional_market' => $contract['regional_market']
        );
        $this->view->arr_regional_area = $arr_regional_area;

    }

}
?>