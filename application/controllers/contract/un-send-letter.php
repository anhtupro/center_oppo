<?php 

$back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract/staff-preview';
$flashMessenger = $this->_helper->flashMessenger;
$db             = Zend_Registry::get('db');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

if($this->getRequest()->isPost()){
    if(in_array($userStorage->group_id,array(HR_ID,ADMINISTRATOR_ID))){
        $db->beginTransaction();
        try{
			$ids          = $params['ids'];
			$data         = array('send_letter'=> NULL);
			$QAsmContract = new Application_Model_AsmContract();
			$where        = $QAsmContract->getAdapter()->quoteInto('id IN (?)',$ids);
			$QAsmContract->update($data,$where);
			$db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch(Exception $e){
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $db->rollBack();
        }
    }
    $this->_redirect($back_url);
}