<?php
/**
 * @author nguoinaodo
 * @create 2015-12-25
 */

class JobController extends My_Controller_Action
{
    public function indexAction()
    {  
        require_once 'job' . DIRECTORY_SEPARATOR .'index.php';
    }

    // delete
    public function delAction()
    {
        $id = $this->getRequest()->getParam('id');
        $QModel = new Application_Model_Job();
        $where = $QModel->getAdapter()->quoteInto('id = ?', $id);

        $jobRowset = $QModel->find($id);
        $job = $jobRowset->current();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $user_id = $userStorage->id;
        $user_group = $userStorage->group_id;

        try {
            $QModel->delete($where);

            $info = 'DELETE: job request : ' . $job['title'] . 'id = ' . $job['id'];

            $QLog = new Application_Model_Log();

            $ip = $this->getRequest()->getServer('REMOTE_ADDR');

            $QLog->insert(array(
                'info' => $info,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
            ));

        } catch (Exception $e) {

        }

        $this->_redirect('/job');
    }
    
    public function  categoryAction()
    {
        $QJobCat = new Application_Model_JobCategory();
        $cat =  $QJobCat->fetchAll();
        $this->view->cat = $cat;
    }
    
    public function  detailsCatAction()
    {
        $id = $this->getRequest()->getParam('id');
        $QJobCat = new Application_Model_JobCategory();
        
        if($id)
        {
            $where = $QJobCat->getAdapter()->quoteInto('id = ?', $id);
            $cat = $QJobCat->fetchRow($where);
            $this->view->cat = $cat;
        }        
        
    }
    
    public function createCatAction(){
        if ($this->getRequest()->getMethod() == 'POST') {
            
            $QJobCat = new Application_Model_JobCategory();
            
            $name = $this->getRequest()->getParam('name');
            $id = $this->getRequest()->getParam('id_cat');
            
            $data = array(
                'name' => $name
            );
            
            if(!$id){
                $QJobCat->insert($data);
            }
            else{
                $where = $QJobCat->getAdapter()->quoteInto('id = ?', $id);
                $QJobCat->update($data,$where);
            }
            
            $this->_redirect('/job/category');
        }
                
    }
    
    public function delCatAction(){
        $id = $this->getRequest()->getParam('id');
        $QJobCat = new Application_Model_JobCategory();
        
        if($id)
        {
            $QJob = new Application_Model_JobCategory();
            $where = $QJob->getAdapter()->quoteInto('category = ?', $id);
            $job = $QJob->fetchAll($where);
            if(!$job){
                $where = $QJobCat->getAdapter()->quoteInto('id = ?', $id);
                $cat = $QJobCat->delete($where);
                $this->view->cat = $cat;
            }
            $this->_redirect('/job/category');
        }        
    }
    
    public function  viewCvAction()
    {
        $id = $this->getRequest()->getParam('id');
        $flashMessenger = $this->_helper->flashMessenger;
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
//        $QLogSystemTrade       = new Application_Model_LogSystemTrade();
//        $log_system_trade = [
//            'staff_id' => $userStorage->id,
//            'action'   => 'view-cv?id='.$id,
//            'created_at'    => date('Y-m-d H:i:s')
//        ];
//        $QLogSystemTrade->insert($log_system_trade);

        if(!$id)
        {
            $flashMessenger->setNamespace('error')->addMessage('Invalid cv');
            $this->_redirect(HOST . 'job');
        }

        $db = Zend_Registry::get('db');
        
        $nestedSelect = "SELECT * FROM province_district GROUP BY province_code";
        
        $select = $db->select()->from(array('p' => 'cv_region'),array());
        $select->joinLeft(array('d' => new Zend_Db_Expr('(' . $nestedSelect . ')')),'p.region_id = d.province_code',array('d.province_name'));
        $select->joinLeft(array('d2' => 'province_district'), 'p.district_id = d2.district_id',array('d2.district_center'));
        $select->where('cv_id = ?', $id);
        
        $cv_region    = $db->fetchAll($select);
        $list_cv_region = null;
        foreach($cv_region as $key=>$value){
            $list_cv_region .= " ".$value['district_center']." - ".$value['province_name']." ,";
        }
        //get other language
        $QCvLanguage = new Application_Model_CvLanguage();
        $where=array();
        $where=$QCvLanguage->getAdapter()->quoteInto('cv_id = ? ',$id);
        $CvLanguage=$QCvLanguage->fetchAll($where);
        $where=null;


        $QCv       = new Application_Model_Cv();
        $cvCurrent = $QCv->find($id);
        $cv = $cvCurrent->current();



        //get address current of cv
        $select = $db->select()->from(array('p' => 'cv'),array());
        $select->joinLeft(array('d' => new Zend_Db_Expr('(' . $nestedSelect . ')')),'p.region_address1 = d.province_code',array('d.province_name'));
        $select->joinLeft(array('d2' => 'province_district'), 'p.region_address2 = d2.district_id',array('d2.district_center'));
        $select->where('id = ?', $id);
        $cv_address    = $db->fetchAll($select);
         $list_cv_address = null;

        foreach($cv_address as $key=>$value){
            $list_cv_address .= " ".$value['district_center']." - ".$value['province_name']." ,";
        }


        $QJob = new Application_Model_Job();
        $QJobCat = new Application_Model_JobCategory();
        
        $QCvCompanyOld = new Application_Model_CvCompanyOld();
        $where_old = $QCvCompanyOld->getAdapter()->quoteInto('cv_id = ?', $id);
        $data_company_old = $QCvCompanyOld->fetchAll($where_old);
        $this->view->data_company_old = $data_company_old;

        if(isset($cv['job_id']) and $cv['job_id'])
        {
            $jobRowset = $QJob->find(intval($cv['job_id']));
            $job       = $jobRowset->current();
            $this->view->job = $job;
        }
        
        if(isset($cv['cat_id']) and $cv['cat_id'])
        {
            $catRowset = $QJobCat->find(intval($cv['cat_id']));
            $cat       = $catRowset->current();
            $this->view->cat = $cat;
        }
        
        $job_url = HOST_VIECLAM.'cv/';
        $job_url_avatar = HOST_VIECLAM.'cv_image/';
        $job_url_source = HOST . 'photo' .
                DIRECTORY_SEPARATOR . 'cv' . DIRECTORY_SEPARATOR . $cv['id'];
        
        $path_info = '';
        $path_info2 = '';
        if(isset($cv['cv_name']) and $cv['cv_name'])
        {
            $temp = explode('_' , $cv['cv_name']);
            $path_info  = $job_url;
            $path_info .= 'temp' . DIRECTORY_SEPARATOR;
            $path_info .= $temp[0] . DIRECTORY_SEPARATOR . $temp[2];
            
            if(isset($cv['source']) and $cv['source'] != 5){
                $path_info = $job_url_source . DIRECTORY_SEPARATOR . $cv['cv_name'];
            }
            
            $path_info2 = $job_url_source . DIRECTORY_SEPARATOR . $cv['cv_name'];
            
            
        }
        
        $path_info_avatar = '';
        if(isset($cv['cv_avatar']) and $cv['cv_avatar'])
        {
            $temp = explode('_' , $cv['cv_avatar']);
            $path_info_avatar  = $job_url_avatar;
            $path_info_avatar .= 'temp' . DIRECTORY_SEPARATOR;
            $path_info_avatar .= $temp[0] . DIRECTORY_SEPARATOR . $temp[2];
        }
        


        if(!$cv)
        {
            $flashMessenger->setNamespace('error')->addMessage('Invalid cv');
            $this->_redirect(HOST . 'job');
        }

        $where = $QCv->getAdapter()->quoteInto('id = ?', $id);
        if(empty($cv['status']) || $cv['status'] == 0){
            $QCv->update(array('status'=>1), $where);
        }

        $this->view->path_info = $path_info;
        $this->view->path_info2 = $path_info2;
        $this->view->path_info_avatar = $path_info_avatar;
        $this->view->CvLanguage        = $CvLanguage;
        $this->view->cv        = $cv;
        if($_GET["dev"]){
        	echo "<pre>";
        	print_r($cv);
        	echo "</pre>";
        }
        $this->view->list_cv_region  = rtrim($list_cv_region, ",");
        $this->view->list_cv_address  = rtrim($list_cv_address, ",");
        $this->view->list_job = $QJobCat->get_cache();
    }
    
    
    public function  cvStatusAction()
    {
        $id = $this->getRequest()->getParam('id');
        $id_cv = $this->getRequest()->getParam('id_cv');
        if(is_numeric($id) && is_numeric($id_cv)){
            $QCv = new Application_Model_Cv();
            $where = $QCv->getAdapter()->quoteInto('id = ?', $id_cv);
            $data = array(
                'status'  =>  $id
            );
            $QCv->update($data,$where);
            echo $id;
            exit;
        }
        exit;
    }

    public  function cvAction()
    {
        require_once 'job' . DIRECTORY_SEPARATOR .'cv.php';
    }
    
    /*
    * Lấy CV gửi về khu vực ASM quản lý
    */
    public  function cvSuggestAction()
    {
        $page    = $this->getRequest()->getParam('page', 1);
        $name    = trim($this->getRequest()->getParam('name'));
        $content = $this->getRequest()->getParam('content');
        $location = $this->getRequest()->getParam('location');
        $dob_from = $this->getRequest()->getParam('dob_from');
        $dob_to   = $this->getRequest()->getParam('dob_to');
        $status   = $this->getRequest()->getParam('status');
        $address  = $this->getRequest()->getParam('address');
        $job      = $this->getRequest()->getParam('job');
        $phone      = $this->getRequest()->getParam('phone');
        $email      = trim($this->getRequest()->getParam('email'));
        
        $schools    = $this->getRequest()->getParam('schools');
        $brand      = $this->getRequest()->getParam('brand');
        $qualification      = $this->getRequest()->getParam('qualification');
        
        $sort     = $this->getRequest()->getParam('sort');
        $desc     = $this->getRequest()->getParam('desc', 1);

        $limit = LIMITATION;
        $total = 0;

        $params = array_filter(array(
            'name'    => $name,
            'location'=> $location,
            'dob_from'=> $dob_from,
            'dob_to'  => $dob_to,
            'status'  => $status,
            'address' => $address,
            'content' => $content,
            'job'     => $job,
            'phone'   => $phone,
            'email'   => $email,
            'sort'    => $sort,
            'desc'    => $desc,
            'schools' =>$schools,
            'brand'   =>$brand,
            'qualification'=>$qualification
        ));
        $params['sort'] = $sort;
        $params['desc'] = $desc;
        $params['filter_display'] = 1;
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (in_array(
            $userStorage->title,
            array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM, SALE_SALE_ASM_STANDBY)
        )) {
            $QAsm = new Application_Model_Asm();
            $cachedASM = $QAsm->get_cache($userStorage->id);
            $tem = $cachedASM['province'];
            if ($tem)
                $list_province_ids = $tem;
            else
                $list_province_ids = -1;
            
            $params['list_province_ids'] = $list_province_ids;
        }
        //Lọc 

        $QModel = new Application_Model_Cv();
        $cv = $QModel->fetchPagination_suggest($page, $limit, $total, $params);
        $QRegionalMarket = new Application_Model_RegionalMarket();
        $regional_market = $QRegionalMarket->get_cache();
        $this->view->regional_market = $regional_market;

        $this->view->params        = $params;
        $this->view->sort          = $sort;
        $this->view->desc          = $desc;
        $this->view->cv            = $cv;

        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST.'job/cv'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);

        $flashMessenger       = $this->_helper->flashMessenger;
        $messages             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
            
        
        $QJob = new Application_Model_Job();
        $Job = $QJob->fetchAll();
        $this->view->job = $Job;
    }
    
    /* Xóa Cv*/
    public function delCvAction(){

        $url = htmlspecialchars($_SERVER['HTTP_REFERER']);

        $id = $this->getRequest()->getParam('id');
        $QModel = new Application_Model_Cv();
        $QCvLanguage = new Application_Model_CvLanguage();

        $where = $QModel->getAdapter()->quoteInto('id = ?', $id);

        try {
            $QModel->delete($where);
            
            $where=null;
            $where = $QCvLanguage->getAdapter()->quoteInto('cv_id = ?', $id);
            $QCvLanguage->delete($where);

            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('success');
            $this->_redirect($url);

        } catch (Exception $e) {

        }
    }

    public  function  approveAction()
    {
        $id = $this->getRequest()->getParam('id');
        $flashMessenger = $this->_helper->flashMessenger;
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $user_id     = $userStorage->id;
        $user_group  = $userStorage->group_id;
        $currentTime = date('Y-m-d h:i:s');
        $QJob = new Application_Model_Job();
        $QArea = new Application_Model_Area();

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $this->view->all_province_cache = $QRegionalMarket->get_cache();

        $QJobCategory = new Application_Model_JobCategory();
        $this->view->job_cat = $QJobCategory->get_cache();

        if (in_array($userStorage->title, array(SALE_SALE_ASM))) {
            $QAsm = new Application_Model_Asm();
            $cachedASM = $QAsm->get_cache($userStorage->id);
            $tem = $cachedASM['area'];
            $this->view->areas = $tem;
        }
        else
        {
            $tem = $QArea->get_cache();
            $this->view->areas_all = $tem;
        }
        $this->view->areas_catche = $QArea->get_cache();

        $QTeam = new Application_Model_Team();
        $whereDepartment[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', 0);
        $whereDepartment[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
        $listDepartment = $QTeam->fetchAll($whereDepartment);
        $this->view->listDepartment = $listDepartment;

        if(isset($id) and $id)
        {
            $QJobRegional = new Application_Model_JobRegional();
            $job_regional = $QJobRegional->get_regional_by_id($id);
            $this->view->job_regional = $job_regional;

            $jobCurrent = $QJob->find($id);
            $job = $jobCurrent->current();
            $this->view->job = $job;


            /* get team + title */
            $where_team = array();
            $where_team[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $job['department']);
            $where_team[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
            $team_view = $QTeam->fetchAll($where_team);
            $this->view->team_view = $team_view;

            $where_title = array();
            $where_title[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $job['team']);
            $where_title[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
            $team_title = $QTeam->fetchAll($where_title);
            $this->view->team_title = $team_title;
            /* end get team and title*/

            $rowset = $QRegionalMarket->find($job->regional_market_id);

            if ($rowset)
            {
                $this->view->regional_market = $regional_market = $rowset->current();
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);
                $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

                $rowset = $QArea->find($regional_market['area_id']);
                $this->view->area = $rowset->current();
            }

        }

        $jobRowset = $QJob->find($id);
        $job       = $jobRowset->current();
        $this->view->job = $job;

        if ($this->getRequest()->getMethod() == 'POST') {

            try {

                $db = Zend_Registry::get('db');
                $db->beginTransaction();

                if (!$id)
                    throw new exception('Invalid id');


                $job = $QJob->find($id);
                $job = $job->current();

                if (!$job)
                    throw new exception('Invalid id');

                $data = array(
                    'approved_at' => $currentTime,
                    'approved_by' => $user_id,
                    'status'      => 1
                );

                $where = $QJob->getAdapter()->quoteInto('id = ?' , $id);



                $QJob->update($data, $where);



                $info = 'Verify: job request : ' . $job['title'] . 'id = ' . $job['id'];

                $QLog = new Application_Model_Log();

                $ip = $this->getRequest()->getServer('REMOTE_ADDR');



                $QLog->insert(array(
                    'info' => $info,
                    'user_id' => $userStorage->id,
                    'ip_address' => $ip,
                    'time' => date('Y-m-d H:i:s'),
                ));

                $db->commit();

                $flashMessenger->setNamespace('success')->addMessage('Done!');
                $this->_redirect(HOST . 'job');



            } catch (exception $e) {

                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                $this->_redirect(HOST . 'job');

            }
        }

    }
    
    public  function  hideAction()
    {
        $id = $this->getRequest()->getParam('id');
        $type = $this->getRequest()->getParam('type');

        $QJob = new Application_Model_Job();
        if(isset($id) and $id)
        {
            $data = array(
                'status'      => $type
            );
            
            $where = $QJob->getAdapter()->quoteInto('id = ?' , $id);
            $QJob->update($data, $where);
            

        }

        $Ws    = new Application_Model_Job();
        
        $params_ws = [
            'id' => $id
        ];
        
        // $datas = $Ws->fetchAllJob($params_ws);
        // if ($datas) {
        //     $result = $Ws->selectJobWebservice($datas);
        // }

        $this->_redirect(HOST . 'job');
        

    }

    public function createAction()
    {
        require_once 'job' . DIRECTORY_SEPARATOR .'create.php';
    }
    
    public function getTeamAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getRequest()->getMethod() == 'POST')
        {

            $departmentID = $this->getRequest()->getParam('department_id');
            $QTeam = new Application_Model_Team();
            $whereTeam = array();
            $whereTeam[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $departmentID);
            $whereTeam[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
            $Teams = $QTeam->fetchAll($whereTeam);
            if ($Teams)
            {
                echo json_encode($Teams->toArray());
            }

        }
    }

    public function getJobTitleAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getRequest()->getMethod() == 'POST')
        {

            $teamID = $this->getRequest()->getParam('team_id');
            $QTeam = new Application_Model_Team();
            $whereJobTile = array();
            $whereJobTile[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $teamID);
            $whereJobTile[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
            $whereJobTile[] = $QTeam->getAdapter()->quoteInto('is_hidden = ?', 0);
            $jobTitle = $QTeam->fetchAll($whereJobTile);
            if ($jobTitle)
            {
                echo json_encode($jobTitle->toArray());
            }

        }
    }

    public function viewAction()
    {
        $id = $this->getRequest()->getParam('id');

        $QJob = new Application_Model_Job();
        $jobCurrentset = $QJob->find($id);
        $result = $jobCurrentset->current()->toArray();
     
        $QJobRegional = new Application_Model_JobRegional();
        $job_regional = $QJobRegional->get_regional_by_id($id);
        
        $this->view->job = $result;
        $this->view->job_regional = $job_regional;
    }
    
    

    public function saveAddnoteAction(){

        $url = htmlspecialchars($_SERVER['HTTP_REFERER']);
        
        $id     = $this->getRequest()->getParam('id');
        $note   = $this->getRequest()->getParam('content');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $name = $userStorage->firstname . ' ' . $userStorage->lastname;

        $QModel = new Application_Model_Cv();
        $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
        $CV = $QModel->fetchRow($where);
        //echo $CV['note']; die();
        $this->view->note = $CV['note'];
        
        try {

            $note_update = $CV['note'].'$'. '['.date('d-m-Y h:i').'] ' . $name. ' : ' .$note; 
            $data = array('note' => $note_update);

            $QModel->update($data,$where);
            
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('success');
            $this->_redirect(str_replace('&amp;', '&', $url));
        } catch (Exception $e) {
            
        }
    }

    public function createCvAction(){
        require_once 'job' . DIRECTORY_SEPARATOR .'create-cv.php';
    }

    public function saveCvAction(){
        require_once 'job' . DIRECTORY_SEPARATOR .'save-cv.php';
    }

    public function checkCvAction(){
        require_once 'job' . DIRECTORY_SEPARATOR .'check-cv.php';
    }

    public function updateCvDateAction(){
        $id     = $this->getRequest()->getParam('id');

        $QCv = new Application_Model_Cv();
        $where = $QCv->getAdapter()->quoteInto('id = ?', $id);
        $data = array(
            'created_at' => date('Y-m-d H:i:s')
        );
        $QCv->update($data,$where); 
        echo "success";
        exit;
    }
	
	public function getInfoAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $id     = $this->getRequest()->getParam('id_cv');
        $QCv = new Application_Model_Cv();
        $cvCurrentset = $QCv->find($id);
        $result = $cvCurrentset->current()->toArray();
        $result['en_name'] = My_String::khongdau( $result['name'] );
        $job_id= $result['job_id'];
        
        if($job_id) {
            $QJob = new Application_Model_Job();
            $jobCurrentset = $QJob->find($job_id);
            $result_job = $jobCurrentset->current()->toArray();
            $result['job_title'] = $result_job['title'];
        }   
        
        $db = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        //get staff info
        $QStaff = new Application_Model_Staff();
        $QOffice = new Application_Model_Office();
        $office_cache = $QOffice->get_cache();
        $select_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $infoStaff = $QStaff->fetchRow($select_staff);
        
        $result['email']            = $result['email'];
        $result['lastname']         = $infoStaff->lastname;
        $result['office_location']  = $office_cache[$infoStaff->office_id];
        $result['phone_admin']      = $infoStaff->phone_number;
        $result['gender']           = $infoStaff->gender;
        
        
//         $QJobRegional = new Application_Model_JobRegional();
//         $job_regional = $QJobRegional->get_regional_by_id($id);
        
        //$this->view->job_regional = $job_regional;
        
        echo json_encode($result);
    }
    
    public function sendEmailAction(){
        $hr_signature = '
                    <h3 style="color: #00b050;margin-bottom: 0;">Recruitment Team</h3>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">T (84-8) 39202555 - ext: 111</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">F (84-8) 39202555 - ext: 120</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">E <a href="mailto:recruitment@oppo-aed.vn">recruitment@oppo-aed.vn</a></p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">12th Floor, Lim II Tower, No. 62A CMT8 St., Ward 6, Dist. 3, HCMC, VN</p>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 0;"><a href="http://vieclam.oppomobile.vn/">www.vieclam.oppomobile.vn</a></p>';
        
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $id             = $this->getRequest()->getParam('id_cv');
        $name           = $this->getRequest()->getParam('name');
        $en_name        = $this->getRequest()->getParam('en_name');
        $time           = $this->getRequest()->getParam('time');
        list($h, $m) = explode(':', $time);
        if($h >= 12 and $m >0 ){
            $ext = 'PM';
        }else{
            $ext = 'AM';
        }
        $time           = $this->getRequest()->getParam('time');
        $date           = $this->getRequest()->getParam('date');
        $kindly_contact     = $this->getRequest()->getParam('kindly_contact');
        $en_kindly_contact  = $this->getRequest()->getParam('en_kindly_contact');
        $phone          = $this->getRequest()->getParam('phone');
        $address        = $this->getRequest()->getParam('address');
        $en_address     = $this->getRequest()->getParam('en_address');
        $position       = $this->getRequest()->getParam('position');
        $en_position    = $this->getRequest()->getParam('en_position');
        $email          = $this->getRequest()->getParam('email');
        $kind_of_times  = $this->getRequest()->getParam('kind_of_times');
        $en_kind_of_times  = $this->getRequest()->getParam('en_kind_of_times');
        $bring_cv       = $this->getRequest()->getParam('bring_cv');
        
        
        /////////////////////////
        /////// CONFIG
        /////////////////////////
        try {
            error_reporting(~E_ALL);
            ini_set("display_error", 0);
            set_time_limit(0);
            $mailfrom = 'vandiem.nguyen@oppo-aed.vn'; // Config this
            $mailfromname = "OPPO Tuyển dụng"; // Config this
            //123321abc
            $subject = 'Interview Invitation - OPPO Vietnam';
            $content = '<div><p class="">
                Dear '. $en_name.',
                <br><br>Thank you for applying to OPPO Viet Nam. As discussed via telephone, we kindly confirm the '. $en_kind_of_times .' interview as below:';
            $content .= '<br><br>Position: '. $en_position;
            $content .= '<br>Time: '. $time .' '. $ext . ' , ' . $date ;
            $content .= '<br>Address: '. $en_address;
            $content .= '<br>Kindly Contact: '.$en_kindly_contact;
            $content .= '<br>Tel: '.$phone;
            $content .= '<br><br>Please kindly confirm your attendance at the interview.';
            if($bring_cv == 1){
                $content .= '<br><br>Kindly bring copy documents & ID Card to the interview.';
            }
            
            $content .= '<br>Sincerely,';
            $content .= '<br><br>---------------------------------------------------------------';
            $content .= '<br><br>Chào '. $name .',
                     <br><br>Cám ơn bạn đã quan tâm đến thông tin tuyển dụng tại OPPO Việt Nam. Như đã trao đổi qua điện thoại, chúng tôi xác nhận cuộc hẹn phỏng vấn vòng ' .$kind_of_times. ' với bạn, cụ thể như sau: ';
            $content .= '<br><br>Vị trí: '. $position;
            $content .= '<br>Thời gian: '.  $time . ' ngày ' . $date ;
            $content .= '<br>Địa điểm: '. $address;
            $content .= '<br>Người liên hệ: '. $kindly_contact;
            $content .= '<br>SĐT: '.$phone;
            if($bring_cv == 1){
                $content .= '<br><br>Vui lòng mang theo hồ sơ photo & CMND khi đến buổi phỏng vấn';
            }
            
            $content .= '<br><br>Trân trọng,'. $hr_signature;
           
            $mailto = array();
          //  $mailto[] = 'nvdiemit@gmail.com';
			 $mailto[] = $email;
            $res = $this->sendmail($mailto, $subject, $content);
            
            $data = array('status'=> 7);
            if($res == 1){
                $QCv = new Application_Model_Cv();
                $where = $QCv->getAdapter()->quoteInto('id = ?', $id);
                $QCv->update($data,$where);
            
                $reponse['status']=1;
                $reponse['message']="Thành công";
            }else{
                $reponse['status']=$res;
                $reponse['message']="Lỗi";
            }
            
            echo json_encode($reponse);
        }catch (Exception $e) {
            
        }
        
    }
    
    public function emailDenyAction(){
        $hr_signature = '
                    <h3 style="color: #00b050;margin-bottom: 0;">Recruitment Team</h3>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">T (84-8) 39202555 - ext: 111</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">F (84-8) 39202555 - ext: 120</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">E <a href="mailto:recruitment@oppo-aed.vn">recruitment@oppo-aed.vn</a></p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">12th Floor, Lim II Tower, No. 62A CMT8 St., Ward 6, Dist. 3, HCMC, VN</p>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 0;"><a href="http://vieclam.oppomobile.vn/">www.vieclam.oppomobile.vn</a></p>';
        
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $id             = $this->getRequest()->getParam('id_cv');
        $name           = $this->getRequest()->getParam('name');
        $en_name        = $this->getRequest()->getParam('en_name');
        $email     = $this->getRequest()->getParam('email');
       
        /////////////////////////
        /////// CONFIG
        /////////////////////////
        try {
            error_reporting(~E_ALL);
            ini_set("display_error", 0);
            set_time_limit(0);
            $mailfrom = 'recruitment@oppo-aed.vn'; // Config this
            $mailfromname = "OPPO tuyển dụng"; // Config this
            //123321abc
            $subject = 'Thank you Letter - OPPO Vietnam';
			
			$content = 'Chào '. $name .',<br><br>Cám ơn bạn đã dành thời gian quan tâm để cơ hội việc làm và ứng tuyển tại OPPO Việt Nam';
            $content .= ' <br><br>Chúng tôi đánh giá cao khả năng của bạn, tuy nhiên chúng tôi rất tiếc phải thông báo bạn chưa phù hợp với vị trí ứng tuyển. Chúng tôi sẽ lưu hồ sơ của bạn và liên hệ lại khi có vị trí phù hợp hơn với bạn trong thời gian tới.';
            $content .= '<br><br>Thay mặt OPPO Việt Nam, chúc bạn nhiều sức khỏe và luôn thành công.';
			
			$content .= '<br><br>---------------------------------------------------------------';
			
            $content .= '<div><p class="">Dear '. $en_name.',
                <br><br>Thank you very much for applying for OPPO Viet Nam';
            $content .= '<br><br>We highly appreciate your qualification and skills. After careful consideration, we regret to inform you that we are unable to consider your application for this position. However, we will keep your application in our system and will contact you in the future when there is any suitable position that fits your qualifications';
            $content .= '<br><br>On behalf of OPPO Viet Nam, we sincerely wish you perfect health and success in your career.'. $hr_signature;
           
            $mailto = array();
         //   $mailto[] = 'nvdiemit@gmail.com';
			 $mailto[] = $email;
            $res = $this->sendmail($mailto, $subject, $content);
            
            $data = array('status'=> 8);
            if($res == 1){
                $QCv = new Application_Model_Cv();
                $where = $QCv->getAdapter()->quoteInto('id = ?', $id);
                $QCv->update($data,$where);
                
                $reponse['status']=1;
                $reponse['message']="Thành công";
            }else{
                $reponse['status']=$res;
                $reponse['message']="Lỗi";
            }
            /////////////////////////
            /////// GET STAFFs HAVING BIRTHDAY TODAY
            /////////////////////////
            $flashMessenger       = $this->_helper->flashMessenger;
            $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
            $this->view->messages_success = $messages_success;
            $back_url = HOST . 'job/cv';
//             echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
//             echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
//             echo '<div class="alert alert-success">Success!</div>';
//             echo '<script>setTimeout(function(){parent.location.href="' . $back_url .
//             '"}, 10000)</script>';
            
                
            
            echo json_encode($reponse);
        }catch (Exception $e) {
            
        }
        
        
    }
    
    
    private function sendmail($to, $subject, $maildata, $image = ''){
        try {
            
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            $email_cc = $userStorage->email;
            
            //todo send mail
            $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

            $config = array(
                'auth' => $app_config->mail->smtp->auth,
                'username' => 'recruitment@oppo-aed.vn',
                'password' => '0pp0Te@m2019',
                'port' => $app_config->mail->smtp->port,
                'ssl' => $app_config->mail->smtp->ssl
            );


            $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);
            
            $mail = new Zend_Mail($app_config->mail->smtp->charset);
            
            $mail->setBodyHtml($maildata);
            if($image != ''){
                $mail->addAttachment($image);
            }
            
            $mail->setFrom('recruitment@oppo-aed.vn', 'OPPO Recruitment');

            $mail->addTo($to);
            $mail->addCc($email_cc);
            
            $mail->setSubject($subject);

            $r = $mail->send($transport);

            if (!$r)
                return -1;
            else return 1;
        } catch (Exception $exc) {
            return -1;
        }
    }
    
    private function sendmailjob($to, $subject, $maildata, $image = ''){
        include_once APPLICATION_PATH.'/../library/phpmail/class.phpmailer.php';
        $app_config   = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config       = $app_config->mail->smtp;

        $mailfrom     = 'recruitment@oppo-aed.vn';
    // $mailfrom     = $config->user2;
        $mailfromname = 'OPPO Tuyển dụng';
        $mail = new PHPMailer();

        $body = $maildata; // nội dung email
        $body = eregi_replace("[\]",'',$body);

        $mail->IsHTML();

        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = $config->ssl;                 // sets the prefix to the servier // Config this
        $mail->Host       = $config->host;      // sets GMAIL as the SMTP server // Config this
        $mail->Port       = $config->port;                   // set the SMTP port for the GMAIL server // Config this
       // $mail->Username   = $config->user2;  // GMAIL username // Config this
        $mail->Username   = 'recruitment@oppo-aed.vn';  // GMAIL username // Config this
     //   $mail->Password   = $config->pass2;            // GMAIL password // Config this
        $mail->Password   = '0pp0Te@m2019';            // GMAIL password // Config this
        
        $mail->From = $mailfrom;
        $mail->FromName = $mailfromname;

        $mail->SetFrom($mailfrom, $mailfromname); //Định danh người gửi

        //$mail->AddReplyTo($mailfrom, $mailfromname); //Định danh người sẽ nhận trả lời

        $mail->Subject    = $subject; //Tiêu đề Mail

        $mail->AltBody    = "Để xem tin này, vui lòng bật chế độ hiển thị mã HTML!"; // optional, comment out and test

        // $mail->AddAttachment($image);
        if($image != '')
            $mail->AddEmbeddedImage($image, 'embed_img');

        $mail->MsgHTML($body);

        if (is_array($to)) {
            foreach ($to as $key => $value) {
                $mail->AddAddress($value, ''); //Gửi tới ai ?
            }
        } else {
            $mail->AddAddress($to, ''); //Gửi tới ai ?
        }
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QStaff = new Application_Model_Staff();
        $select_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $infoStaff = $QStaff->fetchRow($select_staff);
        $emailInfo      = $infoStaff->email;
//        $mail->addBcc($emailInfo);
        $mail->addBcc('recruitment@oppo-aed.vn');

        if(!$mail->Send() ) {
//             echo date('Y-m-d H:i:s')." - Failed\n";
            return -1;
        } else {
            //echo date('Y-m-d H:i:s')." - Done\n"; //DEBUG
            return 1;
        }

    }
    
    function postSMS($phone, $content){
         
        try {
            require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
            include_once 'Sms.php';
    
            $client = new nusoap_client("http://brandsms.vn:8018/VMGAPI.asmx?wsdl", 'wsdl', '', '', '', '');
            $err = $client->getError();
            	
            if ($err) {
                echo "SMS Error";exit;
            }
            	
            $sendTime     = date('d/m/Y h:i');
            $phone = preg_replace('/[^0-9]/','',$phone);
    
            if(!in_array(strlen($phone) , array(10,11)))
            {
                //echo "Invalid phone number";
            }
    
            $fphone = preg_replace('/^0/', '84', $phone);
            $fphone = preg_replace('/^\+84/', '84', $fphone);
    
            $result = $client->call('BulkSendSms',
                array(
                    'msisdn' => $fphone,
                    'alias'=> 'OPPO',
                    'message' => sprintf('%s', $content),
                    'sendTime'=>$sendTime,
                    'authenticateUser'=>'oppo2',
                    'authenticatePass'=>'vmg123456'
                ),'','',''
                );
    
            if ($client->fault) {
                //echo "- " . $phone . " : FAULT : <br /><pre>" . $result."</pre><br />";
            } else {
                // Check for errors
                $err = $client->getError();
                if ($err) {
                    // Display the error
                    //echo "- " . $phone . " : ERROR : <br /><pre>" . $err."</pre><br />";
    
                } else {
                    return ($result);
                }
            }
        }
        catch (Exception $e){
            $message = $e->getMessage();
            if (!isset($code)) {
                $code = 2;
                $message = 'Unknown error';
            }
            $rs['code'] = $code;
            $rs['message'] = $message;
    
        }
    }
    /* end of post SMS */
    
    function testSmsAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $phone = '01687048216';
        $content = 'test send sms';
        $rs = $this->postSMS($phone, $content);
        echo "<pre>";
        print_r($rs);
        echo "</pre>";
        echo 'success';
        exit;
    }
	
	function sendSmsAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $time           = $this->getRequest()->getParam('time');
        $date           = $this->getRequest()->getParam('date');
        $kindly_contact     = $this->getRequest()->getParam('kindly_contact');
        $en_kindly_contact  = $this->getRequest()->getParam('en_kindly_contact');
        $phone          = $this->getRequest()->getParam('phone');
        $address        = $this->getRequest()->getParam('address');
        $en_address     = $this->getRequest()->getParam('en_address');
        $position       = $this->getRequest()->getParam('position');
        $en_position    = $this->getRequest()->getParam('en_position');
        $email          = $this->getRequest()->getParam('email');
        $id             = $this->getRequest()->getParam('id_cv');
        $name           = $this->getRequest()->getParam('name');
        $en_name        = $this->getRequest()->getParam('en_name');
        
        $content .= 'Gui ' . $en_name . ',' . 'Phong nhan su OPPO xac nhan lich phong van voi bạn cho vi tri nhu sau: ';
        $content .= 'Thoi gian: ' . $time . 'ngay ' . $date;
        $content .= 'DT ho tro: 0839202555 (nhanh 111) Hen gap ban tai buoi phong van. Tran trong';
        
        $rs = $this->postSMS($phone, $content);
        
        $reponse['status']=1;
        $reponse['message']="Thành công";
        
        echo json_encode($reponse);
        
    }
	
    function _exportExcelCv($data){
        $QCvLanguage = new Application_Model_CvLanguage();
        $CvLanguage=$QCvLanguage->fetchAll();
        ini_set("memory_limit", -1);
        ini_set("display_error", 0);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            '#',
            'Tên',
            'Giới Tính',
            'Địa chỉ',
            'Khu vực đăng tuyển',
            'Vị trí',
            'Department',
            'Nguyện vọng làm tại',            
            'Ngày sinh',
            'Số điện thoại',
            'Email', 
            'Ngày tạo',
            'Trạng thái',
            'Nguồn',
            'ID',
            'Ngành',
            'Ngoại ngữ',
            'Trình độ ngoại ngữ',
            'Tiềm năng',
            'Mức lương',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $regional_market = $QRegionalMarket->get_cache();

        $QCvSource = new Application_Model_CvSource();
        $source    = $QCvSource->fetchAll();
        $data_source = [];
        foreach ($source as $key => $value) {
            $data_source[$value['id']] = $value['name'];
        }

                

        foreach($data as $item){


            $other_language=""; 
            $other_language_level=""; 

            if(!empty($CvLanguage)){
                foreach ($CvLanguage as $key_language => $value_language) {
                    if($value_language["cv_id"]==$item["id"]){
                           switch ($value_language["language"]) {
                                 case '1':
                                 $other_language=$other_language."Tiếng-Anh ";    
                                 break;
                                 case '2':
                                 $other_language=$other_language."Tiếng-Hoa ";    
                                 break;
                                 case '3':
                                 $other_language=$other_language."Tiếng-Hàn ";    
                                 break;
                                 case '4':
                                 $other_language=$other_language."Tiếng-Nhật ";    
                                 break;                                 
                                 case '5':
                                 $other_language=$other_language."Tiếng-Pháp ";    
                                 break;
                            }
                           switch ($value_language["level"]) {
                                 case '1':
                                 $other_language_level=$other_language_level."Sơ-cấp ";    
                                 break;
                                 case '2':
                                 $other_language_level=$other_language_level."Trung-cấp ";    
                                 break;
                                 case '3':
                                 $other_language_level=$other_language_level."Cao-cấp ";    
                                 break;
                           }
                     }
                 } 
                }
            $other_language= str_replace("-"," ",str_replace(" ",", ",trim($other_language)));
            $other_language_level= str_replace("-"," ",str_replace(" ",", ",trim($other_language_level)));

            $region          = !empty($regional_market[$item['regional_market']]) ? $regional_market[$item['regional_market']] : '';
            $district_center = $item['district_center']." - ".$item['province_name'];
            $dob             = new DateTime($item['dob']);
            $dob             = date_format($dob,"d/m/Y");

            $created_at = new DateTime($item['created_at']);
            $created_at = date_format($created_at,"d/m/Y");

            $list_status = unserialize(JOB_STATUS);
            $status = $list_status[$item['status']];

            $src = $data_source[$item['source']];

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['name']);
            $sheet->setCellValue($alpha++.$index, ( (!empty($item['gender']) == 1) ? 'Nam' : 'Nữ') );
            $sheet->setCellValue($alpha++.$index, $item['address']);
            $sheet->setCellValue($alpha++.$index, $item['job_area']);
            $sheet->setCellValue($alpha++.$index, $item['job_title']);


            $sheet->setCellValue($alpha++.$index, $item['department_name']);
            $sheet->setCellValue($alpha++.$index, $item['cv_area']);
            $sheet->setCellValue($alpha++.$index, $dob);
            $sheet->setCellValue($alpha++.$index, $item['phone']);
            $sheet->setCellValue($alpha++.$index, $item['email']);
            $sheet->setCellValue($alpha++.$index, $created_at);
            $sheet->setCellValue($alpha++.$index, !empty($status) ? $status : 'Chưa xem');
            $sheet->setCellValue($alpha++.$index, !empty($src) ? $src : ' ');
            $sheet->setCellValue($alpha++.$index, $item['id']);
           
            if(!empty($item['cat_name'])){
                 $sheet->setCellValue($alpha++.$index, $item['cat_name']);
            }else{
            $sheet->setCellValue($alpha++.$index, $item['job_group_name']);
            }
            $sheet->setCellValue($alpha++.$index, $other_language);
            $sheet->setCellValue($alpha++.$index, $other_language_level);

            $sheet->setCellValue($alpha++.$index, !empty($item['is_potential']) ? "x":null);
            $sheet->setCellValue($alpha++.$index, !empty($item['salary']) ? number_format($item['salary']):null);
            $index++;

        }

        $filename = 'Report_CV_'.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }
	   
    public function massSendEmailAction(){
        require_once 'job' . DIRECTORY_SEPARATOR .'mass-send-email.php';
    }
    
    public function saveSendEmailAction(){
        require_once 'job' . DIRECTORY_SEPARATOR .'save-send-email.php';
    }
    
    public function contentSendMail($name,$email,$title,$link){
        require_once 'job' . DIRECTORY_SEPARATOR .'content-send-email.php';
    }
    
    public function _exportEmail($data_export){
        require_once 'PHPExcel.php';
        
        $alphaExcel = new My_AlphaExcel();
        
        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Email',
        );
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }
        
        foreach($data_export as $key => $val)
        {
            $alphaExcel = new My_AlphaExcel();
        
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val );
        
        }
        
        $filename = 'Send_Email_Success_' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        
        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
		
		
    }

    public function sendMailAction(){
        $QJob = new Application_Model_Job();
        $action_jobs = $QJob->fetchSendMailList();
        if(is_array($action_jobs) && count($action_jobs)!=0){
            foreach ($action_jobs as $cv) {
                // GET CONTENT
                $this_mailto = $cv['infor']['email'];
                $title       = $cv['infor']['title'];
                $this_subject = "Hồ sơ ứng tuyển cho vị trí $title";
                $this_content = $this->getMailContent($cv['infor'],$cv['cvs']);
                // SEND MAIL
                 $this->sendmailjob($this_mailto, $this_subject, $this_content);
            }
        }else{
            echo "no cv is sent yet";
        }
        die();
    }
    

    public function getMailContent($job, $cvs){
        $QLogMail = new Application_Model_LogJobEmail();
        $job_id = $job['id'];
        $job_title = $job['title'];
        $mail_template = "Xin chào [created_by],<br><br>Hệ thống center OPPO vừa nhân được [count_cv] hồ sơ ứng tuyển cho $job_title<br>";
        
        $mail_template = str_replace('[created_by]', $job['fullname'], $mail_template);
        $mail_template = str_replace('[count_cv]', count($cvs), $mail_template);
        
        // echo $mail_template;
        foreach ($cvs as $key => $item) {
            $this_link = HOST."job/view-cv?id=". $item['id'];
            $mail_template.= 'Link truy cập hồ sơ '.($key+1).': <a href="'.$this_link.'" target="_blank">Đường dẫn</a> <br>';
                // SEND MAIL AND INSERT TO LOG
                $QLogMail->insert([
                    'job_id'    =>  $job_id,
                    'cv_id'     =>  $item['id'],
                    'sent_at'   =>  date('Y-m-d H:i:s'),
                ]);
        }

        $mail_template.="<br>Trân trọng.";
        $hr_signature = '
                    <h3 style="color: #00b050;margin-bottom: 0;">Recruitment Team</h3>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">T (84-8) 39202555 - ext: 111</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">F (84-8) 39202555 - ext: 120</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">E <a href="mailto:recruitment@oppo-aed.vn">recruitment@oppo-aed.vn</a></p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">12th Floor, Lim II Tower, No. 62A CMT8 St., Ward 6, Dist. 3, HCMC, VN</p>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 0;"><a href="http://vieclam.oppomobile.vn/">www.vieclam.oppomobile.vn</a></p>';
        return $mail_template.$hr_signature;
    }


}
