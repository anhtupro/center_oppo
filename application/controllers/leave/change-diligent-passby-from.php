<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$diligent_passby_from = $this->getRequest()->getParam('diligent_passby_from');
$leave_id = $this->getRequest()->getParam('leave_id');

$QLeaveDetail = new Application_Model_LeaveDetail();
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$staff_id = $userStorage->id;
if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $diligent_passby_from)) {
    print_r(json_encode(array('code' => -1, 'message' => 'Không đúng định dạng. Vui lòng chọn trên popup!')));
    exit;
}

if (!empty($leave_id) AND !empty($diligent_passby_from)) {
    $where = $QLeaveDetail->getAdapter()->quoteInto('`id` = ?', $leave_id);
    $leave_detail = $QLeaveDetail->find($leave_id)->current();
    $leave_type = $leave_detail->toArray()['leave_type'];

    $datetime = date('Y-m-d H:i:s');
    $data = array(
        'diligent_passby_from' => $diligent_passby_from,
        'diligent_passby_to' =>date('Y-m-d', strtotime("+12 months", strtotime($diligent_passby_from))),
        'system_note' => "UPDATE_BY:$staff_id created_at: $datetime"
    );
    if ($leave_type == 17) {
        $QLeaveDetail->update($data, $where);
        print_r(json_encode(array('code' => 1, 'message' => 'Thay đổi thành công')));
        exit;
    } else {
        print_r(json_encode(array('code' => -1, 'message' => 'Thất bại')));
        exit;
    }
} else {
    print_r(json_encode(array('code' => -1, 'message' => 'Thay đổi thất bại')));
    exit;
}
