<?php

$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$sauchedo_to_date = $this->getRequest()->getParam('sauchedo_to_date');
$leave_id = $this->getRequest()->getParam('leave_id');
$staff_id_leave = $this->getRequest()->getParam('staff_id_leave');

$QLeaveDetail = new Application_Model_LeaveDetail();
$QLeaveDetailDay = new Application_Model_LeaveDetailDay();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;
$list_detail_leave = $QLeaveDetail->getLeaveById(null, $staff_id_leave);
$old_detail_leave = $QLeaveDetail->getLeaveById($leave_id, $staff_id_leave);

if (!empty($leave_id) AND!empty($sauchedo_to_date)) {

    if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $sauchedo_to_date)) {
        print_r(json_encode(array('code' => -1, 'message' => 'Không đúng định dạng. Vui lòng chọn trên popup!')));
        exit();
    }
    if ($sauchedo_to_date < $old_detail_leave['joined_at']) {
        print_r(json_encode(array('code' => -1, 'message' => 'Ngày nghỉ phép phải lớn hơn ngày vào làm')));
        exit();
    }
    if ($sauchedo_to_date < $old_detail_leave['from_date']) {
        print_r(json_encode(array('code' => -1, 'message' => 'Ngày nghỉ phép không hợp lệ')));
        exit();
    }

    if (!empty($QLeaveDetail->checkDupLeaveChangeToDate($leave_id, $staff_id_leave, $old_detail_leave['from_date'], $sauchedo_to_date))) {
        print_r(json_encode(array('code' => -1, 'message' => 'Ngày nghỉ phép đã tồn tại')));
        exit();
    }
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        $where = $QLeaveDetail->getAdapter()->quoteInto('`id` = ?', $leave_id);
        $leave_detail = $QLeaveDetail->find($leave_id)->current();
        $to_date_old = $leave_detail->toArray()['to_date'];
        $leave_type = $leave_detail->toArray()['leave_type'];

        $datetime = date('Y-m-d H:i:s');

        $select_total = $db->select()->from(array('ad' => 'all_date'), array('total' => 'COUNT(ad.id)'))
                ->join(array('st' => 'staff'), "st.id = $staff_id")
                ->join(array('t' => 'team'), 't.id = st.title')
                ->joinLeft(array('ada' => 'all_date_advanced'), 'ada.date = ad.date AND ada.type_id = t.policy_group AND ada.type = 4')
                ->where("ad.date BETWEEN '$leave_detail->from_date' AND '$sauchedo_to_date'")
                ->where('ada.id IS NULL');
        $result_total = $db->fetchAll($select_total);
        $total = $result_total[0]['total'];

        $data = array(
            'total' => $total,
            'to_date' => $sauchedo_to_date,
            'to_date_before' => $to_date_old,
            'system_note' => "UPDATE_BY:$staff_id  Update to_date_old: $to_date_old created_at: $datetime"
        );
        $QLeaveDetail->update($data, $where);

        // detail day
        $where_detail_day = [];
        $where_detail_day[] = $QLeaveDetailDay->getAdapter()->quoteInto('leave_detail_id = ?', $leave_id);
        $QLeaveDetailDay->delete($where_detail_day);

        $sql = "INSERT INTO `leave_detail_day` (`leave_detail_id`, `date`, `is_special_day`, `is_year_leave`)
    		SELECT
    		$leave_id AS `leave_detail_id`,
    		ad.date AS `date`,
    		IF(ada.id IS NULL, 0, 1) AS `is_special_day`,
    		0 AS `is_year_leave`
    		FROM `all_date` ad
    		JOIN `staff` st
    		ON st.id = $leave_detail->staff_id
    		JOIN `team` t
    		ON t.id = st.title
    		LEFT JOIN `all_date_advanced` ada
    		ON ada.date = ad.date
    		AND ada.type_id = t.policy_group
    		AND ada.type = 4
    		WHERE
    		(ad.date BETWEEN '$leave_detail->from_date' AND '$sauchedo_to_date')";
        $stmt = $db->query($sql);

        $db->commit();
        print_r(json_encode(array('code' => 1, 'message' => 'Thay đổi thành công')));
        exit();
    } catch (Exception $ex) {
        $db->rollback();
        print_r(json_encode(array('code' => -1, 'message' => 'Thay đổi thất bại')));
        exit();
    }
} else {
    print_r(json_encode(array('code' => -1, 'message' => 'Thay đổi thất bại')));
    exit();
}