<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$to_date_after = $this->getRequest()->getParam('to_date_after');
$leave_id         = $this->getRequest()->getParam('leave_id');
$staff_id_leave         = $this->getRequest()->getParam('staff_id_leave');

$QLeaveDetail         = new Application_Model_LeaveDetail();
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$staff_id = $userStorage->id;
$list_detail_leave =$QLeaveDetail->getLeaveById(null,$staff_id_leave);
$old_detail_leave =$QLeaveDetail->getLeaveById($leave_id,$staff_id_leave);
if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$to_date_after)) {
    print_r(json_encode(array('code'=>-1,'message'=>'Không đúng định dạng. Vui lòng chọn trên popup!')));
    exit;
}
if($to_date_after<$old_detail_leave['joined_at']){
    print_r(json_encode(array('code'=>-1,'message'=>'Ngày nghỉ phép phải lớn hơn ngày vào làm')));
    exit;
}
if($to_date_after<$old_detail_leave['from_date']){
    print_r(json_encode(array('code'=>-1,'message'=>'Ngày nghỉ phép không hợp lệ')));
    exit;
}
foreach($list_detail_leave as $key=>$value){
    if($value['id']!=$old_detail_leave['id']){
        if($sauchedo_to_date>$value['from_date']&&$sauchedo_to_date<$value['to_date']){
           $a= print_r(json_encode(array('code'=>-1,'message'=>'Ngày nghỉ phép đã tồn tại')));
            exit;
        }
    }
}
$type = 1;
if(!empty($leave_id) AND !empty($to_date_after) ){
    $where        = $QLeaveDetail->getAdapter()->quoteInto('`id` = ?',$leave_id);
    $leave_detail = $QLeaveDetail->find($leave_id)->current();
    $to_date_old  = $leave_detail->toArray()['to_date'];
    $leave_type   = $leave_detail->toArray()['leave_type']; 
          
        $datetime = date ( 'Y-m-d H:i:s' );
        $data = array(
            'to_date_after'        => $to_date_after,
        );
        if(in_array($leave_type, array(17, 23)) ){
            $QLeaveDetail->update($data, $where);
            print_r(json_encode(array('code'=>1,'message'=>'Thay đổi thành công')));
            exit;
        }else{
            print_r(json_encode(array('code'=>-1,'message'=>'Thất bại')));
            exit;
        }
}else{
    print_r(json_encode(array('code'=>-1,'message'=>'Thay đổi thất bại')));
    exit;
}
