<?php 
$this->_helper->viewRenderer->setNoRender();
$QLeaveDetail = new Application_Model_LeaveDetail();
$QStaff       = new Application_Model_Staff();
$staff_code = $this->getRequest()->getParam('code');
if(!empty($staff_code)){
    $where          = array();
    $where[]        = $QStaff->getAdapter()->quoteInto('code = ?', $staff_code);
    $result_staff   = $QStaff->fetchRow($where);
    echo "<pre>";
    print_r($result_staff);
    echo "</pre>";
    exit();
}

$time = $this->getRequest()->getParam('time', date('m').'/'.date('Y'));

$array_time = explode("/", $time);
$month = $array_time[0];
$year = $array_time[1];

$from = $year . '-' . $month . '-01';
$to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

$params = array(
    'staff_id' => Zend_Auth::getInstance()->getStorage()->read()->id,
    'month' => $month,
    'year' => $year,
    'from' => $from,
    'to' => $to,
);

$db  = Zend_Registry::get('db');
//Lấy chu kỳ phép
$stmt = $db->prepare("SELECT * FROM v_staff_contract_begin WHERE staff_id = $staff_id");
//$stmt = $db->prepare('SELECT MAX(from_date) from_date, MAX(to_date) to_date, TIMESTAMPDIFF(MONTH,DATE_ADD(MAX(from_date), INTERVAL -1 MONTH),MAX(to_date)) total FROM leave_life_time');
$stmt->execute();
$result_staff_contract_begin = $stmt->fetchAll();
$stmt->closeCursor();
$this->view->staff_contract_begin = $result_staff_contract_begin[0];

$data = $QLeaveDetail->_select($limit, $page, $params);
$this->view->data = $data;
$this->view->limit = $limit;
$this->view->offset = $page;
$this->view->total = $data['total'];
$this->view->params = $params;
$this->view->url = HOST . 'leave/list-my-leave' . ('' ? '?' . http_build_query('') . '&' : '?');
$QLeavInfo = new Application_Model_LeaveInfo();
$this->view->stock = $QLeavInfo->getStockById($staff_id);
$this->view->year_stock = $QLeavInfo->getYearStockById($staff_id);
$this->view->historyLeave = $QLeavInfo->getHistoryLeave($staff_id);
$flashMessenger = $this->_helper->flashMessenger;
$this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
