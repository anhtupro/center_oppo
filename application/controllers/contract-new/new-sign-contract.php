<?php

$flashMessenger    = $this->_helper->flashMessenger;
$userStorage       = Zend_Auth::getInstance()->getStorage()->read();
$db                = Zend_Registry::get('db');
$export            = $this->getRequest()->getParam('export');
$name              = $this->getRequest()->getParam('name', '');
$code              = $this->getRequest()->getParam('code', '');
$office            = $this->getRequest()->getParam('office', -1);
$signed_to         = $this->getRequest()->getParam('signed_to', '');
$area_id           = $this->getRequest()->getParam('area_id', NULL);
$department        = $this->getRequest()->getParam('department', NULL);
$team              = $this->getRequest()->getParam('team', NULL);
$title             = $this->getRequest()->getParam('title', NULL);
$off               = $this->getRequest()->getParam('off', 0);
$contract_term     = $this->getRequest()->getParam('contract_term', NULL);
$company_id        = $this->getRequest()->getParam('company_id', NULL);
$btnSetPrintStatus = $this->getRequest()->getParam('btnSetPrintStatus');
$btnSetDate        = $this->getRequest()->getParam('btnSetDate');
$ids               = $this->getRequest()->getParam('id');
$update_probation  = $this->getRequest()->getParam('update_probation');
$print_type        = $this->getRequest()->getParam('print_type');

$params = array(
    'name'             => trim($name),
    'code'             => trim($code),
    'signed_to'        => $signed_to,
    'print_type'         => $print_type,
    'office'           => $office,
    'area_id'          => $area_id,
    'department'       => $department,
    'team'             => $team,
    'title'            => $title,
    'off'              => $off,
    'contract_term'    => $contract_term,
    'company_id'       => $company_id,
    'ids'              => $ids,
    'update_probation' => $update_probation
);
$limit  = 600;
$total  = 0;
$desc   = $this->getRequest()->getParam('desc', 1);
$sort   = $this->getRequest()->getParam('sort');
$page   = $this->getRequest()->getParam('page', 1);


if ($update_probation) {
    $this->_updateCurrentProbation($params);
}
if ($btnSetPrintStatus) {
    $this->_setPrintStatus($params);
}
if ($export) {
    $limit = $page  = 0;
}
$staff_contract_model = new Application_Model_StaffContract();
$list                 = $staff_contract_model->StaffContractNew($page, $limit, $total, $params);

if ($export) {
    $this->_exportInfo($list);
}
$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$messages_error   = $flashMessenger->setNamespace('error')->getMessages();

$this->view->list             = $list;
$this->view->desc             = $desc;
$this->view->sort             = $sort;
$this->view->limit            = $limit;
$this->view->total            = $total;
$this->view->page             = $page;
$this->view->url              = HOST . 'contract-new/new-sign-contract' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset           = $limit * ($page - 1);
$this->view->params           = $params;
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;


$QProposal             = new Application_Model_AsmProposal();
$proposals             = $QProposal->get_all();
$this->view->proposals = $proposals;

$QContractType             = new Application_Model_ContractTypes();
$QContractType             = $QContractType->get_cache();
$this->view->contractTerms = $QContractType;

$QProvince             = new Application_Model_Province();
$provinces             = $QProvince->get_all();
$this->view->provinces = $provinces;

$QTeam                                   = new Application_Model_Team();
$recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$this->view->userStorage                 = $userStorage;


$area_id_confirm = array();
$group_id        = $userStorage->group_id;
if ($group_id != HR_ID) {
    $sql      = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = " . $userStorage->id .
            " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = " . $userStorage->id;
    $stmt1    = $db->query($sql);
    $area_tmp = $stmt1->fetchAll();
    foreach ($area_tmp as $item) {
        $area_id_confirm[] = $item['area_id'];
    }
    $this->view->area_id_confirm = $area_id_confirm;
}

$teams = array();
if ($department AND count($department) > 0) {
    foreach ($department as $key => $value) {
        $teams += $recursiveDeparmentTeamTitle[$value]['children'];
    }
}
$this->view->teams = $teams;

$titles = array();
if ($team AND $department AND count($team) > 0) {
    foreach ($team as $key => $value) {
        $titles += $teams[$value]['children'];
    }
}
$this->view->titles = $titles;

$QArea             = new Application_Model_Area();
$areas             = $QArea->get_cache();
$this->view->areas = $areas;
