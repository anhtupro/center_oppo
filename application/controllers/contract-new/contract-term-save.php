<?php
if ($this->getRequest()->getMethod() == 'POST'){
    $QModel = new Application_Model_ContractTerm();

    $id = $this->getRequest()->getParam('id');
    $name = $this->getRequest()->getParam('name');
    $addday = $this->getRequest()->getParam('addday');
    $start_date = $this->getRequest()->getParam('start_date');
    $end_date = $this->getRequest()->getParam('end_date');
    
    $data = array(
        'name_contract'       => $name,
        'addday'     => $addday,
        'start_date' => $start_date,
        'end_date'   => $end_date
    );

    if ($id){
        $where = $QModel->getAdapter()->quoteInto('id = ?', $id);

        $QModel->update($data, $where);
    } else {
        $QModel->insert($data);
    }

    //remove cache
    $cache = Zend_Registry::get('cache');
    $cache->remove('contract_term_cache');

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}

$back_url = $this->getRequest()->getParam('back_url');

$this->_redirect( ( $back_url ? $back_url : HOST.'contract-new/contract-term' ) );