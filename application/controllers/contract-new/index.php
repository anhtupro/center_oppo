<?php 
$btnConfirm        = $this->getRequest()->getParam('btnConfirm');
$rollback          = $this->getRequest()->getParam('btnRollback');
$export            = $this->getRequest()->getParam('export');
$export_info       = $this->getRequest()->getParam('export_info');
$refresh           = $this->getRequest()->getParam('btnRefresh');
$btnSetDate        = $this->getRequest()->getParam('btnSetDate');
$btnSetPrintStatus = $this->getRequest()->getParam('btnSetPrintStatus');
$btnApproveByStaff = $this->getRequest()->getParam('btnApproveByStaff',0);

$ids               = $this->getRequest()->getParam('id');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id                = $userStorage->id;
$db                      = Zend_Registry::get('db');
$this->view->userStorage = $userStorage;

$flashMessenger     = $this->_helper->flashMessenger;
$name               = $this->getRequest()->getParam('name','');
$code               = $this->getRequest()->getParam('code','');
$status             = $this->getRequest()->getParam('status',-1);
$office             = $this->getRequest()->getParam('office',-1);
$expired_from       = $this->getRequest()->getParam('expired_from',date('01/m/Y'));
$expired_to         = $this->getRequest()->getParam('expired_to',date('t/m/Y'));

if($expired_to != ''){
    if($expired_to == date('t/m/Y')){
        $date_tmp = My_Date::normal_to_mysql($expired_to);
        $date_ex  = date_create($date_tmp);
        date_add($date_ex,date_interval_create_from_date_string("5 days"));  
        $expired_to = date_format($date_ex,"d/m/Y");  
    }
}


$signed_from        = $this->getRequest()->getParam('signed_from','');
$signed_to          = $this->getRequest()->getParam('signed_to','');
$print_type         = $this->getRequest()->getParam('print_type');
$letter_status      = $this->getRequest()->getParam('letter_status');
$area_id            = $this->getRequest()->getParam('area_id',NULL);
$department         = $this->getRequest()->getParam('department',NULL);
$team               = $this->getRequest()->getParam('team',NULL);
$title              = $this->getRequest()->getParam('title',NULL);
$different          = $this->getRequest()->getParam('different',NULL);
$off                = $this->getRequest()->getParam('off',0);
$contract_term      = $this->getRequest()->getParam('contract_term',NULL);
$print_status       = $this->getRequest()->getParam('print_status',-1);
$company_id         = $this->getRequest()->getParam('company_id',NULL);
$set_date           = $this->getRequest()->getParam('set_date',date('d/m/Y'));
$return_letter_from = $this->getRequest()->getParam('return_letter_from');
$return_letter_to   = $this->getRequest()->getParam('return_letter_to');
$re_active          = $this->getRequest()->getParam('re_active',0);

$desc               = $this->getRequest()->getParam('desc',1);
$sort               = $this->getRequest()->getParam('sort');
$office             = intval($office);
$print_status       = intval($print_status);
$back_url           = $this->getRequest()->getParam('back_url');
$send_letter_from   = $this->getRequest()->getParam('send_letter_from');
$send_letter_to     = $this->getRequest()->getParam('send_letter_to');
$return_letter      = $this->getRequest()->getParam('return_letter',0);
$month              = $this->getRequest()->getParam('confirm_date');
$area_id_confirm    = $this->getRequest()->getParam('area_id_confirm',0);
$confirm            =  $this->getRequest()->GetParam('confirm');
$page               = $this->getRequest()->getParam('page',1);
$limit              = 600;
$total              = 0;
$params = array(
    'name'               => trim($name),
    'code'               => trim($code),
    'status'             => intval($status),
    'expired_from'       => $expired_from,
    'expired_to'         => $expired_to,
    'signed_from'        => $signed_from,
    'signed_to'          => $signed_to,
    'print_type'         => $print_type,
    'letter_status'      => intval($letter_status),
    'return_letter'      => intval($return_letter),
    'office'             => $office,
    'area_id'            => $area_id,
    'department'         => $department,
    'team'               => $team,
    'title'              => $title,
    'different'          => $different,
    'off'                => $off,
    'contract_term'      => $contract_term,
    'company_id'         => $company_id,
    'print_status'       => $print_status,
    'set_date'           => $set_date,
    'return_letter_from' => $return_letter_from,
    'return_letter_to'   => $return_letter_to,
    're_active'          => $re_active,
    'back_url'           => $back_url,
    'return_letter'      => $return_letter,
    'send_letter_from'   => $send_letter_from,
    'send_letter_to'     => $send_letter_to,

    //approve
    'btnConfirm'         => $btnConfirm,
    'confirm'            => $confirm,
    'month'              => $month,
    'area_id_confirm'    => $area_id_confirm,
    'ids'                => $ids,
    'confirm_date'       => date('m/Y'),
    'btnApproveByStaff'  => $btnApproveByStaff
);

if($btnConfirm OR $btnApproveByStaff){
    $this->_confirm($params);
}

if($rollback){
    $this->_rollback($params);
}

if($btnSetDate){
    $this->setTimeReturnLetter($params);
}
if($refresh){
    $this->_refresh($params);
}

if($btnSetPrintStatus){
    $this->_setPrintStatus($params);
}

$expired_from       = ($expired_from OR $expired_from != '') ? My_Date::normal_to_mysql($expired_from): NULL;
$expired_to         = ($expired_to OR $expired_to != '') ? My_Date::normal_to_mysql($expired_to): NULL;
$signed_from        = ($signed_from) ? My_Date::normal_to_mysql($signed_from) : NULL;
$signed_to          = ($signed_to) ? My_Date::normal_to_mysql($signed_to) : NULL;
$return_letter_from = ($return_letter_from) ? My_Date::normal_to_mysql($return_letter_from) : NULL;
$return_letter_to   = ($return_letter_to) ? My_Date::normal_to_mysql($return_letter_to) : NULL;
$send_letter_from   = ($send_letter_from) ? My_Date::normal_to_mysql($send_letter_from) : NULL;
$send_letter_to     = ($send_letter_to) ? My_Date::normal_to_mysql($send_letter_to) : NULL;

$QProposal             = new Application_Model_AsmProposal();
$proposals             = $QProposal->get_all();
$this->view->proposals = $proposals;

$QContractTerm = new Application_Model_ContractTerm();
$contractTerms = $QContractTerm->get_cache();
$this->view->contractTerms = $contractTerms;

$QProvince               = new Application_Model_Province();
$provinces               = $QProvince->get_all();
$this->view->provinces   = $provinces;

$QTeam = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

$area_id_confirm = array();
$group_id = $userStorage->group_id;
if($group_id != HR_ID){
    $sql = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = ".$userStorage->id.
        " UNION
            SELECT b.area_id FROM staff a
            INNER JOIN regional_market b ON a.regional_market = b.id
            WHERE a.id = ".$userStorage->id;
    ;
    $stmt1 = $db->query($sql);
    $area_tmp = $stmt1->fetchAll();

    foreach($area_tmp as $item):
        $area_id_confirm[] = $item['area_id'];
    endforeach;
    $this->view->area_id_confirm = $area_id_confirm;
}

$teams = array();
if( $department AND count($department) > 0 ){
    foreach ($department as $key => $value) {
        $teams  += $recursiveDeparmentTeamTitle[$value]['children'];
    }
}
$this->view->teams = $teams;

$titles = array();
if($team AND $department AND count($team) > 0){
    foreach ($team as $key => $value) {
        $titles += $teams[$value]['children'];;
    }
}
$this->view->titles = $titles;

$QArea             = new Application_Model_Area();
$areas             = $QArea->get_cache();
$this->view->areas = $areas;

$strtAreaId      = ( count($area_id) > 0 ) ? implode(',', $area_id) : '';
$strDepartment   = ( count($department) > 0 ) ? implode(',', $department) : '';
$strTeam         = ( count($team) > 0 ) ? implode(',', $team) : '';
$strTitle        = ( count($title) > 0 ) ? implode(',', $title) : '';
$strContractTerm = ( count($contract_term) > 0) ? implode(',', $contract_term) : '';
$strCompanyId    = ( count($company_id) > 0 ) ? implode(',', $company_id) : '';

$sql = 'CALL p_ASM_Contract_2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@total,?,?,?,?,?,?)';

if($export OR $export_info){
    $limit = $page = 0;
}

$sql_params = array(
    $staff_id,
    $params['name'],
    $params['code'],
    $office,
    $letter_status,
    $print_type,
    $params['status'],
    $signed_from,
    $signed_to,
    $expired_from,
    $expired_to,
    $strtAreaId,
    $strDepartment,
    $strTeam,
    $strTitle,
    $different,
    $off,
    $strContractTerm,
    $return_letter,
    $strCompanyId,
    $page,
    $limit,
    $print_status,
    $return_letter_from,
    $return_letter_to,
    $re_active,
    $send_letter_from,
    $send_letter_to
);

$stmt = $db->query($sql,$sql_params);
// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($stmt->fetchAll());die;
// }
$list = $stmt->fetchAll();
$stmt->closeCursor();

if($export){
    $this->_export($list);
}elseif($export_info){
    $this->_exportInfo($list);
}else{

}

$total = $db->fetchOne('select @total');
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$messages_error               = $flashMessenger->setNamespace('error')->getMessages();

$this->view->list             = $list;
$this->view->desc             = $desc;
$this->view->sort             = $sort;
$this->view->limit            = $limit;
$this->view->total            = $total;
$this->view->page             = $page;
$this->view->url              = HOST . 'contract/' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset           = $limit * ($page - 1);
$this->view->params           = $params;
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;

?>