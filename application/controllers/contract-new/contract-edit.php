<?php

$id = $this->getRequest()->getPost('id');
$salary_basic_contract = $this->getRequest()->getPost('salary_basic_contract',0); 

$QStaffSalary = new Application_Model_StaffSalary();
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$date = date('Y-m-d H:i:s');

$response=array('status'=>0);
if (!empty($id)) {
    
    $where = $QStaffSalary->getAdapter()->quoteInto('staff_contract_id = ?', $id);
    $StaffSalary_Row = $QStaffSalary->fetchRow($where);
    
    if(!empty($StaffSalary_Row)){
                     
                 $data_update = array( 'salary_basic_contract'=> $salary_basic_contract );
                 
                 $result =$QStaffSalary->update($data_update,$where);
                 $StaffSalary_Row_new = $QStaffSalary->fetchRow($where);
                 
                if(!empty($result) && !empty($StaffSalary_Row_new)){            
                  $response['status']=1;
                  $response['message']="Thành công";
                  $response['data']= $StaffSalary_Row_new->toArray();
                }else{
                   $response['message']="Edit thất bại"; 
                }            
    }else{
        $response['message']="Không tồn tại trong bảng staff_salary."; 
    }
}else{
    $response['message']="Thiếu id của hợp đồng";
}
echo json_encode($response);
exit();