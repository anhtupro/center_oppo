<?php

$db = Zend_Registry::get('db');


// $QContractTmpProcess = new Application_Model_ContractTmpProcess();
// $where = $QContractTmpProcess->getAdapter()->quoteInto('1');



// $data = $QContractTmpProcess->fetchAll($where);
// $db   = Zend_Registry::get('db');
// foreach ($data as $key => $val){
//     $id             = $val['id'];
//    $department_id   = $val['department_id'];
//    $team            = $val['team'];
//    $team_id         = $val['team_id'];
//    $title           = $val['title'];
//    $sql = "SELECT t.* FROM `team` t 
//             WHERE t.`parent_id` = $team_id
//             AND t.`name` LIKE '$title'
//             AND t.`del` <> 1
//             ";
//    $stmt = $db->prepare($sql);
//    $stmt->execute();
//    $res = $stmt->fetch();
   
//    $data = array(
//        'title' => $res['id']
//    );
//    $where_update = $QContractProcess->getAdapter()->quoteInto('id = ?', $id);
   
//   $QContractProcess->update($data, $where_update);
  
// }

$page = $this->getRequest()->getParam('page', 1);
$limit = 200;
$total = 0;

$params = array();

$QContractProcess = new Application_Model_ContractProcess();
$QContractTerm    = new Application_Model_ContractTerm();
$QDepartment      = new Application_Model_Department();
$QTeam            = new Application_Model_Team();
$QTitle           = new Application_Model_Title();

$contract_process = $QContractProcess->fetchPagination($page, $limit, $total, $params);

$this->view->team_all       = $QTeam->get_cache();
$this->view->department_all = $QDepartment->get_cache();
$this->view->title_all      = $QTitle->get_cache();
$this->view->contract_term_all      = $QContractTerm->get_cache_contract();

$this->view->contract_process = $contract_process;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST.'contract-new/contract-term/'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset = $limit*($page-1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();

$this->view->messages_success = $messages;

$this->_helper->viewRenderer->setRender('contract-term/contract-process');