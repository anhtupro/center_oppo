<?php
$id = $this->getRequest()->getParam('id');
if ($id) {
    $QModel = new Application_Model_ContractProcess();
    
    $rowset = $QModel->find($id);
    $contract_process = $rowset->current();

    $this->view->contract_process = $contract_process;
}
$QModel = new Application_Model_Department();
$this->view->departments = $QModel->fetchAll();

$QContractTerm = new Application_Model_ContractTerm();
$this->view->termsCached = $QContractTerm->get_cache_contract();

//get teams
$QTeam = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$this->view->teamsCached = $QTeam->get_cache();

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$this->_helper->viewRenderer->setRender('contract-term/contract-process-create');