<?php
if ($this->getRequest()->getMethod() == 'POST'){
    $QModel         = new Application_Model_ContractProcess();

    $id             = $this->getRequest()->getParam('id');
    $department     = $this->getRequest()->getParam('department');
    $team           = $this->getRequest()->getParam('team');
    $title          = $this->getRequest()->getParam('title');
    $contract1      = $this->getRequest()->getParam('contract1');
    $contract2      = $this->getRequest()->getParam('contract2');
    $contract3      = $this->getRequest()->getParam('contract3');
    $contract4      = $this->getRequest()->getParam('contract4');
    
    
    $data = array(
        'department'  => $department,
        'team'        => $team,
        'title'       => $title,
        'contract1'   => $contract1,
        'contract2'   => $contract2,
        'contract3'   => $contract3,
        'contract4'   => $contract4
    );

    if ($id){
        $where = $QModel->getAdapter()->quoteInto('id = ?', $id);

        $QModel->update($data, $where);
    } else {
        $QModel->insert($data);
    }

    //remove cache
    $cache = Zend_Registry::get('cache');
    $cache->remove('contract_process_cache');

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}

$back_url = $this->getRequest()->getParam('back_url');

$this->_redirect( ( $back_url ? $back_url : HOST.'contract-new/contract-process' ) );