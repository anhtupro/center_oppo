<?php 
    $month         = $this->getRequest()->getParam('month',date('m/Y'));
    $export_hr_by_area        = $this->getRequest()->getParam('export_hr_by_area');
    $export        = $this->getRequest()->getParam('export');
    $hr_export        = $this->getRequest()->getParam('hr_export');
    $hr_export_lock   = $this->getRequest()->getParam('hr_export_lock');
    $area          = $this->getRequest()->getParam('area');
    $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
     if( isset($month) AND $month ){
        $date = My_Date::normal_to_mysql($month);
    }else{
        $date    = date('Y') . '-' . date('m') . '-01';
    }
     $params = array(
            'month'         => $month
        );
    $this->view->params = $params;
    $created_by = $userStorage->id;
     $this->view->user_id = $created_by;
    if(in_array($userStorage->id, array(5899, 16269, 25259)) ){
        $sql = "SELECT m.area, count(m.area) soluong  FROM insurance_by_month m

                 WHERE `date` = '$date' GROUP BY m.area ";
    }else{
        $sql = "SELECT m.area, count(m.area) soluong  FROM insurance_by_month m
                 WHERE `date` = '$date' and m.created_by = $created_by GROUP BY m.area ";
    }
    $db = Zend_Registry::get('db');
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $list = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = null;
    if(!empty($hr_export) || !empty($hr_export_lock)){
        if(!empty($hr_export)){
            $sql_hr = "SELECT m.*  FROM insurance_by_month m
                            WHERE m.`date` = '$date' ";
        }elseif(!empty($hr_export_lock)){
        
        $sql_hr = "SELECT m.*  FROM insurance_by_month_hr m
                            WHERE m.`date` = '$date' ";
        }
        
        $stmt_hr = $db->prepare($sql_hr);
        $stmt_hr->execute();
        $data_hr = $stmt_hr->fetchAll();
        $stmt_hr->closeCursor();
        $stmt_hr = null;
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'AREA',
            'CODE',
            'NAME',
            'TITLE',
            'DATE_SIGN',
            'INSURANCE_NUMBER',
            'INSURANCE_CODE',
            'CODE BHYT OLD',
            'NOTE'

        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        $stt = 0;
        foreach($data_hr as $key => $value):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['date_sign']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['insurance_number']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['insurance_code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['household_number']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['note']), PHPExcel_Cell_DataType::TYPE_STRING);


            $index++;
        endforeach;

        $filename = 'area_' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
       
    }
    
    
        
    if(!empty($export) & !empty($area)){
        $sql_by_area = "SELECT m.*  FROM insurance_by_month m
                            WHERE m.`date` = '$date' and m.`area` = '$area' ";
      
        $stmt = $db->prepare($sql_by_area);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = null;
        
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'AREA',
            'CODE',
            'NAME',
            'TITLE',
            'DATE_SIGN',
            'INSURANCE_NUMBER',
            'INSURANCE_CODE',
            'HOUSEHOLD_NUMBER',
            'NOTE'

        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        $stt = 0;
        foreach($data as $key => $value):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['date_sign']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['insurance_number']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['insurance_code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['household_number']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['note']), PHPExcel_Cell_DataType::TYPE_STRING);


            $index++;
        endforeach;

        $filename = 'area_' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

if(!empty($export_hr_by_area) & !empty($area)){
    $sql_by_area = "SELECT m.*  FROM insurance_by_month_hr m
                            WHERE m.`date` = '$date' and m.`area` = '$area' ";

    $stmt = $db->prepare($sql_by_area);
    $stmt->execute();
    $data = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = null;

    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
        'STT',
        'AREA',
        'CODE',
        'NAME',
        'TITLE',
        'DATE_SIGN',
        'INSURANCE_NUMBER',
        'INSURANCE_CODE',
        'HOUSEHOLD_NUMBER',
        'NOTE'

    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }
    $index = 2;
    $stt = 0;
    foreach($data as $key => $value):
        $alpha = 'A';
        $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['date_sign']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['insurance_number']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['insurance_code']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['household_number']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['note']), PHPExcel_Cell_DataType::TYPE_STRING);


        $index++;
    endforeach;

    $filename = 'area_hr_' . date('d-m-Y');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}
    
    $this->view->list = $list;
