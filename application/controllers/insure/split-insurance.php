<?php 
$id = $this->getRequest()->getParam('id');
$QLeaveType = new Application_Model_LeaveType();
$this->view->leaveType = $QLeaveType->get_type_name();
 
if($id){
    $db = Zend_Registry::get('db');
    $cols = array(
        'p.*',
        'staff_name' => 'CONCAT(s.firstname," ",s.lastname)',
    );
    $select = $db->select()
    ->from(array('p'=>'leave_detail'),$cols)//leave_detail
    ->join(array('s'=>'staff'),'s.id= p.staff_id',array())
    ->where('p.id = ?',$id);
    $row = $db->fetchRow($select);
    $this->view->row = $row;
 
//    $selectList = $db->select()
//    ->from(array('p'=>'off_childbearing'),array('p.*'))
//    ->where('pre_time_off_id = ?',$id);
//    $list = $db->fetchAll($selectList);
//    $this->view->list = $list;
}