<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$QStaff = new Application_Model_Staff();
$QStaffUnionFunding = new Application_Model_StaffUnionFunding();
$QTimeUnionFunding = new Application_Model_TimeUnionFunding();

define('START_ROW', 2);
define('MONTH', 1);
define('YEAR', 2);
define('STAFF_CODE', 3);
define('STATUS', 4);
echo '<link href="/css/bootstrap.min.css" rel="stylesheet">';
$upload = $this->getRequest()->getParam('upload');
if ($upload == 1) {
    if ($this->getRequest()->getMethod() == 'POST') { // Big IF
        $flashMessenger = $this->_helper->flashMessenger;
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $progress = new My_File_Progress('parent.set_progress');
        $progress->flush(0);

        $save_folder = 'upload_union_funding';
        $new_file_path = '';
        $requirement = array(
            'Size' => array('min' => 5, 'max' => 5000000),
            'Count' => array('min' => 1, 'max' => 1),
            'Extension' => array('xls', 'xlsx'),
        );

        try {
            $file = My_File::get($save_folder, $requirement, true);

            if (!$file || !count($file))
                throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                    . DIRECTORY_SEPARATOR . $file['folder'];
            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        //read file
        include 'PHPExcel/IOFactory.php';
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {

            $this->view->errors = $e->getMessage();
            return;
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $dataMassUploadLog = array();
        $list_staff = array();
        $dataMassUploadPay = array();
        $dataMassUploadNotpay = array();
        $time_up = '';
        $month = '';
        $year = '';
        for ($row = START_ROW; $row <= $highestRow; $row++) {
            try {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = isset($rowData[0]) ? $rowData[0] : array();
                if (!empty($rowData[MONTH]) && !empty($rowData[YEAR]) && !empty($rowData[STAFF_CODE]) && ($rowData[STATUS] == 'PAY' || $rowData[STATUS] == 'NOT-PAY')) {
                    if ($row == START_ROW) {
                        $month = $rowData[MONTH];
                        $year = $rowData[YEAR];
                        $time_up = $rowData[YEAR] . '-' . $rowData[MONTH] . '-01';
                    } else {
                        $time_tmp = $rowData[YEAR] . '-' . $rowData[MONTH] . '-01';
                        if ($time_up != $time_tmp) {
                            throw new Exception("Please, Check time upload!");
                        }
                    }
                    $data = [
                        'staff_code' => $rowData[STAFF_CODE],
                        'month' => $rowData[MONTH],
                        'year' => $rowData[YEAR],
                        'status' => (($rowData[STATUS] == 'PAY') ? 1 : 0),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id
                    ];
                    $dataMassUploadLog[] = $data;
                    $list_staff[] = $rowData[STAFF_CODE];
                    if ($rowData[STATUS] == 'PAY') {
                        $dataMassUploadPay[] = trim($rowData[STAFF_CODE]);
                    } else {
                        $dataMassUploadNotpay[] = trim($rowData[STAFF_CODE]);
                    }
                } else {
                    throw new Exception("Please, Check data !");
                }
            } catch (Exception $e) {
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
                exit;
            }
            // nothing here
        } // END loop through order rows
        $progress->flush(30);
        $db = Zend_Registry::get('db');
        try {
            $db->beginTransaction();
            //check staff exits
            $where = array();
            $where[] = $QStaff->getAdapter()->quoteInto('code IN(?)', $list_staff);
            $staff_exits = $QStaff->fetchAll($where);
            if (count($list_staff) != count($staff_exits)) {
                throw new Exception("Check staffs upload");
            }

            //check exit not insert
            $where = $QTimeUnionFunding->getAdapter()->quoteInto('time_upload = ?', $time_up);
            $data_check = $QTimeUnionFunding->fetchRow($where);
            $id_time = 0;
            $data_staff_curent = [];
            $check_code = 0;
            if (empty($data_check)) {
                // ADD THEM TIME R TINH TIEP
                $data_time = [
                    'month' => $month,
                    'year' => $year,
                    'time_upload' => $time_up
                ];
                $id_time = $QTimeUnionFunding->insert($data_time);

                $data_staff_curent = $QStaffUnionFunding->getDataInsert($time_up);
                //$QStaffUnionFunding->insertData($time_up,$id_time);
            } else {
                $check_code = 1;
                $id_time = $data_check['id'];
                $where = $QStaffUnionFunding->getAdapter()->quoteInto('id_time = ?', $id_time);
                $data_ext_tmp = $QStaffUnionFunding->fetchAll($where)->toArray();
                foreach ($data_ext_tmp as $value) {
                    $data_staff_curent[] = $value['staff_code'];
                }
            }

            if (!empty($dataMassUploadLog)) {
                My_Controller_Action::insertAllrow($dataMassUploadLog, 'log_staff_union_funding');
            }

            $progress->flush(50);
            $data_insert_tmp = [];
            if ($check_code == 0) {
                $data_insert_tmp = array_unique(array_merge($dataMassUploadPay, $data_staff_curent), SORT_REGULAR);
                $data_insert_tmp = array_diff($data_insert_tmp, $dataMassUploadNotpay);
            } else {
                if (!empty($dataMassUploadNotpay)) {
                    $where = [];
                    $where[] = $QStaffUnionFunding->getAdapter()->quoteInto('staff_code IN(?)', $dataMassUploadNotpay);
                    $where[] = $QStaffUnionFunding->getAdapter()->quoteInto('id_time = ?', $id_time);
                    $QStaffUnionFunding->delete($where);
                }
                $data_insert_tmp = array_diff($dataMassUploadPay, $data_staff_curent);
            }

            if (!empty($data_insert_tmp)) {
                $data_insert = [];
                foreach ($data_insert_tmp as $key => $value) {
                    $data_insert[] = [
                        'staff_code' => $value,
                        'id_time' => $id_time
                    ];
                }
                My_Controller_Action::insertAllrow($data_insert, 'staff_union_funding');
                $progress->flush(80);
            }

            $db->commit();
            $progress->flush(99);
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-success">Success!</div>';
            exit;
        } catch (Exception $e) {
            $db->rollback();
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }
    }
}