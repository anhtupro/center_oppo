<?php 
$staff_id = $this->getRequest()->getParam('staff_id');
        $unit_code_id               = $this->getRequest()->getParam('unit_code_id',0);
        $insurance_number           = $this->getRequest()->getParam('insurance_number',NULL);
        $hospital_id                = $this->getRequest()->getParam('hospital_id');
        $note_ins                   = $this->getRequest()->getParam('note_ins','');
        $have_book                  = $this->getRequest()->getParam('have_book',NULL);
        $close_book                 = $this->getRequest()->getParam('close_book',NULL);
        $return_ins_book_time       = $this->getRequest()->getParam('return_ins_book_time',NULL);
        $return_book_declare_ins    = $this->getRequest()->getParam('return_book_declare_ins',NULL);
        $delivery_card_ins          = $this->getRequest()->getParam('delivery_card_ins',NULL);
        $take_card_from_staff       = $this->getRequest()->getParam('take_card_from_staff',NULL);
        $date_insurance_increase    = $this->getRequest()->getParam('date_insurance_increase',NULL);
        $date_insurance_off         = $this->getRequest()->getParam('date_insurance_off',NULL);
        $date_insurance_off_declare = $this->getRequest()->getParam('date_insurance_off_declare',NULL);

        if($have_book){
            $have_book = My_Date::normal_to_mysql($have_book);
        }else{
            $have_book = NULL;
        }


        if($close_book){
            $close_book = My_Date::normal_to_mysql($close_book);
        }else{
            $close_book = NULL;
        }


        if($return_ins_book_time){
            $return_ins_book_time = My_Date::normal_to_mysql($return_ins_book_time);
        }else{
            $return_ins_book_time = NULL;
        }


        if($return_book_declare_ins){
            $return_book_declare_ins = My_Date::normal_to_mysql($return_book_declare_ins);
        }else{
            $return_book_declare_ins = NULL;
        }


        if($delivery_card_ins){
            $delivery_card_ins = My_Date::normal_to_mysql($delivery_card_ins);
        }else{
            $delivery_card_ins = NULL;
        }


        if($take_card_from_staff){
            $take_card_from_staff = My_Date::normal_to_mysql($take_card_from_staff);
        }else{
            $take_card_from_staff = NULL;
        }


        if($date_insurance_increase){
            $date_insurance_increase = My_Date::normal_to_mysql($date_insurance_increase);
        }else{
            $date_insurance_increase = NULL;
        }

        if($date_insurance_off){
            $date_insurance_off = My_Date::normal_to_mysql($date_insurance_off);
        }else{
            $date_insurance_off = NULL;
        }

        if($date_insurance_off_declare){
            $date_insurance_off_declare = My_Date::normal_to_mysql($date_insurance_off_declare);
        }else{
            $date_insurance_off_declare = NULL;
        }

        $data = array(
            'unit_code_id'               => intval($unit_code_id),
            'insurance_number'           => trim($insurance_number),
            'hospital_id'                => intval($hospital_id),
            'note_ins'                   => trim($note_ins),
            'have_book'                  => $have_book,
            'close_book'                 => $close_book,
            'return_ins_book_time'       => $return_ins_book_time,
            'return_book_declare_ins'    => $return_book_declare_ins,
            'delivery_card_ins'          => $delivery_card_ins,
            'take_card_from_staff'       => $take_card_from_staff,
            'date_insurance_increase'    => $date_insurance_increase,
            'date_insurance_off'         => $date_insurance_off,
            'date_insurance_off_declare' => $date_insurance_off_declare,
        );
        $flashMessenger = $this->_helper->flashMessenger;
        $db = Zend_Registry::get('db');
        $QStaff = new Application_Model_Staff();
        $QInsuranceLogInfo = new Application_Model_InsuranceLogInfo();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db->beginTransaction();
        try{
            $beforeStaff = $QStaff->find($staff_id)->current();

            $where = $QStaff->getAdapter()->quoteInto('id = ?',$staff_id);
            $QStaff->update($data,$where);

            $afterStaff = $QStaff->find($staff_id)->current();
            $before = $beforeStaff->toArray();
            $after = $afterStaff->toArray();
            $QInsuranceLogInfo->save($before,$after,$staff_id,$userStorage->id);

            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch (Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect('/insurance/info?staff_id='.$staff_id);

?>