<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$lock_time = My_Util::escape_string($this->getRequest()->getParam('lock_time'));
$db = Zend_Registry::get('db');
$QStaffUnionFunding = new Application_Model_StaffUnionFunding();
$QCompany = new Application_Model_Company();
if ($lock_time) {
    $month = date("m", strtotime($lock_time));
    $year = date("Y", strtotime($lock_time));
    
    $tbl_staff = "staff_" . (int) $month . '_' . $year;
    
    $companies = $QCompany->get_cache();
    $list_export = $QStaffUnionFunding->exportSalaryUnionFunding($month, $year, $lock_time);

    $company_list = [];
    foreach ($list_export as $item) {
        if (!in_array($item['unit_code_id'], $company_list))
            $company_list[] = $item['unit_code_id'];
    }
    if (!empty($list_export)) {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $sheet = $PHPExcel->getActiveSheet();
        $i = 0;
        foreach ($company_list as $company) {
            if ($i != 0)
                $sheet = $PHPExcel->createSheet($i);
            $heads = array(
                'STT',
                'MÃ CODE',
                'HỌ VÀ TÊN',
                'LƯƠNG TRÍCH ĐÓNG BHXH',
                'TRÍCH ĐÓNG CÔNG ĐOÀN',
                'DEPARTMENT',
                'TEAM',
                'TITLE',
                'AREA',
                'GROUP',
                'OFFICE LOCATION',
            );

            $alpha = 'A';
            $index = 1;
            foreach ($heads as $key) {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }
            $sheet->getStyle('A1:Q1')->applyFromArray(array('font' => array('bold' => true)));
            $sheet->getColumnDimension('A')->setWidth(10);
            $sheet->getColumnDimension('B')->setWidth(30);
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(15);
            $sheet->getColumnDimension('F')->setWidth(30);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('H')->setWidth(20);


            $intCount = 1;
            $index = 2;
            $stt = 1;
            foreach ($list_export as $_key => $_value) {
                if ($_value['unit_code_id'] == $company) {
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['fullname'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($_value['salary']), PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($_value['salary_congdoan']), PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['department'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['team'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['title'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['area'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['group_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_value['office_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
                }
            }
            $sheet->setTitle($companies[$company]);
            $i++;
        }
        $filename = 'Công_đoàn - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }else{
        echo 'Tháng này chưa được upload danh sách công đoàn';
        exit;
    }
}

    