<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$QHospital = new Application_Model_Hospital();
$QHospitalHideContract = new Application_Model_HospitalHideContract();
define('START_ROW', 2);
define('id_hospital', 0);
define('name_hospital', 1);
define('status', 2);
echo '<link href="/css/bootstrap.min.css" rel="stylesheet">';
$upload = $this->getRequest()->getParam('upload');
if ($upload == 1) {
    if ($this->getRequest()->getMethod() == 'POST') { // Big IF
        $flashMessenger = $this->_helper->flashMessenger;
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $progress = new My_File_Progress('parent.set_progress');
        $progress->flush(0);

        $save_folder = 'upload_hospital';
        $new_file_path = '';
        $requirement = array(
            'Size' => array('min' => 5, 'max' => 5000000),
            'Count' => array('min' => 1, 'max' => 1),
            'Extension' => array('xls', 'xlsx'),
        );

        try {
            $file = My_File::get($save_folder, $requirement, true);

            if (!$file || !count($file))
                throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                    . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        //read file
        include 'PHPExcel/IOFactory.php';
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {

            $this->view->errors = $e->getMessage();
            return;
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $_failed = false;
        $total_row = array();
        $dataMassUpload = array();

        $dataMassUploadHide = array();
        $data_id_hospital_hide = array();

        $dataMassUploadUnHide = array();
        $data_id_hospital_unhide = array();
        for ($row = START_ROW; $row <= $highestRow; $row++) {
            try {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = isset($rowData[0]) ? $rowData[0] : array();
                if (!empty($rowData[id_hospital]) && !empty($rowData[name_hospital]) && !empty($rowData[status]) && ($rowData[status] == 'Hide' || $rowData[status] == 'Unhide')) {
                    $data['id_hospital'] = $rowData[id_hospital];
                    $dataMassUpload[] = $data;
                    if ($rowData[status] == 'Hide') {
                        $dataMassUploadHide[] = $data;
                        $data_id_hospital_hide[] = $rowData[id_hospital];
                    } else {
                        $dataMassUploadUnHide[] = $data;
                        $data_id_hospital_unhide[] = $rowData[id_hospital];
                    }
                } else {
                    throw new Exception("Please, Check data !");
                }
            } catch (Exception $e) {
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
                exit;
            }
            // nothing here
        } // END loop through order rows

        $progress->flush(30);
        $db = Zend_Registry::get('db');
        try {
            $db->beginTransaction();
            if (!empty($dataMassUploadHide)) {
                $data_check = $QHospital->checkHospital($data_id_hospital_hide);
                if (!empty(array_diff($data_id_hospital_hide, $data_check))) {
                    throw new Exception("The hospital is not on the system");
                }
 
                $data_check_hide = $QHospital->checkHospitalHide($data_id_hospital_hide);
                if (!empty($data_check_hide)) {
                    $string_hospital_err = 'ID ' . implode(",", $data_check_hide) . ' ' . 'have been hidden';
                    throw new Exception($string_hospital_err);
                }
                My_Controller_Action::insertAllrow($dataMassUploadHide, 'hospital-hide-contract');
            }

            if (!empty($dataMassUploadUnHide)) {
                $data_check_hide = $QHospital->checkHospitalHide($data_id_hospital_unhide);
                if (!empty(array_diff($data_id_hospital_unhide, $data_check_hide))) {
                    $string_hospital_err = 'ID ' . implode(",", array_diff($data_id_hospital_unhide, $data_check_hide)) . ' ' . 'is not hidden yet';
                    throw new Exception($string_hospital_err);
                }

                $where = $QHospitalHideContract->getAdapter()->quoteInto('id_hospital IN(?)', $data_id_hospital_unhide);
                $QHospitalHideContract->delete($where);
            }

            $db->commit();
            $progress->flush(99);
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-success">Success!</div>';
            exit;
        } catch (Exception $e) {
            $db->rollback();
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }
    }
}