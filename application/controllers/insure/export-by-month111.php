<?php 

   include 'PHPExcel.php';
  
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout->disableLayout();
    
    $lock_time             = $this->getRequest()->getParam('lock_time');
    
 
   //Trich nop
    $db             = Zend_Registry::get('db');

    $sql1 = "SELECT st.id as 'staff_id',
                st.insurance_number as 'insurance_number',
                 uc.unit_code AS 'unit_code_name',
                 st.`code`,
                 CONCAT(st.firstname, ' ', st.lastname) AS 'fullname',
                 li.*,
                 CASE 
                        WHEN li.`lock_type` = 1 THEN 'Trích nộp hàng tháng'
                        WHEN li.`lock_type` = 2 THEN 'Trích nộp do tham gia ngược từ quá khứ'
                        WHEN li.`lock_type` = 3 THEN 'Trích nộp điều chỉnh lương ngược từ quá khứ'
                        WHEN li.`lock_type` = 4 THEN 'Truy thu BHYT do nhân viên nghỉ việc'
                        WHEN li.`lock_type` = 5 THEN 'Hoàn trả tiền BH do báo giảm ở quá khứ'
                                            
                        END AS 'lock_name',
                 CASE 
                        WHEN li.`lock_type` = 4 THEN '4.5%'
                        WHEN li.`lock_type` <> 4 THEN '32%'
                        END AS 'INS_RATE'
            FROM lock_insurance li 
            LEFT JOIN staff st ON li.staff_id = st.id
            LEFT JOIN unit_code uc ON uc.id = li.unit_code_id
                    WHERE lock_time = '$lock_time' AND li.`unit_code_id` = 1 
                     ORDER BY li.`lock_type` ASC";
                    
    $stmt1 = $db->prepare($sql1);
    $stmt1->execute();
    $data1 = $stmt1->fetchAll();


    $stmt1->closeCursor();
    $stmt1 = null;
    
    $sql2 = "SELECT st.id as 'staff_id',
                 st.insurance_number as 'insurance_number',
                 uc.unit_code AS 'unit_code_name',
                 st.`code`,
                 CONCAT(st.firstname, ' ', st.lastname) AS 'fullname',
                 li.*,
                 CASE 
                        WHEN li.`lock_type` = 1 THEN 'Trích nộp hàng tháng'
                        WHEN li.`lock_type` = 2 THEN 'Trích nộp do tham gia ngược từ quá khứ'
                        WHEN li.`lock_type` = 3 THEN 'Trích nộp điều chỉnh lương ngược từ quá khứ'
                        WHEN li.`lock_type` = 4 THEN 'Truy thu BHYT do nhân viên nghỉ việc'
                                            WHEN li.`lock_type` = 5 THEN 'Hoàn trả tiền BH do báo giảm ở quá khứ'
                        END AS 'lock_name',
                 CASE 
                        WHEN li.`lock_type` = 4 THEN '4.5%'
                        WHEN li.`lock_type` <> 4 THEN '32%'
                        END AS 'INS_RATE'
            FROM lock_insurance li 
            LEFT JOIN staff st ON li.staff_id = st.id
            LEFT JOIN unit_code uc ON uc.id = li.unit_code_id
                    WHERE lock_time = '$lock_time' AND li.`unit_code_id` = 2 
                    ORDER BY li.`lock_type` ASC";
                    
    $stmt2 = $db->prepare($sql2);
    $stmt2->execute();
    $data2 = $stmt2->fetchAll();

    $stmt2->closeCursor();
    $stmt2 = null;

    //sheet oppo

    foreach ($data1 as $key => $value) {
        if ($value['lock_type'] == 1) {
            $totalRowLockType1 += 1;
        }
        if ($value['lock_type'] == 2) {
            $totalRowLockType2 += 1;
        }
        if ($value['lock_type'] == 3) {
            $totalRowLockType3 += 1;
        }
        if ($value['lock_type'] == 4) {
            $totalRowLockType4 += 1;
        }
        if ($value['lock_type'] == 5) {
            $totalRowLockType5 += 1;
        }
    }      

    if(!empty($data1)) {
        $rowCount = 8;    
        $index = 0; // số thứ tự
        $totalRows = 0; 

        $objExcel = new PHPExcel();
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'oppo.xlsx';

        $objExcel->createSheet();
        
        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objExcel   = $objReader->load($path);
        $objExcel->setActiveSheetIndex(0);
        $sheet = $objExcel->getActiveSheet()->setTitle('Oppo');
        
        $sheet->setCellValue('A5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y' ));

        foreach ($data1 as $key => $row) {
            $rowCount++;
            $index++;
            // set name for each lock_type
            if ($totalRows == 0) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách nhân viên trong bảng lương');
                //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
               if ($totalRowLockType1 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
                
            }
            if ($totalRows == $totalRowLockType1) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách trích nộp do tham gia ngược từ quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType2 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
                    
            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách trích nộp chỉnh lương ngược từ quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType3 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 

            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách truy thu BHYT do nhân viên nghỉ việc');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType4 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }

            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3 +$totalRowLockType4) {

                $sheet->setCellValue('A' . $rowCount, 'Danh sách hoàn trả tiền BH do báo giảm ở quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                
                $index = 1;
               if ($totalRowLockType5 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }
            // end set name for each lock_type
            $sheet->getStyle("H:O")->getNumberFormat()->setFormatCode('#,##0'); // set comma for number
            
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $row['code']);
            $sheet->setCellValue('C' . $rowCount, $row['fullname']);
            $sheet->setCellValue('D' . $rowCount, $row['insurance_number']);
            $sheet->setCellValue('E' . $rowCount, $row['from_month']);
            $sheet->setCellValue('F' . $rowCount, $row['to_month']);
            $sheet->setCellValue('G' . $rowCount, $row['INS_RATE']);
            $sheet->setCellValueExplicit('H' . $rowCount, $row['salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('I' . $rowCount, $row['salary'] * 32 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('J' . $rowCount, ($row['salary'] * 17.5 / 100), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('K' . $rowCount, $row['salary'] * 8 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('L' . $rowCount, $row['salary'] * 3 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('M' . $rowCount, $row['salary'] * 1.5 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('N' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('O' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
     
          

            // set text center for each row 
             $sheet->getStyle('A'. $rowCount . ':Q' . $rowCount)
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
             // end set text center  for each row 
             $totalRows++;
        } // end foreach

        if ($totalRowLockType5 == 0) {
            $rowCount++;
            $sheet->setCellValue('A' . $rowCount, 'Danh sách hoàn trả tiền BH do báo giảm ở quá khứ');
            $sheet->getStyle('A' . $rowCount)->applyFromArray(array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Verdana',
                 'color' => array('rgb' => 'FF0000')
        )));
            $rowCount++;
        }

        // add 2 option with dummy data
        $rowCount++;
        $sheet->setCellValue('A' . $rowCount, 'NHÂN VIÊN NGHỈ VIỆC TRẢ THẺ BHYT');
        $sheet->getStyle('A' . $rowCount)->applyFromArray( array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FF0000'),
                'size'  => 12,
                'name'  => 'Verdana'
        )));
        $rowCount += 2;
        $sheet->setCellValue('A' . $rowCount, 'Giảm trùng');
        $sheet->getStyle('A' . $rowCount)->applyFromArray( array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FF0000'),
                'size'  => 12,
                'name'  => 'Verdana'
        )));



        // end add 2 option with dummy data
        

        // set style for sheet 
        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A8:O'. $rowCount)->applyFromArray($styleArray);
                  

        for ($col = 'B'; $col !== 'Q'; $col++) { 
             $sheet->getColumnDimension($col)
              ->setAutoSize(true);  
        }
        // end set style for sheet  
        $setLockTypeName1 = '';
        $setLockTypeName2 = '';
        $setLockTypeName3 = '';
        $setLockTypeName4 = '';
        $setLockTypeName5 = '';

        $totalRowLockType1 = 0;
        $totalRowLockType2 = 0;
        $totalRowLockType3 = 0;
        $totalRowLockType4 = 0;
        $totalRowLockType5 = 0;

    } // end if
    //end sheet oppo
    

    //sheet di dong thong minh
    foreach ($data2 as $key => $value) {
        if ($value['lock_type'] == 1) {
            $totalRowLockType1 += 1;
        }
        if ($value['lock_type'] == 2) {
            $totalRowLockType2 += 1;
        }
        if ($value['lock_type'] == 3) {
            $totalRowLockType3 += 1;
        }
        if ($value['lock_type'] == 4) {
            $totalRowLockType4 += 1;
        }
        if ($value['lock_type'] == 5) {
            $totalRowLockType5 += 1;
        }
    }
    // echo "<pre>";print_r($totalRowLockType1);die;

    if(!empty($data2)) {       
        $rowCount = 8;    
        $index = 0; // số thứ tự
        $totalRows = 0; 

        $objExcel->createSheet();
        $objExcel->setActiveSheetIndex(1);
        $sheet = $objExcel->getActiveSheet()->setTitle('Di động thông minh');
        // $sheet->setCellValue('A5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y' ));

        foreach ($data2 as $key => $row) {
            $rowCount++;
            $index++;
            // set name for each lock_type
            if ($totalRows == 0) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách nhân viên trong bảng lương');
                //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
               if ($totalRowLockType1 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
                
            }
            if ($totalRows == $totalRowLockType1) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách trích nộp do tham gia ngược từ quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType2 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
                    
            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách trích nộp chỉnh lương ngược từ quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType3 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 

            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3) {
                $sheet->setCellValue('A' . $rowCount, 'Danh sách truy thu BHYT do nhân viên nghỉ việc');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType4 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }

            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3 +$totalRowLockType4) {

                $sheet->setCellValue('A' . $rowCount, 'Danh sách hoàn trả tiền BH do báo giảm ở quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                
                $index = 1;
               if ($totalRowLockType5 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }
            // end set name for each lock_type
            $sheet->getStyle("H:O")->getNumberFormat()->setFormatCode('#,##0'); // set comma for number
            
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $row['code']);
            $sheet->setCellValue('C' . $rowCount, $row['fullname']);
            $sheet->setCellValue('D' . $rowCount, $row['insurance_number']);
            $sheet->setCellValue('E' . $rowCount, $row['from_month']);
            $sheet->setCellValue('F' . $rowCount, $row['to_month']);
            $sheet->setCellValue('G' . $rowCount, $row['INS_RATE']);
            $sheet->setCellValueExplicit('H' . $rowCount, $row['salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('I' . $rowCount, $row['salary'] * 32 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('J' . $rowCount, ($row['salary'] * 17.5 / 100), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('K' . $rowCount, $row['salary'] * 8 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('L' . $rowCount, $row['salary'] * 3 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('M' . $rowCount, $row['salary'] * 1.5 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('N' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('O' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
     
          

            // set text center for each row 
             $sheet->getStyle('A'. $rowCount . ':Q' . $rowCount)
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
             // end set text center  for each row 
             $totalRows++;
        } // end foreach

        if ($totalRowLockType5 == 0) {
            $rowCount++;
            $sheet->setCellValue('A' . $rowCount, 'Danh sách hoàn trả tiền BH do báo giảm ở quá khứ');
            $sheet->getStyle('A' . $rowCount)->applyFromArray( array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FF0000'),
                'size'  => 12,
                'name'  => 'Verdana'
            )));
            $rowCount++;
        }

        // add 2 option with dummy data
        $rowCount++;
        $sheet->setCellValue('A' . $rowCount, 'NHÂN VIÊN NGHỈ VIỆC TRẢ THẺ BHYT');
        $sheet->getStyle('A' . $rowCount)->applyFromArray( array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FF0000'),
                'size'  => 12,
                'name'  => 'Verdana'
        )));
        $rowCount += 2;
        $sheet->setCellValue('A' . $rowCount, 'Giảm trùng');
        $sheet->getStyle('A' . $rowCount)->applyFromArray( array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FF0000'),
                'size'  => 12,
                'name'  => 'Verdana'
        )));



        // end add 2 option with dummy data
        // set head template for sheet 2
        // 
        $sheet->setCellValue('A1', 'Tên đơn vị: CÔNG TY CỔ PHẦN DI ĐỘNG THÔNG MINH');
        $sheet->setCellValue('A2', 'Địa chỉ: 56 HỒ TÙNG MẬU, P. BẾN NGHÉ, Q. 1, TP. HCM');
        $sheet->setCellValue('E3', 'DANH SÁCH LAO ĐỘNG ĐÓNG BH  - TA8330A');
        $sheet->setCellValue('E4', 'CÔNG TY CỔ PHẦN DI ĐỘNG THÔNG MINH');
        $sheet->mergeCells('E5:H5');
        $sheet->setCellValue('E5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y' ));

        $sheet->mergeCells('A6:A7');
        $sheet->setCellValue('A6', 'Stt');
        $sheet->mergeCells('B6:B7');
        $sheet->setCellValue('B6', 'Mã Nhân Viên');
        $sheet->mergeCells('C6:C7');
        $sheet->setCellValue('C6', 'Họ và tên');
        $sheet->mergeCells('D6:D7');
        $sheet->setCellValue('D6', 'Số sổ BHXH');
        $sheet->mergeCells('E6:E7');
        $sheet->setCellValue('E6', 'Thời gian tham BH');
        $sheet->mergeCells('F6:F7');
        $sheet->setCellValue('F6', 'Thời gian đóng BH');
        $sheet->mergeCells('G6:G7');
        $sheet->setCellValue('G6', 'Tỷ lệ đóng');
        $sheet->mergeCells('H6:H7');
        $sheet->setCellValue('H6', 'LƯƠNG TRÍCH ĐÓNG BHXH');
        $sheet->mergeCells('I6:I7');
        $sheet->setCellValue('I6', '32%');
        $sheet->mergeCells('J6:K6');
        $sheet->setCellValue('J6', 'BHXH');
        $sheet->mergeCells('L6:M6');
        $sheet->setCellValue('L6', 'BHYT');
        $sheet->mergeCells('N6:O6');
        $sheet->setCellValue('N6', 'BHTN');

        $sheet->setCellValue('J7', 'NSDLD 17.5%');
        $sheet->setCellValue('K7', 'NLD 8%');
        $sheet->setCellValue('L7', 'NSDLD 3%');
        $sheet->setCellValue('M7', 'NLD 1.5%');
        $sheet->setCellValue('N7', 'NSDLD 1%');
        $sheet->setCellValue('O7', 'NLD 1%');

        // $sheet->setCellValue('A8', '1');
        // $sheet->setCellValue('B8', '2');
        // $sheet->setCellValue('C8', '3');
        // $sheet->setCellValue('D8', '4');
        // $sheet->setCellValue('E8', '5');
        // $sheet->setCellValue('F8', '6');
        // $sheet->setCellValue('J8', '7');
        // $sheet->setCellValue('K8', '8');
        // $sheet->setCellValue('L8', '9');
        // $sheet->setCellValue('M8', '10');
        // $sheet->setCellValue('N8', '11');
        // $sheet->setCellValue('O8', '12');



        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A8:O'. $rowCount)->applyFromArray($styleArray);

        
      
        for ($col = 'B'; $col !== 'K'; $col++) { 
             $sheet->getColumnDimension($col)
              ->setAutoSize(true);  
        }
        // end set style for sheet  
        $setLockTypeName1 = '';
        $setLockTypeName2 = '';
        $setLockTypeName3 = '';
        $setLockTypeName4 = '';
        $setLockTypeName5 = '';

         $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman'
        ));


         $sheet->getStyle('A1:A2')->applyFromArray($styleArray);
         $sheet->getStyle('E3:E5')->applyFromArray(array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman',
                'color' => array('rgb' => '0000FF')
        )));

        $sheet->getStyle('A6:N6')->applyFromArray($styleArray);
        $sheet->getStyle('A7:O7')->applyFromArray($styleArray);

        $sheet->getColumnDimension('E')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('F')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('G')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('J')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('L')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('N')->setWidth(12)->setAutoSize(false);

        $sheet->getColumnDimension('H')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('I')->setWidth(12)->setAutoSize(false);
        $sheet->getStyle('A6:O6')->getAlignment()->setWrapText(true); 
        $sheet->getStyle('E7:O7')->getAlignment()->setWrapText(true); 
        $sheet->getStyle('J6')->getAlignment()->setWrapText(false); 
        $sheet->getStyle('L6')->getAlignment()->setWrapText(false); 
        $sheet->getStyle('N6')->getAlignment()->setWrapText(false); 

        $sheet->getStyle('A5:O7')
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    

        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A6:I7')->applyFromArray($styleArray); 
         $sheet->getStyle('J7:O7')->applyFromArray($styleArray);  

        $sheet->getStyle('J6')->applyFromArray(array(
            'borders' => array(
                'bottom' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('K6')->applyFromArray(array(
            'borders' => array(
                'bottom' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 

         $sheet->getStyle('L6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('M6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )

            )
        )); 

         $sheet->getStyle('N6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('O6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )

            )
        ));  

        $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

        $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);

    } // end if
    //end sheet di dong thong minh


    
    // sheet BW0282A-Oppo --- test - fake data
    if(!empty($data2)) {
        
        $rowCount = 8;
        $index = 0;    
        $objExcel->createSheet();
        $objExcel->setActiveSheetIndex(2);
        $sheet = $objExcel->getActiveSheet()->setTitle('BW0282A-Oppo');


        foreach ($data2 as $key => $row) {
            $rowCount++;
            $index++;
            // set name for each lock_type
            if ($row['lock_type'] == 1 && $setLockTypeName1 !== 'already set') {
                $sheet->setCellValue('A' . $rowCount, 'Danh Sách Lao Động Nước Ngoài Trong Bảng Lương');
                //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $rowCount++;
                $setLockTypeName1 = 'already set';
            }

            // end set name for each lock_type
            $sheet->getStyle("H:O")->getNumberFormat()->setFormatCode('#,##0'); // set comma for number
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $row['code']);
            $sheet->setCellValue('C' . $rowCount, $row['fullname']);
            $sheet->setCellValue('D' . $rowCount, $row['insurance_number']);
            $sheet->setCellValue('E' . $rowCount, $row['from_month']);
            $sheet->setCellValue('F' . $rowCount, $row['to_month']);
            $sheet->setCellValue('G' . $rowCount, $row['INS_RATE']);
            $sheet->setCellValueExplicit('H' . $rowCount, $row['salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('I' . $rowCount, $row['salary'] * 32 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('J' . $rowCount, ($row['salary'] * 17.5 / 100), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('K' . $rowCount, $row['salary'] * 8 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('L' . $rowCount, $row['salary'] * 3 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('M' . $rowCount, $row['salary'] * 1.5 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('N' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('O' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
     
          

            // set text center for each row 
             $sheet->getStyle('A'. $rowCount . ':Q' . $rowCount)
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
             // end set text center  for each row 
             
        } // end foreach

        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A8:O'. $rowCount)->applyFromArray($styleArray);

        //set bottom
        $rowCount +=2;
        $sheet->setCellValue('J' . $rowCount, 'TPHCM, ngày '. date('d') . ' tháng ' . date('m') . ' năm ' . date('Y'));

        $sheet->getStyle('J' . $rowCount)->applyFromArray(array(
            'font'  => array(
                'color' => array('rgb' => '0040FF')
        )));

        $rowCount += 1;

        $sheet->setCellValue('B' . $rowCount, 'Người lập bảng');
        $sheet->setCellValue('E' . $rowCount, 'Kế toán');
        $sheet->setCellValue('J' . $rowCount, 'Duyệt');

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman'
        ));
        $sheet->getStyle("B$rowCount:O$rowCount")->applyFromArray($styleArray);

        $rowCount += 5;

        $sheet->setCellValue('B' . $rowCount, 'Nguyễn Thị Hoàng Thi');
        $sheet->setCellValue('J' . $rowCount, 'Tô Thị Thu Thủy');

         $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman'
        ));
        $sheet->getStyle("B$rowCount:O$rowCount")->applyFromArray($styleArray);

       
        //end set bottom

        // echo "<pre>";print_r($rowCount);die;
        // set head template for sheet 2
        // 
        $sheet->setCellValue('A1', 'Tên đơn vị: CÔNG TY CỔ PHẦN DI ĐỘNG THÔNG MINH');
        $sheet->setCellValue('A2', 'Địa chỉ: 27 Nguyễn Trung Trực, P.Bến Thành, Quận 1, TPHCM');
        $sheet->setCellValue('E3', 'DANH SÁCH LAO ĐỘNG ĐÓNG BH - BW0282A');
        $sheet->setCellValue('E4', 'CÔNG TY CỔ PHẦN DI ĐỘNG THÔNG MINH');
        $sheet->mergeCells('E5:H5');
        $sheet->setCellValue('E5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y' ));

        $sheet->mergeCells('A6:A7');
        $sheet->setCellValue('A6', 'Mã công ty');
        $sheet->mergeCells('B6:B7');
        $sheet->setCellValue('B6', 'Mã Nhân Viên');
        $sheet->mergeCells('C6:C7');
        $sheet->setCellValue('C6', 'Họ và tên');
        $sheet->mergeCells('D6:D7');
        $sheet->setCellValue('D6', 'Số sổ BHXH');
        $sheet->mergeCells('E6:E7');
        $sheet->setCellValue('E6', 'Thời gian tham BH');
        $sheet->mergeCells('F6:F7');
        $sheet->setCellValue('F6', 'Thời gian đóng BH');
        $sheet->mergeCells('G6:G7');
        $sheet->setCellValue('G6', 'Tỷ lệ đóng');
        $sheet->mergeCells('H6:H7');
        $sheet->setCellValue('H6', 'LƯƠNG TRÍCH ĐÓNG BHXH');
        $sheet->mergeCells('I6:I7');
        $sheet->setCellValue('I6', '32%');
        $sheet->mergeCells('J6:K6');
        $sheet->setCellValue('J6', 'BHXH');
        $sheet->mergeCells('L6:M6');
        $sheet->setCellValue('L6', 'BHYT');
        $sheet->mergeCells('N6:O6');
        $sheet->setCellValue('N6', 'BHTN');

        $sheet->setCellValue('J7', 'NSDLD 17.5%');
        $sheet->setCellValue('K7', 'NLD 8%');
        $sheet->setCellValue('L7', 'NSDLD 3%');
        $sheet->setCellValue('M7', 'NLD 1.5%');
        $sheet->setCellValue('N7', 'NSDLD 1%');
        $sheet->setCellValue('O7', 'NLD 1%');

       
        
      
        for ($col = 'B'; $col !== 'K'; $col++) { 
             $sheet->getColumnDimension($col)
              ->setAutoSize(true);  
        }
        // end set style for sheet  
        $setLockTypeName1 = '';
        $setLockTypeName2 = '';
        $setLockTypeName3 = '';
        $setLockTypeName4 = '';
        $setLockTypeName5 = '';

         $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman'
        ));


         $sheet->getStyle('A1:A2')->applyFromArray($styleArray);
         $sheet->getStyle('E3:E5')->applyFromArray(array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman',
                'color' => array('rgb' => '0000FF')
        )));

        $sheet->getStyle('A6:N6')->applyFromArray($styleArray);
        $sheet->getStyle('A7:O7')->applyFromArray($styleArray);

        $sheet->getColumnDimension('B')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('E')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('F')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('G')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('J')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('L')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('N')->setWidth(12)->setAutoSize(false);

        $sheet->getColumnDimension('H')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('I')->setWidth(12)->setAutoSize(false);
        $sheet->getStyle('A6:O6')->getAlignment()->setWrapText(true); 
        $sheet->getStyle('E7:O7')->getAlignment()->setWrapText(true); 
        $sheet->getStyle('J6')->getAlignment()->setWrapText(false); 
        $sheet->getStyle('L6')->getAlignment()->setWrapText(false); 
        $sheet->getStyle('N6')->getAlignment()->setWrapText(false); 

        $sheet->getStyle('A5:O7')
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    

        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A6:I7')->applyFromArray($styleArray); 
         $sheet->getStyle('J7:O7')->applyFromArray($styleArray);  

        $sheet->getStyle('J6')->applyFromArray(array(
            'borders' => array(
                'bottom' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('K6')->applyFromArray(array(
            'borders' => array(
                'bottom' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 

         $sheet->getStyle('L6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('M6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )

            )
        )); 

         $sheet->getStyle('N6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('O6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )

            )
        ));  

        $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

        $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);

    } // end if    

    // sheet BW1074A-tm  --- test - fake data
    if(!empty($data1)) {
        $index = 0;
        $rowCount = 8;
        
        $objExcel->createSheet();
        $objExcel->setActiveSheetIndex(3);
        $sheet = $objExcel->getActiveSheet()->setTitle('BW1074A-tm');


        foreach ($data1 as $key => $row) {
            $rowCount++;
            $index++;
            // set name for each lock_type
            if ($row['lock_type'] == 1 && $setLockTypeName1 !== 'already set') {
                $sheet->setCellValue('A' . $rowCount, 'Danh Sách Lao Động Nước Ngoài Trong Bảng Lương');
                //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $rowCount++;
                $setLockTypeName1 = 'already set';
            }

            // end set name for each lock_type
            $sheet->getStyle("H:O")->getNumberFormat()->setFormatCode('#,##0'); // set comma for number
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $row['code']);
            $sheet->setCellValue('C' . $rowCount, $row['fullname']);
            $sheet->setCellValue('D' . $rowCount, $row['insurance_number']);
            $sheet->setCellValue('E' . $rowCount, $row['from_month']);
            $sheet->setCellValue('F' . $rowCount, $row['to_month']);
            $sheet->setCellValue('G' . $rowCount, $row['INS_RATE']);
            $sheet->setCellValueExplicit('H' . $rowCount, $row['salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('I' . $rowCount, $row['salary'] * 32 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('J' . $rowCount, ($row['salary'] * 17.5 / 100), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('K' . $rowCount, $row['salary'] * 8 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('L' . $rowCount, $row['salary'] * 3 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('M' . $rowCount, $row['salary'] * 1.5 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('N' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('O' . $rowCount, $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
     
          

            // set text center for each row 
             $sheet->getStyle('A'. $rowCount . ':Q' . $rowCount)
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
             // end set text center  for each row 
             
        } // end foreach

        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A8:O'. $rowCount)->applyFromArray($styleArray);

        //set bottom
        $rowCount +=2;
        $sheet->setCellValue('J' . $rowCount, 'TPHCM, ngày '. date('d') . ' tháng ' . date('m') . ' năm ' . date('Y'));
        $sheet->getStyle('J' . $rowCount)->applyFromArray(array(
            'font'  => array(
                'color' => array('rgb' => '0040FF')
        )));
        $rowCount += 1;

        $sheet->setCellValue('B' . $rowCount, 'Người lập bảng');
        $sheet->setCellValue('E' . $rowCount, 'Kế toán');
        $sheet->setCellValue('J' . $rowCount, 'Duyệt');

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman'
        ));
        $sheet->getStyle("B$rowCount:O$rowCount")->applyFromArray($styleArray);

        $rowCount += 5;

        $sheet->setCellValue('B' . $rowCount, 'Trần Thị Thanh Thư');
        $sheet->setCellValue('J' . $rowCount, 'Vũ Thị Cẩm Vân');

         $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman'
        ));
        $sheet->getStyle("B$rowCount:O$rowCount")->applyFromArray($styleArray);

       
        //end set bottom

        // echo "<pre>";print_r($rowCount);die;
        // set head template for sheet 2
        // 
        $sheet->setCellValue('A1', 'Tên đơn vị: CÔNG TY CỔ PHẦN DI ĐỘNG THÔNG MINH');
        $sheet->setCellValue('A2', 'Địa chỉ: 56 HỒ TÙNG MÂU, P.BẾN NGHÉ, Q.1, TP.HCM');
        $sheet->setCellValue('E3', 'DANH SÁCH LAO ĐỘNG ĐÓNG BH - BW0282A');
        $sheet->setCellValue('E4', 'CÔNG TY CỔ PHẦN DI ĐỘNG THÔNG MINH');
        $sheet->mergeCells('E5:H5');
        $sheet->setCellValue('E5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y' ));

        $sheet->mergeCells('A6:A7');
        $sheet->setCellValue('A6', 'Mã công ty');
        $sheet->mergeCells('B6:B7');
        $sheet->setCellValue('B6', 'Mã Nhân Viên');
        $sheet->mergeCells('C6:C7');
        $sheet->setCellValue('C6', 'Họ và tên');
        $sheet->mergeCells('D6:D7');
        $sheet->setCellValue('D6', 'Số sổ BHXH');
        $sheet->mergeCells('E6:E7');
        $sheet->setCellValue('E6', 'Thời gian tham BH');
        $sheet->mergeCells('F6:F7');
        $sheet->setCellValue('F6', 'Thời gian đóng BH');
        $sheet->mergeCells('G6:G7');
        $sheet->setCellValue('G6', 'Tỷ lệ đóng');
        $sheet->mergeCells('H6:H7');
        $sheet->setCellValue('H6', 'LƯƠNG TRÍCH ĐÓNG BHXH');
        $sheet->mergeCells('I6:I7');
        $sheet->setCellValue('I6', '32%');
        $sheet->mergeCells('J6:K6');
        $sheet->setCellValue('J6', 'BHXH');
        $sheet->mergeCells('L6:M6');
        $sheet->setCellValue('L6', 'BHYT');
        $sheet->mergeCells('N6:O6');
        $sheet->setCellValue('N6', 'BHTN');

        $sheet->setCellValue('J7', 'NSDLD 17.5%');
        $sheet->setCellValue('K7', 'NLD 8%');
        $sheet->setCellValue('L7', 'NSDLD 3%');
        $sheet->setCellValue('M7', 'NLD 1.5%');
        $sheet->setCellValue('N7', 'NSDLD 1%');
        $sheet->setCellValue('O7', 'NLD 1%');

       
        
      
        for ($col = 'B'; $col !== 'K'; $col++) { 
             $sheet->getColumnDimension($col)
              ->setAutoSize(true);  
        }
        // end set style for sheet  
        $setLockTypeName1 = '';
        $setLockTypeName2 = '';
        $setLockTypeName3 = '';
        $setLockTypeName4 = '';
        $setLockTypeName5 = '';

         $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman'
        ));


         $sheet->getStyle('A1:A2')->applyFromArray($styleArray);
         $sheet->getStyle('E3:E5')->applyFromArray(array(
            'font'  => array(
                'bold'  => true,
                'size'  => 12,
                'name'  => 'Times New Roman',
                'color' => array('rgb' => '0000FF')
        )));

        $sheet->getStyle('A6:N6')->applyFromArray($styleArray);
        $sheet->getStyle('A7:O7')->applyFromArray($styleArray);
        
        $sheet->getColumnDimension('B')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('E')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('F')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('G')->setWidth(10)->setAutoSize(false);
        $sheet->getColumnDimension('J')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('L')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('N')->setWidth(12)->setAutoSize(false);

        $sheet->getColumnDimension('H')->setWidth(12)->setAutoSize(false);
        $sheet->getColumnDimension('I')->setWidth(12)->setAutoSize(false);
        $sheet->getStyle('A6:O6')->getAlignment()->setWrapText(true); 
        $sheet->getStyle('E7:O7')->getAlignment()->setWrapText(true); 
        $sheet->getStyle('J6')->getAlignment()->setWrapText(false); 
        $sheet->getStyle('L6')->getAlignment()->setWrapText(false); 
        $sheet->getStyle('N6')->getAlignment()->setWrapText(false); 

        $sheet->getStyle('A5:O7')
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    

        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A6:I7')->applyFromArray($styleArray); 
         $sheet->getStyle('J7:O7')->applyFromArray($styleArray);  

        $sheet->getStyle('J6')->applyFromArray(array(
            'borders' => array(
                'bottom' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('K6')->applyFromArray(array(
            'borders' => array(
                'bottom' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 

         $sheet->getStyle('L6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('M6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )

            )
        )); 

         $sheet->getStyle('N6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )); 
        $sheet->getStyle('O6')->applyFromArray(array(
            'borders' => array(
                'top' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
                'right' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                )

            )
        ));  

        $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

        $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);

    } // end if 
    




        // download file
        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'insurance';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
                
