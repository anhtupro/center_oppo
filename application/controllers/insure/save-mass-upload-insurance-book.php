<?php

$this->_helper->layout->disableLayout();
$QStaff            = new Application_Model_Staff();
$QInsuranceLogInfo = new Application_Model_InsuranceLogInfo();
$messages_success  = array();
$messages_error    = array();
$error_list        = array();
$userStorage       = Zend_Auth::getInstance()->getStorage()->read();

if ( $this->getRequest()->getMethod() == 'POST' ) {
    
    set_time_limit(0);
    ini_set('memory_limit', -1);
    $save_folder   = 'insurance';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 15000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );

    try {

        $file = My_File::get($save_folder, $requirement, true);
        if (!$file || !count($file)) throw new Exception("Upload failed");
        $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
        $uploaded_dir  = My_File::getDefaultDir() .DIRECTORY_SEPARATOR.$save_folder. DIRECTORY_SEPARATOR. $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR. $file['filename'];
    } catch (Exception $e) {
        $this->view->messages_error = $e->getMessage();
        return;
    }

    //read file
    include 'PHPExcel/IOFactory.php';

    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $path_info     = pathinfo($inputFileName);
        $extension     = $path_info['extension'];
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {
        $this->view->messages_error = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet         = $objPHPExcel->getSheet(0);
    $highestRow    = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();
    $_failed       = false;

    //get colum name
    $rowData         = $sheet->rangeToArray('A' . 1 . ':' . $highestColumn . 1,NULL, TRUE, FALSE);
    $rowData         = $rowData[0];
    $code_cols       = NULL;
    $staff_name_cols = NULL;
    $arr_cols        = array();
    
    foreach($rowData as $key => $value){
        switch (trim($value)) {
            case 'code':
                    $code_cols = $key;
                break;

            case 'staff_name':
                    $staff_cols = $key;
                break;    

            case 'insurance_number':
                    $arr_cols[$key] = 'insurance_number';
                break;

            case 'have_book':
                    $arr_cols[$key] = 'have_book';
                break;
                
            case 'close_book':
                    $arr_cols[$key] = 'close_book';
                break;

            case 'return_book':
                    $arr_cols[$key] = 'return_ins_book_time';
                break;
                        
            default:
                
                break;
        }
    }

    if($code_cols != 'code' AND $staff_cols != 'staff_name'){
        $error_list[] = 'Vui lòng chọn đúng template';
    }

    $date = date('Y-m-d');
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        if(count($arr_cols ) > 0){
            for ($row = 2; $row <= $highestRow; $row++) {

                $rowData    = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
                $rowData    = $rowData[0];
                $code       = trim($rowData[0]);
                $staff_name = trim($rowData[1]);
                $data       = array();

                //Nếu đã duyệt hết
                if($code == '' and $staff_name == ''){
                    break;
                }

                foreach($arr_cols as $key => $value){
                    if($value == 'insurance_number'){
                        $data[$value] = $rowData[$key];
                    }else{
                        $data[$value] = ( isset($rowData[$key]) AND strtolower($rowData[$key]) == 'x' ) ? $date : NULL;    
                    }
                }   
                
                $select = $db->select()
                        ->from(array('a'=>'staff'),array('a.*'))
                        ->where('a.code = ?',$code);
                $staff = $db->fetchRow($select);

                if($staff){
                    $where      = $QStaff->getAdapter()->quoteInto('code = ?',$code);
                    $QStaff->update($data,$where);

                    //log
                    $after      = $db->fetchRow($select);
                    $QInsuranceLogInfo->save($staff,$after,$staff['id'],$userStorage->id);

                }else{
                    $error_list[] = $code .' not exist';
                    if($insurance_number == ''){
                        $error_list[] = 'Insurance number can not be empty';
                    }
                }
            }
        }else{
            $error_list[] = 'Vui lòng chọn thông tin insurance_number hoặc have_book hoặc close_book hoặc return_book ';
        }

        if(count($error_list) > 0){
            $db->rollBack();
        }

        $db->commit();

    } catch (Exception $e) {
        $db->rollback();
        $error_list[] = $e->getMessage();
    }

    if(count($error_list) == 0){
        $messages_success = 'Done';
    }

} // END

$this->view->error_list = $error_list;
$this->view->messages_success = $messages_success;
$this->view->messages_error = $messages_error;
