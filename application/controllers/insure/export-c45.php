<?php 
	set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $alpha    = 'A';
        $index    = 1;
        $heads = array(
            'stt',
            'Công ty',
            'Đơn vị',
            'code',
            'name',
            'dob',
            'chức vụ',
            'số sổ',
            'tăng',
            'giảm',
            'lương',
            'có sổ',
            'chốt sổ',
            'trả sổ',
            'lấy thẻ từ bảo hiểm',
            'chia thẻ về khu vực',
            'thu hồi thẻ từ nhân viên',
            'trả thẻ cho BH',
            'note',
            'Trạng thái'

        );
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha++ . $index, $key);
        }
        $index = 2;
        $stt = 1;
        foreach($data as $row):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit($stt++, PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['company_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['unit_code_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['dob'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['title_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['insurance_number'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['tang'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['giam'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['salary'], PHPExcel_Cell_DataType::TYPE_STRING);

            $have_book            = (isset($row['have_book']) AND $row['have_book']) ? 'x' : '';
            $close_book           = (isset($row['close_book']) AND $row['close_book']) ? 'x' : '';
            $return_book          = (isset($row['return_ins_book_time']) AND $row['return_ins_book_time']) ? 'x' : '';
            $take_card_from_ins   = (isset($row['take_card_from_ins']) AND $row['take_card_from_ins']) ? 'x' : '';
            $delivery_card        = (isset($row['delivery_card_ins']) AND $row['delivery_card_ins']) ? 'x' : '';
            $take_card_from_staff = (isset($row['take_card_from_staff']) AND $row['take_card_from_staff']) ? 'x' : '';
            $return_card_ins_time = (isset($row['return_card_ins_time']) AND $row['return_card_ins_time']) ? 'x' : '';

            $sheet->getCell($alpha++.$index)->setValueExplicit($have_book, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($close_book, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($return_book, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($take_card_from_ins, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($delivery_card, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($take_card_from_staff, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($return_card_ins_time, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['note'], PHPExcel_Cell_DataType::TYPE_STRING);
            if($row['status'] == 1){
                $status = 'ON';
            }elseif($row['status'] == 3){
                $status = 'OFF';
            }

            $sheet->getCell($alpha++.$index)->setValueExplicit($status, PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;

        $filename = 'c45';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

?>