<?php 
	$code         = $this->getRequest()->getParam('code');
        $name         = $this->getRequest()->getParam('name');
        $email        = $this->getRequest()->getParam('email');
        $area         = $this->getRequest()->getParam('area');
        $created_from = $this->getRequest()->getParam('created_from');
        $created_to   = $this->getRequest()->getParam('created_to');
        $export       = $this->getRequest()->getParam('export', 0);
        $page         = $this->getRequest()->getParam('page', 1);
        $sort         = $this->getRequest()->getParam('sort', 'created_at');
        $desc         = $this->getRequest()->getParam('desc', 0);
        $limit        = 20;
        $total        = 0;

        $params = array(
            'sort'         => $sort,
            'desc'         => $desc,
            'code'         => $code,
            'name'         => $name,
            'email'        => $email,
            'area'         => $area,
            'created_from' => $created_from,
            'created_to'   => $created_to,
            'export'       => $export
        );


        $QRegionalMarket              = new Application_Model_RegionalMarket();
        $regional_markets             = $QRegionalMarket->get_cache();
        $this->view->regional_markets = $regional_markets;

        $QSalary                      = new Application_Model_Salary();
        $list                         = $QSalary->fetchPagination($page, $limit, $total, $params);

        if($export){

        }
        
        $this->view->list   = $list;
        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST.'insurance/list-salary/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);

        $QArea                        = new Application_Model_Area();
        $this->view->area_cache       = $QArea->get_cache();
        
        $QRegion                      = new Application_Model_RegionalMarket();
        $this->view->all_region_cache = $QRegion->get_cache();
        
        $QTeam                        = new Application_Model_Team();
        $this->view->teams            = $QTeam->get_cache();
        $this->view->titles           = $QTeam->get_all('vn');

        $flashMessenger               = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;
	
?>