<?php
$staff_id = $this->getRequest()->getParam('staff_id');
$page = $this->getRequest()->getParam('page',1);
$desc = $this->getRequest()->getParam('desc',1);
$sort = $this->getRequest()->getParam('sort','created_at');
$total = 0;
$limit = LIMITATION;
$params = array(
    'staff_id' => intval($staff_id),
    'sort' => $sort,
    'desc' => $desc
);
$QStaff = new Application_Model_Staff();
$staff = $QStaff->find($staff_id)->current();
$this->view->staff = $staff;

$QInsuranceLogInfo = new Application_Model_InsuranceLogInfo();
$list = $QInsuranceLogInfo->fetchPagination($page,$limit,$total,$params);
$diffs = array();
if(count($list) > 0){
    foreach($list as $item){
        $before = unserialize($item['before']);
        $after = unserialize($item['after']);
        $diffs[] = array(
            'diffs' => array_diff_assoc($after,$before),
            'before' => $before,
            'after' => $after,
            'created_at' => $item['created_at'],
            'created_by_name' => $item['created_by_name']

        );
    }
}
$this->view->diffs = $diffs;

$labels = array(
    'unit_code_id'               => 'Mã đơn vị',
    'insurance_number'           => 'Số sổ BH',
    'hospital_id'                => 'Bệnh viện',
    'note_ins'                   => 'Ghi chú',
    'have_book'                  => 'Có sổ',
    'close_book'                 => 'Chốt sổ',
    'return_ins_book_time'       => 'Trả sổ',
    'return_book_declare_ins'    => 'Ngày báo trả sổ',
    'delivery_card_ins'          => 'Chia thẻ về khu vực',
    'take_card_from_staff'       => 'Thu hồi thẻ từ nhân viên',
    'date_insurance_increase'    => 'Ngày báo tăng lùi',
    'date_insurance_off'         => 'Ngày báo giảm lùi',
    'date_insurance_off_declare' => 'Ngày đi báo giảm lùi',
    'firstname'                  => 'Họ',
    'lastname'                   => 'Tên',
    'gender'                     => 'Giới tính',
    'dob'                        => 'Ngày sinh',
    'ID_number'                  => 'Chứng minh'
);

$this->view->labels = $labels;
$this->view->desc   = $desc;
$this->view->sort   = $sort;
$this->view->params = $params;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->url    = HOST.'insurance/log'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset = $limit*($page-1);
?>