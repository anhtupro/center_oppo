<?php 
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$date  = $this->getRequest()->getParam('date_approved',NULL);
	$leave_id = $this->getRequest()->getParam('leave_id',NULL);
	$QLeaveDetail = new Application_Model_LeaveDetail();
	
	if(!empty($date) AND !empty($leave_id)){
	    $date_approved = DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
	    $where = $QLeaveDetail->getAdapter()->quoteInto('id = ?', $leave_id);
	    $data = array(
	        'insurance_approved_at' => $date_approved
	    );
	    $QLeaveDetail->update($data, $where);
	}