<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$staff_id           = $this->getRequest()->getParam('staff_id');
$sauchedo               = $this->getRequest()->getParam('sauchedo');
$sauchedo_to_date     = $this->getRequest()->getParam('sauchedo_to_date');
$leave_id         = $this->getRequest()->getParam('leave_id');
$option_type        = $this->getRequest()->getParam('option_type');

$QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
$QLeaveDetail         = new Application_Model_LeaveDetail();
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$type = 1;
if(!empty($leave_id) AND ($sauchedo == 1) AND !empty($sauchedo_to_date) ){
    $where             = array();
    $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('id = ?' , $leave_id);
    
    
    $leave_detail = $QLeaveDetail->find($leave_id)->current();
    $to_date_old  = $leave_detail->toArray()['to_date'];
    
        $datetime = date ( 'Y-m-d H:i:s' );
        $data = array(
            'to_date'        => DateTime::createFromFormat('d/m/Y', $sauchedo_to_date)->format('Y-m-d'),
            'system_note'    => "Update to_date_old: $to_date_old created_at: $datetime"
        );
        
        $QLeaveDetail->update($data, $where);
        
//     if($option_type == 7){
//         $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('`option` = ?',$option_type);
//     }else{
//         $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('`option` IN (5,6)');
//     }
    
//     $result            = $QInsuranceStaffBasic->fetchRow($where);
//     $data = array(
//         'staff_id'          => $staff_id,
//         'type'              => $type,
//         'option'            => $option_type,
//         'time_effective'    => $time_effective,
//         'time_alter'        => DateTime::createFromFormat('d/m/Y', $time_alter)->format('Y-m-d'),
//         'created_at'        => date ( 'Y-m-d H:i:s' ),
//         'created_by'        => $userStorage->id
//     );
    
//     if(empty($result)){
//          $QInsuranceStaffBasic->insert($data);
//      }else{
//          $QInsuranceStaffBasic->update($data, $where);
//      }
}else{
   return array('code'=>1,'message'=>'Done');
}
