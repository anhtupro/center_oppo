<?php 
    $btn = $this->getRequest()->getParam('submit_btn');
    $btn_hr = $this->getRequest()->getParam('submit_btn_hr');
    define('START_ROW', 9);
    define('area', 1);
    define('code', 2);
    define('name', 3);
    define('title', 4);
    define('date_sign', 5);
    define('insurance_number', 6);
    define('insurance_code', 7);
    define('household_number', 8);
    define('note', 9);
    $datetime             = date('Y-m-d H:i:s');
    $userStorage          = Zend_Auth::getInstance()->getStorage()->read();
    $QInsuranceLogInfo    = new Application_Model_InsuranceLogInfo();
    $this->_helper->layout->disableLayout();
    $month                = $this->getRequest()->getParam('month',date('m/Y'));
    $QStaff               = new Application_Model_Staff();
    $QAsm                 = new Application_Model_Asm(); 
    $QInsuranceByMonth    = new Application_Model_InsuranceByMonth();
    $QInsuranceByMonthHr  = new Application_Model_InsuranceByMonthHr();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $area_permission = $list_regions['area'];

    if( isset($month) AND $month ){
        $date = My_Date::normal_to_mysql($month);
    }else{
        $date    = date('Y') . '-' . date('m') . '-01';
    }

    if ($this->getRequest()->getMethod() == 'POST') { // Big IF
  
    set_time_limit(0);
    ini_set('memory_limit', -1);
    // file_put_contents(APPLICATION_PATH.'/../public/files/mou/lock', '1');
    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
    
    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }
    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {    

        $this->view->errors = $e->getMessage();
        return;
    }
    //  Get worksheet dimensions
    $sheet           = $objPHPExcel->getSheet(0);
    $highestRow      = $sheet->getHighestRow();
    $highestColumn   = $sheet->getHighestColumn();
  
    $error_list      = array();
    $success_list    = array();
    $store_code_list = array();
    $number_of_order = 0;
    $total_value     = 0;
    $total_order_row = 0;
    $order_list      = array();

    $arr_inserts   = array();
    $arr_order_ids = array();

    $_failed         = false;
    $total_row       = array();
    $data_staff_code = [];
    $dataMassUpload  = array();
    $data_staff_code = array();
    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            
            if(!empty($rowData[code])){
                $data_staff_code[] = trim($rowData[code]);   
                $date_sign = null;
                if(!empty($rowData[date_sign])){
                   
//                    $time_obj = date_create_from_format('d/m/Y', $rowData[date_sign]);
//                    $time_obj = DateTime::createFromFormat('d/m/Y', $rowData[date_sign]);
                   // $stringDate = \PHPExcel_Style_NumberFormat::toFormattedString($rowData[date_sign], 'YYYY-MM-DD');
//                    $date_sign = $time_obj->format('Y-m-d');
                   // $date_sign = $stringDate;
                }
                        
                $data = array(
//                    'area'               => trim($rowData[area]),
                    'code'               => trim($rowData[code]),
                    'name'               => trim($rowData[name]),
                    'title'              => trim($rowData[title]),
                    //'date_sign'          => $date_sign,
                    'insurance_number'   => trim($rowData[insurance_number]),
                    'insurance_code'     => trim($rowData[insurance_code]),
                    'household_number'   => trim($rowData[household_number]),
                    'note'               => trim($rowData[note]),
                    'date'               =>  $date,
                    'created_at'         => $datetime,
                    'created_by'         => $userStorage->id
                );
                $dataMassUpload[] = $data;
            }
         
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }
        // nothing here
    } // END loop through order rows
    
    $progress->flush(30);
    $db = Zend_Registry::get('db');
//    $db->beginTransaction();
    
    try {
        if (!empty($dataMassUpload)) {
                $str_where = "'" .implode("', '",$data_staff_code). "'" ;
                    $sql_staff_area = "select re.id, s.code, re.name
                                FROM staff s
                                INNER JOIN regional_market rm on rm.id = s.regional_market
                                INNER JOIN area re on rm.area_id = re.id
                                WHERE s.`code` IN ($str_where)
                                GROUP BY s.id";
                        $staff_area = $db->prepare($sql_staff_area);
                        $staff_area->execute();
                        $list_staff_area = $staff_area->fetchAll(); 
                        $area_import = array();
                        $area_staff_import = array();
                        
                        if(!empty($list_staff_area)){
                            foreach ($list_staff_area as $k => $param) {
                                $area_import[] = $param['id'];
                                $area_staff_import[$param['code']] = $param['name'];
                            }
                        }
                        
                        $count_area_diff =  count(array_diff($area_import, $area_permission));
                      
                        if($count_area_diff > 0){
                             echo "Nhân viên không thuộc khu vực bạn quản lý";
                             exit();
                        }
                    $select = $db->select()
                            ->from(array('p' => 'staff'), array('p.code'))
                            ->where('p.code in (?)', $data_staff_code);
                            $result_code = $db->fetchCol($select);
                    if (count($data_staff_code) <> count($result_code)) {
                       
                        $diff = array_diff($data_staff_code, $result_code);
                     
                        echo "Nhân viên không tồn tại hoặc đã nghỉ việc hoặc trong file bị trùng code: " . implode($diff, ',');
                        throw new Exception("Nhân viên không tồn tại hoặc đã nghỉ việc hoặc trong file bị trùng code: " . implode($diff, ','));
                    }else{
                        foreach ($dataMassUpload as $k => $param) {
                            $dataMassUpload[$k]['area'] = $area_staff_import[$param['code']];
                        }
                       
                        if($btn == 1){
                            $created_by  = $userStorage->id;
                            $where_del = array();
                            $where_del[] = $QInsuranceByMonth->getAdapter()->quoteInto('created_by = ?', $created_by);
                            $where_del[] = $QInsuranceByMonth->getAdapter()->quoteInto(" `date`  = ? ", $date);
                            $QInsuranceByMonth->delete($where_del);
                            My_Controller_Action::insertAllrow($dataMassUpload, 'insurance_by_month');
                            echo '<h3 style="background-color:#28a745 !important; color: white">Thành công</h3>';
                        }
                        if($btn_hr == 1) {
                            $where_hr_del = array();
                            $where_hr_del[] = $QInsuranceByMonthHr->getAdapter()->quoteInto(" `date`  = ? ", $date);
                            $QInsuranceByMonthHr->delete($where_hr_del);
                            My_Controller_Action::insertAllrow($dataMassUpload, 'insurance_by_month_hr');
                        }
                        echo '<h3 style="background-color:#28a745 !important; color: white">Thành công</h3>';
                    }
    
//            exit();
        } else {
            throw new Exception("Danh sách nhân viên không hợp lệ");
        }
//        $db->commit();
        $progress->flush(99);
        
        $progress->flush(100);
//      
    } catch (Exception $e) {
//        $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}
