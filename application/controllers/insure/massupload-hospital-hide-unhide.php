<?php
$QHospital = new Application_Model_Hospital();
$export = $this->getRequest()->getParam('export');
if ($export) {
    $list = $QHospital->getListHospitalHide();
    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
        'STT',
        'HOSPITAL ID',
        'HOSPITAL NAME',
        'HOSPITAL CODE',
        'PROVINCE NAME',
        'PROVINCE CODE',
        'NOTE',
    );
    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }
    $intCount = 1;
    $index = 2;
    $stt = 1;
    try {
        if ($list)
            foreach ($list as $_key => $_order) {
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['id'],
                        PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['province_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['province_code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['note'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename = 'Hospital_hide_' . date('d-m-Y H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}