<?php 
    $id          = $this->getRequest()->getParam('id');
    $district_id = $this->getRequest()->getParam('district_id');
    
    $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
    $submit = $this->getRequest()->getParam('submit', 0);
    $QStaffContract = new Application_Model_StaffContract();
    
    if(!empty($submit)){
        if($userStorage->id == 5899 && !empty($district_id)){
            $where = $QStaffContract->getAdapter ()->quoteInto ( "id = ?", $id);
            $data = array(
                'district_id' => $district_id
            );
            echo $id . '-' . $district_id;
            $QStaffContract->update($data, $where);
              $this->_redirect('/insure/check-contract');
        }
    }
    $this->view->id = $id;
    $this->view->district_id = $district_id;
//    $this->_redirect('/insure/check-contract');
   