<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$staff_id           = $this->getRequest()->getParam('staff_id');
$chedo               = $this->getRequest()->getParam('chedo');
$chedo_from_date     = $this->getRequest()->getParam('chedo_from_date');
$leave_id         = $this->getRequest()->getParam('leave_id');

$QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
$QLeaveDetail         = new Application_Model_LeaveDetail();
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
if(!empty($leave_id) AND ($chedo == 1) AND !empty($chedo_from_date) ){
    $where             = array();
    $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('id = ?' , $leave_id);
    
    $leave_detail = $QLeaveDetail->find($leave_id)->current();
    $from_date_old  = $leave_detail->toArray()['from_date'];
    
        $datetime = date ( 'Y-m-d H:i:s' );
        $data = array(
            'from_date'        => DateTime::createFromFormat('d/m/Y', $chedo_from_date)->format('Y-m-d'),
            'system_note'    => "Update to_date_old: $from_date_old created_at: $datetime"
        );
        
        $QLeaveDetail->update($data, $where);
}else{
   return array('code'=>1,'message'=>'Done');
}
