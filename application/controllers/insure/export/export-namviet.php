<?php

//Trich nop Nam Viet
$sql6 = "SELECT st.id as 'staff_id',
                st.insurance_number as 'insurance_number',
                 uc.unit_code AS 'unit_code_name',
                 st.`code`,
                 CONCAT(st.firstname, ' ', st.lastname) AS 'fullname',
                 li.*,
                 CASE 
                        WHEN li.`lock_type` = 1 THEN 'Trích nộp hàng tháng'
                        WHEN li.`lock_type` = 2 THEN 'Trích nộp do tham gia ngược từ quá khứ'
                        WHEN li.`lock_type` = 3 THEN 'Trích nộp điều chỉnh lương ngược từ quá khứ'
                        WHEN li.`lock_type` = 4 THEN 'Truy thu BHYT do nhân viên nghỉ việc'
                        WHEN li.`lock_type` = 5 THEN 'Hoàn trả tiền BH do báo giảm ở quá khứ'
                        WHEN li.`lock_type` = 6 THEN 'Giảm trùng'                    
                        END AS 'lock_name',
                 CASE 
                        WHEN li.`lock_type` = 4 THEN '4.5%'
                        WHEN li.`lock_type` <> 4 THEN '32%'
                        END AS 'INS_RATE'
            FROM lock_insurance li 
            LEFT JOIN staff st ON li.staff_id = st.id
            LEFT JOIN unit_code uc ON uc.id = li.unit_code_id
            WHERE lock_time = '$lock_time' AND li.`unit_code_id` = 4 
            AND st.`id_place_province` != 64 AND li.number_of_month > 0
            AND li.`lock_type` <> 3
            ORDER BY li.`lock_type`, st.`id` ASC "
;
$stmt6 = $db->prepare($sql6);
$stmt6->execute();
$data6 = $stmt6->fetchAll();

$stmt6->closeCursor();
$stmt6 = null;
$totalRowLockType1 = 0;
$totalRowLockType2 = 0;
$totalRowLockType3 = 0;
$totalRowLockType4 = 0;
$totalRowLockType5 = 0;
$totalRowLockType6 = 0;
$total1 = $total2 = $total3 = $total4 = $total5 = $total6 = $total3_dif = 0;
if (!empty($data6)) {
    foreach ($data6 as $key => $value) {
        if ($value['lock_type'] == 1) {
            $totalRowLockType1 += 1;
            $total1 += $value['amount'];
        }
        if ($value['lock_type'] == 2) {
            $totalRowLockType2 += 1;
            $total2 += $value['amount'];
        }
        //chi thuthuy.nguyen bỏ
        if ($value['lock_type'] == 4) {
            $totalRowLockType4 += 1;
            $total4 += $value['amount'];
        }
        if ($value['lock_type'] == 5) {
            $totalRowLockType5 += 1;
            $total5 += $value['amount'];
        }
        if ($value['lock_type'] == 6) {
            $totalRowLockType6 += 1;
            $total6 += $value['amount'];
        }
    }

    $rowCount = 8;
    $index = 0; // số thứ tự
    $totalRows = 0;


    $objExcel->createSheet();
    $objExcel->setActiveSheetIndex($sheetCompanyOPPO);
    $sheetCompanyOPPO++;
    $sheet = $objExcel->getActiveSheet()->setTitle('TW9929A - NAM VIỆT');

    $sheet->setCellValue('A5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y'));

    foreach ($data6 as $key => $row) {
        $rowCount++;
        $index++;
        // set name for each lock_type
        if ($totalRows == 0) {
            // total salary for lock_type = 1
            $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
            $sheet->setCellValue('I' . $rowCount, number_format($total1));
            $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary

            $sheet->setCellValue('A' . $rowCount, 'Danh sách nhân viên trong bảng lương');
            //  set style for lock_type_name
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FF0000'),
                    'size' => 12,
                    'name' => 'Verdana'
            ));

            $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
            //
            if ($totalRowLockType1 == 0) {
                $rowCount += 2;
            } else {
                $rowCount++;
            }
        }
        if ($totalRows == $totalRowLockType1) {
            // total salary for lock_type = 2
            $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
            $sheet->setCellValue('I' . $rowCount, number_format($total2));
            $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);
            $sheet->setCellValue('A' . $rowCount, 'Danh sách trích nộp do tham gia ngược từ quá khứ');
            //  set style for lock_type_name
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FF0000'),
                    'size' => 12,
                    'name' => 'Verdana'
            ));

            $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
            //
            $index = 1;
            if ($totalRowLockType2 == 0) {
                $rowCount += 2;
            } else {
                $rowCount++;
            }
        }
        if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3) {
            // total salary for lock_type = 1
            $sheet->setCellValue('G' . $rowCount, 'TỔNG:');
            $sheet->setCellValue('H' . $rowCount, $total3_dif);
            $sheet->setCellValue('J' . $rowCount, 'TỔNG:');
            $sheet->setCellValue('K' . $rowCount, $total3);
            $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
            $rowCount++;

            $sheet->setCellValue('A' . $rowCount, 'Danh sách truy thu BHYT do nhân viên nghỉ việc');
            $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
            $sheet->setCellValue('I' . $rowCount, $total4);
            $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
            //
            //  set style for lock_type_name
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FF0000'),
                    'size' => 12,
                    'name' => 'Verdana'
            ));

            $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
            //
            $index = 1;
            if ($totalRowLockType4 == 0) {
                $rowCount += 2;
            } else {
                $rowCount++;
            }
        }
        if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3 + $totalRowLockType4) {

            $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
            $sheet->setCellValue('I' . $rowCount, $total5);
            $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
            $sheet->setCellValue('A' . $rowCount, 'Danh sách hoàn trả tiền BH do báo giảm ở quá khứ');
            //  set style for lock_type_name
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FF0000'),
                    'size' => 12,
                    'name' => 'Verdana'
            ));

            $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);

            $index = 1;
            if ($totalRowLockType5 == 0) {
                $rowCount += 2;
            } else {
                $rowCount++;
            }
        }
        if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3 + $totalRowLockType4 + $totalRowLockType5) {
            $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
            $sheet->setCellValue('I' . $rowCount, $total6);
            $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
            $sheet->setCellValue('A' . $rowCount, 'Giảm trùng');
            //  set style for lock_type_name
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FF0000'),
                    'size' => 12,
                    'name' => 'Verdana'
            ));

            $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);

            $index = 1;
            if ($totalRowLockType6 == 0) {
                $rowCount += 2;
            } else {
                $rowCount++;
            }
        }
        // end set name for each lock_type
        $sheet->getStyle("H:O")->getNumberFormat()->setFormatCode('#,##0'); // set comma for number


        $sheet->setCellValue('A' . $rowCount, $index);
        $sheet->setCellValue('B' . $rowCount, $row['code']);
        $sheet->setCellValue('C' . $rowCount, $row['fullname']);
        $sheet->setCellValue('D' . $rowCount, $row['insurance_number']);
        $sheet->setCellValue('E' . $rowCount, ($row['lock_type'] == 3) ? $row['salary'] : $row['from_month']);
        $sheet->setCellValue('F' . $rowCount, ($row['lock_type'] == 3) ? $row['old_salary'] : $row['to_month']);
        $sheet->setCellValue('G' . $rowCount, $row['INS_RATE']);
        $sheet->setCellValueExplicit('H' . $rowCount, ($row['lock_type'] == 3) ? ($row['salary'] - $row['old_salary']) : $row['salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $col_i = ($row['lock_type'] == 4) ? 4.5 : 32;

//                    $sheet->setCellValueExplicit('I' . $rowCount, ($row['lock_type'] == 3) ? $row['from_month'] : $row['number_of_month'] * $row['salary'] *  $col_i / 100, ($row['lock_type'] == 3) ? PHPExcel_Cell_DataType::TYPE_STRING : PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->setCellValueExplicit('I' . $rowCount, ($row['lock_type'] == 3) ? $row['from_month'] : $row['amount'], ($row['lock_type'] == 3) ? PHPExcel_Cell_DataType::TYPE_STRING : PHPExcel_Cell_DataType::TYPE_NUMERIC);

        if ($row['lock_type'] == 3) {
            $col_j = $row['to_month'];
        } elseif ($row['lock_type'] == 4) {
            $col_j = 0;
        } else {
            $col_j = $row['number_of_month'] * ($row['salary'] * 17.5 / 100);
        }
        $sheet->setCellValueExplicit('J' . $rowCount, $col_j, ($row['lock_type'] == 3) ? PHPExcel_Cell_DataType::TYPE_STRING : PHPExcel_Cell_DataType::TYPE_NUMERIC);

        $sheet->setCellValueExplicit('K' . $rowCount, ($row['lock_type'] == 3) ? $row['number_of_month'] * ($row['salary'] - $row['old_salary']) * 32 / 100 : ($row['lock_type'] == 4 ? 0 : $row['number_of_month'] * $row['salary'] * 8 / 100 ), PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->setCellValueExplicit('L' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 3 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->setCellValueExplicit('M' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 1.5 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);

        $sheet->setCellValueExplicit('N' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->setCellValueExplicit('O' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);

        // set text center for each row
        $sheet->getStyle('A' . $rowCount . ':Q' . $rowCount)
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // end set text center  for each row
        $totalRows++;
    } // end foreach


    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );
    $sheet->getStyle('A8:O' . $rowCount)->applyFromArray($styleArray);

    //set bottom
    $rowCount += 2;
    $sheet->setCellValue('J' . $rowCount, 'TPHCM, ngày ' . date('d') . ' tháng ' . date('m') . ' năm ' . date('Y'));

    $sheet->getStyle('J' . $rowCount)->applyFromArray(array(
        'font' => array(
            'color' => array('rgb' => '0040FF')
    )));

    $rowCount += 1;

    $sheet->setCellValue('B' . $rowCount, 'Người lập bảng');
    $sheet->setCellValue('E' . $rowCount, 'Kế toán');
    $sheet->setCellValue('J' . $rowCount, 'Duyệt');

    $styleArray = array(
        'font' => array(
            'bold' => true,
            'size' => 12,
            'name' => 'Times New Roman'
    ));
    $sheet->getStyle("B$rowCount:O$rowCount")->applyFromArray($styleArray);

    $rowCount += 5;

    $sheet->setCellValue('B' . $rowCount, 'Nguyễn Thị Thu Thuý');
    $sheet->setCellValue('J' . $rowCount, 'Tô Thị Thu Thủy');

    $styleArray = array(
        'font' => array(
            'bold' => true,
            'size' => 12,
            'name' => 'Times New Roman'
    ));
    $sheet->getStyle("B$rowCount:O$rowCount")->applyFromArray($styleArray);

    //end set bottom

    $sheet->setCellValue('A1', 'Tên đơn vị: CÔNG TY NAM VIỆT CAPITAL');
    $sheet->setCellValue('A2', 'Địa chỉ: 56 Hồ Tùng Mậu, Phường Bến Nghé, Quận 1, TP. HCM');
    $sheet->setCellValue('E3', 'DANH SÁCH LAO ĐỘNG ĐÓNG BH - TW9929A');
    $sheet->setCellValue('E4', 'CÔNG TY NAM VIỆT CAPITAL');
    $sheet->mergeCells('E5:H5');
    $sheet->setCellValue('E5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y'));

    $sheet->mergeCells('A6:A7');
    $sheet->setCellValue('A6', 'Mã công ty');
    $sheet->mergeCells('B6:B7');
    $sheet->setCellValue('B6', 'Mã Nhân Viên');
    $sheet->mergeCells('C6:C7');
    $sheet->setCellValue('C6', 'Họ và tên');
    $sheet->mergeCells('D6:D7');
    $sheet->setCellValue('D6', 'Số sổ BHXH');
    $sheet->mergeCells('E6:E7');
    $sheet->setCellValue('E6', 'Thời gian tham BH');
    $sheet->mergeCells('F6:F7');
    $sheet->setCellValue('F6', 'Thời gian đóng BH');
    $sheet->mergeCells('G6:G7');
    $sheet->setCellValue('G6', 'Tỷ lệ đóng');
    $sheet->mergeCells('H6:H7');
    $sheet->setCellValue('H6', 'LƯƠNG TRÍCH ĐÓNG BHXH');
    $sheet->mergeCells('I6:I7');

    if ($lock_time < '2018-12-01') {
        $sheet->setCellValue('I6', '4.5%');
    } else {
        $sheet->setCellValue('I6', '32%');
    }

    $sheet->mergeCells('J6:K6');
    $sheet->setCellValue('J6', 'BHXH');
    $sheet->mergeCells('L6:M6');
    $sheet->setCellValue('L6', 'BHYT');
    $sheet->mergeCells('N6:O6');
    $sheet->setCellValue('N6', 'BHTN');

    $sheet->setCellValue('J7', 'NSDLD 17.5%');
    $sheet->setCellValue('K7', 'NLD 8%');
    $sheet->setCellValue('L7', 'NSDLD 3%');
    $sheet->setCellValue('M7', 'NLD 1.5%');
    $sheet->setCellValue('N7', 'NSDLD 1%');
    $sheet->setCellValue('O7', 'NLD 1%');

    for ($col = 'B'; $col !== 'K'; $col++) {
        $sheet->getColumnDimension($col)
                ->setAutoSize(true);
    }
    // end set style for sheet
    $setLockTypeName1 = '';
    $setLockTypeName2 = '';
    $setLockTypeName3 = '';
    $setLockTypeName4 = '';
    $setLockTypeName5 = '';
    $setLockTypeName6 = '';

    $totalRowLockType1 = 0;
    $totalRowLockType2 = 0;
    $totalRowLockType3 = 0;
    $totalRowLockType4 = 0;
    $totalRowLockType5 = 0;
    $totalRowLockType6 = 0;

    $styleArray = array(
        'font' => array(
            'bold' => true,
            'size' => 12,
            'name' => 'Times New Roman'
    ));

    $sheet->getStyle('A1:A2')->applyFromArray($styleArray);
    $sheet->getStyle('E3:E5')->applyFromArray(array(
        'font' => array(
            'bold' => true,
            'size' => 12,
            'name' => 'Times New Roman',
            'color' => array('rgb' => '0000FF')
    )));

    $sheet->getStyle('A6:N6')->applyFromArray($styleArray);
    $sheet->getStyle('A7:O7')->applyFromArray($styleArray);

    $sheet->getColumnDimension('B')->setWidth(10)->setAutoSize(false);
    $sheet->getColumnDimension('E')->setWidth(10)->setAutoSize(false);
    $sheet->getColumnDimension('F')->setWidth(10)->setAutoSize(false);
    $sheet->getColumnDimension('G')->setWidth(10)->setAutoSize(false);
    $sheet->getColumnDimension('J')->setWidth(12)->setAutoSize(false);
    $sheet->getColumnDimension('L')->setWidth(12)->setAutoSize(false);
    $sheet->getColumnDimension('N')->setWidth(12)->setAutoSize(false);

    $sheet->getColumnDimension('H')->setWidth(12)->setAutoSize(false);
    $sheet->getColumnDimension('I')->setWidth(12)->setAutoSize(false);
    $sheet->getStyle('A6:O6')->getAlignment()->setWrapText(true);
    $sheet->getStyle('E7:O7')->getAlignment()->setWrapText(true);
    $sheet->getStyle('J6')->getAlignment()->setWrapText(false);
    $sheet->getStyle('L6')->getAlignment()->setWrapText(false);
    $sheet->getStyle('N6')->getAlignment()->setWrapText(false);

    $sheet->getStyle('A5:O7')
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );
    $sheet->getStyle('A6:I7')->applyFromArray($styleArray);
    $sheet->getStyle('J7:O7')->applyFromArray($styleArray);

    $sheet->getStyle('J6')->applyFromArray(array(
        'borders' => array(
            'bottom' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            ),
            'top' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    ));
    $sheet->getStyle('K6')->applyFromArray(array(
        'borders' => array(
            'bottom' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            ),
            'top' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            ),
            'right' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    ));

    $sheet->getStyle('L6')->applyFromArray(array(
        'borders' => array(
            'top' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    ));
    $sheet->getStyle('M6')->applyFromArray(array(
        'borders' => array(
            'top' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            ),
            'right' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    ));

    $sheet->getStyle('N6')->applyFromArray(array(
        'borders' => array(
            'top' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    ));
    $sheet->getStyle('O6')->applyFromArray(array(
        'borders' => array(
            'top' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            ),
            'right' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    ));

    $styleArray = array(
        'font' => array(
            'bold' => true,
            'color' => array('rgb' => 'FF0000'),
            'size' => 12,
            'name' => 'Verdana'
    ));

    $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
} // end if