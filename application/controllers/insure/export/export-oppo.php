<?php 
    //Trich nop OPPO
    $sql1 = "SELECT st.id as 'staff_id',
                st.insurance_number as 'insurance_number',
                 uc.unit_code AS 'unit_code_name',
                 st.`code`,
                 CONCAT(st.firstname, ' ', st.lastname) AS 'fullname',
                 li.*,
                 CASE 
                        WHEN li.`lock_type` = 1 THEN 'Trích nộp hàng tháng'
                        WHEN li.`lock_type` = 2 THEN 'Trích nộp do tham gia ngược từ quá khứ'
                        WHEN li.`lock_type` = 3 THEN 'Trích nộp điều chỉnh lương ngược từ quá khứ'
                        WHEN li.`lock_type` = 4 THEN 'Truy thu BHYT do nhân viên nghỉ việc'
                        WHEN li.`lock_type` = 5 THEN 'Hoàn trả tiền BH do báo giảm ở quá khứ'
                        WHEN li.`lock_type` = 6 THEN 'Giảm trùng'                    
                        WHEN li.`lock_type` = 7 THEN 'Other'                    
                        END AS 'lock_name',
                 CASE 
                        WHEN li.`lock_type` = 4 THEN '4.5%'
                        WHEN li.`lock_type` <> 4 THEN '32%'
                        END AS 'INS_RATE'
            FROM lock_insurance li 
            LEFT JOIN staff st ON li.staff_id = st.id
            LEFT JOIN unit_code uc ON uc.id = li.unit_code_id
                    WHERE lock_time = '$lock_time' AND li.`unit_code_id` = 1 
                    AND st.`id_place_province` not in (64, 65) AND li.number_of_month > 0
                     ORDER BY li.`lock_type`, st.`id` ASC "
            ;
    //1
    $stmt1 = $db->prepare($sql1);
    $stmt1->execute();
    $data1 = $stmt1->fetchAll();

    $stmt1->closeCursor();
    $stmt1 = null;
    
        // set style for excel
    $style_lock_type_name = array(
        'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => 'FF0000'),
            'size'  => 12,
            'name'  => 'Verdana'
            ));//  set style for lock_type_name     
    $style_total_salary = array(
        'font'  => array(
            'bold'  => true
        ));
    $style_border = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );  

    $total1 = $total2 = $total3 = $total4 = $total5 = $total6 = $total7 =  $total3_dif = 0;
    if(!empty($data1)) {
        foreach ($data1 as $key => $value) {
            if ($value['lock_type'] == 1) {
                $totalRowLockType1 += 1;
//                $total1 += $value['number_of_month'] * $value['salary'] * 32 / 100;
                $total1 += $value['amount'];
            }
            if ($value['lock_type'] == 2) {
                $totalRowLockType2 += 1;
//                $total2 += $value['number_of_month'] * $value['salary'] * 32 / 100; 
                $total2 +=  $value['amount']; 
            }
            if ($value['lock_type'] == 3) {
                $totalRowLockType3 += 1;
//                $total3 += $value['number_of_month'] * ($value['salary'] - $value['old_salary']) * 32 / 100; 
                $total3 +=  $value['amount'];
                $total3_dif += ($value['salary'] - $value['old_salary']); 

            }
            if ($value['lock_type'] == 4) {
                $totalRowLockType4 += 1;
//                $total4 += $value['number_of_month'] * $value['salary'] * 4.5 / 100; 
                $total4 +=  $value['amount'];
            }
            if ($value['lock_type'] == 5) {
                $totalRowLockType5 += 1;
//                $total5 += $value['number_of_month'] * $value['salary'] * 32 / 100; 
                $total5 +=  $value['amount'];
            }
            if ($value['lock_type'] == 6) {
                $totalRowLockType6 += 1;
//                $total6 += $value['number_of_month'] * $value['salary'] * 32 / 100; 
                $total6 +=  $value['amount'];
            }
            
             if ($value['lock_type'] == 7) {
                $totalRowLockType7 += 1;
//                $total6 += $value['number_of_month'] * $value['salary'] * 32 / 100; 
                $total7 +=  $value['amount'];
            }
        }     
    
        $rowCount = 8;    
        $index = 0; // số thứ tự
        $totalRows = 0; 

        $objExcel = new PHPExcel();
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'oppo.xlsx';

        $objExcel->createSheet();
        
        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objExcel   = $objReader->load($path);
        $objExcel->setActiveSheetIndex($sheetCompanyOPPO);
        $sheetCompanyOPPO ++;
        $sheet = $objExcel->getActiveSheet()->setTitle('VĨNH KHANG - TA7074A');
        
        $sheet->setCellValue('A5', 'Tháng ' . date_format(date_create($lock_time), 'm/Y' ));

        foreach ($data1 as $key => $row) {
            $rowCount++;
            $index++;
            // set name for each lock_type
            if ($totalRows == 0) {
                // total salary for lock_type = 1
                $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('I' . $rowCount, number_format($total1));
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
                
                $sheet->setCellValue('A' . $rowCount, 'Danh sách nhân viên trong bảng lương');
                //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
               if ($totalRowLockType1 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }
            if ($totalRows == $totalRowLockType1) {
                // total salary for lock_type = 2
                $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('I' . $rowCount, number_format($total2));
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);
                $sheet->setCellValue('A' . $rowCount, 'Danh sách trích nộp do tham gia ngược từ quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType2 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
                    
            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2) {
                // total salary for lock_type = 1
                $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('I' . $rowCount, $total2);
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
                
                $sheet->setCellValue('A' . $rowCount, 'Danh sách trích nộp chỉnh lương ngược từ quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                
                // add new salary, old salary row for lock_type = 3
                $sheet->setCellValue('E' . $rowCount, 'ML Mới');
                $sheet->setCellValue('F' . $rowCount, 'ML Cũ');
                $sheet->setCellValue('H' . $rowCount, 'Chênh lệch');
                $sheet->setCellValue('I' . $rowCount, 'Từ tháng');
                $sheet->setCellValue('J' . $rowCount, 'Đến tháng');
                $sheet->setCellValue('K' . $rowCount, 'Số tiền');
                $sheet->getStyle('J' . $rowCount)->applyFromArray($style_total_salary); 
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary); 
                $sheet->getStyle('K' . $rowCount)->applyFromArray($style_total_salary); 
                    //style sheet
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 9,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('E' . $rowCount)->applyFromArray($styleArray);
                $sheet->getStyle('F' . $rowCount)->applyFromArray($styleArray);
                $sheet->getStyle('H' . $rowCount)->applyFromArray($styleArray);
                $sheet->getStyle('O' . $rowCount)->applyFromArray($styleArray);
                //end 
                
                $index = 1;
                if ($totalRowLockType3 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 

            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3) {
                // total salary for lock_type = 1
                $sheet->setCellValue('G' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('H' . $rowCount, $total3_dif);
                $sheet->setCellValue('J' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('K' . $rowCount, $total3);
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
                $rowCount++;
                
                $sheet->setCellValue('A' . $rowCount, 'Danh sách truy thu BHYT do nhân viên nghỉ việc');
                $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('I' . $rowCount, $total4);
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
                //
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                //
                $index = 1;
                if ($totalRowLockType4 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3 +$totalRowLockType4) {
                
                $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('I' . $rowCount, $total5);
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
                $sheet->setCellValue('A' . $rowCount, 'Danh sách hoàn trả tiền BH do báo giảm ở quá khứ');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                
                $index = 1;
               if ($totalRowLockType5 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3 +$totalRowLockType4 + $totalRowLockType5) {
                $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('I' . $rowCount, $total6);
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
                $sheet->setCellValue('A' . $rowCount, 'Giảm trùng');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                
                $index = 1;
               if ($totalRowLockType6 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }
            if ($totalRows == $totalRowLockType1 + $totalRowLockType2 + $totalRowLockType3 +$totalRowLockType4 + $totalRowLockType5 + $totalRowLockType6) {
                $sheet->setCellValue('H' . $rowCount, 'TỔNG:');
                $sheet->setCellValue('I' . $rowCount, $total7);
                $sheet->getStyle('I' . $rowCount)->applyFromArray($style_total_salary);  //  set style for total_salary
                $sheet->setCellValue('A' . $rowCount, 'Other');
                 //  set style for lock_type_name   
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                        'size'  => 12,
                        'name'  => 'Verdana'
                    ));

                $sheet->getStyle('A' . $rowCount)->applyFromArray($styleArray);
                
                $index = 1;
               if ($totalRowLockType7 == 0) { 
                    $rowCount += 2;
                }
                else {
                    $rowCount++;
                } 
            }
            
            
            // end set name for each lock_type
            $sheet->getStyle("H:O")->getNumberFormat()->setFormatCode('#,##0'); // set comma for number
         

            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $row['code']);
            $sheet->setCellValue('C' . $rowCount, $row['fullname']);
            $sheet->setCellValue('D' . $rowCount, $row['insurance_number']);    
            $sheet->setCellValue('E' . $rowCount, ($row['lock_type'] == 3) ? $row['salary'] : $row['from_month']);
            $sheet->setCellValue('F' . $rowCount, ($row['lock_type'] == 3) ? $row['old_salary'] : $row['to_month']);
            $sheet->setCellValue('G' . $rowCount, $row['INS_RATE']);
            $sheet->setCellValueExplicit('H' . $rowCount, ($row['lock_type'] == 3) ? ($row['salary'] - $row['old_salary']) : $row['salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
             $col_i = ($row['lock_type'] == 4) ? 4.5 : 32;
//            $sheet->setCellValueExplicit('I' . $rowCount, ($row['lock_type'] == 3) ? $row['from_month'] : $row['number_of_month'] * $row['salary'] *  $col_i / 100, ($row['lock_type'] == 3) ? PHPExcel_Cell_DataType::TYPE_STRING : PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('I' . $rowCount, ($row['lock_type'] == 3) ? $row['from_month'] :  $row['amount'], ($row['lock_type'] == 3) ? PHPExcel_Cell_DataType::TYPE_STRING : PHPExcel_Cell_DataType::TYPE_NUMERIC);
            
            if($row['lock_type'] == 3){
                $col_j = $row['to_month'];
            }elseif($row['lock_type'] == 4) {
                $col_j = 0;
            }else{
                $col_j = $row['number_of_month'] * ($row['salary'] * 17.5 / 100);
            }       
            $sheet->setCellValueExplicit('J' . $rowCount, $col_j , ($row['lock_type'] == 3) ? PHPExcel_Cell_DataType::TYPE_STRING : PHPExcel_Cell_DataType::TYPE_NUMERIC);
            
            $sheet->setCellValueExplicit('K' . $rowCount, ($row['lock_type'] == 3) ? $row['number_of_month'] * ($row['salary'] - $row['old_salary']) * 32 / 100 : ($row['lock_type'] == 4 ? 0 : $row['number_of_month'] * $row['salary'] * 8 / 100 ) , PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('L' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 3 / 100  , PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('M' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 1.5 / 100 , PHPExcel_Cell_DataType::TYPE_NUMERIC);
            
            $sheet->setCellValueExplicit('N' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->setCellValueExplicit('O' . $rowCount, ($row['lock_type'] == 3 || $row['lock_type'] == 4) ? 0 : $row['number_of_month'] * $row['salary'] * 1 / 100, PHPExcel_Cell_DataType::TYPE_NUMERIC);
     
            // set text center for each row 
             $sheet->getStyle('A'. $rowCount . ':Q' . $rowCount)
             ->getAlignment()
             ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
             // end set text center  for each row 
             $totalRows++;
        } // end foreach
        

        // set style for sheet 
        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $sheet->getStyle('A8:O'. $rowCount)->applyFromArray($styleArray);
                  

        for ($col = 'B'; $col !== 'Q'; $col++) { 
             $sheet->getColumnDimension($col)
              ->setAutoSize(true);  
        }
        // end set style for sheet  
        $setLockTypeName1 = '';
        $setLockTypeName2 = '';
        $setLockTypeName3 = '';
        $setLockTypeName4 = '';
        $setLockTypeName5 = '';
        $setLockTypeName6 = '';
        $setLockTypeName7 = '';

        $totalRowLockType1 = 0;
        $totalRowLockType2 = 0;
        $totalRowLockType3 = 0;
        $totalRowLockType4 = 0;
        $totalRowLockType5 = 0;
        $totalRowLockType6 = 0;
        $totalRowLockType7 = 0;

    } // end if
    //end sheet oppo