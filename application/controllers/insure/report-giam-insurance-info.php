<?php 

set_time_limit(0);
ini_set('memory_limit', '5120M');

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();

require_once 'PHPExcel.php';

$alphaExcel = new My_AlphaExcel();

$PHPExcel = new PHPExcel();
$heads = array(
    $alphaExcel->ShowAndUp() => 'Công ty',
    $alphaExcel->ShowAndUp() => 'Code',
    $alphaExcel->ShowAndUp() => 'Họ tên',
    $alphaExcel->ShowAndUp() => 'Số sổ',
    $alphaExcel->ShowAndUp() => 'Ngày sinh',
    $alphaExcel->ShowAndUp() => 'Nữ (X)',
    $alphaExcel->ShowAndUp() => 'Chức danh',
    $alphaExcel->ShowAndUp() => 'Tiền đồng',
    $alphaExcel->ShowAndUp() => 'Ngày hiệu lực',
    $alphaExcel->ShowAndUp() => 'Nhập ký hiệu loại Tăng',
    $alphaExcel->ShowAndUp() => 'Số hợp đồng',
    $alphaExcel->ShowAndUp() => 'Diễn giải loại hợp đồng',
    $alphaExcel->ShowAndUp() => 'Ngày ký HĐ/QĐ',
    $alphaExcel->ShowAndUp() => 'Tỷ lệ đóng(%)',
    $alphaExcel->ShowAndUp() => 'Đối tượng',
    $alphaExcel->ShowAndUp() => 'Mã tỉnh',
    $alphaExcel->ShowAndUp() => 'Mã bệnh viện',
    $alphaExcel->ShowAndUp() => 'Mã dân tộc',
    $alphaExcel->ShowAndUp() => 'Số CMND',
    $alphaExcel->ShowAndUp() => 'Ngày cấp',
    $alphaExcel->ShowAndUp() => 'Mã tỉnh',
    $alphaExcel->ShowAndUp() => 'Mã Xã/phường(khai sinh)',
    $alphaExcel->ShowAndUp() => 'Mã Quận/huyện(khai sinh)',
    $alphaExcel->ShowAndUp() => 'Mã Tỉnh/TP(khai sinh)',
    $alphaExcel->ShowAndUp() => 'Số nhà, đường phố, thôn xóm(hộ khẩu)',
    $alphaExcel->ShowAndUp() => 'Mã Xã/phường(hộ khẩu)',
    $alphaExcel->ShowAndUp() => 'Mã Quận/huyện(hộ khẩu)',
    $alphaExcel->ShowAndUp() => 'Mã Tỉnh/TP(hộ khẩu)',
    $alphaExcel->ShowAndUp() => 'Số nhà, đường phố, thôn xóm(tạm trú)',
    $alphaExcel->ShowAndUp() => 'Mã Xã/phường(tạm trú)',
    $alphaExcel->ShowAndUp() => 'Mã Quận/huyện(tạm trú)',
    $alphaExcel->ShowAndUp() => 'Mã Tỉnh/TP(tạm trú) ',
    $alphaExcel->ShowAndUp() => 'Số điện thoại liên hệ',
    $alphaExcel->ShowAndUp() => 'Email',
    $alphaExcel->ShowAndUp() => 'Mã vùng',
    $alphaExcel->ShowAndUp() => 'Chủ hộ',
    $alphaExcel->ShowAndUp() => 'Relative ',
    $alphaExcel->ShowAndUp() => 'Kết thúc nghỉ ',
    $alphaExcel->ShowAndUp() => 'Ngày dự sinh ',
        
);

$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

foreach($heads as $key => $value)
{
    $sheet->setCellValue($key.'1', $value);
}

$index = 1;

foreach($deCreateInfo as $key => $value)
{
    $alphaExcel = new My_AlphaExcel();
     
    $kyhieu = '';
    $option = $value['option'];
    if(in_array($option, array(3,4))){
        $kyhieu = 'GH';
    }elseif (in_array($option, array(5,6) )){
        $kyhieu = 'DC';
    }elseif (in_array($option, array(10,14) )){
        $kyhieu = 'KL';
    }elseif (in_array($option, array(11,15) )){
        $kyhieu = 'TS';
    }elseif (in_array($option, array(8,12) )){
        $kyhieu = 'OF_1';
    }elseif (in_array($option, array(9,13) )){
        $kyhieu = 'OF_2';
    }elseif (in_array($option, array(16,17) )){
        $kyhieu = 'ON';
    }
    
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['unit_code_name']);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
    
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['insurance_number'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dob'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['gender'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['job_title'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['salary'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['time_effective'])?date('d/m/Y', strtotime($value['time_effective'])):'');
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $kyhieu ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['ContractCode'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['ContractTerm'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['time_effective'])?date('d/m/Y', strtotime($value['time_effective'])):'');
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '32' ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '1' ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['MaTinh'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['MaBV'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['MaDT'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ID_number'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['ID_date'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ID_place'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ward1'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['district_name1'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['city_name1'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['address3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ward3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['district_name3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['city_name3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['address2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ward2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['district_name2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['city_name2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['phone_number'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['email'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['area_nationality_code'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), My_Staff_Relative::get($value['relative_type']),PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['to_date'])?date('d/m/Y', strtotime($value['to_date'])):'');
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['due_date'])?date('d/m/Y', strtotime($value['due_date'])):'');
    
    $index++;

}
$filename = 'Giảm - ' . date('Y-m-d H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit();




