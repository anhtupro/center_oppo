<?php

$flashMessenger = $this->_helper->flashMessenger;
$date_approved  = $this->getRequest()->getParam('date_approved', NULL);
$name           = $this->getRequest()->getParam('name');
$company_id     = $this->getRequest()->getParam('company_id');
$code           = $this->getRequest()->getParam('code');
$ins_number     = $this->getRequest()->getParam('ins_number');
$type           = $this->getRequest()->getParam('type', -1);
$option         = $this->getRequest()->getParam('option', 0);

$locked       = $this->getRequest()->getParam('locked', -1);
$month        = $this->getRequest()->getParam('month', date('m/Y'));
$unit_code_id = $this->getRequest()->getParam('unit_code_id');
$before       = $this->getRequest()->getParam('before', 1);
$total        = 0;
$page         = $this->getRequest()->getParam('page', 1);
$desc         = $this->getRequest()->getParam('desc', 0);
$sort         = $this->getRequest()->getParam('sort', 'locked');
$limit        = 500;
$report       = $this->getRequest()->getParam('export', 0);
$search       = $this->getRequest()->getParam('search', 0);

$action_insurance = $this->getRequest()->getParam('action_insurance');
$report_tang      = $this->getRequest()->getParam('report_tang', 0);
$report_giam      = $this->getRequest()->getParam('report_giam', 0);
$report_chotso    = $this->getRequest()->getParam('report_chotso', 0);
$s_qdnv           = $this->getRequest()->getParam('s_qdnv');
$get_qdnv         = $this->getRequest()->getParam('get_qdnv');
$get_qdnkl        = $this->getRequest()->getParam('get_qdnkl');
$tang_moi_ids     = $this->getRequest()->getParam('tang_moi_ids', null);
$giam_han_ids     = $this->getRequest()->getParam('giam_han_ids', null);
$dieuchinh_ids    = $this->getRequest()->getParam('dieuchinh_ids', null);
$chedo_ids        = $this->getRequest()->getParam('chedo_ids', null);
$chedo_after_ids  = $this->getRequest()->getParam('chedo_after_ids', null);
$total_ids        = $this->getRequest()->getParam('total_ids', null);


$back_url = $this->getRequest()->getParam('back_url', '/insurance/report-list');

$QInsuranceStaff = new Application_Model_InsuranceStaff();

$params = array(
    'date_ins'      => $date_ins,
    'date_approved' => $date_approved,
    'name'          => $name,
    'company_id'    => $company_id,
    'unit_code_id'  => intval($unit_code_id),
    'code'          => $code,
    'ins_number'    => $ins_number,
    'type'          => intval($type),
    'option'        => intval($option),
    'locked'        => intval($locked),
    'month'         => $month,
    'before'        => intval($before),
    's_qdnv'        => trim($s_qdnv),
);

if (isset($params['month']) AND $params['month']) {
    $date = My_Date::normal_to_mysql($params['month']);
} else {
    $date = date('Y') . '-' . date('m') . '-01';
}

$date_ins    = (trim($params['date_ins'])) ? My_Date::normal_to_mysql($params['date_ins']) : NULL;
$paramsStore = array(
    $date,
    $params['unit_code_id'],
    NULL,
    isset($params['name']) ? $params['name'] : '',
    isset($params['code']) ? $params['code'] : '',
);

$db          = Zend_Registry::get('db');

$stmt_inCreate = $db->prepare('CALL PR_Increate_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode) ');
$stmt_inCreate->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
//        $stmt_inCreate->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
$stmt_inCreate->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
$stmt_inCreate->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
$stmt_inCreate->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
$stmt_inCreate->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);

$stmt_inCreate->execute();
$inCreate             = $stmt_inCreate->fetchAll();
$stmt_inCreate->closeCursor();
$this->view->inCreate = $inCreate;

//DeCreate giam
$stmt_deCreate = $db->prepare('CALL PR_Decreate_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode) ');
$stmt_deCreate->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
$stmt_deCreate->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
$stmt_deCreate->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
$stmt_deCreate->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
$stmt_deCreate->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);

$stmt_deCreate->execute();
$deCreate             = $stmt_deCreate->fetchAll();
$stmt_deCreate->closeCursor();
$this->view->deCreate = $deCreate;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id     = $userStorage->id;
if ($user_id == 1) {
    echo "<pre>";
    print_r($data);
    die;
}
//Adjust dieu chinh
$stmt_adjust = $db->prepare('CALL PR_Adjust_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode) ');
$stmt_adjust->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
$stmt_adjust->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
$stmt_adjust->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
$stmt_adjust->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
$stmt_adjust->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);

$stmt_adjust->execute();
$adjust             = $stmt_adjust->fetchAll();
$stmt_adjust->closeCursor();
$this->view->adjust = $adjust;


//Special che do
$stmt_special = $db->prepare('CALL PR_Special_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode) ');
$stmt_special->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
$stmt_special->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
$stmt_special->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
$stmt_special->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
$stmt_special->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);

$stmt_special->execute();
$special             = $stmt_special->fetchAll();
$stmt_special->closeCursor();
$this->view->special = $special;
//
//        //After Leave sau che do
$stmt_after_leave    = $db->prepare('CALL PR_After_Leave_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode) ');
$stmt_after_leave->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
$stmt_after_leave->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
$stmt_after_leave->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
$stmt_after_leave->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
$stmt_after_leave->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);

$stmt_after_leave->execute();
$after_leave             = $stmt_after_leave->fetchAll();
$stmt_after_leave->closeCursor();
$this->view->after_leave = $after_leave;


//Transfer 
$stmt_transfer = $db->prepare('CALL PR_Transfer_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode) ');
$stmt_transfer->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
$stmt_transfer->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
$stmt_transfer->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
$stmt_transfer->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
$stmt_transfer->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);

$stmt_transfer->execute();
$transfer             = $stmt_transfer->fetchAll();
$stmt_transfer->closeCursor();
$this->view->transfer = $transfer;

//Change
$stmt_change = $db->prepare('CALL PR_Change_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode) ');
$stmt_change->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
$stmt_change->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
$stmt_change->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
$stmt_change->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
$stmt_change->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);

$stmt_change->execute();
$change             = $stmt_change->fetchAll();
$stmt_change->closeCursor();
$this->view->change = $change;

if ($report AND $action_insurance == '#tang') {
    $this->reportTangInsuranceAction($inCreate);
}

if ($report AND $action_insurance == '#giam') {
    $this->reportGiamInsuranceAction($deCreate);
}

if ($report AND $action_insurance == '#dieuchinh') {
    $this->reportDieuChinhInsuranceAction($adjust);
}

if ($report AND $action_insurance == '#chedo') {
    $this->reportBatthuongAction($change);
}

if ($report_tang AND $action_insurance == '#tang') {
    $stmt_inCreateInfo = $db->prepare('CALL PR_Increate_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
    $stmt_inCreateInfo->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
    $stmt_inCreateInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
    $stmt_inCreateInfo->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
    $stmt_inCreateInfo->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
    $stmt_inCreateInfo->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);
    $stmt_inCreateInfo->bindParam('pStaffId', My_Util::escape_string($tang_moi_ids), PDO::PARAM_STR);

    $stmt_inCreateInfo->execute();
    $inCreateInfo = $stmt_inCreateInfo->fetchAll();
    $stmt_inCreateInfo->closeCursor();

    $this->reportTangInsuranceInfoAction($inCreateInfo);
}

if ($report_giam AND $action_insurance == '#giam') {
    $stmt_deCreateInfo = $db->prepare('CALL PR_Decreate_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
    $stmt_deCreateInfo->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
    $stmt_deCreateInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
    $stmt_deCreateInfo->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
    $stmt_deCreateInfo->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
    $stmt_deCreateInfo->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);
    $stmt_deCreateInfo->bindParam('pStaffId', My_Util::escape_string($giam_han_ids), PDO::PARAM_STR);

    $stmt_deCreateInfo->execute();
    $deCreateInfo = $stmt_deCreateInfo->fetchAll();
    $stmt_deCreateInfo->closeCursor();

    $this->reportGiamInsuranceInfoAction($deCreateInfo);
}

if ($report_giam AND $action_insurance == '#chedo') {

    $stmt_ChedoInfo = $db->prepare('CALL PR_Special_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pLeaveId) ');
    $stmt_ChedoInfo->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
    $stmt_ChedoInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
    $stmt_ChedoInfo->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
    $stmt_ChedoInfo->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
    $stmt_ChedoInfo->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);
    $stmt_ChedoInfo->bindParam('pLeaveId', My_Util::escape_string($chedo_ids), PDO::PARAM_STR);

    $stmt_ChedoInfo->execute();
    $chedoInfo = $stmt_ChedoInfo->fetchAll();
    $stmt_ChedoInfo->closeCursor();

    $this->reportGiamInsuranceInfoAction($chedoInfo);
}

if ($report_tang AND $action_insurance == '#chedo') {

    $stmt_SauChedoInfo = $db->prepare('CALL PR_After_Leave_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pLeaveId) ');
    $stmt_SauChedoInfo->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
    $stmt_SauChedoInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
    $stmt_SauChedoInfo->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
    $stmt_SauChedoInfo->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
    $stmt_SauChedoInfo->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);
    $stmt_SauChedoInfo->bindParam('pLeaveId', My_Util::escape_string($chedo_after_ids), PDO::PARAM_STR);

    $stmt_SauChedoInfo->execute();
    $sauchedoInfo = $stmt_SauChedoInfo->fetchAll();
    $stmt_SauChedoInfo->closeCursor();

    $this->reportTangInsuranceInfoAction($sauchedoInfo);
}

if ($report_tang AND $action_insurance == '#total') {
    $stmt_TotalIncreate = $db->prepare('CALL PR_Total_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pInsuranceId) ');
    $stmt_TotalIncreate->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
    $stmt_TotalIncreate->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
    $stmt_TotalIncreate->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
    $stmt_TotalIncreate->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
    $stmt_TotalIncreate->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);
    $stmt_TotalIncreate->bindParam('pInsuranceId', My_Util::escape_string($total_ids), PDO::PARAM_STR);
    $stmt_TotalIncreate->execute();
    $totalIncreate      = $stmt_TotalIncreate->fetchAll();
    $stmt_TotalIncreate->closeCursor();
    $this->reportTangInsuranceInfoAction($totalIncreate);
}

if ($report_giam AND $action_insurance == '#total') {
    $stmt_TotalDecreate = $db->prepare('CALL PR_Total_Insurance (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pInsuranceId) ');
    $stmt_TotalDecreate->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
    $stmt_TotalDecreate->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
    $stmt_TotalDecreate->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
    $stmt_TotalDecreate->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
    $stmt_TotalDecreate->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);
    $stmt_TotalDecreate->bindParam('pInsuranceId', My_Util::escape_string($total_ids), PDO::PARAM_STR);
    $stmt_TotalDecreate->execute();
    $totalDecreate      = $stmt_TotalDecreate->fetchAll();
    $stmt_TotalDecreate->closeCursor();

    $this->reportGiamInsuranceInfoAction($totalDecreate);
}

if ($report_tang AND $action_insurance == '#dieuchinh') {
    //Adjust 
    $stmt_adjustInfo = $db->prepare('CALL PR_Adjust_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
    $stmt_adjustInfo->bindParam('r', My_Util::escape_string($paramsStore[0]), PDO::PARAM_STR);
    $stmt_adjustInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
    $stmt_adjustInfo->bindParam('pDateIns', My_Util::escape_string($paramsStore[2]), PDO::PARAM_STR);
    $stmt_adjustInfo->bindParam('pName', My_Util::escape_string($paramsStore[3]), PDO::PARAM_STR);
    $stmt_adjustInfo->bindParam('pCode', My_Util::escape_string($paramsStore[4]), PDO::PARAM_STR);
    $stmt_adjustInfo->bindParam('pStaffId', My_Util::escape_string($dieuchinh_ids), PDO::PARAM_STR);

    $stmt_adjustInfo->execute();
    $adjust_info = $stmt_adjustInfo->fetchAll();
    $stmt_adjustInfo->closeCursor();
    $this->reportTangInsuranceInfoAction($adjust_info);
}
if ($search || $export) {
    $list             = $QInsuranceStaff->fetchPaginationReport($page, $limit, $total, $params);
    $this->view->list = $list;
}


$QCompanies            = new Application_Model_Company();
$companies             = $QCompanies->get_cache();
$this->view->companies = $companies;

$QUnitCode              = new Application_Model_UnitCode();
$unit_codes             = $QUnitCode->get_all();
$this->view->unit_codes = $unit_codes;

$QInsuranceOption    = new Application_Model_InsuranceOption();
$options             = $QInsuranceOption->get_cache();
$this->view->options = $options;

$active = "#tang";

if (!empty($inCreate)) {
    $active = "#tang";
} elseif (!empty($deCreate)) {
    $active = "#giam";
} elseif (!empty($adjust)) {
    $active = "#dieuchinh";
} elseif (!empty($special) OR ! empty($after_leave) OR ! empty($change)) {
    $active = "#chedo";
} elseif (!empty($list)) {
    $active = "#total";
}

$this->view->active           = $active;
$this->view->desc             = $desc;
$this->view->sort             = $sort;
$this->view->params           = $params;
$this->view->limit            = $limit;
$this->view->total            = $total;
$this->view->url              = HOST . 'insurance/report/' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset           = $limit * ($page - 1);
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages         = $messages;
$this->view->messages_success = $messages_success;
