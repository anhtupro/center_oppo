<?php
$code = trim($this->getRequest()->getParam('imei', ''));
$time            = $this->getRequest()->getParam('month',date('m/Y'));
$db = Zend_Registry::get('db');
$userStorage          = Zend_Auth::getInstance()->getStorage()->read();
$created_by = $userStorage->id;
$flashMessenger = $this->_helper->flashMessenger;
$export           = $this->getRequest()->getParam('export');

if(!empty($export)){
    $time = explode("/", $time);

    $month=$time[0];
    $year=$time[1];



    $sql1="SELECT st.code FROM lock_insurance li INNER JOIN staff st ON li.staff_id = st.id WHERE
                li.not_link_to_salary=1 AND MONTH(li.lock_time)=$month AND YEAR(li.lock_time)=$year";
    $stmt1 = $db->prepare($sql1);

    $stmt1->execute();

    $data = $stmt1->fetchAll();

//    print_r($data);
//    die();

    $stmt1->closeCursor();
    $stmt1 = null;

    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'Staff Code',
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;

    try {
        if ($data)
            foreach ($data as $_key => $_order) {

                $alpha = 'A';
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code'], PHPExcel_Cell_DataType::TYPE_STRING);

                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename  = 'Lock_insurance' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;

    $flashMessenger->setNamespace('success')->addMessage('Thành công.');
    $this->_redirect(HOST . 'insure/passby-payable');
}else{
    if(!empty($code)){

        $params = array(
            'month'         => $time,
        );
        if( isset($params['month']) AND $params['month'] ){
            $date = My_Date::normal_to_mysql($params['month']);
        }else{
            $date    = date('Y') . '-' . date('m') . '-01';
        }
        $code = explode("\n", $code);

        if (count($code) == 1 && $code[0] == '') {
            echo 'No Input Data';
            exit;
        }
        $db->beginTransaction();
    try {
        $string_code = "'" .implode("','",$code). "'" ;
		$str_where = preg_replace('/\s+/','',$string_code);

        $sql1 = "INSERT INTO lock_insurance_passby (`staff_id`, `lock_type`, `created_at`, `created_by`, `lock_time`) select s.id, 1, NOW(), $created_by, '$date' 
                        FROM  staff s
                        WHERE s.`code` IN ($str_where)";
        $stmt1 = $db->prepare($sql1);
        $stmt1->execute();
        $stmt1->closeCursor();
        $stmt1 = null;
        
        $sql = "UPDATE lock_insurance li 
                       INNER JOIN staff s ON s.id = li.staff_id AND li.lock_type = 1 AND lock_time = '$date'
                       SET li.`not_link_to_salary` = 1
                       WHERE s.`code` IN ($str_where)";
       
        $staff_insurance = $db->prepare($sql);
        $staff_insurance->execute();
        
        $db->commit();    
        $flashMessenger->setNamespace('success')->addMessage('Thành công.');
        $this->_redirect(HOST . 'insure/passby-payable');
        
    } catch (Exception $ex) {
         $db->rollback();
        $this->view->errors = $e->getMessage();

        return;
    }
       

 }
 
}
 $flashMessenger = $this->_helper->flashMessenger;
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->view->messages_error = $messages_error;
