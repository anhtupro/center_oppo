<?php 
    $QHospital          = new Application_Model_Hospital();
    $export      	= $this->getRequest()->getParam('export');
    $hospital_name 	= $this->getRequest()->getParam('hospital_name');
    $area_id            = $this->getRequest()->getParam('area_id');
    $province 		= $this->getRequest()->getParam('province');
    $province_code 	= $this->getRequest()->getParam('province_code');
    $code 		= $this->getRequest()->getParam('code');
    $page               = $this->getRequest()->getParam('page',1);
    
    $db = Zend_Registry::get('db');
    $params  = array(
                    'hospital_name' 	=> $hospital_name,
                    'area_id'      	=> $area_id,
                    'province'     	=> $province,
                    'code'              => $code,
                    'province_code'     => $province_code,
                    
            );
    
    $total   = 0;
    $limit   = LIMITATION;
    
    if($export){
        $list = $QHospital->fetchPagination($page, NULL, $total, $params);
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
		
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
			'HOSPITAL ID',
            'HOSPITAL NAME',
            'HOSPITAL CODE',
            'PROVINCE NAME',
            'PROVINCE CODE',
            'NOTE',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key){
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $intCount = 1;   
        $index = 2;
        $stt = 1;
        try{
            if ($list)
                foreach ($list as $_key => $_order){
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
					
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['id'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
						
					$sheet->getCell($alpha++ . $index)->setValueExplicit($_order['hospital_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);   
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['province_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['province_code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);   
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['note'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
                }
        }
        catch (exception $e)
        {
            exit;
        }

        $filename = 'Hospital_list_' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }else{
        $list = $QHospital->fetchPagination($page, $limit, $total, $params);
    }
    
    $flashMessenger             = $this->_helper->flashMessenger;
    $messages                   = $flashMessenger->setNamespace('success')->getMessages();
    $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages       = $messages;
    $this->view->messages_error = $messages_error;
    $this->view->params     = $params;
    $this->view->limit      = $limit;
    $this->view->list       = $list;
    $this->view->total      = $total;
    $this->view->url        = HOST . 'insurance/list-hospital' . ($params ? '?'.http_build_query($params).'&' : '?');
    $this->view->offset     = $limit * ($page - 1);
    $QProvince              = new Application_Model_Province();
    $provinces              = $QProvince->get_all();
    $this->view->provinces  = $provinces;