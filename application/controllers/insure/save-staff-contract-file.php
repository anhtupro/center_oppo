<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;
$QStaffContractFile = new Application_Model_StaffContractFile();

$staff_contract_id = $this->getRequest()->getParam('staff_contract_id');
$file= $_FILES['contract_file'];
foreach($file['name'] as $key=> $value) {
    if ($file['size'][$key] != 0) {
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'staff_contract' .
            DIRECTORY_SEPARATOR;
        $upload = new Zend_File_Transfer();
        $upload->setOptions(array('ignoreNoFile' => true));
        $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
        $upload->addValidator('Size', false, array('max' => '4MB'));
        $upload->addValidator('ExcludeExtension', false, 'php,sh');


        $name_file_k ='contract_file_'.$key.'_' ;

        $files = $upload->getFileInfo($name_file_k);
        $data_file = array();

        if (isset($files[$name_file_k]) and $files[$name_file_k]) {
            $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);
            $upload->setDestination($uploaded_dir);

            //Rename
            $old_name = $fileInfo['name'];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode); // đuôi file
            $new_name = 'Staff_contract_' . date('Ymd') . substr(microtime(), 2, 4) . md5(uniqid('', true)) . '.' . $extension;
            $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
            $r = $upload->receive(array($name_file_k));

            if ($r) {
                $data_file['file_name'] = $new_name;
            } else {
                $messages = $upload->getMessages();
                foreach ($messages as $msg) {
                    throw new Exception(" ERROR: " . $msg . " !!");
                }
            }
        }
        
        require_once "Aws_s3.php";
        $s3_lib = new Aws_s3();
        $file1 = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name ; 
        $s3_lib->uploadS3($file1, "files/staff_contract/", $new_name);
        
        $params  = array(
            'staff_contract_id' 	=> $staff_contract_id,
            'name'          	=> $new_name,
            'url'          	=> '/files/staff_contract/'.$new_name,
            'created_at'        =>date('Y-m-d H:i:s'),
            'created_by'        => $userStorage->id
        );
        
        $result = $QStaffContractFile->save($params);
    }


}
    if($result['code'] <= 0){
        $flashMessenger->setNamespace('error')->addMessage($result['message']);
    }else{
        $flashMessenger->setNamespace('success')->addMessage($result['message']);
    }
    $this->_redirect('/insure/create-staff-contract-file?staff_contract_id='.$staff_contract_id);



