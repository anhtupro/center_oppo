<?php 
$flashMessenger = $this->_helper->flashMessenger;
$QInsurance     = new Application_Model_Insurance();
$delete         = $this->getRequest()->getParam('delete');
$lock           = $this->getRequest()->getParam('lock');
$tang           = $this->getRequest()->getParam('tang');
$giam           = $this->getRequest()->getParam('giam');
$dieuchinh      = $this->getRequest()->getParam('dieuchinh');
$chedo          = $this->getRequest()->getParam('chedo');
$sauchedo       = $this->getRequest()->getParam('sauchedo');
$change       = $this->getRequest()->getParam('change');

$month          = $this->getRequest()->getParam('month',date('m/Y'));
$unit_code_id   = $this->getRequest()->getParam('unit_code_id');
$staff_ids      = $this->getRequest()->getParam('staff_ids');
$leave_ids      = $this->getRequest()->getParam('leave_ids');


$ids            = $this->getRequest()->getParam('ids');
$qdnv           = $this->getRequest()->getParam('qdnv');
$back_url       = $this->getRequest()->getParam('back_url','/insure/report-list');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$db             = Zend_Registry::get('db');
$QInsuranceInfo = new Application_Model_InsuranceInfo();
$QInsuranceStaff = new Application_Model_InsuranceStaff();

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

    if( isset($month) AND $month ){
        $date = My_Date::normal_to_mysql($month);
    }else{
        $flashMessenger->setNamespace('error')->addMessage("Please enter month to export!");
    }
    
    $paramsStore = array(
        $date,
        intval($unit_code_id),
        NULL,
        '',
        ''
    );
    
    $date_ins = date('Y-m-d');
    $datetime = date('Y-m-d H:i:s');
    
    if(!empty($lock) AND !empty($tang)){
        
        $stmt_inCreateInfo = $db->prepare('CALL PR_Increate_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
        $stmt_inCreateInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
        $stmt_inCreateInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
        
        
        $stmt_inCreateInfo->execute();
        $inCreateInfo = $stmt_inCreateInfo->fetchAll();
        $stmt_inCreateInfo->closeCursor();
        
    
        $db->beginTransaction();
        try{
            foreach($inCreateInfo as $item){
                $data       = array(
                    'staff_id'      => $item['staff_id'],
                    'type'          => $item['type'],
                    'option'        => $item['option'],
                    'time_effective'      => $item['time_effective'],
                    'time'          => $item['r'],
                    'time_ins'      => $date_ins,
                    'time_alter'    => $item['time_alter'],
                    'note'          => 'TM_note_new',
                    'created_at'    => $datetime,
                    'created_by'    => $userStorage->id,
                    'company_id'    => $item['company_id'],
                    'unit_code_id'  => $item['unit_code_id'],
                    'rate'          => $item['INS_RATE'],
                    'department'    => $item['department'],
                    'team'          => $item['team'],
                    'title'         => $item['title'],
                    'del'           => 0,
                    'locked'        => 1,
                    'salary'        => $item['salary'],
                    'old_salary'    => $item['old_salary'],
                    'regional_market'  => $item['regional_market'],
                    'district'      => $item['district_id'],
                    'locked_at'     => $datetime,
                    'locked_by'     => $userStorage->id,
                    'time_created_at'  => $datetime,
                    'money_out'     => $item['salary'],
                    'system_note'   => 'system auto generate'
                );
                $QInsuranceStaff->insert($data);
                
            }

            $selectStaff = $db->select()
            ->from(array('a'=>'insurance_staff'),array('staff_id'))
            ->where('staff_id IN (?)',$staff_ids)
            ->group('staff_id');
            $listStaff = $db->fetchAll($selectStaff);

            foreach($listStaff as $item_row){
                $QInsuranceInfo->add($item_row['staff_id']);
            }

            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch (Exception $e){
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $db->rollBack();
        }
    }elseif(!empty($lock) AND !empty($giam)){
        
        $stmt_deCreateInfo = $db->prepare('CALL PR_Decreate_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
        $stmt_deCreateInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
        $stmt_deCreateInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
        
        
        $stmt_deCreateInfo->execute();
        $deCreateInfo = $stmt_deCreateInfo->fetchAll();
        $stmt_deCreateInfo->closeCursor();
        
        foreach($deCreateInfo as $item_de){
            $data_de       = array(
                'staff_id'      => $item_de['staff_id'],
                'type'          => $item_de['type'],
                'option'        => $item_de['option'],
                'time_effective'=> $item_de['time_effective'],
                'time'          => $item_de['time_ins'],
                'time_ins'      => $date_ins,
                'time_alter'    => $item_de['time_alter'],
                'note'          => 'GH_note_new',
                'qdnv'          => $item_de['qdnv'],
                'created_at'    => $datetime,
                'created_by'    => $userStorage->id,
                'company_id'    => $item_de['company_id'],
                'unit_code_id'  => $item_de['unit_code_id'],
                'rate'          => $item_de['INS_RATE'],
                'department'    => $item_de['department'],
                'team'          => $item_de['team'],
                'title'         => $item_de['title'],
                'del'           => 0,
                'locked'        => 1,
                'salary'        => $item_de['salary'],
                'old_salary'    => $item_de['old_salary'],
                'regional_market'  => $item_de['regional_market'],
                'district'      => $item['district_id'],
                'locked_at'     => $datetime,
                'locked_by'     => $userStorage->id,
                'time_created_at'  => $datetime,
                'money_in'     => $item_de['salary'],
                'system_note'   => 'system auto generate'
            );
            if(!empty($item_de['qdnv'])){
                $QInsuranceStaff->insert($data_de);
            }
        }
    }elseif (!empty($lock) AND !empty($dieuchinh)){
        //Adjust
        $stmt_adjustInfo = $db->prepare('CALL PR_Adjust_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
        $stmt_adjustInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
        $stmt_adjustInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
        
        $stmt_adjustInfo->execute();
        $adjust_info = $stmt_adjustInfo->fetchAll();
        $stmt_adjustInfo->closeCursor();
        
     
            foreach($adjust_info as $item_adj){
            $data_adj       = array(
                'staff_id'      => $item_adj['staff_id'],
                'type'          => $item_adj['type'],
                'option'        => $item_adj['option'],
                'time_effective'=> $item_adj['time_effective'],
                'time'          => $item_adj['time_ins'],
                'time_ins'      => $date_ins,
                'time_alter'    => $item_adj['time_alter'],
                'note'          => 'DC_note_new',
                'created_at'    => $datetime,
                'created_by'    => $userStorage->id,
                'company_id'    => $item_adj['company_id'],
                'unit_code_id'  => $item_adj['unit_code_id'],
                'rate'          => $item_adj['INS_RATE'],
                'department'    => $item_adj['department'],
                'team'          => $item_adj['team'],
                'title'         => $item_adj['title'],
                'del'           => 0,
                'locked'        => 1,
                'salary'        => $item_adj['salary'],
                'old_salary'    => $item_adj['old_salary'],
                'regional_market'  => $item_adj['regional_market'],
                'district'      => $item['district_id'],
                'locked_at'     => $datetime,
                'locked_by'     => $userStorage->id,
                'time_created_at'  => $datetime,
                'money_out'     => $item_adj['salary']
            );
       
            if($item_adj['salary'] > 0){
                $QInsuranceStaff->insert($data_adj);

            }
        }
   }elseif (!empty($lock) AND !empty($chedo) AND !empty($leave_ids)){
       //Chedo
       $stmt_ChedoInfo = $db->prepare('CALL PR_Special_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pLeaveId) ');
       $stmt_ChedoInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
       $stmt_ChedoInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
       $stmt_ChedoInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
       $stmt_ChedoInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
       $stmt_ChedoInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
       $stmt_ChedoInfo->bindParam('pLeaveId', $leave_ids, PDO::PARAM_STR);
       
       $stmt_ChedoInfo->execute();
       $chedo_info = $stmt_ChedoInfo->fetchAll();
       $stmt_ChedoInfo->closeCursor();
        
               foreach($chedo_info as $chedo){
                $data_chedo       = array(
                'staff_id'      => $chedo['staff_id'],
                'type'          => $chedo['type'],
                'option'        => $chedo['option'],
                'time_effective'=> $chedo['time_effective'],
                'time'          => $chedo['time_ins'],
                'time_ins'      => $date_ins,
                'time_alter'    => $chedo['time_alter'],
                'note'          => 'CD_note_new',
                'created_at'    => $datetime,
                'created_by'    => $userStorage->id,
                'company_id'    => $chedo['company_id'],
                'unit_code_id'  => $chedo['unit_code_id'],
                'rate'          => $chedo['INS_RATE'],
                'department'    => $chedo['department'],
                'team'          => $chedo['team'],
                'title'         => $chedo['title'],
                'del'           => 0,
                'locked'        => 1,
                'salary'        => $chedo['salary'],
                'old_salary'    => $chedo['old_salary'],
                'regional_market' => $chedo['regional_market'],
                'district'      => $item['district_id'],
                'locked_at'     => $datetime,
                'locked_by'     => $userStorage->id,
                'time_created_at'  => $datetime,
                'money_out'     => $chedo['salary'],
                'system_note'   => 'system auto generate'
            );
                
                $QInsuranceStaff->insert($data_chedo);
                
        }
   }elseif (!empty($lock) AND !empty($sauchedo) AND !empty($leave_ids)){
       $stmt_SauChedoInfo = $db->prepare('CALL PR_After_Leave_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pLeaveId) ');
       $stmt_SauChedoInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
       $stmt_SauChedoInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pLeaveId', $leave_ids, PDO::PARAM_STR);
       
       $stmt_SauChedoInfo->execute();
       $sauchedo_info = $stmt_SauChedoInfo->fetchAll();
       $stmt_SauChedoInfo->closeCursor();
       
       foreach($sauchedo_info as $chedo_after){
           $data_chedo_after   = array(
               'staff_id'      => $chedo_after['staff_id'],
               'type'          => $chedo_after['type'],
               'option'        => $chedo_after['option'],
               'time_effective'=> $chedo_after['time_effective'],
               'time'          => $chedo_after['time_ins'],
               'time_ins'      => $date_ins,
               'time_alter'    => $chedo_after['time_alter'],
               'note'          => 'ON_note_new',
               'created_at'    => $datetime,
               'created_by'    => $userStorage->id,
               'company_id'    => $chedo_after['company_id'],
               'unit_code_id'  => $chedo_after['unit_code_id'],
               'rate'          => $chedo_after['INS_RATE'],
               'department'    => $chedo_after['department'],
               'team'          => $chedo_after['team'],
               'title'         => $chedo_after['title'],
               'del'           => 0,
               'locked'        => 1,
               'salary'        => $chedo_after['salary'],
               'old_salary'    => $chedo_after['old_salary'],
               'regional_market' => $chedo_after['regional_market'],
               'district'      => $item['district_id'],
               'locked_at'     => $datetime,
               'locked_by'     => $userStorage->id,
               'time_created_at'  => $datetime,
               'money_out'     => $chedo_after['salary'],
               'system_note'   => 'system auto generate'
               
           );
           
           $QInsuranceStaff->insert($data_chedo_after);
       
        }
   }elseif (!empty($lock) AND !empty($change) AND !empty($staff_ids)){
       
       $stmt_ChangeInfo = $db->prepare('CALL PR_Change_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
       $stmt_ChangeInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
       $stmt_ChangeInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
       $stmt_ChangeInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
       $stmt_ChangeInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
       $stmt_ChangeInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
       $stmt_ChangeInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
        
       $stmt_ChangeInfo->execute();
       $change_info = $stmt_ChangeInfo->fetchAll();
       $stmt_ChangeInfo->closeCursor();
       
       foreach($change_info as $change){
           $data   = array(
               'staff_id'      => $change['staff_id'],
               'type'          => $change['type'],
               'option'        => $change['option'],
               'time_effective'=> $change['time_effective'],
               'time'          => $change['time_ins'],
               'time_ins'      => $date_ins,
               'time_alter'    => $change['time_alter'],
               'note'          => 'Change_note_new',
               'created_at'    => $datetime,
               'created_by'    => $userStorage->id,
               'company_id'    => $change['company_id'],
               'unit_code_id'  => $change['unit_code_id'],
               'rate'          => $change['INS_RATE'],
               'department'    => $change['department'],
               'team'          => $change['team'],
               'title'         => $change['title'],
               'del'           => 0,
               'locked'        => 1,
               'salary'        => $change['salary'],
               'old_salary'    => $change['old_salary'],
               'regional_market' => $change['regional_market'],
               'district'      => $item['district_id'],
               'locked_at'     => $datetime,
               'locked_by'     => $userStorage->id,
               'time_created_at'  => $datetime,
               'from_date'     => $change['time_effective'],
               'to_date'       => $change['r1'],
               'system_note'   => 'system auto generate'
           );
            
           $QInsuranceStaff->insert($data);
            
       }
       
   }
// }else{
//     $flashMessenger->setNamespace('error')->addMessage('Please select item to process');
//     $this->_redirect($back_url);
// }
// $flashMessenger->setNamespace('error')->addMessage('Error');
// $this->_redirect($back_url);
