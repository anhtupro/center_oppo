<?php 
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$leave_id = $this->getRequest()->getParam('leave_id',NULL);
	$passby_leave_on = $this->getRequest()->getParam('passby_leave_on',NULL);
	$QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
	$QLeaveDetail = new Application_Model_LeaveDetail();
	$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
	
	if(!empty($leave_id) AND !empty($passby_leave_on)){
	    $leave_detail  = $QLeaveDetail->find($leave_id)->current()->toArray();
	    $datetime = date('Y-m-d H:i:s');
	    $data       = array(
	        'staff_id'      => $leave_detail['staff_id'],
	        'type'          => 1,
	        'option'        => 16,
	        'time_effective'=> $leave_detail['to_date'],
	        'time_alter'    => $leave_detail['to_date'],
	        'note'          => 'Disable_new',
	        'created_at'    => $datetime,
	        'created_by'    => $userStorage->id,
	        'passby'        => 1,
	        'to_date'       => $leave_detail['to_date'],
	        'leave_id'      => $leave_id,
	        'leave_type'    =>  $leave_detail['leave_type'],
	        'system_note'   => 'system auto generate'
	    );
	    
	    $QInsuranceStaffBasic->insert($data);
	     
	}else{
	    if(!empty($leave_id)){
	        $where = $QLeaveDetail->getAdapter()->quoteInto('id = ?', $leave_id);
	        $data = array(
	            'insurance_passby' => 1
	        );
	        $QLeaveDetail->update($data, $where);
	    }
	}
	
