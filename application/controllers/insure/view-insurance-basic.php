<?php 
$db = Zend_Registry::get('db');
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$sql = "SELECT 
st.id as staff_id,
					st.`code` AS 'staff_code',
					CONCAT(st.firstname, ' ', st.lastname) AS 'staff_name',
					CONCAT( TRIM(t.name_vn),' - ',TRIM(p1.name),' - ',TRIM(ai.`name`) ) job_title,
					st.insurance_number,
					st.dob,
					CASE WHEN st.gender = 0 THEN 'x' ELSE NULL END gender,
					st.company_id as 'unit_code_id',
					uc.unit_code AS 'unit_code_name',	
					LPAD(p2.`code`,2,0) MaTinh,
					LPAD(h1.`code`,3,0) MaBV,
					ni.code_insurance MaDT,
					st.ID_number,
					DATE_FORMAT(st.ID_date,'%d/%m/%Y') as 'ID_date',
					LPAD(pr.`code`,2,'0') AS ID_place,
					h.ward AS 'ward1', h.district_name AS 'district_name1', h.city_name AS 'city_name1',
					i.address as 'address3', i.ward AS 'ward3', i.district_name AS 'district_name3', i.city_name AS 'city_name3',
					k.address AS 'address2', k.ward AS 'ward2', k.district_name AS 'district_name2', k.city_name AS 'city_name2',
					st.phone_number, st.email,
					ai.code as 'area_nationality_code',
					sr.full_name,
					sr.relative_type,
					st.department,
					st.team,
					st.title,
					st.regional_market,
					g.area_national_id
					
	FROM staff st 
	INNER JOIN team t ON st.title = t.id
	LEFT JOIN staff_relative sr ON st.id = sr.staff_id
	LEFT JOIN nationality_ins ni ON st.nationality = ni.map
	LEFT JOIN v_staff_address h ON st.id = h.staff_id AND h.address_type = 1
  LEFT JOIN v_staff_address i ON st.id = i.staff_id AND i.address_type = 3
  LEFT JOIN v_staff_address k ON st.id = k.staff_id AND k.address_type = 2
	LEFT JOIN province pr ON pr.id = st.id_place_province
	INNER JOIN regional_market f ON f.id = st.regional_market
	INNER JOIN province g ON g.id = f.province_id
  INNER JOIN regional_market r ON r.id = st.regional_market
  INNER JOIN province p1 ON p1.id = r.province_id
	LEFT JOIN area_nationality ai ON ai.id = p1.area_national_id
	LEFT JOIN unit_code uc ON uc.id = st.unit_code_id
	LEFT JOIN hospital h1 ON h1.id = st.hospital_id 
  LEFT JOIN province p2 ON h1.province_id = p2.id
  WHERE st.`status` <> 0 
  GROUP BY st.id
  LIMIT 0,30  
    ";

$stmt = $db->prepare($sql);
$stmt->execute();
$data = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = null;
echo "<pre>";
print_r($data);
echo "</pre>";
exit();
set_time_limit(0);
ini_set('memory_limit', '5120M');

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();

require_once 'PHPExcel.php';

$alphaExcel = new My_AlphaExcel();

$PHPExcel = new PHPExcel();
$heads = array(
    $alphaExcel->ShowAndUp() => 'Công ty',
    $alphaExcel->ShowAndUp() => 'Code',
    $alphaExcel->ShowAndUp() => 'Họ tên',
    $alphaExcel->ShowAndUp() => 'Số sổ',
    $alphaExcel->ShowAndUp() => 'Ngày sinh',
    $alphaExcel->ShowAndUp() => 'Nữ (X)',
    $alphaExcel->ShowAndUp() => 'Chức danh',
    $alphaExcel->ShowAndUp() => 'Tiền đồng',
    $alphaExcel->ShowAndUp() => 'Ngày hiệu lực',
    $alphaExcel->ShowAndUp() => 'Nhập ký hiệu loại Tăng',
    $alphaExcel->ShowAndUp() => 'Số hợp đồng',
    $alphaExcel->ShowAndUp() => 'Diễn giải loại hợp đồng',
    $alphaExcel->ShowAndUp() => 'Ngày ký HĐ/QĐ',
    $alphaExcel->ShowAndUp() => 'Tỷ lệ đóng(%)',
    $alphaExcel->ShowAndUp() => 'Đối tượng',
    $alphaExcel->ShowAndUp() => 'Mã tỉnh',
    $alphaExcel->ShowAndUp() => 'Mã bệnh viện',
    $alphaExcel->ShowAndUp() => 'Mã dân tộc',
    $alphaExcel->ShowAndUp() => 'Số CMND',
    $alphaExcel->ShowAndUp() => 'Ngày cấp',
    $alphaExcel->ShowAndUp() => 'Mã tỉnh',
    $alphaExcel->ShowAndUp() => 'Mã Xã/phường',
    $alphaExcel->ShowAndUp() => 'Mã Quận/huyện',
    $alphaExcel->ShowAndUp() => 'Mã Tỉnh/TP',
    $alphaExcel->ShowAndUp() => 'Số nhà, đường phố, thôn xóm',
    $alphaExcel->ShowAndUp() => 'Mã Xã/phường',
    $alphaExcel->ShowAndUp() => 'Mã Quận/huyện',
    $alphaExcel->ShowAndUp() => 'Mã Tỉnh/TP',
    $alphaExcel->ShowAndUp() => 'Số nhà, đường phố, thôn xóm',
    $alphaExcel->ShowAndUp() => 'Mã Xã/phường',
    $alphaExcel->ShowAndUp() => 'Mã Quận/huyện',
    $alphaExcel->ShowAndUp() => 'Mã Tỉnh/TP',
    $alphaExcel->ShowAndUp() => 'Số điện thoại liên hệ',
    $alphaExcel->ShowAndUp() => 'Email',
    $alphaExcel->ShowAndUp() => 'Mã vùng',
    $alphaExcel->ShowAndUp() => 'Chủ hộ',
    $alphaExcel->ShowAndUp() => 'Relative ',
);

$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

foreach($heads as $key => $value)
{
    $sheet->setCellValue($key.'1', $value);
}

$index = 1;
foreach($data as $key => $value)
{
    $alphaExcel = new My_AlphaExcel();
    $kyhieu = '';
    $option = $value['option'];
    if(in_array($option, array(1,2))){
        $kyhieu = 'TM';
    }elseif (in_array($option, array(5,6) )){
        $kyhieu = 'DC';
    }
    
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['unit_code_name']);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
    
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['insurance_number'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dob'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['gender'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['job_title'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['salary'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['time_effective'])?date('d/m/Y', strtotime($value['time_effective'])):'');
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $kyhieu ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['ContractCode'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['ContractTerm'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['time_effective'])?date('d/m/Y', strtotime($value['time_effective'])):'');
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '32' ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), '1' ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['MaTinh'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['MaBV'],PHPExcel_Cell_DataType::TYPE_STRING);
    
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['MaDT'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ID_number'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['ID_date'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ID_place'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ward1'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['district_name1'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['city_name1'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['address3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ward3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['district_name3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['city_name3'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['address2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['ward2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['district_name2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['city_name2'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['phone_number'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['email'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['area_nationality_code'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['full_name'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), My_Staff_Relative::get($value['relative_type']),PHPExcel_Cell_DataType::TYPE_STRING);
    
    
    
    $index++;

}
$filename = 'Tăng - ' . date('Y-m-d H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit();




