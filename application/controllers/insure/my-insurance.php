<?php 
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$staff_id = $userStorage->id;
    $db = Zend_Registry::get('db');
    $cols = array(
        'a.*',
        'title_name'      => 'CONCAT(b.name_vn," - ",h.name," - ",k.name)',
        'team_name'       => 'c.name',
        'department_name' => 'd.name',
        'regional_name'   => 'e.name',
        'area_name'       => 'f.name',
        'gender_name'     => 'IF(i.gender = 1,"NAM","NỮ")',
        'status_name'     => '(CASE WHEN a.status = 0 THEN "OFF" WHEN a.status = 1 THEN "ON" WHEN a.status = 2 THEN "TEMP OFF" ELSE "CHILDBEARING" END)',
        'id_number_ins'   => 'IFNULL(i.ID_number,a.ID_number)',
        'dob_ins'         => 'IFNULL(i.dob,a.dob)',
        'firstname_ins'   => 'IFNULL(i.firstname,a.firstname)',
        'lastname_ins'    => 'IFNULL(i.lastname,a.lastname)',
    );
    $select = $db->select()
        ->from(array('a'=>'staff'),$cols)
        ->joinLeft(array('ho'=>'hospital'),'ho.id = a.hospital_id',array('id_hospital'=>'ho.id','code_hospital'=>'ho.code','name_hospital'=>'ho.name','province_name_hospital'=>'ho.province_name','del_hospital'=>'ho.del'))
        ->joinLeft(array('i'=>'insurance_info'),'i.staff_id = a.id',array())
        ->join(array('b'=>'team'),'a.title = b.id',array())
        ->join(array('c'=>'team'),'b.parent_id = c.id',array())
        ->join(array('d'=>'team'),'c.parent_id = d.id',array())
        ->join(array('e'=>'regional_market'),'e.id = a.regional_market',array())
        ->join(array('f'=>'area'),'f.id = e.area_id',array())
        ->joinLeft(array('h'=>'province'),'e.province_id = h.id',array())
        ->joinLeft(array('k'=>'area_nationality'),'k.id = h.area_national_id',array())
        ->where('a.id = ?',$staff_id)
        ->group('a.id')
        ;
    $staff = $db->fetchRow($select);
    $this->view->staff = $staff;
    
    $QUnitCode = new Application_Model_UnitCode();
    $unit_codes = $QUnitCode->get_cache();
    $this->view->unit_codes = $unit_codes;


    $selectIncrease = $db->select()
        ->from(array('a'=>'pre_status'),array('a.*'))
        ->where('`option` = ?',1)
        ->where('a.staff_id = ?',$staff_id)
        ->order('a.time_effective DESC')
        ->limit(1)
    ;
    $increase = $db->fetchRow($selectIncrease);
    $this->view->increase = $increase;

    $increase_text = '';
    if(isset($increase['time_alter']) AND $increase['time_alter']){
        $date_increase = date_create($increase['time_alter']);
        if( intval( date('d',strtotime($increase['time_alter']) ) ) > 15 ){
            $date_increase->add(new DateInterval('P20D'));
        }
        $increase_text = $date_increase->format('m/Y');
    }

    if($increase){
        $selectDecrease = $db->select()
            ->from(array('a'=>'pre_status'),array('a.*'))
            ->where('a.`option` = ?',3)
            ->where('a.staff_id = ?',$staff_id)
            ->where('a.time_alter >= ?',$increase['time_alter'])
            ->order('a.time_alter DESC')
            ->limit(1)
        ;
        $decrease  = $db->fetchRow($selectDecrease);
        $this->view->decrease = $decrease;

        $decrease_text = '';
        if(isset($decrease['time_alter']) AND $decrease['time_alter']){
            $date_decrease = date_create($decrease['time_alter']);
            if( intval( date('d',strtotime($decrease['time_alter']) ) ) > 15 ){
                $date_decrease->add(new DateInterval('P1M'));
            }
            $decrease_text = $date_decrease->format('m/Y');
        }
    }

        $select_salary = $db->select()
        ->from(array('a'=>'staff_salary'),array('*', 'salary'=>'insurance_salary'))
        ->where('staff_id = ? AND to_date IS NULL',$staff_id)
        ->order('id DESC')
        ->limit(1);
    $this->view->current_salary = $db->fetchRow($select_salary);

    $this->view->decrease_text = $decrease_text;
    $this->view->increase_text = $increase_text;

    $QProvince = new Application_Model_Province();
    $provinces = $QProvince->get_all2();
    $this->view->provinces = $provinces;

    $QHospital = new Application_Model_Hospital();
    $provinceHospital = 0;
    if(isset($staff['hospital_id']) AND $staff['hospital_id']){
        $rowHospital = $QHospital->find($staff['hospital_id'])->current();
        $provinceHospital = $rowHospital['province_id'];
    }
    $currentHospitals = $QHospital->get_all(array('province_id'=> $provinceHospital));
    $this->view->currentHospitals = $currentHospitals;
    $this->view->provinceHospital = $provinceHospital;

    $flashMessenger = $this->_helper->flashMessenger;
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
    $this->view->messages_success = $messages_success;

    $select_qdnv = $db->select()
        ->from(array('a'=>'pre_status'),array('a.*'))
        ->where('staff_id = ?',$staff_id)
        ->where('`option` = ?',3)
        ->order('time_alter DESC')
        ->limit(1)
    ;
    $qdnv = $db->fetchRow($select_qdnv);
    $this->view->qdnv = $qdnv;
     
    $QInsuranceStaff  = new Application_Model_InsuranceStaff();
    $list             = $QInsuranceStaff->showHistoryStaff($staff_id);
    
    
    $this->view->list = $list;
    
