<?php 
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        
        $month            = $this->getRequest()->getParam('month',date('m/Y'));
        $lock             = $this->getRequest()->getParam('lock');
        $params = array(
            'month'         => $month,
        );
         
         if( isset($params['month']) AND $params['month'] ){
             $date = My_Date::normal_to_mysql($params['month']);
         }else{
             $date    = date('Y') . '-' . date('m') . '-01';
         }

         $db             = Zend_Registry::get('db');
         $QLockInsurance = new Application_Model_LockInsurance();
         $QStaffUnionFunding = new Application_Model_StaffUnionFunding();
       
         $datetime = date('Y-m-d H:i:s');
         $unit_code_id     = $this->getRequest()->getParam('unit_code_id',0);
         $unit_code_id = intval($unit_code_id) ; 
         
         //staff dong cong doan
         $staff_luong_congdoan = $QStaffUnionFunding->getStaffUnionFunding($date);
         //Trich nop
         $stmt = $db->prepare('CALL PR_Insurance_Payable (:r , :pUnitCodeId) ');
         $stmt->bindParam('r', $date, PDO::PARAM_STR);
         $stmt->bindParam('pUnitCodeId', $unit_code_id, PDO::PARAM_INT);
        
         $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
         $created_by     = $userStorage->id;
         
         $stmt->execute();
         $payable = $stmt->fetchAll();
         $stmt->closeCursor();
         $data_lock_decreate = $data_lock_back = $data_lock_able = $data_lock_join_back = array(); 
         if(!empty($payable)){
             $staff_id_arr = [];
             foreach($payable as $item){
                $staff_id_arr[] = $item['staff_id'];
             }
             foreach($payable as $item){
                 $data      = array(
                     'staff_id'     => $item['staff_id'],
                     'lock_type'    => 1,
                     'created_at'   => $datetime,
                     'created_by'   => $created_by,
                     'unit_code_id' => $item['unit_code_id'],
                     'salary'       => $item['salary'],
                     'from_month'   => $item['fromMonth'],
                     'to_month'     => $item['toMonth'],
                     'number_of_month' => $item['number_of_month'],
                     'amount'       => $item['amount'],
                     'system_note'  => 'system auto generate',
                     'lock_time'    => $date,
                     'salary_congdoan' => in_array($item['staff_id'],$staff_luong_congdoan)? (empty($item['salary'])?0:$item['salary']):0
                 );
                 $data_lock_able[] = $data;
             }
             if(!empty($data_lock_able)){
                My_Controller_Action::insertAllrow($data_lock_able, 'lock_insurance');
             }
         }
         
         //tham gia nguoc
         $stmt_join_back = $db->prepare('CALL PR_Insurance_Join_Back (:r , :pUnitCodeId) ');
         $stmt_join_back->bindParam('r', $date, PDO::PARAM_STR);
         $stmt_join_back->bindParam('pUnitCodeId', $unit_code_id, PDO::PARAM_INT); 
         $stmt_join_back->execute();
         $join_back = $stmt_join_back->fetchAll();
         $stmt_join_back->closeCursor();
         
         if(!empty($join_back)){
             $staff_id_arr = [];
             foreach($join_back as $item){
                $staff_id_arr[] = $item['staff_id'];
             }
             foreach($join_back as $item){
                 $data      = array(
                     'staff_id'     => $item['staff_id'],
                     'lock_type'    => 2,
                     'created_at'   => $datetime,
                     'created_by'   => $created_by,
                     'unit_code_id' => $item['unit_code_id'],
                     'salary'       => $item['salary'],
                     'from_month'   => $item['fromMonth'],
                     'to_month'     => $item['toMonth'],
                     'number_of_month' => $item['number_of_month'],
                     'amount'       => $item['amount'],
                     'system_note'  => 'system auto generate',
                     'lock_time'    => $date,
                     'salary_congdoan' => in_array($item['staff_id'],$staff_luong_congdoan)? (empty($item['salary'])?0:$item['salary']):0
                 );
                 $data_lock_join_back[] = $data;
//                 $QLockInsurance->insert($data);
             }
             if(!empty($data_lock_join_back)){
                My_Controller_Action::insertAllrow($data_lock_join_back, 'lock_insurance');
             }
         }
        
         //dieu chinh nguoc
         $stmt_adjust_back = $db->prepare('CALL PR_Insurance_Adjust_Back (:r, :pUnitCodeId) ');
         $stmt_adjust_back->bindParam('pUnitCodeId', $unit_code_id, PDO::PARAM_STR); 
         $stmt_adjust_back->bindParam('r', $date, PDO::PARAM_STR);
         
         $stmt_adjust_back->execute();
         $adjust_back = $stmt_adjust_back->fetchAll();
         $stmt_adjust_back->closeCursor();
         
         if(!empty($adjust_back)){
             $staff_id_arr = [];
             foreach($adjust_back as $item){
                $staff_id_arr[] = $item['staff_id'];
             }
             foreach($adjust_back as $item){
                 $data      = array(
                     'staff_id'     => $item['staff_id'],
                     'lock_type'    => 3,
                     'created_at'   => $datetime,
                     'created_by'   => $created_by,
                     'unit_code_id' => $item['unit_code_id'],
                     'salary'       => $item['salary'],
                     'old_salary'   => $item['old_salary'],
                     'from_month'   => $item['fromMonth'],
                     'to_month'     => $item['toMonth'],
                     'number_of_month' => $item['number_of_month'],
                     'amount'       => $item['amount'],
                     'system_note'  => 'system auto generate',
                     'lock_time'    => $date,
                     'salary_congdoan' => in_array($item['staff_id'],$staff_luong_congdoan)? (empty($item['salary'])?0:$item['salary']):0
                 );
                  $data_lock_back[] = $data;
//                 $QLockInsurance->insert($data);
             }
             if(!empty($data_lock_back)){
                My_Controller_Action::insertAllrow($data_lock_back, 'lock_insurance');
             }
         }

         //giam
         $stmt_decreate_back = $db->prepare('CALL PR_Insurance_Decreate_Back (:r, :pUnitCodeId) ');
         $stmt_decreate_back->bindParam('r', $date, PDO::PARAM_STR);
         $stmt_decreate_back->bindParam('pUnitCodeId', $unit_code_id, PDO::PARAM_INT);  
         $stmt_decreate_back->execute();
         $decreate = $stmt_decreate_back->fetchAll();
         $stmt_decreate_back->closeCursor();
        
        if(!empty($decreate)){
            $staff_id_arr = [];
             foreach($decreate as $item){
                $staff_id_arr[] = $item['staff_id'];
             }
             foreach($decreate as $item){
                 $data      = array(
                     'staff_id'     => $item['staff_id'],
                     'lock_type'    => 4,
                     'created_at'   => $datetime,
                     'created_by'   => $created_by,
                     'unit_code_id' => $item['unit_code_id'],
                     'salary'       => $item['salary'],
                     'from_month'   => $item['fromMonth'],
                     'to_month'     => $item['toMonth'],
                     'number_of_month' => $item['number_of_month'],
                     'amount'       => $item['amount'],
                     'system_note'  => 'system auto generate',
                     'lock_time'    => $date,
                     'salary_congdoan' => in_array($item['staff_id'],$staff_luong_congdoan)? (empty($item['salary'])?0:$item['salary']):0
                 );
                 $data_lock_decreate[] = $data;
//                 $QLockInsurance->insert($data);
             }
              if(!empty($data_lock_decreate)){
                My_Controller_Action::insertAllrow($data_lock_decreate, 'lock_insurance');
             }
        }
        
         //return
        $stmt_decreate_return = $db->prepare('CALL PR_Insurance_Decreate_Return (:r, :pUnitCodeId) ');
        $stmt_decreate_return->bindParam('r', $date, PDO::PARAM_STR);
        $stmt_decreate_return->bindParam('pUnitCodeId', $unit_code_id, PDO::PARAM_INT);
        $stmt_decreate_return->execute();
        $recreate = $stmt_decreate_return->fetchAll();
        $stmt_decreate_return->closeCursor();
        
        if(!empty($recreate)){
            $staff_id_arr = [];
             foreach($recreate as $item){
                $staff_id_arr[] = $item['staff_id'];
             }
            foreach($recreate as $item){
                 $data      = array(
                     'staff_id'     => $item['staff_id'],
                     'lock_type'    => 5,
                     'created_at'   => $datetime,
                     'created_by'   => $created_by,
                     'unit_code_id' => $item['unit_code_id'],
                     'salary'       => $item['salary'],
                     'from_month'   => $item['fromMonth'],
                     'to_month'     => $item['toMonth'],
                     'number_of_month' => $item['number_of_month'],
                     'amount'       => $item['amount'],
                     'system_note'  => 'system auto generate',
                     'lock_time'    => $date,
                     'salary_congdoan' => in_array($item['staff_id'],$staff_luong_congdoan)? (empty($item['salary'])?0:$item['salary']):0
                 );
                 $data_lock_recreate[] = $data;
             }
             if(!empty($data_lock_recreate)){
                My_Controller_Action::insertAllrow($data_lock_recreate, 'lock_insurance');
             }
        } 
         
