<?php 
    $userStorage       = Zend_Auth::getInstance()->getStorage()->read();
    $back_url          = $this->getRequest()->getParam('back_url');
    $db                = Zend_Registry::get('db');
    $export            = $this->getRequest()->getParam('export');
    $name              = $this->getRequest()->getParam('name', '');
    $code              = $this->getRequest()->getParam('code', '');
    $office            = $this->getRequest()->getParam('office', -1);
    $signed_to         = $this->getRequest()->getParam('signed_to', date('d/m/Y', strtotime(date('Y-m-t') . "+5 days")));
    $area_id           = $this->getRequest()->getParam('area_id', NULL);
    $department        = $this->getRequest()->getParam('department', NULL);
    $team              = $this->getRequest()->getParam('team', NULL);
    $title             = $this->getRequest()->getParam('title', NULL);
    $off               = $this->getRequest()->getParam('off', 0);
    $contract_term     = $this->getRequest()->getParam('contract_term', NULL);
    $company_id        = $this->getRequest()->getParam('company_id', NULL);
    $btnSetPrintStatus = $this->getRequest()->getParam('btnSetPrintStatus');
    $ids               = $this->getRequest()->getParam('id');
    $month             = $this->getRequest()->getParam('confirm_date');
    $btnSetDisable     = $this->getRequest()->getParam('btnSetDisable');
    $update_contract   = $this->getRequest()->getParam('update_contract');
    
    if ($signed_to != '') {
        if ($signed_to == date('t/m/Y')) {
            $date_tmp  = My_Date::normal_to_mysql($signed_to);
            $date_ex   = date_create($date_tmp);
            date_add($date_ex, date_interval_create_from_date_string("5 days"));
            $signed_to = date_format($date_ex, "d/m/Y");
        }
    }
    if ($signed_from != '') {
        if ($signed_from == date('01/m/Y')) {
            $date_tmp    = My_Date::normal_to_mysql($signed_from);
            $date_ex     = date_create($date_tmp);
            $signed_from = date_format($date_ex, "d/m/Y");
        }
    }
        
    $params = array(
            'name'              => trim($name),
            'code'              => trim($code),
            'signed_to'         => $signed_to,
            'office'            => $office,
            'area_id'           => $area_id,
            'department'        => $department,
            'team'              => $team,
            'title'             => $title,
            'off'               => $off,
            'contract_term'     => $contract_term,
            'company_id'        => $company_id,
            'month'             => $month,
            'ids'               => $ids,
            'btnApproveByStaff' => $btnApproveByStaff,
            'btnConfirm'        => $btnConfirm,
            'confirm_date'      => date('m/Y'),
            'area_id_confirm'   => $area_id_confirm,
            'back_url'          => $back_url,
        );

    $total = 0;
    $limit = $page  = 0;
    $staff_contract_model = new Application_Model_StaffContract();

    $renewable = $staff_contract_model->renewable($page, $limit, $total, $params);
    $this->view->renewable              = $renewable ;
    //In hd de xuat tai ky

    $print_renewable      = $staff_contract_model->renewablePrint($page, $limit, $total, $params);
    $this->view->print_renewable = $print_renewable;
    
    //In hd bat thuong 
    $print_appendix       = $staff_contract_model->AppendixPrint($page, $limit, $total, $params);
    $this->view->print_appendix = $print_appendix;
    
    //In hd moi
    $print_new_contract   = $staff_contract_model->StaffContractNew($page, $limit, $total, $params);
    $this->view->print_new_contract = $print_new_contract;
    
    $limit = 20;
    $total = 0;
    //Lich su
    $history             = $staff_contract_model->history($page, $limit, $total, $params);
    $this->view->history = $history;

    if(!empty($renewable)){
        $active = "#taiky";
    }
    elseif(!empty($print_renewable)){
        $active = "#print_taiky";
    }elseif (!empty($print_appendix)){
        $active = "#batthuong";
    }elseif (!empty($print_new_contract)){
        $active = "#moi";
    }elseif (!empty($history)){
        $active = "#lichsu";
    }
    $this->view->active   = $active;
    $this->view->params   = $params;
    $QContractType             = new Application_Model_ContractTypes();
    $QContractType             = $QContractType->get_cache();
    $this->view->contractTerms = $QContractType;