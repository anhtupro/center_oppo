<?php
$QStaffUnionFunding = new Application_Model_StaffUnionFunding();
$export = $this->getRequest()->getParam('export');
if ($export) {
    $time = date('Y-m-01');
    $list = $QStaffUnionFunding->getDataInsert($time);
    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
        'STT',
        'STAFF CODE'
    );
    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }
    $intCount = 1;
    $index = 2;
    $stt = 1;
    try {
        if ($list)
            foreach ($list as $_key => $_order) {
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order,
                        PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename = 'List_staff_congdoan_' . date('d-m-Y H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}