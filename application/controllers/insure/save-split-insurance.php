<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';

$userStorage           = Zend_Auth::getInstance()->getStorage()->read();
$QPInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
$QLeaveDetail          = new Application_Model_LeaveDetail();

$id       = $this->getRequest()->getParam('id');
$staff_id = $this->getRequest()->getParam('staff_id');

$from_date = $this->getRequest()->getParam('from_date', array());
$to_date   = $this->getRequest()->getParam('to_date', array());
$off_type  = $this->getRequest()->getParam('off_type', array());
$reason    = $this->getRequest()->getParam('reason', array());
$off_type  = $this->getRequest()->getParam('off_type', array());



$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($id) {

    try {

        if (in_array('', $from_date) || in_array('', $to_date) || in_array('', $off_type) || empty($from_date[1]) || empty($to_date[1])) {
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
            echo '<div class="alert alert-error">Lỗi : Dữ liệu truyền vào chưa đầy đủ. Vui lòng kiểm tra lại.</div>';
            exit;
        }

        //chuyển về định dạng để lưu
        $month_obj    = date_create_from_format('d/m/Y', $from_date[0]);
        $from_date[0] = $month_obj->format('Y-m-d');
        $month_obj    = date_create_from_format('d/m/Y', $to_date[0]);
        $to_date[0]   = $month_obj->format('Y-m-d');

        $month_obj    = date_create_from_format('d/m/Y', $from_date[1]);
        $from_date[1] = $month_obj->format('Y-m-d');
        $month_obj    = date_create_from_format('d/m/Y', $to_date[1]);
        $to_date[1]   = $month_obj->format('Y-m-d');


        $where           = $QLeaveDetail->getAdapter()->quoteInto('id = ?', $id);
        $LeaveDetail_Row = $QLeaveDetail->fetchRow($where);

        if (empty($LeaveDetail_Row)) {
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
            echo '<div class="alert alert-error">Lỗi : ID truyền vào không tồn tại.</div>';
            exit;
        } else {

            $where_check    = $QPInsuranceStaffBasic->getAdapter()->quoteInto('leave_id = ?', $id);
            $StaffBasic_Row = $QPInsuranceStaffBasic->fetchRow($where_check);

            if (!empty($StaffBasic_Row)) {
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                echo '<div class="alert alert-error">Lỗi : Phép này đã được tách trước đó. Vui lòng kiểm tra lại.</div>';
                exit;
            }

            if (($from_date[0] > $to_date[0]) || ($to_date[0] >= $from_date[1]) || ($from_date[1] > $to_date[1])) {
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                echo '<div class="alert alert-error">Lỗi : Ngày bạn nhập vào không đúng. Vui lòng kiểm tra lại.</div>';
                exit;
            }

            if ($off_type[0] == 17) {//Thai sản                                    
                $option_0 = 1;
            } elseif ($off_type[0] == 23) {//Nghỉ không Lương                                    
                $option_0 = 10;
            } else {
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                echo '<div class="alert alert-error">Lỗi : Loại phép này ko được tách. Vui lòng kiểm tra lại.</div>';
                exit;
            }

            if ($off_type[1] == 17) {//Thai sản                                    
                $option_1 = 1;
            } elseif ($off_type[1] == 23) {//Nghỉ không Lương                                    
                $option_1 = 10;
            } else {
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                echo '<div class="alert alert-error">Lỗi : Loại phép này ko được tách. Vui lòng kiểm tra lại.</div>';
                exit;
            }

            $data_insert_0 = array(
                'staff_id'       => $LeaveDetail_Row['staff_id'],
                'type'           => 2,
                'option'         => $option_0,
                'time_effective' => $from_date[0],
                'time_alter'     => $from_date[0],
                'to_date'        => $to_date[0],
                'note'           => $reason[0],
                'leave_id'       => $id,
                'leave_type'     => $off_type[0],
                'created_by'     => $userStorage->id,
                'created_at'     => date('Y-m-d H:i:s')
            );
            $data_insert_1 = array(
                'staff_id'       => $LeaveDetail_Row['staff_id'],
                'type'           => 2,
                'option'         => $option_1,
                'time_effective' => $from_date[1],
                'time_alter'     => $from_date[1],
                'to_date'        => $to_date[1],
                'note'           => $reason[1],
                'leave_id'       => $id,
                'leave_type'     => $off_type[1],
                'created_by'     => $userStorage->id,
                'created_at'     => date('Y-m-d H:i:s')
            );

            $r1 = $QPInsuranceStaffBasic->insert($data_insert_0);
            $r2 = $QPInsuranceStaffBasic->insert($data_insert_1);
        }//End else


        if (empty($r1) || empty($r2)) {
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
            echo '<div class="alert alert-error">Lỗi : Insert Không thành công.</div>';
            exit;
        }

        $db->commit();
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
        echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
        echo '<div class="alert alert-success">Done</div>';
        /// load lại trang
        $back_url = " report-list";
        echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
    } catch (Exception $e) {
        $db->rollBack();
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
        echo '<div class="alert alert-error">Lỗi : ' . $e->getMessage() . '</div>';
    }
    exit;
} else {

    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : Thiếu ID truyền vào.</div>';
    exit;
}
