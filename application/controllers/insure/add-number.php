<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$number_of_month    = $this->getRequest()->getParam('number_of_month');
$lock_type          = $this->getRequest()->getParam('lock_type');
$option_type          = $this->getRequest()->getParam('option_type');
$staff_id           = $this->getRequest()->getParam('staff_id');
$lock_time          = $this->getRequest()->getParam('lock_time');

$QLockInsurancePre   = new Application_Model_LockInsurancePre();
$userStorage 	    = Zend_Auth::getInstance ()->getStorage ()->read ();

$data = array(
    'number_of_month' => $number_of_month,
    'lock_type'       => $lock_type,
    'option_type'     => $option_type,
    'staff_id'        => $staff_id,
    'lock_time'       => $lock_time,
    'created_by'      => $userStorage->id,
    'created_at'      => date ( 'Y-m-d H:i:s')
);
if( !empty($lock_type ) AND !empty($staff_id) ){
    $where             = array();
    $where[]           = $QLockInsurancePre->getAdapter()->quoteInto('lock_type = ?' , $lock_type);
    $where[]           = $QLockInsurancePre->getAdapter()->quoteInto('option_type = ?' , $option_type);
    $where[]           = $QLockInsurancePre->getAdapter()->quoteInto('staff_id = ?' ,  $staff_id);
    $where[]           = $QLockInsurancePre->getAdapter()->quoteInto("lock_time = ? " ,$lock_time );
    $rowInsurance      =  $QLockInsurancePre->fetchRow($where); 
   
    if(!empty($rowInsurance)){
        $QLockInsurancePre->update($data,$where);
    }else{
        $QLockInsurancePre->insert($data);
    }

}else{
   return array('code'=>1,'message'=>'Done');
}
