<?php 
// config for template
define('START_ROW', 2);
define('NAME', 0);
define('EMAIL', 1);
define('TITLE', 2);
define('LINK', 3);


$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$QStaff = new Application_Model_Staff();
if ($this->getRequest()->getMethod() == 'POST') { // Big IF
    set_time_limit(0);
    ini_set('memory_limit', -1);
    // file_put_contents(APPLICATION_PATH.'/../public/files/mou/lock', '1');
    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
    $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }
    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {

        $this->view->errors = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet           = $objPHPExcel->getSheet(0);
    $highestRow      = $sheet->getHighestRow();
    $highestColumn   = $sheet->getHighestColumn();
  
    $error_list      = array();
    $success_list    = array();
    $store_code_list = array();
    $number_of_order = 0;
    $total_value     = 0;
    $total_order_row = 0;
    $order_list      = array();

    $arr_inserts   = array();
    $arr_order_ids = array();

    $_failed         = false;
    $total_row       = array();
    $data_staff_code = [];

    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            if (empty($rowData[NAME])) {
                throw new Exception("Empty NAME, row: " . $row . ".Data:" . implode(",", $rowData));
            }
            if (empty($rowData[EMAIL]) && empty($rowData[INSURANCE_SALARY])) {
                throw new Exception("Empty EMAIL, row: " . $row . ".Data:" . implode(",", $rowData));
            }
            if (empty($rowData[TITLE])) {
                throw new Exception("Empty TITLE, row: " . $row . ".Data:" . implode(",", $rowData));
            }
            $data_staff_email[] = trim($rowData[EMAIL]);
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        // nothing here
    } // END loop through order rows
    $progress->flush(30);
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    
    try {
        if (!empty($data_staff_email)) {
    
            $created_at          = date('Y-m-d H:i:s');
            $created_by          = $userStorage->id;
            for ($row = START_ROW; $row <= $highestRow; $row++) {
                $rowData       = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData       = isset($rowData[0]) ? $rowData[0] : array();
                
                /////////////////////////
                /////// CONFIG
                /////////////////////////
                $name  = $rowData[0];
                $email = $rowData[1];
                $title = $rowData[2];
                $link  = $rowData[3];
                
                error_reporting(~E_ALL);
                ini_set("display_error", 0);
                set_time_limit(0);
                $percent = round($row * 70 / count($data_staff_email), 1);
                $progress->flush($percent);
                if($row<$highestRow){
                    sleep(20);
                }
               
            }// end loop
        } else {
            throw new Exception("Danh sách nhân viên không hợp lệ");
        }
        
//        $progress->flush(99);
//        
//        $progress->flush(100);
//      
    } catch (Exception $e) {
        $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}
