<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$total              = $this->getRequest()->getParam('total');
$insurance_id       = $this->getRequest()->getParam('insurance_id');
$disiable       = $this->getRequest()->getParam('disiable');

$QInsuranceStaff    = new Application_Model_InsuranceStaff();
$userStorage 	    = Zend_Auth::getInstance ()->getStorage ()->read ();

if(!empty($disiable) AND ($total == 1) AND !empty($insurance_id) ){
    $where             = array();
    $where[]           = $QInsuranceStaff->getAdapter()->quoteInto('id = ?' , $insurance_id);
    
    $updated_by        =  $userStorage->id;
        $datetime = date ( 'Y-m-d H:i:s' );
        $data = array(
            'del'  => 1,
            'del_at'  => $datetime, 
            'note'    => "Delete By: $updated_by created_at: $datetime"
        );
        
        $QInsuranceStaff->update($data, $where);
        
}else{
   return array('code'=>1,'message'=>'Done');
}
