<?php

include 'PHPExcel.php';

$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();

$db = Zend_Registry::get('db');
$lock_time = $this->getRequest()->getParam('lock_time');
$sheetCompanyOPPO = 0;
$ty_le_month = ($lock_time < '2018-12-01') ? 4.5 : 8;
require_once 'export/export-oppo.php';
require_once 'export/export-thongminh.php';
require_once 'export/export-realme.php';
require_once 'export/export-oppo-foreigner.php';
require_once 'export/export-thongminh-foreigner.php';

if ($lock_time >= '2019-03-01') {
    require_once 'export/export-namviet.php';
}
if ($lock_time >= '2021-01-01') {
    require_once 'export/export-namviet-land.php';
}

// download file
$objWriter = new PHPExcel_Writer_Excel2007($objExcel);
$filename = 'insurance';
$objWriter->save($filename);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
header('Set-Cookie: fileLoading=true');
readfile($filename);
exit;




