<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$staff_id           = $this->getRequest()->getParam('staff_id');
$tang               = $this->getRequest()->getParam('tang');
$time_effective     = $this->getRequest()->getParam('time_effective');
$time_alter         = $this->getRequest()->getParam('time_alter');

$QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
if(!empty($staff_id) AND ($tang == 1) AND !empty($time_effective) AND !empty($time_alter)){
    $where             = array();
    $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('staff_id = ?' , $staff_id );
    $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('time_effective = ?' , $time_effective);
    $result            = $QInsuranceStaffBasic->fetchRow($where);
    $data = array(
        'staff_id'          => $staff_id,
        'type'              => 1,
        'option'            => 1,
        'time_effective'    => $time_effective,
        'time_alter'        => DateTime::createFromFormat('d/m/Y', $time_alter)->format('Y-m-d'),
        'created_at'        => date ( 'Y-m-d H:i:s' ),
        'created_by'        => $userStorage->id
    );
    
    if(empty($result)){
         $QInsuranceStaffBasic->insert($data);
     }else{
         $QInsuranceStaffBasic->update($data, $where);
         
     }
}
