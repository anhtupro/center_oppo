<?php
class PgcheckController extends My_Controller_Action
{
    
    public function init(){
        $list_channel = [2316,8366,2363,10007,3,2,1,4];
        $this->list_channel = $list_channel;
    }
    
    public function indexAction() {
        
        $date = $this->getRequest()->getParam('date');
        
        if(!$date){
            $date = date('Y-m-d');
        }
        
        $QPgcheckDate  = new Application_Model_PgcheckDate();
        $QAsm  = new Application_Model_Asm();
        
        $params = [
            'date' => $date,
            'channel' => $this->list_channel
        ];
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $id = $userStorage->id;
        
        $area = $QAsm->get_cache($id);
    
        $params['area_list'] = !empty($area['area']) ? $area['area'] : [0];
        
        $channel_list = $QPgcheckDate->getListChannel($params);
        
        $category_group = $QPgcheckDate->getListCategoryGroup();
        $get_total_store = $QPgcheckDate->getTotalStoreChannel($params);
        
        $total_store = [];
        foreach($get_total_store as $key=>$value){
            $total_store[$value['channel_id']] = $value['total_store'];
        }
        
        $get_date_check = $QPgcheckDate->getTotalCategoryCheck($params);
        $date_check = [];
        foreach($get_date_check as $key=>$value){
            $date_check[$value['channel_id']][$value['group_id']] = $value['total'];
        }
        
        $this->view->total_store = $total_store;
        $this->view->date_check = $date_check;
        
        $this->view->channel_list = $channel_list;
        $this->view->category_group = $category_group;
        $this->view->params = $params;
    }
    
    public function reportCategoryAction(){
        
        $group_category_id = $this->getRequest()->getParam('group_category_id');
        $date = $this->getRequest()->getParam('date');
        $region_id = $this->getRequest()->getParam('region_id');
        
        $QPgcheckDate  = new Application_Model_PgcheckDate();
        $QArea  = new Application_Model_Area();
        $QAsm  = new Application_Model_Asm();
        
        $params = [
            'group_category_id' => $group_category_id,
            'date'  => $date,
            'channel' => $this->list_channel,
            'region_id' => $region_id
        ];
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $id = $userStorage->id;
        
        $area = $QAsm->get_cache($id);
    
        $params['area_list'] = !empty($area['area']) ? $area['area'] : [0];
        $params['list_area'] = $params['area_list'];
        
        $group_category = $QPgcheckDate->getGroupCategory($group_category_id);
        
        $category_group = $QPgcheckDate->getListCategoryGroupById($group_category_id);
        $get_total_store = $QPgcheckDate->getTotalStoreChannelByArea($params);
        
        
        $total_store = [];
        foreach($get_total_store as $key=>$value){
            $total_store[$value['area_id']][$value['channel_id']] = $value['total_store'];
        }
        
        $get_date_check = $QPgcheckDate->getTotalCategoryCheckByArea($params);
        $date_check = [];
        foreach($get_date_check as $key=>$value){
            $date_check[$value['area_id']][$value['channel_id']] = $value['total'];
        }
        
        $channel_list = $QPgcheckDate->getListChannel($params);
        $area = $QPgcheckDate->getAreaList($params);
        
        
        $this->view->group_category = $group_category;
        $this->view->total_store = $total_store;
        $this->view->date_check = $date_check;
        $this->view->channel_list = $channel_list;
        $this->view->area = $area;
        $this->view->category_group = $category_group;
        $this->view->params = $params;
        
        
    }
    
    public function reportCategoryRegionAction(){
        
        $group_category_id = $this->getRequest()->getParam('group_category_id');
        $date = $this->getRequest()->getParam('date');
        
        $QPgcheckDate  = new Application_Model_PgcheckDate();
        $QArea  = new Application_Model_Area();
        $QAsm  = new Application_Model_Asm();
        $QRegion  = new Application_Model_Region();
        
        $params = [
            'group_category_id' => $group_category_id,
            'date'  => $date,
            'channel' => $this->list_channel
        ];
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $id = $userStorage->id;
        
        $area = $QAsm->get_cache($id);
        $region = $QRegion->get_cache();
    
        $params['area_list'] = !empty($area['area']) ? $area['area'] : [0];
        $params['list_area'] = $params['area_list'];
        
        $group_category = $QPgcheckDate->getGroupCategory($group_category_id);
        
        $category_group = $QPgcheckDate->getListCategoryGroupById($group_category_id);
        $get_total_store = $QPgcheckDate->getTotalStoreChannelByRegion($params);
        
        $total_store = [];
        foreach($get_total_store as $key=>$value){
            $total_store[$value['region_id']][$value['channel_id']] = $value['total_store'];
        }
        
        $get_date_check = $QPgcheckDate->getTotalCategoryCheckByRegion($params);
        
        $date_check = [];
        foreach($get_date_check as $key=>$value){
            $date_check[$value['region_id']][$value['channel_id']] = $value['total'];
        }
        
        $channel_list = $QPgcheckDate->getListChannel($params);
        $area = $QArea->getAreaList($params);
        
        $this->view->region = $region;
        $this->view->group_category = $group_category;
        $this->view->total_store = $total_store;
        $this->view->date_check = $date_check;
        $this->view->channel_list = $channel_list;
        $this->view->area = $area;
        $this->view->category_group = $category_group;
        $this->view->params = $params;
        
        
    }
    
    public function checkAction() {
        
        $this->_helper->viewRenderer->setRender('baotri');
        
        /*
        $pg_id = $this->getRequest()->getParam('pg_id');
        $id = $this->getRequest()->getParam('id');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $pg_id = $userStorage->id;
        
        $QPgcheckDate  = new Application_Model_PgcheckDate();
        $QPgcheckDateDetails  = new Application_Model_PgcheckDateDetails();
        $QPgcheckFile = new Application_Model_PgcheckFile();
        $QPgcheckChannelFile = new Application_Model_PgcheckChannelFile();
        
        if(!empty($id)){
            $this->view->pgcheck_date = $QPgcheckDate->fetchRow(['id = ?' => $id]);
            $pgcheck_date_details = $QPgcheckDateDetails->fetchRow(['pgcheck_date_id = ?' => $id]);
            
            
            $list_category = [];
            foreach($pgcheck_date_details as $key=>$value){
                $list_category[] = $value['pgcheck_category_id'];
            }
            $this->view->list_category = $list_category;
        }
        
        $channel_id = $QPgcheckDate->getChannelIdFromStaff($pg_id);
        
        $channel_id_list = NULL;
        foreach($channel_id as $key=>$value){
            $channel_id_list[] = $value;
        }
        
        $image_pgs = $QPgcheckChannelFile->getListImageChannel($channel_id_list);
        $image_btn = $QPgcheckChannelFile->getListBtnChannel($channel_id_list);
        $image_flash_sale = $QPgcheckChannelFile->getListFlashSaleChannel($channel_id_list);
        
        
        $info_pgs = $QPgcheckDate->getInfoPgs($pg_id);
        $list_store = $QPgcheckDate->getListStore($pg_id);
        
        $category = $QPgcheckDate->getListCategory();
        
        $this->view->info_pgs = $info_pgs;
        $this->view->list_store = $list_store;
        $this->view->category = $category;
        $this->view->pg_id = $pg_id;
        $this->view->channel_id = $channel_id;
        
        $this->view->image_pgs = $image_pgs;
        $this->view->image_btn = $image_btn;
        $this->view->image_flash_sale = $image_flash_sale;
        
        $flashMessenger     = $this->_helper->flashMessenger;
        $messages           = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
       
        if($this->getRequest()->isPost()) {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();

            try {
                
                
                 
                $pg_id  = $this->getRequest()->getParam('pg_id');
                $store_id  = $this->getRequest()->getParam('store_id');
                $note  = $this->getRequest()->getParam('note');
                $category_id  = $this->getRequest()->getParam('category_id');
                
                $is_off  = $this->getRequest()->getParam('is_off');
                $is_btn  = $this->getRequest()->getParam('is_btn');
                $shift  = $this->getRequest()->getParam('shift');
                
                $userStorage    = Zend_Auth::getInstance()->getStorage()->read();

                $data = array(
                    'pg_id'   => $pg_id,
                    'store_id' => $store_id,
                    'note'    => $note,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id,
                    'is_off' => $is_off,
                    'is_btn' => $is_btn,
                    'shift' => $shift,
                );
                
                $pgcheck_date_id = $QPgcheckDate->insert($data);
                
                foreach($category as $key=>$value){
                    $details = [
                        'pgcheck_date_id' => $pgcheck_date_id,
                        'pgcheck_category_id' => $value['id'],
                        'status' => (in_array($value['id'], $category_id)) ? 1 : 0,
                    ];
                    $QPgcheckDateDetails->insert($details);
                }
                
                
                
                //Upload file
                if(!empty($_FILES['image_pg']['tmp_name'])){
                    
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'pgcheck' . DIRECTORY_SEPARATOR . $pgcheck_date_id . DIRECTORY_SEPARATOR . '1';

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);
                    
                    $image1 = $this->getRequest()->getParam('file_upload1');
                    $name1 = md5(uniqid('', true)) . $pgcheck_date_id . '-pg_check' . $userStorage->id.".jpg";
                    $file1 = $uploaded_dir . DIRECTORY_SEPARATOR . $name1;
                    
                    $source = fopen($image1, 'r');
                    $destination = fopen($file1, 'w');
                    $success1 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);
                    
                    $id_image_pg = $QPgcheckFile->getIdImagePg($store_id, 1);
                    $id_image_pg = !empty($id_image_pg['id']) ? $id_image_pg['id'] : 0;
                    
                    if ($success1) {
                        $file_type1 = [
                            'pgcheck_date_id' => $pgcheck_date_id,
                            'url'       => "/files/pgcheck/".$pgcheck_date_id."/1/" . $name1,
                            'type'      => 1,
                            'channel_file_id' => $id_image_pg
                        ];

                        $QPgcheckFile->insert($file_type1);
                    }
                }
                
                //Upload file
                if(!empty($_FILES['image_flash_sale']['tmp_name'])){
                    
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'pgcheck' . DIRECTORY_SEPARATOR . $pgcheck_date_id . DIRECTORY_SEPARATOR . '2';

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);
                    
                    $image2 = $this->getRequest()->getParam('file_upload2');
                    $name2 = md5(uniqid('', true)) . $pgcheck_date_id . '-pg_check' . $userStorage->id.".jpg";
                    $file2 = $uploaded_dir . DIRECTORY_SEPARATOR . $name2;
                    
                    $source = fopen($image2, 'r');
                    $destination = fopen($file2, 'w');
                    $success2 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);
                    
                    $id_image_pg = $QPgcheckFile->getIdImagePg($store_id, 2);
                    $id_image_pg = !empty($id_image_pg['id']) ? $id_image_pg['id'] : 0;
                    
                    if ($success2) {
                        $file_type2 = [
                            'pgcheck_date_id' => $pgcheck_date_id,
                            'url'       => "/files/pgcheck/".$pgcheck_date_id."/2/" . $name2,
                            'type'      => 2,
                            'channel_file_id' => $id_image_pg
                        ];

                        $QPgcheckFile->insert($file_type2);
                    }
                }
                
                //Upload file
                if(!empty($_FILES['image_btn']['tmp_name'])){
                    
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'pgcheck' . DIRECTORY_SEPARATOR . $pgcheck_date_id . DIRECTORY_SEPARATOR . '3';

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);
                    
                    $image3 = $this->getRequest()->getParam('file_upload3');
                    $name3 = md5(uniqid('', true)) . $pgcheck_date_id . '-pg_check' . $userStorage->id.".jpg";
                    $file3 = $uploaded_dir . DIRECTORY_SEPARATOR . $name3;
                    
                    $source = fopen($image3, 'r');
                    $destination = fopen($file3, 'w');
                    $success3 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);
                    
                    $id_image_pg = $QPgcheckFile->getIdImagePg($store_id, 3);
                    $id_image_pg = !empty($id_image_pg['id']) ? $id_image_pg['id'] : 0;
                    
                    if ($success3) {
                        $file_type3 = [
                            'pgcheck_date_id' => $pgcheck_date_id,
                            'url'       => "/files/pgcheck/".$pgcheck_date_id."/3/" . $name3,
                            'type'      => 3,
                            'channel_file_id' => $id_image_pg
                        ];

                        $QPgcheckFile->insert($file_type3);
                    }
                }
                
                
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Hoàn thành');                

            } catch (Exception $e) {
                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                $this->redirect('/pgcheck/list-pgs');
                
            }
            $this->redirect('/pgcheck/list-pgs');
        }
         */
    }
    
    public function viewAction() {
        $id = $this->getRequest()->getParam('id');
        
        $QPgcheckDate  = new Application_Model_PgcheckDate();
        $QPgcheckFile  = new Application_Model_PgcheckFile();
        $QPgcheckDateDetails  = new Application_Model_PgcheckDateDetails();
        $QStore  = new Application_Model_Store();
        
        $pgcheck_date = $QPgcheckDate->fetchRow(['id = ?' => $id]);
        
        
        $pgcheck_date_details = $QPgcheckDateDetails->fetchRow(['pgcheck_date_id = ?' => $id]);
        $store = $QStore->fetchRow(['id = ?' => $pgcheck_date['store_id']]);
        
        $category = $QPgcheckDate->getListCategoryById($id);
        $info_pgs = $QPgcheckDate->getInfoPgs($pgcheck_date['pg_id']);
        
        $pg_image = $QPgcheckFile->getImagePgcheckdate($id, 1);
        $flash_sale_image = $QPgcheckFile->getImagePgcheckdate($id, 2);
        $btn_image = $QPgcheckFile->getImagePgcheckdate($id, 3);

        $flashMessenger     = $this->_helper->flashMessenger;
        $messages           = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        
        $this->view->pgcheck_date = $pgcheck_date;
        $this->view->category = $category;
        $this->view->info_pgs = $info_pgs;
        $this->view->store = $store;
        $this->view->pg_image = $pg_image;
        $this->view->flash_sale_image = $flash_sale_image;
        $this->view->btn_image = $btn_image;
        
        
    }
    
    public function listAction() {
        $sort = $this->getRequest()->getParam('sort', '');
        $desc = $this->getRequest()->getParam('desc', 1);
        $pg_name = $this->getRequest()->getParam('pg_name');
        $pg_code = $this->getRequest()->getParam('pg_code');
        $store_name = $this->getRequest()->getParam('store_name');
        $area_id = $this->getRequest()->getParam('area_id');
        $title = $this->getRequest()->getParam('title');
        $plan_id = $this->getRequest()->getParam('plan_id');
        $appraisal = $this->getRequest()->getParam('appraisal');
        $created_by = $this->getRequest()->getParam('created_by');
        $export = $this->getRequest()->getParam('export');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $QPgcheckDate = new Application_Model_PgcheckDate();
        $QArea = new Application_Model_Area();
        $QAsm = new Application_Model_Asm();
        $QPgcheckCategory = new Application_Model_PgcheckCategory();
        
        $list_category = $QPgcheckCategory->getList();
        
        $params = array(
            'pg_name' => $pg_name,
            'pg_code' => $pg_code,
            'store_name' => $store_name,
            'area_id' => $area_id,
        );
        

        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $page = $this->getRequest()->getParam('page', 1);
        $limit = 10;
        $total = 0;
        
        //Pgs
        if($userStorage->title == 182 || $userStorage->title == 293){
            $params['created_by'] = $userStorage->id;
        }
        
        //Trainer Team
        if($userStorage->title == 628 || $userStorage->title == 626 || $userStorage->title == 627){
            $area = $QAsm->get_cache($userStorage->id);
            $params['area_list'] = !empty($area['area']) ? $area['area'] : exit;
        }
        
        $result = $QPgcheckDate->fetchPagination($page, $limit, $total, $params);

        
        $this->view->list = $result;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->params = $params;
        $this->view->desc = $desc;
        $this->view->sort = $sort;
        $this->view->list_category = $list_category;
        $this->view->url = HOST . 'pgcheck/list' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
        $this->view->offset = $limit * ($page - 1);
        $this->view->area = $QArea->get_cache();
        
        $flashMessenger     = $this->_helper->flashMessenger;
        $messages_error           = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages_error;

        $messages_success   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        
        if(isset($export) and $export == 1)
        {
            set_time_limit( 0 );
            error_reporting( 0 );
            ini_set('display_error', 0);
            ini_set('memory_limit', -1);

            $name = "Reception Dealer ".date('Ymd').".csv";
            $fp   = fopen('php://output', 'w');

            header('Content-Type: text/csv; charset=utf-8');
            echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
            header('Content-Disposition: attachment; filename='.$name);

            $heads = array(
                'WARRANTY ID',
                'NAME',
                'PHONE',
                'PRODUCT',
                'COLOR',
                'SERIAL NUMBER',
                'MACHINE NUMBER',
                'DATE RECEIVE',
                'STATUS',
                'DEALER',
            );

            fputcsv($fp, $heads);



            foreach($result as $row)
            {
                $write  = array(
                    (isset($row['warranty_number']) and $row['warranty_number']) ? $row['warranty_number'] : NULL,
                    (isset($row['customer_name']) and $row['customer_name']) ? $row['customer_name'] : NULL,
                    (isset($row['customer_phone']) and $row['customer_phone']) ? $row['customer_phone'] : NULL,
                    (isset($row['good_name']) and $row['good_name']) ? $row['good_name'] : NULL,
                    (isset($row['color_name']) and $row['color_name']) ? $row['color_name'] : NULL,
                    (isset($row['number_imei']) and $row['number_imei']) ? $row['number_imei'] : NULL,
                    (isset($row['number_imei2']) and $row['number_imei2']) ? $row['number_imei2'] : NULL,
                    (isset($row['time_warranty']) and $row['time_warranty']) ? $row['time_warranty'] : NULL,
                    (isset($row['status']) and $row['status']) ? $row['status'] : NULL,
                    (isset($row['fullname']) and $row['fullname']) ? $row['fullname'] : NULL,
                );

                fputcsv($fp, $write);

            }

            fclose($fp);
            exit;
        }
        
    }
    
    public function listPgsAction() {
        
        $sort = $this->getRequest()->getParam('sort', '');
        $desc = $this->getRequest()->getParam('desc', 1);
        $pg_name = $this->getRequest()->getParam('pg_name');
        $pg_code = $this->getRequest()->getParam('pg_code');
        $store_name = $this->getRequest()->getParam('store_name');
        $area_id = $this->getRequest()->getParam('area_id');
        $title = $this->getRequest()->getParam('title');
        $plan_id = $this->getRequest()->getParam('plan_id');
        $appraisal = $this->getRequest()->getParam('appraisal');
        $created_by = $this->getRequest()->getParam('created_by');
        $date = $this->getRequest()->getParam('date');
        
        $export = $this->getRequest()->getParam('export');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $QPgcheckDate = new Application_Model_PgcheckDate();
        $QArea = new Application_Model_Area();
        $QAsm = new Application_Model_Asm();
        $QPgcheckCategory = new Application_Model_PgcheckCategory();
        
        $list_category = $QPgcheckCategory->getList();
        
        $params = array(
            'pg_name' => $pg_name,
            'pg_code' => $pg_code,
            'store_name' => $store_name,
            'area_id' => $area_id,
            'date'  => !empty($date) ? $date : date('Y-m-d'),
            'export'    => $export
        );
        

        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $page = $this->getRequest()->getParam('page', 1);
        $limit = 10;
        $total = 0;
        
        //Pgs
        if($userStorage->title == 182 || $userStorage->title == 293){
            $params['created_by'] = $userStorage->id;
        }
        
        //Trainer Team
        if($userStorage->title == 628 || $userStorage->title == 626 || $userStorage->title == 627){
            $area = $QAsm->get_cache($userStorage->id);
            $params['area_list'] = !empty($area['area']) ? $area['area'] : exit;
        }
        
        $params['channel'] = unserialize(PGCHECK_CHANNEL);
        
        $result = $QPgcheckDate->fetchPaginationStaff($page, $limit, $total, $params);

        
        $this->view->list = $result;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->params = $params;
        $this->view->desc = $desc;
        $this->view->sort = $sort;
        $this->view->list_category = $list_category;
        $this->view->url = HOST . 'pgcheck/list-pgs' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
        $this->view->offset = $limit * ($page - 1);
        $this->view->area = $QArea->get_cache();
        
        $flashMessenger     = $this->_helper->flashMessenger;
        $messages_error           = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages_error;

        $messages_success   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        
        if(isset($export) and $export == 1)
        {
            
            
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'stt'                   =>'#',
                'ten_pgs'               =>'Tên PGs',
                'pg_code'               =>'PGs Code',
                'ten_shop'              =>'Tên shop',
                'store_code'            =>'Code shop',
                'channel'               =>'Kênh',
                'ngay'                  =>'Ngày',
                'khu_vuc'               =>'Khu vực',
                'tinh_trang'            =>'Có kiểm tra hình ảnh',
            );
            
            
            foreach($list_category as $key=>$value){
                $heads[$key] = $value;
            }
            
            $heads['note'] = 'Note';
            $heads['image'] = 'Hình ảnh Pgs';
            $heads['image2'] = 'Hình ảnh FlashSale';
            $heads['image3'] = 'Hình ảnh BTN';
            
            $heads['created_at'] = 'Thời gian tạo';
            $heads['is_off'] = 'Off';
            $heads['is_btn'] = 'Có bàn trải nghiệm';
            $heads['shift'] = 'Ca làm việc';

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            $alpha = 'A';
            $index = 1;
            foreach ($heads as $key)
            {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }
            $index = 2;
            $types = unserialize(TYPE_TRAINING_REPORT);
            $QArea                       = new Application_Model_Area();
            $cachedArea      = $QArea->get_cache();

            $QRegionalMarket                  = new Application_Model_RegionalMarket();
            $cachedRegionalMarket = $QRegionalMarket->get_cache();

            $QStore                      = new Application_Model_Store();
            $cachedStore     = $QStore->get_cache();

            $date = $params['date'];
            
            $intCount = 1;
            foreach ($result as $_key => $pgcheck)
            {
                
                $status = !empty($pgcheck['pgcheck_date_details_id']) ? 'Có kiểm tra' : 'Không kiểm tra';
                
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['pg_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['pg_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['store_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['store_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['channel_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($date,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['area_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($status,PHPExcel_Cell_DataType::TYPE_STRING);
                
                
                $list_cat_check = explode(",", $pgcheck['list_category']);
                foreach($list_category as $key=>$value){
                    
                    $check = NULL;
                    if(in_array($key, $list_cat_check)){
                        $check = 'X';
                    }
                    
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($check, PHPExcel_Cell_DataType::TYPE_STRING);
                }
                
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['note'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['image_pgs'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['image_flashsale'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['image_btn'],PHPExcel_Cell_DataType::TYPE_STRING);
                
                
                $is_off = (!empty($pgcheck['is_off']) AND $pgcheck['is_off'] == 1) ? 'Có' : 'Không';
                $is_btn = (!empty($pgcheck['is_btn']) AND $pgcheck['is_btn'] == 1) ? 'Có' : 'Không';
                
                $shift = !empty($pgcheck['shift']) ? ($pgcheck['shift'] == 1 ? 'Ca sáng' : 'Ca chiều') : NULL;
                
                
                $sheet->getCell($alpha++ . $index)->setValueExplicit($pgcheck['created_at'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($is_off,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($is_btn,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($shift,PHPExcel_Cell_DataType::TYPE_STRING);
                
                $index++;
            }
                

            $filename = 'Pgcheck-' . date('d-m-Y H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }
        
    }
    
    public function listImageSampleAction() {
        $sort = $this->getRequest()->getParam('sort', '');
        $desc = $this->getRequest()->getParam('desc', 1);
        $pg_name = $this->getRequest()->getParam('pg_name');
        $pg_code = $this->getRequest()->getParam('pg_code');
        $store_name = $this->getRequest()->getParam('store_name');
        $area_id = $this->getRequest()->getParam('area_id');
        $title = $this->getRequest()->getParam('title');
        $plan_id = $this->getRequest()->getParam('plan_id');
        $appraisal = $this->getRequest()->getParam('appraisal');
        $created_by = $this->getRequest()->getParam('created_by');
        $date = $this->getRequest()->getParam('date');
        
        $export = $this->getRequest()->getParam('export');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $QPgcheckDate = new Application_Model_PgcheckDate();
        $QArea = new Application_Model_Area();
        $QAsm = new Application_Model_Asm();
        $QPgcheckCategory = new Application_Model_PgcheckCategory();
        
        $list_category = $QPgcheckCategory->getList();
        
        $params = array(
            'pg_name' => $pg_name,
            'pg_code' => $pg_code,
            'store_name' => $store_name,
            'area_id' => $area_id,
            'date'  => !empty($date) ? $date : date('Y-m-d'),
            'export'    => $export
        );
        

        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $page = $this->getRequest()->getParam('page', 1);
        $limit = 100;
        $total = 0;
        
        //Pgs
        if($userStorage->title == 182 || $userStorage->title == 293){
            $params['created_by'] = $userStorage->id;
        }
        
        //Trainer Team
        if($userStorage->title == 628 || $userStorage->title == 626 || $userStorage->title == 627){
            $area = $QAsm->get_cache($userStorage->id);
            $params['area_list'] = !empty($area['area']) ? $area['area'] : exit;
        }
        
        $result = $QPgcheckDate->fetchChannelImage($page, $limit, $total, $params);

        
        $this->view->list = $result;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->params = $params;
        $this->view->desc = $desc;
        $this->view->sort = $sort;
        $this->view->list_category = $list_category;
        $this->view->url = HOST . 'pgcheck/list-image-sample' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
        $this->view->offset = $limit * ($page - 1);
        $this->view->area = $QArea->get_cache();
        
        $flashMessenger     = $this->_helper->flashMessenger;
        $messages_error           = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages_error;

        $messages_success   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
    }
    
    public function createImageSampleAction() {
        
        $channel_id  = $this->getRequest()->getParam('channel_id');
        
        $QPgcheckDate = new Application_Model_PgcheckDate();
        $QPgcheckCategory = new Application_Model_PgcheckCategory();
        $QPgcheckChannelFile = new Application_Model_PgcheckChannelFile();
        
        
        $params = [
            'channel' => [$channel_id]
        ];
        
        $list_channel = $QPgcheckDate->getListChannel($params);
        
        $channel = [];
        foreach($list_channel as $key=>$value){
            $channel[$value['channel_id']] = $value['channel_name'];
        }
        
        $image_pg = $QPgcheckChannelFile->fetchRow(['channel_id = ?' => $channel_id, 'is_del = ?' => 0, 'type = ?' => 1]);
        $image_flash_sale = $QPgcheckChannelFile->fetchRow(['channel_id = ?' => $channel_id, 'is_del = ?' => 0, 'type = ?' => 2]);
        $image_btn = $QPgcheckChannelFile->fetchRow(['channel_id = ?' => $channel_id, 'is_del = ?' => 0, 'type = ?' => 3]);
        
        
        $channel_id_list = [$channel_id];
        $list_category = $QPgcheckDate->getListCategoryChannel($channel_id_list);
        
        $group_category = $QPgcheckCategory->getList();
        
        
        $this->view->group_category = $group_category;
        $this->view->list_channel = $list_channel;
        $this->view->channel_id = $channel_id;
        $this->view->channel = $channel;
        $this->view->list_category = $list_category;
        
        $this->view->image_pg = $image_pg;
         $this->view->image_flash_sale = $image_flash_sale;
        $this->view->image_btn = $image_btn;
        
        
        $flashMessenger     = $this->_helper->flashMessenger;
        $messages           = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        
        if($this->getRequest()->isPost()) {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();

            try {
                 
                $channel_id  = $this->getRequest()->getParam('channel_id');
                $category  = $this->getRequest()->getParam('category');
                $group_id  = $this->getRequest()->getParam('group_id');
                
                foreach($category as $key=>$value){
                    $data_update = [
                        'name' => $value,
                        'group_id' => $group_id[$key]
                    ];
                    $QPgcheckCategory->update($data_update, ['id = ?' => $key]);
                }
                
                $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
                
                //Upload file
                if(!empty($_FILES['image_pg']['tmp_name'])){
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '1';

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);
                    $tmpFilePath = $_FILES['image_pg']['tmp_name'];


                    if ($tmpFilePath != "") {
                        $old_name = $_FILES['image_pg']['name'];
                        $tExplode = explode('.', $old_name);
                        $extension = end($tExplode);

                        if($_FILES['image_pg']['size'] > 80000000)
                        {
                            $flashMessenger->setNamespace('error')->addMessage( "File Upload dung lượng quá lớn (Vui lòng upload file dưới 8M)" );
                            $this->redirect('/pgcheck/list-image-sample');
                            
                            return;
                        }

                        if(!in_array($extension, ['png', 'PNG', 'jpg', 'jpeg', 'JPEG'])){
                            $flashMessenger->setNamespace('error')->addMessage( "File Upload sai định dạng: ".$extension );
                            $this->redirect('/pgcheck/list-image-sample');
                            
                            return;
                        }

                        $new_name = md5(uniqid('', true)) . $channel_id . '-pg_check' . $userStorage->id."." .$extension;
                        $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                            $url = DIRECTORY_SEPARATOR . 'files' .
                                DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '1' . DIRECTORY_SEPARATOR . $new_name;
                        } else {
                            $url = NULL;
                        }
                        
                        ////Resize Anh
                        $url_resize =  'files' .DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '1' . DIRECTORY_SEPARATOR . $new_name;
                        $image = new My_Image_Resize();
                        $image->load($url_resize);
                        $image->resizeToWidth(1200);
                        $image->save($url_resize);
                        ////END Resize
                        
                        //Move file to S3
                        require_once 'Aws_s3.php';
                        $s3_lib = new Aws_s3();
                        
                        $file_location = 'files' .
                                DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '1' . DIRECTORY_SEPARATOR . $new_name;
                        $detination_path = 'files/pgcheck-channel/'.$channel_id.'/1/';
                        $destination_name = $new_name;
                        $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
                        
                        if($upload_s3['message'] != 'ok'){
                            echo json_encode([
                                'status' => 0,
                                'message' => "Upload S3 không thành công",
                            ]);
                            return;
                        }
                        // END - Move file to S3
                        
                    } else {
                        $url = NULL;
                    }

                    $file_type1 = [
                        'channel_id' => $channel_id,
                        'url'       => $url,
                        'type'      => 1
                    ];
                    
                    $QPgcheckChannelFile->update(['is_del' => 1], ['channel_id = ?' => $channel_id, 'type = ?' => 1, 'is_del = ?' => 0]);
                    $QPgcheckChannelFile->insert($file_type1);
                }
                
                //Upload file flash sale
                if(!empty($_FILES['image_flash_sale']['tmp_name'])){
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '2';

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);
                    $tmpFilePath = $_FILES['image_flash_sale']['tmp_name'];

                    
                    if ($tmpFilePath != "") {
                        $old_name = $_FILES['image_flash_sale']['name'];
                        $tExplode = explode('.', $old_name);
                        $extension = end($tExplode);

                        if($_FILES['image_flash_sale']['size'] > 80000000)
                        {
                            
                            $flashMessenger->setNamespace('error')->addMessage( "File Upload dung lượng quá lớn (Vui lòng upload file dưới 8M)" );
                            $this->redirect('/pgcheck/list-image-sample');
                            
                            return;
                        }

                        if(!in_array($extension, ['png', 'PNG', 'jpg', 'jpeg', 'JPEG'])){
                            
                            $flashMessenger->setNamespace('error')->addMessage( "File Upload sai định dạng: ".$extension );
                            $this->redirect('/pgcheck/list-image-sample');
                            
                            return;
                        }

                        $new_name = md5(uniqid('', true)) . $channel_id . '-pg_check' . $userStorage->id."." .$extension;
                        $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                            $url = DIRECTORY_SEPARATOR . 'files' .
                                DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $new_name;
                        } else {
                            $url = NULL;
                        }
                        
                        ////Resize Anh
                        $url_resize =  'files' .DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $new_name;
                        $image = new My_Image_Resize();
                        $image->load($url_resize);
                        $image->resizeToWidth(1200);
                        $image->save($url_resize);
                        ////END Resize
                        
                        //Move file to S3
                        require_once 'Aws_s3.php';
                        $s3_lib = new Aws_s3();

                        $file_location = 'files' .
                                DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $new_name;;
                        $detination_path = 'files/pgcheck-channel/'.$channel_id.'/2/';
                        $destination_name = $new_name;

                        $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
                        if($upload_s3['message'] != 'ok'){
                            echo json_encode([
                                'status' => 0,
                                'message' => "Upload S3 không thành công",
                            ]);
                            return;
                        }
                        // END - Move file to S3
                        
                    } else {
                        $url = NULL;
                    }
                    
                    $file_type2 = [
                        'channel_id' => $channel_id,
                        'url'       => $url,
                        'type'      => 2
                    ];
                    
                    $QPgcheckChannelFile->update(['is_del' => 1], ['channel_id = ?' => $channel_id, 'type = ?' => 2, 'is_del = ?' => 0]);
                    $QPgcheckChannelFile->insert($file_type2);
                    
                }
                
                
                //Upload file
                if(!empty($_FILES['image_btn']['tmp_name'])){
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '3';

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);
                    $tmpFilePath = $_FILES['image_btn']['tmp_name'];

                    
                    if ($tmpFilePath != "") {
                        $old_name = $_FILES['image_btn']['name'];
                        $tExplode = explode('.', $old_name);
                        $extension = end($tExplode);

                        if($_FILES['image_btn']['size'] > 80000000)
                        {
                            
                            $flashMessenger->setNamespace('error')->addMessage( "File Upload dung lượng quá lớn (Vui lòng upload file dưới 8M)" );
                            $this->redirect('/pgcheck/list-image-sample');
                            
                            return;
                        }

                        if(!in_array($extension, ['png', 'PNG', 'jpg', 'jpeg', 'JPEG'])){
                            
                            $flashMessenger->setNamespace('error')->addMessage( "File Upload sai định dạng: ".$extension );
                            $this->redirect('/pgcheck/list-image-sample');
                            
                            return;
                        }

                        $new_name = md5(uniqid('', true)) . $channel_id . '-pg_check' . $userStorage->id."." .$extension;
                        $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                            $url = DIRECTORY_SEPARATOR . 'files' .
                                DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $new_name;
                        } else {
                            $url = NULL;
                        }
                        
                        ////Resize Anh
                        $url_resize =  'files' .DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $new_name;
                        $image = new My_Image_Resize();
                        $image->load($url_resize);
                        $image->resizeToWidth(1200);
                        $image->save($url_resize);
                        ////END Resize
                        
                        //Move file to S3
                        require_once 'Aws_s3.php';
                        $s3_lib = new Aws_s3();

                        $file_location = 'files' .
                                DIRECTORY_SEPARATOR . 'pgcheck-channel' . DIRECTORY_SEPARATOR . $channel_id . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $new_name;
                        $detination_path = 'files/pgcheck-channel/'.$channel_id.'/3/';
                        $destination_name = $new_name;

                        $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
                        if($upload_s3['message'] != 'ok'){
                            echo json_encode([
                                'status' => 0,
                                'message' => "Upload S3 không thành công",
                            ]);
                            return;
                        }
                        // END - Move file to S3
                        
                    } else {
                        $url = NULL;
                    }
                    
                    $file_type3 = [
                        'channel_id' => $channel_id,
                        'url'       => $url,
                        'type'      => 3
                    ];
                    
                    $QPgcheckChannelFile->update(['is_del' => 1], ['channel_id = ?' => $channel_id, 'type = ?' => 3, 'is_del = ?' => 0]);
                    $QPgcheckChannelFile->insert($file_type3);
                    
                }
                
                
                
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Hoàn thành');     
                $this->redirect('/pgcheck/list-image-sample');

            } catch (Exception $e) {
                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                $this->redirect('/pgcheck/list-image-sample');
                
            }
            $this->redirect('/pgcheck/list-image-sample');
        }
        
    }
    
    public function resizeImageAction(){
        
        $QPgcheckFile  = new Application_Model_PgcheckFile();
        $image = new My_Image_Resize();
        $list_image = $QPgcheckFile->fetchAll(['pgcheck_date_id > ?' => 5000,'pgcheck_date_id < ?' => 6000]);
        
        
        foreach($list_image as $key=>$value){
            echo ltrim($value['url'],"/").'<br>';
            ////Resize Anh
            //$url_resize =  'files' .DIRECTORY_SEPARATOR . 'pgcheck' . DIRECTORY_SEPARATOR . $pgcheck_date_id . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $new_name;
            $url_resize = ltrim($value['url'],"/");    
            $image->load($url_resize);
            $image->resizeToWidth(1000);
            $image->save($url_resize);
            ////END Resize
        }
        exit;
        
        
    }
    
    
    

}
