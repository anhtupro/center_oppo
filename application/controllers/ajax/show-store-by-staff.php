<?php 
	$staff_id = $this->getRequest()->getParam('staff_id');
    $QStore = new Application_Model_Store();
    $QStaff = new Application_Model_Staff();
    $stores = array();
    $staff_sale = array();
    $total = 0;
    $params = array(
        'staff_id' => $staff_id,
        'sales_direct' => 1
    );

    $staff = $QStaff->find($staff_id)->current();
    if( $staff and in_array($staff->title,My_Staff_Title::getPg())){
        unset($params['sales_direct']);
        $params['have_pg'] = 1;
    } elseif ($staff and in_array($staff->title,My_Staff_Title::getStoreLeader())) {
        unset($params['sales_direct']);
        $params['have_store_leader'] = 1;
    }
    $this->view->staff = $staff;


    if($staff_id){
        $stores = $QStore->fetchPagination(0,NULL,$total,$params);
    }

    $this->view->stores = $stores;

    if( $staff and !in_array($staff->title,My_Staff_Title::getPg())){
        $staff_sale = $QStore->get_sale_pg_leader($staff_id);
        $this->view->staff_sale = $staff_sale;

    }
    $this->_helper->layout()->disablelayout(true);
?>