<?php 
	$from_date = $this->getRequest()->getParam('from_date');
	$to_date   = $this->getRequest()->getParam('to_date');
	$area_id   = $this->getRequest()->getParam('area_id');

	if($from_date){
		$from_date = My_Date::normal_to_mysql($from_date);
	}

	if($to_date){
		$to_date = My_Date::normal_to_mysql($to_date);
	}

	$QRegionalMarket = new Application_Model_RegionalMarket();
	if($area_id){
		$r = $QRegionalMarket->getRegionByTime($from_date,$to_date,$area_id);
	}

	$this->_helper->json->sendJson($r);
	exit;
	
?>