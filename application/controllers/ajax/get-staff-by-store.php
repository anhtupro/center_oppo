<?php
/**
 * Created by PhpStorm.
 * User: Hac
 * Date: 3/23/2016
 * Time: 2:15 PM
 */
$store_id = $this->getRequest()->getParam('store_id');
$QStore = new Application_Model_Store();
$store = $QStore->find($store_id)->current();
$this->view->store = $store;
$params['store_id'] = $store_id;
$QStaff = new Application_Model_Staff();
$result = $QStaff->getStaffByStore($params);
$this->view->result = $result;
$this->_helper->layout()->disableLayout(true);