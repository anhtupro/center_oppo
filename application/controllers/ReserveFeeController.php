<?php

class ReserveFeeController extends My_Controller_Action
{
    public function init(){}

    public function indexAction(){
    }
    public function reserveFeeAction(){
        require_once 'reserve-fee' . DIRECTORY_SEPARATOR . 'create.php';
    }
    public function saveReserveFeeAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try{
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();


            $department_id = $this->getRequest()->getParam('department_id');
            $reserve_fee = $this->getRequest()->getParam('reserve_fee');
            $reserve_date = $this->getRequest()->getParam('reserve_date');

            if(!empty($reserve_date)){
                 $data=array(
                "amount" => $reserve_fee,
                "department_id" => $department_id,
                "created_by" => $userStorage->id,
                "created_at" => $reserve_date,
                );
            $QReserveFee=new Application_Model_ReserveFee();
            $result=$QReserveFee->insert($data);
            }
           

            echo json_encode([
                'status' => 1,
            ]);
            return;
        }catch (Exception $e){
            echo json_encode([
                'status' => 0,
                'message' => "Tạo đề xuất không thành công".$e,
            ]);
            return;
        }

    }
}
