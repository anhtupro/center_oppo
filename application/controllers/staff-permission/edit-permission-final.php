<?php
$flashMessenger = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QStaffPermissions = new Application_Model_StaffPermissions();
$QStaffPermissionDetail = new Application_Model_StaffPermissionDetail();
$QLogStaffPermissions = new Application_Model_LogStaffPermissions();

$id = $this->getRequest()->getParam('id');
$group_permission = $this->getRequest()->getParam('group_permission', 0);
$level_manage = $this->getRequest()->getParam('is_manager', 0);
$area_id = $this->getRequest()->getParam('area_id', 0);
$office_id = $this->getRequest()->getParam('office_id', 0);
$department_id = $this->getRequest()->getParam('app_department_id', 0);
$team_id = $this->getRequest()->getParam('app_team_id', 0);
$title_id = $this->getRequest()->getParam('app_title_id', 0);
$staff_id = $this->getRequest()->getParam('staff_id', 0);
$filter_area = $this->getRequest()->getParam('filter_area', 0);

if ($this->getRequest()->getMethod() == 'POST') {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        // Check validate
        if (empty($group_permission)) {
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn Tool name');
            $this->_redirect("/staff-permission/update-permission-final?id=" . $id);
        }
      
        //log
        $where_permissions = [];
        $where_permissions = $QStaffPermissions->getAdapter()->quoteInto('id = ?', $id);
        $data_staff_permission = $QStaffPermissions->fetchRow($where_permissions);
        
        $where_staff_permission_detail = [];
        $where_staff_permission_detail = $QStaffPermissionDetail->getAdapter()->quoteInto('id_staff_permissions = ?', $id);
        $data_staff_permission_detail = $QStaffPermissionDetail->fetchAll($where_staff_permission_detail);
        
        $QLogStaffPermissions->insert([
           'id_staff_permission' => $id,
            'staff_permission' => json_encode($data_staff_permission->toArray()),
            'staff_permission_detail' => json_encode($data_staff_permission_detail->toArray()),
            'updated_at' => date('Y-m-d H-i-s'),
            'updated_by' => $userStorage->id
        ]);
        //end log
        //
        //update staff permission
        $data_permissions = [
            'group_permission' => $group_permission,
            'level_manage' => $level_manage
        ];
        $QStaffPermissions->update($data_permissions, $where_permissions);
        //end update staff permission
        
        //del staff permission detail
        $QStaffPermissionDetail->delete($where_staff_permission_detail);
        //end del staff permission detail
        
        //insert data detail
        $data = array(
            My_StaffPermissions::TYPE_AREA_ID => $area_id,
            My_StaffPermissions::TYPE_PROVINCE_ID => $province_id,
            My_StaffPermissions::TYPE_DISTRICT_ID => $district_id,
            My_StaffPermissions::TYPE_DEPARTMENT_ID => $department_id,
            My_StaffPermissions::TYPE_TEAM_ID => $team_id,
            My_StaffPermissions::TYPE_TITLE_ID => $title_id,
            My_StaffPermissions::TYPE_OFFICE_ID => $office_id,
            My_StaffPermissions::TYPE_STAFF_ID => $staff_id,
            My_StaffPermissions::TYPE_FILTER_AREA_ID => $filter_area
        );
        $data_staff_permission_detail = [];
        foreach ($data as $k => $val) {
            if (!empty($val) && isset($val)) {
                if (is_array($val)) {
                    foreach ($val as $value_d) {
                        $data_staff_permission_detail[] = [
                            'id_staff_permissions' => $id,
                            'type_app' => $k,
                            'value_app' => $value_d
                        ];
                    }
                } else {
                    $data_staff_permission_detail[] = [
                        'id_staff_permissions' => $id,
                        'type_app' => $k,
                        'value_app' => $val
                    ];
                }
            }
        }
        
        if(!empty($data_staff_permission_detail)){
            My_Controller_Action::insertAllrowDB($data_staff_permission_detail,'staff_permission_detail',$db);
        }
        
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Thành công');
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/staff-permission/update-permission-final?id='.$id);
    }
    $this->_redirect("/staff-permission/list-permission-final");

    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_success = $messages_success;
    $this->view->messages_error = $messages_error;
}
