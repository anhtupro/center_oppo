<?php
$flashMessenger = $this->_helper->flashMessenger;

$QGroupPermission = new Application_Model_GroupPermission();
$QStaffPermissions = new Application_Model_StaffPermissions();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_permission=$QGroupPermission->fetchAll();

$tool_name = My_Util::escape_string($this->getRequest()->getParam('tool_name'));
$data_tool = My_Util::escape_string($this->getRequest()->getParam('data_tool'));

if($tool_name != null) {
    $data_group = array(
        'name' => $tool_name
    );
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        $id_group_new = $QGroupPermission->insert($data_group); 

        if($data_tool != 0) {
            $QStaffPermissions->coppyData($db, $data_tool, $id_group_new, $userStorage->id);
        }
        
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Tạo tool thành công!');
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/staff-permission/create-permission-final');
    }
    $this->_redirect("/staff-permission/add-tool-final");
}

$this->view->group_permission = $group_permission;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$messages_error               = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;
$this->_helper->viewRenderer->setRender('add-tool-final');