<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

// no limit time
set_time_limit(0);
ini_set('memory_limit', -1);
error_reporting(~E_ALL);
ini_set('display_error', 0);
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();
$heads = array(
    'STT',
    'TOOL NAME',
    'TITLE',
    'LEVEL MANAGE',
    'AREA',
    'OFFICE',
    'DEPARTMENT APPROVE',
    'TEAM APPROVE',
    'TITLE APPROVE',
    'STAFF APPROVE',
    'FILTER BY AREA',
);


$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();
$PHPExcel->getActiveSheet()->setTitle("Title");
$alpha = 'A';
$index = 1;
foreach ($heads as $key) {
    $sheet->setCellValue($alpha . $index, $key);
    $alpha++;
}
$index = 2;
$intCount = 1;
try {
    if ($data_e)
        foreach ($data_e as $_key => $_order) {
            $alpha = 'A';
            $tick = (!empty($_order['tick_fillter_area']))?'X':'';
            $sheet->setCellValue($alpha++ . $index, $intCount++);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['group_permission'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['full_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['level'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['office_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['dept_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['team_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['title_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($tick, PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        }
} catch (exception $e) {
    exit;
}

//sheet 2
$PHPExcel->createSheet();
$sheet = $PHPExcel->setActiveSheetIndex(1);
$PHPExcel->getActiveSheet()->setTitle("Staff");
$alpha = 'A';
$index = 1;
foreach ($heads as $key) {
    $sheet->setCellValue($alpha . $index, $key);
    $alpha++;
}
$index = 2;
$intCount = 1;
try {
    if ($dataStaff_e)
        foreach ($dataStaff_e as $_key => $_order) {
            $alpha = 'A';
            $tick = (!empty($_order['tick_fillter_area']))?'X':'';
            $sheet->setCellValue($alpha++ . $index, $intCount++);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['group_permission'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['full_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['level'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['office_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['dept_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['team_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['title_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($tick, PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        }
} catch (exception $e) {
    exit;
}


$filename = 'List_permission' . date('Y_m_d');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit();
