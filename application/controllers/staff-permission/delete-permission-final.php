<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger = $this->_helper->flashMessenger;
$id = $this->getRequest()->getParam('id');

$params = array(
    "del" => 1,
);
$QStaffPermissions = new Application_Model_StaffPermissions();
$where = $QStaffPermissions->getAdapter()->quoteInto('id = ?', $id);
$QStaffPermissions->update($params, $where);
$flashMessenger->setNamespace('success')->addMessage('Đã cập nhật');
header('Location: ' . $_SERVER['HTTP_REFERER']);