<?php
$flashMessenger = $this->_helper->flashMessenger;
$QGroupPermission = new Application_Model_GroupPermission();
$QTypePermission = new Application_Model_TypePermission();
$QStaffPermissions = new Application_Model_StaffPermissions();
$QStaffPermissionDetail = new Application_Model_StaffPermissionDetail();
$QArea = new Application_Model_Area();
$QOffice = new Application_Model_Office();
$QTeam      = new Application_Model_Team();
$QStaff = new Application_Model_Staff();

$id = $this->getRequest()->getParam('id');

$where = [];
$where = $QStaffPermissions->getAdapter()->quoteInto('id = ?', $id);
$staff_permission_tmp = $QStaffPermissions->fetchRow($where);

$where = [];
$where = $QStaffPermissionDetail->getAdapter()->quoteInto('id_staff_permissions = ?', $id);
$staff_permission_detail_tmp = $QStaffPermissionDetail->fetchAll($where);
$staff_permission_detail = [];
foreach ($staff_permission_detail_tmp as $value){
    $staff_permission_detail[$value->type_app][] = $value->value_app;
}
if($staff_permission_tmp['type'] == 1){
    $this->view->getStaffName = $QStaff->getAllName($staff_permission_tmp['value']);
}
if($staff_permission_tmp['type'] == 2){
    $this->view->titleTeamDepartmentPermission = $QTeam->getTitleTeamDepartment($staff_permission_tmp['value']);
}

$this->view->group_permission = $QGroupPermission->fetchAll();
$this->view->type_permission = $QTypePermission->getTypeGroup();
$this->view->office = $QOffice->fetchAllOffice();
$this->view->areas = $QArea->GetAll();
$this->view->staff_permission_tmp = $staff_permission_tmp;
$this->view->staff_permission_detail = $staff_permission_detail;
$this->view->department = $QTeam->getAllDepartment($staff_permission_tmp['value']);
$this->view->team = $QTeam->getAllTeam($staff_permission_tmp['value']);
$this->view->title = $QTeam->getAllTitle($staff_permission_tmp['value']);
$this->view->allDepartmentApproved = $QTeam->getDepartmentApproved();

if(isset($staff_permission_detail[My_StaffPermissions::TYPE_DEPARTMENT_ID][0]))
    $this->view->allTeamApproved = $QTeam->getTeamApproved($staff_permission_detail[My_StaffPermissions::TYPE_DEPARTMENT_ID][0]);
if(isset($staff_permission_detail[My_StaffPermissions::TYPE_TEAM_ID][0]))
    $this->view->allTitleApproved = $QTeam->getTitleApproved($staff_permission_detail[My_StaffPermissions::TYPE_TEAM_ID][0]);
if(isset($staff_permission_detail[My_StaffPermissions::TYPE_STAFF_ID]) && !empty($staff_permission_detail[My_StaffPermissions::TYPE_STAFF_ID]))
    $this->view->staffName = $QStaff->getAllName($staff_permission_detail[My_StaffPermissions::TYPE_STAFF_ID]);

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$messages_error               = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;