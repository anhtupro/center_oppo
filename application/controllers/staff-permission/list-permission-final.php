<?php

$flashMessenger = $this->_helper->flashMessenger;
$limit = 10;
$total  = 0;
$total1  = 0;
$QGroupPermission = new Application_Model_GroupPermission();
$QStaffPermissions = new Application_Model_StaffPermissions();
$QTeam = new Application_Model_Team();
$QArea = new Application_Model_Area();
$QOffice = new Application_Model_Office();

$tool_name = My_Util::escape_string($this->getRequest()->getParam('tool-name',0));
$staff_code = My_Util::escape_string($this->getRequest()->getParam('staff-code',''));
$department = My_Util::escape_string($this->getRequest()->getParam('department',0));
$team = My_Util::escape_string($this->getRequest()->getParam('team',0));
$title = My_Util::escape_string($this->getRequest()->getParam('title',0));
$area_approve = My_Util::escape_string($this->getRequest()->getParam('area-approve',0));
$office_approve = My_Util::escape_string($this->getRequest()->getParam('office-approve',0));
$department_approve = My_Util::escape_string($this->getRequest()->getParam('department-approve',0));
$team_approve = My_Util::escape_string($this->getRequest()->getParam('team-approve',0));
$title_approve = My_Util::escape_string($this->getRequest()->getParam('title-approve',0));
$staff_code_approve = My_Util::escape_string($this->getRequest()->getParam('staff-code-approve',''));
$export = My_Util::escape_string($this->getRequest()->getParam('export',0));
$page = $this->getRequest()->getParam('page', 1);
$page1 = $this->getRequest()->getParam('apage', 1);

$param = [
    'tool_name'   =>  $tool_name   ,
    'staff_code'   =>  $staff_code   ,
    'department'   =>  $department   ,
    'team'   =>  $team   ,
    'title'   =>  $title   ,
    'area_approve'   =>  $area_approve   ,
    'office_approve'   =>  $office_approve   ,
    'department_approve'   =>  $department_approve   ,
    'team_approve'   =>  $team_approve   ,
    'title_approve'   =>  $title_approve   ,
    'staff_code_approve'   =>  $staff_code_approve,
    'apage' =>$page1
];

$param1 = [
    'tool_name'   =>  $tool_name   ,
    'staff_code'   =>  $staff_code   ,
    'department'   =>  $department   ,
    'team'   =>  $team   ,
    'title'   =>  $title   ,
    'area_approve'   =>  $area_approve   ,
    'office_approve'   =>  $office_approve   ,
    'department_approve'   =>  $department_approve   ,
    'team_approve'   =>  $team_approve   ,
    'title_approve'   =>  $title_approve   ,
    'staff_code_approve'   =>  $staff_code_approve,
    'page' => $page
];

if(isset($export) && $export == 1){
    $data_e = $QStaffPermissions->getListData(2,$param,0,0,$total);
    $dataStaff_e = $QStaffPermissions->getListData(1,$param,0,0,$total1);
    $this->exportListPermission($data_e,$dataStaff_e);
}

$data = $QStaffPermissions->getListData(2,$param,$page,$limit,$total);
$dataStaff = $QStaffPermissions->getListData(1,$param,$page1,$limit,$total1);

$active = "#titleList";        
if(!empty($data)){
	$active = "#titleList";
}elseif (!empty($dataStaff)){
	$active = "#staffList";
}

$this->view->total = $total;
$this->view->total1 = $total1;
$this->view->limit = $limit;
$this->view->offset = ($page-1)*$limit;
$this->view->offset1 = ($page1-1)*$limit;

$this->view->param  = $param ; 
$this->view->active = $active;

$this->view->data = $data;
$this->view->dataStaff = $dataStaff;
$this->view->recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();;
$this->view->tool_name = $QGroupPermission->fetchAll();;
$this->view->area_list = $QArea->get_cache();
$this->view->office = $QOffice->fetchAllOffice();
$this->view->url = HOST . 'staff-permission/list-permission-final' . ($param ? '?' . http_build_query($param) . '&' : '?');
$this->view->url1 = HOST . 'staff-permission/list-permission-final' . ($param1 ? '?' . http_build_query($param1) . '&a' : '?');

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$messages_error               = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;
$this->_helper->viewRenderer->setRender('list-final');