<?php
$QGroupPermission = new Application_Model_GroupPermission();
$QTypePermission = new Application_Model_TypePermission();
$QOffice    = new Application_Model_Office();
$QArea = new Application_Model_Area();
$QTeam      = new Application_Model_Team();
$QRegionalMarket = new Application_Model_RegionalMarket();

$group_permission=$QGroupPermission->fetchAll();
$type_permission=$QTypePermission->getTypeGroup();
$department_tmp = $QTeam->get_list_department();
$all_office =  $QOffice->fetchAllOffice();
$department = [];
foreach($department_tmp as $key => $value){
    $department[] = [
        'id' => $key, 
        'name' => $value
    ];
}
$this->view->group_permission = $group_permission;
$this->view->type_permission = $type_permission;
$this->view->areas = $QArea->get_cache();
$this->view->team = $QTeam->get_cache();
$this->view->title = $QTeam->get_cache_title();
$this->view->office = $all_office;
$this->view->department = $department;

$flashMessenger = $this->_helper->flashMessenger;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$messages_error               = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;

$this->_helper->viewRenderer->setRender('add-final');