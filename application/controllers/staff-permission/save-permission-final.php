<?php

$flashMessenger = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QStaffPermissions = new Application_Model_StaffPermissions();

$sn = $this->getRequest()->getParam('sn');
$group_permission = $this->getRequest()->getParam('group_permission', 0);
$type = $this->getRequest()->getParam('type_permission');
$value = $this->getRequest()->getParam('value_permission');
$level_manage = $this->getRequest()->getParam('is_manager', 0);
$area_id = $this->getRequest()->getParam('area_id', 0);
$office_id = $this->getRequest()->getParam('office_id', 0);
$department_id = $this->getRequest()->getParam('app_department_id', 0);
$team_id = $this->getRequest()->getParam('app_team_id', 0);
$title_id = $this->getRequest()->getParam('app_title_id', 0);
$staff_id = $this->getRequest()->getParam('staff_id', 0);
$filter_area = $this->getRequest()->getParam('filter_area', 0);

if ($this->getRequest()->getMethod() == 'POST') {

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        // Check validate
        if (empty($group_permission)) {
            throw new Exception("Vui lòng chọn Tool name");
        }

        if ($type == '0') {
            throw new Exception("Vui lòng chọn Type permission");
        } else {
            if ($type == '1') {
                if (empty($value)) {
                    throw new Exception("Vui lòng chọn Staff permission");
                }
            } else if ($type == '2') {
                if ($value == '0') {
                    throw new Exception("Vui lòng chọn Title permission");
                }
            }
        }

        $id_staff_permission = $QStaffPermissions->insert([
            'group_permission' => $group_permission,
            'type' => $type,
            'value' => $value,
            'level_manage' => $level_manage,
            'created_by' => $userStorage->id,
            'created_at' => date('Y-m-d H-i-s')
        ]);

        //insert data detail
        $data = array(
            My_StaffPermissions::TYPE_AREA_ID => $area_id,
            My_StaffPermissions::TYPE_PROVINCE_ID => $province_id,
            My_StaffPermissions::TYPE_DISTRICT_ID => $district_id,
            My_StaffPermissions::TYPE_DEPARTMENT_ID => $department_id,
            My_StaffPermissions::TYPE_TEAM_ID => $team_id,
            My_StaffPermissions::TYPE_TITLE_ID => $title_id,
            My_StaffPermissions::TYPE_OFFICE_ID => $office_id,
            My_StaffPermissions::TYPE_STAFF_ID => $staff_id,
            My_StaffPermissions::TYPE_FILTER_AREA_ID => $filter_area
        );
        $check_data_detail = 0;
        $data_staff_permission_detail = [];
        foreach ($data as $k => $val) {
            if (!empty($val) && isset($val)) {
                if (is_array($val)) {
                    foreach ($val as $value_d) {
                        $check_data_detail = 1;
                        $data_staff_permission_detail[] = [
                            'id_staff_permissions' => $id_staff_permission,
                            'type_app' => $k,
                            'value_app' => $value_d
                        ];
                    }
                } else {
                    $check_data_detail = 1;
                    $data_staff_permission_detail[] = [
                        'id_staff_permissions' => $id_staff_permission,
                        'type_app' => $k,
                        'value_app' => $val
                    ];
                }
            }
        }
        if($check_data_detail == 0){
            throw new Exception("Vui lòng chọn ít nhất một thông tin approve");
        }

        if(!empty($data_staff_permission_detail)){
            My_Controller_Action::insertAllrowDB($data_staff_permission_detail,'staff_permission_detail',$db);
        }
        
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Thành công');
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/staff-permission/create-permission-final');
    }
    $this->_redirect("/staff-permission/list-permission-final");

    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_success = $messages_success;
    $this->view->messages_error = $messages_error;
}
