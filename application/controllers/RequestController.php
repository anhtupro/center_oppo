<?php
class RequestController extends My_Controller_Action
{
    public function createRequestAction()
    {
        require_once 'request' . DIRECTORY_SEPARATOR . 'create-request.php';
    }
    public function confirmRequestAction()
    {
        require_once 'request' . DIRECTORY_SEPARATOR . 'confirm-request.php';
    }
    public function saveCreateRequestAction()
    {
        require_once 'request' . DIRECTORY_SEPARATOR . 'save-create-request.php';
    }
    public function listRequestAction(){
       require_once 'request' . DIRECTORY_SEPARATOR . 'list-request.php';
    }
    public function editRequestAction(){
       require_once 'request' . DIRECTORY_SEPARATOR . 'edit-request.php';
    }
   
}