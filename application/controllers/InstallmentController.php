<?php

class InstallmentController extends My_Controller_Action {

    public function viewProductAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'view-product.php';
    }

    public function ajaxViewPackageAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'ajax-view-package.php';
    }

    public function listPackageAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'list-package.php';
    }

    public function editPackageAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'edit-package.php';
    }

    public function deletePackageAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'delete-package.php';
    }

    public function createPackageAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'create-package.php';
    }

    public function copyPackageAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'copy-package.php';
    }

    public function uploadFileAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'upload-file.php';
    }

    public function downloadFileTemplateAction()
    {
        require_once 'installment' . DIRECTORY_SEPARATOR . 'download-file-template.php';
    }
}
