<?php

class InventoryCampaignController extends My_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('layout_new');
    }
    public function checkShopAction()
    {
        require_once 'inventory-campaign' . DIRECTORY_SEPARATOR . 'check-shop.php';
    }

    public function saveCheckShopAction()
    {
        require_once 'inventory-campaign' . DIRECTORY_SEPARATOR . 'save-check-shop.php';
    }



    public function createCampaignAction()
    {
        require_once 'inventory-campaign' . DIRECTORY_SEPARATOR . 'create-campaign.php';
    }

    public function saveCreateCampaignAction()
    {
        require_once 'inventory-campaign' . DIRECTORY_SEPARATOR . 'save-create-campaign.php';
    }

    public function listCampaignAction()
    {
        require_once 'inventory-campaign' . DIRECTORY_SEPARATOR . 'list-campaign.php';
    }

    public function saveDeleteCampaignAction()
    {
        require_once 'inventory-campaign' . DIRECTORY_SEPARATOR . 'save-delete-campaign.php';
    }
}