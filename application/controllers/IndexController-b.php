<?php

class IndexController extends My_Controller_Action
{

    public function init()
    {
        // parent::init();
    }

    /**
     * Dashboard
     */
    public function indexAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        // lấy thông tin user đăng nhập
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QDashboard  = new Application_Model_Dashboard();
        $group_id    = $userStorage->group_id;
        $user_id     = $userStorage->id;
         if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
            ADMINISTRATOR_ID,
            HR_ID,
            HR_EXT_ID,
            BOARD_ID,
            SALES_ADMIN_ID)))

        {


            // lọc dashboard theo user id
            $where_1 = $QDashboard->getAdapter()->quoteInto('FIND_IN_SET(?, staff_ids)', $userStorage->
                id);

            // trường hợp user này đc thêm vào nhóm rồi thì lọc thêm theo group
            if (isset($userStorage->group_id))
            {
                $where_2 = $QDashboard->getAdapter()->quoteInto('FIND_IN_SET(?, group_ids)', $userStorage->
                    group_id);
                $dashboards = $QDashboard->fetchAll(' ( ' . $where_1 . ' OR ' . $where_2 .
                    ' ) AND status = 1');

            } else
            {
                $dashboards = $QDashboard->fetchAll($where_1 . '  AND status = 1');
            }

            /**
             * các ô dashboard
             * @var array
             */
            $regions = array();

            $QStaff = new Application_Model_Staff();
            $QReligion = new Application_Model_Religion();
            $QTeam = new Application_Model_Team();
            $QNationality = new Application_Model_Nationality();
            $QArea = new Application_Model_Area();
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $QGroup = new Application_Model_Group();
            $QContractTerm = new Application_Model_ContractTerm();
            $QContractType = new Application_Model_ContractType();
            $QStaffLog = new Application_Model_StaffLog();
            $QStaff = new Application_Model_Staff();

            $this->view->staffs = $QStaff->get_cache();
            $this->view->religion = $QReligion->get_cache();
            $this->view->team = $QTeam->get_cache();
            $this->view->nationality = $QNationality->get_cache();
            $this->view->area = $QArea->get_cache();
            $this->view->regionalMarket = $QRegionalMarket->get_cache_all();
            $this->view->group = $QGroup->get_cache();
            $this->view->contractTerm = $QContractTerm->get_cache();
            $this->view->contractType = $QContractType->get_cache();

            $fields = array(
                's.id',
                's.code',
                's.email',
                's.firstname',
                's.lastname',
                's.regional_market',
                's.title',
                's.department',
                's.team',
                's.joined_at',
            );

            //	Lấy danh sách các staff theo điều kiện riêng ở mỗi dasboard
            foreach ($dashboards as $key => $value)
            {
                $cols_existed = explode(',', $value['cols_existed']);
                $cols_needed = explode(',', $value['cols_needed']);

                /*if ($value['id'] == My_Dashboard_Dashboards::EMAIL_CREATE) {
                    $QDashboardStaff = new Application_Model_DashboardStaff();
                    $regions[$value['id']] = $QDashboardStaff->fetchStaffById($value['id'], $fields);

                } else*/
                if ($value['table'] == 'recent') {
                    $page = $this->getRequest()->getParam('page', 1);
                    $limit = 10;
                    $total = 0;
                    $params = array();
                    $log_res = $QStaffLog->fetchPagination($page, $limit, $total, $params);
                    $logs = array();

                    foreach ($log_res as $k => $log)
                    {
                        $logs[] = array(
                            'time'    => $log['time'],
                            'user_id' => $log['user_id'],
                            'ip'      => $log['ip_address'],
                            'before'  => unserialize($log['before']),
                            'after'   => unserialize($log['after']),
                            'diffs'   => array(),
                            'type'    => $log['type'],
                            'object'  => $log['object'],
                        );
                    }

                    $n = count($logs);

                    for ($i = 0; $i < $n; $i++)
                    {
                        $logs[$i]['diffs'] = array_diff_assoc($logs[$i]['after'], $logs[$i]['before']);
                    }

                    $regions[$value['id']] = $logs;
                } elseif ($value['table'] == 'off')
                {
                    $where = array();
                    $where[] = $QStaff->getAdapter()->quoteInto('off_date IS NOT NULL AND off_date <> 0 AND off_date <> \'\'',
                        1);
                    $where[] = $QStaff->getAdapter()->quoteInto('old_email IS NOT NULL', 1);
                    $regions[$value['id']] = $QStaff->fetchAll($where);
                }
                elseif ($value['table'] == 'staff_transfer')
                {
                    $this->view->staff_print_contract = 1;
                }

                else
                {
                    $regions[$value['id']] = $QStaff->get_by_step($cols_existed, $cols_needed, $value['table']);
                }

            }

            // bỏ các staff thuộc danh sách dismiss
            $QDismiss = new Application_Model_DashboardDismiss();
            // $where = $QDismiss->getAdapter()->quoteInto('user_id = ?', $userStorage->id);
            $dismisses = $QDismiss->fetchAll();

            /**
             * Danh sách staff dismiss theo dashboard
             * @var array
             */
            $dismiss = array();

            foreach ($dismisses as $key => $value)
            {
                if (!isset($dismiss[$value['dashboard_id']]))
                {
                    $dismiss[$value['dashboard_id']] = array();
                }

                $dismiss[$value['dashboard_id']][] = $value['staff_id'];
            }

            $this->view->dashboards = $dashboards;
            $this->view->dismiss = $dismiss;
            $this->view->regions = $regions;
            $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER') ? $this->
                getRequest()->getServer('HTTP_REFERER') : HOST;
        } else{
            //load dashboard cho nhân viên
            $QModel         = new Application_Model_Inform();
            $QStaff         = new Application_Model_Staff();
            $QTime          = new Application_Model_Time();
            $QTiming        = new Application_Model_Timing();
            $QTimeOffAdd    = new Application_Model_OffDateAdd();
            $QDepartment    = new Application_Model_Department();
            $QContractTerm  = new Application_Model_ContractTerm();
            $QNofitfication = new Application_Model_Notification();
            $QOffHistory    = new Application_Model_OffHistory();

            $StaffRowSet = $QStaff->find($user_id);
            $staff = $StaffRowSet->current();


            $QArea = new Application_Model_Area();
            $this->view->areas = $QArea->get_cache();

            $month = date('m');
            $year  = date('Y');

            $QRegionalMarket = new Application_Model_RegionalMarket();
            $this->view->all_province_cache = $QRegionalMarket->get_cache();

            //get area & province
            $rowset = $QRegionalMarket->find($staff->regional_market);

            if ($rowset)
            {
                $this->view->regional_market = $regional_market = $rowset->current();
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);

                $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

                $rowset = $QArea->find($regional_market['area_id']);
                $this->view->area = $rowset->current();
            }

            $day = 0;
            $params = array('user_id' => $user_id, 'month' => $month , 'year' => $year);

            if (My_Staff::isPgTitle($staff['title']))
            {
                $day = $QTime->getDayDashboard($params);
            } else
            {
                $day = $QTime->getDayDashboard($params);
            }

            $sabbatical = $QTimeOffAdd->getTotalOff($user_id);
            $first_day_this_month = date('Y-m-01');
            $last_day_this_month = date('Y-m-t');

            $QGood = new Application_Model_Good();
            $this->view->goods = $QGood->get_cache();
            
            $QGoodColor = new Application_Model_GoodColor();
            $this->view->good_colors = $QGoodColor->get_cache();

            $sell_out = My_Kpi::fetchGrid($userStorage->id, $first_day_this_month, $last_day_this_month);

            $this->view->contract_term = $QContractTerm->get_cache();
            $department = $QDepartment->get_cache();
            $params = array(
                'staff_id' => $userStorage->id,
                'filter'   => true,
                'status'   => 1,
                );

            $store_list = My_Staff::getOwnStores($userStorage->id);
            $list_imei_expired = My_Staff::getImeiExpired($userStorage->id);
            $store_list_array = array();

            foreach ($store_list as $k => $v)
            {
                $store_list_array[$k]['store_name'] = $v['store_name'];
                $store_list_array[$k]['from'] = $v['from'];
                $store_list_array[$k]['to'] = $v['to'];
            }


            $params['group_cat'] = 1;
            $params['filter_display'] = 1;
            $page = 1;
            $limit = 5;
            $total = 0;
            $inform = $QModel->fetchPagination($page, $limit, $total, $params);

            //$notifications = $QNofitfication->fetchPagination($page, $limit, $total, $params);
            $notifications=null;
            $params = array('name' => $user_id);

            $off = $QOffHistory->fetchPagination($page, $limit, $total, $params);


            if (isset($off) and $off)
            {
                $this->view->off = $off;
            }

            $this->view->list_expired  = $list_imei_expired;
            $this->view->store_list    = $store_list_array;
            $this->view->notifications = $notifications;
            $this->view->sellout       = $sell_out;
            $this->view->sabbatical    = $sabbatical;
            $this->view->day           = $day;
            $this->view->department    = $department;
            $this->view->staff         = $staff;
            $this->view->informs       = $inform;
            $this->_helper->viewRenderer('index/dashboard', null, true);
            $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER') ? $this->
                getRequest()->getServer('HTTP_REFERER') : HOST;
        }


        $flashMessenger             = $this->_helper->flashMessenger;
        $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
        $messages                   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages       = $messages;
        $this->view->messages_error = $messages_error;

        // -----

        $ik = new Application_Model_NewImeiKpi();
        $data = array(
            'staff_id' => $userStorage->id,
            'from' => date('Y-m-01 00:00:00'),
            'to' => date('Y-m-d H:i:s')
        );
        $data = $ik->GetAll($data);
        $this->view->res = $data;
    }

    public function eventTimeAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QTime = new Application_Model_Time();
        $data = $QTime->getTimeByStaffInCurrentMonth($userStorage->id);
        $array_date = array();
        foreach($data as $value)
        {
            $array_date[] = array(
                'title' => 'Check-in',
                'start' => $value['date'],
                'end' => $value['date'],
            );
        }
        echo json_encode($array_date);
        die;
    }
}
