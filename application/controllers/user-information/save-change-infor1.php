<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$ID_number = $this->getRequest()->getParam('ID_number');
$id_place_province = $this->getRequest()->getParam('id_place_province');
$ID_date = $this->getRequest()->getParam('ID_date');
$phone_number = $this->getRequest()->getParam('phone_number');
$id_card_district = $this->getRequest()->getParam('id_card_district');
$id_card_province = $this->getRequest()->getParam('id_card_province');
$id_card_ward = $this->getRequest()->getParam('id_card_ward');
$id_card_ward_id = $this->getRequest()->getParam('id_card_ward_id');
$id_card_address = $this->getRequest()->getParam('id_card_address');
$id_photo = $this->getRequest()->getParam('id_photo');
$id_photo_back = $this->getRequest()->getParam('id_photo_back');

$flashMessenger = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staffId = $userStorage->id;

$QStaff = new Application_Model_Staff();
$QStaffTempNew = new Application_Model_StaffTempNew();

$currentStaff = $QStaff->fetchRow($QStaff->getAdapter()->quoteInto('id = ?', $staffId))->toArray();
$currentPhoto = $currentStaff['photo'];
$currentIdPhoto = $currentStaff['id_photo'];
$currentIdPhotoBack = $currentStaff['id_photo_back'];


        $db = Zend_Registry::get('db');
        $db->beginTransaction();
foreach ($_FILES as $imageType => $imageInfor) {
    // check if user upload each type of image
    if (!$imageInfor['name']) {
        continue;
    }
    $targetDir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $staffId;
    $extension = strtolower(pathinfo($imageInfor['name'], PATHINFO_EXTENSION));
    $newName = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

    // kiểm tra loại hình ảnh và set folder để lưu
    if ($imageType == 'photo') {
        $message = 'Cập nhật ảnh thẻ thành công';
        $targetDir = $targetDir;
    }
    if ($imageType == 'id_photo') {
        $message = 'Cập nhật ảnh mặt trước CMND thành công';
        $targetDir = $targetDir . DIRECTORY_SEPARATOR . 'ID_Front';
    }
    if ($imageType == 'id_photo_back') {
        $message = 'Cập nhật ảnh mặt sau CMND thành công';
        $targetDir = $targetDir . DIRECTORY_SEPARATOR . 'ID_Back';
    }

    if (!is_dir($targetDir)) {
        @mkdir($targetDir, 0775, true);
    }

    $targetFile = $targetDir . DIRECTORY_SEPARATOR . $newName;

    // Check file size
    if ($imageInfor["size"] > 5000000) {
        $flashMessenger->setNamespace('error')->addMessage('Ảnh có dung lượng vượt quá 5MB');
        $this->_redirect('/user-information/index');
    }
    // Check file type
    if ($extension != "jpg" && $extension != "png" && $extension != "jpeg") {
        $flashMessenger->setNamespace('error')->addMessage('Ảnh sai định dạng');
        $this->_redirect('/user-information/index');
    }
    //save image
    if (move_uploaded_file($imageInfor["tmp_name"], $targetFile)) {
        $arrPhoto [$imageType] = $newName;
        $flashMessenger->setNamespace('success')->addMessage($message);
    } else {
        $flashMessenger->setNamespace('error')->addMessage('Lỗi trong quá trình lưu ảnh');
        $this->_redirect('/user-information/index');
    }
}

if (!$currentIdPhoto && !$currentIdPhotoBack) {
    $QStaff->update($arrPhoto, $QStaff->getAdapter()->quoteInto('id = ?', $staffId));
} else {
    if ($arrPhoto['photo'] && ! $arrPhoto['id_photo'] && ! $arrPhoto['id_photo_back']) { // if user upload  avatar photo only
        $QStaff->update([
            'photo' => $arrPhoto['photo']
        ], $QStaff->getAdapter()->quoteInto('id = ?', $staffId));

        $db->commit();
        $this->_redirect('/user-information/index');
    }

    // user update CMND and phone number information to approve
    $is_head_office = $QStaff->isHeadOffice($userStorage->id);

    $edited_staff = $QStaffTempNew->is_exist($staffId);
    if (! $edited_staff) {
        $data = [
            'staff_id' => $staffId,
            'ID_number' => $ID_number ? $ID_number : Null,
            'id_place_province' => $id_place_province ? $id_place_province : Null,
            'ID_date' => $ID_date ? date('Y-m-d',strtotime(str_replace('/','-', $ID_date))) : Null,
            'phone_number' => $phone_number ? $phone_number : null,
            'id_card_district' => $id_card_district ? $id_card_district : Null,
            'id_card_province' => $id_card_province ? $id_card_province : Null,
            'id_card_ward' => $id_card_ward ? $id_card_ward : Null,
            'id_card_ward_id' => $id_card_ward_id ? $id_card_ward_id : null,
            'id_card_address' => $id_card_address ? $id_card_address : Null,
            'id_photo'   => $arrPhoto['id_photo'] ? $arrPhoto['id_photo'] : $currentIdPhoto,
            'id_photo_back'   => $arrPhoto['id_photo_back'] ? $arrPhoto['id_photo_back'] : $currentIdPhotoBack,
            'is_approved' => $is_head_office ? 1 : 0,
            'created_at' => date('Y-m-d H:i:s')
        ];
        $QStaffTempNew->insert($data);
    }

}

        $db->commit();

$this->_redirect('/user-information/index');

