<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$ID_number = $this->getRequest()->getParam('ID_number');
$id_place_province = $this->getRequest()->getParam('id_place_province');
$ID_date = $this->getRequest()->getParam('ID_date');
$ID_date = $ID_date ? str_replace('/', '-', $ID_date) : Null;

$phone_number = $this->getRequest()->getParam('phone_number');
$id_card_district = $this->getRequest()->getParam('id_card_district');
$id_card_province = $this->getRequest()->getParam('id_card_province');
$id_card_ward = $this->getRequest()->getParam('id_card_ward');
$id_card_ward_id = $this->getRequest()->getParam('id_card_ward_id');
$id_card_address = $this->getRequest()->getParam('id_card_address');
$id_photo = $this->getRequest()->getParam('id_photo');
$id_photo_back = $this->getRequest()->getParam('id_photo_back');
$card_nationality = $this->getRequest()->getParam('card_nationality');
$card_religion = $this->getRequest()->getParam('card_religion');
$marital_status = $this->getRequest()->getParam('marital_status');

$flashMessenger = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staffId = $userStorage->id;

$QStaff = new Application_Model_Staff();
$QStaffTempNew = new Application_Model_StaffTempNew();

$currentStaff = $QStaff->fetchRow($QStaff->getAdapter()->quoteInto('id = ?', $staffId))->toArray();
$currentPhoto = $currentStaff['photo'];
$currentIdPhoto = $currentStaff['id_photo'];
$currentIdPhotoBack = $currentStaff['id_photo_back'];
$ID_card_infor = $QStaff->getIDCardInfor($staffId);

$db = Zend_Registry::get('db');
$db->beginTransaction();
require_once "Aws_s3.php";
$s3_lib = new Aws_s3();
foreach ($_FILES as $imageType => $imageInfor) {
    // check if user upload each type of image
    if (!$imageInfor['name']) {
        continue;
    }
    $targetDir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $staffId;
    $extension = strtolower(pathinfo($imageInfor['name'], PATHINFO_EXTENSION));
    $newName = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
    $targets3 = 'photo/staff/'.$staffId;

    // kiểm tra loại hình ảnh và set folder để lưu
    if ($imageType == 'photo') {
        $message = 'Cập nhật ảnh thẻ thành công';
        $targetDir = $targetDir;
        $targets3 = $targets3;
    }
    if ($imageType == 'id_photo') {
        $message = 'Cập nhật ảnh mặt trước CMND thành công';
        $targetDir = $targetDir . DIRECTORY_SEPARATOR . 'ID_Front';
        $targets3 = $targets3.'/ID_Front';
    }
    if ($imageType == 'id_photo_back') {
        $message = 'Cập nhật ảnh mặt sau CMND thành công';
        $targetDir = $targetDir . DIRECTORY_SEPARATOR . 'ID_Back';
        $targets3 = $targets3.'/ID_Back';
    }

    if (!is_dir($targetDir)) {
        @mkdir($targetDir, 0775, true);
    }

    $targetFile = $targetDir . DIRECTORY_SEPARATOR . $newName;

    // Check file size
    if ($imageInfor["size"] > 5000000) {
        $flashMessenger->setNamespace('error')->addMessage('Ảnh có dung lượng vượt quá 5MB');
        $this->_redirect('/user-information/index');
    }
    // Check file type
    if ($extension != "jpg" && $extension != "png" && $extension != "jpeg") {
        $flashMessenger->setNamespace('error')->addMessage('Ảnh sai định dạng');
        $this->_redirect('/user-information/index');
    }
    //save image
    if (move_uploaded_file($imageInfor["tmp_name"], $targetFile)) {
        $arrPhoto [$imageType] = $newName;
        //up s3
        $file_upload = $targetFile;
        $destination =  $targets3. '/';// s3 su dung dir /
        $s3_lib->uploadS3($file_upload, $destination, $newName);
        
        $flashMessenger->setNamespace('success')->addMessage($message);
    } else {
        $flashMessenger->setNamespace('error')->addMessage('Lỗi trong quá trình lưu ảnh');
        $this->_redirect('/user-information/index');
    }
}

$oldInformation = [
    'phone_number' => $currentStaff ['phone_number'],
    'ID_number' => $currentStaff ['ID_number'],
    'id_place_province' => $currentStaff ['id_place_province'],
    'ID_date' => date('Y-m-d', strtotime($currentStaff ['ID_date'])),
    'id_photo' => $currentStaff ['id_photo'],
    'id_photo_back' => $currentStaff ['id_photo_back'],
    'id_card_province' => $ID_card_infor ['id_card_province'],
    'id_card_district' => $ID_card_infor ['id_card_district'],
    'id_card_ward_id' => $ID_card_infor ['id_card_ward_id'],
    'street' => $ID_card_infor ['street'],
    'nationality' => $currentStaff ['nationality'],
    'religion' => $currentStaff ['religion'],
    'marital_status' => $currentStaff ['marital_status']
];
$newInformation = [
    'phone_number' => $phone_number,
    'ID_number' => $ID_number,
    'id_place_province' => $id_place_province,
    'ID_date' => $ID_date ? date('Y-m-d', strtotime($ID_date)) : Null,
    'id_photo' => $arrPhoto['id_photo'] ? $arrPhoto['id_photo'] : $currentStaff ['id_photo'],
    'id_photo_back' => $arrPhoto['id_photo_back'] ? $arrPhoto['id_photo_back'] : $currentStaff ['id_photo_back'],
    'id_card_province' => $id_card_province,
    'id_card_district' => $id_card_district,
    'id_card_ward_id' => $id_card_ward_id,
    'street' => $id_card_address,
    'nationality' => $card_nationality,
    'religion' => $card_religion,
    'marital_status' => $marital_status
];

if (!$currentIdPhoto && !$currentIdPhotoBack) {
    if($arrPhoto['photo']){
        $QStaff->update([
            'photo' => $arrPhoto['photo']
        ], $QStaff->getAdapter()->quoteInto('id = ?', $staffId));
    }
    $QStaff->update($arrPhoto, $QStaff->getAdapter()->quoteInto('id = ?', $staffId));
} else {
    if ($arrPhoto['photo'] && !$arrPhoto['id_photo'] && !$arrPhoto['id_photo_back']) { // if user upload  avatar photo only
        $QStaff->update([
            'photo' => $arrPhoto['photo']
        ], $QStaff->getAdapter()->quoteInto('id = ?', $staffId));

        $db->commit();
        $this->_redirect('/user-information/index');
    }

    if($arrPhoto['photo']){
        $QStaff->update([
            'photo' => $arrPhoto['photo']
        ], $QStaff->getAdapter()->quoteInto('id = ?', $staffId));
    }

    // user update CMND and phone number information to approve
    $is_head_office = $QStaff->isHeadOffice($userStorage->id);

    $edited_staff = $QStaffTempNew->is_exist($staffId);
    if ($oldInformation != $newInformation) { // check  user có thay đổi thông tin gì không
        if (!$edited_staff) {
            $data = [
                'staff_id' => $staffId,
                'ID_number' => $ID_number ? $ID_number : Null,
                'id_place_province' => $id_place_province ? $id_place_province : Null,
                'ID_date' => $ID_date ? date('Y-m-d', strtotime(str_replace('/', '-', $ID_date))) : Null,
                'phone_number' => $phone_number ? $phone_number : null,
                'id_card_district' => $id_card_district ? $id_card_district : Null,
                'id_card_province' => $id_card_province ? $id_card_province : Null,
                'id_card_ward' => $id_card_ward ? $id_card_ward : Null,
                'id_card_ward_id' => $id_card_ward_id ? $id_card_ward_id : null,
                'id_card_address' => $id_card_address ? $id_card_address : Null,
                'nationality' => $card_nationality ? $card_nationality : null,
                'religion' => $card_religion ? $card_religion : null,
                'marital_status' => $marital_status ? $marital_status : null,
                'id_photo' => $arrPhoto['id_photo'] ? $arrPhoto['id_photo'] : $currentIdPhoto,
                'id_photo_back' => $arrPhoto['id_photo_back'] ? $arrPhoto['id_photo_back'] : $currentIdPhotoBack,
                'is_approved' => $is_head_office ? 1 : 0,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $QStaffTempNew->insert($data);
        }
    }

}

$db->commit();

$this->_redirect('/user-information/index');

