<?php
    $staff_id = $this->getRequest()->getParam('staff_id');
    $manager_id = $this->getRequest()->getParam('manager_id');
    $submit = $this->getRequest()->getParam('submit', 0);

    if(!empty($submit))
    {
        $sql = "INSERT INTO `personal_permission` (`line_manager`, `staff_id`)
                VALUES (:manager_id, :staff_id)
                ON DUPLICATE KEY UPDATE staff_id = VALUES(`staff_id`)";

        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('manager_id', $manager_id, PDO::PARAM_INT);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();

        $stmt->closeCursor();
        $stmt = $db = null;
        $this->_redirect('/time-machine/personal-permission');
    }