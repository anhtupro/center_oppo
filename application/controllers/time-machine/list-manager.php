<?php
    $month          = $this->getRequest()->getParam('month', intval(date('m')));
    $year           = $this->getRequest()->getParam('year', intval(date('Y')));
    $name           = $this->getRequest()->getParam('name');
    $code           = $this->getRequest()->getParam('code');
    $export         = $this->getRequest()->getParam('export',NULL);
    $export_all     = $this->getRequest()->getParam('export_all',0);
    $export_detail  = $this->getRequest()->getParam('export_detail',0);

    $department     = $this->getRequest()->getParam('department');
    $team           = $this->getRequest()->getParam('team');
    $title          = $this->getRequest()->getParam('title');
    $area           = $this->getRequest()->getParam('area');

    $QTeam          = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $QArea          = new Application_Model_Area();
    $all_area       =  $QArea->fetchAll(null, 'name');
    $this->view->areas = $all_area;
    $QLogSendMailManager = new Application_Model_LogSendMailManager();
    $QLogStaffWarning = new Application_Model_LogStaffWarning();
    $db = Zend_Registry::get('db');
    $where_red = array();
    $where_red[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year" );
    $where_red[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "MONTH(penalty_day) = $month" );
    $where_red[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "warning_type = 3" );
    $list_red    = $QLogStaffWarning->fetchAll($where_red);

    $arr_locked_red = array();
    if(!empty($list_red)){
        foreach ($list_red->toArray() as $k => $val){
            $arr_locked_red[]=$val['staff_id'];
        }
    }
    $where_sendMail = array();
    $where_sendMail[] = $QLogSendMailManager->getAdapter ()->quoteInto ( "year = $year" );
    $where_sendMail[] = $QLogSendMailManager->getAdapter ()->quoteInto ( "month = $month" );
    $list_sendMail   = $QLogSendMailManager->fetchAll($where_sendMail);

    $arr_sendMail = array();
    if(!empty($list_sendMail)){
        foreach ($list_sendMail->toArray() as $k => $val){
            $arr_sendMail[]=$val['staff_id'];
        }
    }
    $data = array();
    if(!empty($arr_locked_red)){
        $str_staff_red = implode(",", $arr_locked_red);
        $sql1 = "SELECT st.id,	st.`code`,
                    st2.`id` as staff_id_manager,
                    CONCAT(st.firstname,' ', st.lastname) AS 'staff_name',
                    CONCAT(st2.firstname, ' ',st2.lastname) AS 'staff_name_leader',
                    st2.code as staff_code,
                    0 AS is_leader,
                    0 AS is_manager,
                    st2.`id` is_manager,
                    st2.`code` code_manager
            FROM
                    staff st
            INNER JOIN personal_permission pp ON st.`id` = pp.staff_id
            INNER JOIN staff st2 ON pp.`line_manager` = st2.`id`

            WHERE
                    st.id IN (:p_str_staff_red)
            ";
      
        $stmt1 = $db->prepare($sql1);
        $stmt1->bindParam('p_str_staff_red', My_Util::escape_string($str_staff_red), PDO::PARAM_STR);
        $stmt1->execute();
        $data1 = $stmt1->fetchAll();
        $stmt1->closeCursor();
      
        $sql = "SELECT * from (
	SELECT
				st.`id`,
				st.`code`,
				st2.`id` as staff_id_manager,
				CONCAT(st.firstname, ' ', st.lastname ) AS 'staff_name',
				CONCAT( st2.firstname, ' ', st2.lastname ) AS 'staff_name_leader',
				IFNULL(b.staff_code,b1.staff_code) staff_code,
				IFNULL(b.is_leader,0) is_leader,
				IF(IFNULL(b.is_leader,0) = 0, 1, 0) is_manager ,
				IFNULL(b.staff_code,b1.staff_code) AS code_manager
			 FROM
						 staff st
						 LEFT JOIN ( 
									SELECT * FROM staff_permission WHERE `is_leader` = 1 and staff_code not in (13080027, 18110144,'13080002')
						 ) b ON b.department_id = st.`department` AND (IF(st.title = 341 , b.staff_code = '14080121', 1 = 1  ) ) AND (IFNULL(b.team_id, 0) = 0 OR st.team = b.team_id) AND (IF(b.staff_code = '1304KH41', st.title = b.title_id, (IFNULL(b.title_id, 0) = 0 OR st.title = b.title_id)  )  ) AND ( b.is_all_office = 1 OR st.office_id = b.office_id) and st.`code` <> b.`staff_code`
						 LEFT JOIN ( 
									SELECT * FROM staff_permission WHERE `is_manager` = 1
						 ) b1 ON b1.department_id = st.`department` AND (IF(st.title = 341 , b1.staff_code = '14080121', 1 = 1  ) ) AND (IFNULL(b1.team_id, 0) = 0 OR st.team = b1.team_id) AND (IF(b1.staff_code = '1304KH41', st.title = b1.title_id, (IFNULL(b1.title_id, 0) = 0 OR st.title = b1.title_id)  )  ) AND ( b1.is_all_office = 1 OR st.office_id = b1.office_id)
							
				LEFT JOIN staff st2 ON IFNULL(b.staff_code,b1.staff_code) = st2.`code`
			 WHERE
				
				st.id IN ($str_staff_red)
			 AND st.`code` <> st2.`code`
			 AND st2.`code` NOT IN ('15010021', '13080027', '18110144', '14080121', '19080429', '13080002')
			
				
				GROUP BY st.`code` ,  st2.`code`
			   ORDER BY st.`code`, st2.title ASC
)		b
GROUP BY b.`code`

";
        $stmt = $db->prepare($sql);
//        $stmt->bindParam('str_staff_red', My_Util::escape_string($str_staff_red), PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $arr_staff = [];
      foreach ($data as $k => $val){
            $arr_staff[]=$val['id'];
        }
          $list_staff_str =  implode(",",$arr_staff);
//          echo $list_staff_str;
        $sql1 = "
                SELECT
                st.`id`,
                st.`code`,
                st2.`id` as staff_id_manager,
                CONCAT(st.firstname, ' ', st.lastname ) AS 'staff_name',
                CONCAT( st2.firstname, ' ', st2.lastname ) AS 'staff_name_leader',
                b.staff_code,
                b.is_leader,
                b.is_manager ,
                IF(b.is_leader = 1,b.staff_code,b.staff_code) code_manager
               FROM
                staff st
               LEFT JOIN (
                SELECT
                 *
                FROM
                 staff_permission
                WHERE
                 `is_leader` > 0
                OR `is_manager` > 0 
                ORDER BY `is_leader` DESC

               ) b ON b.department_id = st.`department`
               AND (IF(st.title = 341 ,
                    b.staff_code = '14080121', 1 = 1 
                ) )
               AND (
                IFNULL( b.team_id, 0) = 0
                OR st.team = b.team_id
               )
               AND (
               IF(b.staff_code = '1304KH41', st.title = b.title_id, (IFNULL(b.title_id, 0) = 0
                OR st.title = b.title_id)  )
               )
               AND (
                b.is_all_office = 1
                OR st.office_id = b.office_id
               )
               LEFT JOIN staff st2 ON b.`staff_code` = st2.`code`
               WHERE
                st.id NOT IN ($list_staff_str)
                AND st.id  IN ($str_staff_red)
                    
               AND st.`code` <> st2.`code`
               AND st2.`code`IN ( '14080121')
          
                GROUP BY st.`code` ,  st2.`code`
			   ORDER BY st.`code`, code_manager ";
        $stmt1 = $db->prepare($sql1);
        $stmt1->execute();
        $data2 = $stmt1->fetchAll();
        $stmt1->closeCursor();

    }

    $params = array(
    'name' => $name,
    'code' => $code,
    'email' => $email,
    'month' => $month,
    'year' => $year,
    'area' => $area,
    'department' => $department,
    'team' => $team,
    'title' => $title
    );
    $dataTong = array_merge($data1, $data, $data2);
        $sql_del = "DELETE FROM `sendEmailManager` WHERE 1";
        $stmtDel = $db->prepare($sql_del);
        $stmtDel->execute();
        $stmtDel->closeCursor();

if(!empty($data1)){
    My_Controller_Action::insertAllrow($data1, 'sendEmailManager');
}
if(!empty($data)){
    My_Controller_Action::insertAllrow($data, 'sendEmailManager');
}
if(!empty($data2)){
 My_Controller_Action::insertAllrow($data2, 'sendEmailManager');
}


   

    $sql_data = "SELECT a.*, te.name as department, te1.name as team, t3.name as title FROM `sendEmailManager` a INNER JOIN staff st on st.id = a.id INNER JOIN team te on te.id = st.department INNER JOIN team te1 on te1.id = st.team INNER JOIN team t3 on t3.id = st.title WHERE 1 order by st.department, a.`code_manager` , st.team, st.title";
    $stmtData = $db->prepare($sql_data);
    $stmtData->execute();
    $dataAll = $stmtData->fetchAll();
    $stmtData->closeCursor();
    if(!empty($export)){
        ini_set("memory_limit", -1);
            ini_set("display_error", 0);
            error_reporting(~E_ALL);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'STT',
                'CODE',
                'NAME',
                'DEPARTMENT',
                'TEAM',
                'TITLE',            
                'MANAGER',            
            );
            
            $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;
        $i = 1;
        
        foreach($dataAll as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['code']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['department']);
            $sheet->setCellValue($alpha++.$index, $item['team']);
            $sheet->setCellValue($alpha++.$index, $item['title']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name_leader']);
            $index++;
        }
        $filename = 'List_manager'.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }
    $QTestEmail =  new Application_Model_TestEmail();
    $test_email = $QTestEmail->fetchRow();
    if($test_email){
        $this->view->is_test         = 1;
    }
    $this->view->params = $params;
//    $this->view->data   = array_merge($data1, $data, $data2);
//    $this->view->data   = $dataTong;
    $this->view->data   = $dataAll;
    $this->view->arr_sendMail   = $arr_sendMail;