<?php
$submit         = $this->getRequest()->getParam('submit');
if(!empty($submit)){

    define('START_ROW', 2);
    define('ID_TEMP_TIME',0);// cột A;
    define('ID_TIME_LATE',1);// cột B;

    $this->_helper->layout->disableLayout();
    $flashMessenger = $this->_helper->flashMessenger;
    $QTempTime = new Application_Model_TempTime();
    $QTimeLate = new Application_Model_TimeLate();
    
    $QStaff            = new Application_Model_Staff();
    
    $type       = $this->getRequest()->getParam('type');
    set_time_limit(0);
    ini_set('memory_limit', -1);

//    $progress = new My_File_Progress('parent.set_progress');
//    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );

    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];

        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
    }
    catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

//read file
    include 'PHPExcel/IOFactory.php';
//  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    }
    catch (Exception $e) {

        $this->view->errors = $e->getMessage();
        return;
    }


//  Get worksheet dimensions
    $sheet         = $objPHPExcel->getSheet(0);
    $highestRow    = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $data_staff_code = [];

    for ($row = 0; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
        }
        catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        // nothing here
    } // END loop through order rows

$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
  $data_temp_time = array();
  $data_time_late = array();
//    $progress->flush(30);
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        $code_array=array();
        $id_array=array();
        for ($row = START_ROW; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            if(!empty(trim($rowData[ID_TEMP_TIME]))){
                $data_temp_time[] = trim($rowData[ID_TEMP_TIME]);
            }
            if(!empty(trim($rowData[ID_TIME_LATE]))){
                $data_time_late[] = trim($rowData[ID_TIME_LATE]);
            }

        }// end loop
        
        if(!empty($data_temp_time)){
            $str_id_temp_time = implode(",",$data_temp_time);
            if($type == 1){
                $sql =  "UPDATE `temp_time` set `passby` = 1, `hr_reject` = 1 WHERE id in ($str_id_temp_time)";
            }elseif($type == 2){
                $sql =  "UPDATE `temp_time` set `hr_reject` = 1 WHERE id in ($str_id_temp_time)";
            }else{
                $sql =  "UPDATE `temp_time` set `passby` = 0,  `hr_reject` = 0 WHERE id in ($str_id_temp_time)";
            }
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = null;
        }

        if(!empty($data_time_late)){
            $str_id_time_late = implode(",",$data_time_late);
             if($type == 1){
                $sql1 = "UPDATE  `time_late` set `passby` = 1, `hr_reject` = 1 WHERE id in ($str_id_time_late)";
            }elseif($type == 2){
                $sql1 = "UPDATE  `time_late` set `hr_reject` = 1 WHERE id in ($str_id_time_late)";
            }else{
                $sql1 = "UPDATE  `time_late` set `passby` = 0, `hr_reject` = 0 WHERE id in ($str_id_time_late)";
            }
            $stmt1 = $db->prepare($sql1);
            $stmt1->execute();
            $stmt1->closeCursor();
            $stmt1 = null;
        }

    $flashMessenger->setNamespace('success')->addMessage('Thành công.');
    $db->commit();    

    $this->_redirect(HOST . 'time-machine/hr-passby');
        
    }
    catch (Exception $e) {
        $db->rollback();
        $this->view->errors = $e->getMessage();

        return;
    }
}
$QReasonTempTime = new Application_Model_ReasonTempTime();
$reason_temp_time=$QReasonTempTime->fetchAll()->toArray();
$this->view->reason_temp_time = $reason_temp_time;
$flashMessenger = $this->_helper->flashMessenger;
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->view->messages_error = $messages_error;