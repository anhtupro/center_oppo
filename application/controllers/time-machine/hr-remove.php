<?php
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout->disableLayout();
    $month = $this->getRequest()->getParam('month');
    $year = $this->getRequest()->getParam('year');
    $from = $year . '-' . $month . '-01';
    $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

    $db = Zend_Registry::get('db');

    $sql = "DELETE FROM log_staff_warning WHERE penalty_day = '$to'";

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $stmt->closeCursor();
    echo json_encode (array(
            'status' => 1,
            'message' => "success",
            'sql'    => $sql
        ));