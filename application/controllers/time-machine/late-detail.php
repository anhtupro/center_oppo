<?php

    $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('year');
        $staff_id = $this->getRequest()->getParam('staff_id');
		      
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$QLogStaffWarning = new Application_Model_LogStaffWarning();

        $where = array();
        $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "MONTH(penalty_day) = $month" );
        $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year" );
        $locked_day = $QLogStaffWarning->fetchRow ( $where );
        
        if(!empty($locked_day)){
            $where_log = array();
			//$where_log[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "penalty_day= '2017-06-30'" );
           $where_log[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "MONTH(penalty_day) = $month" );
            $where_log[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year" );
            $where_log[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "staff_id = $staff_id" );
            $staff_info = $QLogStaffWarning->fetchRow ( $where_log );
        }
		
        $params = array(
            'month' => $month,
            'year' => $year,
            'staff_id' => $staff_id,
            'from_date' => $from,
            'to_date' => $to
        );
      
        $db = Zend_Registry::get('db');
        $sql = "CALL `detail_late_fix`(:from_date, :to_date, :staff_id)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $from, PDO::PARAM_STR);
        $stmt->bindParam('to_date', $to, PDO::PARAM_STR);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = null;

        $sql_day = "CALL `detail_late_day`(:from_date, :to_date, :staff_id)";
        $stmt_day = $db->prepare($sql_day);
        $stmt_day->bindParam('from_date', $from, PDO::PARAM_STR);
        $stmt_day->bindParam('to_date', $to, PDO::PARAM_STR);
        $stmt_day->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt_day->execute();
        
        $day_detail = $stmt_day->fetchAll();
        $stmt_day->closeCursor();
        $stmt_day = null;
        
        $date_late = !empty($day_detail[0]['date_late']) ? $day_detail[0]['date_late'] : $day_detail[1]['date_late'];
        $day_temp_time = !empty($day_detail[1]['day_temp_time']) ?  $day_detail[1]['day_temp_time'] : $day_detail[0]['day_temp_time'];
      
        $html = "";
        $name = "";
        $department = "";
        $number_15 = $number_30 = 0;
        $number_phat = 0;

        $data_return = array();
        
        $list_penalty_day = array();
        
        $warning_type = 3;
        $penalty_day = '2018-02-28';
        $where_warning_type[]           = $QLogStaffWarning->getAdapter()->quoteInto('warning_type = ?' , $warning_type);
        $where_warning_type[]           = $QLogStaffWarning->getAdapter()->quoteInto('penalty_day = ?' , $penalty_day);
        $where_warning_type[]           = $QLogStaffWarning->getAdapter()->quoteInto('staff_id = ?' , $staff_id);
        $result_warning_type            = $QLogStaffWarning->fetchRow($where_warning_type);
        $data_return['solan'] = !empty($result_warning_type) ? 2 : 1;
        $stt = 0;
        foreach($data as $key => $value)
        {
            $stt++;
            $list_penalty_day[] = date('d/m/Y', strtotime($value['day'])) ;
            $check_in_at = $value['check_in_at'];
            $check_out_at = $value['check_out_at'];
            $temp_time_id = $value['temp_time_id'];
            $day = $value['day'];
            
            $input_temp_time = empty($temp_time_id) ? $day : NULL;
            
            $in_time = !empty($check_in_at) ? date('H:i', strtotime($check_in_at)) : '';
            $out_time = !empty($check_out_at) ? date('H:i', strtotime($check_out_at)) : '';
            
            $html .= "<tr>";
            $html .= "<td>" . $value['staff_code'] . "</td>";
            
            $html .= "<td>" . $value['staff_name'] . "</td>";
            $html .= "<td>" . date('d/m/Y', strtotime($value['day'])) . "</td>";
            $html .= "<td>" .   $in_time . "</td>";
            $html .= "<td>" .   $out_time . "</td>";
            $html .= "<td>" . $value['time_late_reason']."</td>";
            $html .= "<td>" . $value['temp_time_reason'] ."</td>";
            $html .= "<td>" . ($value['leave'] == '0'?$value['leave_reason']:"") ."</td>";
            $html .= "<td>" . '<input rel_time_machine="'.$input_temp_time .'" rel_temp_time="'.$value['temp_time_id'].'"  rel="'.$value['id'].'" type="checkbox" class="boqua" name="boqua[]" 
                
                value="\''.
                    $day
            
            .'\'">' ."</td>";
            $html .= "</tr>";
            if($value['30'] == 1)
            {
                $number_30++;
            }
            else
            {
                $number_15++;
            }
            $name = $value['staff_name'];
            $department = $value['department_name'];
            $title = $value['title_name'];
        }
		 if(!empty($locked_day)){
            $data_return['number_15'] = $staff_info['late_15_30'];
            $data_return['number_30'] = $staff_info['late_30'];
            $data_return['forget_check_in'] = $staff_info['not_check_in'];
            
            $data_return['phat'] = $staff_info['sum_penalty_day'];
            $data_return['ngayxuphat']= $list_penalty_day;
            $data_return['date_late']= $date_late;
            $data_return['day_temp_time']= $day_temp_time;
        }else {
            $data_return['number_15'] = $number_15;
            $data_return['number_30'] = $number_30;
           // $data_return['forget_check_in'] = $staff_info['forget_check_in'];
            $number_phat = $number_30*0.5 + intval($number_15/3)*0.5;
            $data_return['phat'] = $number_phat;
            $data_return['ngayxuphat']= $list_penalty_day;
            $data_return['date_late']= $date_late;
            $data_return['day_temp_time']= $day_temp_time;
            
        }
        $data_return['html'] = $html;
        $data_return['name'] = $name;
        $data_return['staff_department'] = $department;
        $data_return['staff_title'] = $title;
        
        //$data_return['number_15'] = $number_15;
        //$data_return['number_30'] = $number_30;
        //$number_phat = $number_30*0.5 + intval($number_15/3)*0.5;
        //$data_return['phat'] = $number_phat;
        $data_return['staff_id']= $staff_id;
        $data_return['boqua']= '1';
		//$data_return['ngayxuphat']= $list_penalty_day;
        echo json_encode( $data_return );

        die;