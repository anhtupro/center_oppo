<?php
    $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $code           = $this->getRequest()->getParam('code');
        $db = Zend_Registry::get('db');
        $sql = "SELECT st.code,s1.`email`, sp.* FROM staff st 
                INNER JOIN `staff_permission` sp ON sp.department_id = st.department AND ( sp.team_id IS NULL OR sp.team_id = st.team ) 
                 AND sp.`is_manager` = 1 
                INNER JOIN `staff` s1 ON sp.staff_code = s1.`code` 
                WHERE st.`code` = :code
                UNION ALL
                SELECT st.code,s1.`email`, sp.* FROM staff st 
                INNER JOIN `staff_permission` sp ON sp.department_id = st.department AND ( sp.team_id IS NULL OR sp.team_id = st.team ) 
                AND sp.`is_leader` = 1 AND (sp.office_id = st.office_id OR sp.is_all_office = 1)
                INNER JOIN `staff` s1 ON sp.staff_code = s1.`code`
                WHERE st.`code` = :code AND s1.`code` NOT IN ('15010021')
                ";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('code', $code, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt =  null;
            
            $ccStaff = array();
            foreach ($data as $staff){
                $ccStaff[] = $staff['email'];
            }
            echo json_encode(array(
                'data' => $ccStaff
            ));