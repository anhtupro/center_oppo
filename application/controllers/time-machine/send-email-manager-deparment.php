<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$code = $this->getRequest()->getParam('staff_code');
$month = $this->getRequest()->getParam('month', intval(date('m')));
$year = $this->getRequest()->getParam('year', intval(date('Y')));
$QLogStaffWarning = new Application_Model_LogStaffWarning();
$QLogSendMailManager = new Application_Model_LogSendMailManager();
$db = Zend_Registry::get('db');
$where_red = array();
$where_red[] = $QLogStaffWarning->getAdapter()->quoteInto("YEAR(penalty_day) = $year");
$where_red[] = $QLogStaffWarning->getAdapter()->quoteInto("MONTH(penalty_day) = $month");
$where_red[] = $QLogStaffWarning->getAdapter()->quoteInto("warning_type = 3");
$list_red = $QLogStaffWarning->fetchAll($where_red);
$QTestEmail = new Application_Model_TestEmail();
$check_email = $QTestEmail->fetchRow();

$arr_locked_red = array();
if (!empty($list_red)) {
    foreach ($list_red->toArray() as $k => $val) {
        $arr_locked_red[] = $val['staff_id'];
    }
}

$data = array();
if (!empty($arr_locked_red)) {
    $str_staff_red = implode(",", $arr_locked_red);

    $sql1 = "SELECT 
            st.`id`,
            st.`code`,
            st2.`id` as staff_id_manager,
            CONCAT(st.firstname,' ', st.lastname) AS 'staff_name',
            CONCAT(st2.firstname, ' ',st2.lastname) AS 'staff_name_leader',
            st2.code as staff_code,
            0 AS is_leader,
            0 AS is_manager,
            st2.`id` is_manager,
            st2.`code` code_manager
            FROM staff st
            INNER JOIN personal_permission pp ON st.`id` = pp.staff_id
            INNER JOIN staff st2 ON pp.`line_manager` = st2.`id`
            WHERE st.id IN ($str_staff_red) AND st2.`code` = '$code' ";
    $stmt1 = $db->prepare($sql1);
    $stmt1->execute();

    $data1 = $stmt1->fetchAll();
    $stmt1->closeCursor();

    $sql = "SELECT b.id from (
                SELECT
                    st.`id`,
                    st.`code`,
                    st2.`id` as staff_id_manager,
                    CONCAT(st.firstname, ' ', st.lastname ) AS 'staff_name',
                    CONCAT( st2.firstname, ' ', st2.lastname ) AS 'staff_name_leader',
                    b1.staff_code as staff_code,
                    IFNULL(b1.is_leader,0) is_leader,
                    IF(IFNULL(b1.is_leader,0) = 0, 1, 0) is_manager ,
                    b1.staff_code AS code_manager
                    FROM staff st
                    LEFT JOIN ( 
                        SELECT * FROM staff_permission WHERE `is_manager` = 1
                        ) b1 ON b1.department_id = st.`department`                
                    LEFT JOIN staff st2 ON b1.staff_code = st2.`code`
                    WHERE st.id IN ($str_staff_red)
                    AND st.`code` <> st2.`code`
                    AND st2.`department` IN ('156', '154')
                    GROUP BY st.`code` ,  st2.`code`
                    ORDER BY st.`code`, st2.title ASC
            )b
            WHERE b.code_manager = '$code'
            GROUP BY b.`code`
            
            ";

    $stmt = $db->prepare($sql);
    $stmt->execute();

    $data = $stmt->fetchAll();
    $stmt->closeCursor();
    $sql2 = "
                SELECT b.id from (
                    SELECT
                        st.`id`,
                        st.`code`,
                        st2.`id` as staff_id_manager,
                        CONCAT(st.firstname, ' ', st.lastname ) AS 'staff_name',
                        CONCAT( st2.firstname, ' ', st2.lastname ) AS 'staff_name_leader',
                        b1.staff_code as staff_code,
                        IFNULL(b1.is_leader,0) is_leader,
                        IF(IFNULL(b1.is_leader,0) = 0, 1, 0) is_manager ,
                        b1.staff_code AS code_manager
                        FROM staff st
                        LEFT JOIN ( 
                            SELECT * FROM staff_permission WHERE `is_manager` = 1
                            ) b1 ON b1.department_id = st.`department` AND (IF(st.title = 341 , b1.staff_code = '14080121', 1 = 1  ) ) AND (IFNULL(b1.team_id, 0) = 0 OR st.team = b1.team_id) AND (IF(b1.staff_code = '1304KH41', st.title = b1.title_id, (IFNULL(b1.title_id, 0) = 0 OR st.title = b1.title_id)  )  ) AND ( b1.is_all_office = 1 OR st.office_id = b1.office_id)                
                        LEFT JOIN staff st2 ON b1.staff_code = st2.`code`
                        WHERE  st.id IN ($str_staff_red) AND st.`code` <> st2.`code` AND st2.`department` IN ('150', '159') 
                        GROUP BY st.`code` ,  st2.`code`
                        ORDER BY st.`code`, st2.title ASC
                )b WHERE b.code_manager = '$code'
                GROUP BY b.`code` ";
    $stmt2 = $db->prepare($sql2);
    $stmt2->execute();

    $data2 = $stmt2->fetchAll();
    $stmt2->closeCursor();

    $staff_send_email = array();
    $data3 = array_merge($data, $data2);
    foreach (array_merge($data1, $data3) as $k => $staff_by_id) {
        $staff_send_email[] = $staff_by_id['id'];
    }

    $str_send_mail = implode(",", $staff_send_email);

    $from = $year . '-' . $month . '-01';
    $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $sql_send_email = "CALL `get_late_by_list_staff_id`(:p_from_date, :p_to_date, :p_staff_id)";
    $stmt_send_email = $db->prepare($sql_send_email);
    $stmt_send_email->bindParam('p_from_date', $from, PDO::PARAM_STR);
    $stmt_send_email->bindParam('p_to_date', $to, PDO::PARAM_STR);
    $stmt_send_email->bindParam('p_staff_id', $str_send_mail, PDO::PARAM_STR);
    $stmt_send_email->execute();
    $data_send_email = $stmt_send_email->fetchAll();

    $stmt_send_email->closeCursor();
    $xhtml = '';
    $content_new = '<body lang="EN-US" class="MsgBody MsgBody-html" style="margin: 0px; height: auto; overflow: visible; width: 878px;"><div style=""><div><div class="WordSection1"><p class="MsoNormal" style="line-height: 17pt;"><span style="font-family: &quot;times new roman&quot;, serif;">Dear Anh/Chị</span></p><p class="MsoNormal" style="line-height: 17pt;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">Phòng Nhân sự gửi Anh/Chị danh sách nhân viên vi phạm chuyên cần tháng ' . $month . '/'.$year.':</span></p>'
            . '<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="1079" style="width: 808.95pt; margin-left: 5.15pt; border-collapse: collapse;">'
            . '<tbody><tr style="height: 51pt;">'
            . '<td width="33" nowrap="" style="width: 24.4pt; border: 1pt solid windowtext; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">TT</span></b></p></td>'
            . '<td width="71" nowrap="" style="width: 53pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">Mã NV</span></b></p></td>'
            . '<td width="200" nowrap="" style=" border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" style="line-height: 17pt; text-align: center; "><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">Họ tên NV</span></b></p></td>'
            . '<td width="65" nowrap="" style="width: 48.85pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">Phòng ban</span></b></p></td>'
            . '<td width="54" nowrap="" style="width: 40.8pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">Bộ phận</span></b></p></td>'
            . '<td width="62" nowrap="" style="width: 46.7pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">Chức vụ</span></b></p></td>'
            . '<td width="60" nowrap="" style="width: 45pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">Khu vực</span></b></p></td>'
            . '<td width="200" style="width: 150.1pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoListParagraphCxSpFirst" align="center" style="margin-left:0vw; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b><span style="font-size: 1rem; line-height: 150%; color: black;">2h &lt;&nbsp;&nbsp;Thời gian đi trễ&nbsp; <u>&lt;</u>&nbsp; 3h</span></b></p></td>'
            . '<td width="174" style="width: 130.5pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoListParagraphCxSpMiddle" align="center" style="margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b><span style="font-size: 1rem; line-height: 150%; color: black;">Thời gian đi trễ<br>&nbsp; &gt;&nbsp; 3h </span></b></p></td>'
            . '<td width="192" style="width: 2in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoListParagraphCxSpLast" align="center" style="margin-left: 0in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b><span style="font-size: 1rem; line-height: 150%; color: black;">Quên check in từ lần thứ 3 trở đi</span></b></p></td>'
            . '<td width="78" style="width: 58.5pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">Tổng<br>mức phạt</span></b></p></td></tr>';
    $stt = 0;
    foreach ($data_send_email as $key => $value) {
        $stt++;
        $code_staff = $value['staff_code'];
        $name = $value['staff_name'];
        $department = $value['department_name'];
        $team = $value['team_name'];
        $title = $value['title_name'];
        $area = $value['area'];
        $thoi_gian_1 = ($value['total_late'] <= 180 && $value['total_late'] > 120) ? $value['thoi_gian'] : '';
        $phat_1 = ($value['total_late'] <= 180 && $value['total_late'] > 120) ? 0.5 : '';
        $thoi_gian_2 = ($value['total_late'] > 180 ) ? $value['thoi_gian'] : '';
        $forget_check_in_day = ($value['forget_check_in'] >= 3) ? $value['forget_check_in'] - 2 : '';
        $phat_forget_check_in_day = ($value['forget_check_in'] >= 3) ? $value['forget_check_in'] - 2 : '';
        $ngay_phat = ($value['ngay_phat'] > 0) ? $value['ngay_phat'] . ' ngày công' : '';
        $late_less_3 = (My_Controller_Action::rule_penalti($value['total_late']) <= 0.5) ? My_Controller_Action::rule_penalti($value['total_late']) : 0;
        $late_less_3_tmp = (My_Controller_Action::rule_penalti($value['total_late']) <= 0.5) ? My_Controller_Action::rule_penalti($value['total_late']) : '';
        $late_over_3 = (My_Controller_Action::rule_penalti($value['total_late']) > 0.5) ? My_Controller_Action::rule_penalti($value['total_late']) : 0;
        $tong_phat = $forget_check_in_day + $late_less_3 + $late_over_3 . ' ngày công';
        $number_check_in = floatval($value['forget_check_in']);

        $late_15 = $value['15_30'];
        $late_30 = $value['30'];
        $forget_check_in = $value['forget_check_in'];
        $phat = $value['songayphat'];
        $xhtml .= '<tr style="height: 51pt;">'
                . '<td width="33" nowrap="" style="width: 24.4pt; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $stt . '</span></p></td>'
                . '<td width="71" nowrap="" style="width: 53pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $code_staff . '</span></p></td>'
                . '<td width="89" nowrap="" style="width: 67.1pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" style="line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $name . '</span></p></td>'
                . '<td width="65" nowrap="" style="width: 48.85pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $department . '</span></p></td>'
                . '<td width="54" nowrap="" style="width: 40.8pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $team . '</span></p></td>'
                . '<td width="62" nowrap="" style="width: 46.7pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $title . '</span></p></td>'
                . '<td width="60" nowrap="" style="width: 45pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $area . '</span></p></td>'
                . '<td width="200" style="width: 150.1pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $thoi_gian_1 . '</span></p></td>'
                . '<td width="174" style="width: 130.5pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $thoi_gian_2 . '</span></p></td>'
                . '<td width="192" style="width: 2in; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $forget_check_in_day . '</span></p></td>'
                . '<td width="78" style="width: 58.5pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><span style="font-size: 1rem; font-family: &quot;times new roman&quot;, serif;">' . $tong_phat . '</span></p></td></tr>';
    }
    $end = '</tbody></table><p class="MsoNormal" style="line-height: 17pt;"><span style="font-family: &quot;times new roman&quot;, serif;">Anh/Chị<span style="color: black;"> cập nhật thông tin giúp em.</span></span></p><p class="MsoNormal"><span style="color: rgb(31, 73, 125);">&nbsp;</span></p><div><table class="MsoNormalTable" cellspacing="0" cellpadding="0" style="border-collapse: collapse;"> <tbody > <tr style="height: 38.9pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"><p class="MsoNormal"><b style="color: #028553;font-family:sans-serif;font-size: 15px;">Ms.NGUYEN THI PHUONG NGA</b><br><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">C&B Specialist</span></p></td></tr><tr style="height: 37.5pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 37.5pt;"><p class="MsoNormal" ><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">T: <span class="Object" id="OBJ_PREFIX_DWT68_com_zimbra_phone"><a style="color: black;" href="callto:(84-8) 3920 2555" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()">(84-8) 3920 2555</a></span> - Ext: 105 <br>E: <span class="Object" role="link" id="OBJ_PREFIX_DWT69_ZmEmailObjectHandler"><a href="mailto:phuongnga.nguyen@oppo-aed.vn" target="_blank">phuongnga.nguyen@oppo-aed.vn</a></span><br> M: <span class="Object" id="OBJ_PREFIX_DWT68_com_zimbra_phone"><a href="callto:(84) 036 6001 601" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()" style="color: black;">(84) 036 6001 601</a></span> <br></span></span><span style="color: black;"></span> </p> </td> </tr> <tr style="height: 38.9pt;"> <td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"> <p class="MsoNormal"> <b style="color: #028553; font-size: 12px;font-family:arial, sans-serif;">OPPO Authorized Exclusive Distributor<br>CÔNG TY CỔ PHẦN KỸ THUẬT & KHOA HỌC VĨNH KHANG</b> <span style="font-size: 10pt; font-family: arial, sans-serif; color: black;"><br >12<sup >th</sup><b >&nbsp;</b>Floor, Lim II Tower<br>No. 62A CMT8 St., Dist. 3, HCMC, VN</span><span style="color: rgb(31, 73, 125);"></span> </p> </td> </tr> <tr style="height: 16.5pt;"> <td width="374" style="width: 3.9in; padding: 0in 5.4pt; height: 16.5pt;"> <p class="MsoNormal" ><span style="color: #028553;"><span class="Object" role="link" id="OBJ_PREFIX_DWT71_com_zimbra_url"><a style="text-decoration-color:#028553" href="http://www.oppomobile.vn/" target="_blank" ><span style="font-size: 10pt; font-family: arial, sans-serif; color:#028553;font-weight: 600;">www.oppomobile.vn</span></a></span></span> </p> </td> </tr> </tbody> </table><p class="MsoNormal"><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div><p class="MsoNormal"><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div></div></div></body>';

    $subject = 'Thông báo danh sách nhân viên vi phạm chuyên cần tháng ' . $month . '/' . $year;
    $QStaff = new Application_Model_Staff();
    $select_staff = $QStaff->getAdapter()->quoteInto("code = '$code'", '');
    $infoStaff = $QStaff->fetchRow($select_staff);
    $emailInfo = $infoStaff->email;
    $staff_id = $infoStaff->id;
    $department = $infoStaff->department;
    $ccStaff = array();
    if ($check_email) {
        $mailto = 'congtrang.huynh@oppo-aed.vn';
//        $ccStaff[] = 'dieptruc.nguyen@oppo-aed.vn';
    } else {

        $mailto = $emailInfo;
    }
    //$ccStaff[] = 'phuongnga.nguyen@oppo-aed.vn';
    //gui email nhe
    $content_full = $content_new . $xhtml . $end;
    $res = $this->sendmailnew($mailto, $subject, $content_full, NULL, $ccStaff);
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $data_send_mail = array(
        'staff_id' => $staff_id,
        'month' => $month,
        'year' => $year,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id
    );
    $QLogSendMailManager->insert($data_send_mail);
    $reponse['status'] = 1;
    $reponse['message'] = "Thành công";
    echo json_encode($reponse);
}
        