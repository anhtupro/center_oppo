<?php
    $id = $this->getRequest()->getParam('id');

        if(!empty($id))
        {
            $sql = "DELETE FROM `personal_permission` WHERE id = :id";
            $db = Zend_Registry::get('db');
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = $db = null;

            $this->_redirect('/time-machine/personal-permission');
            return;
        }