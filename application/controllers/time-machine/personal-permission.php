<?php
    $code = $this->getRequest()->getParam('code');
        $name = $this->getRequest()->getParam('name');

        $db = Zend_Registry::get('db');

        $sql = "SELECT 
                    pp.*,
                    CONCAT(st.firstname, ' ', st.lastname) as `manager_name`,
                    st.code as `manager_code`,
                    CONCAT(st2.firstname, ' ', st2.lastname) as `staff_name`,
                    st2.code as `staff_code`,
                    t1.name as `manager_department`,
                    t2.name as `staff_department`
                FROM `personal_permission` pp
                JOIN `staff` st ON pp.line_manager = st.id
                JOIN `staff` st2 ON pp.staff_id = st2.id
                JOIN `team` t1 ON st.department = t1.id
                JOIN `team` t2 ON st2.department = t2.id
                WHERE TRUE";

        if(!empty($code))
        {
            $sql .= " AND st.code = '" . $code . "' ";
        }

        if(!empty($name))
        {
            $sql .= " AND CONCAT(st.firstname, ' ', st.lastname) LIKE '%" . $name . "%' ";
        }

        $stmt = $db->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        $this->view->code = $code;
        $this->view->name = $name;
        $this->view->data = $data;