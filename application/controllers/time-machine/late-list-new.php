<?php
    $month            = $this->getRequest()->getParam('month', intval(date('m')));
    $year             = $this->getRequest()->getParam('year', intval(date('Y')));
    $name             = $this->getRequest()->getParam('name');
    $code             = $this->getRequest()->getParam('code');
    $export           = $this->getRequest()->getParam('export',NULL);
    $export_all       = $this->getRequest()->getParam('export_all',0);
    $export_detail    = $this->getRequest()->getParam('export_detail',0);
    $export_not_penalty    = $this->getRequest()->getParam('export_not_penalty',0);
    $removeColor    = $this->getRequest()->getParam('remove_color',0);
    $export_full    = $this->getRequest()->getParam('export_full',0);
    $department       = $this->getRequest()->getParam('department');
    $team             = $this->getRequest()->getParam('team');
    $title            = $this->getRequest()->getParam('title');
    $area             = $this->getRequest()->getParam('area');
        
    $from = $year . '-' . $month . '-01';
    $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
    
    $QLogStaffWarmingDetail = new Application_Model_LogStaffWarningDetail();
    $QCheckIn               = new Application_Model_CheckIn();
    $QLogSendMail           = new Application_Model_LogSendMail();
    $QLogStaffWarning       = new Application_Model_LogStaffWarning();
    $QTeam                  = new Application_Model_Team();
      $QLogSendMailManager = new Application_Model_LogSendMailManager();
    $where = array();
    $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "MONTH(penalty_day) = $month", NULL);
    $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year" , NULL);
    $locked_day = $QLogStaffWarning->fetchRow ( $where );
            
    $where_red = array();
//    $where_red[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year" ,NULL);
    $where_red[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "warning_type = 3" , NULL);
    $list_red    = $QLogStaffWarning->fetchAll($where_red);

    
    $where_yellow = array();
    $where_yellow[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year",NULL );
    $where_yellow[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "warning_type <> 0" ,NULL);
    $where_yellow[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "warning_type <> 3" ,NULL);
    $list_yellow   = $QLogStaffWarning->fetchAll($where_yellow);
        
    $where_sendMail = array();
    $where_sendMail[] = $QLogSendMail->getAdapter ()->quoteInto ( "year = $year",NULL );
    $where_sendMail[] = $QLogSendMail->getAdapter ()->quoteInto ( "month = $month" ,NULL);
    $list_sendMail   = $QLogSendMail->fetchAll($where_sendMail);
 
    //Remove Color
    if(!empty($removeColor)){
        $QLogSendMail->delete($where_sendMail);
        $QLogSendMailManager->delete($where_sendMail);
    }
     
    
    $arr_locked_red = array();
    if(!empty($list_red)){
        foreach ($list_red->toArray() as $k => $val){
            $arr_locked_red[]=$val['staff_id'];
        }
    }
   
    $arr_locked_yellow = array();
    if(!empty($list_yellow)){
        foreach ($list_yellow->toArray() as $k => $val){
            $arr_locked_yellow[]=$val['staff_id'];
        }
    }
        
    $arr_sendMail = array();
    if(!empty($list_sendMail)){
        foreach ($list_sendMail->toArray() as $k => $val){
            $arr_sendMail[]=$val['staff_id'];
        }
    }
       
    $params = array(
        'from_date' => $from,
        'to_date' => $to,
        'month' => $month,
        'year' => $year,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'code'  => $code,
        'name'  => $name,
        'area'  => $area
    );
    $db = Zend_Registry::get('db');

    $sql = "CALL `get_late_list_final`(:from_date, :to_date, :pDepartment, :pTeam, :pTitle, :pName, :pCode, :pArea)";

    $stmt = $db->prepare($sql);
    $stmt->bindParam('from_date', My_Util::escape_string($params['from_date']), PDO::PARAM_STR);
    $stmt->bindParam('to_date', My_Util::escape_string($params['to_date']), PDO::PARAM_STR);
    $stmt->bindParam('pDepartment', My_Util::escape_string($params['department']), PDO::PARAM_STR);
    $stmt->bindParam('pTeam', My_Util::escape_string($params['team']), PDO::PARAM_STR);
    $stmt->bindParam('pTitle', My_Util::escape_string($params['title']), PDO::PARAM_STR);
    $stmt->bindParam('pName', My_Util::escape_string($params['name']), PDO::PARAM_STR);
    $stmt->bindParam('pCode', My_Util::escape_string($params['code']), PDO::PARAM_STR);
    $stmt->bindParam('pArea', My_Util::escape_string($params['area']), PDO::PARAM_STR);

    $stmt->execute();

    $data = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt=null;

//    $data = $QCheckIn->listLate($params);
    $sql_pre = "TRUNCATE TABLE late_list_by_month";
    $stmt_pre = $db->prepare($sql_pre);
    $stmt_pre->execute();
    $stmt_pre = null;
                
    $list_staff = array();
    $dataInsert = array();
    foreach ($data as $key => $val){
        $list_staff[] =  $val['staff_id'];
        $forget_check_in = ($val['forget_check_in'] > 2) ? $val['forget_check_in'] - 2 : 0;
        $late_less_3 = (My_Controller_Action::rule_penalti($val['total_late']) <= 0.5) ? My_Controller_Action::rule_penalti($val['total_late']) : 0; 
        $late_over_3 = (My_Controller_Action::rule_penalti($val['total_late']) > 0.5) ? My_Controller_Action::rule_penalti($val['total_late']) : 0; 
        $number_phat =  $forget_check_in + $late_less_3 + $late_over_3 ;
        $dataInsert[$key] = $val;
        $dataInsert[$key]['ngay_phat'] = $number_phat;
    }
    if(!empty($dataInsert)){
        My_Controller_Action::insertAllrow($dataInsert, 'late_list_by_month');
    }
    
    $sql_data = "SELECT * FROM `late_list_by_month` WHERE 1 order by `ngay_phat` DESC";
    
    $stmtData = $db->prepare($sql_data);
    $stmtData->execute();
    $dataAll = $stmtData->fetchAll();
    $stmtData->closeCursor();
    
    $list_staff_str =  implode(",",$list_staff);
    $QArea = new Application_Model_Area();
    $all_area =  $QArea->fetchAll(null, 'name');
    $this->view->areas = $all_area;
   
    if($export_all == 1){
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $alphaExcel = new My_AlphaExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'MÃ NHÂN VIÊN',
            $alphaExcel->ShowAndUp() => 'TÊN NHÂN VIÊN',
            $alphaExcel->ShowAndUp() => 'PHÒNG BAN',
            $alphaExcel->ShowAndUp() => 'TEAM',
            $alphaExcel->ShowAndUp() => 'TITLE',
            $alphaExcel->ShowAndUp() => 'KHU VỰC',
            $alphaExcel->ShowAndUp() => '2H ĐẾN 3H',
            $alphaExcel->ShowAndUp() => 'TRÊN 3H',
            $alphaExcel->ShowAndUp() => 'QUÊN CHECK IN TỪ LẦN THỨ 3 TRỞ ĐI',
            $alphaExcel->ShowAndUp() => 'SỐ NGÀY PHẠT',
            $alphaExcel->ShowAndUp() => 'PHẠT',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
            $sheet->setCellValue($key.'1', $value);
            $sheet->getStyle('A1:K1')->applyFromArray(array('font' => array('bold' => true)));
               
            foreach ($data as $key => $value) {
                $staff_code     = $value['staff_code'];
                $staff_name     = $value['staff_name'];
                $department_name = $value['department_name'];
                $team_name      = $value['team_name'];
                $title_name     = $value['title_name'];
                $area           = $value['area'];
                $late_15_30     = ($value['total_late'] < 180 && $value['total_late'] > 60) ? $value['thoi_gian'] : '';
                $late_30        = ($value['total_late'] > 180 ) ? $value['thoi_gian'] : '';
                $forget_check_in = ($value['forget_check_in'] > 2) ? $value['forget_check_in'] - 2 : '';
                $number_30      = floatval($value['30']);
                $number_15_30   = floatval($value['15_30']);
                
                $late_less_3 = (My_Controller_Action::rule_penalti($value['total_late']) <= 0.5) ? My_Controller_Action::rule_penalti($value['total_late']) : 0; 
                $late_over_3 = (My_Controller_Action::rule_penalti($value['total_late']) > 0.5) ? My_Controller_Action::rule_penalti($value['total_late']) : 0; 
                
                $number_check_in = ($value['forget_check_in'] > 2) ? $value['forget_check_in'] - 2 : 0;
//                if($value['ngay_phat'] > 0 && $value['total_late'] > 180){
//                    $number_phat = $value['ngay_phat'] . ' ngày + '.  $value['thoi_gian'];
//                }elseif($value['ngay_phat'] == 0 && $value['total_late'] > 180){
//                    $number_phat =   $value['thoi_gian'];
//                }elseif($value['ngay_phat'] > 0){
//                    $number_phat =  $value['ngay_phat'] . ' ngày';
//                }else{
//                    $number_phat =  0;
//                }
                $number_phat =  $number_check_in + $late_less_3 + $late_over_3 ;
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $staff_code,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $department_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $team_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $title_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $area);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $late_less_3);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $late_over_3);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $forget_check_in);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $number_phat);
                $phat = in_array($value['staff_id'], $arr_locked_red) ? 'X' : '';
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $phat);
            }
                $filename_over = 'ChuyenCan-Export All ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
                $objWriter->save('php://output');
                exit;
        }
        
        if($export_detail){
            $db = Zend_Registry::get('db');
            $sql = "CALL `detail_late_fix`(:from_date, :to_date, :p_staff_id)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
            $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
            $stmt->bindParam('p_staff_id', My_Util::escape_string($list_staff_str), PDO::PARAM_STR);
            $stmt->execute();
            $result_ep = $stmt->fetchAll();
            $stmt = null;
                
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();
            
                    
            $heads = array(
                $alphaExcel->ShowAndUp() => 'ID BSC',
                $alphaExcel->ShowAndUp() => 'ID ĐT/VS',
                $alphaExcel->ShowAndUp() => 'MNV',
                $alphaExcel->ShowAndUp() => 'TÊN',
                $alphaExcel->ShowAndUp() => 'PHÒNG BAN',
                $alphaExcel->ShowAndUp() => 'BỘ PHẬN',
                $alphaExcel->ShowAndUp() => 'CHỨC DANH',
                $alphaExcel->ShowAndUp() => 'KHU VỰC',
                $alphaExcel->ShowAndUp() => 'NGÀY VÀO LÀM',
                $alphaExcel->ShowAndUp() => 'NGÀY NGHỈ',
                $alphaExcel->ShowAndUp() => 'NGÀY',
                $alphaExcel->ShowAndUp() => 'GIỜ VÀO',
                $alphaExcel->ShowAndUp() => 'GIỜ RA',
                $alphaExcel->ShowAndUp() => 'THỜI GIAN TRỄ',
                $alphaExcel->ShowAndUp() => 'TÍNH PHẠT',
                $alphaExcel->ShowAndUp() => 'CÁC LOẠI BỔ SUNG CÔNG',
                $alphaExcel->ShowAndUp() => 'NHÓM LÝ DO',
                $alphaExcel->ShowAndUp() => 'LÝ DO CHI TIẾT',
                $alphaExcel->ShowAndUp() => 'CÁC LOẠI BỔ SUNG ĐI TRỄ VỀ SỚM',
                $alphaExcel->ShowAndUp() => 'LÝ DO CẦN XÁC NHẬN',
                $alphaExcel->ShowAndUp() => 'LÝ DO CHI TIẾT',
                $alphaExcel->ShowAndUp() => 'LÝ DO CẦN XÁC NHẬN VỀ SỚM',
                $alphaExcel->ShowAndUp() => 'LÝ DO CHI TIẾT',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            foreach($heads as $key => $value)
            $sheet->setCellValue($key.'1', $value);
            $sheet->getStyle('A1:W1')->applyFromArray(array('font' => array('bold' => true)));
            
            foreach ($result_ep as $key => $value) {
                $temp_time_id       = $value['temp_time_id'];
                $time_late_id       = $value['id_time_late'];
                $staff_code         = $value['staff_code'];
                $staff_name         = $value['staff_name'];
                $department_name    = $value['department_name'];
                $team_name          = $value['team_name'];
                $title_name         = $value['title_name'];
                $area_name          = $value['area_name'];
                $joined_at          = $value['joined_at'];
                $off_date           = $value['off_date'];
                $check_in_day       = $value['day'];
                $check_in_at        = $value['check_in_at'];
                $check_out_at       = $value['check_out_at'];
                $check_in_late_time = $value['check_in_late_time'];
                $temp_time_reason   = $value['temp_time_reason'];
                $time_late_reason   = $value['time_late_reason'];
                $group_temp_time    = $value['group_temp_time'];
                $type_temp_time     = $value['type_temp_time'];
                $group_time_late    = $value['group_time_late'];
                $type_time_late     = $value['type_time_late'];
                $kyhieu             = $value['kyhieu'];
                $type_soon_late             = $value['type_soon_late'];
                $reason_soon             = $value['reason_soon'];
                
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $temp_time_id);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $time_late_id);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $staff_code,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $department_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $team_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $title_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $area_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date("d/m/Y", strtotime($joined_at)));
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($off_date) ? date("H:i", strtotime($off_date)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date("d/m/Y", strtotime($check_in_day)));
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($check_in_at) ? date("H:i", strtotime($check_in_at)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($check_out_at) ? date("H:i", strtotime($check_out_at)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($check_in_late_time) ? date("H:i", strtotime($check_in_late_time)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $kyhieu);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $group_temp_time);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_temp_time);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $temp_time_reason);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $group_time_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_time_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $time_late_reason);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_soon_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_soon_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $reason_soon);

            }
                $filename_over = 'ChuyenCan -Detail ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
                $objWriter->save('php://output');
                exit;
        }
        
        
        if($export_not_penalty){
            $db = Zend_Registry::get('db');
            $sql = "CALL `PR_day_not_penalty`(:from_date, :to_date, :p_staff_id)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
            $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
            $stmt->bindParam('p_staff_id', My_Util::escape_string($list_staff_str), PDO::PARAM_STR);
            $stmt->execute();
            $result_ep = $stmt->fetchAll();
            $stmt = null;
            
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();
            $heads = array(
                $alphaExcel->ShowAndUp() => 'MÃ NHÂN VIÊN',
                $alphaExcel->ShowAndUp() => 'TÊN NHÂN VIÊN',
                $alphaExcel->ShowAndUp() => 'DEPARTMENT',
                $alphaExcel->ShowAndUp() => 'TEAM',
                $alphaExcel->ShowAndUp() => 'TITLE',
                $alphaExcel->ShowAndUp() => 'AREA',
                $alphaExcel->ShowAndUp() => 'NGÀY VÀO LÀM',
                $alphaExcel->ShowAndUp() => 'NGÀY OFF',
                $alphaExcel->ShowAndUp() => 'NGÀY',
                $alphaExcel->ShowAndUp() => 'GIỜ VÀO',
                $alphaExcel->ShowAndUp() => 'GIỜ RA',
                $alphaExcel->ShowAndUp() => 'THỜI GIAN TRỄ',
                $alphaExcel->ShowAndUp() => 'LÝ DO XÁC NHẬN CÔNG',
                $alphaExcel->ShowAndUp() => 'LÝ DO XÁC NHẬN ĐI TRỄ/VỀ SỚM',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            foreach($heads as $key => $value)
            $sheet->setCellValue($key.'1', $value);
            $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));
            
            foreach ($result_ep as $key => $value) {
                $staff_code         = $value['staff_code'];
                $staff_name         = $value['staff_name'];
                $department_name    = $value['department_name'];
                $team_name          = $value['team_name'];
                $title_name         = $value['title_name'];
                $area_name          = $value['area_name'];
                $joined_at          = $value['joined_at'];
                $off_date           = $value['off_date'];
                $check_in_day       = $value['day'];
                $check_in_at        = $value['check_in_at'];
                $check_out_at       = $value['check_out_at'];
                $check_in_late_time = $value['check_in_late_time'];
                $temp_time_reason   = $value['temp_time_reason'];
                $time_late_reason   = $value['time_late_reason'];
              
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $staff_code,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $department_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $team_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $title_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $area_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date("d/m/Y", strtotime($joined_at)));
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($off_date) ? date("H:i", strtotime($off_date)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date("d/m/Y", strtotime($check_in_day)));
                 $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($check_in_at) ? date("H:i", strtotime($check_in_at)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($check_out_at) ? date("H:i", strtotime($check_out_at)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($check_in_late_time) ? date("H:i", strtotime($check_in_late_time)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $temp_time_reason);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $time_late_reason);

                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), '');
            }
            $filename_over = 'ChuyenCan -Detail ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
            $objWriter->save('php://output');
            exit;
            
        }

        if($export_full){
            $db = Zend_Registry::get('db');
            $sql = "CALL `PR_day_full`(:from_date, :to_date, :p_staff_id)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
            $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
            $stmt->bindParam('p_staff_id', My_Util::escape_string($list_staff_str), PDO::PARAM_STR);
            $stmt->execute();
            $result_ep = $stmt->fetchAll();
            $stmt = null;

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();
            $heads = array(
                $alphaExcel->ShowAndUp() => 'ID BSC',
                $alphaExcel->ShowAndUp() => 'ID ĐT/VS',
                $alphaExcel->ShowAndUp() => 'MNV',
                $alphaExcel->ShowAndUp() => 'TÊN',
                $alphaExcel->ShowAndUp() => 'PHÒNG BAN',
                $alphaExcel->ShowAndUp() => 'BỘ PHẬN',
                $alphaExcel->ShowAndUp() => 'CHỨC DANH',
                $alphaExcel->ShowAndUp() => 'KHU VỰC',
                $alphaExcel->ShowAndUp() => 'NGÀY VÀO LÀM',
                $alphaExcel->ShowAndUp() => 'NGÀY NGHỈ',
                $alphaExcel->ShowAndUp() => 'NGÀY',
                $alphaExcel->ShowAndUp() => 'GIỜ VÀO',
                $alphaExcel->ShowAndUp() => 'GIỜ RA',
                $alphaExcel->ShowAndUp() => 'THỜI GIAN TRỄ',
                $alphaExcel->ShowAndUp() => 'TÍNH PHẠT',
                $alphaExcel->ShowAndUp() => 'CÁC LOẠI BỔ SUNG CÔNG',
                $alphaExcel->ShowAndUp() => 'NHÓM LÝ DO',
                $alphaExcel->ShowAndUp() => 'LÝ DO CHI TIẾT',
                $alphaExcel->ShowAndUp() => 'CÁC LOẠI BỔ SUNG ĐI TRỄ VỀ SỚM',
                $alphaExcel->ShowAndUp() => 'LÝ DO CẦN XÁC NHẬN',
                $alphaExcel->ShowAndUp() => 'LÝ DO CHI TIẾT',
                $alphaExcel->ShowAndUp() => 'LÝ DO CẦN XÁC NHẬN VỀ SỚM',
                $alphaExcel->ShowAndUp() => 'LÝ DO CHI TIẾT',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);
            $sheet->getStyle('A1:W1')->applyFromArray(array('font' => array('bold' => true)));

            foreach ($result_ep as $key => $value) {
                $temp_time_id       = $value['temp_time_id'];
                $time_late_id       = $value['time_late_id'];
                $staff_code         = $value['staff_code'];
                $staff_name         = $value['staff_name'];
                $department_name    = $value['department_name'];
                $team_name          = $value['team_name'];
                $title_name         = $value['title_name'];
                $area_name          = $value['area_name'];
                $joined_at          = $value['joined_at'];
                $off_date           = $value['off_date'];
                $check_in_day       = $value['day'];
                $check_in_at        = $value['check_in_at'];
                $check_out_at       = $value['check_out_at'];
                $check_in_late_time = $value['check_in_late_time'];
                $temp_time_reason   = $value['temp_time_reason'];
                $time_late_reason   = $value['time_late_reason'];
                $group_temp_time    = $value['group_temp_time'];
                $type_temp_time     = $value['type_temp_time'];
                $group_time_late    = $value['group_time_late'];
                $type_time_late     = $value['type_time_late'];
                $kyhieu             = $value['kyhieu'];
                $type_soon_late             = $value['type_soon_late'];
                $reason_soon             = $value['reason_soon'];

                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $temp_time_id);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $time_late_id);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $staff_code,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $staff_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $department_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $team_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $title_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $area_name);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date("d/m/Y", strtotime($joined_at)));
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($off_date) ? date("H:i", strtotime($off_date)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date("d/m/Y", strtotime($check_in_day)));
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($check_in_at) ? date("H:i", strtotime($check_in_at)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($check_out_at) ? date("H:i", strtotime($check_out_at)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($check_in_late_time) ? date("H:i", strtotime($check_in_late_time)) : '');
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $kyhieu);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $group_temp_time);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_temp_time);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $temp_time_reason);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $group_time_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_time_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $time_late_reason);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_soon_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $type_soon_late);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $reason_soon);

                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), '');
            }
            $filename_over = 'ChuyenCan -Detail-Full ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
            $objWriter->save('php://output');
            exit;

        }
        $QTestEmail =  new Application_Model_TestEmail();
        $test_email = $QTestEmail->fetchRow();
	
        $this->view->is_test = !empty($test_email) ? 1 : 0;
                
        $this->view->arr_locked         = $arr_locked_red;
        $this->view->arr_locked_yellow  = $arr_locked_yellow;
        $this->view->arr_sendMail       = $arr_sendMail;
        $this->view->params             = $params;
        $this->view->data               = $dataAll;
        $this->view->locked_day         = $locked_day;

        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;