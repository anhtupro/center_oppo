<?php
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout->disableLayout();
    $staff_id = $this->getRequest()->getParam('staff_id');
    $month = $this->getRequest()->getParam('month');
    $year = $this->getRequest()->getParam('year');
    $late_day = $this->getRequest()->getParam('late_day');
    if(!empty($staff_id) & !empty($month) & !empty($year)){

        $sql = "UPDATE `time_machine`
                SET late_passby = 1
                WHERE staff_id = $staff_id
                AND check_in_day IN ($late_day)
        ";

        $db = Zend_Registry::get('db');
        $db->query($sql);

        echo json_encode (array(
            'status' => 1,
            'message' => "success"
        ));
        exit();
    }