<?php
    $code = $this->getRequest()->getParam('code');
    $staff_name = $this->getRequest()->getParam('staff_name');
    $email = $this->getRequest()->getParam('email');
    $ip = $this->getRequest()->getParam('ip');

    $params = array(
        'code' => $code,
        'staff_name' => $staff_name,
        'email' => $email,
        'ip' => $ip
    );

    $QCheckIn = new Application_Model_CheckIn();
    $data = $QCheckIn->fetchStaffMap($params);
    $this->view->params = $params;
    $this->view->data = $data;