<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$staff_id       = $this->getRequest()->getParam('staff_id');
$code           = $this->getRequest()->getParam('staff_code');
$month          = $this->getRequest()->getParam('month', intval(date('m')));
$year           = $this->getRequest()->getParam('year', intval(date('Y')));
$time_dealine          = $this->getRequest()->getParam('time_dealine');

$QTestEmail =  new Application_Model_TestEmail();
$check_email = $QTestEmail->fetchRow();
$db = Zend_Registry::get('db');
  $QCheckIn = new Application_Model_CheckIn();
    $QLogSendMail = new Application_Model_LogSendMail();
    $ccStaff = array();
foreach($staff_id as $key => $value){
    $ccStaff = [];
    //$ccStaff[]  = 'dieptruc.nguyen@oppo-aed.vn';

    $from = $year . '-' . $month . '-01';
    $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
  
    $params = array(
        'from_date' => $from,
        'to_date' => $to,
        'month' => $month,
        'year' => $year,
        'department' => NULL,
        'team' => NULL,
        'title' => NULL,
        'code'  => $code[$key],
        'name'  => NULL,
        'area'  => NULL
    );

    $data = $QCheckIn->listLate($params);
    $content = $subject = $day_15_30 = $day_30 = $forget_check_in_day = '';
    $mailto = array();

    if(!empty($data[0])){
        $thoi_gian_1 = ($data[0]['total_late'] <= 180 && $data[0]['total_late'] > 120) ?  $data[0]['thoi_gian'] : ''  ;
        $phat_1 = ($data[0]['total_late'] <= 180 && $data[0]['total_late'] > 120) ?  0.5 . ' ngày công' : ''  ;

        $thoi_gian_2     = ($data[0]['total_late'] > 180 ) ?  $data[0]['thoi_gian'] : ''  ;
        $thoi_gian_2_phat     = ($data[0]['total_late'] > 180 ) ? My_Controller_Action::rule_penalti($data[0]['total_late']) . ' ngày công' : '';
        $forget_check_in_day = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 : '';
        $phat_forget_check_in_day = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 . ' ngày công' : '';
        $ngay_phat = ($data[0]['ngay_phat'] > 0) ? $data[0]['ngay_phat'] . ' ngày công' : ''  ;

        $forget_check_in = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 : 0;
        $late_less_3     = (My_Controller_Action::rule_penalti($data[0]['total_late']) <= 0.5) ? My_Controller_Action::rule_penalti($data[0]['total_late']) : 0;
        $late_over_3     = (My_Controller_Action::rule_penalti($data[0]['total_late']) > 0.5) ? My_Controller_Action::rule_penalti($data[0]['total_late']) : 0;
        $tong_phat       =  $forget_check_in + $late_less_3 + $late_over_3 . ' ngày công';

        $content_new = '<body lang="EN-US" style="margin: 0px; height: auto; overflow: visible; width: 878px;" class="MsgBody MsgBody-html">
                <div style=""><div style=""><style></style><div class="WordSection1" style="">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="984" style="width: 10.25in; border-collapse: collapse;">
                <tbody style=""><tr style="height: 15pt;">
                <td width="121" nowrap="" valign="bottom" style="width: 90.75pt; background: white; padding: 0in 5.4pt; height: 15pt;">
                <p class="MsoNormal" style="line-height: 115%;">
                <span style="font-family: &quot;times new roman&quot;, serif; color: black;">Chào bạn,</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="227" nowrap="" colspan="2" valign="bottom" style="width: 170.25pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="174" nowrap="" colspan="2" valign="bottom" style="width: 130.5pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="462" nowrap="" colspan="2" valign="bottom" style="width: 346.5pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td></tr><tr style="height: 15.75pt;">
                <td width="522" nowrap="" colspan="5" valign="bottom" style="width: 391.5pt; background: white; padding: 0in 5.4pt; height: 15.75pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">Phòng Nhân sự gửi bạn thông tin vi phạm chuyên cần tháng '.$month.'/'.$year.'</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
            . '<td width="462" nowrap="" colspan="2" valign="bottom" style="width: 346.5pt; background: white; padding: 0in 5.4pt; height: 15.75pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td></tr><tr style="height: 24.75pt;">'
            . '<td width="121" nowrap="" style="width: 90.75pt; border: 1pt solid windowtext; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Nội dung</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
            . '<td width="203" nowrap="" style="width: 152.25pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpFirst" align="center" style="margin-left: 0.25in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b style=""><span style="font-size: 9pt; line-height: 150%; color: black;">2h &lt;&nbsp;&nbsp;Tổng số thời gian đi trễ&nbsp; <u>&lt;</u>&nbsp; 3h</span></b></p></td>'
            . '<td width="168" nowrap="" colspan="2" style="width: 1.75in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpMiddle" align="center" style="margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b style=""><span style="font-size: 9pt; line-height: 150%; color: black;">Tổng số thời gian đi trễ&nbsp; &gt;&nbsp; 3h </span></b></p></td>'
            . '<td width="240" nowrap="" colspan="2" style="width: 2.5in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpLast" align="center" style="margin-left: 0in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b style=""><span style="font-size: 9pt; line-height: 150%; color: black;">Quên check in từ lần thứ 3 trở đi </span></b></p></td>'
            . '<td width="252" style="width: 189pt; padding: 0in; height: 24.75pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
            . '<td width="121" nowrap="" style="width: 90.75pt; border-top: none; border-left: 1pt solid windowtext; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Thời gian vi phạm</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
            . '<td width="203" nowrap="" style=" text-align: center ; width: 152.25pt; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$thoi_gian_1.'</span></td>'
            . '<td width="168" nowrap="" colspan="2" style=" text-align: center ; width: 1.75in; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$thoi_gian_2.'</span></td>'
            . '<td width="240" nowrap="" colspan="2" style=" text-align: center ;  width: 2.5in; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$forget_check_in_day.'</span></td>'
            . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
            . '<td width="121" nowrap="" style="width: 90.75pt; border-top: none; border-left: 1pt solid windowtext; border-bottom: none; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Mức phạt</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
            . '<td width="203" nowrap="" style="width: 152.25pt; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b style=""><i style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: rgb(89, 89, 89);"></span></i></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$phat_1.'</span></p></td>'
            . '<td width="168" nowrap="" colspan="2" style="text-align: center ; width: 1.75in; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$thoi_gian_2_phat.'</span></td>'
            . '<td width="240" nowrap="" colspan="2" style="width: 2.5in; border-top: none; border-left: none; border-bottom: 1pt solid rgb(166, 166, 166); border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><span style="font-size: 9pt;text-align: center; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: rgb(89, 89, 89);">'.$phat_forget_check_in_day.'</span></p></td>'
            . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
            . '<td width="121" nowrap="" style="width: 90.75pt; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
            . '<td width="611" nowrap="" colspan="5" style="width: 458.25pt; border-top: 1pt solid rgb(166, 166, 166); border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid black; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Tổng mức phạt: '.$tong_phat.'</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
            . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 32.25pt;">'
            . '<td width="732" colspan="6" style="width: 549pt; background: white; padding: 0in 5.4pt; height: 32.25pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family:">Bạn vui lòng phản hồi nếu thông tin chưa chính xác hoặc cần điều chỉnh qua email phuongnga.nguyen@oppo-aed.vn hoặc hotline 3920 2555 - Ext: 105 gặp Ms. Nga Nhân sự.
              <b style="">Deadline phản hồi trước '.$time_dealine.'.</b> <br style="">
              Phòng Nhân sự sẽ căn cứ nội dung trong mail để thực hiện xử phạt theo đúng quy định Công ty.</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
            . '<td width="252" style="width: 189pt; padding: 0in; height: 32.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="">'
            . '<td width="121" style="width: 90.75pt; padding: 0in;"></td>'
            . '<td width="203" style="width: 152.25pt; padding: 0in;"></td>'
            . '<td width="24" style="width: 0.25in; padding: 0in;"></td>'
            . '<td width="144" style="width: 1.5in; padding: 0in;"></td>'
            . '<td width="30" style="width: 22.5pt; padding: 0in;"></td>'
            . '<td width="210" style="width: 157.5pt; padding: 0in;"></td>'
            . '<td width="252" style="width: 189pt; padding: 0in;"></td></tr></tbody></table><p class="MsoNormal" ><span style="color: rgb(31, 73, 125);">&nbsp;</span></p><div><table class="MsoNormalTable" cellspacing="0" cellpadding="0" style="border-collapse: collapse;"> <tbody > <tr style="height: 38.9pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"><p class="MsoNormal"><b style="color: #028553;font-family:sans-serif;font-size: 15px;">Ms.NGUYEN THI PHUONG NGA</b><br><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">C&B Specialist</span></p></td></tr><tr style="height: 37.5pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 37.5pt;"><p class="MsoNormal" ><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">T: <span class="Object" id="OBJ_PREFIX_DWT68_com_zimbra_phone"><a style="color: black;" href="callto:(84-8) 3920 2555" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()">(84-8) 3920 2555</a></span> - Ext: 105 <br>E: <span class="Object" role="link" id="OBJ_PREFIX_DWT69_ZmEmailObjectHandler"><a href="mailto:phuongnga.nguyen@oppo-aed.vn" target="_blank">phuongnga.nguyen@oppo-aed.vn</a></span><br> M: <span class="Object" id="OBJ_PREFIX_DWT68_com_zimbra_phone"><a href="callto:(84) 036 6001 601" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()" style="color: black;">(84) 036 6001 601</a></span> <br></span></span><span style="color: black;"></span> </p> </td> </tr> <tr style="height: 38.9pt;"> <td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"> <p class="MsoNormal"> <b style="color: #028553; font-size: 12px;font-family:arial, sans-serif;">OPPO Authorized Exclusive Distributor<br>CÔNG TY CỔ PHẦN KỸ THUẬT & KHOA HỌC VĨNH KHANG</b> <span style="font-size: 10pt; font-family: arial, sans-serif; color: black;"><br >12<sup >th</sup><b >&nbsp;</b>Floor, Lim II Tower<br>No. 62A CMT8 St., Dist. 3, HCMC, VN</span><span style="color: rgb(31, 73, 125);"></span> </p> </td> </tr> <tr style="height: 16.5pt;"> <td width="374" style="width: 3.9in; padding: 0in 5.4pt; height: 16.5pt;"> <p class="MsoNormal" ><span style="color: #028553;"><span class="Object" role="link" id="OBJ_PREFIX_DWT71_com_zimbra_url"><a style="text-decoration-color:#028553" href="http://www.oppomobile.vn/" target="_blank" ><span style="font-size: 10pt; font-family: arial, sans-serif; color:#028553;font-weight: 600;">www.oppomobile.vn</span></a></span></span> </p> </td> </tr> </tbody> </table><p class="MsoNormal" ><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div><p class="MsoNormal" ><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div></div></div></body>';

        $subject = 'THÔNG BÁO VI PHẠM CHUYÊN CẦN THÁNG '.$month.'/'.$year;
        if($check_email){
            $mailto[] = 'congtrang.huynh@oppo-aed.vn';
//            $ccStaff[] = 'dieptruc.nguyen@oppo-aed.vn';
        }
        else{
            $mailto[] = $data[0]['email'];
        }

        $res = $this->sendmailnew($mailto, $subject, $content_new,NULL,$ccStaff);
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if($res == 1){
            $data_send_mail = array(
                'staff_id' => $value,
                'month'    => $month,
                'year'     =>$year,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id
            );
            $QLogSendMail->insert($data_send_mail);
            $reponse['status']=1;
            $reponse['message']="Thành công";
        }else{
            $reponse['status']=$res;
            $reponse['message']="Lỗi";
        }
    }
}
echo json_encode($reponse);
