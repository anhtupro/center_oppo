<?php
    $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('year');
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        
        $db = Zend_Registry::get('db');
        $datetime = date('Y-m-d H:i:s');
        $sql = "DELETE FROM log_staff_warning WHERE penalty_day = '$to'";
        $stmt = $db->prepare($sql);
        $stmt->execute();
      
         $selectStaff = $db->select()->from(array('s'=>'log_staff_warning'),array('staff_id' => 's.staff_id'))
//            ->where("YEAR(s.penalty_day) = $year ", null)
            ->group('s.staff_id');
        $data_staff = $db->fetchAll($selectStaff);

        if(!empty($data_staff)){
            foreach ($data_staff as $k => $val){
                $arr_locked_yellow[]=$val['staff_id'];
            }
        }
        $params = array(
            'from_date'     => $from,
            'to_date'       => $to,
            'month'         => $month,
            'year'          => $year,
            'department'    => NULL,
            'team'          => NULL,
            'title'         => NULL,
            'code'          => NULL,
            'name'          => NULL,
            'area'          => NULL
        );
        $QCheckIn = new Application_Model_CheckIn();
        $data = $QCheckIn->listLate($params);
        $QLogStaffWarning  = new Application_Model_LogStaffWarning();
        foreach ($data as $value) {
            
            if($value['ngay_phat'] > 0 && $value['total_late'] > 180){
                  $tong_phat  = $value['ngay_phat'] . ' ngày + '.  $value['thoi_gian'];
                }elseif($value['ngay_phat'] == 0 && $value['total_late'] > 180){
                    $tong_phat  =  $value['thoi_gian'];
                }elseif($value['ngay_phat'] > 0){
                    $tong_phat  = $value['ngay_phat'] . ' ngày';
                }else{
                    $tong_phat  = 0;
                }
            $data_insert = array(
                'staff_id' => $value['staff_id'],
                'penalty_day' => $to,
                'late_1' => ($value['total_late'] <= 180 && $value['total_late'] > 60) ? $value['thoi_gian'] : '',
                'late_2' => ($value['total_late'] > 180 ) ? $value['thoi_gian'] : '',
                'tong_phat' => $tong_phat,
                'locked_day' => $datetime,
                'warning_type' => (!in_array($value['staff_id'], $arr_locked_yellow ) || $tong_phat == 0 ) ? 1 : 3,
                'not_check_in' => $value['forget_check_in']
            );
            
            $QLogStaffWarning->insert($data_insert);
            
        }
            
//        $sql = "CALL `PR_log_diligence_fix`(:from_date, :to_date)";
    
//        $stmt = $db->prepare($sql);
//        $stmt->bindParam('from_date', $from, PDO::PARAM_STR);
//        $stmt->bindParam('to_date', $to, PDO::PARAM_STR);
//        
//        $stmt->execute();
//      
//        $stmt->closeCursor();
        echo json_encode (array(
                'status' => 1,
                'message' => "success"
            ));