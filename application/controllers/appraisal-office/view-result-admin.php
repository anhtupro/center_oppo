<?php
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $QToDo = new Application_Model_AppraisalOfficeToDo();

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;
    $is_head = $QToDo->checkIsHead($userStorage->id);
    $current_lang = $userStorage->defaut_language;

    if(!$is_admin AND !$is_head){
        $this->_redirect("/appraisal-office/index-staff");
    }

    $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
    $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
    $QTeam = new Application_Model_Team();
    
    $level = $this->storage['level'];

    $params = [
        'staff_head_id' => $userStorage->id,
        'is_admin'      => $is_admin,
        'staff_head_id' => $userStorage->id,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
    ];
    $all_member = $QAppraisalOfficeMember->getAllMemberApprovedResult($params);
    
    $get_plan = $QAppraisalOfficePlan->getListPlan($params);
    
    $plan = [];
    foreach($get_plan as $key=>$value){
        $plan_tmp = $value;
        $plan_tmp['aop_name'] = ($current_lang == 1) ? $value['aop_name'] : $value['aop_name_eng'];
        $plan[] = $plan_tmp;
    }
    
    $member = [];
    foreach($all_member as $key=>$value){
        if($current_lang == 1){
            $member[$value['aop_name']][$value['star']][] = $value;
        }
        else{
            $member[$value['aop_name_eng']][$value['star']][] = $value;
        }
    }

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();

    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;

    $this->view->member = $member;
    $this->view->plan = $plan;

    $this->view->params = $params;
    
    
    