<?php

    $db              = Zend_Registry::get('db');
    $db->beginTransaction();

    try {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }
        
        $plan_id = $this->getRequest()->getParam('plan_id');
        $plan_old_id = $this->getRequest()->getParam('plan_old_id');
        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        
        $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
        $QAppraisalOfficeField = new Application_Model_AppraisalOfficeField();
        $QAppraisalOfficeTask = new Application_Model_AppraisalOfficeTask();
        
        $where   = array();
        $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aop_id = ?', $plan_old_id);
        $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aom_staff_id = ?', $userId);
        $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aom_is_deleted = 0');
        
        $member = $QAppraisalOfficeMember->fetchRow($where)->toArray();
        
        //Update member mới del = 1
        $where   = array();
        $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aop_id = ?', $plan_id);
        $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aom_staff_id = ?', $userId);
        $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aom_is_deleted = 0');
        $QAppraisalOfficeMember->update(['aom_is_deleted' => 1], $where);
        //End update
        
        if(!$member){
            echo json_encode([
                'status' => 1,
                'message' => 'Quý trước chưa có thiết lập (member)!'
            ]);
            return;
        }
        
        $fields = $QAppraisalOfficeField->fetchAll(['aom_id = ?' => $member['aom_id'], 'aof_is_deleted = ?' => 0])->toArray();
        

        if(!$fields){
            echo json_encode([
                'status' => 1,
                'message' => 'Quý trước chưa có thiết lập (field)!'
            ]);
            return;
        }
        
        //Insert data
        $data_member = [
            'aop_id' => $plan_id,
            'aom_contact_id' => $member['aom_contact_id'],
            'aom_head_department_id' => $member['aom_head_department_id'], 
            'aom_staff_id' => $member['aom_staff_id'],
            //'aom_head_approved' => $member['aom_head_approved'],
            //'aom_staff_approved' => $member['aom_staff_approved'],
            'aom_init_ratio' => $member['aom_init_ratio'],
            'aom_created_at' => date('Y-m-d H:i:s'),
            'aom_created_by' => $userId,
        ];
        $id_member = $QAppraisalOfficeMember->insert($data_member);
        
        foreach($fields as $field){
            
            $data_field = [
                'aom_id' => $id_member,
                'aof_name' => $field['aof_name'],
                'aof_created_at' => date('Y-m-d H:i:s'),
                'aof_created_by' => $userId,
            ];
            $id_field = $QAppraisalOfficeField->insert($data_field);
            
            $tasks = $QAppraisalOfficeTask->fetchAll(['aof_id = ?' => $field['aof_id'], 'aot_is_deleted = ?' => 0])->toArray();
            
            foreach($tasks as $task){
                $data_task = [
                    'aof_id' => $id_field,
                    'aot_target' => $task['aot_target'],
                    //'aot_target_review' => $task['aot_target_review'],
                    'aot_task' => $task['aot_task'],
                    'aot_skill' => $task['aot_skill'],
                    'aot_ratio' => $task['aot_ratio'],
                    'aot_created_at' => date('Y-m-d H:i:s'),
                    'aot_created_by' => $userId,
                ];
                $QAppraisalOfficeTask->insert($data_task);
            }
        }
        
        //END Insert data
        
        echo json_encode([
            'status' => 0,
            'data' => [
                'plan_id' => $plan_id,
                'plan_old_id' => $plan_old_id,
                'member' => $member,
                'userId'  => $userId,
                'data_member' => $data_member,
                'fields' => $fields,
                'tasks' => $tasks,
            ]
        ]);
        $db->commit();
        return;
            
    } catch (Exception $ex) {
        echo json_encode([
            'status' => 1,
            'message' => $ex->getMessage()
        ]);
        $db->rollback();
        return;
    }
    
    
    