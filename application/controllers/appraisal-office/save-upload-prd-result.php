<?php
    
    

    $plan_id = $this->getRequest()->getParam('plan_id');
    $no_result = $this->getRequest()->getParam('no_result');

    include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';

    error_reporting(0);
    set_time_limit(0);

    $current_month = date('m', strtotime(date('Y-m')." -1 month"));
    $current_year  = date('Y', strtotime(date('Y-m')." -1 month"));

    echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';
    echo '<div class="theme-showcase">';
    //================Process Uploaded File====================
    If($_FILES["file"]["tmp_name"] != ""){
        if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
                //die("Không phải file XLSX !");
        }
        if ($_FILES["file"]["error"] > 0)
        {
        die("Return Code: " . $_FILES["file"]["error"] . "<br />");
        }
        move_uploaded_file($_FILES["file"]["tmp_name"], "files/appraisal-ofice/data.xlsx");
    }
    //===================================================

    //===================Main=======================

    $inputfile = 'files/appraisal-ofice/data.xlsx';

    $xlsx = new SimpleXLSX($inputfile);
    $data = $xlsx->rows();

    

    
    $i = 0;
    $num_cols = 0;
    while ($data[0][$i++] <> '') {
      $num_cols++;
    }

    define('fullname',          0);
    define('code',              1);
    define('nguoi_danh_gia',    2);
    define('linh_vuc',	 	3);
    define('tieu_chuan', 	4);
    define('nhiem_vu', 		5);
    define('ky_nang', 		6);
    define('ty_le', 		7);
    define('ket_qua', 		8);

    $i = 1;
    $num_rows = 0;
    while ($data[$i++][4] <> '') {
      $num_rows++;
    }

    $all_data = [];

    $fullname = NULL;
    $code = NULL;
    $linh_vuc = NULL;
    for($i = 1; $i <= $num_rows; $i++){

        $fullname = !empty($data[$i][fullname]) ? $data[$i][fullname] : $fullname;
        $code = !empty($data[$i][code]) ? trim($data[$i][code]) : trim($code);
        $nguoi_danh_gia = !empty($data[$i][nguoi_danh_gia]) ? trim($data[$i][nguoi_danh_gia]) : trim($nguoi_danh_gia);
        $linh_vuc = !empty($data[$i][linh_vuc]) ? $data[$i][linh_vuc] : $linh_vuc;
        $tieu_chuan = $data[$i][tieu_chuan];
        $nhiem_vu = $data[$i][nhiem_vu];
        $ky_nang = $data[$i][ky_nang];
        $ty_le = $data[$i][ty_le];
        $ket_qua = $data[$i][ket_qua];

        $all_data[] = [
            'fullname' => $fullname,
            'code'  => $code,
            'nguoi_danh_gia' => $nguoi_danh_gia,
            'linh_vuc' => $linh_vuc,
            'tieu_chuan' => $tieu_chuan,
            'nhiem_vu' => $nhiem_vu,
            'ky_nang' => $ky_nang,
            'ty_le' => $ty_le,
            'ket_qua' => $ket_qua
        ];
    }

    $task = [];
    foreach($all_data as $key=>$value){
        $task[$value['code']][$value['linh_vuc']][] = $value;
    }
    
    $nguoi_danh_gia = [];
    foreach($all_data as $key=>$value){
        $nguoi_danh_gia[$value['code']] = $value['nguoi_danh_gia'];
    }
    
    
    $ketqua= [
        'A+' => 5,
        'A' => 4,
        'B' => 3,
        'C' => 2,
        'D' => 1,
    ];
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
    $QStaff = new Application_Model_Staff();
    $QAppraisalOfficeField = new Application_Model_AppraisalOfficeField();
    $QAppraisalOfficeTask = new Application_Model_AppraisalOfficeTask();
    
    if ($this->getRequest()->getMethod() == 'POST') {

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            foreach($task as $key=>$value){
                $aom_staff = $QStaff->fetchRow(['code = ?' => $key]);
                
                if(empty($aom_staff)){
                    echo "Nhân viên không tồn tại!:".$key;
                    $db->rollback();
                    exit;
                }
                
                $aom_head_department = $QStaff->fetchRow(['code = ?' => $nguoi_danh_gia[$key]]);
                
                if(empty($aom_head_department)){
                    echo "Người đánh giá không tồn tại!:".$key;
                    $db->rollback();
                    exit;
                }
                
                //Ai upload rồi thì ko cập nhật nữa
                $where = [];
                $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aop_id = ?', $plan_id);
                $where[] = $QAppraisalOfficeMember->getAdapter()->quoteInto('aom_staff_id = ?', $aom_staff->id);
                $QAppraisalOfficeMember->update( ['aom_is_deleted' => 1], $where);
                
                //Insert AppraisalOfficeMember
                $data_member = [
                    'aop_id' => $plan_id,
                    'aom_contact_id' => NULL,
                    'aom_head_department_id' => $aom_head_department->id,
                    'aom_staff_id' => $aom_staff->id,
                    'aom_head_approved' => 0,
                    'aom_staff_approved' => 0,
                    'aom_init_ratio' => 100,
                    'aom_created_at' => date('Y-m-d H:i:s'),
                    'aom_created_by' => $userStorage->id,
                    'aom_updated_at' => NULL,
                    'aom_updated_by' => NULL,
                    'aom_is_deleted' => 0,
                ];
                
                $aom_id = $QAppraisalOfficeMember->insert($data_member);

                foreach($value as $k=>$v){
                    $field_insert = [
                        'aom_id' => $aom_id,
                        'aof_name' => $k,
                        'aof_created_at' => date('Y-m-d H:i:s'),
                        'aof_created_by' => $userStorage->id,
                    ];


                    $field_id = $QAppraisalOfficeField->insert($field_insert);

                    foreach($v as $i=>$j){

                        $ty_le = $j['ty_le']*100;
                        $ket_qua = $ketqua[$j['ket_qua']];
                        
                        if(empty($ket_qua) AND empty($no_result)){
                            echo "Kết quả không chính xác!";
                            $db->rollback();
                            exit;
                        }

                        if($ty_le <= 100 AND $ty_le > 0){
                            $task_insert = [
                                'aof_id'        => $field_id,
                                'aot_target'    => $j['tieu_chuan'],
                                'aot_task'      => $j['nhiem_vu'],
                                'aot_skill'     => $j['ky_nang'],
                                'aot_ratio'     => $ty_le,
                                'aot_head_department_appraisal' => !empty($no_result) ? 0 : $ket_qua
                            ];
                            $task_id = $QAppraisalOfficeTask->insert($task_insert);
                        }
                        else{
                            echo "Tỷ lệ không chính xác!";
                            $db->rollback();
                            exit;
                        }
                    }
                }
                echo "Hoàn Thành: ".$key."<br>";
            }
            $db->commit();

        } catch (Exception $e) {
            $db->rollback();
            echo 'Caught exception: ', $e->getMessage(), "\n";exit;
        }

        unlink($inputfile);


    }

    $this->_helper->layout->disableLayout();