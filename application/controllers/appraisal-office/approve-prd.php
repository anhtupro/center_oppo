<?php
    $planId = $this->getRequest()->getParam('plan_id');
    $aom_staff_id = $this->getRequest()->getParam('aom_staff_id');
    $rollback = $this->getRequest()->getParam('rollback'); 
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $review_staff = $aom_staff_id;


    $QPlan = new Application_Model_AppraisalOfficePlan();
    $QStaff = new Application_Model_Staff();


    $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $planId]);
    $this->view->staff = $QStaff->fetchRow(['id = ?' => $review_staff]);
    $this->view->rollback = $rollback;
    
    
    