<?php
    $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
    $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $params = [
        'staff_id' => $userStorage->id
    ];
    $all_member = $QAppraisalOfficeMember->getAllMemberApproved($params);

    $plan = $QAppraisalOfficePlan->getListPlan($params);

    $plan_active = $QAppraisalOfficePlan->checkActivePlan($userStorage->id);

    $member = [];

    foreach($all_member as $key=>$value){
        $member[$value['aop_name']] = $value;
    }

    $this->view->params = $params;
    $this->view->member = $member;
    $this->view->plan = $plan;
    $this->view->plan_active = $plan_active;
    
    
    