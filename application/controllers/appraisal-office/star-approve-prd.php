<?php
    $planId = $this->getRequest()->getParam('plan_id');
    $aom_staff_id = $this->getRequest()->getParam('aom_staff_id');
        
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();


    $QPlan = new Application_Model_AppraisalOfficePlan();
    $QStaff = new Application_Model_Staff();


    $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $planId]);
    $this->view->staff = $QStaff->fetchRow(['id = ?' => $aom_staff_id]);
    
    
    