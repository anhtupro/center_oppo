<?php

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $planId = $this->getRequest()->getParam('plan_id');
    $aom_staff_id = $this->getRequest()->getParam('aom_staff_id');
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $QToDo = new Application_Model_AppraisalOfficeToDo();
    $staff = $QToDo->getStaff($planId, $aom_staff_id);
    if(count($staff) > 0) {
            $staff = $staff[0];
    }

    $QMember = new Application_Model_AppraisalOfficeMember();
    $member = $QMember->fetchRow(sprintf(
            'aom_staff_id = %s and aom_is_deleted = 0 and aop_id = %s',
            $aom_staff_id,
            $planId
    ));

    $fields = [];
    if ($member) {
            $member = $member->toArray();
            $QField = new Application_Model_AppraisalOfficeField();
            $fields = $QField->fetchAll(sprintf(
                    'aom_id = %s and aof_is_deleted = 0',
                    $member['aom_id']
            ))->toArray();
    }

    $tasks = [];
    if (count($fields) > 0) {
            $fieldIds = [];
            foreach ($fields as $field) {
                    $fieldIds[] = $field['aof_id'];
            }
            $QTask = new Application_Model_AppraisalOfficeTask();
            $tasks = $QTask->fetchAll(sprintf(
                    'aof_id in (%s) and aot_is_deleted = 0',
                    implode(', ', $fieldIds)
            ))->toArray();
    }
    
    

    $crossReviews = [];
    if (count($tasks) > 0) {
            $taskIds = [];
            foreach ($tasks as $task) {
                    $taskIds[] = $task['aot_id'];
            }

            /** @var Zend_Db_Adapter_Abstract $db */
            $db = Zend_Registry::get('db');
            $stmt = $db
                    ->select()
                    ->from(['cr' => 'appraisal_office_cross_review'], ['cr.*'])
                    ->join(['s' => 'staff'], 'cr.aocr_staff_id = s.id', ['s.id', 's.code', 's.lastname', 's.firstname', 's.email', 's.department', 's.team', 's.title'])
                    ->where(sprintf(
                            'aot_id in (%s) and aocr_is_deleted = 0',
                            implode(', ', $taskIds)
                    ));
            $crossReviews = $db->fetchAll($stmt);
    }
    

    //$QHeadDepartment = new Application_Model_HeadOfDepartment();
    //$headId = $QHeadDepartment->getHeadIdByDepartment($staff['department']);

    $headId = $QToDo->getStaffApproveByStaff($aom_staff_id);
    $is_approve = ($headId['from_id'] == $userStorage->id) ? 1 : 0;
    
    if($userStorage->id == 8807 || $userStorage->id == 2584){
        $is_approve = 1;
    }
    
    echo json_encode([
            'plan_id' => $planId,
            'head_id' => $headId,
            'is_approve' => $is_approve,
            'staff' => $staff,
            'member' => $member,
            'fields' => $fields,
            'tasks' => $tasks,
            'cross_reviews' => $crossReviews
    ]);
    
    