<?php
    $planId = $this->getRequest()->getParam('plan_id');
        
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $review_staff = $userStorage->id;


    $QPlan = new Application_Model_AppraisalOfficePlan();
    $QStaff = new Application_Model_Staff();


    $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $planId]);
    $this->view->staff = $QStaff->fetchRow(['id = ?' => $review_staff]);
    
    
    