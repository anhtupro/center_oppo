<?php
    $title_id = $this->getRequest()->getParam('title_id');
    $title_recommend = $this->getRequest()->getParam('title_recommend');
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    if(!$title_id){
        echo json_encode([
            'status' => 1,
            'message' => 'Thiếu chức danh'
        ]);
        exit;
    }
    
    if(!title_recommend){
        echo json_encode([
            'status' => 1,
            'message' => 'Thiếu chức danh gợi ý'
        ]);
        exit;
    }
    
    
    $db = Zend_Registry::get('db');
    try {
        $db->beginTransaction();
        
        $QCapacity = new Application_Model_Capacity();
        $QCapacityField = new Application_Model_CapacityField();
        $QCapacityFieldDictionary = new Application_Model_CapacityFieldDictionary();
        $QCapacityScience = new Application_Model_CapacityScience();
        
        if($title_recommend){
            //Copy từ chức danh sang chức danh
            
            $where = NULL;
            $where = $QCapacity->getAdapter()->quoteInto('title_id = ?', $title_id);
            $QCapacity->update(['is_deleted' => 1], $where);
            
            $data = [
                'title_id' => $title_id,
                'is_deleted' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
            ];
            $capacity_id = $QCapacity->insert($data);

            $fields = $QCapacity->getField($title_recommend);
            foreach($fields as $key=>$value){

                $data_field = [
                    'capacity_id' => $capacity_id,
                    'title' => $value['title'],
                    'kpi' => $value['kpi'],
                    'desc' => $value['desc'],
                    'ratio' => $value['ratio'],
                ];
                $field_id = $QCapacityField->insert($data_field);

                $dictionary = $QCapacityFieldDictionary->fetchAll(['field_id = ?' => $value['field_id'], 'is_deleted = ?' => 0]);


                foreach($dictionary as $k=>$v){
                    $data_dictionary = [
                        'field_id' => $field_id,
                        'dictionary_id' => $v['dictionary_id'],
                        'level' => $v['level'],
                        'share' => $v['share'],
                    ];
                    $QCapacityFieldDictionary->insert($data_dictionary);
                }

                $dictionary_science = $QCapacityScience->fetchAll(['capacity_field_id = ?' => $value['field_id'], 'is_deleted = ?' => 0]);

                foreach($dictionary_science as $k=>$v){
                    $data_science = [
                        'capacity_field_id' => $field_id,
                        'desc' => $v['desc'],
                        'share' => $v['share'],
                        'level' => $v['level'],
                    ];
                    $QCapacityScience->insert($data_science);
                }

            }
            
        }

        $db->commit();
        
        echo json_encode([
            'status' => 0,
            'message' => 'success'
        ]);
        exit;
        
    } catch (Exception $e) {

        $db->rollback();

        echo json_encode([
            'status' => 1,
            'message' => 'Error System '.$e->getMessage()
        ]);
        exit;
    }
    
    
    