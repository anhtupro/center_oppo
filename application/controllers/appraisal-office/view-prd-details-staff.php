<?php
    $staff_id = $this->getRequest()->getParam('staff_id');
    $plan_id = $this->getRequest()->getParam('plan_id');
    $view_star = $this->getRequest()->getParam('view_star');


    $userStorage = Zend_Auth::getInstance()->getStorage()->read();


    $QPlan = new Application_Model_AppraisalOfficePlan();
    $QStaff = new Application_Model_Staff();
    $QToDo = new Application_Model_AppraisalOfficeToDo();
    $QField = new Application_Model_AppraisalOfficeField();

    $list_staff = $QToDo->getListStaffView($plan_id, NULL, $staff_id, NULL);

    $get_field = $QField->getFieldPrd($plan_id, NULL);
    $get_task = $QField->getTaskPrd($plan_id, NULL);


    $field = [];
    foreach($get_field as $key=>$value){
        $field[$value['aom_staff_id']][] = $value;
    }

    $task = [];
    foreach($get_task as $key=>$value){
        $task[$value['aom_staff_id']][$value['aof_id']][] = $value;
    }

    $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $plan_id]);
    //$this->view->staff = $QStaff->fetchRow(['id = ?' => $review_staff]);
    $this->view->list_staff = $list_staff;
    $this->view->field = $field;
    $this->view->task = $task;
    $this->view->viewStar = $star;
    $this->view->view_star = $view_star;
    
    
    