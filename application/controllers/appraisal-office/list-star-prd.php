<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;

    $QTeam = new Application_Model_Team();
    $QAppraisalOfficeToDo = new Application_Model_AppraisalOfficeToDo();
    $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    $plan = $QAppraisalOfficePlan->fetchRow(['aop_id = ?' => $plan_id]);
    
    
    //Nếu admin thì cũng approve như thường
    $level = $this->storage['level'] ? ($this->storage['level'] = 1 ? 2 : $this->storage['level']) : NULL;
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $QMember = new Application_Model_AppraisalOfficeMember();
    $page = $this->getRequest()->getParam('page', 1);
    $limit = 20;
    $total = 0;
    $result = $QMember->fetchPaginationPrdApprove2($page, $limit, $total, $params);

    $this->view->plan = $plan;
    
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'appraisal-office/list-star-prd' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    
    