<?php
    $plan_id = $this->getRequest()->getParam('plan_id');
    
    $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id = $userStorage->id;
    
    $list_plan = $QAppraisalOfficePlan->fetchAll(['aop_to >= ?' => date('Y-m-d'), 'aop_type = ?' => 1, 'aop_is_deleted = ?' => 0]);
    
    $list_plan_staff_result = $QAppraisalOfficePlan->getListPlanStaff($staff_id);
    
    $list_plan_staff = [];
    foreach($list_plan_staff_result as $key=>$value){
        $list_plan_staff[$value['aop_id']] = [
            'aop_id' => $value['aop_id'],
            'aom_id' => $value['aom_id'],
            'set_prd' => $value['set_prd'],
            'aom_staff_id' => $value['aom_staff_id'],
            'aom_staff_approved' => $value['aom_staff_approved'],
            'aop_status' => $value['aop_status']
        ];
    }
    
    
    $this->view->list_plan = $list_plan;
    $this->view->list_plan_staff = $list_plan_staff;
    
    
    