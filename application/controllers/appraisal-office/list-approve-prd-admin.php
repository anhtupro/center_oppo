<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');
    $status = $this->getRequest()->getParam('status');
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;

    $QTeam = new Application_Model_Team();
    $QAppraisalOfficeToDo = new Application_Model_AppraisalOfficeToDo();
    $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    $plan = $QAppraisalOfficePlan->fetchRow(['aop_id = ?' => $plan_id]);
    
    //Nếu admin thì cũng approve như thường
    $level = $this->storage['level'];
    $list_plan = $QAppraisalOfficePlan->fetchAll(['aop_to >= ?' => date('Y-m-d'), 'aop_type = ?' => 1, 'aop_is_deleted = ?' => 0]);
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
        'status' => $status
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $QMember = new Application_Model_AppraisalOfficeMember();
    $page = $this->getRequest()->getParam('page', 1);
    $limit = 20;
    $total = 0;
    $result = $QMember->fetchPaginationPrdApprove2($page, $limit, $total, $params);

    $this->view->plan = $plan;
    $this->view->kpi_status = unserialize(KPI_STATUS);
    $this->view->list_plan = $list_plan;
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'appraisal-office/list-approve-prd-admin' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    
    