<?php

define('ASM', 179);
define('ASM_STANDBY', 181);
define('PGPB', 182);
define('SALES', 183);
define('SALES_LEADER', 190);
define('SENIOR_PROMOTER', 293);
define('PB_SALES', 312);
define('STORE_LEADER', 373);

class ManageStoreStaffController extends My_Controller_Action{

	public function indexAction(){
		// $result            = $this->getRequest()->getParam('store_id');
		// echo $result; die;
		//$this->view->test123 = "123123";

		
		// $this->view->type = $type;
		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
		    $this->_helper->layout->disableLayout();
		    $this->_helper->viewRenderer->setNoRender();

		    $SearchStore = $this->getRequest()->getParam('txt_store');

		    //echo $SearchStaff;die;
		    $db     = Zend_Registry::get('db');

		    $sql  = 'SELECT id,`name` FROM store WHERE `name` LIKE "%'.$SearchStore.'%"';
		    $stmt = $db->prepare($sql);

		    $stmt->execute();

		    $data = $stmt->fetchAll();
		    $stmt->closeCursor();
		    $db = $stmt = null;

		    $this->view->store_ajax = $data;
		    // echo "<pre>";print_r($data);die;
		    $this->render('partials/search-store');
		}
		 
	}

	public function staffAction(){
		
		$list_allow_title = "182,183,293,312,373,190,181,179";
		 
		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
		    
		    	$SearchStaff = $this->getRequest()->getParam('txt_staff');
		    	$db     = Zend_Registry::get('db');
		    	
		    	// echo $SearchStaff;die;
		    	$fullname = $code = $email = '%' . $SearchStaff . '%';
		    	
		    	$sql ='SELECT s.id,s.title,t.name as title_name,s.code,s.email,CONCAT(s.firstname, " ", s.lastname) as fullname 
		    			FROM staff as s 
		    			INNER JOIN team as t
		    			ON s.title = t.id
		    			WHERE off_date IS NULL
		    			AND ( CONCAT(s.firstname, " ", s.lastname) LIKE :fullname ) OR ( s.code LIKE :code ) OR ( s.email = IF(POSITION("@oppomobile.vn" IN :email) > 0, :email, CONCAT(:email, "@oppomobile.vn")) )
		    			HAVING title IN ('.$list_allow_title.')';
		    	// echo $sql;die;

				$stmt = $db->prepare($sql);
				$stmt->bindParam("fullname",  $fullname, PDO::PARAM_STR);
				$stmt->bindParam("code",  $code, PDO::PARAM_STR);
            	$stmt->bindParam("email",  $email, PDO::PARAM_STR);
			    $stmt->execute();

			   	// $data2 = [];
			    $data2 = $stmt->fetchAll();
			    $stmt->closeCursor();
			    $db = $stmt = null;

			    //echo json_encode($json);
			    $this->view->staff_ajax = $data2;
			    //echo "<pre>";print_r($data2);die;
			    // echo "<pre>";print_r(json_encode($data2, true));die;
			    $this->render('partials/search-staff');
		    
		}
		
	}
	public function saveAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){

			$userStorage = Zend_Auth::getInstance()->getStorage()->read();

			$manage_store_staff = new Application_Model_ManageStoreStaff();
			$id_store =  $this->getRequest()->getParam('store_id');
			$id_staff =  $this->getRequest()->getParam('staff_id');
			$type     =  $this->getRequest()->getParam('type');
			$from     =  $this->getRequest()->getParam('from');
			$is_level =  $this->getRequest()->getParam('is_level');
			// echo "<pre>";print_r($userStorage);die;
			$list_store = implode(",",$id_store);
			
			
		 
			if(empty($from)) 
				exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Vui lòng chọn ngày gán shop.')));
			if(!$id_staff) 
				exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Vui lòng chọn Staff.')));
			if(count($id_store) <= 0)
				exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Vui lòng chọn Store.')));
			
			$get_title = $manage_store_staff->get_title($id_staff);
			$from	= $from ? DateTime::createFromFormat('d/m/Y',$from)->format('Y-m-d') : NULL;
			$from_view = strtotime($from.date(' H:i:s'));
			$data = $manage_store_staff->get_day_transfer($id_staff);

			$check_province = $manage_store_staff->check_province($id_staff,$list_store);
			/*KIỂM TRA CÁC ĐIỀU KIỆN CHUNG CỦA PGPB,PB SALES,SALES,SALES LEADER*/
			// echo "<pre>";print_r($data);die;
			
			if($check_province == 0)
			{
				exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Nhân viên không cùng tỉnh với shop này nên không thể gán , xin vui lòng kiểm tra lại.')));
			}
			

			/*KIỂM TRA TẤT CẢ CÁC ĐIỀU KIỆN KHI GÁN SHOP CỦA PGPB,PB SALES,SALES,SALES LEADER*/

			if($type == 1)
			{
				$table = "store_staff_log";
				$check_store = $manage_store_staff->check_store($table,$id_staff,$list_store);
				$check_leader_is_sales = $manage_store_staff->check_leader_is_sales($id_staff);
				$check_pgpb = $manage_store_staff->check_pgpb($list_store);

				// echo "<pre>";print_r($check_pgpb);die;

				$info = array(
								's_id'		=> Zend_Auth::getInstance()->getStorage()->read()->id,
								'type'		=> $type,
								'is_level'	=> $is_level,
								'staff_id'	=> $id_staff,
								'store_id'	=> $id_store,
								'date'		=> $from.date(' H:i:s')
						);

				if($get_title['title'] == 190  && $is_level == 0 && $check_store )
				{
					exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Shop này đã có PGPB bạn không thể gán thêm PGPB, xin vui lòng kiểm tra lại.')));
				}
				if($get_title['title'] == 182  && ($is_level == 0 || $is_level == 2))
				{
					exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Sales Leader không thể gán vào shop với chức danh là PGPB hoặc PB Sales , xin vui lòng kiểm tra lại.')));
				}
				if(count($check_store) > 0)
				{
					exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Nhân viên này đã được gán vào shop này rồi nên không thể gán được nữa , xin vui lòng kiểm tra lại.')));
				}
				if($get_title['title'] == 190 && count($check_leader_is_sales) > 6)
				{
					exit(json_encode(array('status' => false, 'message' => 'Lỗi ! 1 Leader chỉ được gán tối đa 5 shop làm Sales , xin vui lòng kiểm tra lại.')));
				}

				if($data['from_date'])
				{
					$day_transfer = strtotime($data['from_date']);
					if($from_view < $day_transfer)
					{
						exit(json_encode(array('status' => false,'message' => 'Lỗi ! Ngày gán shop không được nhỏ hơn ngày Transfer , vui lòng kiểm tra lại.')));
					}
					else
					{
						$string = '('.$id_staff.','.implode(",$is_level,$from_view),($id_staff,$is_level", $id_store).','.$is_level.','.$from_view.')';
						$res = $manage_store_staff->saveStoreStaff($string);

						if($res)
						{
							echo json_encode(array('status' => true,'success' => 'Gán shop thành công.'));
						  	$logStaff = $manage_store_staff->logStaff($info);
						}	
						else
							echo json_encode(array('status' => false));
					}
				}
				else if(!$data['from_date'])
				{
					$data2 = $manage_store_staff->get_joined_at($id_staff);
					$joined_at = strtotime($data2['joined_at']);
					if($from_view < $joined_at)
					{
						exit(json_encode(array('status' => false,'message' => 'Lỗi ! Ngày vào làm không được nhỏ hơn ngày gán shop , vui lòng kiểm tra lại.')));
					}
					else
					{
						
						$string = '('.$id_staff.','.implode(",$is_level,$from_view),($id_staff,$is_level", $id_store).','.$is_level.','.$from_view.')';
					
						$res = $manage_store_staff->saveStoreStaff($string);
				
						if($res)
						{
							echo json_encode(array('status' => true,'success' => 'Gán shop thành công.'));
						  	$logStaff = $manage_store_staff->logStaff($info);
						}
						else
							echo json_encode(array('status' => false));
					}
				}
			}
				
			/*KIỂM TRA TẤT CẢ CÁC ĐIỀU KIỆN KHI GÁN SHOP CỦA LEADER*/

			if($type == 2)
			{
				$table = "store_leader_log";
				$check_store = $manage_store_staff->check_store($table,$id_staff,$list_store);
				$check_region_share = $manage_store_staff->check_region_share($id_staff);
				// echo "<pre>";print_r($data);die;
				// echo $data['from_date'];die;
				// echo "<pre>";print_r($check_region_share);die;
				
				
				$info = array(
								's_id'		=> Zend_Auth::getInstance()->getStorage()->read()->id,
								'type'		=> $type,
								'staff_id'	=> $id_staff,
								'store_id'	=> $id_store,
								'date'		=> $from.date(' H:i:s')
						);
				// echo "<pre>";print_r($info);die;
				if ($get_title['title'] != 190)
				{
					exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Nhân viên này không có quyền gán shop gián tiếp , xin vui lòng kiểm tra lại.')));
				}
				

				if(count($check_store) > 0)
				{
					exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Nhân viên này đã được gán vào shop này rồi nên không thể gán được nữa , xin vui lòng kiểm tra lại.')));
				}

				if(empty($check_region_share))
				{
					exit(json_encode(array('status' => false, 'message' => 'Lỗi ! Leader này chưa có region share nên chưa được gán shop , xin vui lòng kiểm tra lại.')));
				}

				if($data['from_date'])
				{
					$day_transfer = strtotime($data['from_date']);
					if($from_view < $day_transfer)
					{
						exit(json_encode(array('status' => false,'message' => 'Lỗi ! Ngày gán shop không được nhỏ hơn ngày Transfer , vui lòng kiểm tra lại.')));
					}
					else
					{
						$string = "($id_staff,".implode(",$from_view),($id_staff,", $id_store).",$from_view)";
						$res = $manage_store_staff->saveStoreLeader($string);



						if($res)
						{
							echo json_encode(array('status' => true,'success' => 'Gán shop thành công.'));
							$logLeader = $manage_store_staff->logLeader($info);
						}
						else
							echo json_encode(array('status' => false));
					}
				}
				else if(!$data['from_date'])
				{
					$data2 = $manage_store_staff->get_joined_at($id_staff);
					$joined_at = strtotime($data2['joined_at']);
					if($from_view < $joined_at)
					{
						exit(json_encode(array('status' => false,'message' => 'Lỗi ! Ngày vào làm không được nhỏ hơn ngày gán shop , vui lòng kiểm tra lại.')));
					}
					else
					{
						
						$string = "($id_staff,".implode(",$from_view),($id_staff,", $id_store).",$from_view)";
					
						$res = $manage_store_staff->saveStoreLeader($string);
						
						if($res)
						{
							echo json_encode(array('status' => true,'success' => 'Gán shop thành công.'));
							$logLeader = $manage_store_staff->logLeader($info);
						}
						else
							echo json_encode(array('status' => false));
					}
				}
			}	

		}
		
	}
}