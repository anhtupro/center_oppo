<?php
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$delete_cost_id = $this->getRequest()->getParam('delete_cost_id');
$company            = $this->getRequest()->getParam('company');
$channel            = $this->getRequest()->getParam('channel_id');
$category            = $this->getRequest()->getParam('category_id');
$type            = $this->getRequest()->getParam('type_id');
$date_ct            = $this->getRequest()->getParam('date_ct');
$number_ct            = $this->getRequest()->getParam('number_ct');
$content            = $this->getRequest()->getParam('content');
$number_temp            = $this->getRequest()->getParam('number_temp');
$number_real            = $this->getRequest()->getParam('number_real');
$date_tt            = $this->getRequest()->getParam('date_tt');
$number_tt            = $this->getRequest()->getParam('number_tt');
$ct_xh            = $this->getRequest()->getParam('ct_xh');
$from_date            = $this->getRequest()->getParam('from_date');
$to_date            = $this->getRequest()->getParam('to_date');
$export           = $this->getRequest()->getParam('export');

$flashMessenger       = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QCosting = new Application_Model_Costing();
$QCompanyCosting = new Application_Model_CompanyCosting();
$QChannelCosting = new Application_Model_ChannelCosting();
$QCategoryCosting = new Application_Model_CategoryCosting();
$QTypeApproveCosting = new Application_Model_TypeApproveCosting();
$QTypeApproveDetailsCosting = new Application_Model_TypeApproveDetailsCosting();
include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
require_once 'phpmail'.DIRECTORY_SEPARATOR.'class.phpmailer.php';

$limit = LIMITATION;
$total = 0;
$title = $userStorage->title;
$params = [
	'title'				=> $title,
	'sort'				=> $sort,
	'desc'				=> $desc,
	'company_id'		=> $company,
	'channel_id'		=> $channel,
	'category_id'		=> $category,
	'type_id'			=> $type,
	'date_ct'			=> $date_ct,
	'number_ct'			=> $number_ct,
	'content'			=> $content,
	'number_temp'		=> $number_temp,
	'number_real'		=> $number_real,
	'date_tt'			=> $date_tt,
	'number_tt'			=> $number_tt,
	'ct_xh'				=> $ct_xh,
	'staff_id'			=> $userStorage->id,
    'to_date'           => $to_date,
    'from_date'         => $from_date
];
//var_dump($params['ct_xh']); exit;
//var_dump($params); exit;
$resultList 	= $QCosting->fetchPagination($page, $limit, $total, $params);

$company 		= $QCompanyCosting->getCompany();
$channel 		= $QChannelCosting->getChannel();
$category 		= $QCategoryCosting->getCategory();
$typeapprove 	= $QTypeApproveCosting->getTypeApprove();


$data_type_details = $QTypeApproveDetailsCosting->getData();


$data_details = [];
foreach ($data_type_details as $key => $value) {
	$data_details[$value['type_approve_id']][] = $value;
}


if(!empty($export)){
		require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        
		$PHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
        $sheet->setCellValue('A1',  'CHI PHÍ SALE '.date('Y', strtotime(date('Y-m-d'))));
        $PHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $PHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setSize(20);
         $PHPExcel->getActiveSheet()->getStyle('A2:M2')->getFill()->applyFromArray(array(
		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		    'startcolor' => array(
		         'rgb' => '32CD32'
		    )));
        $style = array(
        'font'  => array(
            'bold'  => true
        ));
       
        $index = 2;
        $heads = array(
         	'STT',
            'KA',
            'HẠNG MỤC',
            'NỘI DUNG',
            'CP TẠM TÍNH',
            'CHI PHÍ TĂNG/GIẢM',
            'CHI PHÍ THỰC HIỆN (+VAT)',
            'CHI PHÍ THỰC HIỆN (-VAT)',
            'THANH TOÁN/ CẤN TRỪ/ XUẤT HÀNG (+VAT)',
            'CHƯA THANH TOÁN',
            'NGÀY THANH TOÁN',
            'NGÀY HỢP ĐỒNG',
            'SỐ HỢP ĐỒNG'
        );
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $PHPExcel->getActiveSheet()->getColumnDimension($alpha)->setAutoSize(true);
             $PHPExcel->getActiveSheet()->getStyle($alpha. $index)->applyFromArray($style);
            $alpha++;
        }
         $PHPExcel->getActiveSheet()->freezePaneByColumnAndRow(2, 4);

        //text-align center
      //   $PHPExcel->setActiveSheetIndex(0)->getStyle('A1:R10000')->getAlignment()->applyFromArray(
     	// 	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
   		 // );
      //   $PHPExcel->setActiveSheetIndex(0)->getStyle('A1:R10000')->getAlignment()->applyFromArray(
     	// 	array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
   		 // );
       
        //set màu cho 2 header
        // $PHPExcel->getActiveSheet()->getStyle('E1:K1')->getFill()->applyFromArray(array(
        // 'type' => PHPExcel_Style_Fill::FILL_SOLID,
        // 'startcolor' => array(
        //      'rgb' => '228B22'
        // )));
        // $PHPExcel->getActiveSheet()->getStyle('A2:K2')->getFill()->applyFromArray(array(
        // 'type' => PHPExcel_Style_Fill::FILL_SOLID,
        // 'startcolor' => array(
        //      'rgb' => '32CD32'
        // )));
        // $PHPExcel->getActiveSheet()->getStyle('L1:R1')->getFill()->applyFromArray(array(
        // 'type' => PHPExcel_Style_Fill::FILL_SOLID,
        // 'startcolor' => array(
        //      'rgb' => '00868B'
        // )));
        // $PHPExcel->getActiveSheet()->getStyle('L2:R2')->getFill()->applyFromArray(array(
        // 'type' => PHPExcel_Style_Fill::FILL_SOLID,
        // 'startcolor' => array(
        //      'rgb' => '00C5CD'
        // )));
        
        $data_export = $QCosting->exportDetailsCosting($params);
        $index = 4;
        $total_temp = 0;
        $total_real=0;
        $total_change = 0;
        $total_pay = 0;
        $total_not_pay = 0;
        foreach($data_export as $key=>$value){
         $alpha = 'A';
        // $sheet->getStyle('A1:B2')->getNumberFormat()->setFormatCode('#,##0.00');
         $sheet->getCell($alpha++ . $index)->setValueExplicit($key+1,PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['channel'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['category'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['content'],PHPExcel_Cell_DataType::TYPE_STRING);

         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cost_temp'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['number_change'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cost_real'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cost_real'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['total_pay'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cost_real'] - $value['total_pay'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['lastdate_pay'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['lastdate_ct'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['number_ct'],PHPExcel_Cell_DataType::TYPE_STRING);
       

        $sheet->getStyle('E3:E'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('F3:F'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('G3:G'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('H3:H'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('I3:I'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('J3:J'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
         $total_temp+=$value['cost_temp'];
         $total_real+=$value['cost_real'];
         $total_change+=$value['number_change'];
         $total_pay+=$value['total_pay'];
         $total_not_pay+=($value['cost_real'] - $value['total_pay']);
         $index++;
        } 
        
       //  $footer =array(
       // 	'TỔNG CỘNG',
       // 	'TỔNG TIỀN SAU ĐIỀU CHỈNH'
       // );
       //  $alpha='A';
       //  foreach ($footer as $key)
       //  {
       //      $sheet->setCellValue($alpha . $index, $key);
       //      $alpha='L';
       //  }
        $sheet->setCellValue('A3','TỔNG CỘNG :');
        //merge cell
       /* $PHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$index.':J'.$index.'');
        $PHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$index.':Q'.$index.'');*/
        //set màu cho dòng tổng tiền
        $PHPExcel->getActiveSheet()->getStyle('A3:M3')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => 'FFFF00'
        )));
        //   $PHPExcel->getActiveSheet()->getStyle('L'.$index.':R'.$index.'')->getFill()->applyFromArray(array(
        // 'type' => PHPExcel_Style_Fill::FILL_SOLID,
        // 'startcolor' => array(
        //      'rgb' => 'D1EEEE'
        // )));
        //set 2 giá trị tổng tiền cho cell
        $sheet->setCellValue('E3', $total_temp);
        $sheet->setCellValue('F3', $total_change);
        $sheet->setCellValue('G3', $total_real);
        $sheet->setCellValue('I3', $total_pay);
        $sheet->setCellValue('J3', $total_not_pay);
        $sheet->getStyle('A3:M3')->getNumberFormat()->setFormatCode('#,##0.00');
        //tạo all border
        $styleArray = array(
  		 'borders' => array(
    	 'allborders' => array(
     	 'style' => PHPExcel_Style_Border::BORDER_THIN
    	)));
        $PHPExcel->getActiveSheet()->getStyle('A1:M'.$index.'')->applyFromArray($styleArray);
        $filename = ' Details-Costing-Export (' . date('d-m-Y H:i:s'.')');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
}

if (!empty($delete_cost_id)) {
        $where_update = $QCosting->getAdapter()->quoteInto('id = ?', $delete_cost_id);
        $data_update = array(
            'del' => 1
        );
        $QCosting->update($data_update, $where_update);
    }

$this->view->company = $company;
$this->view->channel = $channel;
$this->view->category = $category;
$this->view->typeapprove = $typeapprove;

$this->view->data_details = $data_details;


$this->view->list= $resultList;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->url = HOST.'costing/list-costing'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit * ($page - 1);


if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             	= $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}
?>