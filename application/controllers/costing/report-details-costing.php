<?php
$from_date  			= $this->getRequest()->getParam('from_date',date('01/01/Y'));
$to_date  			= $this->getRequest()->getParam('to_date',date('d/m/Y'));
$QCosting 				= new Application_Model_Costing();
$QCategoryCosting 		= new Application_Model_CategoryCosting();
$QChannelCosting 		= new Application_Model_ChannelCosting();

$params = [
	'channel'	=> $channel
];
$list_date = array(
       
    );

	$to = date_create_from_format("d/m/Y", $to_date)->format("Y-m-d"); 
	$from = date_create_from_format("d/m/Y", $from_date)->format("Y-m-d");

	for( $i = intval(date('m', strtotime(date('Y-m-d'))))-intval(date('m', strtotime($from))); $i >= intval(date('m', strtotime(date('Y-m-d')))) - intval(date('m', strtotime($to))) ; $i--){
		$list_date[$i] = date('Y-m-d', strtotime(date('Y-m')." -".$i." month"));
	}
$params['from_date'] = $from;
$params['to_date'] = $to;
$channel 			= $QChannelCosting->getChannel();
$details_costing 	= $QCosting->getDetailsCosting($params);
$details = array();
    foreach ($details_costing as $key => $value) {
    	$details[$value['chanel_id']][$value['DYear']][$value['DMonth']] = $value;
    }

 // echo '<pre>';
 // print_r($details);
 // echo '</prev>';



 $date_chart = array(
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );

$details_chart = $QCosting->getDetailsCostingChart($params);
$chart = array();
    foreach ($details_chart as $key => $value) {
    	$chart[$value['chanel_id']][$value['DYear']][$value['DMonth']] = $value;
    }
 // echo '<pre>';
 // print_r($chart);
 // echo '</prev>';
$this->view->chart 			= $chart;

$this->view->params 		= $params;
$this->view->channel 		= $channel;

$this->view->list_date 		= $list_date;
$this->view->date_chart 	= $date_chart;
$this->view->details 		= $details;