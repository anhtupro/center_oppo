<?php
$channel  			= $this->getRequest()->getParam('channel');
$from_date  			= $this->getRequest()->getParam('from_date',date('01/01/Y'));
$to_date  			= $this->getRequest()->getParam('to_date',date('d/m/Y'));
$QCosting 			= new Application_Model_Costing();
$QCategoryCosting = new Application_Model_CategoryCosting();
$QChannelCosting = new Application_Model_ChannelCosting();

$params = [
	'channel'	=> $channel,
];
$list_date = array();

	$to = date_create_from_format("d/m/Y", $to_date)->format("Y-m-d"); 
	$from = date_create_from_format("d/m/Y", $from_date)->format("Y-m-d");

	for( $i = intval(date('m', strtotime(date('Y-m-d'))))-intval(date('m', strtotime($from))); $i >= intval(date('m', strtotime(date('Y-m-d')))) - intval(date('m', strtotime($to))) ; $i--){
		$list_date[$i] = date('Y-m-d', strtotime(date('Y-m')." -".$i." month"));
	}
	
$params['from_date'] = $from;
$params['to_date'] = $to;

$channel = $QChannelCosting->getChannel();
$total_costing = $QCosting->getTotalCosting($params);
$total = array();
    foreach ($total_costing as $key => $value) {
    	$total[$value['category_id']][$value['DYear']][$value['DMonth']] = $value;
    }
$category = $QCategoryCosting->getCategory();

$sell_out = $QCosting->getSellout($params);
$total_sell_out = array();
    foreach ($sell_out as $key => $value) {
    	$total_sell_out[$value['DYear']][$value['DMonth']] = $value;
    }

$sell_in = $QCosting->getSellin($params);
$total_sell_in = array();
    foreach ($sell_in as $key => $value) {
    	$total_sell_in[$value['DYear']][$value['DMonth']] = $value;
    }

$cost = $QCosting->getCost($params);    
$total_cost = array();
    foreach ($cost as $key => $value) {
    	$total_cost[$value['DYear']][$value['DMonth']] = $value;
    }

	// echo '<pre>';
	//  print_r($total_sell_in);
	//  echo '</prev>';
//var_dump(2486400000); exit;
$this->view->total_sell_out = $total_sell_out;
$this->view->total_sell_in = $total_sell_in;
$this->view->total_cost = $total_cost;
$this->view->params = $params;
$this->view->channel = $channel;
$this->view->list_date = $list_date;
$this->view->total = $total;
$this->view->category = $category;