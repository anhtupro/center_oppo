<?php
$id   					= $this->getRequest()->getParam('id');
$act  					= $this->getRequest()->getParam('act');
$submit   				= $this->getRequest()->getParam('submit');
$company_id  			= $this->getRequest()->getParam('company');
$channel_id  			= $this->getRequest()->getParam('channel');
$category_id  			= $this->getRequest()->getParam('category');
$type  					= $this->getRequest()->getParam('type');
$content  				= $this->getRequest()->getParam('content');
$cost_temp  			= $this->getRequest()->getParam('cost_temp');
$cost_real 				= $this->getRequest()->getParam('cost_real');
$number_change 			= $this->getRequest()->getParam('number_change');
$note  					= $this->getRequest()->getParam('note');
$act  					= $this->getRequest()->getParam('act');
$date_offer  			= $this->getRequest()->getParam('date_offer');

$QCompanyCosting 		= new Application_Model_CompanyCosting();
$QChannelCosting 		= new Application_Model_ChannelCosting();
$QCategoryCosting 		= new Application_Model_CategoryCosting();
$QTypeApproveCosting 	= new Application_Model_TypeApproveCosting();
$QCosting 				= new Application_Model_Costing();
$QFileCosting 			= new Application_Model_FileCosting();
$flashMessenger     	= $this->_helper->flashMessenger;

$company 				= $QCompanyCosting->getCompany();
$channel 				= $QChannelCosting->getChannel();
$category 				= $QCategoryCosting->getCategory();
$typeapprove 			= $QTypeApproveCosting->getTypeApprove();
//$date_offer = date_create_from_format("d/m/Y", $date_offer)->format("Y-m-d");
$updated_at = date('Y-m-d H:i:s');
$params 	= array(
	'id'	=> $id,
	'act' 	=> $act 
);

$data = $QCosting->getData($params);

$this->view->data 			= $data;
$this->view->company 		= $company;
$this->view->channel 		= $channel;
$this->view->category 		= $category;
$this->view->typeapprove 	= $typeapprove;
$this->view->params 		= $params;
//Nhập thực chi

if(!empty($submit) && !empty($act) && $act == 'thucchi'){
	$back_url = HOST.'costing/edit-costing?id='.$id.'&act='.$act;
	try{
		$number_change 	= str_replace(",","",$number_change);
		$number_change  = intval($number_change);
		$cost_real 		= str_replace(",","",$cost_real);
		$cost_real 		= intval($cost_real);

		$where = $QCosting->getAdapter()->quoteInto('id = ?', $id);
			$data = array(
				'cost_real'			=> $cost_real,
				'number_change'		=> $number_change,
				'updated_at'		=>$updated_at
			);
		$QCosting->update($data,$where);
		$flashMessenger->setNamespace('success')->addMessage('Cập nhật số thực chi thành công !');
		$this->redirect($back_url);
	}catch (exception $e) {
		$flashMessenger->setNamespace('error')->addMessage('Có lỗi: '.$e);
		$this->_redirect(HOST . 'costing/edit-costing?id='.$id);
	}
}
//Edit
if(!empty($submit) && empty($act))
{
	$back_url = HOST.'costing/edit-costing?id='.$id;
	try{
		 
		$cost_temp 		 = str_replace(",","",$cost_temp);
		$cost_temp 		 =intval($cost_temp);
		$where_update 	 = $QCosting->getAdapter()->quoteInto('id = ?', $id);
			$data_update = array(
				'company_id'	=> $company_id,
				'category_id'	=> $category_id,
				'chanel_id'		=> $channel_id,
				'cost_temp'		=> $cost_temp,
				'content'		=> $content,
				'note'			=> $note,
				'created_by'	=> $userStorage->id,
				'updated_at'	=> $updated_at,
				'status'		=> 1,
				'type_approve'	=> $type,
				'date_offer'	=> $date_offer,
				'del'			=> 0
			);
		$QCosting->update($data_update,$where_update);
			$total = count($_FILES['file']['name']);
			for($i = 0; $i < $total; $i++){
			 if($_FILES['file']['name'][$i] != ""){
				$where_del = $QFileCosting->getAdapter()->quoteInto('cost_id = ?', $id);
				$QFileCosting->delete($where_del);
				
			}
		}
			for($i=0; $i<$total; $i++){
			 if($_FILES['file']['name'][$i] != ""){
				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
								DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
								DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $id;

				if (!is_dir($uploaded_dir))
				@mkdir($uploaded_dir, 0777, true);
				$tmpFilePath 		= $_FILES['file']['tmp_name'][$i];
				if ($tmpFilePath  	!= ""){
					$old_name 		= $_FILES['file']['name'][$i];
					$tExplode 		= explode('.', $old_name);
					$extension  	= end($tExplode);
					$new_name 		= 'UPLOAD-COSTING-' . md5(uniqid('', true)) . '.' . $extension;
					$newFilePath 	= $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
					
						if(move_uploaded_file($tmpFilePath, $newFilePath)) {
							$url    = 'files' .
									DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
						}else{
							$url = NULL;
						}
				}else{
					$url = NULL;
				}

				$detail_file = [
					'cost_id' 	=> $id,
					'url'  		=> $url,
					'type'	    => 1,
					'name' 		=>$new_name,
				];
				$QFileCosting->insert($detail_file);
			}
		}
		$flashMessenger->setNamespace('success')->addMessage('Cập nhật chi phí thành công !');
		$this->redirect($back_url);
	}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Có lỗi: '.$e);
			$this->_redirect(HOST . 'costing/edit-costing?id='.$id);
	}
}

if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             	= $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

?>