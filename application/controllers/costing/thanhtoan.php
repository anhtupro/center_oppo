<?php
$id   				= $this->getRequest()->getParam('id');
$submit   				= $this->getRequest()->getParam('submit');

$date_chungtu = $this->getRequest()->getParam('date_chungtu');
$number_chungtu = $this->getRequest()->getParam('number_chungtu');
$date_pay = $this->getRequest()->getParam('date_thanhtoan');
$number_pay = $this->getRequest()->getParam('number_thanhtoan');
$cantru_xuathang = $this->getRequest()->getParam('cantru_xuathang');

$QPayCosting = new Application_Model_PayCosting();
$QCompanyCosting = new Application_Model_CompanyCosting();
$QChannelCosting = new Application_Model_ChannelCosting();
$QCategoryCosting = new Application_Model_CategoryCosting();
$QTypeApproveCosting = new Application_Model_TypeApproveCosting();
$QCosting = new Application_Model_Costing();
$QFileCosting = new Application_Model_FileCosting();
$flashMessenger     = $this->_helper->flashMessenger;
$userStorage                    = Zend_Auth::getInstance()->getStorage()->read();


$company = $QCompanyCosting->getCompany();
$channel = $QChannelCosting->getChannel();
$category = $QCategoryCosting->getCategory();
$typeapprove = $QTypeApproveCosting->getTypeApprove();

$created_at = date('Y-m-d H:i:s');
$params = array(
	'id'	=> $id,
	'act' => $act 
);

$data = $QCosting->getData($params);

$history = $QPayCosting->getHistory($params);

$this->view->history =$history;
$this->view->data = $data;
$this->view->company = $company;
$this->view->channel = $channel;
$this->view->category = $category;
$this->view->typeapprove = $typeapprove;
$this->view->params= $params;


//Thanh toán
if(!empty($submit)){
	$back_url = HOST.'costing/thanhtoan?id='.$id;
	try{
		$date_pay = date_create_from_format("d/m/Y", $date_pay)->format("Y-m-d");
		$date_chungtu = date_create_from_format("d/m/Y", $date_chungtu)->format("Y-m-d");
		$number_pay = str_replace(",","",$number_pay);
		$number_pay =intval($number_pay);
		$cantru_xuathang = str_replace(",","",$cantru_xuathang);
		$cantru_xuathang =intval($cantru_xuathang);
		
			$data=array(
				'cost_id'			=> $id,
				'date_pay'			=> $date_pay,
				'number_pay'		=> $number_pay,
				'cantru_xuathang'	=> $cantru_xuathang,
				'number_ct'			=> $number_chungtu,
				'date_ct'			=> $date_chungtu,
				'created_at'		=> $created_at,
				'created_by'		=> $userStorage->id
			);
		$QPayCosting->insert($data);


		$total 	= count($_FILES['file']['name']);
		//var_dump($_FILES['file']['name']); exit;
		for($i = 0; $i < $total; $i++){
			if($_FILES['file']['name'][$i] != ""){

				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
							DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
							DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $id;

			if (!is_dir($uploaded_dir))
			@mkdir($uploaded_dir, 0777, true);
			$tmpFilePath 		= $_FILES['file']['tmp_name'][$i];
			if ($tmpFilePath 	!= ""){
				$old_name 		= $_FILES['file']['name'][$i];
				$tExplode 		= explode('.', $old_name);
				$extension  	= end($tExplode);
				$new_name 		= 'UPLOAD-COSTING-PAY-' . md5(uniqid('', true)) . '.' . $extension;
				$newFilePath 	= $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
				
					if(move_uploaded_file($tmpFilePath, $newFilePath)) {
						$url    = 'files' .
								DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
					}else{
						$url 	= NULL;
					}
			}else{
				$url = NULL;
			}

			$detail_file = [
				'cost_id' 	=> $id,
				'url'  		=> $url,
				'type'	   	=> 2,
				'name'		=>$new_name,
			];

			$QFileCosting->insert($detail_file);
			}
			
		}

		$flashMessenger->setNamespace('success')->addMessage('Thanh toán thành công !');
		$this->redirect($back_url);
	}catch (exception $e) {
		$flashMessenger->setNamespace('error')->addMessage('Có lỗi: '.$e);
		$this->_redirect(HOST . 'costing/thanhtoan?id='.$id);
	}
}

if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             	= $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

?>