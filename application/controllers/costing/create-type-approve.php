<?php
$submit            = $this->getRequest()->getParam('submit');
$name_type            = $this->getRequest()->getParam('name_type');
$type_signature            = $this->getRequest()->getParam('type_signature');
$staff_id            = $this->getRequest()->getParam('staff_id');
$key_sign            = $this->getRequest()->getParam('key_sign');
$size_img            = $this->getRequest()->getParam('size_img');
$flashMessenger                 = $this->_helper->flashMessenger;
$QTypeApproveCosting = new Application_Model_TypeApproveCosting();
$QTypeApproveDetailsCosting = new Application_Model_TypeApproveDetailsCosting();
$QTypeSignatureCosting = new Application_Model_TypeSignatureCosting();
$QCosting = new Application_Model_Costing();

$db                             = Zend_Registry::get('db');
$db->beginTransaction();

$sign = $QTypeSignatureCosting->getData();
$staff = $QCosting->getStaff();

if(!empty($submit)){
	try{
		$data = array(
			'name'	=> $name_type,
			);
		$id = $QTypeApproveCosting->insert($data);
		$total = count($_FILES['img']['name']); 	
				for($i = 0; $i < $total; $i++){
					if($_FILES['img']['name'][$i]!="")
					{
						$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
					DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'img';
					if (!is_dir($uploaded_dir))
					@mkdir($uploaded_dir, 0777, true);
					$tmpFilePath 	 = $_FILES['img']['tmp_name'][$i];

					if ($tmpFilePath != ""){
						$old_name 	 = $_FILES['img']['name'][$i];
						$tExplode 	 = explode('.', $old_name);
						$extension   = end($tExplode);
						$new_name 	 = $old_name;
						$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
							if(move_uploaded_file($tmpFilePath, $newFilePath)) {
								$url = 'img' .
										DIRECTORY_SEPARATOR .$new_name;
							}else{
								$url = NULL;
							}
					}else{
						$url = NULL;
					}
						$url_img[] = $url;
					}
				}
				foreach ($type_signature as $key => $value) {
					$details = [
						'type_approve_id' 	=> $id,
						'staff_id'			=> $staff_id[$key],
						'type'				=> $value,
						'key_signature'		=> $key_sign[$key],
						'swh_signature'		=> $size_img[$key],
						'img_signature'		=> $url_img[$key]
					];
					$QTypeApproveDetailsCosting->insert($details);
				}
		

		$db->commit();
		$flashMessenger->setNamespace('success')->addMessage('Tạo mới thành công!');
		$this->redirect(HOST.'costing/list-type');
	}catch (Exception $e)
	{
		$db->rollBack();
	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi!! ");
    	$this->redirect(HOST.'costing/create-type-approve?id='.$id);
	}
	


}

$this->view->staff = $staff;
$this->view->sign = $sign;

if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;
	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}