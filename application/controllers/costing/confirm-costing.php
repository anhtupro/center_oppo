<?php
$id   						= $this->getRequest()->getParam('id');
$submit   					= $this->getRequest()->getParam('submit');
$type_id  					= $this->getRequest()->getParam('type_id');
$company_id  				= $this->getRequest()->getParam('company');
$channel_id  				= $this->getRequest()->getParam('channel');
$category_id  				= $this->getRequest()->getParam('category');
$type  						= $this->getRequest()->getParam('type');
$content  					= $this->getRequest()->getParam('content');
$cost_temp 	 				= $this->getRequest()->getParam('cost_temp');
$note  						= $this->getRequest()->getParam('note');

$QFileCosting 				= new Application_Model_FileCosting();
$QCompanyCosting 			= new Application_Model_CompanyCosting();
$QChannelCosting 			= new Application_Model_ChannelCosting();
$QCategoryCosting 			= new Application_Model_CategoryCosting();
$QTypeApproveCosting 		= new Application_Model_TypeApproveCosting();
$QCosting 					= new Application_Model_Costing();
$QTypeApproveDetailsCosting = new Application_Model_TypeApproveDetailsCosting();
$QApproveCosting 			= new Application_Model_ApproveCosting();
$flashMessenger     		= $this->_helper->flashMessenger;
$userStorage                = Zend_Auth::getInstance()->getStorage()->read();

$company 			= $QCompanyCosting->getCompany();
$channel 			= $QChannelCosting->getChannel();
$category 			= $QCategoryCosting->getCategory();
$typeapprove 		= $QTypeApproveCosting->getTypeApprove();

$params = array(
	'id'			=> $id,
	'staff_id'		=> $userStorage->id
);

$data 				= $QCosting->getData($params);
$file 				= $QFileCosting->getFile($params);

$this->view->file 			= $file;
$this->view->data 			= $data;
$this->view->company 		= $company;
$this->view->channel 		= $channel;
$this->view->category 		= $category;
$this->view->typeapprove 	= $typeapprove;
$this->view->params 		= $params;


if(!empty($submit))
{
	$cost_temp 	= str_replace(",","",$cost_temp);
	$cost_temp 	= intval($cost_temp);
	$created_at = date('Y-m-d H:i:s');
	$back_url 	= HOST.'costing/confirm-costing?id='.$id;

	try{
		$where 		= array();
		$where[] 	= $QTypeApproveDetailsCosting->getAdapter()->quoteInto('type_approve_id = ?', $type_id);
		$where[] 	= $QTypeApproveDetailsCosting->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
		$data 		= $QTypeApproveDetailsCosting->fetchAll($where);

		foreach ($data as $key => $value) {
			
			$details = [
				'cost_id'					=> $id,
				'type_id'					=>$value['type'],
				'type_approve_details_id'	=> $value['id'],
				'created_at'				=> $created_at,
				'status'					=> 1
			];
			$QApproveCosting->insert($details);
		}

		if(!empty($file[0]['file_name']))
		{
			////INSERT CHỮ KÝ
			require_once 'phpword/vendor/autoload.php';
			//$template lấy file biểu mẫu có sẵn
		    $template = 'files'.DIRECTORY_SEPARATOR.'costing'.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$file[0]['file_name'];
		    move_uploaded_file($template,$template);
                    chmod($template, 0777);
                    
                    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template);
		    //Tìm từ khóa ${???} trong file word để thay bằng hình ảnh hoặc text
		    if($userStorage->id == 765){
		    	 $templateProcessor->setImg('mrdongimg',array('src' => 'img/approved.jpg','swh'=>'100'));
		    	 $templateProcessor->setValue('mrdong','Jiang Guo Dong');
		    }
		   
		    if($userStorage->id == 103)
		    {
		    	$templateProcessor->setImg('mrkhaimg',array('src' => 'img/approved.jpg','swh'=>'100'));
		    	$templateProcessor->setValue('mrkha','Đỗ Quang Kha');
		    }
		    
		    $templateProcessor->saveAs('files'.DIRECTORY_SEPARATOR.'costing'.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$file[0]['file_name']);

                }

		$flashMessenger->setNamespace('success')->addMessage('Xác nhận chi phí thành công !');
		$this->redirect($back_url);
	}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Có lỗi: '.$e);
			$this->_redirect(HOST . 'costing/confirm-costing?id='.$id);
	}
}

if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             	= $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

?>