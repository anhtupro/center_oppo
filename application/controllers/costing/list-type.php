<?php
$delete_type_id = $this->getRequest()->getParam('delete_type_id');
$flashMessenger       = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QTypeApproveCosting = new Application_Model_TypeApproveCosting();
$QTypeApproveDetailsCosting = new Application_Model_TypeApproveDetailsCosting();

$title = $userStorage->title;
$params = [
	'title'	=> $title
];
$data_list = $QTypeApproveCosting->getDataList();

if (!empty($delete_type_id)) {
        
        $where_delete = $QTypeApproveCosting->getAdapter()->quoteInto('id = ?', $delete_type_id);
        $QTypeApproveCosting->delete($where_delete);
        $where_del = $QTypeApproveDetailsCosting->getAdapter()->quoteInto('type_approve_id = ?', $delete_type_id);
        $QTypeApproveDetailsCosting->delete($where_del);
    exit;
 }

$this->view->list = $data_list;
$this->view->params = $params;

if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;
	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}