<?php
$QCompanyCosting = new Application_Model_CompanyCosting();
$QChannelCosting = new Application_Model_ChannelCosting();
$QCategoryCosting = new Application_Model_CategoryCosting();
$QTypeApproveCosting = new Application_Model_TypeApproveCosting();
$flashMessenger     = $this->_helper->flashMessenger;

$company = $QCompanyCosting->getCompany();
$channel = $QChannelCosting->getChannel();
$category = $QCategoryCosting->getCategory();
$typeapprove = $QTypeApproveCosting->getTypeApprove();

$this->view->company = $company;
$this->view->channel = $channel;
$this->view->category = $category;
$this->view->typeapprove = $typeapprove;


if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             	= $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}
?>