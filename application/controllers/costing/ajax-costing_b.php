<?php

$cost_id = $this->getRequest()->getParam('cost_id');
$approved = $this->getRequest()->getParam('approved');
$type_id = $this->getRequest()->getParam('type_id');

$QCosting = new Application_Model_Costing();
$QFileCosting = new Application_Model_FileCosting();
$QTypeApproveDetailsCosting = new Application_Model_TypeApproveDetailsCosting();
$QApproveCosting = new Application_Model_ApproveCosting();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$params = array(
    'id' => $cost_id,
    'staff_id' => $userStorage->id
);



$result = 1;

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {

    $file = $QFileCosting->getFile($params);
    $check_approved = $QCosting->getData($params);
    $created_at = date('Y-m-d H:i:s');
    if (!empty($approved) && $approved == 1) {
        $where = array();
        $where[] = $QTypeApproveDetailsCosting->getAdapter()->quoteInto('type_approve_id = ?', $type_id);
        $where[] = $QTypeApproveDetailsCosting->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
        $data = $QTypeApproveDetailsCosting->fetchAll($where);

        foreach ($data as $key => $value) {

            $details = [
                'cost_id' => $cost_id,
                'type_id' => $value['type'],
                'type_approve_details_id' => $value['id'],
                'created_at' => $created_at,
                'status' => 1
            ];
            $QApproveCosting->insert($details);
        }
        
        $approve_costing = $QApproveCosting->getApprove($cost_id);
        
        if (!empty($file[0]['file_name'])) {
            ////INSERT CHỮ KÝ
            require_once 'phpword/vendor/autoload.php';
            //$template lấy file biểu mẫu có sẵn
            $template = 'files' . DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $cost_id . DIRECTORY_SEPARATOR . $file[0]['file_name'];
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template);
            
            
            foreach($approve_costing as $k=>$v){
                $templateProcessor->setImg($v['key_signature'], array('src' => $v['img_signature'], 'swh' => $v['swh_signature']));
            }
            
            $templateProcessor->saveAs('files' . DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $cost_id . DIRECTORY_SEPARATOR . 'temp_'.$file[0]['file_name']);
            
            $file_url = 'files' . DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $cost_id . DIRECTORY_SEPARATOR . 'temp_'.$file[0]['file_name'];
            
            $QFileCosting->update(['url_signature' => $file_url], $QFileCosting->getAdapter()->quoteInto('cost_id = ?', $cost_id));
            
            /*
            //Tìm từ khóa ${???} trong file word để thay bằng hình ảnh hoặc text
            if ($userStorage->id == 765) {
                $templateProcessor->setImg('mrdongimg', array('src' => 'img/dong_signature.png', 'swh' => '100'));
                $templateProcessor->setValue('mrdong', 'Jiang Guo Dong');
            }

            if ($userStorage->id == 103) {
                $templateProcessor->setImg('mrkhaimg', array('src' => 'img/kha_signature.png', 'swh' => '100'));
                $templateProcessor->setValue('mrkha', 'Đỗ Quang Kha');
            }

            if ($userStorage->id == 15) {
                $templateProcessor->setImg('mrskimimg', array('src' => 'img/kim_signature.png', 'swh' => '100'));
                $templateProcessor->setValue('mrskim', 'Trần Ngọc Kim');
            }
            //Check loại chị Kim, anh Kha
            if ($type_id == 4) {
                if (count($check_approved[0]['staff_waiting']) == count($check_approved[0]['staff_approved'])) {
                    if ($check_approved[0]['company_id'] == 1) {
                        $templateProcessor->setImg('dongdau', array('src' => 'img/con_dau_cty.jpg', 'swh' => '100'));
                    }
                    if ($check_approved[0]['company_id'] == 2) {
                        $templateProcessor->setImg('dongdau', array('src' => 'img/con_dau_van.jpg', 'swh' => '100'));
                    }
                }
            }
            $templateProcessor->saveAs('files' . DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $cost_id . DIRECTORY_SEPARATOR . $file[0]['file_name']);
            
            */
        }
    }
    if (!empty($cost_id) && empty($approved)) {
        $where_update = $QCosting->getAdapter()->quoteInto('id = ?', $cost_id);
        $data_update = array(
            'del' => 1
        );
        $QCosting->update($data_update, $where_update);
    }


    $db->commit();

    echo $result;
    exit;
} catch (Exception $e) {
    $db->rollback();
    $result = $e->getMessage();

    echo $result;
    exit;
}

