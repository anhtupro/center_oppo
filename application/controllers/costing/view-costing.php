<?php

$id   					= $this->getRequest()->getParam('id');


$QCompanyCosting 		= new Application_Model_CompanyCosting();
$QChannelCosting 		= new Application_Model_ChannelCosting();
$QCategoryCosting 		= new Application_Model_CategoryCosting();
$QTypeApproveCosting 	= new Application_Model_TypeApproveCosting();
$QCosting 				= new Application_Model_Costing();
$QFileCosting 			= new Application_Model_FileCosting();
$flashMessenger     	= $this->_helper->flashMessenger;

$company 				= $QCompanyCosting->getCompany();
$channel 				= $QChannelCosting->getChannel();
$category 				= $QCategoryCosting->getCategory();
$typeapprove 			= $QTypeApproveCosting->getTypeApprove();

$params 	= array(
	'id'	=> $id
);

$data = $QCosting->getData($params);
$file = $QFileCosting->getFile($params);

$this->view->data 			= $data;
$this->view->company 		= $company;
$this->view->channel 		= $channel;
$this->view->category 		= $category;
$this->view->typeapprove 	= $typeapprove;
$this->view->params 		= $params;
$this->view->file 			= $file;


if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             	= $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

?>