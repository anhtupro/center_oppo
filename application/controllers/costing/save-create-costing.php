<?php
$company  			= $this->getRequest()->getParam('company');
$channel  			= $this->getRequest()->getParam('channel');
$category  			= $this->getRequest()->getParam('category');
$type  				= $this->getRequest()->getParam('type');
$content  			= $this->getRequest()->getParam('content');
$cost_temp  		= $this->getRequest()->getParam('cost_temp');
$note  				= $this->getRequest()->getParam('note');

$QCosting 			= new Application_Model_Costing();
$QFileCosting 		= new Application_Model_FileCosting();
$userStorage    	= Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger     = $this->_helper->flashMessenger;

$cost_temp 			= str_replace(",","",$cost_temp);
$cost_temp 			= intval($cost_temp);
$created_at 		= date('Y-m-d H:i:s');

try{
	$data = array(
		'company_id'	=> $company,
		'category_id'	=> $category,
		'chanel_id'		=> $channel,
		'cost_temp'		=> $cost_temp,
		'content'		=> $content,
		'note'			=> $note,
		'created_by'	=> $userStorage->id,
		'created_at'	=> $created_at,
		'status'		=> 1,
		'type_approve'	=> $type,
		'del'			=> 0
		);
		$id 	= $QCosting->insert($data);

		$total 	= count($_FILES['file']['name']);

		for($i = 0; $i < $total; $i++){
			$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
							DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
							DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $id;

			if (!is_dir($uploaded_dir))
			@mkdir($uploaded_dir, 0777, true);
			$tmpFilePath 		= $_FILES['file']['tmp_name'][$i];
			if ($tmpFilePath 	!= ""){
				$old_name 		= $_FILES['file']['name'][$i];
				$tExplode 		= explode('.', $old_name);
				$extension  	= end($tExplode);
				$new_name 		= 'UPLOAD-COSTING-' . md5(uniqid('', true)) . '.' . $extension;
				$newFilePath 	= $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
				
					if(move_uploaded_file($tmpFilePath, $newFilePath)) {
						$url    = 'files' .
								DIRECTORY_SEPARATOR . 'costing' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
					}else{
						$url 	= NULL;
					}
			}else{
				$url = NULL;
			}
                        
                        chmod($newFilePath, 0777);

			$detail_file = [
				'cost_id' 	=> $id,
				'url'  		=> $url,
				'type'	   	=> 1,
				'name'		=>$new_name,
			];

			$QFileCosting->insert($detail_file);
		}

		$flashMessenger->setNamespace('success')->addMessage('Đề xuất chi phí thành công !');
		$this->redirect(HOST.'costing/list-costing');

}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Có lỗi: '.$e);
			$this->_redirect(HOST . 'costing/create-costing');
}

if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             	= $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}
?>