<?php
class DevicesController extends My_Controller_Action{
    public function init(){
        
    }
    public function showAllAction(){
        require_once 'devices' . DIRECTORY_SEPARATOR .'show-all.php';
    }
    public function createDevicesAction(){
        require_once 'devices' . DIRECTORY_SEPARATOR .'create-devices.php';
    }
    public function saveDevicesAction(){
        require_once 'devices' .DIRECTORY_SEPARATOR . 'save-devices.php';
    }
    public function editDevicesAction(){
        require_once 'devices' . DIRECTORY_SEPARATOR .'edit-devices.php';
    }
}
