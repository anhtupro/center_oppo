<?php
class SalaryController extends My_Controller_Action
{
    
    public function indexAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'index.php';
    }
    
    public function importKpiFullAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'import-kpi-full.php';
    }
    
    public function importCostAreaKpiSaveAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'import-cost-area-kpi-save.php';
    }
    
    public function importSalaryFullAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'import-salary-full.php';
    }
    
    public function importCostAreaSalarySaveAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'import-cost-area-salary-save.php';
    }
    
    
    public function importSalaryAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'import-salary.php';
    }
    
    public function importSalarySaveAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'import-salary-save.php';
    }
    public function reportAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'report.php';
    }
     public function historyAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'history.php';
    }
     public function reportBiAction()
    {
        require_once 'salary'.DIRECTORY_SEPARATOR.'reportbi.php';
    }
    
    
    /*
    public function importSalaryAction()
    {
        $from_date = $this->getRequest()->getParam('from_date');
        $type = $this->getRequest()->getParam('type');
        $import = $this->getRequest()->getParam('import');

            if(!empty($import))
            {
                $upload = new Zend_File_Transfer();
                $uniqid = uniqid('', true);
                $userStorage = Zend_Auth::getInstance()->getStorage()->read();

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                    . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'
                    . DIRECTORY_SEPARATOR . 'timing'
                    . DIRECTORY_SEPARATOR . $userStorage->id
                    . DIRECTORY_SEPARATOR . $uniqid;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                    $upload->setDestination($uploaded_dir);

                    $upload->setValidators(array(
                        'Size'  => array('min' => 50, 'max' => 25000000),
                        'Count' => array('min' => 1, 'max' => 1),
                        'Extension' => array('xlsx'),
                    ));

                    $errors = $upload->getErrors();
                    $sError = null;

                    if ($errors and isset($errors[0])) {
                        switch ($errors[0]) {
                            case 'fileUploadErrorIniSize':
                                $sError = 'File size is too large';
                                break;
                            case 'fileMimeTypeFalse':
                                $sError = 'The file you selected weren\'t the type we were expecting';
                                break;
                            case 'fileExtensionFalse':
                                $sError = 'Please choose a PO file in XLSX format';
                                break;
                            case 'fileCountTooFew':
                                $sError = 'Please choose a PO file in XLSX format';
                                break;
                            case 'fileUploadErrorNoFile':
                                $sError = 'Please choose a PO file in XLSX format';
                                break;
                            case 'fileSizeTooBig':
                                $sError = 'File size is too big';
                                break;
                        }
                        throw new Exception($sError);
                    }

                    $upload->receive();

                    $path_info = pathinfo($upload->getFileName());
                    $filename = $path_info['filename'];
                    $extension = $path_info['extension'];

                    $old_name = $filename . '.'.$extension;
                    $new_name = 'UPLOAD-'.md5($filename . uniqid('', true)) . '.'.$extension;


                if (is_file($uploaded_dir . DIRECTORY_SEPARATOR . $old_name)){
                    rename($uploaded_dir . DIRECTORY_SEPARATOR . $old_name, $uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
                } else {
                    $new_name = $old_name;
                }
                
                $path = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name; 

                if(!empty($path))
                {
                    $header = array(
                        1 => 'area',
                        2 => 'province_id',
                        3 => 'total_salary',
                        4 => 'probation_salary',
                        5 => 'base_salary',
                        6 => 'food_salary',
                        7 => 'fuel_salary',
                        8 => 'phone_salary',
                        9 => 'kpi',
                        10 => 'probation_kpi',
                        11 => 'work_cost',
                    );

                    $readExcel = new My_ReadExcel($path);
                    $data = $readExcel->ConvertToArray($header);

                    $QArea = new Application_Model_Area();
                    $area_flip = array_flip($QArea->get_cache());

                    $QR = new Application_Model_RegionalMarket();
                    $rg_flip = array_flip($QR->get_cache());

                    foreach ($data as $key => $value) 
                    {
                        $data[$key]['area'] = $area_flip[$data[$key]['area']];
                        $data[$key]['province_id'] = $rg_flip[$data[$key]['province_id']];
                    }

                    $array_values = array();

                    foreach($data as $key => $val)
                    {
                        $value = array();
                        if(!empty($val['area']))
                        {
                            $value[] = $val['area'];
                            $value[] = $val['province_id'];
                            $value[] = $val['total_salary'];
                            $value[] = $val['probation_salary'];
                            $value[] = $val['base_salary'];
                            $value[] = empty($val['food_salary'])?0:$val['food_salary'];
                            $value[] = empty($val['food_salary'])?0:$val['food_salary'];
                            $value[] = empty($val['phone_salary'])?0:$val['phone_salary'];
                            $value[] = "'" . $val['kpi'] . "'";
                            $value[] = "'" . $val['probation_kpi'] . "'";
                            $value[] = empty($val['work_cost'])?0:$val['work_cost'];;

                            $array_values[] = "(" . implode("," , $value) . ")";
                        }
                    }
                    
                    $sql = "";

                    foreach ($array_values as $key_values => $value_values)
                    {
                        $sql .= $value_values;
                        if(count($array_values) != $key_values+1)
                        {
                            $sql .= ",";
                        }
                    }

                    $QSalaryLog = new Application_Model_SalaryLog();

                    $from_date = DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d');
                    $to_date = date('Y-m-d', strtotime('-1 day', strtotime($from_date)));

                    $QSalaryLog->importSalary($sql, $from_date, $to_date, $type, $userStorage->id);

                }
            }
        }*/
        
        
        
}