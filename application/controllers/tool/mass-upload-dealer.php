<?php

$submit   = $this->getRequest()->getParam('submit');
$QDealerLoyalty = new Application_Model_DealerLoyalty();
$flashMessenger       = $this->_helper->flashMessenger;

	$db   = Zend_Registry::get('db');
	$db->beginTransaction();
if(!empty($submit)){
			
    if($_FILES['file']['name']!=NULL){

    	try{

			$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
							DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'mass-upload-dealer';

			if (!is_dir($uploaded_dir))
			@mkdir($uploaded_dir, 0777, true);
			$tmpFilePath = $_FILES['file']['tmp_name'];

			if ($tmpFilePath != ""){
				$old_name 	= $_FILES['file']['name'];
				$tExplode 	= explode('.', $old_name);
				$extension  = end($tExplode);
				$new_name = 'FILE-' . md5(uniqid('', true)) .'.' . $extension;
				$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
				if(move_uploaded_file($tmpFilePath, $newFilePath)) {

					$url= 'mass-upload'.DIRECTORY_SEPARATOR .$new_name;
				}else{
					$url = NULL;
				}
			}else{
				$url = NULL;
			}
		//đọc file excel
		  require_once 'PHPExcel.php';
            $cacheMethod   = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
            $cacheSettings = array( 'memoryCacheSize' => '32MB');
            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

            switch ($extension) {
                case 'xls':
                    $objReader = PHPExcel_IOFactory::createReader('Excel5');
                    break;
                case 'xlsx':
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    break;
                default:
                    throw new Exception("Invalid file extension");
                    break;
            }

            $objReader->setReadDataOnly(true);


            $objPHPExcel  = $objReader->load($uploaded_dir . DIRECTORY_SEPARATOR . $new_name);

            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

            $highestRow         = $objWorksheet->getHighestRow();
            $highestColumn      = $objWorksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
			
		$imei =array();
		$created_at=date('Y-m-d H:i:s');
        for ($i=2; $i <= $highestRow ; $i++) { 
            if(!empty(trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue()))){

                $from = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCellByColumnAndRow(2, $i)->getValue()));
                $to = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCellByColumnAndRow(3, $i)->getValue()));
               
                $imei = [
					'dealer_id' => trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue()),
                    'loyalty_plan_id' => trim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue()),
                    'from_date' => $from,
					'to_date' => $to
                ];
                $QDealerLoyalty->insert($imei);

            }
        	
           
        }
		
		 unlink($url);
		 $db->commit();
		
		$flashMessenger->setNamespace('success')->addMessage('Upload dữ liệu thành công!');
		$this->redirect(HOST.'tool/mass-upload-dealer');
	 }catch (Exception $e)
		{
			$db->rollBack();
		    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
	    	$this->redirect(HOST.'tool/mass-upload-dealer');
		}
   }
}
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages          = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages = $messages;
}