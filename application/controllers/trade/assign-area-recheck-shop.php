<?php
$roundId = $this->getRequest()->getParam('round_id');

$QAssignAreaRecheckShop = new Application_Model_AssignAreaRecheckShop();
$QArea = new Application_Model_Area();

$listStaff = $QAssignAreaRecheckShop->getAll($roundId);
$listArea = $QArea->getAll();

$this->view->listStaff = json_encode($listStaff);
$this->view->roundId = $roundId;
$this->view->listArea = $listArea;
