<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$area_id = $this->getRequest()->getParam('area_id');
$stage_id = $this->getRequest()->getParam('stage_id');

$params = [
    'stage_id' => $stage_id,
    'area_id' => $area_id
];

$QCheckPosmStage = new Application_Model_CheckPosmStage();

// theo số lượng hạng mục
// theo khu vực
$statistic_store_area = $QCheckPosmStage->getAllStoreSale($params);
$statistic_store_not_finish_area = $QCheckPosmStage->getNotFinishStoreSale($params);

foreach ($statistic_store_area as $value) {
    $count_store_finish = $value['count_store'] - $statistic_store_not_finish_area [$value['sale_id']] ['count_store'];
    $count_all_store = $value['count_store'];

    $percent_finish_area [$value['sale_id']] ['sale_name'] = $value['sale_name'];
    $percent_finish_area [$value['sale_id']] ['count_store_all'] = $count_all_store;
    $percent_finish_area [$value['sale_id']] ['count_store_finish'] = $count_store_finish;
    $percent_finish_area [$value['sale_id']] ['percent_finish'] = round($count_store_finish * 100 / $count_all_store);
}


echo json_encode([
    'status' => 0,
    'list_sale' => $percent_finish_area,
]);

