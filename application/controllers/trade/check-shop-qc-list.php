<?php
$QCheckShopQcResult = new Application_Model_CheckShopQcResult();
$QQuarter = new Application_Model_Quarter();
$QArea = new Application_Model_Area();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QRegionalMarket = new Application_Model_RegionalMarket();

$current_quarter = $QQuarter->getQuarter(date('m'));

$quarter = intval($this->getRequest()->getParam('quarter', $current_quarter));
$year = intval($this->getRequest()->getParam('year', date('Y')));
$store_id = intval($this->getRequest()->getParam('store_id'));
$store_name = My_Util::escape_string(TRIM($this->getRequest()->getParam('store_name')));
$area_id = intval($this->getRequest()->getParam('area_id'));
$district = intval($this->getRequest()->getParam('district'));
$regional_market = intval($this->getRequest()->getParam('regional_market'));
$big_area_id = intval($this->getRequest()->getParam('big_area_id'));
$eligigle_to_review = intval($this->getRequest()->getParam('eligigle_to_review'));
$status_review = intval($this->getRequest()->getParam('status_review'));
$export = $this->getRequest()->getParam('export');
$rule_sellout_dealer = 75;
$page = $this->getRequest()->getParam('page', 1);
$limit = 20;

$time_quarter = $QQuarter->getTimeFromTo($quarter, $year);

$params = [
    'list_area' =>  $this->storage['area_id'],
    'area_list' =>  $this->storage['area_id'],
    'quarter' => $quarter,
    'year' => $year,
    'store_id' => $store_id,
    'store_name' => $store_name,
    'area_id' => $area_id,
    'district' => $district,
    'regional_market' => $regional_market,
    'staff_id' => $staff_id,
    'big_area_id' => $big_area_id,
    'from_date' => $time_quarter['from_date'],
    'to_date' => $time_quarter['to_date'],
    'from_date_time' => $time_quarter['from_date_time'],
    'to_date_time' => $time_quarter['to_date_time'],
    'eligigle_to_review' => $eligigle_to_review,
    'rule_sellout_dealer' => $rule_sellout_dealer,
    'status_review' => $status_review
];
if ($export) {
    $QCheckShopQcResult->export($params);
}

$list = $QCheckShopQcResult->fetchPagination($page, $limit,$total, $params);

$list_big_area = $QArea->getBigArea();
$list_area = $QArea->getListArea($params);
$list_sale  = $QAppCheckShop->getSaleArea();
$list_channel = $QAppCheckShop->getChannel();


// search

if ($area_id) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regional_market) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$this->view->list = $list;
$this->view->list_big_area = $list_big_area;
$this->view->list_area = $list_area;
$this->view->list_sale = $list_sale;
$this->view->list_channel = $list_channel;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/check-shop-qc-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);