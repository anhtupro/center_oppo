<?php

$QAppAir = new Application_Model_AppAir();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QStaff = new Application_Model_Staff();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QAppStatus = new Application_Model_AppStatus();

$flashMessenger       = $this->_helper->flashMessenger;
/*PHÂN QUYỀN*/
$title 		 = $userStorage->title;
$id_user= $userStorage->id;
$id   = $this->getRequest()->getParam('id');

 $params = array(
            'id'    =>$id,
            'title' => $title,
            'id_user'=>$id_user,
            'area_id'   => $this->storage['area_id']
            );
 
 $page           = $this->getRequest()->getParam('page', 1);
 $limit          = 5;
 $total          = 0;
$rows = $QAppAir->fetchPagination($page, $limit, $total, $params);

$this->view->result   = $rows;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->params     = $params;
$this->view->url = HOST.'trade/air'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit*($page-1); 
	// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
