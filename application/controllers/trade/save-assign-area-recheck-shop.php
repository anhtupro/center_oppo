<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$staffId = $this->getRequest()->getParam('staff_id');
$roundId = $this->getRequest()->getParam('round_id');
$listAreaId = $this->getRequest()->getParam('list_area_id');

$QAssignAreaRecheckShop = new Application_Model_AssignAreaRecheckShop();

$where = [
    'staff_id = ?' => $staffId,
    'recheck_shop_round_id = ?' => $roundId
];


if ($listAreaId !== null) {
    $QAssignAreaRecheckShop->delete($where);


    foreach ($listAreaId as $areaId) {
        if ($areaId) {
            $QAssignAreaRecheckShop->insert([
                'staff_id' => $staffId,
                'area_id' => $areaId,
                'recheck_shop_round_id' => $roundId
            ]);
        }
    }
}


// list staff and assgin area
$listStaff = $QAssignAreaRecheckShop->getAll($roundId);

echo json_encode([
   'status' => 0,
   'listStaff' =>  $listStaff
]);






