<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$storeId = $this->getRequest()->getParam('store_id');

$QCheckShopEntire = new Application_Model_CheckShopEntire();

$db = Zend_Registry::get("db");
$select = $db->select()
    ->from(['r' => DATABASE_TRADE.'.check_shop_entire'], [
        'r.id',
        'created_at' => "DATE_FORMAT(r.created_at, '%d/%m/%Y %H:%i')",
        'r.error_image',
        'created_by' => "CONCAT(st.firstname, ' ', st.lastname)"
    ])
    ->joinLeft(['st' => 'staff'], 'r.created_by = st.id', [])
    ->where('r.store_id = ?', $storeId)
    ->order('r.created_at DESC')
    ->limit(20);

$listHistory = $db->fetchAll($select);


echo json_encode([
    'status' => 0,
    'listHistory' => $listHistory
]);
