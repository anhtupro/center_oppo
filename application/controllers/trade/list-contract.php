<?php 
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);

//SEARCH
$name_search            = $this->getRequest()->getParam('name_search');
$from_date            	= $this->getRequest()->getParam('from_date');
$to_date            	= $this->getRequest()->getParam('to_date');

$QAppContract		= new Application_Model_AppContract();

$userStorage 		= Zend_Auth::getInstance()->getStorage()->read();

$flashMessenger       = $this->_helper->flashMessenger;
$limit = LIMITATION;
$total = 0;

$params = [
	'name_search' 		=> $name_search,
	'from_date'			=> $from_date,
	'to_date'			=> $to_date
];

$params['sort'] = $sort;
$params['desc'] = $desc;

if( in_array($this->storage['title'], [SALES_TITLE]) ){
    $params['staff_id'] = $this->storage['staff_id'];
}

$list_contract = $QAppContract->fetchPagination($page, $limit, $total, $params); 

$this->view->params=$params;
$this->view->list= $list_contract;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/list-contract' . ($params ? '?' . http_build_query($params) .
	'&' : '?');
$this->view->offset = $limit * ($page - 1);

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>
