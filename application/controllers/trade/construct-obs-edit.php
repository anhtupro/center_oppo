<?php 


$QAppCheckshop = new Application_Model_AppCheckshop();
$QConstructObs = new Application_Model_ConstructObs();
$QConstructObsDetail = new Application_Model_ConstructObsDetail();
$QConstructObsPrice = new Application_Model_ConstructObsPrice();
$QConstructObsFile = new Application_Model_ConstructObsFile();


$id = $this->getRequest()->getParam('id');

$construct_obs = $QConstructObs->get($id);
$store_id = $construct_obs['store_id'];
$params = [
    'construct_obs_id' => $id,
    'store_id' => $store_id
];

$list_category_outside = $QConstructObsDetail->getDetail($id, 3);
$list_category_inside = $QConstructObsDetail->getDetail($id, 4);

$store_info = $QAppCheckshop->getStoreInfo($store_id);
$category_store = $QAppCheckshop->getInfoCheckshop($params);

// chi phí
$params['price_type'] = 0;
$result_price = $QConstructObsPrice->getPrice($params);
$list_price = $result_price['list_price'];
$total_price = $result_price['total_price'];

$params['price_type'] = 2;
$result_price_last = $QConstructObsPrice->getPrice($params);
$list_price_last = $result_price_last['list_price'];
$total_price_last = $result_price_last['total_price'];


$params['price_type'] = 4;
$result_price_review = $QConstructObsPrice->getPrice($params);
$list_price_review = $result_price_review['list_price'];
$total_price_review = $result_price_review['total_price'];
// hình ảnh và file
$list_file = $QConstructObsFile->get($id);


$this->view->store_info = $store_info;
$this->view->category_store = $category_store;
$this->view->construct_obs = $construct_obs;
$this->view->list_category_outside = $list_category_outside;
$this->view->list_category_inside = $list_category_inside;

$this->view->list_price = $list_price;
$this->view->total_price = $total_price;

$this->view->list_price_last = $list_price_last;
$this->view->total_price_last = $total_price_last;

$this->view->list_price_review = $list_price_review;
$this->view->total_price_review = $total_price_review;

$this->view->list_file = $list_file;
