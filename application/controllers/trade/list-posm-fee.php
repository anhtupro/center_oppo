<?php
$month = $this->getRequest()->getParam('month');
$year = date('Y');
$page = $this->getRequest()->getParam('page');
$fromDate = $this->getRequest()->getParam('from_date');
$toDate = $this->getRequest()->getParam('to_date');
$costType = $this->getRequest()->getParam('cost_type');
$limit = 10;
$total = 0;

$QCampaign = new Application_Model_Campaign();
$QCampaignArea = new Application_Model_CampaignArea();
$QAdditionalPosmCost = new Application_Model_AdditionalPosmCost();

$params = [
    'list_area' => $this->storage['area_id'],
    'year' => $year,
    'month' => $month,
    'from_date' => $fromDate,
    'to_date' => $toDate,
    'cost_type' => $costType
];
$list = $QCampaign->getListPosmFee($page, $limit, $total, $params);
$totalAdditionalPosmCost = $QAdditionalPosmCost->getTotal($params);

$this->view->offset = $limit * ($page - 1);
$this->view->params = $params;
$this->view->totalAdditionalPosmCost = $totalAdditionalPosmCost;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->list = $list;
$this->view->url = HOST . 'trade/list-posm-fee/' . ($params ? '?' . http_build_query($params) . '&' : '?');



