<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$repair_details_id = $this->getRequest()->getParam('repair_details_id');
$contract_number = $this->getRequest()->getParam('contract_number');

$QRepairDetails = new Application_Model_RepairDetails();

$QRepairDetails->update([
   'contract_number' => $contract_number ? TRIM($contract_number) : ''
], ['id = ?' => $repair_details_id]);

echo json_encode([
   'status' => 0
]);