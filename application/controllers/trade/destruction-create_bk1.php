<?php

$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category_id');
$destruction_details_type = $this->getRequest()->getParam('destruction_details_type');
$destruction_details_tp = $this->getRequest()->getParam('destruction_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');
$image = $this->getRequest()->getParam('image');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QDestruction = new Application_Model_Destruction();
$QDestructionDetails = new Application_Model_DestructionDetails();
$QDestructionDetailsType = new Application_Model_DestructionDetailsType();
$QDestructionDetailsTp = new Application_Model_DestructionDetailsTp();
$QDestructionDetailsFile = new Application_Model_DestructionDetailsFile();
$QDestructionType = new Application_Model_DestructionType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$flashMessenger       = $this->_helper->flashMessenger;

$data_destruction_type = $QDestructionType->fetchAll();
$destruction_type = [];
foreach($data_destruction_type as $key=>$value){
    $destruction_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$params = array_filter(array(
    'store_id' => $store_id,
    'staff_id'  => $userStorage->id
));


if (!empty($submit)) {
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        $destruction = [
            'store_id'  => $store_id,
            'status' => 1,
            'create_at' => date('Y-m-d H:i:s'),
            'staff_id'  => $userStorage->id
        ];
        
        
        $destruction_id = $QDestruction->insert($destruction);

        foreach($category_id as $key=>$value){
            
              
            //nếu các hạng mục đặc biệt như biển hiệu 43, tranh biển hiệu 48, vách hình ảnh 803
            if(in_array($value, [43,48,803])){

                $flag_category_spec = 1;
            }
            $where_update = $QDestruction->getAdapter()->quoteInto('id = ?', $destruction_id);
            $data_update=array(
                'manager_approve'      => $flag_category_spec
            );
            $QDestruction->update($data_update,$where_update);
            //END
            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);
            
            $destruction_details = [
                'destruction_id' => $destruction_id,
                'category_id'   => $value,
                'quantity'  => $number,
                'imei_sn' => $imei[$key],
                'note'  => $note[$key],
                'status'    => 1
            ];
            
            
            $destruction_details_id = $QDestructionDetails->insert($destruction_details);

            foreach($destruction_details_type[$value] as $k=>$v){
                $rdt_insert = [
                    'destruction_details_id' => $destruction_details_id,
                    'destruction_type_id'    => $v
                ];
                $QDestructionDetailsType->insert($rdt_insert);
                
            }

            foreach($destruction_details_tp[$value] as $k=>$v){
                $rdt_insert = [
                    'destruction_details_id' => $destruction_details_id,
                    'tp_id'    => $v
                ];
                $QDestructionDetailsTp->insert($rdt_insert);
            }
            
            foreach($image[$value] as $k=>$v){
                
                $data = $v;
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . $destruction_id. DIRECTORY_SEPARATOR . $destruction_details_id;

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                
                $url = '/photo/destruction/' . $destruction_id. '/' . $destruction_details_id . '/image_'.$k.'.png';

                file_put_contents($uploaded_dir.'/image_'.$k.'.png', $data);
                
                $rdf_insert = [
                    'destruction_details_id' => $destruction_details_id,
                    'url'    => $url,
                    'type'   => 1
                ];
                $QDestructionDetailsFile->insert($rdf_insert);
            }

        }
        
        $db->commit();
        
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        
        $this->_redirect(HOST . 'trade/destruction-edit?id='.$destruction_id);
        
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
        
        $this->_redirect(HOST . 'trade/destruction-create');
        
    }
    
    
    
}

$this->view->params = $params;

$result = $QCategory->getCategory();
$this->view->category = $result;
$this->view->destruction_type = $destruction_type;


$resultCamp = $QCampaign->getListStore($params);
$this->view->store = $resultCamp;

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>