<?php
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$has_contructor = $this->getRequest()->getParam('has_contructor');
$submit            = $this->getRequest()->getParam('submit');
$export            = $this->getRequest()->getParam('export');

//SEARCH
$name_search            = $this->getRequest()->getParam('name_search');
$price            = $this->getRequest()->getParam('price');
$status            = $this->getRequest()->getParam('status');
$type            = $this->getRequest()->getParam('type');
$description            = $this->getRequest()->getParam('description');



$area_id_search         = $this->getRequest()->getParam('area_id_search');
$sale_id            	= $this->getRequest()->getParam('staff_id');

$QAppCheckShop 			= new Application_Model_AppCheckshop();
$QAppCheckShopDetail 	= new Application_Model_AppCheckshopDetail();
$QAppCheckShop 			= new Application_Model_AppCheckshop();
$QArea 					= new Application_Model_Area();
$QCategory 		= new Application_Model_Category();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger       = $this->_helper->flashMessenger;
$limit = 10;
$total = 0;
$params = [
	'price'  		=> $price,
	'status' 		=> $status,
	'type'	 		=> $type,
	'description' 	=> $description,
    'has_contructor'   => $has_contructor,
];

$params['name_search'] = $name_search;
$params['sort'] = $sort;
$params['desc'] = $desc;
$params['export'] = $export;
$params['area_id'] = $this->storage['area_id'];

$resultList = $QCategory->fetchPagination($page, $limit, $total, $params);

$list_sale  = $QAppCheckShop->getSaleArea();
$area_list = $QArea->getAreaList($params);
// echo '<pre>';
// var_dump($data); exit;

if(!empty($export)){
	// echo '<prev>';
	// print_r($resultList); exit;
		include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
		require_once 'PHPExcel.php';
		$PHPExcel = new PHPExcel();
		$PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        $heads = array(
         	'STT',
            'HẠNG MỤC',
            'GIÁ',
            'TRẠNG THÁI',
            'LOẠI',
            'MÔ TẢ'
        );
        //header 
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
     
        //column area
       
       
        $index = 2;
         foreach($resultList as $key=>$value){
         	 $alpha = 'A';
	         $sheet->getCell($alpha++. $index)->setValueExplicit($key+1,PHPExcel_Cell_DataType::TYPE_NUMERIC);
	          $sheet->getCell($alpha++. $index)->setValueExplicit($value['name'],PHPExcel_Cell_DataType::TYPE_STRING);
	          $sheet->getCell($alpha++. $index)->setValueExplicit($value['price'],PHPExcel_Cell_DataType::TYPE_STRING);
	          $sheet->getCell($alpha++. $index)->setValueExplicit(($value['status']==1)?'Enabled':'Disabled',PHPExcel_Cell_DataType::TYPE_STRING);
	          $sheet->getCell($alpha++. $index)->setValueExplicit(($value['type']==1)?'POSM':'Other',PHPExcel_Cell_DataType::TYPE_STRING);
	           $sheet->getCell($alpha++. $index)->setValueExplicit($value['desc'],PHPExcel_Cell_DataType::TYPE_STRING);
	         $index++;
     	}

	

        $filename = 'Category - ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
}
$this->view->data = $data;

$this->view->area = $area_list;
$this->view->list_sale = $list_sale;
$this->view->list= $resultList;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
//http://center_oppo.test/trade/list-category-posm?
$this->view->url = HOST.'trade/list-category-posm'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit * ($page - 1);


if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
?>
