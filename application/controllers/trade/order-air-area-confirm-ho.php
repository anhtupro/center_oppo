<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$orderQuantity = $this->getRequest()->getParam('order_quantity');
$receiveQuantity = $this->getRequest()->getParam('receive_quantity');
$status = $this->getRequest()->getParam('status');
$areaId  = $this->getRequest()->getParam('area_id');
$stageId  = $this->getRequest()->getParam('stage_id');
$categoryId = $this->getRequest()->getParam('category_id');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$QOrderAirAreaStatus = new Application_Model_OrderAirAreaStatus();
$QOrderAirArea = new Application_Model_OrderAirArea();
$QOrderAirDetail = new Application_Model_OrderAirDetail();
$QInventoryAreaPosm = new Application_Model_InventoryAreaPosm();
$QInventoryAreaPosmDetail = new Application_Model_InventoryAreaPosmDetail();
$QCategory = new Application_Model_Category();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();


$head_office_area_id = 65;

$whereStatus = [
    'area_id = ?' => $areaId,
    'order_air_stage_id = ?' => $stageId
];


$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($status == 1) {
    foreach ($orderQuantity as $id => $quantity) {
        $QOrderAirArea->update([
            'order_quantity' => $quantity ? $quantity : 0
        ], ['id = ?' => $id]);
    }

    $QOrderAirAreaStatus->update([
        'status' => 2
    ], $whereStatus);

    $result = [
        'status' => 0,
        'status_name' =>  $QOrderAirAreaStatus->getStatusNameOrderAir(2, 14)
    ];

}

if ($status == 2) {
    foreach ($receiveQuantity as $id => $quantity) {
    

        if ($quantity) {
            $QOrderAirArea->update([
                'receive_quantity' => $quantity ? $quantity : 0
            ], ['id = ?' => $id]);

            // kiểm tra số luọng trong kho head office
            $is_enough_quantity = $QInventoryAreaPosmDetail->checkEnoughQuantityHeadOffice($head_office_area_id, $categoryId[$id], $quantity);

            if (! $is_enough_quantity) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Kho HO không có đủ số lượng để xuất hàng. Vui lòng liên hệ với bộ phận HO và thử lại!'
                ]);

                return;
            }

            $isCategoryAirInventory = $QCategory->isCategoryAirInventory($categoryId[$id]);

            if ($isCategoryAirInventory) { //nếu hạng mục nằm trong kho hạng mục đầu tư
                $existedCategory = $QAppCheckshopDetailChild->fetchRow([
                    'category_id = ?' => $categoryId[$id],
                    'area_id = ?' => $areaId
                ]);

                if ($existedCategory) {
                    $QAppCheckshopDetailChild->update([
                        'quantity' => $existedCategory['quantity'] + $quantity,
                    ], ['id = ?' => $existedCategory['id']]);
                } else {
                    $QAppCheckshopDetailChild->insert([
                        'category_id'  => $categoryId[$id],
                        'quantity' => $quantity,
                        'area_id' => $areaId
                    ]);
                }
            } else { // nếu hạng mục nằm trong kho posm
                // insert detail into posm warehouse

                $QInventoryAreaPosmDetail->insert([
                    'area_id' => $areaId,
                    'category_id' => $categoryId[$id],
                    'quantity' => $quantity ? $quantity : Null,
                    'type' => 1,
                    'date' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id
                ]);

                // update total into posm warehouse
                $where = [
                    'area_id = ?' => $areaId,
                    'category_id = ?' => $categoryId[$id]
                ];
                $inventoryPosmRow = $QInventoryAreaPosm->fetchRow($where);

                if ($inventoryPosmRow) {
                    $QInventoryAreaPosm->update([
                        'quantity' => $quantity + $inventoryPosmRow['quantity']
                    ], $where);
                } else {
                    $QInventoryAreaPosm->insert([
                        'area_id' => $areaId,
                        'category_id' => $categoryId[$id],
                        'quantity' => $quantity ? $quantity : Null
                    ]);
                }

            }


            // trừ số lượng trong kho head office


            // insert detail into posm warehouse

            $QInventoryAreaPosmDetail->insert([
                'area_id' => $head_office_area_id,
                'category_id' => $categoryId[$id],
                'quantity' => $quantity ? $quantity : Null,
                'type' => 2,
                'date' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'note' => 'Khu vực đặt hàng'

            ]);

            // update total into posm warehouse
            $where_head_office = [
                'area_id = ?' => $head_office_area_id,
                'category_id = ?' => $categoryId[$id]
            ];

            $inventoryPosmRowHeadOffice = $QInventoryAreaPosm->fetchRow($where_head_office);

            $QInventoryAreaPosm->update([
                'quantity' => $inventoryPosmRowHeadOffice['quantity'] - $quantity
            ], $where_head_office);



        }

    }

    $QOrderAirAreaStatus->update([
        'status' => 3
    ], $whereStatus);

    $result = [
        'status' => 0,
        'status_name' =>  $QOrderAirAreaStatus->getStatusNameOrderAir(3,14)
    ];
}

$db->commit();

echo json_encode($result);

