<?php
$roundId = $this->getRequest()->getParam('round_id');
$areaId = $this->getRequest()->getParam('area_id');
$storeId = $this->getRequest()->getParam('store_id');
$storeName = $this->getRequest()->getParam('store_name');
$regionalMarket = $this->getRequest()->getParam('regional_market');
$district = $this->getRequest()->getParam('district');
$staffId = $this->getRequest()->getParam('staff_id');
$fromDate = $this->getRequest()->getParam('from_date');
$toDate = $this->getRequest()->getParam('to_date');
$export = $this->getRequest()->getParam('export');
$page = $this->getRequest()->getParam('page', 1);
$status = $this->getRequest()->getParam('status');
$recheck_type = $this->getRequest()->getParam('recheck_type', 1);
$error_image = $this->getRequest()->getParam('error_image');
$limit = LIMITATION;

$QStore= new Application_Model_Store();
$QRegionalMarket= new Application_Model_RegionalMarket();
$QRecheckShop = new Application_Model_RecheckShop ();
$QArea = new Application_Model_Area();
$QAssignAreaRecheckShop = new Application_Model_AssignAreaRecheckShop();
$QRegionalMarket = new Application_Model_RegionalMarket();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QRecheckShopRound = new Application_Model_RecheckShopRound();

//$QStore = new Application_Model_Store();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$recheckShopRound = $QRecheckShopRound->fetchRow(['id = ?' => $roundId]);

$listAssignAreaId = $QAssignAreaRecheckShop->getListAreaId($userStorage->id, $roundId);

$params = [
    'round_id' => $roundId,
    'area_id' => $areaId,
    'store_id' => $storeId,
    'store_name' => $storeName,
    'list_assign_area_id' => $listAssignAreaId ? $listAssignAreaId : [0],
    'regional_market' => $regionalMarket,
    'district' => $district,
    'staff_id' => $staffId,
    'from_date' => $fromDate,
    'to_date' => $toDate,
    'limit_store' => $recheckShopRound['limit_store'],
    'status' => $status,
    'recheck_type' => $recheck_type,
    'list_area' => $this->storage['area_id'],
    'error_image' => $error_image
];

if ($_GET['dev'] == 1) {
    echo '<pre>';
    print_r($params);
    die;

}

if ($export) {
    $QRecheckShop->exportTime($params);
}

$listRecheckShop = $QRecheckShop->fetchPagination($page, $limit, $total, $params);
$listArea = $QArea->getListArea($params);

$listSelfArea = $QArea->fetchAll(['id IN (?)' => $this->storage['area_id']], 'name')->toArray();

$list_sale  = $QAppCheckShop->getSaleArea();

if ($areaId) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $areaId);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regionalMarket) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regionalMarket);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$isRunning = $QRecheckShopRound->isRunning($roundId);

$this->view->listRecheckShop = $listRecheckShop;
$this->view->roundId = $roundId;
$this->view->listArea = $listArea;
$this->view->params = $params;
$this->view->list_sale = $list_sale;
$this->view->isRunning = $isRunning;
$this->view->listSelfArea = $listSelfArea;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);

$this->view->url = HOST . 'trade/list-recheck-shop' . ($params ? '?' . http_build_query($params) . '&' : '?');



