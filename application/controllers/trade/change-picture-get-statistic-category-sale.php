<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$area_id = $this->getRequest()->getParam('area_id');
$stage_id = $this->getRequest()->getParam('stage_id');


if (!$area_id && !$stage_id) {
   return;
}

$params = [
    'stage_id' => $stage_id,
    'area_id' => $area_id
];

$QCategory = new Application_Model_Category();
$QChangePictureDetail = new Application_Model_ChangePictureDetail();

// theo số lượng hạng mục
$list_sale = $QChangePictureDetail->getStatisticCategorySale($params);
//$list_category = $QCategory->fetchAll(['has_painting = 1 OR is_picture = 1'])->toArray();
$list_category = $QChangePictureDetail->getListCategory($stage_id);

echo json_encode([
    'status' => 0,
    'list_sale' => $list_sale,
    'list_category' =>  $list_category
]);

