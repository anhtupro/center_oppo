<?php
$roundId = $this->getRequest()->getParam('round_id');
$storeId = $this->getRequest()->getParam('store_id');

$QAppCheckshop = new Application_Model_AppCheckshop();
$QCategory = new Application_Model_Category();

$latestCheckShop = $QAppCheckshop->getLatestCheckShop($storeId);

// get detail checkshop
if ($latestCheckShop['id']) {
    // image check shop
    $img_check = $QAppCheckshop->getImg($latestCheckShop['id']);
    $img_different = $QAppCheckshop->getImgDifferent($latestCheckShop['id']);

    $listCheckShopDetail = $QAppCheckshop->getCheckshopDetails($latestCheckShop['id']);
    // lấy ra id những category đã checkshop
    foreach ($listCheckShopDetail as $detail) {
        $currentCategoryId [] = $detail['category_id'];
    }
}



$listCategory = $QCategory->getCategoryCheckshop();

// lấy ra danh sách category trừ nhưng category đã checkshop
foreach ($listCategory as $key => $category) {
    if (in_array($category['id'], $currentCategoryId)) {
        unset($listCategory[$key]);
    }
}

$sellout = $QAppCheckshop->getSellout($storeId);

$this->view->listCheckShopDetail = $listCheckShopDetail;
$this->view->listCategory = $listCategory;
$this->view->latestCheckShop = $latestCheckShop;
$this->view->sellout = $sellout;
$this->view->roundId = $roundId;
$this->view->storeId = $storeId;
$this->view->img_check = $img_check;
$this->view->img_different = $img_different;

