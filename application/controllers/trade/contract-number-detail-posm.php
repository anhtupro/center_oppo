<?php

$campaign_id = $this->getRequest()->getParam('campaign_id');

$contract_number = TRIM($contract_number);

$params = [
  'campaign_id' => $campaign_id
];

$QContractorCategory = new Application_Model_ContractorCategory();
$get_contructor_category = $QContractorCategory->getContructorCategory($params);

$contructor_category = [];
foreach($get_contructor_category as $key=>$value){
    if(!$contructor_category[$value['contractor_id']]){
        $contructor_category[$value['contractor_id']] = [
            'contructor_name' => $value['contructor_name'],
            'category_list' => [
                $value['category_id'] => $value
            ]
        ];
    }
    else{
        $contructor_category[$value['contractor_id']]['category_list'][$value['category_id']] = $value;
    }
}

$this->view->params = $params;
$this->view->contructor_category = $contructor_category;

$flashMessenger       = $this->_helper->flashMessenger;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;