<?php
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$page_reject = $this->getRequest()->getParam('page_reject', 1);
$page_remove = $this->getRequest()->getParam('page_remove', 1);
$name_search = $this->getRequest()->getParam('name_search');
$area_id_search = $this->getRequest()->getParam('area_id_search');
$staff_id = $this->getRequest()->getParam('staff_id');
$status_id = $this->getRequest()->getParam('status_id');
$title = $this->getRequest()->getParam('title');

$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');

$export = $this->getRequest()->getParam('export');
$hasFee = $this->getRequest()->getParam('has_fee');
$isKa = $this->getRequest()->getParam('is_ka');
$isBrandshop = $this->getRequest()->getParam('is_brandshop');
$hasFee = $this->getRequest()->getParam('has_fee');
$statusFinish = $this->getRequest()->getParam('status_finish');
$month = $this->getRequest()->getParam('month');
$contractor_id = $this->getRequest()->getParam('contractor_id');
$contract_number = $this->getRequest()->getParam('contract_number');
$big_area_id = $this->getRequest()->getParam('big_area_id');
$export_review_cost = $this->getRequest()->getParam('export_review_cost');
$review_cost = $this->getRequest()->getParam('review_cost');
$year = date('Y');

$list_reject_active = $this->getRequest()->getParam('list_reject_active');
$list_remove_active = $this->getRequest()->getParam('list_remove_active');

$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QStore = new Application_Model_Store();
$QAppStatus = new Application_Model_AppStatus();
$QRepair = new Application_Model_Repair();
$QDestruction = new Application_Model_Destruction();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QContructors = new Application_Model_Contructors();


$flashMessenger = $this->_helper->flashMessenger;
$limit = LIMITATION;
$total = 0;

$params = [
    'name_search' => $name_search,
    'area_id_search' => $area_id_search,
    'staff_id' => $staff_id,
    'status_id' => $status_id,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'list_area' => $this->storage['area_id'],
    'title' => $title,
    'has_fee' => $hasFee,
    'is_ka' => $isKa,
    'is_brandshop' => $isBrandshop,
    'year' => $year,
    'month' => $month,
    'status_finish' => $statusFinish,
    'contractor_id' => $contractor_id,
    'contract_number' => TRIM($contract_number),
    'big_area_id' => $big_area_id,
    'review_cost' => $review_cost
];

$params['sort'] = $sort;
$params['desc'] = $desc;

if (in_array($this->storage['title'], [SALES_TITLE, PGS_SUPERVISOR])) {
    $params['staff_id'] = $this->storage['staff_id'];
}

$params['area_id'] = $this->storage['area_id'];

$repair = $QRepair->fetchPagination($page, $limit, $total, $params); // danh sách chờ xác nhận
$repair_reject = $QRepair->fetchPaginationReject($page_reject, $limit, $total_reject, $params); // danh sách bị từ chối
$repair_remove = $QRepair->fetchPaginationRemove($page_remove, $limit, $total_remove, $params); // danh sách bị từ chối

$contract_number = $QRepair->getContractNumber($params);

$area_list = $QArea->getAreaList($params);

if ($export) {
    $statistics = $QRepair->getStatistic($params);
    $QRepair->export($statistics);
}
if ($export_review_cost) {
    $statistics = $QRepair->getStatisticReviewCost($params);
    $QRepair->exportReviewCost($statistics);
}
$listContractor = $QContructors->fetchAll();
$listBigArea = $QArea->getBigArea();
$this->view->listBigArea = $listBigArea;

$this->view->list_sale = $QAppCheckShop->getSaleArea();
$this->view->area = $area_list;
$this->view->app_status = $QAppStatus->get_cache(4);
$this->view->params = $params;
$this->view->list_normal = $repair;
$this->view->list_reject = $repair_reject;
$this->view->list_remove = $repair_remove;
$this->view->contract_number = $contract_number;
$this->view->listContractor = $listContractor;

// danh sách chờ xác nhận
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/repair-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);


// danh sách từ chối
$this->view->total_reject = $total_reject;
$this->view->url_reject = HOST . 'trade/repair-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset_reject = $limit * ($page_reject - 1);

// danh sách hủy
$this->view->total_remove = $total_remove;
$this->view->url_remove = HOST . 'trade/repair-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset_remove = $limit * ($page_remove - 1);




$this->view->list_reject_active = $list_reject_active;
$this->view->list_remove_active = $list_remove_active;

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>
