<?php 

	$campaign_id   			 = $this->getRequest()->getParam('campaign_id');
	$quantity 				= $this->getRequest()->getParam('quantity');
	$price   				= $this->getRequest()->getParam('price');
	$quantity_area  		= $this->getRequest()->getParam('quantity_area');
	$contractor_area_id  	= $this->getRequest()->getParam('contractor_area_id');

	$category_id    = $this->getRequest()->getParam('category_id');
	$area_id    	= $this->getRequest()->getParam('area_id');
	$submit     	= $this->getRequest()->getParam('submit');
	$contractor_id  = $this->getRequest()->getParam('contractor_id');

	$QCampaign 	   				= new Application_Model_Campaign();
	$QArea 		   				= new Application_Model_Area();
	$QCampaignArea 				= new Application_Model_CampaignArea();
	$QCampaignContractor 		= new Application_Model_CampaignContractor();
	$QCampaignAreaContractor	= new Application_Model_CampaignAreaContractor();
	$QContractorArea			= new Application_Model_ContractorArea();
	$QAreaIn         			= new Application_Model_AreaIn();


	$area        = $QArea->get_cache_bi();
	if(!empty($this->storage['area_id'])){
		$area = [];
		foreach($QArea->get_cache_bi() as $key=>$value){
			if(in_array($key, $this->storage['area_id'])){
				$area[$key] = $value;
			}
		}
	}

	$params = [
		'campaign_id'    => $campaign_id,
		'contractor_id'  => $contractor_id,
                'area_id'        => $this->storage['area_id']
	];

	$area_status = $QCampaignArea->get_status_area($params);
	//$campaign_contractor = $QCampaignContractor->getData($params);

        

	$where 		   = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign 	   = $QCampaign->fetchRow($where);

	$category      		= $QCampaign->getCategory($params);
	$category_campaign 	= $QCampaign->getCategoryCampaign($params);

	$where 					= $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
	$campaign_area_check 	= $QCampaignArea->fetchAll($where);

	if(count($campaign_area_check) == 0){

		$campaign_area_insert = $QCampaignArea->getCampaignAreaInsert($params);
		foreach($campaign_area_insert as $key=>$value){
			$QCampaignArea->insert(
				[
					'campaign_id'  	 => $value['campaign_id'],
					'area_id' 	  	 => $value['area_id'],
					'category_id' 	 => $value['category_id'],
					'price' 		 => $value['price'],
					'status' 		 => 1,
				]
			);
		}
		
	}
        

	//Get data campaign_area
	$order = $QCampaignArea->getCampaignArea($params);
	$data_order    = [];
	foreach($order as $key=>$value){
		$data_order[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data

	//Get data campaign_area
	$campaign_area = $QCampaignAreaContractor->getContractorArea($params);
	$data = [];
	foreach($campaign_area as $key=>$value){
		$data[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data

	//Get data campaign_area_contractor
	$campaign_area_contractor = $QCampaignAreaContractor->getAreaIn($params);
	$data_area_contractor = [];
	foreach($campaign_area_contractor as $key=>$value){
		$data_area_contractor[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get campaign_area_contractor


	if(!empty($submit) AND $submit == 2){

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            $alpha = 'A';
            $index = 1;


            //Phần đầu
            $heads = array(
                '',
                ''
            );

            foreach($category_campaign as $key=>$value){
                    $heads[] = $value['category_id'];
            }

            foreach ($heads as $key)
            {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }

            $alpha = 'A';
            $index = 2;
            //END Phần đầu

            //Phần đầu 2
            $heads = array(
                    'ID Khu vực',
                'Khu vực'
            );

            foreach($category_campaign as $key=>$value){
                    $heads[] = $value['name'];
            }

            foreach ($heads as $key)
            {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }
            $index = 3;
            //END Phần đầu 2


            //KHU VỰC
            foreach($area as $key=>$value){
                    $alpha = 'A';
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($key ,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($value ,PHPExcel_Cell_DataType::TYPE_STRING);

                    
                    $index++;
            }
            //END Khu vực


            $filename = ' Campaign-Limit ' . date('d-m-Y H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
	}


	if(!empty($submit) AND $submit == 1){
            
		if($_FILES["file"]["tmp_name"] != ""){

			include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';

			error_reporting(0);
			set_time_limit(0);
			
			//===================Main=======================

			if($_FILES["file"]["tmp_name"] != ""){
				if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
					//echo ("Không phải file XLSX !");exit;
				}
				if ($_FILES["file"]["error"] > 0)
			    {
			    	echo ("Return Code: " . $_FILES["file"]["error"] . "<br />");exit;
			    }
				move_uploaded_file($_FILES["file"]["tmp_name"], "files/limit_campaign/data.xlsx");
			}

			$inputfile = 'files/limit_campaign/data.xlsx';
			$xlsx = new SimpleXLSX($inputfile);
			$data = $xlsx->rows();


			//Đếm số cột
			$i = 3;
			$num_cols = 0;
			while ($data[0][$i++] <> '') {
			  $num_cols++;
			}

			//Đếm số dòng
			$i = 1;
			$num_rows = 0;
			while ($data[$i++][0] <> '') {
			  $num_rows++;
			}

			for($i = 2; $i <= $num_rows; $i++){
				$area_id = $data[$i][0];

				for($j = 2; $j <= $num_cols; $j++){
					$cat_id 	= $data[0][$j];
					$quantity 	= $data[$i][$j];
					
					$where = [];
					$where[] = $QCampaignArea->getAdapter()->quoteInto('area_id = ?', $area_id);
					$where[] = $QCampaignArea->getAdapter()->quoteInto('category_id = ?', $cat_id);
					$details = [
						'quantity' => (int)$quantity
					];
					$QCampaignArea->update($details, $where);
				}
				
			}


			unlink($inputfile);

			//=================Function Area===================
		    $this->_helper->layout->disableLayout();
		    $back_url = HOST.'trade/area-in?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;
		    $this->redirect($back_url);
		}
		else{
			foreach($quantity as $key=>$value){
				$data_ = array(
					'campaign_id'	=> $campaign_id,
					'contractor_id' => $contractor_id,
					'area_id' 		=> $area_id[$key],
					'category_id'   => $category_id[$key],
					'quantity'		=> $value,
					'price'			=> $price[$key],
					'status'		=> 1
				);
				$QAreaIn->insert($data_);
			}
			$back_url = HOST.'trade/area-in?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;
		    $this->redirect($back_url);
		}
	}


	
	if(!empty($submit) AND $submit == 3){
		foreach($contractor_area_id as $key=>$value){
			$where = $QContractorArea->getAdapter()->quoteInto('id = ?', $value);
			$data_ = array(
				'quantity_area'		=> $quantity_area[$key],
				'area_date'			=> date('Y-m-d H:i:s')
			);
			$QContractorArea->update($data_, $where);
		}
		$back_url = HOST.'trade/area-in?campaign_id='.$campaign_id;
		    $this->redirect($back_url);
	}
	

	$this->view->area     		= $area;
	$this->view->campaign 		= $campaign;
	$this->view->category 		= $category;
	$this->view->data     		= $data;
	$this->view->contractor     = $QCampaign->getContractor($params);
	$this->view->params         = $params;
	$this->view->data_area_contractor = $data_area_contractor;
	$this->view->category_campaign    = $category_campaign;
	$this->view->data_order           = $data_order;
	$this->view->area_status 	= $area_status;

