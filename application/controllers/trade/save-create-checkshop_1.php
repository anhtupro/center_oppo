<?php


$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category_id');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$latitude = $this->getRequest()->getParam('latitude');
$longitude = $this->getRequest()->getParam('longitude');
$category_brand_id = $this->getRequest()->getParam('category_brand_id');
$quantity_brand = $this->getRequest()->getParam('quantity_brand');
$brand_id = $this->getRequest()->getParam('brand_id');
$image = $this->getRequest()->getParam('image');
$image_repair = $this->getRequest()->getParam('image_repair');
$check_shop_entire_id = $this->getRequest()->getParam('check_shop_entire_id');
$is_repair = $this->getRequest()->getParam('is_repair');
$has_change_picture = $this->getRequest()->getParam('has_change_picture');
$list_change_picture_detail_id = $this->getRequest()->getParam('change_picture_detail_id');

$dev = $this->getRequest()->getParam('dev');

$back_url = HOST . 'trade/check-shop-list';
$flashMessenger = $this->_helper->flashMessenger;


$QCheckshop = new Application_Model_AppCheckshop();
$QStore = new Application_Model_Store();
$QCheckshopDetails = new Application_Model_AppCheckShopDetail();
$QAppCheckShopFile = new Application_Model_AppCheckShopFile();
$QAppCheckshopDetailBrand = new Application_Model_AppCheckshopDetailBrand();
$QCheckShopEntireFile = new Application_Model_CheckShopEntireFile();
$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QErrorImageShopCurrent = new Application_Model_ErrorImageShopCurrent();
$QCheckShopEntireErrorImage = new Application_Model_CheckShopEntireErrorImage();
$QChangePictureDetail = new Application_Model_ChangePictureDetail();
//if ($dev) {
//    echo '<pre>';
//    print_r($quantity);
//    die;
//}

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {

    //Check GPS
    $last_checkshop = $QCheckshop->getLastCs($store_id);

//    if(!empty($last_checkshop['latitude'])){
//        $distance_gps = My_DistanceGps::getDistance($last_checkshop['latitude'], $last_checkshop['longitude'], $latitude, $longitude, "K");
//
//        if($distance_gps > 2){
////            $flashMessenger->setNamespace('error')->addMessage('Vị trí GPs không chính xác!.'.$distance_gps.'('.$last_checkshop['latitude'].','.$last_checkshop['longitude'].')('.$latitude.','.$longitude.')');
////            $this->getResponse()->setHeader('Content-Type', 'text/plain')
////                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
////            exit;
//        }
//    }
    //END





    //Nếu có lock: Check số lượng checkshop khớp với số lượng chốt
    $investments_lock = $QCheckshop->getCheckshopLock($store_id);

    if (!empty($investments_lock)) {

        $cat_check = [];
        foreach ($category_id as $key => $value) {

            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);

            $cat_check['quantity'][$value] = $number;
            $cat_check['imei_sn'][$value] = $imei[$key] ? $imei[$key] : NULL;
        }

        $cat_lock = [];
        foreach ($investments_lock as $key => $value) {
            $cat_lock['quantity'][$value['category_id']] = $value['quantity'];
            $cat_lock['imei_sn'][$value['category_id']] = $value['imei_sn'];
        }

        if ($cat_check['quantity'] != $cat_lock['quantity']) {
            $flashMessenger->setNamespace('error')->addMessage("Số lượng không khớp với số chốt.");
            $this->getResponse()->setHeader('Content-Type', 'text/plain')
                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
            exit;
        }
//
//        if ($cat_check['imei_sn'] != $cat_lock['imei_sn']) {
////            $flashMessenger->setNamespace('error')->addMessage("Mã QR Code không khớp với mã chốt.");
////            $this->getResponse()->setHeader('Content-Type', 'text/plain')
////                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
////            exit;
//        }

    }

    if (!empty($latitude) && !empty($longitude)) {
        // update is last check shop
        $whereIsLast = [
            'store_id = ?' => $store_id,
            'is_last = ?' => 1
        ];

        $QCheckshop->update([
            'is_last' => 0
        ],$whereIsLast);

        // insert
        $data = [
            'store_id' => $store_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id'],
            'latitude' => !empty($latitude) ? $latitude : '',
            'longitude' => !empty($longitude) ? $longitude : '',
            'status' => 1,
            'is_last' => 1
        ];
        $id = $QCheckshop->insert($data);
    } else {
        $db->rollBack();
        $flashMessenger->setNamespace('error')->addMessage("Chưa xác định được tọa độ. Vui lòng thử lại!!");
        $this->getResponse()->setHeader('Content-Type', 'text/plain')
            ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
        exit;
    }


    if (count($image) < 3) {
        $db->rollBack();
        $flashMessenger->setNamespace('error')->addMessage("Thất bại. Vui lòng upload ít nhất 3 ảnh!!" );
        $this->getResponse()->setHeader('Content-Type', 'text/plain')
            ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
        exit;
    }

    foreach ($image as $type => $v) {

        $array_data = explode(',', $v);
        $image_base64 = $array_data[1];

        $data = base64_decode($image_base64);

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
            DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . $id;

        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);

        $newName = md5(uniqid('', true)) . '.png';

        $url = '/photo/checkshop/' . $id . '/' . $newName;

        file_put_contents($uploaded_dir . '/' . $newName, $data);

        $url_resize = 'photo' .
            DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $newName;

//            //Resize Anh
        $image = new My_Image_Resize();
        $image->load($url_resize);
        $image->resizeToWidth(700);
        $image->save($url_resize);
//                //END Resize


        if ($url) {
            $img = [
                'checkshop_id' => $id,
                'url' => TRIM($url, '/'),
                'type' => $type
            ];
            $QAppCheckShopFile->insert($img);
        }
        //
    }


    if (!empty($category_id)) {
        foreach ($category_id as $key => $value) {
            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);
            $details = [
                'checkshop_id' => $id,
                'category_id' => $category_id[$key],
                'quantity' => $number,
                'imei_sn' => $imei[$key]
            ];

            $QCheckshopDetails->insert($details);
        }

    } else {
//            $db->rollBack();
//            $flashMessenger->setNamespace('error')->addMessage("Thất bại. Không có hạng mục để kiểm tra!!");
//            $this->getResponse()->setHeader('Content-Type', 'text/plain')
//                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
//            exit;
    }

    // save category brand
    if ($category_brand_id) {
        for ($i = 0; $i < count($category_brand_id); $i++) {
            $QAppCheckshopDetailBrand->insert([
                'checkshop_id' => $id,
                'category_id' => $category_brand_id[$i] ? $category_brand_id[$i] : Null,
                'quantity' => $quantity_brand[$i] ? $quantity_brand[$i] : Null,
                'brand_id' => $brand_id[$i] ? $brand_id[$i] : Null
            ]);
        }
    }


    // image repair
    if ($is_repair) {

        // ảnh
        foreach ($image_repair as $type => $v) {

            $array_data = explode(',', $v);
            $image_base64 = $array_data[1];

            $data = base64_decode($image_base64);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_entire_id . DIRECTORY_SEPARATOR;
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/check_shop_entire/' . $check_shop_entire_id . '/' . $newName;

            file_put_contents($uploaded_dir . '/' . $newName, $data);

            $url_resize = 'photo' .
                DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_entire_id . DIRECTORY_SEPARATOR . $newName;

            //Resize Anh
            $image = new My_Image_Resize();
            $image->load($url_resize);
            $image->resizeToWidth(700);
            $image->save($url_resize);
            // //END Resize

            $QCheckShopEntireFile->insert([
                'url' => $url,
                'type' => $type,
                'check_shop_id' => $check_shop_entire_id,
                'image_type' => 3,
                'created_by' => $this->storage['staff_id']
            ]);

        }



        // cập nhật các lỗi hình ảnh hiện tại
        foreach ($is_repair as $entire_error_image_id => $status_repair) {
            $QCheckShopEntireErrorImage->update([
                'is_repair' => $status_repair ? $status_repair : 0
            ], ['id = ?' => $entire_error_image_id]);
        }

        $error_shop = $QCheckShopEntireErrorImage->checkErrorShop($check_shop_entire_id);

        $QCheckShopEntire->update([
            'is_repair' => $error_shop ? 0 : 1,
            'is_sale_repair' => $error_shop ? 0 : 1,
            'repair_by' => $this->storage['staff_id']
        ],['id = ?' => $check_shop_entire_id]);

        $current_error_shop = $QErrorImageShopCurrent->fetchRow(['store_id = ?' => $store_id]);

        if ($current_error_shop) {
            $QErrorImageShopCurrent->update([
                'error_image' => $error_shop ? 1 : 0
            ], ['store_id = ?' => $store_id]);
        } else {
            $QErrorImageShopCurrent->insert([
                'store_id' => $store_id,
                'error_image' => $error_shop ? 1 : 0
            ]);
        }
    }
    //save image repair


    // thay tranh

    if ($has_change_picture) {
        // cập nhật mới
        foreach ($has_change_picture as $change_picture_detail_id => $value) {
            $QChangePictureDetail->update([
                'has_changed_picture' => 1
            ], ['id = ?' => $change_picture_detail_id]);
        }
    }

    //end thay tranh

    $db->commit();
    $flashMessenger->setNamespace('success')->addMessage('Thành công!');
    $this->getResponse()->setHeader('Content-Type', 'text/plain')
        ->setBody(HOST . 'trade/check-shop-list')->sendResponse();
    exit;


} catch (Exception $e) {
    $db->rollBack();
    $flashMessenger->setNamespace('error')->addMessage("Có lỗi: " . $e->getMessage());
    $this->getResponse()->setHeader('Content-Type', 'text/plain')
        ->setBody($back_url)->sendResponse();
    exit;
}
// ---------------HIEN THI THONG BAO ----------------------

