<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$listCategoryChild = $this->getRequest()->getParam('list_category_child');
$categoryParentName = $this->getRequest()->getParam('category_parent_name');
$categoryParentId = $this->getRequest()->getParam('category_parent_id');

$QCategoryAdditional = new Application_Model_CategoryAdditional();
$QCategory = new Application_Model_Category();


$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($categoryParentId) { // save edit

    $QCategoryAdditional->update([
       'name' =>  $categoryParentName
    ], ['id = ?' => $categoryParentId]);

    $QCategory->update([
        'parent_id_additional' => Null
    ], ['parent_id_additional = ?' => $categoryParentId]);
        
    if ($listCategoryChild) {
        foreach ($listCategoryChild as $categoryChild) {
            $QCategory->update([
                'parent_id_additional' => $categoryParentId
            ], ['id = ?' => $categoryChild]);
        }
    }
} else { // save create
    $categoryParentId = $QCategoryAdditional->insert([
        'name' =>  $categoryParentName
    ]);

    if ($listCategoryChild) {
        foreach ($listCategoryChild as $categoryChild) {
            $QCategory->update([
                'parent_id_additional' => $categoryParentId
            ], ['id = ?' => $categoryChild]);
        }
    }
}

$db->commit();

echo json_encode([
   'status' => 0
]);
