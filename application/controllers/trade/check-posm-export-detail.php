<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$stage_id = $this->getRequest()->getParam('stage_id');
$params = [
  'stage_id' => $stage_id,
  'area_list' => $this->storage['area_id']
];

$QCheckPosmStage = new Application_Model_CheckPosmStage();

$stage = $QCheckPosmStage->fetchRow(['id = ?' => $stage_id]);

$QCheckPosmStage->exportDetail($params);