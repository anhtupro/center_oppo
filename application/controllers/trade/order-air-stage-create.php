<?php
$stageId = $this->getRequest()->getParam('stage_id');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QOrderAirStage = new Application_Model_OrderAirStage();
$QOrderAirDetail = new Application_Model_OrderAirDetail();
$QPurchasingType = new Application_Model_PurchasingType();

$listCategory = $QCategory->getCategoryOrderAir();
$purchasingTypeByDepartment  = $QPurchasingType->getCacheTypeByDepartment();

if ($stageId) {
    $orderAirStage = $QOrderAirStage->fetchRow(['id = ?'=> $stageId]);
    $listStageDetail = $QOrderAirDetail->fetchAll(['order_air_stage_id = ?' => $stageId]);
}

$this->view->stageId = $stageId;
$this->view->listCategory = $listCategory;
$this->view->orderAirStage = $orderAirStage;
$this->view->listStageDetail = $listStageDetail;
$this->view->purchasing_type = $purchasingTypeByDepartment[$userStorage->department];

