<?php
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$submit = $this->getRequest()->getParam('submit');
$key_search = $this->getRequest()->getParam('search_shop_check');
$name_search = $this->getRequest()->getParam('name_search');
$area_id = $this->getRequest()->getParam('area_id');
$regional_market = $this->getRequest()->getParam('regional_market');
$district = $this->getRequest()->getParam('district');
$staff_id = $this->getRequest()->getParam('staff_id');
$is_ka = $this->getRequest()->getParam('is_ka');
$is_ka_details = $this->getRequest()->getParam('is_ka_details');

$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$type_qc = $this->getRequest()->getParam('type_qc');


$is_pending = $this->getRequest()->getParam('is_pending');

$month = $this->getRequest()->getParam('month', date('m', strtotime(date('Y-m') . " -1 month")));
$year = $this->getRequest()->getParam('year', date('Y', strtotime(date('Y-m') . " -1 month")));

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QStore = new Application_Model_Store();
$QCampaign = new Application_Model_Campaign();
$QCategory = new Application_Model_Category();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QAppCheckShopDetail = new Application_Model_AppCheckshopDetail();
$QAppFile = new Application_Model_AppFile();
$QArea = new Application_Model_Area();
$QAppCheckShopQc = new Application_Model_AppCheckshopQc(); //TUONG
$flashMessenger = $this->_helper->flashMessenger;
$limit = 20;
$total = 0;

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'list_area' => $this->storage['area_id'],
    'area_id' => $area_id,
    'regional_market' => $regional_market,
    'district' => $district,
    'staff_id' => $staff_id,
    'name_search' => $name_search,
    'is_ka' => $is_ka,
    'is_ka_details' => $is_ka_details,
    'checkshop_qc_selectbox' => $checkshop_qc_selectbox, //TUONG
    'month' => $month,
    'year' => $year,
    'type_qc' => $type_qc,
    'is_pending' => $is_pending
];

if ($area_id) {
    $QRegionalMarket = new Application_Model_RegionalMarket();
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regional_market) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$params['sort'] = $sort;
$params['desc'] = $desc;
$params['title'] = $userStorage->title;
$params['key_search'] = $key_search;
if ($userStorage->title == SALES_TITLE) {// -------------------HIỂN THỊ ĐỀ XUẤT DÀNH CHO SALE --------------
    $params['staff_id'] = $userStorage->id;

}

$list_sale = $QAppCheckShop->getSaleArea();
$list_channel = $QAppCheckShop->getChannel();
$list_checkshop_qc = $QAppCheckShopQc->get_cache();

if (!empty($is_pending) AND $is_pending == 1) {

    $shop_check_in_month_arr = [];
    $shop_check_in_month = $QAppCheckShop->getListCheckshopInMonth($params);
    foreach ($shop_check_in_month as $key => $value) {
        $shop_check_in_month_arr[] = $value['store_id'];
    }

    $params['shop_check_in_month_arr'] = $shop_check_in_month_arr;
}

$resultList = $QAppCheckShop->getUpdateInforShopList($page, $limit, $total, $params);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();


$list_store = [];
foreach ($resultList as $key => $value) {
    $list_store[] = $value['store_id'];
}

$area_list = $QArea->getAreaList($params);

//$this->view->store =$list_store;
$this->view->list_sale = $list_sale;
$this->view->list_channel = $list_channel;
$this->view->list_checkshop_qc = $list_checkshop_qc;//TUONG
$this->view->area = $area_list;
$this->view->list = $resultList;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->url = HOST . 'trade/update-info-shop-list' . ($params ? '?' . http_build_query($params) . '&' : '?');

$this->view->offset = $limit * ($page - 1);

// ---------------hien thi thong bao ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages())) {

    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}

?>
