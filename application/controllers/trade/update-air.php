<?php
$QAppAirFile = new Application_Model_AppAirFile();
$QAppAir = new Application_Model_AppAir();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QCategory = new Application_Model_Category();
$flashMessenger       = $this->_helper->flashMessenger;
$title = $userStorage->title;

$id = $this->getRequest()->getParam('id');
$submit = $this->getRequest()->getParam('submit');

$params = array(
	'id' => $id,
	'title'=>$title
);
$this->view->params = $params;
$back_url = HOST.'trade/air';
//lấy hạng mục
$result = $QAppAir->getCategory();
$this->view->category = $result;


//Lấy thông tin shop
$shop=$QAppAir->getShopDeXuat($id);
$this->view->shop=$shop;
//lấy status
 $status=$QAppAir->getStatus($id);
 $this->view->status=$status;
//lấy ds hạng mục đã đề xuất
$dexuat=$QAppAir->getDSDeXuatUpdate($params);
//var_dump($dexuat); exit;
$this->view->dexuat=$dexuat;
//xóa dữ liệu cũ

//lấy đường dẫn file đề xuất
$file =  $QAppAirFile->getFileDecord($id);
$this->view->file = $file;
// ---------------HIEN THI THONG BAO ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages          = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages = $messages;
}