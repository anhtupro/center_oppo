<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$parentId = $this->getRequest()->getParam('parent_id');

$QInventoryCategoryPosmChild = new Application_Model_InventoryCategoryPosmChild();

$listCategoryChild = $QInventoryCategoryPosmChild->get($parentId);


echo json_encode([
   'status' => 0,
   'listCategoryChild' => $listCategoryChild
]);