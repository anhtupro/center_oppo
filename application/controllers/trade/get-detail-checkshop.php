<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$storeId = $this->getRequest()->getParam('store_id');
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QStore = new Application_Model_Store();

$detail = $QAppCheckshopDetailChild->getDetailCheckshop($storeId);
$store = $QStore->fetchRow(['id = ?' => $storeId])->toArray();
$data = [
    'code' => 1,
    'data'  => $detail,
    'store' => $store
];

echo json_encode($data);
exit;

