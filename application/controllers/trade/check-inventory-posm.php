<?php
$areaId = $this->getRequest()->getParam('area_id');

$QCategory = new Application_Model_Category();
$QInventoryAreaPosm = new Application_Model_InventoryAreaPosm();
$QInventoryAreaPosmDetail = new Application_Model_InventoryAreaPosmDetail();
$QArea = new Application_Model_Area();
$QInventoryCategoryPosmParent = new Application_Model_InventoryCategoryPosmParent();

$params = [
    'area_id' => $areaId,
    'statistic_posm' => 1
];

//$listCategory = $QCategory->GetCategoryPosm($params);

if ($areaId != 65) { // head office
    $params ['not_inventory_head_office'] = 1;
}

$listParentCategory = $QInventoryCategoryPosmParent->getAll($params);

$listCurrentCategory = $QInventoryAreaPosm->getAll($params);
$listHistory = $QInventoryAreaPosmDetail->getDetail($params);


$this->view->listCurrentCategory = $listCurrentCategory;
//$this->view->listCategory = $listCategory;
$this->view->listParentCategory = $listParentCategory;
$this->view->areaId = $areaId;
$this->view->areaName = $QArea->fetchRow(['id = ?' => $areaId])->toArray()['name'];
$this->view->listHistory = $listHistory;
