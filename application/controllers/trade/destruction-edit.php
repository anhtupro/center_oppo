<?php

$id = $this->getRequest()->getParam('id');
$category_id = $this->getRequest()->getParam('category_id');
$destruction_details_type = $this->getRequest()->getParam('destruction_details_type');
$destruction_details_tp = $this->getRequest()->getParam('destruction_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppStatus = new Application_Model_AppStatus();
$QContructors = new Application_Model_Contructors();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QDestruction = new Application_Model_Destruction();
$QDestruction = new Application_Model_Destruction();
$QDestructionDetails = new Application_Model_DestructionDetails();
$QDestructionDetailsType = new Application_Model_DestructionDetailsType();
$QDestructionDetailsTp = new Application_Model_DestructionDetailsTp();
$QDestructionPart = new Application_Model_DestructionPart();
$QDestructionType = new Application_Model_DestructionType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QDestructionQuotation = new Application_Model_DestructionQuotation();
$QDestructionPrice = new Application_Model_DestructionPrice();
$QDestructionImage = new Application_Model_DestructionImage();

$flashMessenger       = $this->_helper->flashMessenger;

$data_destruction_type = $QDestructionType->fetchAll();
$destruction_type = [];
foreach($data_destruction_type as $key=>$value){
    $destruction_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$app_status = $QAppStatus->get_cache(12);
$app_status_title_cache = $QAppStatusTitle->get_cache(12);

$app_status_title = [];
foreach($this->storage['title_list'] as $key=>$value){
    $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);
}
$destruction_row = $QDestruction->fetchRow(['id = ?' => $id]);
$params = array(
    'id' => $id,
    'destruction_id' => $id,
    'version' => $destruction_row['version'],
    'store_id' => $destruction_row['store_id']
);

$contructors = $QContructors->fetchAll();
$destruction_details = $QDestructionDetails->getDestructionDetails($params);
foreach ($destruction_details as $detail) {
    if ($detail['destruction_type'] == 1) { // nhà thầu điều chuyển
        $destruction_details_contractor [] = $detail;

    }
    if ($detail['destruction_type'] == 2) { // khu vực điều chuyển
        $destruction_details_area [] = $detail;

    }
}


$destruction_part = $QDestructionDetailsType->getDestructionDetailsPart($params);


$params['price_type'] = 0;
$result_destruction_price = $QDestructionPrice->getDestructionQuotation($params);
$list_price = $result_destruction_price['list_price'];
$total_price = $result_destruction_price['total_price'];

$params['price_type'] = 2;
$result_destruction_price_last = $QDestructionPrice->getDestructionQuotation($params);
$list_price_last = $result_destruction_price_last['list_price'];
$total_price_last = $result_destruction_price_last['total_price'];

$params['price_type'] = 4;
$result_destruction_price_review = $QDestructionPrice->getDestructionQuotation($params);
$list_price_review = $result_destruction_price_review['list_price'];
$total_price_review = $result_destruction_price_review['total_price'];

$destruction = $QDestruction->getDestruction($params);
$sellout = $QAppCheckshop->getSellout($destruction['store_id']);

$params['store_id'] = $destruction['store_id'];

$app_checkshop = $QAppCheckshop->getInfoCheckshop($params);

$listImage = $QDestructionImage->getImage($id);

$this->view->params = $params;
$this->view->category = $result;
$this->view->destruction_type = $destruction_type;
$this->view->destruction_details = $destruction_details;
$this->view->destruction = $destruction;
$this->view->sellout = $sellout;
$this->view->app_checkshop = $app_checkshop;
$this->view->app_status = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->contructors = $contructors;
$this->view->list_price = $list_price;
$this->view->total_price = $total_price;
$this->view->list_price_last = $list_price_last;
$this->view->total_price_last = $total_price_last;
$this->view->destruction_details_contractor = $destruction_details_contractor;
$this->view->destruction_details_area = $destruction_details_area;
$this->view->list_price_review = $list_price_review;
$this->view->total_price_review = $total_price_review;
$this->view->listImage = $listImage;

$this->view->staff_id = $userStorage->id;
$this->view->special_user = [15038];


$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>