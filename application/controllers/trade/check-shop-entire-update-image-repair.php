<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$check_shop_entire_id = $this->getRequest()->getParam('check_shop_entire_id');
$image_repair = $this->getRequest()->getParam('image_repair');

$QCheckShopEntireFile = new Application_Model_CheckShopEntireFile();

foreach ($image_repair as $type => $v) {

    $array_data = explode(',', $v);
    $image_base64 = $array_data[1];

    $data = base64_decode($image_base64);

    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
        DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_entire_id . DIRECTORY_SEPARATOR;
    if (!is_dir($uploaded_dir))
        @mkdir($uploaded_dir, 0777, true);

    $newName = md5(uniqid('', true)) . '.png';

    $url = '/photo/check_shop_entire/' . $check_shop_entire_id . '/' . $newName;

    file_put_contents($uploaded_dir . '/' . $newName, $data);

    $url_resize = 'photo' .
        DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_entire_id . DIRECTORY_SEPARATOR . $newName;

    //Resize Anh
    $image = new My_Image_Resize();
    $image->load($url_resize);
    $image->resizeToWidth(700);
    $image->save($url_resize);
    // //END Resize

    $QCheckShopEntireFile->insert([
        'url' => $url,
        'type' => $type,
        'check_shop_id' => $check_shop_entire_id,
        'image_type' => 3,
        'created_by' => $this->storage['staff_id']
    ]);

}

echo json_encode([
    'status' => 0
]);

return;