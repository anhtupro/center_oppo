<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$list_quantity = $this->getRequest()->getParam('quantity');
$list_position = $this->getRequest()->getParam('position');
$store_id = $this->getRequest()->getParam('store_id');
$note = $this->getRequest()->getParam('note');
$error_image = $this->getRequest()->getParam('error_image');
$list_error_note = $this->getRequest()->getParam('error_note');
$image = $this->getRequest()->getParam('image');
$image_error = $this->getRequest()->getParam('image_error');
$image_repair = $this->getRequest()->getParam('image_repair');
$is_repair = $this->getRequest()->getParam('is_repair');
$latitude = $this->getRequest()->getParam('latitude');
$longitude = $this->getRequest()->getParam('longitude');

$list_category_brand_id = $this->getRequest()->getParam('category_brand_id');
$list_quantity_brand = $this->getRequest()->getParam('quantity_brand');
$list_brand_id = $this->getRequest()->getParam('brand_id');

// display product
$product_display_name = $this->getRequest()->getParam('product_display_name');
$category_display_id = $this->getRequest()->getParam('category_display_id');
$position_display = $this->getRequest()->getParam('position_display');
$point_display = $this->getRequest()->getParam('point_display');
$promotion = $this->getRequest()->getParam('promotion');
$promotion_value = $this->getRequest()->getParam('promotion_value');
$quantity_category_display = $this->getRequest()->getParam('quantity_category_display');
$note = $this->getRequest()->getParam('note');

$has_change_picture = $this->getRequest()->getParam('has_change_picture');
$list_change_picture_detail_id = $this->getRequest()->getParam('change_picture_detail_id');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QRecheckShop = new Application_Model_RecheckShop();
$QRecheckShopDetail = new Application_Model_RecheckShopDetail();
$QRecheckShopFile = new Application_Model_RecheckShopFile();
$QRecheckShopRound = new Application_Model_RecheckShopRound();
$QRecheckShopErrorImage = new Application_Model_RecheckShopErrorImage();

$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QCheckShopEntireDetail = new Application_Model_CheckShopEntireDetail();
$QCheckShopEntireDetailBrand = new Application_Model_CheckShopEntireDetailBrand();
$QCheckShopEntireDisplayProduct = new Application_Model_CheckShopEntireDisplayProduct();
$QCheckShopEntireErrorImage = new Application_Model_CheckShopEntireErrorImage();
$QCheckShopEntireImageRatio = new Application_Model_CheckShopEntireImageRatio();
$QCheckShopEntirePointTotal = new Application_Model_CheckShopEntirePointTotal();
$QCheckShopEntireFile = new Application_Model_CheckShopEntireFile();
$QErrorImageShopCurrent = new Application_Model_ErrorImageShopCurrent();

$QCheckShopEntireDisplayProductDetail = new Application_Model_CheckShopEntireDisplayProductDetail();
$QCheckShopEntireDisplayProductPromotion = new Application_Model_CheckShopEntireDisplayProductPromotion();

$QChangePictureDetail = new Application_Model_ChangePictureDetail();

// check GPS

if (!$latitude || !$longitude) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng bật GPS!'
    ]);
    return;
}

// check image check shop
if (count($image) < 3) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chụp ít nhất 3 hình ảnh tại shop !'
    ]);
    return;
}

// check image error
if ($error_image == 1 && count($image_error) < 1) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chụp ít nhất 1 hình ảnh hạng mục bị lỗi !'
    ]);
    return;
}

//// check image repair
//if ($is_repair == 1 && count($image_repair) < 1) {
//    echo json_encode([
//        'status' => 1,
//        'message' => 'Vui lòng chụp ít nhất 1 hình ảnh sửa chữa hạng mục !'
//    ]);
//    return;
//}
//// end check
///
///

$count_error = count($list_error_note);
$count_repair = count($is_repair);
if ($error_image) {
    if ($count_repair == $count_error) {
        $is_repair_all = 1;
    } else {
        $is_repair_all = 0;
    }
} else {
    $is_repair_all = 0;
}


$db = Zend_Registry::get('db');
$db->beginTransaction();

try {
    // update is last check shop
    $whereIsLast = [
      'store_id = ?' => $store_id,
        'is_last = ?' => 1
    ];

    $QCheckShopEntire->update([
        'is_last' => 0
    ],$whereIsLast);

    // insert
    $check_shop_id = $QCheckShopEntire->insert([
        'store_id' => $store_id,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
        'error_image' => $error_image ? $error_image : 0,
        'status' => 1,
        'is_last' => 1,
        'is_repair' => $is_repair_all ? $is_repair_all : 0,
//        'repair_by' => $is_repair ? $userStorage->id : Null,
        'note' => $note,
        'latitude' => $latitude ? $latitude : '',
        'longitude' => $longitude ? $longitude : ''
    ]);

    if ($list_quantity) {
        foreach ($list_quantity as $categoryId => $quantity) {
            $QCheckShopEntireDetail->insert([
                'check_shop_id' => $check_shop_id,
                'category_id' => $categoryId,
                'quantity' => $quantity,
                'position' => $list_position[$categoryId]
            ]);
        }
    }

    if ($error_image) {
        if ($list_error_note) {
            foreach ($list_error_note as $errorId => $errorNote) {
                $QCheckShopEntireErrorImage->insert([
                    'check_shop_id' => $check_shop_id,
                    'error_image_id' => $errorId,
                    'note' => $errorNote ? $errorNote : '',
                    'is_repair' => $is_repair[$errorId] ? $is_repair[$errorId] : 0,
                    'repair_by' => $is_repair[$errorId] ? $userStorage->id : 0,
                    'repair_type' => $is_repair[$errorId] ? 1 : 0
                ]);
            }
        }
    }

    // save category brand
    if ($list_category_brand_id) {
        for ($i = 0; $i < count($list_category_brand_id); $i++) {
            $QCheckShopEntireDetailBrand->insert([
                'check_shop_id' => $check_shop_id,
                'category_id' => $list_category_brand_id[$i] ? $list_category_brand_id[$i] : Null,
                'quantity' => $list_quantity_brand[$i] ? $list_quantity_brand[$i] : Null,
                'brand_id' => $list_brand_id[$i] ? $list_brand_id[$i] : Null
            ]);
        }
    }

    // save current error shop
    if ($error_image) {
        if ($is_repair_all) {
            $current_error_image = 0;
        } else {
            $current_error_image = 1;
        }
    } else {
        $current_error_image = 0;
    }

    $current_error_shop = $QErrorImageShopCurrent->fetchRow(['store_id = ?' => $store_id]);

    if ($current_error_shop) {
        $QErrorImageShopCurrent->update([
            'error_image' => $current_error_image ? $current_error_image : 0
        ], ['store_id = ?' => $store_id]);
    } else {
        $QErrorImageShopCurrent->insert([
           'store_id' => $store_id,
           'error_image' => $current_error_image ? $current_error_image : 0
        ]);
    }
    // end save current error shop



    // save image

    // image check shop
    foreach ($image as $type => $v) {

        $array_data = explode(',', $v);
        $image_base64 = $array_data[1];

        $data = base64_decode($image_base64);

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
            DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_id . DIRECTORY_SEPARATOR;
        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);

        $newName = md5(uniqid('', true)) . '.png';

        $url = '/photo/check_shop_entire/' . $check_shop_id . '/' . $newName;

        file_put_contents($uploaded_dir . '/' . $newName, $data);

        $url_resize = 'photo' .
            DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_id . DIRECTORY_SEPARATOR . $newName;

        //Resize Anh
        $image = new My_Image_Resize();
        $image->load($url_resize);
        $image->resizeToWidth(700);
        $image->save($url_resize);
        // //END Resize

        $QCheckShopEntireFile->insert([
            'url' => $url,
            'type' => $type,
            'check_shop_id' => $check_shop_id,
            'image_type' => 1
        ]);

    }

    // image error
    foreach ($image_error as $type => $v) {

        $array_data = explode(',', $v);
        $image_base64 = $array_data[1];

        $data = base64_decode($image_base64);

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
            DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_id . DIRECTORY_SEPARATOR;
        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);

        $newName = md5(uniqid('', true)) . '.png';

        $url = '/photo/check_shop_entire/' . $check_shop_id . '/' . $newName;

        file_put_contents($uploaded_dir . '/' . $newName, $data);

        $url_resize = 'photo' .
            DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_id . DIRECTORY_SEPARATOR . $newName;

        //Resize Anh
        $image = new My_Image_Resize();
        $image->load($url_resize);
        $image->resizeToWidth(700);
        $image->save($url_resize);
        // //END Resize

        $QCheckShopEntireFile->insert([
            'url' => $url,
            'type' => $type,
            'check_shop_id' => $check_shop_id,
            'image_type' => 2,
            'created_by' => $userStorage->id
        ]);

    }

    // image repair
    foreach ($image_repair as $type => $v) {

        $array_data = explode(',', $v);
        $image_base64 = $array_data[1];

        $data = base64_decode($image_base64);

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
            DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_id . DIRECTORY_SEPARATOR;
        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);

        $newName = md5(uniqid('', true)) . '.png';

        $url = '/photo/check_shop_entire/' . $check_shop_id . '/' . $newName;

        file_put_contents($uploaded_dir . '/' . $newName, $data);

        $url_resize = 'photo' .
            DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_id . DIRECTORY_SEPARATOR . $newName;

        //Resize Anh
        $image = new My_Image_Resize();
        $image->load($url_resize);
        $image->resizeToWidth(700);
        $image->save($url_resize);
        // //END Resize

        $QCheckShopEntireFile->insert([
            'url' => $url,
            'type' => $type,
            'check_shop_id' => $check_shop_id,
            'image_type' => 3,
            'created_by' => $userStorage->id
        ]);

    }
    // end save photo



    //save display product
    $total_point_image_ratio = 0;
    $total_point_display_product = 0;
    foreach ($product_display_name as $brand_id => $array_product_name) {
        foreach ($array_product_name as $key => $product_name) {
            if ($product_name) {
                $total_point_each_product = 0;
                $display_product_id = $QCheckShopEntireDisplayProduct->insert([
                    'check_shop_id' => $check_shop_id,
                    'brand_id' => $brand_id ? $brand_id : 0,
                    'product_name' => $product_name ? $product_name : ''
                ]);

                foreach ($category_display_id[$brand_id][$key] as $index => $category_id) {
                    if ($category_id) {
                        $position =  $position_display[$brand_id][$key][$index];
                        $point =  $point_display[$brand_id][$key][$index] + 1;  // 1: với mỗi hạng mục sẽ +1 điểm
                        $total_point_each_product += $point;

                        $quantity = $quantity_category_display[$brand_id][$key][$index];
                        $list_promotion = $promotion[$brand_id][$key][$index];
                        $list_promotion_value = $promotion_value[$brand_id][$key][$index];

                        $product_detail_id = $QCheckShopEntireDisplayProductDetail->insert([
                            'display_product_id' => $display_product_id,
                            'category_id' => $category_id ? $category_id : 0,
                            'position' => $position ? $position : 0,
                            'point' => $point ? $point : 0,
                            'quantity' => $quantity ? $quantity : 0
                        ]);

                        // promotion
                        if ($product_detail_id) {
                            foreach ($list_promotion as $k => $promotion_id) {
                                $pro_val = $list_promotion_value[$k];

                                $QCheckShopEntireDisplayProductPromotion->insert([
                                    'product_detail_id' =>  $product_detail_id,
                                    'promotion_id' => $promotion_id ? $promotion_id : 0,
                                    'promotion_value' => $pro_val  ? $pro_val : ''
                                ]);
                            }
                        }

                    }
                }

                // update total point each product
                $display_product_id = $QCheckShopEntireDisplayProduct->update([
                    'total_point' => $total_point_each_product
                ], ['id = ?' => $display_product_id]);
            }
        }
    }




    // thay tranh
    if ($has_change_picture) {
        // cập nhật mới
        foreach ($has_change_picture as $change_picture_detail_id => $value) {
            $QChangePictureDetail->update([
                'has_changed_picture' => 1
            ], ['id = ?' => $change_picture_detail_id]);
        }
    }
    //end thay tranh


    $db->commit();
  
    echo json_encode([
        'status' => 0,
        'check_shop_id'	=> $check_shop_id
    ]);

}catch (\Exception $e){
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage() . ' Có lỗi'
    ]);
}