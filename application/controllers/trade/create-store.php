<?php

$id                     = $this->getRequest()->getParam('id');
$name                   = $this->getRequest()->getParam('name');
$address                = $this->getRequest()->getParam('address');
$regional_market        = $this->getRequest()->getParam('regional_market');
$district               = $this->getRequest()->getParam('district');
$submit                 = $this->getRequest()->getParam('submit');

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QArea = new Application_Model_Area();
$QStoreOutside = new Application_Model_StoreOutside();


if($this->getRequest()->getMethod()=='POST')
{
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        $data = [
            'name' => $name,
            'address' => $address,
            'regional_market' => $regional_market,
            'district' => $district,
        ];
        
        if(empty($id)){
            $id = $QStoreOutside->insert($data);
        }
        else{
            $where = $QStoreOutside->getAdapter()->quoteInto("id = ?", $id);
            $QStoreOutside->update($data, $where);
        }
        
        $db->commit();
        
        $flashMessenger ->setNamespace('success')->addMessage('Hoàn thành!');
        $this->_redirect('/trade/check-shop-outside-list');
            
        
    } catch (Exception $e) {
        $db->rollback();
        $e->getMessage();
        
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->_redirect('/trade/create-store');
        
    }
    
}

$params = [
  'list_area' =>   $this->storage['area_id']
];



$area_list = $QArea->getAreaList($params);
$this->view->area_list = $area_list;

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;



