<?php
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);

// ---T
$name_from = $this->getRequest()->getParam('name_from');
$name_to = $this->getRequest()->getParam('name_to');
$area_id_search = $this->getRequest()->getParam('area_id_search');
$sale_id = $this->getRequest()->getParam('staff_id');
$status_id = $this->getRequest()->getParam('status_id');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$title = $this->getRequest()->getParam('title');
$export = $this->getRequest()->getParam('export');
$hasFee = $this->getRequest()->getParam('has_fee');
$month = $this->getRequest()->getParam('month');
$statusFinish = $this->getRequest()->getParam('status_finish');
$contractor_id = $this->getRequest()->getParam('contractor_id');
$contract_number = $this->getRequest()->getParam('contract_number');
$big_area_id = $this->getRequest()->getParam('big_area_id');
$review_cost = $this->getRequest()->getParam('review_cost');
$export_review_cost = $this->getRequest()->getParam('export_review_cost');

$year = date('Y');

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAppStatus = new Application_Model_AppStatus();
$QTransfer = new Application_Model_Transfer();
$QDestruction = new Application_Model_Destruction();
// ---T
$QAppCheckShop = new Application_Model_AppCheckshop();
$QArea = new Application_Model_Area();
$QContructors = new Application_Model_Contructors();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$flashMessenger = $this->_helper->flashMessenger;
$limit = LIMITATION;
$total = 0;

// ---T
$params = [
    'status_id' => $status_id,
    'name_from' => $name_from,
    'name_to' => $name_to,
    'area_id_search' => $area_id_search,
    'sale_id' => $sale_id,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'list_area' => $this->storage['area_id'],
    'title' => $title,
    'has_fee' =>$hasFee,
    'year' => $year,
    'month' => $month,
    'status_finish' => $statusFinish,
    'contractor_id' => $contractor_id,
    'contract_number' => TRIM($contract_number),
    'big_area_id' => $big_area_id,
    'review_cost' => $review_cost
];



$params['sort'] = $sort;
$params['desc'] = $desc;

if (in_array($this->storage['title'], [SALES_TITLE])) {
    $params['staff_id'] = $this->storage['staff_id'];
}

$params['area_id'] = $this->storage['area_id'];

$transfer = $QTransfer->fetchPagination($page, $limit, $total, $params);

if ($export) {
    $statistics = $QTransfer->getStatistic($params);
    $QTransfer->export($statistics);
}

if ($export_review_cost) {
    $statistics = $QTransfer->getStatisticReviewCost($params);
    $QTransfer->exportReviewCost($statistics);
}

$listContractor = $QContructors->fetchAll();
$listBigArea = $QArea->getBigArea();
$this->view->listBigArea = $listBigArea;
// ---T
$list_sale = $QAppCheckShop->getSaleArea();
$area_list = $QArea->getAreaList($params);
$this->view->area = $area_list;
$this->view->list_sale = $list_sale;
$this->view->listContractor = $listContractor;

$this->view->app_status = $QAppStatus->get_cache(6);
$this->view->params = $params;
$this->view->list = $transfer;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/transfer-list' . ($params ? '?' . http_build_query($params) .
        '&' : '?');
$this->view->offset = $limit * ($page - 1);

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>
