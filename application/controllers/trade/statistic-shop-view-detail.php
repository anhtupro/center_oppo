<?php
$storeId = $this->getRequest()->getParam('store_id');

$QStatisticShop = new Application_Model_StatisticShop();
$QStatisticShopDetail = new Application_Model_StatisticShopDetail();
$QStore = new Application_Model_Store();

$storeInfo = $QStore->fetchRow(['id = ?' => $storeId])->toArray();
$storeDetail = $QStatisticShopDetail->getShopDetail($storeId);


$this->view->storeInfor = $storeInfo;
$this->view->storeDetail = $storeDetail;


