<?php
set_time_limit(0);
ini_set('memory_limit', '300M');

$storeId = $this->getRequest()->getParam('store_id');
$storeName = $this->getRequest()->getParam('store_name');
$areaId = $this->getRequest()->getParam('area_id');
$isUpdated = $this->getRequest()->getParam('is_updated');
$regionalMarket = $this->getRequest()->getParam('regional_market');
$district = $this->getRequest()->getParam('district');
$staff_id	 = $this->getRequest()->getParam('staff_id');
$is_ka           = $this->getRequest()->getParam('is_ka');
$is_ka_details   = $this->getRequest()->getParam('is_ka_details');
$hasCategory = $this->getRequest()->getParam('has_category');
$change_picture_stage = $this->getRequest()->getParam('change_picture_stage');
$exportFacade = $this->getRequest()->getParam('export_facade');
$exportBanner = $this->getRequest()->getParam('export_banner');
$exportInfo = $this->getRequest()->getParam('export_info');
$export_change_picture = $this->getRequest()->getParam('export_change_picture');
$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;


$params = [
    'list_area' => $this->storage['area_id'],
    'store_id' => $storeId,
    'store_name' => $storeName,
    'area_id' => $areaId,
    'area_list' => $this->storage['area_id'],
    'district' => $district,
    'regional_market' => $regionalMarket,
    'is_ka'        => $is_ka,
    'is_ka_details' => $is_ka_details,
    'staff_id'     => $staff_id,
    'is_updated' => $isUpdated,
    'has_category' => $hasCategory,
    'is_updated_facade' => $isUpdated,
    'is_updated_standard_image' => $isUpdated,
    'change_picture_stage' => $change_picture_stage
];

$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QArea = new Application_Model_Area();
$QRegionalMarket = new Application_Model_RegionalMarket();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QChangePictureStage = new Application_Model_ChangePictureStage();
$QPhoneProduct = new Application_Model_PhoneProduct();
if ($exportFacade) {
    $params['facade_type'] = 2;
    $file_name = 'Kích thước mặt tiền ';

    $dataExport = $QAppCheckshopDetailChild->getDataExportFacadeShop($params);
    $QAppCheckshopDetailChild->exportFacadeShop($dataExport, $file_name);
}

if ($exportBanner) {
    $params['facade_type'] = 1;
    $file_name = 'Kích thước Banner ';

    $dataExport = $QAppCheckshopDetailChild->getDataExportFacadeShop($params);
    $QAppCheckshopDetailChild->exportFacadeShop($dataExport, $file_name);
}


if ($exportInfo) {
    ini_set("memory_limit", -1);
    ini_set("display_error", 1);
    error_reporting(~E_ALL);
    $dataExport = $QAppCheckshopDetailChild->getDataExportInfoCategory($params);
    $QAppCheckshopDetailChild->exportInfoCategory($dataExport);
}

if ($export_change_picture && $change_picture_stage) {
    ini_set("memory_limit", -1);
    ini_set("display_error", 1);
    error_reporting(~E_ALL);

    $params['export_change_picture'] = $export_change_picture;
    $dataExport = $QAppCheckshopDetailChild->getDataExportInfoCategory($params);
    $QAppCheckshopDetailChild->exportChangePicture($dataExport);
}



$listStoreIsLock = $QAppCheckshopDetailChild->getShopIsLock($page, $limit,$total, $params);
$listArea = $QArea->getListArea($params);


if ($areaId) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $areaId);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regionalMarket) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regionalMarket);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$list_sale  = $QAppCheckShop->getSaleArea();
$list_channel = $QAppCheckShop->getChannel();

// thay tranh
$list_change_picture_stage = $QChangePictureStage->fetchAll(null, ['id DESC'], 5)->toArray();
$change_picture_stage_valid = $QChangePictureStage->getValidStage();
$list_key_visual = $QPhoneProduct->getProductChangePicture($change_picture_stage_valid['id']);


$this->view->list_change_picture_stage = $list_change_picture_stage;
$this->view->list_sale = $list_sale;
$this->view->list_channel = $list_channel;
$this->view->listStoreIsLock = $listStoreIsLock;
$this->view->listArea = $listArea;
$this->view->params = $params;
$this->view->change_picture_stage_valid = $change_picture_stage_valid;
$this->view->list_key_visual = $list_key_visual;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/update-checkshop-info-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);