<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore            = new Application_Model_Store();
$QRemoveShop       = new Application_Model_RemoveShop();
$QRemoveShopReason = new Application_Model_RemoveShopReason();

$id       = $this->getRequest()->getParam('id');
 
if (!empty($id)) {
  
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        $where          = $QRemoveShop->getAdapter()->quoteInto('id = ?', $id);
        $removeShop_row = $QRemoveShop->fetchRow($where);

        if (($removeShop_row['status'] != 1) || $removeShop_row['created_by'] != $userStorage->id) {
            $flashMessenger->setNamespace('error')->addMessage('Chỉ Xóa được đề xuất trạng thái Pending và chính người đề xuất mới có quyền xóa.!');
            $this->_redirect(HOST . 'trade/remove-shop-list');
        }

        $QRemoveShop->delete($where);

        $info = "remove_shop_del_" . $id . "_" . serialize($data_update);

        // log
        $QLog = new Application_Model_Log();
        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');


        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));

 
        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/remove-shop-list');
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
        $this->_redirect(HOST . 'trade/remove-shop-list');
    }
}

$this->view->params           = $params;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
