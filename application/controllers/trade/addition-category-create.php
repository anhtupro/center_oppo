<?php
$QArea = new Application_Model_Area();
$QCategory = new Application_Model_Category();
$QAdditionCategoryReason = new Application_Model_AdditionCategoryReason();
$params = [
    'area_list' => $this->storage['area_id']
];

$list_area = $QArea->getListArea($params);
$list_category = $QCategory->getCategoryCheckshop($params);
$list_reason = $QAdditionCategoryReason->fetchAll();

$this->view->list_area = $list_area;
$this->view->list_category = $list_category;
$this->view->list_reason = $list_reason;