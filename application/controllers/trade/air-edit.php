<?php

$id = $this->getRequest()->getParam('id');
$category_id = $this->getRequest()->getParam('category_id');
$repair_details_type = $this->getRequest()->getParam('repair_details_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppStatus = new Application_Model_AppStatus();
$QContructors = new Application_Model_Contructors();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QDestruction = new Application_Model_Destruction();
$QAir = new Application_Model_AppAir();
$QAirDetails = new Application_Model_AirDetails();
$QRepairDetails = new Application_Model_RepairDetails();
$QRepairDetailsType = new Application_Model_RepairDetailsType();
$QRepairDetailsTp = new Application_Model_RepairDetailsTp();
$QRepairPart = new Application_Model_RepairPart();
$QRepairType = new Application_Model_RepairType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QMaterial = new Application_Model_Material();
$QAirQuotation = new Application_Model_AirQuotation();

$flashMessenger       = $this->_helper->flashMessenger;

// $data_repair_type = $QRepairType->fetchAll();
$repair_type = [];
foreach($data_repair_type as $key=>$value){
    $repair_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$app_status = $QAppStatus->get_cache(8);
$app_status_title_cache = $QAppStatusTitle->get_cache(8);
$app_status_title = [];
foreach($this->storage['title_list'] as $key=>$value){
    $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);
}

$params = array(
    'id' => $id,
    'air_id' => $id,
    'title' => $this->storage['title']

);

$back_url = HOST.'trade/air-create';

$contructors = $QContructors->fetchAll();
$air_details = $QAirDetails->getAirDetails($params);

// $air_details_type = $QRepairDetailsType->getRepairDetailsType($params);
// $repair_part = $QRepairDetailsType->getRepairDetailsPart($params);

$air = $QAir->getAir($params);

$sellout = $QAppCheckshop->getSellout($air['store_id']);

$params['store_id'] = $air['store_id'];

$app_checkshop = $QAppCheckshop->getInfoCheckshop($params);
$air_material = $QMaterial->fetchAll();
$material = $air_material->toArray();

//quotation
$params['price_type'] = 0;
$result_air_quotation = $QAirQuotation->getAirDetailsQuotation($params);
$air_quotation = $result_air_quotation['air_quotation'];
$total_price_quotation = $result_air_quotation['sum_total_price_quotation'];

$params['price_type'] = 2;
$result_air_quotation_last = $QAirQuotation->getAirDetailsQuotation($params);
$air_quotation_last = $result_air_quotation_last['air_quotation'];
$total_price_quotation_last = $result_air_quotation_last['sum_total_price_quotation'];

$params['price_type'] = 4;
$result_air_quotation_review = $QAirQuotation->getAirDetailsQuotation($params);
$air_quotation_review = $result_air_quotation_review['air_quotation'];
$total_price_quotation_review = $result_air_quotation_review['sum_total_price_quotation'];


$this->view->material = $material; 
$this->view->params = $params;
// $this->view->part = $QRepairPart->fetchAll();
$this->view->category = $result;
$this->view->repair_type = $repair_type;
$this->view->air_details = $air_details;
$this->view->air = $air;
$this->view->sellout = $sellout;
$this->view->app_checkshop = $app_checkshop;
$this->view->app_status = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->contructors = $contructors;
$this->view->repair_part = $repair_part;
$this->view->repair_details_type = $repair_details_type;

$this->view->air_quotation = $air_quotation;
$this->view->total_price_quotation = $total_price_quotation;
$this->view->air_quotation_last = $air_quotation_last;
$this->view->total_price_quotation_last = $total_price_quotation_last;
$this->view->air_quotation_review = $air_quotation_review;
$this->view->total_price_quotation_review = $total_price_quotation_review;

$this->view->staff_id = $userStorage->id;
$this->view->special_user = [15038];

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>