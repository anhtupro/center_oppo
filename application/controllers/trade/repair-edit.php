<?php

$id = $this->getRequest()->getParam('id');
$category_id = $this->getRequest()->getParam('category_id');
$repair_details_type = $this->getRequest()->getParam('repair_details_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppStatus = new Application_Model_AppStatus();
$QContructors = new Application_Model_Contructors();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QDestruction = new Application_Model_Destruction();
$QRepair = new Application_Model_Repair();
$QRepairDetails = new Application_Model_RepairDetails();
$QRepairDetailsType = new Application_Model_RepairDetailsType();
$QRepairDetailsTp = new Application_Model_RepairDetailsTp();
$QRepairPart = new Application_Model_RepairPart();
$QRepairType = new Application_Model_RepairType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QRepairQuotation = new Application_Model_RepairQuotation();

$flashMessenger       = $this->_helper->flashMessenger;

$data_repair_type = $QRepairType->fetchAll();
$repair_type = [];
foreach($data_repair_type as $key=>$value){
    $repair_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$app_status = $QAppStatus->get_cache(4);
$app_status_title_cache = $QAppStatusTitle->get_cache(4);

$app_status_title = [];

foreach($this->storage['title_list'] as $key=>$value){
    $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);
}



$params = array(
    'id' => $id,
    'repair_id' => $id
);

$contructors = $QContructors->fetchAll();
$repair_details = $QRepairDetails->getRepairDetails($params);
$repair_details_type = $QRepairDetailsType->getRepairDetailsType($params);

$repair_details_has_fee = [];
$repair_details_has_part = [];
foreach($repair_details_type as $key=>$value){
    foreach($value as $k=>$v){
        if($v['repair_tp_id'] == 1){
            $repair_details_has_fee[$key] = 1;
        }
        
        if($v['repair_tp_id'] == 3){
            $repair_details_has_part[$key] = 1;
        }
    }
}

$repair_part = $QRepairDetailsType->getRepairDetailsPart($params);

$params['price_type'] = 0;
$result_repair_quotation = $QRepairQuotation->getRepairDetailsQuotation($params);
$repair_quotation = $result_repair_quotation['repair_quotation'];
$total_price_quotation = $result_repair_quotation['sum_total_price_quotation'];

$params['price_type'] = 2;
$result_repair_quotation_last = $QRepairQuotation->getRepairDetailsQuotation($params);
$repair_quotation_last = $result_repair_quotation_last['repair_quotation'];
$total_price_quotation_last = $result_repair_quotation_last['sum_total_price_quotation'];

$params['price_type'] = 4;
$result_repair_quotation_review = $QRepairQuotation->getRepairDetailsQuotation($params);
$repair_quotation_review = $result_repair_quotation_review['repair_quotation'];
$total_price_quotation_review = $result_repair_quotation_review['sum_total_price_quotation'];



$repair = $QRepair->getRepair($params);
$sellout = $QAppCheckshop->getSellout($repair['store_id']);

$params['store_id'] = $repair['store_id'];

$app_checkshop = $QAppCheckshop->getInfoCheckshop($params);

$this->view->params = $params;
$this->view->part = $QRepairPart->getAll();

$this->view->category = $result;
$this->view->repair_type = $repair_type;
$this->view->repair_details = $repair_details;
$this->view->repair = $repair;
$this->view->sellout = $sellout;
$this->view->app_checkshop = $app_checkshop;
$this->view->app_status = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->contructors = $contructors;
$this->view->repair_part = $repair_part;
$this->view->repair_details_type = $repair_details_type;
$this->view->repair_details_has_fee = $repair_details_has_fee;
$this->view->repair_details_has_part = $repair_details_has_part;
$this->view->repair_quotation = $repair_quotation;
$this->view->total_price_quotation = $total_price_quotation;
$this->view->repair_quotation_last = $repair_quotation_last;
$this->view->total_price_quotation_last = $total_price_quotation_last;
$this->view->repair_quotation_review = $repair_quotation_review;
$this->view->total_price_quotation_review = $total_price_quotation_review;

$this->view->staff_id = $userStorage->id;
$this->view->special_user = [15038];

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>