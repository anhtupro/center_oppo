<?php

$season = $this->getRequest()->getParam('season');

$year = $this->getRequest()->getParam('year', date('Y'));
$month = $this->getRequest()->getParam('month');

$fromDate = $this->getRequest()->getParam('from_date');
$fromDate = $fromDate ? date('Y-m-d', strtotime($fromDate)) : date('01-m-Y');

$toDate = $this->getRequest()->getParam('to_date');
$toDate = $toDate ? date('Y-m-d', strtotime($toDate)) : date('t-m-Y');

$params = [
    'area_list' => $this->storage['area_id'],
//    'season' => $season,
//    'month_in_season' => $monthInSeason,
    'year' => $year,
    'month' => $month
//    'from_date' => $fromDate,
//    'to_date' => $toDate
];

if ($season) {
    $monthInSeason = [1, 2, 3];
    $params ['month_in_season'] = $monthInSeason;
    $params ['season'] = $season;
} else {
    $params ['from_date'] = $fromDate;
    $params ['to_date'] = $toDate;
}


$QRepair = new Application_Model_Repair();
$QAppAir = new Application_Model_AppAir();
$QArea = new Application_Model_Area();
$QAdditionalAirCost = new Application_Model_AdditionalAirCost();
$QCategoryAdditional = new Application_Model_CategoryAdditional();
$QAdditionalPosmCost = new Application_Model_AdditionalPosmCost();
$QCampaignArea = new Application_Model_CampaignArea();
$QAdditionalPart = new Application_Model_AdditionalPart();
$QAdditionalPartCost = new Application_Model_AdditionalPartCost();
$QTransfer = new Application_Model_Transfer();
$QDestruction = new Application_Model_Destruction();

//$listAdditionalCategory = $QCategoryAdditional->getAll();
$params['additional_part_type'] = [4,6,8,12];
$listAdditionalPart =   $QAdditionalPart->getAll($params);

$listArea = $QArea->getListArea($params);

//$params ['status_finish'] = 6;
$repairCost = $QRepair->getCost($params);

//$params['status_finish'] = 5;
$airCost = $QAppAir->getCost($params);

//$params['status_finish'] = 6;
$transferCost = $QTransfer->getCost($params);

//$params['status_finish'] = 7;
$destructionCost = $QDestruction->getCost($params);

foreach ($listArea as $area) {
    $partCost [$area['id']] [1] = $airCost [$area['id']] [1];
    $partCost [$area['id']] [2] = $airCost [$area['id']] [2];
    $partCost [$area['id']] [3] = $repairCost [$area['id']] [3];
    $partCost [$area['id']] [4] = $repairCost [$area['id']] [4];
    $partCost [$area['id']] [5] = $repairCost [$area['id']] [5];
    $partCost [$area['id']] [6] = $transferCost [$area['id']];
    $partCost [$area['id']] [7] = $destructionCost [$area['id']];

    $urlPart [$area['id']] [1] = "/trade/air-list?is_ka=0&has_fee=1&area_id_search=" . $area['id'] . "&from_date=" . $params['from_date'] . "&to_date=" . $params['to_date'];
    $urlPart [$area['id']] [2] = "/trade/air-list?is_ka=1&has_fee=1&area_id_search=" . $area['id'] . "&from_date=" . $params['from_date'] . "&to_date=" . $params['to_date'];
    $urlPart [$area['id']] [3] = "/trade/repair-list?has_fee=1&is_ka=0&area_id_search=" . $area['id'] . "&from_date=" . $params['from_date'] . "&to_date=" . $params['to_date'];
    $urlPart [$area['id']] [4] = "/trade/repair-list?has_fee=1&is_ka=1&area_id_search=" . $area['id'] . "&from_date=" . $params['from_date'] . "&to_date=" . $params['to_date'];
    $urlPart [$area['id']] [5] = "/trade/repair-list?has_fee=1&is_brandshop=1&area_id_search=" . $area['id'] . "&from_date=" . $params['from_date'] . "&to_date=" . $params['to_date'];
    $urlPart [$area['id']] [6] = "/trade/transfer-list?has_fee=1&area_id_search=" . $area['id'] . "&from_date=" . $params['from_date'] . "&to_date=" . $params['to_date'];
    $urlPart [$area['id']] [7] = "/trade/destruction-list?has_fee=1&area_id_search=" . $area['id'] . "&from_date=" . $params['from_date'] . "&to_date=" . $params['to_date'];
}

$additionalPartCost = $QAdditionalPartCost->getCost($params);

//get total cost
foreach ($listArea as $area) {

    foreach ($partCost [$area['id']] as $cost) {
        $totalCostArea [$area['id']] += $cost;
    }

    foreach ($additionalPartCost [$area['id']] as $additionalCost) {
        $totalCostArea [$area['id']] += $additionalCost ['cost'];
    }

    $totalCost += $totalCostArea [$area['id']];
}


$this->view->listArea = $listArea;
$this->view->airCost = $airCost;
$this->view->repairCost = $repairCost;
$this->view->partCost = $partCost;
$this->view->additionalCost = $additionalAirCost;
$this->view->additionalPosmCost = $additionalPosmCost;
$this->view->additionalPartCost = $additionalPartCost;
$this->view->posmCost = $posmCost;
//$this->view->listAdditionalCategory = $listAdditionalCategory;
$this->view->listAdditionalPart = $listAdditionalPart;
$this->view->totalCostArea = $totalCostArea;
$this->view->totalCost = $totalCost;
$this->view->season = $season;
$this->view->year = $year;
$this->view->month = $month;
$this->view->params = $params;
$this->view->urlPart = $urlPart;
