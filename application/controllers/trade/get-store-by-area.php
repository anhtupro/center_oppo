<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$areaId = $this->getRequest()->getParam('area_id');

$params = [
    'list_area_id' => $areaId
];

$QStore = new Application_Model_Store();
$listStore = $QStore->getListStore($params);

echo json_encode([
    'status' => 0,
    'listStore' => $listStore
]);
