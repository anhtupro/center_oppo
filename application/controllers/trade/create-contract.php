<?php
   
$contrac_name = $this->getRequest()->getParam('contrac_name');
$contract_type = $this->getRequest()->getParam('contract_type');
$contract_contractor = $this->getRequest()->getParam('contract_contractor');
$details = $this->getRequest()->getParam('details');
$contract_check = $this->getRequest()->getParam('contract_check');
$submit = $this->getRequest()->getParam('submit');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QAirDetails = new Application_Model_AirDetails();
$QTransferDetails = new Application_Model_TransferDetails();
$QRepairDetails     = new Application_Model_RepairDetails;
$QDestructionDetails    = new Application_Model_DestructionDetails;
$QAppContractFile = new Application_Model_AppContractFile();
$QAppContract = new Application_Model_AppContract();
$QContructors = new Application_Model_Contructors();

$flashMessenger       = $this->_helper->flashMessenger;

$params = array_filter(array(
    'staff_id'  => $userStorage->id
));


if (!empty($submit)) {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        // insert into Details
        $contract = [
            'title'  => $contrac_name,
            'type' => $contract_type,
            'contructors_id' => $contract_contractor,
            'status'        => 1,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        
        $contract_id = $QAppContract->insert($contract);

        //file báo giá
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'contract_file' . DIRECTORY_SEPARATOR .  $contract_id;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['contrac_file']['tmp_name'];
                
                $old_name = $_FILES['contrac_file']['name'];
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                
            
                
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'contrac_file' . DIRECTORY_SEPARATOR . $contract_id . DIRECTORY_SEPARATOR . $new_name;

                    $dtf = [
                        'contract_id' => $contract_id,
                        'file_name'   => $new_name,
                        'url'   => '/'.$url,
                        'type' => 1
                    ];
                    $QAppContractFile->insert($dtf);
                }
               

///
        foreach($details as $key=>$value){

            if($contract_check[$value][0]=='on'){
                //thi công
                if($contract_type == 1){
                $where = $QAirDetails->getAdapter()->quoteInto('id = ?', $value);
                $data = [
                    'contract_id'   => $contract_id
                ];
                $QAirDetails->update($data,$where);
                }
                //điều chuyển
                if($contract_type == 2){
                    $where = $QTransferDetails->getAdapter()->quoteInto('id = ?', $value);
                    $data = [
                        'contract_id'   => $contract_id
                    ];
                    $QTransferDetails->update($data,$where);
                }
                //sửa chửa
                if($contract_type == 3){
                    $where = $QRepairDetails->getAdapter()->quoteInto('id = ?', $value);
                    $data = [
                        'contract_id'   => $contract_id
                    ];
                    $QRepairDetails->update($data,$where);
                }
                //tiêu hủy
                if($contract_type == 4){
                    $where = $QDestructionDetails->getAdapter()->quoteInto('id = ?', $value);
                    $data = [
                        'contract_id'   => $contract_id
                    ];
                    $QDestructionDetails->update($data,$where);
                }
            }
            
            
        }
///    
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/edit-contract?id='.$contract_id);
        
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
        $this->_redirect(HOST . 'trade/create-contract');
        
    }
}


$contructors = $QContructors->fetchAll();
$this->view->contructors = $contructors;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>