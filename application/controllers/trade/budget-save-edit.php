<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$budgetId = $this->getRequest()->getParam('budget_id');
$totalBudget = $this->getRequest()->getParam('total_budget');
$detailBudget = $this->getRequest()->getParam('detail_budget');
$year = $this->getRequest()->getParam('year');

$QBudget = new Application_Model_Budget();
$QBudgetDetail = new Application_Model_BudgetDetail();

$params = [
  'year' => $year
];

$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($budgetId) {
    $where = $QBudget->getAdapter()->quoteInto('id = ?', $budgetId);
    $QBudget->update([
        'total_value' => $totalBudget ? $totalBudget : Null
    ], $where);

    foreach ($detailBudget as $type => $budgetValue) {
        $whereDetail = [];
        $whereDetail [] = $QBudgetDetail->getAdapter()->quoteInto('budget_id = ?', $budgetId);
        $whereDetail [] = $QBudgetDetail->getAdapter()->quoteInto('type = ?', $type);

        $QBudgetDetail->update([
            'value' => $budgetValue ? $budgetValue : Null
        ], $whereDetail);
    }

    $listBudget = $QBudget->getListBudget($params);

    $db->commit();

    echo json_encode([
        'status' => 0,
        'listBudget' => $listBudget
    ]);


}

