<?php
$id = $this->getRequest()->getParam('id');

$QAdditionalPart = new Application_Model_AdditionalPart();

if ($id) {
    $additionalPart = $QAdditionalPart->fetchRow(['id = ?' => $id])->toArray();
}

$this->view->additionalPart = $additionalPart;



