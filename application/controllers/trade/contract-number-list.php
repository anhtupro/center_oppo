<?php
$contract_number = $this->getRequest()->getParam('contract_number');
$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;

$params = [
    'contract_number' => TRIM($contract_number)
];

$QContractNumber = new Application_Model_ContractNumber();

$list_contract = $QContractNumber->fetchPagination($page, $limit, $total, $params);
$list_air_detail = $QContractNumber->getAirContractTotal($params);
$list_repair_detail = $QContractNumber->getRepairContractTotal($params);
$list_transfer_detail = $QContractNumber->getTransferContractTotal($params);
$list_destruction_detail = $QContractNumber->getDestructionContractTotal($params);


$this->view->list_contract = $list_contract;
$this->view->list_air_detail = $list_air_detail;
$this->view->list_repair_detail = $list_repair_detail;
$this->view->list_transfer_detail = $list_transfer_detail;
$this->view->list_destruction_detail = $list_destruction_detail;
$this->view->params = $params;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'trade/contract-number-list' . ($params ? '?' . http_build_query($params) . '&' : '?');


