<?php 

	$campaign_id    = $this->getRequest()->getParam('campaign_id');
	$quantity_order = $this->getRequest()->getParam('quantity_order');
	$category_id    = $this->getRequest()->getParam('category_id');
	$area_id    	= $this->getRequest()->getParam('area_id');
	$price    		= $this->getRequest()->getParam('price');
	$submit     	= $this->getRequest()->getParam('submit');


	$QCampaign 	   	= new Application_Model_Campaign();
	$QArea 		   	= new Application_Model_Area();
	$QCampaignArea 	= new Application_Model_CampaignArea();

	$area        = $QArea->get_cache_bi();
	if(!empty($this->storage['area_id'])){
		$area = [];
		foreach($QArea->get_cache_bi() as $key=>$value){
			if(in_array($key, $this->storage['area_id'])){
				$area[$key] = $value;
			}
		}
	}

	$params = [
		'campaign_id' => $campaign_id,
		'area_id'	  => $this->storage['area_id']
	];


if($_GET['dev'] == 1){
            echo "<pre>";
            var_dump($params);
            exit;
        }

$area_status = $QCampaignArea->get_status_area($params);


	$max_status  = $QCampaignArea->get_status_max($params);
        $min_status  = $QCampaignArea->get_status_min($params);


$where 		   = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign 	   = $QCampaign->fetchRow($where);

	$category      		= $QCampaign->getCategory($params);
	$category_campaign 	= $QCampaign->getCategoryCampaign($params);


$where 					= $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
	$campaign_area_check 	= $QCampaignArea->fetchAll($where);

	
//	if(count($campaign_area_check) == 0){
//
//		$campaign_area_insert = $QCampaignArea->getCampaignAreaInsert($params);
//		foreach($campaign_area_insert as $key=>$value){
//			$QCampaignArea->insert(
//				[
//					'campaign_id'  	 => $value['campaign_id'],
//					'area_id' 	  	 => $value['area_id'],
//					'category_id' 	 => $value['category_id'],
//					'price' 		 => $value['price'],
//					'status' 		 => 1,
//				]
//			);
//		}
//	}



//Get data campaign_area
	$campaign_area = $QCampaignArea->getCampaignArea($params);
	$data = [];
	foreach($campaign_area as $key=>$value){
		$data[$value['area_id']][$value['category_id']] = $value;
	}

//END Get data

	
	//Get data campaign_area
	$contractor_category = $QCampaignArea->getContractorCategory($params);
	$data_price = [];
	foreach($contractor_category as $key=>$value){
		$data_price[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data


	if(!empty($submit) AND $submit == 2){

		require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;


        //Phần đầu
        $heads[] = '';
        $heads[] = '';

        foreach($category_campaign as $key=>$value){
        	$heads[] = '';
        	$heads[] = $value['category_id'];
        }

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $alpha = 'A';
        $index = 2;
        //END Phần đầu

        //Phần đầu 2
        $sheet->setCellValue($alpha . $index, 'ID Khu vực');
        $alpha++;
        $sheet->setCellValue($alpha . $index, 'Khu vực');
        $alpha++;

        foreach($category_campaign as $key=>$value){
        	$col_1 = $alpha . $index;

        	$sheet->setCellValue($alpha . $index, $value['name']);
        	$alpha++;

        	$col_2 = $alpha . $index;

        	$sheet->setCellValue($alpha . $index, $value['name']);
        	$alpha++;

        	$sheet->mergeCells($col_1.':'.$col_2);
        }

        $index = 3;
        //END Phần đầu 2

        //KHU VỰC
        foreach($area as $key=>$value){
        	$alpha = 'A';
        	$sheet->getCell($alpha++ . $index)->setValueExplicit($key ,PHPExcel_Cell_DataType::TYPE_STRING);
        	$sheet->getCell($alpha++ . $index)->setValueExplicit($value ,PHPExcel_Cell_DataType::TYPE_STRING);
        	
        	foreach($category_campaign as $k=>$v){
        		$quantity_limit = !empty($data[$key][$v['category_id']]['quantity_limit']) ? $data[$key][$v['category_id']]['quantity_limit'] : 0;
        		$quantity = !empty($data[$key][$v['category_id']]['quantity']) ? $data[$key][$v['category_id']]['quantity'] : 0;
	        	$sheet->getCell($alpha++ . $index)->setValueExplicit($quantity_limit ,PHPExcel_Cell_DataType::TYPE_STRING);
	        	$sheet->getCell($alpha++ . $index)->setValueExplicit($quantity ,PHPExcel_Cell_DataType::TYPE_STRING);
	        }
	        $index++;
        }
        //END Khu vực


        $filename = ' Campaign-Limit ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
	}
 
	if(!empty($submit) AND $submit == 1){

        if($_FILES["file"]["tmp_name"] != ""){

			include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';

			error_reporting(0);
			set_time_limit(0);
			
			//===================Main=======================

			if($_FILES["file"]["tmp_name"] != ""){
				if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
					//echo ("Không phải file XLSX !");exit;
				}
				if ($_FILES["file"]["error"] > 0)
			    {
			    	echo ("Return Code: " . $_FILES["file"]["error"] . "<br />");exit;
			    }
				move_uploaded_file($_FILES["file"]["tmp_name"], "files/limit_campaign/data.xlsx");
			}

			$inputfile = 'files/limit_campaign/data.xlsx';
			$xlsx = new SimpleXLSX($inputfile);
			$data = $xlsx->rows();

			//Đếm số cột
			$i = 3;
			$num_cols = count($category_campaign)*2+2;

			//Đếm số dòng
			$i = 1;
			$num_rows = 0;
			while ($data[$i++][0] <> '') {
			  $num_rows++;
			}

			$sql = "";

			for($i = 2; $i <= $num_rows; $i++){

				$area_id = $data[$i][0];

				for($j = 2; $j <= $num_cols; $j++){

					$cat_id 	= $data[0][$j];

					//echo $campaign_id.'/'.$area_id.'/'.$cat_id.'/'.(int)$data[$i][$j].'<br/>';
					
					if(!empty($cat_id) AND !empty($area_id) and in_array($area_id, $params['area_id'])){

						//echo $campaign_id.'/'.$area_id.'/'.$cat_id.'/'.(int)$data[$i][$j].'<br/>';

						$sql .= "UPDATE `".DATABASE_TRADE."`.`campaign_area` SET quantity=".(int)$data[$i][$j]." WHERE campaign_id=".$campaign_id." AND area_id=".$area_id." AND category_id=".$cat_id." LIMIT 1;";
					}

				}
				
			}
			//echo $sql;exit;
			

			if(!empty($sql)){
	            $db = Zend_Registry::get('db');
				$stmt = $db->prepare($sql);
	            $stmt->execute();
	            $stmt->closeCursor();
        	}

			unlink($inputfile);


			//=================Function Area===================
		    $this->_helper->layout->disableLayout();
		    $back_url = HOST.'trade/order-campaign?campaign_id='.$campaign_id;
		    $this->redirect($back_url);
		}
		else{
      
            $sql = "";

			foreach($quantity_order as $key=>$value){

				foreach($value as $k=>$v){

					$sql .= "UPDATE `".DATABASE_TRADE."`.`campaign_area` SET quantity=".$v." WHERE campaign_id=".$campaign_id." AND area_id=".$key." AND category_id=".$k." LIMIT 1;";
				}

			}

			$sql 		= rtrim($sql, ',');
			if(!empty($sql)){
	            $db = Zend_Registry::get('db');
				$stmt = $db->prepare($sql);
	            $stmt->execute();
	            $stmt->closeCursor();
        	}

			$back_url = HOST.'trade/order-campaign?campaign_id='.$campaign_id;
		    $this->redirect($back_url);
		}
	}


$this->view->area     			= $area;
	$this->view->campaign 			= $campaign;
	$this->view->category 		 	= $category;
	$this->view->category_campaign  = $category_campaign;
	$this->view->data     			= $data;
	$this->view->data_price 		= $data_price;
	$this->view->area_status        = $area_status;
	$this->view->max_status         = $max_status;
        $this->view->min_status         = $min_status;
        
