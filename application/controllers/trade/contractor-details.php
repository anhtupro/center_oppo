<?php 
	
	$campaign_id = $this->getRequest()->getParam('campaign_id');

	$QAppContractorQuantity = new Application_Model_AppContractorQuantity();
	$QCampaign = new Application_Model_Campaign();

	$where = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign = $QCampaign->fetchRow($where);

	$params = [
		'staff_id'    	=> $this->storage['staff_id'],
		'title'		  	=> $this->storage['title'],
		'campaign_id'	=> $campaign_id
	];

	$contractor = $QAppContractorQuantity->getContractor($params);

	$params['contractor_id'] = $contractor['id'];

	$campaign_category = $QAppContractorQuantity->getCampaignCategory($params);
	

	$this->view->contractor        = $contractor;
	$this->view->campaign          = $campaign;
	$this->view->campaign_category = $campaign_category;
	



