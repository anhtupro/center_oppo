<?php 
	
	$data_order 	= $this->getRequest()->getParam('data_order');
	$order_id 		= $this->getRequest()->getParam('order_id');
	$campaign_id 	= $this->getRequest()->getParam('campaign_id');
	$note 			= $this->getRequest()->getParam('note');

	$data_order = json_decode($data_order, true);

	$QArea 				= new Application_Model_Area();
	$QCampaign 			= new Application_Model_Campaign();
	$QAppOrderDetails 	= new Application_Model_AppOrderDetails();
	$QAsm 	     		= new Application_Model_Asm();
	$QAppOrderConfirm 	= new Application_Model_AppOrderConfirm();
	$QAppStatusTitle  	= new Application_Model_AppStatusTitle();

	$area = $QArea->get_cache();
	$get_status_campaign = $QCampaign->getStatusCampaign();

	$area_id = !empty($this->storage['area_id']) ? $this->storage['area_id'] : NULL;

	$flashMessenger = $this->_helper->flashMessenger;

    $params = [
		'campaign_id' => $campaign_id,
		'staff_id'    => $this->storage['staff_id'],
		'area_id'	  => $area_id
	];

	$app_status_title = $QAppStatusTitle->get_cache(TU_BAN_BUCGOC);
	$status_title = $app_status_title[$this->storage['title']];

	$back_url = HOST.'trade/confirm-order?campaign_id='.$campaign_id;

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		$order_details = $QAppOrderDetails->getDataDetails($params);

		$status = 0;

		foreach($order_details as $key=>$value){

			if($status != 0 AND $status !=  $value['status']){
				$flashMessenger->setNamespace('error')->addMessage('Khu vực '.$area[$value['area_id']].' : '.$get_status_campaign[$value['status']].'!');
    			$this->redirect($back_url);
			}

			$status = $value['status'];

			if( in_array($value['status'], $status_title) ){
				$where = $QAppOrderDetails->getAdapter()->quoteInto('id = ?', $value['order_details_id']);
				$data = [
					'status'	=> ($value['status'] + 1)
				];

				$QAppOrderDetails->update($data,$where);

				$log_confirm = [
					'staff_id'			=> $this->storage['staff_id'],
					'order_details_id'	=> $value['id'],
					'confirmed_at'		=> date('Y-m-d H:i:s'),
					'status'			=> ($value['status'] + 1),
				];

				$QAppOrderConfirm->insert($log_confirm);
			}
		}

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Duyệt thành công!');
    	$this->redirect($back_url);


	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;
	




