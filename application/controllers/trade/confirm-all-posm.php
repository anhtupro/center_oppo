<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$campaign_id = $this->getRequest()->getParam('campaign_id');
$status = $this->getRequest()->getParam('status');

$QCampaignArea = new Application_Model_CampaignArea();

$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($status == 3) {
    $where = [
      'status = ?' => 3,
      'campaign_id = ?' => $campaign_id
    ];

    $QCampaignArea->update([
       'status' => 4
    ], $where);
}


if ($status == 4) {
    $where = [
        'status = ?' => 4,
        'campaign_id = ?' => $campaign_id
    ];

    $QCampaignArea->update([
        'status' => 5
    ], $where);
}

$db->commit();

echo json_encode([
   'status' => 0
]);
