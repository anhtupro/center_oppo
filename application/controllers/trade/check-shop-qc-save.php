<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$QCheckShopQcResult = new Application_Model_CheckShopQcResult();
$QAreaStaff = new Application_Model_AreaStaff();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$type = intval($this->getRequest()->getParam('type')); // 1: tmk local, 2: tmk leader
$qc_id = intval($this->getRequest()->getParam('qc_id'));
$note = My_Util::escape_string($this->getRequest()->getParam('note'));
$month = intval($this->getRequest()->getParam('month'));
$year = intval($this->getRequest()->getParam('year'));
$store_id = intval($this->getRequest()->getParam('store_id'));
$quarter = intval($this->getRequest()->getParam('quarter'));

if (!$type || !$qc_id || !$month || !$year || !$store_id || !$quarter) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không đủ thông tin'
    ]);
    return;
}

// check store thuộc khu vực quản lý
$legal_store = $QAreaStaff->checkLegalStore($store_id, $userStorage->id);

if (! $legal_store) {
    echo json_encode([
        'status' => 1,
        'message' => 'Store không thuộc khu vực bạn quản lý !'
    ]);
    return;
}


try {
    if ($type == 2) {
        $qc_tmk_local = $QCheckShopQcResult->getQc($month, $year, $store_id, 1);
        if (!$qc_tmk_local) {
            echo json_encode([
                'status' => 1,
                'message' => 'TMK Local phải chấm trước!'
            ]);
            return;
        }

        if (! in_array($userStorage->title, [TRADE_MARKETING_LEADER, TRADE_MARKETING_LEADER_HO])) {
            echo json_encode([
                'status' => 1,
                'message' => 'Chỉ TMK Leader mới có quyền chấm loại này'
            ]);
            return;
        }
    }

    // check qc đã được chấm
    $exist_qc = $QCheckShopQcResult->getQc($month, $year, $store_id, $type);
    if ($exist_qc) {
        echo json_encode([
            'status' => 1,
            'message' => 'Đã đánh giá loại này rồi. Vui lòng chọn loại khác !'
        ]);
        return;
    }

    $QCheckShopQcResult->insert([
        'store_id' => $store_id,
        'month' => $month,
        'year' => $year,
        'type' => $type,
        'note' => $note ? $note : NULL,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
        'qc_id' => $qc_id,
        'quarter' => $quarter
    ]);

} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}


echo json_encode([
    'status' => 0
]);
