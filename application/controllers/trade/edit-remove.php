<?php 
$id   = $this->getRequest()->getParam('id');
$staff_id   = $this->getRequest()->getParam('staff_id');
$category   = $this->getRequest()->getParam('category');
$store   = $this->getRequest()->getParam('store');
$sl   = $this->getRequest()->getParam('sl');
$lydo   = $this->getRequest()->getParam('lydo');
$submit   = $this->getRequest()->getParam('submit');
$delete   = $this->getRequest()->getParam('delete');
$backlink   = $this->getRequest()->getParam('backlink');
$destruction_id   = $this->getRequest()->getParam('iddestruction');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QDestructionFile = new Application_Model_DestructionFile();
$QDestruction = new Application_Model_Destruction();
$flashMessenger       = $this->_helper->flashMessenger;
	// lay row record theo 

if (!empty($id))
{
	$result = $QDestruction->GetList($id);
}
	//kiem tra quyen chinh sua
if($result['0']['staff_id'] == $userStorage->id || $staff_id == $userStorage->id){
		// lay bien hien thi ra man hinh
	$params = array_filter(array(
		'staff_id'=>$userStorage->id,
		'id'=>$id,
		'category' => $result[0]['category_id'],
		'store'=> $result[0]['store_id'],
		'sl'=> $result[0]['sl'],
		'lydo'=> $result[0]['lydo']
	));
	$this->view->params = $params;
	if($submit == 1){//----------------------XOA HINH ANH -------------------
		$temp =0;
		$filename_temp="";
		$file_id   = $this->getRequest()->getParam('file_id');
		$params['destruction_id']=$destruction_id;
		$dataAll = $QDestructionFile->GetList($params);
		$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
				DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
				DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . $destruction_id;
		foreach ($dataAll as $key => $value) {
			$temp++;
			if ($value['id'] == $file_id ) {
				$filename_temp=$value['file_name'];
			}
		}
		if($temp >1){// KIEM TRA SO LUONG FILE ANH
			unlink($uploaded_dir.DIRECTORY_SEPARATOR.$value['file_name']);
			$whereDestructionFile = $QDestructionFile->getAdapter()->quoteInto('id = ?', $file_id);
			$QDestructionFile->delete($whereDestructionFile);
			$flashMessenger->setNamespace('success')->addMessage('Thông báo xóa hình ảnh thành công');
			$this->_redirect(HOST . 'trade/edit-remove?id='.$destruction_id);
		}else{
			$flashMessenger->setNamespace('error')->addMessage('Thông báo phải có trên 2 hình ảnh mới được xóa');
			$this->_redirect(HOST . 'trade/edit-remove?id='.$destruction_id);
		}
		
	}
		// event submit button
	if($submit =="Cập nhật" ){
		$data = array_filter(
			array(
				'category_id'=>$category,
				'store_id'=>$store,
				'sl'=>$sl ,
				'lydo'=>$lydo
			));
		$result =$QDestruction->update ($data,'id = '.$destruction_id);

		try {
			$data = array_filter(
				array(
					'category_id'=>$category,
					'store_id'=>$store,
					'sl'=>$sl ,
					'lydo'=>$lydo
				));
			$result =$QDestruction->update ($data,'id = '.$destruction_id);
			$flashMessenger->setNamespace('success')->addMessage('Thông báo cập nhật thành công');

			//kiem tra xem co up file chua?
			$upload = new Zend_File_Transfer();
			$upload->setOptions(array('ignoreNoFile'=>true));
            //check function
			if (function_exists('finfo_file'))
				$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif','application/vnd.ms-powerpoint'));

			$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
			$upload->addValidator('Size', false, array('max' => '2MB'));
			$upload->addValidator('ExcludeExtension', false, 'php,sh');
			$files = $upload->getFileInfo();
			if($files['image']['error']==0){

				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
				DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
				DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . $destruction_id;

				$fileInfo = (isset($files['image']) and $files['image']) ? $files['image'] : null;
				if (!is_dir($uploaded_dir))
					@mkdir($uploaded_dir, 0777, true);
				$upload->setDestination($uploaded_dir);

            //Rename
				$old_name = $fileInfo['name'];
				$tExplode = explode('.', $old_name);
				$extension = end($tExplode);
				$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
				$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
				$r = $upload->receive(array('image'));
				if($r){
					$data_file['file_name'] = $new_name;
				}
				else{
					$messages = $upload->getMessages();
					foreach ($messages as $msg)
						throw new Exception($msg);
				}
	// ----------insert hinh anh len database -----------
				$data_file['destruction_id'] = $destruction_id;
				$result =$QDestructionFile->insert($data_file);
	// --------lay hinh anh vua upload  moi dc tai len---------
				$params['id']=$result;
				$dataAll = $QDestructionFile->GetList($params);
				$this->view->dataAll = $dataAll;
			}
			$this->_redirect(HOST . 'trade/edit-remove?id='.$destruction_id);
		} catch (Exception $e) {
			$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
		}

	}

	    //---------------- event button delete ---------------
	if(!empty($delete)){
		$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
		DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
		DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . $destruction_id;
		try {
			$where = $QDestruction->getAdapter()->quoteInto('id = ?', $destruction_id);
			$QDestruction->delete($where);
			//----------------lay danh luu tru trong thu -----------
			$params['destruction_id'] =$destruction_id;
			$dataAll = $QDestructionFile->GetList($params);
			// ---------- Xoa file trong folder--------------
			foreach ($dataAll as $key => $value) {
				unlink($uploaded_dir.DIRECTORY_SEPARATOR.$value['file_name']);
			}
			$rmdir =rmdir ($uploaded_dir);
			// -------- Xoa tren database --------------
			$where = $QDestructionFile->getAdapter()->quoteInto('destruction_id = ?', $destruction_id);
			$QDestructionFile->delete($where);
			$flashMessenger->setNamespace('success')->addMessage('Thông báo xóa thành công');
			$this->_redirect(HOST . 'trade/list-remove');

		} catch (Exception $e) {
			$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
			$this->_redirect(HOST . 'trade/edit-remove?id='.$destruction_id);
		}
	}
	$params['destruction_id'] =$id;

	$dataAll = $QDestructionFile->GetList($params);
	$this->view->dataAll = $dataAll;
	// print_r($dataAll);
	// exit;
		//----------lay danh sach hang muc-------------
	$QCategory = new Application_Model_Category();
	$result = $QCategory->getall();
	$this->view->category = $result;
		//----------lay danh sach cua hang-----------
	$QStore = new Application_Model_Store();
	$result = $QStore->getall();
	$this->view->store = $result;

}else{
	$flashMessenger->setNamespace('error')->addMessage("Không có quyền chỉnh sửa");
	$this->_redirect(HOST . 'trade/list-remove');
}
// ------------ hien thi thong  bao-----------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}
?>