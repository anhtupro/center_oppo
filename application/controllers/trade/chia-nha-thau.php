<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QAppAir = new Application_Model_AppAir();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QAppAirFile= new Application_Model_AppAirFile();
$title = $userStorage->title;
$id = $this->getRequest()->getParam('id');
$flashMessenger       = $this->_helper->flashMessenger;
$params = array(
	'id' => $id,
	'title'=>$title
);
//lấy ds nhà thầu
$contructor=$QAppAir->getContructor();
$this->view->contructor=$contructor;

// lấy status
 $status=$QAppAir->getStatus($id);
 $this->view->status=$status;

$list=$QAppAir->getDSDeXuat($params);
//var_dump($list); exit;
$this->view->list=$list;
//lấy thông tin shop đề xuất
$shop=$QAppAir->getShopDeXuat($id);
$this->view->shop=$shop;
//lấy ảnh design
 $img=$QAppAirFile->getImg($id);
 $this->view->img=$img;
$this->view->params = $params;
//lấy thông tin chia nhà thầu
$nhathau=$QAppAir->getInfor($params);
//var_dump($nhathau); exit;
$data_nhathau=[];
foreach ($nhathau as $key => $value) {
	$data_nhathau[$value['air_detail_id']][]=$value;
}

$this->view->data_nhathau = $data_nhathau;
//var_dump($data_nhathau); exit;

	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}