<?php
$roundId = $this->getRequest()->getParam('round_id');
$storeId = $this->getRequest()->getParam('store_id');

$QAppCheckshop = new Application_Model_AppCheckshop();
$QCategory = new Application_Model_Category();
$QErrorImage = new Application_Model_ErrorImage();
$RecheckShopRound = new Application_Model_RecheckShopRound();

$recheck_shop = $RecheckShopRound->fetchRow(['id = ?' => $roundId]);
//$latestCheckShop = $QAppCheckshop->getLatestCheckShop($storeId); // tạm đóng sẽ mở lại sau khi hết đợt 'check 10H'
$latestCheckShop = $QAppCheckshop->getLatestCheckShopTest($storeId);

// get detail checkshop
if ($latestCheckShop['id']) {
    // image check shop
    $img_check = $QAppCheckshop->getImg($latestCheckShop['id']);
    $img_different = $QAppCheckshop->getImgDifferent($latestCheckShop['id']);

    $img_check_list = $QAppCheckshop->getListImg($latestCheckShop['created_at'], $storeId, $latestCheckShop['id']);
    $img_different_list = $QAppCheckshop->getImgDifferentList($latestCheckShop['created_at'], $storeId, $latestCheckShop['id']);


    $listCheckShopDetail = $QAppCheckshop->getCheckshopDetails($latestCheckShop['id']);
    // lấy ra id những category đã checkshop
    foreach ($listCheckShopDetail as $detail) {
        $currentCategoryId [] = $detail['category_id'];
    }
}



$listCategory = $QCategory->getCategoryCheckshop();

// lấy ra danh sách category trừ nhưng category đã checkshop
foreach ($listCategory as $key => $category) {
    if (in_array($category['id'], $currentCategoryId)) {
        unset($listCategory[$key]);
    }
}

$listErrorImage = $QErrorImage->fetchAll(['is_deleted IS NULL'])->toArray();

$sellout = $QAppCheckshop->getSellout($storeId);

$this->view->listCheckShopDetail = $listCheckShopDetail;
$this->view->listCategory = $listCategory;
$this->view->latestCheckShop = $latestCheckShop;
$this->view->sellout = $sellout;
$this->view->roundId = $roundId;
$this->view->storeId = $storeId;
$this->view->img_check = $img_check;
$this->view->img_different = $img_different;
$this->view->img_check_list = $img_check_list;
$this->view->img_different_list = $img_different_list;

$this->view->listErrorImage = $listErrorImage;
$this->view->recheck_shop = $recheck_shop;



