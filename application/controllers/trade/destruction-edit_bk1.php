<?php

$id = $this->getRequest()->getParam('id');
$category_id = $this->getRequest()->getParam('category_id');
$destruction_details_type = $this->getRequest()->getParam('destruction_details_type');
$destruction_details_tp = $this->getRequest()->getParam('destruction_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppStatus = new Application_Model_AppStatus();
$QContructors = new Application_Model_Contructors();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QDestruction = new Application_Model_Destruction();
$QDestruction = new Application_Model_Destruction();
$QDestructionDetails = new Application_Model_DestructionDetails();
$QDestructionDetailsType = new Application_Model_DestructionDetailsType();
$QDestructionDetailsTp = new Application_Model_DestructionDetailsTp();
$QDestructionPart = new Application_Model_DestructionPart();
$QDestructionType = new Application_Model_DestructionType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QDestructionQuotation = new Application_Model_DestructionQuotation();

$flashMessenger       = $this->_helper->flashMessenger;

$data_destruction_type = $QDestructionType->fetchAll();
$destruction_type = [];
foreach($data_destruction_type as $key=>$value){
    $destruction_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$app_status = $QAppStatus->get_cache(12);
$app_status_title_cache = $QAppStatusTitle->get_cache(12);

$app_status_title = [];
foreach($this->storage['title_list'] as $key=>$value){
    $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);
}

$params = array(
    'id' => $id,
    'destruction_id' => $id
);

$contructors = $QContructors->fetchAll();
$destruction_details = $QDestructionDetails->getDestructionDetails($params);
// var_dump($destruction_details); exit;
$destruction_part = $QDestructionDetailsType->getDestructionDetailsPart($params);
$result_destruction_quotation_last = $QDestructionQuotation->getDestructionDetailsQuotationLast($params);
$destruction_quotation_last = $result_destruction_quotation_last['destruction_quotation'];
$total_price_quotation_last = $result_destruction_quotation_last['sum_total_price_quotation'];

$result_destruction_quotation = $QDestructionQuotation->getDestructionDetailsQuotation($params);
$destruction_quotation = $result_destruction_quotation['destruction_quotation'];
$total_price_quotation = $result_destruction_quotation['sum_total_price_quotation'];

$destruction = $QDestruction->getDestruction($params);
$sellout = $QAppCheckshop->getSellout($destruction['store_id']);

$params['store_id'] = $destruction['store_id'];

$app_checkshop = $QAppCheckshop->getInfoCheckshop($params);

$check_destruction_tp = $QDestructionDetails->getDestructionDetailsTp($params);

$check_tp = 0;
foreach($check_destruction_tp as $key=>$value){
    if($value['tp_id']==1)
    	$check_tp = 1;
}



$this->view->check_tp = $check_tp;
$this->view->params = $params;
$this->view->part = $QDestructionPart->fetchAll();
$this->view->category = $result;
$this->view->destruction_type = $destruction_type;
$this->view->destruction_details = $destruction_details;
$this->view->destruction = $destruction;
$this->view->sellout = $sellout;
$this->view->app_checkshop = $app_checkshop;
$this->view->app_status = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->contructors = $contructors;
$this->view->destruction_part = $destruction_part;
$this->view->destruction_quotation = $destruction_quotation;
$this->view->total_price_quotation = $total_price_quotation;
$this->view->destruction_quotation_last = $destruction_quotation_last;
$this->view->total_price_quotation_last = $total_price_quotation_last;

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>