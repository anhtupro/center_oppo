<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$season = $this->getRequest()->getParam('season');
$year = $this->getRequest()->getParam('year');
$areaId = $this->getRequest()->getParam('area_id');
$airIaCost = $this->getRequest()->getParam('air_ia_cost');
$repairIaCost = $this->getRequest()->getParam('repair_ia_cost');
$repairKaCost = $this->getRequest()->getParam('repair_ka_cost');
$repairBrandshopCost = $this->getRequest()->getParam('repair_brandshop_cost');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$params = [
    'season' => $season,
    'year' => $year
];

$QAdditionalAirCost = new Application_Model_AdditionalAirCost();

    // check error
    if (! $areaId || ! $season || ! $year) {
        echo json_encode([
            'status' => 1,
            'message' => 'Vui lòng chọn quí, năm, và khu vực!'
        ]);
        return;
    }

    // check if season and area is existed already
    $isExisted = $QAdditionalAirCost->fetchRow([
        'season = ?' => $season,
        'area_id = ?' => $areaId
    ]);
    if ($isExisted) {
        echo json_encode([
            'status' => 1,
            'message' => 'Khu vực này quí ' . $season . ' đã được tạo rồi'
        ]);
        return;
    }
    // end check error

    //save
    $QAdditionalAirCost->insert([
        'area_id' => $areaId,
        'air_ia_cost' => $airIaCost ? $airIaCost : Null,
        'repair_ia_cost' => $repairIaCost ? $repairIaCost : Null,
        'repair_ka_cost' => $repairKaCost ? $repairKaCost : Null,
        'repair_brandshop_cost' => $repairBrandshopCost ? $repairBrandshopCost : Null,
        'season' => $season,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
        'year' => $year
    ]);

$listAdditionalCost = $QAdditionalAirCost->getCost($params);

echo json_encode([
   'status' => 0,
   'listAdditionalCost' => $listAdditionalCost
]);
