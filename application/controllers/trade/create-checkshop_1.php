<?php

// check device
//$userAgentString = $_SERVER['HTTP_USER_AGENT'];
//if (strpos($userAgentString, 'Chrome')) echo 'Chrome';
//if (strpos($userAgentString, 'Firefox')) echo 'Firefox';
// end check device

$store_id = $this->getRequest()->getParam('id');
$dev = $this->getRequest()->getParam('dev');

$QBiTrade = new Application_Model_BiTrade();
$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QCheckshop = new Application_Model_AppCheckshop();
$QCheckshopDetailBrand = new Application_Model_AppCheckshopDetailBrand();
$QBrand = new Application_Model_BrandTrade();
$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QCheckShopEntireErrorImage = new Application_Model_CheckShopEntireErrorImage();
$QCheckShopEntireFile = new Application_Model_CheckShopEntireFile();
$QChangePictureStage = new Application_Model_ChangePictureStage();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();

$where = $QStore->getAdapter()->quoteInto('id = ?', $store_id);
$store = $QStore->fetchRow($where);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$investments = $QBiTrade->getCheckshopByStore($store_id);
$checkshop_last = $QCheckshop->getLastCheckshop($store_id);

$params = [
    'store_id' => $store_id
];

$flashMessenger = $this->_helper->flashMessenger;
// /var_dump($investments); exit;

$params = [];

foreach ($investments as $key => $value) {
    $params['investments_cat'][] = $value['category_id'];
}

//Nếu có lock
$investments_lock = $QCheckshop->getCheckshopLock($store_id);


if (!empty($investments_lock)) {
    $investments = $investments_lock;
}

$checkshop = [];
foreach ($checkshop_last as $key => $value) {
    $checkshop[$value['category_id']][] = $value;
}

$ua = strtolower($_SERVER['HTTP_USER_AGENT']);


if (stripos($ua, 'android') !== false) { // && stripos($ua,'mobile') !== false) {
    preg_match('/Android (\d+(?:\.\d+)+)[;)]/', $_SERVER['HTTP_USER_AGENT'], $matches);
    $this->view->android_version = (int)$matches[1];
}

$checkshopDetailBrand = $QCheckshopDetailBrand->getDetail($store_id);
$listCategoryBrand = $QCategory->getCategoryBrand();
$listBrand = $QBrand->fetchAll(['is_checkshop = ?' => 1], ['ordering_sale ASC'])->toArray();


$category_checkshop = $QCategory->getCategoryCheckshop($params);

$store_error_info = $QCheckShopEntire->getStoreErrorInfo($store_id);

if ($store_error_info['error_image'] == 1 && $store_error_info['check_shop_entire_id']) {
    $list_error = $QCheckShopEntireErrorImage->getError($store_error_info['check_shop_entire_id']);
    $list_image_error = $QCheckShopEntireFile->get($store_error_info['check_shop_entire_id'], 2);
}


// thay tranh

$check_shop_id_lock = $QCheckshop->getCheckShopIdLock($store_id);

$change_picture_stage = $QChangePictureStage->getStageForSale();
$params['change_picture_stage_id'] = $change_picture_stage['id']; // chỉ lấy dữ liệu của đơt gần nhất


if ($change_picture_stage) {
    $store_is_valid_change_picture = $QChangePictureStage->checkStoreValid($store_id, $change_picture_stage['id']);
}

if ($check_shop_id_lock) {
    $params['only_picture'] = 1; // chỉ lấy những HM có tranh
    $checkshopDetailChild = $QAppCheckshopDetailChild->getDetailChild($check_shop_id_lock);
    $checkshopDetailParent = $QAppCheckshopDetailChild->getDetailParent($check_shop_id_lock, $params);
  
    $params['checkshop_id'] = $check_shop_id_lock;
    $listCategoryPainting = $QAppCheckshopDetailChildPainting->getPainting($params);

    // lịch sử thay tranh
    $history_change_picture = $QChangePictureStage->getHistoryChangePicture($check_shop_id_lock, $params);
    $history_change_picture_child = $QChangePictureStage->getHistoryChangePictureChild($check_shop_id_lock, $params);
}




// end thay tranh

$this->view->dev = $dev;
$this->view->investments = $investments;
$this->view->investments_lock = $investments_lock;
$this->view->store = $store;
$this->view->checkshopDetailBrand = $checkshopDetailBrand;
$this->view->listCategoryBrand = $listCategoryBrand;
$this->view->listBrand = $listBrand;
//$this->view->checkshop_last = $checkshop_last;
$this->view->checkshop = $checkshop;
$this->view->category = $category_checkshop;

$this->view->store_error_info = $store_error_info;
$this->view->list_error = $list_error;
$this->view->list_image_error = $list_image_error;

$this->view->change_picture_stage = $change_picture_stage;
$this->view->checkshopDetailChild = $checkshopDetailChild;
$this->view->checkshopDetailParent = $checkshopDetailParent;
$this->view->listCategoryPainting = $listCategoryPainting;
$this->view->history_change_picture = $history_change_picture;
$this->view->history_change_picture_child = $history_change_picture_child;
$this->view->store_is_valid_change_picture = $store_is_valid_change_picture;
// ---------------HIEN THI THONG BAO ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages())) {

    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
}


if ($userStorage->id == 15038) {
    $this->view->user_spe = true;
}
