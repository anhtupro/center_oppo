<?php 

//var_dump($imei); exit;
	$checkshop_id = $this->getRequest()->getParam('id');
	$QAppCheckshop    = new Application_Model_AppCheckshop();
	$QStore           = new Application_Model_Store();
	$QBiTrade    		= new Application_Model_BiTrade();
	$QCheckshopDetails = new Application_Model_AppCheckShopDetail();
	$QAppCheckShopFile = new Application_Model_AppCheckShopFile();
	$QCategory    		= new Application_Model_Category();

	$flashMessenger = $this->_helper->flashMessenger;

	$check_shop 		   = $QAppCheckshop->getShopInfo($checkshop_id);
	$checkshop_details = $QAppCheckshop->getCheckshopDetails($checkshop_id);
	$checkshop_last   = $QAppCheckshop->getLastCheckshop($check_shop['store_id']);
	$investments 	  = $QBiTrade->getCheckshopByStore($check_shop['store_id']);
	$checkshop = [];
	foreach($checkshop_last as $key=>$value){
		$checkshop[$value['category_id']][] = $value;
	}

	$params =array(
		'checkshop_id' =>$checkshop_id
	);
	foreach($investments as $key=>$value){
		$params['investments_cat'][] = $value['category_id'];
	}

	$img_check =	$QAppCheckshop->getImg($checkshop_id);
	$img_different = $QAppCheckshop->getImgDifferent($checkshop_id);
	$where = $QStore->getAdapter()->quoteInto('id = ?', $check_shop['store_id'] );
	$store = $QStore->fetchRow($where);

	$category_checkshop = $QCategory->getCategoryCheckshop($params);	
	$this->view->investments = $investments;
	$this->view->store       = $store;
	$this->view->img_check=$img_check;
	$this->view->img_different=$img_different;
	$this->view->checkshop         = $checkshop;
	$this->view->checkshop_details = $checkshop_details;
	//$this->view->checkshop_last = $checkshop_last;
	$this->view->params=$params;

	$this->view->category    	= $category_checkshop;

	//var_dump($checkshop); exit;
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}

