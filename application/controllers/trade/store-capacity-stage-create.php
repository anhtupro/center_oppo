<?php
$id = $this->getRequest()->getParam('id');

$QStoreCapacityStage = new Application_Model_StoreCapacityStage();
$QStoreCapacity = new Application_Model_StoreCapacity();
$QStoreCapacityAssign = new Application_Model_StoreCapacityAssign();

$list_capacity = $QStoreCapacity->fetchAll()->toArray();
$this->view->list_capacity = $list_capacity;

if ($id) {
    $stage = $QStoreCapacityStage->fetchRow(['id = ?' => $id]);
    $list_capacity_old = $QStoreCapacityAssign->getCapacity($id);
    $this->view->stage = $stage;
    $this->view->list_capacity_old = $list_capacity_old;
}

