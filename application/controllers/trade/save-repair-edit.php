<?php
require_once "Aws_s3.php";
$s3_lib = new Aws_s3();

$repair_id = $this->getRequest()->getParam('repair_id');
$repair_details_id = $this->getRequest()->getParam('repair_details_id');
$contructors_id = $this->getRequest()->getParam('contructors_id');
$price = $this->getRequest()->getParam('price');
$price_final = $this->getRequest()->getParam('price_final');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$repair_date = $this->getRequest()->getParam('repair_date');
$image = $this->getRequest()->getParam('image');

$part = $this->getRequest()->getParam('part');
$part_quantity = $this->getRequest()->getParam('part_quantity');

$repair_details_type_id = $this->getRequest()->getParam('repair_details_type_id');
$repair_type = $this->getRequest()->getParam('repair_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');

$repair_quotation_title = $this->getRequest()->getParam('repair_quotation_title');
$repair_quotation_quantity = $this->getRequest()->getParam('repair_quotation_quantity');
$repair_quotation_total_price = $this->getRequest()->getParam('repair_quotation_total_price');

$contract_number = $this->getRequest()->getParam('contract_number');
$review_cost = $this->getRequest()->getParam('review_cost');
$review_note = $this->getRequest()->getParam('review_note');
$month_debt = $this->getRequest()->getParam('month_debt');


$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QRepair = new Application_Model_Repair();
$QRepairDetails = new Application_Model_RepairDetails();
$QRepairDetailsFile = new Application_Model_RepairDetailsFile();
$QRepairDetailsPart = new Application_Model_RepairDetailsPart();
$QRepairConfirm = new Application_Model_RepairConfirm();
$QRepairDetailsType = new Application_Model_RepairDetailsType();
$QRepairQuotation = new Application_Model_RepairQuotation();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QArea = new Application_Model_Area();
$QAppNoti = new Application_Model_AppNoti();

$where_repair = $QRepair->getAdapter()->quoteInto('id = ?', $repair_id);
$repair = $QRepair->fetchRow($where_repair);

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {

    //Hủy đề xuất
    if ($remove) {
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QRepair->update($remove_date, $where_repair);

        // notify
        $area_id = $QArea->getAreaByStore($repair['store_id']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(4, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất SỬA CHỮA bị hủy",
                'link' => "/trade/repair-edit?id=" . $repair_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/repair-edit?id=' . $repair_id);
    }
    //END Hủy đề xuất

    //Reject: status => 1, quay về bước trade local duyệt đề xuất
    if ($reject) {

        if ($repair['status'] < 4) {
            $repair_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 1,
                'reject_by' => $userStorage->id,
                'reject_at' => date('Y-m-d H:i:s')
            ];

            $status_notify = 1;
        }

        if ($repair['status'] > 4) {
            $repair_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 4,
                'reject_by' => $userStorage->id,
                'reject_at' => date('Y-m-d H:i:s')
            ];
            $status_notify = 4;
        }

        $QRepair->update($repair_data, $where_repair);

        // notify
        $area_id = $QArea->getAreaByStore($repair['store_id']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(4, $status_notify, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất SỬA CHỮA bị từ chối",
                'link' => "/trade/repair-edit?id=" . $repair_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/repair-edit?id=' . $repair_id);


    }
    //END Reject


    //1: Chờ TMK Local xác nhận
    if ($repair['status'] == 1) {

        foreach ($repair_details_id as $key => $value) {
            $where = $QRepairDetails->getAdapter()->quoteInto('id = ?', $value);

            $data = [
                'contructors_id' => !empty($contructors_id[$value]) ? $contructors_id[$value] : 0,
                'price' => !empty($price[$value]) ? $price[$value] : 0,
            ];
            $QRepairDetails->update($data, $where);

            $where = [];
            $where[] = $QRepairDetailsFile->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairDetailsFile->getAdapter()->quoteInto('type = ?', 2);
            $QRepairDetailsFile->delete($where);

            foreach ($_FILES['file']['name'][$value] as $k => $v) {

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $value;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['file']['tmp_name'][$value][$k];

                $old_name = $v;
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {

                    // upload image to server s3
                    $destination_s3 = 'photo/repair_file/2/' . $value . '/';
                    $upload_s3 = $s3_lib->uploadS3($newFilePath, $destination_s3, $new_name);
                    // upload image to server s3

                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $value . DIRECTORY_SEPARATOR . $new_name;

                    $dtf = [
                        'repair_details_id' => $value,
                        'url' => '/' . $url,
                        'type' => 2
                    ];
                    $QRepairDetailsFile->insert($dtf);
                }
            }

            //Linh kiện cty cung cấp
            $where = NULL;
            $where = $QRepairDetailsPart->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $QRepairDetailsPart->delete($where);

            foreach ($part[$value] as $k => $v) {
                $dtp = [
                    'repair_details_id' => $value,
                    'repair_part_id' => !empty($v) ? $v : NULL,
                    'quantity' => !empty($part_quantity[$value][$k]) ? $part_quantity[$value][$k] : NULL,
                    'status' => 1,
                ];
                $QRepairDetailsPart->insert($dtp);
            }
            //END Linh kiện cty cung cấp

            //Upload $repair_details_type_id

            foreach ($repair_details_type_id[$value] as $k => $v) {
                $rdt_update = [
                    'repair_tp_id' => $repair_details_tp[$value][$k]
                ];

                $where = $QRepairDetailsType->getAdapter()->quoteInto('id = ?', $v);
                $QRepairDetailsType->update($rdt_update, $where);

            }

            $where = [];
            $where[] = $QRepairDetailsType->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairDetailsType->getAdapter()->quoteInto('id NOT IN (?)', $repair_details_type_id[$value]);
            $QRepairDetailsType->update(['del' => 1], $where);


            //Cập nhật lại quotation cũ = 1 (hủy)
            $where = [];
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('status = ?', 0);
            $QRepairQuotation->update(['status' => 1], $where);
            // Insert quotation
            foreach ($repair_quotation_title [$value] as $index => $title) {
                if ($title) {
                    $data_quotation = [
                        'repair_details_id' => $value,
                        'title' => $title,
                        'quantity' => $repair_quotation_quantity [$value] [$index] ? $repair_quotation_quantity [$value] [$index] : Null,
                        'total_price' => $repair_quotation_total_price [$value] [$index],
                        'status' => 0
                    ];
                    $QRepairQuotation->insert($data_quotation);
                }

            }

        }

        $QRepair->update(['status' => 2, 'reject' => 0], $where_repair);

        $status_notify = 2;

    }//$repair['status'] == 1

    //2: Chờ TMK Leader xác nhận
    if ($repair['status'] == 2) {
        $QRepair->update(['status' => 4], $where_repair);
        $status_notify = 4;
    }

    //3: Chờ ASM xác nhận
    if ($repair['status'] == 3) {
        $QRepair->update(['status' => 4, 'reject' => 0], $where_repair);
        $status_notify = 4;

    }


    //4: Chờ TMK Local xác nhận nghiệm thu
    if ($repair['status'] == 4) {
        //UPload file hình ảnh nghiệm thu
        $tag = 0;

        foreach ($image as $key => $value) {

            foreach ($value as $k => $v) {
                $data = $v;

                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
      

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $key;
  

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $url = '/photo/repair_file/3/' . $key . '/image_' . $k . '.png';

                $image = new My_Image_Resize();
                $image->load($url);
                $image->resizeToWidth(800);

                $new_name = 'image_' . $k . '.png';

                $file = $uploaded_dir . '/' . $new_name;

                $success_upload = file_put_contents($file , $data);

                // upload image to server s3

                $destination_s3 = 'photo/repair_file/3/' . $key . '/';

                if ($success_upload) {
                    $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $new_name);
                }
                // upload image to server s3

                $rdf_insert = [
                    'repair_details_id' => $key,
                    'url' => $url,
                    'type' => 3
                ];
                $QRepairDetailsFile->insert($rdf_insert);

                $tag = 1;
            }
        }

        if ($tag != 1) {
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload file hình ảnh nghiệm thu.');
            $this->_redirect(HOST . 'trade/repair-edit?id=' . $repair_id);
        }

        foreach ($repair_details_id as $key => $value) {
            // Insert quotation
            //Cập nhật lại quotation cũ = 1 (hủy)
            $where = [];
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('status = ?', 2);
            $QRepairQuotation->update(['status' => 3], $where);
            foreach ($repair_quotation_title [$value] as $index => $title) {
                $data_quotation = [
                    'repair_details_id' => $value,
                    'title' => $title,
                    'quantity' => $repair_quotation_quantity [$value] [$index] ? $repair_quotation_quantity [$value] [$index] : Null,
                    'total_price' => $repair_quotation_total_price [$value] [$index] ? $repair_quotation_total_price [$value] [$index] : Null,
                    'status' => 2
                ];
                $QRepairQuotation->insert($data_quotation);
            }

            // update contract number
            $QRepairDetails->update([
               'contract_number' => $contract_number[$value] ? TRIM($contract_number[$value]) : ''
            ], ['id = ?' => $value]);
        }

        $QRepair->update(['status' => 5], $where_repair);

        $status_notify = 5;



    }//if($repair['status'] == 4)

    //5: Chờ TMK Leader xác nhận hoàn thành
    if ($repair['status'] == 5) {
        $repair_details_part = $QRepairDetailsPart->getPart($repair_id);
        if ($repair_details_part) {
            $areaId = $QArea->getAreaByStore($repair['store_id']);
            foreach ($repair_details_part as $detail) {

                if ($detail['category_id']) {
                    $where = [
                        'area_id = ?' => $areaId,
                        'category_id = ?' => $detail['category_id']
                    ];

                    $categoryInWarehouse = $QAppCheckshopDetailChild->fetchRow($where);

                    if ($categoryInWarehouse['quantity']) {
                        $QAppCheckshopDetailChild->update([
                            'quantity' => $categoryInWarehouse['quantity'] - $detail['quantity']
                        ], $where);
                    }

                }

            }
        }

        // update contract number
        foreach ($repair_details_id as $key => $value) {
            $QRepairDetails->update([
                'contract_number' => $contract_number[$value] ? TRIM($contract_number[$value]) : ''
            ], ['id = ?' => $value]);
        }

        $QRepair->update(['status' => 6], $where_repair);

        $status_notify = 6;

    }


    // chờ thanh toán
    if ($repair['status'] == 6) {


        foreach ($repair_details_id as $key => $value) {
            if ($review_cost[$value] == 2) {
                // Insert quotation
                $where = [];
                $where[] = $QRepairQuotation->getAdapter()->quoteInto('repair_details_id = ?', $value);
                $where[] = $QRepairQuotation->getAdapter()->quoteInto('status = ?', 4);
                $QRepairQuotation->update(['status' => 5], $where);

                    foreach ($repair_quotation_title [$value] as $index => $title) {
                        $data_quotation = [
                            'repair_details_id' => $value,
                            'title' => $title,
                            'quantity' => $repair_quotation_quantity [$value] [$index] ? $repair_quotation_quantity [$value] [$index] : Null,
                            'total_price' => $repair_quotation_total_price [$value] [$index] ? $repair_quotation_total_price [$value] [$index] : 0,
                            'status' => 4
                        ];
                        $QRepairQuotation->insert($data_quotation);
                    }

            }

            if ($review_cost[$value] == 1) { // chi phí chính xác
                $where = [];
                $where[] = $QRepairQuotation->getAdapter()->quoteInto('repair_details_id = ?', $value);
                $where[] = $QRepairQuotation->getAdapter()->quoteInto('status = ?', 2);

                $data_insert_quotation = $QRepairQuotation->fetchAll($where);

                foreach ($data_insert_quotation as $quotation) {
                    $QRepairQuotation->insert([
                        'repair_details_id' => $quotation['repair_details_id'] ? $quotation['repair_details_id'] : Null,
                        'title' => $quotation['title'] ? $quotation['title'] : Null,
                        'quantity' => $quotation['quantity'] ? $quotation['quantity'] : Null,
                        'total_price' => $quotation['total_price'] ? $quotation['total_price'] : Null,
                        'status' => 4
                    ]);
                }
            }

            $QRepairDetails->update([
                'review_cost' => $review_cost[$value],
                'review_note' => $review_note[$value],
                'month_debt' => $month_debt[$value]
            ], ['id = ?' => $value]);
        }

        $QRepair->update(['status' => 7], $where_repair);

        $status_notify = 7;

    }

    //REPAIR CONFIRM
    $rp_confirm = [
        'repair_id' => $repair_id,
        'repair_status' => $repair['status'],
        'confirm_by' => $this->storage['staff_id'],
        'created_at' => date('Y-m-d H:i:s')
    ];
    $QRepairConfirm->insert($rp_confirm);
    //END REPAIR CONFIRM

    // update reject = 0
    $QRepair->update([
        'reject' => 0
    ], ['id = ?' => $repair_id]);

    // notify
    $area_id = $QArea->getAreaByStore($repair['store_id']);
    $list_staff_notify = $QAppStatusTitle->getStaffToNotify(4, $status_notify, $area_id);

    foreach ($list_staff_notify as $staff_notify) {
        $data_noti = [
            'title' => "TRADE MARKETING OPPO VIETNAM",
            'message' => "Có một đề xuất SỬA CHỮA đang chờ bạn duyệt",
            'link' => "/trade/repair-edit?id=" . $repair_id,
            'staff_id' => $staff_notify
        ];

//        $QAppNoti->sendNotification($data_noti);
    }

    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/repair-edit?id=' . $repair_id);


} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
    $this->_redirect(HOST . 'trade/repair-edit?id=' . $repair_id);

}

?>