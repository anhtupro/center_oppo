<?php
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);

//SEARCH
$name_search            = $this->getRequest()->getParam('name_search');
$area_id_search         = $this->getRequest()->getParam('area_id_search');
$sale_id            	= $this->getRequest()->getParam('staff_id');
$status_id            	= $this->getRequest()->getParam('status_id');
$from_date            	= $this->getRequest()->getParam('from_date');
$to_date            	= $this->getRequest()->getParam('to_date');
$title            	= $this->getRequest()->getParam('title'); 
$hasFee            	= $this->getRequest()->getParam('has_fee');
$isKa            	= $this->getRequest()->getParam('is_ka');
$statusFinish = $this->getRequest()->getParam('status_finish');
$contractor_id = $this->getRequest()->getParam('contractor_id');
$contract_number = $this->getRequest()->getParam('contract_number');
$export_review_cost = $this->getRequest()->getParam('export_review_cost');
$review_cost = $this->getRequest()->getParam('review_cost');

$month = $this->getRequest()->getParam('month');
$year = date('Y');

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAir = new Application_Model_AppAir();
$QDestruction=new Application_Model_Destruction();
$QContructors = new Application_Model_Contructors();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$flashMessenger       = $this->_helper->flashMessenger;
$area_id = $this->storage['area_id'];

$limit = LIMITATION;
$total = 0;
$params = [
	'status_id'			=> $status_id,
	'name_search' 		=> $name_search,
	'area_id_search'	=> $area_id_search,
	'sale_id'			=> $sale_id,
	'from_date'			=> $from_date,
	'to_date'			=> $to_date,
	// 'list_area'      => $this->storage['area_id']
	'area_id'           =>   $area_id,
        'title'             => $title,
    'has_fee' => $hasFee,
    'year' => $year,
    'month' => $month,
    'is_ka' => $isKa,
    'status_finish' => $statusFinish,
    'contractor_id' => $contractor_id,
    'contract_number' => TRIM($contract_number),
    'review_cost' => $review_cost

];

if($_GET['dev'] == 1){
    echo "<pre>";
    var_dump($params);exit;
}

$params['sort'] = $sort;
$params['desc'] = $desc;


//-----------------KIỂM TRA TITLE ĐỂ HIỂN THỊ NÚT ACTION---------------

if ($userStorage->title == SALES_TITLE){// -------------------HIỂN THỊ ĐỀ XUẤT DÀNH CHO SALE --------------
	$params['staff_id']= $userStorage->id;
}

if ($export_review_cost) {
    $statistics = $QAir->getStatisticReviewCost($params);
    $QAir->exportReviewCost($statistics);
}

$result =$QAir->fetchPagination($page, $limit, $total, $params);

$listContractor = $QContructors->fetchAll();
$contract_number = $QAir->getContractNumber($params);

$this->view->contract_number = $contract_number;
$this->view->listContractor = $listContractor;

$params['staff_title']= $userStorage->title;
$QAppCheckShop 		= new Application_Model_AppCheckshop();
$QArea 				= new Application_Model_Area();
$area_list = $QArea->getAreaList($params);

$this->view->area = $area_list;
$list_sale  = $QAppCheckShop->getSaleArea();
$this->view->list_sale = $list_sale;
$QAppStatus 		= new Application_Model_AppStatus();
$this->view->app_status = $QAppStatus->get_cache(8);

$this->view->params=$params;
$this->view->list= $result;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/air-list' . ($params ? '?' . http_build_query($params) .
	'&' : '?');
$this->view->offset = $limit * ($page - 1);


	// -------------RETURN CATEGORY ---------------
$result = $QCategory->GetAll();
$this->view->category = $result;

	// -------------RETURN STORE ---------------
// $result = $QStore->getAll($params);

// $this->view->store = $result;
	// ---------------hien thi thong bao ----------------------

if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

