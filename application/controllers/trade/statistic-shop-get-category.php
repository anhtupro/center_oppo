<?php
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $storeId = $this->getRequest()->getParam('store_id');

    $QStatisticShop = new Application_Model_StatisticShop();
    $QStatisticShopDetail = new Application_Model_StatisticShopDetail();

    if (! $storeId) {
        echo json_encode([
           'status' => 1,
           'message' => 'Không nhận được Shop ID'
        ]);
        return;
    }

    $listCategory = $QStatisticShopDetail->getShopDetail($storeId);

    echo json_encode([
       'status' => 0,
       'listCategory' => $listCategory
    ]);

