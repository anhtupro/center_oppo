<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$areaId = $this->getRequest()->getParam('area_id');
$season = $this->getRequest()->getParam('season');
$year = $this->getRequest()->getParam('year');
$listPosmCost = $this->getRequest()->getParam('list_posm_cost');

$QAdditionalPosmCost = new Application_Model_AdditionalPosmCost();

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    if (!$areaId || !$season || !$year) {
        echo json_encode([
            'status' => 1,
            'message' => 'Vui lòng nhập đủ khu vực, quí, năm!'
        ]);
        return;
    }

    $where = [
        'season = ?' => $season,
        'year = ?' => $year,
        'area_id = ?' => $areaId
    ];
    $QAdditionalPosmCost->delete($where);

    foreach ($listPosmCost as $categoryId => $posmCost) {
        $QAdditionalPosmCost->insert([
            'area_id' => $areaId,
            'season' => $season,
            'year' => $year,
            'category_id_additional' => $categoryId,
            'cost' => $posmCost ? $posmCost : Null,
            'date' => '2019-03-31'
        ]);
    }

    $db->commit();

    echo json_encode([
        'status' => 0
    ]);
} catch (\Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
}




