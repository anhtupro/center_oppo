<?php

$export = $this->getRequest()->getParam('export');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$area_id = $this->getRequest()->getParam('area_id');
$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;

$from_date = $from_date ? date('Y-m-d 00:00:00', strtotime($from_date)) : Null;
$to_date = $to_date ? date('Y-m-d 23:59:59', strtotime($to_date)) : Null;

$QArea = new Application_Model_Area();
$QInventoryAreaPosmDetail = new Application_Model_InventoryAreaPosmDetail();


$params = [
    'area_list' => $this->storage['area_id'],
    'from_date' => $from_date,
    'to_date' => $to_date,
    'area_id' => $area_id
];

$listArea = $QArea->getListArea($params);

if ($export) {
    $QInventoryAreaPosmDetail->exportHistory($params);
}


$this->view->listArea = $listArea;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'trade/check-inventory-list' . '?';