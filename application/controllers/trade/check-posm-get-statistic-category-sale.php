<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$area_id = $this->getRequest()->getParam('area_id');
$stage_id = $this->getRequest()->getParam('stage_id');


$params = [
    'stage_id' => $stage_id,
    'area_id' => $area_id
];

$QCheckPosmStage = new Application_Model_CheckPosmStage();
$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QCheckPosmDetail = new Application_Model_CheckPosmDetail();

// theo số lượng hạng mục
$list_sale = $QCheckPosmStage->getStatisticCategorySale($params);
//$list_category = $QCategory->fetchAll(['check_posm = ?' => 1])->toArray();
$list_category = $QCheckPosmDetail->getListCategory($stage_id);

echo json_encode([
   'status' => 0,
   'list_sale' => $list_sale,
    'list_category' =>  $list_category
]);

