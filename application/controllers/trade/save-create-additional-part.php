<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$partName = $this->getRequest()->getParam('part_name');
$partType = $this->getRequest()->getParam('part_type');
$id = $this->getRequest()->getParam('id');

$QAdditionalPart = new Application_Model_AdditionalPart();

// edit
if ($id) {
    $QAdditionalPart->update([
       'name' => $partName,
       'type' => $partType ? $partType : Null
    ], ['id = ?' => $id]);

} else { // create
    $QAdditionalPart->insert([
        'name' => $partName,
        'type' => $partType ? $partType : Null
    ]);
}


echo json_encode([
    'status' => 0
]);