<?php 

	$id = $this->getRequest()->getParam('id');
	
	$QShipment 			= new Application_Model_AppShipment();
	$QTransporter 		= new Application_Model_AppTransporter();
	$QArea 				= new Application_Model_Area();
	$QShipmentDetails 	= new Application_Model_AppShipmentDetails();

	$area 			= $QArea->get_cache();
	$transporter 	= $QTransporter->get_cache();

	$params = [
		'shipment_id' => $id
	];

	if($this->storage['group'] == CONTRACTOR_GROUP_ID){
		$contractor = $QShipment->getContractorById($this->storage['staff_id']);
		$params['contractor_id'] = $contractor['id'];
	}

	$where 		= $QShipment->getAdapter()->quoteInto('id = ?', $id);
	$shipment 	= $QShipment->fetchRow($where);

	$shipment_details = $QShipmentDetails->fetchShipmentDetails($params);

	$this->view->area     = $area;
	$this->view->shipment = $shipment;
	$this->view->shipment_details = $shipment_details;
	$this->view->group      = $this->storage['group'];
	$this->view->transporter = $transporter;


