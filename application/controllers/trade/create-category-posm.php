<?php
$name = $this->getRequest()->getParam('name');
$price = $this->getRequest()->getParam('price');
$type_id = $this->getRequest()->getParam('type');
$desc = $this->getRequest()->getParam('desc');
$status = $this->getRequest()->getParam('status');
$status_order = $this->getRequest()->getParam('status_order');
$is_brand = $this->getRequest()->getParam('is_brand');
$submit = $this->getRequest()->getParam('submit');
$list_contractor = $this->getRequest()->getParam('contractor');
$list_code = $this->getRequest()->getParam('code');
$list_price = $this->getRequest()->getParam('price');
$is_purchasing_request = $this->getRequest()->getParam('is_purchasing_request');
$urgent_date = $this->getRequest()->getParam('urgent_date');
$purchasing_type = $this->getRequest()->getParam('purchasing_type');
$unit = $this->getRequest()->getParam('unit');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QContractorPrice = new Application_Model_ContractorPrice();
$QPurchasingRequest = new Application_Model_PurchasingRequest();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
$QPurchasingCodeCategory = new Application_Model_PurchasingCodeCategory();
$QPurchasingCode = new Application_Model_PurchasingCode();
$QPurchasingType = new Application_Model_PurchasingType();
$QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
$QSupplierContructors = new Application_Model_SupplierContructors();
$QContructors = new Application_Model_Contructors();

$purchasingTypeByDepartment  = $QPurchasingType->getCacheTypeByDepartment();

$flashMessenger = $this->_helper->flashMessenger;
$contractor_code = $QContractorPrice->getContractorCode($params);
$this->view->contractor_code = $contractor_code;
$this->view->purchasing_type = $purchasingTypeByDepartment[$userStorage->department];


if (!empty($flashMessenger->setNamespace('error')->getMessages())) {

    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
}
?>