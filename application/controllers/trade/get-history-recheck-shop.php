<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$roundId = $this->getRequest()->getParam('round_id');
$storeId = $this->getRequest()->getParam('store_id');

$QRecheckShop = new Application_Model_RecheckShop();

$db = Zend_Registry::get("db");
$select = $db->select()
             ->from(['r' => DATABASE_TRADE.'.recheck_shop'], [
                 'r.id',
                 'created_at' => "DATE_FORMAT(r.created_at, '%d/%m/%Y %H:%i')"
             ])
            ->where('r.recheck_shop_round_id = ?', $roundId)
            ->where('r.store_id = ?', $storeId)
            ->order('r.created_at DESC');

$listHistory = $db->fetchAll($select);

echo json_encode([
   'status' => 0,
   'listHistory' => $listHistory
]);
