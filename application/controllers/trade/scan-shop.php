<?php 
	
	$category_id = $this->getRequest()->getParam('category_id');
	$campaign_id = $this->getRequest()->getParam('campaign_id');

	$QCategory 			= new Application_Model_Category();
	$QArea   			= new Application_Model_Area();
	$QAppOrderDetails 	= new Application_Model_AppOrderDetails();
	$QAppOrder          = new Application_Model_AppOrder();
	$QCampaign          = new Application_Model_Campaign();
	$QAreaIn          	= new Application_Model_AppAreaIn();
	

	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id 	 = $userStorage->id;
	$title 		 =  $userStorage->title;
    $group 		 =  $userStorage->group_id;

    $params = [
		'campaign_id' => $campaign_id,
		'category_id' => $category_id,
		'staff_id'    => $staff_id,
		'area_id'	  => $this->storage['area_id']
	];

    if($title == SALES_TITLE){
		$where[] = $QAppOrder->getAdapter()->quoteInto('created_by = ?', $staff_id);
		$where[] = $QAppOrder->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
		$app_order_check = $QAppOrder->fetchRow($where);
		$params['order_id'] = $app_order_check['id'];
	}

	$list_store = $QCampaign->getListStore($params);

	$where    = $QCategory->getAdapter()->quoteInto('id = ?', $category_id );
	$category = $QCategory->fetchRow($where);

	$where    = $QArea->getAdapter()->quoteInto('id IN (?)', $area_id );
	$area     = $QArea->fetchAll($where);

	$order_details  = $QAppOrderDetails->getDataDetails($params);
	$params['store_id'] = [];
	foreach($order_details as $key => $value){
		$params['store_id'][] = $value['store_id'];
	}

	$imei_scan_in   = $QAppOrderDetails->getImeiScanIn($params);
	$list_imei = [];
	foreach($imei_scan_in as $key=>$value){
		$list_imei[$value['store_id']] = [
			'quantity'  => $value['quantity'],
			'imei' 		=> $value['list_imei']
		];
	}
	

	$list_imei_area = $QAreaIn->getListImeiArea($params);

	//$list_imei = $QAppOrderDetails->getImeiArea($params);
	//$list_imei_in = $QAppOrderDetails->getImeiAreaIn($params);


	$this->view->category 		= $category;
	$this->view->order_details  = $order_details;
	$this->view->area           = $area;
	$this->view->list_imei      = $list_imei;
	$this->view->list_imei_in   = $list_imei_in;
	$this->view->list_store     = $list_store;
	$this->view->params         = $params;
	$this->view->list_imei_area = $list_imei_area;

	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;

	


