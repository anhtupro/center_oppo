<?php
require_once "Aws_s3.php";

$store_from = $this->getRequest()->getParam('store_from');
$store_to = $this->getRequest()->getParam('store_to');
$warehouse_to = $this->getRequest()->getParam('warehouse_to');
$warehouse_from = $this->getRequest()->getParam('warehouse_from');
$category_id = $this->getRequest()->getParam('category_id');
$repair_details_type = $this->getRequest()->getParam('repair_details_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');
$image = $this->getRequest()->getParam('image-from');
$image_to = $this->getRequest()->getParam('image-to');
$transfer_type = $this->getRequest()->getParam('transfer_type');
$app_checkshop_detail_child_id = $this->getRequest()->getParam('app_checkshop_detail_child_id');
$area_from = $this->getRequest()->getParam('area_from');
$area_to = $this->getRequest()->getParam('area_to');



$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QTransfer = new Application_Model_Transfer();
$QTransferDetails = new Application_Model_TransferDetails();
$QRepairDetailsTp = new Application_Model_RepairDetailsTp();
$QTransferFile = new Application_Model_TransferFile();
$QRepairType = new Application_Model_RepairType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QTransferReason = new Application_Model_TransferReason();
$QTransferDetailsChild = new Application_Model_TransferDetailsChild();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QInventoryAreaImei = new Application_Model_AirInventoryAreaImei();
$QTransferImage = new Application_Model_TransferImage();
$QStore = new Application_Model_Store();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();

$flashMessenger       = $this->_helper->flashMessenger;

$data_repair_type = $QRepairType->fetchAll();
$repair_type = [];
foreach($data_repair_type as $key=>$value){
    $repair_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$params = array_filter(array(
    'store_id' => $store_id,
    'staff_id'  => $userStorage->id
));


if (!empty($submit)) {



    if(! $category_id){
        $flashMessenger->setNamespace('error')->addMessage('Không có hạng mục nào được chọn!');
        $this->_redirect(HOST . 'trade/create-transfer');
    }

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        $special_category_id = [803, 43, 48];

//        1: cần TKM Manager approve , 2: cần ASM approve, 3: bình thường
        foreach ($category_id as $index => $id) {
            if (in_array($id, $special_category_id)) {
                $approve_type = 1;
            } else {
                if ($transfer_type[$index] == 1) {
                    if (! $approve_type || $approve_type == 3)
                        $approve_type = 2;
                }

                if ($transfer_type[$index] == 2) {
                    if (! $approve_type)
                        $approve_type = 3;
                }

            }
        }

        $transfer = [
            'store_from'  => $store_from ? $store_from : Null,
            'store_to'  => !empty($store_to) ? $store_to : 0,
            'warehouse_to' => $warehouse_to ? $warehouse_to : Null,
            'warehouse_from' => $warehouse_from ? $warehouse_from : Null,
            'area_to' => $warehouse_to ? $area_to : Null,
            'area_from' => $warehouse_from ? $area_from : Null,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'staff_id'  => $userStorage->id,
            'version' => 2,
            'approve_type' => $approve_type,
            'note' => $note ? $note : Null
        ];

        $transfer_id = $QTransfer->insert($transfer);


        foreach($category_id as $key=>$value){

            if($quantity[$key] <= 0){
                $flashMessenger->setNamespace('error')->addMessage('Số lượng hạng mục không hợp lệ!');
                $this->_redirect(HOST . 'trade/create-transfer');
            }

            $transfer_details = [
                'transfer_id' => $transfer_id,
                'category_id'   => $value,
                'quantity'  => $quantity[$key],
                'transfer_reason_id' => $repair_details_type[$key],
                'transfer_type' => $transfer_type[$key],
                'status'    => 1,
                'app_checkshop_detail_child_id' => ($app_checkshop_detail_child_id[$key] && $app_checkshop_detail_child_id[$key] !== 'undefined') ? $app_checkshop_detail_child_id[$key] : Null
            ];
            $transfer_details_id = $QTransferDetails->insert($transfer_details);

            $transfer_details ['transfer_details_id'] = $transfer_details_id;
            $QTransferDetailsChild->insert($transfer_details);

            // đánh dấu hạng mục đang được điều chuyển
            if ($app_checkshop_detail_child_id[$key]) {
                $QAppCheckshopDetailChild->update([
                    'in_processing' => 1
                ], ['id = ?' => $app_checkshop_detail_child_id[$key] ]);
            }

        }


        //upload image

        $tag = 0;

        foreach($image as $k=>$v){


            $data = $v;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id . DIRECTORY_SEPARATOR . 'image_category';

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/transfer/' . $transfer_id . '/image_category/' . $newName;

            $file = $uploaded_dir . '/' . $newName;

            $success_upload = file_put_contents($file, $data);

            // upload image to server s3
            $s3_lib = new Aws_s3();
            $destination_s3 = 'photo/transfer/' . $transfer_id . '/image_category/';

            if ($success_upload) {
                $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
            }
            // upload image to server s3


            $rdf_insert = [
                'transfer_id' => $transfer_id,
                'url'    => $url,
                'type'   => 1
            ];
            $QTransferImage->insert($rdf_insert);
            $tag = 1;
        }

        if($tag == 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload Hình ảnh hạng mục điều chuyển!');
            $this->_redirect(HOST . 'trade/create-transfer');
        }

        $tag = 0;


        foreach($image_to as $k=>$v){

            $data = $v;

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id. DIRECTORY_SEPARATOR .'image_shop_to';

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/transfer/' . $transfer_id. '/image_shop_to/' . $newName;

            $file = $uploaded_dir . '/' . $newName;

            $success_upload = file_put_contents($file, $data);

            // upload image to server s3

            $s3_lib = new Aws_s3();
            $destination_s3 = 'photo/transfer/' . $transfer_id . '/image_shop_to/';

            if ($success_upload) {
                $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
            }
            // upload image to server s3


            $rdf_insert = [
                'transfer_id' => $transfer_id,
                'url'    => $url,
                'type'   => 2
            ];
            $QTransferImage->insert($rdf_insert);
            $tag = 1;
        }

        if($tag == 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload Hình ảnh khu vực đặt vật dụng!');
            $this->_redirect(HOST . 'trade/create-transfer');
        }


        // notify
        $area_id = $QArea->getAreaByStore($store_from);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(6, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất ĐIỀU CHUYỂN đang chờ bạn duyệt",
                'link' => "/trade/transfer-edit?id=" . $transfer_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }


        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');

        $this->_redirect(HOST . 'trade/transfer-edit?id='.$transfer_id);


    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());

        $this->_redirect(HOST . 'trade/create-transfer');

    }



}

$params['area_list'] = $this->storage['area_id'];
$listArea = $QArea->getListArea($params);
$this->view->listArea = $listArea;

$result = $QCategory->getCategory();
$this->view->category = $result;
$this->view->repair_type = $repair_type;
$this->view->transfer_reason = $QTransferReason->fetchAll()->toArray();


$params['area_id'] = $this->storage['area_id'];
//
$store_to = $QCampaign->getListStoreArea($params);
$this->view->store_to = $store_to;

$params['list_area_id'] = $this->storage['area_id'];
$store = $QStore->getListStore($params);
$this->view->store = $store;

$this->view->params = $params;

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>