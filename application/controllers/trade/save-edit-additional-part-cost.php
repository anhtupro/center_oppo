<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$areaId = $this->getRequest()->getParam('area_id');
$season = $this->getRequest()->getParam('season');
$year = $this->getRequest()->getParam('year');
$month = $this->getRequest()->getParam('month');
$listPartCost = $this->getRequest()->getParam('list_part_cost');

$date = $year . '-' . $month . '-01' ;

$QAdditionalPartCost = new Application_Model_AdditionalPartCost();

if (!$areaId || !$year) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng nhập đủ khu vực, năm!'
    ]);
    return;
}
if (!$season && !$month) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chọn quí hoặc tháng!'
    ]);
    return;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    if ($season && !$month) { // cho trườn hợp ngoại lệ

        $where = [
            'season = ?' => $season,
            'year = ?' => $year,
            'area_id = ?' => $areaId
        ];
        $QAdditionalPartCost->delete($where);

        foreach ($listPartCost as $categoryId => $partCost) {
            $QAdditionalPartCost->insert([
                'area_id' => $areaId,
                'season' => $season,
                'year' => $year,
                'additional_part_id' => $categoryId,
                'cost' => $partCost ? $partCost : Null,
                'date' => '2019-03-31'
            ]);
        }
    }

    if ($month && ! $season) {
        $where = [
            'month = ?' => $month,
            'year = ?' => $year,
            'area_id = ?' => $areaId
        ];
        $QAdditionalPartCost->delete($where);

        foreach ($listPartCost as $categoryId => $partCost) {
            $QAdditionalPartCost->insert([
                'area_id' => $areaId,
                'month' => $month,
                'year' => $year,
                'additional_part_id' => $categoryId,
                'cost' => $partCost ? $partCost : Null,
                'date' => $date
            ]);
        }
    }

    $db->commit();
    
    echo json_encode([
        'status' => 0
    ]);
} catch (\Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
}




