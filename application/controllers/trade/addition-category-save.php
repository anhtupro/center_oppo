<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$store_id = $this->getRequest()->getParam('store_id');
$list_category_id = $this->getRequest()->getParam('category_id');
$list_reason_id = $this->getRequest()->getParam('reason_id');
$list_quantity = $this->getRequest()->getParam('quantity');
$note = $this->getRequest()->getParam('note');
$image = $this->getRequest()->getParam('image');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QAdditionCategory = new Application_Model_AdditionCategory();
$QAdditionCategoryDetail = new Application_Model_AdditionCategoryDetail();
$QAdditionCategoryImage = new Application_Model_AdditionCategoryImage();

if (!$store_id) {
    echo json_encode([
       'status' => 1,
       'message' => 'Chưa chọn store!'
    ]);
    return;
}

if (!$list_category_id || !$list_reason_id) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa chọn hạng mục hoặc lý do !'
    ]);
    return;
}

if (count($image) <= 0 ) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa upload ảnh !'
    ]);
    return;
}

$db = Zend_Registry::get('db');
 $db->beginTransaction();

 $addition_id = $QAdditionCategory->insert([
    'store_id' => $store_id,
    'status' => 1,
     'created_by' => $userStorage->id,
     'created_at' => date('Y-m-d H:i:s')
 ]);

foreach ($list_category_id as $key => $category_id) {
    $reason_id = $list_reason_id[$key];
    $quantity = $list_quantity[$key];

    $QAdditionCategoryDetail->insert([
       'addition_id' => $addition_id,
       'category_id' => $category_id,
       'reason_id' => $reason_id ,
        'quantity' => $quantity
    ]);

 }


// save image
foreach($image as $k=>$v){

    $data = $v;
    list($type, $data) = explode(';', $data);
    list(, $data)      = explode(',', $data);
    $data = base64_decode($data);

    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'addition_category' . DIRECTORY_SEPARATOR . $addition_id;

    if (!is_dir($uploaded_dir))
        @mkdir($uploaded_dir, 0777, true);

    $newName = md5(uniqid('', true)) . '.png';

    $url = '/photo/addition_category/' . $addition_id . '/' . $newName;

    $file = $uploaded_dir . '/' . $newName;
    $success_upload = file_put_contents($file, $data);


    $url_resize = 'photo' .
        DIRECTORY_SEPARATOR . 'addition_category' . DIRECTORY_SEPARATOR . $addition_id . DIRECTORY_SEPARATOR . $newName;

    //Resize Anh
    $image = new My_Image_Resize();
    $image->load($url_resize);
    $image->resizeToWidth(800);
    $image->save($url_resize);
    //END Resize


    // upload image to server s3
    require_once "Aws_s3.php";
    $s3_lib = new Aws_s3();
    $destination_s3 = 'photo/addition_category/' . $addition_id . '/';

    if ($success_upload) {
        $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
    }
    // upload image to server s3

    $image_insert = [
        'addition_id' => $addition_id,
        'url'    => $url,
        'type' => 1
    ];

    $QAdditionCategoryImage->insert($image_insert);
}




 $db->commit();

echo json_encode([
    'status' => 0
]);

