<?php
        
    $area_id = $this->getRequest()->getParam('area_id');
    $campaign_id = $this->getRequest()->getParam('campaign_id');
    
    $QCampaignArea = new Application_Model_CampaignArea();
    $QAppStatusTitle = new Application_Model_AppStatusTitle();
    $QAppStatus = new Application_Model_AppStatus();
    $QArea = new Application_Model_Area();
    $QAppNoti = new Application_Model_AppNoti();

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $title = $userStorage->title;
    if(in_array($userStorage->id, [9837])){
        $title = TRADE_MARKETING_SUPERVISOR;
    }
    
    $app_status = $QAppStatus->get_cache(3);
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();

    try {
        
        $where = [];
        $where[] = $QCampaignArea->getAdapter()->quoteInto('area_id = ?', $area_id);
        $where[] = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
        $campaign_area = $QCampaignArea->fetchRow($where);
 
        $status_title = $QAppStatusTitle->get_cache_title(3);
	$status_title = $status_title[$title];
        
        
        $app_status = $QAppStatus->get_cache(3);
        $app_status_title_cache = $QAppStatusTitle->get_cache(3);
        
        $app_status_title = [];

        foreach($this->storage['title_list'] as $key=>$value){
            $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);
        }



        if(!in_array($campaign_area['status'], $app_status_title) OR !in_array($area_id, $this->storage['area_id'])){
         
            $data = [
                'code' => 2,
                'message' => "Xác nhận không hợp lệ"
            ];
            echo json_encode($data);
            exit;
        }
        
        $details = [
            'status' => ($campaign_area['status']+1)
        ];
        $QCampaignArea->update($details, $where);

        // notify
        $status_notify = $campaign_area['status']+1;

        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(3, $status_notify, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đơn ĐẶT HÀNG POSM đang chờ bạn xác nhận",
                'link' => "/trade/confirm-posm?campaign_id=" . $campaign_id,
                'staff_id' => $staff_notify
            ];

            $QAppNoti->sendNotification($data_noti);
        }

        
        $db->commit();
        
        $data = [
            'code' => 1,
            'status'  => $app_status[($campaign_area['status']+1)],
            'message' => 'Hoàn thành'
        ];
        
    } catch (Exception $e) {
        $db->rollBack();
        $data = [
            'code' => 2,
            'message' => $e->getMessage()
        ];
    }
    
    echo json_encode($data);exit;
    
    


