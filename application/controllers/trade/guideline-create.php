<?php
$id = $this->getRequest()->getParam('id');

$QGuideline = new Application_Model_Guideline();
if ($id) {
    $guideline = $QGuideline->fetchRow(['id = ?' => $id]);
}

$this->view->id = $id;
$this->view->guideline = $guideline;
