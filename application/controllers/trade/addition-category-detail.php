<?php
$id = $this->getRequest()->getParam('id');
$QAdditionCategory = new Application_Model_AdditionCategory();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$addition_info = $QAdditionCategory->getInfo($id);
$addition_detail = $QAdditionCategory->getDetail($id);
$addition_image = $QAdditionCategory->getImage($id);

$current_category = $QAppCheckshop->getCheckshopLock($addition_info['store_id'], NULL);

$has_permission = $QAppStatusTitle->checkPermission($userStorage->title, $addition_info['status'], 19);

$this->view->addition_info = $addition_info;
$this->view->addition_detail = $addition_detail;
$this->view->addition_image = $addition_image;
$this->view->current_category = $current_category;
$this->view->has_permission = $has_permission;