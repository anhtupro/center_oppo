<?php
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$name_search     = $this->getRequest()->getParam('name_search');
$category_id         = $this->getRequest()->getParam('category_id');
$area_id_search  			= $this->getRequest()->getParam('area_id_search');


$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QStore = new Application_Model_Store();
$QAppStatus = new Application_Model_AppStatus();
$QAirInventoryProduct = new Application_Model_AirInventoryArea();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QAppCheckShop = new Application_Model_AppCheckshop();

$flashMessenger       = $this->_helper->flashMessenger;
$limit = LIMITATION;
$total = 0;

$params = [
    'category_id'   => $category_id,
    'area_id_search'       => $area_id_search
];

$params['sort'] = $sort;
$params['desc'] = $desc;

if( in_array($this->storage['title'], [SALES_TITLE]) ){
    $params['staff_id'] = $this->storage['staff_id'];
}

$params['area_id'] = $this->storage['area_id'];

$repair = $QAirInventoryProduct->fetchPagination($page, $limit, $total, $params);

$this->view->list_sale  = $QAppCheckShop->getSaleArea();
$result = $QCategory->GetAll();
$this->view->area = $QArea->get_cache();
$this->view->category = $result;
$this->view->params=$params;
$this->view->list= $repair;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/inventory-product' . ($params ? '?' . http_build_query($params) .
	'&' : '?');
$this->view->offset = $limit * ($page - 1);

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>
