<?php

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore            = new Application_Model_Store();
$QStaff            = new Application_Model_Staff();
$QOpenShop       = new Application_Model_OpenShop();
$QOpenShopReason = new Application_Model_OpenShopReason();
//$QOpenShopFile = new Application_Model_RemoveShopFile();
$QAppStatus = new Application_Model_AppStatus();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppCheckshop = new Application_Model_AppCheckshop();

$app_status = $QAppStatus->get_cache(10);
$app_status_title = $QAppStatusTitle->get_cache(10);
$app_status_title = $app_status_title[$this->storage['title']];

$id       = $this->getRequest()->getParam('id');

if (!empty($id)) {
    $open_shop                = $QOpenShop->getOpenShop($id);
    $this->view->open_shop    = $open_shop;
    
    
//    $where                      = $QOpenShopFile->getAdapter()->quoteInto('open_shop_id = ?', $id);
//    $open_shop_file             = $QOpenShopFile->fetchAll($where);
//    $this->view->open_shop_file = $open_shop_file;
    
    //$list_store  = $QStore->getShopAccessByStaff();
//    $where_store = $QStore->getAdapter()->quoteInto('id = ?', $removeShop_row['store_id']);
//    $list_store  = $QStore->fetchAll($where_store);
    
    $store_info = $QAppCheckshop->getStoreInfo($open_shop['store_id']);
    
    $staff_cache = $QStaff->get_cache();
    $this->view->staff_cache = $staff_cache;
    //$this->view->stores        = $list_store;
    $reason_cache             = $QOpenShopReason->get_cache();
    $this->view->reason_active = $reason_cache;
    $this->view->store_info = $store_info;

}

$this->view->app_status       = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->params           = $params;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
