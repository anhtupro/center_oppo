<?php 
	
	$area_id 		= $this->getRequest()->getParam('area_id');
	$cat_id  		= $this->getRequest()->getParam('cat_id');
	$contractor_id  = $this->getRequest()->getParam('contractor_id');
	$campaign_id  	= $this->getRequest()->getParam('campaign_id');


	$params = [
		'area_id' 	=> $area_id,
		'cat_id' 	=> $cat_id,
		'contractor_id'	=> $contractor_id,
		'campaign_id'	=> $campaign_id
	];

	$QContractorArea 	= new Application_Model_ContractorArea();
	$contractor_area    = $QContractorArea->getDetails($params);


	$data[] = NULL;
    foreach($contractor_area as $key=>$value){
    	$created_at 				= $value['created_at'] ? date('H:s d/m/Y', strtotime($value['created_at'])) : 0;
    	$quantity 					= $value['quantity'] ? $value['quantity'] : 0;
    	$quantity_area 				= $value['quantity_area'] ? $value['quantity_area'] : 0;
    	$contractor_area_id 		= $value['id'] ? $value['id'] : 0;
    	$area_date 					= $value['area_date'] ? 1 : 0;
        $data[] 					= ['created_at'=>$created_at, 'quantity'=>$quantity, 'quantity_area'=>$quantity_area, 'contractor_area_id'=> $contractor_area_id, 'area_date' => $area_date];
    }

	echo json_encode($data);exit;




