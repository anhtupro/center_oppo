<?php

$check_shop_id = $this->getRequest()->getParam('check_shop_id');

$params = [
  'check_shop_id' => $check_shop_id
];

$QCategory = new Application_Model_Category();
$QPhoneProduct = new Application_Model_PhoneProduct();
$QPromotion = new Application_Model_Promotion();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QCheckShopEntireErrorImage = new Application_Model_CheckShopEntireErrorImage();
$QCheckShopEntireFile = new Application_Model_CheckShopEntireFile();
$QCheckShopEntireDetail = new Application_Model_CheckShopEntireDetail();
$QCheckShopEntireDetailBrand = new Application_Model_CheckShopEntireDetailBrand();
$QCheckShopEntireImageRatio = new Application_Model_CheckShopEntireImageRatio();
$QCheckShopEntireDisplayProduct = new Application_Model_CheckShopEntireDisplayProduct();
$QCheckShopEntireDisplayProductDetail = new Application_Model_CheckShopEntireDisplayProductDetail();
$QCheckShopEntireDisplayProductPromotion = new Application_Model_CheckShopEntireDisplayProductPromotion();
$QCheckShopEntirePointTotal = new Application_Model_CheckShopEntirePointTotal();
$QCheckShopEntirePromotion = new Application_Model_CheckShopEntirePromotion();
$QCheckShopEntirePromotionDetail = new Application_Model_CheckShopEntirePromotionDetail();
$QBrand = new Application_Model_BrandTrade();



$check_shop_entire = $QCheckShopEntire->get($params);

$store_info = $QAppCheckshop->getStoreInfo($check_shop_entire['store_id']);

$list_category_check_shop = $QCheckShopEntireDetail->getCheckShopDetail($params);
$list_category_check_shop_brand = $QCheckShopEntireDetailBrand->getCheckShopDetail($params);

// get image
$list_image_checkShop = $QCheckShopEntireFile->getImageShop($check_shop_id, 1);
$list_image_checkshop_different = $QCheckShopEntireFile->getImgDifferent($check_shop_id, 1);

$list_image_error = $QCheckShopEntireFile->get($check_shop_id, 2);
$list_image_repair = $QCheckShopEntireFile->get($check_shop_id, 3);
//end image

$list_error_image = $QCheckShopEntireErrorImage->getError($check_shop_id);


// image ratio and display product
//$list_ratio_image = $QCheckShopEntireImageRatio->get($params);
$list_display_product = $QCheckShopEntireDisplayProduct->get($params);
$list_display_product_detail = $QCheckShopEntireDisplayProductDetail->get($params);
$list_display_product_promotion = $QCheckShopEntireDisplayProductPromotion->get($params);

//$total_point = $QCheckShopEntirePointTotal->get($params);
//end image ratio and display product


// promotion
$list_promotion = $QCheckShopEntirePromotion->get($params);
$list_promotion_detail = $QCheckShopEntirePromotionDetail->get($params);
// end promotion


$sellout = $QAppCheckshop->getSellout($check_shop_entire['store_id']);
$level = $QAppCheckshop->getLevelStore($check_shop_entire['store_id']);
$list_brand = $QBrand->fetchAll()->toArray();

// style color in view
$color = [
    1 => '#2AAD6F',
    2 => 'blue',
    4 => '#415fff',
    5 => '#fbb244',
    6 => 'orangered',
    7 => '#aa0f0f'
];


$this->view->store_info = $store_info;
$this->view->sellout = $sellout;
$this->view->level = $level;
$this->view->list_brand = $list_brand;

$this->view->list_category_check_shop = $list_category_check_shop;
$this->view->list_category_check_shop_brand = $list_category_check_shop_brand;

$this->view->list_image_checkShop = $list_image_checkShop;
$this->view->list_image_checkshop_different = $list_image_checkshop_different;
$this->view->list_image_error = $list_image_error;
$this->view->list_image_repair = $list_image_repair;


$this->view->list_error_image = $list_error_image;
$this->view->check_shop_entire = $check_shop_entire;

$this->view->list_ratio_image = $list_ratio_image;
$this->view->list_display_product = $list_display_product;
$this->view->list_display_product_detail = $list_display_product_detail;
$this->view->list_display_product_promotion = $list_display_product_promotion;
$this->view->total_point = $total_point;

$this->view->list_promotion = $list_promotion;
$this->view->list_promotion_detail = $list_promotion_detail;
$this->view->color = $color;