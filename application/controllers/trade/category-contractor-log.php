<?php 
	
	$contractor_id          = $this->getRequest()->getParam('contractor_id');

	$QCampaign 	   			= new Application_Model_Campaign();
	$QContructors 		   	= new Application_Model_Contructors();
	$QCategoryContractor 	= new Application_Model_CategoryContractor();

	$where 					= $QContructors->getAdapter()->quoteInto('id = ?', $contractor_id);
	$category_contractor 	= $QContructors->fetchRow($where);

	$params = [
		'contractor_id' => $contractor_id
	];

	$limit = LIMITATION;
	$total = 0;
	//$category_contractor = $QCategoryContractor->fetchPagination($page, $limit, $total, $params);


	$this->view->offset 				= $limit * ($page - 1);
	$this->view->category_contractor    = $category_contractor;
	$this->view->params 				= $params;
    $this->view->limit  				= $limit;
    $this->view->total 					= $total;
	$this->view->url 					= HOST . 'trade/category-contractor/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');




