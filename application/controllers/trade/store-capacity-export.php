<?php
require_once 'PHPExcel.php';

$stage_id = intval($this->getRequest()->getParam('stage_id'));

$PHPExcel = new PHPExcel();
$QStoreCapacityResult = new Application_Model_StoreCapacityResult();
$params = [
  'area_list' => $this->storage['area_id'],
  'stage_id' => $stage_id  
];

$data = $QStoreCapacityResult->getDataExport($params);


$heads = array(
    'Store ID',
    'Channel',
    'Tên shop',
    'Khu vực',
    'Tỉnh',
    'Quận huyện',
    'Năng lực'
);


$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 2;

$i = 1;

foreach($data as $item){
    $alpha    = 'A';

    $sheet->setCellValue($alpha++.$index, $item['store_id']);
    $sheet->setCellValue($alpha++.$index, $item['channel']);
    $sheet->setCellValue($alpha++.$index, $item['store_name']);
    $sheet->setCellValue($alpha++.$index, $item['area_name']);
    $sheet->setCellValue($alpha++.$index, $item['province_name']);
    $sheet->setCellValue($alpha++.$index, $item['district_name']);
    $sheet->setCellValue($alpha++.$index, $item['capacity_name']);

    $index++;
}

$filename = 'Đánh giá năng lực shop' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
