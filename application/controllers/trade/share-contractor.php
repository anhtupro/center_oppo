<?php

$campaign_id    = $this->getRequest()->getParam('campaign_id');
	$input_code 	= $this->getRequest()->getParam('input_code');
	$price   		= $this->getRequest()->getParam('price');
	$quantity_order = $this->getRequest()->getParam('quantity_order');


$category_id    = $this->getRequest()->getParam('category_id');
	$area_id    	= $this->getRequest()->getParam('area_id');
	$submit     	= $this->getRequest()->getParam('submit');
	$contractor_id  = $this->getRequest()->getParam('contractor_id');

	$QCampaign 	   			= new Application_Model_Campaign();
	$QCategory 	   			= new Application_Model_Category();
	$QArea 		   			= new Application_Model_Area();
	$QCampaignArea 			= new Application_Model_CampaignArea();
	$QCampaignContractor 	= new Application_Model_CampaignContractor();
	$QCampaignAreaContractor	= new Application_Model_CampaignAreaContractor();
	$QAppStatusTitle			= new Application_Model_AppStatusTitle();


$flashMessenger = $this->_helper->flashMessenger;

	$area           = $QArea->get_cache();

	$params = [
		'campaign_id'    => $campaign_id,
		'contractor_id'  => $contractor_id
	];

	$area_status = $QCampaignArea->get_status_area($params);
    $max_status  = $QCampaignArea->get_status_max($params);
	$min_status  = $QCampaignArea->get_status_min($params);
	$status_title = $QAppStatusTitle->get_cache_title(3);
	$status_title = $status_title[$this->storage['title']];


//$campaign_contractor = $QCampaignContractor->getData($params);



	$where 		   = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign 	   = $QCampaign->fetchRow($where);

	$category      		= $QCampaign->getCategory($params);
	$category_campaign 	= $QCampaign->getCategoryCampaign($params);

	$category_cache = [];
	foreach($category as $key=>$value){
		$category_cache[$value['id']] = $value['name'];
	}


	$where 					= $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
	$campaign_area_check 	= $QCampaignArea->fetchAll($where);

	if(count($campaign_area_check) == 0){

		$campaign_area_insert = $QCampaignArea->getCampaignAreaInsert($params);
		foreach($campaign_area_insert as $key=>$value){
			$QCampaignArea->insert(
				[
					'campaign_id'  	 => $value['campaign_id'],
					'area_id' 	  	 => $value['area_id'],
					'category_id' 	 => $value['category_id'],
					'price' 		 => $value['price'],
					'status' 		 => 1,
				]
			);
		}
		
	}

	//Get data campaign_area
	$campaign_area = $QCampaignArea->getCampaignArea($params);
	$data = [];
	foreach($campaign_area as $key=>$value){
		$data[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data


//Get data campaign_area
	$contractor_category = $QCampaignArea->getContractorCategory($params);
	$data_price = [];
	foreach($contractor_category as $key=>$value){
		$data_price[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data

	//Get data campaign_area_contractor
	$campaign_area_contractor = $QCampaignAreaContractor->getCampaignAreaContractor($params);
	$data_area_contractor = [];
	foreach($campaign_area_contractor as $key=>$value){
		$data_area_contractor[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get campaign_area_contractor

	$back_url = HOST.'trade/share-contractor?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;

	if(!empty($submit) AND $submit == 2){

		require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;


        //Phần đầu
        $heads[] = '';
        $heads[] = '';

        foreach($category_campaign as $key=>$value){
        	$heads[] = '';
        	$heads[] = $value['category_id'];
        }

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $alpha = 'A';
        $index = 2;
        //END Phần đầu

        //Phần đầu 2
        $sheet->setCellValue($alpha . $index, 'ID Khu vực');
        $alpha++;
        $sheet->setCellValue($alpha . $index, 'Khu vực');
        $alpha++;

        foreach($category_campaign as $key=>$value){
        	$col_1 = $alpha . $index;

        	$sheet->setCellValue($alpha . $index, $value['name']);
        	$alpha++;

        	$col_2 = $alpha . $index;

        	$sheet->setCellValue($alpha . $index, $value['name']);
        	$alpha++;

        	$sheet->mergeCells($col_1.':'.$col_2);
        }

        $index = 3;
        //END Phần đầu 2

        //KHU VỰC
        foreach($area as $key=>$value){
        	$alpha = 'A';
        	$sheet->getCell($alpha++ . $index)->setValueExplicit($key ,PHPExcel_Cell_DataType::TYPE_STRING);
        	$sheet->getCell($alpha++ . $index)->setValueExplicit($value ,PHPExcel_Cell_DataType::TYPE_STRING);
        	
        	foreach($category_campaign as $k=>$v){
        		$quantity_order = !empty($data[$key][$v['category_id']]['quantity']) ? $data[$key][$v['category_id']]['quantity'] : NULL;

        		$code = !empty($data_price[$key][$v['category_id']]['code']) ? $data_price[$key][$v['category_id']]['code'] : NULL;

	        	$sheet->getCell($alpha++ . $index)->setValueExplicit(($quantity_order > 0) ? $quantity_order : NULL ,PHPExcel_Cell_DataType::TYPE_STRING);
	        	$sheet->getCell($alpha++ . $index)->setValueExplicit(($quantity_order > 0) ? $code : NULL ,PHPExcel_Cell_DataType::TYPE_STRING);
	        }
	        $index++;
        }
        //END Khu vực


        $filename = ' Share Contractor ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
	}


	if(!empty($submit) AND $submit == 1){
		if($_FILES["file"]["tmp_name"] != ""){

			$db = Zend_Registry::get('db');
			$db->beginTransaction();
			try {

				include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';

				error_reporting(0);
				set_time_limit(0);
				
				//===================Main=======================

				if($_FILES["file"]["tmp_name"] != ""){
					if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
						//echo ("Không phải file XLSX !");exit;
					}
					if ($_FILES["file"]["error"] > 0)
				    {
				    	echo ("Return Code: " . $_FILES["file"]["error"] . "<br />");exit;
				    }
					move_uploaded_file($_FILES["file"]["tmp_name"], "files/limit_campaign/share-contractor.xlsx");
				}

				$inputfile = 'files/limit_campaign/share-contractor.xlsx';
				$xlsx = new SimpleXLSX($inputfile);
				$data_xlsx = $xlsx->rows();

				//Đếm số cột
				$num_cols = count($category_campaign)*2+2;

				//Đếm số dòng
				$i = 1;
				$num_rows = 0;
				while ($data_xlsx[$i++][0] <> '') {
				  $num_rows++;
				}

				for($i = 2; $i <= $num_rows; $i++){

					$area_id = $data_xlsx[$i][0];

					for($j = 3; $j <= $num_cols; $j++){
						$cat_id 	= $data_xlsx[0][$j];
						
						if(!empty($cat_id) AND !empty($area_id)){

							$input_code = $data_xlsx[$i][$j];

							$quantity   = !empty($data[$area_id][$cat_id]['quantity']) ? $data[$area_id][$cat_id]['quantity'] : 0;

							if($quantity > 0){

								$params_contractor = [
									'campaign_id' 		=> $campaign_id,
									'area_id'			=> $area_id,
									'category_id'		=> $cat_id,
									'code'				=> $input_code,
								];

								$contractor_category = $QCampaignAreaContractor->getContractorCategory($params_contractor);

								if(empty($contractor_category)){

									$flashMessenger->setNamespace('error')->addMessage("Chưa chọn nhà sản xuất: <b>".$area[$area_id]."/".$category_cache[$cat_id].'/'.$input_code.'</b>');
									$this->redirect($back_url);
								}

								$data_ = array(
									'campaign_id'	=> $campaign_id,
									'contractor_id' => $contractor_category['contractor_id'],
									'area_id' 		=> $area_id,
									'category_id'   => $cat_id,
									'quantity'		=> $quantity,
									'price'			=> $contractor_category['price'],
									'status'		=> 1
								);

								$QCampaignAreaContractor->insert($data_);
					            
							}
							
						}
						
					}
					
				}
				unlink($inputfile);

				//Cập nhật lại status campaign_area
				$where = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
				$campaign_area = $QCampaignArea->fetchRow($where);
				$QCampaignArea->update( ['status'=>($min_status+1)] , $where);
				//END cập nhật lại status campaign_area

				//=================Function Area===================
			    $flashMessenger->setNamespace('success')->addMessage('Chia số lượng cho nhà thầu thành công!');
				$db->commit();
				$this->redirect($back_url);
				
			} catch (Exception $e) {
				$db->rollBack();
				$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
				$this->redirect($back_url);
			}

		}
		else{

			$db = Zend_Registry::get('db');
			$db->beginTransaction();
			try {

				foreach($quantity_order as $key=>$value){

					if($value > 0){
						$params_contractor = [
							'campaign_id' 		=> $campaign_id,
							'area_id'			=> $area_id[$key],
							'category_id'		=> $category_id[$key],
							'code'				=> $input_code[$key],
						];

						$contractor_category = $QCampaignAreaContractor->getContractorCategory($params_contractor);

						if(empty($contractor_category)){

							$flashMessenger->setNamespace('error')->addMessage("Chưa chọn nhà sản xuất: <b>".$area[$area_id[$key]]."/".$category_cache[$category_id[$key]].'/'.$input_code[$key].'</b>');
							$this->redirect($back_url);
						}

						$data_ = array(
							'campaign_id'	=> $campaign_id,
							'contractor_id' => $contractor_category['contractor_id'],
							'area_id' 		=> $area_id[$key],
							'category_id'   => $category_id[$key],
							'quantity'		=> $value,
							'price'			=> $contractor_category['price'],
							'status'		=> 1
						);

						$QCampaignAreaContractor->insert($data_);
					}
				}
				
				//Cập nhật lại status campaign_area
				$where = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
				$campaign_area = $QCampaignArea->fetchRow($where);
				$QCampaignArea->update( ['status'=>($min_status+1)], $where);
				//END cập nhật lại status campaign_area

				$flashMessenger->setNamespace('success')->addMessage('Chia số lượng cho nhà thầu thành công!');
				$db->commit();
				$this->redirect($back_url);
			} catch (Exception $e) {
				$db->rollBack();
				$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
				$this->redirect($back_url);
			}
		}
	}

	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;
	

	$this->view->area     		= $area;
	$this->view->campaign 		= $campaign;
	$this->view->category 		= $category;
	$this->view->data     		= $data;
	$this->view->contractor     = $QCampaign->getContractor($params);
	$this->view->params         = $params;
	$this->view->data_area_contractor 	= $data_area_contractor;
	$this->view->data_price 			= $data_price;
	$this->view->category_campaign 		= $category_campaign;
	$this->view->area_status        = $area_status;
	$this->view->max_status         = $max_status;
	$this->view->min_status         = $min_status;
	$this->view->status_title 		= $status_title;

