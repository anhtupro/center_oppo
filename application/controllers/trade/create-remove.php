<?php
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$category   = $this->getRequest()->getParam('category');
$store   = $this->getRequest()->getParam('store');
$sl   = $this->getRequest()->getParam('sl', 0);
$lydo   = $this->getRequest()->getParam('lydo','');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$submit   = $this->getRequest()->getParam('submit');
$flashMessenger       = $this->_helper->flashMessenger;

$params = array_filter(array(
	'category' => $category,
	'store'=>$store,
	'sl'=>$sl,
	'lydo'=>$lydo
));
$this->view->params = $params;
$result="";

if($submit == 2){ // -----event click xac nhan---------
	//---------kiem tra xem co up file chua?----------
	$upload = new Zend_File_Transfer();
	$upload->setOptions(array('ignoreNoFile'=>true));

            //check function
	if (function_exists('finfo_file'))
		$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif','application/vnd.ms-powerpoint'));

	$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
	$upload->addValidator('Size', false, array('max' => '2MB'));
	$upload->addValidator('ExcludeExtension', false, 'php,sh');
	$files = $upload->getFileInfo();
	if($files['image']['error']==0){ // neu up file ko loi thi tien hanh insert
		// insert  record bang destruction
		$QDestruction = new Application_Model_Destruction();
		$result_query =$QDestruction->insert(array(
			'category_id'=>$category,
			'store_id'=>$store,
			'sl'=>$sl,
			'lydo'=>$lydo,
			'staff_id'=>$userStorage->id,
			'date'=>date('Y-m-d H:i:s')
		));
		


        // ------------------ upload image----------------------------------------------------------
		$data_file = array();

		$fileInfo = (isset($files['image']) and $files['image']) ? $files['image'] : null;
		$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
		DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
		DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . $result_query;
		if (!is_dir($uploaded_dir))
			@mkdir($uploaded_dir, 0777, true);
		$upload->setDestination($uploaded_dir);

            //Rename
		$old_name = $fileInfo['name'];
		$tExplode = explode('.', $old_name);
		$extension = end($tExplode);
		$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
		$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
		$r = $upload->receive(array('image'));
		if($r){
			$data_file['file_name'] = $new_name;
		}
		else{
			$messages = $upload->getMessages();
			foreach ($messages as $msg)
				throw new Exception($msg);
		}
	// --------------insert hinh anh len database-------------
		$data_file['destruction_id'] = $result_query;
		$QDestructionFile = new Application_Model_DestructionFile();
		$result =$QDestructionFile->insert($data_file);
	//---------- lay hinh anh vua upload  moi dc tai len -----------
		$params['id']=$result;
		$QDestructionFile =  new Application_Model_DestructionFile();
		$dataAll = $QDestructionFile->GetList($params);
		$this->view->dataAll = $dataAll;
		if($result_query){
			$flashMessenger->setNamespace('success')->addMessage('Thông báo đề xuất thành công ');
			$this->_redirect(HOST . 'trade/edit-remove?id='.$result_query);
		}
	}else{

		$flashMessenger->setNamespace('error')->addMessage('Thông báo không thành công. Vui lòng kiểm tra lại thông tin và hình ảnh');
		$this->_redirect(HOST . 'trade/create-remove');
	}

}
// ---------------hien thi thong bao ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;

}
	//lay danh sach hang muc
$QCategory = new Application_Model_Category();
$result = $QCategory->getall();
$this->view->category = $result;
	//lay danh sach cua hang
$QStore = new Application_Model_Store();
$result = $QStore->getall();
$this->view->store = $result;



?>
