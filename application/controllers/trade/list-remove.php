<?php 
	$sort            = $this->getRequest()->getParam('sort', '');
	$desc            = $this->getRequest()->getParam('desc', 1);
	$page            = $this->getRequest()->getParam('page', 1);
	$total = 0;
	$list = new Application_Model_Destruction();
	
	$limit = LIMITATION;
	$params['sort'] = $sort;
	$params['desc'] = $desc;

	$result =$list->fetchPagination($page, $limit, $total, $params);
	$this->view->list= $result;

	$this->view->desc = $desc;
	$this->view->sort = $sort;
	$this->view->limit = $limit;
	$this->view->total = $total;
	$this->view->url = HOST . 'trade/list-remove' . ($params ? '?' . http_build_query($params) .
		'&' : '?');
	$this->view->offset = $limit * ($page - 1);
	$flashMessenger       = $this->_helper->flashMessenger;
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}

 if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
 
 	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;

 }
?>