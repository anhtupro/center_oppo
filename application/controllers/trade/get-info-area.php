<?php 
$QRegionalMarket=new Application_Model_RegionalMarket();
$QStore=new Application_Model_Store();

$data = [];
$data['info'] = [
	'province'=>array(),
	'district'=>array(),
	'shop'=>array(),
];

$area_id = $this->getRequest()->getParam('area_id');
$province = $this->getRequest()->getParam('province');
$district = $this->getRequest()->getParam('district');
if (!empty($area_id )){
	$where =$QRegionalMarket->getAdapter()->quoteInto('area_id = ?',$area_id);
	$result = $QRegionalMarket->fetchAll($where);

	foreach ($result as $key => $value) {
		$data['info']['province'][$key] =  [
			'key' =>$value['id'],
			'name'=>$value['name']
		];
	}

}
if (!empty($province)){
	$where =$QRegionalMarket->getAdapter()->quoteInto('parent = ?',$province);
	$resultDistrict = $QRegionalMarket->fetchAll($where);

	foreach ($resultDistrict as $key => $value) {
		$data['info']['district'][$key] = [
			'key' =>$value['id'],
			'name'=>$value['name']
		];
	}
}

if (!empty($district)){
	$where =$QStore->getAdapter()->quoteInto('(del <> 1 or del is null) and district = ?',$district);
	$resultShop = $QStore->fetchAll($where);
	
	foreach ($resultShop as $key => $value) {
		$data['info']['shop'][$key] = [
			'key' =>$value['id'],
			'name'=>$value['name']
		];
	}
}



echo json_encode($data);exit;