<?php 
	
	$data_order 	= $this->getRequest()->getParam('data_order');
	$order_id 		= $this->getRequest()->getParam('order_id');
	$campaign_id 	= $this->getRequest()->getParam('campaign_id');
	$note 			= $this->getRequest()->getParam('note');

	$data_order = json_decode($data_order, true);


	$QAppOrderDetails 	= new Application_Model_AppOrderDetails();
	$QAsm 	     		= new Application_Model_Asm();
	$QAppOrderConfirm 	= new Application_Model_AppOrderConfirm();
	$QAppStatusTitle  	= new Application_Model_AppStatusTitle();
	$QAppContractorQuantity = new Application_Model_AppContractorQuantity();
	$QCampaign 			= new Application_Model_Campaign();

	$area_id = !empty($this->storage['area_id']) ? $this->storage['area_id'] : NULL;

	$flashMessenger = $this->_helper->flashMessenger;

    $params = [
		'campaign_id' => $campaign_id,
		'staff_id'    => $this->storage['staff_id'],
		'area_id'	  => $area_id
	];

	$app_status_title = $QAppStatusTitle->get_cache(TU_BAN_BUCGOC);

	$status_title = $app_status_title[$this->storage['title']];

	$back_url = HOST.'trade/quantity-contractor?campaign_id='.$campaign_id;

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		$category_quantity = $QCampaign->getCategoryQuantity($params);

		$data_quantity = [];
		foreach ($category_quantity as $key => $value) {
			$data_quantity[$value['category_id']] = $value['quantity'];
		}


		foreach ($data_order as $key => $value) {

			$quantity = 0;

			foreach ($value as $k => $v) {

				$quantity = $quantity + $v['quantity'];

				$data = [
					'contractor_id'	=> $v['id'],
					'campaign_id'	=> $campaign_id,
					'category_id'	=> $key,
					'quantity'		=> $v['quantity'],
					'created_by'	=> $this->storage['staff_id'],
					'created_at'	=> date('Y-m-d H:i:s'),
					'status'		=> 1
				];
				$QAppContractorQuantity->insert($data);
			}

			if($data_quantity[$key] != $quantity){
				$flashMessenger->setNamespace('error')->addMessage('Số lượng đặt hàng khác số lượng chia cho nhà thầu!');
    			$this->redirect($back_url);
			} 

		}
		

		$order_details = $QAppOrderDetails->getDataDetails($params);

		foreach($order_details as $key=>$value){


			if(in_array($value['status'], $status_title) ){
				$where = $QAppOrderDetails->getAdapter()->quoteInto('id = ?', $value['order_details_id']);
				$data = [
					'status'	=> ($value['status'] + 1)
				];
				$QAppOrderDetails->update($data,$where);
			}

		}

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Thành công!');
    	$this->redirect($back_url);


	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;
	




