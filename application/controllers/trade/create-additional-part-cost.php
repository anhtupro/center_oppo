<?php
$season = $this->getRequest()->getParam('season');
$year = $this->getRequest()->getParam('year', date('Y'));
$month = $this->getRequest()->getParam('month');

$QAdditionalPosmCost = new Application_Model_AdditionalPosmCost();
$QArea = new Application_Model_Area();
$QCategoryAdditional = new Application_Model_CategoryAdditional();
$QAdditionalPart = new Application_Model_AdditionalPart();
$QAdditionalPartCost = new Application_Model_AdditionalPartCost();

$params = [
    'season' => $season,
    'year' => $year,
    'month' => $month
];

//$listAdditionalCost = $QAdditionalPosmCost->getCost($params);
$listArea = $QArea->getListArea();
//$listAdditionalCategory = $QCategoryAdditional->getAll();

$listAdditionalPart = $QAdditionalPart->getAll();
$listAdditionalPartCost = $QAdditionalPartCost->getCost($params);

$this->view->listAdditionalPart = $listAdditionalPart;
$this->view->listAdditionalPartCost = $listAdditionalPartCost;

$this->view->listAdditionalCost = $listAdditionalCost;
$this->view->listArea = $listArea;
$this->view->listAdditionalCategory = $listAdditionalCategory;
$this->view->season = $season;
$this->view->year = $year;
$this->view->month = $month;

