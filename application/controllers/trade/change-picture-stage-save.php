<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
include 'PHPExcel/IOFactory.php';
// config for excel template excel
define('START_ROW', 2);
define('STORE_ID', 0);


$name = $this->getRequest()->getParam('name');
$from_date_check = $this->getRequest()->getParam('from_date_check');
$to_date_check = $this->getRequest()->getParam('to_date_check');
$stage_id = $this->getRequest()->getParam('stage_id');
$list_product_id = $this->getRequest()->getParam('product_id');


$QChangePictureStage = new Application_Model_ChangePictureStage();
$QChangePictureAssignPhone = new Application_Model_ChangePictureAssignPhone();
$QChangePictureAssignStore = new Application_Model_ChangePictureAssignStore();

$db = Zend_Registry::get('db');
$db->beginTransaction();


if ($stage_id) {
    $QChangePictureStage->update([
        'name' => $name,
        'from_date_check' => $from_date_check ? date('Y-m-d', strtotime($from_date_check)) : NULL,
        'to_date_check' => $to_date_check ? date('Y-m-d', strtotime($to_date_check)) : NULL
    ], ['id = ?' => $stage_id]);

    if ($list_product_id) {
        $QChangePictureAssignPhone->delete(['stage_id = ?' => $stage_id]);

        foreach ($list_product_id as $product_id) {
            $QChangePictureAssignPhone->insert([
                'stage_id' => $stage_id,
                'product_id' => $product_id
            ]);
        }
    }


} else {
    $stage_id_insert = $QChangePictureStage->insert([
        'name' => $name,
        'from_date_check' => $from_date_check ? date('Y-m-d', strtotime($from_date_check)) : NULL,
        'to_date_check' => $to_date_check ? date('Y-m-d', strtotime($to_date_check)) : NULL
    ]);

    if ($list_product_id) {
        foreach ($list_product_id as $product_id) {
            $QChangePictureAssignPhone->insert([
               'stage_id' => $stage_id_insert,
                'product_id' => $product_id
            ]);
        }
    }

}


// mass upload list store
$stage_id = $stage_id ? $stage_id : $stage_id_insert;
// upload and save file
if ($_FILES['file']['name']) { // if has file upload

    $save_folder = 'change_picture_massupload';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 15000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx')
    );

    try {
        $file = My_File::get($save_folder, $requirement);
    
        if (!$file) {
            echo json_encode([
                'status' => 1,
                'message' => 'Upload failed'
            ]);
            return;
        }
        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    //read file
    //  Choose file to read
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }
 
    // read sheet
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();
    try {
        $QChangePictureAssignStore->delete(['stage_id = ?' => $stage_id]);


        $insertQuery = "INSERT INTO trade_marketing.change_picture_assign_store(stage_id, store_id) VALUES ";
    
        for ($row = START_ROW; $row <= $highestRow; ++$row) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = $rowData[0];
    
            $storeId = TRIM($rowData[STORE_ID]);
    
            if ($storeId) {
                $listStoreId [] = $storeId;
            }
        }

       $array_store_id = array_unique($listStoreId);

        foreach ($array_store_id as $store_id) {
            $value = '(' . $stage_id . ',' . $store_id  . '),';
            $insertQuery .= $value;
        }


        if (! $listStoreId) {
            echo json_encode([
                'status' => 1,
                'message' => 'Định dạng file không đúng. Vui lòng điền thông tin dựa trên file mẫu !'
            ]);
            return;
        }
    
        // check if store_id is valid
    
    
        // execute query

        $insertQuery = TRIM($insertQuery, ',');

        if($insertQuery){
    
            $stmt = $db->prepare($insertQuery);
            $stmt->execute();
    
            $stmt->closeCursor();

        }


        // check if store ID is valid
       $db = Zend_Registry::get("db");
       $select = $db->select()
                    ->from(['a' => DATABASE_TRADE.'.change_picture_assign_store'], [
                        'a.store_id'
                    ])
       ->joinLeft(['s' => 'store'], 'a.store_id = s.id AND (s.del IS NULL OR s.del = 0)', [])
       ->where('a.stage_id = ?', $stage_id)
       ->where('s.id IS NULL');

       $list_store_invalid = $db->fetchCol($select);

        if ($list_store_invalid) {
            echo json_encode([
                'status' => 1,
                'message' => 'ID Shop không có hoặc đã được đóng code: ' . implode(', ', $list_store_invalid) . '. Vui lòng kiểm tra lại file và thử lại.'
            ]);

            return;

        }

        //

    
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
    }

}


$db->commit();

echo json_encode([
   'status' => 0
]);