<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$id = $this->getRequest()->getParam('id');
$airIaCost = $this->getRequest()->getParam('air_ia_cost');
$season = $this->getRequest()->getParam('season');
$year = $this->getRequest()->getParam('year');
$repairIaCost = $this->getRequest()->getParam('repair_ia_cost');
$repairKaCost = $this->getRequest()->getParam('repair_ka_cost');
$repairBrandshopCost = $this->getRequest()->getParam('repair_brandshop_cost');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$params = [
  'season' => $season,
    'year' => $year
];

$QAdditionalAirCost = new Application_Model_AdditionalAirCost();
$where = ['id = ?' => $id];
if ($id) {
    $QAdditionalAirCost->update([
        'air_ia_cost' => $airIaCost ? $airIaCost : Null,
        'repair_ia_cost' => $repairIaCost ? $repairIaCost : Null,
        'repair_ka_cost' => $repairKaCost ? $repairKaCost : Null,
        'repair_brandshop_cost' => $repairBrandshopCost ? $repairBrandshopCost : Null,
    ], $where);
}

$listAdditionalCost = $QAdditionalAirCost->getCost($params);

echo json_encode([
   'status' => 0,
    'listAdditionalCost' => $listAdditionalCost
]);

