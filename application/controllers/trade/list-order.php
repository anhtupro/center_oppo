<?php 

	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id 	 = $userStorage->id;

	$QCampaign 		= new Application_Model_Campaign();
	$QAsm      		= new Application_Model_Asm();
	$QAreaStaff     = new Application_Model_AreaStaff();

	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id 	 = $userStorage->id;
	$title 		 = $userStorage->title;
    $group 		 = $userStorage->group_id;
    $email       = $userStorage->email;

	$params = [];

    if($title == SALES_TITLE){
    	$params['staff_id']	= $staff_id;
    }
    elseif($title == SALE_SALE_ASM){
    	$asm 	 = $QAsm->get_cache($staff_id);
    	$area_id = $asm['area'];
    	$params['area_id']	= $area_id;
    }
    elseif($title == TRADE_MARKETING_EXECUTIVE){
    	$area_staff = $QAreaStaff->getAreaTrade($email);
    	$params['area_id']	= $area_staff;
    }

	$campaign          = $QCampaign->getCampaign($params);
    $campaign_status   = $QCampaign->getCampaignStatus($params);

    $status = [];
    foreach ($campaign_status as $key => $value) {
        $status[$value['campaign_id']] = $value['status_name'];
    }
    //var_dump($campaign); exit;
	$this->view->campaign = $campaign;
    $this->view->status   = $status;
