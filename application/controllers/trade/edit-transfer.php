<?php 
	
	$id 	 			= $this->getRequest()->getParam('id');
	$QReason 			= new Application_Model_AppReason();
	$QCategory  		= new Application_Model_Category();
	$QArea  			= new Application_Model_Area();
	$QCampaign  		= new Application_Model_Campaign();
	$QTransfer  		= new Application_Model_Transfer();
	$QTransferFile  	= new Application_Model_TransferFile();
	$QAppStatusTitle 	= new Application_Model_AppStatusTitle();
	$userStorage 		= Zend_Auth::getInstance()->getStorage()->read();
	$staff_id 	 		= $userStorage->id;
	$title 				= $userStorage->title;
	$params = [
		'id'    		=> $id,
		'title'			=> $title
	];
	$cache 				= $QAppStatusTitle->get_cache(6);
	$this->view->title_status = $cache[$title];
	$status 			=$QTransfer->getStatus($id);
	$this->view->status =$status;
	$this->view->params =$params;
    $category 			= $QCategory->getCategory();
    $transfer 			= $QTransfer->getTransfer($params);
    $transfer_details 	= $QTransfer->getTransferDetails($params);
    $params['type'] 	= 1;
    $transfer_file_from = $QTransferFile->getTransferFile($params);
    $params['type'] 	= 2;
    $transfer_file_to   = $QTransferFile->getTransferFile($params);
	$resultReason 					= $QReason->GetAll();
	$this->view->reason 			= $resultReason;
	$this->view->category 			= $category;
	$this->view->area       		= $QArea->get_cache();
	$this->view->transfer   		= $transfer;
	$this->view->list_store 		= $list_store;
	$this->view->transfer_details 	= $transfer_details;
	$this->view->transfer_file_from = $transfer_file_from;
	$this->view->transfer_file_to   = $transfer_file_to;

