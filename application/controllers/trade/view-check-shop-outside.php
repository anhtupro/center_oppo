<?php
$checkShopId = $this->getRequest()->getParam('check_shop_id');

$QCheckShopOutSide = new Application_Model_CheckShopOutside();
$QCheckShopOutSideDetail = new Application_Model_CheckShopOutsideDetail();
$QCheckShopOutSideFile = new Application_Model_CheckShopOutsideFile();
$QStoreOutside = new Application_Model_StoreOutside();
$QCheckshopOutsideDetailBrand = new Application_Model_CheckShopOutsideDetailBrand();

$checkShopOutside = $QCheckShopOutSide->getCheckShopOutside($checkShopId);
$listCheckShopOutsideDetail = $QCheckShopOutSideDetail->getDetail($checkShopId);
$listImage = $QCheckShopOutSideFile->get($checkShopId);
$listImageDifferent = $QCheckShopOutSideFile->getImgDifferent($checkShopId);
$checkshopDetailBrand = $QCheckshopOutsideDetailBrand->getDetail($checkShopId);


$this->view->checkShopOutside = $checkShopOutside;
$this->view->listCheckShopOutsideDetail = $listCheckShopOutsideDetail;
$this->view->listImage = $listImage;
$this->view->listImageDifferent = $listImageDifferent;
$this->view->checkshopDetailBrand = $checkshopDetailBrand;


