<?php

$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;

$QAdditionalPart = new Application_Model_AdditionalPart();

$listAdditionalPart = $QAdditionalPart->fetchPagination($page, $limit, $total, $params);

$this->view->listAdditionalPart = $listAdditionalPart;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);

$this->view->url = HOST . 'trade/list-additional-part' . ($params ? '?' . http_build_query($params) . '&' : '?');



