<?php 
	$id 	 		= $this->getRequest()->getParam('id');
	$confirm 	 	= $this->getRequest()->getParam('confirm');
	$QReason 		= new Application_Model_AppReason();
	$QCategory  	= new Application_Model_Category();
	$QArea  		= new Application_Model_Area();
	$QCampaign  	= new Application_Model_Campaign();
	$QTransfer  	= new Application_Model_Transfer();
	$QTransferFile  = new Application_Model_TransferFile();
	$QStatusTitle   = new Application_Model_AppStatusTitle();
	$userStorage 	= Zend_Auth::getInstance()->getStorage()->read();
	$staff_id 	 	= $userStorage->id;
	$title          = $userStorage->title;
	$flashMessenger = $this->_helper->flashMessenger;
	$params = [
		'id'    	=> $id,
		'title'		=>$title
	];
	$where 			= $QCategory->getAdapter()->quoteInto("group_bi IS NOT NULL", NULL);
    $category 		= $QCategory->fetchAll($where);
    $transfer 			= $QTransfer->getTransfer($params);
    $transfer_details 	= $QTransfer->getTransferDetails($params);
    $params['type'] 	= 1;
    $transfer_file_from = $QTransferFile->getTransferFile($params);
    $params['type'] 	= 2;
    $transfer_file_to   = $QTransferFile->getTransferFile($params);
    $cache = $QStatusTitle->get_cache(6);
    $this->view->title_status =$cache[$title];
    ////XÁC NHẬN
	if(!empty($confirm)){
		$back_url 		= HOST.'trade/view-transfer?id='.$id;
		$flashMessenger = $this->_helper->flashMessenger;
		$db             = Zend_Registry::get('db');
		$db->beginTransaction();
		try
		{
			 if(in_array($transfer['status'], $cache[$title])){
				$where = $QTransfer->getAdapter()->quoteInto("id = ?", $id);
				$QTransfer->update(['status'=>($transfer['stt']+1)] ,$where);
				$db->commit();
				$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công!');
			}
		}
		catch (Exception $e)
		{
			$db->rollBack();

		    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
		}
		$this->redirect($back_url);
	}
	//END XÁC NHẬN
	$resultReason 					= $QReason->GetAll();
	$this->view->reason 			= $resultReason;
	$this->view->category 			= $category;
	$this->view->area       		= $QArea->get_cache();
	$this->view->transfer   		= $transfer;
	$this->view->list_store 		= $list_store;
	$this->view->transfer_details 	= $transfer_details;
	$this->view->transfer_file_from = $transfer_file_from;
	$this->view->transfer_file_to   = $transfer_file_to;
// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;
	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          			= $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages 		= $messages;
	}
?>
