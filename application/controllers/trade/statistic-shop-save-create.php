<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$listQuantity = $this->getRequest()->getParam('list_quantity');
$storeId = $this->getRequest()->getParam('store_id');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QStatisticShop = new Application_Model_StatisticShop();
$QStatisticShopDetail = new Application_Model_StatisticShopDetail();

$db = Zend_Registry::get('db');
$db->beginTransaction();

$statisticShopId = $QStatisticShop->insert([
   'store_id' => $storeId,
    'created_by' => $userStorage->id,
    'created_at' => date('Y-m-d H:i:s')
]);

foreach ($listQuantity as $categoryId => $quantity) {
    $QStatisticShopDetail->insert([
       'statistic_shop_id' => $statisticShopId,
       'category_id' => $categoryId,
       'quantity' => $quantity
    ]);
}

$db->commit();

echo json_encode([
   'status' => 0
]);
    

