<?php
	
	$id 			= $this->getRequest()->getParam('id');
	$dx =			$this->getRequest()->getParam('dx');
	$store_from 	= $this->getRequest()->getParam('store_from');
	$store_to 		= $this->getRequest()->getParam('store_to');

	$warehouse_from 	= $this->getRequest()->getParam('store_from');
	$warehouse_to 		= $this->getRequest()->getParam('store_to');
	
	$reason_id = $this->getRequest()->getParam('reason_id');

	$category_id = $this->getRequest()->getParam('category');
	$quantity 	 = $this->getRequest()->getParam('quantity');

	$submit 			= $this->getRequest()->getParam('submit');

	$QTransfer   		= new Application_Model_Transfer();
	$QTransferDetails   = new Application_Model_TransferDetails();
	$QTransferFile      = new Application_Model_TransferFile(); 
	
	$flashMessenger = $this->_helper->flashMessenger;
	$db             = Zend_Registry::get('db');
	$db->beginTransaction();

	$status=$QTransfer->getStatus($id);

if(!empty($submit="Xác Nhận") && $status['status']==1)
{
	try{
			$where_update = $QTransfer->getAdapter()->quoteInto('id = ?', $id);
			$data_update=array(
				'status'      =>2
				);
			$QTransfer->update($data_update,$where_update);
			$db->commit();
			$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công!');
			$this->redirect(HOST.'trade/transfer-list');

	}catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
}elseif(!empty($submit="Xác Nhận") && $status['status']==2)
{
	try{
			$where_update = $QTransfer->getAdapter()->quoteInto('id = ?', $id);
			$data_update=array(
				'status'      =>3
				);
			$QTransfer->update($data_update,$where_update);
			$db->commit();
			$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công!');
			$this->redirect(HOST.'trade/transfer-list');

	}catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
}