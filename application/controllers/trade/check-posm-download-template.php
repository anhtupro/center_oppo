<?php

$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

require_once 'PHPExcel.php';

$QCategory = new Application_Model_Category();
$PHPExcel = new PHPExcel();


$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();


$list_category = $QCategory->fetchAll(['check_posm = ?' => 1], ['id']);

$heads = array(
    'Store ID'
);


$alpha = 'B';
$sheet->setCellValue('A1' , 'ID hạng mục (KHÔNG chỉnh sửa thông số của dòng 1 này)');

foreach ($list_category as $category) {
    $heads [] = $category['name'];

    //set dòng 1
    $sheet->setCellValue($alpha . 1 , $category['id']);
    $alpha++;
}


// set dòng 2
$alpha = 'A';
$index = 2;
foreach ($heads as $key) {
    $sheet->setCellValue($alpha . $index, $key);
    $alpha++;
}





$filename = 'Template_POSM' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;