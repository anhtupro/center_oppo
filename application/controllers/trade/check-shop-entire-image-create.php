<?php
$roundId = $this->getRequest()->getParam('round_id');
$storeId = $this->getRequest()->getParam('store_id');

$QAppCheckshop = new Application_Model_AppCheckshop();
$QCategory = new Application_Model_Category();
$QErrorImage = new Application_Model_ErrorImage();
$QBrandTrade = new Application_Model_BrandTrade();
$QPhoneProduct = new Application_Model_PhoneProduct();
$QPromotion = new Application_Model_Promotion();
$QCheckShopEntireDetailBrand = new Application_Model_CheckShopEntireDetailBrand();
$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QChangePictureStage = new Application_Model_ChangePictureStage();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();

$storeInfo = $QAppCheckshop->getStoreInfo($storeId);

$lockCheckShop = $QAppCheckshop->fetchRow([
    'store_id = ?' => $storeId,
    'is_lock = ?' => 1
]);

// get detail checkshop
if ($lockCheckShop['id']) {

    $listCheckShopDetail = $QAppCheckshop->getCheckshopDetails($lockCheckShop['id']);
    // lấy ra id những category đã checkshop
    foreach ($listCheckShopDetail as $detail) {
        $currentCategoryId [] = $detail['category_id'];
    }
}

$listCategory = $QCategory->getCategoryCheckshop();

// lấy ra danh sách category trừ nhưng category đã checkshop
foreach ($listCategory as $key => $category) {
    if (in_array($category['id'], $currentCategoryId)) {
        unset($listCategory[$key]);
    }
}

// hạng mục đối thủ
$lastest_check_shop = $QCheckShopEntire->fetchRow([
   'store_id = ?' => $storeId,
   'is_last = ?' => 1 
]);

if ($lastest_check_shop['id']) {
    $last_detail_rival = $QCheckShopEntireDetailBrand->getDetail($lastest_check_shop['id']);
}


$listCategoryBrand = $QCategory->fetchAll(['check_shop_rival = ?' => 1]);
$listBrand = $QBrandTrade->fetchAll(['is_checkshop = ?' => 1])->toArray();
$listErrorImage = $QErrorImage->fetchAll(['is_deleted IS NULL'])->toArray();

$sellout = $QAppCheckshop->getSellout($storeId);
$level = $QAppCheckshop->getLevelStore($storeId);


// hiện trạng posm + thông tin khuyến mãi
$list_category = $QCategory->fetchAll(['check_shop_entire = ?' => 1]);
$list_phone_product = $QPhoneProduct->fetchAll(['status = ?' => 1]);
$list_brand = $QBrandTrade->fetchAll(['is_checkshop = ?' => 1], ['ordering ASC'])->toArray();
$list_promotion = $QPromotion->fetchAll(['status = ?' => 1])->toArray();
$list_category_display = $QCategory->fetchAll(['is_display_image = ?' => 1]);

// style color in view
$color = [
    1 => '#2AAD6F',
    2 => 'blue',
    4 => '#415fff',
    5 => '#fbb244',
    6 => 'orangered',
    7 => '#aa0f0f'
];


// thay tranh

$check_shop_id_lock = $QAppCheckshop->getCheckShopIdLock($storeId);

$change_picture_stage = $QChangePictureStage->getStageForSale();
$params['change_picture_stage_id'] = $change_picture_stage['id']; // chỉ lấy dữ liệu của đơt gần nhất


if ($change_picture_stage) {
    $store_is_valid_change_picture = $QChangePictureStage->checkStoreValid($storeId, $change_picture_stage['id']);
}

if ($check_shop_id_lock) {
    $params['only_picture'] = 1; // chỉ lấy những HM có tranh
    $checkshopDetailChild = $QAppCheckshopDetailChild->getDetailChild($check_shop_id_lock);
    $checkshopDetailParent = $QAppCheckshopDetailChild->getDetailParent($check_shop_id_lock, $params);

    $params['checkshop_id'] = $check_shop_id_lock;
    $listCategoryPainting = $QAppCheckshopDetailChildPainting->getPainting($params);

    // lịch sử thay tranh
    $history_change_picture = $QChangePictureStage->getHistoryChangePicture($check_shop_id_lock, $params);
    $history_change_picture_child = $QChangePictureStage->getHistoryChangePictureChild($check_shop_id_lock, $params);
}
// end thay tranh




$this->view->list_category = $list_category;
$this->view->list_phone_product = $list_phone_product;
$this->view->check_shop_id = $check_shop_id;
$this->view->list_brand = $list_brand;
$this->view->list_promotion = $list_promotion;
$this->view->list_category_display = $list_category_display;
$this->view->color = $color;


$this->view->listCheckShopDetail = $listCheckShopDetail;
$this->view->listCategory = $listCategory;
$this->view->lockCheckShop = $lockCheckShop;
$this->view->sellout = $sellout;
$this->view->roundId = $roundId;
$this->view->storeId = $storeId;
$this->view->listErrorImage = $listErrorImage;
$this->view->storeInfo = $storeInfo;
$this->view->listCategoryBrand = $listCategoryBrand;
$this->view->listBrand = $listBrand;
$this->view->level = $level;
$this->view->last_detail_rival = $last_detail_rival;


$this->view->change_picture_stage = $change_picture_stage;
$this->view->checkshopDetailChild = $checkshopDetailChild;
$this->view->checkshopDetailParent = $checkshopDetailParent;
$this->view->listCategoryPainting = $listCategoryPainting;
$this->view->history_change_picture = $history_change_picture;
$this->view->history_change_picture_child = $history_change_picture_child;
$this->view->store_is_valid_change_picture = $store_is_valid_change_picture;
