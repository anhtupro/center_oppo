<?php
$page = $this->getRequest()->getParam('page', 1);
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$limit = LIMITATION;
$total = 0;

$QChangePictureStage = new Application_Model_ChangePictureStage();

$list = $QChangePictureStage->fetchPagination($page, $limit, $total, $params);

$this->view->list = $list;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/change-picture-stage-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
      