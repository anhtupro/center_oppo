<?php

$page = $this->getRequest()->getParam('page', 1);
$area_id = $this->getRequest()->getParam('area_id');
$status = $this->getRequest()->getParam('status');
$store_name = $this->getRequest()->getParam('store_name');
$remove = $this->getRequest()->getParam('remove');
$limit = LIMITATION;

$QAdditionCategory = new Application_Model_AdditionCategory();
$QArea = new Application_Model_Area();
$params = [
    'list_area' => $this->storage['area_id'],
    'area_id' => $area_id,
    'store_name' => TRIM($store_name),
    'status' => $status,
    'remove' => $remove
];

$list = $QAdditionCategory->fetchPagination($page, $limit,$total, $params);
$list_area = $QArea->getListArea($params);


$this->view->params = $params;
$this->view->list_area = $list_area;
$this->view->list = $list;
$this->view->limit = $limit;
$this->view->total = $total;

$this->view->url = HOST . 'trade/addition-category-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);