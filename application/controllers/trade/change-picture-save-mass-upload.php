<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
include 'PHPExcel/IOFactory.php';
// config for excel template excel


require_once "Aws_s3.php";
$s3_lib = new Aws_s3();


// row
define('START_ROW', 2);

// column
define('STORE_ID', 0);
define('CHILD_ID', 7);
define('CHILD_PAINTING_ID', 8);
define('KEY_VISUAL', 13);
define('LOGO', 14);
define('NOTE', 15);

$stage_id = $this->getRequest()->getParam('stage_id');
$userStorage  = Zend_Auth::getInstance()->getStorage()->read();

$QChangePictureStage = new Application_Model_ChangePictureStage();
$QChangePictureDetail = new Application_Model_ChangePictureDetail();
$QPhoneProduct = new Application_Model_PhoneProduct();
$QStore = new Application_Model_Store();
$QAppNoti = new Application_Model_AppNoti();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();

$change_picture_stage_valid = $QChangePictureStage->getValidStage();
$stage_id = $change_picture_stage_valid['id'];

if (! $change_picture_stage_valid) {
    echo json_encode([
        'stauts' => 1,
        'message' => 'Hiện không có đợt thay tranh nào!'
    ]);

    return;
}


$db = Zend_Registry::get("db");
$db->beginTransaction();

// upload and save file
if ($_FILES['file_change_picture']['name']) { // if has file upload

    $save_folder = 'change_picture_massupload';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 15000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx')
    );

    try {
        $file = My_File::get($save_folder, $requirement);

        if (!$file) {
            echo json_encode([
                'status' => 1,
                'message' => 'Upload failed'
            ]);
            return;
        }
        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

        // upload image to server s3
        $destination_s3 = 'files/' . $save_folder . '/' . $file['folder'] . '/';
        $upload_s3 = $s3_lib->uploadS3($inputFileName, $destination_s3, $file['filename']);
        // upload image to server s3

//        $url_file = '/files/' . $save_folder. '/' . $file['folder'] . '/' . $file['filename'];
//
//        if ($url_file) {
//            $QCheckPosmStage->update([
//                'url_file' => $url_file
//            ],['id = ?' => $stage_id]);
//        }

    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    //read file
    //  Choose file to read
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    // read sheet
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();


    try {

        $insert_header_query = "INSERT INTO trade_marketing.change_picture_detail(child_painting_id, product_id, logo_type, note, stage_id, created_by, app_checkshop_detail_child_id, store_id, category_id) VALUES ";

        $list_key_visual = $QPhoneProduct->getProductChangePicture($stage_id);
        foreach ($list_key_visual as $key_visual) {
            $list_key_visual_id [] = $key_visual['id'];
        }

        $array_store_id = [];
        $list_store_invalid  = [];

        for ($row = START_ROW; $row <= $highestRow; ++$row) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = $rowData[0];

            $store_id = TRIM($rowData[STORE_ID]);
            $child_id = $rowData[CHILD_ID] ? TRIM($rowData[CHILD_ID]) : Null;
            $child_painting_id = $rowData[CHILD_PAINTING_ID] ? TRIM($rowData[CHILD_PAINTING_ID]) : Null;
            $key_visual = $rowData[KEY_VISUAL] ? TRIM($rowData[KEY_VISUAL]) : 0;
            $logo = TRIM($rowData[LOGO]) == 1 ? 1 : (TRIM($rowData[LOGO]) == 0 ? 2 : Null);
            $note = $rowData[NOTE] ? TRIM($rowData[NOTE]) : '';

//            if (in_array($store_id, $array_store_id) || !$store_id) {
//                $list_store_invalid [] = $store_id;
//                continue;
//            }

            // kiểm tra key visual có nằm trong đợt do HO tạo hay không
            if ($key_visual && !in_array($key_visual, $list_key_visual_id)) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Mã số Key Visual (' . $key_visual . ') không nằm trong danh sách Key Visual của đợt thay tranh này, tại Store ID:' . $store_id . ' . Vui lòng kiểm tra file và thử lại! '
                ]);

                return;
            }

            // kiểm tra store
            if (! $store_id) {
                continue;
            }

            $list_store_id [] = $store_id;

            // end kiểm tra visual


            if ($child_painting_id) {
                // get category_id from child_paitning_id
                $row_painting_child = $QAppCheckshopDetailChildPainting->fetchRow(['id = ?' => $child_painting_id]);
                $row_detail_child = $QAppCheckshopDetailChild->fetchRow(['id = ?' => $row_painting_child['app_checkshop_detail_child_id']]);
                $category_id = $row_detail_child['category_id'];
                //end get category_id

                $exist_picture = $QChangePictureDetail->fetchRow([
                    'child_painting_id = ?' => $child_painting_id,
                    'stage_id = ?' => $stage_id
                ]);
// sau này sẽ  báo lỗi ko tìm thấy hạng mục thay vì bỏ qua
                if (! $category_id) {
                    continue;
                }
// sau này sẽ  báo lỗi ko tìm thấy hạng mục thay vì bỏ qua

                if ($exist_picture) {
                    $QChangePictureDetail->update([
                        'product_id' => $key_visual,
                        'logo_type' => $logo,
                        'note' => $note,
                        'stage_id' => $stage_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id,
                        'store_id' => $store_id,
                        'category_id' => $category_id
                    ], [
                        'child_painting_id = ?' => $child_painting_id,
                        'stage_id = ?' => $stage_id
                    ]);
                } else {
                    if ($key_visual) {
//                        $QChangePictureDetail->insert([
//                            'child_painting_id' => $child_painting_id,
//                            'product_id' => $key_visual,
//                            'logo_type' => $logo,
//                            'note' => $note,
//                            'stage_id' => $stage_id,
//                            'created_at' => date('Y-m-d H:i:s'),
//                            'created_by' => $userStorage->id
//                        ]);

                        // insert
                        $value_insert = '('
                            . $child_painting_id . ','
                            . $key_visual  .  ','
                            . $logo  .  ','
                            . "'" . $note . "'" .  ','
                            . $stage_id   .  ','
                            . $userStorage->id  .  ','
                            . 'Null' .  ','
                            . $store_id .  ','
                            . $category_id
                            . '),';
                        $insert_body_query .= $value_insert;
                    }

                }



            } elseif ($child_id) {  // ID hạng mục và ID chi tiết tranh

                // get category_id from child_paitning_id
                $row_detail_child = $QAppCheckshopDetailChild->fetchRow(['id = ?' => $child_id]);
                $category_id = $row_detail_child['category_id'];
                //end get category_id

// sau này sẽ  báo lỗi ko tìm thấy hạng mục thay vì bỏ qua
                if (! $category_id) {
                    continue;
                }
// sau này sẽ  báo lỗi ko tìm thấy hạng mục thay vì bỏ qua

                $exist_picture = $QChangePictureDetail->fetchRow([
                    'app_checkshop_detail_child_id = ?' => $child_id,
                    'stage_id = ?' => $stage_id
                ]);

                if ($exist_picture) {
                    $QChangePictureDetail->update([
                        'product_id' => $key_visual,
                        'logo_type' => $logo,
                        'note' => $note,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id,
                        'store_id' => $store_id,
                        'category_id' => $category_id
                    ], ['id = ?' => $exist_picture['id']]);
                } else {
                    if ($key_visual) {
//                        $QChangePictureDetail->insert([
//                            'app_checkshop_detail_child_id' => $child_id,
//                            'product_id' => $key_visual,
//                            'logo_type' => $logo,
//                            'note' => $note,
//                            'stage_id' => $stage_id,
//                            'created_at' => date('Y-m-d H:i:s'),
//                            'created_by' => $userStorage->id
//                        ]);

                        // insert
                        $value_insert = '('
                            . 'Null' . ','
                            . $key_visual  .  ','
                            . $logo  .  ','
                            . "'" . $note . "'" .  ','
                            . $stage_id    .  ','
                            . $userStorage->id  .  ','
                            . $child_id .  ','
                            . $store_id .  ','
                            . $category_id
                            . '),';
                        $insert_body_query .= $value_insert;

                    }

                }


            }

        }

        // check if store_id is valid
        $params = [
            'array_store' => $list_store_id,
            'list_area_id' => $this->storage['area_id'],
            'change_picture_stage' => $stage_id
        ];
        
        $list_invalid_store_id = $QAppNoti->getStoreInvalid($params);

        if ($list_invalid_store_id) {
            $list_invalid_store_id = implode(', ', $list_invalid_store_id);
            echo json_encode([
                'status' => 1,
                'message' => 'Shop có ID: ' . $list_invalid_store_id . ' không nằm trong danh sách shop của lần triển khai này hoặc không thuộc khu vực bạn quản lý. Vui lòng kiểm tra và thử lại !'
            ]);
            return;
        }


        if ($insert_body_query) {
            $insert_query = $insert_header_query . $insert_body_query;
            $insert_query = TRIM($insert_query, ',');

            if($insert_query){
                $stmt1 = $db->prepare($insert_query);
                $stmt1->execute();
                $stmt1->closeCursor();
            }
        }



    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

}


$db->commit();

echo json_encode([
   'status' => 0 
]);

