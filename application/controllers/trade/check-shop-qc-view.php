<?php
$QCheckShopQcResult = new Application_Model_CheckShopQcResult();
$QAppCheckshopQc = new Application_Model_AppCheckshopQc();
$QQuarter = new Application_Model_Quarter();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppCheckshopFile = new Application_Model_AppCheckShopFile();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$quarter = intval($this->getRequest()->getParam('quarter'));
$year = intval($this->getRequest()->getParam('year'));
$store_id = intval($this->getRequest()->getParam('store_id'));
$rule_count_check_shop = 2;
$rule_sellout_dealer = 75;

$pre_time = $QQuarter->getPreQuarter($quarter, $year);



if (!$quarter || !$year || !$store_id) {
    echo 'Không đủ thông tin. Vui lòng quay lại';
    die;
}

$params = [
    'quarter' => $quarter,
    'year' => $year,
    'store_id' => $store_id
];

$time_quarter = $QQuarter->getTimeFromTo($quarter, $year);
$list_qc = $QAppCheckshopQc->fetchAll(['status = ?' => 1]);
$list_month = $QQuarter->fetchAll(['quarter = ?' => $quarter]);
$data_local= $QCheckShopQcResult->getResultQc($quarter, $year, $store_id, 1);
$data_leader = $QCheckShopQcResult->getResultQc($quarter, $year, $store_id, 2);
$store_info = $QAppCheckshop->getStoreInfo($store_id);
$store_category = $QAppCheckshop->getCheckshopLock($store_id);
$store_sellout = $QCheckShopQcResult->getSelloutStoreByMonth($store_id, $time_quarter['from_date'], $time_quarter['to_date']);
$dealer_sellout = $QCheckShopQcResult->getSelloutDealer($store_id, $time_quarter['from_date'], $time_quarter['to_date']);
$dealer_parent_info = $QCheckShopQcResult->getDealerParent($store_id);
$list_image = $QAppCheckshopFile->getImageByMonth($store_id, $time_quarter['from_date_time'], $time_quarter['to_date_time']);
$count_eligible_check_shop = $QCheckShopQcResult->getCountEligibleCheckShop($time_quarter['from_date_time'], $time_quarter['to_date_time'], $store_id);

// data last quarter 
$data_local_pre_quarter = $QCheckShopQcResult->getResultQc($pre_time['pre_quarter'], $pre_time['pre_year'], $store_id, 1);
$data_leader_pre_quarter = $QCheckShopQcResult->getResultQc($pre_time['pre_quarter'], $pre_time['pre_year'], $store_id, 2);
$list_month_pre_quarter = $QQuarter->fetchAll(['quarter = ?' => $pre_time['pre_quarter']]);

$this->view->userStorage = $userStorage;
$this->view->list_qc = $list_qc;
$this->view->list_month = $list_month;
$this->view->data_local = $data_local;
$this->view->data_leader = $data_leader;
$this->view->params = $params;
$this->view->store_info = $store_info;
$this->view->store_category = $store_category;
$this->view->store_sellout = $store_sellout;
$this->view->dealer_sellout = $dealer_sellout;
$this->view->list_image = $list_image;
$this->view->count_eligible_check_shop = $count_eligible_check_shop;
$this->view->rule_count_check_shop = $rule_count_check_shop;
$this->view->rule_sellout_dealer = $rule_sellout_dealer;
$this->view->dealer_parent_info = $dealer_parent_info;

$this->view->data_local_pre_quarter = $data_local_pre_quarter;
$this->view->data_leader_pre_quarter = $data_leader_pre_quarter;
$this->view->list_month_pre_quarter = $list_month_pre_quarter;
$this->view->pre_time = $pre_time;