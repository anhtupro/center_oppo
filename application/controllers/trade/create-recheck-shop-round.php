<?php

$id = $this->getRequest()->getParam('id');

$QRecheckShopRound = new Application_Model_RecheckShopRound();

if ($id) {
    $recheckShopRound = $QRecheckShopRound->fetchRow(['id = ?' => $id])->toArray();
    $this->view->recheckShopRound = $recheckShopRound;
}
