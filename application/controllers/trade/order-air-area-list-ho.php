<?php
$stageId  = $this->getRequest()->getParam('stage_id');

$QArea = new Application_Model_Area();
$QOrderAirDetail = new Application_Model_OrderAirDetail();
$QOrderAirArea = new Application_Model_OrderAirArea();
$QOrderAirAreaStatuts = new Application_Model_OrderAirAreaStatus();
$QOrderAirStage = new Application_Model_OrderAirStage();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$params = [
    'area_list' => $this->storage['area_id'],
    'stage_id' => $stageId,
    'type' => 14
];

$listArea = $QArea->getListArea($params);
$listCategory = $QOrderAirDetail->getCategory($stageId);
$listOrderAreaDetail = $QOrderAirArea->getDetail($stageId);


$listStatus = $QOrderAirAreaStatuts->getStatus($params);
$orderAirStage = $QOrderAirStage->fetchRow(['id = ?' => $stageId]);
$appStatusTitle = $QAppStatusTitle->get_cache(14);

$totalDetail = $QOrderAirStage->getTotalDetail($params);

$this->view->listArea = $listArea;
$this->view->listCategory = $listCategory;
$this->view->listOrderAreaDetail = $listOrderAreaDetail;
$this->view->listStatus = $listStatus;
$this->view->stageId = $stageId;
$this->view->orderAirStage = $orderAirStage;
$this->view->title = $this->storage['title'];
$this->view->appStatusTitle = $appStatusTitle;
$this->view->specialUser = [15038];
$this->view->staffId = $this->storage['staff_id'];
$this->view->totalDetail = $totalDetail;



