<?php 

	$category_id   = $this->getRequest()->getParam('category_id');
	$contractor_id = $this->getRequest()->getParam('contractor_id');
	$campaign_id   = $this->getRequest()->getParam('campaign_id');
	
	$QCategory 		= new Application_Model_Category();
	$QContractorOut = new Application_Model_AppContractorOut();

	$params = [
		'campaign_id' 		=> $campaign_id,
		'category_id' 		=> $category_id,
		'contractor_id'		=> $contractor_id
	];

	$category 			= $QCategory->getCategoryByCampaign($params);
	$contractor_out 	= $QContractorOut->getContractorOut($params);

	$this->view->contractor_out = $contractor_out;
	$this->view->category 	= $category;
	$this->view->params   	= $params;

    $this->_helper->layout->setLayout('layout_new');
	



