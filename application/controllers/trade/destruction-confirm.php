<?php 
$id   				= $this->getRequest()->getParam('id');
$submit   			= $this->getRequest()->getParam('submit');

$userStorage 		= Zend_Auth::getInstance()->getStorage()->read();
$QDestruction 		= new Application_Model_Destruction();
$QAppFile 			= new Application_Model_AppFile();
$flashMessenger     = $this->_helper->flashMessenger;

if (!empty($id))
{
	$result = $QDestruction->GetList($id);
}
// --------------------- LẤY THÔNG TIN NHÂN VIÊN------------
$paramsData['staff_id'] =$resultRepair[0]['staff_id'];
$resultInformStaff =$QDestruction->GetInformStaff($paramsData);
// -------------------- BIẾN HIỂN THỊ RA VIEW -----------

$params = array_filter(array(
	'staff_name'				=>$resultInformStaff['firstname'].' '.$resultInformStaff['lastname'],
	'id'						=>$id,
	'categoryname' 				=> $result[0]['categoryname'],
	'storename'					=> $result[0]['storename'],
	'sl'						=> $result[0]['sl'],
	'lydo'						=> $result[0]['lydo'],
	'date'						=>$result[0]['date'],
	'staff_id'					=>$result[0]['staff_id'],
	'status'					=>$result[0]['status'],
	'name_status'				=>$result[0]['name_status'],
	'staff_title'				=>$userStorage->title,
	'status_title'				=>$result[0]['title_status']
));
//EVENT XAC NHAN DE XUAT HUY
if (!empty($submit)) {	
	if($userStorage->title == $result[0]['title_status']){ // KHI ASM XAC NHAN
		$data = array_filter(array(
			'status'	=> $result[0]['status']+1
		));
		try{
			$result = $QDestruction->update ($data,'id = '.$id);
			$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công');
			$this->_redirect(HOST . 'trade/destruction-confirm?id='.$id);
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Xác nhận không thành công');
			$this->_redirect(HOST . 'trade/destruction-edit?id='.$id);
		}
	}
}
$params['parrent_id'] 	= $id;
$dataAll 				= $QAppFile->GetListDestruction($params);
$this->view->dataAll 	= $dataAll;
// ---------------hien thi thong bao ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){ 
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}
$this->view->params = $params;
?>