<?php
$checkshopId = $this->getRequest()->getParam('checkshop_id');  // is_lock = 1
$dev = $this->getRequest()->getParam('dev');

$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QCategoryInventoryType = new Application_Model_CategoryInventoryType();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QCategoryMaterial = new Application_Model_CategoryMaterial();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();
$QStoreFacadeSize = new Application_Model_StoreFacadeSize();
$QStandardImage = new Application_Model_StandardImage();
$QChangePictureStage = new Application_Model_ChangePictureStage();
$QChangePictureDetail = new Application_Model_ChangePictureDetail();
$QPhoneProduct = new Application_Model_PhoneProduct();
$QCategory = new Application_Model_Category();
$QStoreCategoryDeployment = new Application_Model_StoreCategoryDeployment();

$checkshop = $QAppCheckshop->getShopInfo($checkshopId);
$storeInfo = $QAppCheckshop->getStoreInfo($checkshop['store_id']);
$lastCheckshop = $QAppCheckshopDetailChild->getLastCheckshop($checkshop['store_id']);

$params['checkshop_id'] = $checkshopId;

$checkshopDetailChild = $QAppCheckshopDetailChild->getDetailChild($checkshopId);
$checkshopDetailParent = $QAppCheckshopDetailChild->getDetailParent($checkshopId);
$categoryType = $QCategoryInventoryType->getType();
$listMaterial = $QCategoryMaterial->fetchAll();
$listTypeChild = $QCategoryInventoryType->fetchAll(['is_child = ?' => 1]);
$listCategoryPainting = $QAppCheckshopDetailChildPainting->getPainting($params);

// img
$img_check = $QAppCheckshop->getImg($lastCheckshop['id']);
$img_different = $QAppCheckshop->getImgDifferent($lastCheckshop['id']);
$img_different_list = $QAppCheckshop->getImgDifferentList($lastCheckshop['created_at'], $lastCheckshop['store_id'], $lastCheckshop['id']);
$img_check_list = $QAppCheckshop->getListImg($lastCheckshop['created_at'], $lastCheckshop['store_id'], $lastCheckshop['id']);

// store facade size
if ($storeInfo['id']) {
    $listFacadeSize = $QStoreFacadeSize->fetchAll([  // kich thước banner
        'store_id = ?' => $storeInfo['id'],
        'type = ?' => 1
    ]);

    $listAppearanceSize = $QStoreFacadeSize->fetchAll([  // kích thước mặt tiền
        'store_id = ?' => $storeInfo['id'],
        'type = ?' => 2
    ]);

    $list_standard_image = $QStandardImage->fetchAll(['store_id = ?' => $storeInfo['id']]);
}

// đợt thay tranh
$change_picture_stage = $QChangePictureStage->getValidStage();
if ($change_picture_stage) {
    $store_is_valid_change_picture = $QChangePictureStage->checkStoreValid($storeInfo['id'], $change_picture_stage['id']);
}


// lịch sử thay tranh
$history_change_picture = $QChangePictureStage->getHistoryChangePicture($checkshopId);
$history_change_picture_child = $QChangePictureStage->getHistoryChangePictureChild($checkshopId);

if ($dev == 1) {
    echo '<pre>';
    print_r($history_change_picture);
    die;
}

// thay tranh hiện tại
$current_change_picture = $QChangePictureStage->getCurrentChangePicture($checkshopId);
$current_change_picture_child = $QChangePictureStage->getCurrentChangePictureChild($checkshopId);

// danh sách điện thoại
if ($change_picture_stage) {
    $list_phone_product = $QPhoneProduct->getProductChangePicture($change_picture_stage['id']);
}

// danh sách hạng mục có thể đặt tại shop
$list_category_deployment = $QCategory->fetchAll(['category_deployment = ?' => 1]);
$list_current_deployment = $QStoreCategoryDeployment->get($storeInfo['id']);

$this->view->checkshopDetailChild = $checkshopDetailChild;
$this->view->checkshopDetailParent = $checkshopDetailParent;
$this->view->categoryType = $categoryType;
$this->view->storeInfo = $storeInfo;
$this->view->checkshop = $checkshop;
$this->view->listMaterial = $listMaterial;
$this->view->listTypeChild = $listTypeChild;
$this->view->listCategoryPainting = $listCategoryPainting;
$this->view->lastCheckshop = $lastCheckshop;


$this->view->img_different = $img_different;
$this->view->img_different_list = $img_different_list;
$this->view->img_check = $img_check;
$this->view->img_check_list = $img_check_list;

$this->view->listFacadeSize = $listFacadeSize;
$this->view->listAppearanceSize = $listAppearanceSize;
$this->view->list_standard_image = $list_standard_image;

$this->view->change_picture_stage = $change_picture_stage;
$this->view->history_change_picture = $history_change_picture;
$this->view->current_change_picture = $current_change_picture;

$this->view->history_change_picture_child = $history_change_picture_child;
$this->view->current_change_picture_child = $current_change_picture_child;
$this->view->store_is_valid_change_picture = $store_is_valid_change_picture;


$this->view->list_phone_product = $list_phone_product;
$this->view->list_category_deployment = $list_category_deployment;
$this->view->list_current_deployment = $list_current_deployment;