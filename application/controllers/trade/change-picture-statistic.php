<?php
$stage_id = $this->getRequest()->getParam('stage_id');
$export_store_finish = $this->getRequest()->getParam('export_store_finish');

$params = [
    'stage_id' => $stage_id,
    'area_list' => $this->storage['area_id']
];

$QCheckPosmStage = new Application_Model_CheckPosmStage();
$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QCheckPosmDetail = new Application_Model_CheckPosmDetail();

$QChangePictureStage = new Application_Model_ChangePictureStage();
$QChangePictureDetail = new Application_Model_ChangePictureDetail();

$stage = $QChangePictureStage->fetchRow(['id = ?' => $stage_id]);
//$list_category = $QCategory->fetchAll(['has_painting = 1 OR is_picture = 1'])->toArray();
$list_category = $QChangePictureDetail->getListCategory($stage_id);


$list_area = $QArea->getListArea($params);

if ($export_store_finish) {
    $QChangePictureDetail->exportStoreFinish($params);
}

// theo số lượng hạng mục
$statistic_category = $QChangePictureDetail->getStatisticCategory($params);
$statistic_category_all = $QChangePictureDetail->getStatisticCategoryAll($params);// tất cả khu vực quản lý

// theo số lượng hạng mục


// theo số lượng shop đã hoàn thành
// theo khu vực
$statistic_store_area = $QChangePictureDetail->getAllStoreEachArea($params);
$statistic_store_not_finish_area = $QChangePictureDetail->getAllStoreEachArea($params, true);


// tất cả
$statistic_store_all = $QChangePictureDetail->getAllStore($params);
$statistic_store_not_finish_all = $QChangePictureDetail->getAllStore($params, true);
// end theo khu vực


// biểu đồ shop hoàn thành
foreach ($statistic_store_area as $value) {
    $count_store_finish = $value['count_store'] - $statistic_store_not_finish_area [$value['area_id']] ['count_store'];
    $count_all_store = $value['count_store'];

    $percent_finish_area [$value['area_id']] ['area_name'] = $value['area_name'];
    $percent_finish_area [$value['area_id']] ['count_store_all'] = $count_all_store;
    $percent_finish_area [$value['area_id']] ['count_store_finish'] = $count_store_finish;
    $percent_finish_area [$value['area_id']] ['percent_finish'] = round($count_store_finish * 100 / $count_all_store);
}
// sorting array by percent
usort($percent_finish_area, function($a, $b) {
    return  $b['percent_finish'] - $a['percent_finish'];
});

// end biểu đồ shop hoàn thành




$this->view->list_category = $list_category;
$this->view->list_area = $list_area;
$this->view->stage = $stage;

$this->view->statistic_category = $statistic_category;
$this->view->statistic_category_all = $statistic_category_all;

$this->view->statistic_store_area = $statistic_store_area;
$this->view->statistic_store_not_finish_area = $statistic_store_not_finish_area;

$this->view->statistic_store_all = $statistic_store_all;
$this->view->statistic_store_not_finish_all = $statistic_store_not_finish_all;

//// chart
$this->view->percent_finish_area = json_encode($percent_finish_area);


