<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$air_details_id = $this->getRequest()->getParam('air_details_id');
$contract_number = $this->getRequest()->getParam('contract_number');

$QAirDetails = new Application_Model_AirDetails();

$QAirDetails->update([
    'contract_number' => $contract_number ? TRIM($contract_number) : ''
], ['id = ?' => $air_details_id]);

echo json_encode([
    'status' => 0
]);