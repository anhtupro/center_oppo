<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$addition_id = $this->getRequest()->getParam('addition_id');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QAdditionCategory = new Application_Model_AdditionCategory();
$QAdditionCategoryDetail = new Application_Model_AdditionCategoryDetail();
$QAdditionCategoryHistory = new Application_Model_AdditionCategoryHistory();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$addition = $QAdditionCategory->fetchRow(['id = ?' => $addition_id]);



$has_permission = $QAppStatusTitle->checkPermission($userStorage->title, $addition['status'], 19);
if (!$has_permission) {
    echo json_encode([
        'status' => 1,
        'Không có quyền thao tác hoạt động này !'
    ]);

    return;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();
// Hủy đề xuất
if ($remove) {
    $QAdditionCategory->update([
        'remove' => 1,
        'removed_by' => $userStorage->id,
        'removed_at' => date('Y-m-d H:i:s'),
        'remove_note' => $remove_note

    ], ['id = ?' => $addition_id]);

    $QAdditionCategoryHistory->insert([
       'addition_id' => $addition_id,
       'created_at' =>  date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id ,
        'type' => 2,
        'status' => $addition['status']
    ]);

    $db->commit();

    echo json_encode([
        'status' => 0
    ]);

    return;
}
// end hủy đề xuất


// duyêt
$addition_detail = $QAdditionCategoryDetail->fetchAll(['addition_id = ?' => $addition_id]);
$QAdditionCategory->insertToShop($addition['store_id'], $addition_detail);


$QAdditionCategory->update([
    'status' => 2
], ['id = ?' => $addition_id]);

$QAdditionCategoryHistory->insert([
    'addition_id' => $addition_id,
    'created_at' =>  date('Y-m-d H:i:s') ,
    'created_by' => $userStorage->id,
    'type' => 1,
    'status' => $addition['status']
]);

$db->commit();

echo json_encode([
    'status' => 0
]);

return;

// end duyệt
