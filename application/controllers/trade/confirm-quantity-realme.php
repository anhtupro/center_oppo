<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$checkshop_id = $this->getRequest()->getParam('checkshop_id');
$store_id = $this->getRequest()->getParam('store_id');

$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$db = Zend_Registry::get('db');
$db->beginTransaction();

$array_category_realme = $QAppCheckshop->getCheckshopDetails($checkshop_id, 2); // lấy ra HM realme

$checkshop_lock = $QAppCheckshop->fetchRow([
    'store_id = ?' => $store_id,
    'is_lock = ?' => 1
]);

if (!$checkshop_lock) {
    $checkshop_lock_id = $QAppCheckshop->insert([
        'store_id' => $store_id,
        'is_lock' => 1,
        'created_by' => $userStorage->id,
        'created_at' => date('Y-m-d H:i:s')
    ]);
} else {
    $checkshop_lock_id = $checkshop_lock['id'];
}

$QAppCheckshop->deleteCategoryRealme($checkshop_lock_id);

// insert hạng mục realme chốt mới vào

foreach ($array_category_realme as $category) {

    $category_id = $category['category_id'];
    $quantity = $category['quantity'];

    if ($category_id) {
        $QAppCheckshopDetail->insert([
            'checkshop_id' => $checkshop_lock_id,
            'category_id' => $category_id,
            'quantity' => $quantity
        ]);

        for ($i = 0; $i < $quantity; $i++) {
            $QAppCheckshopDetailChild->insert([
                'checkshop_id' => $checkshop_lock_id,
                'category_id' => $category_id,
                'quantity' => 1
            ]);
        }
    }


}

$db->commit();

echo json_encode([
    'status' => 0
]);