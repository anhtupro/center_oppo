<?php

$id = $this->getRequest()->getParam('id');
$category_id = $this->getRequest()->getParam('category_id');
$repair_details_type = $this->getRequest()->getParam('repair_details_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppStatus = new Application_Model_AppStatus();
$QContructors = new Application_Model_Contructors();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QDestruction = new Application_Model_Destruction();
$QTransfer = new Application_Model_Transfer();
$QTransferDetails = new Application_Model_TransferDetails();
$QRepairDetailsType = new Application_Model_RepairDetailsType();
$QRepairDetailsTp = new Application_Model_RepairDetailsTp();
$QRepairPart = new Application_Model_RepairPart();
$QRepairType = new Application_Model_RepairType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QTransferQuotation = new Application_Model_TransferQuotation();
$QInventoryArea = new Application_Model_AirInventoryArea();
$QTransferDetailsChild = new Application_Model_TransferDetailsChild();
$QTransferImage = new Application_Model_TransferImage();
$QTransferPrice = new Application_Model_TransferPrice();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();

$flashMessenger       = $this->_helper->flashMessenger;



$data_repair_type = $QRepairType->fetchAll();
$repair_type = [];
foreach($data_repair_type as $key=>$value){
    $repair_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$app_status = $QAppStatus->get_cache(6);

$app_status_title_cache = $QAppStatusTitle->get_cache(6);
$app_status_title = [];
foreach($this->storage['title_list'] as $key=>$value){
    $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);
}


$transfer = $QTransfer->fetchRow(['id = ?'=> $id]);

$params = array(
    'id' => $id,
    'repair_id' => $id,
    'transfer_id' => $id,
    'version' => $transfer['version'],
    'area_from' => $transfer['area_from'],
    'store_from' => $transfer['store_from']
);

$contructors = $QContructors->fetchAll();
$transfer_details = $QTransferDetails->getTransferDetails($params);
foreach ($transfer_details as $detail) {
    if ($detail['transfer_type'] == 1) { // nhà thầu điều chuyển
        $transfer_details_contractor [] = $detail;

    }
    if ($detail['transfer_type'] == 2) { // khu vực điều chuyển
        $transfer_details_area [] = $detail;

    }
}


foreach ($transfer_details as $detail) {
    $listAppCheckshopDetailId [] = $detail['app_checkshop_detail_child_id'];
}
$params['list_app_checkshop_detail_child_id'] = $listAppCheckshopDetailId;
$listCategoryPainting = $QAppCheckshopDetailChildPainting->getPainting($params);

$repair_details_type = $QRepairDetailsType->getRepairDetailsType($params);
$repair_part = $QRepairDetailsType->getRepairDetailsPart($params);


$params['price_type'] = 0;
$result_transfer_price = $QTransferPrice->getTransferQuotation($params);
$list_price = $result_transfer_price['list_price'];
$total_price = $result_transfer_price['total_price'];

$params['price_type'] = 2;
$result_transfer_price_last = $QTransferPrice->getTransferQuotation($params);
$list_price_last = $result_transfer_price_last['list_price'];
$total_price_last = $result_transfer_price_last['total_price'];


$params['price_type'] = 4;
$result_transfer_price_review = $QTransferPrice->getTransferQuotation($params);
$list_price_review = $result_transfer_price_review['list_price'];
$total_price_review = $result_transfer_price_review['total_price'];


$transfer = $QTransfer->getTransfer($params);


if ($transfer['store_from_id']) {
    $sellout = $QAppCheckshop->getSellout($transfer['store_from_id']);

    $params['store_id'] = $transfer['store_from_id'];
    $app_checkshop = $QAppCheckshop->getInfoCheckshop($params);
}
if ($transfer['store_to_id']) {
    $sellout_to = $QAppCheckshop->getSellout($transfer['store_to_id']);

    $params['store_id'] = $transfer['store_to_id'];
    $app_checkshop_to = $QAppCheckshop->getInfoCheckshop($params);
}
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if ($transfer['area_from']) {
    $warehouseFromInfo = $QInventoryArea->getInfoWarehouseTotal($transfer['area_from']);
//    $params['area_id'] = $transfer['area_from'];
//    $warehouseFromInfo = $QAppCheckshopDetailChild->getInfoWarehouseTotal($transfer['area_from']);
}

$listImage = $QTransferImage->getImage($id);

$this->view->params = $params;
$this->view->part = $QRepairPart->fetchAll();
$this->view->category = $result;
$this->view->repair_type = $repair_type;
$this->view->transfer_details = $transfer_details;
$this->view->transfer = $transfer;
$this->view->listImage = $listImage;
$this->view->listCategoryPainting = $listCategoryPainting;

$this->view->sellout = $sellout;
$this->view->sellout_to = $sellout_to;
$this->view->app_checkshop = $app_checkshop;
$this->view->app_checkshop_to = $app_checkshop_to;
$this->view->app_status = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->contructors = $contructors;
$this->view->repair_part = $repair_part;
$this->view->repair_details_type = $repair_details_type;
$this->view->list_price = $list_price;
$this->view->total_price = $total_price;
$this->view->list_price_last = $list_price_last;
$this->view->total_price_last = $total_price_last;
$this->view->warehouseFromInfo = $warehouseFromInfo;
$this->view->transfer_details_contractor = $transfer_details_contractor;
$this->view->transfer_details_area = $transfer_details_area;
$this->view->list_price_review = $list_price_review;
$this->view->total_price_review = $total_price_review;

$this->view->staff_id = $userStorage->id;
$this->view->special_user = [15038];

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>