<?php
$id   = $this->getRequest()->getParam('id');
$status   = $this->getRequest()->getParam('status');
$submit	    = $this->getRequest()->getParam('submit');
$price	    = $this->getRequest()->getParam('price');
$priceformat	    = $this->getRequest()->getParam('priceformat');
$contructors	    = $this->getRequest()->getParam('contructors');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QDestruction = new Application_Model_Destruction();
$QStore = new Application_Model_Store();
$QRepair = new Application_Model_Repair();
$QAppFile = new Application_Model_AppFile();
$QContructors = new Application_Model_Contructors();
$QQuotation = new Application_Model_Quotation();

$currentTime= new DateTime(date('Y-m-d'));
$flashMessenger       = $this->_helper->flashMessenger;
$QAppStatusTitle 		= new Application_Model_AppStatusTitle();
$title =$userStorage->title;
$cache = $QAppStatusTitle->get_cache(5);
$cache_free = $QAppStatusTitle->get_cache(4);
$this->view->title_status = $cache[$title];
$this->view->title_status_free = $cache_free[$title];

$resultRepair = $QRepair->GetList($id);
$status = $resultRepair[0]['status'];
$type = $resultRepair[0]['type'];
$quotation_id=$resultRepair[0]['quotation_id'];
if ($submit && $type == 1){

 // -------------------BAO HANH MIEN PHI------------------
	if($userStorage->title == $resultRepair[0]['staff_title'] ){
		try{
			$data = array_filter(array(
				'status'=>$status+1,
				'update_at'=>date('Y-m-d H:i:s'),
				'contructors_id'=>(!empty($contructors)?$contructors:0)
			));
			$result =$QRepair->update ($data,'id = '.$id);
			$flashMessenger->setNamespace('success')->addMessage('Trade xác nhận thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);

		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Xác nhận Trade không thành công');
			$this->_redirect(HOST . 'Trade/repair-confirm?id='.$id);
		}
	}
}else if ($submit && $type ==2){ // -----------------------SỬA CHỮA CÓ PHÍ ---------------------
	if($userStorage->title == $resultRepair[0]['staff_title'] && $status == 19 ){
		// -----------------KHI TRADE LOCAL XÁC NHẬN ----------------------
		try{
			$data = array_filter(array(
				'status'=>$status+1,
				'contructors_id'=>$contructors,
				'update_at'=>date('Y-m-d H:i:s')
				
			));
			$result =$QRepair->update ($data,'id = '.$id);
			$flashMessenger->setNamespace('success')->addMessage('Trade Local chọn nhà thầu thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Trade Local chọn nhà thầu không thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}
	}else if ($userStorage->title ==  $resultRepair[0]['staff_title'] && $status == 20 ){
		// -----------------KHI TRADE LOCAL BÁO GIÀ----------------------
		// ------------------KIỂM TRA THÔNG TIN GIÁ TIỀN -------------------
		$price = str_replace(",","",$price);
		$price =intval ($price);
		if (!is_numeric($price) || $price ==0){
			$flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đúng giá tiền');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}

		try{
			// ----------------------- UPLOAD FILE PDF ---------------------

			$upload = new Zend_File_Transfer();
			$upload->setOptions(array('ignoreNoFile'=>true));
			if (function_exists('finfo_file'))
				$upload->addValidator('MimeType', false, array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/pdf','application/zip','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ));

			$upload->addValidator('Extension', false, 'doc,docx,pdf,xlsx');
			$upload->addValidator('Size', false, array('max' => '3MB'));
			$upload->addValidator('ExcludeExtension', false, 'php,sh');
			$files = $upload->getFileInfo();

			if($files['pdf']['error']==0){
				$data_file = array();
				
				$fileInfo = (isset($files['pdf']) and $files['pdf']) ? $files['pdf'] : null;
				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
				DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'file' . DIRECTORY_SEPARATOR.
				$id;
				if (!is_dir($uploaded_dir))
					@mkdir($uploaded_dir, 0777, true);
				$upload->setDestination($uploaded_dir);

            //Rename
				$old_name = $fileInfo['name'];
				$tExplode = explode('.', $old_name);
				$extension = end($tExplode);
				$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
				
				$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
				$r = $upload->receive(array('pdf'));
				if($r){
					$data_file['file_name'] = $new_name;
				}
				else{
					$messages = $upload->getMessages();
					foreach ($messages as $msg)
						throw new Exception($msg);
				}
				// --------------INSERT HINH ANH LEN DATABASE-------------
				$data_file['parrent_id'] = $id;
				$data_file['type']=5;
				$result =$QAppFile->insert($data_file);
			}

			
			
			$dataQuota['name']= $resultRepair[0]['categoryname'];
			$dataQuota['price']=$price;
			$resultQuota=$QQuotation->insert($dataQuota);

			$data = array_filter(array(
				'status'=>22,
				'quotation_id'=>$resultQuota,
				'update_at'=>date('Y-m-d H:i:s')
			));
			$result =$QRepair->update ($data,'id = '.$id);
			
			$flashMessenger->setNamespace('success')->addMessage('Trade Local báo giá thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Trade Local báo giá không thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}
	}
	// else if ($userStorage->title == TRADE_MARKETING_EXECUTIVE && $status == 21 ){
	// 	//-----------------------------TRADE LOCAL XÁC NHẬN ---------------------
	// 	try{
	// 		//nếu có sửa giá
	// 		$price = str_replace(",","",$price);
	// 		$price =intval ($price);
	// 		if (!is_numeric($price) || $price ==0){
	// 			$flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đúng giá tiền');
	// 			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
	// 		}
	// 		$where_update= $QQuotation->getAdapter()->quoteInto('id = ?',$quotation_id);
	// 		$dataQuota['name']= $resultRepair[0]['categoryname'];
	// 		$dataQuota['price']=$price;
	// 		$QQuotation->update($dataQuota,$where_update);

	// 		//upload file báo giá
	// 		$upload = new Zend_File_Transfer();
	// 		$upload->setOptions(array('ignoreNoFile'=>true));
	// 		if (function_exists('finfo_file'))
	// 			$upload->addValidator('MimeType', false, array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/pdf','application/zip','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ));

	// 		$upload->addValidator('Extension', false, 'doc,docx,pdf,xlsx');
	// 		$upload->addValidator('Size', false, array('max' => '3MB'));
	// 		$upload->addValidator('ExcludeExtension', false, 'php,sh');
	// 		$files = $upload->getFileInfo();
	// 		if($files['pdf']['name']!=""){
	// 			if($files['pdf']['error']==0){
	// 				$data_file = array();
					
	// 				$fileInfo = (isset($files['pdf']) and $files['pdf']) ? $files['pdf'] : null;
	// 				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
	// 				DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'file' . DIRECTORY_SEPARATOR.
	// 				$id;
	// 				if (!is_dir($uploaded_dir))
	// 					@mkdir($uploaded_dir, 0777, true);
	// 				$upload->setDestination($uploaded_dir);

	//             //Rename
	// 				$old_name = $fileInfo['name'];
	// 				$tExplode = explode('.', $old_name);
	// 				$extension = end($tExplode);
	// 				$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
					
	// 				$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
	// 				$r = $upload->receive(array('pdf'));
	// 				if($r){
	// 					$data_file['file_name'] = $new_name;
	// 				}
	// 				else{
	// 					$messages = $upload->getMessages();
	// 					foreach ($messages as $msg)
	// 						throw new Exception($msg);
	// 				}
	// 				// --------------INSERT HINH ANH LEN DATABASE-------------
	// 				$where_file=array();
	// 				$where_file[]= $QAppFile->getAdapter()->quoteInto('parrent_id = ?',$id);
	// 				$where_file[]= $QAppFile->getAdapter()->quoteInto('type = ?',5);
	// 				$QAppFile->update($data_file,$where_file);
	// 			}
	// 		}

			
	// 		//confirm
	// 		$data = array_filter(array(
	// 			'status'=>22,
	// 			'update_at'=>date('Y-m-d H:i:s')
	// 		));
	// 		$result =$QRepair->update ($data,'id = '.$id);
	// 		$flashMessenger->setNamespace('success')->addMessage('Trade Local kiểm tra thành công');
	// 		$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
	// 	}catch (exception $e) {
	// 		$flashMessenger->setNamespace('error')->addMessage('Trade Local kiểm tra không thành công');
	// 		$this->_redirect(HOST . 'Trade/repair-confirm?id='.$id);
	// 	}
	// }
	else if ($userStorage->title == TRADE_MARKETING_EXECUTIVE && $status == 22 ){
		//-----------------------------TRADE LEADER XÁC NHẬN ---------------------
		try{
			//nếu có sửa giá
			$price = str_replace(",","",$price);
			$price =intval ($price);
			if (!is_numeric($price) || $price ==0){
				$flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đúng giá tiền');
				$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
			}
			$where_update= $QQuotation->getAdapter()->quoteInto('id = ?',$quotation_id);
			$dataQuota['name']= $resultRepair[0]['categoryname'];
			$dataQuota['price']=$price;
			$QQuotation->update($dataQuota,$where_update);

			//upload file báo giá
			$upload = new Zend_File_Transfer();
			$upload->setOptions(array('ignoreNoFile'=>true));
			if (function_exists('finfo_file'))
				$upload->addValidator('MimeType', false, array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/pdf','application/zip','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ));

			$upload->addValidator('Extension', false, 'doc,docx,pdf,xlsx');
			$upload->addValidator('Size', false, array('max' => '3MB'));
			$upload->addValidator('ExcludeExtension', false, 'php,sh');
			$files = $upload->getFileInfo();
			if($files['pdf']['name']!=""){
				if($files['pdf']['error']==0){
					$data_file = array();
					
					$fileInfo = (isset($files['pdf']) and $files['pdf']) ? $files['pdf'] : null;
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
					DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'file' . DIRECTORY_SEPARATOR.
					$id;
					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);
					$upload->setDestination($uploaded_dir);

	            //Rename
					$old_name = $fileInfo['name'];
					$tExplode = explode('.', $old_name);
					$extension = end($tExplode);
					$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
					
					$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
					$r = $upload->receive(array('pdf'));
					if($r){
						$data_file['file_name'] = $new_name;
					}
					else{
						$messages = $upload->getMessages();
						foreach ($messages as $msg)
							throw new Exception($msg);
					}
					// --------------INSERT HINH ANH LEN DATABASE-------------
					$where_file=array();
					$where_file[]= $QAppFile->getAdapter()->quoteInto('parrent_id = ?',$id);
					$where_file[]= $QAppFile->getAdapter()->quoteInto('type = ?',5);
					$QAppFile->update($data_file,$where_file);
				}
			}
			$flashMessenger->setNamespace('success')->addMessage('Cập nhật báo giá thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Cập nhật báo giá không thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}
	}
	else if ($userStorage->title == TRADE_MARKETING_LEADER && $status == 22 ){
		//-----------------------------TRADE LEADER XÁC NHẬN ---------------------
		try{
			
			$data = array_filter(array(
				'status'=>23,
				'update_at'=>date('Y-m-d H:i:s')
			));
			$result =$QRepair->update ($data,'id = '.$id);
			$flashMessenger->setNamespace('success')->addMessage('Trade Leader xác nhận thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Xác nhận Trade Leader không thành công');
			$this->_redirect(HOST . 'Trade/repair-confirm?id='.$id);
		}
	}
	else if ($userStorage->title == TRADE_MARKETING_EXECUTIVE && $status == 23 ){
		//-----------------------------TRADE LOCAL XÁC NHẬN ---------------------
		try{
			//nếu có sửa giá
			$price = str_replace(",","",$price);
			$price =intval ($price);
			if (!is_numeric($price) || $price ==0){
				$flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đúng giá tiền');
				$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
			}
			$where_update= $QQuotation->getAdapter()->quoteInto('id = ?',$quotation_id);
			$dataQuota['name']= $resultRepair[0]['categoryname'];
			$dataQuota['price']=$price;
			$QQuotation->update($dataQuota,$where_update);

			//upload file báo giá
			$upload = new Zend_File_Transfer();
			$upload->setOptions(array('ignoreNoFile'=>true));
			if (function_exists('finfo_file'))
				$upload->addValidator('MimeType', false, array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/pdf','application/zip','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ));

			$upload->addValidator('Extension', false, 'doc,docx,pdf,xlsx');
			$upload->addValidator('Size', false, array('max' => '3MB'));
			$upload->addValidator('ExcludeExtension', false, 'php,sh');
			$files = $upload->getFileInfo();
			if($files['pdf']['name']!=""){
				if($files['pdf']['error']==0){
					$data_file = array();
					
					$fileInfo = (isset($files['pdf']) and $files['pdf']) ? $files['pdf'] : null;
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
					DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'file' . DIRECTORY_SEPARATOR.
					$id;
					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);
					$upload->setDestination($uploaded_dir);

	            //Rename
					$old_name = $fileInfo['name'];
					$tExplode = explode('.', $old_name);
					$extension = end($tExplode);
					$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
					
					$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
					$r = $upload->receive(array('pdf'));
					if($r){
						$data_file['file_name'] = $new_name;
					}
					else{
						$messages = $upload->getMessages();
						foreach ($messages as $msg)
							throw new Exception($msg);
					}
					// --------------INSERT HINH ANH LEN DATABASE-------------
					$where_file=array();
					$where_file[]= $QAppFile->getAdapter()->quoteInto('parrent_id = ?',$id);
					$where_file[]= $QAppFile->getAdapter()->quoteInto('type = ?',5);
					$QAppFile->update($data_file,$where_file);
				}
			}
			//upload ảnh nghiệm thu
			if(!empty($_FILES['img']['name']))
            {
                $total = count($_FILES['img']['name']);
                for($i=0; $i<$total; $i++) {

                    //Upload hình ?nh
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
				DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
				DIRECTORY_SEPARATOR . 'repair' . DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . $id;

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);

                    $tmpFilePath = $_FILES['img']['tmp_name'][$i];

                    if ($tmpFilePath != ""){
                        $old_name   = $_FILES['img']['name'][$i];
                        $tExplode   = explode('.', $old_name);
                        $extension  = end($tExplode);
                        $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

                        $newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;

                        if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                            $url         = 'photo' .
                                DIRECTORY_SEPARATOR . 'repair' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
                        }
                        else{
                            $url = NULL;
                        }
                    }
                    else{
                        $url = NULL;
                    }
                $details = [
                    
					'type'	   => 6,
					'file_name'=>$new_name,
					'parrent_id' =>$id
                ];
                $QAppFile->insert($details);
            }
        }else{
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Upload ảnh nghiệm thu thất bại!");
            $this->_redirect(HOST . 'Trade/repair-confirm?id='.$id);
        }
        //confirm
			$data = array_filter(array(
				'status'=>24,
				'update_at'=>date('Y-m-d H:i:s')
			));
			$result =$QRepair->update ($data,'id = '.$id);
			$flashMessenger->setNamespace('success')->addMessage('Trade Local xác nhận thành công');
			$this->_redirect(HOST . 'trade/repair-confirm?id='.$id);
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Xác nhận Trade Local không thành công');
			$this->_redirect(HOST . 'Trade/repair-confirm?id='.$id);
		}
	}
}

// -------------------LẤY TÊN FILE PDF-------------------
$parrent_id['parrent_id'] =$id;
$dataAll = $QAppFile->GetListRepair($parrent_id);
foreach ($dataAll as $key => $value) {
	$checkname =strpos($value['file_name'],".docx");
	if($checkname){
		$filepdf =$value['file_name'];
		unset($dataAll[$key]);
	}
}
// --------------------- LẤY THÔNG TIN NHÂN VIÊN------------
$paramsData['staff_id'] =$resultRepair[0]['staff_id'];
$resultInformStaff =$QDestruction->GetInformStaff($paramsData);
//----------------------LẤY ẢNH NGHIỆM THU---------------

$parrent_id['parrent_id'] =$id;
$img = $QAppFile->GetImgRepair($parrent_id);
//----------------------LẤY FILE DOCUMENT----------------
$document = $QAppFile->GetDocument($parrent_id);

// -------------------- BIẾN HIỂN THỊ RA VIEW -----------
$params = array_filter(array(
	'id'=>$id,
	'categoryname' => $resultRepair[0]['categoryname'],
	'storename'=> $resultRepair[0]['storename'],
	'note'=> $resultRepair[0]['note'],
	'ngaytao'=> $resultRepair[0]['ngaytao'],
	'ngaycapnhat'=> $resultRepair[0]['ngaycapnhat'],
	'staff_create_name'=>$resultInformStaff['firstname'].' '. $resultInformStaff['lastname'],
	'price'=>number_format( $resultRepair[0]['price']),
	'contructors'=> $resultRepair[0]['contructors'],
	'status_name'=> $resultRepair[0]['status_name'],
	'typename'=>($type ==1) ? "Bảo hành miễn phí" : "Sửa chữa có tính phí",
	'filepdf'=>$filepdf ,
	'status'=>$status,
	'submit'=>$submit,
	'type'=>$type,
	'priceformat',$priceformat,
	'staff_title'=>$userStorage->title,
	'status_title'=>$resultRepair[0]['staff_title'],
	'img'=>$img,
	'title'	=>$title,
	'document'	=>$document[0]['file_name']
));
$this->view->params = $params;

//------------------ RETURN NHÀ THẦU -------------
$result = $QContructors->getAll();
$this->view->contructors = $result;

// -------------------RETURN HINH ANH ------------------

$this->view->dataAll = $dataAll;

	// -------------RETURN CATEGORY ---------------
$result = $QCategory->GetAll();
$this->view->category = $result;
	// -------------RETURN STORE ---------------
$result = $QStore->GetAll($params);
$this->view->store = $result;
	// ---------------HIEN THI THONG BAO ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}
?>