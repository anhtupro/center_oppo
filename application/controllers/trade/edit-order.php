<?php 
	
	$campaign_id = $this->getRequest()->getParam('campaign_id');
	$order_id 	 = $this->getRequest()->getParam('order_id');

	$QCampaign   = new Application_Model_Campaign();
	$QStore 	 = new Application_Model_Store();
	$QAppOrder 	 = new Application_Model_AppOrder();
	$QAppOrderDetails = new Application_Model_AppOrderDetails();
	$QAppStatus  = new Application_Model_AppStatus();

	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id 	 = $userStorage->id;
	$title 		 =  $userStorage->title;
    $group 		 =  $userStorage->group_id;

	//Kiểm tra user (SALE) đã tạo đơn chưa, nếu tạo rồi thì sử dụng trên đơn đó
	if(empty($order_id)){

		if($title == SALES_TITLE){
			$where[] = $QAppOrder->getAdapter()->quoteInto('created_by = ?', $staff_id);
			$where[] = $QAppOrder->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
			$app_order_check = $QAppOrder->fetchRow($where);
			if($app_order_check){
				$back_url = HOST.'trade/create-order?campaign_id='.$campaign_id.'&order_id='.$app_order_check['id'];
				$this->redirect($back_url);
			}
		}
		elseif(in_array($title, [SALE_SALE_ASM, TRADE_MARKETING_EXECUTIVE, TRADE_MARKETING_LEADER, TRADE_MARKETING_SUPERVISOR])){
			$back_url = HOST.'trade/confirm-order?campaign_id='.$campaign_id;
			$this->redirect($back_url);
		}

		
	}
	//END

	$params = [
		'campaign_id' => $campaign_id,
		'order_id'	  => $order_id,
		'staff_id'    => $staff_id
	];


	$where = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign = $QCampaign->fetchRow($where);

	$category = $QCampaign->getCategory($params);

	$list_store = $QCampaign->getListStore($params);


	if($order_id){
		$order_details = $QAppOrderDetails->getDataDetails($params);

		$where = $QAppOrder->getAdapter()->quoteInto('id = ?', $order_id);
		$app_order = $QAppOrder->fetchRow($where);

		$this->view->app_order     = $app_order;
		$this->view->order_details = $order_details;

		$max_status = 0;

		foreach($order_details as $key=>$value){
			$max_status = ($value['status'] > $max_status) ? $value['status'] : $max_status;
		}
		$this->view->max_status	= $max_status;
		$this->view->app_status = $QAppStatus->get_cache(TU_BAN_BUCGOC);

	}

	$this->view->campaign = $campaign;
	$this->view->category = $category;
	$this->view->list_store = $list_store;


	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;


