<?php 
	$id 				= $this->getRequest()->getParam('id');
	$dx					= $this->getRequest()->getParam('dx');
	$store_from 		= $this->getRequest()->getParam('store_from',0);
	$store_to 			= $this->getRequest()->getParam('store_to',0);
	$warehouse_from 	= $this->getRequest()->getParam('warehouse_from',0);
	$warehouse_to 		= $this->getRequest()->getParam('warehouse_to',0);
	$reason_id 			= $this->getRequest()->getParam('reason_id');
	$category_id 		= $this->getRequest()->getParam('category');
	$imei 		 		= $this->getRequest()->getParam('imei');
	$quantity 	 		= $this->getRequest()->getParam('quantity');
	$submit 			= $this->getRequest()->getParam('submit');
	$QTransfer   		= new Application_Model_Transfer();
	$QTransferDetails   = new Application_Model_TransferDetails();
	$QTransferFile      = new Application_Model_TransferFile(); 
	$flashMessenger 	= $this->_helper->flashMessenger;
	$db             	= Zend_Registry::get('db');
	$db->beginTransaction();
	$back_url 			= HOST.'trade/create-transfer';
	try
	{
		if($id){
			$where = $QTransfer->getAdapter()->quoteInto("id = ?", $id);
			$data = [
				'reason_id'   => $reason_id,
			];
			$QTransfer->update($data, $where);
			$where = $QTransferDetails->getAdapter()->quoteInto("transfer_id = ?", $id);
			$QTransferDetails->delete($where);
			foreach($category_id as $key=>$value){
				$quantity_imei = count(explode(' ',$imei[$key])) - 1;
				$details = [
					'transfer_id' => $id,
					'category_id' => $value,
					'quantity'    => !empty($imei[$key]) ? intval($quantity_imei) : intval($quantity[$key]),
					'imei'		  => $imei[$key]
				];
				$QTransferDetails->insert($details);
			}
		}
		else{
			$data = [
				'store_from'  => intval($store_from)?intval($store_from):0,
				'store_to'	  => intval($store_to)?intval($store_to):0,
				'warehouse_from'  => intval($warehouse_from)?intval($warehouse_from):0,
				'warehouse_to'	  => intval($warehouse_to)?intval($warehouse_to):0,
				'status'	  => 1,
				'created_at'  => date('Y-m-d H:i:s'),
				'created_by'  => $this->storage['staff_id'],
				'created_at'  => date('Y-m-d H:i:s'),
				'reason_id'   => $reason_id,
			];
			$id = $QTransfer->insert($data);
			foreach($category_id as $key=>$value){
				$quantity_imei = count(explode(' ',$imei[$key])) - 1;
				$details = [
					'transfer_id' => $id,
					'category_id' => $value,
					'quantity'    => !empty($imei[$key]) ? intval($quantity_imei) : intval($quantity[$key]),
					'imei'		  => $imei[$key]
				];
				$QTransferDetails->insert($details);
			}
		}
		if(!empty($store_from)){
			//UPLOAD HÌNH ẢNH
			$total = count($_FILES['file_from']['name']);
			//kiểm tra đã đủ 3 ảnh nội ngoại thất?
			$total_from = 0;
			for($i=0; $i<$total; $i++) {
				if($_FILES['file_from']['name'][$i]!="")
				{
					$total_from++;
				}
			}
			//kiểm tra đã đủ 3 ảnh nội ngoại thất?
			if($total_from >= 3){
				for($i=0; $i<$total; $i++) {
					//Upload hình ảnh
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
								DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
								DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $id;
					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);
				  	$tmpFilePath = $_FILES['file_from']['tmp_name'][$i];
				  	if ($tmpFilePath != ""){
				  		$old_name 	= $_FILES['file_from']['name'][$i];
						$tExplode 	= explode('.', $old_name);
						$extension  = end($tExplode);
						$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
					    $newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
					    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
					    	$url         = 'photo' .
								DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
					    }
					    else{
					    	$url = NULL;
					    }
					}
					else{
						$url = NULL;
					}

					$transfer_file = [
						'url'			=> $url,
						'transfer_id'	=> $id,
						'type'			=> 1
					];

					$QTransferFile->insert($transfer_file);
				}
			}else{
				$flashMessenger->setNamespace('error')->addMessage('Thất bại! Thiếu ảnh nội/ngoại thất shop điều chuyển!');
	    		$this->redirect($back_url);
			}
			//END UPLOAD HÌNH ẢNH
		}
		if(!empty($store_to)){
			//UPLOAD HÌNH ẢNH
			$total_to = count($_FILES['file_to']['name']);
			//kiểm tra đã đủ 3 ảnh nội ngoại thất?
			$total_to_to = 0;
			for($i=0; $i<$total_to; $i++) {
				if($_FILES['file_to']['name'][$i]!="")
				{
					$total_to_to++;
				}
			}
			//kiểm tra đã đủ 3 ảnh nội ngoại thất?
			if($total_to_to >= 3){
				for($i=0; $i<$total_to; $i++) {
					//Upload hình ảnh
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
								DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
								DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $id;
					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);
				  	$tmpFilePath = $_FILES['file_to']['tmp_name'][$i];
				  	if ($tmpFilePath != ""){
				  		$old_name 	= $_FILES['file_to']['name'][$i];
						$tExplode 	= explode('.', $old_name);
						$extension  = end($tExplode);
						$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
					    $newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
					    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
					    	$url         = 'photo' .
								DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
					    }
					    else{
					    	$url = NULL;
					    }
					}
					else{
						$url = NULL;
					}
					$transfer_file = [
						'url'			=> $url,
						'transfer_id'	=> $id,
						'type'			=> 2
					];
					$QTransferFile->insert($transfer_file);
				}
					//END UPLOAD HÌNH ẢNH
			}else{
				$flashMessenger->setNamespace('error')->addMessage('Thất bại! Thiếu ảnh nội/ngoại thất shop điều chuyển đến!');
	    		$this->redirect($back_url);
			}	
		}
		$db->commit();
		$flashMessenger->setNamespace('success')->addMessage('Đề xuất thành công!');
    	$this->_redirect(HOST.'trade/transfer-list');
	}
	catch (Exception $e)
	{
		$db->rollBack();
	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;

