<?php
$contractor_id = $this->getRequest()->getParam('contractor_id');
$category_id = $this->getRequest()->getParam('category_id');
//edit
$submit_edit = $this->getRequest()->getParam('submit_edit');
$id_edit = $this->getRequest()->getParam('id_edit');
$todate_edit = $this->getRequest()->getParam('todate_edit');
$data_fromdate = $this->getRequest()->getParam('data_fromdate');
//create new
$submit = $this->getRequest()->getParam('submit');
$price = $this->getRequest()->getParam('price');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$max_date = $this->getRequest()->getParam('max_date');


$QCategory = new Application_Model_Category();
$QContractorPrice = new Application_Model_ContractorPrice();
$QContractor = new Application_Model_Contructors();
$flashMessenger         = $this->_helper->flashMessenger;

    $params = [
        'contractor_id'   => $contractor_id,
        'category_id'   => $category_id
    ];


$data = $QContractorPrice->getHistory($params);

if(!empty($submit_edit) && $submit_edit==1)
{

	if(!empty($data_fromdate) && !empty($todate_edit)){
		$fromdate = date_create_from_format("d/m/Y", $data_fromdate)->format("Y-m-d");
		$todate = date_create_from_format("d/m/Y", $todate_edit)->format("Y-m-d");

		if($fromdate > $todate){
			
			 $flashMessenger->setNamespace('error')->addMessage( "Ngày áp dụng giá không hợp lệ!");
				$this->redirect(HOST.'trade/contractor-price-history?contractor_id='.$contractor_id.'&category_id='.$category_id);
		}
		
		$where = $QContractorPrice->getAdapter()->quoteInto('id = ?', $id_edit);
			$data=array(
				'to_date' => $todate
			);
		$QContractorPrice->update($data,$where);
		$flashMessenger->setNamespace('success')->addMessage('Cập nhật thành công!');
		$this->redirect(HOST.'trade/contractor-price-history?contractor_id='.$contractor_id.'&category_id='.$category_id);
	}
	
}

if(!empty($submit))
{


	foreach ($price as $key => $value) {
		if(!empty($from_date[$key]))
		{

			$from = date_create_from_format("d/m/Y", $from_date[$key])->format("Y-m-d");
			$max = date_create_from_format("d/m/Y", $max_date)->format("Y-m-d");
			
			if($from <= $max){
				
				$flashMessenger->setNamespace('error')->addMessage( "Ngày áp dụng giá không hợp lệ!");
				$this->redirect(HOST.'trade/contractor-price-history?contractor_id='.$contractor_id.'&category_id='.$category_id);
			}
		}

		if(!empty($to_date[$key])){
			$to = date_create_from_format("d/m/Y", $to_date[$key])->format("Y-m-d");
		}

		if(!empty($from_date[$key]) && !empty($to_date[$key])){
			$to = date_create_from_format("d/m/Y", $to_date[$key])->format("Y-m-d");
			$from = date_create_from_format("d/m/Y", $from_date[$key])->format("Y-m-d");
			if($from > $to)
			{
				  $flashMessenger->setNamespace('error')->addMessage( "Ngày áp dụng giá không hợp lệ!");
				$this->redirect(HOST.'trade/contractor-price-history?contractor_id='.$contractor_id.'&category_id='.$category_id);
			}
			
			
		}
		 
		$data=array(
			'contractor_id' => $contractor_id,
			'category_id'	=> $category_id,
			'from_date' => !empty($from)?$from:null,
			'to_date' => !empty($to)?$to:null,
			'price'	=> $value
		);
	
	$QContractorPrice->insert($data);
	}
	
	$flashMessenger->setNamespace('success')->addMessage('Tạo mới thành công!');
	$this->redirect(HOST.'trade/contractor-price-history?contractor_id='.$contractor_id.'&category_id='.$category_id);
}


// echo '<pre>';
// print_r($data); exit;

$this->view->data = $data;

// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
?>
