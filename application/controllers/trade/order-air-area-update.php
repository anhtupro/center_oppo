
<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$orderQuantity = $this->getRequest()->getParam('order_quantity');
$receiveQuantity = $this->getRequest()->getParam('receive_quantity');
$status = $this->getRequest()->getParam('status');
$areaId  = $this->getRequest()->getParam('area_id');
$stageId  = $this->getRequest()->getParam('stage_id');
$categoryId = $this->getRequest()->getParam('category_id');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$QOrderAirAreaStatus = new Application_Model_OrderAirAreaStatus();
$QOrderAirArea = new Application_Model_OrderAirArea();
$QOrderAirDetail = new Application_Model_OrderAirDetail();
$QInventoryAreaPosm = new Application_Model_InventoryAreaPosm();
$QInventoryAreaPosmDetail = new Application_Model_InventoryAreaPosmDetail();
$QCategory = new Application_Model_Category();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();


$whereStatus = [
    'area_id = ?' => $areaId,
    'order_air_stage_id = ?' => $stageId
];


$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($status == 2) {
    foreach ($orderQuantity as $id => $quantity) {
        $QOrderAirArea->update([
            'order_quantity' => $quantity ? $quantity : 0
        ], ['id = ?' => $id]);
    }

    $result = [
        'status' => 0
    ];
}

$db->commit();

echo json_encode($result);

