<?php

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore          = new Application_Model_Store();
$QOpenShop       = new Application_Model_OpenShop();
$QOpenShopReason = new Application_Model_OpenShopReason();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppStatus      = new Application_Model_AppStatus();
$QStore          = new Application_Model_Store();
$QAppCheckshop   = new Application_Model_AppCheckshop();

$list_store                = $QStore->getShopDeleteByStaff();
$this->view->stores        = $list_store;
$reason_active             = $QOpenShopReason->get_cache_active();
$this->view->reason_active = $reason_active;


$store_id = $this->getRequest()->getParam('store_id');
$reason   = $this->getRequest()->getParam('reason');
$note     = $this->getRequest()->getParam('note', null);
$submit   = $this->getRequest()->getParam('submit');
$image    = $this->getRequest()->getParam('image');
$id       = $this->getRequest()->getParam('id');

$app_status = $QAppStatus->get_cache(10);
$app_status_title = $QAppStatusTitle->get_cache(10);
$app_status_title = $app_status_title[$this->storage['title']];


if (!empty($id)) {
    $where                    = $QOpenShop->getAdapter()->quoteInto('id = ?', $id);
    $openShop_row             = $QOpenShop->fetchRow($where);
    $this->view->openShop_row = $openShop_row;
}

if (!empty($submit)) {

    if (empty($store_id) || empty($reason)) {
        $flashMessenger->setNamespace('error')->addMessage('Vui Lòng nhập đầy đủ thông tin!.');
        $this->_redirect(HOST . 'trade/open-shop');
    }
    
    $where_store = [];
    $where_store[] = $QStore->getAdapter()->quoteInto('id = ?', $store_id);
    $where_store[] = $QStore->getAdapter()->quoteInto('del = 0 or del is null');
    $store = $QStore->fetchRow($where_store);
    if($store){
        $flashMessenger->setNamespace('error')->addMessage('Shop '.$store['name']. ' đã được mở!');
        $this->_redirect(HOST . 'trade/open-shop');
    }
    
    
    
    $where_store = $QStore->getAdapter()->quoteInto('id = ?', $store_id);
    $store = $QStore->fetchRow($where_store);
     
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
         
        if (!empty($id)) {//update            
            $where          = $QOpenShop->getAdapter()->quoteInto('id = ?', $id);
            $openShop_row = $QOpenShop->fetchRow($where);

            if (($openShop_row['status'] != 1) || $openShop_row['created_by'] != $userStorage->id) {
                $flashMessenger->setNamespace('error')->addMessage('Chỉ Edit được đề xuất chưa Approve và chính người đề xuất mới có quyền chỉnh sửa.!');
                $this->_redirect(HOST . 'trade/open-shop?id=' . $id);
            }

            $data_update = array(
                'store_id'   => $store_id,
                'reason'     => $reason,
                'note'       => $note,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => $userStorage->id
            );

            $QOpenShop->update($data_update, $where);
            
            $info = "open_shop_update_" . $id . "_" . serialize($data_update);
        } else {//insert

            $where = [];
            $where[] = $QOpenShop->getAdapter()->quoteInto('store_id = ?', $store_id);
            $where[] = $QOpenShop->getAdapter()->quoteInto('reject IS NULL');
            $remove_store = $QOpenShop->fetchRow($where);
            if($remove_store){
                $flashMessenger->setNamespace('error')->addMessage('Shop đã được đề xuất!');
                $this->_redirect(HOST . 'trade/open-shop');
            }
             
            $data_insert = array(
                'store_id'   => $store_id,
                'reason'     => $reason,
                'note'       => $note,
                'status'     => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
            );
 
            $id   = $QOpenShop->insert($data_insert);
             
            $info = "open_shop_create_" . $id . "_" . serialize($data_insert);
        }
         

        // log
        $QLog = new Application_Model_Log();
        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');


        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));


        if (empty($id)) {
            $flashMessenger->setNamespace('error')->addMessage('Không thành công.');
            $this->_redirect(HOST . 'trade/open-shop?id=' . $id);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/open-shop?id=' . $id);
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
        $this->_redirect(HOST . 'trade/open-shop');
    }
}

$this->view->params           = $params;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
