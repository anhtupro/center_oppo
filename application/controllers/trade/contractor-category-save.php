<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$campaign_id    = $this->getRequest()->getParam('campaign_id');
$input_code 	= $this->getRequest()->getParam('input_code');

$price   		= $this->getRequest()->getParam('price');
$quantity_order = $this->getRequest()->getParam('quantity_order');

$category_id    = $this->getRequest()->getParam('category_id');
$area_id    	= $this->getRequest()->getParam('area_id');
$submit     	= $this->getRequest()->getParam('submit');
$contractor_id  = $this->getRequest()->getParam('contractor_id');


$QContractorCategory	= new Application_Model_ContractorCategory();

    if($_FILES["file"]["tmp_name"] != ""){

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';

            error_reporting(0);
            set_time_limit(0);

            //===================Main=======================

            if($_FILES["file"]["tmp_name"] != ""){
                if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
                    //echo ("Không phải file XLSX !");exit;
                }
                if ($_FILES["file"]["error"] > 0)
                {
                    echo ("Return Code: " . $_FILES["file"]["error"] . "<br />");exit;
                }
                move_uploaded_file($_FILES["file"]["tmp_name"], "files/limit_campaign/share-contractor.xlsx");
            }

            $inputfile = 'files/limit_campaign/share-contractor.xlsx';
            $xlsx = new SimpleXLSX($inputfile);
            $data_xlsx = $xlsx->rows();

            //Đếm số cột
            $num_cols = count($category_campaign)+2;

            //Đếm số dòng
            $i = 1;
            $num_rows = 0;
            while ($data_xlsx[$i++][0] <> '') {
                $num_rows++;
            }

            $where_del = $QContractorCategory->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
            $QContractorCategory->delete($where_del);

            $sql = "INSERT INTO `".DATABASE_TRADE."`.`contractor_category`(campaign_id, contractor_id, area_id, category_id, price, status) VALUES";

            for($i = 2; $i <= $num_rows; $i++){

                $area_id = $data_xlsx[$i][0];

                for($j = 2; $j <= $num_cols; $j++){
                    $cat_code 	= $data_xlsx[0][$j];
                    $cat_id     = $category_code[$cat_code];
                    $input_code = $data_xlsx[$i][$j];


                    if(!empty($cat_id) AND !empty($area_id) AND !empty($input_code)){

                        //Xử lý chính
                        if(!empty($contractor_price[$contructor[$input_code]][$cat_id])){
                            $sql .= "(".$campaign_id.", ".$contructor[$input_code].", ".$area_id.", ".$cat_id.", ".$contractor_price[$contructor[$input_code]][$cat_id].", 1),";
                        }
                        else{
                            $db->rollBack();
                            $flashMessenger->setNamespace('error')->addMessage('Price NULL : '.$input_code.'/'.$category_cache[$cat_id]);
                            $this->redirect($back_url);
                        }

                        //END Xử lý chính

                    }

                }

            }

            $sql 		= rtrim($sql, ',');

            //INSERT
            if(!empty($sql)){
                $db = Zend_Registry::get('db');
                $stmt = $db->prepare($sql);
                $stmt->execute();
                $stmt->closeCursor();
            }

            unlink($inputfile);

            //Cập nhật lại status campaign_area
//				$where = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
//				$campaign_area = $QCampaignArea->fetchRow($where);
//				$QCampaignArea->update( ['status'=>($min_status+1)] , $where);
            //END cập nhật lại status campaign_area

            //=================Function Area===================
            $flashMessenger->setNamespace('success')->addMessage('Chia nhà thầu theo từng khu vực, từng hạng mục thành công!');
            $db->commit();
            $this->redirect($back_url);

        } catch (Exception $e) {
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->redirect($back_url);
        }

    }
    else{

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $where_del = $QContractorCategory->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
            $QContractorCategory->delete($where_del);

            $sql = "INSERT INTO `".DATABASE_TRADE."`.`contractor_category`(campaign_id, contractor_id, area_id, category_id, price, status) VALUES";

            $current_date = date('Y-m-d H:i:s');

            foreach($input_code as $key=>$value){
                if(!empty($value) AND !empty($contructor[$value])){
                    if(!empty($contractor_price[$contructor[$value]][$category_id[$key]])){

                        $sql .= "(".$campaign_id.", ".$contructor[$value].", ".$area_id[$key].", ".$category_id[$key].", ".$contractor_price[$contructor[$value]][$category_id[$key]].", 1),";

                    }
                    else{
                        $db->rollBack();
                        $flashMessenger->setNamespace('error')->addMessage('Price NULL : '.$value.'/'.$category_id[$key]);
                        $this->redirect($back_url);
                    }

                }

            }
            $sql 		= rtrim($sql, ',');

            //INSERT
            $db = Zend_Registry::get('db');
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $stmt->closeCursor();

            $flashMessenger->setNamespace('success')->addMessage('Chia nhà thầu theo từng khu vực, từng hạng mục thành công!');
            $db->commit();
            $this->redirect($back_url);
        } catch (Exception $e) {
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->redirect($back_url);
        }
    }

