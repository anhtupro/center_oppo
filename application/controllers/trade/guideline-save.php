<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$id = $this->getRequest()->getParam('id');

$QGuidelineFile = new Application_Model_GuidelineFile();

//save photo
$upload = new Zend_File_Transfer();
$images = $upload->getFileInfo();

$upload->addValidator('Extension', false, 'jpg,jpeg,png,pdf');
$upload->addValidator('Size', false, array('max' => '10MB'));
$upload->addValidator('ExcludeExtension', false, 'php,sh');
$upload->addValidator('Count', false, 5);


$upload_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'guideline_trade_marketing' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR;
$url_save_to_database = '/files/guideline_trade_marketing/' . $id . '/';
if (!is_dir($upload_dir)) {
    mkdir($upload_dir, 0755, true);
}

foreach ($images as $image_type => $image_infor) {
    if (!$image_infor['name']) { // if there is no file is uploaded
        continue;
    }
    $extension = pathinfo($image_infor['name'], PATHINFO_EXTENSION);
    $new_name = $upload_dir . DIRECTORY_SEPARATOR . md5(uniqid('', true)) . '.' . $extension;
    $upload->addFilter('Rename', array('target' => $new_name)); // Set a new destination path

    // check file is valid or not
    if (!$upload->isValid($image_type)) {
        $array_error = $upload->getMessages();

        if (isset($array_error['fileSizeTooBig'])) {
            echo json_encode([
                'status' => 1,
                'message' => 'File có dung lượng vượt quá 5MB'
            ]);
            return;
        }
        if (isset($array_error['fileExtensionFalse'])) {
            echo json_encode([
                'status' => 1,
                'message' => 'File không đúng định dạng'
            ]);
            return;
        }
        if ($array_error['fileCountTooMany']) {
            echo json_encode([
                'status' => 1,
                'message' => 'Bạn chỉ được upload tối đa 5 file'
            ]);
            return;
        }
    }

    $result = $upload->receive($image_type); // save image
    $list_photo [] = [
        'url' => $url_save_to_database .  basename($new_name),
        'real_name' => $image_infor['name']
    ];
}

if ($list_photo) {
//
    foreach ($list_photo as $photo) {
        $QGuidelineFile->insert([
            'guideline_id' => $id,
            'url' => $photo['url'],
            'name' => $photo['real_name'],
        ]);
    }
}
// end save photo

echo json_encode([
    'status' => 0
]);