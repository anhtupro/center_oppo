<?php

$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category_id');
$destruction_details_type = $this->getRequest()->getParam('destruction_details_type');
$destruction_details_tp = $this->getRequest()->getParam('destruction_details_tp');
$destruction_type = $this->getRequest()->getParam('destruction_type');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');
$image = $this->getRequest()->getParam('image');
$inventory_area_detail_id = $this->getRequest()->getParam('inventory_area_detail_id');
$app_checkshop_detail_child_id = $this->getRequest()->getParam('app_checkshop_detail_child_id');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QDestruction = new Application_Model_Destruction();
$QDestructionDetails = new Application_Model_DestructionDetails();
$QDestructionDetailsFile = new Application_Model_DestructionDetailsFile();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QDestructionDetailsChild = new Application_Model_DestructionDetailsChild();
$QDestructionImage = new Application_Model_DestructionImage();
$QCampaign = new Application_Model_Campaign();
$QStore = new Application_Model_Store();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QArea = new Application_Model_Area();
$QAppNoti = new Application_Model_AppNoti();

$flashMessenger       = $this->_helper->flashMessenger;

$params = array_filter(array(
    'store_id' => $store_id,
    'staff_id'  => $userStorage->id
));

if (!empty($submit)) {

    if(! $category_id){
        $flashMessenger->setNamespace('error')->addMessage('Không có hạng mục nào được chọn!');
        $this->_redirect(HOST . 'trade/destruction-create');
    }

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        $special_category_id = [803, 43, 48];

        foreach ($category_id as $index => $id) {
            if (in_array($id, $special_category_id)) {
                $approve_type = 1;
            } else {
                if ($destruction_type[$index] == 1) {
                    if (! $approve_type || $approve_type == 3)
                        $approve_type = 2;
                }

                if ($destruction_type[$index] == 2) {
                    if (! $approve_type)
                        $approve_type = 3;
                }

            }
        }

        $destruction = [
            'store_id'  => $store_id,
            'status' => 1,
            'create_at' => date('Y-m-d H:i:s'),
            'staff_id'  => $userStorage->id,
            'version' => 2,
            'approve_type' => $approve_type,
            'note' => $note
        ];


        $destruction_id = $QDestruction->insert($destruction);

        foreach($category_id as $key=>$value){


            if($quantity[$key] <= 0){
                $flashMessenger->setNamespace('error')->addMessage('Số lượng hạng mục không hợp lệ!');
                $this->_redirect(HOST . 'trade/destruction-create');
            }

            $destruction_details = [
                'destruction_id' => $destruction_id,
                'category_id'   => $value,
                'quantity'  => $quantity[$key],
                'destruction_type' => $destruction_type[$key],
                'status'    => 1,
                'app_checkshop_detail_child_id' => ($app_checkshop_detail_child_id[$key] && $app_checkshop_detail_child_id[$key] !== 'undefined') ? $app_checkshop_detail_child_id[$key] : Null
            ];
            $destruction_details_id = $QDestructionDetails->insert($destruction_details);
     

            $destruction_details ['destruction_details_id'] = $destruction_details_id;
            $QDestructionDetailsChild->insert($destruction_details);

            // đánh dấu hạng mục đang được tiêu hủy
            if ($app_checkshop_detail_child_id[$key]) {
                $QAppCheckshopDetailChild->update([
                    'in_processing' => 2
                ], ['id = ?' => $app_checkshop_detail_child_id[$key] ]);
            }

        }

        // upload image
        $tag = 0;
        foreach($image as $k=>$v){

            $data = $v;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . $destruction_id. DIRECTORY_SEPARATOR . 'image_create';

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/destruction/' . $destruction_id . '/image_create/' . $newName;

            $file = $uploaded_dir . '/' . $newName;

            $success_upload = file_put_contents($uploaded_dir . '/' . $newName, $data);


            // upload image to server s3
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            $destination_s3 = 'photo/destruction/' . $destruction_id . '/image_create/';

            if ($success_upload) {
                $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
            }
            // upload image to server s3

            $destruction_image = [
                'destruction_id' => $destruction_id,
                'url'    => $url,
                'type'   => 1
            ];
            $QDestructionImage->insert($destruction_image);
            $tag = 1;
        }
        if($tag == 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload Hình ảnh hạng mục!');
            $this->_redirect(HOST . 'trade/destruction-create');
        }

        // notify
        $area_id = $QArea->getAreaByStore($store_id);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(12, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất TIÊU HỦY đang chờ bạn duyệt",
                'link' => "/trade/destruction-edit?id=" . $destruction_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');

        $this->_redirect(HOST . 'trade/destruction-edit?id='.$destruction_id);

    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());

        $this->_redirect(HOST . 'trade/destruction-create');

    }



}

$this->view->params = $params;

$result = $QCategory->getCategory();
$this->view->category = $result;
$this->view->destruction_type = $destruction_type;


//$resultCamp = $QCampaign->getListStore($params);
//$this->view->store = $resultCamp;

$params['list_area_id'] = $this->storage['area_id'];
$store = $QStore->getListStore($params);
$this->view->store = $store;



$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>