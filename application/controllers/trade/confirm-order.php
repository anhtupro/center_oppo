<?php 
	

	$campaign_id = $this->getRequest()->getParam('campaign_id');

	$QCampaign   	  = new Application_Model_Campaign();
	$QStore 	 	  = new Application_Model_Store();
	$QAppOrder 	  	  = new Application_Model_AppOrder();
	$QAppOrderDetails = new Application_Model_AppOrderDetails();
	$QAsm 	     	  = new Application_Model_Asm();
	$QAppStatus  	  = new Application_Model_AppStatus();
	$QAppStatusTitle  = new Application_Model_AppStatusTitle();


	$area_id = !empty($this->storage['area_id']) ? $this->storage['area_id'] : NULL;

	$params = [
		'campaign_id' => $campaign_id,
		'staff_id'    => $staff_id,
		'area_id'	  => $area_id
	];

	$status_title = $QAppStatusTitle->get_cache(TU_BAN_BUCGOC);

	$where 		= $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign 	= $QCampaign->fetchRow($where);

	$category 	= $QCampaign->getCategory($params);

	$order_details  = $QAppOrderDetails->getDataDetails($params);

	$max_status = 0;

	foreach($order_details as $key=>$value){
		$max_status = ($value['status'] > $max_status) ? $value['status'] : $max_status;
	}


	$this->view->order_details = $order_details;
	$this->view->max_status	= $max_status;
	//var_dump($order_details); exit;
	$this->view->status_title = $status_title[$this->storage['title']];
	$this->view->storage    = $this->storage;
	$this->view->app_status = $QAppStatus->get_cache(TU_BAN_BUCGOC);
	$this->view->campaign = $campaign;
	$this->view->category = $category;
	$this->view->list_store = $list_store;


	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;


