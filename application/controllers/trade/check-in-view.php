<?php
$checkInId = $this->getRequest()->getParam('check_in_id');

$QTradeCheckInDetail = new Application_Model_TradeCheckInDetail();
$QTradeCheckInImage = new Application_Model_TradeCheckInImage();
$QTradeCheckIn = new Application_Model_TradeCheckIn();

$staffInfo = $QTradeCheckInDetail->getStaff($checkInId);
$listCheckInDetail = $QTradeCheckInDetail->get($checkInId);
$listImageCheckIn = $QTradeCheckInImage->get($checkInId);
$checkIn = $QTradeCheckIn->fetchRow(['id = ?' => $checkInId])->toArray();


$this->view->listCheckInDetail = $listCheckInDetail;
$this->view->listImageCheckIn= $listImageCheckIn;
$this->view->staffInfo = $staffInfo;
$this->view->checkIn = $checkIn;
