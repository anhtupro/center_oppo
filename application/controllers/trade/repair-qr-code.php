<?php
$store_id   = $this->getRequest()->getParam('store');
$note    = $this->getRequest()->getParam('note');
$submit	    = $this->getRequest()->getParam('submit');
$type	    = $this->getRequest()->getParam('type');
$imei_sn    = $this->getRequest()->getParam('imei');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QDestruction = new Application_Model_Destruction();
$QRepair = new Application_Model_Repair();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QImei = new Application_Model_ImeiTrade();

$flashMessenger       = $this->_helper->flashMessenger;
$params = array_filter(array(
	'store'=>$store_id,
	'submit'=>$submit,
	'note'=>$note,
	'type'=>$type // ------------LOẠI SỬA CHỮA , BẢO HÀNH HAY CÓ TÍNH PHÍ---------
));

if (!empty($submit)) {//----------------KHI NHẤN NÚT XÁC NHẬN----------------

	if(isset($imei_sn) and $imei_sn){ 
		$params['id'] = $imei_sn;
		$result = $QImei->GetId($params);
		if(!empty($result['category_id']) && !empty($result['store_id'])){
			$category_id=$result['category_id'];
			$store_id=$result['store_id'];
		}else {
			$flashMessenger->setNamespace('error')->addMessage('Đề xuất không thành công. Mã code không hợp lệ ');
			$this->_redirect(HOST . 'trade/repair-qr-code');
		}
	}
	
	//----------------KIỂM TRA THÔNG TIN FILE UPLOAD----------------
	$upload = new Zend_File_Transfer();
	$upload->setOptions(array('ignoreNoFile'=>true));

	            //check function
	if (function_exists('finfo_file'))
		$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));

	$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
	$upload->addValidator('Size', false, array('max' => '10MB'));
	$upload->addValidator('ExcludeExtension', false, 'php,sh');
	$files = $upload->getFileInfo();

		if($files['image']['error']==0){ // NEU UP FILE KO LOI THI TIEN HANH INSERT

			try{

				$result_query =$QRepair->insert(array(
					'store_id'=>$store_id,
					'category_id'=>$category_id,
					'note'=>$note,
					'create_at'=>date('Y-m-d H:i:s'),
					'update_at'=>date('Y-m-d H:i:s'),
					'type'=>$type,
					'staff_id'=>$userStorage->id,
					'status'=> ($type == 1)? 16:19
				));
				
				$data_file = array();
				// -------------INSERT HÌNH ẢNH NGOẠI THẤT VÀ NỘI THẤT -------------
				$fileInfo['image'] = (isset($files['image']) and $files['image']) ? $files['image'] : null;
				$fileInfo['imageoutside'] = (isset($files['imageoutside']) and $files['imageoutside']) ? $files['imageoutside'] : null;
				foreach ($fileInfo as $key => $value) {
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
					DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
					DIRECTORY_SEPARATOR . 'repair' . DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . $result_query;
					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);
					$upload->setDestination($uploaded_dir);

            //Rename
					$old_name =$value['name'];
					$tExplode = explode('.', $old_name);
					$extension = end($tExplode);
					$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
					$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
					$r = $upload->receive(array($key));
					if($r){
						$data_file['file_name'] = $new_name;
					}
					else{
						$messages = $upload->getMessages();
						foreach ($messages as $msg)
							throw new Exception($msg);
					}
	// --------------INSERT HINH ANH LEN DATABASE-------------
					$data_file['parrent_id'] = $result_query;
					$data_file['type']=1;
					$result =$QAppFile->insert($data_file);
				}
				$flashMessenger->setNamespace('success')->addMessage('Đề xuất thành công ');
				$this->_redirect(HOST . 'trade/repair-edit?id='.$result_query);

			}
			catch (exception $e) {
				if($result_query ){
					$whereRepair = $QRepair->getAdapter()->quoteInto('id = ?',$result_query);
					$QRepair->delete($whereRepair);
					$whereAppfile = $QAppFile->getAdapter()->quoteInto('type ="1" and parrent_id = ?',$result_query);
					$QAppFile->delete($whereAppfile);
				}
				$flashMessenger->setNamespace('error')->addMessage('Đề xuất không thành công. Vui lòng kiểm tra lại thông tin và hình ảnh');
				$this->_redirect(HOST . 'trade/repair-qr-code'	);
			}

		}
	}

	$this->view->params = $params;
	// -------------RETURN CATEGORY ---------------
	$result = $QCategory->GetAll();
	$this->view->category = $result;
	// -------------RETURN STORE ---------------
	$paramsData['staff_id'] = $userStorage->id;
	$resultCamp = $QCampaign->getListStore($paramsData);
	$this->view->store = $resultCamp;
	// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages = $messages;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages_success = $messages_success;
	}
	?>