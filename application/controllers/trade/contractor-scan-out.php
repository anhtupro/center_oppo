<?php 

	$category_id			= $this->getRequest()->getParam('category_id');
	$contractor_id   		= $this->getRequest()->getParam('contractor_id');
	$shipment_details_id   	= $this->getRequest()->getParam('shipment_details_id');
	$campaign_id   			= $this->getRequest()->getParam('campaign_id');
	$list_imei   			= $this->getRequest()->getParam('list_imei');

	$QImeiTrade 				= new Application_Model_ImeiTrade();
	$QAppContractorIn 			= new Application_Model_AppContractorIn();
	$QAppContractorOut 			= new Application_Model_AppContractorOut();
	$QAppContractorQuantity 	= new Application_Model_AppContractorQuantity();

	$list_imei = explode(',', rtrim($list_imei));

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{
		foreach ($list_imei as $key => $value) {


			$imei_sn = RTRIM(LTRIM($value));

			if($imei_sn != ''){

				$params = [
					'shipment_details_id'	=> $shipment_details_id,
					'imei_sn' 				=> $imei_sn,
				];

				$imei_info = $QAppContractorQuantity->checkImeiScan($params);

				if($imei_info['imei_in'] == 0){
					echo 'Imei không có trong tồn kho, hoặc không đúng hạng mục!';exit;
				}

				if($imei_info['imei_out'] == 1){
					echo 'Imei đã được xuất kho!';exit;
				}
				
				$data = [
					'contractor_id' 	=> $contractor_id,
					'campaign_id'	 	=> $campaign_id,
					'category_id' 		=> $category_id,
					'imei_sn' 			=> $imei_sn,
					'created_at' 		=> date('Y-m-d H:i:s'),
					'created_by'		=> $this->storage['staff_id'],
					'app_shipment_details'	=> $shipment_details_id
				];
				$QAppContractorOut->insert($data);

				//Cập nhật lại status app_order_details
				$db = Zend_Registry::get('db');
                $sql = "UPDATE `".DATABASE_TRADE."`.`app_order_details` p
						LEFT JOIN `".DATABASE_TRADE."`.`app_order` o ON o.id = p.order_id
						LEFT JOIN hr.store s ON s.id = p.store_id
						LEFT JOIN hr.regional_market r ON r.id = s.regional_market
						SET p.`status` = 8
						WHERE p.`status` = 7 AND o.campaign_id = :campaign_id AND p.category_id = :category_id AND r.area_id = :area_id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('campaign_id', $imei_info['campaign_id'], PDO::PARAM_INT);
                $stmt->bindParam('category_id', $imei_info['category_id'], PDO::PARAM_INT);
                $stmt->bindParam('area_id', $imei_info['area_id'], PDO::PARAM_INT);
                $stmt->execute();

                $stmt->closeCursor();
				//END Cập nhật lại status app_order_details

			}
				
		}

		$db->commit();
		echo 'success';exit;

	}
	catch (Exception $e)
	{
		$db->rollBack();

	    echo "Có lỗi: ".$e->getMessage();
	    exit;
	}




