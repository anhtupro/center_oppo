<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$listCategory = $this->getRequest()->getParam('category_id');
$listQuantity = $this->getRequest()->getParam('quantity');
$listHeight = $this->getRequest()->getParam('height');
$listWidth = $this->getRequest()->getParam('width');
$listMaterial= $this->getRequest()->getParam('material');
$listCategoryType = $this->getRequest()->getParam('category_type');
$listCategoryChildType = $this->getRequest()->getParam('category_type_child');
$listWidthPainting = $this->getRequest()->getParam('width_painting');
$listHeightPainting = $this->getRequest()->getParam('height_painting');
$listMaterialPainting = $this->getRequest()->getParam('material_painting');

$areaId = $this->getRequest()->getParam('area_id');
$listCheckshopDetailChildId = $this->getRequest()->getParam('app_checkshop_detail_child_id');


$QInventoryArea = new Application_Model_AirInventoryArea();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();
$QCategory = new Application_Model_Category();

$params = [];
$listIdCategoryCheckshop = $QCategory->getIdCategoryCheckshop($params);

if (!$areaId) {
    echo json_encode([
       'status' => 1,
       'message' => 'Thiếu ID khu vực!'
    ]);
    return;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();

//$QInventoryArea->delete(['area_id = ?' => $areaId]);


foreach ($listCategory as $key => $categoryId) {
    if ($listCheckshopDetailChildId[$key]) { // edit
        $childId = $listCheckshopDetailChildId[$key];
        $QAppCheckshopDetailChild->update([
            'width' => $listWidth[$key] ? $listWidth[$key] : Null,
            'height' => $listHeight[$key] ? $listHeight[$key] : Null,
            'category_type' => $listCategoryType[$key] ? $listCategoryType[$key] : Null,
            'category_type_child' => $listCategoryChildType[$key] ? $listCategoryChildType[$key] : Null,
            'material_id' => $listMaterial[$key] ? $listMaterial[$key] : Null,
            'quantity' => $listQuantity[$key] ? $listQuantity[$key] : 0
        ], ['id = ?' => $listCheckshopDetailChildId[$key]]);

        $QAppCheckshopDetailChildPainting->delete(['app_checkshop_detail_child_id = ?' => $childId]);

        // add painting
        foreach ($listWidthPainting[$key] as $index => $widthPainting) {
            $heightPainting = $listHeightPainting[$key][$index] ? $listHeightPainting[$key][$index] : Null;
            $materialPainting = $listMaterialPainting[$key][$index] ? $listMaterialPainting[$key][$index] : Null;

            if ($widthPainting || $heightPainting || $materialPainting) {
                $QAppCheckshopDetailChildPainting->insert([
                    'width' =>  $widthPainting ? $widthPainting : Null,
                    'height' =>  $heightPainting ,
                    'material_id' => $materialPainting,
                    'app_checkshop_detail_child_id' => $childId
                ]);
            }
        }

    } else { // create new

        $dataInsert = [
            'category_id' =>  $categoryId,
            'width' => $listWidth[$key] ? $listWidth[$key] : Null,
            'height' => $listHeight[$key] ? $listHeight[$key] : Null,
            'category_type' => $listCategoryType[$key] ? $listCategoryType[$key] : Null,
            'category_type_child' => $listCategoryChildType[$key] ? $listCategoryChildType[$key] : Null,
            'material_id' => $listMaterial[$key] ? $listMaterial[$key] : Null,
//            'quantity' => $listQuantity[$key] ? $listQuantity[$key] : Null,
            'area_id' => $areaId ? $areaId : Null,
            'is_warehouse' => 1
        ];

        // Nếu là hạng mục checkshop, lưu theo mỗi dòng số lượng 1
        if (in_array($categoryId, $listIdCategoryCheckshop)) {
            $dataInsert ['quantity'] = 1;
            for ($i = 0; $i < $listQuantity[$key]; $i++) {
                $childId = $QAppCheckshopDetailChild->insert($dataInsert);

                // add painting
                foreach ($listWidthPainting[$key] as $index => $widthPainting) {
                    $heightPainting = $listHeightPainting[$key][$index] ? $listHeightPainting[$key][$index] : Null;
                    $materialPainting = $listMaterialPainting[$key][$index] ? $listMaterialPainting[$key][$index] : Null;

                    if ($widthPainting || $heightPainting || $materialPainting) {
                        $QAppCheckshopDetailChildPainting->insert([
                            'width' =>  $widthPainting ? $widthPainting : Null,
                            'height' =>  $heightPainting ,
                            'material_id' => $materialPainting,
                            'app_checkshop_detail_child_id' => $childId
                        ]);
                    }
                }
            }
        } else {
            $dataInsert ['quantity'] = $listQuantity[$key] ? $listQuantity[$key] : 0;
            $childId = $QAppCheckshopDetailChild->insert($dataInsert);
        }
    }

}

$db->commit();

echo json_encode([
   'status' => 0
]);

