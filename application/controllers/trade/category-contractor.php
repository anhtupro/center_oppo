<?php 
	
	$page           		= $this->getRequest()->getParam('page');

	$QCampaign 	   			= new Application_Model_Campaign();
	$QArea 		   			= new Application_Model_Area();
	$QCategoryContractor 	= new Application_Model_CategoryContractor();


	$area           = $QArea->get_cache();

	$params = [
		'campaign_id' => $campaign_id
	];

	$limit = LIMITATION;
	$total = 0;
	$category_contractor = $QCategoryContractor->fetchPagination($page, $limit, $total, $params);


	$this->view->offset 				= $limit * ($page - 1);
	$this->view->category_contractor    = $category_contractor;
	$this->view->params 				= $params;
    $this->view->limit  				= $limit;
    $this->view->total 					= $total;
	$this->view->url 					= HOST . 'trade/category-contractor/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');




