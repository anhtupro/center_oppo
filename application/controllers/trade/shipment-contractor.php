<?php 

	

	$page     		= $this->getRequest()->getParam('page', 1);
	$campaign_id    = $this->getRequest()->getParam('campaign_id');
	$category_id    = $this->getRequest()->getParam('category_id');

	$QShipment 			= new Application_Model_AppShipment();
	$QShipmentDetails 	= new Application_Model_AppShipmentDetails();

	$limit = LIMITATION;
	$total = 0;

	$params = [
		'campaign_id'	=> $campaign_id
	];

	if($this->storage['group'] == CONTRACTOR_GROUP_ID){
		$contractor = $QShipment->getContractorById($this->storage['staff_id']);
		$params['contractor_id'] = $contractor['id'];
	}

	$shipment = $QShipment->fetchPagination($page, $limit, $total, $params);
	
	$imei_scan = $QShipmentDetails->getListImeiScanShipment($params);

	$data_scan = [];
	foreach ($imei_scan as $key => $value) {
		$data_scan[$value['shipment_id']] = [
			'quantity'		=> $value['quantity'],
			'list_imei'		=> $value['list_imei']
		];
	}

	$this->view->data_scan  = $data_scan;
	$this->view->shipment 	= $shipment;
	$this->view->limit  	= $limit;
	$this->view->total  	= $total;
	$this->view->group      = $this->storage['group'];

