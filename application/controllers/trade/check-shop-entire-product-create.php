<?php
$check_shop_id = $this->getRequest()->getParam('check_shop_id');

$QCategory = new Application_Model_Category();
$QPhoneProduct = new Application_Model_PhoneProduct();
$QBrandTrade = new Application_Model_BrandTrade();
$QPromotion = new Application_Model_Promotion();

$list_category = $QCategory->fetchAll(['check_shop_entire = ?' => 1]);
$list_phone_product = $QPhoneProduct->fetchAll(['status = ?' => 1]);
$list_brand = $QBrandTrade->fetchAll(['is_checkshop = ?' => 1], ['ordering ASC'])->toArray();
$list_promotion = $QPromotion->fetchAll(['status = ?' => 1])->toArray();
$list_category_display = $QCategory->fetchAll(['is_display_image = ?' => 1]);

// style color in view
$color = [
    1 => '#2AAD6F',
    2 => 'blue',
    4 => '#415fff',
    5 => '#fbb244',
    6 => 'orangered',
    7 => '#aa0f0f'
];


$this->view->list_category = $list_category;
$this->view->list_phone_product = $list_phone_product;
$this->view->check_shop_id = $check_shop_id;
$this->view->list_brand = $list_brand;
$this->view->list_promotion = $list_promotion;
$this->view->list_category_display = $list_category_display;
$this->view->color = $color;