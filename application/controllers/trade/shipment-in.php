<?php 

	$shipment_id = $this->getRequest()->getParam('shipment_id');

	$QShipment 			= new Application_Model_AppShipment();
	$QShipmentDetails 	= new Application_Model_AppShipmentDetails();
	$QTransporter 		= new Application_Model_AppTransporter();

	$where = $QShipment->getAdapter()->quoteInto('id = ?', $shipment_id);
	$shipment = $QShipment->fetchRow($where);

	$params = [
		'shipment_id' => $shipment_id
	];

	if($this->storage['group'] == CONTRACTOR_GROUP_ID){
		$contractor = $QShipment->getContractorById($this->storage['staff_id']);
		$params['contractor_id'] = $contractor['id'];
	}

	if( in_array($this->storage['title'], [TRADE_MARKETING_EXECUTIVE]) ){
		$params['area_id'] = $this->storage['area_id'];
	}

	$shipment_details = $QShipmentDetails->fetchShipmentDetails($params);
	$imei_scan = $QShipmentDetails->getListImeiScanArea($params);

	$data_scan = [];
	foreach ($imei_scan as $key => $value) {
		$data_scan[$value['category_id']] = [
			'quantity'		=> $value['quantity'],
			'list_imei'		=> $value['list_imei']
		];
	}

	$this->view->transporter 		= $QTransporter->get_cache();
	$this->view->shipment 			= $shipment;
	$this->view->shipment_details 	= $shipment_details;
	$this->view->data_scan          = $data_scan;
