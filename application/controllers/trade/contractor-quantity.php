<?php 
	
	$campaign_id   = $this->getRequest()->getParam('campaign_id');

	$QAppOrderDetails 			= new Application_Model_AppOrderDetails();
	$QArea 			  			= new Application_Model_Area();
	$QCategory 		  			= new Application_Model_Category();
	$QAppContractorQuantity 	= new Application_Model_AppContractorQuantity();

	$params = [
		'campaign_id'	=> $campaign_id
	];

	$details 				= $QAppOrderDetails->getDetailsArea($params);
	$contractor_quantity 	= $QAppContractorQuantity->getDataByCampaign($params);
	$contractor_out			= $QAppContractorQuantity->getContractorQuantityOut($params);

	$data_details 		 = [];
	$area_list 			 = [];
	$category_list		 = [];
	$contractor_list	 = [];
	$contractor_out_list = [];

	foreach($details as $key=>$value){
		$area_list[] 		= $value['area_id'];
		$category_list[]	= $value['category_id'];
		$data_details[$value['area_id']][$value['category_id']] = $value['quantity'];
	}

	foreach ($contractor_quantity as $key => $value) {
		if(empty($contractor_list[$value['category_id']][$value['contractor_id']])){
			$contractor_list[$value['category_id']][$value['contractor_id']] = array(
																					'contractor_quantity_id' => $value['contractor_quantity_id'],
																					'quantity'		=> $value['quantity'],
																					'quantity_in'	=> $value['quantity_in'],
																					'quantity_out'	=> $value['quantity_out'], 
																				);
		}
		else{
			$contractor_list[$value['category_id']][$value['contractor_id']]['quantity'] 		+= $value['quantity'];
			$contractor_list[$value['category_id']][$value['contractor_id']]['quantity_in'] 	+= $value['quantity_in'];
			$contractor_list[$value['category_id']][$value['contractor_id']]['quantity_out'] 	+= $value['quantity_out'];
		}
	}

	foreach($contractor_out as $key=>$value){
		if(empty($contractor_out_list[$value['id']][$value['area_id']])){
			$contractor_out_list[$value['id']][$value['area_id']] = $value['quantity'];
		}
	}


	$where = $QArea->getAdapter()->quoteInto('id IN (?)',$area_list);
	$area  = $QArea->fetchAll($where);

	$where 		= $QCategory->getAdapter()->quoteInto('id IN (?)', $category_list);
	$category 	= $QCategory->fetchAll($where);

	$this->view->area 					= $area;
	$this->view->category   			= $category;
	$this->view->contractor_list  		= $contractor_list;
	$this->view->contractor 			= $QAppContractorQuantity->getContractorAll();
	$this->view->data_details 			= $data_details;
	$this->view->contractor_out_list    = $contractor_out_list;
	$this->view->params                 = $params;


	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;

	$this->_helper->layout->setLayout('layout');


	