<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);



$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QChangePictureStage = new Application_Model_ChangePictureStage();

$change_picture_stage = $QChangePictureStage->getValidStage();

if (! $change_picture_stage) {
    return ;
}

$params = [
  'list_area' => $this->storage['area_id'],
  'change_picture_stage'  => $change_picture_stage['id']
];

$data = $QChangePictureStage->getDataPicture($params);
$QChangePictureStage->downloadTemplateMassUpload($data);

