<?php
$areaId = $this->getRequest()->getParam('area_id');

$QCategory = new Application_Model_Category();
$QInventoryAreaPosm = new Application_Model_InventoryAreaPosm();
$QArea = new Application_Model_Area();
$QInventoryAreaPosmDetail = new Application_Model_InventoryAreaPosmDetail();

$params = [
    'area_id' => $areaId
];

$listCategory = $QCategory->GetCategoryPosm();
//$listCurrentCategory = $QInventoryAreaPosm->getAll($params);
$listCurrentCategoryParent = $QInventoryAreaPosm->getParentQuantity($params);
$listCurrentCategoryChild = $QInventoryAreaPosm->getChildQuantity($params);
$listHistory = $QInventoryAreaPosmDetail->getDetail($params);

//$this->view->listCurrentCategory = $listCurrentCategory;
$this->view->listCurrentCategoryParent = $listCurrentCategoryParent;
$this->view->listCurrentCategoryChild = $listCurrentCategoryChild;
$this->view->listCategory = $listCategory;
$this->view->areaId = $areaId;
$this->view->areaName = $QArea->fetchRow(['id = ?' => $areaId])->toArray()['name'];
$this->view->listHistory = $listHistory;
