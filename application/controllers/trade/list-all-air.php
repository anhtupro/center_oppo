<?php


$QAppAir = new Application_Model_AppAir();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QStaff = new Application_Model_Staff();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QAppStatus = new Application_Model_AppStatus();
$title=$userStorage->title;
$id   = $this->getRequest()->getParam('id');
 $params = array(
            'id'    =>$id,
            'title'=>$title
            );
 $page           = $this->getRequest()->getParam('page', 1);
 $limit          = 5;
 $total          = 0;
$rows = $QAppAir->fetchPaginationAll($page, $limit, $total, $params);

$this->view->result   = $rows;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->params     = $params;
$this->view->url = HOST.'trade/list-all-air'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit*($page-1); 



