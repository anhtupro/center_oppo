<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$latitude = $this->getRequest()->getParam('latitude');
$longitude = $this->getRequest()->getParam('longitude');
$listTitle = $this->getRequest()->getParam('title');
$listContent = $this->getRequest()->getParam('content');

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$QTradeCheckIn = new Application_Model_TradeCheckIn();
$QTradeCheckInDetail = new Application_Model_TradeCheckInDetail();
$QTradeCheckInImage = new Application_Model_TradeCheckInImage();
$QHelper = new Application_Model_Helper();



if ($latitude && $longitude) {

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    
    $checkInId = $QTradeCheckIn->insert([
       'staff_id' => $userStorage->id,
       'longitude' => $longitude,
        'latitude' => $latitude,
        'created_at' => date('Y-m-d H:i:s')
    ]);

    foreach ($listTitle as $key => $title) {
        $QTradeCheckInDetail->insert([
           'check_in_id' => $checkInId,
           'title' =>  $title,
            'content' => $listContent[$key] ? $listContent[$key] : ''
        ]);
    }

    // save image
    $normalizedArrayFile = $QHelper->normalizedArrayFile($_FILES);
    
    foreach ($normalizedArrayFile as $nameAttr => $arrImage) {
        $target_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
            DIRECTORY_SEPARATOR . 'trade-check-in' . DIRECTORY_SEPARATOR . $checkInId . DIRECTORY_SEPARATOR;
        if (!is_dir($target_dir)) {
            @mkdir($target_dir, 0775, true);
        }
        foreach ($arrImage as $key => $image) {
            $extension = strtolower(pathinfo($image['name'],PATHINFO_EXTENSION));
            $newName = md5(date('Y-m-d H:i:s') . rand(1,1000)) . '.' . $extension;
            $target_file = $target_dir . $newName;

            // Check file size
            if ($image["size"] > 20000000) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Dung lượng file quá lớn, xin thử lại!'
                ]);
                return;
            }


            // check upload file
            if (! move_uploaded_file($image["tmp_name"], $target_file)) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Có lỗi trong quá trình upload file, xin vui lòng thử lại.'
                ]);
                return;
            }

            $path = '/photo/trade-check-in/' . $checkInId . '/' . $newName;

            //Resize Image
            $image = new My_SimpleImage();
            $image->load($target_file);
            $image->resizeToWidth(800);
            $image->save($target_file);
            //END Resize

            // upload image to server s3
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            $destination_s3 = 'photo/trade-check-in/' . $checkInId . '/';

            $upload_s3 = $s3_lib->uploadS3($target_file, $destination_s3, $newName);
            // upload image to server s3

            $QTradeCheckInImage->insert([
                'url' => $path,
                'check_in_id' => $checkInId
            ]);
        }
    }
    
    $db->commit();

    echo json_encode([
       'status' => 0
    ]);
    
} else {
    echo json_encode([
       'status' => 1,
       'message' => 'Chưa xác định được tọa độ. Vui lòng kiểm tra lại GPS và refresh lại trang. '
    ]);
    return;
}