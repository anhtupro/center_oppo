<?php

$store_from = $this->getRequest()->getParam('store_from');
$store_to = $this->getRequest()->getParam('store_to');
$warehouse_to = $this->getRequest()->getParam('warehouse_to');
$warehouse_from = $this->getRequest()->getParam('warehouse_from');
$category_id = $this->getRequest()->getParam('category_id');
$repair_details_type = $this->getRequest()->getParam('repair_details_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');
$image = $this->getRequest()->getParam('image-from');
$image_to = $this->getRequest()->getParam('image-to');
$transfer_type = $this->getRequest()->getParam('transfer_type');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QTransfer = new Application_Model_Transfer();
$QTransferDetails = new Application_Model_TransferDetails();
$QRepairDetailsTp = new Application_Model_RepairDetailsTp();
$QTransferFile = new Application_Model_TransferFile();
$QRepairType = new Application_Model_RepairType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QTransferReason = new Application_Model_TransferReason();
$flashMessenger       = $this->_helper->flashMessenger;

$data_repair_type = $QRepairType->fetchAll();
$repair_type = [];
foreach($data_repair_type as $key=>$value){
    $repair_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$params = array_filter(array(
    'store_id' => $store_id,
    'staff_id'  => $userStorage->id
));


if (!empty($submit)) {
    if(! $category_id){
        $flashMessenger->setNamespace('error')->addMessage('Không có hạng mục nào được chọn!');
        $this->_redirect(HOST . 'trade/create-transfer');
    }
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        $transfer = [
            'store_from'  => $store_from ? $store_from : Null,
            'store_to'  => !empty($store_to) ? $store_to : 0,
            'warehouse_to' => $warehouse_to ? $warehouse_to : Null,
            'warehouse_from' => $warehouse_from ? $warehouse_from : Null,
            'area_to' => $warehouse_to ? $this->storage['area_id'][0] : Null,
            'area_from' => $warehouse_from ? $this->storage['area_id'][0] : Null,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'staff_id'  => $userStorage->id
        ];
        
        $transfer_id = $QTransfer->insert($transfer);

        foreach($category_id as $key=>$value){


             //nếu các hạng mục đặc biệt như biển hiệu 43, tranh biển hiệu 48, vách hình ảnh 803
            if(in_array($value, [43,48,803])){

                $flag_category_spec = 1;
            }
            $where_update = $QTransfer->getAdapter()->quoteInto('id = ?', $transfer_id);
            $data_update=array(
                'manager_approve'      => $flag_category_spec
            );
            $QTransfer->update($data_update,$where_update);
            //END
            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);
            if($number == 0){
                $flashMessenger->setNamespace('error')->addMessage('Số lượng hạng mục không hợp lệ!');
                $this->_redirect(HOST . 'trade/create-transfer');
            }
            
            $transfer_details = [
                'transfer_id' => $transfer_id,
                'category_id'   => $value,
                'quantity'  => $number,
                'transfer_reason_id' => $repair_details_type[$key],
                'imei_sn' => $imei[$key],
                'note'  => $note[$key],
                'transfer_type' => $transfer_type[$key],
                'status'    => 1,
            ];
            
            $transfer_details_id = $QTransferDetails->insert($transfer_details);
            
            $tag = 0;
            
            foreach($image[$key] as $k=>$v){
                
                $data = $v;
                
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id. DIRECTORY_SEPARATOR . $transfer_details_id. DIRECTORY_SEPARATOR . '1';

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                
                $url = '/photo/transfer/' . $transfer_id. '/' . $transfer_details_id . '/1/image_'.$k.'.png';

                file_put_contents($uploaded_dir.'/image_'.$k.'.png', $data);
                
                $rdf_insert = [
                    'transfer_details_id' => $transfer_details_id,
                    'url'    => $url,
                    'type'   => 1
                ];
                $QTransferFile->insert($rdf_insert);
                $tag = 1;
            }
            
            if($tag == 0){
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload Hình ảnh hạng mục điều chuyển!');
                $this->_redirect(HOST . 'trade/create-transfer');
            }
            
            $tag = 0;
            
            foreach($image_to[$key] as $k=>$v){
                
                $data = $v;
                
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id. DIRECTORY_SEPARATOR . $transfer_details_id. DIRECTORY_SEPARATOR . '2';

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                
                $url = '/photo/transfer/' . $transfer_id. '/' . $transfer_details_id . '/2/image_'.$k.'.png';

                file_put_contents($uploaded_dir.'/image_'.$k.'.png', $data);
                
                $rdf_insert = [
                    'transfer_details_id' => $transfer_details_id,
                    'url'    => $url,
                    'type'   => 2
                ];
                $QTransferFile->insert($rdf_insert);
                $tag = 1;
            }
            
            if($tag == 0){
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload Hình ảnh khu vực đặt vật dụng!');
                $this->_redirect(HOST . 'trade/create-transfer');
            }
            

        }
        
        $db->commit();
        
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        
        $this->_redirect(HOST . 'trade/create-transfer');
        
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
        
        $this->_redirect(HOST . 'trade/create-transfer');
        
    }
    
    
    
}



$result = $QCategory->getCategory();
$this->view->category = $result;
$this->view->repair_type = $repair_type;
$this->view->transfer_reason = $QTransferReason->fetchAll()->toArray();


$resultCamp = $QCampaign->getListStore($params);
$this->view->store = $resultCamp;

$params['area_id'] = $this->storage['area_id'];

$store_to = $QCampaign->getListStoreArea($params);
$this->view->store_to = $store_to;

$this->view->params = $params;

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>