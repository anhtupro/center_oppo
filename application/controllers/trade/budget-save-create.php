<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$listAreaId = $this->getRequest()->getParam('area_id');
$totalBudget = $this->getRequest()->getParam('total_budget');
$detailBudget = $this->getRequest()->getParam('detail_budget');
$year = $this->getRequest()->getParam('year');

$QBudget = new Application_Model_Budget();
$QBudgetDetail = new Application_Model_BudgetDetail();

$db = Zend_Registry::get('db');
if (! $year) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng nhập năm'
    ]);
    return;
}

try {
    $db->beginTransaction();

    foreach ($listAreaId as $areaId) {
        //check if area_id and year is existed
        if ($QBudget->isExisted($areaId, $year)) {
            echo json_encode([
                'status' => 1,
                'message' => 'Khu vực và năm đã tồn tại'
            ]);
            return;
        }
        //save
        $budgetId = $QBudget->insert([
            'area_id' => $areaId,
            'total_value' => $totalBudget[$areaId] ? $totalBudget[$areaId] : Null,
            'year' => $year
        ]);

        foreach ($detailBudget[$areaId] as $type => $budgetValue) {
            $QBudgetDetail->insert([
                'budget_id' => $budgetId,
                'type' => $type,
                'value' => $budgetValue ? $budgetValue : Null
            ]);
        }
    }

    $db->commit();
    echo json_encode([
       'status' => 0
    ]);
    return;
} catch (\Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => 'Có lỗi trong quá trình lưu'
    ]);
    return;
}
