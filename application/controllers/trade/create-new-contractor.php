<?php
	$name 			= $this->getRequest()->getParam('name');
	$short_name 	= $this->getRequest()->getParam('short_name');
	$address 		= $this->getRequest()->getParam('address');
	$desc 			= $this->getRequest()->getParam('desc');
	$code 			= $this->getRequest()->getParam('code');
	$username 			= $this->getRequest()->getParam('username');
	$password 			= $this->getRequest()->getParam('password');
	$submit 		= $this->getRequest()->getParam('submit');
	$QContructors 		= new Application_Model_Contructors();
	$QStaffContractor 	= new Application_Model_StaffContractor();
	$flashMessenger       = $this->_helper->flashMessenger;
	
if(!empty($submit)){
try{
	$created_at=date('Y-m-d H:i:s');
$get_name = explode(' ', $name);
$lastname = $get_name[count($get_name)-1];
$firstname = $get_name[count($get_name)-3].' '.$get_name[count($get_name)-2];
$email = $QStaffContractor->stripUnicode(strtolower($get_name[count($get_name)-2]).strtolower($get_name[count($get_name)-1]));
	$staff_data = [
		'contract_type'			=> 1,
		'contract_signed_at'	=> $created_at,
		'contract_expired_at'	=> $created_at,
		'department'			=> 156,
		'team'					=>1,
		'title'					=> 199,
		'joined_at'				=> $created_at,
		 'off_type'				=> 0,
		'dob'					=> $created_at,
		'regional_market'		=> 4188,
		//'ID_number'				=> 080089000042,
		'ID_date'				=> $created_at,
		'nationality'			=> 4,
		'religion'				=> 3,
		//'phone_number'			=> 0948209404,
		'updated_by'			=> 5899,
		'company_id'			=> 1,
		'marital_status'		=> 1,
		'firstname'				=> $firstname,
		'lastname'				=> $lastname,
		'email'					=> $username.'@oppomobile.vn',
		'password'				=> md5($password)
	];

$id = $QStaffContractor->insert($staff_data);

	$data = [
		'name'	=> $name,
		'short_name'	=> $short_name,
		'address'	=> $address,
		'desc'		=> $desc,
		'code'		=> $code,
		'type'		=> 2,
		'user'		=> $id
	];
	$QContructors->insert($data);
	$flashMessenger->setNamespace('success')->addMessage('Tạo nhà thầu thành công !');
	$this->_redirect(HOST . 'trade/create-new-contractor');
}catch (Exception $e) {
			$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
			$this->_redirect(HOST . 'trade/create-new-contractor');
		}

}

	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
?>