<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$storeId = $this->getRequest()->getParam('store_id');

$QCheckShopOutside = new Application_Model_CheckShopOutside();
$QCheckShopOutsideDetail = new Application_Model_CheckShopOutsideDetail();

$where = [
    'store_id = ?' => $storeId,
    'is_deleted = ?' => 0
];
$listHistory = $QCheckShopOutside->fetchAll($where, 'created_at DESC')->toArray();

//get information shop
$db = Zend_Registry::get("db");
$select = $db->select()
             ->from(['s' => DATABASE_TRADE . '.store_outside'], [
                    'district_name' => 'd.name',
                    'area_name' => 'a.name',
                    'store_name' => 's.name',
                   'province_name' => 'e.name'
             ])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['e' => 'regional_market'], 's.regional_market = e.id', [])
            ->joinLeft(['a' => 'area'], 'e.area_id = a.id', [])
            ->where('s.id = ?', $storeId)
            ->where('s.del  = 0 or s.del is null');
$storeInfo = $db->fetchRow($select);

//get history checkshop outside
$listHistory = $QCheckShopOutside->getHistory($storeId);

echo json_encode([
   'status' => 0,
   'listHistory' => $listHistory,
    'storeInfor' => $storeInfo
]);


