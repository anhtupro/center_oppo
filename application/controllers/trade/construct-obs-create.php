<?php

$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$Store = new Application_Model_Store();
$QContructors = new Application_Model_Contructors();

$params = array_filter(array(
    'area_list' => $this->storage['area_id']
));


$list_category_outside = $QCategory->fetchAll(['type = ?' => 3], 'name')->toArray();
$list_category_inside = $QCategory->fetchAll(['type = ?' => 4], 'name')->toArray();
$list_contractor = $QContructors->fetchAll()->toArray();

$list_store = $Store->getStoreBrandshop(NULL);
// echo "<pre>";
// print_r($list_store);
// echo "</pre>";
// die();
$this->view->list_store = $list_store;

$this->view->list_category_outside = $list_category_outside;
$this->view->list_category_inside = $list_category_inside;
$this->view->list_contractor = $list_contractor;