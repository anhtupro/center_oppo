<?php

$campaign_id = $this->getRequest()->getParam('campaign_id');
$input_quantity = $this->getRequest()->getParam('input_quantity');
$category_id = $this->getRequest()->getParam('category_id');
$area_confirm = $this->getRequest()->getParam('area_confirm');
$QCampaign = new Application_Model_Campaign();
$QArea = new Application_Model_Area();
$QCampaignArea = new Application_Model_CampaignArea();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppStatus = new Application_Model_AppStatus();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$title = $userStorage->title;
if (in_array($userStorage->id, [9837])) {
    $title = TRADE_MARKETING_SUPERVISOR;
}

$area = $QCampaignArea->getAreaByCampaign($campaign_id);

//9837 binhsang.ly
if (!empty($this->storage['area_id']) AND $this->storage['staff_id'] != 9837) {
    $area = [];
    foreach ($QCampaignArea->getAreaByCampaign($campaign_id) as $key => $value) {
        if (in_array($key, $this->storage['area_id'])) {
            $area[$key] = $value;
        }
    }
}

$params = [
    'campaign_id' => $campaign_id,
    'area_id' => array_keys($area)
];


$area_status = $QCampaignArea->get_status_area($params);
$min_status = $QCampaignArea->get_status_min($params);
$max_status = $QCampaignArea->get_status_max($params);
$status_title = $QAppStatusTitle->get_cache_title(3);
$status_title = $status_title[$title];

$app_status = $QAppStatus->get_cache(3);
$app_status_title_cache = $QAppStatusTitle->get_cache(3);

$app_status_title = [];

foreach ($this->storage['title_list'] as $key => $value) {
    $app_status_title = array_merge((array)$app_status_title, (array)$app_status_title_cache[$value]);
}


$where = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
$campaign = $QCampaign->fetchRow($where);

$category = $QCampaign->getCategory($params);
$category_campaign = $QCampaign->getCategoryCampaign($params);

$where = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
$campaign_area_check = $QCampaignArea->fetchAll($where);

//Get data campaign_area
$params['confirm'] = 1;
$campaign_area = $QCampaignArea->getCampaignArea($params);

$data = [];
foreach ($campaign_area as $key => $value) {
    $data[$value['area_id']][$value['category_id']] = $value;
}
//END Get data


//Get data campaign_area
$contractor_category = $QCampaignArea->getContractorCategory($params);
$data_price = [];
foreach ($contractor_category as $key => $value) {
    $data_price[$value['area_id']][$value['category_id']] = $value;
}
//END Get data

$flashMessenger = $this->_helper->flashMessenger;


$this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $flashMessenger->setNamespace('success')->getMessages();

$this->view->area = $area;
$this->view->campaign = $campaign;
$this->view->category = $category;
$this->view->category_campaign = $category_campaign;
$this->view->data = $data;
$this->view->data_price = $data_price;
$this->view->min_status = $min_status;
$this->view->max_status = $max_status;
$this->view->status_title = $status_title;
$this->view->app_status_title = $app_status_title;
$this->view->area_status = $area_status;
$this->view->staff_title = $userStorage->title;
$this->view->area_storage = $this->storage['area_id'];



