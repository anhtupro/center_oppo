<?php 
	$campaign_id    = $this->getRequest()->getParam('campaign_id');
	$input_code 	= $this->getRequest()->getParam('input_code');

	$price   		= $this->getRequest()->getParam('price');
	$quantity_order = $this->getRequest()->getParam('quantity_order');

	$category_id    = $this->getRequest()->getParam('category_id');
	$area_id    	= $this->getRequest()->getParam('area_id');
	$submit     	= $this->getRequest()->getParam('submit');
	$confirm     	= $this->getRequest()->getParam('confirm');
	$contractor_id  = $this->getRequest()->getParam('contractor_id');


$QCampaign 	   			= new Application_Model_Campaign();
	$QCategory 	   			= new Application_Model_Category();
	$QArea 		   			= new Application_Model_Area();
	$QCampaignArea 			= new Application_Model_CampaignArea();
	$QCampaignContractor 	= new Application_Model_CampaignContractor();
	$QCampaignAreaContractor	= new Application_Model_CampaignAreaContractor();
	$QAppStatusTitle			= new Application_Model_AppStatusTitle();
	$QContructors			= new Application_Model_Contructors();
	$QContractorPrice		= new Application_Model_ContractorPrice();
	$QContractorCategory	= new Application_Model_ContractorCategory();

	$flashMessenger = $this->_helper->flashMessenger;

	$params = [
		'campaign_id'    => $campaign_id,
		'contractor_id'  => $contractor_id,
                'area_id'        => $this->storage['area_id']
	];

	$area           	= $QArea->get_cache_bi();
	$contructor     	= $QContructors->getCode();
        
	$contractor_price	= $QContractorPrice->getPrice();
        
	$contractor_code	= $QContractorPrice->getContractorDefault($params);
        
	$category_code 		= $QCategory->getCode();
        $category_cache 	= $QCategory->get_cache();


	$area_status = $QCampaignArea->get_status_area($params);
	$max_status  = $QCampaignArea->get_status_max($params);
	$min_status  = $QCampaignArea->get_status_min($params);
	$status_title = $QAppStatusTitle->get_cache_title(3);


	$status_title = $status_title[$this->storage['title']];
        

	//$campaign_contractor = $QCampaignContractor->getData($params);



	$where 		   = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign 	   = $QCampaign->fetchRow($where);

	$category      		= $QCampaign->getCategory($params);
	$category_campaign 	= $QCampaign->getCategoryCampaign($params);

	$category_cache = [];
	foreach($category as $key=>$value){
		$category_cache[$value['id']] = $value['name'];
	}


	$where 					= $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
	$campaign_area_check 	= $QCampaignArea->fetchAll($where);

//	if(count($campaign_area_check) == 0){
//
//		$campaign_area_insert = $QCampaignArea->getCampaignAreaInsert($params);
//		foreach($campaign_area_insert as $key=>$value){
//			$QCampaignArea->insert(
//				[
//					'campaign_id'  	 => $value['campaign_id'],
//					'area_id' 	  	 => $value['area_id'],
//					'category_id' 	 => $value['category_id'],
//					'price' 		 => $value['price'],
//					'status' 		 => 1,
//				]
//			);
//		}
//
//	}



	//Get data campaign_area
	$campaign_area = $QCampaignArea->getCampaignArea($params);
	$data = [];
	foreach($campaign_area as $key=>$value){
		$data[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data

	//Get data campaign_area
	$contractor_category = $QCampaignArea->getContractorCategory($params);
	$data_price = [];
	foreach($contractor_category as $key=>$value){
		$data_price[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data

	//Get data campaign_area_contractor
	$campaign_area_contractor = $QCampaignAreaContractor->getCampaignAreaContractor($params);
	$data_area_contractor = [];
	foreach($campaign_area_contractor as $key=>$value){
		$data_area_contractor[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get campaign_area_contractor

	$back_url = HOST.'trade/contractor-category?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;

	if(!empty($submit) AND $submit == 2){

		require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        //Phần đầu
        $heads[] = '';
        $heads[] = '';

        foreach($category_campaign as $key=>$value){
        	$heads[] = $value['id'];
        }

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
   
        $alpha = 'A';
        $index = 2;
        //END Phần đầu

        //Phần đầu 2
        $sheet->setCellValue($alpha . $index, 'ID Khu vực');
        $alpha++;
        $sheet->setCellValue($alpha . $index, 'Khu vực');
        $alpha++;

        foreach($category_campaign as $key=>$value){
        	$col_1 = $alpha . $index;

        	$sheet->setCellValue($alpha . $index, $value['name']);
        	$alpha++;
        }

        $index = 3;
        //END Phần đầu 2

        //KHU VỰC
        foreach($area as $key=>$value){
        	$alpha = 'A';
        	$sheet->getCell($alpha++ . $index)->setValueExplicit($key ,PHPExcel_Cell_DataType::TYPE_STRING);
        	$sheet->getCell($alpha++ . $index)->setValueExplicit($value ,PHPExcel_Cell_DataType::TYPE_STRING);
        	
        	foreach($category_campaign as $k=>$v){

        		$code = !empty($contractor_code[$key][$v['category_id']]) ? $contractor_code[$key][$v['category_id']] : NULL;

	        	$sheet->getCell($alpha++ . $index)->setValueExplicit($code ,PHPExcel_Cell_DataType::TYPE_STRING);
	        }
	        $index++;
        }
        //END Khu vực


        $filename = ' Share Contractor ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
	}



if(!empty($confirm) AND $confirm == 1){
 

        if($_FILES["file"]["tmp_name"] != ""){

			$db = Zend_Registry::get('db');
			$db->beginTransaction();
			try {

				include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';

				error_reporting(0);
				set_time_limit(0);
				
				//===================Main=======================

				if($_FILES["file"]["tmp_name"] != ""){
					if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
						//echo ("Không phải file XLSX !");exit;
					}
					if ($_FILES["file"]["error"] > 0)
				    {
				    	echo ("Return Code: " . $_FILES["file"]["error"] . "<br />");exit;
				    }
					move_uploaded_file($_FILES["file"]["tmp_name"], "files/limit_campaign/share-contractor.xlsx");
				}

				$inputfile = 'files/limit_campaign/share-contractor.xlsx';
				$xlsx = new SimpleXLSX($inputfile);
				$data_xlsx = $xlsx->rows();

				//Đếm số cột
				$num_cols = count($category_campaign)+2;

				//Đếm số dòng
				$i = 1;
				$num_rows = 0;
				while ($data_xlsx[$i++][0] <> '') {
				  $num_rows++;
				}

				$where_del = $QContractorCategory->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
				$QContractorCategory->delete($where_del);

				$sql = "INSERT INTO `".DATABASE_TRADE."`.`contractor_category`(campaign_id, contractor_id, area_id, category_id, price, status) VALUES";

				for($i = 2; $i <= $num_rows; $i++){

					$area_id = $data_xlsx[$i][0];

					for($j = 2; $j <= $num_cols; $j++){
						$cat_code 	= $data_xlsx[0][$j];
						$cat_id     = $category_code[$cat_code];
						$input_code = $data_xlsx[$i][$j];

						
						if(!empty($cat_id) AND !empty($area_id) AND !empty($input_code)){

                            //Xử lý chính
							if(!empty($contractor_price[$contructor[$input_code]][$cat_id])){
								$sql .= "(".$campaign_id.", ".$contructor[$input_code].", ".$area_id.", ".$cat_id.", ".$contractor_price[$contructor[$input_code]][$cat_id].", 1),";
							}
							else{
								$db->rollBack();
								$flashMessenger->setNamespace('error')->addMessage('Price NULL : '.$input_code.'/'.$category_cache[$cat_id]);
								$this->redirect($back_url);
							}
							
							//END Xử lý chính

						}
						
					}
					
				}

				$sql 		= rtrim($sql, ',');

	            //INSERT 
	            if(!empty($sql)){
		            $db = Zend_Registry::get('db');
					$stmt = $db->prepare($sql);
		            $stmt->execute();
		            $stmt->closeCursor();
	        	}

				unlink($inputfile);

				//Cập nhật lại status campaign_area
//				$where = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
//				$campaign_area = $QCampaignArea->fetchRow($where);
//				$QCampaignArea->update( ['status'=>($min_status+1)] , $where);
				//END cập nhật lại status campaign_area

				//=================Function Area===================
			    $flashMessenger->setNamespace('success')->addMessage('Chia nhà thầu theo từng khu vực, từng hạng mục thành công!');
				$db->commit();
				$this->redirect($back_url);
				
			} catch (Exception $e) {
				$db->rollBack();
				$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
				$this->redirect($back_url);
			}

		}
		else{

			$db = Zend_Registry::get('db');
			$db->beginTransaction();
			try {

				$where_del = $QContractorCategory->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
				$QContractorCategory->delete($where_del);

				$sql = "INSERT INTO `".DATABASE_TRADE."`.`contractor_category`(campaign_id, contractor_id, area_id, category_id, price, status) VALUES";

				$current_date = date('Y-m-d H:i:s');

				foreach($input_code as $key=>$value){
					if(!empty($value) AND !empty($contructor[$value])){
						if(!empty($contractor_price[$contructor[$value]][$category_id[$key]])){

							$sql .= "(".$campaign_id.", ".$contructor[$value].", ".$area_id[$key].", ".$category_id[$key].", ".$contractor_price[$contructor[$value]][$category_id[$key]].", 1),";

						}
						else{
							$db->rollBack();
							$flashMessenger->setNamespace('error')->addMessage('Price NULL : '.$value.'/'.$category_id[$key]);
							$this->redirect($back_url);
						}
						
					}
					
				}
				$sql 		= rtrim($sql, ',');

                //INSERT 
	            $db = Zend_Registry::get('db');
				$stmt = $db->prepare($sql);
	            $stmt->execute();
	            $stmt->closeCursor();

				$flashMessenger->setNamespace('success')->addMessage('Chia nhà thầu theo từng khu vực, từng hạng mục thành công!');
				$db->commit();
				$this->redirect($back_url);
			} catch (Exception $e) {
				$db->rollBack();
				$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
				$this->redirect($back_url);
			}
		}
	}


	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;
	

	$this->view->area     		= $area;
	$this->view->campaign 		= $campaign;
	$this->view->category 		= $category;
	$this->view->data     		= $data;
	$this->view->contractor     = $QCampaign->getContractor($params);
	$this->view->params         = $params;
	$this->view->data_area_contractor 	= $data_area_contractor;
	$this->view->data_price 			= $data_price;
	$this->view->category_campaign 		= $category_campaign;
	$this->view->area_status        = $area_status;
	$this->view->max_status         = $max_status;
	$this->view->min_status         = $min_status;
	$this->view->status_title 		= $status_title;
	$this->view->contractor_code    = $contractor_code;


