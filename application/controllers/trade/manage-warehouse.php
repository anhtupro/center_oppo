<?php

$areaId = $this->getRequest()->getParam('area_id');
$categoryId = $this->getRequest()->getParam('category_id');
$export = $this->getRequest()->getParam('export');

$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;

$QInventoryArea = new Application_Model_AirInventoryArea();
$QArea = new Application_Model_Area();
$QCategory = new Application_Model_Category();

$params = [
    'area_id' => $areaId,
    'category_id' => $categoryId,
    'export' => $export
];

$listInventory = $QInventoryArea->fetchPagination($page, $limit, $total, $params);
$listCategory = $QCategory->getAll();
$listArea = $QArea->getAll();

if ($export) {
    $QInventoryArea->export($listInventory);
}

$this->view->listInventory = $listInventory;
$this->view->listArea = $listArea;
$this->view->listCategory = $listCategory;
$this->view->areaId = $areaId;
$this->view->categoryId = $categoryId;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'trade/manage-warehouse' . '?';
