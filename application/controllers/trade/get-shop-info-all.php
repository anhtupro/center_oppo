<?php 
	
	$store_id 	 = $this->getRequest()->getParam('id_shop');
	$category_id = $this->getRequest()->getParam('category_id');
	$category_id = 1;

	$params = [
		'store_id' 		=> $store_id,
		'category_id' 	=> $category_id
	];

	$QStore 			= new Application_Model_Store();
	$QBiTrade 			= new Application_Model_BiTrade();
	$QKpiMonth  		= new Application_Model_KpiMonth();
	$QCheckshopDetail   = new Application_Model_AppCheckshopDetail();

	$checkshop = $QCheckshopDetail->getDataCheckshop($params);

	$where = $QStore->getAdapter()->quoteInto("id = ?", $store_id);
    $store = $QStore->fetchRow($where);

    $investments = $QBiTrade->getDataByStore($store_id);

    $sellout_lastmonth = $QBiTrade->getSelloutLastMonth($store_id);

    $investments_img = $QKpiMonth->get_investments_by_store($params);

    $rand = rand(1,4);

    /* Tính hạn mức */
    $hanmuc = 0;
    if($sellout_lastmonth['quantity'] >= 5 AND $sellout_lastmonth['quantity'] <15){
    	if($category_id == 1){
    		$hanmuc = 1;
    	}
    	else{
    		$hanmuc = 0;
    	}
    }
    elseif($sellout_lastmonth['quantity'] >= 15 AND $sellout_lastmonth['quantity'] <50){
    	if($category_id == 1){
    		$hanmuc = 2;
    	}
    	else{
    		$hanmuc = 0;
    	}
    }
    elseif($sellout_lastmonth['quantity'] >= 50){
    	if($category_id == 1){
    		$hanmuc = 3;
    	}
    	else{
    		$hanmuc = 1;
    	}
    }
    /* END Tính hạn mức */

	$data = [];
	
	//Hạn mức
	$data['hanmuc'] = [
		'store_id'		=> $store_id,
		'han_muc'		=> $hanmuc,
		'da_dang_ky'	=> 0,
		'shop_name'		=> $store['name']
	];


	//Đầu tư
	$data['dautu'][] = [
		'ten_hang_muc' => '',
		'so_luong'	   => 0,
		'gia'	   	   => 0,
		'thanh_tien'   => 0,
		'ngay'		   => ''
	];

	foreach ($investments as $key => $value) {

		$data['dautu'][] = [
			'ten_hang_muc' => $value['category_name'],
			'so_luong'	   => $value['quantity'],
			'ngay'		   => date('d/m/Y', strtotime($value['invested_at'])),
			'gia'		   => $value['value'],
			'id_hang_muc'  => $value['category_id']
		];

		if($value['category_id'] == $category_id){
			$data['hanmuc']['da_dang_ky'] = $value['quantity'];
		}
	}
	

	//Hình ảnh
	$data['hinhanh'][] = [
		'ten_hang_muc' => '',
		'url'	   	   => '',
	];

	foreach ($investments_img as $key => $value) {

		if(isset($value['photo']) and $value['photo']){
			$data['hinhanh'][] = [
				'ten_hang_muc' => $value['name'],
				'url'	   	   => "https://trade-marketing.opposhop.vn/photo/investment/".$value['id']."/".$value['photo'],
			];
		}

		if(isset($value['photo1']) and $value['photo1']){
			$data['hinhanh'][] = [
				'ten_hang_muc' => $value['name'],
				'url'	   	   => "https://trade-marketing.opposhop.vn/photo/investment/".$value['id']."/".$value['photo1'],
			];
		}

		if(isset($value['photo2']) and $value['photo2']){
			$data['hinhanh'][] = [
				'ten_hang_muc' => $value['name'],
				'url'	   	   => "https://trade-marketing.opposhop.vn/photo/investment/".$value['id']."/".$value['photo2'],
			];
		}

		
	}

	//Số lượng checkshop gần nhất
	$data['checkshop'] = [];

	foreach($checkshop as $key=>$value){
		$data['checkshop'][] = [
			'category_id' 	=> $value['category_id'],
			'category_name' => $value['category_name'],
			'group_bi'		=> $value['group_bi'],
		];
	}
	//END Số lượng checkshop gần nhất

	echo json_encode($data);exit;




