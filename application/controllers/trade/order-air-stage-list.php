<?php
$areaId = $this->getRequest()->getParam('area_id');
$additionalPartType = $this->getRequest()->getParam('additional_part_type');
$fromDate = $this->getRequest()->getParam('from_date');
$toDate = $this->getRequest()->getParam('to_date');
$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;

$QOrderAirStage = new Application_Model_OrderAirStage();
$QAppNoti = new Application_Model_AppNoti();

$params = [
    'area_list' => $this->storage['area_id'],
    'area_id' => $areaId,
    'from_date' => $fromDate,
    'to_date' => $toDate,
    'additional_part_type' => $additionalPartType
];

$listStage = $QOrderAirStage->fetchPagination($page, $limit, $total, $params);

if ($additionalPartType) {
    $additionalOrderCost = $QAppNoti->getAdditionalCost($params);
    $this->view->additionalOrderCost = $additionalOrderCost;
}


$this->view->listStage = $listStage;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'trade/order-air-stage-list' . ($params ? '?' . http_build_query($params) . '&' : '?');