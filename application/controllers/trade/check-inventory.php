<?php


$areaId = $this->getRequest()->getParam('area_id');

$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QInventoryArea = new Application_Model_AirInventoryArea();
$QCategoryInventoryType = new Application_Model_CategoryInventoryType();
//$QInventoryAreaTest = new Application_Model_AirInventoryAreaTest();
$QCategoryMaterial = new Application_Model_CategoryMaterial();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();

$params = [
  'area_id' => $areaId
];


//$listCategory = $QCategory->getCategoryCheckshop($params);
$listCategory = $QCategory->getCategoryAirStatistic($params);
//$currentCategories = $QInventoryArea->getCategory($params);
$currentCategories = $QAppCheckshopDetailChild->getCategoryWarehouse($params);

$categoryType = $QCategoryInventoryType->getType();

$listMaterial = $QCategoryMaterial->fetchAll();

$listTypeChild = $QCategoryInventoryType->fetchAll(['is_child = ?' => 1]);
$listCategoryPainting = $QAppCheckshopDetailChildPainting->getPainting($params);
$listIdCategoryCheckshop = $QCategory->getIdCategoryCheckshop($params);

$this->view->listIdCategoryCheckshop = $listIdCategoryCheckshop;
$this->view->listCategoryPainting = $listCategoryPainting;
$this->view->listTypeChild = $listTypeChild;
$this->view->listMaterial = $listMaterial;
$this->view->listCategory = $listCategory;
$this->view->currentCategories = $currentCategories;
$this->view->categoryType = $categoryType;
$this->view->areaId = $areaId;
$this->view->areaName = $QArea->fetchRow(['id = ?' => $areaId])->toArray()['name'];


