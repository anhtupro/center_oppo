<?php 
	
	$data 			= $this->getRequest()->getParam('data');
	$campaign_id 	= $this->getRequest()->getParam('campaign_id');

	$QAppContractorOut = new Application_Model_AppContractorOut();

	$flashMessenger = $this->_helper->flashMessenger;

	$data = json_decode($data, true);

	$back_url = HOST.'trade/contractor-quantity?campaign_id='.$campaign_id;

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		foreach($data as $key=>$value){
			foreach ($value as $k => $v) {
				if($v){

					$where = [];
					$where[] = $QAppContractorOut->getAdapter()->quoteInto('contractor_quantity_id = ?',$key);
					$where[] = $QAppContractorOut->getAdapter()->quoteInto('area_id = ?',$k);

					$contractor_out = $QAppContractorOut->fetchRow($where);

					if(!$contractor_out){
						$data_out = [
							'contractor_quantity_id' => $key,
							'area_id'				 => $k,
							'quantity'				 => $v,
							'out_date'				 => date('Y-m-d H:i:s'),
							'status'				 => 1,
							'created_by'			 => $this->storage['staff_id']
						];

						$QAppContractorOut->insert($data_out);
					}
					else{
						$data_out = [
							'quantity'				 => $v,
							'updated_by'			 => $this->storage['staff_id'],
							'updated_at'			 => date('Y-m-d H:i:s'),
						];

						$QAppContractorOut->update($data_out,$where);
					}
					
				}
				
			}
		}


		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Thành công!');
    	$this->redirect($back_url);

	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;








