<?php
$contract_number = $this->getRequest()->getParam('contract_number');
$contract_number = TRIM($contract_number);

$params = [
  'contract_number' => $contract_number
];

$QContractNumber = new Application_Model_ContractNumber();

$list_air_contract = $QContractNumber->getAirContractDetail($contract_number);
$list_repair_contract = $QContractNumber->getRepairContractDetail($contract_number);
$list_transfer_contract = $QContractNumber->getTransferContractDetail($contract_number);
$list_destruction_contract = $QContractNumber->getDestructionContractDetail($contract_number);

$list_air_cost = $QContractNumber->getAirContractCost($contract_number);
$list_repair_cost = $QContractNumber->getRepairContractCost($contract_number);
$list_transfer_cost = $QContractNumber->getTransferContractCost($contract_number);
$list_destruction_cost = $QContractNumber->getDestructionContractCost($contract_number);

// get contractor name
if ($list_air_contract[0]['contractor_name']) {
    $contractor_name = $list_air_contract[0]['contractor_name'];
} elseif ($list_repair_contract[0]['contractor_name']) {
    $contractor_name = $list_repair_contract[0]['contractor_name'];
} elseif ($list_transfer_contract[0]['contractor_name']) {
    $contractor_name = $list_transfer_contract[0]['contractor_name'];
} elseif ($list_destruction_contract[0]['contractor_name']) {
    $contractor_name = $list_destruction_contract[0]['contractor_name'];
}


$this->view->list_air_contract = $list_air_contract;
$this->view->list_repair_contract = $list_repair_contract;
$this->view->list_transfer_contract = $list_transfer_contract;
$this->view->list_destruction_contract = $list_destruction_contract;
$this->view->contractor_name = $contractor_name;

$this->view->list_air_cost = $list_air_cost;
$this->view->list_repair_cost = $list_repair_cost;
$this->view->list_transfer_cost = $list_transfer_cost;
$this->view->list_destruction_cost = $list_destruction_cost;

$this->view->params = $params;

$flashMessenger       = $this->_helper->flashMessenger;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;