    <?php

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

//    $month = $this->getRequest()->getParam('month');
//    $month = $month ? $month : date('m');
//    $year = $year ? $year : date('Y');
    $fromDate = $this->getRequest()->getParam('from_date');
    $fromDate = $fromDate ? date('Y-m-d', strtotime($fromDate)) : date('Y-01-01');

    $toDate = $this->getRequest()->getParam('to_date');
    $toDate = $toDate ? date('Y-m-d', strtotime($toDate)) : date('Y-m-d');

    $year = date('Y');
    $areaId = $this->getRequest()->getParam('area_id');

    $QAppNoti = new Application_Model_AppNoti();
    $QBudget = new Application_Model_Budget();
    $QArea = new Application_Model_Area();
    $QCategory = new Application_Model_Category();
    $QStoreTempStop = new Application_Model_StoreTempStop();
    $QKpiMonth = new Application_Model_KpiMonth();

    $params = [
        'area_list' => $this->storage['area_id'] ? $this->storage['area_id'] : [0],
        'list_area' => $this->storage['area_id'] ? $this->storage['area_id'] : [0],
        'title' => $this->storage['title'],
//        'year' => date('Y'),
        'month' => $month,
        'year' => $year,
        'from_date' => $fromDate,
        'to_date' => $toDate,
        'area_id' => $areaId,
        'is_staff_realme' => $this->storage['is_staff_realme']
    ];

    if (in_array($this->storage['title'], [SALES_TITLE, PGS_SUPERVISOR, SALES_LEADER_TITLE])) {
        $params['staff_id'] = $this->storage['staff_id'];
    }

    // phân loại staff để chọn ra store quản lý
    if (in_array($this->storage['title'], [SALES_TITLE, PGS_SUPERVISOR])) {
        $params['type_staff'] = 1;
    } elseif (in_array($this->storage['title'], [SALES_LEADER_TITLE])) {
        $params['type_staff'] = 2;
    } else {
        $params['type_staff'] = 3;
    }

    $params['type'] = 4;
    $params['table'] = DATABASE_TRADE . '.repair';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'staff_id';
    $repair = $QAppNoti->getPendingRepair($params);
    $repair_success = $QAppNoti->getSuccessRepair($params);

    $params['type'] = 12;
    $params['table'] = DATABASE_TRADE . '.destruction';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'staff_id';
    $destruction = $QAppNoti->getPendingRepair($params);
    $destruction_success = $QAppNoti->getSuccessRepair($params);


    $params['type'] = 11;
    $params['table'] = 'remove_shop';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'created_by';
    $remove_shop = $QAppNoti->getPendingRepair($params);
    $remove_shop_success = $QAppNoti->getSuccessRepair($params);


    // transfer
    $params['type'] = 6;
    $params['table'] = DATABASE_TRADE . '.transfer';
    $params['store_id'] = 'store_from';
    $params['created_by'] = 'staff_id';
    $transfer = $QAppNoti->getPendingRepair($params);
    $transfer_success = $QAppNoti->getSuccessRepair($params);


    $params['type'] = 8;
    $params['table'] = DATABASE_TRADE . '.app_air';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'created_by';
    $air = $QAppNoti->getPendingRepair($params);
    $air_success = $QAppNoti->getSuccessRepair($params);

//    $checkshop_info = $QAppNoti->getCheckshopInfo($params);
    $count_checkshop = $QAppNoti->getCountCheckshop($params);

    // Ngân sách theo tháng
    $params['cost_type'] = 1;

//    $params['status_finish'] = 7;
    $destructionCost = $QAppNoti->getDestructionCost($params);

//    $params['status_finish'] = 6;
    $transferCost = $QAppNoti->getTransferCost($params);

//    $params['status_finish'] = 6;
    $params['additional_part_type'] = 4;
    $repairCost = $QAppNoti->getRepairCost($params);
    $additionalRepairCost = $QAppNoti->getAdditionalCost($params);

//    $params['status_finish'] = 5;
    $params['additional_part_type'] = 8;
    $airCost = $QAppNoti->getAirCost($params);
    $additionalAirCost = $QAppNoti->getAdditionalCost($params);

    $params['additional_part_type'] = 2;
    $orderAirCost = $QAppNoti->getOrderAirCost($params);
    $additionalOrderCost = $QAppNoti->getAdditionalCost($params);

    $posmCost = $QAppNoti->getPosmCost($params);
    $additionalPosmCost = $QAppNoti->getAdditionalPosmCost($params);
    

    $totalMixCost = $destructionCost + $transferCost + $repairCost['ia'] + $repairCost['ka'] + $repairCost['brandshop'] + $additionalRepairCost + $airCost + $additionalAirCost; // bao gồm chi phí thi công, sửa chữa, tiêu hủy, điều chuyển
    $totalCost =  $posmCost + $additionalPosmCost + $orderAirCost + $additionalOrderCost + $totalMixCost;

    $listArea = $QArea->getListArea($params);

    // hiển thị title chi phí
    if (! $areaId) {
        if (count($listArea) > 3) {
            $countArea = count($listArea);
        } else {
            foreach ($listArea as $area) {
                $listAreaName [] = $area['name'];
            }
            $stringAreaName = implode(', ', $listAreaName);
        }
    } else {
        $stringAreaName = $QArea->fetchRow(['id = ?' => $areaId])->name;
    }

    // Ngân sách theo năm
    unset($params['month']);
    unset($params['from_date']);
    unset($params['to_date']);
    $params ['season'] = 1;
    $params ['year'] = date('Y');

    $params['additional_part_type'] = 8;
    $airCostYear = $QAppNoti->getAirCost($params);
    $additionalAirCostSeason = $QAppNoti->getAdditionalCostSeason($params);

    $params['additional_part_type'] = 4;
    $repairCostYear = $QAppNoti->getRepairCost($params);
    $additionalRepairCostSeason = $QAppNoti->getAdditionalCostSeason($params);

    $destructionCostYear = $QAppNoti->getDestructionCost($params);

    $transferCostYear = $QAppNoti->getTransferCost($params);

    $orderCostYear = $QAppNoti->getOrderCostYear($params);

    $posmCostYear = $QAppNoti->getPosmCostYear($params);
    $additionalPosmCostSeason = $QAppNoti->getAdditionalPosmCostSeason($params);

    $totalCostYear =   $airCostYear + $additionalAirCostSeason + $posmCostYear + $additionalPosmCostSeason + $orderCostYear + $transferCostYear + $destructionCostYear + $repairCostYear['ia'] + $repairCostYear['ka'] + $repairCostYear['brandshop'] + $additionalRepairCostSeason;
    $totalBudget = $QBudget->getTotalBudget($params);

    $remainBudget = $totalBudget - $totalCostYear;

    $params['from_date'] = $fromDate;
    $params['to_date'] = $toDate;


    /// hiển thị hạng mục đầu tư
    $listCategoryCheckshop = $QCategory->getCategoryCheckshop($params);
    $statisticCategory = $QAppNoti->getStatisticCategory($params);

    // thống kê shop tạm đóng 
//    $store_temp_stop_ka = $QStoreTempStop->statisticShopKA($params);
//    $store_temp_stop_ind = $QStoreTempStop->statisticShopIND($params);

    //Lấy danh sách nhân sự
    $params['list_team'] = [TRADE_TEAM];
    $count_list_staff = $QKpiMonth->countListStaff($params);


    $this->view->store_temp_stop_ka = $store_temp_stop_ka;
    $this->view->store_temp_stop_ind = $store_temp_stop_ind;
    $this->view->repair_pending = count($repair);
    $this->view->destruction_pending = count($destruction);
    $this->view->remove_shop_pending = count($remove_shop);
    $this->view->transfer_pending = count($transfer);
    $this->view->air_pending = count($air);

    $this->view->repair_success = $repair_success;
    $this->view->destruction_success = $destruction_success;
    $this->view->remove_shop_success = $remove_shop_success;
    $this->view->transfer_success = $transfer_success;
    $this->view->air_success = $air_success;

    $this->view->total_pending_self = count($repair) + count($destruction) + count($transfer) + count($air) + count($remove_shop);
    $this->view->total_success = $repair_success['success'] + $destruction_success['success'] + $transfer_success['success'] + $air_success['success'] + $remove_shop_success['success'];
    $this->view->total_pending = $repair_success['pending'] + $destruction_success['pending'] + $transfer_success['pending'] + $air_success['pending'] + $remove_shop_success['pending'];

    $this->view->title = $this->storage['title'];
    $this->view->checkshop_info = $checkshop_info;

    $this->view->destructionCost = $destructionCost;
    $this->view->transferCost = $transferCost;
    $this->view->repairCost = $repairCost;
    $this->view->airCost = $airCost;
    $this->view->posmCost = $posmCost;
    $this->view->orderAirCost = $orderAirCost;
    $this->view->additionalOrderCost = $additionalOrderCost;
    $this->view->totalCost = $totalCost;
    $this->view->totalBudget = $totalBudget;
    $this->view->remainBudget = $remainBudget;
    $this->view->year = $year;
    $this->view->month = $month;
    $this->view->params = $params;
    $this->view->listArea = $listArea;
    $this->view->additionalAirCost = $additionalAirCost;
    $this->view->additionalPosmCost = $additionalPosmCost;

    $this->view->stringAreaName = $stringAreaName;
    $this->view->countArea = $countArea;
    $this->view->totalMixCost = $totalMixCost;

    $this->view->listCategoryCheckshop = $listCategoryCheckshop;
    $this->view->statisticCategory = $statisticCategory;
    $this->view->count_checkshop = $count_checkshop;

    $this->view->count_list_staff = $count_list_staff;




