<?php
$area_id = $this->getRequest()->getParam('area_id');

$params = [
    'list_area_id' => $area_id
];

$QStore = new Application_Model_Store();
$list_store = $QStore->getListStore($params);

$this->view->list_store = $list_store;
$this->_helper->layout()->disablelayout(true);
