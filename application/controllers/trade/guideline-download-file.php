<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$fileId = $this->getRequest()->getParam('file_id');

$QGuidelineFile = new Application_Model_GuidelineFile();
$file = $QGuidelineFile->fetchRow(['id = ?' => $fileId]);

$fileDownload = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . $file['url'];

if (file_exists($fileDownload)) {

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $file['name'] . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fileDownload));
    readfile($fileDownload);
    exit;
}