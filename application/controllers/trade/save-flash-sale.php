<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$store_id = $this->getRequest()->getParam('store_id');
$ids = $this->getRequest()->getParam('ids');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$QStoreFlashSale = new Application_Model_StoreFlashSale();

if (!$store_id || !$ids) {
//    echo  json_encode([
//       'status' => 1,
//       'message' => 'Chưa lựa chọn!'
//    ]);
//
//    return;
}

$where = $QStoreFlashSale->getAdapter()->quoteInto('store_id = ?', $store_id);
$QStoreFlashSale->update(['is_del' => 1], $where);

foreach($ids as $key=>$value){
    $QStoreFlashSale->insert([
        'store_id' => $store_id,
        'flash_sale_id' => $value,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
    ]);
}

echo json_encode([
    'status' => 0
]);
