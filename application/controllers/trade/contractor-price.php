<?php

    $page = $this->getRequest()->getParam('page',1);
    $name = $this->getRequest()->getParam('name');
    $contructor_name = $this->getRequest()->getParam('contructor_name');
    $code = $this->getRequest()->getParam('code');
    $has_contructor = $this->getRequest()->getParam('has_contructor');

    $QContractorPrice = new Application_Model_ContractorPrice();

    $params = [
        'campaign_id' => $campaign_id,
        'name'  => $name,
        'contructor_name'   => $contructor_name,
        'has_contructor'   => $has_contructor,
        'code'  => $code
    ];

    $limit = LIMITATION;
    $total = 0;
    $category = $QContractorPrice->fetchPagination($page, $limit, $total, $params);

    $this->view->offset = $limit * ($page - 1);
    $this->view->category = $category;
    $this->view->params = $params;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->url = HOST . 'trade/contractor-price/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');




