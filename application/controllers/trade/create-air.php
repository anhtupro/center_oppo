<?php
$quantity = $this->getRequest()->getParam('quantity');
$category_id   = $this->getRequest()->getParam('category');
$width = $this->getRequest()->getParam('width');
$wide = $this->getRequest()->getParam('wide');
$deep = $this->getRequest()->getParam('deep');
$total = $this->getRequest()->getParam('total'); //var_dump($total); exit;
$dvt = $this->getRequest()->getParam('dvt');
$dem=$this->getRequest()->getParam('dem');
$store_id   = $this->getRequest()->getParam('store');
$id = $this->getRequest()->getParam('id');
$cate_quotation = $this->getRequest()->getParam('baogia');
$unit = $this->getRequest()->getParam('unit');
$soluong = $this->getRequest()->getParam('soluong');
$dongia = $this->getRequest()->getParam('dongia');
$tongtien= $this->getRequest()->getParam('tongtien');
$submit	    = $this->getRequest()->getParam('submit');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QCampaign = new Application_Model_Campaign();
$QAppAir = new Application_Model_AppAir();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QAppAirFile = new Application_Model_AppAirFile();
$QAppQuotation = new Application_Model_AppQuotation();
$QAppQuotationCat = new Application_Model_AppQuotationCat();
$flashMessenger       = $this->_helper->flashMessenger;
$params = array(
	'id' => $id
);
$back_url = HOST.'trade/air'; 
/*TẠO ĐỀ XUẤT THI CÔNG*/
$db             = Zend_Registry::get('db');
$db->beginTransaction();



if(!empty($store_id) && !empty($category_id) &&!empty($_FILES['files_dx']['name']))
{
	try
	{
		$upload = new Zend_File_Transfer();
		
		$upload->addValidator('FilesSize',false,array('max' => '10MB'));
		
		if (!$upload->isValid()) {

    		$db->rollBack();
		    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Dung lượng file vượt mức cho phép !");
	    	$this->redirect(HOST.'trade/create-air');
		}
		
		$last_number = $QAppAir->getLastNumber() ? $QAppAir->getLastNumber() : 0;
		$sn = 'DXTC'.str_pad( ($last_number['id'] + 1), 5, "0", STR_PAD_LEFT);
		//$file_id=str_pad( ($last_number['id'] + 1), 5, "0", STR_PAD_LEFT);
		$date=date('Y-m-d H:i:s');
		$appair = [
			'sn'  		 => $sn,
			'created_by'=>$userStorage->id,
			'store_id'=>$store_id,
			//'file_id'=>$file_id,
		    'created_at'=>$date,
		    'status'=>1
				];
		$appair_id = $QAppAir->insert($appair);

		// /Lưu file đề xuất
				$totals = count($_FILES['files_dx']['name']);
					//Upload hình file
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
								DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
								DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $appair_id;

					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);

					$tmpFilePath = $_FILES['files_dx']['tmp_name'];

					if ($tmpFilePath != ""){

						$old_name 	= $_FILES['files_dx']['name'];
						$tExplode 	= explode('.', $old_name);
						$extension  = end($tExplode);
						$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

						$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;

						if(move_uploaded_file($tmpFilePath, $newFilePath)) {
							$url         = 'files' .
								DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $appair_id. DIRECTORY_SEPARATOR .$new_name;
						}
						else{
							$url = NULL;
						}
					}
					else{
						$url = NULL;
					}

					$details = [
						'air_id' => $appair_id,
						'url'  => $url,
						'type'	   => 2,
						'file_name'=>$new_name,
						'contructor_id' =>0
					];
					//var_dump($details); exit;
					$QAppAirFile->insert($details);
				
				 //luu vao air_detail
		foreach ($category_id as $key=> $value) {
				if(!empty($deep[$key]))
				{
					$d=$deep[$key];
				}else{
					$d=0;
				}
			$insert = $QAppAirDetail->insert(array(
			    'air_id'=>$appair_id,
			    'cate_id'=>$value,
			    'width'  => $width[$key],
			    'wide'=>$wide[$key],
			    'deep'=>$d,
			    'total'=>$total[$key],
			    'dvt'=>$dvt[$key],
			    'quantity'	=> $quantity[$key]
			));
		}
		
		$db->commit();
		$flashMessenger->setNamespace('success')->addMessage('Tạo phiếu thành công!');
    	$this->redirect($back_url);
	}catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Đề xuất thất bại".$e->getMessage() );
    	$this->redirect($back_url);
	}
}
/*END TẠO ĐỀ XUẤT THI CÔNG*/


	$this->view->params = $params;
	// -------------RETURN CATEGORY ---------------
	$result = $QAppAir->getCategory();
	//var_dump($result); exit;
	$this->view->category = $result;
	// -------------RETURN STORE ---------------
	$paramsData['staff_id'] = $userStorage->id;
	$resultCamp = $QCampaign->getListStore($paramsData);
	$this->view->store = $resultCamp;
	/*Danh sách quotation*/

	$quotation_cat=$QAppQuotationCat->fetchAll();
	$this->view->quotation_cat=$quotation_cat;

	/*-------------------*/
	// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
	?>
	


