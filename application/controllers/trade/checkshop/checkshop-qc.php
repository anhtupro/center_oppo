<?php
    
    $checkshop_id = $this->getRequest()->getParam('checkshop_id');
    $checkshop_id_is_lock = $this->getRequest()->getParam('checkshop_id_is_lock');
    $lock = $this->getRequest()->getParam('lock');
    $leader_lock = $this->getRequest()->getParam('leader_lock');
    $remove_lock = $this->getRequest()->getParam('remove_lock');
    $remove_leader_lock = $this->getRequest()->getParam('remove_leader_lock');
    $qc_id = $this->getRequest()->getParam('qc_id');
    $type = $this->getRequest()->getParam('type');
    $note  = $this->getRequest()->getParam('note');
    
    $params = [
        'qc_id' => $qc_id,
        'note'  => $note
    ];
    
    $staff_id = $this->storage['staff_id'];
    $title = $this->storage['title'];
    $staff_qc = unserialize(QC_TITLE);
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $QCheckshop = new Application_Model_AppCheckshop();
    $QCheckshopDetail = new Application_Model_AppCheckshopDetail();
    $QCheckshopQc = new Application_Model_AppCheckshopQc();
    $QCheckshopMapQc = new Application_Model_AppCheckshopMapQc();
    $QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();

    $flashMessenger       = $this->_helper->flashMessenger;
    
    $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
    
    $back_url = '/trade/view-checkshop?id='.$checkshop_id;
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        if(!empty($lock)){
//            echo 'Chức năng này đã tạm dừng hoạt động !';
//            exit();
            $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
            $is_local_lock = $QCheckshop->fetchRow($where);
            
            if($is_local_lock){
                $flashMessenger->setNamespace('error')->addMessage( "Shop đã được xác nhận số lượng!" );
                $this->redirect($back_url);
            }
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_leader_lock = ?', 1);
            $where[] = $QCheckshop->getAdapter()->quoteInto('id <> ?', $checkshop_id);
            $is_leader_lock = $QCheckshop->fetchRow($where);
            
//            if($is_leader_lock){
//                $flashMessenger->setNamespace('error')->addMessage( "Không được xác nhận ngày khác với ngày Sale Leader xác nhận trước đó!" );
//                $this->redirect($back_url);
//            }
            
            $where = $QCheckshop->getAdapter()->quoteInto('id = ?', $checkshop_id);
            $QCheckshop->update(['is_lock' => 1, 'is_lock_by' => $userStorage->id], $where);

            // thông tin chi tiết hạng mục
            $checkshopDetail = $QCheckshopDetail->fetchAll(['checkshop_id = ?' => $checkshop_id]);
            foreach ($checkshopDetail as $detail) {
                $arrayImei = [];
                if ($detail['imei_sn']) {
                    $stringImei = str_replace(' ', '', $detail['imei_sn']);
                    $arrayImei = explode(',', $stringImei);
                }
                for ($i=0; $i<$detail['quantity']; $i++) {
                    $QAppCheckshopDetailChild->insert([
                        'checkshop_id' => $detail['checkshop_id'],
                        'category_id' => $detail['category_id'],
                        'quantity' => 1,
                        'imei' => $arrayImei[$i] ? TRIM(TRIM($arrayImei[$i], ',')) : Null
                    ]);
                }

            }
        }
        
        if(!empty($leader_lock)){
            echo 'Chức năng này đã tạm dừng hoạt động !';
            exit();
            $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_leader_lock = ?', 1);
            $is_leader_lock = $QCheckshop->fetchRow($where);
            
            if($is_leader_lock){
                $flashMessenger->setNamespace('error')->addMessage( "Shop đã được xác nhận số lượng!" );
                $this->redirect($back_url);
            }
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
            $where[] = $QCheckshop->getAdapter()->quoteInto('id <> ?', $checkshop_id);
            $is_local_lock = $QCheckshop->fetchRow($where);
            
            if($is_local_lock){
                $flashMessenger->setNamespace('error')->addMessage( "Không được xác nhận ngày khác với ngày TMK Local xác nhận trước đó!" );
                $this->redirect($back_url);
            }
            
            $where = $QCheckshop->getAdapter()->quoteInto('id = ?', $checkshop_id);
            $QCheckshop->update(['is_leader_lock' => 1, 'is_leader_lock_by' => $userStorage->id], $where);

        }
        
        if(!empty($remove_lock)){
            echo 'Chức năng này đã tạm dừng!';
            exit();

            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
            $checkshop_is_local_lock = $QCheckshop->fetchRow($where);

            // check if shop has category is transfering or repairing or destructing
            $alert_shop_is_processing = $QAppCheckshopDetailChild->checkCategoryIsProcessing($checkshop_is_local_lock['id']);


            if ($alert_shop_is_processing) {
                $flashMessenger->setNamespace('error')->addMessage($alert_shop_is_processing . '. Vui lòng hoàn thành hoặc hủy các đề xuất trước khi hủy xác nhận số lượng!');
                $this->redirect($back_url);
            }

            // delete current category in shop
            $QAppCheckshopDetailChild->delete(['checkshop_id = ?' => $checkshop_is_local_lock['id']]);

            // remove lock
            $QCheckshop->update(['is_lock' => NULL], $where);

        }
        
        if(!empty($remove_leader_lock)){
            echo 'Chức năng này đã tạm dừng hoạt động !';
            exit();
            $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
            
            $where = [];
            
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_leader_lock = ?', 1);
            
            $QCheckshop->update(['is_leader_lock' => NULL], $where);

        }
        
        if(!empty($qc_id)){
            
            //Nếu Trade-local chưa đánh giá thì ko cho TMK Leader đánh giá
            if($type == 2){
                $check_local = $QCheckshopMapQc->checkQc($checkshop['store_id'], $checkshop['updated_at'], 1);
                if(!$check_local){
                    $flashMessenger->setNamespace('error')->addMessage( "Trade Local chưa đánh giá!" );
                    $this->redirect($back_url);
                }
            }
            
         /*   if($type == 3){
                $check_local = $QCheckshopMapQc->checkQc($checkshop['store_id'], $checkshop['updated_at'], 2);
                if(!$check_local){
                    $flashMessenger->setNamespace('error')->addMessage( "Trade Leader chưa đánh giá!" );
                    $this->redirect($back_url);
                }
            }*/
            //END ...

            $data = [
                'checkshop_id'  => $checkshop_id,
                'qc_id'    => $qc_id,
                'note'  => $note,
                'type'  => $type,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $check_qc = $QCheckshopMapQc->checkQc($checkshop['store_id'], $checkshop['updated_at'], $type);

            if($check_qc){
                $flashMessenger->setNamespace('error')->addMessage( "Shop đã đánh giá." );
                $this->redirect($back_url);
            }
            else{
                $QCheckshopMapQc->insert($data);
            }
                
   
        }
        
        
        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage( "Hoàn thành!");
        $this->redirect($back_url);
        
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
        $this->redirect($back_url);
    }
    
    
    
    
	
