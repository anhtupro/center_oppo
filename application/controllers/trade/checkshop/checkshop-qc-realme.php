<?php
    
    $checkshop_id = $this->getRequest()->getParam('checkshop_id_realme');
    $qc_id = $this->getRequest()->getParam('qc_id_realme');
    $type = $this->getRequest()->getParam('type_realme');
    $note  = $this->getRequest()->getParam('note_realme');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $QCheckshop = new Application_Model_AppCheckshop();
    $QCheckshopMapQcRealme = new Application_Model_AppCheckshopMapQcRealme();

    $flashMessenger       = $this->_helper->flashMessenger;
    
    $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
    
    $back_url = '/trade/view-checkshop?id='.$checkshop_id;
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        if(!empty($qc_id)){
            
            //Nếu Trade-local chưa đánh giá thì ko cho TMK Leader đánh giá
            if($type == 2){
                $check_local = $QCheckshopMapQcRealme->checkQc($checkshop['store_id'], $checkshop['updated_at'], 1);
                if(!$check_local){
                    $flashMessenger->setNamespace('error')->addMessage( "Trade Local chưa đánh giá!" );
                    $this->redirect($back_url);
                }
            }
            $data = [
                'checkshop_id'  => $checkshop_id,
                'qc_id'    => $qc_id,
                'note'  => $note,
                'type'  => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'create_by' => $userStorage->id
            ];

            $check_qc = $QCheckshopMapQcRealme->checkQc($checkshop['store_id'], $checkshop['updated_at'], $type);

            if($check_qc){
                $flashMessenger->setNamespace('error')->addMessage( "Shop đã đánh giá." );
                $this->redirect($back_url);
            }
            else{
                $QCheckshopMapQcRealme->insert($data);
            }
                
   
        }
        
        
        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage( "Hoàn thành!");
        $this->redirect($back_url);
        
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
        $this->redirect($back_url);
    }
    
    
    
    
	
