<?php
    
    $checkshop_id = $this->getRequest()->getParam('checkshop_id');
    $lock = $this->getRequest()->getParam('lock');
    $leader_lock = $this->getRequest()->getParam('leader_lock');
    $remove_lock = $this->getRequest()->getParam('remove_lock');
    $remove_leader_lock = $this->getRequest()->getParam('remove_leader_lock');
    $qc_id = $this->getRequest()->getParam('qc_id');
    $type = $this->getRequest()->getParam('type');
    $note  = $this->getRequest()->getParam('note');
    
    $params = [
        'qc_id' => $qc_id,
        'note'  => $note
    ];
    
    $staff_id = $this->storage['staff_id'];
    $title = $this->storage['title'];
    $staff_qc = unserialize(QC_TITLE);
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $QCheckshop = new Application_Model_AppCheckshop(); 
    $QCheckshopQc = new Application_Model_AppCheckshopQc();
    $QCheckshopMapQc = new Application_Model_AppCheckshopMapQc();
    $flashMessenger       = $this->_helper->flashMessenger;
    
    $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
    
    $back_url = '/trade/view-checkshop?id='.$checkshop_id;
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        if(!empty($lock)){
            
            $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
            $is_local_lock = $QCheckshop->fetchRow($where);
            
            if($is_local_lock){
                $flashMessenger->setNamespace('error')->addMessage( "Shop đã được xác nhận số lượng!" );
                $this->redirect($back_url);
            }
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_leader_lock = ?', 1);
            $where[] = $QCheckshop->getAdapter()->quoteInto('id <> ?', $checkshop_id);
            $is_leader_lock = $QCheckshop->fetchRow($where);
            
            if($is_leader_lock){
                $flashMessenger->setNamespace('error')->addMessage( "Không được xác nhận ngày khác với ngày Sale Leader xác nhận trước đó!" );
                $this->redirect($back_url);
            }
            
            $where = $QCheckshop->getAdapter()->quoteInto('id = ?', $checkshop_id);
            $QCheckshop->update(['is_lock' => 1, 'is_lock_by' => $userStorage->id], $where);

        }
        
        if(!empty($leader_lock)){

            $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_leader_lock = ?', 1);
            $is_leader_lock = $QCheckshop->fetchRow($where);
            
            if($is_leader_lock){
                $flashMessenger->setNamespace('error')->addMessage( "Shop đã được xác nhận số lượng!" );
                $this->redirect($back_url);
            }
            
            $where = [];
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
            $where[] = $QCheckshop->getAdapter()->quoteInto('id <> ?', $checkshop_id);
            $is_local_lock = $QCheckshop->fetchRow($where);
            
            if($is_local_lock){
                $flashMessenger->setNamespace('error')->addMessage( "Không được xác nhận ngày khác với ngày TMK Local xác nhận trước đó!" );
                $this->redirect($back_url);
            }
            
            $where = $QCheckshop->getAdapter()->quoteInto('id = ?', $checkshop_id);
            $QCheckshop->update(['is_leader_lock' => 1, 'is_leader_lock_by' => $userStorage->id], $where);

        }
        
        if(!empty($remove_lock)){

            $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
            
            $where = [];
            
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
            
            $QCheckshop->update(['is_lock' => NULL], $where);

        }
        
        if(!empty($remove_leader_lock)){

            $checkshop = $QCheckshop->fetchRow($QCheckshop->getAdapter()->quoteInto('id = ?',$checkshop_id));
            
            $where = [];
            
            $where[] = $QCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
            $where[] = $QCheckshop->getAdapter()->quoteInto('is_leader_lock = ?', 1);
            
            $QCheckshop->update(['is_leader_lock' => NULL], $where);

        }
        
        if(!empty($qc_id)){
            
            //Nếu Trade-local chưa đánh giá thì ko cho TMK Leader đánh giá
            if($type == 2){
                $check_local = $QCheckshopMapQc->checkQc($checkshop['store_id'], $checkshop['updated_at'], 1);
                if(!$check_local){
                    $flashMessenger->setNamespace('error')->addMessage( "Trade Local chưa đánh giá!" );
                    $this->redirect($back_url);
                }
            }
            
            if($type == 3){
                $check_local = $QCheckshopMapQc->checkQc($checkshop['store_id'], $checkshop['updated_at'], 2);
                if(!$check_local){
                    $flashMessenger->setNamespace('error')->addMessage( "Trade Leader chưa đánh giá!" );
                    $this->redirect($back_url);
                }
            }
            //END ...

            $data = [
                'checkshop_id'  => $checkshop_id,
                'qc_id'    => $qc_id,
                'note'  => $note,
                'type'  => $type,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $check_qc = $QCheckshopMapQc->checkQc($checkshop['store_id'], $checkshop['updated_at'], $type);

            if($check_qc){
                $flashMessenger->setNamespace('error')->addMessage( "Shop đã đánh giá." );
                $this->redirect($back_url);
            }
            else{
                $QCheckshopMapQc->insert($data);
            }
                
   
        }
        
        
        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage( "Hoàn thành!");
        $this->redirect($back_url);
        
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
        $this->redirect($back_url);
    }
    
    
    
    
	
