<?php
$recheckShopId = $this->getRequest()->getParam('recheck_shop_id');

$QRecheckShop = new Application_Model_RecheckShop();
$QRecheckShopDetail = new Application_Model_RecheckShopDetail();
$QCategory = new Application_Model_Category();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QRecheckShopFile = new Application_Model_RecheckShopFile();
$QRecheckShopErrorImage = new Application_Model_RecheckShopErrorImage();

$recheckShop = $QRecheckShop->getRecheckShop($recheckShopId);
$appCheckShopId = $recheckShop['app_check_shop_id'];
$storeId = $recheckShop['store_id'];

$listRecheckShopDetail = $QRecheckShopDetail->getRecheckShopDetail($recheckShopId);
$sellout = $QAppCheckshop->getSellout($storeId);
// image recheck shop
$listImageRecheckShop = $QRecheckShopFile->get($recheckShopId);
$listImageRecheckshopDifferent = $QRecheckShopFile->getImgDifferent($recheckShopId);

// image check shop
if ($appCheckShopId) {
    $listImageCheckShop = $QAppCheckshop->getImg($appCheckShopId);
    $listImageCheckShopDifferent = $QAppCheckshop->getImgDifferent($appCheckShopId);
}

$listErrorImage = $QRecheckShopErrorImage->getError($recheckShopId);


$this->view->recheckShop = $recheckShop;
$this->view->listRecheckShopDetail = $listRecheckShopDetail;
$this->view->sellout = $sellout;

$this->view->listImageCheckShop = $listImageCheckShop;
$this->view->listImageCheckShopDifferent = $listImageCheckShopDifferent;

$this->view->listImageRecheckShop = $listImageRecheckShop;
$this->view->listImageRecheckshopDifferent = $listImageRecheckshopDifferent;
$this->view->listErrorImage = $listErrorImage;