<?php 
	
	$campaign_id    = $this->getRequest()->getParam('campaign_id');
	$category_id    = $this->getRequest()->getParam('category_id');
	$store_id    	= $this->getRequest()->getParam('store_id');
	$list_imei    	= $this->getRequest()->getParam('list_imei');

	$list_imei = json_decode($list_imei, true);

	$QAppContractorIn 			= new Application_Model_AppContractorIn();
	$QAppContractorOut 			= new Application_Model_AppContractorOut();
	$QAppOrderDetails           = new Application_Model_AppOrderDetails();
	$QAppOrder                  = new Application_Model_AppOrder();
	$QAppStoreIn                = new Application_Model_AppStoreIn();

	$flashMessenger = $this->_helper->flashMessenger;
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id 	 = $userStorage->id;
	$title 		 =  $userStorage->title;
    $group 		 =  $userStorage->group_id;

    $back_url = HOST.'trade/scan-shop?category_id='.$category_id.'&campaign_id='.$campaign_id;

    $params = [
		'campaign_id' => $campaign_id,
		'category_id' => $category_id,
		'staff_id'    => $staff_id
	];

	if($title == SALES_TITLE){
		$where[] = $QAppOrder->getAdapter()->quoteInto('created_by = ?', $staff_id);
		$where[] = $QAppOrder->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
		$app_order_check = $QAppOrder->fetchRow($where);
		$params['order_id'] = $app_order_check['id'];
	}

	$store = $QAppOrder->getStoreInfo($store_id);

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		foreach ($list_imei as $key => $value) {

			$imei_sn = RTRIM(LTRIM($value));

			if($imei_sn != ''){

				$check_imei_store = $QAppStoreIn->checkImeiStore($imei_sn);
				
				if(!$check_imei_store){
					$flashMessenger->setNamespace('error')->addMessage('Imei này chưa được Nhập kho KV!');
    				$this->redirect($back_url);
				}

				if($check_imei_store['area_id'] != $store['area_id']){
					$flashMessenger->setNamespace('error')->addMessage('Sai khu vực.'.$check_imei_store['area_id'].'/'.$store['area_id']);
    				$this->redirect($back_url);
				}

				if(!empty($check_imei_store['imei_in_store'])){
					$flashMessenger->setNamespace('error')->addMessage('Imei đã được Scan vào Shop.');
    				$this->redirect($back_url);
				}

				$data = [
					'store_id' 		=> $store_id,
					'campaign_id'	=> $check_imei_store['campaign_id'],
					'category_id'	=> $check_imei_store['category_id'],
					'imei_sn'		=> $check_imei_store['imei_sn'],
					'created_at'	=> date('Y-m-d H:i:s'),
					'created_by'	=> $this->storage['staff_id']
				];

				$QAppStoreIn->insert($data);

				//Cập nhật lại status app_order_details
				$db = Zend_Registry::get('db');
                $sql = "UPDATE `".DATABASE_TRADE."`.`app_order_details` p
						LEFT JOIN `".DATABASE_TRADE."`.`app_order` o ON o.id = p.order_id
						SET p.`status` = 10
						WHERE p.`status` = 9 AND o.campaign_id = :campaign_id AND p.category_id = :category_id AND p.store_id = :store_id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('campaign_id', $check_imei_store['campaign_id'], PDO::PARAM_INT);
                $stmt->bindParam('category_id', $check_imei_store['category_id'], PDO::PARAM_INT);
                $stmt->bindParam('store_id', $check_imei_store['store_id'], PDO::PARAM_INT);
                $stmt->execute();

                $stmt->closeCursor();
				//END Cập nhật lại status app_order_details

				
			}

			//Kiểm tra shop đã scan đủ chưa
			$order_details  = $QAppOrderDetails->getDataDetails($params);

			$store_order = [];
			foreach($order_details as $key=>$value){
				$store_order[$value['store_id']] = $value['quantity'] - $value['total_scan'];
			}

			if($store_order[$store_id] < 0){
				$flashMessenger->setNamespace('error')->addMessage('Vượt quá số lượng đặt hàng!');
		    	$this->redirect($back_url);
			}
			//END - Kiểm tra shop đã scan đủ chua 
				
		}

		$db->commit();
		$flashMessenger->setNamespace('success')->addMessage('Scan Thành công!');
    	$this->redirect($back_url);
    	
	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}




