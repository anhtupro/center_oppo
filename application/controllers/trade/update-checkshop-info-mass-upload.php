<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$type = $this->getRequest()->getParam('type');

include 'PHPExcel/IOFactory.php';
// config for excel template
define('START_ROW', 2);
define('STORE_ID', 0);
define('WIDTH', 3);
define('HEIGHT', 4);


$QStore = new Application_Model_Store();
//

$save_folder = 'massupload_shop_facade_size';
$requirement = array(
    'Size' => array('min' => 5, 'max' => 15000000),
    'Count' => array('min' => 1, 'max' => 1),
    'Extension' => array('xls', 'xlsx')
);
// upload and save file

try {
    $file = My_File::get($save_folder, $requirement);

    if (!$file) {
        echo json_encode([
            'status' => 1,
            'message' => 'Upload failed'
        ]);
        return;
    }
    $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
        . DIRECTORY_SEPARATOR . $file['folder'];
    $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

//read file
//  Choose file to read
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

// read sheet
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
try {
    $insertQuery = "INSERT INTO trade_marketing.store_facade_size(store_id, width, height, type) VALUES ";
    $deleteQuery = "DELETE FROM trade_marketing.store_facade_size WHERE type = " . $type . " AND store_id IN";
    $updateQuery = "UPDATE trade_marketing.app_checkshop SET updated_facade_size = 1 WHERE is_lock = 1 AND store_id IN";

    for ($row = START_ROW; $row <= $highestRow; ++$row) {
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        $rowData = $rowData[0];

        $storeId = TRIM($rowData[STORE_ID]);
        $width = $rowData[WIDTH] ? $rowData[WIDTH] : 'NULL';
        $height = $rowData[HEIGHT] ? $rowData[HEIGHT] : 'NULL';

        if ($storeId) {
            $listStoreId [] = $storeId;
            $value = '(' . $storeId . ',' . $width . ',' . $height . ',' . $type . '),';
            $insertQuery .= $value;
        }

    }

    if (! $listStoreId) {
        echo json_encode([
            'status' => 1,
            'message' => 'Định dạng file không đúng. Vui lòng điền thông tin dựa trên file mẫu !'
        ]);
        return;
    }
    
    // check if store_id is valid
    $params ['list_area_id'] = $this->storage['area_id'];
    $listOwnStore = $QStore->getListStore($params);

    foreach ($listOwnStore as $store) {
        $listOwnStoreId [] = $store['id'];
    }

    foreach ($listStoreId as $storeId) {
        if (! in_array($storeId, $listOwnStoreId) ) {
            $listInvalidStoreId [] = $storeId;
        }
    }

    if ($listInvalidStoreId) {
        $listInvalidStoreId = implode(', ', $listInvalidStoreId);
        echo json_encode([
            'status' => 1,
            'message' => 'Shop có ID: ' . $listInvalidStoreId . ' không đúng hoặc không thuộc khu vực bạn quản lý. Vui lòng kiểm tra và thử lại !'
        ]);
        return;
    }

    // execute query
    $listStoreId = '(' . implode(',', $listStoreId) . ')';

    $deleteQuery .= $listStoreId;
    $updateQuery .= $listStoreId;
    $insertQuery = TRIM($insertQuery, ',');


    if($insertQuery){
        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        $stmt = $db->prepare($deleteQuery);
        $stmt->execute();

        $stmt1 = $db->prepare($insertQuery);
        $stmt1->execute();

        $stmt2 = $db->prepare($updateQuery);
        $stmt2->execute();

        $stmt->closeCursor();

        $db->commit();

        echo json_encode([
            'status' => 0
        ]);
        return;
    }

    echo json_encode([
        'status' => 1,
        'message' => 'File trống'
    ]);
    return;


} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
}

