<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore = new Application_Model_Store();
$QRemoveShop = new Application_Model_RemoveShop();
$QRemoveShopReason = new Application_Model_RemoveShopReason();
$QRemoveShopConfirm = new Application_Model_RemoveShopConfirm();
$QLogOnoffStore = new Application_Model_LogOnoffStore();
$QStoreStaffLog = new Application_Model_StoreStaffLog();
$QStoreLeaderLog = new Application_Model_StoreLeaderLog();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();
$QStoreMapping = new Application_Model_StoreMapping();

$id = $this->getRequest()->getParam('id');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');
$listRemoveShopAproveAll = json_decode($this->getRequest()->getParam('list_remove_shop_approve_all'), true);


$where_remove = $QRemoveShop->getAdapter()->quoteInto('id = ?', $id);
$remove_shop = $QRemoveShop->fetchRow($where_remove);

// Duyệt tất cả đề xuất cho sale asisstant
if ($listRemoveShopAproveAll && $listRemoveShopAproveAll !== 'null') {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();


    foreach ($listRemoveShopAproveAll as $remove_shop) {
        $where = [];
        $where [] = $QStoreStaffLog->getAdapter()->quoteInto('store_id = ?', $remove_shop['store_id']);
        $where [] = $QStoreStaffLog->getAdapter()->quoteInto('released_at IS NULL');
        $store_staff_log = $QStoreStaffLog->fetchRow($where);

        if ($store_staff_log) {
            echo json_encode([
                'status' => 1,
                'message' => 'Vui lòng kiểm tra phần gán gỡ shop, gỡ SALE, PGs! tại store ' . $remove_shop['store_id']
            ]);
            return;
        }

        $QRemoveShop->update(['status' => 5], $QRemoveShop->getAdapter()->quoteInto('id = ?', $remove_shop['id']));

        $where_store = $QStore->getAdapter()->quoteInto('id = ?', $remove_shop['store_id']);
        $QStore->update(['del' => 1], $where_store);
  

        $log_onoff_store = [
            'store_id' => $remove_shop['store_id'],
            'type' => 1,
            'from_date' => date('Y-m-d H:i:s')
        ];
        $QLogOnoffStore->insert($log_onoff_store);

        //remove shop confirm
        $confirm = [
            'remove_shop_id' => $remove_shop['id'],
            'remove_shop_status' => $remove_shop['status'],
            'confirm_by' => $this->storage['staff_id'],
            'created_at' => date('Y-m-d H:i:s')
        ];

        $QRemoveShopConfirm->insert($confirm);
    } // endforeach

    $db->commit();

    echo json_encode([
        'status' => 0
    ]);
    return;
} else {
    if (! $id) {
        echo json_encode([
            'status' => 1,
            'message' => 'Vui lòng chọn khu vực'
        ]);
        return;
    }
}// endif

if (!empty($id)) {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        //
//        $where = [];
//        $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('store_id = ?', $remove_shop['store_id']);
//        $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at IS NULL');
//        $store_leader_log = $QStoreLeaderLog->fetchRow($where);
//        if($store_leader_log){
//            $flashMessenger->setNamespace('error')->addMessage('Vui lòng kiểm tra phần gán gỡ shop, gỡ Sale Leader!');
//            $this->_redirect(HOST . 'trade/remove-shop-detail?id='.$id);
//        }
        //

        //Reject
        if ($reject) {
            $QRemoveShop->update(['reject' => 1, 'reject_note' => $reject_note, 'rejected_at' => date('Y-m-d H:i:s'), 'rejected_by' => $this->storage['staff_id']], $where_remove);

            $db->commit();

            $flashMessenger->setNamespace('success')->addMessage('Success!');
            $this->_redirect(HOST . 'trade/remove-shop-detail?id=' . $id);
        }
        //END Reject

        if ($remove_shop['status'] == 1) {
            $QRemoveShop->update(['status' => 2], $where_remove);

            $status_notify = 2;

        }


        if ($remove_shop['status'] == 2) {
            $QRemoveShop->update(['status' => 3], $where_remove);
            $status_notify = 3;

        }

        if ($remove_shop['status'] == 3) {

            // check pg sale
            $select_store_staff_log = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)"
                ])
                ->joinLeft(['st' => 'staff'], 'l.staff_id = st.id', [])
                ->where('l.store_id = ?', $remove_shop['store_id'])
                ->where('l.released_at IS NULL');

            $result_store_staff_log = $db->fetchCol($select_store_staff_log);


            if ($result_store_staff_log) {
                $string_store_staff_log = implode(', ', $result_store_staff_log);

                $flashMessenger->setNamespace('error')->addMessage('Vui lòng kiểm tra phần gán gỡ shop, gỡ SALE, PGs: ' . $string_store_staff_log);
                $this->_redirect(HOST . 'trade/remove-shop-detail?id=' . $id);
            }
            // end check pg sale

            $QRemoveShop->update(['status' => 4], $where_remove);

            $status_notify = 4;

        }

        if ($remove_shop['status'] == 4) {


            // check pg sale
            $select_store_staff_log = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)"
                ])
                ->joinLeft(['st' => 'staff'], 'l.staff_id = st.id', [])
                ->where('l.store_id = ?', $remove_shop['store_id'])
                ->where('l.released_at IS NULL');

            $result_store_staff_log = $db->fetchCol($select_store_staff_log);


            if ($result_store_staff_log) {
                $string_store_staff_log = implode(', ', $result_store_staff_log);

                $flashMessenger->setNamespace('error')->addMessage('Vui lòng kiểm tra phần gán gỡ shop, gỡ SALE, PGs: ' . $string_store_staff_log);
                $this->_redirect(HOST . 'trade/remove-shop-detail?id=' . $id);
            }
            // end check pg sale


            $QRemoveShop->update(['status' => 5], $where_remove);

            $where_store = $QStore->getAdapter()->quoteInto('id = ?', $remove_shop['store_id']);
            $QStore->update(['del' => 1], $where_store);

//            $store_id_realme = $QStoreMapping->fetchRow(['id_oppo = ?' => $remove_shop['store_id']])['id_rm'];
//            if ($store_id_realme) {
//                $QStoreRealme->update([
//                    'del' => 1
//                ], ['id = ?' => $store_id_realme]);
//            }

            $log_onoff_store = [
                'store_id' => $remove_shop['store_id'],
                'type' => 1,
                'from_date' => date('Y-m-d H:i:s')
            ];
            $QLogOnoffStore->insert($log_onoff_store);


            $status_notify = 5;

        }

        //REPAIR CONFIRM
        $rp_confirm = [
            'remove_shop_id' => $id,
            'remove_shop_status' => $remove_shop['status'],
            'confirm_by' => $this->storage['staff_id'],
            'created_at' => date('Y-m-d H:i:s')
        ];
        $QRemoveShopConfirm->insert($rp_confirm);
        //END REPAIR CONFIRM


        // notify
        $area_id = $QArea->getAreaByStore($remove_shop['store_id']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(11, $status_notify, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất ĐÓNG SHOP đang chờ bạn duyệt",
                'link' => "/trade/remove-shop-detail?id=" . $id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/remove-shop-detail?id=' . $id);
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
        $this->_redirect(HOST . 'trade/remove-shop-detail?id=' . $id);
    }
} else {
    $flashMessenger->setNamespace('error')->addMessage('Thiếu ID truyền vào.!');
    $this->_redirect(HOST . 'trade/remove-shop-detail?id=' . $id);
}

$this->view->params = $params;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
