<?php
	$QCategory = new Application_Model_Category();
	$QCampaign = new Application_Model_Campaign();
	$QReason = new Application_Model_AppReason ();
	$QArea = new Application_Model_Area();
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	//lấy danh mục
	$result = $QCategory->GetAll();
	$this->view->category = $result;
//	print_r($result->toArray());
	///lấy ds lí do
	$resultReason = $QReason->GetAll();
	$this->view->reason = $resultReason;
	//lấy store
	$paramsData['staff_id'] = $userStorage->id;
	$resultCamp = $QCampaign->getListStore($paramsData);
	$this->view->store = $resultCamp;
	//lấy area
	$resultArea =$QArea->GetAll();
	$this->view->area = $resultArea;

?>