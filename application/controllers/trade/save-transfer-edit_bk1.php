<?php

$transfer_id = $this->getRequest()->getParam('transfer_id');
$transfer_details_id = $this->getRequest()->getParam('transfer_details_id');
$contructors_id = $this->getRequest()->getParam('contructors_id');
$price = $this->getRequest()->getParam('price');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$category_id = $this->getRequest()->getParam('category_id');
$quantity = $this->getRequest()->getParam('quantity');
$area_from = $this->getRequest()->getParam('area_from');
$area_to = $this->getRequest()->getParam('area_to');
$warehouse_to = $this->getRequest()->getParam('warehouse_to');
$warehouse_from = $this->getRequest()->getParam('warehouse_from');

$transfer_date = $this->getRequest()->getParam('transfer_date');
$image = $this->getRequest()->getParam('image');
$manager_approve = $this->getRequest()->getParam('manager_approve');
$transfer_quotation_title = $this->getRequest()->getParam('transfer_quotation_title');
$transfer_quotation_quantity = $this->getRequest()->getParam('transfer_quotation_quantity');
$transfer_quotation_total_price = $this->getRequest()->getParam('transfer_quotation_total_price');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QTransfer = new Application_Model_Transfer();
$QTransferDetails = new Application_Model_TransferDetails();
$QTransferFile = new Application_Model_TransferFile();
$QTransferConfirm = new Application_Model_TransferConfirm();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAirInventoryArea = new Application_Model_AirInventoryArea();
$QAirInventoryAreaImei = new Application_Model_AirInventoryAreaImei();
$QTransferQuotation = new Application_Model_TransferQuotation();
$QInventoryOrderIn = new Application_Model_InventoryOrderIn();
$QInventoryOrderOut = new Application_Model_InventoryOrderOut();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();

$where_transfer = $QTransfer->getAdapter()->quoteInto('id = ?', $transfer_id);
$transfer = $QTransfer->fetchRow($where_transfer);

$where = $QTransferDetails->getAdapter()->quoteInto('transfer_id = ?', $transfer_id);
$transfer_details = $QTransferDetails->fetchAll($where);

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    // Hủy đề xuất
    if ($remove) {
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QTransfer->update($remove_date, $where_transfer);

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
    }

    //Reject
    if ($reject) {
        if ($transfer['status'] < 4) {
            $transfer_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 1
            ];
        }

        if ($transfer['status'] > 4) {
            $transfer_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 4
            ];
        }

        if ($transfer['status'] == 4) {
            $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
        }


        $QTransfer->update($transfer_data, $where_transfer);
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);


    }
    //END Reject


    //1: Chờ TMK Local xác nhận
    if ($transfer['status'] == 1) {

        foreach ($transfer_details_id as $key => $value) {
            $where = $QTransferDetails->getAdapter()->quoteInto('id = ?', $value);

            $data = [
                'contructors_id' => $contructors_id[$value],
                'price' => $price[$value],
            ];
            $QTransferDetails->update($data, $where);

//            $where = [];
//            $where[] = $QTransferFile->getAdapter()->quoteInto('transfer_details_id = ?', $value);
//            $where[] = $QTransferFile->getAdapter()->quoteInto('type = ?', 3);
//            $QTransferFile->delete($where);
//
//            foreach($_FILES['file']['name'][$value] as $k=>$v){
//
//                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
//                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id. DIRECTORY_SEPARATOR . $value. DIRECTORY_SEPARATOR . '3';
//
//                if (!is_dir($uploaded_dir))
//                    @mkdir($uploaded_dir, 0777, true);
//
//                $tmpFilePath = $_FILES['file']['tmp_name'][$value][$k];
//
//                $old_name = $v;
//                $tExplode = explode('.', $old_name);
//                $extension = end($tExplode);
//                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
//                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
//
//                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
//                    $url = DIRECTORY_SEPARATOR. 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id. DIRECTORY_SEPARATOR . $value. DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $new_name;
//
//                    $dtf = [
//                        'transfer_details_id' => $value,
//                        'url'   => $url,
//                        'type' => 3
//                    ];
//                    $QTransferFile->insert($dtf);
//                }
//            }

            // Insert quotation
            $where = [];
            $where [] = $QTransferQuotation->getAdapter()->quoteInto('transfer_details_id = ?', $value);
            $where [] = $QTransferQuotation->getAdapter()->quoteInto('status = 0');
            $QTransferQuotation->update(['status' => 1], $where);

            foreach ($transfer_quotation_title [$value] as $index => $title) {
                if ($title && $transfer_quotation_total_price [$value] [$index]) {
                    $data_quotation = [
                        'transfer_details_id' => $value,
                        'title' => $title,
                        'quantity' => $transfer_quotation_quantity [$value] [$index] ? $transfer_quotation_quantity [$value] [$index] : NULL,
                        'total_price' => $transfer_quotation_total_price [$value] [$index],
                        'status' => 0
                    ];
                    $QTransferQuotation->insert($data_quotation);

                } else {
                    $flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đầy đủ nội dung và thành tiền trong upload báo giá!');
                    $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
                }

            }
        }

        $QTransfer->update([
            'status' => 2,
            'reject' => NULL,
            'reject_note' => NULL
        ], $where_transfer);

    }//$transfer['status'] == 1

    //2: Chờ TMK Leader xác nhận
    if ($transfer['status'] == 2) {
        $QTransfer->update(['status' => 3], $where_transfer);
    }

    //3: Chờ ASM xác nhận
    if ($transfer['status'] == 3) {
        $QTransfer->update([
            'status' => 4,
            'reject' => 0
        ], $where_transfer);
    }


    //4: Chờ Trade local nghiệm thu
    if ($transfer['status'] == 4) {

        $tag = 0;

        foreach ($image as $key => $value) {

            foreach ($value as $k => $v) {
                $data = $v;

                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . '4';

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $url = DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . '4' . DIRECTORY_SEPARATOR . $new_name . '/image_' . $k . '.png';

                file_put_contents($uploaded_dir . '/image_' . $k . '.png', $data);

                $rdf_insert = [
                    'transfer_details_id' => $key,
                    'url' => $url,
                    'type' => 4
                ];
                $QTransferFile->insert($rdf_insert);
                $tag = 1;
            }
        }

        // Insert quotation
        foreach ($transfer_details_id as $key => $value) {
            $where = [];
            $where[] = $QTransferQuotation->getAdapter()->quoteInto('transfer_details_id = ?', $value);
            $where[] = $QTransferQuotation->getAdapter()->quoteInto('status = 2');
            $QTransferQuotation->update(['status' => 3], $where);

            foreach ($transfer_quotation_title [$value] as $index => $title) {

                if ($title) {
                    $data_quotation = [
                        'transfer_details_id' => $value,
                        'title' => $title,
                        'quantity' => $transfer_quotation_quantity [$value] [$index] ? $transfer_quotation_quantity [$value] [$index] : NULL,
                        'total_price' => $transfer_quotation_total_price [$value] [$index] ? $transfer_quotation_total_price [$value] [$index] : Null,
                        'status' => 2
                    ];
                    $QTransferQuotation->insert($data_quotation);
                }
            }
        }

        if ($tag != 1) {
            $flashMessenger->setNamespace('error')->addMessage("Vui lòng upload ảnh nghiệm thu.");
            $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
        }

        $QTransfer->update(['status' => 5, 'reject' => 0], $where_transfer);
    }

    //5: Chờ TMK Leader xác nhận hoàn thành
    if ($transfer['status'] == 5) {
        
        if (($transfer['warehouse_to'])) {  // chuyển đến kho

            foreach ($transfer_details as $key => $value) {

                $existedInventory = $QAirInventoryArea->getExistedInventory($value['category_id'], $transfer['area_to']);
                // lưu tổng
                if ($existedInventory) {
                    $QAirInventoryArea->update([
                        'quantity' => $existedInventory['quantity'] + $value['quantity']
                    ], ['id = ?' => $existedInventory['id']]);

                } else {
                    $QAirInventoryArea->insert([
                        'category_id' => $value['category_id'],
                        'quantity' => $value['quantity'],
                        'area_id' => $transfer['area_to'] ? $transfer['area_to'] : NULL
                    ]);
                }


                // Lưu lịch sử vận chuyển đến kho
                $QInventoryOrderIn->insert([
                    'sn' => $value['imei_sn'] ? $value['imei_sn'] : Null,
                    'category_id' => $value['category_id'],
                    'warehouse_id' => $transfer['area_to'],
                    'quantity' => $value['quantity'],
                    'type' => 1,
                    'from_source_id' => $transfer_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id
                ]);

                $tranfer_from_to = $QTransfer->tranferFromToArea($transfer['store_from'], $value['category_id'], $value['quantity'], $value['imei_sn'], $transfer['area_to']);

            }
//            $tranfer_from_to = $QTransfer->tranferFromToArea($transfer['store_from'], $value['category_id'], $value['quantity'], $value['imei_sn']);
        }


        if (($transfer['warehouse_from'])) {  // chuyển từ kho
            foreach ($transfer_details as $key => $value) {
                // lưu vào kho
                $existedInventory = $QAirInventoryArea->getExistedInventory($value['category_id'], $transfer['area_from']);

                if ($existedInventory) {
                    $QAirInventoryArea->update([
                        'quantity' => $existedInventory['quantity'] - $value['quantity']
                    ], ['id = ?' => $existedInventory['id']]);
                }

                // Lưu lịch sử xuất kho
                $QInventoryOrderOut->insert([
                    'sn' => $value['imei_sn'] ? $value['imei_sn'] : Null,
                    'category_id' => $value['category_id'],
                    'warehouse_id' => $transfer['area_from'],
                    'quantity' => $value['quantity'],
                    'type' => 1,
                    'from_source_id' => $transfer_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id
                ]);

                // cập nhật lại app checkshop
                $where = [
                    'store_id = ?' => $transfer['store_to'],
                    'is_lock = ?' => 1
                ];

                $checkshop = $QAppCheckshop->fetchRow($where);
     

                if(($checkshop)){
                    $whereDetail = [
                        'checkshop_id = ?' => $checkshop['id'],
                        'category_id = ?' =>   $value['category_id']
                    ];

                    $checkshopDetail = $QAppCheckshopDetail->fetchRow($whereDetail);
                    // cập nhật checkshop detail
                    if ($checkshopDetail) { // nếu trong checkshop đã có hạng mục rồi
                        $data_update ['quantity'] = $checkshopDetail['quantity'] + $value['quantity'];

                        if(!empty($value['imei_sn'])){
                            if ($checkshopDetail['imei_sn']) {
                                $imei_transfer = $value['imei_sn'] . ', ' . $checkshopDetail['imei_sn'];
                            } else {
                                $imei_transfer = $value['imei_sn'] ;
                            }

                            $data_update ['imei_sn'] = $imei_transfer ? $imei_transfer : Null;
                        }

                        $QAppCheckshopDetail->update($data_update, $whereDetail);
                    } else {
                        $QAppCheckshopDetail->insert([
                            'category_id' =>  $value['category_id'],
                            'quantity' => $value['quantity'],
                            'checkshop_id' => $checkshop['id'],
                            'imei_sn' => $value['imei_sn'] ? $value['imei_sn'] : Null
                        ]);
                    }

                    // xóa imei trong inventory area imei
                    $arrayImei = explode(", ",$value['imei_sn']);
                    foreach ($arrayImei as $imei) {
                        $QAirInventoryAreaImei->delete(['imei_sn = ?' => trim($imei)]);
                    }
                } else {
                    $flashMessenger->setNamespace('error')->addMessage('Shop is not lock');
                    $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
                }

            }

        }

        //UPDATE lại số lượng shop điều chuyển

            if (!empty($transfer['store_to']) && !empty($transfer['store_from'])) {
                foreach ($transfer_details as $key => $value) {
                    $tranfer_from_to = $QTransfer->tranferFromTo($transfer['store_from'], $transfer['store_to'], $value['category_id'], $value['quantity'], $value['imei_sn']);
            }
            //            } else {
//                $tranfer_from_to = $QTransfer->tranferFromToArea($transfer['store_from'], $value['category_id'], $value['quantity'], $value['imei_sn']);
//            if ($transfer['warehouse_to']) {
//                $tranfer_from_to = $QTransfer->tranferFromToArea($transfer['store_from'], $value['category_id'], $value['quantity'], $value['imei_sn'], $transfer['warehouse_to']);
//            }


        }

        if (! empty($tranfer_from_to) && $tranfer_from_to['code'] != 1) {
            $flashMessenger->setNamespace('error')->addMessage($tranfer_from_to['message']);
            $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
        }
        //END UPDATE

        $QTransfer->update(['status' => 6], $where_transfer);

    }//if($transfer['status'] == 7)


    //TRANSFER CONFIRM
    $rp_confirm = [
        'transfer_id' => $transfer_id,
        'transfer_status' => $transfer['status'],
        'confirm_by' => $this->storage['staff_id'],
        'created_at' => date('Y-m-d H:i:s')
    ];
    $QTransferConfirm->insert($rp_confirm);
    //END TRANSFER CONFIRM

    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);


} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
    $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);

}

?>