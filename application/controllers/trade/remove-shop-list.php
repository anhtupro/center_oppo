<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);

$store_code = $this->getRequest()->getParam('store_code');
$store_name = $this->getRequest()->getParam('store_name');
$created_from = $this->getRequest()->getParam('created_from');
$created_to = $this->getRequest()->getParam('created_to');
$reason = $this->getRequest()->getParam('reason');
$status_id = $this->getRequest()->getParam('status_id');
$area = $this->getRequest()->getParam('area');
$title = $this->getRequest()->getParam('title');
$export = $this->getRequest()->getParam('export');

$QStore = new Application_Model_Store();
$QRemoveShop = new Application_Model_RemoveShop();
$QRemoveShopReason = new Application_Model_RemoveShopReason();
$QAppStatus = new Application_Model_AppStatus();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QArea = new Application_Model_Area();

$app_status = $QAppStatus->get_cache(11);
$app_status_title = $QAppStatusTitle->get_cache(11);
$app_status_title = $app_status_title[$this->storage['title']];

$reason_cache = $QRemoveShopReason->get_cache();
$this->view->reason_cache = $reason_cache;

$limit = LIMITATION;
$total = 0;

$params = array(
    'store_code' => trim($store_code),
    'store_name' => trim($store_name),
    'created_from' => $created_from,
    'created_to' => $created_to,
    'reason' => $reason,
    'status_id' => $status_id,
    'area_id' => $area_id,
    'area' => $area,
    'title' => $title,
    'export' => $export
);

if (in_array($userStorage->title, array(SALES_TITLE, SALES_LEADER_TITLE, SALE_SALE_ASM))) {

    $list_store = $QStore->getShopAccessByStaff();
    $shop_access = array();
    foreach ($list_store as $value) {
        $shop_access[] = $value['id'];
    }
    $params['access_shop_id'] = $shop_access;
}

$params['area_id'] = $this->storage['area_id'];

$removeShopList = $QRemoveShop->fetchPagination($page, $limit, $total, $params);

unset($params['access_shop_id']);

// list để duyệt tất cả đề xuất cho assistant
if ($params['area'] && $status_id == 4) {
    $params['approve_all'] = 1;
    $listRemoveShopAproveAll = $QRemoveShop->fetchPagination($page, $limit, $total, $params);
    unset($params['approve_all']);
}

// export
if ($export) {
    $QRemoveShop->export($removeShopList);
}

$this->view->area = $QArea->get_cache();
$this->view->app_status = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->params = $params;
$this->view->list = $removeShopList;
$this->view->listRemoveShopAproveAll = json_encode($listRemoveShopAproveAll);
$this->view->status = $status_id;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/remove-shop-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

