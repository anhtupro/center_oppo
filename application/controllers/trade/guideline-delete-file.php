<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$fileId = $this->getRequest()->getParam('file_id');
$QGuidelineFile = new Application_Model_GuidelineFile();

$QGuidelineFile->update([
   'is_deleted' => 1
], ['id = ?' => $fileId]);

echo json_encode([
   'status' => 0
]);