<?php

$air_id = $this->getRequest()->getParam('air_id');
$air_details_id = $this->getRequest()->getParam('air_details_id');
$contructors_id = $this->getRequest()->getParam('contructors_id');

$category_id = $this->getRequest()->getParam('category_id');
$width_details = $this->getRequest()->getParam('width_details');
$high_details = $this->getRequest()->getParam('high_details');
$material_details = $this->getRequest()->getParam('material_details');
$location_details = $this->getRequest()->getParam('location_details');


$price = $this->getRequest()->getParam('price');
$final_price = $this->getRequest()->getParam('final_price');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');

$air_date = $this->getRequest()->getParam('air_date');
$image = $this->getRequest()->getParam('image');

$part = $this->getRequest()->getParam('part');
$part_quantity = $this->getRequest()->getParam('part_quantity');

$width = $this->getRequest()->getParam('width');
$wide = $this->getRequest()->getParam('wide');
$deep = $this->getRequest()->getParam('deep');
$total = $this->getRequest()->getParam('total');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger       = $this->_helper->flashMessenger;

$QAir = new Application_Model_AppAir();
$QAirStatus = new Application_Model_AirStatus();
$QAirDetails = new Application_Model_AirDetails();
$QAirDetailsFile = new Application_Model_AirDetailsFile();
$QAirDetailsPart = new Application_Model_RepairDetailsPart();
$QRepairConfirm = new Application_Model_RepairConfirm();

$QAirMapProduct = new Application_Model_AirMapProduct();
$QAirProduct = new Application_Model_AirProduct();
$QAirProductImage = new Application_Model_AirProductImage();

$where_air = $QAirDetails->getAdapter()->quoteInto('id = ?', $air_id);
$air = $QAir->fetchRow($where_air);

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    if ($remove) {
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QAir->update($remove_date, $where_air);

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/air-edit?id=' . $air_id);
    }
    //Reject
    if($reject){
        $QAir->update([
            'reject' => 1,
            'status' => $air['status'] - 1
        ], $where_air);

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);
    }
    //END Reject
    
    
    //1: Chờ trade leader xác nhận
    if($air['status'] == 1){
        $QAir->update(['status' => 2], $where_air);

        $QAirStatus->insert([
            'air_id' => $air_id,
            'status_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id']
        ]);
    }
    
    //2: Chờ  ASM  xác nhận
    if($air['status'] == 2){
        $QAir->update(['status' => 3], $where_air);

        $QAirStatus->insert([
            'air_id' => $air_id,
            'status_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id']
        ]);
    }
    

    //3: Chờ trade local  nghiệm thu
    if($air['status'] == 3){
        
     $tag = 0;
        
        foreach($image as $key=>$value){
            
            foreach($value as $k=>$v){
                $data = $v;
                
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $key;

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);

                $url = '/photo/air_file/3/' . $key . '/image_'.$k.'.png';

                file_put_contents($uploaded_dir.'/image_'.$k.'.png', $data);

                $rdf_insert = [
                    'air_details_id' => $key,
                    'url'    => $url,
                    'type'   => 3
                ];
                $QAirDetailsFile->insert($rdf_insert);
                
                $tag = 1;
            }
        }
        foreach($air_details_id as $key=>$value){


            $product = [
                'category_id' => $category_id[$value],
                'width'       => $width[$value],
                'high'        => $wide[$value],
                'material_id' => null,
                'product_type_id' => null,
                'location'        => null,
                'mica_id'         => null
            ];
            $product_id = $QAirProduct->insert($product);

            $air_map_product = [
                'air_id' => $air_id,
                'product_id' => $product_id
            ];
            $QAirMapProduct->insert($air_map_product);

            foreach ($width_details[$value] as $key => $val) {
                $product_image = [
                    'product_id' => $product_id,
                    'width'      => $val,
                    'high'       => $high_details[$value][$key],
                    'material_id'   => $material_details[$value][$key],
                    'location'   => $location_details[$value][$key]
                ];

                $QAirProductImage->insert($product_image);
            }

            $where = $QAirDetails->getAdapter()->quoteInto('id = ?', $value);
            // lưu edit chiều dài rộng nganng diện tích

            //Upload file nghiệm thu
            foreach($_FILES['files']['name'][$value] as $k=>$v){

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '6' . DIRECTORY_SEPARATOR . $value;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['files']['tmp_name'][$value][$k];

                $old_name = $v;
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '6' . DIRECTORY_SEPARATOR . $value . DIRECTORY_SEPARATOR . $new_name;

                    $dtf = [
                        'air_details_id' => $value,
                        'url'   => '/'.$url,
                        'type' => 6
                    ];
                    $QAirDetailsFile->insert($dtf);
                }

            }

            //Upload giá cuối cùng
            $where = $QAirDetails->getAdapter()->quoteInto('id = ?', $value);
            $data = [
                'final_price' => $final_price[$value]
            ];
            $QAirDetails->update($data, $where);
           
        }
        if($tag == 1){
            $QAir->update(['status' => 4], $where_air);

            $QAirStatus->insert([
                'air_id' => $air_id,
                'status_id' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->storage['staff_id']
            ]);
        }
        else{
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload file hình ảnh nghiệm thu.');
            $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);
        }
        
    }

    //4:chờ Trade Leader xác nhận ngiệm thu
     if($air['status'] == 4){

        $QAir->update(['status' => 5], $where_air);
        $QAirStatus->insert([
            'air_id' => $air_id,
            'status_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id']
        ]);
    }
    
    //4:Trade Leader file biên bản nghiệm thu
//    if($air['status'] == 6){
//         foreach($air_details_id as $key=>$value){
//
//            foreach($_FILES['files']['name'][$value] as $k=>$v){
//
//                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
//                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
//                        DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '6' . DIRECTORY_SEPARATOR . $value;
//
//                if (!is_dir($uploaded_dir))
//                    @mkdir($uploaded_dir, 0777, true);
//
//                $tmpFilePath = $_FILES['files']['tmp_name'][$value][$k];
//
//                $old_name = $v;
//                $tExplode = explode('.', $old_name);
//                $extension = end($tExplode);
//                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
//                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
//
//                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
//                    $url = 'photo' .
//                        DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '6' . DIRECTORY_SEPARATOR . $value . DIRECTORY_SEPARATOR . $new_name;
//
//                    $dtf = [
//                        'air_details_id' => $value,
//                        'url'   => '/'.$url,
//                        'type' => 6
//                    ];
//                    $QAirDetailsFile->insert($dtf);
//                }
//               
//            }
//        }
//
//        $QAir->update(['status' => 7], $where_air);
//        $QAirStatus->insert([
//            'air_id' => $air_id,
//            'status_id' =>7,
//            'created_at' => date('Y-m-d H:i:s'),
//            'created_by' => $this->storage['staff_id']
//        ]);
//    }

    
    //air CONFIRM
    // $rp_confirm = [
    //     'air_id' => $air_id,
    //     'air_status' => $air['status'],
    //     'confirm_by' => $this->storage['staff_id'],
    //     'created_at'    => date('Y-m-d H:i:s')
    // ];
    // $QairConfirm->insert($rp_confirm);
    //END air CONFIRM
    
    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);
    

} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
    $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);

}

?>