<?php 
$id   = $this->getRequest()->getParam('id');
$status   = $this->getRequest()->getParam('status');
$submit   = $this->getRequest()->getParam('submit');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QDestruction = new Application_Model_Destruction();
$flashMessenger       = $this->_helper->flashMessenger;

if (!empty($id))
{
	$result = $QDestruction->GetList($id);
}
// ---------------------THIET LAP HIEN THI CHO USER -------------------
if ($result[0]['staff_id'] == ASM_ID && $result[0]['status'] == 1 ){// HIEN THI NUT SUBMIT CHO ASM
	$display =0;
}else if ($result[0]['staff_id'] == TRADE_ID && $result[0]['status'] == 2){//HIEN THI NUT SUBMIT CHO TRADE
	$display =1;
}else if ($result[0]['staff_id'] == ASM_ID && $result[0]['status'] == 2){// KHONG CHO ASM SUBMIT NUA
	$display =2;
}
else if ($result[0]['staff_id'] == TRADE_ID && $result[0]['status'] == 1){//KHONG CHO TRADE SUBMIT TRUOC TRADE
	$display =3;
}else if (!empty($submit)&&$status !=  $result[0]['status'] ) {// SAU KHI NHAN SUBMIT, KO HIEN THI THONG BAO
	$display =4;
}
else $display=-1; // CAC TRUONG HOP KHAC
$params = array_filter(array(
	'staff_name'=>$userStorage->firstname.' '.$userStorage->lastname,
	'id'=>$id,
	'categoryname' => $result[0]['categoryname'],
	'storename'=> $result[0]['storename'],
	'sl'=> $result[0]['sl'],
	'lydo'=> $result[0]['lydo'],
	'date'=>$result[0]['date'],
	'staff_id'=>$result[0]['staff_id'],
	'status'=>$result[0]['status'],
	'display'=>$display
));

//event xac nhan de xuat huy
if (!empty($submit)) {	
	$flashMessenger->resetNamespace() ;
	$destruction_id   = $this->getRequest()->getParam('iddestruction');	
	$status   = $this->getRequest()->getParam('status');
	if($userStorage->id == ASM_ID && $status == 1 ){ // KHI ASM XAC NHAN
		$data = array_filter(array(
			'status'=>2
		));
		try{
			$result =$QDestruction->update ($data,'id = '.$destruction_id);
			$flashMessenger->setNamespace('success')->addMessage('Thông báo ASM xác nhận thành công');
			$this->_redirect(HOST . 'trade/confirm-remove?id='.$destruction_id);
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Thông báo ASM không thành công');
			$this->_redirect(HOST . 'trade/list-remove?id='.$destruction_id);
		}
	}else if ($userStorage->id == TRADE_ID && $status == 2){// KHI TRADE XAC NHAN
		$data = array_filter(array(
			'status'=>3
		));
		$result =$QDestruction->update ($data,'id = '.$destruction_id);
		try{
			$result =$QDestruction->update ($data,'id = '.$destruction_id);
			$flashMessenger->setNamespace('success')->addMessage('Thông báo Trade xác nhận thành công');
			$this->_redirect(HOST . 'trade/list-remove');
		}catch (exception $e) {
			$flashMessenger->setNamespace('error')->addMessage('Thông báo Trade xác nhận không thành công ');
			$this->_redirect(HOST . 'trade/list-remove');
		}
	}
}
// ---------------hien thi thong bao ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){ 
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}
$this->view->params = $params;
?>