<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
require_once 'PHPExcel.php';

$stage_id = $this->getRequest()->getParam('stage_id');

$db = Zend_Registry::get("db");
$select = $db->select()
    ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
       'a.*'
    ])
    ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
    ->where('a.stage_id = ?', $stage_id);

$data = $db->fetchAll($select);


$QCategory = new Application_Model_Category();
$PHPExcel = new PHPExcel();

$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

$list_category = $QCategory->fetchAll(['check_posm = ?' => 1], ['id']);


$heads = array(
    'Store ID'
);


$alpha = 'B';
$sheet->setCellValue('A1' , 'ID hạng mục (KHÔNG chỉnh sửa thông số của dòng 1 này)');

foreach ($list_category as $category) {
    $heads [] = $category['name'];

    //set dòng 1
    $sheet->setCellValue($alpha . 1 , $category['id']);
    $alpha++;
}


// set dòng 2
$alpha = 'A';
foreach ($heads as $key) {
    $sheet->setCellValue($alpha . '2', $key);
    $alpha++;
}


$alpha = 'A';
$index    = 2;
$i = 1;

foreach($data as $item){

    $alpha    = 'A';
    $sheet->setCellValue($alpha++.$index, $item['store_id']);
    $sheet->setCellValue($alpha++.$index, $item['store_name']);
    $index++;

}



$filename = 'Danh sách đã upload' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;