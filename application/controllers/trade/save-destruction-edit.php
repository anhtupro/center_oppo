<?php
require_once "Aws_s3.php";
$s3_lib = new Aws_s3();

$destruction_id = $this->getRequest()->getParam('destruction_id');
$destruction_details_id = $this->getRequest()->getParam('destruction_details_id');
$contractor_id = $this->getRequest()->getParam('contractor_id');
$price = $this->getRequest()->getParam('price');
$price_final = $this->getRequest()->getParam('price_final');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');
$check_tp = $this->getRequest()->getParam('check_tp');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$destruction_date = $this->getRequest()->getParam('destruction_date');
$image = $this->getRequest()->getParam('image');

$destruction_quotation_title = $this->getRequest()->getParam('destruction_quotation_title');
$destruction_quotation_quantity = $this->getRequest()->getParam('destruction_quotation_quantity');
$destruction_quotation_total_price = $this->getRequest()->getParam('destruction_quotation_total_price');
$review_cost = $this->getRequest()->getParam('review_cost');
$review_note = $this->getRequest()->getParam('review_note');
$month_debt = $this->getRequest()->getParam('month_debt');

$part = $this->getRequest()->getParam('part');
$part_quantity = $this->getRequest()->getParam('part_quantity');
$manager_approve = $this->getRequest()->getParam('manager_approve');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QDestruction = new Application_Model_Destruction();
$QDestructionDetails = new Application_Model_DestructionDetails();
$QDestructionDetailsFile = new Application_Model_DestructionDetailsFile();
$QDestructionDetailsPart = new Application_Model_DestructionDetailsPart();
$QDestructionConfirm = new Application_Model_DestructionConfirm();
$QDestructionQuotation = new Application_Model_DestructionQuotation();
$QDestructionDetailsChild = new Application_Model_DestructionDetailsChild();
$QAirInventoryAreaImei = new Application_Model_AirInventoryAreaImei();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QDestructionPrice = new Application_Model_DestructionPrice();
$QDestructionImage = new Application_Model_DestructionImage();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();

$where_destruction = $QDestruction->getAdapter()->quoteInto('id = ?', $destruction_id);
$destruction = $QDestruction->fetchRow($where_destruction);

$where = $QDestructionDetails->getAdapter()->quoteInto('destruction_id = ?', $destruction_id);
$destruction_details = $QDestructionDetails->fetchAll($where);
$destruction_details_child = $QDestructionDetailsChild->fetchAll($where);

$db = Zend_Registry::get('db');
$db->beginTransaction();


try {
    // Hủy đề xuất
    if ($remove) {
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QDestruction->update($remove_date, $where_destruction);

        // cập nhật trạng thái của hạng mục
        foreach ($destruction_details_child as $value) {
            if ($value['app_checkshop_detail_child_id']) {
                $QAppCheckshopDetailChild->update([
                    'in_processing' => Null
                ], ['id = ?' => $value['app_checkshop_detail_child_id']]);
            }
        }


        // notify
        $area_id = $QArea->getAreaByStore($destruction['store_id']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(12, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất TIÊU HỦY bị hủy",
                'link' => "/trade/destruction-edit?id=" . $destruction_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
    }

    //Reject
    if ($reject) {
        if (in_array($destruction['status'], [2,3,4])) {
            $destruction_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 1,
                'reject_by' => $userStorage->id,
                'reject_at' => date('Y-m-d H:i:s')
            ];

            $status_notify = 1;
        }

        if (in_array($destruction['status'], [6])) {
            $destruction_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 5,
                'reject_by' => $userStorage->id,
                'reject_at' => date('Y-m-d H:i:s')
            ];

            $status_notify = 5;
        }

        if (in_array($destruction['status'], [1,5])) {
            $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
        }


        $QDestruction->update($destruction_data, $where_destruction);


        // notify
        $area_id = $QArea->getAreaByStore($destruction['store_id']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(12, $status_notify, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất TIÊU HỦY bị từ chối",
                'link' => "/trade/destruction-edit?id=" . $destruction_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);


    }
    //END Reject


    //1: Chờ TMK Local xác nhận
    if ($destruction['status'] == 1) {
        // chọn nhà thầu
        $QDestruction->update([
            'contractor_id' =>  $contractor_id ? $contractor_id : Null
        ], ['id = ?' => $destruction_id]);

        // điền giá
        $where = [];
        $where [] = $QDestructionPrice->getAdapter()->quoteInto('destruction_id = ?', $destruction_id);
        $where [] = $QDestructionPrice->getAdapter()->quoteInto('status = ?', 0);

        $QDestructionPrice->update(['status' => 1], $where);

        foreach ($destruction_quotation_title as $index => $title) {
            if ($title && $destruction_quotation_total_price [$index]) {
                $data_quotation = [
                    'destruction_id' => $destruction_id,
                    'title' => $title,
                    'quantity' => $destruction_quotation_quantity [$index] ? $destruction_quotation_quantity [$index] : NULL,
                    'total_price' => $destruction_quotation_total_price [$index],
                    'status' => 0
                ];
                $QDestructionPrice->insert($data_quotation);

            } else {
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đầy đủ nội dung và thành tiền trong upload báo giá!');
                $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
            }

        }

        if ($destruction['approve_type'] ) {
            if ($destruction['approve_type'] == 1 || $destruction['approve_type']  == 2) {
                $status = 2;
            }
            if ($destruction['approve_type'] == 3) {
                $status = 5;
            }
        } else {
            $status = 2;
        }

        $QDestruction->update(['status' => $status], $where_destruction);

        $status_notify = $status;

    }

    //2: Chờ TMK Leader xác nhận
    if ($destruction['status'] == 2) {

        if($destruction['approve_type'] && $destruction['approve_type'] == 1) {
            $status = 4;
        } else {
            $status = 5;
        }

        $QDestruction->update(['status' => $status], $where_destruction);

        $status_notify = $status;

    }

    //3: Chờ ASM xác nhận
    if ($destruction['status'] == 3) {

        if($destruction['approve_type'] && $destruction['approve_type'] == 1) {
            $status = 4;
        } else {
            $status = 5;
        }

        $QDestruction->update(['status' => $status], $where_destruction);

        $status_notify = $status;

    }

    //4: Chờ TMK Manager xác nhận
    if ($destruction['status'] == 4) {
        $QDestruction->update(['status' => 5], $where_destruction);

        $status_notify = 5;
    }


    // Chờ Trade local nghiệm thu
    if ($destruction['status'] == 5) {
        // upload image attendance
        $tag = 0;

        foreach($image as $k=>$v){

            $data = $v;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . $destruction_id . DIRECTORY_SEPARATOR . 'image_acceptance';

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/destruction/' . $destruction_id . '/image_acceptance/' . $newName;

            $file = $uploaded_dir . '/' . $newName;

            $success_upload = file_put_contents($file , $data);

            // upload image to server s3
            $destination_s3 = 'photo/destruction/' . $destruction_id . '/image_acceptance/';

            if ($success_upload) {
                $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
            }
            // upload image to server s3

            $destruction_image = [
                'destruction_id' => $destruction_id,
                'url'    => $url,
                'type'   => 3
            ];
            $QDestructionImage->insert($destruction_image);
            $tag = 1;
        }


        if ($tag != 1) {
            $flashMessenger->setNamespace('error')->addMessage("Vui lòng upload ảnh nghiệm thu.");
            $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
        }

        // price
        $where = [];
        $where [] = $QDestructionPrice->getAdapter()->quoteInto('destruction_id = ?', $destruction_id);
        $where [] = $QDestructionPrice->getAdapter()->quoteInto('status = ?', 2);

        $QDestructionPrice->update(['status' => 3], $where);

        foreach ($destruction_quotation_title as $index => $title) {
            if ($title && $destruction_quotation_total_price [$index]) {
                $data_quotation = [
                    'destruction_id' => $destruction_id,
                    'title' => $title,
                    'quantity' => $destruction_quotation_quantity [$index] ? $destruction_quotation_quantity [$index] : NULL,
                    'total_price' => $destruction_quotation_total_price [$index],
                    'status' => 2
                ];
                $QDestructionPrice->insert($data_quotation);

            } else {
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đầy đủ nội dung và thành tiền trong báo giá!');
                $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
            }

        }

        $QDestruction->update(['status' => 6], $where_destruction);

        $status_notify = 6;
    }

    //7: Chờ TMK Leader xác nhận hoàn thành
    if ($destruction['status'] == 6) {

     

        // Tiêu hủy ở shop
        if ($destruction['store_id']) {
            $status_code = $QDestruction->destructFromShop($destruction, $destruction_details_child);
        }


        // set error
        if ($status_code['code'] == 2) {
            $flashMessenger->setNamespace('error')->addMessage($status_code['message']);
            $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
        }

        $QDestruction->update(['status' => 7], $where_destruction);

        $status_notify = 7;

    }


    // chờ HO xác nhận chi phí thanh toán
    if ($destruction['status'] == 7) {
        if ($review_cost == 2) { // chi phí sai
            // price
            $where = [];
            $where [] = $QDestructionPrice->getAdapter()->quoteInto('destruction_id = ?', $destruction_id);
            $where [] = $QDestructionPrice->getAdapter()->quoteInto('status = ?', 4);
            $QDestructionPrice->update(['status' => 5], $where);

            foreach ($destruction_quotation_title as $index => $title) {
                    $data_quotation = [
                        'destruction_id' => $destruction_id,
                        'title' => $title,
                        'quantity' => $destruction_quotation_quantity [$index] ? $destruction_quotation_quantity [$index] : NULL,
                        'total_price' => $destruction_quotation_total_price [$index] ? $destruction_quotation_total_price [$index] : Null,
                        'status' => 4
                    ];
                    $QDestructionPrice->insert($data_quotation);

            }


        }

        if ($review_cost == 1) { // chi phí chính xác
            $data_price = $QDestructionPrice->fetchAll([
                'destruction_id = ?' => $destruction_id,
                'status = ?' => 2
            ]);

            foreach ($data_price as $price) {
                $QDestructionPrice->insert([
                    'destruction_id' => $price['destruction_id'] ? $price['destruction_id'] : Null,
                    'title' => $price['title'] ? $price['title'] : Null,
                    'quantity' => $price['quantity'] ? $price['quantity'] : Null,
                    'total_price' => $price['total_price'] ? $price['total_price'] : Null,
                    'status' => 4
                ]);
            }
        }

        $QDestruction->update([
            'status' => 8,
            'review_cost' => $review_cost,
            'review_note' => $review_note,
            'month_debt' => $month_debt
        ], $where_destruction);

        $status_notify = 8;


    }


    //DESTRUCTION CONFIRM
    $destruction_confirm = [
        'destruction_id' => $destruction_id,
        'destruction_status' => $destruction['status'],
        'confirm_by' => $this->storage['staff_id'],
        'created_at' => date('Y-m-d H:i:s')
    ];
    $QDestructionConfirm->insert($destruction_confirm);
    //END DESTRUCTION CONFIRM
    $QDestruction->update(['reject' => 0], $where_destruction);

    // notify
    $area_id = $QArea->getAreaByStore($destruction['store_id']);
    $list_staff_notify = $QAppStatusTitle->getStaffToNotify(12, $status_notify, $area_id);

    foreach ($list_staff_notify as $staff_notify) {
        $data_noti = [
            'title' => "TRADE MARKETING OPPO VIETNAM",
            'message' => "Có một đề xuất TIÊU HỦY đang chờ bạn duyệt",
            'link' => "/trade/destruction-edit?id=" . $destruction_id,
            'staff_id' => $staff_notify
        ];

//        $QAppNoti->sendNotification($data_noti);
    }

    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);


} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
    $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);

}

?>