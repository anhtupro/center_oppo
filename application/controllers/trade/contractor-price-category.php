<?php

    $contractor_id = $this->getRequest()->getParam('contractor_id');

    $QCategory = new Application_Model_Category();
    $QContractorPrice = new Application_Model_ContractorPrice();
    $QContractor = new Application_Model_Contructors();
    $flashMessenger         = $this->_helper->flashMessenger;

    $params = [
        'contractor_id'   => $contractor_id
    ];

    $contractor = $QContractor->fetchRow($QContractor->getAdapter()->quoteInto('id = ?', $contractor_id));
	$this->view->contractor = $contractor->toArray();
     $contractor_price = $QContractorPrice->getContractorCategoryPrice($params);
    // echo '<pre>';
    // print_r($contractor_price); exit;
    foreach($contractor_price as $key=>$value){
        $params['not_category'][] = $value['category_id'];
    }
    $category_code = $QContractorPrice->getCategoryCode($params);

    if($this->getRequest()->getMethod()=='POST')
    {
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $category = $this->getRequest()->getParam('category');
            $contractor_del = $this->getRequest()->getParam('contractor_del');
            $price = $this->getRequest()->getParam('price');

            $current_date = date('Y-m-d H:i:s');

            //Xóa 
            // $where = $QContractorPrice->getAdapter()->quoteInto('contractor_id = ?', $contractor_id);
            // $QContractorPrice->update(['to_date' => $current_date],$where);

            //Điều chỉnh 
            foreach($category as $key=>$value){

                if(!empty($value) AND $value != ''){

                    $data_add = [
                        'contractor_id'  => $contractor_id,
                        'category_id'  => $value,
                        'price'  => $price[$key],
                        'from_date'  => $current_date,
                        'to_date'   => NULL
                    ];

                    $QContractorPrice->insert($data_add);
                }
            }

            $db->commit();

            $flashMessenger->setNamespace('success')->addMessage('Thành công!');

            $this->_redirect('/trade/contractor-price-category?contractor_id='.$contractor_id);

        } catch (Exception $e) {
            $db->rollback();
            $e->getMessage();

            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->_redirect('/trade/contractor-price-category?contractor_id='.$contractor_id);
        }


    }


    $this->view->contractor_price = $contractor_price;
    $this->view->category_code = $category_code;

    $messages             = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
    $messages_error       = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

    //$this->_helper->viewRenderer->setRender('category-contructor/category-price-create');




