<?php

$checkshop_id = $this->getRequest()->getParam('checkshop_id');
$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category_id');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$latitude = $this->getRequest()->getParam('latitude');
$longitude = $this->getRequest()->getParam('longitude');
$back_url = HOST . 'trade/check-shop-list';
$flashMessenger = $this->_helper->flashMessenger;
$QAppCheckshop = new Application_Model_AppCheckshop();
$QStore = new Application_Model_Store();
$QBiTrade = new Application_Model_BiTrade();
$QCheckshopDetails = new Application_Model_AppCheckShopDetail();
$QAppCheckShopFile = new Application_Model_AppCheckShopFile();

$db = Zend_Registry::get('db');
$db->beginTransaction();


if ((!empty($latitude) && !empty($longitude)) || 1 == 1) {
    $where_update = $QAppCheckshop->getAdapter()->quoteInto('id = ?', $checkshop_id);
    $date = date('Y-m-d H:i:s');
    $data_update = [
//        'latitude' => !empty($latitude) ? $latitude : '',
//        'longitude' => !empty($longitude) ? $longitude : '',
        'updated_at' => $date,
        'status' => 1
    ];
    $QAppCheckshop->update($data_update, $where_update);
    //xóa dữ liệu cũ
    $where_delete = $QCheckshopDetails->getAdapter()->quoteInto('checkshop_id = ?', $checkshop_id);
    $QCheckshopDetails->delete($where_delete);
} else {
    $db->rollBack();
    $flashMessenger->setNamespace('error')->addMessage("Chưa xác định được tọa độ. Vui lòng thử lại!!");
    $this->getResponse()->setHeader('Content-Type', 'text/plain')
        ->setBody(HOST . 'trade/edit-checkshop?id=' . $store_id)->sendResponse();
    exit;
}

try {
    
    //Nếu có lock: Check số lượng checkshop khớp với số lượng chốt
    $investments_lock = $QAppCheckshop->getCheckshopLock($store_id);

    if(!empty($investments_lock)){
        
        $cat_check = [];
        foreach($category_id as $key => $value){
            
            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);
            
            $cat_check['quantity'][$value] = $number;
            $cat_check['imei_sn'][$value] = $imei[$key] ? $imei[$key] : NULL;
        }
        
        $cat_lock = [];
        foreach($investments_lock as $key => $value){
            $cat_lock['quantity'][$value['category_id']] = $value['quantity'];
            $cat_lock['imei_sn'][$value['category_id']] = $value['imei_sn'];
        }
        
        if($cat_check['quantity'] != $cat_lock['quantity']){

            $flashMessenger->setNamespace('error')->addMessage("Số lượng không khớp với số chốt.");
            $this->getResponse()->setHeader('Content-Type', 'text/plain')
                ->setBody(HOST . 'trade/edit-checkshop?id=' . $checkshop_id)->sendResponse();
            exit;
        }
        
        if($cat_check['imei_sn'] != $cat_lock['imei_sn']){

//            $flashMessenger->setNamespace('error')->addMessage("Mã QR Code không khớp với mã chốt.");
//            $this->getResponse()->setHeader('Content-Type', 'text/plain')
//                ->setBody(HOST . 'trade/edit-checkshop?id=' . $checkshop_id)->sendResponse();
//            exit;
        }

    }
    
    $total = count($_FILES['image']['name']);
    if ($total >= 3) {
        for ($i = 0; $i < $total; $i++) {
            if (!empty($_FILES['image']['name'][$i])) {

                $where_delete_file = array();
                $where_delete_file[] = $QAppCheckShopFile->getAdapter()->quoteInto('checkshop_id = ?', $checkshop_id);
                $where_delete_file[] = $QAppCheckShopFile->getAdapter()->quoteInto('type = ?', $i);
                $QAppCheckShopFile->delete($where_delete_file);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . $checkshop_id;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['image']['tmp_name'][$i];

                if ($tmpFilePath != "") {
                    $old_name = $_FILES['image']['name'][$i];
                    $tExplode = explode('.', $old_name);
                    $extension = end($tExplode);
                    $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                    $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $url = 'photo' .
                            DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . $checkshop_id . DIRECTORY_SEPARATOR . $new_name;
                        //Resize Anh

                        $image = new My_Image_Resize();
                        $image->load($url);
                        $image->resizeToWidth(800);
                        $image->save($url);

                        //END Resize

                    } else {
                        $url = NULL;
                    }
                } else {
                    $db->rollBack();
                    $flashMessenger->setNamespace('error')->addMessage("Thiếu ảnh. Vui lòng kiểm tra lại!!");
                    $this->getResponse()->setHeader('Content-Type', 'text/plain')
                        ->setBody(HOST . 'trade/edit-checkshop?id=' . $checkshop_id)->sendResponse();
                    exit;
                }

                if ($url[$i] != null) {
                    $img = [
                        'checkshop_id' => $checkshop_id,
                        'url' => $url,
                        'type' => $i
                    ];
                    $QAppCheckShopFile->insert($img);
                }
            }
        }
        foreach ($category_id as $key => $value) {
            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }

            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);
            $details = [
                'checkshop_id' => $checkshop_id,
                'category_id' => $category_id[$key],
                'quantity' => $number,
                'imei_sn' => $imei[$key]
            ];

            $QCheckshopDetails->insert($details);
        }
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Thành công!');
        $this->getResponse()->setHeader('Content-Type', 'text/plain')
            ->setBody(HOST . 'trade/check-shop-list')->sendResponse();
        exit;
    } else {
        $db->rollBack();
        $flashMessenger->setNamespace('error')->addMessage("upload ít nhất 3 ảnh. Vui lòng kiểm tra lại!!");
        $this->getResponse()->setHeader('Content-Type', 'text/plain')
            ->setBody(HOST . 'trade/edit-checkshop?id=' . $checkshop_id)->sendResponse();
        exit;
    }
} catch (Exception $e) {
    $db->rollBack();
    $flashMessenger->setNamespace('error')->addMessage("Có lỗi: " . $e->getMessage());
    $this->getResponse()->setHeader('Content-Type', 'text/plain')
        ->setBody($back_url)->sendResponse();
    exit;
}
