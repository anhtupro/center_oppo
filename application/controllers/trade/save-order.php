<?php 
	
	$data_order 	= $this->getRequest()->getParam('data_order');
	$order_id 		= $this->getRequest()->getParam('order_id');
	$campaign_id 	= $this->getRequest()->getParam('campaign_id');
	$note 			= $this->getRequest()->getParam('note');

	$data_order = json_decode($data_order, true);

	$flashMessenger = $this->_helper->flashMessenger;
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id = $userStorage->id;

	$back_url = HOST.'trade/create-order?campaign_id='.$campaign_id;

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		$QAppOrder 			= new Application_Model_AppOrder();
		$QAppOrderDetails 	= new Application_Model_AppOrderDetails();
		$QAppStatus			= new Application_Model_AppStatus();
		$QAppOrderImage		= new Application_Model_AppOrderImage();


		$data = [
			'campaign_id'	=> $campaign_id,
			'created_by'	=> $staff_id,
			'created_at'	=> date('Y-m-d H:i:s'),
			'note'			=> $note,
			'status'		=> 1,
			'type'			=> TU_BAN_BUCGOC
		];

		if(empty($order_id)){
			$order_id = $QAppOrder->insert($data);
		}
		else{
			//Nếu có order_id rồi thì xóa hết details rồi thêm lại
			$where = $QAppOrderDetails->getAdapter()->quoteInto('order_id = ?', $order_id);
			$QAppOrderDetails->delete($where);
		}
		
		foreach ($data_order as $key => $value) {
			foreach ($value as $k => $v) {

				if($v['quantity'] > 0){
					$data_details = [
						'order_id'		 => $order_id,
						'store_id'		 => $v['id'],
						'category_id'	 => $key,
						'quantity'		 => $v['quantity'],
						'quantity_limit' => $v['hanmuc_dk'],
						'status'		 => 1,
					];

					$order_details = $QAppOrderDetails->insert($data_details);

					$data_image = [
						'order_details_id' => $order_details,
						'url'			   => $v['url']
					];
					$QAppOrderImage->insert($data_image);
				}

			}

		}

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Đăng ký thành công!');
    	$this->redirect($back_url.'&order_id='.$order_id);


	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url.'&order_id='.$order_id);
	}
	exit;




