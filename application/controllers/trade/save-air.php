<?php
$category_id   = $this->getRequest()->getParam('category');
$quantity   = $this->getRequest()->getParam('quantity');
$quantity_nhathau   = $this->getRequest()->getParam('quantity_nhathau');
$air_detail_id   = $this->getRequest()->getParam('air_detail_id');
//excel
$export   = $this->getRequest()->getParam('export');

$horizontal   = $this->getRequest()->getParam('horizontal');
$height   = $this->getRequest()->getParam('height');
$deep   = $this->getRequest()->getParam('deep');
$total_dt   = $this->getRequest()->getParam('total_dt');
$quantity   = $this->getRequest()->getParam('quantity');
$price = $this->getRequest()->getParam('price');
$total_price = $this->getRequest()->getParam('total_price');
$quotation_cat_id = $this->getRequest()->getParam('quotation_cat_id');
$quotation_id = $this->getRequest()->getParam('quotation_id');
$contractor_id = $this->getRequest()->getParam('contractor_id');
$cate_quo_id = $this->getRequest()->getParam('cate_quo_id');

$ngang = $this->getRequest()->getParam('ngang');
$cao = $this->getRequest()->getParam('cao');
$sau = $this->getRequest()->getParam('sau');
$tongdt = $this->getRequest()->getParam('tongdt');

$dem=$this->getRequest()->getParam('dem');
$store_id   = $this->getRequest()->getParam('store');

$id = $this->getRequest()->getParam('id');
$cate_quotation = $this->getRequest()->getParam('baogia');
$unit = $this->getRequest()->getParam('unit');
$soluong = $this->getRequest()->getParam('soluong');
$dongia = $this->getRequest()->getParam('dongia');
$tongtien= $this->getRequest()->getParam('tongtien');
  
$contructor=$this->getRequest()->getParam('contructor');
$cate_id=$this->getRequest()->getParam('cate');
$id_detail=$this->getRequest()->getParam('id_detail');
$hangmuc_id= $this->getRequest()->getParam('hangmuc_id');
$submit	    = $this->getRequest()->getParam('submit');
$edit	    = $this->getRequest()->getParam('edit');

$total_dt_edit = $this->getRequest()->getParam('total_dt_edit');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QCampaign = new Application_Model_Campaign();
$QAppAir = new Application_Model_AppAir();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QAppAirFile= new Application_Model_AppAirFile();
$QAppQuotation = new Application_Model_AppQuotation();
$QAppQuotationCat = new Application_Model_AppQuotationCat();
$QAirContructor = new Application_Model_AppAirContructor();

include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
require_once 'phpmail'.DIRECTORY_SEPARATOR.'class.phpmailer.php';

$flashMessenger       = $this->_helper->flashMessenger;

$db   = Zend_Registry::get('db');
$db->beginTransaction();

$id_user=$userStorage->id;
$title =$userStorage->title;
$params = array(
	'id' => $id,
	'id_contractor'=> $id_user
);
$contractor = $QAppAir->getContructor_ID($id_user);
$id_contractor=$contractor['id'];
$back_url = HOST.'trade/air';
//lấy status
  $select = $QAppAir->select();
            $select->where('id = ?',$id); 
            $status = $QAppAir->fetchRow($select);
if(!empty($export)){
		//tách dữ liệu value của export lấy id nhà thầu vào id category
        $ex_contrac_cate=explode('-',$export);
        $id_con=$ex_contrac_cate[1];
        $id_ca=$ex_contrac_cate[0];
		require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'E';
        $index = 1;

        $PHPExcel->setActiveSheetIndex(0)->mergeCells('A1:A2');
        $PHPExcel->setActiveSheetIndex(0)->mergeCells('B1:B2');
        $PHPExcel->setActiveSheetIndex(0)->mergeCells('C1:C2');
        $PHPExcel->setActiveSheetIndex(0)->mergeCells('D1:D2');
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'PHÂN LOẠI');
        $sheet->setCellValue('C1', 'ID (KHÔNG ĐƯỢC XÓA)');
        $sheet->setCellValue('D1', 'HẠNG MỤC');
         $PHPExcel->getActiveSheet()->getStyle('A1:D2')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '32CD32'
        )));

        $headgroup =array(
        	'BÁO GIÁ BAN ĐẦU',
        	'SAU KHI ĐIỀU CHỈNH'
        );
         foreach ($headgroup as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha='L';
        }
        $index ++;
        $alpha = 'E';
        $heads = array(
         	
            'CHIỀU NGANG',
            'CHIỀU CAO',
            'CHIỀU SÂU',
            'TỔNG DIỆN TÍCH',
            'SỐ LƯỢNG',
            'ĐƠN GIÁ',
            'THÀNH TIỀN',
            'CHIỀU NGANG',
            'CHIỀU CAO',
            'CHIỀU SÂU',
            'TỔNG DIỆN TÍCH',
            'SỐ LƯỢNG',
            'ĐƠN GIÁ',
            'THÀNH TIỀN'
        );
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
         $PHPExcel->setActiveSheetIndex(0)->mergeCells('E1:K1');
        $PHPExcel->setActiveSheetIndex(0)->mergeCells('L1:R1');

        //text-align center
        $PHPExcel->setActiveSheetIndex(0)->getStyle('A1:R10000')->getAlignment()->applyFromArray(
     		array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
   		 );
        $PHPExcel->setActiveSheetIndex(0)->getStyle('A1:R10000')->getAlignment()->applyFromArray(
     		array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
   		 );
       
        //set màu cho 2 header
         $PHPExcel->getActiveSheet()->getStyle('E1:K1')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '228B22'
        )));
         $PHPExcel->getActiveSheet()->getStyle('A2:K2')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '32CD32'
        )));
         $PHPExcel->getActiveSheet()->getStyle('L1:R1')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '00868B'
        )));
          $PHPExcel->getActiveSheet()->getStyle('L2:R2')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '00C5CD'
        )));
        
        $data = $QAppQuotation->getExcel($params,$id_ca,$id_con);
        

        $index = 3;
        $total_first = 0;
        $total=0;
        foreach($data as $key=>$value){
         $alpha = 'A';
         $sheet->getCell($alpha++ . $index)->setValueExplicit($key+1,PHPExcel_Cell_DataType::TYPE_NUMERIC);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['type'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['quotation_cat_id'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['quotation_cate_name'],PHPExcel_Cell_DataType::TYPE_STRING);

         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['horizontal'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['height'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['deep'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['total_dt'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['quantity'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['price'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['total_price'],PHPExcel_Cell_DataType::TYPE_STRING);
       
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['horizontal1'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['height1'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['deep1'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['total_dt1'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['quantity1'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['price1'],PHPExcel_Cell_DataType::TYPE_STRING);
         $sheet->getCell($alpha++ . $index)->setValueExplicit($value['total_price1'],PHPExcel_Cell_DataType::TYPE_STRING);

         $total_first+=$value['total_price'];
         $total=$total +($value['total_price1']+$value['total_price']);
         ($value['quotation_cat_id']==$value['quotation_cat_id1']) ? $total -= $value['total_price']:$total;
         $index++;
        } 
        
        $footer =array(
       	'TỔNG TIỀN BAN ĐẦU',
       	'TỔNG TIỀN SAU ĐIỀU CHỈNH'
       );
        $alpha='A';
        foreach ($footer as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha='L';
        }
        //merge cell
        $PHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$index.':J'.$index.'');
        $PHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$index.':Q'.$index.'');
        //set màu cho dòng tổng tiền
        $PHPExcel->getActiveSheet()->getStyle('A'.$index.':K'.$index.'')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => 'C1FFC1'
        )));
          $PHPExcel->getActiveSheet()->getStyle('L'.$index.':R'.$index.'')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => 'D1EEEE'
        )));
          //set 2 giá trị tổng tiền cho cell
        $sheet->setCellValue('K' . $index, $total_first);
        $sheet->setCellValue('R' . $index, $total);
        //tạo all border
        $styleArray = array(
  		 'borders' => array(
    	 'allborders' => array(
     	 'style' => PHPExcel_Style_Border::BORDER_THIN
    	)));
        $PHPExcel->getActiveSheet()->getStyle('A1:R'.$index.'')->applyFromArray($styleArray);
        $filename = ' Quotation ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
}
if($status['status']==1)
{
	try{

		$total = count($_FILES['files']['name']);
		//var_dump($total); exit;
		$upload = new Zend_File_Transfer();
		//$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
		$upload->addValidator('Size',false,array('max' => '13MB'));
		
		if (!$upload->isValid()) {

    		$db->rollBack();
		    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Dung lượng file vượt mức cho phép. !");
	    	$this->redirect(HOST.'trade/edit-air?id='.$id);
		}
		for($i=0; $i<$total; $i++){
			$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
							DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
							DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $id;

			if (!is_dir($uploaded_dir))
			@mkdir($uploaded_dir, 0777, true);
			$tmpFilePath = $_FILES['files']['tmp_name'][$i];
			if ($tmpFilePath != ""){
				$old_name 	= $_FILES['files']['name'][$i];
				$tExplode 	= explode('.', $old_name);
				$extension  = end($tExplode);
				$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
				$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
					if(move_uploaded_file($tmpFilePath, $newFilePath)) {
						$url    = 'photo' .
								DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
					}else{
						$url = NULL;
					}
			}else{
				$url = NULL;
			}

			$details = [
				'air_id' => $id,
				'url'  => $url,
				'type'	   => 1,
				'file_name'=>$new_name,
				'contructor_id' =>0
			];
			$QAppAirFile->insert($details);
			//cập nhật status khi approvez
		}
		$where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
			$data_update=array(
				'status'      =>8
			);
		$QAppAir->update($data_update,$where_update);
		$db->commit();
		$flashMessenger->setNamespace('success')->addMessage('Upload ảnh design thành công!');
		$this->redirect(HOST.'trade/edit-air?id='.$id);

	}catch (Exception $e)
	{
		$db->rollBack();
	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Vui lòng chọn file upload!! ");
    	$this->redirect(HOST.'trade/edit-air?id='.$id);
	}
				
				
}elseif($status['status']==8){
	if(!empty($contructor)){	
		try{
			//số lượng đề xuất
			$soluong_dx=$QAppAir->getSoluongdexuat($id);
			$quanti=intval($soluong_dx[0]['quanti']);
			//số lượng chia
			$soluong_chia=0;
			foreach ($air_detail_id as $key=> $value){
				$data_ = array(
					'air_detail_id' => $air_detail_id[$key],
					'contructor_id' => $contructor[$key],
					'quantity' 		=> $quantity_nhathau[$key],
					);
				$soluong_chia+=$quantity_nhathau[$key];
				$QAirContructor->insert($data_);
			}
			if($soluong_chia==$quanti){
				$db->commit();
				$where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
				$data_update=array(
					'status'=>2
				);
				$QAppAir->update($data_update,$where_update);
				$flashMessenger->setNamespace('success')->addMessage('Chia nhà thầu thành công!');
				$this->redirect(HOST.'trade/chia-nha-thau?id='.$id);
			
			}elseif($soluong_chia>$quanti){
				$db->rollBack();
				$flashMessenger->setNamespace('error')->addMessage( "Chia nhà thầu vượt quá số lượng!!");
				$this->redirect(HOST.'trade/chia-nha-thau?id='.$id);
			}else{
                $db->rollBack();
                $flashMessenger->setNamespace('error')->addMessage( "Chia nhà thầu chưa đủ số lượng!!");
                $this->redirect(HOST.'trade/chia-nha-thau?id='.$id);
            }
		}catch (Exception $e)
			{
				$db->rollBack();
				$flashMessenger->setNamespace('error')->addMessage( "Chia nhà thầu thất bại!! ".$e->getMessage() );
				$this->redirect(HOST.'trade/chia-nha-thau?id='.$id);
			}
	}else{
		$db->rollBack();
		$flashMessenger->setNamespace('error')->addMessage( "Chia nhà thầu thất bại!! ".$e->getMessage() );
		$this->redirect(HOST.'trade/chia-nha-thau?id='.$id);
	}		

}
elseif ($status['status']==2) {
	if(!empty($submit) && !empty($_FILES['files_baogia']['name']))
	{
		try
		{
			// $upload = new Zend_File_Transfer();
			// $upload->addValidator('FilesSize',false,array('max' => '10MB'));
			// if (!$upload->isValid()) {

	  //   		$db->rollBack();
			//     $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Dung lượng file vượt mức cho phép !");
		 //    	  $this->redirect(HOST.'trade/edit-air?id='.$id);
			// }

			$totals = count($_FILES['files_baogia']['name']);
			for($i=0; $i<$totals; $i++){
			    if($_FILES['files_baogia']['name'][$i]!=NULL){
					$where_del_quo= array();
					$where_del_quo[]  = $QAppQuotation->getAdapter()->quoteInto('air_id = ?',$id);
					$where_del_quo[]  = $QAppQuotation->getAdapter()->quoteInto('contractor_id = ?',$id_contractor);
					$where_del_quo[]  = $QAppQuotation->getAdapter()->quoteInto('category_id = ?',$hangmuc_id[$i]);
					$QAppQuotation -> delete($where_del_quo);
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
									DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
									DIRECTORY_SEPARATOR . 'excel' . DIRECTORY_SEPARATOR . $id;
					if (!is_dir($uploaded_dir))
					@mkdir($uploaded_dir, 0777, true);
					$tmpFilePath = $_FILES['files_baogia']['tmp_name'][$i];
					if ($tmpFilePath != ""){
						$old_name 	= $_FILES['files_baogia']['name'][$i];
						$tExplode 	= explode('.', $old_name);
						$extension  = end($tExplode);
						$new_name = 'QUOTATION-' . md5(uniqid('', true)) .'.' . $extension;
						$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
						if(move_uploaded_file($tmpFilePath, $newFilePath)) {
							$url= 'files' .DIRECTORY_SEPARATOR.'excel'.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$new_name;
						}else{
							$url = NULL;
						}
					}else{
						$url = NULL;
					}
				$details = [
					'air_id' => $id,
					'url'  => $url,
					'type'	   => 4,
					'file_name'=>$new_name,
					'contructor_id'=>$id_contractor
					];
				$QAppAirFile->insert($details);
				//đọc file excel
				$inputfile = $url;
				$xlsx = new SimpleXLSX($inputfile);
				$data = $xlsx->rows();
				$quotation =array();
				$created_at=date('Y-m-d H:i:s');
				foreach ($data as $key => $value) {
					$key_ = $key+2;
					if($data[$key_][13]!=0){
						$d=0;
						if(!empty($data[$key_][8]))
						{
							$d=$data[$key_][8];
						}
						$quotation = [
							'category_id' => $hangmuc_id[$i],
							'quotation_cat_id' => $data[$key_][2],	
							'horizontal'=>$data[$key_][6],
							'height'=>$data[$key_][7],
							'deep'=>$d,
							'total_dt'=>$data[$key_][9],
							'quantity'=>$data[$key_][11],
							'price'=>$data[$key_][12],
							'total_price'=>$data[$key_][13],
							'created_at'=>$created_at,
							'air_id'=>$id,
							'contractor_id'=>$id_contractor
						];

						$QAppQuotation->insert($quotation);
					}	
				}
			   }
			
			}
			
			//cập nhật status khi approve
			$chia_nha_thau=$QAppAir->getChiaNhaThau($id);
			$this->view->chia_nha_thau=$chia_nha_thau;
			//$this->view->chia_nha_thau=$chia_nha_thau;
			$nhathau_upload=$QAppAir->getNhaThauUpload($id);
			$this->view->nhathau_upload=$nhathau_upload;
			if($nhathau_upload==$chia_nha_thau){
				$where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
				$data_update=array(
					'status' => 3
				);
			$QAppAir->update($data_update,$where_update);
			}
			$flashMessenger->setNamespace('success')->addMessage('Báo giá thành công!');
			$db->commit();
	    	$this->redirect($back_url);
		}catch (Exception $e)
		{
			$db->rollBack();
		    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
	    	$this->redirect($back_url);
		}
	}else{
		$db->rollBack();
		$flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Upload báo giá thất bại!");
	    $this->redirect(HOST.'trade/edit-air?id='.$id);
	}
}elseif ($status['status']==3 && $title==199) {
	if(!empty($submit) && !empty($_FILES['files_baogia']['name']))
	{
		try
		{
			// $upload = new Zend_File_Transfer();
		
			// $upload->addValidator('FilesSize',false,array('max' => '10MB'));
			
			// if (!$upload->isValid()) {

	  //   		$db->rollBack();
			//     $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Dung lượng file vượt mức cho phép !");
		 //    	$this->redirect(HOST.'trade/edit-air?id='.$id);
			// }
			$totals = count($_FILES['files_baogia']['name']);
			for($i=0; $i<$totals; $i++){
				if($_FILES['files_baogia']['name'][$i]!=NULL){
					$where_del_quo= array();
					$where_del_quo[]  = $QAppQuotation->getAdapter()->quoteInto('air_id = ?',$id);
					$where_del_quo[]  = $QAppQuotation->getAdapter()->quoteInto('contractor_id = ?',$id_contractor);
					$where_del_quo[]  = $QAppQuotation->getAdapter()->quoteInto('category_id = ?',$hangmuc_id[$i]);
					$QAppQuotation->delete($where_del_quo);
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
									DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
									DIRECTORY_SEPARATOR . 'excel' . DIRECTORY_SEPARATOR . $id;
					if (!is_dir($uploaded_dir))
					@mkdir($uploaded_dir, 0777, true);
					$tmpFilePath = $_FILES['files_baogia']['tmp_name'][$i];
					if ($tmpFilePath != ""){
						$old_name 	= $_FILES['files_baogia']['name'][$i];
						$tExplode 	= explode('.', $old_name);
						$extension  = end($tExplode);
						$new_name = 'QUOTATION-' . md5(uniqid('', true)) .'.' . $extension;
						$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
						if(move_uploaded_file($tmpFilePath, $newFilePath)) {
							$url= 'files' .DIRECTORY_SEPARATOR.'excel'.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$new_name;
						}else{
							$url = NULL;
						}
					}else{
						$url = NULL;
					}
				$details = [
					'air_id' => $id,
					'url'  => $url,
					'type'	   => 4,
					'file_name'=>$new_name,
					'contructor_id'=>$id_contractor
				];
				$QAppAirFile->insert($details);
				//đọc file excel
				$inputfile = $url;
				$xlsx = new SimpleXLSX($inputfile);
				$data = $xlsx->rows();
				$quotation =array();
				$created_at=date('Y-m-d H:i:s');
				foreach ($data as $key => $value) {
					$key_ = $key+2;
					if($data[$key_][13]!=0){
						$d=0;
						if(!empty($data[$key_][8])){
							$d=$data[$key_][8];
						}
						$quotation = [
							'category_id' => $hangmuc_id[$i],
							'quotation_cat_id' => $data[$key_][2],	
							'horizontal'=>$data[$key_][6],
							'height'=>$data[$key_][7],
							'deep'=>$d,
							'total_dt'=>$data[$key_][9],
							'quantity'=>$data[$key_][11],
							'price'=>$data[$key_][12],
							'total_price'=>$data[$key_][13],
							'created_at'=>$created_at,
							'air_id'=>$id,
							'contractor_id'=>$id_contractor
						];
						$QAppQuotation->insert($quotation);
					}	
				}
			   }
			
			}
			$flashMessenger->setNamespace('success')->addMessage('Báo giá thành công!');
			$db->commit();
	    	$this->redirect($back_url);
		}catch (Exception $e)
		{
			$db->rollBack();
		    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
	    	$this->redirect($back_url);
		}
	}else{
			$db->rollBack();
		    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Upload báo giá thất bại!");
	    	$this->redirect(HOST.'trade/edit-air?id='.$id);
	}
}
elseif($status['status']==9){
	if(!empty($submit)){
	 if(!empty($horizontal) && !empty($height) && !empty($deep) && !empty($total_dt_edit) && !empty($quantity) && !empty($price) && !empty($total_price)){
		$created_at=date('Y-m-d H:i:s');
		foreach ($quotation_id as $key=> $value){
			$data_quo =array(
				'quotation_cat_id'	=> $quotation_cat_id[$key],
				'horizontal'		=> $horizontal[$key],
				'height'  			=> $height[$key],
				'deep'				=> $deep[$key],
				'total_dt'			=> $total_dt_edit[$key],
				'quantity'			=> $quantity[$key],
				'price'				=> $price[$key],
				'total_price'		=> $total_price[$key],
				'created_at' 		=> $created_at,
				'air_id'			=> $id,
				'contractor_id' 	=> $contractor_id[$key],
				'category_id'		=> $cate_quo_id[$key]
			);
			$where_quo=array();
			$where_quo[]  = $QAppQuotation->getAdapter()->quoteInto('air_id = ?',$id);
			$where_quo[]  = $QAppQuotation->getAdapter()->quoteInto('contractor_id = ?',$id_contractor);
			$where_quo[]  = $QAppQuotation->getAdapter()->quoteInto('quotation_cat_id = ?',$quotation_cat_id[$key]);
			$where_quo[]  = $QAppQuotation->getAdapter()->quoteInto('category_id = ?',$cate_quo_id[$key]);	
					//var_dump($where_quo); exit;		
			$QAppQuotation->update($data_quo,$where_quo);
		}	
	}
		//cập nhật step trong app_contructor
	$where_xacnhan = $QAirContructor->getAdapter()->quoteInto('contructor_id = ?', $id_contractor);
	$data_xacnhan=array(
		'step'      =>1
	);
	$QAirContructor->update($data_xacnhan,$where_xacnhan);
	$db->commit();
	$nhathau_xacnhan=$QAppAir->getNhathauXacNhan($id);
	$this->view->nhathau_xacnhan=$nhathau_xacnhan;
	//$this->view->chia_nha_thau=$chia_nha_thau;
	$nhathau_upload=$QAppAir->getNhaThauUpload($id);
	$this->view->nhathau_upload=$nhathau_upload;
	//var_dump($nhathau_xacnhan); exit;
	if($nhathau_xacnhan==$nhathau_upload){
			$where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
			$data_update=array(
				'status'      =>6
			);
			$QAppAir->update($data_update,$where_update);
	}
		
	$flashMessenger->setNamespace('success')->addMessage('Xác nhận báo giá thành công!');
	$this->redirect($back_url);
	}else{
		$this->redirect($back_url);
  }
	
}
elseif($status['status']==6 && $title==199 ){
	if(!empty($submit)){
		if(!empty($horizontal) && !empty($height) && !empty($total_dt_edit) && !empty($quantity) && !empty($price) && !empty($total_price))
		{
			$created_at=date('Y-m-d H:i:s');
			foreach ($quotation_id as $key=> $value) {	
					if(!empty($edit[$key]))
					{
							$data_quo =array(
						    'quotation_cat_id'	=>$quotation_cat_id[$key],
						    'horizontal'		=>$horizontal[$key],
						    'height'  			=> $height[$key],
						    'deep'				=>$deep[$key],
						    'total_dt'			=>$total_dt_edit[$key],
						    'quantity'			=>$quantity[$key],
						    'price'				=>$price[$key],
						    'total_price'		=> $total_price[$key],
						    'created_at' 		=> $created_at,
						    'air_id'			=> $id,
						    'contractor_id' 	=> $contractor_id[$key],
						    'category_id'		=> $cate_quo_id[$key]
						);
						$where_quo=array();
						$where_quo[]  = $QAppQuotation->getAdapter()->quoteInto('edit_id = ?', $edit[$key]);	

						$QAppQuotation->update($data_quo,$where_quo);
					}else{
						
						$insert = $QAppQuotation->insert(array(
						    'quotation_cat_id'	=>$quotation_cat_id[$key],
						    'horizontal'		=>$horizontal[$key],
						    'height'  			=> $height[$key],
						    'deep'				=>$deep[$key],
						    'total_dt'			=>$total_dt_edit[$key],
						    'quantity'			=>$quantity[$key],
						    'price'				=>$price[$key],
						    'total_price'		=> $total_price[$key],
						    'created_at' 		=> $created_at,
						    'air_id'			=> $id,
						    'contractor_id' 	=> $contractor_id[$key],
						    'edit_id'			=> $value,
						    'category_id'		=> $cate_quo_id[$key]
						));	
				}
				$db->commit();
			 }
				$flashMessenger->setNamespace('success')->addMessage('Xác nhận báo giá thành công!');
				$this->redirect($back_url);
			}else
			{
				$this->redirect($back_url);
			}
	}
}
elseif($status['status']==3){
	
	if(!empty($submit)){
		    $where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
				 $data_update=array(
					'status'      =>4
			        );
				$QAppAir->update($data_update,$where_update);
				$db->commit();
				$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công!');
				$this->redirect($back_url);
			}else
			{
				$this->redirect($back_url);
			}
	
}
elseif($status['status']==4){
	            $where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
				  $data_update=array(
					'status'      =>5
			        );
				$QAppAir->update($data_update,$where_update);
				$db->commit();
				$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công!');
				$this->redirect($back_url);
}
elseif($status['status']==5){
	
		try{
			$upload = new Zend_File_Transfer();
		
			$upload->addValidator('Size',false,array('max' => '13MB'));
			//$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
			if (!$upload->isValid()) {

	    		$db->rollBack();
			    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Dung lượng file vượt mức cho phép.. !");
		    	  $this->redirect(HOST.'trade/edit-air?id='.$id);
			}

			if(!empty($_FILES['file']['name']))
			{
				$total = count($_FILES['file']['name']);
				for($i=0; $i<$total; $i++) {

					//Upload hình ?nh
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
								DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
								DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $id;

					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);

					$tmpFilePath = $_FILES['file']['tmp_name'][$i];

					if ($tmpFilePath != ""){
						$old_name 	= $_FILES['file']['name'][$i];
						$tExplode 	= explode('.', $old_name);
						$extension  = end($tExplode);
						$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

						$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;

						if(move_uploaded_file($tmpFilePath, $newFilePath)) {
							$url         = 'photo' .
								DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
						}
						else{
							$url = NULL;
						}
					}
					else{
						$url = NULL;
					}
				$details = [
					'air_id' => $id,
					'url'  => $url,
					'type'	   => 3,
					'file_name'=>$new_name,
					'contructor_id'=>0
				];
				$QAppAirFile->insert($details);
				//cập nhật status khi approve
				$where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
				$data_update=array(
					'status'      =>9
			    );
				$QAppAir->update($data_update,$where_update);
				}
				$db->commit();
			    $flashMessenger->setNamespace('success')->addMessage('Upload ảnh nghiệm thu thành công!');
			    $this->redirect($back_url);
		}else{
			$db->rollBack();
			$flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Upload ảnh nghiệm thu thất bại!");
		    $this->redirect(HOST.'trade/edit-air?id='.$id);
		}
	}catch (Exception $e){
		$db->rollBack();
		$flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Upload ảnh nghiệm thu thất bại!");
	    $this->redirect(HOST.'trade/edit-air?id='.$id);
	}
	
}elseif($status['status']==6){
	if(!empty('submit')){
		$where_update = $QAppAir->getAdapter()->quoteInto('id = ?', $id);
		$data_update=array(
			'status'      =>7
		);
		$QAppAir->update($data_update,$where_update);
		$db->commit();
		$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công!');
		$this->redirect($back_url);
	}
}
	// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
?>

