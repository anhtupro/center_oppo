<?php 

	$category_id    = $this->getRequest()->getParam('category_id');
	$area_id    	= $this->getRequest()->getParam('area_id');
	$list_imei    	= $this->getRequest()->getParam('list_imei');

	$QAppContractorOut 	= new Application_Model_AppContractorOut();
	$QAppAreaIn 		= new Application_Model_AppAreaIn();

	$list_imei = explode(',', rtrim($list_imei));

	$db = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{
		foreach ($list_imei as $key => $value) {

			$imei_sn = RTRIM(LTRIM($value));

			if($imei_sn != ''){

				$check_imei_area = $QAppAreaIn->checkImeiArea($imei_sn);
				
				if(!$check_imei_area){
					echo 'Imei này chưa được Scan xuất kho NSX';exit;
				}

				if($check_imei_area['area_id'] != $area_id){
					echo 'Sai khu vực.';exit;
				}

				if(!empty($check_imei_area['imei_in_area'])){
					echo 'Imei đã được Scan nhập kho khu vực.';exit;
				}

				$data = [
					'area_id' 		=> $check_imei_area['area_id'],
					'campaign_id'	=> $check_imei_area['campaign_id'],
					'category_id'	=> $check_imei_area['category_id'],
					'imei_sn'		=> $check_imei_area['imei_sn'],
					'created_at'	=> date('Y-m-d H:i:s'),
					'created_by'	=> $this->storage['staff_id']
				];

				$QAppAreaIn->insert($data);

				//Cập nhật lại status app_order_details
				$db = Zend_Registry::get('db');
                $sql = "UPDATE `".DATABASE_TRADE."`.`app_order_details` p
						LEFT JOIN `".DATABASE_TRADE."`.`app_order` o ON o.id = p.order_id
						LEFT JOIN store s ON s.id = p.store_id
						LEFT JOIN regional_market r ON r.id = s.regional_market
						SET p.`status` = 9
						WHERE p.`status` = 8 AND o.campaign_id = :campaign_id AND p.category_id = :category_id AND r.area_id = :area_id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('campaign_id', $check_imei_area['campaign_id'], PDO::PARAM_INT);
                $stmt->bindParam('category_id', $check_imei_area['category_id'], PDO::PARAM_INT);
                $stmt->bindParam('area_id', $area_id, PDO::PARAM_INT);
                $stmt->execute();

                $stmt->closeCursor();
				//END Cập nhật lại status app_order_details

			}
				
		}

		$db->commit();
		echo 'success';exit;

	}
	catch (Exception $e)
	{
		$db->rollBack();

	    echo "Có lỗi: ".$e->getMessage();
	    exit;
	}




