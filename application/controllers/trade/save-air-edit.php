<?php

require_once "Aws_s3.php";
$s3_lib = new Aws_s3();

$air_id = $this->getRequest()->getParam('air_id');
$air_details_id = $this->getRequest()->getParam('air_details_id');
$contructors_id = $this->getRequest()->getParam('contructors_id');

$category_id = $this->getRequest()->getParam('category_id');
$width_details = $this->getRequest()->getParam('width_details');
$high_details = $this->getRequest()->getParam('high_details');
$material_details = $this->getRequest()->getParam('material_details');
$location_details = $this->getRequest()->getParam('location_details');


$price = $this->getRequest()->getParam('price');
$final_price = $this->getRequest()->getParam('final_price');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');

$air_date = $this->getRequest()->getParam('air_date');
$image = $this->getRequest()->getParam('image');

$part = $this->getRequest()->getParam('part');
$part_quantity = $this->getRequest()->getParam('part_quantity');

$width = $this->getRequest()->getParam('width');
$wide = $this->getRequest()->getParam('wide');
$deep = $this->getRequest()->getParam('deep');
$total = $this->getRequest()->getParam('total');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$air_quotation_title = $this->getRequest()->getParam('air_quotation_title');
$air_quotation_quantity = $this->getRequest()->getParam('air_quotation_quantity');
$air_quotation_total_price = $this->getRequest()->getParam('air_quotation_total_price');

$contract_number = $this->getRequest()->getParam('contract_number');
$review_cost = $this->getRequest()->getParam('review_cost');
$review_note = $this->getRequest()->getParam('review_note');
$month_debt = $this->getRequest()->getParam('month_debt');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger       = $this->_helper->flashMessenger;

$QAir = new Application_Model_AppAir();
$QAirStatus = new Application_Model_AirStatus();
$QAirDetails = new Application_Model_AirDetails();
$QAirDetailsFile = new Application_Model_AirDetailsFile();
$QAirDetailsPart = new Application_Model_RepairDetailsPart();
$QRepairConfirm = new Application_Model_RepairConfirm();

$QAirMapProduct = new Application_Model_AirMapProduct();
$QAirProduct = new Application_Model_AirProduct();
$QAirProductImage = new Application_Model_AirProductImage();

$QAirQuotation = new Application_Model_AirQuotation();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();

$where_air = $QAirDetails->getAdapter()->quoteInto('id = ?', $air_id);
$air = $QAir->fetchRow($where_air);
$air_details = $QAirDetails->fetchAll(['air_id = ?' => $air_id])->toArray();



$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    if ($remove) {
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QAir->update($remove_date, $where_air);

        // notify
        $area_id = $QArea->getAreaByStore($air['store_id']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(8, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất THI CÔNG bị hủy",
                'link' => "/trade/air-edit?id=" . $air_id,
                'staff_id' => $staff_notify
            ];
//            $result = $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/air-edit?id=' . $air_id);
    }
    //Reject
    if($reject){
        $QAir->update([
            'reject' => 1,
            'status' => $air['status'] - 1,
            'reject_by' => $userStorage->id,
            'reject_note' => $reject_note
        ], $where_air);

        $staff_notify = $air['status'] - 1;

        // notify
        $area_id = $QArea->getAreaByStore($air['store_id']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(8, $staff_notify, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất THI CÔNG bị từ chối",
                'link' => "/trade/air-edit?id=" . $air_id,
                'staff_id' => $staff_notify
            ];
//            $result = $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);
    }
    //END Reject
    
    
    //1: Chờ trade leader xác nhận
    if($air['status'] == 1){
        $QAir->update(['status' => 3], $where_air);

        $QAirStatus->insert([
            'air_id' => $air_id,
            'status_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id']
        ]);

        $status_notify = 3;

    }
    
    //2: Chờ  ASM  xác nhận
//    if($air['status'] == 2){
//        $QAir->update(['status' => 3], $where_air);
//
//        $QAirStatus->insert([
//            'air_id' => $air_id,
//            'status_id' => 3,
//            'created_at' => date('Y-m-d H:i:s'),
//            'created_by' => $this->storage['staff_id']
//        ]);
//
//        $status_notify = 3;
//
//    }
    

    //3: Chờ trade local  nghiệm thu
    if($air['status'] == 3){
        
     $tag = 0;
        
        foreach($image as $key=>$value){
            
            foreach($value as $k=>$v){
                $data = $v;
                
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $key;

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);

                $url = '/photo/air_file/3/' . $key . '/image_'.$k.'.png';

                $new_name = 'image_'.$k.'.png';

                $file = $uploaded_dir . '/' . $new_name;

                $success_upload = file_put_contents($file, $data);

                // upload image to server s3
                $destination_s3 = 'photo/air_file/3/' . $key . '/';

                if ($success_upload) {
                    $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $new_name);
                }
                // upload image to server s3

                $rdf_insert = [
                    'air_details_id' => $key,
                    'url'    => $url,
                    'type'   => 3
                ];
                $QAirDetailsFile->insert($rdf_insert);
                
                $tag = 1;
            }
        }
        foreach($air_details_id as $key=>$value){
            $product = [
                'category_id' => $category_id[$value],
                'width'       => $width[$value],
                'high'        => $wide[$value],
                'material_id' => null,
                'product_type_id' => null,
                'location'        => null,
                'mica_id'         => null
            ];
            $product_id = $QAirProduct->insert($product);

            $air_map_product = [
                'air_id' => $air_id,
                'product_id' => $product_id
            ];
            $QAirMapProduct->insert($air_map_product);

            foreach ($width_details[$value] as $key => $val) {
                $product_image = [
                    'product_id' => $product_id,
                    'width'      => $val,
                    'high'       => $high_details[$value][$key],
                    'material_id'   => $material_details[$value][$key],
                    'location'   => $location_details[$value][$key]
                ];

                $QAirProductImage->insert($product_image);
            }

            $where = $QAirDetails->getAdapter()->quoteInto('id = ?', $value);
            // lưu edit chiều dài rộng nganng diện tích

            //Upload file nghiệm thu
            foreach($_FILES['files']['name'][$value] as $k=>$v){

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '6' . DIRECTORY_SEPARATOR . $value;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['files']['tmp_name'][$value][$k];

                $old_name = $v;
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    // upload image to server s3
                    $destination_s3 = 'photo/air_file/6/' . $value . '/';
                    $upload_s3 = $s3_lib->uploadS3($newFilePath, $destination_s3, $new_name);
                    // upload image to server s3


                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '6' . DIRECTORY_SEPARATOR . $value . DIRECTORY_SEPARATOR . $new_name;

                    $dtf = [
                        'air_details_id' => $value,
                        'url'   => '/'.$url,
                        'type' => 6
                    ];
                    $QAirDetailsFile->insert($dtf);
                }

            }
   
            //Upload giá cuối cùng
            $where = [];
            $where[] = $QAirQuotation->getAdapter()->quoteInto('air_details_id = ?', $value);
            $where[] = $QAirQuotation->getAdapter()->quoteInto('status = ?', 2);
            $QAirQuotation->update(['status' => 3], $where);

            foreach ($air_quotation_title [$value] as $index => $title) {
                $data_quotation = [
                    'air_details_id' => $value,
                    'title' => $title,
                    'quantity' => $air_quotation_quantity [$value] [$index] ? $air_quotation_quantity [$value] [$index] : Null,
                    'total_price' => $air_quotation_total_price [$value] [$index] ? $air_quotation_total_price [$value] [$index] : Null,
                    'status' => 2
                ];
                $QAirQuotation->insert($data_quotation);
            }

            // update contract number
            $QAirDetails->update([
                'contract_number' => $contract_number[$value] ? TRIM($contract_number[$value]) : ''
            ], ['id = ?' => $value]);

        }
        if($tag == 1){
            $QAir->update(['status' => 4], $where_air);

            $QAirStatus->insert([
                'air_id' => $air_id,
                'status_id' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->storage['staff_id']
            ]);

            $status_notify = 4;

        }
        else{
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload file hình ảnh nghiệm thu.');
            $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);
        }
        
    }

    //4:chờ Trade Leader xác nhận ngiệm thu
     if($air['status'] == 4){
         // chuyển vào shop
         $status_code = $QAirDetails->transferToShop($air['store_id'], $air_details);

         if ($status_code['code'] == 2) {
             $flashMessenger->setNamespace('error')->addMessage($status_code['message']);
             $this->_redirect(HOST . 'trade/air-edit?id=' . $air_id);
         }

         $QAir->update(['status' => 5], $where_air);
         $QAirStatus->insert([
            'air_id' => $air_id,
            'status_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id']
        ]);

         $status_notify = 5;

     }

    if($air['status'] == 5){
        foreach ($air_details_id as $key => $value) {
            if ($review_cost[$value] == 2) { // chi phí sai
                // Insert quotation
                $where = [];
                $where[] = $QAirQuotation->getAdapter()->quoteInto('air_details_id = ?', $value);
                $where[] = $QAirQuotation->getAdapter()->quoteInto('status = ?', 4);
                $QAirQuotation->update(['status' => 5], $where);

                    foreach ($air_quotation_title [$value] as $index => $title) {
                        $data_quotation = [
                            'air_details_id' => $value,
                            'title' => $title,
                            'quantity' => $air_quotation_quantity [$value] [$index] ? $air_quotation_quantity [$value] [$index] : Null,
                            'total_price' => $air_quotation_total_price [$value] [$index] ? $air_quotation_total_price [$value] [$index] : Null,
                            'status' => 4
                        ];
                        $QAirQuotation->insert($data_quotation);
                    }

            }

            if ($review_cost[$value] == 1) { // chi phí chính xác
                $where = [];
                $where[] = $QAirQuotation->getAdapter()->quoteInto('air_details_id = ?', $value);
                $where[] = $QAirQuotation->getAdapter()->quoteInto('status = ?', 2);

                $data_insert_quotation = $QAirQuotation->fetchAll($where);

                foreach ($data_insert_quotation as $quotation) {
                    $QAirQuotation->insert([
                        'air_details_id' => $quotation['air_details_id'] ? $quotation['air_details_id'] : Null,
                        'title' => $quotation['title'] ? $quotation['title'] : Null,
                        'quantity' => $quotation['quantity'] ? $quotation['quantity'] : Null,
                        'total_price' => $quotation['total_price'] ? $quotation['total_price'] : Null,
                        'status' => 4
                    ]);
                }
            }

            $QAirDetails->update([
                'review_cost' => $review_cost[$value],
                'review_note' => $review_note[$value],
                'month_debt' => $month_debt[$value],
                'contract_number' => $contract_number[$value] ? TRIM($contract_number[$value]) : ''
            ], ['id = ?' => $value]);
        }

        $QAir->update(['status' => 6], $where_air);

        $status_notify = 6;
    }


    $QAir->update(['reject' => 0], $where_air);

    // notify
    $area_id = $QArea->getAreaByStore($air['store_id']);
    $list_staff_notify = $QAppStatusTitle->getStaffToNotify(8, $status_notify, $area_id);


    foreach ($list_staff_notify as $staff_notify) {
        $data_noti = [
            'title' => "TRADE MARKETING OPPO VIETNAM",
            'message' => "Có một đề xuất THI CÔNG đang chờ bạn duyệt",
            'link' => "/trade/air-edit?id=" . $air_id,
            'staff_id' => $staff_notify
        ];
//        $result = $QAppNoti->sendNotification($data_noti);
    }


    $db->commit();




    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);
    

} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
    $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);

}

?>