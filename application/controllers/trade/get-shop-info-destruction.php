<?php
$storeId = $this->getRequest()->getparam('shop_id');

$QStore = new Application_Model_Store();
$QCheckShopDetail = new Application_Model_AppCheckshopDetail();
$QCheckShopFile = new Application_Model_AppCheckshopFile();

$where = $QStore->getAdapter()->quoteInto("id = ?", $storeId);
$store = $QStore->fetchRow($where);

$response = [
    'store_name' => $store['name'],
    'category' => [],
    'check_shop_image' => []
];

try {
    $checkShopDetails = $QCheckShopDetail->getDataCheckshop(['store_id' => $storeId]);
    $checkShopFiles = $QCheckShopFile->getFileCheckshop(['store_id' => $storeId]);
    if (count($checkShopDetails) == 0 && count($checkShopFiles) == 0) {
        throw new Zend_Exception('Shop haven\'t been check before');
    }
} catch (Zend_Exception $ex) {
    echo json_encode($response);
    exit;
}

foreach ($checkShopDetails as $checkShopDetail) {
    $response['category'][] = [
        'category_name' => $checkShopDetail['category_name'],
        'category_id' => $checkShopDetail['category_id'],
        'quantity' => $checkShopDetail['quantity'],
        'imei_sn' => $checkShopDetail['imei_sn'],
        'group_bi' => $checkShopDetail['group_bi'],
    ];
}

foreach ($checkShopFiles as $checkShopFile) {
    $response['check_shop_image'][] = [
        'image_url' => $checkShopFile['url'],
        'type' => $checkShopFile['type']
    ];
}

echo json_encode($response);
exit;