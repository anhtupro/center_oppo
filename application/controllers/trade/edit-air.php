<?php

$category_id   = $this->getRequest()->getParam('category');

$ngang = $this->getRequest()->getParam('ngang');
$cao = $this->getRequest()->getParam('cao');
$sau = $this->getRequest()->getParam('sau');
$tongdt = $this->getRequest()->getParam('tongdt');

$dem=$this->getRequest()->getParam('dem');
$store_id   = $this->getRequest()->getParam('store');

$id = $this->getRequest()->getParam('id');
$cate_quotation = $this->getRequest()->getParam('baogia');
$unit = $this->getRequest()->getParam('unit');
$soluong = $this->getRequest()->getParam('soluong');
$dongia = $this->getRequest()->getParam('dongia');
$tongtien= $this->getRequest()->getParam('tongtien');



$submit	    = $this->getRequest()->getParam('submit');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QCampaign = new Application_Model_Campaign();
$QAppAir = new Application_Model_AppAir();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QAppAirFile= new Application_Model_AppAirFile();
$QAppQuotation = new Application_Model_AppQuotation();
$QAppQuotationCat = new Application_Model_AppQuotationCat();
$QAppStatusTitle = new Application_Model_AppStatusTitle();

$cache = $QAppStatusTitle->get_cache(7);//GET cache lưu vào file  riêng

$flashMessenger       = $this->_helper->flashMessenger;
$title = $userStorage->title;
$id_user=$userStorage->id;
$params = array(
	'id' => $id,
	'title'=>$title,
	'id_user'=>$id_user
);

$back_url = HOST.'trade/create-air';
//lấy số lượng nhà thầu thi công của 1 air
$chia_nha_thau=$QAppAir->getChiaNhaThau($id);
$this->view->chia_nha_thau=$chia_nha_thau;
$nhathau_upload=$QAppAir->getNhaThauUpload($id);
$this->view->nhathau_upload=$nhathau_upload;

//lấy ds quotation bị thay đổi
$change_quotation= $QAppAir->getChangeQuotation($id);
$this->view->change=$change_quotation;
// lấy status
 $status=$QAppAir->getStatus($id);
 // lấy file đề xuất
$file_dx=$QAppAir->getUrlFile($id);
//lấy file báo giá
$file_quotation=$QAppAir->getUrlFileQuotation($id);
//lấy ảnh design
 $img=$QAppAirFile->getImg($id);
 //lấy số nhà thầu 1 app_air trong bảng báo giá
$count_contructor=$QAppAir->getCountnhathau($id);

$this->view->count_contructor=$count_contructor;
 //lấy thông tin báo giá
 $quotation=$QAppQuotation->getQuotation($params);

$quotation_data = [];
foreach($quotation as $key=>$value){
	$quotation_data[$value['category_id']][$value['contractor_id']][] = $value;
}


//lấy ảnh nghiệm thu
 $results=$QAppAirFile->getImgResult($id);

$list=$QAppAir->getDSDeXuat($params);

if(isset($params['title']) and $params['title']==199){
	$list=$QAppAir->getDSDeXuatNhathau($params);
}

//lấy thông tin shop đề xuất
$shop=$QAppAir->getShopDeXuat($id);

	// -------------RETURN CATEGORY ---------------
	$result = $QCategory->fetchAll();
	
	// -------------RETURN STORE ---------------
	$paramsData['staff_id'] = $userStorage->id;
	$resultCamp = $QCampaign->getListStore($paramsData);
	
	/*Danh sách quotation phân loại*/
	// $where = $QAppQuotationCat->getAdapter()->quoteInto('parent_id % 2 = ?',0);
	// $quotation_cat=$QAppQuotationCat->fetchAll($where);
	
	/*-------------------*/
	
	$this->view->status=$status;
	$this->view->file_dx=$file_dx;
	$this->view->file_quotation=$file_quotation;
	$this->view->img=$img;
	$this->view->quotation=$quotation;
	$this->view->quotation_data=$quotation_data;
	$this->view->img_nghiemthu=$results;
	$this->view->category = $result;
	$this->view->store = $resultCamp;
	$this->view->quotation_cat=$quotation_cat;
	$this->view->list=$list;
	$this->view->shop=$shop;
	$this->view->params = $params;
	//get cache
	$this->view->title_status = $cache[$title];
	//var_dump($cache); exit;
	// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
	?>
	


