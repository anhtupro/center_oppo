<?php

$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$id = $this->getRequest()->getParam('id');


$QStandardImage = new Application_Model_StandardImage();


$image = $QStandardImage->fetchRow(['id = ?' => $id]);
$url = TRIM( $image['url'], '/');

unlink($url);

$QStandardImage->delete(['id = ?' => $id]);

echo json_encode([
    'status' => 0
]);
