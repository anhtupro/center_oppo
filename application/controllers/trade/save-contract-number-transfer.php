<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$contractNumber = $this->getRequest()->getParam('contract_number');
$transferId = $this->getRequest()->getParam('transfer_id');
$QTransfer = new Application_Model_Transfer();

$QTransfer->update([
    'contract_number' => $contractNumber ? TRIM($contractNumber) : ''
], ['id = ?' => $transferId]);

echo json_encode([
   'status' => 0,
    'contract_number' => $contractNumber
]);