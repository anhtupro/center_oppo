<?php 

	$campaign_id    = $this->getRequest()->getParam('campaign_id');
	$input_quantity = $this->getRequest()->getParam('input_quantity');
	$category_id    = $this->getRequest()->getParam('category_id');
	$area_confirm   = $this->getRequest()->getParam('area_confirm');
	$submit     	= $this->getRequest()->getParam('submit');
	$export     	= $this->getRequest()->getParam('export');
	$QCampaign 	   		= new Application_Model_Campaign();
	$QArea 		   		= new Application_Model_Area();
	$QCampaignArea 		= new Application_Model_CampaignArea();
	$QAppStatusTitle 	= new Application_Model_AppStatusTitle();
        $QAppStatus 	= new Application_Model_AppStatus();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $title = $userStorage->title;
        if(in_array($userStorage->id, [9837])){
            $title = TRADE_MARKETING_SUPERVISOR;
        }
        
	$area        = $QCampaignArea->getAreaByCampaign($campaign_id);
        
        //9837 binhsang.ly
	if(!empty($this->storage['area_id']) AND $this->storage['staff_id'] != 9837){
		$area = [];
		foreach($QCampaignArea->getAreaByCampaign($campaign_id) as $key=>$value){
			if(in_array($key, $this->storage['area_id'])){
				$area[$key] = $value;
			}
		}
	}
        
        
	$params = [
		'campaign_id' => $campaign_id,
		'area_id'	  => array_keys($area)
	];


$area_status 		= $QCampaignArea->get_status_area($params);
	$min_status  		= $QCampaignArea->get_status_min($params);
	$max_status  		= $QCampaignArea->get_status_max($params);
	$status_title  		= $QAppStatusTitle->get_cache_title(3);
	$status_title = $status_title[$title];
        
        $app_status = $QAppStatus->get_cache(3);
        $app_status_title_cache = $QAppStatusTitle->get_cache(3);
        
        $app_status_title = [];

        foreach($this->storage['title_list'] as $key=>$value){
            $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);
        }


	$where 		   = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign 	   = $QCampaign->fetchRow($where);

	$category      = $QCampaign->getCategory($params);
	$category_campaign 	= $QCampaign->getCategoryCampaign($params);

	$where 					= $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
	$campaign_area_check 	= $QCampaignArea->fetchAll($where);

	//Get data campaign_area
	$params['confirm'] = 1;
	$campaign_area = $QCampaignArea->getCampaignArea($params);

	$data = [];
	foreach($campaign_area as $key=>$value){
		$data[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data


	//Get data campaign_area
	$contractor_category = $QCampaignArea->getContractorCategory($params);
	$data_price = [];
	foreach($contractor_category as $key=>$value){
		$data_price[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data

	$flashMessenger = $this->_helper->flashMessenger;


	if(!empty($submit) AND $submit == 1){

            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            try {

                    foreach($area_confirm as $key=>$value){
                            $where = [];
                            $where[] = $QCampaignArea->getAdapter()->quoteInto('area_id = ?', $value);
                            $where[] = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
                            $campaign_area_ = $QCampaignArea->fetchRow($where);

                            $details = [
                                    'status'		=> ($min_status+1)
                            ];
                            $QCampaignArea->update($details, $where);
                    }


                    $flashMessenger->setNamespace('success')->addMessage('Done');
                    $db->commit();

                    $back_url = HOST.'trade/confirm-posm?campaign_id='.$campaign_id;
                $this->redirect($back_url);

            } catch (Exception $e) {
                    $db->rollBack();
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

                    $back_url = HOST.'trade/confirm-posm?campaign_id='.$campaign_id;
                $this->redirect($back_url);
            }

	}

if(!empty($export)){

    $params = [
			'campaign_id' => $campaign_id,
			'area_id'	  => $this->storage['area_id']
		];

    $category_campaign 	= $QCampaign->getCategoryCampaign($params);

    $area        = $QArea->get_cache_bi();

    if(!empty($this->storage['area_id'])){
        $area = [];
			foreach($QArea->get_cache_bi() as $key=>$value){
				if(in_array($key, $this->storage['area_id'])){
					$area[$key] = $value;
				}
			}
		}

    $area_status 		= $QCampaignArea->get_status_area($params);

    //Get data campaign_area
		$params['confirm'] = 1;
		$campaign_area = $QCampaignArea->getCampaignArea($params);

		$data = [];
		foreach($campaign_area as $key=>$value){
			$data[$value['area_id']][$value['category_id']] = $value;
		}
		//END Get data


		//Get data campaign_area
		$contractor_category = $QCampaignArea->getContractorCategory($params);
		$data_price = [];
		foreach($contractor_category as $key=>$value){
			$data_price[$value['area_id']][$value['category_id']] = $value;
		}

		//END Get data
		//var_dump($area); exit;
		include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
		require_once 'PHPExcel.php';
		$PHPExcel = new PHPExcel();
		$PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        $heads = array(
         	
            'KHU VỰC',
            'TRẠNG THÁI',
            'ACTION'
        );
        //header 
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        //header category
        $alpha = 'D';
         foreach ($category_campaign as $key => $value)
        {
            $sheet->setCellValue($alpha . $index, $value['name']);
            $alpha++;
        }
        //column area
       
        $index = 3;
        $total_column = [];

         foreach($area as $key=>$value){
         	 $alpha = 'A';
         	 $total_area =  0;

	         $sheet->getCell($alpha++. $index)->setValueExplicit($value,PHPExcel_Cell_DataType::TYPE_STRING);
	          $sheet->getCell($alpha++. $index)->setValueExplicit($area_status[$key]['name'],PHPExcel_Cell_DataType::TYPE_STRING);
	           $sheet->getCell($alpha++. $index)->setValueExplicit('---',PHPExcel_Cell_DataType::TYPE_STRING);

	             foreach($category_campaign as $k=>$v){

	             	 

	             	$quantity_limit = !empty($data[$key][$v['category_id']]['quantity_limit']) ? $data[$key][$v['category_id']]['quantity_limit'] : 0;
	             	$quantity_order = !empty($data[$key][$v['category_id']]['quantity']) ? $data[$key][$v['category_id']]['quantity'] : 0;
	             	$price = !empty($data_price[$key][$v['category_id']]['price']) ? $data_price[$key][$v['category_id']]['price'] : 0;

	             	$total_column[$v['category_id']][]+=$quantity_order*$price;

	            
	             $total_area +=$quantity_order*$price;
	            $sheet->getCell($alpha++ . $index)->setValueExplicit($quantity_order,PHPExcel_Cell_DataType::TYPE_NUMERIC);
	             
	             }
	            $sheet->getCell($alpha++ . $index)->setValueExplicit($total_area,PHPExcel_Cell_DataType::TYPE_NUMERIC);
	         $index++;
     	}

	 //   echo '<pre>';
	 // print_r($total_column); exit;
     	$alpha = 'D';
     	foreach ($category_campaign as $key => $value) {
     		
     		 $sheet->getCell($alpha++ . $index)->setValueExplicit(array_sum($total_column[$value['category_id']]),PHPExcel_Cell_DataType::TYPE_NUMERIC);
     	}

        $filename = ' POSM ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
		
	}



	$this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->area     		= $area;
	$this->view->campaign 		= $campaign;
	$this->view->category 		= $category;
	$this->view->category_campaign  = $category_campaign;
	$this->view->data     		= $data;
	$this->view->data_price 	= $data_price;
	$this->view->min_status 	= $min_status;
	$this->view->max_status 	= $max_status;
	$this->view->status_title 	= $status_title;
        $this->view->app_status_title 	= $app_status_title;
	$this->view->area_status 	= $area_status;
	$this->view->staff_title        = $userStorage->title;
        $this->view->area_storage       = $this->storage['area_id'];
        


