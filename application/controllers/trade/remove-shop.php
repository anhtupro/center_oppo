<?php

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore            = new Application_Model_Store();
$QRemoveShop       = new Application_Model_RemoveShop();
$QRemoveShopReason = new Application_Model_RemoveShopReason();
$QRemoveShopFile   = new Application_Model_RemoveShopFile();
$QAppStatusTitle   = new Application_Model_AppStatusTitle();
$QAppStatus   = new Application_Model_AppStatus();
$QStore   = new Application_Model_Store();
$QAppCheckshop   = new Application_Model_AppCheckshop();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();

$list_store                = $QAppNoti->getListShopByStaff();
$this->view->stores        = $list_store;
$reason_active             = $QRemoveShopReason->get_cache_active();
$this->view->reason_active = $reason_active;



$store_id = $this->getRequest()->getParam('store_id');
$reason   = $this->getRequest()->getParam('reason');
$note     = $this->getRequest()->getParam('note', null);
$submit   = $this->getRequest()->getParam('submit');
$image    = $this->getRequest()->getParam('image');
$id       = $this->getRequest()->getParam('id');
$latitude       = $this->getRequest()->getParam('latitude');
$longitude      = $this->getRequest()->getParam('longitude');

$app_status = $QAppStatus->get_cache(9);
$app_status_title = $QAppStatusTitle->get_cache(9);
$app_status_title = $app_status_title[$this->storage['title']];


if (!empty($id)) {
    $where                      = $QRemoveShop->getAdapter()->quoteInto('id = ?', $id);
    $removeShop_row             = $QRemoveShop->fetchRow($where);
    $this->view->removeShop_row = $removeShop_row;
    
    $where                      = $QRemoveShopFile->getAdapter()->quoteInto('remove_shop_id = ?', $id);
    $remove_shop_file           = $QRemoveShopFile->fetchAll($where);
    $this->view->remove_shop_file = $remove_shop_file;
    
}

if (!empty($submit)) {

    if (empty($store_id) || empty($reason)) {
        $flashMessenger->setNamespace('error')->addMessage('Vui Lòng nhập đầy đủ thông tin!.');
        $this->_redirect(HOST . 'trade/remove-shop');
    }
    
    $where_store = [];
    $where_store[] = $QStore->getAdapter()->quoteInto('id = ?', $store_id);
    $where_store[] = $QStore->getAdapter()->quoteInto('del = 1');
    $store = $QStore->fetchRow($where_store);
    if($store){
        $flashMessenger->setNamespace('error')->addMessage('Shop '.$store['name']. ' đã được đóng!');
        $this->_redirect(HOST . 'trade/remove-shop');
    }
    
    //
    $where = [];
    $where[] = $QRemoveShop->getAdapter()->quoteInto('store_id = ?', $store_id);
    $where[] = $QRemoveShop->getAdapter()->quoteInto('reject IS NULL');
    $where[] = $QRemoveShop->getAdapter()->quoteInto('status <> ?', 5);
    $remove_store = $QRemoveShop->fetchRow($where);
    if($remove_store){
        $flashMessenger->setNamespace('error')->addMessage('Shop đã được đề xuất!');
        $this->_redirect(HOST . 'trade/remove-shop');
    }
    //
    
    $where_store = $QStore->getAdapter()->quoteInto('id = ?', $store_id);
    $store = $QStore->fetchRow($where_store);
    
    $distance_gps = My_DistanceGps::getDistance($store['latitude'], $store['longitude'], $latitude, $longitude, "K");
                    
    if($distance_gps > 2){
//        $flashMessenger->setNamespace('error')->addMessage('Vị trí GPs không chính xác!.'.$distance_gps);
//        $this->_redirect(HOST . 'trade/remove-shop');
    }
    
    //Kiểm tra shop còn đầu tư thì ko cho đóng
    $params = [];
    $params['store_id'] = $store['id'];
    $app_checkshop = $QAppCheckshop->getInfoCheckshop($params);
    $quantity = 0;
    foreach($app_checkshop as $key=>$value){
        $quantity = $quantity + $value['quantity'];
    }
    
    if($quantity > 0){
        $flashMessenger->setNamespace('error')->addMessage('Đang còn đầu tư tại shop nên không thể đóng.');
        $this->_redirect(HOST . 'trade/remove-shop');
    }
    //END
    
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        $tag = 0;
        
        if (!empty($id)) {//update            
            $where          = $QRemoveShop->getAdapter()->quoteInto('id = ?', $id);
            $removeShop_row = $QRemoveShop->fetchRow($where);

            if (($removeShop_row['status'] != 1) || $removeShop_row['created_by'] != $userStorage->id) {
                $flashMessenger->setNamespace('error')->addMessage('Chỉ Edit được đề xuất chưa Confrim và chính người đề xuất mới có quyền chỉnh sửa.!');
                $this->_redirect(HOST . 'trade/remove-shop?id=' . $id);
            }

            $data_update = array(
                'store_id'   => $store_id,
                'reason'     => $reason,
                'note'       => $note,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => $userStorage->id
            );

            $QRemoveShop->update($data_update, $where);
            
            $where          = $QRemoveShopFile->getAdapter()->quoteInto('remove_shop_id = ?', $id);
            $QRemoveShopFile->delete($where);
            
            foreach($image as $k=>$v){
                
                $data = $v;
                
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'remove_shop' . DIRECTORY_SEPARATOR . $id;

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                
                $url = '/photo/remove_shop/' . $id. '/image_'.$k.'.png';

                $new_name = 'image_'.$k.'.png';

                $file = $uploaded_dir . '/' . $new_name;

                $success_upload = file_put_contents($file, $data);

                // upload image to server s3
                require_once "Aws_s3.php";
                $s3_lib = new Aws_s3();
                $destination_s3 = 'photo/remove_shop/' . $id . '/';

                if ($success_upload) {
                    $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $new_name);
                }
                // upload image to server s3
                
                $rdf_insert = [
                    'remove_shop_id' => $id,
                    'url'    => $url,
                    'type'   => 1
                ];
                $QRemoveShopFile->insert($rdf_insert);
                
                $tag = 1;
            }

            $info = "remove_shop_update_" . $id . "_" . serialize($data_update);
        } else {//insert
            
            $status = 1;
            
            //Neu ASM de xuat thi ko can duyet
            if($app_status_title[0] == 2){
                $status = 3;
            }
            
            $data_insert = array(
                'store_id'   => $store_id,
                'reason'     => $reason,
                'note'       => $note,
                'status'     => $status,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'latitude'   => $latitude,
                'longitude'  => $longitude
            );

            if ($userStorage->title == 179) {//asm
                $data_insert['status']        = 2;
                $data_insert['confirmed_by'] = $userStorage->id;
                $data_insert['confirmed_at'] = date('Y-m-d H:i:s');
            }

            $id   = $QRemoveShop->insert($data_insert);
            
            
            foreach($image as $k=>$v){
                
                $data = $v;
                
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'remove_shop' . DIRECTORY_SEPARATOR . $id;

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                
                $url = '/photo/remove_shop/' . $id. '/image_'.$k.'.png';

                $new_name = 'image_'.$k.'.png';

                $file = $uploaded_dir . '/' . $new_name;

                $success_upload = file_put_contents($file, $data);


                // upload image to server s3
                require_once "Aws_s3.php";
                $s3_lib = new Aws_s3();
                $destination_s3 = 'photo/remove_shop/' . $id . '/';

                if ($success_upload) {
                    $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $new_name);
                }
                // upload image to server s3

                
                $rdf_insert = [
                    'remove_shop_id' => $id,
                    'url'    => $url,
                    'type'   => 1
                ];
                $QRemoveShopFile->insert($rdf_insert);
                
                $tag = 1;
            }
            
            $info = "remove_shop_create_" . $id . "_" . serialize($data_insert);
        }
        
        if($tag == 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload hình ảnh !');
            $this->_redirect(HOST . 'trade/remove-shop');
        }

        // log
        $QLog = new Application_Model_Log();
        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');


        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));


        if (empty($id)) {
            $flashMessenger->setNamespace('error')->addMessage('Không thành công.');
            $this->_redirect(HOST . 'trade/remove-shop?id=' . $id);
        }


        // notify
        $area_id = $QArea->getAreaByStore($store_id);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(11, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất ĐÓNG SHOP đang chờ bạn duyệt",
                'link' => "/trade/remove-shop-detail?id=" . $id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/remove-shop?id=' . $id);
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
        $this->_redirect(HOST . 'trade/remove-shop');
    }
}

$this->view->params           = $params;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
