<?php

$stage_id = $this->getRequest()->getParam('stage_id');

$QStoreCapacityAssign = new Application_Model_StoreCapacityAssign();

$list_capacity = $QStoreCapacityAssign->getCapacity($stage_id);

$this->view->stage_id = $stage_id;
$this->view->list_capacity = $list_capacity;