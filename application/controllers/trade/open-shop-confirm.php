<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore             = new Application_Model_Store();
$QOpenShop        = new Application_Model_OpenShop();
$QOpenShopReason  = new Application_Model_OpenShopReason();
$QOpenShopConfirm = new Application_Model_OpenShopConfirm();
$QLogOnoffStore     = new Application_Model_LogOnoffStore();
$QStoreStaffLog     = new Application_Model_StoreStaffLog();
$QStoreLeaderLog    = new Application_Model_StoreLeaderLog();

$id     = $this->getRequest()->getParam('id');
$reject = $this->getRequest()->getParam('reject');

$where_open = $QOpenShop->getAdapter()->quoteInto('id = ?', $id);
$open_shop  = $QOpenShop->fetchRow($where_open);

if (!empty($id)) {

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        //Reject
        if ($reject) {
            if ($open_shop['status'] != 1) {
                $flashMessenger->setNamespace('error')->addMessage('Trạng thái ko hợp lệ. Vui lòng kiểm tra lại.!');
                $this->_redirect(HOST . 'trade/open-shop-detail?id='.$id);
            }
            
            $QOpenShop->update(['reject' => 1, 'rejected_at' => date('Y-m-d H:i:s'), 'rejected_by' => $this->storage['staff_id']], $where_open);
            $db->commit();

            $flashMessenger->setNamespace('success')->addMessage('Đã Reject Thành công!');
            $this->_redirect(HOST . 'trade/open-shop-detail?id=' . $id);
        }
        //END Reject
        
        if ($open_shop['reject'] == 1){
            $flashMessenger->setNamespace('error')->addMessage('Đề xuất này đã bị Reject trước khi bạn xác nhận.!');
            $this->_redirect(HOST . 'trade/open-shop-detail?id='.$id);
        }


        if ($open_shop['status'] == 1) {
            $QOpenShop->update(['status' => 2 , 
                'approved_by' => $this->storage['staff_id'], 
                'approved_at'=>date('Y-m-d H:i:s') ], $where_open);

            $where_store = $QStore->getAdapter()->quoteInto('id = ?', $open_shop['store_id']);
            $QStore->update(['del' => 0], $where_store);

            $log_onoff_store = [
                'store_id'  => $open_shop['store_id'],
                'type'      => 2,
                'from_date' => date('Y-m-d H:i:s')
            ];
            $QLogOnoffStore->insert($log_onoff_store);
        } else {
            $flashMessenger->setNamespace('error')->addMessage('Trạng thái ko hợp lệ. Vui lòng kiểm tra lại.!');
            $this->_redirect(HOST . 'trade/remove-shop-detail?id='.$id);
        }

        //REPAIR CONFIRM
        $rp_confirm = [
            'open_shop_id'     => $id,
            'open_shop_status' => $open_shop['status'],
            'confirm_by'         => $this->storage['staff_id'],
            'created_at'         => date('Y-m-d H:i:s')
        ];
        $QOpenShopConfirm->insert($rp_confirm);
        //END REPAIR CONFIRM

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/open-shop-detail?id=' . $id);
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
        $this->_redirect(HOST . 'trade/open-shop-detail?id=' . $id);
    }
} else {
    $flashMessenger->setNamespace('error')->addMessage('Thiếu ID truyền vào.!');
    $this->_redirect(HOST . 'trade/open-shop-detail?id=' . $id);
}

$this->view->params           = $params;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
