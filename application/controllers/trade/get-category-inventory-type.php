<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$categoryId = $this->getRequest()->getParam('category_id');

$QCategoryInventoryTypeAssign = new Application_Model_CategoryInventoryTypeAssign();

$params = [
  'category_id' => $categoryId
];

$listType = $QCategoryInventoryTypeAssign->getType($params);

echo json_encode([
   'status' => 0,
   'listType' => $listType
]);