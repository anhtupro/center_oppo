<?php

$page = $this->getRequest()->getParam('page');
$category_id = $this->getRequest()->getParam('category_id');
$area_id = $this->getRequest()->getParam('area_id');
$campaign_id = $this->getRequest()->getParam('campaign_id');
$export = $this->getRequest()->getParam('export');

$QCampaign = new Application_Model_Campaign();
$QArea = new Application_Model_Area();
$QCampaignArea = new Application_Model_CampaignArea();
$QCategory = new Application_Model_Category();

$flashMessenger = $this->_helper->flashMessenger;

$area = $QArea->get_cache();
$params = [
    'campaign_id' => $campaign_id,
    'category_id' => $category_id,
    'area_id' => $area_id,
    'area_list' => $this->storage['area_id']
];

$list_area = $QArea->getListArea($params);
$list_category = $QCategory->fetchAll(['status = ?' => 1]);

if ($export) {
    $QCampaignArea->exportAll($params);
}

$limit = LIMITATION;
$total = 0;
$campaign = $QCampaign->fetchPagination($page, $limit, $total, $params);



$this->view->offset = $limit * ($page - 1);
$this->view->campaign = $campaign;
$this->view->params = $params;
$this->view->list_area = $list_area;
$this->view->list_category = $list_category;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/list-posm/' . ($params ? '?' . http_build_query($params) .
        '&' : '?');


$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;

$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
	
