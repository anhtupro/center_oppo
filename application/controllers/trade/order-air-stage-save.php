<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$stageId = $this->getRequest()->getParam('stage_id');
$stageName = $this->getRequest()->getParam('stage_name');
$fromDate = $this->getRequest()->getParam('from_date');
$toDate = $this->getRequest()->getParam('to_date');
$listCategoryId = $this->getRequest()->getParam('category_id');
$listPrice = $this->getRequest()->getParam('price');
$listCurrency = $this->getRequest()->getParam('currency');
$is_purchasing_request = $this->getRequest()->getParam('is_purchasing_request');
$urgent_date = $this->getRequest()->getParam('urgent_date');
$purchasing_type = $this->getRequest()->getParam('purchasing_type');

$type = $this->getRequest()->getParam('type', 1);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QOrderAirStage = new Application_Model_OrderAirStage();
$QOrderAirDetail = new Application_Model_OrderAirDetail();
$QOrderAirArea = new Application_Model_OrderAirArea();
$QOrderAirAreaStatus = new Application_Model_OrderAirAreaStatus();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();
$QPurchasingRequest = new Application_Model_PurchasingRequest();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
$QPurchasingCodeCategory = new Application_Model_PurchasingCodeCategory();
$QCategory = new Application_Model_Category();

$listArea = $QArea->getListArea($params);

if ($stageId) {
    $orderAirStage = $QOrderAirStage->fetchRow(['id = ?'=> $stageId]);
    if ($orderAirStage['from_date'] <= date('Y-m-d') || $orderAirStage['purchasing_request_id']) {
        echo json_encode([
            'status' => 1,
            'message' =>  'Không thể chỉnh sửa vì đơn hàng đã chạy hoặc đã liên kết với Purchasing Request'
        ]);

        return;
    }
}



foreach ($listArea as $area) {
    $arrayAreaId [] = $area['id'];
}

$db = Zend_Registry::get('db');
$db->beginTransaction();

// purchasing request
if ($is_purchasing_request) {
    $sn = date('YmdHis') . substr(microtime(), 2, 4);

    $purchasing_request_id = $QPurchasingRequest->insert([
        'name' => $stageName,
        'urgent_date' => date('Y-m-d', strtotime(str_replace('/', '-', $urgent_date))),
        'type' => $purchasing_type,
        'area_id' => 65,
        'department_id' => $userStorage->department,
        'team_id' => $userStorage->team,
        'shipping_address' => 'Trade marketing',
        'sn' => $sn,
        'status' => 1,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id
    ]);

    foreach ($listCategoryId as $key => $category_id) {
        $category = $QCategory->fetchRow(['id = ?' => $category_id]);
        $purchasing_code_id = $QPurchasingCodeCategory->getPurchasingCode($category_id, 1);

        if ($purchasing_code_id) {
            $QPurchasingRequestDetails->insert([
                'pr_id' => $purchasing_request_id,
                'pmodel_code_id' => $purchasing_code_id,
                'quantity' => 0
            ]);
        } else {
            echo json_encode([
                'status' => 1,
                'message' =>  'Vui lòng liên kết hạng mục với Purchasing: ' . $category['name']
            ]);

            return;
        }
    }

}

//end purchasing request


if ($stageId) {
    $QOrderAirStage->update([
        'name' => $stageName,
        'from_date' => date('Y-m-d', strtotime($fromDate)),
        'to_date' => date('Y-m-d', strtotime($toDate)),
        'type' => $type,
        'purchasing_request_id' => $purchasing_request_id ? $purchasing_request_id : 0

    ], ['id = ?' => $stageId]);
} else {
    $stageId = $QOrderAirStage->insert([
        'name' => $stageName,
        'from_date' => date('Y-m-d', strtotime($fromDate)),
        'to_date' => date('Y-m-d', strtotime($toDate)),
        'type' => $type,
        'purchasing_request_id' => $purchasing_request_id ? $purchasing_request_id : 0
    ]);

    foreach ($listArea as $area) {
        $QOrderAirAreaStatus->insert([
            'order_air_stage_id' => $stageId,
            'area_id' => $area['id'],
            'status' => 1
        ]);
    }

}

$QOrderAirDetail->delete(['order_air_stage_id = ?' => $stageId]);
$QOrderAirArea->delete(['order_air_stage_id = ?' => $stageId]);

foreach ($listCategoryId as $key => $categoryId) {
    $orderAirDetailId = $QOrderAirDetail->insert([
        'order_air_stage_id' => $stageId,
        'category_id' => $categoryId,
        'price' => $listPrice[$key],
        'currency' => $listCurrency[$key]
    ]);

    foreach ($listArea as $area) {
        $QOrderAirArea->insert([
           'order_air_detail_id' => $orderAirDetailId,
            'area_id' => $area['id'],
            'order_air_stage_id' => $stageId
        ]);
    }
}

// notify

if ($type == 1) { // dat hang thi cong do local dat hang

    $list_staff_notify = $QAppStatusTitle->getStaffToNotify(13, 1, $arrayAreaId);

    foreach ($list_staff_notify as $staff_notify) {
        $data_noti = [
            'title' => "TRADE MARKETING OPPO VIETNAM",
            'message' => "Có một đơn ĐẶT HÀNG HẠNG MỤC ĐẦU TƯ đang chờ bạn đặt số lượng",
            'link' => "/trade/order-air-area-list?stage_id=" . $stageId,
            'staff_id' => $staff_notify
        ];

//        $QAppNoti->sendNotification($data_noti);
    }
}

$db->commit();

echo json_encode([
   'status' => 0
]);
