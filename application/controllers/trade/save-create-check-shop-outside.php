<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category_id');
$quantity = $this->getRequest()->getParam('quantity');
$latitude = $this->getRequest()->getParam('latitude');
$longitude = $this->getRequest()->getParam('longitude');
$lastCheckShopId = $this->getRequest()->getParam('last_check_shop_id');
$category_brand_id = $this->getRequest()->getParam('category_brand_id');
$quantity_brand = $this->getRequest()->getParam('quantity_brand');
$brand_id = $this->getRequest()->getParam('brand_id');
$back_url = HOST . 'trade/check-shop-outside-list';
$flashMessenger = $this->_helper->flashMessenger;

$QCheckShopOutside = new Application_Model_CheckShopOutside();
$QStore = new Application_Model_Store();
$QCheckShopOutsideDetail = new Application_Model_CheckShopOutsideDetail();
$QCheckShopOutsideFile = new Application_Model_CheckShopOutsideFile();
$QCheckshopOutsideDetailBrand = new Application_Model_CheckShopOutsideDetailBrand();

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {
    $total = count($_FILES['image']['name']);

    if (!empty($latitude) && !empty($longitude)) {
        $data = [
            'store_id' => $store_id,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id'],
            'latitude' => !empty($latitude) ? $latitude : '',
            'longitude' => !empty($longitude) ? $longitude : '',
            'last_check_shop_id' => $lastCheckShopId ? $lastCheckShopId : NULL
        ];
        $id = $QCheckShopOutside->insert($data);
    } else {
        echo json_encode([
            'status' => 1,
            'message' => 'Chưa xác định được tọa độ vui lòng thử lại!'
        ]);
        return;
    }

    $type = 0;
    for ($i = 0; $i < $total; $i++) {
        if ($_FILES['image']['name'][$i] != "") {
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'checkshop_outside' . DIRECTORY_SEPARATOR . $id;

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $tmpFilePath = $_FILES['image']['tmp_name'][$i];

            if ($tmpFilePath != "") {
                $old_name = $_FILES['image']['name'][$i];
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'checkshop_outside' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
                    //Resize Anh
                    $image = new My_Image_Resize();
                    $image->load($url);
                    $image->resizeToWidth(500);
                    $image->save($url);
                    //END Resize

                    // upload image to server s3
                    require_once "Aws_s3.php";
                    $s3_lib = new Aws_s3();
                    $destination_s3 = 'photo/checkshop_outside/' . $id . '/';

                    $upload_s3 = $s3_lib->uploadS3($newFilePath, $destination_s3, $new_name);
                    // upload image to server s3

                } else {
                    $url = NULL;
                }
            } else {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Upload thiếu ảnh. Vui lòng kiểm tra lại!'
                ]);
                return;
            }
            if ($url[$i] != null) {
                $img = [
                    'check_shop_outside_id' => $id,
                    'url' => $url,
                    'type' => $type
                ];
                $QCheckShopOutsideFile->insert($img);
                $type++;
            }
            //
        }
    }

//    if (!empty($category_id)) {
//        foreach ($category_id as $key => $value) {
//            $imey = explode(',', $imei[$key]);
//            $num = 0;
//            for ($i = 0; $i < count($imey); $i++) {
//                if ($imey[$i] != '') {
//                    $num++;
//                }
//            }
//            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);
//            $details = [
//                'check_shop_outside_id' => $id,
//                'category_id' => $category_id[$key],
//                'quantity' => $number,
//                'imei_sn' => $imei[$key] ? $imei[$key] : NULL
//            ];
//
//            $QCheckShopOutsideDetail->insert($details);
//        }
//    }

    // save brand
    if ($category_brand_id) {
        for ($i = 0; $i < count($category_brand_id); $i++) {
            $QCheckshopOutsideDetailBrand->insert([
                'checkshop_id' => $id,
                'category_id' => $category_brand_id[$i] ? $category_brand_id[$i] : Null,
                'quantity' => $quantity_brand[$i] ? $quantity_brand[$i] : Null,
                'brand_id' => $brand_id[$i] ? $brand_id[$i] : Null
            ]);
        }
    }

    $db->commit();

    echo json_encode([
        'status' => 0
    ]);
} catch (Exception $e) {
    $db->rollBack();
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}
// ---------------HIEN THI THONG BAO ----------------------

