<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

include 'PHPExcel/IOFactory.php';
// config for excel template
define('START_ROW', 2);
define('AREA_ID', 0);
define('STORE_ID', 1);

$round_id = $this->getRequest()->getParam('round_id');

$QStore = new Application_Model_Store();
$QAssignStoreRecheckShop = new Application_Model_AssignStoreRecheckShop();
//

$save_folder = 'massupload_assign_store_recheck_shop';
$requirement = array(
    'Size' => array('min' => 5, 'max' => 15000000),
    'Count' => array('min' => 1, 'max' => 1),
    'Extension' => array('xls', 'xlsx')
);
// upload and save file

try {
    $file = My_File::get($save_folder, $requirement);

    if (!$file) {
        echo json_encode([
            'status' => 1,
            'message' => 'Upload failed'
        ]);
        return;
    }
    $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
        . DIRECTORY_SEPARATOR . $file['folder'];
    $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

//read file
//  Choose file to read
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

// read sheet
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
try {
    $QAssignStoreRecheckShop->delete(['round_id = ?' => $round_id]);

    $insertQuery = "INSERT INTO trade_marketing.assign_store_recheck_shop(round_id, store_id) VALUES ";

    for ($row = START_ROW; $row <= $highestRow; ++$row) {
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        $rowData = $rowData[0];

        $storeId = TRIM($rowData[STORE_ID]);

        if ($storeId) {
            $listStoreId [] = $storeId;
            $value = '(' . $round_id . ',' . $storeId  . '),';
            $insertQuery .= $value;
        }
    }

    if (! $listStoreId) {
        echo json_encode([
            'status' => 1,
            'message' => 'Định dạng file không đúng. Vui lòng điền thông tin dựa trên file mẫu !'
        ]);
        return;
    }

    // check if store_id is valid


    // execute query

    $insertQuery = TRIM($insertQuery, ',');

    if($insertQuery){
        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        $stmt = $db->prepare($insertQuery);
        $stmt->execute();

        $stmt->closeCursor();

        $db->commit();

        echo json_encode([
            'status' => 0
        ]);
        return;
    }

    echo json_encode([
        'status' => 1,
        'message' => 'File trống'
    ]);
    return;


} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
}

