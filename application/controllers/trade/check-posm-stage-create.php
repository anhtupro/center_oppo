<?php
$id = $this->getRequest()->getParam('id');

$QCheckPosmStage = new Application_Model_CheckPosmStage();
$QCategory = new Application_Model_Category();
$QCheckPosmDetail = new Application_Model_CheckPosmDetail();

$list_category = $QCategory->fetchAll(['check_posm = ?' => 1])->toArray();
$this->view->list_category = $list_category;

if ($id) {
    $stage = $QCheckPosmStage->fetchRow(['id = ?' => $id]);
    $list_category_old = $QCheckPosmDetail->getCategory($id);
    $this->view->stage = $stage;
    $this->view->list_category_old = $list_category_old;
}

