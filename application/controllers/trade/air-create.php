<?php
require_once "Aws_s3.php";
$s3_lib = new Aws_s3();

$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category');
$repair_details_type = $this->getRequest()->getParam('repair_details_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$contractor = $this->getRequest()->getParam('contractor');
$price = $this->getRequest()->getParam('price');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');
$image = $this->getRequest()->getParam('image');
$image_design = $this->getRequest()->getParam('image_design');

$width = $this->getRequest()->getParam('width');
$wide = $this->getRequest()->getParam('wide');
$deep = $this->getRequest()->getParam('deep');
$total = $this->getRequest()->getParam('total'); //var_dump($total); exit;
$dvt = $this->getRequest()->getParam('dvt');

//price
$air_quotation_title = $this->getRequest()->getParam('air_quotation_title');
$air_quotation_quantity = $this->getRequest()->getParam('air_quotation_quantity');
$air_quotation_total_price = $this->getRequest()->getParam('air_quotation_total_price');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QContructors = new Application_Model_Contructors();
$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QDestruction = new Application_Model_Destruction();
$QAir = new Application_Model_AppAir();
$QAirDetails = new Application_Model_AirDetails();
$QAirDetailsFile = new Application_Model_AirDetailsFile();
$QRepairType = new Application_Model_RepairType();
$QCampaign = new Application_Model_Campaign();
$QAirQuotation = new Application_Model_AirQuotation();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QArea = new Application_Model_Area();
$QAppNoti = new Application_Model_AppNoti();

$flashMessenger       = $this->_helper->flashMessenger;

// $data_repair_type = $QRepairType->fetchAll();
// $repair_type = [];
// foreach($data_repair_type as $key=>$value){
//     $repair_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
// }

$params = array_filter(array(
    'store_id' => $store_id,
    'staff_id'  => $userStorage->id
));

 $params['area_list'] = $this->storage['area_id'];


if (!empty($submit)) {

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        // insert into Air
        $air = [
            'store_id'  => $store_id,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by'  => $userStorage->id
        ];
       
        $air_id = $QAir->insert($air);

        foreach($category_id as $key=>$value){

            if(!empty($deep[$key]))
                {
                    $d=$deep[$key];
                }else{
                    $d=0;
                }


            $air_details_id = $QAirDetails->insert(array(
                'air_id'=>$air_id,
                'cate_id'=>$value,
                'width'  => $width[$key],
                'wide'=>$wide[$key],
                'deep'=>$d,
                'total'=>$total[$key],
                'dvt'=>'cái',
                'quantity'  => 1,
                'note'  => $note[$key],
                'contractor_id' => $contractor[$key]
            ));

                //file báo giá
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $air_details_id;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['files_price']['tmp_name'][$key];
                
                $old_name = $_FILES['files_price']['name'][$key];
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                

                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
      
                if (move_uploaded_file($tmpFilePath, $newFilePath)) {

                    // upload image to server s3
                    $destination_s3 = 'photo/air_file/2/' . $air_details_id . '/';
                    $upload_s3 = $s3_lib->uploadS3($newFilePath, $destination_s3, $new_name);
                    // upload image to server s3


                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $air_details_id . DIRECTORY_SEPARATOR . $new_name;

                    $dtf = [
                        'air_details_id' => $air_details_id,
                        'url'   => '/'.$url,
                        'type' => 2
                    ];
                    $QAirDetailsFile->insert($dtf);
                }

            foreach($image_design[$key + 1] as $k=>$v){

                    $data = $v;
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'air_file' . DIRECTORY_SEPARATOR . '5' . DIRECTORY_SEPARATOR . $air_details_id;

                    if (!is_dir($uploaded_dir))
                                @mkdir($uploaded_dir, 0777, true);

                    $url = '/photo/air_file/5/' . $air_details_id . '/image_'.$k.'.png';

                    $new_name = 'image_'.$k.'.png' ;

                    $file = $uploaded_dir. '/' . $new_name;

                   $success_upload = file_put_contents($file, $data);


                    // upload image to server s3
                    $destination_s3 = 'photo/air_file/5/' . $air_details_id . '/';

                    if ($success_upload) {
                        $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $new_name);
                    }
                    // upload image to server s3


                    $rdf_insert_design = [
                        'air_details_id' => $air_details_id,
                        'url'    => $url,
                        'type'   => 5
                    ];
                    $QAirDetailsFile->insert($rdf_insert_design);  
                    
                    $tag = 1;
                
            }
            foreach($image[ $key + 1] as $k=>$v){
                
                $data = $v;
               
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);

                $data = base64_decode($data);


                $tipo = finfo_open();
                $mime_type = finfo_buffer($tipo, $data, FILEINFO_MIME_TYPE);  // obtiene: image/png
                $arr = explode("/", $mime_type, 2);
                $extension = $arr[1];
              
                //gán đuôi file document
                $arr_extension = [
                    'application/vnd.ms-powerpoint' => '.ppt',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => '.pptx',
                    'application/octet-stream' => '.pptx',
                    'application/vnd.ms-excel' => '.xls',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
                    'application/msword' => '.doc',
                    'application/x-pdf' => '',
                    'application/pdf'   => '.pdf'

                ];

               

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'file' . DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $air_id. DIRECTORY_SEPARATOR . $air_details_id;

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                
                $url = '/file/air/' . $air_id. '/' . $air_details_id . '/file_'.$k.$arr_extension['application/'.$extension];

                $new_name = 'file_'.$k.$arr_extension['application/'.$extension];

                $file = $uploaded_dir . '/' . $new_name;

                file_put_contents($file, $data);


                // upload image to server s3
                $destination_s3 = 'file/air/' . $air_id . '/' . $air_details_id . '/';

                if ($success_upload) {
                    $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $new_name);
                }
                // upload image to server s3

                
                $adf_insert = [
                    'air_details_id' => $air_details_id,
                    'url'    => $url,
                    'type'   => 1
                ];
                $QAirDetailsFile->insert($adf_insert);
                
                $tag = 1;
            }


    

            // Insert quotation
            foreach ($air_quotation_title [$key] as $index => $title) {
                $total_price = $air_quotation_total_price [$key] [$index];
                if ($title && $total_price) {
                    $data_quotation = [
                        'air_details_id' => $air_details_id,
                        'title' => $title,
                        'quantity' => $air_quotation_quantity [$key] [$index] ? $air_quotation_quantity [$key] [$index] : Null,
                        'total_price' => $total_price,
                        'status' => 0
                    ];
                    $QAirQuotation->insert($data_quotation);
                }

            }



            if($tag == 0){
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload hình ảnh từng hạng mục.');
                $this->_redirect(HOST . 'trade/air-create');
            }
        }

        // notify
        $area_id = $QArea->getAreaByStore($store_id);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(8, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất THI CÔNG đang chờ bạn duyệt",
                'link' => "/trade/air-edit?id=" . $air_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();





        $flashMessenger->setNamespace('success')->addMessage('Success!');

        $this->_redirect(HOST . 'trade/air-edit?id='.$air_id);
        
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
        
        $this->_redirect(HOST . 'trade/air-create');
        
    }
    
    
    
}

$this->view->params = $params;

$result = $QCategory->getCategoryAir();
$this->view->category = $result;
$this->view->repair_type = $repair_type;


$resultCamp = $QCampaign->getListStoreDecord($params);
$this->view->store = $resultCamp;
$contructors = $QContructors->fetchAll();
$this->view->contructors = $contructors;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>