<?php 
	
	$id = $this->getRequest()->getParam('contractor_quantity_id');

	$QAppContractorQuantity = new Application_Model_AppContractorQuantity();
	$QAppContractorIn 		= new Application_Model_AppContractorIn();

	$params = [
		'contractor_quantity_id'	=> $id
	];

	$campaign_category = $QAppContractorIn->getInfoIn($params);

	$params['contractor_id'] = $campaign_category['contractor_id'];
	$params['campaign_id'] = $campaign_category['campaign_id'];
	$params['category_id'] = $campaign_category['category_id'];

	$list_imei = $QAppContractorIn->getListImei($params);

	$this->view->list_imei = $list_imei;
	$this->view->campaign_category = $campaign_category;



