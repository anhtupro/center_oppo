<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category_id');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$latitude = $this->getRequest()->getParam('latitude');
$longitude = $this->getRequest()->getParam('longitude');
$category_brand_id = $this->getRequest()->getParam('category_brand_id');
$quantity_brand = $this->getRequest()->getParam('quantity_brand');
$brand_id = $this->getRequest()->getParam('brand_id');
$image = $this->getRequest()->getParam('image');
$image_repair = $this->getRequest()->getParam('image_repair');
$check_shop_entire_id = $this->getRequest()->getParam('check_shop_entire_id');
$is_repair = $this->getRequest()->getParam('is_repair');
$has_change_picture = $this->getRequest()->getParam('has_change_picture');
$list_change_picture_detail_id = $this->getRequest()->getParam('change_picture_detail_id');
$check_posm_quantity = $this->getRequest()->getParam('check_posm_quantity');
$check_posm_note = $this->getRequest()->getParam('check_posm_note');
$list_error_btn = $this->getRequest()->getParam('error_btn');
$list_error_btn_note = $this->getRequest()->getParam('error_btn_note');
$dev = $this->getRequest()->getParam('dev');

$back_url = HOST . 'trade/check-shop-list';
$flashMessenger = $this->_helper->flashMessenger;


$QCheckshop = new Application_Model_AppCheckshop();
$QStore = new Application_Model_Store();
$QCheckshopDetails = new Application_Model_AppCheckShopDetail();
$QAppCheckShopFile = new Application_Model_AppCheckShopFile();
$QAppCheckshopDetailBrand = new Application_Model_AppCheckshopDetailBrand();
$QCheckShopEntireFile = new Application_Model_CheckShopEntireFile();
$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QErrorImageShopCurrent = new Application_Model_ErrorImageShopCurrent();
$QCheckShopEntireErrorImage = new Application_Model_CheckShopEntireErrorImage();
$QChangePictureDetail = new Application_Model_ChangePictureDetail();
$QCheckPosmAssignCategory = new Application_Model_CheckPosmAssignCategory();
$QCheckErrorBtn = new Application_Model_CheckErrorBtn();


// check hạng mục lập lại 2 lần 
$array_count_category = array_count_values($category_id);

foreach ($category_id as $item) {
    $count_duplicate_category_id = $array_count_category[$item];
    if ($count_duplicate_category_id > 1) {
        $flashMessenger->setNamespace('error')->addMessage("Bạn đã chọn trùng hạng mục vui lòng chọn lại !");
        $this->getResponse()->setHeader('Content-Type', 'text/plain')
            ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
        exit;
    }
}



$db = Zend_Registry::get('db');
$db->beginTransaction();
try {

    //Check GPS
    $last_checkshop = $QCheckshop->getLastCs($store_id);

//    if(!empty($last_checkshop['latitude'])){
//        $distance_gps = My_DistanceGps::getDistance($last_checkshop['latitude'], $last_checkshop['longitude'], $latitude, $longitude, "K");
//
//        if($distance_gps > 2){
////            $flashMessenger->setNamespace('error')->addMessage('Vị trí GPs không chính xác!.'.$distance_gps.'('.$last_checkshop['latitude'].','.$last_checkshop['longitude'].')('.$latitude.','.$longitude.')');
////            $this->getResponse()->setHeader('Content-Type', 'text/plain')
////                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
////            exit;
//        }
//    }
    //END





    //Nếu có lock: Check số lượng checkshop khớp với số lượng chốt
    $investments_lock = $QCheckshop->getCheckshopLock($store_id, 1);

    if (!empty($investments_lock)) {

        $cat_check = [];
        foreach ($category_id as $key => $value) {

            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);

            $cat_check['quantity'][$value] = $number;
            $cat_check['imei_sn'][$value] = $imei[$key] ? $imei[$key] : NULL;
        }

        $cat_lock = [];
        foreach ($investments_lock as $key => $value) {
            $cat_lock['quantity'][$value['category_id']] = $value['quantity'];
            $cat_lock['imei_sn'][$value['category_id']] = $value['imei_sn'];
        }

//        if ($cat_check['quantity'] != $cat_lock['quantity']) {
//            $flashMessenger->setNamespace('error')->addMessage("Số lượng không khớp với số chốt.");
//            $this->getResponse()->setHeader('Content-Type', 'text/plain')
//                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
//            exit;
//        }
//
//        if ($cat_check['imei_sn'] != $cat_lock['imei_sn']) {
////            $flashMessenger->setNamespace('error')->addMessage("Mã QR Code không khớp với mã chốt.");
////            $this->getResponse()->setHeader('Content-Type', 'text/plain')
////                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
////            exit;
//        }

    }

    if (!empty($latitude) && !empty($longitude)) {
//    if (1==1) {
        // update is last check shop
        $whereIsLast = [
            'store_id = ?' => $store_id,
            'is_last = ?' => 1
        ];

        $QCheckshop->update([
            'is_last' => 0
        ],$whereIsLast);

        // insert
        $data = [
            'store_id' => $store_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->storage['staff_id'],
            'latitude' => !empty($latitude) ? $latitude : '',
            'longitude' => !empty($longitude) ? $longitude : '',
            'status' => 1,
            'is_last' => 1
        ];
        $id = $QCheckshop->insert($data);
    } else {
        echo json_encode([
            'status' => 1,
            'message' => 'Chưa xác định được tọa độ. Vui lòng thử lại!!'
        ]);
        return;
//        $db->rollBack();
//        $flashMessenger->setNamespace('error')->addMessage("Chưa xác định được tọa độ. Vui lòng thử lại!!");
//        $this->getResponse()->setHeader('Content-Type', 'text/plain')
//            ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
//        exit;
    }


    if (count($image) < 3) {
        echo json_encode([
            'status' => 1,
            'message' => 'Vui lòng upload ít nhất 3 ảnh!!'
        ]);
        return;

//        $db->rollBack();
//        $flashMessenger->setNamespace('error')->addMessage("Thất bại. Vui lòng upload ít nhất 3 ảnh!!" );
//        $this->getResponse()->setHeader('Content-Type', 'text/plain')
//            ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
//        exit;
    }

    foreach ($image as $type => $v) {

        $array_data = explode(',', $v);
        $image_base64 = $array_data[1];

        $data = base64_decode($image_base64);

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
            DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . $id;

        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);

        $newName = md5(uniqid('', true)) . '.png';

        $url = '/photo/checkshop/' . $id . '/' . $newName;

        $file = $uploaded_dir . '/' . $newName;

        $success_upload = file_put_contents($file, $data);

        $url_resize = 'photo' .
            DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $newName;

//            //Resize Anh
        $image = new My_Image_Resize();
        $image->load($url_resize);
        $image->resizeToWidth(800);
        $image->save($url_resize);
//                //END Resize

        // upload image to server s3
        require_once "Aws_s3.php";
        $s3_lib = new Aws_s3();
        $destination_s3 = 'photo/checkshop/' . $id . '/';

        if ($success_upload) {
            $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
            if ($upload_s3['status'] == 0) { // lỗi
                echo json_encode([
                    'status' => 1,
                    'message' => 'Lỗi lưu hình ảnh, Vui lòng submit lại!!'
                ]);
                return;

//                $flashMessenger->setNamespace('error')->addMessage("Lỗi lưu hình, Vui lòng submit lại!!");
//                $this->getResponse()->setHeader('Content-Type', 'text/plain')
//                    ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
//                exit;
            }
        }
        // upload image to server s3


        if ($url) {
            $img = [
                'checkshop_id' => $id,
                'url' => TRIM($url, '/'),
                'type' => $type
            ];
            $QAppCheckShopFile->insert($img);
        }
        //
    }


    if (!empty($category_id)) {
        foreach ($category_id as $key => $value) {
            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);
            $details = [
                'checkshop_id' => $id,
                'category_id' => $category_id[$key],
                'quantity' => $number,
                'imei_sn' => $imei[$key]
            ];

            $QCheckshopDetails->insert($details);
        }

    } else {
//            $db->rollBack();
//            $flashMessenger->setNamespace('error')->addMessage("Thất bại. Không có hạng mục để kiểm tra!!");
//            $this->getResponse()->setHeader('Content-Type', 'text/plain')
//                ->setBody(HOST . 'trade/create-checkshop?id=' . $store_id)->sendResponse();
//            exit;
    }

    // save category brand
    if ($category_brand_id) {
        for ($i = 0; $i < count($category_brand_id); $i++) {
            $QAppCheckshopDetailBrand->insert([
                'checkshop_id' => $id,
                'category_id' => $category_brand_id[$i] ? $category_brand_id[$i] : Null,
                'quantity' => $quantity_brand[$i] ? $quantity_brand[$i] : Null,
                'brand_id' => $brand_id[$i] ? $brand_id[$i] : Null
            ]);
        }
    }


    // image repair
    if ($is_repair) {

        // ảnh
        foreach ($image_repair as $type => $v) {

            $array_data = explode(',', $v);
            $image_base64 = $array_data[1];

            $data = base64_decode($image_base64);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_entire_id . DIRECTORY_SEPARATOR;
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/check_shop_entire/' . $check_shop_entire_id . '/' . $newName;

            file_put_contents($uploaded_dir . '/' . $newName, $data);

            $url_resize = 'photo' .
                DIRECTORY_SEPARATOR . 'check_shop_entire' . DIRECTORY_SEPARATOR . $check_shop_entire_id . DIRECTORY_SEPARATOR . $newName;

            //Resize Anh
            $image = new My_Image_Resize();
            $image->load($url_resize);
            $image->resizeToWidth(800);
            $image->save($url_resize);
            // //END Resize

            $QCheckShopEntireFile->insert([
                'url' => $url,
                'type' => $type,
                'check_shop_id' => $check_shop_entire_id,
                'image_type' => 3,
                'created_by' => $this->storage['staff_id']
            ]);

        }



        // cập nhật các lỗi hình ảnh hiện tại
        foreach ($is_repair as $entire_error_image_id => $status_repair) {
            $QCheckShopEntireErrorImage->update([
                'is_repair' => $status_repair ? $status_repair : 0,
                'repair_by' => $status_repair ? $this->storage['staff_id'] : 0,
                'repair_type' => $status_repair ? 2 : 0
            ], ['id = ?' => $entire_error_image_id]);
        }

        $error_shop = $QCheckShopEntireErrorImage->checkErrorShop($check_shop_entire_id);

        $QCheckShopEntire->update([
            'is_repair' => $error_shop ? 0 : 1,
            'is_sale_repair' => $error_shop ? 0 : 1,
            'repair_by' => $this->storage['staff_id']
        ],['id = ?' => $check_shop_entire_id]);

        $current_error_shop = $QErrorImageShopCurrent->fetchRow(['store_id = ?' => $store_id]);

        if ($current_error_shop) {
            $QErrorImageShopCurrent->update([
                'error_image' => $error_shop ? 1 : 0
            ], ['store_id = ?' => $store_id]);
        } else {
            $QErrorImageShopCurrent->insert([
                'store_id' => $store_id,
                'error_image' => $error_shop ? 1 : 0
            ]);
        }
    }
    //save image repair


    // thay tranh

    if ($has_change_picture) {
        // cập nhật mới
        foreach ($has_change_picture as $change_picture_detail_id => $value) {
            $QChangePictureDetail->update([
                'has_changed_picture' => 1
            ], ['id = ?' => $change_picture_detail_id]);
        }
    }

    //end thay tranh

    // kiểm tra posm
    if ($check_posm_quantity) {
        foreach ($check_posm_quantity as $check_posm_assign_category_id => $posm_quantity) {
            $QCheckPosmAssignCategory->update([
                'real_quantity' => $posm_quantity ? $posm_quantity: 0
            ], ['id = ?' => $check_posm_assign_category_id]);
        }
    }
    if ($check_posm_note) {
        foreach ($check_posm_note as $check_posm_assign_category_id => $posm_note) {
            $QCheckPosmAssignCategory->update([
                'note' => $posm_note
            ], ['id = ?' => $check_posm_assign_category_id]);
        }
    }
    // end kiểm tra posm

    // kiểm tra lỗi btn 3.0
    $QCheckErrorBtn->delete(['store_id = ?' => $store_id]);
    foreach ($list_error_btn as $error_id) {
        $QCheckErrorBtn->insert([
           'store_id' => $store_id,
           'checkshop_id' => $id,
           'error_id' => $error_id,
            'note' => $list_error_btn_note[$error_id]
        ]);
    }
    // kiểm tra lỗi btn 3.0


    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Thành công!');

    echo json_encode([
        'status' => 0
    ]);

//    $this->getResponse()->setHeader('Content-Type', 'text/plain')
//        ->setBody(HOST . 'trade/check-shop-list')->sendResponse();
//    exit;


} catch (Exception $e) {
    $db->rollBack();
    echo json_encode([
        'status' => 1,
        'message' => "Có lỗi: " . $e->getMessage()
    ]);
    return;
//    $flashMessenger->setNamespace('error')->addMessage("Có lỗi: " . $e->getMessage());
//    $this->getResponse()->setHeader('Content-Type', 'text/plain')
//        ->setBody($back_url)->sendResponse();
//    exit;
}
// ---------------HIEN THI THONG BAO ----------------------

