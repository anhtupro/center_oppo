<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$stage_id = $this->getRequest()->getParam('stage_id');

$db = Zend_Registry::get("db");
$select = $db->select()
             ->from(['a' => DATABASE_TRADE.'.change_picture_assign_store'], [
                'a.store_id',
                 'store_name' => 's.name'
             ])
->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
->where('a.stage_id = ?', $stage_id);

$data = $db->fetchAll($select);


require_once 'PHPExcel.php';

$PHPExcel = new PHPExcel();
$heads = array(
    'Store ID',
    'Store name'
);

$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 2;

$i = 1;

foreach($data as $item){

    $alpha    = 'A';
    $sheet->setCellValue($alpha++.$index, $item['store_id']);
    $sheet->setCellValue($alpha++.$index, $item['store_name']);
    $index++;

}



$filename = 'Danh sách đã upload' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;