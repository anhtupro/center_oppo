<?php
$id   = $this->getRequest()->getParam('id');
$status   = $this->getRequest()->getParam('status');
$submit	    = $this->getRequest()->getParam('submit');
$price	    = $this->getRequest()->getParam('price');
$priceformat	    = $this->getRequest()->getParam('priceformat');
$contructors	    = $this->getRequest()->getParam('contructors');
$reason	    = $this->getRequest()->getParam('reason');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QDestruction = new Application_Model_Destruction();
$QStore = new Application_Model_Store();
$QTransfer= new Application_Model_Transfer();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QImei = new Application_Model_ImeiTrade();
$QReason = new Application_Model_AppReason ();

$flashMessenger       = $this->_helper->flashMessenger;

$resultTransfer = $QTransfer->GetList($id);
$status = $resultTransfer[0]['status'];

$checkerror = false;

if($submit){
	if($userStorage->title == $resultTransfer[0]['title_status'] ){
		try{
			if($resultTransfer[0]['status']  == 13 ){
					//---------KIEM TRA XEM CO UP FILE CHUA?----------

				$upload = new Zend_File_Transfer();
				$upload->setOptions(array('ignoreNoFile'=>true));

	            //check function
				if (function_exists('finfo_file'))
					$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));

				$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
				$upload->addValidator('Size', false, array('max' => '10MB'));
				$upload->addValidator('ExcludeExtension', false, 'php,sh');
				$files = $upload->getFileInfo();

					if($files['image']['error']==0){ // NEU UP FILE KO LOI THI TIEN HANH INSERT
						$data_file = array();

						$fileInfo = (isset($files['image']) and $files['image']) ? $files['image'] : null;
						$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
						DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
						DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . 'transfer_file' . DIRECTORY_SEPARATOR . $id;
						if (!is_dir($uploaded_dir))
							@mkdir($uploaded_dir, 0777, true);
						$upload->setDestination($uploaded_dir);

			            //Rename
						$old_name = $fileInfo['name'];
						$tExplode = explode('.', $old_name);
						$extension = end($tExplode);
						$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
						$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
						$r = $upload->receive(array('image'));
						if($r){
							$data_file['file_name'] = $new_name;
						}
						else{
							$messages = $upload->getMessages();
							foreach ($messages as $msg)
								throw new Exception($msg);
						}
				// --------------INSERT HINH ANH LEN DATABASE-------------
						$data_file['parrent_id'] = $id;
						$data_file['type']=4;
						$result =$QAppFile->insert($data_file);
					}
					if(!$result) $checkerror =true;
				}
				if( $checkerror==false ){
					$data = array_filter(array(
						'status'=>$resultTransfer[0]['status']+1,
						'update_at'=>date('Y-m-d H:i:s')
					));
					$result =$QTransfer->update ($data,'id = '.$id);
					$flashMessenger->setNamespace('success')->addMessage('Xác nhận thành công');
					$this->_redirect(HOST . 'trade/transfer-confirm?id='.$id);
				}

			}catch (exception $e) {
				$flashMessenger->setNamespace('error')->addMessage('Xác nhận không thành công');
				$this->_redirect(HOST . 'trade/transfer-confirm?id='.$id);
			}
		}

	}


// --------------------- LẤY THÔNG TIN NHÂN VIÊN------------
	$paramsData['staff_id'] =$resultTransfer[0]['staff_id'];
	$resultInformStaff =$QDestruction->GetInformStaff($paramsData);

// -------------------- BIẾN HIỂN THỊ RA VIEW -----------
	$params = array_filter(array(
		'id'=>$id,
		'category'=>$resultTransfer[0]['category_id'],
		'categoryname' => $resultTransfer[0]['categoryname'],
		'store'=> $resultTransfer[0]['storefrom_id'],
		'store_to'=> $resultTransfer[0]['storeto_id'],
		'storename_from'=> $resultTransfer[0]['storename_from'],
		'storename_to'=> $resultTransfer[0]['storename_to'],
		'ngaytao'=> $resultTransfer[0]['ngaytao'],
		'title_status'=> $resultTransfer[0]['title_status'],
		'title_staff'=>$userStorage->title,
		'staff_create_name'=>$resultInformStaff['firstname'].' '. $resultInformStaff['lastname'],
		'name_status'=>$resultTransfer[0]['name_status'],
		'status_id'=>$resultTransfer[0]['status_id'],
		'reason' => $resultTransfer[0]['reason'],
		'submit'=>$submit
	));

	$this->view->params = $params;

			//----------LAY DANH SACH LY DO-----------
$resultReason = $QReason->GetAll();
$this->view->reason = $resultReason;
// -------------------RETURN HINH ANH ------------------
	$parrent_id['parrent_id'] =$id;
	$dataAll = $QAppFile->GetListTransfer($parrent_id);
	$this->view->dataAll = $dataAll;
// -------------------RETURN HINH ANH SHOP DIEU CHUYEN TOI-----------------
	$parrent_id['parrent_id'] =$id;
	$dataAll = $QAppFile->GetListShopTo($parrent_id);
	$this->view->dataAllShop = $dataAll;
// -------------------RETURN HINH ANH NƠI ĐIỀU CHUYỂN TỚI------------------
	$parrent_id['parrent_id'] =$id;
	$dataAll = $QAppFile->GetListTransferTO($parrent_id);
	$this->view->dataAllTo = $dataAll;
	// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages = $messages;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages_success = $messages_success;
	}
	?>
