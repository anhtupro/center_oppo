<?php
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$category   = $this->getRequest()->getParam('category');
$store   = $this->getRequest()->getParam('store');
$sl   = $this->getRequest()->getParam('sl', 0);
$lydo   = $this->getRequest()->getParam('lydo','');
$imei_sn    = $this->getRequest()->getParam('imei');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QAppFile = new Application_Model_AppFile();
$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QDestruction = new Application_Model_Destruction();
$QCampaign = new Application_Model_Campaign();
$QImei = new Application_Model_ImeiTrade();

$submit   = $this->getRequest()->getParam('submit');
$flashMessenger       = $this->_helper->flashMessenger;

$params = array_filter(array(
	'category' => $category,
	'store'=>$store,
	'sl'=>$sl,
	'lydo'=>$lydo
));

$this->view->params = $params;
$result="";

if($submit){ // -----event click xac nhan---------
	//---------kiem tra xem co up file chua?----------
	if(isset($imei_sn) and $imei_sn){
		$params['id'] = $imei_sn;
		$result = $QImei->GetId($params);
		if(!empty($result['category_id']) && !empty($result['store_id'])){
			$category_id=$result['category_id'];
			$store_id=$result['store_id'];
		}else {
			$flashMessenger->setNamespace('error')->addMessage('Đề xuất không thành công. Mã code không hợp lệ ');
			$this->_redirect(HOST . 'trade/transfer-create');
		}
	}

	if($userStorage->title == SALE_SALE_SALE){
		$upload = new Zend_File_Transfer();
		$upload->setOptions(array('ignoreNoFile'=>true));

            //check function
		if (function_exists('finfo_file'))
			$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif','application/vnd.ms-powerpoint'));

		$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
		$upload->addValidator('Size', false, array('max' => '10MB'));
		$upload->addValidator('ExcludeExtension', false, 'php,sh');
		$files = $upload->getFileInfo();

	if($files['image']['error']==0){ // neu up file ko loi thi tien hanh insert
		// insert  record bang destruction
		try{
			
			$result_query =$QDestruction->insert(array(
				'category_id'=>$category_id,
				'store_id'=>$store_id,
				'sl'=>$sl,
				'lydo'=>$lydo,
				'staff_id'=>$userStorage->id,
				'date'=>date('Y-m-d H:i:s'),
				'status'=>25
			));

        // ------------------ upload image----------------------------------------------------------
			$data_file = array();

			$fileInfo['image'] = (isset($files['image']) and $files['image']) ? $files['image'] : null;
			$fileInfo['imageoutside'] = (isset($files['imageoutside']) and $files['imageoutside']) ? $files['imageoutside'] : null;
			foreach ($fileInfo as $key => $value) {
				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
				DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
				DIRECTORY_SEPARATOR . 'destruction' . DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . $result_query;
				if (!is_dir($uploaded_dir))
					@mkdir($uploaded_dir, 0777, true);
				$upload->setDestination($uploaded_dir);

            //Rename
				$old_name = $value['name'];
				$tExplode = explode('.', $old_name);
				$extension = end($tExplode);
				$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
				$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
				$r = $upload->receive(array($key));
				if($r){
					$data_file['file_name'] = $new_name;
				}
				else{
					$messages = $upload->getMessages();
					foreach ($messages as $msg)
						throw new Exception($msg);
				}
	// --------------insert hinh anh len database-------------
				$data_file['parrent_id'] = $result_query;

				$result =$QAppFile->insert($data_file);
				
			}
	//---------- lay hinh anh vua upload  moi dc tai len -----------
			$params['id']=$result;

			$dataAll = $QAppFile->GetListDestruction($params);
			$this->view->dataAll = $dataAll;
			if($result_query){
				$flashMessenger->setNamespace('success')->addMessage('Đề xuất thành công ');
				$this->_redirect(HOST . 'trade/destruction-edit?id='.$result_query);
			}
		}catch (exception $e) {
			if($result_query ){
				$whereRepair = $QDestruction->getAdapter()->quoteInto('id = ?',$result_query);
				$QDestruction->delete($whereRepair);

				$whereAppfile = $QAppFile->getAdapter()->quoteInto('type ="0" and parrent_id = ?',$result_query);
				$QAppFile->delete($whereAppfile);
			}
			$flashMessenger->setNamespace('error')->addMessage('Đề xuất không thành công. Vui lòng kiểm tra lại thông tin và hình ảnh');

			$this->_redirect(HOST . 'trade/destruction-create?category='.$params['category'].'&store='.$params['store'].'&sl='.$params['sl'].'&lydo='.$lydo);
		}

	}else{
		$this->_redirect(HOST . 'trade/destruction-create?category='.$params['category'].'&store='.$params['store'].'&sl='.$params['sl'].'&lydo='.$lydo);
		$flashMessenger->setNamespace('error')->addMessage('Lỗi file ảnh vui lòng thử lại.');
		$this->_redirect(HOST . 'trade/destruction-create');
	}
}
}
// ---------------hien thi thong bao ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;

}
$paramsData['id_staff'] = $userStorage->id;
$resultArea = $QDestruction->GetStaffEmail($paramsData);
$paramsData['email']=$resultArea['email'];
$resultDestruc =  $QDestruction->GetArea_Staff($paramsData);

	//lay danh sach cua hang
$paramsData['staff_id'] = $userStorage->id;
$resultCamp = $QCampaign->getListStore($paramsData);
$this->view->store = $resultCamp;
	//lay danh sach hang muc

$result = $QCategory->getall();
$this->view->category = $result;





?>
