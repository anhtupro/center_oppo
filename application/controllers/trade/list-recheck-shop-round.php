<?php
$page = $this->getRequest()->getParam('page', 1);
$fromDate = $this->getRequest()->getParam('from_date');
$toDate = $this->getRequest()->getParam('to_date');
$limit = LIMITATION;

$params = [
  'from_date' => $fromDate ? date('Y-m-d', strtotime($fromDate)) : NULL ,
  'to_date' => $toDate ? date('Y-m-d', strtotime($toDate)) : NULL
];

$QRecheckShopRound = new Application_Model_RecheckShopRound();

$listRecheckShopRound = $QRecheckShopRound->fetchPagination($page, $limit, $total, $params);

$this->view->listRecheckShopRound = $listRecheckShopRound;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'trade/list-recheck-shop-round' . '?';