<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore            = new Application_Model_Store();
$QRemoveShop       = new Application_Model_RemoveShop();
$QRemoveShopReason = new Application_Model_RemoveShopReason();

$id       = $this->getRequest()->getParam('id');
 
if (!empty($id)) {
  
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        $where          = $QRemoveShop->getAdapter()->quoteInto('id = ?', $id);
        $removeShop_row = $QRemoveShop->fetchRow($where);

        if ($removeShop_row['status'] != 2) {
            $flashMessenger->setNamespace('error')->addMessage('Chỉ Approve đơn ở trạng thái Confirm. Vui lòng kiểm tra lại.!');
            $this->_redirect(HOST . 'trade/remove-shop-detail?id='.$id);
        }

        
        $data_update = array(
            'status'       => 3,
            'approved_at' => date('Y-m-d H:i:s'),
            'approved_by' => $userStorage->id
        );

        $QRemoveShop->update($data_update, $where);

        $info = "remove_shop_approve_" . $id . "_" . serialize($data_update);

        // log
        $QLog = new Application_Model_Log();
        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');


        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));

 
        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/remove-shop-list');
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
        $this->_redirect(HOST . 'trade/remove-shop-list');
    }
}else{
    $flashMessenger->setNamespace('error')->addMessage('Thiếu ID truyền vào.!');
    $this->_redirect(HOST . 'trade/remove-shop-detail');
}

$this->view->params           = $params;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
