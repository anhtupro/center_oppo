<?php

require_once "Aws_s3.php";
$s3_lib = new Aws_s3();

$transfer_id = $this->getRequest()->getParam('transfer_id');
$transfer_details_id = $this->getRequest()->getParam('transfer_details_id');
$contractor_id = $this->getRequest()->getParam('contractor_id');
$price = $this->getRequest()->getParam('price');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$category_id = $this->getRequest()->getParam('category_id');
$quantity = $this->getRequest()->getParam('quantity');
$area_from = $this->getRequest()->getParam('area_from');
$area_to = $this->getRequest()->getParam('area_to');
$warehouse_to = $this->getRequest()->getParam('warehouse_to');
$warehouse_from = $this->getRequest()->getParam('warehouse_from');

$transfer_date = $this->getRequest()->getParam('transfer_date');
$image = $this->getRequest()->getParam('image');
$manager_approve = $this->getRequest()->getParam('manager_approve');
$transfer_quotation_title = $this->getRequest()->getParam('transfer_quotation_title');
$transfer_quotation_quantity = $this->getRequest()->getParam('transfer_quotation_quantity');
$transfer_quotation_total_price = $this->getRequest()->getParam('transfer_quotation_total_price');
$contract_number = $this->getRequest()->getParam('contract_number');

$review_cost = $this->getRequest()->getParam('review_cost');
$review_note = $this->getRequest()->getParam('review_note');
$month_debt = $this->getRequest()->getParam('month_debt');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QTransfer = new Application_Model_Transfer();
$QTransferDetails = new Application_Model_TransferDetails();
$QTransferFile = new Application_Model_TransferFile();
$QTransferConfirm = new Application_Model_TransferConfirm();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAirInventoryArea = new Application_Model_AirInventoryArea();
$QAirInventoryAreaImei = new Application_Model_AirInventoryAreaImei();
$QTransferQuotation = new Application_Model_TransferQuotation();
$QInventoryOrderIn = new Application_Model_InventoryOrderIn();
$QInventoryOrderOut = new Application_Model_InventoryOrderOut();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QTransferDetailsChild = new Application_Model_TransferDetailsChild();
$QTransferPrice = new Application_Model_TransferPrice();
$QTransferImage = new Application_Model_TransferImage();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();


$where_transfer = $QTransfer->getAdapter()->quoteInto('id = ?', $transfer_id);
$transfer = $QTransfer->fetchRow($where_transfer);

$where = $QTransferDetails->getAdapter()->quoteInto('transfer_id = ?', $transfer_id);
$transfer_details = $QTransferDetails->fetchAll($where);
$transfer_details_child = $QTransferDetailsChild->fetchAll($where);

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    // Hủy đề xuất
    if ($remove) {
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QTransfer->update($remove_date, $where_transfer);

        // cập nhật trạng thái của hạng mục
        foreach ($transfer_details_child as $value) {

            if ($value['app_checkshop_detail_child_id']) {
                $QAppCheckshopDetailChild->update([
                    'in_processing' => Null
                ], ['id = ?' => $value['app_checkshop_detail_child_id']]);
            }
        }

        // notify
        $area_id = $QArea->getAreaByStore($transfer['store_from']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(6, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất ĐIỀU CHUYỂN bị hủy",
                'link' => "/trade/transfer-edit?id=" . $transfer_id,
                'staff_id' => $staff_notify
            ];
//            $result = $QAppNoti->sendNotification($data_noti);
        }


        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
    }

    //Reject
    if ($reject) {
        if (in_array($transfer['status'], [2,3,7] )) {
            $transfer_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 1,
                'reject_by' => $userStorage->id,
                'reject_at' => date('Y-m-d H:i:s')
            ];

            $staff_notify = 1;
        }

        if (in_array($transfer['status'], [5] )) {
            $transfer_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 4,
                'reject_by' => $userStorage->id,
                'reject_at' => date('Y-m-d H:i:s')
            ];

            $staff_notify = 4;

        }

        if (in_array($transfer['status'], [1,4] )) {
            $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
        }


        $QTransfer->update($transfer_data, $where_transfer);

        // notify
        $area_id = $QArea->getAreaByStore($transfer['store_from']);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(6, $staff_notify, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất ĐIỀU CHUYỂN bị từ chối",
                'link' => "/trade/transfer-edit?id=" . $transfer_id,
                'staff_id' => $staff_notify
            ];
//            $result = $QAppNoti->sendNotification($data_noti);
        }


        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);


    }
    //END Reject


    //1: Chờ TMK Local xác nhận
    if ($transfer['status'] == 1) {
        // chọn nhà thầu
        $QTransfer->update([
           'contractor_id' =>  $contractor_id ? $contractor_id : Null
        ], ['id = ?' => $transfer_id]);

        // điền giá
        $where = [];
        $where [] = $QTransferPrice->getAdapter()->quoteInto('transfer_id = ?', $transfer_id);
        $where [] = $QTransferPrice->getAdapter()->quoteInto('status = ?', 0);

        $QTransferPrice->update(['status' => 1], $where);

        foreach ($transfer_quotation_title as $index => $title) {
            if ($title && $transfer_quotation_total_price [$index]) {
                $data_quotation = [
                    'transfer_id' => $transfer_id,
                    'title' => $title,
                    'quantity' => $transfer_quotation_quantity [$index] ? $transfer_quotation_quantity [$index] : NULL,
                    'total_price' => $transfer_quotation_total_price [$index],
                    'status' => 0
                ];
                $QTransferPrice->insert($data_quotation);

            } else {
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đầy đủ nội dung và thành tiền trong upload báo giá!');
                $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
            }

        }


        // kiểm tra xem là loại ĐIỀU CHUYỂN gì để cập nhật status

        if ($transfer['approve_type'] ) {
            if ($transfer['approve_type'] == 1 || $transfer['approve_type']  == 2) {
                $status = 2;
            }
            if ($transfer['approve_type'] == 3) {
                $status = 4;
            }
        } else {
            $status = 2;
        }


        $QTransfer->update([
            'status' => $status,
            'reject' => 0,
            'reject_note' => ''
        ], $where_transfer);

        $status_notify = $status;

    }//$transfer['status'] == 1

    //2: Chờ TMK Leader xác nhận
    if ($transfer['status'] == 2) {

        if($transfer['approve_type'] && $transfer['approve_type'] == 1) {
            $status = 7;
        } else {
            $status = 4;
        }

        $QTransfer->update(['status' => $status], $where_transfer);
        $status_notify = $status;

    }

    //3: Chờ ASM xác nhận
    if ($transfer['status'] == 3) {

        if($transfer['approve_type'] && $transfer['approve_type'] == 1) {
            $status = 7;
        } else {
            $status = 4;
        }

        $QTransfer->update([
            'status' => $status,
            'reject' => 0
        ], $where_transfer);

        $status_notify = $status;

    }

    // chờ TMK manager xác nhận
    if ($transfer['status'] == 7) {
        $QTransfer->update(['status' => 4], $where_transfer);
        $status_notify = 4;
    }


    //4: Chờ Trade local nghiệm thu
    if ($transfer['status'] == 4) {

        // upload image attendance
        $tag = 0;

        foreach($image as $k=>$v){


            $data = $v;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . $transfer_id . DIRECTORY_SEPARATOR . 'image_acceptance';

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/transfer/' . $transfer_id . '/image_acceptance/' . $newName;

            $file = $uploaded_dir . '/' . $newName;

            $success_upload = file_put_contents($file, $data);

            // upload image to server s3
            $destination_s3 = 'photo/transfer/' . $transfer_id . '/image_acceptance/';

            if ($success_upload) {
                $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
            }
            // upload image to server s3

            $rdf_insert = [
                'transfer_id' => $transfer_id,
                'url'    => $url,
                'type'   => 4
            ];
            $QTransferImage->insert($rdf_insert);
            $tag = 1;
        }


        if ($tag != 1) {
            $flashMessenger->setNamespace('error')->addMessage("Vui lòng upload ảnh nghiệm thu.");
            $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
        }

        // price
        $where = [];
        $where [] = $QTransferPrice->getAdapter()->quoteInto('transfer_id = ?', $transfer_id);
        $where [] = $QTransferPrice->getAdapter()->quoteInto('status = ?', 2);

        $QTransferPrice->update(['status' => 3], $where);

        foreach ($transfer_quotation_title as $index => $title) {
            if ($title && $transfer_quotation_total_price [$index]) {
                $data_quotation = [
                    'transfer_id' => $transfer_id,
                    'title' => $title,
                    'quantity' => $transfer_quotation_quantity [$index] ? $transfer_quotation_quantity [$index] : NULL,
                    'total_price' => $transfer_quotation_total_price [$index],
                    'status' => 2
                ];
                $QTransferPrice->insert($data_quotation);

            } else {
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đầy đủ nội dung và thành tiền trong upload báo giá!');
                $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
            }

        }

        $QTransfer->update([
            'status' => 5,
            'reject' => 0
        ], $where_transfer);

        $status_notify = 5;

    }

    //5: Chờ TMK Leader xác nhận hoàn thành
    if ($transfer['status'] == 5) {
        if (($transfer['store_from'])) {  // chuyển từ shop
            foreach ($transfer_details_child as $key => $value) {
                $status_code = $QTransfer->transferFromShop($transfer['store_from'], $value['category_id'], $value['quantity'], $value['imei_sn'], $transfer['area_to'], $value['app_checkshop_detail_child_id'], $transfer['store_to'], $transfer['id']);
            }
        }

        if (($transfer['area_from'])) {  // chuyển từ kho
            foreach ($transfer_details_child as $key => $value) {
                $status_code = $QTransfer->transferFromWarehouse($transfer['store_to'], $value['category_id'], $value['quantity'], $value['imei_sn'], $value['app_checkshop_detail_child_id'], $transfer['area_from'], $transfer['id']);
            }
        }

        if ($status_code['code'] == 2) {
            $flashMessenger->setNamespace('error')->addMessage($status_code['message']);
            $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);
        }

        $QTransfer->update([
            'status' => 6
        ], $where_transfer);

        $status_notify = 6;


    }//if($transfer['status'] == 7)

    // chờ HO xác nhận chi phí thanh toán
    if ($transfer['status'] == 6) {
        if ($review_cost == 2) { // chi phí sai
            // price
            $where = [];
            $where [] = $QTransferPrice->getAdapter()->quoteInto('transfer_id = ?', $transfer_id);
            $where [] = $QTransferPrice->getAdapter()->quoteInto('status = ?', 4);
            $QTransferPrice->update(['status' => 5], $where);

            foreach ($transfer_quotation_title as $index => $title) {
                    $data_quotation = [
                        'transfer_id' => $transfer_id,
                        'title' => $title ? $title : Null,
                        'quantity' => $transfer_quotation_quantity [$index] ? $transfer_quotation_quantity [$index] : NULL,
                        'total_price' => $transfer_quotation_total_price [$index] ? $transfer_quotation_total_price [$index] : 0,
                        'status' => 4
                    ];
                    $QTransferPrice->insert($data_quotation);
            }


        }

        if ($review_cost == 1) { // chi phí chính xác
            $data_price = $QTransferPrice->fetchAll([
                'transfer_id = ?' => $transfer_id,
                'status = ?' => 2
            ]);

            foreach ($data_price as $price) {
                $QTransferPrice->insert([
                   'transfer_id' => $price['transfer_id'] ? $price['transfer_id'] : Null,
                    'title' => $price['title'] ? $price['title'] : Null,
                    'quantity' => $price['quantity'] ? $price['quantity'] : Null,
                    'total_price' => $price['total_price'] ? $price['total_price'] : Null,
                    'status' => 4
                ]);
            }
        }

        $QTransfer->update([
            'status' => 8,
            'review_cost' => $review_cost,
            'review_note' => $review_note,
            'month_debt' => $month_debt
        ], $where_transfer);

        $status_notify = 8;
    }

    //TRANSFER CONFIRM
    $rp_confirm = [
        'transfer_id' => $transfer_id,
        'transfer_status' => $transfer['status'],
        'confirm_by' => $this->storage['staff_id'],
        'created_at' => date('Y-m-d H:i:s')
    ];
    $QTransferConfirm->insert($rp_confirm);
    //END TRANSFER CONFIRM

    $QTransfer->update(['reject' => 0], $where_transfer);

    // notify
    if ($transfer['store_from']) {
        $area_id = $QArea->getAreaByStore($transfer['store_from']);
    } else {
        $area_id = $transfer['area_from'];
    }
    $list_staff_notify = $QAppStatusTitle->getStaffToNotify(6, $status_notify, $area_id);


    foreach ($list_staff_notify as $staff_notify) {
        $data_noti = [
            'title' => "TRADE MARKETING OPPO VIETNAM",
            'message' => "Có một đề xuất ĐIỀU CHUYỂN đang chờ bạn duyệt",
            'link' => "/trade/transfer-edit?id=" . $transfer_id,
            'staff_id' => $staff_notify
        ];
//        $result = $QAppNoti->sendNotification($data_noti);
    }

    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);


} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
    $this->_redirect(HOST . 'trade/transfer-edit?id=' . $transfer_id);

}

?>