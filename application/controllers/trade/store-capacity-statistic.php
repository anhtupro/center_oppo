<?php
$stage_id = intval($this->getRequest()->getParam('stage_id'));
$params ['area_list'] = $this->storage['area_id'];

$params = [
    'area_list' => $this->storage['area_id'],
    'stage_id' => $stage_id
];

$QStoreCapacityAssign = new Application_Model_StoreCapacityAssign();
$QStoreCapacityResult = new Application_Model_StoreCapacityResult();

$QArea = new Application_Model_Area();
$list_capacity = $QStoreCapacityAssign->getCapacity($stage_id);
$list_area = $QArea->getListArea($params);
$statistic = $QStoreCapacityResult->getStatistic($params);

// shop không xếp hạng
$statistic_not_review = $QStoreCapacityResult->getStatisticNotReview($params);

$this->view->list_capacity = $list_capacity;
$this->view->list_area = $list_area;
$this->view->statistic = $statistic;
$this->view->statistic_not_review = $statistic_not_review;
$this->view->params = $params;