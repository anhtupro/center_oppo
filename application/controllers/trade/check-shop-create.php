<?php 
$submit	    = $this->getRequest()->getParam('submit');
$store_id   = $this->getRequest()->getParam('store');
$category   = $this->getRequest()->getParam('category');
$soluong   = $this->getRequest()->getParam('soluong');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QStore = new Application_Model_Store();
$QCampaign = new Application_Model_Campaign();
$QCategory = new Application_Model_Category();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QAppCheckShopDetail = new Application_Model_AppCheckShopDetail();
$QAppFile = new Application_Model_AppFile();

$flashMessenger       = $this->_helper->flashMessenger;
$resultCheckShop ="";
if($submit){

	try{
		// ---------- UPLOAD INFO CHECK SHOP-----------------------
		$resultCheckShop = $QAppCheckShop->insert(array(
			'store_id'=>$store_id,
			'create_at'=>date('Y-m-d H:i:s'),
			'update_at'=>date('Y-m-d H:i:s'),
			'staff_id'=>$userStorage->id
		));
		$id_detail=[];
		$find_cate=[];
		$find_id =[];
		$find_sl=[];
		$dem_cate=0;
		$dem_sl=0;
		foreach ($_POST as $key => $value) {
			$cate =strpos($key,'ategory');
			$soluong =strpos($key,'oluong');
			if($cate==1){
				$id_temp_dem = substr($key,strpos($key,'_')+1);
				$find_id[$dem_cate]['cate_id']=$value;
				$find_id[$dem_cate]['id_temp_dem']=$id_temp_dem;
				$find_cate[$dem_cate] =$value;
				$dem_cate++;
			}
			if($soluong==1){
				$find_sl[$dem_sl] =$value;
				$dem_sl++;
			}
		}
		for ($i=0; $i < $dem_sl; $i++) { 
			$resultDetail=$QAppCheckShopDetail->insert(array(
				'category_id'=>$find_cate[$i],
				'sl'=>$find_sl[$i],
				'parrent_id'=>$resultCheckShop
			));
			$find_id[$i]['detail_id']=$resultDetail;
		}
		// ---------- UPLOAD IMAGE-----------------------
		$upload = new Zend_File_Transfer();
		$upload->setOptions(array('ignoreNoFile'=>true));

	            //check function
		if (function_exists('finfo_file'))
			$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));

		$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
		$upload->addValidator('Size', false, array('max' => '10MB'));
		$upload->addValidator('ExcludeExtension', false, 'php,sh');
		$files = $upload->getFileInfo();

		foreach ($files as $key => $value) {

			if($value['error'] ==0){
				$id_temp_dem_file = substr($key,strpos($key,'_')+1);
				foreach ($find_id as $key1 => $value1) {
					if(intval($value1['id_temp_dem']) == intval($id_temp_dem_file) ){
						$parrent_id_file = $value1['detail_id'];
					}
					
				}

				$data_file = array();

				$fileInfo[$key] = (isset($value) and $value) ? $value : null;

				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
				DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
				DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . 'checkshop_file' . DIRECTORY_SEPARATOR . $parrent_id_file;
				if (!is_dir($uploaded_dir))
					@mkdir($uploaded_dir, 0777, true);
				$upload->setDestination($uploaded_dir);

            //Rename
				$old_name =$value['name'];
				$tExplode = explode('.', $old_name);
				$extension = end($tExplode);
				$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
				$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
				$r = $upload->receive(array($key));
				if($r){
					$data_file['file_name'] = $new_name;
				}
				else{
					$messages = $upload->getMessages();
					foreach ($messages as $msg)
						throw new Exception($msg);
				}
					// --------------INSERT HINH ANH LEN DATABASE-------------
				$data_file['parrent_id'] = $parrent_id_file;
				$data_file['type']=4;
				$result =$QAppFile->insert($data_file);

			}
		}


	}catch(exception $e){
		$flashMessenger->setNamespace('error')->addMessage('Đề xuất không thành công.');
		$this->_redirect(HOST . 'trade/check-shop-create');
	}
}

	// -------------RETURN CATEGORY ---------------
$result = $QCategory->GetAll();
$this->view->category = $result;
		// -------------RETURN STORE ---------------
$paramsData['staff_id'] = $userStorage->id;
$resultCamp = $QCampaign->getListStore($paramsData);
$this->view->store = $resultCamp;

		// ---------------HIEN THI THONG BAO ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

?>