<?php 

	$shipment_id = $this->getRequest()->getParam('shipment_id');

	$QArea 			    = new Application_Model_Area();
	$QShipment 			= new Application_Model_AppShipment();
	$QShipmentDetails 	= new Application_Model_AppShipmentDetails();
	$QTransporter 		= new Application_Model_AppTransporter();

	$where = $QShipment->getAdapter()->quoteInto('id = ?', $shipment_id);
	$shipment = $QShipment->fetchRow($where);

	$params = [
		'shipment_id' => $shipment_id
	];

	if($this->storage['group'] == CONTRACTOR_GROUP_ID){
		$contractor = $QShipment->getContractorById($this->storage['staff_id']);
		$params['contractor_id'] = $contractor['id'];
	}

	$shipment_details = $QShipmentDetails->fetchShipmentDetails($params);

	$shipment_area = $QShipmentDetails->fetchShipmentDetailsArea($params);

	$list_area = [];

	foreach($shipment_area as $key=>$value){
		$list_area[$value['area_id']][] = $value;
	}

	$imei_scan = $QShipmentDetails->getListImeiScan($params);

	$data_scan = [];
	foreach ($imei_scan as $key => $value) {
		$data_scan[$value['category_id']] = [
			'quantity'		=> $value['quantity'],
			'list_imei'		=> $value['list_imei']
		];
	}

	$this->view->transporter 		= $QTransporter->get_cache();
	$this->view->shipment 			= $shipment;
	$this->view->shipment_details 	= $shipment_details;
	$this->view->data_scan          = $data_scan;
	$this->view->list_area          = $list_area;
	$this->view->area               = $QArea->get_cache();
