<?php

$season = $this->getRequest()->getParam('season');

$year = $this->getRequest()->getParam('year', date('Y'));
$month = $this->getRequest()->getParam('month');

$fromDate = $this->getRequest()->getParam('from_date');
$fromDate = $fromDate ? date('Y-m-d', strtotime($fromDate)) : date('01-m-Y');

$toDate = $this->getRequest()->getParam('to_date');
$toDate = $toDate ? date('Y-m-d', strtotime($toDate)) : date('t-m-Y');

$params = [
    'area_list' => $this->storage['area_id'],
    'year' => $year,
    'month' => $month
];

if ($season) {
    $monthInSeason = [1, 2, 3];
    $params ['month_in_season'] = $monthInSeason;
    $params ['season'] = $season;
} else {
    $params ['from_date'] = $fromDate;
    $params ['to_date'] = $toDate;
}


$QArea = new Application_Model_Area();
$QAdditionalAirCost = new Application_Model_AdditionalAirCost();
$QCategoryAdditional = new Application_Model_CategoryAdditional();
$QAdditionalPosmCost = new Application_Model_AdditionalPosmCost();
$QAdditionalPart = new Application_Model_AdditionalPart();
$QAdditionalPartCost = new Application_Model_AdditionalPartCost();

$listAdditionalCategory = $QCategoryAdditional->getAll();
$params['additional_part_type'] = [2];
$listAdditionalPart =   $QAdditionalPart->getAll($params);

$listArea = $QArea->getListArea($params);

$additionalPosmCost = $QAdditionalPosmCost->getCost($params);

//get total cost
foreach ($listArea as $area) {
    foreach ($additionalPosmCost [$area['id']] as $additionalCost) {
        $totalCostArea [$area['id']] += $additionalCost['cost'];
    }

    $totalCost += $totalCostArea [$area['id']];
}


$this->view->listArea = $listArea;
$this->view->additionalPosmCost = $additionalPosmCost;
$this->view->listAdditionalCategory = $listAdditionalCategory;
$this->view->totalCostArea = $totalCostArea;
$this->view->totalCost = $totalCost;
$this->view->season = $season;
$this->view->year = $year;
$this->view->month = $month;
$this->view->params = $params;
