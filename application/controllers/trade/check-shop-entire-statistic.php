<?php
$from_date = $this->getRequest()->getParam('from_date', date('Y-m-01'));
$to_date = $this->getRequest()->getParam('to_date', date('Y-m-d'));

$from_date = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $from_date)));
$to_date = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $to_date)));

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QAreaStaff = new Application_Model_AreaStaff();

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'area_list' => $this->storage['area_id']
];

if ($userStorage->title == TRADE_MARKETING_EXECUTIVE) { // trade local
    $params['staff_id'] = $userStorage->id;
}


$statistic_count_store = $QCheckShopEntire->getStatisticStore($params);
$statistic_count_check = $QCheckShopEntire->getStatisticCheck($params);
$statistic_error = $QCheckShopEntire->getStatisticError($params);
$list_staff = $QAreaStaff->getListStaff($params);

$this->view->statistic_count_check = $statistic_count_check;
$this->view->statistic_count_store = $statistic_count_store;
$this->view->statistic_error = $statistic_error;
$this->view->list_staff = $list_staff;
$this->view->params = $params;

