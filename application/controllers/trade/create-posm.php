<?php

$name = $this->getRequest()->getParam('name');
$type = $this->getRequest()->getParam('type');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$category_id = $this->getRequest()->getParam('category_id');
$price = $this->getRequest()->getParam('price');
$submit = $this->getRequest()->getParam('submit');
$campaign_id = $this->getRequest()->getParam('campaign_id');
$campaign_category_id = $this->getRequest()->getParam('campaign_category_id');

$QCategory = new Application_Model_Category();
$QCampaign = new Application_Model_Campaign();
$QCampaignCategory = new Application_Model_CampaignCategory();
$QCampaignArea = new Application_Model_CampaignArea();
$QArea = new Application_Model_Area();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppNoti = new Application_Model_AppNoti();

$area = $QArea->get_cache_bi();

$flashMessenger = $this->_helper->flashMessenger;

if ($type == TU_BAN_BUCGOC) {
    $where = $QCategory->getAdapter()->quoteInto('group_bi IN (?)', [2, 3]);
} else {
    $where = $QCategory->getAdapter()->quoteInto('type = ?', 1);
}

$category = $QCategory->fetchAll($where);


if (!empty($campaign_id)) {


    $where = [];
    $where[] = $QCampaignCategory->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
    $where[] = $QCampaignCategory->getAdapter()->quoteInto('del = 0 OR del IS NULL');
    $campaign_category = $QCampaignCategory->fetchAll($where);
    $this->view->campaign_category = $campaign_category;


    $where = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
    $campaign = $QCampaign->fetchRow($where);
    $this->view->campaign = $campaign;

}

if (!empty($submit)) {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        $from = explode('/', trim($from_date));
        $to = explode('/', trim($to_date));

        if (!is_array($from) || count($from) != 3 || !is_array($to) || count($to) != 3)
            throw new Exception("Invalid date");

        $from = $from[2] . '-' . $from[1] . '-' . $from[0];
        $to = $to[2] . '-' . $to[1] . '-' . $to[0];

        if (empty($campaign_id)) {
            $data = [
                'name' => $name,
                'from' => $from,
                'to' => $to,
                'type' => $type,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $id = $QCampaign->insert($data);

            $value_insert = '';
            $insertQuery = "INSERT INTO trade_marketing.campaign_area(campaign_id, area_id, category_id, quantity_limit, quantity, price, total_price, status) VALUES ";

            foreach ($category_id as $key => $value) {
                if (!empty($value)) {
                    $data_cat = [
                        'campaign_id' => $id,
                        'category_id' => $value,
                        'report_quantity' => 1,
                        'report_image' => 0,
                        'price' => 0
                    ];
                    $QCampaignCategory->insert($data_cat);


                    foreach ($area as $k => $v) {
                        $value_insert = '(' . $id . ',' . $k . ',' . $value . ',0,0,0,0,2),';
                    
                        $insertQuery .= $value_insert;

                    }

                }
            }


            $insertQuery = TRIM($insertQuery, ',');
  
            $stmt = $db->prepare($insertQuery);
            $stmt->execute();

            $stmt->closeCursor();
 
        } else {
            $value_insert = '';
            $insertQuery = "INSERT INTO trade_marketing.campaign_area(campaign_id, area_id, category_id, quantity_limit, quantity, price, total_price, status) VALUES ";
            $data = [
                'name' => $name,
                'from' => $from,
                'to' => $to,
                'type' => $type,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $where = [];
            $where = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
            $id = $QCampaign->update($data, $where);

            $where = [];
            $where = $QCampaignCategory->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
            $QCampaignCategory->update(['del' => 1], $where);


            foreach ($category_id as $key => $value) {
 
                if (!empty($value)) {
           
                    $data_cat = [
                        'campaign_id' => $campaign_id,
                        'category_id' => $value,
                        'report_quantity' => 1,
                        'report_image' => 0,
                        'price' => 0,
                        'del' => 0
                    ];

                    if (empty($campaign_category_id[$key])) {
                        $QCampaignCategory->insert($data_cat);
                    } else {
                        $where = [];
                        $where[] = $QCampaignCategory->getAdapter()->quoteInto('id = ?', $campaign_category_id[$key]);

                        $QCampaignCategory->update($data_cat, $where);
                    }

                    //Check campaign_area
                    $where_campaign_area = [];
                    $where_campaign_area[] = $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
                    $where_campaign_area[] = $QCampaignArea->getAdapter()->quoteInto('category_id = ?', $value);
                    $campaign_area = $QCampaignArea->fetchRow($where_campaign_area);

                    if (empty($campaign_area)) {
                        foreach ($area as $k => $v) {
                            $value_insert = '(' . $campaign_id . ',' . $k . ',' . $value . ',0,0,0,0,2),';
                            $string_value_insert .= $value_insert;
                        }
                    }

                }

            }


      
            if ($string_value_insert) {
                $insertQuery .= $string_value_insert;
                $insertQuery = TRIM($insertQuery, ',');

                $stmt = $db->prepare($insertQuery);
                $stmt->execute();
                $stmt->closeCursor();
            }

        }


        $flashMessenger->setNamespace('success')->addMessage('Done');
        $db->commit();

        if ($type == TU_BAN_BUCGOC) {
            $this->_redirect(HOST . 'trade/list-campaign?type=' . $type);
        } else {
            $this->_redirect(HOST . 'trade/list-posm');
        }

    } catch (Exception $e) {
        $db->rollBack();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

        if ($type == TU_BAN_BUCGOC) {
            $this->_redirect(HOST . 'trade/list-campaign?type=' . $type);
        } else {
            $this->_redirect(HOST . 'trade/list-posm');
        }
    }
}


$this->view->category = $category;
$this->view->trade_type = unserialize(TRADE_TYPE)[$type];
$this->view->type = $type;

$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;

$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;


	

