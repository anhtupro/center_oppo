<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
include 'PHPExcel/IOFactory.php';
// config for excel template excel
define('START_ROW', 3);
define('START_CATEGORY_ID_COLUMN', 1);
define('STORE_ID', 0);


$name = $this->getRequest()->getParam('name');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$stage_id = $this->getRequest()->getParam('stage_id');
$list_category_id_detail = $this->getRequest()->getParam('category_id');

$QCheckPosmStage = new Application_Model_CheckPosmStage();
$QCheckPosmAssignStore = new Application_Model_CheckPosmAssignStore();
$QCheckPosmAssignCategory = new Application_Model_CheckPosmAssignCategory();
$QCategory = new Application_Model_Category();
$QCheckPosmDetail = new Application_Model_CheckPosmDetail();

$db = Zend_Registry::get('db');
$db->beginTransaction();


if ($stage_id) {
    $QCheckPosmStage->update([
        'name' => $name,
        'from_date' => $from_date ? date('Y-m-d', strtotime($from_date)) : NULL,
        'to_date' => $to_date ? date('Y-m-d', strtotime($to_date)) : NULL
    ], ['id = ?' => $stage_id]);

} else {
    $stage_id_insert = $QCheckPosmStage->insert([
        'name' => $name,
        'from_date' => $from_date ? date('Y-m-d', strtotime($from_date)) : NULL,
        'to_date' => $to_date ? date('Y-m-d', strtotime($to_date)) : NULL
    ]);
}



$stage_id = $stage_id ? $stage_id : $stage_id_insert;

// insert category
$QCheckPosmDetail->delete(['stage_id = ?' => $stage_id]);
foreach ($list_category_id_detail as $value_category_id) {
    $QCheckPosmDetail->insert([
        'stage_id' => $stage_id,
        'category_id' => $value_category_id
    ]);
}
//

// mass upload list store
// upload and save file
if ($_FILES['file']['name']) { // if has file upload

    $save_folder = 'check_posm_massupload';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 15000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx')
    );

    try {
        $file = My_File::get($save_folder, $requirement);

        if (!$file) {
            echo json_encode([
                'status' => 1,
                'message' => 'Upload failed'
            ]);
            return;
        }
        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        
        $url_file = '/files/' . $save_folder. '/' . $file['folder'] . '/' . $file['filename'];

        if ($url_file) {
            $QCheckPosmStage->update([
                'url_file' => $url_file
            ],['id = ?' => $stage_id]);
        }

    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    //read file
    //  Choose file to read
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    // read sheet
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();


    try {
        $QCheckPosmAssignStore->delete(['stage_id = ?' => $stage_id]);
        $QCheckPosmAssignCategory->delete(['stage_id = ?' => $stage_id]);

        $insert_query_assign_store = "INSERT INTO trade_marketing.check_posm_assign_store(stage_id, store_id) VALUES ";
        $insert_query_assign_category = "INSERT INTO trade_marketing.check_posm_assign_category(stage_id, store_id, category_id, quantity) VALUES ";

        $list_category_id = $sheet->rangeToArray('A1' . ':' . $highestColumn . '1', NULL, TRUE, FALSE);
        $list_category_id = $list_category_id[0];
        $array_store_id = [];
        $list_store_invalid  = [];

        for ($row = START_ROW; $row <= $highestRow; ++$row) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = $rowData[0];

            $store_id = TRIM($rowData[STORE_ID]);

            if (in_array($store_id, $array_store_id) || !$store_id) {
                $list_store_invalid [] = $store_id;
                continue;
            }

            $array_store_id [] = $store_id;

            // insert assign store
            $value_assign_store = '(' . $stage_id . ',' . $store_id  . '),';
            $insert_query_assign_store .= $value_assign_store;

            for ($i = START_CATEGORY_ID_COLUMN; $i < count($list_category_id); $i++) {
                $category_id = $list_category_id[$i];
                $quantity = $rowData[$i] ? $rowData[$i] : 0;

                // insert assign category
                if ($quantity > 0) {
                    $value_assign_category = '(' . $stage_id . ',' . $store_id  .  ',' . $category_id  .  ',' . $quantity  . '),';
                    $insert_query_assign_category .= $value_assign_category;
                }

            }
     

        }

        if ($list_store_invalid) {
            echo json_encode([
                'status' => 1,
                'message' => "Store ID bị trùng, hoặc bị trống, hoặc không đúng định dạng số: " . implode(', ', $list_store_invalid)
            ]);
            return;
        }

        // execute query
        $insert_query_assign_store = TRIM($insert_query_assign_store, ',');
        $insert_query_assign_category = TRIM($insert_query_assign_category, ',');

        if($insert_query_assign_store && $insert_query_assign_category){
            $stmt1 = $db->prepare($insert_query_assign_category);
            $stmt2 = $db->prepare($insert_query_assign_store);

            $stmt1->execute();
            $stmt1->closeCursor();

            $stmt2->execute();
            $stmt2->closeCursor();
        }

        // update del =1  store when store has no data in detail
        $update_query = "
        UPDATE  trade_marketing.check_posm_assign_store a
        LEFT JOIN trade_marketing.check_posm_assign_category b ON a.store_id = b.store_id AND a.stage_id = b.stage_id
        SET a.del = 1
        where a.stage_id = " . $stage_id . " AND b.id IS NULL
        ";


        $stmt = $db->prepare($update_query);
        $stmt->execute();
        $stmt->closeCursor();
        //

        // check store is valid
        $list_store_invalid = $QCheckPosmStage->getListStoreInvalid($stage_id);

        if ($list_store_invalid) {
            echo json_encode([
                'status' => 1,
                'message' => "Những Store ID đã đóng code: " . implode(', ', $list_store_invalid) . '. Vui lòng kiểm tra và thử lại'
            ]);
            return;
        }

    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

}


$db->commit();

echo json_encode([
    'status' => 0
]);