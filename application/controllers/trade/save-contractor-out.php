<?php 
	
	$list_imei 	= $this->getRequest()->getParam('list_imei');
	$out_id 	= $this->getRequest()->getParam('out_id');

	$list_imei = json_decode($list_imei, true);

	$flashMessenger = $this->_helper->flashMessenger;
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id = $userStorage->id;

	$back_url = HOST.'trade/contractor-out?id='.$out_id;

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		$QAppContractorIn = new Application_Model_AppContractorIn();

		foreach($list_imei as $key=>$value){
			$data = [
				'contractor_out_id'	=> $out_id,
				'out_date'			=> date('Y-m-d H:i:s')
			];

			$where = $QAppContractorIn->getAdapter()->quoteInto('imei_sn = ?', $value);
			$imei_in = $QAppContractorIn->fetchRow($where);

			if(!$imei_in){
				$flashMessenger->setNamespace('error')->addMessage( "Imei ko có trong kho.");
    			$this->redirect($back_url);
			}

			if($imei_in['out_date']){
				$flashMessenger->setNamespace('error')->addMessage( "Imei đã được scan.");
    			$this->redirect($back_url);
			}

			$QAppContractorIn->update($data, $where);
		}

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Hoàn thành!');
    	$this->redirect($back_url);


	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;




