<?php 
	
	$category_id = $this->getRequest()->getParam('category_id');
	$campaign_id = $this->getRequest()->getParam('campaign_id');

	$QCategory 			= new Application_Model_Category();
	$QArea   			= new Application_Model_Area();
	$QAppOrderDetails 	= new Application_Model_AppOrderDetails();

	$area_id = !empty($this->storage['area_id']) ? $this->storage['area_id'] : NULL;

	$params = [
		'campaign_id' => $campaign_id,
		'area_id'	  => $area_id,
		'category_id' => $category_id
	];

	$where    = $QCategory->getAdapter()->quoteInto('id = ?', $category_id );
	$category = $QCategory->fetchRow($where);

	$where    = $QArea->getAdapter()->quoteInto('id IN (?)', $area_id );
	$area     = $QArea->fetchAll($where);

	$order_details  = $QAppOrderDetails->getDataDetails($params);

	$list_imei = $QAppOrderDetails->getImeiArea($params);
	$list_imei_in = $QAppOrderDetails->getImeiAreaIn($params);


	$this->view->category 		= $category;
	$this->view->order_details  = $order_details;
	$this->view->area           = $area;
	$this->view->list_imei      = $list_imei;
	$this->view->list_imei_in   = $list_imei_in;

	


