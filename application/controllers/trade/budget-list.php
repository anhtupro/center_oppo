<?php

$year = $this->getRequest()->getParam('year');
$areaId = $this->getRequest()->getParam('area_id');

$QBudget = new Application_Model_Budget();
$QBudgetDetail = new Application_Model_BudgetDetail();
$QArea = new Application_Model_Area();

$currentYear = date('Y');

if (!$year) {
    $year = $currentYear;
}

$params = [
    'year' => $year,
    'area_id' => $area_id
];

$listBudget = $QBudget->getListBudget($params);
$listArea = $QArea->fetchAll('del = 0')->toArray();

$this->view->listBudget = json_encode($listBudget);
$this->view->listArea = $listArea;
$this->view->year = $year;
