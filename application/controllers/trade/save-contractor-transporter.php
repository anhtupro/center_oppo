<?php 
	
	$contractor_out_id 	= $this->getRequest()->getParam('contractor_out_id');
	$campaign_id 		= $this->getRequest()->getParam('campaign_id');

	$QAppContractorOut = new Application_Model_AppContractorOut();
	$QAppOrderDetails  = new Application_Model_AppOrderDetails();

	$flashMessenger = $this->_helper->flashMessenger;

    $params = [
		'contractor_out_id' => $contractor_out_id,
	];

	$back_url = HOST.'trade/contractor-transporter?campaign_id='.$campaign_id;

	$db       = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		$contract_out = $QAppContractorOut->getTranferOut($params);
		$area = [];
		$cat  = [];
		foreach($contract_out as $key=>$value){
			$area[] = $value['area_id'];
			$cat[]  = $value['category_id'];
		}

		$params['area'] = $area;
		$params['cat']	= $cat;

		$details_list = $QAppContractorOut->getOrderDetailsTranfer($params);

		$details_id = [];

		foreach ($details_list as $key => $value) {
			$details_id[] = $value['id'];
		}

		$where = $QAppOrderDetails->getAdapter()->quoteInto('id IN (?)', $details_id);
		$QAppOrderDetails->update(['status'=>8], $where);
		

		$where = $QAppContractorOut->getAdapter()->quoteInto('id IN (?)', $contractor_out_id);
		$QAppContractorOut->update(['status'=>2], $where);

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Cập nhật thành công!');
    	$this->redirect($back_url);


	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;
	




