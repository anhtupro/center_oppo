<?php
  
$id 					= $this->getRequest()->getParam('id');
$contrac_name 			= $this->getRequest()->getParam('contrac_name');
$contract_type 			= $this->getRequest()->getParam('contract_type');
$contract_contractor 	= $this->getRequest()->getParam('contract_contractor');
$details_update 				= $this->getRequest()->getParam('details_update');
$contract_check 		= $this->getRequest()->getParam('contract_check');
$submit 				= $this->getRequest()->getParam('submit');
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();

$QAirDetails 			= new Application_Model_AirDetails();
$QTransferDetails 		= new Application_Model_TransferDetails();
$QRepairDetails     	= new Application_Model_RepairDetails;
$QDestructionDetails    = new Application_Model_DestructionDetails;
$QAppContractFile 		= new Application_Model_AppContractFile();
$QAppContract 			= new Application_Model_AppContract();
$QContructors 			= new Application_Model_Contructors();

$flashMessenger       = $this->_helper->flashMessenger;

$params = array_filter(array(
    'staff_id'  => $userStorage->id,
    'id'		=> $id
));

//contract
$data = $QAppContract->getDataContract($params);
$this->view->data = $data;
//contract detail
$params['type'] = $data['type'];
$params['contractor_id'] = $data['contructors_id'];

if($data['type']==1){
	$details = $QAirDetails->getContract($params);
}
if($data['type']==2){
	$details = $QTransferDetails->getContract($params);
}
if($data['type']==3){
	$details = $QRepairDetails->getContract($params);
}
if($data['type']==4){
	$details = $QDestructionDetails->getContract($params);
}

$this->view->details = $details;

if (!empty($submit)) {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
    	
		
        foreach ($details as $key => $value) {
       //thi công

                if($data['type'] == 1){
	                $where = $QAirDetails->getAdapter()->quoteInto('id = ?', $value['id']);
	                $data_update = [
	                    'contract_id'   => null
	                ];
	                $QAirDetails->update($data_update,$where);
                }
                //điều chuyển
                if($data['type'] == 2){
                    $where = $QTransferDetails->getAdapter()->quoteInto('id = ?', $value['id']);
                    $data_update = [
                        'contract_id'   => null
                    ];
                    $QTransferDetails->update($data_update,$where);
                }
                //sửa chửa
                if($data['type'] == 3){
                	//var_dump(1234); exit;
                    $where = $QRepairDetails->getAdapter()->quoteInto('id = ?', $value['id']);
                    $data_update = [
                        'contract_id'   => null
                    ];
                    $QRepairDetails->update($data_update,$where);
                }
                //tiêu hủy
                if($data['type'] == 4){
                    $where = $QDestructionDetails->getAdapter()->quoteInto('id = ?', $value['id']);
                    $data_update = [
                        'contract_id'   => null
                    ];
                    $QDestructionDetails->update($data_update,$where);
                }
        }

 
        //update file hợp đồng
        if(!empty($_FILES['contrac_file_update']['name'])){
        		
        	//xóa file cũ	
        	 $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'contract_file' . DIRECTORY_SEPARATOR .  $id . DIRECTORY_SEPARATOR . $data['file_name'];
              unlink($uploaded_dir);
              $where_file = $QAppContractFile->getAdapter()->quoteInto('contract_id = ?', $id);
        	  $QAppContractFile->delete($where_file);

              //update file báo giá
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'contract_file' . DIRECTORY_SEPARATOR .  $id;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['contrac_file_update']['tmp_name'];
                
                $old_name = $_FILES['contrac_file_update']['name'];
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-ver1-' . md5(uniqid('', true)) . '.' . $extension;
                
            
                
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'contrac_file' . DIRECTORY_SEPARATOR . $contract_id . DIRECTORY_SEPARATOR . $new_name;

                    $dtf = [
                        'contract_id' => $id,
                        'file_name'   => $new_name,
                        'url'   => '/'.$url,
                        'type' => 1
                    ];
                    $QAppContractFile->insert($dtf);
                }
        }
          

/// cập nhật chi tiết chi phí
        foreach($details_update as $key=>$value){
        	
        	//nếu có check
            if($contract_check[$value][0] == 1 || $contract_check[$value][0]=='on'){
                //thi công
                if($contract_type == 1){
                $where = $QAirDetails->getAdapter()->quoteInto('id = ?', $value);
                $data = [
                    'contract_id'   => $id
                ];
                $QAirDetails->update($data,$where);
                }
                //điều chuyển
                if($contract_type == 2){
                    $where = $QTransferDetails->getAdapter()->quoteInto('id = ?', $value);
                    $data = [
                        'contract_id'   => $id
                    ];
                    $QTransferDetails->update($data,$where);
                }
                //sửa chửa
                if($contract_type == 3){
                    $where = $QRepairDetails->getAdapter()->quoteInto('id = ?', $value);
                    $data = [
                        'contract_id'   => $id
                    ];
                    $QRepairDetails->update($data,$where);
                }
                //tiêu hủy
                if($contract_type == 4){
                    $where = $QDestructionDetails->getAdapter()->quoteInto('id = ?', $value);
                    $data = [
                        'contract_id'   => $id
                    ];
                    $QDestructionDetails->update($data,$where);
                }
            }
        }
///    
        $where= $QAppContract->getAdapter()->quoteInto('id = ?', $id);
        // update into Contract
        $contract = [
            'title'  => $contrac_name,
            'type' => $contract_type,
            'contructors_id' => $contract_contractor,
            'status'        => 1,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        
        $QAppContract->update($contract,$where);

        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/edit-contract?id='.$id);
        
    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
        $this->_redirect(HOST . 'trade/edit-contract?id='.$id);
        
    }
}


$contructors = $QContructors->fetchAll();
$this->view->contructors = $contructors;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>