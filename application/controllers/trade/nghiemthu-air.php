<?php
$category_id   = $this->getRequest()->getParam('category');
$quantity = $this->getRequest()->getParam('quantity');
$width = $this->getRequest()->getParam('width');
$wide = $this->getRequest()->getParam('wide');
$deep = $this->getRequest()->getParam('deep');
$total = $this->getRequest()->getParam('total');
$dvt = $this->getRequest()->getParam('dvt');
$QAppAir = new Application_Model_AppAir();
$QAppAirFile = new Application_Model_AppAirFile();
$QAppAirDetail = new Application_Model_AppAirDetail();
$QCategory = new Application_Model_Category();
$flashMessenger       = $this->_helper->flashMessenger;
$title = $userStorage->title;

//var_dump($width); exit;
$id = $this->getRequest()->getParam('id');
$submit = $this->getRequest()->getParam('submit');
$params = array(
	'id' => $id,
	'title'=>$title
);
$back_url = HOST.'trade/air';
$db             = Zend_Registry::get('db');
$db->beginTransaction();
if(!empty($submit))
{
	try{
		
		if(!empty($_FILES['files']['name']))
		{
			$upload = new Zend_File_Transfer();
			$upload->addValidator('FilesSize',false,array('max' => '10MB'));
			
			if (!$upload->isValid()) {

	    		$db->rollBack();
			    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Dung lượng file vượt mức cho phép !");
		    	$this->redirect(HOST.'trade/update-air?id='.$id);
			}
				$where_del= array();
				$where_del[]  = $QAppAirFile->getAdapter()->quoteInto('air_id = ?',$id);
				$where_del[]  = $QAppAirFile->getAdapter()->quoteInto('type = ?',2);
				$QAppAirFile->delete($where_del);
				$totals = count($_FILES['files']['name']);
							//Upload hình file
							$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
										DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
										DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $id;

							if (!is_dir($uploaded_dir))
								@mkdir($uploaded_dir, 0777, true);

							$tmpFilePath = $_FILES['files']['tmp_name'];

							if ($tmpFilePath != ""){

								$old_name 	= $_FILES['files']['name'];
								$tExplode 	= explode('.', $old_name);
								$extension  = end($tExplode);
								$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

								$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;

								if(move_uploaded_file($tmpFilePath, $newFilePath)) {
									$url         = 'files' .
										DIRECTORY_SEPARATOR . 'air' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
								}
								else{
									$url = NULL;
								}
							}
							else{
								$url = NULL;
							}

							$details = [
								'air_id' => $id,
								'url'  => $url,
								'type'	   => 2,
								'file_name'=>$new_name,
								'contructor_id' => 0
							];
							$QAppAirFile->insert($details);
			}
			if(!empty($category_id)){
				$where_delete  = $QAppAirDetail->getAdapter()->quoteInto('air_id = ?', $id);
				$QAppAirDetail->delete($where_delete);		
				foreach ($category_id as $key=> $value) {
						$insert = $QAppAirDetail->insert(array(
					    'air_id'=>$id,
					    'cate_id'=>$value,
					    'width'  => $width[$key],
					    'wide'=>$wide[$key],
					    'deep'=>$deep[$key],
					    'total'=>$total[$key],
					    'dvt'=>$dvt[$key],
					    'quantity'=>$quantity[$key]
						));
				}
			}
		   $db->commit();
		   $flashMessenger->setNamespace('success')->addMessage('Cập nhật thành công!');
		   $this->redirect($back_url);
	}catch(Exception $e)
	{
		$db->rollBack();
		$flashMessenger->setNamespace('error')->addMessage( "Cập nhật thất bại!");
	    $this->redirect(HOST.'trade/update-air?id='.$id);
	}
}else
{
	$db->rollBack();
		$flashMessenger->setNamespace('error')->addMessage( "Cập nhật thất bại!");
	    $this->redirect(HOST.'trade/update-air?id='.$id);
}
// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}