<?php

$repair_id = $this->getRequest()->getParam('repair_id');
$repair_details_id = $this->getRequest()->getParam('repair_details_id');
$contructors_id = $this->getRequest()->getParam('contructors_id');
$price = $this->getRequest()->getParam('price');
$price_final = $this->getRequest()->getParam('price_final');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$repair_date = $this->getRequest()->getParam('repair_date');
$image = $this->getRequest()->getParam('image');

$part = $this->getRequest()->getParam('part');
$part_quantity = $this->getRequest()->getParam('part_quantity');

$repair_details_type_id = $this->getRequest()->getParam('repair_details_type_id');
$repair_type = $this->getRequest()->getParam('repair_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');

$repair_quotation_title = $this->getRequest()->getParam('repair_quotation_title');
$repair_quotation_quantity = $this->getRequest()->getParam('repair_quotation_quantity');
$repair_quotation_total_price = $this->getRequest()->getParam('repair_quotation_total_price');



$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger       = $this->_helper->flashMessenger;

$QRepair = new Application_Model_Repair();
$QRepairDetails = new Application_Model_RepairDetails();
$QRepairDetailsFile = new Application_Model_RepairDetailsFile();
$QRepairDetailsPart = new Application_Model_RepairDetailsPart();
$QRepairConfirm = new Application_Model_RepairConfirm();
$QRepairDetailsType = new Application_Model_RepairDetailsType();
$QRepairQuotation = new Application_Model_RepairQuotation();

$where_repair = $QRepair->getAdapter()->quoteInto('id = ?', $repair_id);
$repair = $QRepair->fetchRow($where_repair);

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    
    //Hủy đề xuất
    if($remove){
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QRepair->update($remove_date, $where_repair);
        
        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/repair-edit?id='.$repair_id);
    }
    //END Hủy đề xuất
    
    //Reject: status => 1, quay về bước trade local duyệt đề xuất
    if($reject){
        
        if($repair['status'] < 4){
            $repair_data = [
                'reject' => 1,
                'reject_note' => $reject_note, 
                'status' => 1
            ];
        }
        
        if($repair['status'] > 4){
            $repair_data = [
                'reject' => 1,
                'reject_note' => $reject_note, 
                'status' => 4
            ];
        }
        
        $QRepair->update($repair_data, $where_repair);
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/repair-edit?id='.$repair_id);
        
        
    }
    //END Reject
    
    
    //1: Chờ TMK Local xác nhận
    if($repair['status'] == 1){
        
        foreach($repair_details_id as $key=>$value){
            $where = $QRepairDetails->getAdapter()->quoteInto('id = ?', $value);

            $data = [
                'contructors_id' => !empty($contructors_id[$value]) ? $contructors_id[$value] : 0,
                'price' => !empty($price[$value]) ? $price[$value] : 0,
            ];
            $QRepairDetails->update($data, $where);

            $where = [];
            $where[] = $QRepairDetailsFile->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairDetailsFile->getAdapter()->quoteInto('type = ?', 2);
            $QRepairDetailsFile->delete($where);

            foreach($_FILES['file']['name'][$value] as $k=>$v){

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $value;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $tmpFilePath = $_FILES['file']['tmp_name'][$value][$k];

                $old_name = $v;
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url = 'photo' .
                        DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $value . DIRECTORY_SEPARATOR . $new_name;

                    $dtf = [
                        'repair_details_id' => $value,
                        'url'   => '/'.$url,
                        'type' => 2
                    ];
                    $QRepairDetailsFile->insert($dtf);
                }
            }
            
            //Linh kiện cty cung cấp
            $where = NULL;
            $where = $QRepairDetailsPart->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $QRepairDetailsPart->delete($where);
            
            foreach($part[$value] as $k=>$v){
                $dtp = [
                    'repair_details_id' => $value,
                    'repair_part_id'    => !empty($v) ? $v : NULL,
                    'quantity'    => !empty($part_quantity[$value][$k]) ? $part_quantity[$value][$k] : NULL,
                    'status'    => 1,
                ];
                $QRepairDetailsPart->insert($dtp);
            }
            //END Linh kiện cty cung cấp
            
            //Upload $repair_details_type_id
            
            foreach($repair_details_type_id[$value] as $k=>$v){
               $rdt_update = [
                   'repair_tp_id' => $repair_details_tp[$value][$k]
               ];
               
               $where = $QRepairDetailsType->getAdapter()->quoteInto('id = ?', $v);
               $QRepairDetailsType->update($rdt_update, $where);
               
            }
            
            $where = [];
            $where[] = $QRepairDetailsType->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairDetailsType->getAdapter()->quoteInto('id NOT IN (?)', $repair_details_type_id[$value]);
            $QRepairDetailsType->update(['del'=>1], $where);
            
            
            //Cập nhật lại quotation cũ = 1 (hủy)
            $where = [];
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('status = 0');
            $QRepairQuotation->update(['status' => 1],$where);
            // Insert quotation
            foreach ($repair_quotation_title [$value] as $index => $title) {
                if(!empty($title)){
                    $data_quotation = [
                        'repair_details_id' => $value,
                        'title' => $title,
                        'quantity' => $repair_quotation_quantity [$value] [$index],
                        'total_price' => $repair_quotation_total_price [$value] [$index],
                        'status' => 0
                    ];
                    $QRepairQuotation->insert($data_quotation);
                    
                }
                
            }

        }
        
        $QRepair->update(['status' => 2, 'reject' => 0], $where_repair);
        
    }//$repair['status'] == 1
    
    //2: Chờ TMK Leader xác nhận
    if($repair['status'] == 2){
        $QRepair->update(['status' => 3], $where_repair);
    }
    
    //3: Chờ ASM xác nhận
    if($repair['status'] == 3){
        $QRepair->update(['status' => 4, 'reject' => 0], $where_repair);
    }
    
    
    //4: Chờ TMK Local xác nhận nghiệm thu
    if($repair['status'] == 4){
        //UPload file hình ảnh nghiệm thu
        $tag = 0;
        
        foreach($image as $key=>$value){
            
            foreach($value as $k=>$v){
                $data = $v;
                
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'repair_file' . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $key;

                if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);

                $url = '/photo/repair_file/3/' . $key . '/image_'.$k.'.png';

                file_put_contents($uploaded_dir.'/image_'.$k.'.png', $data);

                $rdf_insert = [
                    'repair_details_id' => $key,
                    'url'    => $url,
                    'type'   => 3
                ];
                $QRepairDetailsFile->insert($rdf_insert);
                
                $tag = 1;
            }
        }
        
        if($tag != 1){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload file hình ảnh nghiệm thu.');
            $this->_redirect(HOST . 'trade/repair-edit?id='.$repair_id);
        }
        
        foreach($repair_details_id as $key=>$value){
            // Insert quotation
            //Cập nhật lại quotation cũ = 1 (hủy)
            $where = [];
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('repair_details_id = ?', $value);
            $where[] = $QRepairQuotation->getAdapter()->quoteInto('status = 2');
            $QRepairQuotation->update(['status' => 3],$where);
            foreach ($repair_quotation_title [$value] as $index => $title) {
                $data_quotation = [
                    'repair_details_id' => $value,
                    'title' => $title,
                    'quantity' => $repair_quotation_quantity [$value] [$index],
                    'total_price' => $repair_quotation_total_price [$value] [$index],
                    'status' => 2
                ];
                $QRepairQuotation->insert($data_quotation);
            }
        }
        
        $QRepair->update(['status' => 5], $where_repair);
        
        
    }//if($repair['status'] == 4)
    
    //5: Chờ TMK Leader xác nhận hoàn thành
    if($repair['status'] == 5){
        $QRepair->update(['status' => 6], $where_repair);
    }
    
    
    //REPAIR CONFIRM
    $rp_confirm = [
        'repair_id' => $repair_id,
        'repair_status' => $repair['status'],
        'confirm_by' => $this->storage['staff_id'],
        'created_at'    => date('Y-m-d H:i:s')
    ];
    $QRepairConfirm->insert($rp_confirm);
    //END REPAIR CONFIRM
    
    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/repair-edit?id='.$repair_id);
    

} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
    $this->_redirect(HOST . 'trade/repair-edit?id='.$repair_id);

}

?>