<?php
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$submit            = $this->getRequest()->getParam('submit');
$high            = $this->getRequest()->getParam('high');
$width            = $this->getRequest()->getParam('width');
$long            = $this->getRequest()->getParam('long');
$location            = $this->getRequest()->getParam('location');
//SEARCH
$name_search            = $this->getRequest()->getParam('name_search');
$area_id_search         = $this->getRequest()->getParam('area_id_search');
$sale_id            	= $this->getRequest()->getParam('staff_id');

$QAppCheckShop 			= new Application_Model_AppCheckshop();
$QAppCheckShopDetail 	= new Application_Model_AppCheckshopDetail();
$QAppCheckShop 			= new Application_Model_AppCheckshop();
$QArea 					= new Application_Model_Area();
$QCheckshopCategorySize = new Application_Model_CheckshopCategorySize();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger       = $this->_helper->flashMessenger;
$limit = 10;
$total = 0;
$params = [

	'name_search' 		=> $name_search,
	'area_id_search'	=> $area_id_search,
	'sale_id'			=> $sale_id,
	'list_area'      => $this->storage['area_id']
];
$params['sort'] = $sort;
$params['desc'] = $desc;
$params['area_id'] = $this->storage['area_id'];


$resultList = $QAppCheckShop->getShopCategorySize($page, $limit, $total, $params);

$list_store = [];
foreach ($resultList as $key => $value) {
	$list_store[]=$value['store_id'];
}
$data_details = $QAppCheckShop->getDataDetailListStore($list_store);

$data = [];
foreach ($data_details as $key => $value) {
	$data[$value['store_id']][] = $value;
}

$list_sale  = $QAppCheckShop->getSaleArea();
$area_list = $QArea->getAreaList($params);
// echo '<pre>';
// var_dump($data); exit;
$this->view->data = $data;

$this->view->area = $area_list;
$this->view->list_sale = $list_sale;
$this->view->list= $resultList;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->url = HOST.'trade/checkshop-category-size'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit * ($page - 1);



if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

?>
