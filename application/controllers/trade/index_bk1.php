    <?php
//    $month = $this->getRequest()->getParam('month');
//    $month = $month ? $month : date('m');
//    $year = $year ? $year : date('Y');
    $fromDate = $this->getRequest()->getParam('from_date');
    $fromDate = $fromDate ? date('Y-m-d', strtotime($fromDate)) : date('Y-m-01');

    $toDate = $this->getRequest()->getParam('to_date');
    $toDate = $toDate ? date('Y-m-d', strtotime($toDate)) : date('Y-m-t');

    $year = date('Y');
    $areaId = $this->getRequest()->getParam('area_id');

    $QAppNoti = new Application_Model_AppNoti();
    $QBudget = new Application_Model_Budget();
    $QArea = new Application_Model_Area();

    $params = [
        'area_list' => $this->storage['area_id'],
        'list_area' => $this->storage['area_id'],
        'title' => $this->storage['title'],
        'year' => date('Y'),
        'month' => $month,
        'year' => $year,
        'from_date' => $fromDate,
        'to_date' => $toDate,
        'area_id' => $areaId
    ];

    if (in_array($this->storage['title'], [SALES_TITLE])) {
        $params['staff_id'] = $this->storage['staff_id'];
    }

    $params['type'] = 4;
    $params['table'] = DATABASE_TRADE . '.repair';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'staff_id';
    $repair = $QAppNoti->getPendingRepair($params);
    $repair_success = $QAppNoti->getSuccessRepair($params);

    $params['type'] = 12;
    $params['table'] = DATABASE_TRADE . '.destruction';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'staff_id';
    $destruction = $QAppNoti->getPendingRepair($params);
    $destruction_success = $QAppNoti->getSuccessRepair($params);


    $params['type'] = 11;
    $params['table'] = 'remove_shop';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'created_by';
    $remove_shop = $QAppNoti->getPendingRepair($params);
    $remove_shop_success = $QAppNoti->getSuccessRepair($params);

    $params['type'] = 6;
    $params['table'] = DATABASE_TRADE . '.transfer';
    $params['store_id'] = 'store_from';
    $params['created_by'] = 'staff_id';
    $transfer = $QAppNoti->getPendingRepair($params);
    $transfer_success = $QAppNoti->getSuccessRepair($params);

    $params['type'] = 8;
    $params['table'] = DATABASE_TRADE . '.app_air';
    $params['store_id'] = 'store_id';
    $params['created_by'] = 'created_by';
    $air = $QAppNoti->getPendingRepair($params);
    $air_success = $QAppNoti->getSuccessRepair($params);

    // Ngân sách theo tháng
    $params['cost_type'] = 1;
    $destructionCost = $QAppNoti->getDestructionCost($params);
    $transferCost = $QAppNoti->getTransferCost($params);
    $repairCost = $QAppNoti->getRepairCost($params);

    $params['additional_part_type'] = 8; // thi công
    $airCost = $QAppNoti->getAirCost($params);
    $additionalAirCost = $QAppNoti->getAdditionalCost($params);

    $posmCost = $QAppNoti->getPosmCost($params);
    $additionalPosmCost = $QAppNoti->getAdditionalPosmCost($params);

    $params['additional_part_type'] = 2; // đặt hàng
    $orderCost = $QAppNoti->getAdditionalCost($params); // chi phí đặt hàng bổ sung do trên hệ thống chưa có đặt hàng

    //    $totalCost = $destructionCost + $transferCost + $repairCost['ia'] + $repairCost['ka'] + $airCost + $posmCost;
    $totalCost =  $airCost + $additionalAirCost + $posmCost + $additionalPosmCost + $orderCost;

    $listArea = $QArea->getListArea($params);

    // Ngân sách theo năm
    unset($params['month']);
    $destructionCostYear = $QAppNoti->getDestructionCost($params);
    $transferCostYear = $QAppNoti->getTransferCost($params);
    $repairCostYear = $QAppNoti->getRepairCost($params);
    $airCostYear = $QAppNoti->getAirCostYear($params);
    $posmCostYear = $QAppNoti->getPosmCostYear($params);
    $orderCostYear = $QAppNoti->getOrderCostYear($params);
    //    $totalCostYear = $destructionCostYear + $transferCostYear + $repairCostYear['ia'] + $repairCostYear['ka'] + $airCostYear + $posmCostYear;

    $params ['season'] = 1;
    $params['additional_part_type'] = 8; // thi công
    $additionalAirCostSeason = $QAppNoti->getAdditionalCostSeason($params); // chi phí thi công bổ sung quí 1 năm 2019
    $additionalPosmCostSeason = $QAppNoti->getAdditionalPosmCostSeason($params);

    $totalCostYear =   $airCostYear + $additionalAirCostSeason + $posmCostYear + $additionalPosmCostSeason + $orderCostYear ;
    $totalBudget = $QBudget->getTotalBudget($params);

    $remainBudget = $totalBudget - $totalCostYear;

    $checkshop_info = $QAppNoti->getCheckshopInfo($params);


    $this->view->repair_pending = count($repair);
    $this->view->destruction_pending = count($destruction);
    $this->view->remove_shop_pending = count($remove_shop);
    $this->view->transfer_pending = count($transfer);
    $this->view->air_pending = count($air);

    $this->view->repair_success = $repair_success;
    $this->view->destruction_success = $destruction_success;
    $this->view->remove_shop_success = $remove_shop_success;
    $this->view->transfer_success = $transfer_success;
    $this->view->air_success = $air_success;


    $this->view->total_pending_self = count($repair) + count($destruction) + count($transfer) + count($air) + count($remove_shop);
    $this->view->total_success = $repair_success['success'] + $destruction_success['success'] + $transfer_success['success'] + $air_success['success'] + $remove_shop_success['success'];
    $this->view->total_pending = $repair_success['pending'] + $destruction_success['pending'] + $transfer_success['pending'] + $air_success['pending'] + $remove_shop_success['pending'];

    $this->view->title = $this->storage['title'];
    $this->view->checkshop_info = $checkshop_info;

    $this->view->destructionCost = $destructionCost;
    $this->view->transferCost = $transferCost;
    $this->view->repairCost = $repairCost;
    $this->view->airCost = $airCost;
    $this->view->posmCost = $posmCost;
    $this->view->orderCost = $orderCost;
    $this->view->totalCost = $totalCost;
    $this->view->totalBudget = $totalBudget;
    $this->view->remainBudget = $remainBudget;
    $this->view->year = $year;
    $this->view->month = $month;
    $this->view->params = $params;
    $this->view->listArea = $listArea;
    $this->view->additionalAirCost = $additionalAirCost;
    $this->view->additionalPosmCost = $additionalPosmCost;





