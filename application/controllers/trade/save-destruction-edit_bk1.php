<?php

$destruction_id = $this->getRequest()->getParam('destruction_id');
$destruction_details_id = $this->getRequest()->getParam('destruction_details_id');
$contructors_id = $this->getRequest()->getParam('contructors_id');
$price = $this->getRequest()->getParam('price');
$price_final = $this->getRequest()->getParam('price_final');
$file = $this->getRequest()->getParam('file');
$reject = $this->getRequest()->getParam('reject');
$reject_note = $this->getRequest()->getParam('reject_note');
$check_tp = $this->getRequest()->getParam('check_tp');
$remove = $this->getRequest()->getParam('remove');
$remove_note = $this->getRequest()->getParam('remove_note');

$destruction_date = $this->getRequest()->getParam('destruction_date');
$image = $this->getRequest()->getParam('image');

$destruction_quotation_title = $this->getRequest()->getParam('destruction_quotation_title');
$destruction_quotation_quantity = $this->getRequest()->getParam('destruction_quotation_quantity');
$destruction_quotation_total_price = $this->getRequest()->getParam('destruction_quotation_total_price');

$part = $this->getRequest()->getParam('part');
$part_quantity = $this->getRequest()->getParam('part_quantity');
$manager_approve = $this->getRequest()->getParam('manager_approve');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QDestruction = new Application_Model_Destruction();
$QDestructionDetails = new Application_Model_DestructionDetails();
$QDestructionDetailsFile = new Application_Model_DestructionDetailsFile();
$QDestructionDetailsPart = new Application_Model_DestructionDetailsPart();
$QDestructionConfirm = new Application_Model_DestructionConfirm();
$QDestructionQuotation = new Application_Model_DestructionQuotation();


$where_destruction = $QDestruction->getAdapter()->quoteInto('id = ?', $destruction_id);
$destruction = $QDestruction->fetchRow($where_destruction);

$where = $QDestructionDetails->getAdapter()->quoteInto('destruction_id = ?', $destruction_id);
$destruction_details = $QDestructionDetails->fetchAll($where);

$db = Zend_Registry::get('db');
$db->beginTransaction();


try {
    // Hủy đề xuất
    if ($remove) {
        $remove_date = [
            'remove' => 1,
            'remove_at' => date('Y-m-d H:i:s'),
            'remove_by' => $userStorage->id,
            'remove_note' => $remove_note
        ];
        $QDestruction->update($remove_date, $where_destruction);

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
    }

    //Reject
    if ($reject) {
        if ($destruction['status'] < 5) {
            $destruction_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 1
            ];
        }

        if ($destruction['status'] > 5) {
            $destruction_data = [
                'reject' => 1,
                'reject_note' => $reject_note,
                'status' => 5
            ];
        }

        if ($destruction['status'] == 5) {
            $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
        }


        $QDestruction->update($destruction_data, $where_destruction);
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success!');
        $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);


    }
    //END Reject


    //1: Chờ TMK Local xác nhận
    if ($destruction['status'] == 1) {
        if ($check_tp == 0) { //nếu là hủy không phí
            $QDestruction->update(['status' => 2], $where_destruction);
        } else {
            $tag = 0;
            foreach ($destruction_details_id as $key => $value) {
                $where = $QDestructionDetails->getAdapter()->quoteInto('id = ?', $value);

                $data = [
                    'contructors_id' => !empty($contructors_id[$key]) ? $contructors_id[$key] : 0,
                    'price' => !empty($price[$key]) ? $price[$key] : 0,
                ];
                $QDestructionDetails->update($data, $where);

                $where = [];
                $where[] = $QDestructionDetailsFile->getAdapter()->quoteInto('destruction_details_id = ?', $value);
                $where[] = $QDestructionDetailsFile->getAdapter()->quoteInto('type = ?', 2);
                $QDestructionDetailsFile->delete($where);

                // $upload = new Zend_File_Transfer();

                // $upload->addValidator('FilesSize',false,array('max' => '10MB'));
                // if (!$upload->isValid()) {

                //     $db->rollBack();
                //     $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Dung lượng file vượt mức cho phép. !");
                //     $this->redirect(HOST.'trade/destruction-edit?id='.$destruction_id);
                // }

                foreach ($_FILES['file']['name'][$value] as $k => $v) {

                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $value;

                    if (!is_dir($uploaded_dir))
                        @mkdir($uploaded_dir, 0777, true);

                    $tmpFilePath = $_FILES['file']['tmp_name'][$value][$k];

                    $old_name = $v;
                    $tExplode = explode('.', $old_name);
                    $extension = end($tExplode);
                    $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                    $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $url = 'photo' .
                            DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . '2' . DIRECTORY_SEPARATOR . $value . DIRECTORY_SEPARATOR . $new_name;

                        $dtf = [
                            'destruction_details_id' => $value,
                            'url' => '/' . $url,
                            'type' => 2
                        ];
                        $QDestructionDetailsFile->insert($dtf);

                        $tag = 1;
                    }
                }

                foreach ($part[$value] as $k => $v) {
                    $dtp = [
                        'destruction_details_id' => $value,
                        'destruction_part_id' => $v,
                        'quantity' => $part_quantity[$value][$k],
                        'status' => 1
                    ];
                    $QDestructionDetailsPart->insert($dtp);
                }

                //Cập nhật lại quotation cũ = 1 (hủy)
                $where = [];
                $where[] = $QDestructionQuotation->getAdapter()->quoteInto('destruction_details_id = ?', $value);
                $where[] = $QDestructionQuotation->getAdapter()->quoteInto('status = 0');
                $QDestructionQuotation->update(['status' => 1], $where);
                // Insert quotation

                foreach ($destruction_quotation_title [$value] as $index => $title) {
                    if ($title && $destruction_quotation_total_price [$value] [$index]) {
                        $data_quotation = [
                            'destruction_details_id' => $value,
                            'title' => $title,
                            'quantity' => $destruction_quotation_quantity [$value] [$index] ? $destruction_quotation_quantity [$value] [$index] : NULL,
                            'total_price' => $destruction_quotation_total_price [$value] [$index],
                            'status' => 0
                        ];
                        $QDestructionQuotation->insert($data_quotation);

                    } else {
                        $flashMessenger->setNamespace('error')->addMessage('Vui lòng nhập đầy đủ nội dung và thành tiền trong upload báo giá!');
                        $this->_redirect(HOST . 'trade/destruction-edit?id='.$destruction_id);
                    }

                }

            }

            $QDestruction->update(['status' => 2], $where_destruction);

//                if($tag == 1){
//                    $QDestruction->update(['status' => 2], $where_destruction);
//                }
//                else{
//                    $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload file báo giá.');
//                    $this->_redirect(HOST . 'trade/destruction-edit?id='.$destruction_id);
//                }
        }


    }//$destruction['status'] == 1

    //2: Chờ TMK Leader xác nhận
    if ($destruction['status'] == 2) {
        $QDestruction->update(['status' => 3], $where_destruction);
    }

    //3: Chờ ASM xác nhận
    if ($destruction['status'] == 3) {
        if (!empty($manager_approve) && $manager_approve == 1) {
            $QDestruction->update(['status' => 4], $where_destruction);
        } else {
            $QDestruction->update(['status' => 5], $where_destruction);
        }
    }

    //4: Chờ TMK Manager xác nhận
    if ($destruction['status'] == 4) {
//        if($check_tp == 0)
//        {
//            $QDestruction->update(['status' => 6], $where_destruction);
//        }else{
//            $QDestruction->update(['status' => 6], $where_destruction);
//        }
        $QDestruction->update(['status' => 5], $where_destruction);


    }

    //5: Chờ TMK xác nhận thi công
    // if($destruction['status'] == 5){

    //     foreach($destruction_details_id as $key=>$value){
    //         $where = $QDestructionDetails->getAdapter()->quoteInto('id = ?', $value);

    //         $rp_date = str_replace('/', '-', $destruction_date[$key]);
    //         $rp_date = date('Y-m-d', strtotime($rp_date));

    //         $QDestructionDetails->update(['destruction_date' => $rp_date], $where);
    //     }    

    //     $QDestruction->update(['status' => 6], $where_destruction);
    // }

    //6: Chờ Trade local nghiệm thu
    if ($destruction['status'] == 5) {
        //upload ảnh nghiệm thu
        $tag = 0;
        foreach ($image as $key => $value) {
            $where = [];
            $where[] = $QDestructionDetailsFile->getAdapter()->quoteInto('destruction_details_id = ?', $key);
            $where[] = $QDestructionDetailsFile->getAdapter()->quoteInto('type = ?', 3);
            $QDestructionDetailsFile->delete($where);

            foreach ($value as $k => $v) {
                $data = $v;

                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'destruction_file' . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $key;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $url = '/photo/destruction_file/3/' . $key . '/image_' . $k . '.png';

                file_put_contents($uploaded_dir . '/image_' . $k . '.png', $data);

                $rdf_insert = [
                    'destruction_details_id' => $key,
                    'url' => $url,
                    'type' => 3
                ];

                $QDestructionDetailsFile->insert($rdf_insert);

                $tag = 1 ;
            }
        }
        if ($tag != 1) {
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng Upload file hình ảnh nghiệm thu.');
            $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);
        }

        // Insert quotation
        //Cập nhật lại quotation cũ = 3 (hủy)
        foreach ($destruction_details_id as $key => $value) {
            $where = [];
            $where[] = $QDestructionQuotation->getAdapter()->quoteInto('destruction_details_id = ?', $value);
            $where[] = $QDestructionQuotation->getAdapter()->quoteInto('status = 2');
            $QDestructionQuotation->update(['status' => 3], $where);

            foreach ($destruction_quotation_title [$value] as $index => $title) {
                if ($title) {
                    $data_quotation = [
                        'destruction_details_id' => $value,
                        'title' => $title,
                        'quantity' => $destruction_quotation_quantity [$value] [$index] ? $destruction_quotation_quantity [$value] [$index] : NULL,
                        'total_price' => $destruction_quotation_total_price [$value] [$index],
                        'status' => 2
                    ];
                    $QDestructionQuotation->insert($data_quotation);
                }
            }

        }

        $QDestruction->update(['status' => 6], $where_destruction);
    }

    //7: Chờ TMK Leader xác nhận hoàn thành
    if ($destruction['status'] == 6) {
        $QDestruction->update(['status' => 7], $where_destruction);
    }


    //DESTRUCTION CONFIRM
    $destruction_confirm = [
        'destruction_id' => $destruction_id,
        'destruction_status' => $destruction['status'],
        'confirm_by' => $this->storage['staff_id']
    ];
    $QDestructionConfirm->insert($destruction_confirm);
    //END DESTRUCTION CONFIRM

    $db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Success!');
    $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);


} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage('Error Sytems: ' . $e->getMessage());
    $this->_redirect(HOST . 'trade/destruction-edit?id=' . $destruction_id);

}

?>