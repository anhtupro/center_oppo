<?php 
	
	$campaign_id 			= $this->getRequest()->getParam('campaign_id');
	$contractor_quantity_id = $this->getRequest()->getParam('contractor_quantity_id');
	$area_id 				= $this->getRequest()->getParam('area_id');
	$quantity 				= $this->getRequest()->getParam('quantity');
	$transporter_id 		= $this->getRequest()->getParam('transporter_id');
	$price 					= $this->getRequest()->getParam('price');

	$data_order = json_decode($data_order, true);

	$flashMessenger = $this->_helper->flashMessenger;
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id = $userStorage->id;

	$back_url = HOST.'trade/share-quantity?campaign_id='.$campaign_id;

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		$QAppContractorOut = new Application_Model_AppContractorOut();

		foreach($area_id as $key=>$value){
			$data = [
				'contractor_quantity_id'	=> $contractor_quantity_id,
				'area_id'					=> $value,
				'quantity'					=> $quantity[$key],
				'created_by'				=> $staff_id,
				'transporter_id'			=> $transporter_id[$key],
				'price'						=> $price[$key]
			];
			$QAppContractorOut->insert($data);
		}

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Đăng ký thành công!');
    	$this->redirect($back_url);


	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;




