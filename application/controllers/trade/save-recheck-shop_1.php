<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$listQuantity = $this->getRequest()->getParam('quantity');
$roundId = $this->getRequest()->getParam('round_id');
$storeId = $this->getRequest()->getParam('store_id');
$note = $this->getRequest()->getParam('note');
$errorImage = $this->getRequest()->getParam('error_image');
$appCheckShopId = $this->getRequest()->getParam('app_check_shop_id');
$listErrorNote = $this->getRequest()->getParam('error_note');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QRecheckShop = new Application_Model_RecheckShop();
$QRecheckShopDetail = new Application_Model_RecheckShopDetail();
$QRecheckShopFile = new Application_Model_RecheckShopFile();
$QRecheckShopRound = new Application_Model_RecheckShopRound();
$QRecheckShopErrorImage = new Application_Model_RecheckShopErrorImage();

$isRunning = $QRecheckShopRound->isRunning($roundId);
if (! $isRunning) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đợt đánh giá này đã kết thúc.'
    ]);
    return;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();


try {

    $whereUpdate = [
        'recheck_shop_round_id = ?' => $roundId,
        'store_id = ?' => $storeId
    ];
    $QRecheckShop->update([
        'status' => 0
    ],$whereUpdate);

    $recheckShopId = $QRecheckShop->insert([
        'store_id' => $storeId,
        'recheck_shop_round_id' => $roundId,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
        'app_check_shop_id' => $appCheckShopId ? $appCheckShopId : NULL,
        'note' => $note,
        'error_image' => $errorImage,
        'status' => 1
    ]);

    if ($listQuantity) {
        foreach ($listQuantity as $categoryId => $quantity) {
            $QRecheckShopDetail->insert([
                'recheck_shop_id' => $recheckShopId,
                'category_id' => $categoryId,
                'quantity' => $quantity
            ]);
        }
    }

    if ($listErrorNote) {
        foreach ($listErrorNote as $errorId => $errorNote) {
            $QRecheckShopErrorImage->insert([
                'recheck_shop_id' => $recheckShopId,
                'error_image_id' => $errorId,
                'note' => $errorNote ? $errorNote : NULL
            ]);
        }
    }


    // Chỉnh sửa array $_FILES lại cho dễ xử lý
    $normalized_array = [];

    foreach($_FILES as $index => $file) {

        if (!is_array($file['name'])) {
            $normalized_array[$index][] = $file;
            continue;
        }

        foreach($file['name'] as $idx => $name) {
            if($name) {
                $normalized_array[$index][$idx] = [
                    'name' => $name,
                    'type' => $file['type'][$idx],
                    'tmp_name' => $file['tmp_name'][$idx],
                    'error' => $file['error'][$idx],
                    'size' => $file['size'][$idx]
                ];
            }
        }
    }
    //

    foreach ($normalized_array as $nameAttr => $arrImage) {

        $target_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
            DIRECTORY_SEPARATOR . 'recheck_shop' . DIRECTORY_SEPARATOR . $recheckShopId . DIRECTORY_SEPARATOR;
        if (!is_dir($target_dir)) {
            @mkdir($target_dir, 0775, true);
        }
        foreach ($arrImage as $key => $image) {
            $extension = strtolower(pathinfo($image['name'],PATHINFO_EXTENSION));
            $newName = md5(date('Y-m-d H:i:s') . rand(1,1000)) . '.' . $extension;
            $target_file = $target_dir . $newName;

            // Check file size
            if ($image["size"] > 20000000) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'File is too big'
                ]);
                return;

            }
            // check upload file
            if (! move_uploaded_file($image["tmp_name"], $target_file)) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Có lỗi trong quá trình upload file, xin vui lòng thử lại.'
                ]);
                return;
            }

            $path = '/photo/recheck_shop/' . $recheckShopId . '/' . $newName;
            
            //Resize Anh
            $image = new My_Image_Resize();
            $image->load($target_file);
            $image->resizeToWidth(800);
            $image->save($target_file);
            //END Resize

            $QRecheckShopFile->insert([
               'url' => $path,
                'type' => $key,
                'recheck_shop_id' => $recheckShopId
            ]);
        }
    }
    // end save photo

    $db->commit();

    echo json_encode([
        'status' => 0,
		'id'	=> $recheckShopId
    ]);

}catch (\Exception $e){
    echo json_encode([
       'status' => 1,
       'message' => $e->getMessage()
    ]);
}