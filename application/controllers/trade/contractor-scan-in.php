<?php 

	$id_contractor_quantity    = $this->getRequest()->getParam('id_contractor_quantity');
	$list_imei   			   = $this->getRequest()->getParam('list_imei');

	$QImeiTrade 				= new Application_Model_ImeiTrade();
	$QAppOrderDetails 			= new Application_Model_AppOrderDetails();
	$QAppContractorIn 			= new Application_Model_AppContractorIn();
	$QAppContractorQuantity 	= new Application_Model_AppContractorQuantity();

	$where = $QAppContractorQuantity->getAdapter()->quoteInto('id = ?', $id_contractor_quantity );
	$contractor_quantity = $QAppContractorQuantity->fetchRow($where);

	$list_imei = explode(',', rtrim($list_imei));


	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{
		foreach ($list_imei as $key => $value) {

			$imei_sn = RTRIM(LTRIM($value));

			if($imei_sn != ''){

				$where = $QAppContractorIn->getAdapter()->quoteInto('imei_sn = ?', $imei_sn );
				$contractor_in = $QAppContractorIn->fetchAll($where);

				$where = [];
				$where[] = $QAppContractorIn->getAdapter()->quoteInto('contractor_id = ?', $contractor_quantity['contractor_id'] );
				$where[] = $QAppContractorIn->getAdapter()->quoteInto('campaign_id = ?', $contractor_quantity['campaign_id'] );
				$where[] = $QAppContractorIn->getAdapter()->quoteInto('category_id = ?', $contractor_quantity['category_id'] );
				$in    	 = $QAppContractorIn->fetchAll($where);

				$where 	 = $QImeiTrade->getAdapter()->quoteInto('imei_sn = ?', $imei_sn );
				$imei 	 = $QImeiTrade->fetchRow($where);


				if(count($contractor_in)){
					echo 'IMEI: '.$value.' đã được quét nhập kho!';exit;
				}

				if(!$contractor_quantity){
					echo 'Contractor Quantity trống!';exit;
				}

				if(!$imei){
					echo 'IMEI: '.$value.' không tồn tại!';exit;
				}

				if( $imei['good_id'] != $contractor_quantity['category_id'] ){
					//echo 'IMEI: '.$value.' không thuộc hạng mục đang Scan!';exit;
				}

				if(count($in) >= $contractor_quantity['quantity'] ){
					echo 'Đã scan đủ số lượng!';exit;
				}
				
				$data = [
					'contractor_id' 		 => $contractor_quantity['contractor_id'],
					'campaign_id' 		 	 => $contractor_quantity['campaign_id'],
					'category_id'			 => $contractor_quantity['category_id'],
					'imei_sn'				 => $imei_sn,
					'created_at'			 => date('Y-m-d H:i:s')
				];
				$QAppContractorIn->insert($data);

				
				//Cập nhật lại status app_order_details
				$db = Zend_Registry::get('db');
                $sql = "UPDATE `".DATABASE_TRADE."`.`app_order_details` p
						LEFT JOIN `".DATABASE_TRADE."`.`app_order` o ON o.id = p.order_id
						SET p.`status` = 7
						WHERE p.`status` = 6 AND o.campaign_id = :campaign_id AND p.category_id = :category_id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('campaign_id', $contractor_quantity['campaign_id'], PDO::PARAM_INT);
                $stmt->bindParam('category_id', $contractor_quantity['category_id'], PDO::PARAM_INT);
                $stmt->execute();

                $stmt->closeCursor();
				//END Cập nhật lại status app_order_details
			}
				
		}

		$db->commit();
		echo 'success';exit;

	}
	catch (Exception $e)
	{
		$db->rollBack();

	    echo "Có lỗi: ".$e->getMessage();
	    exit;
	}




