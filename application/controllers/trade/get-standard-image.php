<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$store_id = $this->getRequest()->getParam('store_id');

$QStandardImage = new Application_Model_StandardImage();

$list_image = $QStandardImage->fetchAll(['store_id = ?' => $store_id]);

if ($list_image) {
    $list_image = $list_image->toArray();
}

echo json_encode([
    'status' => 0,
    'list_image' => $list_image
]);