<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$store_id = $this->getRequest()->getParam('store_id');
$status = $this->getRequest()->getParam('status');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$QStoreTempStop = new Application_Model_StoreTempStop();
$QStoreTempStopLog = new Application_Model_StoreTempStopLog();

if (!$store_id || !$status) {
    echo  json_encode([
       'status' => 1,
       'message' => 'Chưa chọn câu trả lời!'
    ]);

    return;
}

$exist_store = $QStoreTempStop->fetchRow(['store_id = ?' => $store_id]);

if ($exist_store) {
    $QStoreTempStop->update([
        'status' => $status
    ], ['id = ?' => $exist_store['id']]);
} else {
    if ($status == 3) {
        echo  json_encode([
            'status' => 1,
            'message' => 'Shop đang hoạt động bình thường. Vui lòng chọn câu trả lời khác !'
        ]);

        return;
    }

    $QStoreTempStop->insert([
        'store_id' => $store_id,
        'status' => $status
    ]);
}


$QStoreTempStopLog->insert([
   'store_id' => $store_id,
   'status' => $status,
   'created_by' => $userStorage->id,
   'created_at' => date('Y-m-d H:i:s')
]);



echo json_encode([
    'status' => 0
]);
