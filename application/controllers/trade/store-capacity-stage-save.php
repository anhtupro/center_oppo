<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
include 'PHPExcel/IOFactory.php';
// config for excel template excel
define('START_ROW', 3);
define('START_CATEGORY_ID_COLUMN', 1);
define('STORE_ID', 0);


$name = $this->getRequest()->getParam('name');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$stage_id = $this->getRequest()->getParam('stage_id');
$list_capacity = $this->getRequest()->getParam('capacity_id');

$QStoreCapacityStage = new Application_Model_StoreCapacityStage();
$QStoreCapacityAssign = new Application_Model_StoreCapacityAssign();

$db = Zend_Registry::get('db');
$db->beginTransaction();


if ($stage_id) {
    $QStoreCapacityStage->update([
        'name' => $name,
        'from_date' => $from_date ? date('Y-m-d', strtotime($from_date)) : NULL,
        'to_date' => $to_date ? date('Y-m-d', strtotime($to_date)) : NULL
    ], ['id = ?' => $stage_id]);

} else {
    $stage_id_insert = $QStoreCapacityStage->insert([
        'name' => $name,
        'from_date' => $from_date ? date('Y-m-d', strtotime($from_date)) : NULL,
        'to_date' => $to_date ? date('Y-m-d', strtotime($to_date)) : NULL
    ]);
}

$stage_id = $stage_id ? $stage_id : $stage_id_insert;

// assign capacity
$QStoreCapacityAssign->delete(['stage_id = ?' => $stage_id]);
foreach ($list_capacity as $value_capacity_id) {
    $QStoreCapacityAssign->insert([
        'stage_id' => $stage_id,
        'capacity_id' => $value_capacity_id
    ]);
}
//


$db->commit();

echo json_encode([
    'status' => 0
]);