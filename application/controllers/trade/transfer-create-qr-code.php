<?php
$category_id   = $this->getRequest()->getParam('category');
$store_id   = $this->getRequest()->getParam('store');
$store_to   = $this->getRequest()->getParam('store_to');
$reason	    = $this->getRequest()->getParam('reason');

$imei_sn    = $this->getRequest()->getParam('imei');
$submit	    = $this->getRequest()->getParam('submit');
$area	    = $this->getRequest()->getParam('area');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QDestruction = new Application_Model_Destruction();
$QTransfer = new Application_Model_Transfer();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QImei = new Application_Model_ImeiTrade();
$QArea = new Application_Model_Area();
$QReason = new Application_Model_AppReason ();

$flashMessenger       = $this->_helper->flashMessenger;
$params = array_filter(array(
	'category' => $category_id,
	'store'=>$store_id,
	'submit'=>$submit,
	'store_to'=>$store_to,
	'area'=>$area
));

if (!empty($submit)) {
			//---------KIEM TRA XEM CO UP FILE CHUA?----------
	if(isset($imei_sn) and $imei_sn){
		$params['id'] = $imei_sn;
		$result = $QImei->GetId($params);
		if(!empty($result['category_id']) && !empty($result['store_id'])){
			$category_id=$result['category_id'];
			$store_id=$result['store_id'];
		}else {
			$flashMessenger->setNamespace('error')->addMessage('Đề xuất không thành công. Mã code không hợp lệ ');
			$this->_redirect(HOST . 'trade/transfer-create');
		}
	}
	$upload = new Zend_File_Transfer();
	$upload->setOptions(array('ignoreNoFile'=>true));

	            //check function
	if (function_exists('finfo_file'))
		$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));

	$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
	$upload->addValidator('Size', false, array('max' => '10MB'));
	$upload->addValidator('ExcludeExtension', false, 'php,sh');
	$files = $upload->getFileInfo();

		if($files['image']['error']==0){ // NEU UP FILE KO LOI THI TIEN HANH INSERT
			try{
				$result_query =$QTransfer->insert(array(
					'store_from'=>$store_id,
					'store_to'=>$store_to,
					'category_id'=>$category_id,
					'create_at'=>date('Y-m-d H:i:s'),
					'update_at'=>date('Y-m-d H:i:s'),
					'staff_id'=>$userStorage->id,
					'status'=> 10,
					'reason_id'=>$reason
				));

				$data_file = array();

				$fileInfo['image'] = (isset($files['image']) and $files['image']) ? $files['image'] : null;
				$fileInfo['imageoutside'] = (isset($files['imageoutside']) and $files['imageoutside']) ? $files['imageoutside'] : null;
				$fileInfo['imageoutside1'] = (isset($files['imageoutside1']) and $files['imageoutside1']) ? $files['imageoutside1'] : null;
				foreach ($fileInfo as $key => $value) {
					$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
					DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
					DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . 'transfer_file' . DIRECTORY_SEPARATOR . $result_query;
					if (!is_dir($uploaded_dir))
						@mkdir($uploaded_dir, 0777, true);
					$upload->setDestination($uploaded_dir);

            //Rename
					$old_name =$value['name'];
					$tExplode = explode('.', $old_name);
					$extension = end($tExplode);
					$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
					$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
					$r = $upload->receive(array($key));
					if($r){
						$data_file['file_name'] = $new_name;
					}
					else{
						$messages = $upload->getMessages();
						foreach ($messages as $msg)
							throw new Exception($msg);
					}
					// --------------INSERT HINH ANH LEN DATABASE-------------
					$data_file['parrent_id'] = $result_query;
					$data_file['type']=2;
					$result =$QAppFile->insert($data_file);
				}
				

				if($files['image_shop']['error']==0 && $files['imageshopoutside']['error']==0){ ///  -----------------INSERT HÌNH ẢNH SHOP CẦN CHUYỂN ĐẾN- --
					$data_file = array();

					$fileShopInfo['image_shop'] = (isset($files['image_shop']) and $files['image_shop']) ? $files['image_shop'] : null;
					$fileShopInfo['imageshopoutside'] = (isset($files['imageshopoutside']) and $files['imageshopoutside']) ? $files['imageshopoutside'] : null;
					$fileShopInfo['imageshopoutside1'] = (isset($files['imageshopoutside1']) and $files['imageshopoutside1']) ? $files['imageshopoutside1'] : null;
					foreach ($fileShopInfo as $key => $value) {
						$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
						DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
						DIRECTORY_SEPARATOR . 'transfer' . DIRECTORY_SEPARATOR . 'transfer_file' . DIRECTORY_SEPARATOR . $result_query;
						if (!is_dir($uploaded_dir))
							@mkdir($uploaded_dir, 0777, true);
						$upload->setDestination($uploaded_dir);

            //Rename
						$old_name =$value['name'];
						$tExplode = explode('.', $old_name);
						$extension = end($tExplode);
						$new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
						$upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
						$r = $upload->receive(array($key));
						if($r){
							$data_file['file_name'] = $new_name;
						}
						else{
							$messages = $upload->getMessages();
							foreach ($messages as $msg)
								throw new Exception($msg);
						}
					// --------------INSERT HINH ANH LEN DATABASE-------------
						$data_file['parrent_id'] = $result_query;
						$data_file['type']=3;
						$result =$QAppFile->insert($data_file);
					}
				}

				$flashMessenger->setNamespace('success')->addMessage('Đề xuất thành công ');
				$this->_redirect(HOST . 'trade/transfer-edit?id='.$result_query);

			}
			catch (exception $e) {
				
				$this->view->params = $params;
				if($result_query ){
					$whereRepair = $QRepair->getAdapter()->quoteInto('id = ?',$result_query);
					$QRepair->delete($whereRepair);
					$whereAppfile = $QAppFile->getAdapter()->quoteInto('type ="2" and parrent_id = ?',$result_query);
					$QAppFile->delete($whereAppfile);
				}
				$flashMessenger->setNamespace('error')->addMessage('Đề xuất không thành công. Vui lòng kiểm tra lại thông tin và hình ảnh');
				$this->_redirect(HOST . 'trade/transfer-create?category='.$category_id.'&store='.$store_id.'&store_to='.$store_to);
			}

		}
		
	}
			//----------LAY DANH SACH LY DO-----------
	$resultReason = $QReason->GetAll();
	$this->view->reason = $resultReason;

	$this->view->params = $params;
	// -------------RETURN CATEGORY ---------------
	$result = $QCategory->GetAll();
	$this->view->category = $result;
	// -------------RETURN STORE ---------------
	$paramsData['staff_id'] = $userStorage->id;
	$resultCamp = $QCampaign->getListStore($paramsData);
	$this->view->store = $resultCamp;
	// ----------------RETURN AREA ------------------
	$resultArea =$QArea->GetAll();
	$this->view->area = $resultArea;
	// ---------------HIEN THI THONG BAO ----------------------
	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages = $messages;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages_success = $messages_success;
	}
	?>