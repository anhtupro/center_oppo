<?php 

	$campaign_id    = $this->getRequest()->getParam('campaign_id');
	$quantity 		= $this->getRequest()->getParam('quantity');
	$price   		= $this->getRequest()->getParam('price');
	
	$category_id    = $this->getRequest()->getParam('category_id');
	$area_id    	= $this->getRequest()->getParam('area_id');
	$submit     	= $this->getRequest()->getParam('submit');
	$contractor_id  = $this->getRequest()->getParam('contractor_id');

	$QCampaign 	   				= new Application_Model_Campaign();
	$QArea 		   				= new Application_Model_Area();
	$QCampaignArea 				= new Application_Model_CampaignArea();
	$QCampaignContractor 		= new Application_Model_CampaignContractor();
	$QCampaignAreaContractor	= new Application_Model_CampaignAreaContractor();
	$QContractorArea			= new Application_Model_ContractorArea();
        
        $flashMessenger = $this->_helper->flashMessenger;
        
	$area           = $QArea->get_cache_bi();

	if($this->storage['title'] == CONTRACTOR){
		$contractor_id = $QContractorArea->getContractorId($this->storage['staff_id']);
	}

	$params = [
		'campaign_id'    => $campaign_id,
		'contractor_id'  => $contractor_id
	];
        
//	$area_status = $QCampaignArea->get_status_area($params);
//	$min_status  = $QCampaignArea->get_status_min($params);

	//$campaign_contractor = $QCampaignContractor->getData($params);

	$where 		   = $QCampaign->getAdapter()->quoteInto('id = ?', $campaign_id);
	$campaign 	   = $QCampaign->fetchRow($where);

	//$category      = $QCampaign->getCategory($params);
        
        
        $category      = $QCampaign->getCategoryCampaign($params);
	$category_campaign 	= $QCampaign->getCategoryCampaign($params);
        

	$where 					= $QCampaignArea->getAdapter()->quoteInto('campaign_id = ?', $campaign_id);
	$campaign_area_check 	= $QCampaignArea->fetchAll($where);
        
	if(count($campaign_area_check) == 0){

//		$campaign_area_insert = $QCampaignArea->getCampaignAreaInsert($params);
//		foreach($campaign_area_insert as $key=>$value){
//			$QCampaignArea->insert(
//				[
//					'campaign_id'  	 => $value['campaign_id'],
//					'area_id' 	  	 => $value['area_id'],
//					'category_id' 	 => $value['category_id'],
//					'price' 		 => $value['price'],
//					'status' 		 => 1,
//				]
//			);
//		}
		
	}

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        

	//Get data campaign_area
	$campaign_area = $QCampaignAreaContractor->getCampaignAreaContractor($params);
	$data = [];
	foreach($campaign_area as $key=>$value){
		$data[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get data

	//Get data campaign_area_contractor
	$campaign_area_contractor = $QCampaignAreaContractor->getContractorArea($params);
	$data_area_contractor = [];
	foreach($campaign_area_contractor as $key=>$value){
		$data_area_contractor[$value['area_id']][$value['category_id']] = $value;
	}
	//END Get campaign_area_contractor


	if(!empty($submit) AND $submit == 2){

		require_once 'PHPExcel.php';
                $PHPExcel = new PHPExcel();

                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();
                $alpha = 'A';
                $index = 1;


                //Phần đầu
                $heads = array(
                    '',
                    ''
                );

                foreach($category as $key=>$value){
                        $heads[] = $value['category_id'];
                }

                foreach ($heads as $key)
                {
                    $sheet->setCellValue($alpha . $index, $key);
                    $alpha++;
                }

                $alpha = 'A';
                $index = 2;
                //END Phần đầu

                //Phần đầu 2
                $heads = array(
                        'ID Khu vực',
                    'Khu vực'
                );

                foreach($category as $key=>$value){
                        $heads[] = $value['name'];
                }

                foreach ($heads as $key)
                {
                    $sheet->setCellValue($alpha . $index, $key);
                    $alpha++;
                }
                $index = 3;
                //END Phần đầu 2


                //KHU VỰC
                foreach($area as $key=>$value){
                        $alpha = 'A';
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($key ,PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($value ,PHPExcel_Cell_DataType::TYPE_STRING);

                        foreach($category as $k=>$v){
//                                $quantity_limit = !empty($data[$key][$v['category_id']]['quantity_limit']) ? $data[$key][$v['category_id']]['quantity_limit'] : 0;
//                                $quantity = !empty($data[$key][$v['category_id']]['quantity']) ? $data[$key][$v['category_id']]['quantity'] : 0;
//                                $q = $quantity_limit." - ".$quantity;
//                                $sheet->getCell($alpha++ . $index)->setValueExplicit($q ,PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                        $index++;
                }
                //END Khu vực


                $filename = ' Campaign-Limit ' . date('d-m-Y H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;
	}


	if(!empty($submit) AND $submit == 1){
		if($_FILES["file"]["tmp_name"] != ""){
                    
                    
                        $db = Zend_Registry::get('db');
                        $db->beginTransaction();
                        try {
                                include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';

                                error_reporting(0);
                                set_time_limit(0);

                                //===================Main=======================

                                if($_FILES["file"]["tmp_name"] != ""){
                                        if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
                                                //echo ("Không phải file XLSX !");exit;
                                        }
                                        if ($_FILES["file"]["error"] > 0)
                                    {
                                        echo ("Return Code: " . $_FILES["file"]["error"] . "<br />");exit;
                                    }
                                        move_uploaded_file($_FILES["file"]["tmp_name"], "files/limit_campaign/data.xlsx");
                                }

                                $inputfile = 'files/limit_campaign/data.xlsx';
                                $xlsx = new SimpleXLSX($inputfile);
                                $data_excel = $xlsx->rows();


                                //Đếm số cột
                                $i = 3;
                                $num_cols = 0;
                                while ($data_excel[0][$i++] <> '') {
                                  $num_cols++;
                                }

                                //Đếm số dòng
                                $i = 1;
                                $num_rows = 0;
                                while ($data_excel[$i++][0] <> '') {
                                  $num_rows++;
                                }
                                
                                for($i = 2; $i <= $num_rows; $i++){
                                        $area_id = $data_excel[$i][0];

                                        for($j = 2; $j <= ($num_cols+2); $j++){
                                                $cat_id 	= $data_excel[0][$j];
                                                $quantity 	= $data_excel[$i][$j] ? $data_excel[$i][$j] : 0;
                                                
                                                if($quantity > 0){
                                                    //Kiểm tra nếu đặt hàng quá số lượng sẽ chặn
                                                    $quantity_all = (!empty($data[$area_id][$cat_id]['quantity']) and $data[$area_id][$cat_id]['quantity'] > 0) ? $data[$area_id][$cat_id]['quantity'] : 0;
                                                    $quantity_out = (!empty($data_area_contractor[$area_id][$cat_id]['quantity']) and $data_area_contractor[$area_id][$cat_id]['quantity'] > 0) ? $data_area_contractor[$area_id][$cat_id]['quantity'] : 0;


                                                    if(($quantity_all - $quantity_out) < $quantity){
                                                        $flashMessenger->setNamespace('error')->addMessage("Số lượng không đúng ".$area_id."/".$cat_id);
                                                        $back_url = HOST.'trade/contractor-area?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;
                                                        $this->redirect($back_url);
                                                    }
                                                    //END

                                                    $data_ = array(
                                                            'campaign_id'	=> $campaign_id,
                                                            'contractor_id' => $contractor_id,
                                                            'area_id' 	=> $area_id,
                                                            'category_id'   => $cat_id,
                                                            'quantity'	=> $quantity,
                                                            'quantity_area'	=> 0,
                                                            'created_at'	=> date('Y-m-d H:i:s'),
                                                            'status'	=> 1
                                                    );
                                                    $QContractorArea->insert($data_);

                                                    //Update status KV
                                                    $where = $QCampaignArea->getAdapter()->quoteInto('area_id = ?', $area_id);
                                                    $QCampaignArea->update( ['status'=>($min_status+1)] , $where);
                                                    //END Update


                                                }

                                        }

                                }

                                unlink($inputfile);

                                    //=================Function Area===================
                                
                                
                                $db->commit();
                                $this->_helper->layout->disableLayout();
                                $flashMessenger->setNamespace('success')->addMessage("Hoàn thành!");
                                $back_url = HOST.'trade/contractor-area?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;
                                $this->redirect($back_url);
                            
                            
                                
                        } catch (Exception $e) {
                            $db->rollback();

                            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                            $back_url = HOST.'trade/contractor-area?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;
                            $this->redirect($back_url);
                        }

			
		}
		else{
                    
                    $db = Zend_Registry::get('db');
                    $db->beginTransaction();
                    try {
                        
                        foreach($quantity as $key=>$value){
                                $data_ = array(
                                        'campaign_id'	=> $campaign_id,
                                        'contractor_id' => $contractor_id,
                                        'area_id' 		=> $area_id[$key],
                                        'category_id'   => $category_id[$key],
                                        'quantity'		=> $value,
                                        'quantity_area'	=> 0,
                                        'created_at'	=> date('Y-m-d H:i:s'),
                                        'status'		=> 1
                                );
                                $QContractorArea->insert($data_);

                                //Update status KV
                                $where = $QCampaignArea->getAdapter()->quoteInto('area_id = ?', $area_id[$key]);
                                $QCampaignArea->update( ['status'=>($min_status+1)] , $where);
                                //END Update
                        }
                        
                        $db->commit();
                        $flashMessenger->setNamespace('success')->addMessage("Hoàn thành!");
                        $back_url = HOST.'trade/contractor-area?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;
                        $this->redirect($back_url);
                        
                        
                    } catch (Exception $e) {
                        $db->rollback();
                        $e->getMessage();
                        
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $back_url = HOST.'trade/contractor-area?campaign_id='.$campaign_id.'&contractor_id='.$contractor_id;
                        $this->redirect($back_url);
                        
                    }
                    
                    
		}
	}

	

	$this->view->area     		= $area;
	$this->view->campaign 		= $campaign;
	$this->view->category 		= $category;
	$this->view->data     		= $data;
	$this->view->contractor     = $QCampaign->getContractor($params);
	$this->view->params         = $params;
	$this->view->data_area_contractor = $data_area_contractor;
	$this->view->category_campaign    = $category_campaign;
	$this->view->area_status 	= $area_status;
        
        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();

