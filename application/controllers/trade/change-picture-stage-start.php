<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$stage_id = $this->getRequest()->getParam('stage_id');
$status = $this->getRequest()->getParam('status');


$QChangePictureStage = new Application_Model_ChangePictureStage();

$QChangePictureStage->update([
    'is_running' => 0
], ['is_running = ?' => 1]);

$QChangePictureStage->update([
   'is_running' =>  $status == 1 ? 0 : 1
], ['id = ?' => $stage_id]);

echo  json_encode([
    'status' => 0
]);