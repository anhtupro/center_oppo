<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$staff_id = $this->getRequest()->getParam('staff_id');

$QTradeCheckIn = new Application_Model_TradeCheckIn();

$db = Zend_Registry::get("db");
$select = $db->select()
             ->from(['c' => DATABASE_TRADE.'.check_in'], [
                 'c.id',
                 'created_at' => "DATE_FORMAT(c.created_at, '%d/%m/%Y %H:%i')"
             ])
            ->where('c.staff_id = ?', $staff_id)
            ->order('c.created_at DESC')
            ->limit(20);

$listHistory = $db->fetchAll($select);

echo json_encode([
    'status' => 0,
    'listHistoryCheckIn' => $listHistory
]);
