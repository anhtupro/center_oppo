<?php
        
    $store_id = $this->getRequest()->getParam('store_id');
    
    $QAppCheckshop = new Application_Model_AppCheckshop();
    
    try {
        
        $params = [
            'store_id'  => $store_id
        ];
        
        $app_checkshop = $QAppCheckshop->getInfoCheckshop($params);
        
        $data = [
            'code' => 1,
            'data'  => $app_checkshop
        ];
        
    } catch (Exception $e) {
        
        $data = [
            'code' => 2,
            'message' => $e->getMessage()
        ];
    }
    
    echo json_encode($data);exit;
    
    


