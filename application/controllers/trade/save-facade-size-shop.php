<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();


// kích thước mặt tiền
$listWidthAppearance = $this->getRequest()->getParam('appearance_width');
$listHeightAppearance = $this->getRequest()->getParam('appearance_height');

// kích thước banner
$listWidth = $this->getRequest()->getParam('facade_width');
$listHeight = $this->getRequest()->getParam('facade_height');


$storeId = $this->getRequest()->getParam('store_id');
$checkshopId = $this->getRequest()->getParam('checkshop_id');

$QStoreFacadeSize = new Application_Model_StoreFacadeSize();
$QAppCheckshop = new Application_Model_AppCheckshop();

$db = Zend_Registry::get('db');
$db->beginTransaction();

$QStoreFacadeSize->delete(['store_id = ?' => $storeId]);

if ($listWidth || $listWidthAppearance) {
    foreach ($listWidth as $key => $width) {
        if ($width) {
            $QStoreFacadeSize->insert([
               'store_id' => $storeId,
               'width' => $width ? $width : Null,
                'height' => $listHeight[$key] ? $listHeight[$key] : Null,
                'type' => 1
            ]);
        }
    }


    foreach ($listWidthAppearance as $key => $width) {
        if ($width) {
            $QStoreFacadeSize->insert([
                'store_id' => $storeId,
                'width' => $width ? $width : Null,
                'height' => $listHeightAppearance[$key] ? $listHeightAppearance[$key] : Null,
                'type' => 2
            ]);
        }
    }

    $QAppCheckshop->update([
        'updated_facade_size' => 1
    ], ['id = ?' => $checkshopId]);

}

$db->commit();

echo json_encode([
   'status' => 0
]);

