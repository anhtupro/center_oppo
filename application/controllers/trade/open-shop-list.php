<?php

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);

$store_code   = $this->getRequest()->getParam('store_code');
$store_name   = $this->getRequest()->getParam('store_name');
$created_from = $this->getRequest()->getParam('created_from');
$created_to   = $this->getRequest()->getParam('created_to');
$reason       = $this->getRequest()->getParam('reason');
$status       = $this->getRequest()->getParam('status');
$area       = $this->getRequest()->getParam('area');

$QStore            = new Application_Model_Store();
$QOpenShop       = new Application_Model_OpenShop();
$QOpenShopReason = new Application_Model_OpenShopReason();
$QAppStatus = new Application_Model_AppStatus();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QArea = new Application_Model_Area();

$app_status = $QAppStatus->get_cache(10);
$app_status_title = $QAppStatusTitle->get_cache(10);
$app_status_title = $app_status_title[$this->storage['title']];

$reason_cache             = $QOpenShopReason->get_cache();
$this->view->reason_cache = $reason_cache;

$limit = LIMITATION;
$total = 0;

$params = array(
    'store_code'   => trim($store_code),
    'store_name'   => trim($store_name),
    'created_from' => $created_from,
    'created_to'   => $created_to,
    'reason'       => $reason,
    'status'       => $status,
    'area_id'      => $area_id,
    'area'      => $area
);

if(in_array($userStorage->title, array(SALES_ADMIN_TITLE))){//SALES_TITLE,SALES_LEADER_TITLE,SALE_SALE_ASM
    
    $list_store   = $QStore->getOpenShopListAccessByStaff();
    $shop_access = array();
    foreach ($list_store as $value) {
        $shop_access[] = $value['id'];
    }
    $params['access_shop_id'] = $shop_access;
}

$params['area_id'] = $this->storage['area_id'];

$repair = $QOpenShop->fetchPagination($page, $limit, $total, $params);
unset($params['access_shop_id']);

$this->view->area = $QArea->get_cache();
$this->view->app_status = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->params = $params;
$this->view->list   = $repair;
$this->view->desc   = $desc;
$this->view->sort   = $sort;
$this->view->limit  = $limit;
$this->view->total  = $total;   
$this->view->url    = HOST . 'trade/open-shop-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);

$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

