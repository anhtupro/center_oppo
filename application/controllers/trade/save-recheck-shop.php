<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$listQuantity = $this->getRequest()->getParam('quantity');
$roundId = $this->getRequest()->getParam('round_id');
$storeId = $this->getRequest()->getParam('store_id');
$note = $this->getRequest()->getParam('note');
$errorImage = $this->getRequest()->getParam('error_image');
$appCheckShopId = $this->getRequest()->getParam('app_check_shop_id');
$listErrorNote = $this->getRequest()->getParam('error_note');
$image = $this->getRequest()->getParam('image');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QRecheckShop = new Application_Model_RecheckShop();
$QRecheckShopDetail = new Application_Model_RecheckShopDetail();
$QRecheckShopFile = new Application_Model_RecheckShopFile();
$QRecheckShopRound = new Application_Model_RecheckShopRound();
$QRecheckShopErrorImage = new Application_Model_RecheckShopErrorImage();

$isRunning = $QRecheckShopRound->isRunning($roundId);
$recheck_shop_round = $QRecheckShopRound->fetchRow(['id = ?' => $roundId]);
if (! $isRunning) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đợt đánh giá này đã kết thúc.'
    ]);
    return;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();


try {

    $whereUpdate = [
        'recheck_shop_round_id = ?' => $roundId,
        'store_id = ?' => $storeId
    ];
    $QRecheckShop->update([
        'status' => 0
    ],$whereUpdate);

    $recheckShopId = $QRecheckShop->insert([
        'store_id' => $storeId,
        'recheck_shop_round_id' => $roundId,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
        'app_check_shop_id' => $appCheckShopId ? $appCheckShopId : NULL,
        'note' => $note,
        'error_image' => $errorImage,
        'status' => 1
    ]);

    if ($listQuantity) {
        foreach ($listQuantity as $categoryId => $quantity) {
            $QRecheckShopDetail->insert([
                'recheck_shop_id' => $recheckShopId,
                'category_id' => $categoryId,
                'quantity' => $quantity
            ]);
        }
    }

    if ($listErrorNote) {
        foreach ($listErrorNote as $errorId => $errorNote) {
            $QRecheckShopErrorImage->insert([
                'recheck_shop_id' => $recheckShopId,
                'error_image_id' => $errorId,
                'note' => $errorNote ? $errorNote : NULL
            ]);
        }
    }


    // Chỉnh sửa array $_FILES lại cho dễ xử lý
    if ($recheck_shop_round['recheck_type'] == 2) {
        if (count($image) < 3) {
            echo json_encode([
                'status' => 1,
                'message' => 'Vui lòng chụp ít nhất 3 hình ảnh! '
            ]);
            return;
        }


        foreach ($image as $type => $v) {

            $array_data = explode(',', $v);
            $image_base64 = $array_data[1];

            $data = base64_decode($image_base64);

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'recheck_shop' . DIRECTORY_SEPARATOR . $recheckShopId . DIRECTORY_SEPARATOR;
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $newName = md5(uniqid('', true)) . '.png';

            $url = '/photo/recheck_shop/' . $recheckShopId . '/' . $newName;

            $file = $uploaded_dir . '/' . $newName;

            $success_upload = file_put_contents($file, $data);

            $url_resize = 'photo' .
                DIRECTORY_SEPARATOR . 'recheck_shop' . DIRECTORY_SEPARATOR . $recheckShopId . DIRECTORY_SEPARATOR . $newName;

            //Resize Anh
            $image = new My_Image_Resize();
            $image->load($url_resize);
            $image->resizeToWidth(700);
            $image->save($url_resize);
            // //END Resize

            // upload image to server s3
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            $destination_s3 = 'photo/recheck_shop/' . $recheckShopId . '/';

            if ($success_upload) {
                $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
            }
            // upload image to server s3

            $QRecheckShopFile->insert([
                'url' => $url,
                'type' => $type,
                'recheck_shop_id' => $recheckShopId
            ]);

        }
        // end save photo
    }



    $db->commit();

    echo json_encode([
        'status' => 0,
		'id'	=> $recheckShopId
    ]);

}catch (\Exception $e){
    echo json_encode([
       'status' => 1,
       'message' => $e->getMessage()
    ]);
}