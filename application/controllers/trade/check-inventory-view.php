<?php

$areaId = $this->getRequest()->getParam('area_id');

$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QInventoryArea = new Application_Model_AirInventoryArea();
$QCategoryInventoryType = new Application_Model_CategoryInventoryType();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();

//$QInventoryAreaTest = new Application_Model_AirInventoryAreaTest();
$params = [
    'area_id' => $areaId
];

$currentCategories = $QAppCheckshopDetailChild->getCategoryParentWarehouse($params);
$currentCategoriesDetail = $QAppCheckshopDetailChild->getCategoryDetailWarehouse($params);
$currentCategoriesDetailPainting = $QAppCheckshopDetailChildPainting->getPainting($params);

$listIdCategoryCheckshop = $QCategory->getIdCategoryCheckshop($params);
//$currentCategorieDetail = $QInventoryArea->getCategoryDetail($params);
$categoryInventoryType = $QCategoryInventoryType->fetchAll()->toArray();

$this->view->listIdCategoryCheckshop = $listIdCategoryCheckshop;
$this->view->currentCategoriesDetailPainting = $currentCategoriesDetailPainting;
$this->view->listCategory = $listCategory;
$this->view->currentCategories = $currentCategories;
$this->view->currentCategoriesDetail = $currentCategoriesDetail;
$this->view->categoryInventoryType = $categoryInventoryType;
$this->view->areaId = $areaId;


$this->view->areaName = $QArea->fetchRow(['id = ?' => $areaId])->toArray()['name'];

