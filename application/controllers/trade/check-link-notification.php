<?php
	
	$token = $this->getRequest()->getParam('token');
	$id_notification = $this->getRequest()->getParam('id_notification');
	
	$auth = Zend_Auth::getInstance();
	$QAppNoti = new Application_Model_AppNoti();
	
	$notification = $QAppNoti->fetchRow(['id = ?' => $id_notification]);
	
	if(!empty($notification)){
		//Check token auto login
		if(!empty($token) AND !$auth->hasIdentity()){
			
			$db = Zend_Registry::get('db');
			$auth = Zend_Auth::getInstance();
			$authAdapter = new Zend_Auth_Adapter_DbTable($db);
			$authAdapter->setTableName('staff')
					->setIdentityColumn('email')
					->setCredentialColumn('password');
			$select = $db->select()
					->from(array('p' => 'staff'), array('p.*'));
			$select->where('p.dingtalk_id = ?', $token);
			$resultStaff = $db->fetchRow($select);

			if (!empty($resultStaff)) {
                            
				$md5_pass = $resultStaff['password'];
				$authAdapter->setIdentity($resultStaff['email']);
				$authAdapter->setCredential($md5_pass);
				$result = $auth->authenticate($authAdapter);
                                
				if ($result->isValid()) {
                                    
					$data = $authAdapter->getResultRowObject(null, 'password');
					$QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();

					$where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

					$priviledge = $QStaffPriveledge->fetchRow($where);

					$personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

					$QRegionalMarket = new Application_Model_RegionalMarket();
					$area_id = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
					$data->regional_market_id = $area_id;
                                        
                                        
                                        
					$QTeam = new Application_Model_Team();
					$title = $QTeam->find($data->title)->current();
					$QGroup = new Application_Model_GroupTrade();
					$group = $QGroup->find($title->access_group)->current();
					$data->group_id = $title->access_group;
					$data->role = $group->name;

					if ($personal_accesses) {
						// Thanh edit merge   
						$data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
						//$menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
						$menu = explode(',', $personal_accesses['menu']);
					} else {

						$data->accesses = json_decode($group->access);

						$menu = $group->menu ? explode(',', $group->menu) : null;
					}

					$QMenu = new Application_Model_MenuTrade();
					$where = array();
					$where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

					if ($menu)
						$where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
					else
						$where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

					$menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

					$data->menu = $menus;
                                        $data->login_from_trade = 1;
                                        
					$auth->getStorage()->write($data);
					if (!empty($resultStaff)) {
						$response['status'] = 1;
						$response['data'] = $resultStaff;
					}
                                        
				}
			}
		}
		//END Check token auto login

		if ($auth->hasIdentity()) {
			$this->_redirect(HOST . $notification['link']);
		}
	}
	
	
	
	echo "Done";exit;


