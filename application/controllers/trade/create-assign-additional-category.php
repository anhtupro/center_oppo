<?php
 $categoryParentId= $this->getRequest()->getParam('category_id_additional');

$QCategoryAdditional = new Application_Model_CategoryAdditional();
$QCategory = new Application_Model_Category();

//$listCategoryAdditional = $QCategoryAdditional->getAll();
if ($categoryParentId) {
    $additionalCategory = $QCategoryAdditional->fetchRow(['id = ?' => $categoryParentId])->toArray();
    $listCategoryChild = $QCategory->getAllChildCategory($categoryParentId);
}

$listCategory = $QCategory->GetCategoryPosm();

$this->view->listCategory = $listCategory;
$this->view->listCategoryChild = $listCategoryChild;
$this->view->additionalCategory = $additionalCategory;
