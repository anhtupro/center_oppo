<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
include 'PHPExcel/IOFactory.php';
// config for excel template excel

// row
define('START_ROW', 2);

// column
define('STORE_ID', 0);
define('CAPACITY_ID', 2);


$stage_id = $this->getRequest()->getParam('stage_id');
$userStorage  = Zend_Auth::getInstance()->getStorage()->read();

$QStoreCapacityStage = new Application_Model_StoreCapacityStage();
$QStoreCapacityAssign = new Application_Model_StoreCapacityAssign();
$QStoreCapacityResult = new Application_Model_StoreCapacityResult();
$QStore = new Application_Model_Store();
$QAppNoti = new Application_Model_AppNoti();

$stage_valid = $QStoreCapacityStage->checkValidStage($stage_id);

if (! $stage_valid) {
    echo json_encode([
        'stauts' => 1,
        'message' => 'Đợt đánh giá đã két thúc !'
    ]);

    return;
}


$db = Zend_Registry::get("db");
$db->beginTransaction();

// upload and save file
if ($_FILES['file']['name']) { // if has file upload

    $save_folder = 'change_picture_massupload';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 15000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx')
    );

    try {
        $file = My_File::get($save_folder, $requirement);

        if (!$file) {
            echo json_encode([
                'status' => 1,
                'message' => 'Upload failed'
            ]);
            return;
        }
        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

//        $url_file = '/files/' . $save_folder. '/' . $file['folder'] . '/' . $file['filename'];
//
//        if ($url_file) {
//            $QCheckPosmStage->update([
//                'url_file' => $url_file
//            ],['id = ?' => $stage_id]);
//        }

    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    //read file
    //  Choose file to read
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    // read sheet
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();


    try {

        $insert_header_query = "INSERT INTO trade_marketing.store_capacity_result(stage_id, store_id, capacity_id) VALUES ";
        $insert_body_query = '';
        $list_capacity = $QStoreCapacityAssign->getCapacity($stage_id);
 
        foreach ($list_capacity as $value_capacity) {
            $list_capacity_id [] = $value_capacity['id'];
        }

        $array_store_id = [];
        $list_store_invalid  = [];
        $list_store_id = [];

        for ($row = START_ROW; $row <= $highestRow; ++$row) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = $rowData[0];

            $store_id = intval(TRIM($rowData[STORE_ID]));
            $capacity_id = $rowData[CAPACITY_ID] ? intval(TRIM($rowData[CAPACITY_ID])) : 0;

            // kiểm tra capacity có nằm trong đợt do HO tạo hay không
            if ($capacity_id && !in_array($capacity_id, $list_capacity_id)) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Mã số năng lực (' . $capacity_id . ') không nằm trong danh sách mã năng lực của đợt đánh giá này, tại Store ID:' . $store_id . ' . Vui lòng kiểm tra file và thử lại! '
                ]);

                return;
            }

            // kiểm tra store
            if (! $store_id) {
                continue;
            }

            if (in_array($store_id, $list_store_id)) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Trùng store ID: ' . $store_id
                ]);

                return;
            }

            $list_store_id [] = $store_id;

            // end kiểm tra capacity
                    if ($capacity_id) {
                        // insert
                        $value_insert = '('
                            . $stage_id . ','
                            . $store_id  .  ','
                            . $capacity_id
                            . '),';
                        $insert_body_query .= $value_insert;
                    }
        }

        // check if store_id is valid
        $params = [
            'array_store' => $list_store_id,
            'list_area_id' => $this->storage['area_id']
        ];

        $list_invalid_store_id = $QAppNoti->getStoreInvalid($params);

        if ($list_invalid_store_id) {
            $list_invalid_store_id = implode(', ', $list_invalid_store_id);
            echo json_encode([
                'status' => 1,
                'message' => 'Shop có ID: ' . $list_invalid_store_id . ' không thuộc khu vực bạn quản lý hoặc đã đóng. Vui lòng kiểm tra và thử lại !'
            ]);
            return;
        }

        $QStoreCapacityResult->delete([
           'stage_id = ?' => $stage_id,
           'store_id IN (?)' => $list_store_id
        ]);

        if ($insert_body_query) {
            $insert_query = $insert_header_query . $insert_body_query;
            $insert_query = TRIM($insert_query, ',');

            if($insert_query){
                $stmt1 = $db->prepare($insert_query);
                $stmt1->execute();
                $stmt1->closeCursor();
            }
        }

    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

} else {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chọn file'
    ]);
    return;
}


$db->commit();

echo json_encode([
    'status' => 0
]);

