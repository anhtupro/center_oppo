<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$contractNumber = $this->getRequest()->getParam('contract_number');
$destructionId = $this->getRequest()->getParam('destruction_id');

$QDestruction = new Application_Model_Destruction();

$QDestruction->update([
    'contract_number' => $contractNumber ? TRIM($contractNumber) : ''
], ['id = ?' => $destructionId]);

echo json_encode([
    'status' => 0,
    'contract_number' => $contractNumber
]);