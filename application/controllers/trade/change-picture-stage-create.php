<?php
$id = $this->getRequest()->getParam('id');

$QChangePictureStage = new Application_Model_ChangePictureStage();
$QPhoneProduct = new Application_Model_PhoneProduct();
$QChangePictureAssignPhone = new Application_Model_ChangePictureAssignPhone();

$list_phone_product = $QPhoneProduct->fetchAll();
$this->view->list_phone_product = $list_phone_product;

if ($id) {
    $stage = $QChangePictureStage->fetchRow(['id = ?' => $id]);
    $list_phone_product_edit = $QChangePictureAssignPhone->fetchAll(['stage_id = ?' => $id])->toArray();


    $this->view->list_phone_product_edit = $list_phone_product_edit;
    $this->view->stage = $stage;
}

