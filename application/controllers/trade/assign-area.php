<?php

$QAssignAreaRecheckShop = new Application_Model_AssignAreaRecheckShop();
$QArea = new Application_Model_Area();
$QAreaStaff = new Application_Model_AreaStaff();

$listStaff = $QAreaStaff->getAll();
$listArea = $QArea->getAll();

$this->view->listStaff = json_encode($listStaff);
$this->view->listArea = $listArea;
