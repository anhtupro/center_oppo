<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$QConstructObs = new Application_Model_ConstructObs();
$construct_obs_id = $this->getRequest()->getParam('construct_obs_id');

$construct_obs = $QConstructObs->fetchRow(['id = ?' => $construct_obs_id]);

$db = Zend_Registry::get('db');
$db->beginTransaction();

$where = ['id = ?' => $construct_obs_id];
if($construct_obs['status'] == 2) {
    $QConstructObs->update([
        'status' => 3
    ], $where);
}

if($construct_obs['status'] == 3) {
    $QConstructObs->update([
        'status' => 4
    ], $where);
}

if($construct_obs['status'] == 4) {
    $QConstructObs->update([
        'status' => 5
    ], $where);
}

$db->commit();

echo json_encode([
    'status' => 0
]);