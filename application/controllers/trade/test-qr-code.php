<?php

$store_id = $this->getRequest()->getParam('id');
$dev = $this->getRequest()->getParam('dev');

$QBiTrade    		= new Application_Model_BiTrade();
$QCategory    		= new Application_Model_Category();
$QStore      		= new Application_Model_Store();
$QCheckshop      	= new Application_Model_AppCheckshop();
$QCheckshopDetailBrand = new Application_Model_AppCheckshopDetailBrand();
$QBrand = new Application_Model_Brand();

$where = $QStore->getAdapter()->quoteInto('id = ?', $store_id );
$store = $QStore->fetchRow($where);


$investments 	  = $QBiTrade->getCheckshopByStore($store_id);
$checkshop_last   = $QCheckshop->getLastCheckshop($store_id);



$flashMessenger       = $this->_helper->flashMessenger;
// /var_dump($investments); exit;

$params = [];

foreach($investments as $key=>$value){
    $params['investments_cat'][] = $value['category_id'];
}

//Nếu có lock
$investments_lock = $QCheckshop->getCheckshopLock($store_id);



if(!empty($investments_lock)){
    $investments = $investments_lock;
}


$checkshop = [];
foreach($checkshop_last as $key=>$value){
    $checkshop[$value['category_id']][] = $value;
}

$ua = strtolower($_SERVER['HTTP_USER_AGENT']);


if(stripos($ua,'android') !== false) { // && stripos($ua,'mobile') !== false) {
    preg_match('/Android (\d+(?:\.\d+)+)[;)]/', $_SERVER['HTTP_USER_AGENT'], $matches);
    $this->view->android_version = (int)$matches[1];
}

$checkshopDetailBrand = $QCheckshopDetailBrand->getDetail($store_id);
$listCategoryBrand = $QCategory->getCategoryBrand();
$listBrand = $QBrand->fetchAll(['is_checkshop = ?' => 1])->toArray();


$category_checkshop = $QCategory->getCategoryCheckshop($params);

$this->view->dev = $dev;
$this->view->investments = $investments;
$this->view->investments_lock = $investments_lock;
$this->view->store       = $store;
$this->view->checkshopDetailBrand = $checkshopDetailBrand;
$this->view->listCategoryBrand = $listCategoryBrand;
$this->view->listBrand = $listBrand;
//$this->view->checkshop_last = $checkshop_last;
$this->view->checkshop      = $checkshop;
$this->view->category    	= $category_checkshop;

// ---------------HIEN THI THONG BAO ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

    $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
    $messages          = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
}
