<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$listName = $this->getRequest()->getParam('name');
$id = $this->getRequest()->getParam('id');

$QGuideline = new Application_Model_Guideline();

if (!$id) {
    foreach ($listName as $name) {
        if($name) {
            $QGuideline->insert([
               'name' => $name
            ]);
        }
    }
} else {
    $QGuideline->update([
        'name' => $listName
    ],['id = ?' => $id]);
}

echo json_encode([
   'status' => 0
]);