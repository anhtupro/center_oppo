<?php

$QCategoryAdditional = new Application_Model_CategoryAdditional();
$QCategory = new Application_Model_Category();

$listCategoryAdditional = $QCategoryAdditional->getAll();
$listCategoryChild = $QCategory->getAllChildCategory();
$listCategory = $QCategory->GetCategoryPosm();


$this->view->listCategoryAdditional = $listCategoryAdditional;
$this->view->listCategoryChild = $listCategoryChild;
$this->view->listCategory = $listCategory;

