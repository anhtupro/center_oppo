<?php
$season = $this->getRequest()->getParam('season', 1);
$year = $this->getRequest()->getParam('year', date('Y'));

$QAdditionalPosmCost = new Application_Model_AdditionalPosmCost();
$QArea = new Application_Model_Area();
$QCategoryAdditional = new Application_Model_CategoryAdditional();
$params = [
    'season' => $season,
    'year' => $year
];

$listAdditionalCost = $QAdditionalPosmCost->getCost($params);
$listArea = $QArea->getListArea();
$listAdditionalCategory = $QCategoryAdditional->getAll();

$this->view->listAdditionalCost = $listAdditionalCost;
$this->view->listArea = $listArea;
$this->view->listAdditionalCategory = $listAdditionalCategory;
$this->view->season = $season;
$this->view->year = $year;