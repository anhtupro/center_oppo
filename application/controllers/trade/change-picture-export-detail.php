<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$stage_id = $this->getRequest()->getParam('stage_id');
$params = [
    'stage_id' => $stage_id,
    'area_list' => $this->storage['area_id']
];

$QChangePictureDetail = new Application_Model_ChangePictureDetail();
$QChangePictureStage = new Application_Model_ChangePictureStage();

$stage = $QChangePictureStage->fetchRow(['id = ?' => $stage_id]);


$QChangePictureDetail->exportDetail($params);