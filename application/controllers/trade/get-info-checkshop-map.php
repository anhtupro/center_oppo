<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

$checkshopId = $this->getRequest()->getParam('checkshop_id');
$storeType = $this->getRequest()->getParam('store_type');

$params = [
    'checkshop_id' => $checkshopId
];
$QAppCheckshop = new Application_Model_AppCheckshop();
$QCheckShopOutside = new Application_Model_CheckShopOutside();

if ($storeType == 1) {
    $imageCheckshop = $QAppCheckshop->getImageCheckshop($params);
    $infoShop = $QAppCheckshop->getInforShopMap($params);
}

if ($storeType == 2) {
    $imageCheckshop = $QCheckShopOutside->getImageCheckshop($params);
    $infoShop = $QCheckShopOutside->getInforShopMap($params);
}



echo json_encode([
   'status' => 0,
   'imageCheckshop' => $imageCheckshop,
    'infoShop' => $infoShop
]);