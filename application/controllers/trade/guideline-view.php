<?php
$QGuideline = new Application_Model_Guideline();
$QGuidelineFile = new Application_Model_GuidelineFile();

$listGuideline = $QGuideline->fetchAll([
   'is_deleted IS NULL'
]);

$listFile = $QGuidelineFile->getAll();

$this->view->listGuideline = $listGuideline;
$this->view->listFile= $listFile;

