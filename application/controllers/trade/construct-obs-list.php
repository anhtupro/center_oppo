<?php
$QConstructObs = new Application_Model_ConstructObs();
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);

$list_data = $QConstructObs->fetchPagination($page, $limit, $total, $params);


$this->view->params=$params;
$this->view->list_data = $list_data;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/construct-obs-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);