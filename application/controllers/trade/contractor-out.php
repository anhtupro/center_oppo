<?php 
	
	$id = $this->getRequest()->getParam('id');

	$QContractorOut = new Application_Model_AppContractorOut();


	$params = [
		'id'	=> $id
	];

	$contractor_out 	= $QContractorOut->getContractorOut($params);

	$this->view->contractor_out = $contractor_out;

	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;
