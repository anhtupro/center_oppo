<?php
$page = $this->getRequest()->getParam('page', 1);
$storeName = $this->getRequest()->getParam('store_name');
$storeId = $this->getRequest()->getParam('store_id');
$district = $this->getRequest()->getParam('district');
$areaId = $this->getRequest()->getParam('area_id');
$regionalMarket = $this->getRequest()->getParam('regional_market');
$export = $this->getRequest()->getParam('export');
$flashMessenger = $this->_helper->flashMessenger;
$limit = LIMITATION;
$total = 0;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QStoreOutside = new Application_Model_StoreOutside();
$QCheckShopOutside = new Application_Model_CheckShopOutside();
$QCheckShopOutsideDetail = new Application_Model_CheckShopOutsideDetail();
$QCategory = new Application_Model_Category();
$QAppFile = new Application_Model_AppFile();
$QArea = new Application_Model_Area();
$QRegionalMarket = new Application_Model_RegionalMarket();



$params = [
  'store_name' => $storeName,
//    'province' => $province,
    'district' => $district,
    'regional_market' => $regionalMarket,
    'area_id' => $areaId,
    'area_list' => $this->storage['area_id'],
    'store_id' => $storeId
];

if ($export) {
    $QCheckShopOutsideDetail->export($params);
}

$listStore = $QStoreOutside->fetchPagination($page, $limit, $total, $params);

if ($areaId) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $areaId);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regionalMarket) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regionalMarket);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

if ($userStorage->title == SALES_TITLE) {// -------------------HIỂN THỊ ĐỀ XUẤT DÀNH CHO SALE --------------
    $params['staff_id'] = $userStorage->id;

}
$listArea = $QArea->getListArea($params);

$this->view->listStore = $listStore;
//$this->view->listProvince = $listProvince;
$this->view->listArea = $listArea;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->url = HOST . 'trade/check-shop-outside-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);


