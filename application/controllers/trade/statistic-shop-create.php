<?php

$QStore = new Application_Model_Store();
$QCategory = new Application_Model_Category();

$params = [
    'list_area_id' => $this->storage['area_id']
];

$listStore = $QStore->getListStore($params);
$listCategory = $QCategory->getCategoryCheckshop();

$this->view->listStore = $listStore;
$this->view->listCategory = $listCategory;


