<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$store_id = $this->getRequest()->getParam('store_id');
$shop_size_type1 = $this->getRequest()->getParam('shop_size_type1');
$shop_size_type2 = $this->getRequest()->getParam('shop_size_type2');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$QShopSize = new Application_Model_ShopSize();

if (!$store_id || !$shop_size_type1) {
    echo  json_encode([
       'status' => 1,
       'message' => 'Chưa chọn loại!'
    ]);

    return;
}

$where = $QShopSize->getAdapter()->quoteInto('store_id = ?', $store_id);
$QShopSize->update(['is_del' => 1], $where);

$QShopSize->insert([
    'store_id' => $store_id,
    'shop_size_type_id' => $shop_size_type1,
    'created_at' => date('Y-m-d H:i:s'),
    'created_by' => $userStorage->id,
]);

//$QShopSize->insert([
//    'store_id' => $store_id,
//    'shop_size_type_id' => $shop_size_type2,
//    'created_at' => date('Y-m-d H:i:s'),
//    'created_by' => $userStorage->id,
//]);


echo json_encode([
    'status' => 0
]);
