<?php

$areaId = $this->getRequest()->getParam('area_id');
$staffName = $this->getRequest()->getParam('staff_name');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QTradeCheckIn = new Application_Model_TradeCheckIn();
$QSpecialUser = new Application_Model_SpecialUser();
$QArea = new Application_Model_Area();
$listSpecialUser = $QSpecialUser->getAll();

$params = [
    'list_special_user' => $listSpecialUser,
    'staff_id' => $userStorage->id,
    'area_list' => $this->storage['area_id'],
    'staff_name' => $staffName,
    'area_id' => $areaId,
    'title' => $userStorage->title
];

$list = $QTradeCheckIn->getAll($params);
$listArea = $QArea->getListArea($params);

$this->view->list = $list;
$this->view->listArea = $listArea;
$this->view->params = $params;




