<?php

$storeId = $this->getRequest()->getParam('store_id');

$QCategory = new Application_Model_Category();
$QStoreOutside = new Application_Model_StoreOutside();
$QCheckShopOutside = new Application_Model_CheckShopOutside();
$QCheckShopOutsideDetail = new Application_Model_CheckShopOutsideDetail();
$QCheckshopOutsideDetailBrand = new Application_Model_CheckShopOutsideDetailBrand();
$QBrand = new Application_Model_Brand();

$flashMessenger = $this->_helper->flashMessenger;

$category_checkshop = $QCategory->getCategoryCheckshop($params);
$latestCheckShop = $QCheckShopOutside->getLatestCheckShop($storeId);

if ($latestCheckShop['id']) {
    $listLatestCheckShopDetail = $QCheckShopOutsideDetail->getDetail($latestCheckShop['id']);

    $checkshopDetailBrand = $QCheckshopOutsideDetailBrand->getDetail($latestCheckShop['id']);

}
$listBrand = $QBrand->fetchAll()->toArray();
$listCategoryBrand = $QCategory->getCategoryBrand();


$this->view->store_id = $storeId;
$this->view->category = $category_checkshop;
$this->view->latestCheckShop = $latestCheckShop;
$this->view->listLatestCheckShopDetail = $listLatestCheckShopDetail;

$this->view->checkshopDetailBrand = $checkshopDetailBrand;
$this->view->listCategoryBrand = $listCategoryBrand;
$this->view->listBrand = $listBrand;
//$this->view->storeInfo = $storeInfo;


