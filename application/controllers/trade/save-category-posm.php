<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$name = $this->getRequest()->getParam('name');
$price = $this->getRequest()->getParam('price');
$type_id = $this->getRequest()->getParam('type');
$desc = $this->getRequest()->getParam('desc');
$status = $this->getRequest()->getParam('status');
$status_order = $this->getRequest()->getParam('status_order');
$is_brand = $this->getRequest()->getParam('is_brand');
$submit = $this->getRequest()->getParam('submit');
$list_contractor = $this->getRequest()->getParam('contractor');
$list_code = $this->getRequest()->getParam('code');
$list_price = $this->getRequest()->getParam('list_price');
$is_purchasing_request = $this->getRequest()->getParam('is_purchasing_request');
$urgent_date = $this->getRequest()->getParam('urgent_date');
$purchasing_type = $this->getRequest()->getParam('purchasing_type');
$unit = $this->getRequest()->getParam('unit');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QContractorPrice = new Application_Model_ContractorPrice();
$QPurchasingRequest = new Application_Model_PurchasingRequest();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
$QPurchasingCodeCategory = new Application_Model_PurchasingCodeCategory();
$QPurchasingCode = new Application_Model_PurchasingCode();
$QPurchasingType = new Application_Model_PurchasingType();
$QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
$QSupplierContructors = new Application_Model_SupplierContructors();
$QContructors = new Application_Model_Contructors();

$db = Zend_Registry::get('db');
$db->beginTransaction();

$category_id = $QCategory->insert([
    'name' => $name,
    'price' => $price,
    'status' => $status,
    'status_order' => $status_order,
    'desc' => $desc,
    'type' => $type_id,
    'is_brand_shop' => $is_brand
]);

$QCategory->update([
    'code' => $category_id
], ['id = ?' => $category_id]);


// chia giá hạng mục theo từng nhà thầu
foreach ($list_contractor as $key => $contractor_id) {
    if ($contractor_id) {
        $QContractorPrice->insert([
            'contractor_id' => $contractor_id,
            'category_id' => $category_id,
            'price' => $list_price[$key],
            'from_date' => date('Y-m-d'),
            'to_date' => NULL
        ]);
    }

}
//

// purchasing request
if ($is_purchasing_request) {

    $sn = date('YmdHis') . substr(microtime(), 2, 4);

    $purchasing_request_id = $QPurchasingRequest->insert([
        'name' => $name,
        'urgent_date' => date('Y-m-d', strtotime(str_replace('/', '-', $urgent_date))),
        'type' => $purchasing_type,
        'area_id' => 65,
        'department_id' => $userStorage->department,
        'team_id' => $userStorage->team,
        'shipping_address' => 'Trade marketing',
        'sn' => $sn,
        'status' => 1,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id
    ]);


    // tạo purchasing code
    $select = $db->select()->from(array('p' => 'pmodel_code'), array('new_code' => 'MAX(p.code)+1','length_code' => 'LENGTH(MAX(`code`)+1)'));
    $result = $db->fetchRow($select);

    $i = 1; $string_new_code = '';
    while ($i <= (9-$result['length_code'])){  // Tổng length của chuỗi code là 9, length của number code là $result['length_code']
        $string_new_code .= '0';
        $i++;
    }
    $string_new_code.= $result['new_code'];

    $code['code'] = $string_new_code;

    $code_purchasing_id = $QPurchasingCode->insert([
        'code' => $string_new_code,
        'name' => $name,
        'desc' => $desc,
        'unit' => $unit
    ]);
    //end tạo purchasing code

    $QPurchasingCodeCategory->insert([
        'category_id' => $category_id,
        'code_purchasing_id' => $code_purchasing_id,
        'type' => 5 // posm
    ]);


    $purchasing_detail_id = $QPurchasingRequestDetails->insert([
        'pr_id' => $purchasing_request_id,
        'pmodel_code_id' => $code_purchasing_id,
        'quantity' => 0
    ]);



    foreach ($list_contractor as $contractor_id) {
        // check đã liên kết nhà thầu chưa
        $contractor = $QContructors->fetchRow(['id = ?' => $contractor_id]);
        $supplier_id = $QSupplierContructors->fetchRow(['contructors_id = ?' => $contractor_id])['supplier_id'];
        if (!$supplier_id) {
            echo json_encode([
                'status' => 1,
                'message' => 'Chưa liên kết nhà thầu với Purchasing: ' . $contractor['name']
            ]);

            return;
        }
        //

        $QPurchasingRequestSupplier->insert([
            'pr_id' => $purchasing_request_id,
            'pr_detail_id' => $purchasing_detail_id,
            'supplier_id' => $supplier_id
        ]);
    }

}
//end purchasing request

$db->commit();

echo json_encode([
    'status' => 0
]);
