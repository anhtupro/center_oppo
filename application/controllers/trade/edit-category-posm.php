<?php
	$id 			= $this->getRequest()->getParam('id');
	$name 			= $this->getRequest()->getParam('name');
	$price 	= $this->getRequest()->getParam('price');
	$type_id 		= $this->getRequest()->getParam('type');
	$desc 			= $this->getRequest()->getParam('desc');
	$status 			= $this->getRequest()->getParam('status');
	$status_order			= $this->getRequest()->getParam('status_order');
	$is_brand			= $this->getRequest()->getParam('is_brand');
	$submit 		= $this->getRequest()->getParam('submit');
	$category_id		= $this->getRequest()->getParam('category_id');
	$QCategory 		= new Application_Model_Category();
	
	$flashMessenger       = $this->_helper->flashMessenger;
	$where = $QCategory->getAdapter()->quoteInto('id = ?', $id);
	$data = $QCategory->fetchRow($where);
	$this->view->data = $data;
	$this->view->id = $id;

if(!empty($submit)){

		try{

				$where_update = $QCategory->getAdapter()->quoteInto('id = ?', $category_id);
				$data = [
					'name'				=> $name,
					'price'				=> $price,
					'status'			=> $status,
					'status_order'		=> $status_order,
					'desc'				=> $desc,
					'type'				=> $type_id,
					'is_brand_shop'		=> $is_brand
				];
				$QCategory->update($data,$where_update);
				$flashMessenger->setNamespace('success')->addMessage('Chỉnh sửa hạng mục thành công !');
				$this->_redirect(HOST . 'trade/list-category-posm');
		}catch (Exception $e) {
					$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
					$this->_redirect(HOST . 'trade/edit-category-posm');
		}

}

	if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
	if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
		$messages          = $flashMessenger->setNamespace('success')->getMessages();
		$this->view->messages = $messages;
	}
?>