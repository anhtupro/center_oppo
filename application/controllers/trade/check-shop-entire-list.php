<?php
$area_id = $this->getRequest()->getParam('area_id');
$store_id = $this->getRequest()->getParam('store_id');
$store_name = $this->getRequest()->getParam('store_name');
$regional_market = $this->getRequest()->getParam('regional_market');
$district = $this->getRequest()->getParam('district');
$staffId = $this->getRequest()->getParam('staff_id');
$export = $this->getRequest()->getParam('export');
$page = $this->getRequest()->getParam('page', 1);
$status = $this->getRequest()->getParam('status');
$error_image = $this->getRequest()->getParam('error_image');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$staff_id = $this->getRequest()->getParam('staff_id');
$list_store = $this->getRequest()->getParam('list_store');

$export_time = $this->getRequest()->getParam('export_time');
$export_error = $this->getRequest()->getParam('export_error');

$limit = LIMITATION;

$QStore= new Application_Model_Store();
$QRegionalMarket= new Application_Model_RegionalMarket();
$QRecheckShop = new Application_Model_RecheckShop ();
$QArea = new Application_Model_Area();
$QAssignAreaRecheckShop = new Application_Model_AssignAreaRecheckShop();
$QRegionalMarket = new Application_Model_RegionalMarket();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QRecheckShopRound = new Application_Model_RecheckShopRound();

$QCheckShopEntireImageRatio = new Application_Model_CheckShopEntireImageRatio();
$QCheckShopEntireDisplayProduct = new Application_Model_CheckShopEntireDisplayProduct();
$QCheckShopEntirePointTotal = new Application_Model_CheckShopEntirePointTotal();
$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QCheckShopEntirePromotion = new Application_Model_CheckShopEntirePromotion();
$QCheckShopEntirePromotionDetail= new Application_Model_CheckShopEntirePromotionDetail();
$QAreaStaff = new Application_Model_AreaStaff();


//$QStore = new Application_Model_Store();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$params = [
    'area_id' => $area_id,
    'store_id' => $store_id,
    'store_name' => $store_name,
    'regional_market' => $regional_market,
    'district' => $district,
    'staff_id' => $staffId,
    'status' => $status,
    'list_area' => $this->storage['area_id'],
    'error_image' => $error_image,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'area_list' => $this->storage['area_id'],
    'staff_id' => $staff_id,
    'list_store' => $list_store
];

if ($export_time) {
    $QCheckShopEntire->exportTime($params);
}

if ($export_error) {
    $QCheckShopEntire->exportError($params);
}

$list_store = $QCheckShopEntire->fetchPagination($page, $limit, $total, $params);

$list_area = $QArea->getListArea($params);

//$list_sale  = $QAppCheckShop->getSaleArea();
$list_sale  = $QAreaStaff->getTradeArea(); // tmk local + leader


if ($area_id) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regional_market) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}


$this->view->list_store = $list_store;
$this->view->list_area = $list_area;
$this->view->params = $params;
$this->view->list_sale = $list_sale;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);

$this->view->url = HOST . 'trade/check-shop-entire-list' . ($params ? '?' . http_build_query($params) . '&' : '?');



