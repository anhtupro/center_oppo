<?php
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$page_reject = $this->getRequest()->getParam('page_reject', 1);
$page_remove = $this->getRequest()->getParam('page_remove', 1);
//SEARCH
$name_search = $this->getRequest()->getParam('name_search');
$area_id_search = $this->getRequest()->getParam('area_id_search');
$sale_id = $this->getRequest()->getParam('staff_id');
$status_id = $this->getRequest()->getParam('status_id');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$title = $this->getRequest()->getParam('title');
$export = $this->getRequest()->getParam('export');
$hasFee = $this->getRequest()->getParam('has_fee');
$month = $this->getRequest()->getParam('month');
$statusFinish = $this->getRequest()->getParam('status_finish');
$contractor_id = $this->getRequest()->getParam('contractor_id');
$contract_number = $this->getRequest()->getParam('contract_number');
$big_area_id = $this->getRequest()->getParam('big_area_id');
$year = date('Y');
$export_review_cost = $this->getRequest()->getParam('export_review_cost');
$review_cost = $this->getRequest()->getParam('review_cost');

$list_reject_active = $this->getRequest()->getParam('list_reject_active');
$list_remove_active = $this->getRequest()->getParam('list_remove_active');

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QAppStatus = new Application_Model_AppStatus();
$QDestruction = new Application_Model_Destruction();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QArea = new Application_Model_Area();
$QContructors = new Application_Model_Contructors();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$flashMessenger = $this->_helper->flashMessenger;
$limit = LIMITATION;
$total = 0;

$params = [
    'status_id' => $status_id,
    'name_search' => $name_search,
    'area_id_search' => $area_id_search,
    'sale_id' => $sale_id,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'list_area' => $this->storage['area_id'],
    'title' => $title,
    'has_fee' =>$hasFee,
    'year' => $year,
    'month' => $month,
    'status_finish' => $statusFinish,
    'contractor_id' => $contractor_id,
    'contract_number' => TRIM($contract_number),
    'big_area_id' => $big_area_id,
    'review_cost' => $review_cost,

];

$params['sort'] = $sort;
$params['desc'] = $desc;

if (in_array($this->storage['title'], [SALES_TITLE])) {
    $params['staff_id'] = $this->storage['staff_id'];
}
$params['area_id'] = $this->storage['area_id'];
$list_sale = $QAppCheckShop->getSaleArea();
$destruction = $QDestruction->fetchPagination($page, $limit, $total, $params);
$destruction_reject = $QDestruction->fetchPaginationReject($page_reject, $limit, $total_reject, $params); // danh sách bị từ chối
$destruction_remove = $QDestruction->fetchPaginationRemove($page_remove, $limit, $total_remove, $params); // danh sách bị từ chối

$area_list = $QArea->getAreaList($params);

if ($export) {
    $statistics = $QDestruction->getStatistic($params);
    $QDestruction->export($statistics);
}

if ($export_review_cost) {
    $statistics = $QDestruction->getStatisticReviewCost($params);
    $QDestruction->exportReviewCost($statistics);
}

$listContractor = $QContructors->fetchAll();
$listBigArea = $QArea->getBigArea();
$this->view->listBigArea = $listBigArea;


$this->view->area = $area_list;
$this->view->list_sale = $list_sale;
$this->view->app_status = $QAppStatus->get_cache(12);

$this->view->params = $params;
$this->view->list_normal = $destruction;
$this->view->list_reject = $destruction_reject;
$this->view->list_remove = $destruction_remove;
$this->view->listContractor = $listContractor;

$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'trade/destruction-list' . ($params ? '?' . http_build_query($params) .
        '&' : '?');
$this->view->offset = $limit * ($page - 1);

// danh sách từ chối
$this->view->total_reject = $total_reject;
$this->view->url_reject = HOST . 'trade/destruction-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset_reject = $limit * ($page_reject - 1);

// danh sách hủy
$this->view->total_remove = $total_remove;
$this->view->url_remove = HOST . 'trade/destruction-list' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset_remove = $limit * ($page_remove - 1);


$this->view->list_reject_active = $list_reject_active;
$this->view->list_remove_active = $list_remove_active;

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>
