<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$listCategory = $this->getRequest()->getParam('category_id');
//$listQuantity = $this->getRequest()->getParam('quantity');
$areaId = $this->getRequest()->getParam('area_id');
$listIncreaseQuantity =  $this->getRequest()->getParam('increase_quantity');
$listDecreaseQuantity =  $this->getRequest()->getParam('decrease_quantity');
$listNote =  $this->getRequest()->getParam('note');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QInventoryAreaPosm = new Application_Model_InventoryAreaPosm();
$QInventoryAreaPosmDetail = new Application_Model_InventoryAreaPosmDetail();

if (! $areaId) {
    echo json_encode([
        'status' => 1,
        'message' => 'Thiếu ID khu vực!'
    ]);
    return;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();

// nhập kho
foreach ($listIncreaseQuantity as $categoryId => $quantity) {

    if ($quantity) {
        $QInventoryAreaPosmDetail->insert([
            'area_id' => $areaId,
            'category_id' => $categoryId,
            'quantity' => $quantity ? $quantity : Null,
            'type' => 1,
            'date' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id,
            'note' => $listNote[$categoryId]
        ]);

        $where = [
            'area_id = ?' => $areaId,
            'category_id = ?' => $categoryId
        ];
        $inventoryPosmRow = $QInventoryAreaPosm->fetchRow($where);

        if ($inventoryPosmRow) {
            $QInventoryAreaPosm->update([
                'quantity' => $quantity ? ($quantity + $inventoryPosmRow['quantity']) : $quantity
            ], $where);
        } else {
            $QInventoryAreaPosm->insert([
                'area_id' => $areaId,
                'category_id' => $categoryId,
                'quantity' => $quantity ? $quantity : Null
            ]);
        }
    }


}

// xuất kho
foreach ($listDecreaseQuantity as $categoryId => $quantity) {
    if ($quantity) {
        $QInventoryAreaPosmDetail->insert([
            'area_id' => $areaId,
            'category_id' => $categoryId,
            'quantity' => $quantity ? $quantity : Null,
            'type' => 2,
            'date' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id,
            'note' => $listNote[$categoryId]
        ]);

        $where = [
            'area_id = ?' => $areaId,
            'category_id = ?' => $categoryId
        ];
        $inventoryPosmRow = $QInventoryAreaPosm->fetchRow($where);

        if ($inventoryPosmRow) {
            $QInventoryAreaPosm->update([
                'quantity' => $quantity ? ($inventoryPosmRow['quantity'] - $quantity) : $quantity
            ], $where);
        } else {
            $QInventoryAreaPosm->insert([
                'area_id' => $areaId,
                'category_id' => $categoryId,
                'quantity' => $quantity ? $quantity : Null
            ]);
        }
    }
}

$db->commit();

echo json_encode([
   'status' => 0
]);