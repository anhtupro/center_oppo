<?php

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    
    $contract_number = $this->getRequest()->getParam('contract_number');
    $urgent_date = $this->getRequest()->getParam('urgent_date');
    $submit = $this->getRequest()->getParam('submit');
    
    $QContractNumber = new Application_Model_ContractNumber();
    $QTradeContract = new Application_Model_TradeContract();
    $QTradeContractDetails = new Application_Model_TradeContractDetails();
    $QPurchasingRequest         = new Application_Model_PurchasingRequest();
    $QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
    $QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $flashMessenger = $this->_helper->flashMessenger;
    
    if ($submit && $contract_number) {
        
        $db = Zend_Registry::get('db');
        try {
            $db->beginTransaction();

            $list_air_contract = $QContractNumber->getAirContractDetail($contract_number);
            $list_repair_contract = $QContractNumber->getRepairContractDetail($contract_number);
            $list_transfer_contract = $QContractNumber->getTransferContractDetail($contract_number);
            $list_destruction_contract = $QContractNumber->getDestructionContractDetail($contract_number);

            $list_air_cost = $QContractNumber->getAirContractCost($contract_number);
            $list_repair_cost = $QContractNumber->getRepairContractCost($contract_number);
            $list_transfer_cost = $QContractNumber->getTransferContractCost($contract_number);
            $list_destruction_cost = $QContractNumber->getDestructionContractCost($contract_number);
            
            // get contractor id
            if ($list_air_contract[0]['contractor_id']) {
                $contractor_id = $list_air_contract[0]['contractor_id'];
            } elseif ($list_repair_contract[0]['contractor_id']) {
                $contractor_id = $list_repair_contract[0]['contractor_id'];
            } elseif ($list_transfer_contract[0]['contractor_id']) {
                $contractor_id = $list_transfer_contract[0]['contractor_id'];
            } elseif ($list_destruction_contract[0]['contractor_id']) {
                $contractor_id = $list_destruction_contract[0]['contractor_id'];
            }
            
            // get contractor name
            if ($list_air_contract[0]['contractor_name']) {
                $contractor_name = $list_air_contract[0]['contractor_name'];
            } elseif ($list_repair_contract[0]['contractor_name']) {
                $contractor_name = $list_repair_contract[0]['contractor_name'];
            } elseif ($list_transfer_contract[0]['contractor_name']) {
                $contractor_name = $list_transfer_contract[0]['contractor_name'];
            } elseif ($list_destruction_contract[0]['contractor_name']) {
                $contractor_name = $list_destruction_contract[0]['contractor_name'];
            }
            
            $type_tmk = [
                1 => 'Thi công',
                2 => 'Sửa chữa',
                3 => 'Điều chuyển',
                4 => 'Tiêu hủy',
            ];
            
            $data_trade_contract = [
                'contract_name' => $contract_number,
                'contract_desc' => "TMK Số hợp đồng ".$contract_number,
                'contructors_id' => $contractor_id,
                'urgent_date' => $urgent_date,
                'created_at' => date('Y-m-d'),
                'created_by' => $userStorage->id
            ];
            
            $where = $QTradeContract->getAdapter()->quoteInto('contract_name = ?',$contract_number);
            $trade_contract = $QTradeContract->fetchRow($where);
            
            if($trade_contract){
                $flashMessenger->setNamespace('error')->addMessage('Hợp đồng đã được liên kết.');
                $this->redirect('/trade/contract-number-detail?contract_number='.$contract_number);
            }
            
            $id = $QTradeContract->insert($data_trade_contract);
            
            $trade_contract_details = [];
            //Thi công
            foreach($list_air_contract as $key=>$value){
                $desc = 'Thi công ';
                $total_fee = 0;
                foreach ($list_air_cost[$value['detail_id']] as $air_cost){
                    $desc = $desc.' - '.$air_cost['title'];
                    $total_fee = $total_fee + $air_cost['total_price'];
                }
                
                $trade_contract_details[] = [
                    'trade_contract_id' => $id,
                    'store_id' => $value['store_id'],
                    'store_name' => $value['store_name'],
                    'category_id' => $value['category_id'],
                    'desc' => $desc,
                    'fee' => $total_fee*1.1,
                    'type' => 1,
                ];
            }
            
            //Sửa chữa
            foreach($list_repair_contract as $repair_detail){
                $desc = 'Sửa chữa';
                $total_fee = 0;
                foreach ($list_repair_cost[$repair_detail['detail_id']] as $repair_cost){
                    $desc = $desc.' - '.$repair_cost['title'];
                    $total_fee = $total_fee + $repair_cost['total_price'];
                }
                
                $trade_contract_details[] = [
                    'trade_contract_id' => $id,
                    'store_id' => $repair_detail['store_id'],
                    'store_name' => $repair_detail['store_name'],
                    'category_id' => $repair_detail['category_id'],
                    'desc' => $desc,
                    'fee' => $total_fee*1.1,
                    'type' => 2,
                ];
                
            }
            
            //Điều chuyển
            $desc_transfer = NULL;
            $total_fee_transfer = 0;
            foreach($list_transfer_contract as $transfer){
                $total_fee = 0;
                foreach ($list_transfer_cost[$transfer['id']] as $transfer_cost){
                    $total_fee = $total_fee + $transfer_cost['total_price'];
                }
                
                $desc = 'Điều chuyển - '.$transfer['category_name'];
                
                $trade_contract_details[] = [
                    'trade_contract_id' => $id,
                    'store_id' => $transfer['store_id'],
                    'store_name' => $transfer['store_name'],
                    'category_id' => 1,
                    'desc' => $desc,
                    'fee' => $total_fee*1.1,
                    'type' => 3,
                ];
            }
            
            
            //Tiêu hủy
            $desc_destruction = NULL;
            $total_destruction = 0;
            foreach ($list_destruction_contract as $destruction){
                $total_fee = 0;
                foreach ($list_destruction_cost[$destruction['id']] as $destruction_cost){
                    $total_fee = $total_fee + $destruction_cost['total_price'];
                }
                
                $desc = 'Tiêu hủy - '.$destruction['category_name'];
                
                $trade_contract_details[] = [
                    'trade_contract_id' => $id,
                    'store_id' => $destruction['store_id'],
                    'store_name' => $destruction['store_name'],
                    'category_id' => 1,
                    'desc' => $desc,
                    'fee' => $total_fee*1.1,
                    'type' => 4,
                ];
            }
            
            foreach ($trade_contract_details as $details) {
                $QTradeContractDetails->insert($details);
            }
            
            //Add qua purchasing
            $sn = date('YmdHis') . substr(microtime(), 2, 4);
            $type = 32;//Thi công - Sửa chữa - Điều chuyển - Tiêu hủy
            $area_id = 65; //HEAD OFFICE
            $department_id = 610; //RETAIL
            $team_id = 611; //TRADE MARKETING
            $remark = 'Đề xuất được tạo tự động từ hệ thống Trade Marketing';
            $shipping_address = "Đề xuất được tạo tự động từ hệ thống Trade Marketing";
            $purchasing_request = array(
                'name'             => $data_trade_contract['contract_desc'],
                'type'             => 32,
                'sn'               => $sn,
                'urgent_date'      => $urgent_date,
                'project_id'       => NULL,
                'area_id'          => $area_id,
                'department_id'    => $department_id,
                'team_id'          => $team_id,
                'remark'           => $remark,
                'shipping_address' => $shipping_address,
                'status'           => 1, //new
                'created_by'       => $userStorage->id,
                'created_at'       => date('Y-m-d H:i:s'),
                'del' => 0,
                'contract_number' => $contract_number
            );
            $pr_id = $QPurchasingRequest->insert($purchasing_request);
            
            foreach($trade_contract_details as $contract_details){
                
                $get_code = $QTradeContract->getCodeFromCategoryId($contract_details['category_id'], $contract_details['type']);
                
                
                if(empty($get_code['code_id'])){
                    
                    $category_name = $QTradeContract->getCatName($contract_details['category_id']);
                    
                    $flashMessenger->setNamespace('error')->addMessage('Hạng mục chưa được liên kết với Purchasing: '. $category_name['name'].' - '.$type_tmk[$contract_details['type']]);
                    $this->redirect('/trade/contract-number-detail?contract_number='.$contract_number);
                }
                
                $purchasing_request_details = array(
                    'pr_id'             => $pr_id,
                    'pmodel_code_id'    => $get_code['code_id'],
                    'quantity'          => 1,
                    'installation_at'   => $contract_details['store_name'],
                    'installation_time' => NULL,
                    'note'              => NULL,
                );
                $pr_detail_id = $QPurchasingRequestDetails->insert($purchasing_request_details);
                
                $supplier_id = $QTradeContract->getSupplierFromContructors($data_trade_contract['contructors_id']);
                
                if(empty($supplier_id['supplier_id'])){
                    
                    $contructors_name = $QTradeContract->getContructorsId($data_trade_contract['contructors_id']);
                    
                    $flashMessenger->setNamespace('error')->addMessage('Nhà cung cấp chưa được liên kết với Purchasing: '. $contructors_name['name']);
                    $this->redirect('/trade/contract-number-detail?contract_number='.$contract_number);
                }
                
                $purchasing_request_supplier = array(
                    'pr_id'           => $pr_id,
                    'pr_detail_id'    => $pr_detail_id,
                    'select_supplier' => 1,
                    'supplier_id'     => $supplier_id['supplier_id'],
                    'price_before_dp' => $contract_details['fee'],
                    'price'           => $contract_details['fee'],
                    'ck'              => 0,
                    'fee'             => 0,
                    'article'         => NULL,
                    'warranty'        => NULL,
                    'discount'        => NULL,
                    'article_other'   => NULL
                );

                $QPurchasingRequestSupplier->insert($purchasing_request_supplier);
            }
            
            $db->commit();
            
            $flashMessenger->setNamespace('success')->addMessage('Tạo Purchasing Request thành công !');
            $this->redirect('/trade/contract-number-detail?contract_number='.$contract_number);
            
        } catch (Exception $e) {

            $db->rollback();
            
            $flashMessenger->setNamespace('error')->addMessage('Có lỗi.'.$e->getMessage());
            $this->redirect('/trade/contract-number-detail?contract_number='.$contract_number);
            
        }
        
        
    }
    
    


