<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$listCheckshopDetailChildId = $this->getRequest()->getParam('checkshop_detail_child_id');
$listCategoryType = $this->getRequest()->getParam('category_type');
$listHeigth = $this->getRequest()->getParam('height');
$listWidth = $this->getRequest()->getParam('width');
$listMaterial = $this->getRequest()->getParam('material');
$listCategoryTypeChild = $this->getRequest()->getParam('category_type_child');
$checkshopId = $this->getRequest()->getParam('checkshop_id');
$listHeightPainting = $this->getRequest()->getParam('height_painting');
$listWidthPainting = $this->getRequest()->getParam('width_painting');
$listMaterialPainting = $this->getRequest()->getParam('material_painting');

$list_width_painting_update = $this->getRequest()->getParam('width_painting_update');
$list_height_painting_update = $this->getRequest()->getParam('height_painting_update');
$list_material_painting_update = $this->getRequest()->getParam('material_painting_update');

$change_picture_stage_id = $this->getRequest()->getParam('change_picture_stage_id');
$list_product_id = $this->getRequest()->getParam('product_id');
$list_logo_type = $this->getRequest()->getParam('logo_type');
$list_picture_note = $this->getRequest()->getParam('picture_note');

$list_product_id_child = $this->getRequest()->getParam('product_id_child');
$list_logo_type_child = $this->getRequest()->getParam('logo_type_child');
$list_picture_note_child = $this->getRequest()->getParam('picture_note_child');

$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();
$QChangePictureDetail = new Application_Model_ChangePictureDetail();
$QChangePictureStage = new Application_Model_ChangePictureStage();
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$checkshop = $QAppCheckshop->fetchRow(['id = ?' => $checkshopId]);
$store_id = $checkshop['store_id'];

if ($change_picture_stage_id) {
    $change_picture_stage = $QChangePictureStage->fetchRow(['id = ?' => $change_picture_stage_id]);
}

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {
    foreach ($listCheckshopDetailChildId as $key => $id) {

        $row_detail_child = $QAppCheckshopDetailChild->fetchRow(['id = ?' => $id]);
        $category_id = $row_detail_child['category_id'];

        $QAppCheckshopDetailChild->update([
            'height' => $listHeigth[$key] ? $listHeigth[$key] : Null,
            'width' => $listWidth[$key] ? $listWidth[$key] : Null,
            'category_type' => $listCategoryType[$key] ? $listCategoryType[$key] : Null,
            'category_type_child' => $listCategoryTypeChild[$key] ? $listCategoryTypeChild[$key]  : Null,
            'material_id' => $listMaterial[$key] ? $listMaterial[$key] : Null

        ], ['id = ?' => $id]);


        // insert chi tiết tranh
        foreach ($listWidthPainting[$id] as $index => $widthPainting) {
            // chi tiết tranh
            $heightPainting = $listHeightPainting[$id][$index] ? $listHeightPainting[$id][$index] : Null;
            $materialPainting = $listMaterialPainting[$id][$index] ? $listMaterialPainting[$id][$index] : Null;
            if ($widthPainting || $heightPainting || $materialPainting) {
                $child_painting_id = $QAppCheckshopDetailChildPainting->insert([
                    'app_checkshop_detail_child_id' => $id,
                    'width' => $widthPainting ? $widthPainting : Null,
                    'height' => $heightPainting,
                    'material_id' => $materialPainting
                ]);
            }

            // thay tranh
            if ($change_picture_stage['is_running'] == 1) {
                $product_id = $list_product_id_child[$id][$index] ? $list_product_id_child[$id][$index] : 0;
                $logo_type = $list_logo_type_child[$id][$index] ? $list_logo_type_child[$id][$index] : Null;
                $picture_note = $list_picture_note_child[$id][$index] ? $list_picture_note_child[$id][$index] : Null;

                if ($product_id) {
                    $QChangePictureDetail->insert([
                        'child_painting_id' => $child_painting_id,
                        'product_id' => $product_id,
                        'logo_type' => $logo_type,
                        'note' => TRIM($picture_note),
                        'stage_id' => $change_picture_stage_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id,
                        'store_id' => $store_id,
                        'category_id' => $category_id
                    ]);
                }

            }

        }
    }


    // update những chi tiết tranh đã có
    foreach ($list_width_painting_update as $detail_painting_id => $width_painting_update) {

        // get category_id from child_paitning_id
        $row_painting_child = $QAppCheckshopDetailChildPainting->fetchRow(['id = ?' => $detail_painting_id]);
        $row_detail_child = $QAppCheckshopDetailChild->fetchRow(['id = ?' => $row_painting_child['app_checkshop_detail_child_id']]);
        $category_id = $row_detail_child['category_id'];
        //end get category_id

        $QAppCheckshopDetailChildPainting->update([
            'width' => $width_painting_update ? $width_painting_update : Null,
            'height' => $list_height_painting_update[$detail_painting_id] ? $list_height_painting_update[$detail_painting_id] : Null,
            'material_id' => $list_material_painting_update[$detail_painting_id] ? $list_material_painting_update[$detail_painting_id] : Null
        ], ['id = ?' => $detail_painting_id]);


        if ($change_picture_stage['is_running'] == 1) {
            // update thay tranh
            $exist_picture = $QChangePictureDetail->fetchRow([
                'child_painting_id = ?' => $detail_painting_id,
                'stage_id = ?' => $change_picture_stage_id
            ]);

            if ($exist_picture) {
                $QChangePictureDetail->update([
                    'product_id' => $list_product_id_child[$detail_painting_id] ? $list_product_id_child[$detail_painting_id] : 0,
                    'logo_type' => $list_logo_type_child[$detail_painting_id] ? $list_logo_type_child[$detail_painting_id] : Null,
                    'note' => $list_picture_note_child[$detail_painting_id] ? TRIM($list_picture_note_child[$detail_painting_id]) : '',
                    'stage_id' => $change_picture_stage_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id,
                    'store_id' => $store_id,
                    'category_id' => $category_id

                ], [
                    'child_painting_id = ?' => $detail_painting_id,
                    'stage_id = ?' => $change_picture_stage_id
                ]);
            } else {
                if ($list_product_id_child[$detail_painting_id]) {
                    $QChangePictureDetail->insert([
                        'child_painting_id' => $detail_painting_id,
                        'product_id' => $list_product_id_child[$detail_painting_id],
                        'logo_type' => $list_logo_type_child[$detail_painting_id],
                        'note' => TRIM($list_picture_note_child[$detail_painting_id]),
                        'stage_id' => $change_picture_stage_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id,
                        'store_id' => $store_id,
                        'category_id' => $category_id
                    ]);
                }

            }
        }

    }


    // thay tranh đối với các hạng mục không có chi tiết tranh
    if ($change_picture_stage['is_running'] == 1) {
        foreach ($list_product_id as $child_id =>  $product_id) {

            // get category_id from child_paitning_id
            $row_detail_child = $QAppCheckshopDetailChild->fetchRow(['id = ?' => $child_id]);
            $category_id = $row_detail_child['category_id'];
            //end get category_id

                $exist_picture = $QChangePictureDetail->fetchRow([
                    'app_checkshop_detail_child_id = ?' => $child_id,
                    'stage_id = ?' => $change_picture_stage_id
                ]);

                if ($exist_picture) {
                    $QChangePictureDetail->update([
                        'product_id' => $product_id ? $product_id : 0,
                        'logo_type' => $list_logo_type[$child_id] ? $list_logo_type[$child_id] : Null,
                        'note' => $list_picture_note[$child_id] ? TRIM($list_picture_note[$child_id]) : '',
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id,
                        'store_id' => $store_id,
                        'category_id' => $category_id
                    ], ['id = ?' => $exist_picture['id']]);
                } else {
                    if ($product_id) {
                        $QChangePictureDetail->insert([
                            'app_checkshop_detail_child_id' => $child_id,
                            'product_id' => $product_id,
                            'logo_type' => $list_logo_type[$child_id],
                            'note' => TRIM($list_picture_note[$child_id]),
                            'stage_id' => $change_picture_stage_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => $userStorage->id,
                            'store_id' => $store_id,
                            'category_id' => $category_id
                        ]);
                    }

                }

        }

    }



    // xác nhận sop đã được update thông tin chi tiết hạng mục
    $QAppCheckshop->update([
        'updated_info_detail' => 1
    ], ['id = ?' => $checkshopId]);

    $db->commit();
} catch (\Exception $e) {
    echo json_encode([
       'status' => 1,
       'message' => $e->getMessage()
    ]);
    return;
}

echo json_encode([
    'status' => 0
]);
