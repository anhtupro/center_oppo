<?php 
	
	$store_id = $this->getRequest()->getParam('id_shop');
	$category_id = $this->getRequest()->getParam('category_id');

	$params = [
		'store_id' 		=> $store_id,
		'category_id' 	=> $category_id
	];

	$QStore 	= new Application_Model_Store();
	$QBiTrade 	= new Application_Model_BiTrade();
	$QKpiMonth  = new Application_Model_KpiMonth();
	$QAppRule   = new Application_Model_AppRule();
	$QAppAir	= new Application_Model_AppAir();
	$QAppCheckShop = new Application_Model_AppCheckshop();
	$QAppCheckShopFile = new Application_Model_AppCheckShopFile();
	//$QAppImeiKpi = new Application_Model_ImeiKpi();


	$where = $QStore->getAdapter()->quoteInto("id = ?", $store_id);
    $store = $QStore->fetchRow($where);

	
    $investments = $QBiTrade->getDataByStore($store_id);

    //Lấy hạn mức đầu tư theo S.O
    $sellout_lastmonth 	= $QBiTrade->getSelloutLastMonth($store_id);
    $params['so'] 		= !empty($sellout_lastmonth['quantity']) ? $sellout_lastmonth['quantity'] : 0;
    $get_rule 			= $QAppRule->getRule($params);
    $rule = [];
    foreach($get_rule as $key=>$value){
    	$rule[$value['category_id']] = $value['value'];
    }
    //END get

    $investments_img = $QKpiMonth->get_investments_by_store($params);

    $rand = rand(1,4);

    /* Tính hạn mức */
    $hanmuc = 0;
    if($sellout_lastmonth['quantity'] >= 5 AND $sellout_lastmonth['quantity'] <15){
    	if($category_id == 1){
    		$hanmuc = 1;
    	}
    	else{
    		$hanmuc = 0;
    	}
    }
    elseif($sellout_lastmonth['quantity'] >= 15 AND $sellout_lastmonth['quantity'] <50){
    	if($category_id == 1){
    		$hanmuc = 2;
    	}
    	else{
    		$hanmuc = 0;
    	}
    }
    elseif($sellout_lastmonth['quantity'] >= 50){
    	if($category_id == 1){
    		$hanmuc = 3;
    	}
    	else{
    		$hanmuc = 0;
    	}
    }
    /* END Tính hạn mức */

	$data = [];
	
	//Hạn mức
	$data['hanmuc'] = [
		'store_id'		=> $store_id,
		'han_muc'		=> $hanmuc,
		'da_dang_ky'	=> 0,
		'shop_name'		=> $store['name']
	];


	//Đầu tư
	$data['dautu'][] = [
			'ten_hang_muc' => '',
			'so_luong'	   => '',
			'ngay'		   => ''
		];


	foreach ($investments as $key => $value) {

		$data['dautu'][] = [
			'id_hang_muc'	=>$value['category_id'],
			'group_bi'		=>$value['group_bi'],
			'ten_hang_muc' => $value['category_name'],
			'so_luong'	   => $value['quantity'],
			'ngay'		   => date('d/m/Y', strtotime($value['invested_at'])),
			'gia'		   => $value['value'],
			'hanmuc'	   => !empty($rule[$value['category_id']]) ? $rule[$value['category_id']] : 0
		];

		if($value['category_id'] == $category_id){
			$data['hanmuc']['da_dang_ky'] = $value['quantity'];
		}
	}
	

	//Hình ảnh
	$data['hinhanh'][] = [
		'ten_hang_muc' => '',
		'url'	   	   => '',
	];

	foreach ($investments_img as $key => $value) {

		if(isset($value['photo']) and $value['photo']){
			$data['hinhanh'][] = [
				'ten_hang_muc' => $value['name'],
				'url'	   	   => "https://trade-marketing.opposhop.vn/photo/investment/".$value['id']."/".$value['photo'],
			];
		}

		if(isset($value['photo1']) and $value['photo1']){
			$data['hinhanh'][] = [
				'ten_hang_muc' => $value['name'],
				'url'	   	   => "https://trade-marketing.opposhop.vn/photo/investment/".$value['id']."/".$value['photo1'],
			];
		}

		if(isset($value['photo2']) and $value['photo2']){
			$data['hinhanh'][] = [
				'ten_hang_muc' => $value['name'],
				'url'	   	   => "https://trade-marketing.opposhop.vn/photo/investment/".$value['id']."/".$value['photo2'],
			];
		}

		
	}
	//lấy sellout 3 tháng gần nhất
	$data['sell_out'] = $QAppAir->getSellOut($params);


//TUONG UPDATE 24/11
	 $sellout = $QAppCheckShop->getSellout($store_id);
     $level = $QAppCheckShop->getLevelStore($store_id);
     $data['sellout'] = $sellout;
     $data['level'] = $level['level'];

    $app_checkshop = $QAppCheckShop->getInfoCheckshop($params);
    $data['checkshop'] = $app_checkshop;   
    $data['img'] = $QAppCheckShopFile->getImgCheckshop($params);
	echo json_encode($data);exit;




