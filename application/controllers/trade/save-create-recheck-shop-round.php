<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$id = $this->getRequest()->getParam('id');
$roundName = $this->getRequest()->getParam('round_name');
$fromDate = $this->getRequest()->getParam('from_date');
$toDate = $this->getRequest()->getParam('to_date');
$recheckMonth = $this->getRequest()->getParam('recheck_month');
$limitStore = $this->getRequest()->getParam('limit_store');
$recheck_type = $this->getRequest()->getParam('recheck_type');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QRecheckShopRound = new Application_Model_RecheckShopRound();
$QAssignAreaRecheckShop = new Application_Model_AssignAreaRecheckShop ();
$QStaff = new Application_Model_Staff();
$QAreaStaff = new Application_Model_AreaStaff ();

if (! $id) {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();

    $roundId = $QRecheckShopRound->insert([
        'name' => $roundName,
        'from_date' => date('Y-m-d',strtotime($fromDate)),
        'to_date' => date('Y-m-d',strtotime($toDate)),
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
        'limit_store' => $limitStore ? 1 : Null,
        'recheck_type' => $recheck_type
    ]);

    $listStaffId = $QStaff->getStaffByTeam(TEAM_TRADE_MARKETING);

    if ($recheckMonth) { // đánh giá hàng tháng
        foreach ($listStaffId as $staffId) {
            if ($staffId) {
                $areaStaff = $QAreaStaff->getAreaById($staffId);

                foreach ($areaStaff as $areaId) {
                    $QAssignAreaRecheckShop->insert([
                        'staff_id' => $staffId,
                        'recheck_shop_round_id' => $roundId,
                        'area_id' => $areaId
                    ]);
                }
            }
        }
    } else {
        //asign area for team trade marketing
        foreach ($listStaffId as $staffId) {
            if (in_array($staffId,[150, 156, 15038])) { // hiep.nguyen, khoinguyen.tran, tam.tran
                if ($staffId) {
                    $areaStaff = $QAreaStaff->getAreaById($staffId);

                    foreach ($areaStaff as $areaId) {
                        $QAssignAreaRecheckShop->insert([
                            'staff_id' => $staffId,
                            'recheck_shop_round_id' => $roundId,
                            'area_id' => $areaId
                        ]);
                    }
                }
            } else {
                $QAssignAreaRecheckShop->insert([
                    'staff_id' => $staffId,
                    'recheck_shop_round_id' => $roundId
                ]);
            }

        }
    }


    $db->commit();

    echo json_encode([
        'status' => 0,
        'message' => 'Đã tạo thành công'
    ]);
    return;
} else {
    $QRecheckShopRound->update([
        'name' => $roundName,
        'from_date' => date('Y-m-d',strtotime($fromDate)),
        'to_date' => date('Y-m-d',strtotime($toDate)),
        'updated_at' => date('Y-m-d H:i:s'),
        'updated_by' => $userStorage->id,
        'recheck_type' => $recheck_type
    ], ['id = ?'=> $id ]);

    echo json_encode([
        'status' => 0,
        'message' => 'Đã chỉnh sửa thành công'
    ]);
    return;
}

