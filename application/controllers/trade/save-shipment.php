<?php 
	
	$campaign_id 			= $this->getRequest()->getParam('campaign_id');
	$contractor_quantity_id = $this->getRequest()->getParam('contractor_quantity_id');
	$area_id 				= $this->getRequest()->getParam('area_id');
	$quantity 				= $this->getRequest()->getParam('quantity');
	$transporter_id 		= $this->getRequest()->getParam('transporter_id');
	$price 					= $this->getRequest()->getParam('price');
	$note 					= $this->getRequest()->getParam('note');
	$contractor_id 			= $this->getRequest()->getParam('contractor_id');
	$category_id 			= $this->getRequest()->getParam('category_id');

	$flashMessenger = $this->_helper->flashMessenger;
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id = $userStorage->id;

	$back_url = HOST.'trade/create-shipment';

	$db             = Zend_Registry::get('db');
	$db->beginTransaction();
	try
	{

		$QAppShipment 	   	 = new Application_Model_AppShipment();
		$QAppShipmentDetails = new Application_Model_AppShipmentDetails();

		$last_number = $QAppShipment->getLastNumber() ? $QAppShipment->getLastNumber() : 0;

		$no = 'PXK'.str_pad( ($last_number['id'] + 1), 5, "0", STR_PAD_LEFT);

		$shipment = [
			'no'  			 => $no,
			'transporter_id' => $transporter_id,
			'campaign_id'	 => $campaign_id,
			'note'			 => $note,
			'created_by'	 => $staff_id,
			'created_at'	 => date('Y-m-d H:i:s'),
			'status'		 => 1
		];

		$shipment_id = $QAppShipment->insert($shipment);

		foreach($area_id as $key=>$value){
			$shipment_details = [
				'shipment_id'	=> $shipment_id,
				'area_id'		=> $value,
				'category_id'	=> $category_id[$key],
				'quantity'		=> $quantity[$key],
				'price'			=> $price[$key],
				'contractor_id' => $contractor_id[$key]
			];
			$QAppShipmentDetails->insert($shipment_details);
		}

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Tạo phiếu thành công!');
    	$this->redirect($back_url);


	}
	catch (Exception $e)
	{
		$db->rollBack();

	    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    	$this->redirect($back_url);
	}
	exit;




