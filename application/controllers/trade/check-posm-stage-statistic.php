<?php
$stage_id = $this->getRequest()->getParam('stage_id');
$export_store_finish = $this->getRequest()->getParam('export_store_finish');

$params = [
    'stage_id' => $stage_id,
    'area_list' => $this->storage['area_id']
];


$QCheckPosmStage = new Application_Model_CheckPosmStage();
$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QCheckPosmDetail = new Application_Model_CheckPosmDetail();

$stage = $QCheckPosmStage->fetchRow(['id = ?' => $stage_id]);
//$list_category = $QCategory->fetchAll(['check_posm = ?' => 1])->toArray();
$list_category = $QCheckPosmDetail->getListCategory($stage_id);

$list_area = $QArea->getListArea($params);

if ($export_store_finish) {
    $QCheckPosmStage->exportStoreFinish($params);
}

// theo số lượng hạng mục
$statistic_category = $QCheckPosmStage->getStatisticCategory($params);
$statistic_category_all = $QCheckPosmStage->getStatisticCategoryAll($params);// tất cả khu vực quản lý

// theo số lượng shop đã hoàn thành
// theo khu vực
$statistic_store_area = $QCheckPosmStage->getAllStoreEachArea($params);
$statistic_store_not_finish_area = $QCheckPosmStage->getNotFinishStoreEachArea($params);

// biểu đồ shop hoàn thành
foreach ($statistic_store_area as $value) {
    $count_store_finish = $value['count_store'] - $statistic_store_not_finish_area [$value['area_id']] ['count_store'];
    $count_all_store = $value['count_store'];

    $percent_finish_area [$value['area_id']] ['area_name'] = $value['area_name'];
    $percent_finish_area [$value['area_id']] ['count_store_all'] = $count_all_store;
    $percent_finish_area [$value['area_id']] ['count_store_finish'] = $count_store_finish;
    $percent_finish_area [$value['area_id']] ['percent_finish'] = round($count_store_finish * 100 / $count_all_store);
}
// sorting array by percent
usort($percent_finish_area, function($a, $b) {
    return  $b['percent_finish'] - $a['percent_finish'];
});
// end biểu đồ shop hoàn thành



// biểu đồ thể hiện độ phủ
$statistic_store_has_distribution_area = $QCheckPosmStage->getHasDistributionStoreEachArea($params);

foreach ($statistic_store_area as $value) {
    $count_store_has_distribution = $statistic_store_has_distribution_area [$value['area_id']] ['count_store'];
    $count_all_store = $value['count_store'];

    $percent_has_distribution_area [$value['area_id']] ['area_name'] = $value['area_name'];
    $percent_has_distribution_area [$value['area_id']] ['count_store_all'] = $count_all_store;
    $percent_has_distribution_area [$value['area_id']] ['count_store_has_distribution'] = $count_store_has_distribution;
    $percent_has_distribution_area [$value['area_id']] ['percent_has_distribution'] = round($count_store_has_distribution * 100 / $count_all_store);
}

// sorting array by percent
usort($percent_has_distribution_area, function($a, $b) {
    return  $b['percent_has_distribution'] - $a['percent_has_distribution'];
});
//end  biểu đồ thể hiện độ phủ


// tất cả
$statistic_store_all = $QCheckPosmStage->getAllStore($params);
$statistic_store_not_finish_all = $QCheckPosmStage->getAllFinishStore($params);

$this->view->list_category = $list_category;
$this->view->list_area = $list_area;
$this->view->stage = $stage;
$this->view->statistic_category = $statistic_category;
$this->view->statistic_category_all = $statistic_category_all;

$this->view->statistic_store_area = $statistic_store_area;
$this->view->statistic_store_not_finish_area = $statistic_store_not_finish_area;

$this->view->statistic_store_all = $statistic_store_all;
$this->view->statistic_store_not_finish_all = $statistic_store_not_finish_all;

// chart
$this->view->percent_finish_area = json_encode($percent_finish_area);
$this->view->percent_has_distribution_area = json_encode($percent_has_distribution_area);

