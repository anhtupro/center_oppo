<?php 
	
	$category_id = $this->getRequest()->getParam('category_id');
	$campaign_id = $this->getRequest()->getParam('campaign_id');

	$QAppContractorQuantity = new Application_Model_AppContractorQuantity();
	$QAppOrderDetails 		= new Application_Model_AppOrderDetails();
	$QArea 					= new Application_Model_Area();

	$params = [
		'category_id'    	=> $category_id,
		'campaign_id'		=> $campaign_id,
	];

	$area      		 = $QArea->get_cache();

	$area_list 		 = $QAppContractorQuantity->getAreaContractorQuantity($params);

	$area_contractor_data = [];
	foreach($area_contractor as $key=>$value){
		$area_contractor_data[$value['area_id']] = $value['quantity'];
	}

	$data = [];

	$stt = 1;

	foreach($area_list as $key=>$value){
		$data[] 			= [
			'stt' 			=> $stt,
			'area_id' 		=> $value['area_id'],
			'area_name' 	=> $area[$value['area_id']],
			'category_id'	=> $value['category_id'],
			'quantity'		=> $value['quantity'],
			'campaign_id'	=> $value['campaign_id'],
			'quantity_out'	=> $value['quantity_out'] ? $value['quantity_out'] : 0
		];

		$stt++;
	}

	echo json_encode($data);exit;
	



