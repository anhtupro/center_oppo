<?php 
	
	$page             = $this->getRequest()->getParam('page');
	$submit           = $this->getRequest()->getParam('submit');
	$category_id      = $this->getRequest()->getParam('category_id');

	$price         	  = $this->getRequest()->getParam('price');
	$campaign_id      = $this->getRequest()->getParam('campaign_id');
	$contractor_id    = $this->getRequest()->getParam('contractor_id');
	$ajax    		  = $this->getRequest()->getParam('ajax');

	$QContractorPrice 	= new Application_Model_ContractorPrice();
	$QCampaign 			= new Application_Model_Campaign();
	$QContructors 		= new Application_Model_Contructors();
	$QCategory 			= new Application_Model_Category();

	$params = [
		'campaign_id' 		=> $campaign_id,
		'contractor_id'		=> $contractor_id
	];

	$contractor_price = $QContractorPrice->getContractorPrice($params);

	$flashMessenger = $this->_helper->flashMessenger;

	$campaign 		= $QCampaign->getCampaign($params);
	$contructors 	= $QContructors->GetAll();
	$category       = $QCategory->GetCategoryPosm();

	$limit = LIMITATION;
	$total = 0;
	//$campaign 	   = $QContractorPrice->fetchPagination($page, $limit, $total, $params);

	$back_url = '/trade/contractor-price';
	if(!empty($submit) AND  $submit == 1){
		foreach($category_id as $key=>$value){
			if($price[$key] > 0){

				$where = [];
				$where[] = $QContractorPrice->getAdapter()->quoteInto('contractor_id = ?',  $contractor_id);
				$where[] = $QContractorPrice->getAdapter()->quoteInto('category_id = ?',  $value);
				$QContractorPrice->update(['to_date'=>date('Y-m-d H:i:s')], $where);

				$insert = [
					'contractor_id'	=> $contractor_id,
					'category_id'	=> $value,
					'price'			=> $price[$key],
					'from_date'		=> date('Y-m-d H:i:s')
				];
				$QContractorPrice->insert($insert);

			}
		}
		$flashMessenger->setNamespace('success')->addMessage('Đăng ký thành công!');
    	$this->redirect($back_url);
	}

	if(!empty($ajax) AND  $ajax == 1){
		$params = [
			'contractor_id' => $contractor_id
		];
		$contractor_price = $QContractorPrice->getContractorPrice($params);
		echo json_encode($contractor_price);exit;
	}

	$this->view->campaign   	= $campaign;
	$this->view->contructors 	= $contructors;
	$this->view->category       = $category;
	$this->view->contractor_price = $contractor_price;
	$this->view->contractor_id  = $contractor_id;


	$this->view->offset 	= $limit * ($page - 1);
	$this->view->campaign   = $campaign;
	$this->view->params 	= $params;
    $this->view->limit  	= $limit;
    $this->view->total 		= $total;
	$this->view->url 		= HOST . 'trade/list-posm/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');
	



