<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$areaId = $this->getRequest()->getParam('area_id');

$QInventoryArea = new Application_Model_AirInventoryArea();

if (!$areaId) {
    echo json_encode([
       'status' => 1,
       'message' => 'Thiếu khu vực truyền vào'
    ]);
    return;
}

$infoWarehouse = $QInventoryArea->getInfoWarehouse($areaId);

echo json_encode([
   'status' => 0,
    'data' => $infoWarehouse
]);

