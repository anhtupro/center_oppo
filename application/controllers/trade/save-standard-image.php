<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$image = $this->getRequest()->getParam('image');
$store_id = $this->getRequest()->getParam('store_id');
$category_deployment = $this->getRequest()->getParam('category_deployment');
$quantity_deployment = $this->getRequest()->getParam('quantity_deployment');

$QStandardImage = new Application_Model_StandardImage();
$QStandardImageLog = new Application_Model_StandardImageLog();
$QStoreCategoryDeployment = new Application_Model_StoreCategoryDeployment();
if (!$store_id) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa xác nhận được shop'
    ]);

    return;
}


foreach($image as $k=>$v){

    $data = $v;
    list($type, $data) = explode(';', $data);
    list(, $data)      = explode(',', $data);
    $data = base64_decode($data);

    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'standard_image' . DIRECTORY_SEPARATOR . $store_id;

    if (!is_dir($uploaded_dir))
        @mkdir($uploaded_dir, 0777, true);

    $newName = md5(uniqid('', true)) . '.png';

    $url = '/photo/standard_image/' . $store_id . '/' . $newName;
    
    $file = $uploaded_dir . '/' . $newName;

    $success_upload = file_put_contents($uploaded_dir . '/' . $newName, $data);


    $url_resize = 'photo' .
        DIRECTORY_SEPARATOR . 'standard_image' . DIRECTORY_SEPARATOR . $store_id . DIRECTORY_SEPARATOR . $newName;

            //Resize Anh
    $image = new My_Image_Resize();
    $image->load($url_resize);
    $image->resizeToWidth(800);
    $image->save($url_resize);
              //END Resize


    require_once "Aws_s3.php";
    $s3_lib = new Aws_s3();
    $destination_s3 = 'photo/standard_image/' . $store_id . '/';

    if ($success_upload) {
        $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
    }



    $image_insert = [
        'store_id' => $store_id,
        'url'    => $url
    ];

    $QStandardImage->insert($image_insert);
}


$QStandardImageLog->delete(['store_id = ?' => $store_id]);
$QStandardImageLog->insert([
   'store_id' => $store_id,
   'status' => 1
]);



// hạng mục có thể triển khai ở shop

    $QStoreCategoryDeployment->delete(['store_id = ?' => $store_id]);

    foreach ($category_deployment as $i => $category_id) {
        $QStoreCategoryDeployment->insert([
            'store_id' => $store_id,
            'category_id' => $category_id ? $category_id : Null,
            'quantity' => $quantity_deployment[$i] ? $quantity_deployment[$i] : Null
        ]);
    }

echo json_encode([
   'status' => 0
]);
