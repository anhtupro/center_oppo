<?php
$areaId = $this->getRequest()->getParam('area_id');
$storeId = $this->getRequest()->getParam('store_id');
$storeName = $this->getRequest()->getParam('store_name');
$regionalMarket = $this->getRequest()->getParam('regional_market');
$district = $this->getRequest()->getParam('district');
$staffId = $this->getRequest()->getParam('staff_id');
$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;

$QStatisticShop = new Application_Model_StatisticShop();
$QRegionalMarket= new Application_Model_RegionalMarket();
$QArea = new Application_Model_Area();

$params = [
    'area_id' => $areaId,
    'store_id' => $storeId,
    'store_name' => $storeName,
    'regional_market' => $regionalMarket,
    'district' => $district,
    'staff_id' => $staffId,
    'list_area' => $this->storage['area_id']
];


$listStore = $QStatisticShop->fetchPagination($page, $limit, $total, $params);
$listArea = $QArea->getListArea($params);

if ($areaId) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $areaId);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regionalMarket) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regionalMarket);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$this->view->listStore = $listStore;
$this->view->listArea = $listArea;
$this->view->params = $params;
//$this->view->list_sale = $list_sale;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);

$this->view->url = HOST . 'trade/statistic-shop-list' . ($params ? '?' . http_build_query($params) . '&' : '?');

