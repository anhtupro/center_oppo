<?php
set_time_limit(0);
ini_set('memory_limit', '300M');
$flashMessenger       = $this->_helper->flashMessenger;

$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$submit 	 = $this->getRequest()->getParam('submit');
$key_search	 = $this->getRequest()->getParam('search_shop_check');
$name_search	 = $this->getRequest()->getParam('name_search');
$area_id	 = $this->getRequest()->getParam('area_id');
$regional_market = $this->getRequest()->getParam('regional_market');
$district	 = $this->getRequest()->getParam('district');
$staff_id	 = $this->getRequest()->getParam('staff_id');
$trade_local_id	 = $this->getRequest()->getParam('trade_local_id');
$is_ka           = $this->getRequest()->getParam('is_ka');
$is_ka_details   = $this->getRequest()->getParam('is_ka_details');
$store_id = $this->getRequest()->getParam('store_id');

$checkshop_qc_selectbox = $this->getRequest()->getParam('checkshop_qc_selectbox');//TUONG
$checkshop_qc_selectbox_realme = $this->getRequest()->getParam('checkshop_qc_selectbox_realme');//Author : Trang
$from_date      = $this->getRequest()->getParam('from_date', date('Y-m-01'));
$to_date        = $this->getRequest()->getParam('to_date', date('Y-m-d'));

$from_date = str_replace('/', '-', $from_date);
$to_date = str_replace('/', '-', $to_date);

$from_date_time = date('Y-m-d 00:00:00', strtotime($from_date));
$to_date_time = date('Y-m-d 23:59:59', strtotime($to_date));

$type_qc        = $this->getRequest()->getParam('type_qc');
$type_qc_realme        = $this->getRequest()->getParam('type_qc_realme'); // Author : Trang

$eligible        = $this->getRequest()->getParam('eligible');
$month_eligible  = $this->getRequest()->getParam('month_eligible');
$year_eligible   = $this->getRequest()->getParam('year_eligible');

$is_pending   = $this->getRequest()->getParam('is_pending');
$big_area_id = $this->getRequest()->getParam('big_area_id');
$category_id = $this->getRequest()->getParam('category_id');
$is_lock = $this->getRequest()->getParam('is_lock');
$check_posm_stage_id = $this->getRequest()->getParam('check_posm_stage_id');
$list_store = $this->getRequest()->getParam('list_store');
$change_picture_stage_id = $this->getRequest()->getParam('change_picture_stage_id');
$export_temp_stop = $this->getRequest()->getParam('export_temp_stop');
$export_shop_size = $this->getRequest()->getParam('export_shop_size');
$export_flash_sale = $this->getRequest()->getParam('export_flash_sale');
$export_legal_checkshop = $this->getRequest()->getParam('export_legal_checkshop');
$legal_checkshop = $this->getRequest()->getParam('legal_checkshop');
$export_error_btn = $this->getRequest()->getParam('export_error_btn');
$status_error_btn = $this->getRequest()->getParam('status_error_btn');

$month        = $this->getRequest()->getParam('month', date('m', strtotime(date('Y-m')." -1 month")));
$month = $month ? $month : date('m', strtotime(date('Y-m')." -1 month"));

$year        = $this->getRequest()->getParam('year');
$year = $year ? $year : date('Y', strtotime(date('Y-m')." -1 month"));


if ($from_date && $to_date) {
    $QAppNoti = new Application_Model_AppNoti();
    $month_diff = $QAppNoti->checkDiffMonth($from_date, $to_date);
    if ($month_diff > 3) {
        $flashMessenger->setNamespace('error')->addMessage("Không thể search quá 3 tháng. Vui lòng chọn thời gian lại ");
        $this->_redirect('/trade/check-shop-list');
    }
}

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QStore = new Application_Model_Store();
$QCampaign = new Application_Model_Campaign();
$QCategory = new Application_Model_Category();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QAppCheckShopDetail = new Application_Model_AppCheckshopDetail();
$QAppFile = new Application_Model_AppFile();
$QArea = new Application_Model_Area();
$QAppCheckShopQc = new Application_Model_AppCheckshopQc(); //TUONG
$QAppCheckShopDetailBrand = new Application_Model_AppCheckshopDetailBrand();
$QAreaStaff = new Application_Model_AreaStaff();
$QCheckPosmStage = new Application_Model_CheckPosmStage();
$QChangePictureStage = new Application_Model_ChangePictureStage();
$QStoreTempStop = new Application_Model_StoreTempStop();
$QShopSize = new Application_Model_ShopSize();
$QFlashSale = new Application_Model_FlashSale();
$QStoreFlashSale = new Application_Model_StoreFlashSale();
$QAppCheckShopMapQcRealme = new Application_Model_AppCheckshopMapQcRealme();//Author : Trang
$QAppCheckshopQcRealme = new Application_Model_AppCheckshopQcRealme();//Author : Trang
$QCheckErrorBtn = new Application_Model_CheckErrorBtn();
$limit = 20;
$total = 0;

$params = [
    'from_date'    => $from_date,
    'to_date'      => $to_date,
    'from_date_time' => $from_date_time,
    'to_date_time' => $to_date_time,
    'list_area'      => $this->storage['area_id'],
    'area_id'      => $area_id,
    'regional_market'  => $regional_market,
    'district'         => $district,
    'staff_id'     => $staff_id,
    'name_search'  => $name_search,
    'is_ka'        => $is_ka,
    'is_ka_details' => $is_ka_details,
    'checkshop_qc_selectbox' => $checkshop_qc_selectbox, //TUONG
    'checkshop_qc_selectbox_realme' => $checkshop_qc_selectbox_realme, //Author : Trang
    'month' => $month,
    'year'  => $year,
    'month_realme' => $month_realme,//Author : Trang
    'year_realme'  => $year_realme,//Author : Trang
    'type_qc' => $type_qc,
    'type_qc_realme' => $type_qc_realme,
    'eligible'  => $eligible,
    'month_eligible'  => $month_eligible,
    'year_eligible'  => $year_eligible,
    'is_pending' => $is_pending,
    'big_area_id' => $big_area_id,
    'category_id' => $category_id,
    'trade_local_id' => $trade_local_id,
    'is_lock' => $is_lock,
    'check_posm_stage_id' => $check_posm_stage_id,
    'list_store' => $list_store,
    'change_picture_stage_id' => $change_picture_stage_id,
    'is_staff_realme' => $userStorage->is_realme,
    'from_date_sellout' => $from_date_sellout,
    'to_date_sellout' => $to_date_sellout,
    'quantity_sellout' => $quantity_sellout,
    'store_id' => $store_id,
    'legal_checkshop' => $legal_checkshop,
    'status_error_btn' => $status_error_btn
];

if ($area_id) {
    $QRegionalMarket = new Application_Model_RegionalMarket();
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regional_market) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$params['sort'] = $sort;
$params['desc'] = $desc;
$params['title']=$userStorage->title;
$params['key_search']=$key_search;


if (in_array($userStorage->title, [SALES_TITLE, PGS_SUPERVISOR])) {
    $params['staff_id']= $userStorage->id;
    $this->view->is_sales = true;
}

if (in_array($userStorage->title, [SALES_LEADER_TITLE])) {
    $params['leader_id']= $userStorage->id;
}

// phân loại staff để chọn ra store quản lý
if (in_array($this->storage['title'], [SALES_TITLE, PGS_SUPERVISOR])) {
    $params['type_staff'] = 1;
} elseif (in_array($this->storage['title'], [SALES_LEADER_TITLE])) {
    $params['type_staff'] = 2;
} else {
    $params['type_staff'] = 3;
}


$list_sale  = $QAppCheckShop->getSaleArea();
$list_trade_local  = $QAreaStaff->getTradeArea(); // tmk local

$list_channel = $QAppCheckShop->getChannel();
$list_checkshop_qc = $QAppCheckShopQc->get_cache();
$list_checkshop_qc_realme = $QAppCheckshopQcRealme->get_cache();//Author : Trang

if ($export_temp_stop) {
    $QStoreTempStop->export($params);
}

if ($export_shop_size) {
    $QShopSize->export($params);
}

if($export_flash_sale){
    $QStoreFlashSale->export($params);
}
if ($export_legal_checkshop) {
    $QAppCheckShop->exportLegalCheckshop($params);
}

if ($export_error_btn) {
    $QCheckErrorBtn->export($params);
}

if(!empty($is_pending) AND $is_pending == 1){

    $shop_check_in_month_arr = [];
    $shop_check_in_month = $QAppCheckShop->getListCheckshopInMonth($params);
    foreach($shop_check_in_month as $key=>$value){
        $shop_check_in_month_arr[] = $value['store_id'];
    }

    $params['shop_check_in_month_arr'] = $shop_check_in_month_arr;
}

$resultList = $QAppCheckShop->fetchPagination($page, $limit, $total, $params);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();


$list_store = [];
foreach($resultList as $key=>$value){
    $list_store[] = $value['store_id'];
}

$result_qc = $QAppCheckShop->getListHaveQc($list_store, $month, $year);

$list_check_posm_stage = $QCheckPosmStage->fetchAll();
$list_change_picture_stage = $QChangePictureStage->fetchAll();

//$list_store =$QAppCheckShop->ListShop($params);
//var_dump(count($list_store)); exit;

$area_list = $QArea->getAreaList($params);
$listBigArea = $QArea->getBigArea();
$this->view->listBigArea = $listBigArea;
//$this->view->store =$list_store;
$this->view->list_sale = $list_sale;
$this->view->list_trade_local = $list_trade_local;
$this->view->list_channel = $list_channel;
$this->view->list_checkshop_qc = $list_checkshop_qc;//TUONG
$this->view->list_checkshop_qc_realme = $list_checkshop_qc_realme;//Author : Trang
$this->view->result_qc = $result_qc;
$this->view->area = $area_list;
$this->view->list= $resultList;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->list_check_posm_stage = $list_check_posm_stage;
$this->view->list_change_picture_stage = $list_change_picture_stage;
$this->view->flash_sale = $QFlashSale->fetchAll(['is_del = 0']);

$params['shop_check_in_month_arr'] = []; // fix lỗi URL too long
$this->view->url = HOST.'trade/check-shop-list'.($params ? '?'.http_build_query($params).'&' : '?');

$this->view->offset = $limit * ($page - 1);


// eXPORT
if(!empty($submit) && $submit=='Export')
{
    ini_set("memory_limit", -1);
    ini_set("display_error", 1);
    error_reporting(~E_ALL);
    
//    $data_merge = $QAppCheckShop->getExcelMerge($params);
    $data_merge = $QAppCheckShop->getStoreCheckshop($params);

    require_once 'PHPExcel.php';

    $PHPExcel = new PHPExcel();
    $heads = array(
        'Stt',
        'Kênh',
        'Store ID',
        'Dealer ID',
        'Partner ID',
        'Tên shop',
        'Type',
        'Địa chỉ',
        'Khu vực',
        'Tỉnh',
        'Quận huyện',
        'Tình trạng Local',
        'Tình trạng Leader',
        'Ngày checkshop gần nhất',
        'Người checkshop',
        'Hạng mục',
        'Số lượng',
        'Imei'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet    = $PHPExcel->getActiveSheet();

    $alpha    = 'A';
    $index    = 1;
    foreach($heads as $key)
    {
        $sheet->setCellValue($alpha.$index, $key);
        $alpha++;
    }
    $index    = 2;

    $i = 1;

    foreach($data_merge as $item){
        
        $imei_sn = str_replace("https://trade-marketing.opposhop.vn/tool/scan-imei?imei_sn=", " ", $item['imei']);
        
        $alpha    = 'A';
        $sheet->setCellValue($alpha++.$index, $i++);
        $sheet->setCellValue($alpha++.$index, $item['channel']);
        $sheet->setCellValue($alpha++.$index, $item['store_id']);
        $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
        $sheet->setCellValue($alpha++.$index, $item['partner_id']);
        $sheet->setCellValue($alpha++.$index, $item['store_name']);
        $sheet->setCellValue($alpha++.$index, $item['store_level']);
        $sheet->setCellValue($alpha++.$index, $item['shipping_address']);
        $sheet->setCellValue($alpha++.$index, $item['area_name']);
        $sheet->setCellValue($alpha++.$index, $item['province']);
        $sheet->setCellValue($alpha++.$index, $item['district']);
        $sheet->setCellValue($alpha++.$index, ($item['is_lock'] == 1 ? 'Đã xác nhận' : 'Chưa xác nhận'));
        $sheet->setCellValue($alpha++.$index, ($item['is_leader_lock'] == 1 ? 'Đã xác nhận' : 'Chưa xác nhận'));
        $sheet->setCellValue($alpha++.$index, (!empty($item['checkshop_last']) ? date('d/m/Y', strtotime($item['checkshop_last'])) : NULL));
        $sheet->setCellValue($alpha++.$index, $item['fullname']);
        $sheet->setCellValue($alpha++.$index, $item['category_name']);
        $sheet->setCellValue($alpha++.$index, $item['soluong']);
        $sheet->setCellValue($alpha++.$index, $imei_sn);

        $index++;

    }



    $filename = 'Report_Checkshop' . date('d-m-Y H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;

}

// eXPORT
if(!empty($submit) && $submit=='Export Time')
{
    ini_set("memory_limit", -1);
    ini_set("display_error", 1);
    error_reporting(~E_ALL);
    $data_merge = $QAppCheckShop->exportTime($params);

    require_once 'PHPExcel.php';

    $PHPExcel = new PHPExcel();
    $heads = array(
        'Stt',
        'Kênh',
        'Store ID',
        'Dealer ID',
        'Partner ID',
        'Tên shop',
        'Quận/Huyện',
        'Tỉnh',
        'Khu vực',
        'Tình trạng',
        'Ngày checkshop',
        'Sale',
        'Checkshop By',
        'Title'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet    = $PHPExcel->getActiveSheet();

    $alpha    = 'A';
    $index    = 1;
    foreach($heads as $key)
    {
        $sheet->setCellValue($alpha.$index, $key);
        $alpha++;
    }
    $index    = 2;

    $i = 1;

    foreach($data_merge as $item){
        
        $alpha    = 'A';
        $sheet->setCellValue($alpha++.$index, $i++);
        $sheet->setCellValue($alpha++.$index, $item['channel']);
        $sheet->setCellValue($alpha++.$index, $item['store_id']);
        $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
        $sheet->setCellValue($alpha++.$index, $item['partner_id']);
        $sheet->setCellValue($alpha++.$index, $item['store_name']);
        $sheet->setCellValue($alpha++.$index, $item['district']);
        $sheet->setCellValue($alpha++.$index, $item['province']);
        $sheet->setCellValue($alpha++.$index, $item['area_name']);
        $sheet->setCellValue($alpha++.$index, ($item['is_lock'] == 1 ? 'Ngày xác nhận' : NULL));
        $sheet->setCellValue($alpha++.$index, (!empty($item['checkshop_at']) ? date('d/m/Y', strtotime($item['checkshop_at'])) : NULL));
        $sheet->setCellValue($alpha++.$index, $item['fullname']);
        $sheet->setCellValue($alpha++.$index, $item['fullname_check']);
        $sheet->setCellValue($alpha++.$index, $item['title_check']);

        $index++;

    }
    
    $filename = 'Report_Checkshop_Time' . date('d-m-Y H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
    $objWriter->save('php://output');
    exit;

}

if(!empty($submit) && $submit=='Export QC')
{

	require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        $range_date = $this->month_range(date('Y-m-d', strtotime(date('Y-m')." -4 month")), date('Y-m-d'));
       
        $params ['range_date'] = $range_date;
    //Leader QC
        $alpha = 'I';
        $index = 1;
        foreach($range_date as $key=>$value){
            $sheet->setCellValue($alpha . $index, "Local");
            $alpha++;
            $sheet->setCellValue($alpha . $index, "Leader");
            $alpha++;
            $sheet->setCellValue($alpha . $index, "QC");
            $alpha++;
        }
        
        
        $alpha = 'A';
        $index = 2;
        
        $heads = array(
            'Store ID',
            'Store Name',
            'Store Address',
            'Area',
            'Dealer ID',
            'Dealer Name',
            'ID Gộp',
            'KA/D.C',
        );

        foreach($range_date as $key=>$value){
            $month = intval(date('m', strtotime($value)));
            $heads[] = $month;
            $heads[] = $month;
            $heads[] = $month;
        }


    foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        
        $data = $QAppCheckShop->getListShop($params);
//        $data = $QAppCheckShop->getStoreCheckshop($params);
         
         
         $data_qc = $QAppCheckShop->getQc($params);
         $list_channel_total = $QAppCheckShop->getChannelTotal();
         
         //merge những hàng có tên shop giống nhau trc
         $index++;
         foreach($data as $key =>$value){
            
            $alpha = 'A';
            
            $sheet->setCellValue($alpha++.$index, $value['store_id']);
            $sheet->setCellValue($alpha++.$index, $value['store_name']);
            $sheet->setCellValue($alpha++.$index, $value['shipping_address']);
            $sheet->setCellValue($alpha++.$index, $value['area_name']);
            $sheet->setCellValue($alpha++.$index, $value['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $value['dealer_name']);
            $sheet->setCellValue($alpha++.$index, $value['parent_id']);
            $sheet->setCellValue($alpha++.$index, $value['channel']);
            
            foreach($range_date as $k=>$v){
                $month = intval(date('m', strtotime($v)));
                $qc_local = $data_qc[$value['store_id']][$month][1];
                $qc_leader = $data_qc[$value['store_id']][$month][2];
                $qc = $data_qc[$value['store_id']][$month][3];
                
                $sheet->setCellValue($alpha++.$index, $qc_local);
                $sheet->setCellValue($alpha++.$index, $qc_leader);
                $sheet->setCellValue($alpha++.$index, $qc);
            }
            $index++;
         } 
        
        $filename = ' Export QC ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
}

// export qc realme ; Author : Trang
if(!empty($submit) && $submit=='Export QC Realme')
{

	require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        $range_date = $this->month_range(date('Y-m-d', strtotime(date('Y-m')." -4 month")), date('Y-m-d'));
       
        $params ['range_date'] = $range_date;
    //Leader QC
        $alpha = 'I';
        $index = 1;
        foreach($range_date as $key=>$value){
            $sheet->setCellValue($alpha . $index, "Local");
            $alpha++;
            $sheet->setCellValue($alpha . $index, "Supervisor Realme");
            $alpha++;
            $sheet->setCellValue($alpha . $index, "QC");
            $alpha++;
        }
        
        
        $alpha = 'A';
        $index = 2;
        
        $heads = array(
            'Store ID',
            'Store Name',
            'Store Address',
            'Area',
            'Dealer ID',
            'Dealer Name',
            'ID Gộp',
            'KA/D.C',
        );

        foreach($range_date as $key=>$value){
            $month = intval(date('m', strtotime($value)));
            $heads[] = $month;
            $heads[] = $month;
            $heads[] = $month;
        }


    foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        
        $data = $QAppCheckShop->getListShop($params);
         
        $data_qc = $QAppCheckShopMapQcRealme->getQc($params);
        $list_channel_total = $QAppCheckShop->getChannelTotal();
         
         //merge những hàng có tên shop giống nhau trc
         $index++;
         foreach($data as $key =>$value){
            
            $alpha = 'A';
            
            $sheet->setCellValue($alpha++.$index, $value['store_id']);
            $sheet->setCellValue($alpha++.$index, $value['store_name']);
            $sheet->setCellValue($alpha++.$index, $value['shipping_address']);
            $sheet->setCellValue($alpha++.$index, $value['area_name']);
            $sheet->setCellValue($alpha++.$index, $value['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $value['dealer_name']);
            $sheet->setCellValue($alpha++.$index, $value['parent_id']);
            $sheet->setCellValue($alpha++.$index, $value['channel']);
            
            foreach($range_date as $k=>$v){
                $month = intval(date('m', strtotime($v)));
                $qc_local = $data_qc[$value['store_id']][$month][1];
                $qc_leader = $data_qc[$value['store_id']][$month][2];
                $qc = $data_qc[$value['store_id']][$month][3];
                
                $sheet->setCellValue($alpha++.$index, $qc_local);
                $sheet->setCellValue($alpha++.$index, $qc_leader);
                $sheet->setCellValue($alpha++.$index, $qc);
            }
            $index++;
         } 
        
        $filename = ' Export QC Realme ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
}
//end export qc realme ; Author : Trang

if(!empty($submit) && $submit=='Export QC New')
{

	require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        $range_date = $this->month_range(date('Y-m-d', strtotime(date('Y-m')." -4 month")), date('Y-m-d'));
        
        //Leader QC
        $alpha = 'H';
        $index = 1;
        foreach($range_date as $key=>$value){
 
            $sheet->setCellValue($alpha . $index, "Leader");
            $alpha++;
            $sheet->setCellValue($alpha . $index, "QC");
            $alpha++;
        }
        
        
        $alpha = 'A';
        $index = 2;
        
        $heads = array(
            'Store ID',
            'Store Name',
            'Store Address',
            'Area',
            'Dealer ID',
            'Dealer Name',
            'KA/D.C'
        );

        foreach($range_date as $key=>$value){
            $month = intval(date('m', strtotime($value)));
            $heads[] = $month;
            $heads[] = $month;
        }
        
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        
         $data = $QAppCheckShop->getListShop($params);
         
         $data_qc = $QAppCheckShop->getQcNew($params);
         
         $list_channel_total = $QAppCheckShop->getChannelTotal();
         
         //merge những hàng có tên shop giống nhau trc
         $index++;
         foreach($data as $key =>$value){
            
            $level = 'OTHER';
             
            if($value['is_ka'] == 1){
                $level = $list_channel_total[1][$value['channel']];
            }
            
            if(!empty($value['loyalty_plan_id']) AND $value['is_ka'] != 1){
                $level = $list_channel_total[0][$value['loyalty_plan_id']];
            }
            
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['store_id'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['store_name'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['shipping_address'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area_name'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['dealer_id'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['dealer_name'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($level,PHPExcel_Cell_DataType::TYPE_STRING);
            
            foreach($range_date as $k=>$v){
                $month = intval(date('m', strtotime($v)));
                $qc_leader = !empty($data_qc[$value['store_id']][$month][2]) ? $data_qc[$value['store_id']][$month][2] : 'KHÔNG ĐẠT - Chưa checkshop đủ số lượng theo yêu cầu.';
                $qc = !empty($data_qc[$value['store_id']][$month][3]) ? $data_qc[$value['store_id']][$month][3] : 'KHÔNG ĐẠT - Chưa checkshop đủ số lượng theo yêu cầu.';
                $sheet->getCell($alpha++ . $index)->setValueExplicit($qc_leader,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($qc,PHPExcel_Cell_DataType::TYPE_STRING);
            }
            $index++;
         } 
        
        $filename = ' Check-Shop-Information ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
}

if(!empty($submit) && $submit=='Export Dealer')
{

	require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        $range_date = $this->month_range(date('Y-m-d', strtotime(date('Y-m')." -4 month")), date('Y-m-d'));
        
        
        $alpha = 'A';
        $index = 1;
        
        $heads = array(
            'Dealer ID',
            'Dealer Name',
            'Store Address',
            'Area',
            'KA/D.C',
            'Result',
        );

        foreach($range_date as $key=>$value){
            $month = intval(date('m', strtotime($value)));
            $heads[] = $month;
            $heads[] = $month;
        }
        
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        
         $data = $QAppCheckShop->getListDealer($params);
         
         $data_qc = $QAppCheckShop->getQcNew($params);
         
         $list_channel_total = $QAppCheckShop->getChannelTotal();
         
         //merge những hàng có tên shop giống nhau trc
         $index++;
         foreach($data as $key =>$value){
            
            $level = 'OTHER';
             
            if($value['is_ka'] == 1){
                $level = $list_channel_total[1][$value['channel']];
            }
            
            if(!empty($value['loyalty_plan_id']) AND $value['is_ka'] != 1){
                $level = $list_channel_total[0][$value['loyalty_plan_id']];
            }
            
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['dealer_id'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['shipping_address'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area_name'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($level,PHPExcel_Cell_DataType::TYPE_STRING);
            
            $sheet->getCell($alpha++ . $index)->setValueExplicit("KHÔNG ĐẠT",PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
         } 
        
        $filename = ' Check-Shop-Information ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
        $objWriter->save('php://output');
        exit;
}

if ($submit == 'Export Outside') {
    $QAppCheckShopDetailBrand->exportOutside($params);
}

// END EXPORT
// ---------------hien thi thong bao ----------------------
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

	$messages             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages = $messages;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;
}

?>
