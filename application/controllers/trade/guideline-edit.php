<?php

$guidelineId = $this->getRequest()->getParam('id');

$QGuidelineFile = new Application_Model_GuidelineFile();
$QGuideline = new Application_Model_Guideline();

$listFile = $QGuidelineFile->fetchAll([
    'guideline_id = ?'=> $guidelineId,
    'is_deleted IS NULL'
]);
$guideline = $QGuideline->fetchRow([
    'id = ?' => $guidelineId,
    'is_deleted IS NULL'
]);

$this->view->listFile = $listFile;
$this->view->guideline= $guideline;



