<?php

$checkshop_id = $this->getRequest()->getParam('id');
$check_posm_stage_id = $this->getRequest()->getParam('check_posm_stage_id');
$change_picture_stage_id = $this->getRequest()->getParam('change_picture_stage_id');

$QAppCheckshop = new Application_Model_AppCheckshop();
$QAppCheckshopQc = new Application_Model_AppCheckshopQc();
$QAppCheckshopQcRealme = new Application_Model_AppCheckshopQcRealme();//Author:Trang
$QAppCheckshopMapQc = new Application_Model_AppCheckshopMapQc();
$QAppCheckshopMapQcRealme = new Application_Model_AppCheckshopMapQcRealme();//Author:Trang
$QStore = new Application_Model_Store();
$QCheckshopDetailBrand = new Application_Model_AppCheckshopDetailBrand();
$QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
$QAppCheckshopDetailChildPainting = new Application_Model_AppCheckshopDetailChildPainting();
$QCategory = new Application_Model_Category();
$QCheckPosmStage = new Application_Model_CheckPosmStage();
$QChangePictureStage = new Application_Model_ChangePictureStage();
$QShopSizeType = new Application_Model_ShopSizeType();
$QCheckErrorBtn = new Application_Model_CheckErrorBtn();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$title = $userStorage->title;

//if ($_GET['dev'] == 1) {
//    echo '<pre>';
//    print_r($title);
//    die;
//
//}

//if (in_array($userStorage->id, [557])) {
//    $title = TRADE_MARKETING_SUPERVISOR;
//}


if (!$checkshop_id) {
    echo 'Shop chưa được check shop';
    die;
}


$checkshop = $QAppCheckshop->getShopInfo($checkshop_id);
$checkshop_details = $QAppCheckshop->getCheckshopDetails($checkshop_id, 1);
$checkshop_details_realme = $QAppCheckshop->getCheckshopDetails($checkshop_id, 2);
$img_check = $QAppCheckshop->getImg($checkshop_id);
$img_different = $QAppCheckshop->getImgDifferent($checkshop_id);
$img_different_list = $QAppCheckshop->getImgDifferentList($checkshop['updated_at'], $checkshop['store_id'], $checkshop['id']);
$img_check_list = $QAppCheckshop->getListImg($checkshop['updated_at'], $checkshop['store_id'], $checkshop['id']);

$params = [
    'checkshop_id' => $checkshop_id,
    'store_id' => $checkshop['store_id'],
    'is_staff_realme' => $userStorage->is_realme
];

$eligible = $QAppCheckshop->countEligible(date('m', strtotime($checkshop['updated_at'])), date('Y', strtotime($checkshop['updated_at'])), $checkshop['store_id']);

$store_info = $QAppCheckshop->getStoreInfo($checkshop['store_id']);

$sellout = $QAppCheckshop->getSellout($checkshop['store_id']);

$level = $QAppCheckshop->getLevelStore($checkshop['store_id']);

if ($check_posm_stage_id) {
    $check_posm_stage = $QCheckPosmStage->fetchRow(['id = ?' => $check_posm_stage_id]);
    $list_category_check_posm = $QCheckPosmStage->getCategory($params['store_id']);
}

if ($change_picture_stage_id) {
    $change_picture_stage = $QChangePictureStage->fetchRow(['id = ?' => $change_picture_stage_id]);
    $list_category_change_picture = $QChangePictureStage->getDetailQuantity($change_picture_stage_id, $params['store_id']);
}


$this->view->store_info = $store_info;
$this->view->level = $level;
$this->view->sellout = $sellout;
$this->view->img_different = $img_different;
$this->view->img_different_list = $img_different_list;
$this->view->img_check = $img_check;
$this->view->img_check_list = $img_check_list;
$this->view->checkshop = $checkshop;
$this->view->checkshop_details = $checkshop_details;
$this->view->checkshop_qc = $QAppCheckshopQc->get_cache();
$this->view->checkshop_qc_realme = $QAppCheckshopQcRealme->get_cache();
$this->view->eligible = $eligible;
$this->view->title = $title;
$this->view->check_posm_stage = $check_posm_stage;
$this->view->list_category_check_posm = $list_category_check_posm;
$this->view->change_picture_stage = $change_picture_stage;
$this->view->list_category_change_picture = $list_category_change_picture;
$this->view->shop_size_type = $QShopSizeType->fetchAll();
$this->view->shop_size_old = $QShopSizeType->getShopSizeOld($checkshop['store_id']);

$danhgia_history = $QAppCheckshopMapQc->getHistoryQc($params);
$history = [];
foreach ($danhgia_history as $key => $value) {
    $history[$value['created_at']][] = $value;
}

$qc_history = $QAppCheckshopMapQc->getHistoryQc($params);

$qc_history_realme = $QAppCheckshopMapQcRealme->getHistoryQc($params);// Author : Trang


$this->view->qc_history = $qc_history;
$this->view->qc_history_realme = $qc_history_realme;// Author : Trang

$qc_by_month_data = $QAppCheckshop->getQcByMonth($params);// Author : Trang ; chuyen qua model $QAppCheckShop
$qc_by_month = [];

foreach ($qc_by_month_data as $key => $value) {
    $qc_by_month[$value['month_year']] = $value['eligible'];
}

$qc = [];
foreach ($qc_history as $key => $value) {
    $qc[$value['month_year']][] = $value;
}
//Author : Trang
$qc_realme = [];
foreach ($qc_history_realme as $key => $value) {
    $qc_realme[$value['month_year']][] = $value;
}
//End Author : Trang

$month_qc = [];
$month_qc_realme = [];
$month_range = $this->month_range('2019-10-01', date('Y-m-d'));

foreach ($month_range as $key => $value) {
    $month_year = date('n/Y', strtotime($value));
    // lỗi hệ thống nên tháng 7/2019 chỉ cần 3 lần checkshop hợp lệ sẽ dc đánh giá qc
    if ($month_year == '7/2019') {
        $rule_number_eligible = 3;
    } else {
        $rule_number_eligible = 2;
    }

//    if ($qc_by_month[$month_year] >= $rule_number_eligible OR $month_year == '10/2018') {
    if ($qc_by_month[$month_year] >= $rule_number_eligible ) {
        $month_qc[$month_year] = $qc[$month_year];
        if($month_year >= date('n/y',strtotime('2020-05-01')))// Author : Trang
            $month_qc_realme[$month_year] = $qc_realme[$month_year];// Author : Trang
    } else {
        $month_qc[$month_year][] = ['type_name' => '<span style="color:red">Không đủ điều kiện đánh giá QC</span>', 'title' => '<span style="color:red">Số lần checkshop hợp lệ ' . $qc_by_month[$month_year] . ' < ' . $rule_number_eligible .'</span>'];
        if($month_year >= date('n/y',strtotime('2020-05-01')))// Author : Trang
            $month_qc_realme[$month_year][] = ['type_name' => '<span style="color:red">Không đủ điều kiện đánh giá QC</span>', 'title' => '<span style="color:red">Số lần checkshop hợp lệ ' . $qc_by_month[$month_year] . ' < ' . $rule_number_eligible .'</span>'];
    }
}
// lỗi hệ thống nên tháng 7/2019 chỉ cần 3 lần checkshop hợp lệ sẽ dc đánh giá qc cho phần view
if (date('m-Y', strtotime($checkshop['updated_at'])) == '07-2019') {
    $rule_number_eligible = 3;
} else {
    $rule_number_eligible = 2;
}

$this->view->month_qc = $month_qc;
$this->view->month_qc_realme = $month_qc_realme;// Author : Trang
$this->view->rule_number_eligible = $rule_number_eligible;


$type_qc = [];
foreach ($this->storage['title_list'] as $key => $value) {
    if ($value == TRADE_MARKETING_EXECUTIVE) {
        $type_qc[1] = 'TMK Local đánh giá';
    } else if ($value == TRADE_MARKETING_LEADER) {
        $type_qc[2] = 'TMK Leader đánh giá';
    } else if ($value == TRADE_MARKETING_SUPERVISOR) {
        $type_qc[3] = 'QC đánh giá';
    }
}


$type_qc_realme = [];
if ($title == TRADE_MARKETING_EXECUTIVE) {
    $type_qc_realme[1] = 'TMK Local đánh giá';
} else if (in_array($title, [TRADE_MARKETING_LEADER, TRADE_SUPERVISOR_REALME_TITLE])) {
    $type_qc_realme[1] = 'TMK Local đánh giá';
    $type_qc_realme[2] = 'TMK Supervisor Realme đánh giá';
}



$this->view->type_qc = $type_qc;
$this->view->type_qc_realme = $type_qc_realme;

$where = [];
$where[] = $QAppCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
$where[] = $QAppCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
$lock_store = $QAppCheckshop->fetchRow($where);

$where = [];
$where[] = $QAppCheckshop->getAdapter()->quoteInto('is_leader_lock = ?', 1);
$where[] = $QAppCheckshop->getAdapter()->quoteInto("store_id = ?", $checkshop['store_id']);
$leader_lock_store = $QAppCheckshop->fetchRow($where);

$checkshopDetailBrand = $QCheckshopDetailBrand->getDetail($params['store_id']);

if ($lock_store) {
    $params['company'] = 1; // oppo
    $currentCategories = $QAppCheckshopDetailChild->getDetailParent($lock_store['id'], $params);
    $currentCategoriesDetail = $QAppCheckshopDetailChild->getDetailChild($lock_store['id']);
    $currentCategoriesDetailPainting = $QAppCheckshopDetailChildPainting->getPainting(['checkshop_id' => $lock_store['id']]);
    $listIdCategoryCheckshop = $QCategory->getIdCategoryCheckshop();

    $params['company'] = 2; // oppo
    $currentCategoriesRealme = $QAppCheckshopDetailChild->getDetailParent($lock_store['id'], $params);
    $this->view->currentCategoriesRealme = $currentCategoriesRealme;

}

// check lỗi btn 3.0
$is_store_error_btn = $QCheckErrorBtn->isStoreErrorBtn($params['store_id']);
$current_error_btn = $QCheckErrorBtn->getCurrentErrorName($params['store_id']);
$this->view->current_error_btn = $current_error_btn;
$this->view->is_store_error_btn = $is_store_error_btn;
// check lỗi btn 3.0

$this->view->lock_store = $lock_store;
$this->view->leader_lock_store = $leader_lock_store;

$flashMessenger = $this->_helper->flashMessenger;

$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;
$this->view->checkshopDetailBrand = $checkshopDetailBrand;
$this->view->staff_id = $userStorage->id;

$this->view->currentCategoriesDetailPainting = $currentCategoriesDetailPainting;
$this->view->currentCategories = $currentCategories;
$this->view->currentCategoriesDetail = $currentCategoriesDetail;
$this->view->listIdCategoryCheckshop = $listIdCategoryCheckshop;
$this->view->params = $params;
$this->view->checkshop_details_realme = $checkshop_details_realme;


        

