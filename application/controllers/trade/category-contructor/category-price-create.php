<?php

    $category_id = $this->getRequest()->getParam('category_id');
    $QCategory = new Application_Model_Category();
    $QContractorPrice = new Application_Model_ContractorPrice();
    $flashMessenger         = $this->_helper->flashMessenger;

    $params = [
        'category_id'   => $category_id
    ];

    $category = $QCategory->fetchRow($QCategory->getAdapter()->quoteInto('id = ?', $category_id));
    $contractor_price = $QContractorPrice->getContractorPrice($params);

    foreach($contractor_price as $key=>$value){
        $params['not_contractor'][] = $value['contractor_id'];
    }

    $contractor_code = $QContractorPrice->getContractorCode($params);


    if($this->getRequest()->getMethod()=='POST')
    {
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $contractor = $this->getRequest()->getParam('contractor');
            $contractor_del = $this->getRequest()->getParam('contractor_del');
            $price = $this->getRequest()->getParam('price');

            $current_date = date('Y-m-d H:i:s');

            //Xóa 
            $where = $QContractorPrice->getAdapter()->quoteInto('category_id = ?', $category_id);
            $QContractorPrice->update(['to_date' => $current_date],$where);

            //Điều chỉnh 
            foreach($contractor as $key=>$value){

                if(!empty($value) AND $value != ''){

                    $data_add = [
                        'contractor_id'  => $value,
                        'category_id'  => $category_id,
                        'price'  => $price[$key],
                        'from_date'  => $current_date,
                        'to_date'   => NULL
                    ];

                    $QContractorPrice->insert($data_add);
                }

            }

            $db->commit();

            $flashMessenger->setNamespace('success')->addMessage('Error NOT trainer!');

            $this->_redirect('/trade/category-price-create?category_id='.$category_id);

        } catch (Exception $e) {
            $db->rollback();
            $e->getMessage();

            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->_redirect('/trade/category-price-create?category_id='.$category_id);
        }


    }

    $this->view->category = $category;
    $this->view->contractor_price = $contractor_price;
    $this->view->contractor_code = $contractor_code;

    $messages             = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
    $messages_error       = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

    $this->_helper->viewRenderer->setRender('category-contructor/category-price-create');




