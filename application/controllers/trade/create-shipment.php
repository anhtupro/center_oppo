<?php 
	
	$category_id   = $this->getRequest()->getParam('category_id');
	$contractor_id = $this->getRequest()->getParam('contractor_id');
	$campaign_id   = $this->getRequest()->getParam('campaign_id');
	
	$QCategory 		= new Application_Model_Category();
	$QCampaign 		= new Application_Model_Campaign();
	$QContractorOut = new Application_Model_AppContractorOut();
	$QAppShipment   = new Application_Model_AppShipment();

	$params = [
		'campaign_id' 		=> $campaign_id,
		'category_id' 		=> $category_id,
		'contractor_id'		=> $contractor_id,
		'type'				=> 2
	];

	$last_number = $QAppShipment->getLastNumber() ? $QAppShipment->getLastNumber() : 0;
	$no = 'PXK'.str_pad( ($last_number['id'] + 1), 5, "0", STR_PAD_LEFT);

	$where = $QCampaign->getAdapter()->quoteInto('type = ?', $params['type']);
	$campaign = $QCampaign->fetchAll($where);

	$category 			= $QCategory->getCategoryByCampaign($params);


	$this->view->campaign       = $campaign;
	$this->view->category 		= $category;
	$this->view->params   		= $params;
	$this->view->no             = $no;

	$flashMessenger       = $this->_helper->flashMessenger;
	$messages             = $flashMessenger->setNamespace('success')->getMessages();

	$this->view->messages = $messages;
	$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;
    

