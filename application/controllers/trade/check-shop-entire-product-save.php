<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$check_shop_id = $this->getRequest()->getParam('check_shop_id');
$rank_image_ratio = $this->getRequest()->getParam('rank_image_ratio');
$rank_display_product = $this->getRequest()->getParam('rank_display_product');
$point_display_product = $this->getRequest()->getParam('point_display_product');
$point_image_ratio = $this->getRequest()->getParam('point_image_ratio');
$product_promotion_name = $this->getRequest()->getParam('product_name');
$promotion = $this->getRequest()->getParam('promotion');
$promotion_value = $this->getRequest()->getParam('promotion_value');
$note = $this->getRequest()->getParam('note');

// display image
$product_display_name = $this->getRequest()->getParam('product_display_name');
$category_display_id = $this->getRequest()->getParam('category_display_id');
$position_display = $this->getRequest()->getParam('position_display');
$point_display = $this->getRequest()->getParam('point_display');


$QCheckShopEntireImageRatio = new Application_Model_CheckShopEntireImageRatio();
$QCheckShopEntireDisplayProduct = new Application_Model_CheckShopEntireDisplayProduct();
$QCheckShopEntireDisplayProductDetail = new Application_Model_CheckShopEntireDisplayProductDetail();
$QCheckShopEntirePointTotal = new Application_Model_CheckShopEntirePointTotal();
$QCheckShopEntire = new Application_Model_CheckShopEntire();
$QCheckShopEntirePromotion = new Application_Model_CheckShopEntirePromotion();
$QCheckShopEntirePromotionDetail= new Application_Model_CheckShopEntirePromotionDetail();


$db = Zend_Registry::get('db');
$db->beginTransaction();

$total_point_image_ratio = 0;
$total_point_display_product = 0;

// save image ratio
foreach ($rank_image_ratio as $category_id => $rank_image) {
    $total_point_image_ratio += $point_image_ratio[$category_id];
    $QCheckShopEntireImageRatio->insert([
       'check_shop_id' => $check_shop_id,
       'category_id' => $category_id,
        'rank' => $rank_image ? $rank_image : 0,
        'point' => $point_image_ratio[$category_id] ? $point_image_ratio[$category_id] : 0
    ]);
}

//// save display product -- old version
//foreach ($rank_display_product as $product_id => $rank_product) {
//    $total_point_display_product += $point_display_product[$product_id];
//    $QCheckShopEntireDisplayProduct->insert([
//        'check_shop_id' => $check_shop_id,
//        'product_id' => $product_id,
//        'rank' => $rank_product,
//        'point' => $point_display_product[$product_id]
//    ]);
//}


//save display product
foreach ($product_display_name as $brand_id => $array_product_name) {
    foreach ($array_product_name as $key => $product_name) {
        if ($product_name) {
            $total_point_each_product = 0;
            $display_product_id = $QCheckShopEntireDisplayProduct->insert([
                'check_shop_id' => $check_shop_id,
                'brand_id' => $brand_id ? $brand_id : 0,
                'product_name' => $product_name ? $product_name : ''
            ]);

            foreach ($category_display_id[$brand_id][$key] as $index => $category_id) {
                if ($category_id) {
                    $position =  $position_display[$brand_id][$key][$index];
                    $point =  $point_display[$brand_id][$key][$index] + 1;  // 1: với mỗi hạng mục sẽ +1 điểm
                    $total_point_each_product += $point;

                    $QCheckShopEntireDisplayProductDetail->insert([
                        'display_product_id' => $display_product_id,
                        'category_id' => $category_id ? $category_id : 0,
                        'position' => $position ? $position : 0,
                        'point' => $point ? $point : 0
                    ]);
                }
            }

            // update total point each product
            $display_product_id = $QCheckShopEntireDisplayProduct->update([
                'total_point' => $total_point_each_product
            ], ['id = ?' => $display_product_id]);
        }
    }
}

// save total point each check shop
//$QCheckShopEntirePointTotal->insert([
//   'check_shop_id' => $check_shop_id,
//   'total_point_image_ratio' =>  $total_point_image_ratio,
//   'total_point_display_product' =>  $total_point_display_product,
//    'created_at' => date('Y-m-d H:i:s')
//]);

// save promotion


foreach ($product_promotion_name as $brand_id => $array_name) {
    foreach ($array_name as $key => $name) {
        if ($name) {
            $check_shop_promotion_id = $QCheckShopEntirePromotion->insert([
                'check_shop_id' => $check_shop_id,
                'brand_id' => $brand_id ? $brand_id : 0,
                'product_name' => $name ? $name : ''
            ]);

            foreach ($promotion_value[$brand_id][$key] as $index => $val) {
                $promotion_id =  $promotion[$brand_id][$key][$index];
                $QCheckShopEntirePromotionDetail->insert([
                    'check_shop_promotion_id' => $check_shop_promotion_id,
                    'promotion_value' => $val ? $val : '',
                    'promotion_id' => $promotion_id ? $promotion_id : Null
                ]);
            }
        }


    }

}


$QCheckShopEntire->update([
    'status' => 1,
    'note' => $note ? $note : ''
],['id = ?' => $check_shop_id]);

$db->commit();

echo json_encode([
   'status' => 0
]);