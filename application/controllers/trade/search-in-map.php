<?php
$dev = $this->getRequest()->getParam('dev');
$categoryId = $this->getRequest()->getParam('category_id', 43);
$areaId =  $this->getRequest()->getParam('area_id');
$regional_market = $this->getRequest()->getParam('regional_market');
$district	 = $this->getRequest()->getParam('district');
$staff_id	 = $this->getRequest()->getParam('staff_id');
$is_ka           = $this->getRequest()->getParam('is_ka');
$is_ka_details   = $this->getRequest()->getParam('is_ka_details');
$store_name   = $this->getRequest()->getParam('store_name');
$brandId = $this->getRequest()->getParam('brand_id', [1,2,3,4]);

$QStore = new Application_Model_Store();
$QStoreOutside = new Application_Model_StoreOutside();
$QCategory = new Application_Model_Category();
$QArea = new Application_Model_Area();
$QAppCheckShop = new Application_Model_AppCheckshop();
$QBrand = new Application_Model_Brand();

$params = [
  'category_id' => $categoryId,
    'area_list' => $this->storage['area_id'],
    'area_id' => $areaId,
    'regional_market'  => $regional_market,
    'district'         => $district,
    'staff_id'     => $staff_id,
    'is_ka'        => $is_ka,
    'is_ka_details' => $is_ka_details,
    'store_name' => $store_name,
    'brand_id' => $brandId
];

$listStoreSystem = $QStoreOutside->getListStoreMap($params);
$listStoreOutside = $QStoreOutside->getListStoreOutsideMap($params);
if ($dev == 1) {
    echo '<pre>';
    print_r(123);
    die;

}

$listCategory = $QCategory->getCategoryCheckshop($params);

$listArea = $QArea->getListArea($params);
$list_sale  = $QAppCheckShop->getSaleArea();
$list_channel = $QAppCheckShop->getChannel();

$listStore = array_merge($listStoreSystem, $listStoreOutside);


$listBrand = $QBrand->fetchAll()->toArray();
//foreach ($listStore as $store) {
//    $list [] = [
//        'lat' => (float) $store['lat'],
//        'lng' => (float) $store['lng']
//    ];
//}
//echo '<pre>';
//print_r(json_encode($list);
//die;


if ($areaId) {
    $QRegionalMarket = new Application_Model_RegionalMarket();
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $areaId);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regional_market) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$this->view->listStore = json_encode($listStore);
$this->view->counStore = count($listStore);
$this->view->listCategory = $listCategory;
$this->view->params = $params;
$this->view->listArea = $listArea;
$this->view->list_sale = $list_sale;
$this->view->list_channel = $list_channel;
$this->view->listBrand = $listBrand;



