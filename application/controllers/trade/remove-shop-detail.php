<?php

$id       = $this->getRequest()->getParam('id');

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QStore            = new Application_Model_Store();
$QStaff            = new Application_Model_Staff();
$QRemoveShop       = new Application_Model_RemoveShop();
$QRemoveShopReason = new Application_Model_RemoveShopReason();
$QRemoveShopFile = new Application_Model_RemoveShopFile();
$QAppStatus = new Application_Model_AppStatus();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QAppCheckshop = new Application_Model_AppCheckshop();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$title = $userStorage->title;

if(in_array($userStorage->id, [557, 15038])){
    $title = TRADE_MARKETING_SUPERVISOR;
}

$app_status = $QAppStatus->get_cache(11);
$app_status_title_cache = $QAppStatusTitle->get_cache(11);
$app_status_title = [];
foreach($this->storage['title_list'] as $key=>$value){
    $app_status_title = array_merge( (array)$app_status_title, (array)$app_status_title_cache[$value]);   
}



if (!empty($id)) {
    $remove_shop                = $QRemoveShop->getRemoveShop($id);
    $this->view->remove_shop    = $remove_shop;
    
    
    $where                      = $QRemoveShopFile->getAdapter()->quoteInto('remove_shop_id = ?', $id);
    $remove_shop_file             = $QRemoveShopFile->fetchAll($where);
    $this->view->remove_shop_file = $remove_shop_file;
    
    //$list_store  = $QStore->getShopAccessByStaff();
    $where_store = $QStore->getAdapter()->quoteInto('id = ?', $removeShop_row['store_id']);
    $list_store  = $QStore->fetchAll($where_store);
    
    $store_info = $QAppCheckshop->getStoreInfo($remove_shop['store_id']);
    
    $staff_cache = $QStaff->get_cache();
    $this->view->staff_cache = $staff_cache;
    $this->view->stores        = $list_store;
    $reason_cache             = $QRemoveShopReason->get_cache();
    $this->view->reason_active = $reason_cache;
    $this->view->store_info = $store_info;

}
if ($_GET['dev'] == 1) {
    echo "<pre>";
    print_r($app_status_title);
    echo "</pre>";
    die();
}
$this->view->app_status       = $app_status;
$this->view->app_status_title = $app_status_title;
$this->view->params           = $params;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
