<?php

$store_id = $this->getRequest()->getParam('store_id');
$category_id = $this->getRequest()->getParam('category_id');
$repair_details_type = $this->getRequest()->getParam('repair_details_type');
$repair_details_tp = $this->getRequest()->getParam('repair_details_tp');
$quantity = $this->getRequest()->getParam('quantity');
$imei = $this->getRequest()->getParam('imei');
$note = $this->getRequest()->getParam('note');
$submit = $this->getRequest()->getParam('submit');
$image = $this->getRequest()->getParam('image');
$app_checkshop_detail_child_id = $this->getRequest()->getParam('app_checkshop_detail_child_id');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QCategory = new Application_Model_Category();
$QStore = new Application_Model_Store();
$QDestruction = new Application_Model_Destruction();
$QRepair = new Application_Model_Repair();
$QRepairDetails = new Application_Model_RepairDetails();
$QRepairDetailsType = new Application_Model_RepairDetailsType();
$QRepairDetailsTp = new Application_Model_RepairDetailsTp();
$QRepairDetailsFile = new Application_Model_RepairDetailsFile();
$QRepairType = new Application_Model_RepairType();
$QAppFile = new Application_Model_AppFile();
$QCampaign = new Application_Model_Campaign();
$QAppStatusTitle = new Application_Model_AppStatusTitle();
$QArea = new Application_Model_Area();
$QAppNoti = new Application_Model_AppNoti();

$flashMessenger       = $this->_helper->flashMessenger;

$data_repair_type = $QRepairType->fetchAll();
$repair_type = [];
foreach($data_repair_type as $key=>$value){
    $repair_type[$value['category_id']][] = [ 'id' => $value['id'], 'title' => $value['title'] ];
}

$params = array_filter(array(
    'store_id' => $store_id,
    'staff_id'  => $userStorage->id,
    'area_list' => $this->storage['area_id']
));

$category_brandshop = $QCategory->fetchRow(['id = ?' => 1006])->toArray();


if (!empty($submit)) {
    $this->_helper->layout()->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        $repair = [
            'store_id'  => $store_id,
            'status' => 1,
            'create_at' => date('Y-m-d H:i:s'),
            'staff_id'  => $userStorage->id,
            'create_type' => 2
        ];


        $repair_id = $QRepair->insert($repair);

        foreach($category_id as $key=>$value){

            $tag = 0;

            $imey = explode(',', $imei[$key]);
            $num = 0;
            for ($i = 0; $i < count($imey); $i++) {
                if ($imey[$i] != '') {
                    $num++;
                }
            }
            $number = !empty($imei[$key]) ? $num : (!empty($quantity[$key]) ? $quantity[$key] : 0);

            $repair_details = [
                'repair_id' => $repair_id,
                'category_id'   => $value,
                'quantity'  => $number,
                'imei_sn' => $imei[$key],
                'note'  => $note[$key],
                'status'    => 1,
                'app_checkshop_detail_child_id' => $app_checkshop_detail_child_id[$key] ? $app_checkshop_detail_child_id[$key] : Null
            ];


            $repair_details_id = $QRepairDetails->insert($repair_details);

            foreach($repair_details_type[$key] as $k=>$v){
                $rdt_insert = [
                    'repair_details_id' => $repair_details_id,
                    'repair_type_id'    => $v,
                    'repair_tp_id'      => $repair_details_tp[$key][$k]
                ];
                $QRepairDetailsType->insert($rdt_insert);
            }

            foreach($image[$key] as $k=>$v){

                $data = $v;

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'repair' . DIRECTORY_SEPARATOR . $repair_id. DIRECTORY_SEPARATOR . $repair_details_id;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

                $newName = md5(uniqid('', true)) . '.png';

                $url = '/photo/repair/' . $repair_id. '/' . $repair_details_id . '/' . $newName;

                $file = $uploaded_dir . '/' . $newName;

                $success_upload = file_put_contents($uploaded_dir . '/' . $newName, $data);


                $url_resize = 'photo' .
                    DIRECTORY_SEPARATOR . 'repair' . DIRECTORY_SEPARATOR . $repair_id . DIRECTORY_SEPARATOR . $repair_details_id . DIRECTORY_SEPARATOR . $newName;

                //Resize Anh
                $image_reisze = new My_Image_Resize();
                $image_reisze->load($url_resize);
                $image_reisze->resizeToWidth(800);
                $image_reisze->save($url_resize);
                // //END Resize


                // upload image to server s3
                require_once "Aws_s3.php";
                $s3_lib = new Aws_s3();
                $destination_s3 = 'photo/repair/' . $repair_id . '/' . $repair_details_id . '/';

                if ($success_upload) {
                    $upload_s3 = $s3_lib->uploadS3($file, $destination_s3, $newName);
                }
                // upload image to server s3

                $rdf_insert = [
                    'repair_details_id' => $repair_details_id,
                    'url'    => $url,
                    'type'   => 1
                ];
                $QRepairDetailsFile->insert($rdf_insert);

                $tag = 1;
            }

            if($tag == 0){
                echo json_encode([
                    'status' => 1,
                    'message' => 'Vui lòng Upload hình ảnh từng hạng mục'
                ]);
                return;
            }

        }


        // notify
        $area_id = $QArea->getAreaByStore($store_id);
        $list_staff_notify = $QAppStatusTitle->getStaffToNotify(4, 1, $area_id);

        foreach ($list_staff_notify as $staff_notify) {
            $data_noti = [
                'title' => "TRADE MARKETING OPPO VIETNAM",
                'message' => "Có một đề xuất SỬA CHỮA đang chờ bạn duyệt",
                'link' => "/trade/repair-edit?id=" . $repair_id,
                'staff_id' => $staff_notify
            ];

//            $QAppNoti->sendNotification($data_noti);
        }

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Success!');

        echo json_encode([
            'status' => 0,
            'url' => 'repair-edit?id=' . $repair_id
        ]);
        return;

    } catch (Exception $e) {

        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());

        echo json_encode([
            'status' => 1,
            'message' => 'Error Sytems: '.$e->getMessage()
        ]);
        return;
    }
}


$list_area = $QArea->getListArea($params);

$this->view->list_area = $list_area;
$this->view->params = $params;

$result = $QCategory->getCategory();
$this->view->category = $result;
$this->view->repair_type = $repair_type;
$this->view->category_brandshop = $category_brandshop;


$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

?>