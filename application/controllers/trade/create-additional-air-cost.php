<?php
$season = $this->getRequest()->getParam('season', 1);
$year = $this->getRequest()->getParam('year', date('Y'));

$QAdditionalAirCost = new Application_Model_AdditionalAirCost();
$QArea = new Application_Model_Area();

$params = [
  'season' => $season,
    'year' => $year
];

$listAdditionalCost = $QAdditionalAirCost->getCost($params);
$listArea = $QArea->getListArea();

$this->view->listAdditionalCost = json_encode($listAdditionalCost);
$this->view->listArea = $listArea;
$this->view->season = $season;
$this->view->year = $year;


