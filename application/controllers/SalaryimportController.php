<?php

class SalaryimportController extends My_Controller_Action{

	public function importAction(){
		$this->_helper->layout->disableLayout();
		// $this->_helper->viewRenderer->setNoRender();

		if($this->_request->isPost()){
			include('PHPExcel/IOFactory.php');
			$reader = PHPExcel_IOFactory::createReader('Excel2007');
			// var_dump($_FILES);die;
			$data = array();
			$excel = $reader->load($_FILES['salary']['tmp_name']);
			foreach ($excel->getWorksheetIterator() as $worksheet) {
				$data[$worksheet->getTitle()] = $worksheet->toArray();
			}
			$data = $data['PB - sale'];
			unset(
				$data[0],
				$data[1],
				$data[2],
				$data[3],
				$data[4]
			);

			$note = 'IT import '.date('Y-m-d');
			$from_date = '2016-07-01';
			$type = 3;

			$db = Zend_Registry::get('db');
			$db->query('
				DROP TABLE IF EXISTS salary_pb_sale_bk;
				CREATE TABLE salary_pb_sale_bk(
					province_id INT NULL,
					province VARCHAR(255) NOT NULL PRIMARY KEY,
					base_salary INT NULL,
					bonus_salary INT NULL,
					allowance_1 INT NULL,
					allowance_2 INT NULL,
					allowance_3 INT NULL,
					probation_salary INT NULL,
					kpi TEXT NULL,
					kpi_1 TEXT NULL,
					work_cost INT NULL,
					type INT NULL,
					from_date DATE NULL,
					note VARCHAR(255) NULL
				) ENGINE=INNODB CHARSET UTF8 COLLATE UTF8_UNICODE_CI;
			');
			$sql = null;
			foreach($data as $key => $value){
				$sql[] = '(
					null, "'.$value[1].'", '.$value[4].',
					'.$value[2].', '.$value[5].',
					'.$value[6].', '.$value[7].',
					'.$value[3].', "'.$value[8].'",
					"'.$value[9].'", '.$value[10].',
					'.$type.',
					"'.$from_date.'", "'.$note.'"
				)';
			}
			$db->query('INSERT INTO salary_pb_sale_bk VALUES'.implode(',', $sql));
			$db->query('
				UPDATE salary_pb_sale_bk sp JOIN regional_market r ON sp.province = r.`name` SET sp.province_id = r.id;
				ALTER TABLE salary_pb_sale_bk DROP province;
			');
			$db->query('
				INSERT INTO salary_log(
					province_id,
					base_salary,
					bonus_salary,
					allowance_1,
					allowance_2,
					allowance_3,
					probation_salary,
					kpi,
					kpi_1,
					work_cost,
					type,
					from_date,
					note
				) SELECT * FROM salary_pb_sale_bk;
			');
			$db = null;
			exit('done');
		}
	}

}