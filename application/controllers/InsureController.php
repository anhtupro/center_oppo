<?php

class InsureController extends My_Controller_Action{
    
    public function getStaffAction(){
        $staff_id = $this->getRequest()->getParam('staff_id');
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('a'=>'staff'),array('id','department','team','title','b.insurance_salary','a.regional_market'))
                ->joinLeft(array('b'=>'v_salary_now'),'a.id = b.staff_id',array())
                ->joinLeft(array('e'=>'unit_code'),'(e.company_id = a.company_id AND ((a.`id_place_province` = 64 AND e.`is_foreigner` = 1) OR (a.`id_place_province` <> 64 AND e.`is_foreigner` = 0)))',array('unit_code_id'  => 'e.id',))
		
                ->where('a.id = ?',$staff_id)
        ;
        $staff = $db->fetchRow($select);
        $data = array();
        if($staff){
            $data = $staff;
            $status = 1;
        }else{
            $status = 0;
        }
        $this->_helper->json->sendJson(array('status'=>$status,'staff'=>$data));
    }
    
   //vandiem.nguyen 
    public function insuranceListAction(){
       require_once 'insure'.DIRECTORY_SEPARATOR.'insurance-list.php';
    }
    
    public function editInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'edit-insurance.php';
    }
    
    public function passbyPayableAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'passby-payable.php';
    }
	
	public function saveEditInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'save-edit-insurance.php';
    }
    
	public function myInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'my-insurance.php';
    }
    public function reportListAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'report-list.php';
    }
    
    public function changeInsuranceApproveDateAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-insurance-approve-date.php';
    }
    
    public function changeInsurancePassByAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-insurance-pass-by.php';
    }
    
    public function changeInsuranceMoneyApprovedAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-insurance-money-approved.php';
    }
    
    public function processInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'process-insurance.php';        
    }
    
    public function disableInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'disable-insurance.php';
    }
    
    public function removeInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'remove-insurance.php';
    }
    
    public function qdnvInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'qdnv-insurance.php';
    }
    
    public function reportTangInsuranceAction($inCreate){
        require_once 'insure'.DIRECTORY_SEPARATOR.'report-tang-insurance.php';
    }
    
    public function reportDieuChinhInsuranceAction($adjust){
        require_once 'insure'.DIRECTORY_SEPARATOR.'report-dieu-chinh-insurance.php';
    }
    
    public function reportBatthuongAction($adjust){
        require_once 'insure'.DIRECTORY_SEPARATOR.'report-batthuong.php';
    }
    
    public function reportGiamInsuranceAction($deCreate){
        require_once 'insure'.DIRECTORY_SEPARATOR.'report-giam-insurance.php';
    }
    
    public function createBasicAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'create-basic.php';
    }
    
    public function changeTangTimeAlterAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-tang-time-alter.php';
    }
    
    public function changeGiamTimeAlterAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-giam-time-alter.php';
    }
    
    public function changeDieuChinhTimeAlterAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-dieu-chinh-time-alter.php';
    }
    
    public function changeSauchedoToDateAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-sauchedo-to-date.php';
    }
    public function addNumberAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'add-number.php';
    }
    
    public function changeTotalTimeAlterAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-total-time-alter.php';
    }
    
    public function changeTotalTimeCreateAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-total-time-create.php';
    }
    
    public function changeChedoFromDateAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-chedo-from-date.php';
    }
    
    public function changeSalaryTotalAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'change-salary-total.php';
    }
    
    public function saveInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'save-insurance.php';
    }
	
	public function checkInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'check-insurance.php';
    }
    
    public function reportTangInsuranceInfoAction($inCreateInfo){
        require_once 'insure'.DIRECTORY_SEPARATOR.'report-tang-insurance-info.php';
    }
    
    public function reportGiamInsuranceInfoAction($deCreateInfo){
        require_once 'insure'.DIRECTORY_SEPARATOR.'report-giam-insurance-info.php';
    }
    
    public function createInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'create-insurance.php';
    }
    
    public function splitInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'split-insurance.php';
    }
    
    public function saveSplitInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'save-split-insurance.php';
        
    }
    
    public function viewInsuranceBasicAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'view-insurance-basic.php';
    }
    
    public function trichNopAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'trich-nop.php';
    }
    
    public function recycleBinListAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'recycle-bin-list.php';
    }
	
	public function lockByMonthAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'lock-by-month.php';
    }
	
    public function exportByMonthAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'export-by-month.php';
    }
    
     public function exportCongdoanAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'export-congdoan.php';
    }
    
    public function exportByMonthNewAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'export-by-month-new.php';
    }
    
    public function listHospitalAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'list-hospital.php';
    }
    
    public function createHospitalAction(){
       require_once 'insure'.DIRECTORY_SEPARATOR.'create-hospital.php';
    }
    
    public function massuploadHospitalHideUnhideAction(){
       require_once 'insure'.DIRECTORY_SEPARATOR.'massupload-hospital-hide-unhide.php';
    }
    public function saveUploadHospitalHideAction(){
       require_once 'insure'.DIRECTORY_SEPARATOR.'save-upload-hospital-hide.php';
    }
    public function uploadStaffUnionFundingAction(){
       require_once 'insure'.DIRECTORY_SEPARATOR.'upload-staff-union-funding.php';
    }
    public function saveUploadUnionFundingAction(){
       require_once 'insure'.DIRECTORY_SEPARATOR.'save-upload-union-funding.php';
    }
    
	public function checkContractAction(){
       require_once 'insure'.DIRECTORY_SEPARATOR.'check-contract.php';
    }
	
    public function delHospitalAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $hospital_id            = $this->getRequest()->getParam('hospital_id');
        
        $QHospital  = new Application_Model_Hospital();
        $where = $QHospital->getAdapter()->quoteInto('id = ?',$hospital_id);
        $data_udpate = array('del' => 1);
        
        $QHospital->update($data_udpate, $where);
        
        echo json_encode($hospital_id) ;
    }
    
    public function massUploadInsuranceAction(){
    }
	
	public function adminUploadAction(){
		 require_once 'insure'.DIRECTORY_SEPARATOR.'admin-upload.php';
    }
    
    public function saveMassUploadInsuranceAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'save-mass-upload-insurance.php';
    }
	
	public function saveAdminUploadAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'save-admin-upload.php';
    }
	
	public function logAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'log.php';
    }

    public function advancePaymentListAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $staff_code = $this->getRequest()->getParam('staff_code');
        $stage = $this->getRequest()->getParam('stage');
        $stage_date = $this->getRequest()->getParam('stage_date');
        $company_id = $this->getRequest()->getParam('company_id');
        $export = $this->getRequest()->getParam('export');
        $limit = 10;

        $params = [
            'staff_code' => $staff_code,
            'stage' => $stage,
            'stage_date' => $stage_date,
            'company_id' => $company_id
        ];

        $QCompany = new Application_Model_Company();
        $QInsuranceAdvancePayment = new Application_Model_InsuranceAdvancePayment();

        $companies = $QCompany->fetchAll()->toArray();
        $listStage = $QInsuranceAdvancePayment->fetchPagination($page, $limit, $total, $params);

        if ($export === 'Export') {
            include_once 'PHPExcel.php';
            $list = $QInsuranceAdvancePayment->getAll($params);

            $objExcel = new PHPExcel();
            $objExcel->setActiveSheetIndex(0);
            $sheet = $objExcel->getActiveSheet();
            $rowCount = 2;
            $index = 1;

            $sheet->setCellValue('A1', 'Stt');
            $sheet->setCellValue('B1', 'Đợt');
            $sheet->setCellValue('C1', 'Tháng');
            $sheet->setCellValue('D1', 'Công ty');
            $sheet->setCellValue('E1', 'Họ và tên');
            $sheet->setCellValue('F1', 'Mã nhân viên');
            $sheet->setCellValue('G1', 'Số sổ BHXH');
            $sheet->setCellValue('H1', 'Chế độ');
            $sheet->setCellValue('I1', 'Tình trạng');
            $sheet->setCellValue('J1', 'Ngày sinh của con');
            $sheet->setCellValue('K1', 'Từ ngày');
            $sheet->setCellValue('L1', 'Đến ngày');
            $sheet->setCellValue('M1', 'Tổng ngày');
            $sheet->setCellValue('N1', 'Số tiền');
            $sheet->setCellValue('O1', 'Ngày nộp hồ sơ');
            $sheet->setCellValue('P1', 'Ngày bảo hiểm trả hồ sơ');
            $sheet->setCellValue('Q1', 'Ngày bảo hiểm chuyển tiền');
            $sheet->setCellValue('R1', 'Ngày chi tiền');
            $sheet->setCellValue('S1', 'Ghi chú');

            foreach ($list as $element) {
                $sheet->setCellValue('A' . $rowCount, $index);
                $sheet->setCellValue('B' . $rowCount, $element['stage']);
                $sheet->setCellValue('C' . $rowCount, $element['stage_date']);
                $sheet->setCellValue('D' . $rowCount, $element['company']);
                $sheet->setCellValue('E' . $rowCount, $element['staff_name']);
                $sheet->setCellValue('F' . $rowCount, $element['staff_code']);
                $sheet->setCellValue('G' . $rowCount, $element['book_number']);
                $sheet->setCellValue('H' . $rowCount, ($element['insurance_type'] == 0) ? $element['insurance_type_different'] : $element['insurance_type_name']);
                $sheet->setCellValue('I' . $rowCount, $element['status']);
                $sheet->setCellValue('J' . $rowCount, date('d-m-Y', strtotime($element['birth_date'])));
                $sheet->setCellValue('K' . $rowCount, date('d-m-Y', strtotime($element['from_date'])));
                $sheet->setCellValue('L' . $rowCount, date('d-m-Y', strtotime($element['to_date'])));
                $sheet->setCellValue('M' . $rowCount, $element['total_days']);
                $sheet->setCellValue('N' . $rowCount, $element['cash']);
                $sheet->setCellValue('O' . $rowCount, date('d-m-Y', strtotime($element['submission_date'])));
                $sheet->setCellValue('P' . $rowCount, date('d-m-Y', strtotime($element['insurance_return_date'])));
                $sheet->setCellValue('Q' . $rowCount, date('d-m-Y', strtotime($element['insurance_transfer_cash_date'])));
                $sheet->setCellValue('R' . $rowCount, date('d-m-Y', strtotime($element['payment_date'])));
                $sheet->setCellValue('S' . $rowCount, $element['note']);

                ++$rowCount;
                ++$index;
            }

            //style sheet
            $style_border = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $sheet->getStyle('A0:S' . $rowCount)->applyFromArray($style_border);

            for ($i = 'A'; $i < 'T'; ++$i) {
                $sheet->getStyle($i . 1)->getFont()->setBold(true);
                $sheet->getColumnDimension($i)->setAutoSize(true);
            }

            $filename = 'Trả trước bảo hiểm đợt ' . $stage . ' tháng ' . $stage_date;
            $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }

        $this->view->params = $params;
        $this->view->companies = $companies;
        $this->view->listStage = $listStage;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->offset = $limit * ($page - 1);
        $this->view->url = HOST . 'insure/advance-payment-list' . ($params ? '?' . http_build_query($params) . '&' : '?');

    }

    public function advancePaymentCreateAction()
    {
        $stage = $this->getRequest()->getParam('stage');
        $stage_date = $this->getRequest()->getParam('stage_date');
        $company_id = $this->getRequest()->getParam('company_id');

        $QInsuranceAdvancePayment = new Application_Model_InsuranceAdvancePayment();
        $QCompany = new Application_Model_Company();
        $QInsuranceAdvancePaymentType = new Application_Model_InsuranceAdvancePaymentType();
        $companies = $QCompany->fetchAll()->toArray();
        $insuranceTypes = $QInsuranceAdvancePaymentType->fetchAll()->toArray();

        if ($stage && $stage_date && $company_id) {
            $QInsuranceAdvancePayment = new Application_Model_InsuranceAdvancePayment();
            $QInsuranceAdvancePaymentFile = new Application_Model_InsuranceAdvancePaymentFile();
            $where_file = [];
            $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage = ?', $stage);
            $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage_date = ?', $stage_date);
            $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('company_id = ?', $company_id);
            $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('is_deleted = ?', 0);
            $photos = $QInsuranceAdvancePaymentFile->fetchAll($where_file)->toArray();


            $where = [];
            $where [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage = ?', $stage);
            $where [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage_date = ?', $stage_date);
            $where [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('company_id = ?', $company_id);
            $listAdvancePayment = $QInsuranceAdvancePayment->fetchAll($where)->toArray();
            $company_id = $listAdvancePayment[0]['company_id'];
            $stage_date = $listAdvancePayment[0]['stage_date'];


            $this->view->company_id = $company_id;
            $this->view->stage_date = $stage_date;
            $this->view->stage = $stage;
            $this->view->photos = json_encode($photos);
            $this->view->listAdvancePayment = $listAdvancePayment;
            $this->view->mode = 'update';
        }

        $this->view->companies = $companies;
        $this->view->insuranceTypes = $insuranceTypes;

    }

    public function advancePaymentSaveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $staff_code = $this->getRequest()->getParam('staff_code');
        $staff_name = $this->getRequest()->getParam('staff_name');
        $book_number = $this->getRequest()->getParam('book_number');
        $insurance_type = $this->getRequest()->getParam('insurance_type');
        $insurance_type_different = $this->getRequest()->getParam('insurance_type_different');
        $status = $this->getRequest()->getParam('status');
        $from_date = $this->getRequest()->getParam('from_date');
        $to_date = $this->getRequest()->getParam('to_date');
        $total_days = $this->getRequest()->getParam('total_days');
        $cash = $this->getRequest()->getParam('cash');
        $submission_date = $this->getRequest()->getParam('submission_date');
        $insurance_return_date = $this->getRequest()->getParam('insurance_return_date');
        $insurance_transfer_cash_date = $this->getRequest()->getParam('insurance_transfer_cash_date');
        $payment_date = $this->getRequest()->getParam('payment_date');
        $note = $this->getRequest()->getParam('note');
        $company_id = $this->getRequest()->getParam('company_id');
        $stage = $this->getRequest()->getParam('stage');
        $stage_date = str_replace('/', '-', trim($this->getRequest()->getParam('stage_date')));
        $mode = $this->getRequest()->getParam('mode');
        $birth_date = $this->getRequest()->getParam('birth_date');

        // check if stage and company is existed
        $QInsuranceAdvancePayment = new Application_Model_InsuranceAdvancePayment();
        $existStage = $QInsuranceAdvancePayment->isExistStage($stage, $stage_date, $company_id);
        if ($existStage && $mode !== 'update') {
            echo json_encode([
                'status' => 1,
                'message' => 'Đợt ' . $stage . ' tháng ' . $stage_date . ' công ty này đã tồn tại vui lòng tạo đợt khác'
            ]);
            return;
        }
//          check if staffcode is valid
        foreach ($staff_code as $code) {
            $QStaff = new Application_Model_Staff();
            $staff = $QStaff->fetchRow($QStaff->getAdapter()->quoteInto('code = ?', $code));
            if (!$staff) {
                $invalidCodes [] = $code;
            }
        }
        if ($invalidCodes) {
            echo json_encode([
                'status' => 1,
                'message' => 'Mã nhân viên không tồn tại : ' . implode(', ', $invalidCodes)
            ]);
            return;
        }

        //save photo
        $upload = new Zend_File_Transfer();
        $images = $upload->getFileInfo();

        //test

        $upload->addValidator('Extension', false, 'jpg,jpeg,png,pdf');
        $upload->addValidator('Size', false, array('max' => '5MB'));
        $upload->addValidator('ExcludeExtension', false, 'php,sh');
        $upload->addValidator('Count', false, 5);

        $upload_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'insurance' . DIRECTORY_SEPARATOR . 'advance_payment' . DIRECTORY_SEPARATOR . $stage . '-' . $stage_date . DIRECTORY_SEPARATOR;

        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0755, true);
        }

//        if ($images['files_0_']['name']) { // if there is file is uploaded
//            // delete all old photo
//            $files = glob($upload_dir . '*');
//            foreach ($files as $file) {
//                if (is_file($file))
//                    unlink($file);
//            }
//        }

        foreach ($images as $image_type => $image_infor) {
            if (!$image_infor['name']) { // if there is no file is uploaded
                continue;
            }
            $extension = pathinfo($image_infor['name'], PATHINFO_EXTENSION);
            $new_name = $upload_dir . DIRECTORY_SEPARATOR . md5(uniqid('', true)) . '.' . $extension;
            $upload->addFilter('Rename', array('target' => $new_name)); // Set a new destination path

            // check file is valid or not
            if (!$upload->isValid($image_type)) {
                $array_error = $upload->getMessages();

                if (isset($array_error['fileSizeTooBig'])) {
                    echo json_encode([
                        'status' => 1,
                        'message' => 'File có dung lượng vượt quá 5MB'
                    ]);
                    return;
                }
                if (isset($array_error['fileExtensionFalse'])) {
                    echo json_encode([
                        'status' => 1,
                        'message' => 'File không đúng định dạng'
                    ]);
                    return;
                }
                if ($array_error['fileCountTooMany']) {
                    echo json_encode([
                        'status' => 1,
                        'message' => 'Bạn chỉ được upload tối đa 5 file'
                    ]);
                    return;
                }
            }

            $result = $upload->receive($image_type); // save image
            $list_photo [] = basename($new_name); // save to database
        }

        if ($list_photo) {
//
            $QInsuranceAdvancePaymentFile = new Application_Model_InsuranceAdvancePaymentFile();
//            $where = [];
//            $where [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage = ?', $stage);
//            $where [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage_date = ?', $stage_date);
//            $QInsuranceAdvancePaymentFile->delete($where);
            foreach ($list_photo as $photo) {
                $QInsuranceAdvancePaymentFile->insert([
                    'stage' => $stage,
                    'stage_date' => $stage_date,
                    'company_id' => $company_id,
                    'file' => $photo,
                    'created_by' => $auth->id,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        // end save photo

        $QInsuranceAdvancePayment = new Application_Model_InsuranceAdvancePayment();
        $where = [];
        $where [] = $QInsuranceAdvancePayment->getAdapter()->quoteInto('stage = ?', $stage);
        $where [] = $QInsuranceAdvancePayment->getAdapter()->quoteInto('stage_date = ?', $stage_date);
        $where [] = $QInsuranceAdvancePayment->getAdapter()->quoteInto('company_id = ?', $company_id);

        $QInsuranceAdvancePayment->delete($where);

        for ($i = 0; $i < count($staff_code); ++$i) {
            $QInsuranceAdvancePayment->insert([
                'staff_code' => $staff_code[$i],
                'staff_name' => $staff_name[$i],
                'book_number' => $book_number[$i],
                'insurance_type' => $insurance_type[$i],
                'insurance_type_different' => $insurance_type_different[$i] ? $insurance_type_different[$i] : NULL,
                'status' => $status[$i],
                'birth_date' => $birth_date[$i] ? date('Y-m-d', strtotime($birth_date[$i])) : Null,
                'from_date' => date('Y-m-d', strtotime($from_date[$i])),
                'to_date' => date('Y-m-d', strtotime($to_date[$i])),
                'total_days' => intval($total_days[$i]),
                'cash' => $cash[$i],
                'submission_date' => date('Y-m-d', strtotime($submission_date[$i])),
                'insurance_return_date' => date('Y-m-d', strtotime($insurance_return_date[$i])),
                'insurance_transfer_cash_date' => date('Y-m-d', strtotime($insurance_transfer_cash_date[$i])),
                'payment_date' => date('Y-m-d', strtotime($payment_date[$i])),
                'note' => $note[$i],
                'company_id' => $company_id,
                'stage' => $stage,
                'stage_date' => trim($stage_date),
                'created_by' => $auth->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

        echo json_encode([
            'status' => 0
        ]);
    }

    public function advancePaymentSaveMassUploadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        include 'PHPExcel/IOFactory.php';
        // config for excel template
        define('START_ROW', 2);
        define('STAFF_CODE', 1);
        define('STAFF_NAME', 2);
        define('BOOK_NUMBER', 3);
        define('INSURANCE_TYPE', 4);
        define('STATUS', 5);
        define('BIRTH_DATE', 6);
        define('FROM_DATE', 7);
        define('TO_DATE', 8);
        define('TOTAL_DAYS', 9);
        define('CASH', 10);
        define('SUBMISSION_DATE', 11);
        define('INSURANCE_RETURN_DATE', 12);
        define('INSURANCE_TRANSFER_CASH_DATE', 13);
        define('PAYMENT_DATE', 14);
        define('NOTE', 15);
        define('INSURANCE_TYPE_DIFFERENT', 16);

        //
        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $company_id = $this->getRequest()->getParam('company_id');
        $stage = $this->getRequest()->getParam('stage');
        $stage_date = str_replace('/', '-', trim($this->getRequest()->getParam('stage_date')));

        $QInsuranceAdvancePayment = new Application_Model_InsuranceAdvancePayment();
        $existStage = $QInsuranceAdvancePayment->isExistStage($stage, $stage_date, $company_id);

        if ($existStage) {
            echo json_encode([
                'status' => 1,
                'message' => 'Đợt ' . $stage . ' tháng ' . $stage_date . ' công ty này đã tồn tại vui lòng tạo đợt khác'
            ]);
            return;
        }

        $save_folder = 'massupload_advance_payment_insurance';
        $requirement = array(
            'Size' => array('min' => 5, 'max' => 5000000),
            'Count' => array('min' => 1, 'max' => 1),
            'Extension' => array('xls', 'xlsx')
        );
        // upload and save file

        try {
            $file = My_File::get($save_folder, $requirement, true);
            if (!$file) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Upload failed'
                ]);
                return;
            }
            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                . DIRECTORY_SEPARATOR . $file['folder'];
            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        } catch (Exception $e) {
            echo json_encode([
                'status' => 1,
                'message' => $e->getMessage()
            ]);
            return;
        }

        //read file
        //  Choose file to read
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            echo json_encode([
                'status' => 1,
                'message' => $e->getMessage()
            ]);
            return;
        }

        // read sheet
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        try {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();

            for ($row = START_ROW; $row <= $highestRow; ++$row) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = $rowData[0];

                if ($rowData[STAFF_CODE]) {
                    $listStaffCode [] = $rowData[STAFF_CODE];
                    $data_upload_staff_code [] = trim($rowData[STAFF_CODE]);
                    $data_mass_upload = [
                        'staff_code' => trim($rowData[STAFF_CODE]),
                        'staff_name' => trim($rowData[STAFF_NAME]),
                        'book_number' => trim($rowData[BOOK_NUMBER]),
                        'insurance_type' => trim($rowData[INSURANCE_TYPE]),
                        'insurance_type_different' => (trim($rowData[INSURANCE_TYPE]) == 0) ? trim($rowData[INSURANCE_TYPE_DIFFERENT]) : NULL,
                        'status' => trim($rowData[STATUS]),
                        'birth_date' => date('Y-m-d', strtotime(str_replace('/', '-', trim($rowData[BIRTH_DATE])))),
                        'from_date' => date('Y-m-d', strtotime(str_replace('/', '-', trim($rowData[FROM_DATE])))),
                        'to_date' => date('Y-m-d', strtotime(str_replace('/', '-', trim($rowData[TO_DATE])))),
                        'total_days' => intval(trim($rowData[TOTAL_DAYS])),
                        'cash' => trim($rowData[CASH]),
                        'submission_date' => date('Y-m-d', strtotime(str_replace('/', '-', trim($rowData[SUBMISSION_DATE])))),
                        'insurance_return_date' => date('Y-m-d', strtotime(str_replace('/', '-', trim($rowData[INSURANCE_RETURN_DATE])))),
                        'insurance_transfer_cash_date' => date('Y-m-d', strtotime(str_replace('/', '-', trim($rowData[INSURANCE_TRANSFER_CASH_DATE])))),
                        'payment_date' => date('Y-m-d', strtotime(str_replace('/', '-', trim($rowData[PAYMENT_DATE])))),
                        'note' => trim($rowData[NOTE]),
                        'company_id' => $company_id,
                        'stage' => $stage,
                        'stage_date' => trim($stage_date),
                        'created_by' => $auth->id,
                        'created_at' => date('Y-m-d H:i:s')
                    ];

                    $QInsuranceAdvancePayment->insert($data_mass_upload);
                }
            } // end for

            // check if staffcode is valid
            foreach ($listStaffCode as $code) {
                $QStaff = new Application_Model_Staff();
                $staff = $QStaff->fetchRow($QStaff->getAdapter()->quoteInto('code = ?', $code));
                if (!$staff) {
                    $invalidCodes [] = $code;
                }
            }

            if ($invalidCodes) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Mã nhân viên không tồn tại : ' . implode(', ', $invalidCodes)
                ]);
                return;
            }

            $db->commit();

            echo json_encode([
                'status' => 0
            ]);
        } catch (Exception $e) {
            echo json_encode([
                'status' => 1,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function advancePaymentDownloadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $stage = $this->getRequest()->getParam('stage');
        $stage_date = str_replace('/', '-', trim($this->getRequest()->getParam('stage_date')));
        $company_id = str_replace('/', '-', trim($this->getRequest()->getParam('company_id')));

        $QInsuranceAdvancePaymentFile = new Application_Model_InsuranceAdvancePaymentFile();
        $where_file = [];
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage = ?', $stage);
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage_date = ?', $stage_date);
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('company_id = ?', $company_id);
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('is_deleted = ?', 0);
        $files = $QInsuranceAdvancePaymentFile->fetchAll($where_file, null, null, 'file')->toArray();

        foreach ($files as $file) {
            $arrFile [] = $file['file'];
        }

        $pathDir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'insurance' . DIRECTORY_SEPARATOR . 'advance_payment' . DIRECTORY_SEPARATOR . $stage . '-' . $stage_date;

        $zip_file_temp = tempnam("/tmp", '');
        $zip = new ZipArchive();
        $zip->open($zip_file_temp, ZipArchive::OVERWRITE);
        $dir = opendir($pathDir);

        while ($file = readdir($dir)) {
            if (is_file($pathDir . DIRECTORY_SEPARATOR . $file) && in_array($file, $arrFile)) {
                $zip->addFile($pathDir . DIRECTORY_SEPARATOR . $file, $file);
            }
        }
        $zip->close();

        //download file
        $download_filename = 'Advance payment' . '.zip';
        header("Content-Type: application/zip");
        header("Content-Length: " . filesize($zip_file_temp));
        header("Content-Disposition: attachment; filename=\"" . $download_filename . "\"");

        readfile($zip_file_temp);
        unlink($excel_file_temp);
        unlink($zip_file_temp);
        exit;
    }

    public function advancePaymentRemoveFileAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $file_id = $this->getRequest()->getParam('file_id');
        $stage = $this->getRequest()->getParam('stage');
        $stage_date = $this->getRequest()->getParam('stage_date');
        $company_id = $this->getRequest()->getParam('company_id');

        $QInsuranceAdvancePaymentFile = new Application_Model_InsuranceAdvancePaymentFile();

        $where [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('id = ?', $file_id);
        $QInsuranceAdvancePaymentFile->update(['is_deleted' => 1], $where);

        $where_file = [];
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage = ?', $stage);
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('stage_date = ?', $stage_date);
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('company_id = ?', $company_id);
        $where_file [] = $QInsuranceAdvancePaymentFile->getAdapter()->quoteInto('is_deleted = ?', 0);
        $files = $QInsuranceAdvancePaymentFile->fetchAll($where_file)->toArray();

        echo json_encode([
            'status' => 0,
            'files' => $files
        ]);
        return;

    }

    public function downloadTemplateAdvancePaymentAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $template = $this->getRequest()->getParam('template');

        $file_name = $template . '.xlsx';

        $file = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template'. DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . $file_name;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function saveInsuranceLockAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'create-insurance.php';
        $flashMessenger               = $this->_helper->flashMessenger;
        $userStorage                  = Zend_Auth::getInstance()->getStorage()->read();
        $submit                       = $this->getRequest()->getParam('submit');
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;

        if($submit){
            $staff_id       = $this->getRequest()->getParam('staff_id');
            $lock_type      = $this->getRequest()->getParam('lock_type');
            $unit_code_id   = $this->getRequest()->getParam('unit_code_id');
            $salary         = $this->getRequest()->getParam('salary');
            $old_salary     = $this->getRequest()->getParam('old_salary');
            $from_month     = $this->getRequest()->getParam('from_month');
            $to_month       = $this->getRequest()->getParam('to_month');
            $number_of_month = $this->getRequest()->getParam('number_of_month');
            $amount         = $this->getRequest()->getParam('amount');
            $lock_time      = $this->getRequest()->getParam('lock_time');
            $created_at     = date('Y-m-d H:i:s');
            $created_by     = $userStorage->id;
            $system_note    = $created_by. '_' .$created_at;
            $lock_time      = str_replace('/', '-', $lock_time);
            $lock_time      = '01-'.$lock_time;
            $lock_time      = date("Y-m-d", strtotime($lock_time));
            
            $QUnitCode          = new Application_Model_UnitCode();
            $QLockInsurance = new Application_Model_LockInsurance();
            $QStaffUnionFunding = new Application_Model_StaffUnionFunding();
            $rowUnitCode        = $QUnitCode->find($unit_code_id)->current();
            
            $company_id = $unit_code_id = $rowUnitCode['company_id'];   
            
            $staff_id_arr = array() ; 
            $staff_id_arr[] = $staff_id;
            $staff_luong_congdoan = $QStaffUnionFunding->getStaffUnionFunding($lock_time);
            $params  = array(
                'staff_id'          => $staff_id,
                'lock_type'         => $lock_type,
                'unit_code_id'      => $company_id,
                'salary'      	    => $salary,
                'old_salary'        => $old_salary,
                'from_month'	    => $from_month,
                'to_month'          => $to_month,
                'number_of_month'   => $number_of_month,
                'amount'            => $amount,
                'lock_time'         => $lock_time,
                'created_at'        => $created_at,
                'created_by'        => $created_by,
                'system_note'       => $system_note,
                'salary_congdoan' => in_array($item['staff_id'],$staff_luong_congdoan)? $salary:null
            );
            
            if(!empty($params))
            {
                $result = $QLockInsurance->save($params);

                if($result['code'] <= 0){
                    $flashMessenger->setNamespace('error')->addMessage($result['message']);
//                    $this->view->messages_error = $result['message'];
//                    debug($result['message']); exit;
                }else{
                    $flashMessenger->setNamespace('success')->addMessage('Đã cập nhật');
                }
               $this->_redirect("/insure/save-insurance-lock");

            }
            else
            {
                $flashMessenger->setNamespace('error')->addMessage('Không có dữ liệu');
                $this->_redirect("/insure/save-insurance-lock");
            }
        }
    }

    public function insuranceStaffBasicListAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $QInsuranceAStaffBasic = new Application_Model_InsuranceStaffBasic();
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $inCreate = $QInsuranceAStaffBasic->get_by_option(1);
        $this->view->inCreate = $inCreate;
        $deCreate = $QInsuranceAStaffBasic->get_by_option(2);
        $this->view->deCreate = $deCreate;
        $adjust =  $QInsuranceAStaffBasic->get_by_option(3);
        $this->view->adjust = $adjust;
        $special =   $QLeaveDetail->getByInsurancePassBy();
        $this->view->special = $special;
        $after_leave = $QInsuranceAStaffBasic->get_by_option(5);
        $this->view->after_leave = $after_leave;
        $change = $QInsuranceAStaffBasic->get_by_option(6);
        $this->view->change = $change;
        $active = "#tang";
        if(!empty($inCreate)){
            $active = "#tang";
        }elseif (!empty($deCreate)){
            $active = "#giam";
        }elseif (!empty($adjust)){
            $active = "#dieuchinh";
        }elseif ( !empty($special) OR !empty($after_leave) OR !empty($change) ){
            $active = "#chedo";
        }
        $this->view->active  = $active;
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;

    }
    public function createStaffContractFileAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $QStaffContractFile = new Application_Model_StaffContractFile();
        $QStaffContract = new Application_Model_StaffContract();
        $staff_contract_id = $this->getRequest()->getParam('staff_contract_id');
        $where = $QStaffContract->getAdapter()->quoteInto('id = ?', $staff_contract_id);
        $StaffContract_Row = $QStaffContract->fetchRow($where);
       
        $this->view->staff_contract_id  = $staff_contract_id;
        $this->view->staffContract_Row  = $StaffContract_Row;
        $this->view->staff_contract_file  = $QStaffContractFile->getByStaffContract($staff_contract_id);
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;
    }
    public function saveStaffContractFileAction(){
        require_once 'insure'.DIRECTORY_SEPARATOR.'save-staff-contract-file.php';
    }
    public function deleteStaffContractFileAction(){
        $id = $this->getRequest()->getParam('id');
        $staff_contract_id = $this->getRequest()->getParam('staff_contract_id');
        $flashMessenger = $this->_helper->flashMessenger;
        $QStaffContractFile = new Application_Model_StaffContractFile();
        $url = $QStaffContractFile->geUrlById($id);
        $url_delete=APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR .$url['url'];
        $result = $QStaffContractFile->remove($id);
        if ($result['code'] <= 0) {
            $flashMessenger->setNamespace('error')->addMessage($result['message']);
        } else {
            $flashMessenger->setNamespace('success')->addMessage($result['message']);
         unlink($url_delete);
        }
        $this->_redirect('/insure/create-staff-contract-file?staff_contract_id='.$staff_contract_id);

    }
    public function disableAction(){
        $id = $this->getRequest()->getParam('id');

        $flashMessenger = $this->_helper->flashMessenger;

        $QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
        if(!empty($id))
            $result = $QInsuranceStaffBasic->removeDisable($id);
        else $result['message'] = 'Restore fail';
        if ($result['code'] <= 0) {
            $flashMessenger->setNamespace('error')->addMessage($result['message']);
        } else {
            $flashMessenger->setNamespace('success')->addMessage($result['message']);
        }
        $this->_redirect('/insure/insurance-staff-basic-list');

    }
}