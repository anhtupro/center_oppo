<?php 
class ImeiLostController extends My_Controller_Action
{
	public function indexAction(){

		$imei_sn = $this->getRequest()->getParam('imei_sn');
		$type    = $this->getRequest()->getParam('type');
		$dismiss = $this->getRequest()->getParam('dismiss',0);
		$good_id = $this->getRequest()->getParam('good_id');
		$page    = $this->getRequest()->getParam('page',1);
		$export = $this->getRequest()->getParam('export');
		$limit   = LIMITATION;
		$sort    = '';
		$desc    = 1;
		$total   = 0;
		$params = array(
			'imei_sn' => $imei_sn,
			'type'    => $type,
			'dismiss' => $dismiss,
			'good_id' => $good_id
		);

		$db = Zend_Registry::get('db');
		$QImeiLost = new Application_Model_ImeiLost();
		
		if($export){
			$list = $QImeiLost->fetchPagination($page,NULL,$total,$params);
			$this->_export($list);
		}

		$list = $QImeiLost->fetchPagination($page,$limit,$total,$params);
		$this->view->list = $list;

		$this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->params           = $params;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->url              = HOST.'imei-lost/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset           = $limit*($page-1);
        
        $flashMessenger = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;

        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $this->view->uri = $uri;

        $types = unserialize(IMEI_LOST_ACTION);
        $this->view->types = $types;
        $this->view->dismiss_list = array(0=>'not dismiss',1=>'dismiss',);

        $select_good = $db->select()	
        	->from(array('a'=>WAREHOUSE_DB.'.good'),array('id','name'))
        	->where('a.cat_id = ?',11)
        	->order('name ASC');
        $goods = $db->fetchPairs($select_good);
        $this->view->goods = $goods;
	}

	public function detailAction(){
		$imei_sn        = $this->getRequest()->getParam('imei_sn');
		$back_url       = $this->getRequest()->getParam('back_url','/imei-lost/');
		$flashMessenger = $this->_helper->flashMessenger;
		$QImeiLost      = new Application_Model_ImeiLost();
		$result         = $QImeiLost->detail($imei_sn);
		if(count($result) == 0){
			//$flashMessenger->setNamespace('error')->addMessage('imei not exist in lost list');
		}
		$this->view->result = $result;

		$types = unserialize(IMEI_LOST_ACTION);
        $this->view->types = $types;

        if($this->getRequest()->isXmlHttpRequest()){
        	$this->_helper->layout()->disableLayout(true);
        }
	}

	public function dismissAction(){
		$imei_sn        = $this->getRequest()->getParam('imei_sn');
		$back_url       = $this->getRequest()->getParam('back_url','/imei-lost');
		$QImeiLost      = new Application_Model_ImeiLost();
		$flashMessenger = $this->_helper->flashMessenger;
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$result = $QImeiLost->dismiss($imei_sn);
			if($result['code'] < 0){
				$flashMessenger->setNamespace('error')->addMessage($result['message']);
			}
			$flashMessenger->setNamespace('success')->addMessage('Done');
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
			$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
		}

		$this->_redirect($back_url);
	}

	private function _export($data){
		set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $alpha    = 'A';
        $index    = 1;
        $heads = array(
                'STT',
                'Product',
                'Color',
                'imei',
                'type',
                'Tracking time'
            );
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha++ . $index, $key);
        }

		$index = 2;
		$stt   = 1;
		$types = unserialize(IMEI_LOST_ACTION);
        foreach($data as $row):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit($stt++, PHPExcel_Cell_DataType::TYPE_STRING);
 			$sheet->getCell($alpha++.$index)->setValueExplicit($row['good_name'], PHPExcel_Cell_DataType::TYPE_STRING);
 			$sheet->getCell($alpha++.$index)->setValueExplicit($row['good_color_name'], PHPExcel_Cell_DataType::TYPE_STRING);
 			$sheet->getCell($alpha++.$index)->setValueExplicit($row['imei_sn'], PHPExcel_Cell_DataType::TYPE_STRING);

 			$str_type = '';
 			$group_type = explode(',', $row['type_group']);
 			if(count($group_type)){
	            foreach ($group_type as $key => $value) {
	                $str_type .= $types[$value].', ';
	            }    
	        }

 			$sheet->getCell($alpha++.$index)->setValueExplicit($str_type, PHPExcel_Cell_DataType::TYPE_STRING);
 			$sheet->getCell($alpha++.$index)->setValueExplicit(date('d/m/Y',strtotime($row['lasted'])), PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;

        $filename = 'Imei_lost_'.date('Y_m_d');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
	}

	
}