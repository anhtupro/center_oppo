<?php

class TsController extends My_Controller_Action
{

    public function indexAction()
    {
        
        echo strtotime('2017-04-30 00:00:00'); die;
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                    . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files\kho_tang_ca.xlsx'; 
        $readExcel = new My_ReadExcel($path);
        $data = $readExcel->ConvertToArray($header);

        echo '<pre>';
        print_r($data); die;

        $tableRender = new My_TableRender();

        $class = 'table table-bordered';

        $params_insuranlist = array(
            'class' => $class,
            'header' => array(
                'code' => 'Mã nhân viên',
                'name' => 'Họ tên',
                'department' => 'Bộ phận',
                'title' => 'Chức vụ',
                'insurance_number' => 'Số sổ bảo hiểm',
                'month_join' => 'Tháng tham gia bảo hiểm',
                'month_sub' => 'Tháng giảm bảo hiểm',
                'status' => 'Trạng thái',
                'action' => 'Thao tác'

            ),
            'number_row_empty_data' => 10
        );

        


        $tableInsurance = $tableRender->renderTable($params_insuranlist);
        $this->view->tableInsurance = $tableInsurance;
    }

    public function transferToContractAction()
    {
        $note = $this->getRequest()->getParam('note');
        $transfer_id = $this->getRequest()->getParam('transfer_id');
        $submit = $this->getRequest()->getParam('submit', 0);
        $type = $this->getRequest()->getParam('type', 0);

        if(!empty($submit))
        {
            $number = 0;
            if($type == 2)
            {
                $array_transfer_id = explode(",", $transfer_id);
                print_r($array_transfer_id); die;
                foreach($array_transfer_id as $key => $value)
                {
                    if(is_numeric($value))
                    {
                        My_Staff_Log::transfer_to_contract(intval($value));
                        $number++;
                    }
                }
            }
            elseif($type == 1 AND !empty($note))
            {
                $db = Zend_Registry::get('db');
                $sql = "SELECT id FROM `staff_transfer` WHERE note = :note";
                
                $stmt = $db->prepare($sql);
                $stmt->bindParam('note', $note, PDO::PARAM_STR);

                $stmt->execute();
                $data = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;

                foreach($array_transfer_id as $key => $value)
                {
                    echo $value['id']; die;
                    if(is_numeric($value['id']))
                    {
                        // My_Staff_Log::transfer_to_contract(intval($value['id']));
                        $number++;
                    }
                }

            }
            
            $this->view->number = $number;
        }
        else
        {
            $this->view->number = -1;
        }
        // 
    }

    public function checkPhotoAction()
    {
        $sql = "SELECT id, photo, id_photo, id_photo_back
                FROM `staff`
                WHERE status = 1 
                    AND (photo IS NOT NULL
                            OR id_photo IS NOT NULL 
                            OR id_photo_back IS NOT NULL)
                    ";
        
        $db = Zend_Registry::get('db');
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $array_photo = array();

        foreach($data as $key => $value)
        {
            $img_url = '/photo/staff/' . $value['id'] . '/';
            if(!file_exists(APPLICATION_PATH . '/../public' . $img_url . $value['photo']) && !empty($value['photo']))
            {
                $array_photo[] = $value['id'];
                echo $value['id'] . '<br >';
            }
            elseif(!file_exists(APPLICATION_PATH . '/../public' . $img_url . 'ID_Front/' . $value['id_photo']) && !empty($value['id_photo']))
            {
                $array_photo[] = $value['id'];
                echo $value['id'] . '<br >';
            }
            elseif(!file_exists(APPLICATION_PATH . '/../public' . $img_url . 'ID_Back/' . $value['id_photo_back']) && !empty($value['id_photo_back']))
            {
                $array_photo[] = $value['id'];
                echo $value['id'] . '<br >';
            }
        }

        // echo '<pre>';
        // print_r($array_photo); die;

        exit;
    }

    public function reportAction()
    {
        $tableRender = new My_TableRender();

        $class = 'table table-bordered';

        $params_insuranlist = array(
            'class' => $class,
            'header' => array(
                'stt' => 'STT',
                'mdv' => 'Mã đơn vị',
                'mdv' => 'Mã đơn vị',
            ),
            'number_row_empty_data' => 10
        );

        


        $tableInsurance1 = $tableRender->renderTable($params_insuranlist);
        $this->view->tableInsurance = $tableInsurance;
    }

    public function apiTimeAction()
    {
        $db = Zend_Registry::get('db');

        $getdata = $this->getRequest()->getParam('getdata');
        $month = $this->getRequest()->getParam('month', intval(date('m')));
        $year = $this->getRequest()->getParam('year', intval(date('Y')));

        // if(!empty($getdata))
        {
            $from_date = $year . '-' . $month . '-01';
            $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
            $to_date = $year . '-' . $month . '-' . $number_day_of_month;

            $from_date = '2016-06-01';
            $to_date = '2016-06-30';

            $cong_dinh_muc = 0;

            $from_date_unix = strtotime($from_date);
            $to_date_unix = strtotime($to_date);

            for($i = $from_date_unix; $i <= $to_date_unix; $i+=86400)
            {
                if(date('D', $i) != 'Sun')
                {
                    $cong_dinh_muc++;
                }
            }


            $sql = "
                SELECT
                    st.code as `staff`,
                    t.*,
                    cgm.company_group as `company_group`,
                    day(t.created_at) as `day`
                FROM `staff` st
                LEFT JOIN `company_group_map` cgm ON st.title = cgm.title 
                LEFT JOIN `time` t ON t.staff_id = st.id
                WHERE 
                    (t.id IS NULL OR
                    DATE(t.created_at) BETWEEN :from_date AND :to_date)
                    AND t.off <> 1
                    AND t.status = 1
                    AND t.shift <> 7 
            ";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
            $stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
            $stmt->execute();

            $data_time = $stmt->fetchAll();
            $stmt->closeCursor();

            echo '<pre>';
            $array_api_data = array();

            foreach($data_time as $key_time => $value_time)
            {
                $var = '';
                if(in_array($value_time['shift'], array(1, 3, 6, 0)))
                {
                    $var = 'X';
                }
                elseif(in_array($value_time['shift'], array(2)))
                {
                    $var = 'G';
                }
                $array_api_data[$value_time['staff_id']]['company_group'] = $value_time['company_group'];
                $array_api_data[$value_time['staff_id']]['time'][$value_time['day']] = $var;
            }

            
            $sql_training = "SELECT
                                tcd.staff_id,
                                tcd.new_staff_id,
                                day(tct.date) as `day`
                            FROM `trainer_course_detail` tcd
                            JOIN `trainer_course_timing` tct ON tct.course_detail_id = tcd.id
                            WHERE tct.date BETWEEN :from_date AND :to_date";

            $stmt = $db->prepare($sql_training);
            $stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
            $stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
            $stmt->execute();

            $data_training = $stmt->fetchAll();
            $stmt->closeCursor();

            foreach($data_training as $key_training => $value_training)
            {
                $array_api_data[$value_training['staff_id']]['time'][$value_training['day']] = 'T';
            }

            foreach($array_api_data as $key_api => &$value_api)
            {
                $total_time = 0;
                $gay = 0;
                $training = 0;
                $leave = 0;
                $special_x2 = 0;    
                $special_x3 = 0;
                $special_x4 = 0;
                $sunday = 0;

                foreach($value_api['time'] as $key_api_time => $value_api_time)
                {
                    switch ($value_api_time) {
                        case 'X':
                            $total_time++;
                            break;
                        case 'G':
                            $total_time++;
                            $gay++;
                            break;
                        case 'T':
                            $training++;
                            break;
                        case 'S':
                            $sunday++;
                            break;
                        default:
                            //
                            break;
                    }
                }
                $value_api['tong'] = $total_time;
                $value_api['gay'] = $gay;
                $value_api['training'] = $training;
                $value_api['leave'] = $leave;
                $value_api['special_x2'] = $special_x2;
                $value_api['special_x3'] = $special_x3;
                $value_api['special_x4'] = $special_x4;
                $value_api['sunday'] = $sunday;
            }

            $sql_special = "SELECT
                                day(ada.date) as `day`,
                                ada.type_id as `company_group`,
                                ada.multiple as `multiple`,
                                dc.code as `code`,
                                dc.code_half as `code_half`,
                                ada.public as `public`
                            FROM `all_date_advanced` ada
                            JOIN `date_category` as dc ON ada.category_date = dc.id
                            WHERE
                                ada.is_off = 1
                                AND ada.date BETWEEN :from_date AND :to_date";

            $stmt = $db->prepare($sql_special);
            $stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
            $stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
            $stmt->execute();

            $data_special = $stmt->fetchAll();
            $stmt->closeCursor();

            $array_special = array();

            foreach($data_special as $key_special => $value_special)
            {
                $array_special[$value_special['company_group']][$value_special['day']] = $value_special;
            }

            $array_values = array();
            
            foreach($array_api_data as $key_values => $values_values)
            {
                if(!empty($key_values))
                {
                    $array_values[] = "(" . $key_values .
                                        ", " . $month . 
                                        ", " . $year .
                                        ", " . $cong_dinh_muc .
                                        ", " . $values_values['tong'] .
                                        ", " . $values_values['gay'] .
                                        ", " . $values_values['training'] .
                                        ", " . $values_values['leave'] .
                                        ", " . $values_values['special_x2'] .
                                        ", " . $values_values['special_x3'] .
                                        ", " . $values_values['special_x4'] .
                                        ", " . $values_values['sunday'] .
                                        ")"; 
                }
            }
            $sql_delete = "DELETE FROM `temp_staff_time` WHERE `month` = :month AND `year` = :year";
            $stmt = $db->prepare($sql_delete);
            $stmt->bindParam('month', $month, PDO::PARAM_INT);
            $stmt->bindParam('year', $year, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->closeCursor();

            $sql_insert = "INSERT INTO `temp_staff_time` 
                            (`staff_id`, `month`, `year`, `cong_dinh_muc` ,
                                 `total`, `break`,
                                `training`, `leave`, `special_x2`, `special_x3`, 
                                `special_x4`, `sunday`) VALUES " . implode(", ", $array_values) . ";";
            $stmt = $db->prepare($sql_insert);
            $stmt->execute();

            $stmt->closeCursor();
            $stmt = $db = null;
        }

        die;
    }

}

// class alphaExcel
// {
//     protected $_first;
//     protected $_second;

//     public function alphaExcel()
//     {
//         $this->_first = '';
//         $this->_second = 'A';
//     }

//     public function Show()
//     {
//         return $this->_first.$this->_second;
//     }

//     public function ShowAndUp()
//     {
//         $current = $this->Show();
//         $this->Up();
//         return $current;
//     }

//     public function Up()
//     {
//         if($this->_first == '')
//         {
//             if($this->_second == 'Z')
//             {
//                 $this->_second = 'A';
//                 $this->_first = 'A';
//             }
//             else
//             {
//                 $this->_second++;
//             }
//         }
//         else
//         {
//             if($this->_second == 'Z')
//             {
//                 $this->_second++;
//                 $this->_first = 'A';
//             }
//             else
//             {
//                 $this->_second++;
//             }
//         }
//     }
// }

