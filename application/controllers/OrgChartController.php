<?php

class OrgChartController extends My_Controller_Action {

    public function indexAction() {

        $flashMessenger = $this->_helper->flashMessenger;
        try {

            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $department = $userStorage->department;
            $team = $userStorage->team;
            $staffId = $userStorage->id;

            //array data for drawchart

            $chartData = array(
                1 => array(
                    'id' => 152,
                    'chart' => 82,
                    'parent_id' => 0,
                    'name' => 'Dept. Sales',
                ),
                2 => array(
                    'id' => 75,
                    'chart' => 83,
                    'parent_id' => 152,
                    'name' => '-- Dept. Sales - Team Sales',
                ),
                3 => array(
                    'id' => 394,
                    'chart' => 84,
                    'parent_id' => 152,
                    'name' => '-- Dept. Sales - Team Key Account',
                ),
                4 => array(
                    'id' => 406,
                    'chart' => 85,
                    'parent_id' => 0,
                    'name' => 'Dept. Sales Online',
                ),
                5 => array (
                    'id'=> 610,
                    'chart' => 86,
                    'parent_id' => 0,
                    'name'=>'Dept. Retail',
                ),
                6 => array (
                    'id'=> 630,
                    'chart' => 87,
                    'parent_id' => 610,
                    'name'=>'-- Dept. Retail - Team Brand Shop',
                ),
                7 => array (
                    'id'=> 611,
                    'chart' => 89,
                    'parent_id' => 610,
                    'name'=>'-- Dept. Retail - Team Trade Marketing',
                ),
                8 => array (
                    'id'=> 619,
                    'chart' => 88,
                    'parent_id' => 610,
                    'name'=>'-- Dept. Retail - Team Training',
                ),
                9 => array(
                    'id' => 630,
                    'chart' => 87,
                    'parent_id' => 610,
                    'name' => 'Dept. BrandShop',
                ),
                10 => array(
                    'id' => 619,
                    'chart' => 88,
                    'parent_id' => 610,
                    'name' => 'Dept. Training',
                ),
                11 => array(
                    'id' => 611,
                    'chart' => 89,
                    'parent_id' => 610,
                    'name' => 'Dept. Trade Marketing',
                ),
                12 => array(
                    'id' => 557,
                    'chart' => 90,
                    'parent_id' => 0,
                    'name' => 'Dept. GTM',
                ),
                13 => array(
                    'id' => 156,
                    'chart' => 91,
                    'parent_id' => 0,
                    'name' => 'Dept. Marketing',
                ),
                14 => array(
                    'id' => 159,
                    'chart' => 92,
                    'parent_id' => 0,
                    'name' => 'Dept. Service',
                ),
                15 => array(
                    'id' => 160,
                    'chart' => 93,
                    'parent_id' => 0,
                    'name' => 'Dept. Call Center',
                ),
                16 => array(
                    'id' => 400,
                    'chart' => 94,
                    'parent_id' => 0,
                    'name' => 'Dept. Purchasing',
                ),
                17 => array(
                    'id' => 149,
                    'chart' => 97,
                    'parent_id' => 0,
                    'name' => 'Dept. Finance',
                ),
                18 => array(
                    'id' => 150,
                    'chart' => 98,
                    'parent_id' => 0,
                    'name' => 'Dept. Logistic',
                ),
                19 => array(
                    'id' => 151,
                    'chart' => 99,
                    'parent_id' => 0,
                    'name' => 'Dept. Test',
                ),
                20 => array(
                    'id' => 154,
                    'chart' => 73,
                    'parent_id' => 0,
                    'name' => 'Dept. HR',
                ),
                21 => array(
                    'id' => 153,
                    'chart' => 95,
                    'parent_id' => 0,
                    'name' => 'Dept. Technology',
                )
            );

            $parentChart = array(152, 610, 557, 156, 159, 160, 400, 153, 149, 150, 151);

            $managerId = array(4, 7, 103, 753, 765, 10557, 1451, 26859, 963, 24979, 156, 240, 326, 2906, 5899, 2, 22716, 8807, 6705, 11967,26612);
            $isManager = false;
            if (in_array($staffId, $managerId))
                $isManager = true;

            //end array data for drawchart
            // echo $select;
            // exit;


            $group_id = $userStorage->group_id;
            $QInform = new Application_Model_Inform();
            //100 la ID Chart tong
            $inform = $QInform->find(100);

            $inform = $inform->current();

            $QInformFile = new Application_Model_InformFile();
            $where = $QInformFile->getAdapter()->quoteInto('inform_id = ?', $id);
            $files = $QInformFile->fetchAll($where);

            if (!$inform)
                throw new Exception("Invalid ID");

            $QInformTitle = new Application_Model_InformTitle();
            //$check = $QInformTitle->check_view($id);

            $this->view->files = $files;
            $this->view->userStorage = $userStorage;
            $this->view->inform = $inform;
            $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
            $this->view->chartData = $chartData;
            $this->view->parentChart = $parentChart;
            $this->view->department = $department;
            $this->view->team = $team;
            $this->view->isManager = $isManager;
        } catch (exception $e) {
            echo $e->getMessage();
            exit;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->_redirect(HOST . 'manage/inform');
        }
    }

    public function getChartContentAction() {
        $this->_helper->viewRenderer->setNoRender();

        $id = $this->getRequest()->getParam('chartId');

        $QInform = new Application_Model_Inform();
        $inform = $QInform->find($id);
        $inform = $inform->current();

        echo json_encode(array(
            'content' => $inform->content,
            'title' => $inform->title,
        ));
        exit;
    }

}
