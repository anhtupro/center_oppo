<?php
/**
 * @author buu.pham
 * @create 2015-09-09 15:44
 * Quản lý target của pg, sale; asm bảo vệ các nv nào cần cho thử thách thêm
 */
class TargetController extends My_Controller_Action
{
    /**
     * @author buu.pham
     * Cài mức target - chưa cần giao diện
     * @return [type] [description]
     */
    public function configAction()
    {
			
        $this->_helper->viewRenderer->setNoRender();
        error_reporting(1);
        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
        $this->wssURI = 'https://cs.realmeshop.vn/wss?wsdl';
        $this->namespace = 'OPPOVN';

        $client = new nusoap_client($this->wssURI);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = true;
		$imei  = '861433043002410';
        $wsParams = array(
            'Imei' =>  $imei
        );
        $result_check = $client->call("getInfoImeiAdvance", $imei);
		echo "<pre>";
    print_r($result_check);
    echo "</pre>";
    }
    
    
    /**
     * @author ToanDinh
     * Thay đổi số lượng target
     * @return [target] 
     */
    public function  changeTargetAction()
    {
        $QSalesTarget = new Application_Model_SalesTarget();
        $this->view->listTarget = $QSalesTarget->fetchAll();
        
        $option = $this->getRequest()->getParam('option');
        $id = $this->getRequest()->getParam('id');
  
        if($option == 'del' and $id){
            $where_ = $QSalesTarget->getAdapter()->quoteInto('id = ?', $id);
            $QSalesTarget->delete($where_);
            
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('Delete success');
            $this->redirect(HOST.'target/change-target');

        }
        
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
        
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->error_messages = $messages;   
    
    }
    
    
    public function  changeTargetCreateAction()
    {   
        $QSalesTarget = new Application_Model_SalesTarget();
        $id = $this->getRequest()->getParam('id');
        
        if($id){
            $where = $QSalesTarget->getAdapter()->quoteInto('id = ?', $id);
            $targetByID = $QSalesTarget->fetchRow($where);
            $this->view->targetByID =  $targetByID;
            
        }
        
        if ($this->getRequest()->getMethod() == 'POST'){

             try{
                 
                $QSalesTarget = new Application_Model_SalesTarget();
                $id = $this->getRequest()->getParam('id');
                
                $object_id = $this->getRequest()->getParam('object_id');
                $target = $this->getRequest()->getParam('target');
                $from_date = $this->getRequest()->getParam('from_date');
                $from_date = date('Y-m-d', strtotime($from_date));
                                                                   
                 $data = array(
                     'object_id'   => $object_id,
                     'target'      => $target,
                     'from_date'   => $from_date,
                 );

//                    	echo "<pre>";
//	print_r($data);
//	echo "</pre>"; die();
                 if ($id){

                     $where_ = $QSalesTarget->getAdapter()->quoteInto('id = ?', $id);
                     $QSalesTarget->update($data, $where_);
                    
                    $flashMessenger = $this->_helper->flashMessenger;
                    $flashMessenger->setNamespace('success')->addMessage('Update success');
                    $this->redirect(HOST.'target/change-target');
                	
                 } else {
                     $QSalesTarget->insert($data);
                    $flashMessenger = $this->_helper->flashMessenger;
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                    $this->redirect(HOST.'target/change-target');
                 }
                 
                

             }
             catch(exception $e)
             {
                 $flashMessenger = $this->_helper->flashMessenger;
                 $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                	$this->view->messages = $messages;
             }

         }

            
    }
    

    /**
     * @author buu.pham
     * Danh sách các PG/Sale chưa đạt target
     * @return [type] [description]
     */
    public function listAction()
    {
        $sort     = $this->getRequest()->getParam('sort', 'total');
        $desc     = $this->getRequest()->getParam('desc', 1);
        $month    = $this->getRequest()->getParam('month', date('m/Y'));
        $name     = $this->getRequest()->getParam('name');
        $area     = $this->getRequest()->getParam('area');
        $province = $this->getRequest()->getParam('province');
        $district = $this->getRequest()->getParam('district');
        $email    = $this->getRequest()->getParam('email');
        $paction  = $this->getRequest()->getParam('paction', My_Sale_Target_Action::ALL);
        $type     = $this->getRequest()->getParam('type');
        $export = $this->getRequest()->getParam('export',0);

        $page   = $this->getRequest()->getParam('page', 1);
        $limit  = LIMITATION;
        $total  = 0;

        if (empty($month) || !date_create_from_format('m/Y', $month))
            $month = date('m/Y');

        $month_obj = date_create_from_format('m/Y', $month);
        $from = $month_obj->format('01/m/Y');
        $to = $month_obj->format('t/m/Y');

        $month_target = $month_obj->format('m');
        $year_target = $month_obj->format('Y');

        $params = array(
            'from'     => $from,
            'to'       => $to,
            'month'    => $month,
            'month_target' => $month_target,
            'year_target' => $year_target,
            'name'     => $name,
            'area'     => $area,
            'province' => $province,
            'district' => $district,
            'email'    => $email,
            'type'     => $type,
            'paction'  => $paction,
            'sort'     => $sort,
            'desc'     => $desc,
            'export' => $export
        );

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userStorage || !isset($userStorage->id)) $this->_redirect(HOST);

        $group_id = $userStorage->group_id;

        $QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $QTeam = new Application_Model_Team();
        $this->view->teams = $QTeam->get_cache();

        $QRegionalMarket = new Application_Model_RegionalMarket();

        if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
            // lấy khu vực của asm
            $QAsm = new Application_Model_Asm();
            $asm_cache = $QAsm->get_cache();
            $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
        }

        if ($area) {
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area);
            $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
        }

        if ($province) {
            $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $province); 
            $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
        }

        $QSalesTarget = new Application_Model_SalesTarget();

        try {
            $targets = $QSalesTarget->fetchPagination($page, $limit, $total, $params);
     
            if($export){
                $this->_exportTargetList($targets);
            }

            $this->view->targets = $targets;

        } catch (Exception $e) {
            $this->view->error = $e->getMessage();
        }
        $this->view->limit       = $limit;
        $this->view->total       = $total;
        $this->view->page        = $page;
        $this->view->current_col = $sort;
        $this->view->desc        = $desc;
        $this->view->offset      = $limit*($page-1);
        $this->view->params      = $params;
        $this->view->url         = HOST.'target/list'.( $params ? '?'.http_build_query($params).'&' : '?' );

    }

    /**
     * @author buu.pham
     * ASM bảo vệ nv chưa đạt target
     * @return [type] [description]
     */
    public function actionAction()
    {
        $type     = $this->getRequest()->getParam('type');
        $staff_id = $this->getRequest()->getParam('staff_id');
        $action   = $this->getRequest()->getParam('paction');
        $from     = $this->getRequest()->getParam('from');
        $to       = $this->getRequest()->getParam('to');
        $note     = $this->getRequest()->getParam('note');
        $month     = $this->getRequest()->getParam('month');
        $month_target    = $this->getRequest()->getParam('month_target');
        $year_target     = $this->getRequest()->getParam('year_target');
        

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id)) throw new Exception("Invalid user", 8);

            if (!$action || !in_array($action, array(My_Sale_Target_Action::KILL, My_Sale_Target_Action::PROTECT)))
                throw new Exception("Invalid action", 1);

            if (!$type || !in_array($type, array(My_Staff_Group::PG, My_Staff_Group::SALES)))
                throw new Exception("Invalid type", 6);

            if (!$staff_id || !intval($staff_id))
                throw new Exception("Invalid staff id", 2);

            if (!$from || !date_create_from_format("d/m/Y", $from))
                throw new Exception("Invalid From date", 4);

            if (!$to || !date_create_from_format("d/m/Y", $to))
                throw new Exception("Invalid To date", 5);

            if ($note) $note = My_String::trim($note);

            $staff_id = intval($staff_id);
            $from     = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
            $to       = date_create_from_format("d/m/Y", $to)->format("Y-m-d");

            $QStaff      = new Application_Model_Staff();
            $where       = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $staff_check = $QStaff->fetchRow($where);
            if (!$staff_check) throw new Exception("Invalid staff", 3);


            $sell_out = 0;

            switch ($type) {
                case My_Staff_Group::PG:
                    $sell_out = $db->fetchOne(sprintf(
                        "SELECT fn_get_sell_out_pg(%d, '%s', '%s')",
                        $staff_id,
                        $from,
                        $to
                    ));
                    break;
                case My_Staff_Group::SALES:
                    $sell_out = $db->fetchOne(sprintf(
                        "SELECT fn_get_sell_out_sale(%d, '%s', '%s')",
                        $staff_id,
                        $from,
                        $to
                    ));
                    break;

                default:
                    throw new Exception("Invalid type", 6);
                    break;
            }

            if (My_Sale_Target_Action::PROTECT == $action) {
                $allow_protect = $db->fetchOne(
                    sprintf(
                        "SELECT fn_allow_protect(%d, %d, '%s', '%s', '%s', %s)",
                        $staff_id,
                        $type,
                        $staff_check['joined_at'],
                        $from,
                        $to,
                        $sell_out
                    )
                );

                if (!$allow_protect) throw new Exception("Cannot protect this staff", 7);
            }

            $result = array('result' => 0, 'message' => 'Success'); // biến result dùng trả về json nếu success
            $result['action'] = $action;
            $result['staff_name'] = $staff_check['firstname'].' '.$staff_check['lastname'];

            if(My_Sale_Target_Action::PROTECT == $action){
                // ghi log lần protect
                $QSalesTargetProtectLog = new Application_Model_SalesTargetProtectLog();
                $data = array(
                    'staff_id'    => $staff_id,
                    'action_type' => $action,
                    'action_at'   => date('Y-m-d H:i:s'),
                    'action_by'   => $userStorage->id,
                    'note'        => $note,
                    'sell_out'    => $sell_out,
                    'type'        => $type,
                    //'sales_target_protect_id' => $id,
                    'month_target'            => $month_target,
                    'year_target'             => $year_target,
                );
                $QSalesTargetProtectLog->insert($data);
            }else {
                // trường hợp KILL
                $QSalesTargetProtect = new Application_Model_SalesTargetProtect();
                $where = array();
                $where[] = $QSalesTargetProtect->getAdapter()->quoteInto('kill_at IS NOT NULL', date('y'));
                $where[] = $QSalesTargetProtect->getAdapter()->quoteInto('staff_id = ?', $staff_id);
                $protect = $QSalesTargetProtect->fetchRow($where);
                
                if ($protect && isset($protect['kill_at'])) throw new Exception("This staff was killed", 8);
                $kill_at = date('Y-m-d H:i:s');

                $data_kill = array(
                    'staff_id' => $staff_id,
                    'kill_at' => $kill_at,
                    'note' => $note,
                );
                $result['kill_at'] = date('d/m/Y H:i:s', strtotime($kill_at));
                $QSalesTargetProtect->insert($data_kill);
            }
            
            $QSalesTargetProtectLog = new Application_Model_SalesTargetProtectLog();
            $where_ = array();
            $where_[] = $QSalesTargetProtectLog->getAdapter()->quoteInto('year_target = ?', $year_target);
            $where_[] = $QSalesTargetProtectLog->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $protectLog = $QSalesTargetProtectLog->fetchAll($where_);
            // số lần bảo vệ sẽ count(staff_id) theo năm đang view
            $result['protect_time'] = count($protectLog);
            $result['log_id'] = $staff_id;
            $result['year_target'] = $year_target;
      
            $db->commit();

            exit(json_encode($result));
        } catch (Exception $e) {
            $db->rollback();
            exit(json_encode(array('result' => $e->getCode(), 'message' => $e->getMessage())));
        }
    }

    /**
     * @author buu.pham
     * Xem log các lần bảo vệ
     * @return [type] [description]
     */
    public function viewAction()
    {
        $this->_helper->layout->disableLayout();
        $id = $this->getRequest()->getParam('id');
        $year_target = $this->getRequest()->getParam('year_target');
        
        try {

            $QSalesTargetProtectLog = new Application_Model_SalesTargetProtectLog(); 
            $where = array(); 
            $where[] = $QSalesTargetProtectLog->getAdapter()->quoteInto('staff_id = ?', $id); 
            $where[] = $QSalesTargetProtectLog->getAdapter()->quoteInto('year_target = ?', $year_target); 
            $this->view->logs = $QSalesTargetProtectLog->fetchAll($where); 

            $QStaff = new Application_Model_Staff();
            $this->view->staffs = $QStaff->get_cache();
        } catch (Exception $e) {

        }
    }

    private  function _exportTargetList($data,$params = NULL){
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'STT',
            'Name',
            'Code',
            'Title',
            'Email',
            'Area',
            'Province',
            'Joined At',
            'Working Time',
            'Sell Out',
            'Protect',
            'Kill',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        $QTeam = new Application_Model_Team();
        $titles = $QTeam->get_cache();

        // echo "<pre>";print_r($data);die;
        try
        {
            $intCount = 1;
            foreach ($data as $_key => $item)
            {
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['firstname'].' '.$item['lastname'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($titles[$item['title']],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(str_replace(EMAIL_SUFFIX, '', $item['email']),PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['area'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['province'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(date('d/m/Y', strtotime($item['joined_at'])),PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['diff'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['total'],PHPExcel_Cell_DataType::TYPE_STRING);
                $protect = $kill = '';

                if (isset($item['protect_time_target']) && $item['protect_time_target']){
                    $protect = $item['protect_time_target'];
                }

                if (isset($item['kill_at'])){
                    $kill = date('d/m/Y H:i:s', strtotime($item['kill_at']));
                }

                $sheet->getCell($alpha++ . $index)->setValueExplicit($protect,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($kill,PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
        }catch (exception $e){

        }

        $month = (isset($params['month']) AND $params['month']) ? $params['month'] : '';
        $filename = 'Target ' . $month;
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
    
    public function settingAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'setting.php';
    }
    
    public function infoSettingAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'info-setting.php';
    }
	
	 public function infoSettingAsmAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'info-setting-asm.php';
    }
    
    public function infoStoreAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'info-store.php';
    }
    
    public function addTargetAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'add-target.php';
    }
	
	 public function asmAddTargetAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'asm-add-target.php';
    }
    
    public function approveTargetAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'approve-target.php';
    }
    
    public function approveAllAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'approve-all.php';
    }
    public function rejectAllAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'reject-all.php';
    }
     public function rejectTargetAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'reject-target.php';
    }
    
    public function listStoreAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'list-store.php';
    }
	
	
	public function listStoreNotSaleAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'list-store-not-sale.php';
    }
	public function listStoreBySaleAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'list-store-by-sale.php';
    }
	public function listStoreNotLeaderAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'list-store-not-leader.php';
    }
	
	public function listStoreNotSaleNotLeaderAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'list-store-not-sale-not-leader.php';
    }
    public function getProductAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'get-product.php';
    }
    
    public function targetLeaderAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'target-leader.php';
    }
    
    public function salaryAreaAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'salary-area.php';
    }
    public function healthCostSalaryAction(){   
                                    //create at 24-06-2019
        require_once 'target'.DIRECTORY_SEPARATOR.'health-cost-salary.php';
    }
    public function salaryPgAreaAction(){                            
        require_once 'target'.DIRECTORY_SEPARATOR.'salary-pg-area.php';
    }
    public function targetAsmAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'target-asm.php';
    }
	
    public function testApiAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'test-api.php';
    }
	
    public function saveMassUploadPgAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'save-mass-upload-pg.php';
    }
    
    public function massUploadPgAction(){
        
    }
    
    public function resultSalaryPgAreaAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'result-salary-pg-area.php';
    }
	
    public function bonusPgAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'bonus-pg.php';
    }
    public function healthAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'health.php';
    }
	
    public function salaryAdminAreaAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'salary-admin-area.php';
    }
	
	public function saveMassUploadAdminAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'save-mass-upload-admin.php';
    }
	public function massUploadAdminAction(){
        
    }
    
    public function salaryAdminAreaTwentyAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'salary-admin-area-twenty.php';
    }
    
    public function massUploadAdminTwentyAction(){
        
    }
    
    public function saveMassUploadAdminTwentyAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'save-mass-upload-admin-twenty.php';
    }
    
    public function listTransferBankAction(){
        require_once 'target'.DIRECTORY_SEPARATOR.'list-transfer-bank.php';
    }
}

