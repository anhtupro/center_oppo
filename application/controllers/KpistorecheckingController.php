<?php
class KpistorecheckingController extends My_Controller_Action
{
	public function init(){
		$this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
		// echo '<pre>';print_r($this->userStorage);die;
	}

	public function indexAction(){
 
	$QTeam  = new Application_Model_Team();
		$db		= Zend_Registry::get('db');
		$type	= My_Util::escape_string($this->getRequest()->getParam('type'));
		//echo $type;die;
		$code	= My_Util::escape_string($this->getRequest()->getParam('code'));
		$email	= My_Util::escape_string($this->getRequest()->getParam('email'));
		$filter	= My_Util::escape_string($this->getRequest()->getParam('filter'));
		$store_name = My_Util::escape_string($this->getRequest()->getParam('name_store'));
		$regional_market = $this->userStorage->regional_market;
		$area_id = $this->userStorage->regional_market_id;
		$userStorage = Zend_Auth::getInstance ()->getStorage ()->read ();
                $staff_title_info = $userStorage->title;
                $team_info = $QTeam->find($staff_title_info);
                $team_info = $team_info->current();
                $group_id = $team_info['access_group'];

//		$group_id= $this->userStorage->group_id;
		
		$user_id = $this->userStorage->id;
		
		$QAsm          = new Application_Model_Asm();
		$list_regions  = $QAsm->get_cache($user_id);
		$list_province = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();
		$list_area     = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
		$list_area_str  = implode(",",$list_area);

		$is_admin = (in_array($group_id, array(ADMINISTRATOR_ID, SALES_EXT_ID,HR_ID))) ? 1 : 0;
                if(!$is_admin){
                    $regional_market=implode($list_province,",");
                   
                }
                $this->view->regional_market = $this->userStorage->regional_market_id;
		
		$this->view->store_name	= $store_name;
		$this->view->group_id	= $group_id;
		$this->view->user_id = $user_id;
                
		if($store_name)
		{
			$store_name = explode(PHP_EOL, trim($store_name));
			$store_name = '"'.trim(implode('","', $store_name)).'"';
		}
		else
			$store_name = NULL;

		$search	= new Application_Model_KpiStoreChecking();

		$get_info_staff			= $search->getStaffByEmail($code,$email);

		$this->view->area_id	= $get_info_staff['area_id'];

		$this->view->list		= $get_info_staff;

		$this->view->staffid	= $get_info_staff['id'];
		if ($code != NULL || $email != NULL)
		{
			if($type == 1 )
			{
				$filter == "on" ? $store_on = 1 : $store_on = NULL;
				$table = "store_staff_log";
				$result_staff		= $search->searchStore($table,$code,$email,$store_name,$group_id,$area_id,$regional_market,$store_on, $is_admin);
				$this->view->result	= $result_staff;
			}
			else 
			{
				$filter == "on" ? $store_on = 1 : $store_on = NULL;
				$table = "store_leader_log";
				$result_leader		= $search->searchStore($table,$code,$email,$store_name,$group_id,$area_id,$regional_market,$store_on, $is_admin);
				$this->view->result	= $result_leader;
			}

		}
		
		$this->view->code		= $code;
		$this->view->email		= $email;
		$this->view->type		= $type;
		if($type == 1)
		{
			$this->view->shop = "Shop trực tiếp";
		}
		else if($type == 2)
		{
			$this->view->shop = "Shop gián tiếp";
		}
		$this->view->filter		= $filter;
		$this->view->id_staff	= $get_info_staff['id'];
			
	}

	public function massUpdateAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){

			$params = array_merge(
				array(
					'id'	=> null,
					'fd'	=> null,
					'td'	=> null,
					'type'	=> null,

				),
				$this->_request->getParams()
			);
			
			
	 	    $type = $params['type'];

			$kpistore = new Application_Model_KpiStoreChecking();

			$data = array(
				'id'	=> $params['id'],
				'from'	=> $params['fd'],
				'to'	=> $params['td']
			);
			
			$from	= $params['fd'] ? DateTime::createFromFormat('d/m/Y', $params['fd'])->format('Y-m-d').date(' H:i:s') : NULL;
			$from   = strtotime($from);
			
			$to		= $params['td'] ? DateTime::createFromFormat('d/m/Y', $params['td'])->format('Y-m-d').date(' H:i:s') : NULL;
			$to     = strtotime($to);

			
			$data['from']	= $from;
			$data['to']		= $to;

			// if(!$to) unset($data['to']);
			// echo '<pre>';print_r($data);die;
			

			$res  = null;
			if ($type == 1)
			 	$res = $kpistore->massUpdateStaff($data);
			else
				$res = $kpistore->massUpdateLeader($data);

			if($res)
				echo json_encode(array('status' => true));
			else
				echo json_encode(array('status' => false));
		}
	}
	
	public function saveAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){

			$params = array_merge(
				array(
					'id'        => null,
					'from_date' => null,
					'to_date'   => null,
					'type'      => null
				),
				$this->_request->getParams()
			);
			
			$tracking = new Application_Model_KpiStoreChecking();


			$type     = $params['type'];
			$fromDate = DateTime::createFromFormat('d/m/Y', $params['from_date']);
			$fromDate = $fromDate ? $fromDate->format('Y-m-d H:i:s') : null;
			$toDate   = DateTime::createFromFormat('d/m/Y', $params['to_date']);
			$toDate   = $toDate ? $toDate->format('Y-m-d H:i:s') : null;

			$tracking_date = array(
				'from_date'	=> $fromDate,
				'to_date'	=> $toDate
			);
			$tracking_date	= json_encode($tracking_date);
			

			$from			= strtotime($fromDate) ? strtotime($fromDate) : null;
			$to				= strtotime($toDate) ? strtotime($toDate) : null;

			if ($type == 1)
			{
				$staff             = new Application_Model_StoreStaffLog();
				$edit              = $staff->findPrimaykey($params['id']);

				$edit->joined_at   = $from;
				$edit->released_at = $to;
				$res = $edit->save();

				$data = array(
					"staff_id"		=> Zend_Auth::getInstance()->getStorage()->read()->id,
					"created_at"	=> date("Y-m-d H:i:s"),
					"table_name"	=>"store_staff_log",
					"id_tracking"	=> $edit->id,
					"params"		=>  $tracking_date
				);
			  	$data = $tracking->storeTracking($data);
				
				if($res)
					echo json_encode(array('status' => true));
				else
					echo json_encode(array('status' => false));

			}
			else if($type == 2)
			{
				$leader            = new Application_Model_StoreLeaderLog();
				$edit              = $leader->findPrimaykey($params['id']);
				$edit->joined_at   = $from;
				$edit->released_at = $to;
				$res = $edit->save();

				$data = array(
					"staff_id"		=> Zend_Auth::getInstance()->getStorage()->read()->id,
					"created_at"	=> date("Y-m-d H:i:s"),
					"table_name"	=>"store_leader_log",
					"id_tracking"	=> $edit->id,
					"params"		=> $tracking_date
				);
			  	$data = $tracking->storeTracking($data);

				if($res)
					echo json_encode(array('status' => true));
				else
					echo json_encode(array('status' => false));
	        } 
	        exit();
		}
	}
	public function removeallstoreAction(){
		$type	= $this->getRequest()->getParam('type');
		$code	= $this->getRequest()->getParam('code');
		$email	= $this->getRequest()->getParam('email');
		
		
		$back_url		= $this->getRequest()->getParam('back_url','/kpistorechecking?code='.$code.'&email='.$email.'&type='.$type.'');
		
		$staff_id		= $this->getRequest()->getParam('staff_id');
		
		$staff			= new Application_Model_Staff();
		$staff_data		= $staff->findStaffid($staff_id);
		
		$off_date		= strtotime($staff_data->off_date);
		
		$db				= Zend_Registry::get('db');
		$sql			= '
		UPDATE store_staff_log 
		SET released_at	='.$off_date.' WHERE staff_id = '.$staff_id.' AND released_at IS NULL;
		UPDATE store_leader_log 
		SET released_at	='.$off_date.' WHERE staff_id = '.$staff_id.' AND released_at IS NULL;
		';
		$db->query($sql);
		$db				= null;

		
		$this->_redirect($back_url);
	}
	public function staffKpiAjaxAction()
	{
		$this->_helper->layout->disableLayout();
		$export	= $this->getRequest()->getParam('export');
		$db             = Zend_Registry::get('db');
		$userStorage = Zend_Auth::getInstance()->getStorage()->read();
		$staff_id       = $userStorage->id; 
		$QAsm         = new Application_Model_Asm();
		$list_regions = $QAsm->get_cache($staff_id );
		$list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();

                
		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
			
			$params = array_merge(
					array(
						'staffid'	=> null,
						'from'		=> null,
						'to'		=> null,
						'regional_market' =>null,
						'area_id' => null,
						'group_id' => null,
					),
					$this->_request->getParams()
				);
			$ik = new Application_Model_NewImeiKpi();
			$checkTitle = $ik->checkTitle($params['staffid']);
			$data = array(
				'staff_id'	=> $params['staffid'],
				'from'		=> $params['from'],
				'to'		=> $params['to'],
				'checkTitle' => $checkTitle[0]['title']
			);



			$data['from']	= $params['from'] ? DateTime::createFromFormat('d/m/Y', $params['from'])->format('Y-m-d') : date('Y-m-01 00:00:00');
			$data['to']		= $params['to'] ? DateTime::createFromFormat('d/m/Y', $params['to'])->format('Y-m-d') : date('Y-m-d H:i:s');


			if(
				$params['regional_market'] == $params['area_id'] 
				|| $params['group_id'] == ADMINISTRATOR_ID 
				|| $params['group_id'] == SALES_EXT_ID 
				|| ($params['group_id'] == SALES_ADMIN_ID 
				 && in_array($params['area_id'], $list_regions))
				||  $params['area_id'] == 54
                || in_array($userStorage->title, [226]) //  C&B SPECIALIST
				)
			{
				// if($userStorage->id == 341){

				$data_new = $ik->Get_All_New_Full($data);

				$this->view->res_new      = $data_new;
				// print_r( $data);
				// }
				
				// $data				= $ik->Get_All_New($data);
				// echo "<pre>";print_r($data);die;
				// $this->view->res	= $data;
				
				
                                
			}
			

		}
	}
	public function getTimingAjaxAction()
	{
		$this->_helper->layout->disableLayout();
		$db             = Zend_Registry::get('db');
		$userStorage = Zend_Auth::getInstance()->getStorage()->read();
		$staff_id       = $userStorage->id; 
		$QAsm         = new Application_Model_Asm();
		$list_regions = $QAsm->get_cache($staff_id );
		$list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
		if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest())
		{
			$params = array_merge(
					array(
						'staffid'	=> null,
						'from'		=> null,
						'to'		=> null,
						'regional_market' =>null,
						'area_id' => null,
						'group_id' => null,
					),
					$this->_request->getParams()
			);

			$timing = new Application_Model_KpiStoreChecking();
			
			$data = array(
				'staff_id'	=> $params['staffid'],
				'from'		=>$params['from'],
				'to'		=>$params['to']
			);
			
			$data['from']	= $params['from'] ? DateTime::createFromFormat('d/m/Y', $params['from'])->format('Y-m-d') : date('Y-m-01 00:00:00');
			$data['to']		= $params['to'] ? DateTime::createFromFormat('d/m/Y', $params['to'])->format('Y-m-d') : date('Y-m-d H:i:s');
			
			 
			if($params['regional_market'] == $params['area_id'] || $params['group_id'] == ADMINISTRATOR_ID 
			|| $params['group_id'] == SALES_EXT_ID 
			|| ($params['group_id'] == SALES_ADMIN_ID 
				 && in_array($params['area_id'], $list_regions))
			||  $params['area_id'] == 54)
			{
				if($data['from'] < '2020-03-01' AND $data['from'] >= '2020-02-01'){
                                    $data = array();
                                }else{
                                    $data				= $timing->getAllTiming($data);
                                }
				
				$this->view->timing	= $data;
			}
		}

	}
        
        public function getTimingAjaxRmAction(){
            $this->_helper->layout->disableLayout();
            $db             = Zend_Registry::get('db');
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $staff_id       = $userStorage->id; 
            $QAsm         = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($staff_id );
            $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
            if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest())
            {
                    $params = array_merge(
                                    array(
                                            'staffid'	=> null,
                                            'from'		=> null,
                                            'to'		=> null,
                                            'regional_market' =>null,
                                            'area_id' => null,
                                            'group_id' => null,
                                    ),
                                    $this->_request->getParams()
                    );

                    $timing = new Application_Model_KpiStoreChecking();

                    $data = array(
                            'staff_id'	=> $params['staffid'],
                            'from'		=>$params['from'],
                            'to'		=>$params['to']
                    );
                    $data['from']	= $params['from'] ? DateTime::createFromFormat('d/m/Y', $params['from'])->format('Y-m-d') : date('Y-m-01 00:00:00');
                    $data['to']		= $params['to'] ? DateTime::createFromFormat('d/m/Y', $params['to'])->format('Y-m-d') : date('Y-m-d H:i:s');


                    if($params['regional_market'] == $params['area_id'] || $params['group_id'] == ADMINISTRATOR_ID 
                    || $params['group_id'] == SALES_EXT_ID 
                    || ($params['group_id'] == SALES_ADMIN_ID 
                             && in_array($params['area_id'], $list_regions))
                    ||  $params['area_id'] == 54){
                           
                        $data				= $timing->getAllTimingRm($data);
                        $this->view->timing_rm	= $data;
                    }
            }
	}
	public function checkAction(){
			
	}
}