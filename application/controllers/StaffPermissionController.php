<?php
    class StaffPermissionController extends My_Controller_Action
    {
        public function indexAction()
        {
            $QStaffPermission = new Application_Model_StaffPermission();

            $code = $this->getRequest()->getParam('code');
            $name = $this->getRequest()->getParam('name');
            $email = $this->getRequest()->getParam('email');
            $department = $this->getRequest()->getParam('department');
            $office = $this->getRequest()->getParam('office');
            $permission = $this->getRequest()->getParam('permission');
            $page = $this->getRequest()->getParam('page', 1);
			$export = $this->getRequest()->getParam('export');
            $params = array(
                "code" => $code,
                "name" => $name,
                "email" => $email,
                "department" => $department,
                "office" => $office,
                "permission" => $permission,
                'limit' => empty($export) ? 10 : NULL,
            );

            $params['offset'] = ($page-1)*$params['limit'];
			$QOffice = new Application_Model_Office();
            $all_office =  $QOffice->fetchAllOffice();
            $this->view->office = $all_office;
			$arr_office = array();
            
			foreach ($all_office as $k => $val){
                $arr_office[$val['id']] = $val['office_name'];
            }
			
            $data = $QStaffPermission->fetchPagination($params);
          
			    if(!empty($export)){
                require_once 'PHPExcel.php';
            
                $alphaExcel = new My_AlphaExcel();
            
                $PHPExcel = new PHPExcel();
                $heads = array(
                    $alphaExcel->ShowAndUp() => 'Mã nhân viên',
                    $alphaExcel->ShowAndUp() => 'Họ tên',
                    $alphaExcel->ShowAndUp() => 'Department',
                    $alphaExcel->ShowAndUp() => 'Team',
                    $alphaExcel->ShowAndUp() => 'Title',
                    $alphaExcel->ShowAndUp() => 'Office',
                    $alphaExcel->ShowAndUp() => 'Permission',
                    $alphaExcel->ShowAndUp() => 'All office',
                    $alphaExcel->ShowAndUp() => 'Approve',
                    $alphaExcel->ShowAndUp() => 'code',

                );
            
                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();
            
                foreach($heads as $key => $value)
                {
                    $sheet->setCellValue($key.'1', $value);
                }
            
                $index = 1;
                foreach($data['data'] as $key => $value)
                {
                    $alphaExcel = new My_AlphaExcel();
                    
                    $office_id = $value['office_id'];
					
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['department'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), ($value['is_all_office'] == 1) ? 'Tất cả văn phòng' : $arr_office[$office_id],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), ($value['is_manager'] == 1) ? 'MANAGER' : 'LEADER',PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), ($value['is_all_office'] == 1) ? 'X' : '',PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), ($value['is_approve'] == 1) ? 'X' : '',PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
            
                }
                $filename = 'Export Permission - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            
            
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
            
                exit;
            }
			
            $this->view->data = $data['data'];
            $this->view->params = $params;
            $this->view->total = $data['total'];
            $this->view->limit = $params['limit'];
            $this->view->offset = $params['offset'];
            $this->view->url = HOST . 'staff-permission/index' . ($params ? '?' . http_build_query($params) . '&' : '?');

            
            $QArea = new Application_Model_Area();
            $all_area =  $QArea->fetchAll(null, 'name');
            $this->view->area = $all_area;

           

            $department = $QStaffPermission->getDepartment();
            $this->view->department = $department;

            $flashMessenger = $this->_helper->flashMessenger;
            $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
            $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages_success = $messages_success;
            $this->view->messages_error   = $messages_error;
        }

    public function createAction(){
        $QStaffPermission = new Application_Model_StaffPermission();
        $QArea = new Application_Model_Area();
        $id = $this->getRequest()->getParam('id');
        $QOffice    = new Application_Model_Office();
        $QTeam      = new Application_Model_Team();
        $QRegionalMarket = new Application_Model_RegionalMarket();

        $department = $QStaffPermission->getDepartment();
        $all_office =  $QOffice->fetchAllOffice();

        $this->view->areas = $QArea->get_cache();
        $this->view->team = $QTeam->get_cache();
        $this->view->title = $QTeam->get_cache_title();
        $this->view->office = $all_office;
        $this->view->department = $department;
        
        if($id) {
            $staffPermission = $QStaffPermission->find($id)->current();
            $this->view->staffPermission = $staffPermission->toArray();
            $this->view->provinces = $QRegionalMarket->nget_province_by_area_cache($staffPermission['area_id']);
            $this->view->districts = $QRegionalMarket->get_district_index_by_province_cache($staffPermission['regional_market']);
            $this->_helper->viewRenderer->setRender('edit');
        }else{
            $this->_helper->viewRenderer->setRender('add');
        }
            
    }

        public function saveAction(){
           
            $flashMessenger = $this->_helper->flashMessenger;
            $id = $this->getRequest()->getParam('id');
            $staff_code = $this->getRequest()->getParam('staff_code');
            $area_id = $this->getRequest()->getParam('area_id',0);
            $province_id = $this->getRequest()->getParam('province_id',0);
            $district_id = $this->getRequest()->getParam('district_id',0);
            $team_id = $this->getRequest()->getParam('team_id',0);
            $title_id = $this->getRequest()->getParam('title_id',0);
            $department_id = $this->getRequest()->getParam('department_id',0);
            $is_all_office = $this->getRequest()->getParam('is_all_office',0);
            $office_id = $this->getRequest()->getParam('office_id',0);
            $is_manager = $this->getRequest()->getParam('is_manager',0);
            $is_leader = $this->getRequest()->getParam('is_leader',0);
            $is_approve = $this->getRequest()->getParam('is_approve',0);
            $code = $this->getRequest()->getParam('code',null);
            if($staff_code==$code){
                $flashMessenger->setNamespace('error')->addMessage('Staff code không được trùng với code');
                $this->_redirect("/staff-permission/");
            }
            if(empty($is_leader) && empty($is_manager)){
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn quyền Leader hoặc Manager');
                $this->_redirect("/staff-permission/");
            }
            if(empty($code) && empty($department_id)){
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn Department');
                $this->_redirect("/staff-permission/");
            }    
            $data  = array(
                'area_id'           => $area_id,
                'regional_market'   => $province_id,
                'district'          => $district_id,
                'department_id'     => $department_id,
                'team_id'      	    => $team_id,
                'title_id'          => $title_id,
                'office_id'	    => $office_id,
                'is_manager'        => $is_manager,
                'is_leader'         => $is_leader,
                'is_approve'        => $is_approve,
                'staff_code'        => $staff_code,
                'is_all_office'     => $is_all_office,
                'code'              => $code
            );
            if(!empty($data)){
                $QStaffPermission = new Application_Model_StaffPermission();
                $result = $QStaffPermission->save($data,$id);
                if($result['code'] <= 0){
                    $flashMessenger->setNamespace('error')->addMessage($result['message']);
                }else{
                    $flashMessenger->setNamespace('success')->addMessage('Đã cập nhật');
                }
                $this->_redirect("/staff-permission/");

            }else{
                $flashMessenger->setNamespace('error')->addMessage('Không có dữ liệu');
                $this->_redirect("/staff-permission/add");
            }

            $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
            $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages_success = $messages_success;
            $this->view->messages_error   = $messages_error;
        }



        public function updateAction()
        {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $flashMessenger = $this->_helper->flashMessenger;
            $id = $this->getRequest()->getParam('id');
            $is_leader = $this->getRequest()->getParam('is_leader');
            $is_manager = $this->getRequest()->getParam('is_manager');

            $params = array(
                "id" => $id,
                "is_leader" => $is_leader,
                "is_manager" => $is_manager,
            );
    
            $QStaffPermission = new Application_Model_StaffPermission();
            $where = $QStaffPermission->getAdapter()->quoteInto('id = ?', $id);
            $QStaffPermission->delete($where);
                        
//            $QStaffPermission->updateStaffPermission($params);
            
            $flashMessenger->setNamespace('success')->addMessage('Đã cập nhật');
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }

        public function ajaxSearchStaffAction()
        {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            if($this->getRequest()->isXmlHttpRequest())
            {
                $input = $this->getRequest()->getParam("input");
                $QStaffPermission = new Application_Model_StaffPermission();
                $data = $QStaffPermission->getStaff($input);
                echo json_encode($data);

                die;
            }
        }

        public function ajaxSearchStaffWorkingAction()
        {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            if($this->getRequest()->isXmlHttpRequest())
            {
                $input = $this->getRequest()->getParam("input");
                $QStaffPermission = new Application_Model_StaffPermission();
                $data = $QStaffPermission->getStaffWorking($input);
                echo json_encode($data);

                die;
            }
        }
        public function listPermissionFinalAction(){    
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'list-permission-final.php';
        }

        public function createPermissionFinalAction(){
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'create-permission-final.php';
        }

        public function addToolFinalAction(){
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'create-tool-permission-final.php';
        }

        public function savePermissionFinalAction(){
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'save-permission-final.php';
        }
		
        public function updatePermissionFinalAction()
        {
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'update-permission-final.php';
        }
		
        public function deletePermissionFinalAction()
        {
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'delete-permission-final.php';
        }
		
        public function editPermissionFinalAction()
        {
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'edit-permission-final.php';
        }
        private function exportListPermission($data_e,$dataStaff_e){
            require_once 'staff-permission' . DIRECTORY_SEPARATOR . 'export-list-permission.php';
        }
    }