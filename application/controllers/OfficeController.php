<?php
class OfficeController extends My_Controller_Action
{
	public function indexAction(){

        $export = $this->getRequest()->getParam('export');

        $office_name = $this->getRequest()->getParam('office_name');
        $office_type = $this->getRequest()->getParam('office_type');
        $area_id = $this->getRequest()->getParam('area_id');
        $province = $this->getRequest()->getParam('province');
        $district = $this->getRequest()->getParam('district');

		$page         = $this->getRequest()->getParam('page',1);
		$sort 				= $this->getRequest()->getParam('sort','created_at');
		$desc	 				= $this->getRequest()->getParam('desc',1);

		$params  = array(
				'office_name' 	=> $office_name,
				'office_type'					=> $office_type,
				'area_id'      	=> $area_id,
				'province'     	=> $province,
				'district'      => $district,
				'sort'         	=> $sort,
				'desc'         	=> $desc
			);

		$total   = 0;
		$limit   = LIMITATION;
		$QOffice = new Application_Model_Office();
		if($export){
        $list = $QOffice->fetchPagination($page, NULL, $total, $params);
        $this->export($list);
    }else{
        $list = $QOffice->fetchPagination($page, $limit, $total, $params);
    }

		//$list = $QOffice->fetchPagination($page, $limit, $total, $params);
		$this->view->list = $list;

		$this->view->sort = $sort;
		$this->view->desc = $desc;
		$this->view->params = $params;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->url = HOST . 'office' . ($params ? '?'.http_build_query($params).'&' : '?');
    $this->view->offset = $limit * ($page - 1);

		$QOfficeType 					 	 = new Application_Model_OfficeType();
		$this->view->office_types 		 	 = $QOfficeType->get_cache();

    $QArea 								 	 = new Application_Model_Area();
    $this->view->areas 		 	 = $QArea->get_cache();

		$flashMessenger             = $this->_helper->flashMessenger;
		$messages                   = $flashMessenger->setNamespace('success')->getMessages();
		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages       = $messages;
		$this->view->messages_error = $messages_error;
	}

	public function ajaxLoadAction(){
		$area_id = $this->getRequest()->getParam('area');
		$db = Zend_Registry::get('db');

		$sql = "SELECT
					o.id as `id`,
					CONCAT(a.name, ' - ', o.office_name) as `name`
				FROM `office` o
				JOIN area a ON o.area_id = a.id
				WHERE o.del = 0
				AND (o.area_id = :area_id)
				ORDER BY `name`";

		$stmt = $db->prepare($sql);
		$stmt->bindParam('area_id', $area_id, PDO::PARAM_INT);

		$stmt->execute();
		$data = $stmt->fetchAll();

		echo json_encode($data); die;
	}

	public function createAction(){

        $QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $id = $this->getRequest()->getParam('id');
        $QOfficeType = new Application_Model_OfficeType();
        $this->view->types = $QOfficeType->get_cache();

    if($id){
			$QOffice = new Application_Model_Office();
			$office = $QOffice->find($id)->current();
			$this->view->office = $office;
            $QRegionalMarket = new Application_Model_RegionalMarket();
            if($office['area_id'] == null || mb_strlen($office['area_id']) == 0) {
                $this->view->provinces = array();
            } else {
                $this->view->provinces = $QRegionalMarket->nget_province_by_area_cache($office['area_id']);
            }
            if($office['province'] == null || mb_strlen($office['province']) == 0) {
                $this->view->districts = array();
            } else {
                $this->view->districts = $QRegionalMarket->get_district_index_by_province_cache($office['province']);
            }
        }
	}

	public function saveAction(){
		$office_name     = $this->getRequest()->getParam('office_name');
		$area_id         = $this->getRequest()->getParam('area_id');
		$id              = $this->getRequest()->getParam('id');
		$type            = $this->getRequest()->getParam('office_type');
		$province 			 = $this->getRequest()->getParam('province');
        	$district 	 = $this->getRequest()->getParam('district', null);
        	$latitude        = $this->getRequest()->getParam('latitude');
        	$longitude       = $this->getRequest()->getParam('longitude');

		$flashMessenger = $this->_helper->flashMessenger;

		if(trim($office_name) == ''){
			$flashMessenger->setNamespace('error')->addMessage('offince name is require');
			$this->_redirect('/office/');
		}

		if(!intval($area_id)){
			$flashMessenger->setNamespace('error')->addMessage('area is require');
			$this->_redirect('/office/');
		}

	        if(!isset($latitude) || !is_numeric($latitude)){
	            $flashMessenger->setNamespace('error')->addMessage('Latitude is not number');
	            $this->_redirect('/office/');
	        }

	        if(!isset($longitude) || !is_numeric($longitude)){
	            $flashMessenger->setNamespace('error')->addMessage('Longitude is not number');
	            $this->_redirect('/office/');
	        }

	        $districtName = null;
			if($district != null) {
	            $QRegionalMarket = new Application_Model_RegionalMarket();
	            $districts = $QRegionalMarket->get_district_index_by_province_cache($province);
	            foreach($districts as $tmpDistrict) {
	                if($tmpDistrict['id'] == $district) {
	                    $districtName = $tmpDistrict['name'];
	                }
	            }
	        }

		$params = array(
			'office_name' 		=> $office_name,
			'area_id'     		=> $area_id,
			'office_type' 		=> $type,
			'province' 			=> $province,
			'district'          => $district,
			'district_name'     => $districtName,
			'latitude'          => $latitude,
			'longitude'         => $longitude
		);
		$QOffice = new Application_Model_Office();
		$result = $QOffice->save($params,$id);
		if($result['code'] <= 0){
			$flashMessenger->setNamespace('error')->addMessage($result['message']);
		}else{
			$flashMessenger->setNamespace('success')->addMessage($result['message']);
		}
		$this->_redirect('/office/');
	}

	public function delAction(){
		$id = $this->getRequest()->getParam('id');
		$id = intval($id);
		$flashMessenger  = $this->_helper->flashMessenger;
		if($id){
			$QOffice = new Application_Model_Office();
			$result = $QOffice->save(array('del'=>1),$id);

			if($result['code'] <= 0){
				$flashMessenger->setNamespace('error')->addMessage($result['message']);
				$this->_redirect('/office');
			}else{
				$flashMessenger->setNamespace('success')->addMessage($result['message']);
				$this->_redirect('/office');
			}
		}else{
			$flashMessenger->setNamespace('error')->addMessage('Please select item to delete');
			$this->_redirect('/office');
		}
	}

	private function export($data){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'OFFICE',
            'AREA',
            'PROVINCE',
            'DISTRICT',
            'TYPE',
            'AREA_ID',
            'PROVINCE_ID',
            'DISTRICT_ID',
            'OFFICE_ID'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        $stt = 0;
        foreach($data as $key => $value):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['office_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['province_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['district_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['type_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area_id']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['province']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['district']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['id']), PHPExcel_Cell_DataType::TYPE_STRING);

            $index++;
        endforeach;

        $filename = 'office_' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function loadOfficeAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $area_id            = $this->getRequest()->getParam('area_id');


        $params = array(
            'area_id'           => $area_id,

        );

        $db     = Zend_Registry::get('db');
        $select = $db->select()->from(array('o'=>'office'),array('o.*'));


        if($area_id){
            $select->where('o.area_id = ? ',$area_id);


        }

        $select->where('o.del = 0 ');
        $result = $db->fetchAll($select);

        echo json_encode($result);

    }

}
