<?php
class TrainingController extends My_Controller_Action
{
    
    public function init()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->staff = $userStorage;
        
        $QCat0 = new Application_Model_LessonCat();
        $whereCat0 = $QCat0->getAdapter()->quoteInto('parent_id = ?', 0);
        $cat = $QCat0->fetchAll($whereCat0,'stt');
        $catArr = array();
        /* Kt có bài viết mới ko */
        $haveNew = array();
        
        $lesson1Arr = array();
        
        $lessonArr = array();
        foreach($cat as $_key => $_value){
            $QCat1 = new Application_Model_LessonCat();
            $whereCat1[] = $QCat1->getAdapter()->quoteInto('parent_id = ?', $_value['id']);
            //$whereCat1[] = $QCat1->getAdapter()->quoteInto('parent_id = ?', $_value['id']);
            $catArr["$_key"] = $QCat1->fetchAll($whereCat1,'stt');
            
            $QLesson1 = new Application_Model_Lesson();
            $whereLesson1 = $QLesson1->getAdapter()->quoteInto('cat_id = ?', $_value['id']);
            $lesson1Arr["$_key"] = $QLesson1->fetchAll($whereLesson1,'stt');
            
            $haveNew["$_key"] = 0;
            
            foreach($catArr["$_key"] as $key=>$value){
                $QLesson = new Application_Model_Lesson();
                $whereLesson[] = $QLesson->getAdapter()->quoteInto('cat_id = ?', $value['id']);
                $whereLesson[] = $QLesson->getAdapter()->quoteInto('id_parent = 0 OR id_parent IS NULL',false);
                $lessonArr["$_key"]["$key"] = $QLesson->fetchAll($whereLesson,'stt');
                
                if($value['new'] == 1){
                    $haveNew["$_key"] = 1;
                }
            }
        }
        $this->view->cat = $cat;
        $this->view->catArr = $catArr;

        $this->view->haveNew = $haveNew;
        $this->view->lesson1Arr = $lesson1Arr;
        $this->view->lessonArr = $lessonArr;
        // var_dump($lessonArr); exit;
        ////Đếm số comment cùng
        if(!isset($userStorage->ID_number)){
            $cmnd = $userStorage->id;
        }
        else{
            $cmnd = $userStorage->ID_number;
        }
        
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'lesson_comment'),
                    array('count'=>'count(*)','lesson_title'=>'b.title')
                )
                ->where('a.published = ?', 2)
                ->where('staff_cmnd = ?', $cmnd)
                ->joinLeft(array('b' => 'lesson'), 'a.lesson_id = b.id', array())
                ->order("a.created_at DESC")
                ->group(array("staff_cmnd","lesson_id"));
        $result = count($db->fetchAll($select));
        $this->view->countComment = $result;
        
        $this->_helper->layout->setLayout('training');
    }
    
    
    public function indexAction()
    {
        $QFileBanner =  new Application_Model_LessonFile();
        $dataBanner =  $QFileBanner->fetchAll();
        
        $QNewLesson = new Application_Model_Lesson();
        $dataNew = $QNewLesson->getNewLesson();
        
        $QHotLesson = new Application_Model_Lesson();
        $dataHot = $QHotLesson->getHotLesson();
        
        $this->view->dataNew = $dataNew;
        $this->view->dataHot = $dataHot;
        $this->view->dataBanner = $dataBanner;
    }
    
    /* CATEGORIES */
    public function catAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $title = $this->getRequest()->getParam('key');
        $content = $this->getRequest()->getParam('content');
        $sort = $this->getRequest()->getParam('sort');
        $desc = $this->getRequest()->getParam('desc', 1);
        
        $limit = LIMITATION;
        $total = 0;
        
        $params = array_filter(array(
            'title' => $title,
            'content' => $content,
            'sort' => $sort,
            'desc' => $desc,
            ));
        $params['sort'] = $sort;
        $params['desc'] = $desc;
        $params['group_cat'] = 1;

                
        $this->view->params = $params;
        $this->view->sort = $sort;
        $this->view->desc = $desc;
        
        
        unset($params['group_cat']);
        $QModel = new Application_Model_LessonCat();
        $inform = $QModel->fetchPagination($page, $limit, $total, $params);
        $QInformCategory = new Application_Model_LessonCat();
        $inform_category = $QInformCategory->get_cache();
        $informs = array();
        $this->view->inform_category = $inform_category;
        $this->view->informs = $inform;
        
        
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'training/cat/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');
        $this->view->offset = $limit * ($page - 1);
        
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;  
    }
    
    public function listCatAction()
    {
        $id = $this->getRequest()->getParam('id');
        
        $QThis = new Application_Model_LessonCat();
        $whereThis = $QThis->getAdapter()->quoteInto('id = ?', $id);
        $data = $QThis->fetchRow($whereThis);
        
        $QCat = new Application_Model_LessonCat();
        $where = $QCat->getAdapter()->quoteInto('parent_id = ?', $id);
        $dataCat = $QCat->fetchAll($where,'stt');
        
        $QLesson = new Application_Model_Lesson();
        $whereLesson = $QLesson->getAdapter()->quoteInto('cat_id = ?', $id);
        $dataLesson = $QLesson->fetchAll($whereLesson,'stt');
        
        $dataLessonCat = array();
        foreach($dataCat as $key=>$value){
            $QLessonCat = new Application_Model_Lesson();
            $whereCat = $QLessonCat->getAdapter()->quoteInto('cat_id = ?', $value['id']);
            $dataLessonCat[$key] = $QLessonCat->fetchAll($whereCat);
        }
            
        $this->view->data = $data;
        $this->view->dataCat = $dataCat;
        $this->view->dataLessonCat = $dataLessonCat;
        $this->view->dataLesson = $dataLesson;
    }
    
    
    public function createCatAction()
    {
        
        $idCat = $this->getRequest()->getParam('cat');
        $id = $this->getRequest()->getParam('id');
        $stt = $this->getRequest()->getParam('stt');
        $new = $this->getRequest()->getParam('new');


        
        if($id){
            $QCategory = new Application_Model_LessonCat();
            $QCategoryWhere = $QCategory->getAdapter()->quoteInto('id = ?', $id);
            $this->view->inform = $QCategory->fetchRow($QCategoryWhere);
        }
        
        $QInformCategory = new Application_Model_LessonCat();
        $where = $QInformCategory->getAdapter()->quoteInto('id = ?', $idCat);
        $catParam = $QInformCategory->fetchRow($where);
        $this->view->catParam = $catParam;
        
        $QInformCategory2 = new Application_Model_LessonCat();
        $dataCat = $QInformCategory2->fetchAll();
        $this->view->category = $dataCat;
        
        if ($this->getRequest()->getMethod() == 'POST'){
            $title = $this->getRequest()->getParam('title');
            $cat = $this->getRequest()->getParam('cat');
            $data = array(
                'title'     => $title,
                'parent_id' => $cat, 
                'stt'       => intval($stt),
                'new'       => !is_null($new) ? intval($new) : 0,
				'published' => 1
            );
            $data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
            if($id){
                $QCat = new Application_Model_LessonCat();
                $whereUpdate = $QCat->getAdapter()->quoteInto('id = ?', $id);
                $QCat->update($data,$whereUpdate);
            }
            else{
                $QCat = new Application_Model_LessonCat();
                $QCat->insert($data);
            }
            
            if($id){
                My_Controller::redirect(HOST . 'training/create-cat?cat='.$idCat.'&id='.$id);
            }
            else{
                My_Controller::redirect(HOST . 'training/create-cat?cat='.$idCat);
            }
        }
    }
    
    public function deleteCatAction()
    {
        $id = $this->getRequest()->getParam('id');
        
        $QLesson = new Application_Model_Lesson();
        $whereLesson = $QLesson->getAdapter()->quoteInto('cat_id = ?', $id);
        $QLesson->delete($whereLesson);
        
        $QCat = new Application_Model_LessonCat();
        $whereCat = $QCat->getAdapter()->quoteInto('id = ?', $id);
        $delete = $QCat->delete($whereCat);
        
        My_Controller::redirect(HOST . 'training/cat');
    }
    
    public function categorySaveAction()
    {
        $name   = $this->getRequest()->getParam('name');
        $id     = $this->getRequest()->getParam('id');
        
        try {
            if (!$name) throw new Exception("Name cannot be blank");
        
            $name   = trim($name);
        
            $QInformCategory = new Application_Model_LessonCat();
        
            $where = array();
            $where[] = $QInformCategory->getAdapter()->quoteInto('title = ?', $name);
            $category_check = $QInformCategory->fetchRow($where);
        
            if ($category_check) throw new Exception("Name exists");
        
            $data = array(
                'title' => $name,
                );
        
            if ($id) {
                $category_check = $QInformCategory->find($id);
                $category_check = $category_check->current();
        
                if (!$category_check) throw new Exception("Invalid ID");
        
                $where = $QInformCategory->getAdapter()->quoteInto('id = ?', $id);
                $QInformCategory->update($data, $where);
            } else {
                $id = $QInformCategory->insert($data);
            }
        
            if ($id) {
                $cache = Zend_Registry::get('cache');
                $cache->remove('lesson_cat_cache');
        
                $category_cache = $QInformCategory->get_cache();
        
        
                exit(json_encode(array(
                    'type'     => 'success',
                    'message'  => 'Success',
                    'new_data' => $category_cache,
                    )));
            } else
                throw new Exception("Insert failed");
                
        } catch (Exception $e) {
            exit(json_encode(array(
                'type' => "danger",
                'message' => $e->getMessage(),
                )));
        }
    }
    
    /* LESSON */
    
    public function createAction()
    {   
        // ***
        $QLesson = new Application_Model_Lesson();
        $cate_tonghop = $QLesson->getTongHopLession();
        $this->view->cate_tonghop=$cate_tonghop;

        $id = $this->getRequest()->getParam('id');
        if ($id)
        {
            $QModel = new Application_Model_Lesson();
            $rowset = $QModel->find($id);
            $inform = $rowset->current();
        
            if (!$inform)
                $this->_redirect(HOST . 'training/create');
        
            $this->view->inform = $inform;
            
            //Lấy tất cả câu hỏi trắc nghiệm
            $QQuestion = new Application_Model_LessonQuestion();
            $where[] = $QQuestion->getAdapter()->quoteInto('lesson_id = ?', $id);
            $where[] = $QQuestion->getAdapter()->quoteInto('type = ?', 0);
            $question = $QQuestion->fetchAll($where);
            
            //Lấy tất cả câu hỏi tự luận
            $QQuestion2 = new Application_Model_LessonQuestion();
            $where2[] = $QQuestion2->getAdapter()->quoteInto('lesson_id = ?', $id);
            $where2[] = $QQuestion2->getAdapter()->quoteInto('type = ?', 1);
            $question2 = $QQuestion2->fetchAll($where2);
            
            //Lấy tất cả câu hỏi comment
            $QComment = new Application_Model_LessonComment();
            $QComment = $QComment->fetchCommentStaff($id);
            
            $answer = array();
            foreach($question as $_key => $_value){
                $QAnswer = new Application_Model_LessonAnswer();
                $whereAns = $QAnswer->getAdapter()->quoteInto('question_id = ?', $_value['id']);
                $answer["$_key"] = $QAnswer->fetchAll($whereAns);
            }
            
            //End dah mục
            $QInformTeam = new Application_Model_LessonCat();
            $where = $QInformTeam->getAdapter()->quoteInto('id = ?', $id);
            $old_objects = $QInformTeam->fetchAll($where);
        
        
            $old_team_objects = array();
            foreach ($old_objects as $_key => $_value)
            {
                $old_team_objects[] = $_value['object_id'];
            }
            
            $QCat = new Application_Model_LessonCat();
            $rowCat = $QCat->find($inform->cat_id);
            $catParam = $rowCat->current();
            $this->view->catParam = $catParam;
        
            $this->view->old_team_objects = $old_team_objects;
            $this->view->userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $this->view->question = $question;
            $this->view->question2 = $question2;
            $this->view->answer = $answer;
            $this->view->comment = $QComment;
        }
        
        
        $idCat = $this->getRequest()->getParam('cat');
        if($idCat){
            $QCat = new Application_Model_LessonCat();
            $rowCat = $QCat->find($idCat);
            $catParam = $rowCat->current();
            $this->view->catParam = $catParam;
        }
        
        $QInformCategory = new Application_Model_LessonCat();
        $this->view->category = $QInformCategory->fetchAll();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;
        
        //back url
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
        
                // ***
        
    }
    
    public function lessonDelAction()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id)
        {
            //Xóa tất cả câu hỏi, câu trả lời
            $QLessonQuestionDel = new Application_Model_LessonQuestion();
            $whereQuestionDel = $QLessonQuestionDel->getAdapter()->quoteInto('lesson_id = ?', $id);
            $delQues = $QLessonQuestionDel->fetchAll($whereQuestionDel);
            
            foreach($delQues as $key => $value){
                $QLessonAnswerDel = new Application_Model_LessonAnswer();
                $whereAnswerDel = $QLessonAnswerDel->getAdapter()->quoteInto('question_id = ?', $id);
                $QLessonAnswerDel->delete($whereAnswerDel);
            }
            $QLessonQuestionDel->delete($whereQuestionDel);
            
            $QLesson = new Application_Model_Lesson();
            $whereLessonDel = $QLesson->getAdapter()->quoteInto('id = ?', $id);
            $QLesson->delete($whereLessonDel);
        }
        My_Controller::redirect(HOST . 'training/lesson');
        
    }

    public function saveLessonAction(){
        //var_dump($_REQUEST['data_answer']);exit;
        if ($this->getRequest()->getMethod() == 'POST')
        {
            $id = $this->getRequest()->getParam('id');
            $stt = is_null($this->getRequest()->getParam('stt')) ? 0 : $this->getRequest()->getParam('stt');
            $QModel = new Application_Model_Lesson();
            $title = $this->getRequest()->getParam('title');
            $cat = $this->getRequest()->getParam('cat');
            $youtube = $this->getRequest()->getParam('youtube');
			$youtube2 = $this->getRequest()->getParam('youtube2');
            $youtube3 = $this->getRequest()->getParam('youtube3');
            $youtube4 = $this->getRequest()->getParam('youtube4');
            $slide = $this->getRequest()->getParam('slide');
            $photo = $this->getRequest()->getParam('file_photo');
            $content = $this->getRequest()->getParam('content');
            $question = $this->getRequest()->getParam('question', array());
            $question_choice = $this->getRequest()->getParam('question_choice', array());
            $scores = $this->getRequest()->getParam('scores', array());
            $data_answer = $this->getRequest()->getParam('data_answer', array());
            $data_check = $this->getRequest()->getParam('data_check', array());
            $commentList = $this->getRequest()->getParam('comment_list', array());
            $from_test = $this->getRequest()->getParam('from_test');
            $to_test = $this->getRequest()->getParam('to_test');

            //File
            $key = $this->getRequest()->getParam('key');
            $file_id = $this->getRequest()->getParam('file_id');
            $hash = $this->getRequest()->getParam('hash');
            $nfile_id = $this->getRequest()->getParam('nfile_id');
            

            $comment = is_null($this->getRequest()->getParam('comment')) ? 0 : $this->getRequest()->getParam('comment');
            $hot = is_null($this->getRequest()->getParam('hot')) ? 0 : $this->getRequest()->getParam('hot');
            $new = is_null($this->getRequest()->getParam('new')) ? 0 : $this->getRequest()->getParam('new');
            // ***
            $total_test = is_null($this->getRequest()->getParam('total_test')) ? 0 : $this->getRequest()->getParam('total_test');

            $id_parent = $this->getRequest()->getParam('id_parent');
           if(!empty($id_parent)){
                $tonghop=null;
           }
            $type_test = $this->getRequest()->getParam('type_test');
            // lưu nội dung
            $data = array(
                'title'     => $title,
                'stt'       => $stt,
                'slide'     => $file_id,
                'cat_id'    =>$cat,
                'youtube'   => $youtube,
				'youtube2'   => $youtube2,
                'youtube3'   => $youtube3,
                'youtube4'   => $youtube4,
                'slide'     => $slide,
                'content'   => $content,
                'comment'   => $comment,
                'question'  => ' ',
                'hot'       =>$hot,
                'new'       =>$new,
                'from_test' => $from_test,
                'to_test'   => $to_test,
                'published' => 1,
                // ***
                'total_test'   =>$total_test,
                'type_test'  =>$type_test,
                'id_parent'    =>$id_parent
                );
            //var_dump($data); exit;
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $data['staff_id'] = $userStorage->id;
            
            if($id){
                $data['updated_at'] = date('Y-m-d H:i:s');
                $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
                $QModel->update($data, $where);
                //add file
                $QFileLog = new Application_Model_FileUploadLog();
                $QInformFile = new Application_Model_LessonFile();
                
                if (isset($file_id) && is_array($file_id))
                {
                    foreach ($file_id as $_key => $_value)
                    {
                        if (!isset($hash[$_key]) || !$hash[$_key] || !isset($key[$_key]) || !$key[$_key])
                            throw new Exception("Invalid key");
        
                        $log = $QFileLog->find($_value);
                        $log = $log->current();
        
                        if (!$log)
                            throw new Exception("Invalid file uploaded");
                        $hash_check = hash("sha256", $log['folder'] . $key[$_key] . $log['filename'] .
                            'lesson' . $log['id']);
        
                        if ($hash[$_key] != $hash_check)
                            throw new Exception("Invalid file");
                     
                        $data = array(
                            'lesson_id' => $id,
                            'file_display_name' => $log['real_file_name'],
                            'name' => $log['filename'],
                            'unique_folder' => $log['folder'],
                            );
        
                        if (isset($nfile_id[$_key]) && $nfile_id[$_key])
                        {
                            $where = $QInformFile->getAdapter()->quoteInto('id = ?', $nfile_id[$_key]);
                            $QInformFile->update($data, $where);
        
                        } else
                        {
                            $QInformFile->insert($data);
                        }
                    } 
                }
                
                //Xóa tất cả câu hỏi, câu trả lời, comment
                $QLessonCommentDel = new Application_Model_LessonComment();
                $whereCommentDel[] = $QLessonCommentDel->getAdapter()->quoteInto('lesson_id = ?', $id);
                foreach($commentList as $key => $value){
                    $whereCommentDel[] = $QLessonCommentDel->getAdapter()->quoteInto("id NOT IN (?)", (int)$value);
                }
                //$delComment = $QLessonCommentDel->delete($whereCommentDel);
                
                
                $QLessonQuestionDel = new Application_Model_LessonQuestion();
                $whereQuestionDel = $QLessonQuestionDel->getAdapter()->quoteInto('lesson_id = ?', $id);
                $delQues = $QLessonQuestionDel->fetchAll($whereQuestionDel);
                
                foreach($delQues as $key => $value){
                    $QLessonAnswerDel = new Application_Model_LessonAnswer();
                    $whereAnswerDel = $QLessonAnswerDel->getAdapter()->quoteInto('question_id = ?', $id);
                    $QLessonAnswerDel->delete($whereAnswerDel);
                }
                $QLessonQuestionDel->delete($whereQuestionDel);
                
                
                
                //save câu hỏi trắc nghiệm
                $QLessonQuestion = new Application_Model_LessonQuestion();
                $QLessonAnswer = new Application_Model_LessonAnswer();
                foreach($question_choice as $_key=>$_value){
                    if(trim($_value) != ''){
                        $data = array(
                            'content' => $_value,
                            'scores' => $scores[$_key],
                            'lesson_id' =>$id,
                            'type' => 0,
                            'published' => 1
                        );
                        $idQuesstion = $QLessonQuestion->insert($data);
                        
                        $dataCheck = json_decode($data_check[$_key],true);
                        foreach(json_decode($data_answer[$_key],true) as $key=>$value){
                            $true = is_null($dataCheck[$key]) ? 0 : $dataCheck[$key];
                            if(trim($value) != ''){
                                $dataAnswer = array(
                                    'title' => $value,
                                    'question_id' =>$idQuesstion,
                                    'true' => $true
                                );
                                $QLessonAnswer->insert($dataAnswer);
                            }
                        }
                    }
    
                }
                //save câu hỏi tự luận
                $QLessonQuestion2 = new Application_Model_LessonQuestion();
                foreach($question as $_key=>$_value){
                    if(trim($_value) != ''){
                        $data = array(
                            'content' => $_value,
                            'scores' => 0,
                            'lesson_id' =>$id,
                            'type' => 1,
                            'published' => 1
                        );
                        $QLessonQuestion2->insert($data);
                    }
                }
                
            }
            else{
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $idLesson = $QModel->insert($data);
                
                
                //add file
                $QFileLog = new Application_Model_FileUploadLog();
                $QInformFile = new Application_Model_LessonFile();
                
                if (isset($file_id) && is_array($file_id))
                {
                    foreach ($file_id as $_key => $_value)
                    {
                        if (!isset($hash[$_key]) || !$hash[$_key] || !isset($key[$_key]) || !$key[$_key])
                            throw new Exception("Invalid key");
        
                        $log = $QFileLog->find($_value);
                        $log = $log->current();
        
                        if (!$log)
                            throw new Exception("Invalid file uploaded");
                        $hash_check = hash("sha256", $log['folder'] . $key[$_key] . $log['filename'] .
                            'lesson' . $log['id']);
        
                        if ($hash[$_key] != $hash_check)
                            throw new Exception("Invalid file");
                     
                        $data = array(
                            'lesson_id' => $idLesson,
                            'file_display_name' => $log['real_file_name'],
                            'name' => $log['filename'],
                            'unique_folder' => $log['folder'],
                            );
        
                        if (isset($nfile_id[$_key]) && $nfile_id[$_key])
                        {
                            $where = $QInformFile->getAdapter()->quoteInto('id = ?', $nfile_id[$_key]);
                            $QInformFile->update($data, $where);
        
                        } else
                        {
                            $QInformFile->insert($data);
                        }
                    } 
                }
                
                //add câu hỏi trắc nghiệm
                $QLessonQuestion = new Application_Model_LessonQuestion();
                $QLessonAnswer = new Application_Model_LessonAnswer();
                foreach($question_choice as $_key=>$_value){
                    if(trim($_value) != ''){
                        $data = array(
                            'content' => $_value,
                            'scores' => $scores[$_key],
                            'lesson_id' =>$idLesson,
                            'type' => 0,
                            'published' => 1
                        );
                        $idQuesstion = $QLessonQuestion->insert($data);
                        
                        $dataCheck = json_decode($data_check[$_key],true);
                        foreach(json_decode($data_answer[$_key],true) as $key=>$value){
                            $true = is_null($dataCheck[$key]) ? 0 : $dataCheck[$key];
                            if(trim($value) != ''){
                                $dataAnswer = array(
                                    'title' => $value,
                                    'question_id' =>$idQuesstion,
                                    'true' => $true
                                );
                                $QLessonAnswer->insert($dataAnswer);
                            }
                        }
                    }
    
                }
                //add câu hỏi tự luận
                $QLessonQuestion2 = new Application_Model_LessonQuestion();
                foreach($question as $_key=>$_value){
                    if(trim($_value) != ''){
                        $data = array(
                            'content' => $_value,
                            'scores' => 0,
                            'lesson_id' =>$idLesson,
                            'type' => 1,
                            'published' => 1
                        );
                        $QLessonQuestion2->insert($data);
                    }
                }
                //END File
            }
            
            // ------------------ upload----------------------------------------------------------
            if($id){
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'lesson' . DIRECTORY_SEPARATOR . $id;
                $arrPhoto = array(
                    'file_photo' => $uploaded_dir,
                    'file_slide' => $uploaded_dir.DIRECTORY_SEPARATOR.'slide'
                );   
            }
            else{
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'lesson' . DIRECTORY_SEPARATOR . $idLesson;
                $arrPhoto = array(
                    'file_photo' => $uploaded_dir,
                    'file_slide' => $uploaded_dir.DIRECTORY_SEPARATOR.'slide'
                );
            }
            
            
            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile'=>true));

            //check function
            if (function_exists('finfo_file'))
                $upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif' ,'application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/pdf'));

            $upload->addValidator('Extension', false, 'jpg,jpeg,png,gif,ppt,pptx,pdf');
            $upload->addValidator('Size', false, array('max' => '20MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');
            $files = $upload->getFileInfo();
            $hasPhoto = false;
            $data_file = array();
            
            foreach ($arrPhoto as $key=>$val){
                $del = 'del_'.$key;
                if (isset($$del) and $$del){
                    $data[$key] = null;

                    @unlink($val . $s[$key]);
                }

                if (isset($files[$key]['name'])){
                    $hasPhoto = true;
                }
            }

            if ($hasPhoto) {

                if (!$upload->isValid()){
                    $errors = $upload->getErrors();

                    $sError = null;

                    if ($errors and isset($errors[0]))
                        switch ($errors[0]){
                            case 'fileUploadErrorIniSize':
                                $sError = 'File size is too large';
                                break;
                            case 'fileMimeTypeFalse':
                            case 'fileExtensionFalse':
                                $sError = 'The file(s) you selected weren\'t the type we were expecting';
                                break;
                            default:
                                $sError = 'The file(s) you selected weren\'t the type we were expecting';
                                break;
                        }

                    throw new Exception($sError);
                }


                foreach ($arrPhoto as $key => $val){
                    $fileInfo = (isset($files[$key]) and $files[$key]) ? $files[$key] : null;
                    if (isset($fileInfo['name']) and $fileInfo['name']) {

                        if (!is_dir($val))
                            @mkdir($val, 0777, true);

                        $upload->setDestination($val);

                        $old_name = $fileInfo['name'];

                        $tExplode = explode('.', $old_name);
                        $extension = end($tExplode);

                        $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

                        $upload->addFilter('Rename', array('target' => $val .
                            DIRECTORY_SEPARATOR . $new_name));

                        $r = $upload->receive(array($key));

                        if ($r)
                            $data_file[$key] = $new_name;
                        else{
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg)
                                throw new Exception($msg);
                        }
                    }
                }
            }
            if ($data_file) {
                $QLess = new Application_Model_Lesson();
                if($id){
                    $whereStaff = $QLess->getAdapter()->quoteInto('id = ?', $id);
                    $QLess->update($data_file, $whereStaff);
                }
                else{
                    $whereStaff = $QLess->getAdapter()->quoteInto('id = ?', $idLesson);
                    $QLess->update($data_file, $whereStaff);
                }
            }
            
            
            if($id){
                My_Controller::redirect(HOST . 'training/create?id='.$id);
            }
            else{
                My_Controller::redirect(HOST . 'training/create?id='.$idLesson);
            }

            
        }
    }
    
    
    public function lessonAction()
    {       
        $page = $this->getRequest()->getParam('page', 1);
        $title = $this->getRequest()->getParam('key');
        $content = $this->getRequest()->getParam('content');
        $sort = $this->getRequest()->getParam('sort');
        $desc = $this->getRequest()->getParam('desc', 1);
        
        $limit = LIMITATION;
        $total = 0;
        
        $params = array_filter(array(
            'title' => $title,
            'content' => $content,
            'sort' => $sort,
            'desc' => $desc,
            ));
        $params['sort'] = $sort;
        $params['desc'] = $desc;
        $params['group_cat'] = 1;

                
        $this->view->params = $params;
        $this->view->sort = $sort;
        $this->view->desc = $desc;
        
        
        unset($params['group_cat']);
        $QModel = new Application_Model_Lesson();
        $inform = $QModel->fetchPagination($page, $limit, $total, $params);
        $QInformCategory = new Application_Model_LessonCat();
        $this->view->informs = $inform;
        
        
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'training/lesson/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');
        $this->view->offset = $limit * ($page - 1);
        
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;        
    }
    

    public function slideLessonAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $save_folder   = 'lesson';
        $new_file_path = '';
        $requirement   = array(
                        'Size'      => array('min' => 50, 'max' => 5000000),
                        'Count'     => array('min' => 1, 'max' => 1),
                        'Extension' => array('jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'zip', 'rar'),
                    );
        
        $file = My_File::get($save_folder, $requirement);
        $result = array();
        
        if ($file && count($file)) {
            $result['success']           = true;
            $result['file_id']           = $file['log_id'];
            $result['keypass']           = uniqid();    
            $result['name']              = $file['real_file_name'];
            $result['hashstr']           = hash("sha256", $file['folder'] . $result['keypass'] . $file['filename'] . $save_folder .$file['log_id']);
        } else
            $result['success'] = false;
        
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        
        exit;
    }
    
    public function paginationCommentAction()
    {
        
        $lesson_id  = $this->getRequest()->getParam('lesson_id');
        $page       = $this->getRequest()->getParam('page');
        
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('c' => 'lesson_comment'),
                    array('s.id as id','content','s.firstname','s.lastname','c.created_at as created_at','d.firstname as firstname2','d.lastname as lastname2','c.id as id_commment')
                )
                ->where('c.lesson_id = ?', $lesson_id)
                ->joinLeft(array('s' => 'staff_training'), 'c.staff_cmnd = s.cmnd', array())
                ->joinLeft(array('d' => 'staff'), 'c.staff_cmnd = d.ID_number', array())
                ->order("created_at DESC")
                ->limit(10,$page);
        $result = $db->fetchAll($select);
        $str = "";
        if($result){
            foreach($result as $key=>$value){
                if($value['firstname']):$name = $value['firstname'].".".$value['lastname'];else:$name = $value['firstname2'].".".$value['lastname2'];endif;
                $str .= "<div class='comment'>
                            <div class='left_comment col-sm-2'>
                                <span class='glyphicon glyphicon-user' aria-hidden='true'><p>$name</p></span>
                                <span class='day_comment'>".date("d/m/Y H:i:s",strtotime($value['created_at']))."</span>
                            </div>
                            <div class='right_comment col-sm-10'>".$value['content']."<input type='hidden' name='comment_list[]' value=''></div>
                        </div>";
            }
        }
        echo $str;
        exit;
    }
    
    
    public function showBoxCommentAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if(!isset($userStorage->ID_number)){
            $cmnd = $userStorage->id;
        }
        else{
            $cmnd = $userStorage->ID_number;
        }
        
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'lesson_comment'),
                    array('a.*','lesson_title'=>'b.title')
                )
                ->where('a.published = ?', 2)
                ->where('staff_cmnd = ?', $cmnd)
                ->joinLeft(array('b' => 'lesson'), 'a.lesson_id = b.id', array())
                ->order("a.created_at DESC")
                ->group(array("staff_cmnd","lesson_id"));
        $result = $db->fetchAll($select);
        $str = " ";
        if($result){
            foreach($result as $key=>$value){                
                $str .= "<a href='".HOST."/training/lesson-details?id=".$value['lesson_id']."'>Vừa có người bình luận cùng bạn trong bài viết <b style='color:red'> ".$value['lesson_title']."</b></a>";
            }
        }
        echo $str;
        exit;
    }
    
    public function lessonDetailsAction()
    {   
        
        $id = $this->getRequest()->getParam('id');

        // *** Lấy 3 loại test tổng hợp
        /*
        $QModel = new Application_Model_Lesson();
        $displayButton = $QModel->getInforDisplay($id);
        $this->view->displayButton=$displayButton;
        */
       // var_dump($displayButton[0]['total']);

        if ($id)
        {
            //Đã đọc comment (show box)
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if(!isset($userStorage->ID_number)){
                $cmnd = $userStorage->id;
            }
            else{
                $cmnd = $userStorage->ID_number;
            }
            $QComment_cm = new Application_Model_LessonComment();
            $where_cm = array();
            $where_cm[] = $QComment_cm->getAdapter()->quoteInto('lesson_id = (?)',$id);
            $where_cm[] = $QComment_cm->getAdapter()->quoteInto('staff_cmnd = (?)',$cmnd);
            $data2['published'] = 1;
            $QComment_cm->update($data2,$where_cm);
            //End
            
            //Đếm số comment cùng
            if(!isset($userStorage->ID_number)){
                $cmnd = $userStorage->id;
            }
            else{
                $cmnd = $userStorage->ID_number;
            }
            
            $db = Zend_Registry::get('db');
            $select = $db->select();
            $select->from(array('a' => 'lesson_comment'),
                        array('count'=>'count(*)','lesson_title'=>'b.title')
                    )
                    ->where('a.published = ?', 2)
                    ->where('staff_cmnd = ?', $cmnd)
                    ->joinLeft(array('b' => 'lesson'), 'a.lesson_id = b.id', array())
                    ->order("a.created_at DESC")
                    ->group(array("staff_cmnd","lesson_id"));
            $result = count($db->fetchAll($select));
            $this->view->countComment = $result;
            //END
            
            $QModel = new Application_Model_Lesson();
            $rowset = $QModel->find($id);
            $inform = $rowset->current();
        
            if (!$inform)
                $this->_redirect(HOST . 'training/create');
        
            $this->view->inform = $inform;
            
            //Lấy tất cả câu hỏi trắc nghiệm
            $QQuestion = new Application_Model_LessonQuestion();
            $where[] = $QQuestion->getAdapter()->quoteInto('lesson_id = ?', $id);
            $where[] = $QQuestion->getAdapter()->quoteInto('type = ?', 0);
            $question = $QQuestion->fetchAll($where);

            // ***Lấy trắc nghiệm TƯỞNG
            $params['lession_online']=1;
            $params['lession_offline']=2;
            $params['test_online']=3;

            $ques1 = $QModel->getQuestionOnline($id);
            $ques2 = $QModel->getQuestionOffline($id);
            $ques3 = $QModel->getQuestionTest($id);
            $answ1= array();
             foreach($ques1 as $_key => $_value){
                $QAnswer = new Application_Model_LessonAnswer();
                $whereAns = $QAnswer->getAdapter()->quoteInto('question_id = ?', $_value['id_question']);
                $answ1["$_key"] = $QAnswer->fetchAll($whereAns);
            }
            $answ2= array();
             foreach($ques2 as $_key => $_value){
                $QAnswer = new Application_Model_LessonAnswer();
                $whereAns = $QAnswer->getAdapter()->quoteInto('question_id = ?', $_value['id_question']);
                $answ2["$_key"] = $QAnswer->fetchAll($whereAns);
            }
             $answ3= array();
             foreach($ques3 as $_key => $_value){
                $QAnswer = new Application_Model_LessonAnswer();
                $whereAns = $QAnswer->getAdapter()->quoteInto('question_id = ?', $_value['id_question']);
                $answ3["$_key"] = $QAnswer->fetchAll($whereAns);
            }
             $this->view->ques1 = $ques1;
             $this->view->ques2 = $ques2;
             $this->view->ques3 = $ques3;
             $this->view->answ1 = $answ1;
             $this->view->answ2 = $answ2;
             $this->view->answ3 = $answ3;

             // lấy tất cả tự luận TƯỞNG

            $write1 = $QModel->getQuestionOnline_tuluan($id);
            $write2 = $QModel->getQuestionOffline_tuluan($id);
            $write3 = $QModel->getQuestionTest_tuluan($id);
            $this->view->write1 = $write1;
            $this->view->write2 = $write2;
            $this->view->write3 = $write3;

            //Lấy tất cả câu hỏi tự luận
            $QQuestion2 = new Application_Model_LessonQuestion();
            $where2[] = $QQuestion2->getAdapter()->quoteInto('lesson_id = ?', $id);
            $where2[] = $QQuestion2->getAdapter()->quoteInto('type = ?', 1);
            $question2 = $QQuestion2->fetchAll($where2);
            
     
            $answer = array();
            foreach($question as $_key => $_value){
                $QAnswer = new Application_Model_LessonAnswer();
                $whereAns = $QAnswer->getAdapter()->quoteInto('question_id = ?', $_value['id']);
                $answer["$_key"] = $QAnswer->fetchAll($whereAns);
            }

            //Lấy tất cả câu hỏi comment
            $QComment = new Application_Model_LessonComment();
            $wCm = $QComment->getAdapter()->quoteInto('lesson_id = ?', $id);
            $QCount_Comment = $QComment->fetchAll($wCm);
            $QComment = $QComment->fetchCommentStaff($id);
            
            //End dah mục
            $QInformTeam = new Application_Model_LessonCat();
            $where = $QInformTeam->getAdapter()->quoteInto('id = ?', $id);
            $old_objects = $QInformTeam->fetchAll($where);
        
        
            $old_team_objects = array();
            foreach ($old_objects as $_key => $_value)
            {
                $old_team_objects[] = $_value['object_id'];
            }
            
        
            $this->view->old_team_objects = $old_team_objects;
            $this->view->userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $this->view->question = $question->toArray();

            $this->view->question2 = $question2;
            $this->view->answer = $answer;

            $this->view->comment = $QComment;
            $this->view->count_comment = $QCount_Comment;
        }
        
        
        $idCat = $this->getRequest()->getParam('cat');
        if($idCat){
            $QCat = new Application_Model_LessonCat();
            $rowCat = $QCat->find($idCat);
            $catParam = $rowCat->current();
            $this->view->catParam = $catParam;
        }
        
        $QInformCategory = new Application_Model_LessonCat();
        $this->view->category_string_cache = $QInformCategory->get_cache();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;
        
        //back url
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER'); 
    }
    
    /* SCORES */
    public function scoresAction(){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if(isset($userStorage->cmnd)){
            $cmnd = $userStorage->cmnd;
            $QTrainer = new Application_Model_StaffTraining();
            $where = $QTrainer->getAdapter()->quoteInto('cmnd = (?)',$cmnd);
            $dataTrainer = $QTrainer->fetchRow($where);
            
            $QScore = new Application_Model_LessonScores();
            $dataScore = $QScore->getScoresDetail($cmnd);
        }
        else{
            $cmnd = $userStorage->code;
            $QTrainer = new Application_Model_Staff();
            $where = $QTrainer->getAdapter()->quoteInto('code = (?)',$cmnd);
            $dataTrainer = $QTrainer->fetchRow($where);
            
            $QScore = new Application_Model_LessonScores();
            $dataScore = $QScore->getScoresDetail2($dataTrainer->ID_number);
        }
        
        
        $dataAVG = 0;
        $count = 0;
        if(count($dataScore)>0){
            foreach($dataScore as $key =>$value){
                $dataAVG += $value['avg'];
                $count++;
            }
            $this->view->avg = $dataAVG/$count;
            $this->view->count = $count;
        }
        else{
            $this->view->avg = 0;
            $this->view->count = 0;
        }
        
        $this->view->data = $dataScore;
        $this->view->dataTrainer = $dataTrainer;
    }
    
    public function deleteStaffAction(){
        $id = $this->getRequest()->getParam('id_staff');
        $QTrainer = new Application_Model_StaffTraining();
		$where = array();
		$where[] = $QTrainer->getAdapter()->quoteInto('training_online = (?)',1);
        $where[] = $QTrainer->getAdapter()->quoteInto('id = (?)',$id);
        $delete = $QTrainer->update(array('del'=>1),$where);
        if($delete){
            echo "success";
            exit;
        }
    }
    
    public function saveScoresAction(){
        if ($this->getRequest()->getMethod() == 'POST')
        {
            $sumScore = 0;
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if(isset($userStorage->cmnd)){
                $idStaff = $userStorage->cmnd;
            }
            else{
                $idStaff = $userStorage->ID_number;
            }
            $type_lesson = $this->getRequest()->getParam('type_lesson');
            $idLesson = $this->getRequest()->getParam('id');
            $data_id_question = json_decode($this->getRequest()->getParam('data_id'));
            $data_scores_question = json_decode($this->getRequest()->getParam('data_scores'));
            $data_answer_question = json_decode($this->getRequest()->getParam('data_answer'));
            
            $question2 = $this->getRequest()->getParam('question2',array());
            $question2_id = $this->getRequest()->getParam('question2_id',array());
            //Add table lesson_scores
            foreach($question2 as $key => $value){
                $QLessonScoresDetail2 = new Application_Model_LessonScoresDetail();
                $dataScoresDetail2 = array(
                    'staff_cmnd' => $idStaff,
                    'question_id' => $question2_id[$key],
                    'answer_ques' => $value,
                );
                $QLessonScoresDetail2->insert($dataScoresDetail2);
                
            }
            
            //Add table lesson_scores_detail
            foreach($data_scores_question as $key => $value){
                $sumScore+=$value;
                $QLessonScoresDetail = new Application_Model_LessonScoresDetail();
                $dataScoresDetail = array(
                    'staff_cmnd' => $idStaff,
                    'question_id' => $data_id_question[$key],
                    'answer_id' => $data_answer_question[$key]
                );
                $QLessonScoresDetail->insert($dataScoresDetail);
            }
            //Add table lesson_scores
            $QLessonScores = new Application_Model_LessonScores();
            $dataScores = array(
                'type'  =>$type_lesson,
                'lesson_id' => $idLesson,
                'staff_cmnd' => $idStaff,
                'scores' => $sumScore
            );
            $dataScores['created_at'] = date('Y-m-d H:i:s');
            $QLessonScores->insert($dataScores);
            
            //Xóa bài làm cũ(chỉ lấy 3 bài làm gần nhất)
            $db = Zend_Registry::get('db');
            $select_limit3 = $db->select()
            ->from(array('a'=>'lesson_scores'),array('a.*'))
            ->where('a.lesson_id = ?',$idLesson)
            ->where('a.staff_cmnd = ?',$idStaff)
            ->order('a.created_at DESC')
            ->limit('3');
            $data_limit3 = $db->fetchAll($select_limit3);
            if($data_limit3['2']){
                $QLessonScores_del = new Application_Model_LessonScores();
                $where_del[] = $QLessonScores_del->getAdapter()->quoteInto('created_at < (?)',$data_limit3['2']['created_at']);
                $where_del[] = $QLessonScores_del->getAdapter()->quoteInto('lesson_id = (?)',$idLesson);
                $where_del[] = $QLessonScores_del->getAdapter()->quoteInto('staff_cmnd = (?)',$idStaff);
                //$QLessonScores_del->delete($where_del);
            }
            $this->_redirect(HOST . 'training/lesson-details?id='.$idLesson);
        }
    }
    
    
    //SAVE COMMENT
    public function saveCommentAction(){
        if ($this->getRequest()->getMethod() == 'POST'){
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if(!isset($userStorage->ID_number)){
                $cmnd = $userStorage->id;
            }
            else{
                $cmnd = $userStorage->ID_number;
            }
            
            $id_lesson = $this->getRequest()->getParam('id_lesson');
            $comment_box = $this->getRequest()->getParam('comment_box');
            if(trim($comment_box) != ''){
                $QLessonComment = new Application_Model_LessonComment();
                $data = array(
                    'content' => $comment_box,
                    'staff_cmnd' => $cmnd,
                    'lesson_id' => $id_lesson,
                    'published' => 1
                );
                $data['created_at'] = date('Y-m-d H:i:s');
                $QLessonComment->insert($data);
            }
            
            $QComment = new Application_Model_LessonComment();
            $where = $QComment->getAdapter()->quoteInto('lesson_id = (?)',$id_lesson);
            $data2['published'] = 2;
            $QComment->update($data2,$where);
            
            $this->_redirect(HOST . 'training/lesson-details?id='.$id_lesson);
        }
    }
    
    //TRAINER
    public function trainerAction(){
        $page        = $this->getRequest()->getParam('page', 1);
        $team        = $this->getRequest()->getParam('team');
        $title       = $this->getRequest()->getParam('title');
        $region      = $this->getRequest()->getParam('region');
        $dtb         = $this->getRequest()->getParam('dtb');
        $content     = $this->getRequest()->getParam('content');
        $sort        = $this->getRequest()->getParam('sort');
        $desc        = $this->getRequest()->getParam('desc', 1);
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $QTeam = new Application_Model_Team();
        $this->view->team = $QTeam->get_cache_team();

        $limit = LIMITATION;
        $total = 0;
        $params = array_filter(array(
            'title'     => $title,
            'region'    => $region,
            'dtb'       => $dtb,
            'content'   => $content,
            'sort'      => $sort,
            'desc'      => $desc,
            'team'      => $team
            ));
        $params['sort'] = $sort;
        $params['desc'] = $desc;
        
        //Phân quyền view kết quả
        if(intval($userStorage->group_id) == intval(ASM_ID) || intval($userStorage->group_id) == intval(TRAINING_LEADER_ID) || intval($userStorage->group_id) == intval(TRAINING_TEAM_ID) || intval($userStorage->group_id) == intval(ASMSTANDBY_ID)){
            $db = Zend_Registry::get('db');
            $selectRegion = $db->select()
            ->from(array('a'=>'area'),array('region'=>'GROUP_CONCAT(d.id)'))
            ->joinLeft(array('b'=>'asm'),'a.id = b.area_id',array())
            ->joinLeft(array('c'=>'staff'),'b.staff_id = c.id',array())
            ->joinLeft(array('d'=>'regional_market'),'a.id = d.area_id',array())
            ->where('b.staff_id = ?',$userStorage->id)
            ->group('b.staff_id');
            
            $list = $db->fetchRow($selectRegion);
            $params['region_access'] = $list['region'];
        }
        
        if(intval($userStorage->group_id) == intval(LEADER_ID)){
            $time = date('Y-m-d H:i:s');
            $db = Zend_Registry::get('db');
            $sql = "SELECT GROUP_CONCAT(a.staff_id) as staff_id 
                    FROM store_staff_log as a
                    WHERE store_id IN (
                    	SELECT store_id
                    	FROM store_leader_log as a
                    	WHERE staff_id = '".$userStorage->id."'
                    	AND FROM_UNIXTIME(joined_at, '%Y-%m-%d') <= '".$time."'
                    	AND released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, '%Y-%m-%d') > '".$time."'
                    	)
                    AND FROM_UNIXTIME(joined_at, '%Y-%m-%d') <= '".$time."'
                    AND released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, '%Y-%m-%d') > '".$time."'
                    AND is_leader = 0";
    
            $stmt = $db->query($sql);
            $result = $stmt->fetch();
            $params['sale_leader'] = $result['staff_id'];
        }
        


        if(intval($userStorage->group_id) == intval(SALES_ID)){
            $time = date('Y-m-d H:i:s');
            $db = Zend_Registry::get('db');
            $sql = "SELECT GROUP_CONCAT(a.staff_id) as staff_id 
                    FROM store_staff_log as a
                    WHERE store_id IN (
                    	SELECT store_id
                    	FROM store_staff_log as a
                    	WHERE staff_id = '".$userStorage->id."'
                    	AND FROM_UNIXTIME(joined_at, '%Y-%m-%d') <= '".$time."'
                    	AND released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, '%Y-%m-%d') > '".$time."'
                    	)
                    AND FROM_UNIXTIME(joined_at, '%Y-%m-%d') <= '".$time."'
                    AND released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, '%Y-%m-%d') > '".$time."'
                    AND is_leader = 0";
    
            $stmt = $db->query($sql);
            $result = $stmt->fetch();
            $params['sale_leader'] = $result['staff_id'];
        }
		
		//Phân quyền cho tienhung.nguyen(Module SALE) và Leader Trainer xem được các bài test của các Trainer và ASM
        if(in_array($userStorage->group_id,array(MODULE_SALE,TRAINING_LEADER_ID,ADMINISTRATOR_ID))){
            $params['title_staff'] = array(SALES_TITLE,PGPB_TITLE,PGPB_2_TITLE,SALE_SALE_ASM,SALE_SALE_ASM_STANDBY,TRAINER_TITLE_ID);
        }
        else{
            $params['title_staff'] = array(SALES_TITLE,PGPB_TITLE,PGPB_2_TITLE, 375);
        }
        //end
        
		if(in_array($userStorage->id, array(3028, 4266))){
			$params['title_staff']	 = NULL;
			$params['region_access'] = NULL;
		}
          
        $this->view->params = $params;
        $this->view->sort   = $sort;
        $this->view->desc   = $desc;
        
        $QModel = new Application_Model_StaffTraining();
        $data = $QModel->fetchPagination_training2($page, $limit, $total, $params);    
        $this->view->data  = $data;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST.'training/trainer'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit * ($page - 1);
        
        $QRegions = new Application_Model_RegionalMarket();
        $where_r = $QRegions->getAdapter()->quoteInto('parent = (?)',0);
        $this->view->region = $QRegions->fetchAll($where_r);

    }   
    
    public function trainerDetailAction(){
        $cmnd = $this->getRequest()->getParam('cmnd');
        
        $QTrainer = new Application_Model_StaffTraining();
        $dataTrainer = $QTrainer->getStaffByCmnd($cmnd);
        
        if($dataTrainer){ 
            $QScore = new Application_Model_LessonScores();
            $dataScore = $QScore->getScoresDetail3($cmnd);
        }
        else{
            $QTrainer2 = new Application_Model_Staff();
            $where2 = $QTrainer->getAdapter()->quoteInto('ID_number = (?)',$cmnd);
            $dataTrainer = $QTrainer2->fetchRow($where2);
            
            $db = Zend_Registry::get('db');
            $select = $db->select()
            ->from(array('a'=>'staff'),array('id'=>'a.id','firstname'=>'a.firstname','lastname'=>'a.lastname','name'=>'d.name','created_at'=>'a.joined_at'))
            ->joinLeft(array('d'=>'regional_market'),'a.regional_market = d.id',array())
            ->where('a.ID_number = ?',$cmnd);
            
            $dataTrainer = $db->fetchRow($select);
            $QScore = new Application_Model_LessonScores();
            $dataScore = $QScore->getScoresDetail2($cmnd);
        }
        $this->view->data = $dataScore;
        $this->view->dataTrainer = $dataTrainer;
        $this->view->cmnd = $cmnd;
        
    }
    
    public function showDetailsScoresAction(){
        $lesson_id = $this->getRequest()->getParam('lesson_id');
        $cmnd      = $this->getRequest()->getParam('cmnd');
        $stt      = $this->getRequest()->getParam('count');
        
        $db = Zend_Registry::get('db');
        
        $select_count = $db->select()
            ->from(array('a'=>'lesson_question'),array('a.*'))
            ->where('a.type = ?',1)
            ->where('a.lesson_id = ?',$lesson_id);
        $data_count = $db->fetchAll($select_count); 
        //Count 
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        
        $select = $db->select()
            ->from(array('a'=>'lesson_question'),array('a.*','answer_ques'=>'b.answer_ques'))
            ->joinLeft(array('b' => 'lesson_scores_detail'), 'a.id = b.question_id', array())
            ->where('a.type = ?',1)
            ->where('b.staff_cmnd = ?',$cmnd)
            ->where('a.lesson_id = ?',$lesson_id)
            ->order('b.id DESC')
            ->limit($total,($stt++)*$total);
        $data = $db->fetchAll($select); 
        $str = "<div class='wrap-answer'>";
        if($data){
            foreach($data as $key=>$value){
                $str .= "<div class='col-md-12 row answer_row'>
                            <div class='col-md-12'><b>".$value['content']."</b>:</div>
                            <div class='col-md-12'>".$value['answer_ques']."</div>
                        </div>";
            }
        }
        echo $str."</div>";
        exit();
    }
    
    public function trainerCheckAction(){
        
    }
    
    
    
    
    
    public function checkIdNumberAction(){
        /*
        $str = "123465  789132 4567 812345679 81234 654654";
        $str = trim(str_replace(' ','',$str));
        $strlen = strlen($str);
        $char = array();
        for( $i = 0; $i <= $strlen; $i+=9 ) {
            $char[] = substr($str,$i,9);
        }
        */
        
        $this->_helper->layout->disableLayout();
        
        $idNumber  = $this->getRequest()->getParam('id_number');
        $idNumber  = trim($idNumber);
        $idNumber  = preg_replace("/(^[\r\n]*|[\r\n]+)[\r\t]*[\s\n]+/", "\n", $idNumber);
        $idNumber  = explode("\n", $idNumber);
        $idNumber  = array_unique($idNumber);
        
        $result = array();
        $result_null = array();
        foreach($idNumber as $key => $value){
            $value = trim($value);
            
            $QStaff = new Application_Model_StaffTraining();
            $where = array();
            $where[] = $QStaff->getAdapter()->quoteInto('cmnd = ?', $value); 
            $where[] = $QStaff->getAdapter()->quoteInto('del IS NULL OR del = 0', null);   
            $data = $QStaff->fetchRow($where);
            

            if($data){
                $result['id'][] = $data['id']; 
                $result['firstname'][] = $data['firstname'];
                $result['lastname'][] = $data['lastname'];
                $result['cmnd'][] = $data['cmnd'];  
                
                $QRegion = new Application_Model_RegionalMarket();
                $whereRegion = $QRegion->getAdapter()->quoteInto('id = ?', $data['regional_market']); 
                $dataRegion = $QRegion->fetchRow($whereRegion);
                $result['region'][] = $dataRegion->name;
                 
            }
            else{
                $result_null['id'][] = ''; 
                $result_null['firstname'][] = '';
                $result_null['lastname'][] = '';
                $result_null['cmnd'][] = $value; 
            }
            
        }  
        

        $QArea = new Application_Model_Lesson();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $dataArea = $QArea->getRegionalMarket($userStorage->id);
        if(count($dataArea) == 0 || $userStorage->id == TRAINER_LEADER){
            $QArea = new Application_Model_RegionalMarket();
            $whereA = $QArea->getAdapter()->quoteInto('parent = ?', 0); 
            $dataArea = $QArea->fetchAll($whereA);
        }
        
        $this->view->list = $result;
        $this->view->list_null = $result_null;
        $this->view->area = $dataArea;
        
    }
    
    public function addTrainerAction(){
        if ($this->getRequest()->getMethod() == 'POST'){
            $name = $this->getRequest()->getParam('hovaten',array());
            $cmnd = $this->getRequest()->getParam('cmnd',array());
            $area = $this->getRequest()->getParam('area',array());
            
            foreach($name as $key => $value){
                if(!is_null(trim($value))){
                    
                    $QStaff = new Application_Model_StaffTraining();
					
					$where[] = $QStaff->getAdapter()->quoteInto('cmnd = ?', $cmnd[$key]); 
					$where[] = $QStaff->getAdapter()->quoteInto('del = ?', 1); 
					$staff_old = $QStaff->fetchRow($where);
					
                    $firstname = explode(" ",$value);
                    $count = count($firstname);
                    
                    $data = array(
                        'firstname'       => str_replace($firstname[$count-1],'',$value),
                        'lastname'        => $firstname[$count-1],
                        'cmnd'            => $cmnd[$key],
                        'team'            => 75,
                        'title'           => 182,
                        'regional_market' => $area[$key],
                        'training_online' => 1,
						'del'			  => 0
                    );
					
					$data['created_at'] = date('Y-m-d H:i:s');
					
					if(isset($staff_old) and $staff_old){
						$QStaff->update($data, $where);
					}
					else{
						$QStaff->insert($data);
					}
                    
                }
            }
            $this->_redirect(HOST . 'training/trainer');
        }
    }
    
    public function bannerAction(){
        $id = $this->getRequest()->getParam('id');
        $QFile =  new Application_Model_LessonFile();
        $where = $QFile->getAdapter()->quoteInto('id = ?', $id);
        $this->view->data = $QFile->fetchRow($where);
        
        $QFileAll =  new Application_Model_LessonFile();
        $dataAll =  $QFileAll->fetchAll();
        
        $this->view->dataAll = $dataAll;
    }
    
    public function removeBannerAction(){
        $id = $this->getRequest()->getParam('id');
        $QFile =  new Application_Model_LessonFile();
        $where = $QFile->getAdapter()->quoteInto('id = ?', $id);
        $delete = $QFile->delete($where);

        if($delete){
            echo "true";
            exit;
        }
    }
    
    public function createBannerAction(){
        $id_banner = $this->getRequest()->getParam('id');
        $title = $this->getRequest()->getParam('title');
        $QFile = new Application_Model_LessonFile();
        $data = array(
            'title' => $title
        );
        if($id_banner){
            $where = $QFile->getAdapter()->quoteInto('id = ?', $id_banner);
            $QFile->update($data,$where);
            $this->_redirect(HOST . 'training/banner?id='.$id_banner);
        }
        else{
            $id = $QFile->insert($data);   
            // ------------------ upload image----------------------------------------------------------
            
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'lesson' . DIRECTORY_SEPARATOR . 'banner' . DIRECTORY_SEPARATOR . $id;
            
            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile'=>true));
            
            //check function
            if (function_exists('finfo_file'))
                $upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif' ,'application/vnd.ms-powerpoint'));
    
            $upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
            $upload->addValidator('Size', false, array('max' => '2MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');
            $files = $upload->getFileInfo();
            $hasPhoto = false;
            $data_file = array();
    
            $fileInfo = (isset($files['image']) and $files['image']) ? $files['image'] : null;
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);
            $upload->setDestination($uploaded_dir);
            
            //Rename
            $old_name = $fileInfo['name'];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode);
            $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
            
            $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
            $r = $upload->receive(array('image'));
            if($r)
                $data_file['file_name'] = $new_name;
            else{
                $messages = $upload->getMessages();
                foreach ($messages as $msg)
                    throw new Exception($msg);
            }
            
            $QFile = new Application_Model_LessonFile();
            if($id){
                $whereFile = $QFile->getAdapter()->quoteInto('id = ?', $id);
                $QFile->update($data_file, $whereFile);
            }
            else{
                $whereFile = $QFile->getAdapter()->quoteInto('id = ?', $idLesson);
                $QFile->update($data_file, $whereFile);
            }
            
            $this->_redirect(HOST . 'training/banner');
         }
    }
    
    //Report điểm theo bài học
    public function reportScoreAction()
    {
        $export = $this->getRequest()->getParam('id', 1);
        $chinhthuc = $this->getRequest()->getParam('chinhthuc');
		
        $db = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
		$title = array(183,312,182,293,274,419,417,403);
		
        //Phân quyền view kết quả
        if(intval($userStorage->group_id) == intval(ASM_ID) || intval($userStorage->group_id) == intval(TRAINING_LEADER_ID) || intval($userStorage->group_id) == intval(TRAINING_TEAM_ID) || intval($userStorage->group_id) == intval(ASMSTANDBY_ID)){
            $db = Zend_Registry::get('db');
            $selectRegion = $db->select()
            ->from(array('a'=>'area'),array('region'=>'GROUP_CONCAT(d.id)'))
            ->joinLeft(array('b'=>'asm'),'a.id = b.area_id',array())
            ->joinLeft(array('c'=>'staff'),'b.staff_id = c.id',array())
            ->joinLeft(array('d'=>'regional_market'),'a.id = d.area_id',array())
            ->where('b.staff_id = ?',$userStorage->id)
            ->group('b.staff_id');
            
            $list = $db->fetchRow($selectRegion);
            $region_access = $list['region'];
        }
    
        //nestedSelect    
        $select1 = $db->select()
        ->from(array('a' => 'staff_training'),
                array( 'id','firstname','lastname','cmnd','team','title','regional_market','created_at','training_online',new Zend_Db_Expr('null') ))
        ->where('a.training_online = ?',1)
        ->where('a.del IS NULL OR a.del = ?',0);
        
		if(isset($chinhthuc) && $chinhthuc == 1){
            $select1->where('1 = ?',0);
        }
		
        //query 2
        $select2 = $db->select()
        ->from(array('a' => 'staff'),
                array( 'id','firstname','lastname','ID_number','team','title','regional_market','joined_at',new Zend_Db_Expr('null'),'id' ))
        ->where('a.department = ?',DEPARTMENT_SALE)
        ->where('a.off_date is null')
        ->where('a.title IN (?)',$title)
        ->where('a.status = ?', 1);

        $nestedSelect = $db->select()
             ->union(array($select1, $select2));
        //
        
        $select = $db->select()
            ->from(array('a' => new Zend_Db_Expr('(' . $nestedSelect . ')')),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.*', 'title_market' => 'b.name', 'title_area' => 'area.name', 
                'dtb' => 'AVG(c.scores)',
                'max_scores' => 'MAX(c.scores)',
                'sothanglamviec' => 'TIMESTAMPDIFF(MONTH,a.created_at,NOW())',
                'songaylamviec' => 'TIMESTAMPDIFF(DAY,a.created_at,NOW())',
                'sokhoahoc' => 'COUNT(Distinct c.lesson_id)',
                'team_name'=>'d.name'));
            
            $select->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market', array());
            $select->joinLeft(array('area' => 'area'), 'b.area_id = area.id', array());
            if(isset($export) and $export and $export != 1)
            {
                 $select->joinLeft(array('c' => new Zend_Db_Expr("(SELECT * FROM lesson_scores WHERE lesson_id = $export)")), 'a.cmnd = c.staff_cmnd', array());
                //$select->where('c.lesson_id = ?',$export);
            }
            else{
                $select->joinLeft(array('c' => 'lesson_scores'), 'a.cmnd = c.staff_cmnd', array());
            }
            $select->joinLeft(array('d' => 'team'), 'd.id = a.title', array());
            
            
            
            if(isset($region_access) and $region_access){
                $regi = explode(',',$region_access);
                if(count($regi))
                    $select->where('a.regional_market IN (?)',$regi);
            }
            
            $select->group('a.id');
        
        $order_str = '';
        
        $select->order(array('training_online DESC','a.id DESC'));


        $result = $db->fetchAll($select);
        if(isset($export) and $export)
        {
            $this->_report($result,$export);
        }
        else{
            $this->_report($result);
        }
        exit;
        
    }
    
    //Report điểm theo bài học
    public function reportNewStaffAction()
    {
        $export = $this->getRequest()->getParam('id', 1);
        
        $db = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        //Phân quyền view kết quả
        if(intval($userStorage->group_id) == intval(ASM_ID) || intval($userStorage->group_id) == intval(TRAINING_LEADER_ID) || intval($userStorage->group_id) == intval(TRAINING_TEAM_ID) || intval($userStorage->group_id) == intval(ASMSTANDBY_ID)){
            $db = Zend_Registry::get('db');
            $selectRegion = $db->select()
            ->from(array('a'=>'area'),array('region'=>'GROUP_CONCAT(d.id)'))
            ->joinLeft(array('b'=>'asm'),'a.id = b.area_id',array())
            ->joinLeft(array('c'=>'staff'),'b.staff_id = c.id',array())
            ->joinLeft(array('d'=>'regional_market'),'a.id = d.area_id',array())
            ->where('b.staff_id = ?',$userStorage->id)
            ->group('b.staff_id');
            
            $list = $db->fetchRow($selectRegion);
            $region_access = $list['region'];
        }
    
        //nestedSelect    
        $select1 = "SELECT `a`.`id`, `a`.`firstname`, `a`.`lastname`, `a`.`cmnd`, `a`.`team`, `a`.`title`, `a`.`regional_market`,`a`.`created_at`,`a`.`training_online`,NULL
                    FROM `staff_training` AS `a`
                    WHERE (a.training_online = 1)
                    AND (a.del = 0 OR a.del IS NULL)
                    AND a.id NOT IN(
                    			SELECT
                    				`a`.`id`
                    			FROM
                    				`staff_training` AS `a`
                    			WHERE a.cmnd IN (SELECT ID_number FROM staff AS s WHERE s.off_date IS NULL))";
        
        //query 2
        $nestedSelect = $db->select()
             ->union(array($select1));
        //
        
        $select = $db->select()
            ->from(array('a' => new Zend_Db_Expr('(' . $nestedSelect . ')')),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.*', 'title_market' => 'b.name', 'title_area' => 'area.name',
                'dtb' => 'AVG(c.scores)',
                'max_scores' => 'MAX(c.scores)',
                'sothanglamviec' => 'TIMESTAMPDIFF(MONTH,a.created_at,NOW())',
                'songaylamviec' => 'TIMESTAMPDIFF(DAY,a.created_at,NOW())',
                'sokhoahoc' => 'COUNT(Distinct c.lesson_id)',
                'team_name'=>'d.name'));
            
            $select->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market', array());
            $select->joinLeft(array('area' => 'area'), 'b.area_id = area.id', array());
            if(isset($export) and $export and $export != 1)
            {
                 $select->joinLeft(array('c' => new Zend_Db_Expr("(SELECT * FROM lesson_scores WHERE lesson_id = $export)")), 'a.cmnd = c.staff_cmnd', array());
                //$select->where('c.lesson_id = ?',$export);
            }
            else{
                $select->joinLeft(array('c' => 'lesson_scores'), 'a.cmnd = c.staff_cmnd', array());
            }
            $select->joinLeft(array('d' => 'team'), 'd.id = a.title', array());
            
            
            
            if(isset($region_access) and $region_access){
                $regi = explode(',',$region_access);
                if(count($regi))
                    $select->where('a.regional_market IN (?)',$regi);
            }
            
            $select->group('a.id');
        
        $order_str = '';
        
        $select->order(array('training_online DESC','a.id DESC'));

        $result = $db->fetchAll($select);
		
        if(isset($export) and $export)
        {
            $this->_report($result,$export);
        }
        else{
            $this->_report($result);
        }
        exit;
        
    }
    
    public function _report($data, $export = null){
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        
        $heads = array(
            '#',
            'Họ tên',
            'Số CMND',
            'Team',
            'Khu vực',
            'Tỉnh thành',
            'Điểm trung bình',
            'Số khóa học',
            'Thời gian làm việc',
            'Kết quả'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $index    = 1;
       
        $alpha    = 'A';
        
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;
        $i = 1;
        
        foreach($data as $item){
            $alpha = 'A';
            
            $kq = ($item['max_scores'] == 10) ? 'PASS' : 'FAIL';
            
            $sheet->setCellValue($alpha++.$index, $i++);
			$sheet->setCellValue($alpha++.$index, $item['firstname']." ".$item['lastname']);
			$sheet->setCellValue($alpha++.$index, $item['cmnd']);
            $sheet->setCellValue($alpha++.$index, $item['team_name']);
            $sheet->setCellValue($alpha++.$index, $item['title_area']);
            $sheet->setCellValue($alpha++.$index, $item['title_market']);
            $sheet->setCellValue($alpha++.$index, $item['dtb']);
            $sheet->setCellValue($alpha++.$index, $item['sokhoahoc']);
            $sothanglamviec = $item['sothanglamviec'] == 0 ? $item['songaylamviec']." Ngày" : $item['sothanglamviec']." Tháng";
            $sheet->setCellValue($alpha++.$index, $sothanglamviec);
            $sheet->setCellValue($alpha++.$index, $kq);
                        
            $index++;

        }
        
        if(isset($id) && $id){
            $QLesson = new Application_Model_Lesson();
            $where = $QLesson->getAdapter()->quoteInto('id = ?',intval($id));
            $Lesson = $QLesson->fetchRow($where);
            
            $filename = $Lesson->title.' - '.date('d/m/Y');
        }
        else{
            $filename = 'Training Online - '.date('d/m/Y');
        }
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
    
    //Report kết quả
    public function reportScoreAllAction()
    {
        $export = 1;
        
        if(isset($export) and $export)
        {
            $db = Zend_Registry::get('db');
            
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            //Phân quyền view kết quả
            if(intval($userStorage->group_id) == intval(ASM_ID) || intval($userStorage->group_id) == intval(TRAINING_LEADER_ID) || intval($userStorage->group_id) == intval(TRAINING_TEAM_ID) || intval($userStorage->group_id) == intval(ASMSTANDBY_ID)){
                $db = Zend_Registry::get('db');
                $selectRegion = $db->select()
                ->from(array('a'=>'area'),array('region'=>'GROUP_CONCAT(d.id)'))
                ->joinLeft(array('b'=>'asm'),'a.id = b.area_id',array())
                ->joinLeft(array('c'=>'staff'),'b.staff_id = c.id',array())
                ->joinLeft(array('d'=>'regional_market'),'a.id = d.area_id',array())
                ->where('b.staff_id = ?',$userStorage->id)
                ->group('b.staff_id');
                
                $list = $db->fetchRow($selectRegion);
                $region_access = $list['region'];
            }
        
            //nestedSelect    
            $select1 = $db->select()
            ->from(array('a' => 'staff_training'),
                    array( 'id','firstname','lastname','cmnd','team','title','regional_market','created_at','training_online',new Zend_Db_Expr('null') ))
            ->where('a.training_online = ?',1);
            
            //query 2
            $title = array(SALES_TITLE,PGPB_TITLE,PGPB_2_TITLE);
            $select2 = $db->select()
            ->from(array('a' => 'staff'),
                    array( 'id','firstname','lastname','ID_number','team','title','regional_market','joined_at',new Zend_Db_Expr('null'),'id' ))
            ->where('a.department = ?',DEPARTMENT_SALE)
            ->where('a.team = ?',SALES_TEAM)
			->where('a.status = ?',1)
            ->where('a.off_date is null')
            ->where('a.title IN (?)',$title);
    
            $nestedSelect = $db->select()
                 ->union(array($select1, $select2));
            //
            
            
            //$scoreSelect
            $select3 = "SELECT lesson_id, scores, staff_cmnd
                FROM(
                SELECT lesson_id, scores, staff_cmnd
                FROM `lesson_scores` AS `a`
                WHERE staff_cmnd = 174007539 
                GROUP BY lesson_id
                ) AS a";
            //
            
            $select = $db->select()
                ->from(array('a' => new Zend_Db_Expr('(' . $nestedSelect . ')')),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.*', 'title_market' => 'b.name', 'title_area' => 'area.name',
                    'dtb' => 'AVG(c.scores)',
                    'sothanglamviec' => 'TIMESTAMPDIFF(MONTH,a.created_at,NOW())',
                    'songaylamviec' => 'TIMESTAMPDIFF(DAY,a.created_at,NOW())',
                    'sokhoahoc' => 'COUNT(Distinct c.lesson_id)',
                    'team_name'=>'d.name'))
                ->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market', array())
                ->joinLeft(array('area' => 'area'), 'b.area_id = area.id', array())
                ->joinLeft(array('c' => new Zend_Db_Expr('(' . $select3 . ')')), 'a.cmnd = c.staff_cmnd', array())
                ->joinLeft(array('d' => 'team'), 'd.id = a.title', array());
                
                if(isset($region_access) and $region_access){
                    $regi = explode(',',$region_access);
                    if(count($regi))
                        $select->where('a.regional_market IN (?)',$regi);
                }
            
                 $select->group('a.id');
            $order_str = '';
            
            $select->order(array('training_online DESC','a.id DESC'));
            $result = $db->fetchAll($select);
            $this->_report_all($result);
        }
        exit;
        
    }
    
    public function _report_all($data){
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        
        $heads = array(
            '#',
            'Name',
            'ID Number',
            'Team',
            'Khu vực',
            'Tỉnh thành',
            'Điểm trung bình',
            'Số khóa học',
            'Time Job'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $index    = 1;
       
        $alpha    = 'A';
        
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;
        $i = 1;
        
        foreach($data as $item){
            $alpha = 'A';
           
            $sheet->setCellValue($alpha++.$index, $i++);
			$sheet->setCellValue($alpha++.$index, $item['firstname']." ".$item['lastname']);
			$sheet->setCellValue($alpha++.$index, $item['cmnd']);
            $sheet->setCellValue($alpha++.$index, $item['team_name']);
            $sheet->setCellValue($alpha++.$index, $item['title_area']);
            $sheet->setCellValue($alpha++.$index, $item['title_market']);
            $sheet->setCellValue($alpha++.$index, $item['dtb']);
            $sheet->setCellValue($alpha++.$index, $item['sokhoahoc']);
            $sothanglamviec = $item['sothanglamviec'] == 0 ? $item['songaylamviec']." Ngày" : $item['sothanglamviec']." Tháng";
            $sheet->setCellValue($alpha++.$index, $sothanglamviec);
            
            $index++;

        }
        
        $filename = 'Training Online - '.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
    
	public function xuatAction(){
		$db = Zend_Registry::get('db');
		
		$sql 	 = 'SELECT * FROM trade_marketing.posm_year WHERE year_sign > 2015';
		$test    = $db->query($sql);
		$data 	 = $test->fetchAll();
		
		$data_all = [];
		foreach($data as $key=>$value){
			$data_all[$value['category_id']][$value['year_sign'].'-'.$value['contructor_id']] = $value['quantity'];
		}
		
		$sql_cat 	= 'SELECT * FROM trade_marketing.posm_year WHERE year_sign > 2015 GROUP BY category_id';
		$cat    	= $db->query($sql_cat);
		$data_cat 	= $cat->fetchAll();
		
		$sql_contruct 	= 'SELECT * FROM trade_marketing.posm_year WHERE year_sign > 2015 GROUP BY `year_sign`, `contructor_id`';
		$contruct    	= $db->query($sql_contruct);
		$data_contruct 	= $contruct->fetchAll();
		
		require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        
		$data_title = [];
		foreach($data_contruct as $items){
			$data_title[] = $items['year_sign'].'-'.$items['contructor_id'];
		}
		
        $heads = array(
            '#',
            'Name'
        );
		
		foreach($data_title as $key=>$value){
			$heads[] = $value;
		}

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $index    = 1;
       
        $alpha    = 'A';
        
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;
        $i = 1;
        
        foreach($data_cat as $item){
            $alpha = 'A';
           
            $sheet->setCellValue($alpha++.$index, $i++);
			$sheet->setCellValue($alpha++.$index, $item['category_name']);
			foreach($data_title as $key=>$value){
				$sheet->setCellValue($alpha++.$index, $data_all[$item['category_id']][$value]);
			}
            
            $index++;

        }
        
        $filename = 'Abc - '.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
	}
	
	public function addJobcardTgddAction(){
		require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
        $this->wssURI = 'https://cs.opposhop.vn/wss?wsdl';
        //$this->wssURI = 'http://cs-live.local/wss?wsdl';
        
        $client = new nusoap_client($this->wssURI);
        //$client->soap_defencoding = 'UTF-8';
        //$client->decode_utf8 = false;

        $ValueDate = date('Y-m-d H:i:s');
        $pass = md5($ValueDate.'d9910eeb2c527a6f1591137609d2badc');

        $wsParams = array(
            'PartnerAccount' => 'TGDĐ01',
            'Password'       => "17e44d914903df86e133f81030521c12",
            'ValueDate'      => "2017-06-15 09:25:44",
            'Jobcard'        => "JDT0012317060000589",
        );

        $result = $client->call("wsAddReceptionPartnerTgdd", $wsParams);
        var_dump($result);exit;
	}


    public function lessonPriviledgeAction()
    {
            $sort            = $this->getRequest()->getParam('sort', '');
            $desc            = $this->getRequest()->getParam('desc', 1);
            $page           = $this->getRequest()->getParam('page', 1);
                $params['sort']                 = $sort;
                $params['desc']                 = $desc;
                $total              = 0;
                $limit              = 1000;
        $QModel = new Application_Model_Lesson();
        $lesson = $QModel->getLessonPriviledge($page, $limit, $total, $params);
        $this->view->lesson= $lesson;
        $this->view->desc       = $desc;
        $this->view->sort       = $sort;
        $this->view->params     = $params;
        $this->view->limit      = $limit;
        $this->view->total      = $total;

        $this->view->url = HOST.'training/lesson-priviledge'.($params ? '?'.http_build_query($params).'&' : '?');
        $this->view->offset = $limit*($page-1); 
    }
    public function saveLessonPriviledgeAction()
    {
        $flashMessenger       = $this->_helper->flashMessenger;
        $QLessonPriviledge = new Application_Model_LessonPriviledge();
         $lesson =  $this->getRequest()->getParam('lesson');
         $staff_main = $this->getRequest()->getParam('staff_main');
         $staff_new = $this->getRequest()->getParam('staff_new');
         $staff_brandshop = $this->getRequest()->getParam('staff_brandshop');


         
         try{

            $where = $QLessonPriviledge->getAdapter()->quoteInto('id > ?', 0);
            $QLessonPriviledge->delete($where);
            
            foreach ($staff_main as $key => $value) {

                foreach($value as $k=>$v){
                    $data = [
                        'lesson_id'=>$key,
                        'type' =>$v
                     ];
                     $QLessonPriviledge->insert($data);
                }

                 
             }

             $flashMessenger->setNamespace('success')->addMessage('Done!');
             $this->redirect(HOST.'training/lesson-priviledge');
         }catch (Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage( "Fail!");
            $this->redirect(HOST.'training/lesson-priviledge');
        }


    if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

        $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages_error;

    }
    if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
        $messages          = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
    }
    }
}




















