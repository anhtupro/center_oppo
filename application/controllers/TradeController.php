<?php
class TradeController extends My_Controller_Action
{

	public function init()
    {
//        echo 'Hệ thống đang tạm dừng để bảo trì. Vui lòng quay lại sau. Xin chân thành cảm ơn!';
        define('TRADE_SUPERVISOR_REALME_TITLE', 79000000);

        $QAreaStaff = new Application_Model_AreaStaff();
        $QAsm       = new Application_Model_Asm();
        
        $QArea       = new Application_Model_Area();
        $QAppNoti       = new Application_Model_AppNoti();
        $QRegionalMarket       = new Application_Model_RegionalMarket();
        $QLogSystemTrade       = new Application_Model_LogSystemTrade();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id    = $userStorage->id;
        $title       =  $userStorage->title;
        $group       =  $userStorage->group_id;
        $email       =  $userStorage->email;

        $storage = [
            'staff_id'  => $staff_id,
            'title'     => $title,
            'group'     => $group,
            'email'     => $email,
            'fullname'  => $userStorage->firstname.' '.$userStorage->lastname,
            'code'      => $userStorage->code,
            'menu'      => $userStorage->menu,
            'photo'     => $userStorage->photo,
            'is_staff_realme' => $userStorage->is_realme
        ];

        $storage['title_list'][] = $title;
        
        //vanthao.dinh 12536 vừa local vừa leader (639), binhsang.ly 9837
        if(in_array($staff_id, [12536,1973,7796,9837])){
            $storage['title_list'][] = TRADE_MARKETING_LEADER;
        }

        
        if(in_array($staff_id, [9837, 5772])){
            $storage['title_list'][] = SALE_SALE_ASM;
        }
        
        //vietson.nguyen 557 vừa là leader vừa là suppervisor vừa là local
        if(in_array($staff_id, [557,1973])){
            $storage['title_list'][] = TRADE_MARKETING_SUPERVISOR;
            $storage['title_list'][] = TRADE_MARKETING_LEADER;
            $storage['title_list'][] = TRADE_MARKETING_EXECUTIVE;
        }
        
        if(in_array($staff_id, [8933, 2565, 2470, 6285, 12536, 7796,14802])){
            $storage['title_list'][] = TRADE_MARKETING_EXECUTIVE;
        }

        if(in_array($title, [179, 181, 308, 391, 392, 462, 463, 464, 465, 466, 467, 471, 533, 535, 536, 534, SALES_ADMIN_TITLE , 662,663,664,665,666,667,668,669,670,671,672, 673,674])){
            $asm     = $QAsm->get_cache($staff_id);
            $area_id = $asm['area'];
            $storage['area_id']  = $area_id;
        }
            elseif( in_array($title, [TRADE_MARKETING_EXECUTIVE, TRADE_MARKETING_LEADER, TRADE_MARKETING_DESIGNER, TRADE_MARKETING_ASSISTANT, TRADE_SUPERVISOR_REALME_TITLE]) ){
//            $area_staff = $QAreaStaff->getAreaTrade($email);
            $area_staff = $QAreaStaff->getArea($staff_id);
                $area_staff = $area_staff ? $area_staff : [0];

                $storage['area_id']  = $area_staff;
        }
        elseif( in_array($title, [638,TRADE_MARKETING_SUPERVISOR, TRADE_MARKETING_MANAGER, TRADE_MARKETING_LEADER_HO, TRADE_MARKETING_SPECIALIST, RETAIL_MANAGER_TITLE]) || in_array($userStorage->id, [2464, 23153, SUPERADMIN_ID])  ){

            $area_staff = [];
            $area = $QArea->get_cache();
            foreach($area as $key=>$value){
                $area_staff[] = $key;
            }

            $storage['area_id']  = $area_staff;
        }
        elseif( in_array($title, [SALES_TITLE, SALES_LEADER_TITLE]) ){
            $area = $QRegionalMarket->getAreaById($staff_id);
//            $storage['area_id'][]  = $area['area_id'];
        }
        
        if( $userStorage->id == 557 || $userStorage->id == 5968 ){

            $area_staff = [];
            $area = $QArea->get_cache();
            foreach($area as $key=>$value){
                $area_staff[] = $key;
            }

            $storage['area_id']  = $area_staff;
        }
        
        
        //Save Log
        $action_link = $this->getRequest()->getActionName();
        if(!in_array($action_link, ['ajax-quotation','save-create-checkshop','save-edit-checkshop','checkshop-qc','edit-checkshop','check-shop-list'])){
            $log_system_trade = [
                'staff_id' => $staff_id,
                'action'   => $action_link,
                'created_at'    => date('Y-m-d H:i:s')
            ];
            $QLogSystemTrade->insert($log_system_trade);
        }
        
 
        //NOTIFICATION
        $params = [
            'area_list'  => $storage['area_id'],
            'title'      => $storage['title'],
            'staff_id'   => $staff_id
        ];
        
        //$noti_tranfer = $QAppNoti->getNotiTranfer($params);
        //$noti_repair  = $QAppNoti->getNotiRepair($params);
      //  $noti_destruction  = $QAppNoti->getNotiDestruction($params);
       // $noti_air  = $QAppNoti->getNotiAir($params);
       // $noti_bantu  = $QAppNoti->getNotiBantu($params);
        //$noti_posm  = $QAppNoti->getNotiPosm($params);

//        $app_noti = [];
//        $total_noti = [];
//
//        foreach($noti_tranfer as $key=>$value){
//            $total_noti[] = [
//                'link'      => '/trade/view-transfer?id='.$value['id'],
//                'text'      => '<b>Điều chuyển: </b>' .$value['status_name'].' MS-'.$value['id']
//            ];
//            $app_noti[126][] = $value;
//        }
//
//
//
//        foreach($noti_repair as $key=>$value){
//            $total_noti[] = [
//                'link'      => '/trade/repair-confirm?id='.$value['id'],
//                'text'      => '<b>Sửa chữa: </b>' .$value['status_name'].' MS-'.$value['id']
//            ];
//            $app_noti[127][] = $value;
//        }
//
//        foreach($noti_destruction as $key=>$value){
//            $total_noti[] = [
//                'link'      => '/trade/destruction-confirm?id='.$value['id'],
//                'text'      => '<b>Tiêu hủy: </b>' .$value['status_name'].' MS-'.$value['id']
//            ];
//            $app_noti[128][] = $value;
//        }
//
//        foreach($noti_air as $key=>$value){
//            $total_noti[] = [
//                'link'      => '/trade/edit-air?id='.$value['id'],
//                'text'      => '<b>Biển - Vách - Cột: </b>' .$value['status_name'].' MS-'.$value['id']
//            ];
//            $app_noti[125][] = $value;
//        }
//
//        foreach($noti_bantu as $key=>$value){
//            $total_noti[] = [
//                'link'      => '/trade/confirm-order?campaign_id='.$value['campaign_id'],
//                'text'      => '<b>Bàn - Tủ - Bục Góc: </b>' .$value['status_name'].' MS-'.$value['id']
//            ];
//            $app_noti[125][] = $value;
//            $app_noti[133][] = $value;
//        }
//
//
//
//        foreach($noti_posm as $key=>$value){
//            $total_noti[] = [
//                'link'      => '/trade/list-posm',
//                'text'      => '<b>POSM: </b>' .$value['campaign_name'].'/' .$value['status_name'].' MS-'.$value['campaign_id']
//            ];
//            $app_noti[125][] = $value;
//            $app_noti[134][] = $value;
//        }
//
//        if( in_array($title, [SALES_TITLE]) ){
//
//            //$noti_checkshop  = $QAppNoti->getNotiCheckshop($params);
//            foreach($noti_checkshop as $key=>$value){
//                // $total_noti[] = [
//                //     'link'      => '',
//                //     'text'      => '<b>Checkshop: </b>/ MS-'
//                // ];
//                $app_noti[129][] = $value;
//            }
//        }



        
//        if($_GET['dev'] == 7){
//            // $test_push = $QAppNoti->send_notification('dg1jx7cwzYI:APA91bFHT1zxlf_n4e73MunWojSh6DZWAoQTQWZBMxrSg8i3-rAp4fuMNKngYoEzvApcorZ51bW1KnBCse1pF_J15jeQO_EaBHAWc8x2cEd_BBGG-OKcP3GPzhtgz_Y2cmf3J7ljw-FD_7H0iAKqPAQby9dmqribBw', 'test.');
//
//            // $test_push = $QAppNoti->send_notification('c3aAMKiBvBs:APA91bGPcwSiJaSG6PID96FhbifjK2n8S_fvM0ip-Edq8cPOYAX4Njxoat9PJdqo8SoaZxhkhzjHbP3V97PYOj7tMxqv0I0lemzBu7xWhIyTFCh1L0DPV3GnvOYxEoRKZtsrvJcMoaJCOfloo7Da-QILglRJY4bJFg', 'test.');
//
//            $test_push = $QAppNoti->send_notification('e1FAAFGlf-k:APA91bEs9h12YEZQUdRryRF9Ic9uRrcqY5cEOUxJKFhzMskHNCQxewXUoXlAfkMm_kzHlhC4iAgLtPagP4NrAgRZGYDuhtPhLfRgeP8CCLlDTdNfJHCY3bh7_Ld1AtnCzIOz1OkhjaN_rwHG9RbEZgEqot0Q77z5xg', 'test.');
//
//            echo "<pre>";
//            var_dump($test_push);exit;
//        }



//        if( in_array($staff_id, [15038]) ){ // khoinguyen.tran
//            $storage['title_list']  = [9999];
//            $storage['title']  = 9999;
//        }

        //END NOTIFICATION

        $this->storage = $storage;
        $this->view->storage = $this->storage;
        $this->view->app_noti = $app_noti;
        $this->view->total_noti = $total_noti;
        
        // if($userStorage->login_from_trade == 1){
        //     $this->_helper->layout->setLayout('layout_trade');
        // }
        // else{
        //     $this->_helper->layout->setLayout('layout_metronic2020');
        // }
        
        $this->_helper->layout->setLayout('layout_metronic2020');
        
    }
     public static function insertAllrow($params,$db){
     $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
     $config = $config->toArray();
     $db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
       'host'     => $config['resources']['db']['params']['host'],
       'username' => $config['resources']['db']['params']['username'],
       'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
       'dbname'   => 'trade_test'
     ));
     $temp= array();
      foreach ($params as $key => $value) {
        if(!empty($value))
        {
         $temp =$params[$key];
         break;
        }
      }
      print_r($temp); exit;
     
    
     $arrkey = array_keys($temp);
     $str_insert = '';
     foreach ($params as $k => $param){
      $str_insert .= "('".implode("', '", $param)."')" . ',';
     }
      
     $str_rows = rtrim($str_insert,',');
      
     $sql  = "INSERT INTO $db ";
    
     $sql .= " (`".implode("`, `", array_keys($temp))."`)";
      
     $sql .= " VALUES $str_rows ";
    //echo $sql; exit;
      $db_log->query($sql);
    }
     public function testAction()
    {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
       
             $total_row[0] = array(
                                'name'   => 'a'
                        ); 
             $total_row[1] = array(
                                'name'   => 'b'
                        ); 
             $this->insertAllrow($total_row,'air_type');

    }
    public function indexAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'index.php';
    }

    public function createOrderAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-order.php';
    }

    public function saveOrderAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-order.php';
    }

    public function confirmOrderAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'confirm-order.php';
    }

    public function saveConfirmOrderAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-confirm-order.php';
    }

    public function orderDetailsAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-details.php';
    }

    public function detailsAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'details.php';
    }

    public function listOrderAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'list-order.php';
    }

    public function orderNewAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-new.php';
    }

    public function repairAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair.php';
    }
    public function destructionListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'destruction-list.php';
    }
    public function destructionCreateAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'destruction-create.php';
    }
    public function saveDestructionEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-destruction-edit.php';
    }
    public function destructionEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'destruction-edit.php';
    }
    public function destructionConfirmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'destruction-confirm.php';
    }
    public function repairListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair-list.php';
    }
    public function repairCreateAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair-create.php';
    }
    public function repairEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair-edit.php';
    }
    public function saveRepairEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-repair-edit.php';
    }
    public function repairQrCodeAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair-qr-code.php';
    }
    public function repairConfirmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair-confirm.php';
    }
    public function transferListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'transfer-list.php';
    }
    public function transferCreateAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'transfer-create.php';
    }
    public function transferCreateQrCodeAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'transfer-create-qr-code.php';
    }
    public function transferEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'transfer-edit.php';
    }
    public function transferConfirmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'transfer-confirm.php';
    }
    public function destructionQrCodeAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'destruction-qr-code.php';
    }
    public function checkShopListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-list.php';
    }
    public function checkShopCreateAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-create.php';
    }
    public function checkShopEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-edit.php';
    }
    public function getShopInfoAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-shop-info.php';
    }
    public function getShopInfoAllAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-shop-info-all.php';
    }

    public function uploadImageAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'upload-image.php';
    }

    // public function qrCodeAction(){
    //     require_once 'trade' . DIRECTORY_SEPARATOR . 'qr-code.php';
    // }

    public function quantityContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'quantity-contractor.php';
    }

    public function createQuantityContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-quantity-contractor.php';
    }

    public function saveQuantityContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-quantity-contractor.php';
    }

    public function contractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor.php';
    }

    public function contractorDetailsAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-details.php';
    }

    public function contractorInAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-in.php';
    }

    public function contractorOutAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-out.php';
    }

    public function contractorScanInAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-scan-in.php';
    }

    public function contractorScanOutAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-scan-out.php';
    }

    public function contractorQuantityAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-quantity.php';
    }
    
    public function getImeiInfoAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-imei-info.php';
    }
    public function getInfoAreaAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-info-area.php';
    }
    public function contractorTransporterAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-transporter.php';
    }

    public function saveContractorTransporterAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-contractor-transporter.php';
    }

    public function scanAreaAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'scan-area.php';
    }

    public function saveScanAreaAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-scan-area.php';
    }

    public function scanShopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'scan-shop.php';
    }

    public function saveScanShopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-scan-shop.php';
    }

    public function saveContractorQuantityAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-contractor-quantity.php';
    }

    public function listCampaignAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'list-campaign.php';
    }

    public function shareQuantityAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'share-quantity.php';
    }

    public function ajaxGetContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'ajax-get-contractor.php';
    }

    public function ajaxGetAreaAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'ajax-get-area.php';
    }

    public function saveShareQuantityAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-share-quantity.php';
    }

    public function contractorOutListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-out-list.php';
    }

    public function saveContractorOutAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-contractor-out.php';
    }

    public function shipmentAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment.php';
    }

    public function createShipmentAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-shipment.php';
    }

    public function saveShipmentAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-shipment.php';
    }

    public function shipmentOutAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment-out.php';
    }

    public function contractorOutDetailAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-out-detail.php';
    }

    public function shipmentDetailsAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment-details.php';
    }

    public function shipmentContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment-contractor.php';
    }

    public function saveShipmentOutAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-shipment-out.php';
    }

    public function shipmentAreaAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment-area.php';
    }

    public function shipmentInAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment-in.php';
    }
    

    public function saveShipmentInAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-shipment-in.php';
    }

    public function shipmentStoreAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment-store.php';
    }

    public function shipmentStoreInAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'shipment-store-in.php';
    }

    public function saveShipmentStoreInAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-shipment-store-in.php';
    }

    public function airAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'air.php';
    }

    public function createAirAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-air.php';
    }
    public function editAirAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'edit-air.php';
    }
    public function ajaxQuotationAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'ajax-quotation.php';
    }
    public function listAllAirAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'list-all-air.php';
    }
    public function saveAirAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-air.php';
    }
    public function updateAirAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'update-air.php';
    }
    public function nghiemthuAirAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'nghiemthu-air.php';
    }
     public function chiaNhaThauAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'chia-nha-thau.php';
    }
     public function uploadExcelAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'upload-excel.php';
    }
    

    public function createCheckshopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-checkshop.php';
    }
	
	public function saveCreateCheckshopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-create-checkshop.php';
    }

    public function viewCheckshopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'view-checkshop.php';
    }

    public function createTransferAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-transfer.php';
    }

    public function saveTransferAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-transfer.php';
    }
	
	public function editTransferAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'edit-transfer.php';
    }

    public function viewTransferAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'view-transfer.php';
    }

    public function listPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'list-posm.php';
    }

    public function limitCampaignAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'limit-campaign.php';
    }

    public function orderCampaignAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-campaign.php';
    }

    public function confirmPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'confirm-posm.php';
    }
    
    public function viewPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'view-posm.php';
    }

    public function shareContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'share-contractor.php';
    }

    public function categoryContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'category-contractor.php';
    }

    public function categoryContractorLogAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'category-contractor-log.php';
    }

    public function contractorAreaAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-area.php';
    }

    public function areaInAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'area-in.php';
    }

    public function contractorAreaDetailsAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-area-details.php';
    }
    
    public function confirmTransferAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'confirm-transfer.php';
    }

    public function contractorPriceAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-price.php';
    }

    public function contractorPriceCreateAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-price-create.php';
    }

    public function contractorCategoryAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-category.php';
    }

    public function contractorCategorySaveAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contractor-category-save.php';
    }

    public function permissionAppAction(){
        $this->_helper->layout->setLayout('layout_permission');
    }

    public function permissionAppOppoAction(){
        $this->_helper->layout->setLayout('layout_permission_oppo');
    }

    public function createPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-posm.php';
    }
    public function editCheckshopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'edit-checkshop.php';
    }
    public function saveEditCheckshopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-edit-checkshop.php';
    }
    
    //Chỉnh sửa giá Hạng mục - nhà thầu
    public function categoryPriceCreateAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'category-contructor' . DIRECTORY_SEPARATOR . 'category-price-create.php';
    }
    
    public function checkshopQcAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . 'checkshop-qc.php';
    }
    public function createNewContractorAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-new-contractor.php';
    }
    
    public function confirmPosmAjaxAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'posm' . DIRECTORY_SEPARATOR . 'confirm-posm-ajax.php';
    }
    
    public function getInfoCheckshopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair' . DIRECTORY_SEPARATOR . 'get-info-checkshop.php';
    }
    
    public function saveTransferEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-transfer-edit.php';
    }
    
    private function month_range($first, $last, $step = '+1 month', $output_format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
    
        while( $current <= $last ) {
    
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
    
        return $dates;
    }

    
    public function removeShopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop.php';
    }
    
    public function removeShopListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-list.php';
    }
    
    public function removeShopDetailAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-detail.php';
    }
    
    public function removeShopDelAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-del.php';
    }
    
    public function removeShopRejectAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-reject.php';
    }
    
    public function removeShopConfirmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-confirm.php';
    }
    public function removeShopApproveAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-approve.php';
    }
    public function removeShopCloseAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-close.php';
    }

    public function openShopAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'open-shop.php';
    }
    
    public function openShopListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'open-shop-list.php';
    }
    
    public function openShopDetailAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'open-shop-detail.php';
    }
    
    public function openShopConfirmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'open-shop-confirm.php';
    }
    
    
      public function checkshopCategorySizeAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'checkshop-category-size.php';
    }
    
	public function airListAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'air-list.php';
    }
    public function airCreateAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'air-create.php';
    }
    public function airEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'air-edit.php';
    }
	
    public function saveAirEditAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-air-edit.php';
    }
	
	public function getInfoShopAction(){
		require_once 'trade' . DIRECTORY_SEPARATOR . 'repair' . DIRECTORY_SEPARATOR . 'get-info-shop.php';
    }
    
    public function createCategoryPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'create-category-posm.php';
    }
    public function listCategoryPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'list-category-posm.php';
    }
     public function editCategoryPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'edit-category-posm.php';
    }
    public function delCategoryPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'del-category-posm.php';
    }
    
    public function listContractorPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'list-contractor-posm.php';
    }
    
    public function editContractorPosmAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'edit-contractor-posm.php';
    }
    
    public function contractorPriceCategoryAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'contractor-price-category.php';
    }
    
    public function contractorPriceHistoryAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'contractor-price-history.php';
    }
     public function inventoryProductAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'inventory-product.php';
    }
    
    public function createContractAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR .'create-contract.php';
    }

    public function budgetCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'budget-create.php';
    }

    public function budgetSaveCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'budget-save-create.php';
    }

    public function budgetSaveEditAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'budget-save-edit.php';
    }

    public function budgetListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'budget-list.php';
    }

    public function listPosmFeeAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'list-posm-fee.php';
    }

    public function detailPosmAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'detail-posm.php';
    }

    public function manageWarehouseAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'manage-warehouse.php';
    }
    
    public function createStoreAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'create-store.php';
    }
    
    

    public function createRecheckShopRoundAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'create-recheck-shop-round.php';
    }

    public function saveCreateRecheckShopRoundAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'save-create-recheck-shop-round.php';
    }

    public function listRecheckShopRoundAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'list-recheck-shop-round.php';
    }

    public function listRecheckShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'list-recheck-shop.php';
    }

    public function recheckShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'recheck-shop.php';
    }

    public function saveRecheckShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR .'save-recheck-shop.php';
    }

    public function getHistoryRecheckShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-history-recheck-shop.php';
    }

    public function viewRecheckShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'view-recheck-shop.php';
    }

    public function assignAreaRecheckShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'assign-area-recheck-shop.php';
    }

    public function saveAssignAreaRecheckShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-assign-area-recheck-shop.php';
    }

    public function listStoreOutsiteAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'list-store-outsite.php';
    }

    public function checkShopOutsideListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-outside-list.php';
    }

    public function createCheckShopOutsideAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-check-shop-outside.php';
    }

    public function saveCreateCheckShopOutsideAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-create-check-shop-outside.php';
    }

    public function getHistoryCheckShopOutsideAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-history-check-shop-outside.php';
    }

    public function viewCheckShopOutsideAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'view-check-shop-outside.php';
    }


    public function costStatisticAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'cost-statistic.php';
    }

    public function createAdditionalAirCostAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-additional-air-cost.php';
    }

    public function saveCreateAdditionalAirCostAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-create-additional-air-cost.php';
    }

    public function saveEditAdditionalAirCostAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-edit-additional-air-cost.php';
    }

    public function createAdditionalPosmCostAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-additional-posm-cost.php';
    }

    public function saveEditAdditionalPosmCostAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-edit-additional-posm-cost.php';
    }

    public function assignAdditionalCategoryAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'assign-additional-category.php';
    }

    public function createAssignAdditionalCategoryAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-assign-additional-category.php';
    }

    public function saveAssignAdditionalCategoryAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-assign-additional-category.php';
    }

    public function getInfoWarehouseAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-info-warehouse.php';
    }

    public function listAdditionalPartAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'list-additional-part.php';
    }

    public function createAdditionalPartAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-additional-part.php';
    }

    public function saveCreateAdditionalPartAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-create-additional-part.php';
    }

    public function createAdditionalPartCostAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'create-additional-part-cost.php';
    }

    public function saveEditAdditionalPartCostAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-edit-additional-part-cost.php';
    }

    public function updateInfoShopListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'update-info-shop-list.php';
    }

    public function costStatisticOrderAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'cost-statistic-order.php';
    }

    public function costStatisticPosmAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'cost-statistic-posm.php';
    }

    public function checkInventoryAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-inventory.php';
    }

    public function checkInventorySaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-inventory-save.php';
    }

    public function checkInventoryListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-inventory-list.php';
    }

    public function checkInventoryViewAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-inventory-view.php';
    }

    public function checkInventoryPosmAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-inventory-posm.php';
    }

    public function checkInventoryPosmSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-inventory-posm-save.php';
    }

    public function checkInventoryPosmViewAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-inventory-posm-view.php';
    }

    public function getCategoryInventoryTypeAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-category-inventory-type.php';
    }

    public function searchInMapAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'search-in-map.php';
    }

    public function getInfoCheckshopMapAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-info-checkshop-map.php';
    }

    public function getCategoryPosmChildAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-category-posm-child.php';
    }

    public function updateCheckshopInfoDetailAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'update-checkshop-info-detail.php';
    }

    public function updateCheckshopInfoListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'update-checkshop-info-list.php';
    }

    public function updateCheckshopInfoSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'update-checkshop-info-save.php';
    }
    public function getDetailCheckshopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-detail-checkshop.php';
    }

    // đặt hàng thi công
    public function orderAirStageCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-stage-create.php';
    }

    public function orderAirStageSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-stage-save.php';
    }

    public function orderAirStageListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-stage-list.php';
    }

    public function orderAirAreaListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-area-list.php';
    }
    public function orderAirAreaConfirmAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-area-confirm.php';
    }

    public function guidelineListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-list.php';
    }

    public function guidelineSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-save.php';
    }

    public function guidelineEditAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-edit.php';
    }

    public function guidelineViewAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-view.php';
    }

    public function guidelineDeleteFileAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-delete-file.php';
    }

    public function guidelineDownloadFileAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-download-file.php';
    }

    public function guidelineCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-create.php';
    }

    public function guidelineSaveCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'guideline-save-create.php';
    }

//
    public function confirmAllPosmAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'confirm-all-posm.php';
    }

    public function saveContractNumberTransferAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-contract-number-transfer.php';
    }

    public function saveContractNumberDestructionAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-contract-number-destruction.php';
    }

    public function saveFacadeSizeShopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-facade-size-shop.php';
    }

    public function updateCheckshopInfoMassUploadAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'update-checkshop-info-mass-upload.php';
    }

    public function saveContractNumberRepairAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-contract-number-repair.php';
    }

    public function saveContractNumberAirAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-contract-number-air.php';
    }




    public function deleteImageOnServerAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'delete-image-on-server.php';
    }

    public function resizeImageOnServerAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'resize-image-on-server.php';
    }


    public function checkInCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-in-create.php';
    }

    public function checkInSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-in-save.php';
    }

    public function checkInListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-in-list.php';
    }
    public function checkInViewAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-in-view.php';
    }


    public function getHistoryCheckInAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-history-check-in.php';
    }

    public function orderAirAreaListHoAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-area-list-ho.php';
    }

    public function orderAirAreaConfirmHoAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-area-confirm-ho.php';
    }

    public function orderAirAreaUpdateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'order-air-area-update.php';
    }

    public function assignAreaAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'assign-area.php';
    }
    public function saveAssignAreaAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-assign-area.php';
    }

    public function checkLinkNotificationAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-link-notification.php';
    }


    public function testSendNotiAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $QAppNoti = new Application_Model_AppNoti();
        $data_noti = [
            'title' => "IT TEST",
            'message' => "IT TEST nội dung",
            'link' => "/trade/air-edit?id=" . 145,
            'staff_id' => 557
        ];

        $QAppNoti->sendNotification($data_noti);
    }


    public function assignStoreRecheckShopMassUploadAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'assign-store-recheck-shop-mass-upload.php';
    }

    public function assignStoreRecheckShopSaveMassUploadAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'assign-store-recheck-shop-save-mass-upload.php';
    }


    public function checkShopEntireImageCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-image-create.php';
    }

    public function checkShopEntireImageSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-image-save.php';
    }

    public function checkShopEntireProductCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-product-create.php';
    }

    public function checkShopEntireProductSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-product-save.php';
    }

    public function checkShopEntireListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-list.php';
    }
    public function checkShopEntireViewAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-view.php';
    }

    public function getHistoryCheckShopEntireAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-history-check-shop-entire.php';
    }

    public function checkShopEntireUpdateImageRepairAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-update-image-repair.php';
    }

    public function checkShopEntireStatisticAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-shop-entire-statistic.php';
    }



// standard image
    public function saveStandardImageAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-standard-image.php';
    }

    public function deleteStandardImageAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'delete-standard-image.php';
    }

    public function getStandardImageAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-standard-image.php';
    }

    public function repairCreateLocalAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'repair-create-local.php';
    }

    public function getStoreByAreaAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'get-store-by-area.php';
    }

    public function removeShopLocalAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'remove-shop-local.php';
    }

    public function paginateRepairAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'paginate-repair.php';
    }




    public function changePictureStageCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-stage-create.php';
    }

    public function changePictureStageListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-stage-list.php';
    }

    public function changePictureStageSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-stage-save.php';
    }

    public function changePictureStageStartAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-stage-start.php';
    }

    public function changePictureDownloadFileAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-download-file.php';
    }

    public function changePictureDownloadMassUploadAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-download-mass-upload.php';
    }

    public function changePictureSaveMassUploadAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-save-mass-upload.php';
    }

    public function changePictureStatisticAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-statistic.php';
    }

    public function changePictureGetStatisticStoreSaleAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-get-statistic-store-sale.php';
    }

    public function changePictureGetStatisticCategorySaleAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-get-statistic-category-sale.php';
    }

    public function changePictureExportDetailAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'change-picture-export-detail.php';
    }




    // kiểm tra posm
    public function checkPosmStageCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-stage-create.php';
    }

    public function checkPosmStageSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-stage-save.php';
    }

    public function checkPosmStageListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-stage-list.php';
    }

    public function checkPosmDownloadTemplateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-download-template.php';
    }

    public function contractNumberListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contract-number-list.php';
    }

    public function contractNumberDetailAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contract-number-detail.php';
    }
    
    public function contractNumberDetailPosmAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contract-number-detail-posm.php';
    }

    public function contractToPurchasingAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'contract-to-purchasing.php';
    }

    public function checkPosmStageStatisticAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-stage-statistic.php';
    }

    public function checkPosmExportDetailAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-export-detail.php';
    }

    public function checkPosmGetStatisticCategorySaleAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-get-statistic-category-sale.php';
    }

    public function checkPosmGetStatisticStoreSaleAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'check-posm-get-statistic-store-sale.php';
    }

    public function saveCategoryPosmAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-category-posm.php';
    }

    public function saveStoreTempStopAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-store-temp-stop.php';
    }

    // đánh giá năng lực shop
    public function storeCapacityStageCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-stage-create.php';
    }

    public function storeCapacityStageSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-stage-save.php';
    }

    public function storeCapacityStageListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-stage-list.php';
    }

    public function storeCapacityStageStatisticAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-stage-statistic.php';
    }

    public function storeCapacityMassUploadAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-mass-upload.php';
    }

    public function storeCapacityMassUploadSaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-mass-upload-save.php';
    }

    public function storeCapacityStatisticAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-statistic.php';
    }

    public function storeCapacityExportAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'store-capacity-export.php';
    }

    public function saveShopSizeAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-shop-size.php';
    }
    
    public function saveFlashSaleAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'save-flash-sale.php';
    }

    public function additionCategoryCreateAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'addition-category-create.php';
    }

    public function additionCategorySaveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'addition-category-save.php';
    }

    public function additionCategoryListAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'addition-category-list.php';
    }
    public function additionCategoryDetailAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'addition-category-detail.php';
    }

    public function additionCategoryApproveAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'addition-category-approve.php';
    }

    public function ajaxGetStoreByAreaAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'ajax-get-store-by-area.php';
    }

    public function ajaxCategoryByStoreAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'ajax-get-category-by-store.php';
    }

    public function ajaxGetCategoryByStoreAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'ajax-get-category-by-store.php';
    }

    public function confirmQuantityRealmeAction()
    {
        require_once 'trade' . DIRECTORY_SEPARATOR . 'confirm-quantity-realme.php';
    }

    //Author : Trang
    public function checkshopQcRealmeAction(){
        require_once 'trade' . DIRECTORY_SEPARATOR . 'checkshop' . DIRECTORY_SEPARATOR . 'checkshop-qc-realme.php';
    }

    public function checkShopQcViewAction(){
        require_once 'trade' .  DIRECTORY_SEPARATOR . 'check-shop-qc-view.php';
    }

    public function checkShopQcSaveAction(){
        require_once 'trade' .  DIRECTORY_SEPARATOR . 'check-shop-qc-save.php';
    }

    public function checkShopQcListAction(){
        require_once 'trade' .  DIRECTORY_SEPARATOR . 'check-shop-qc-list.php';
    }


    public function constructObsCreateAction(){
        require_once 'trade' .  DIRECTORY_SEPARATOR . 'construct-obs-create.php';
    }

    public function constructObsEditAction(){
        require_once 'trade' .  DIRECTORY_SEPARATOR . 'construct-obs-edit.php';
    }

    public function constructObsApproveAction(){
        require_once 'trade' .  DIRECTORY_SEPARATOR . 'construct-obs-approve.php';
    }

    public function constructObsListAction(){
        require_once 'trade' .  DIRECTORY_SEPARATOR . 'construct-obs-list.php';
    }


    public function scanCodeAction()
    {
      
        
    }


}
