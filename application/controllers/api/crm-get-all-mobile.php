<?php
    header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    error_reporting(1);
    require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
    $this->wssURI = 'http://cs.opposhop.vn/wss?wsdl';
    $this->namespace = 'OPPOVN';

    $client = new nusoap_client($this->wssURI);
    $client->soap_defencoding = 'UTF-8';
    $client->decode_utf8 = true;

    $wsParams = array(
    
    );
    $result = $client->call("getListPartShow", $wsParams);

	if (!empty($result)) {
                    $list_group = [];
                    foreach ($result as $value) {
                        $list_group[trim(strtoupper($value['model']))][] = (array) $value;
                    }
                }
                
        $arrkey = array_keys($list_group);
        $str_insert = '';
        foreach ($arrkey as   $param) {
            $str_insert .= "'" . $param . "'" . ',';
        }

        $str_rows = rtrim($str_insert, ',');

        $res                = file_get_contents('php://input');
        $result             = json_decode($res,true);
        $db     = Zend_Registry::get('db');
        $sql    = "SELECT * FROM warehouse.good where del = 0 and UPPER(`desc`) IN ($str_rows)";
            $stmt = $db->prepare($sql);
            $stmt->execute();

            $list = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;
                
            if(!empty($list)){
                    $info = array();
                    foreach ($list as $key => $value) {
                        $info[] = array(
                            'phoneId'   => $value['id'],
                            'phoneName'   => $value['name'],
                            'phoneDesc'   => $value['desc']
                        );
                    }
                echo json_encode(array(
                    'code' => 200,
                    'data' => $info,
                    'msg' => 'success'
                ));
            }