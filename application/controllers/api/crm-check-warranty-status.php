<?php
		header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    
    $res                = file_get_contents('php://input');
    $result             = json_decode($res,true);
    $imeiNumber         = $result['imeiNumber'];
    $phoneNumber        = $result['phoneNumber'];
    $warrantyCardNumber = $result['warrantyCardNumber'];
        
    if(empty($imeiNumber) || empty($phoneNumber) || empty($warrantyCardNumber)){
        echo json_encode(array(
                'code' => 0,
                'data' => 0,
                'msg' => 'imeiNumber or phoneNumber or warrantyCardNumber Ịnvalid'
            ));
        exit();
    }
    error_reporting(1);
    require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
    $this->wssURI = 'http://cs.opposhop.vn/wss?wsdl';
    $this->namespace = 'OPPOVN';

    $client = new nusoap_client($this->wssURI);
    $client->soap_defencoding = 'UTF-8';
    $client->decode_utf8 = true;

    $wsParams = array(
    'WarrantyID' => $warrantyCardNumber
    );
    $result = $client->call("searchIMEIStatusChatBot", $wsParams);

    if(!empty($result['data'])){
                $data = 0;
         if (in_array($result['data'], array('Fixed Waiting Return To Customer')))
                $data = 1;
            elseif (in_array($result['data'], array('Fixed Returned To Customer', 'No Fixed Returned To Customer')))
                $data = 2;
            elseif (!in_array($result['data'], array('Warranty Exception')))
                $data = 3;
            
        echo json_encode(array(
                'code' => 200,
                'data' => $data,
                'msg' => $result['data']
            ));
        exit();
    } else {
        echo json_encode(array(
                'code' => 0,
                'data' => 0,
                'msg' => 'imeiNumber or phoneNumber or warrantyCardNumber Ịnvalid'
            ));
        exit();
    }
