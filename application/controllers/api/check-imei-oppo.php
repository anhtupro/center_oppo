<?php
    header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
  
    $res                = file_get_contents('php://input');
    $result             = json_decode($res,true);
    $imei               = intval($result['imei']);
    $staff_id           = intval($result['staff_id']);
    $timing_date        = $result['timing_date'];
    $store_id_rm        = intval($result['store_id']);

    $db = Zend_Registry::get('db');
    $QStoreMapping      = new Application_Model_StoreMapping();
    $QWebImei           = new Application_Model_WebImei();
    $QStore             = new Application_Model_Store();
   
    $QTimingSaleOppo  = new Application_Model_TimingSaleOppo();
    $store_cache = $QStore->get_cache();

    if(empty($imei) || empty($staff_id) || empty($timing_date) || empty($store_id_rm)){
        echo json_encode(array(
                'code' => 0,
                'data' => 0,
                'msg' => 'Input Ịnvalid'
            ));
        exit();
    }else{
        
        
        // 1. Check Ton tai imei
        $where_imei = $QWebImei->getAdapter()->quoteInto('imei_sn = ?', $imei);
        $imei_check  = $QWebImei->fetchRow($where_imei);
        if(empty($imei_check)){
            echo json_encode(array(
                'code' => 1,
                'data' => 0,
                'msg' => 'Imei này không tồn tại'
            ));
            exit();
        }

        //.24. check store còn hoạt động và đã map store
        $store_is_valid = $QStoreMapping->checkStoreIsValid($store_id_rm);

        if(! $store_is_valid){
            echo json_encode(array(
                'code' => 24,
                'data' => 0,
                'msg' => 'Store chưa được liên kết vối hệ thống OPPO hoặc store đã bị đóng. Vui lòng liên hệ Admin để được hỗ trợ'
            ));
            exit();
        }

        // 2. Check & get Store_id OPPO
        $where_store = $QStoreMapping->getAdapter()->quoteInto('id_rm = ?', $store_id_rm);
        $store_check  = $QStoreMapping->fetchRow($where_store);
        if(empty($store_check)){
            echo json_encode(array(
                'code' => 2,
                'data' => 0,
                'msg' => 'Không tìm thấy cửa hàng này'
            ));
            exit();
        }
        $store_id_oppo = $store_check['id_oppo']; // Store_id OPPO
        
        //3. check cửa hàng hợp lệ
        if (!isset($store_cache[$store_id_oppo])) {
            echo json_encode(array(
                'code' => 3,
                'data' => 0,
                'msg' => 'Cửa hàng không hợp lệ.'
            ));
            exit();
        }
        // 4. cửa hàng TGDĐ
        if (defined("PREVENT_TIMING_AT_TGDD") && PREVENT_TIMING_AT_TGDD && preg_match('/^TGDĐ[\s]?-/', $store_cache[$store_id_oppo])){
             echo json_encode(array(
                'code' => 4,
                'data' => 0,
                'msg' => 'Cửa hàng không hợp lệ. Không được báo ở cửa hàng TGDĐ'
            ));
            exit();
        }
        //5. Check format timing_date
        $new_date = DateTime::createFromFormat('Y-m-d', $timing_date);
        if (!$new_date) {
            echo json_encode(array(
                'code' => 5,
                'data' => 0,
                'msg' => 'Vui lòng chọn đúng định dạng ngày báo cáo.'
            ));
            exit();
        }
        // 20. Check bao truoc
        if (strtotime($timing_date) > strtotime(date('Y-m-d'))) {
            echo json_encode(array(
                'code' => 20,
                'data' => 0,
                'msg' => 'Bạn không thể báo cáo cho trước ngày hiện tại! Vui lòng xem lại ngày.'
            ));
            exit();
        }
        // 20. Check bao sau 2 ngay
        $time_limit_timing = TIME_LIMIT_TIMING;
            if (strtotime(date('Y-m-d')) - strtotime($timing_date) > $time_limit_timing) {
            $day_limit = ceil($time_limit_timing / 86400);
            
            echo json_encode(array(
                'code' => 21,
                'data' => 0,
                'msg' => 'Bạn chỉ có thể báo cáo cho hôm nay và ' . $day_limit . ' ngày trước.'
            ));
            exit();
           
        }
        
        // 6. Check imei du an 
        $QTimimigCheck = new Application_Model_TimingCheck();
        $checkImeiDuAn = $QTimimigCheck->checkImeiDuAn($imei);
        if (isset($checkImeiDuAn) && $checkImeiDuAn && $checkImeiDuAn['flag'] == 0) {
           // $QTimimigCheck->insertImeiDuAnLog($staff_id, $imei, $store);
             echo json_encode(array(
                'code' => 6,
                'data' => 0,
                'msg' => 'Imei này thuộc diện ' . $checkImeiDuAn['note'] . ' . Công ty không tính doanh số với các imei thuộc diện này. Vui lòng liên hệ Tech team nếu có thắc mắc.'
            ));
            exit();
        }
        
        // 7. Check imei
        $info = array();
        $return = $this->checkImei($imei,  null, $info,$tool = null, $reimport = false, $staff_id);
        
         if ($return == 1) {
                
                echo json_encode(array(
                    'code' => 7,
                    'data' => 0,
                    'msg' => 'Bạn đã báo cáo IMEI: ' . $imei . ' rồi, vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Tại cửa hàng [' . $info['store'] . ']'
                ));
            exit();
               
            } elseif ($return == 2) {
                echo json_encode(array(
                    'code' => 8,
                    'data' => 0,
                    'msg' => "IMEI ' . $imei . ' không tồn tại. Vui lòng xem kỹ lại IMEI hoặc liên hệ bộ phận Kỹ thuật."
                ));
            exit();
               
            } elseif ($return == 3 || $return == 5) {
                echo json_encode(array(
                        'code' => 9,
                        'data' => 0,
                        'msg' => 'IMEI '
                . $imei . ' này đã được báo cáo lúc ['
                . date('d/m/Y H:i:s', strtotime($info['date']))
                . ']'
                    ));
                exit();
                echo '<script>
                                parent.palert("IMEI '
                . $imei . ' này đã được báo cáo lúc ['
                . date('d/m/Y H:i:s', strtotime($info['date']))
                . '] <a href=\"#\" data-staff-first=\"'
                . $info['staff_id'] . '\" data-timing-sales-id=\"'
                . $info['timing_sales_id'] . '\" data-imei=\"'
                . $imei . '\" data-case=\"IMEI '
                . $imei . ' này đã được báo cáo lúc ['
                . date('d/m/Y H:i:s', strtotime($info['date'])) . ']\" data-checksum=\"' . sha1(md5($imei) . $info['timing_sales_id']) . '\" class=\"send_notify\">Bấm vào đây</a> để gửi yêu cầu xử lý.");
                                parent.alert("IMEI ' . $imei . ' đã  báo cáo vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Cuộn lên đầu trang để chọn chức năng gửi báo cáo trùng IMEI.");
                            </script>';
                exit();
            } elseif ($return == 4) {
                $QImeiHappyTime = new Application_Model_ImeiHappyTime();
                $dataCheckImei  = $QImeiHappyTime->checkImei($imei);
                if (empty($dataCheckImei)) {
                    echo json_encode(array(
                        'code' => 10,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày, vào ngày [' . $info['activated_at'] . ']. Công ty không tính doanh số đối với các máy đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                    exit();
                }
            } elseif ($return == 6) {
                echo json_encode(array(
                        'code' => 11,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' của máy tặng khách hàng hoặc khóa chấm công, thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales nếu có thắc mắc.'
                    ));
                exit();
            } elseif ($return == 7) {
                echo json_encode(array(
                        'code' => 12,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                exit();
               
            } elseif ($return == 8) {
                echo json_encode(array(
                        'code' => 13,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                exit();
                
            } elseif ($return == 9) {
                 echo json_encode(array(
                        'code' => 14,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                exit();
              
            } elseif ($return == 5) {
                echo json_encode(array(
                        'code' => 15,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Bởi [' . $info['staff'] . '] ở cửa hàng [' . $info['store'] . '] vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . ']. Công ty không tính doanh số đối với các máy đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                exit();
            } elseif ($return == 13) {
//                echo json_encode(array(
//                        'code' => 15,
//                        'data' => 0,
//                        'msg' => 'IMEI ' . $imei . ' chưa được set mapping vui lòng liên hệ Tech Team.'
//                    ));
//                exit();
                
            } elseif ($return == 14) {
//                echo json_encode(array(
//                        'code' => 16,
//                        'data' => 0,
//                        'msg' => 'IMEI ' . $imei . ' của OPPO không tồn tại.'
//                    ));
//                exit();
               
            } elseif ($return == 999) {
//                $QBlock = new Application_Model_BlockCheckingImei();
//                $data   = array(
//                    'staff_id' => $userStorage->id,
//                    'date'     => date('Y-m-d'),
//                );
//
//                try {
//                    @$QBlock->insert($data);
//                } catch (Exception $e) {
//                    
//                }
//
//                $auth = Zend_Auth::getInstance();
//                $auth->clearIdentity();
//                session_destroy();
//
//                echo '<script>
//                            parent.palert("' . sprintf('Bạn đã thử sai hơn %s lần trong 1 ngày.', IMEI_CHECKING_FAILED_IN_1_DAYS) . '");
//                            parent.alert("' . sprintf('Bạn đã thử sai hơn %s lần trong 1 ngày.', IMEI_CHECKING_FAILED_IN_1_DAYS) . '");
//                        </script>';
//                exit;
            }
            
            $checkImei    = $QTimimigCheck->checkImeiDealer($imei, $store_id_oppo);
            $checkImeiTA  = $QTimimigCheck->checkImeiTranAnh($imei, $store_id_oppo);
            $checkImeiBrs = $QTimimigCheck->checkImeiBrandshop($imei, $store_id_oppo);
            $checkImeiVinPro = $QTimimigCheck->checkImeiVinpro($imei);
            
            if(!empty($checkImeiVinPro)){
                echo json_encode(array(
                        'code' => 17,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' thuộc cửa hàng VinPro. Bạn không thể báo Imei này ở cửa hàng không thuộc VinPro . Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                exit();
            }
            
            if (isset($checkImeiBrs) && $checkImeiBrs && $checkImeiBrs['check_sell_in'] == 1 && $checkImeiBrs['check_brs'] == 1 && $checkImeiBrs['is_brand_shop'] != 1 && $checkImeiBrs['type'] != 1) {
                echo json_encode(array(
                        'code' => 18,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' thuộc cửa hàng Brandshop. Bạn không thể báo Imei này ở cửa hàng không thuộc Brandshop . Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                exit();
            }
            
            if (isset($checkImeiTA) && $checkImeiTA && $checkImeiTA['dealer'] == 1 && $checkImeiTA['store'] == 1) {
                # code...
            } elseif (empty($checkImei['status'])) {
                echo json_encode(array(
                        'code' => 19,
                        'data' => 0,
                        'msg' => 'IMEI ' . $imei . ' không thuộc cửa hàng này. Có thể đây là máy mượn/chuyển giữa các cửa hàng. Công ty không tính doanh số với các máy được báo cáo sai cửa hàng. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.'
                    ));
                exit();
            }

        $where_imei_rm = $QTimingSaleOppo->getAdapter()->quoteInto('imei = ?', $imei);
        $imei_check_rm  = $QTimingSaleOppo->fetchRow($where_imei_rm);
        if(!empty($imei_check_rm)){
            echo json_encode(array(
                'code' => 23,
                'data' => 0,
                'msg' => 'Bên Realme đã báo '
            ));
            exit();
        }    
        
    
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('CALL PR_API_Check_Imei_OPPO (:p_imei, :p_store_id, :p_timing_date,:p_staff_id) ');
        $stmt->bindParam('p_imei', $imei, PDO::PARAM_INT);
        $stmt->bindParam('p_store_id',$store_id, PDO::PARAM_INT);
        $stmt->bindParam('p_timing_date', $timing_date, PDO::PARAM_INT);
        $stmt->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
       
        echo json_encode(array(
                'code' => 200,
                'data' => $result,
                'msg' => 'success'
            ));
        exit();
    }
    
    
    
