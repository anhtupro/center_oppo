<?php
		header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    
    $res            = file_get_contents('php://input');
    $result         = json_decode($res,true);
    $phoneId        = $result['phoneId'];
    $db             = Zend_Registry::get('db');
    
    $sql = "SELECT `p`.*, IFNULL(g.`desc`, p.model) AS `desc` FROM `cs_new`.`part` AS `p`
            LEFT JOIN `warehouse`.`good` AS `g` ON (g.name = p.model OR g.`desc` = p.model) WHERE (p.show_website = 1) and  g.id =  $phoneId";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
    
   
        if(!empty($list)){
            $info = array();
            foreach ($list as $key => $value) {
                $info[] = array(
                    'componentsName'   => $value['name'],
                    'componentsPrice'   => $value['price']
                );
            }
            echo json_encode(array(
                'code' => 200,
                'data' => $info,
                'msg' => 'success'
            ));
        }