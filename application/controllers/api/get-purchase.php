<?php
    header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    
    $res                = file_get_contents('php://input');
    $result             = json_decode($res,true);
    $from_date          = $result['start'];
    $to_date            = $result['end'];

    $db = Zend_Registry::get('db');
    
    if(empty($from_date) || empty($to_date)){
        echo json_encode(array(
                'code' => 0,
                'data' => 0,
                'msg' => 'Input Ịnvalid'
            ));
        exit();
    }
//    $from_date = '2019-11-01'; 
//    $to_date = '2019-11-30'; 
  
    $sql    =   "SELECT 
                    'Viet Nam' as 'country',
                    'OPPO' as 'brand',
                    re.`name` as 'purchasing_of_Area',
                    p.urgent_date as 'purchasing_of_time',
                    `de`.name as 'request_of_department',
                    `j`.`name` as 'project_name',
                    `pt`.`name` as 'product_category',
                    c.`name` as 'product_name',
                    c.`desc` as 'product_description',
                    `prs`.`price`*0.000043 as 'unit_price',
                    `prd`.`quantity` as 'total_quantity',
                    `su`.title as 'supplier_name',
                    `prd`.`note`
                FROM
                    purchasing_request AS `p`
                    LEFT JOIN `purchasing_request_details` AS `prd` ON prd.pr_id = p.id
                    LEFT JOIN ".DATABASE_SALARY.".`purchasing_request_supplier` AS `prs` ON prs.pr_detail_id = prd.id AND prs.select_supplier = 1
                    LEFT JOIN `pmodel_code` AS `c` ON c.id = prd.pmodel_code_id
                    LEFT JOIN `purchasing_type` AS `pt` ON pt.id = c.category_id
                    LEFT JOIN `purchasing_project` AS `j` ON j.id = p.project_id
                    LEFT JOIN `staff` AS `s` ON s.id = p.created_by
                    LEFT JOIN `staff` AS `s2` ON s2.id = p.confirm_by
                    LEFT JOIN `staff` AS `s3` ON s3.id = p.approved_by
                    LEFT JOIN `area` AS `re` ON re.id = p.area_id
                    LEFT JOIN `team` AS `de` ON de.id = p.department_id
                    LEFT JOIN `supplier` AS `su` ON su.id = prs.supplier_id
                WHERE
                    (p.del = 0) AND p.`status` = 3
                     AND (p.`urgent_date` BETWEEN '$from_date' AND '$to_date')    
                ORDER BY
                    `p`.`created_at` DESC";
    
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $list = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = $db = null;
    if(!empty($list)){
         echo json_encode(array(
                'code' => 200,
                'data' => $list,
                'msg' => 'success'
            ));
            exit();
    }else{
         echo json_encode(array(
                'code' => 0,
                'data' => '',
                'msg' => 'In valid'
            ));
            exit();
    }
 