<?php
		header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    
    $res            = file_get_contents('php://input');
    $result         = json_decode($res,true);
  
    $db             = Zend_Registry::get('db');
    $info = array();
    
    $sql1 = "SELECT name centerName, address location, phone phoneNumber, description businessHours, email FROM core_services where area = 1";
    $stmt1 = $db->prepare($sql1);
    $stmt1->execute();
    $list1 = $stmt1->fetchAll();
    $stmt1->closeCursor();
    $info[] = array(
        'area'  => 'Trung tâm CSKH OPPO MIỀN BẮC',
        'result'  => $list1,
    );
    $sql2 = "SELECT name centerName, address location, phone phoneNumber, description businessHours, email FROM core_services where area = 2";
    $stmt2 = $db->prepare($sql2);
    $stmt2->execute();
    $list2 = $stmt2->fetchAll();
    $stmt2->closeCursor();
    $info[] = array(
        'area'  => 'Trung tâm CSKH OPPO MIỀN TRUNG & TÂY NGUYÊN',
        'result'  => $list2,
    );
     $sql3 = "SELECT name centerName, address location, phone phoneNumber, description businessHours, email FROM core_services where area = 3";
    $stmt3 = $db->prepare($sql3);
    $stmt3->execute();
    $list3 = $stmt3->fetchAll();
    $stmt3->closeCursor();
    $info[] = array(
        'area'  => 'Trung tâm CSKH OPPO MIỀN NAM',
        'result'  => $list3,
    );
   
        if(!empty($info)){
            echo json_encode(array(
                'code' => 200,
                'data' => $info,
                'msg' => 'success'
            ));
            exit();
        }