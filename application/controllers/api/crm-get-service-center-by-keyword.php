<?php
		header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    
    $res            = file_get_contents('php://input');
    $result         = json_decode($res,true);
    $keyword         = $result['keywords'];
    $db             = Zend_Registry::get('db');
    $info = array();
    
    $sql1 = "SELECT name centerName, address location, phone phoneNumber, description businessHours, email FROM core_services where (name LIKE '%' :keyword '%' or address LIKE '%' :keyword '%' or  phone LIKE '%' :keyword '%') = 1";
    $stmt1 = $db->prepare($sql1);
    $stmt1->bindParam('keyword', $keyword, PDO::PARAM_STR);
    $stmt1->execute();
    $list1 = $stmt1->fetchAll();
    $stmt1->closeCursor();
   
    if(!empty($list1)){
        echo json_encode(array(
            'code' => 200,
            'data' => $list1,
            'msg' => 'success'
        ));
        exit();
    }else{
        echo json_encode(array(
                'code' => 0,
                'data' => 0,
                'msg' => 'keywords Ịnvalid'
            ));
        exit();
    }