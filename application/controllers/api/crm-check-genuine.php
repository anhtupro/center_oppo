<?php
    header("Access-Control-Allow-Origin: *");
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    
    $res                = file_get_contents('php://input');
    $result             = json_decode($res,true);
    $imeiNumber         = $result['imeiNumber'];

    $db = Zend_Registry::get('db');
    
    if(empty($imeiNumber) ){
        echo json_encode(array(
                'code' => 0,
                'data' => 0,
                'msg' => 'imeiNumber Ịnvalid'
            ));
        exit();
    }
    error_reporting(1);
    require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
    $this->wssURI = 'https://warehouse.opposhop.vn/wss?wsdl';
    $this->namespace = 'OPPOVN';

    $client = new nusoap_client($this->wssURI);
    $client->soap_defencoding = 'UTF-8';
    $client->decode_utf8 = false;

    $wsParams = array(
    'imeiNumber' => $imeiNumber
    );
    $result = $client->call("getImeiInfoCheck", $wsParams);

    if($result['code'] == 0){
        print_r(json_encode(array(
                'code' => 200,
                'data' => array(
                                'imei' => $imeiNumber,
                                'product' => $result['good_name'],
                                'color' => $result['good_color'],
                                'pay' => $result['installment'],
                                'result' => 'Hàng chính hãng'
                        ),
                'msg' => 'Hàng chính hãng'
            ))) ;
        exit();
    }else{
          echo json_encode(array(
                'code' => 0,
                'data' => 0,
                'msg' => 'imeiNumber Ịnvalid'
            ));
        exit();
    }
    
                
    
