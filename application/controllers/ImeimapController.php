<?php

class ImeimapController extends My_Controller_Action{

	public function init(){}

	public function indexAction(){
		$im = new Application_Model_Imeimap;
		$res = $im->GetAll();
		// $res = $im->CheckImei(array('imei' => '865425020589548', 'store_id' => '15927'));
		// echo '<pre>';print_r($res);die;
		$this->view->imeimap = $res;

		$flashMessenger = $this->_helper->flashMessenger;
        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
        $this->view->messages_error = $messages_error;
	}

	
	public function updateAction()
	{
		$flashMessenger = $this->_helper->flashMessenger;
		$status = $this->getRequest()->getParam('status', 0);
		$id = $this->getRequest()->getParam('id');

		if(!empty($id))
		{
			$params = array(
				'status' => $status,
				'id' => $id
			);
			$im = new Application_Model_Imeimap();
			$im->updateImeimap($params);
			$flashMessenger->setNamespace('success')->addMessage("Tạo thành công.");
        	$this->redirect('/imeimap');
		}
	}

	public function createAction()
	{
		
		$flashMessenger = $this->_helper->flashMessenger;
		$distributor_id = $this->getRequest()->getParam('distributor_id');
		$get_imei_from = $this->getRequest()->getParam('get_imei_from');
		$status = $this->getRequest()->getParam('status', 0);
		$submit = $this->getRequest()->getParam('submit');
		$id = $this->getRequest()->getParam('id');

		if(!empty($submit))
		{
			$params = array(
				'distributor_id' => $distributor_id,
				'get_imei_from' => $get_imei_from,
				'status' => $status
			);
			$im = new Application_Model_Imeimap();
			$im->insertImeimap($params);
			$flashMessenger->setNamespace('success')->addMessage("Tạo thành công.");
        	$this->redirect('/imeimap');
		}

        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
        $this->view->messages_error = $messages_error;
	}

	public function ajaxGetDistributorAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		if($this->getRequest()->isXmlHttpRequest())
		{
			$name = $this->getRequest()->getParam('name');
			$im = new Application_Model_Imeimap();
			if(!empty($name))
			{
				$params = array("name" => $name);
				$data = $im->GetWarehouseDistributor($params);

				echo json_encode($data);
				die;
			}
			die;
		}
	}

}