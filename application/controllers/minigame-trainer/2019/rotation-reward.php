<?php
$this->_helper->layout->disableLayout();
$QMiniGame2018 			= new Application_Model_MiniGame2018();
$QMiniGame2018Detail 	= new Application_Model_MiniGame2018Detail();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
$staff_id 	= $userStorage->id;
$params = [
	'round'		=> MINIGAME_ROUND,
 	'staff_id'	=> $staff_id
 ];

if (!in_array($staff_id, array(22813, 6705, 5899)))
        $this->redirect($url);
// $data = $QMiniGame2018->getReward($params);
 $data = $QMiniGame2018->getRotationReward($params);
 $this->view->data = $data;

$this->_helper->viewRenderer->setRender('2019/rotation-reward');