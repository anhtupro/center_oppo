<?php
$this->_helper->layout->disableLayout();
$QMiniGame2018 			= new Application_Model_MiniGame2018();
$QMiniGame2018Detail 	= new Application_Model_MiniGame2018Detail();
$QMiniGame2018Answer 	= new Application_Model_MiniGameAnswer2018();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
$user_id 	= $userStorage->id;
if(empty($userStorage)){
    $url = HOST . 'user/login?b='.urldecode('/minigame-trainer/list-answer');
    $this->redirect($url);
}
$params = [
    'staff_id'	=> $user_id,
    'round' =>MINIGAME_ROUND
];

$from_time = MINIGAME_ANSWER_FROM_TIME; //thời gian bắt đầu làm
$to_time = "2019-11-22 12:30:00"; //thời gian kết thúc game
$current_time = date("Y-m-d H:i:s");

if (!in_array($user_id, array(5899)))
    if ($current_time < $from_time) {
        $url = HOST . 'minigame-trainer/index?answer_soon=1';
        $this->redirect($url);
    }
//if (!in_array($user_id, array(22813, 6705, 5899)))
//    if ($current_time > $to_time) {
//        $url = HOST . 'minigame/index?late=1';
//        $this->redirect($url);
//    }

$xml 	= simplexml_load_file(APPLICATION_PATH.MINIGAME_QUESTION_FILE);
$question 	= json_decode(json_encode($xml, true));
//print "<pre>".print_r($question);die;
$lst_answer = $QMiniGame2018Answer->getListAnswer($params);
$lst_correct_answers = array();
foreach ($lst_answer as $key => $value) {
    $lst_correct_answers[$value['question_id']] = $value['answer'];
}

// foreach ($question->question as $key => $value) {
// 	foreach ($data as $k => $val) {
// 		if($value->id == $val['question_id']){

// 		}
// 	}
// }
//print "<pre>".print_r($lst_correct_answers);die;
$this->view->question = $question;
$this->view->lst_correct_answers = $lst_correct_answers;
// /var_dump($question->question[1]->id); exit;
$data = $QMiniGame2018->getResult($params);

$this->view->data = $data;
$this->_helper->viewRenderer->setRender('2019/list-answer');