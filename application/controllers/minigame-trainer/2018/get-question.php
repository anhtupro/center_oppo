<?php
 $question_id			= $this->getRequest()->getParam('question_id');
 $id_master				= $this->getRequest()->getParam('id_master');
 $stt				= $this->getRequest()->getParam('stt');

 $userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
 $QMiniGame2018 		= new Application_Model_MiniGame2018();
 $QMiniGame2018Detail 	= new Application_Model_MiniGame2018Detail();
 $id_staff 				= $userStorage->id;
 $started_at = date("Y-m-d H:i:s");
 $total_question = 10; // tổng số câu hỏi
 $params = [
 	'staff_id'		=> $staff_id,
 	'question_id'	=> $question_id,
 	'id_master'		=> $id_master
 ];


$is_completed = $QMiniGame2018Detail->getisComplete($params);
$arr_question = [];//mảng các id đã làm xong
foreach ($is_completed as $key => $value) {
	$arr_question[] = $value['question_id'];
}

//random ngẫu  nhiên loại trừ những câu đã làm
do{
	if($stt < 6){
		$num = rand(0,9);
	}elseif($stt > 7){
		$num = rand(0,9);
	}else{
		$num = rand(0,9);
	}
}while (in_array($num, $arr_question));

$xml 			= simplexml_load_file(APPLICATION_PATH.'/../public/xml/data-xml.xml');
$data 			= json_decode(json_encode($xml, true));

$question = [];
$question['question'] 		= $data->question[$num]; //lấy câu hỏi ngẫu nhiên từ kq random $num

$get_answer = $QMiniGame2018->getAnswer($data->question[$num]->id);
$question['answer'] = $get_answer[0];

// var_dump($question['answer']); exit;
$time_play = 60; //số thời gian(giây) chơi mỗi câu
$data_details 	= [
	'master_id' 	=> $id_master,
	'question_id'	=> $num,
	'started_at'	=> $started_at,
	'point'			=> 0,
	'diff'			=> $time_play,
	'user_choose'	=> 0
];
$QMiniGame2018Detail->insert($data_details);
echo json_encode($question);
exit;
