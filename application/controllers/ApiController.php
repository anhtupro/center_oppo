<?php
class ApiController extends My_Controller_Action{
    
    private function checkImei($imei, $timing_sales_id, &$info, $tool = null, $reimport = false, $staff_id) {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//        if (!$userStorage || !isset($userStorage->id) || !$userStorage->id) {
//            return 9999;
//        }

        $db     = Zend_Registry::get('db');
        $sql    = "CALL sp_check_imei(?, ?, ?, ?, ?, @result, @_ts_id, @_firstname, @_lastname, @_email, @_store, @_timing_from, @_timing_staff_id, @_active_date, @_good_id, @_color_id)";
        $result = $db->query($sql, array($imei, $staff_id, date('Y-m-d'), $timing_sales_id === null ? 0 : $timing_sales_id, IMEI_ACTIVATION_EXPIRE));

        $sql    = "SELECT @result, @_ts_id, @_firstname, @_lastname, @_email, @_store, @_timing_from, @_timing_staff_id, @_active_date, @_good_id, @_color_id";
        $result = $db->query($sql);
        $result = $result->fetch();

        $info = array(
            'store'           => $result['@_store'],
            'timing_sales_id' => $result['@_ts_id'],
            'staff_id'        => $result['@_timing_staff_id'],
            'date'            => $result['@_timing_from'],
            'activated_at'    => $result['@_active_date'],
            'good_id'         => $result['@_good_id'],
            'color_id'        => $result['@_color_id'],
        );

        if (isset($result['@_firstname']) && isset($result['@_lastname'])) {
            $info['staff'] = $result['@_firstname'] . ' ' . $result['@_lastname'] . ' | '
                    . (isset($result['@_email']) && !empty($result['@_email']) ? str_replace(EMAIL_SUFFIX, '', $result['@_email']) : '(Đã nghỉ)');
        }

        return isset($result['@result']) ? $result['@result'] : 9999;
    }
    public function abcAction(){
        $this->_helper->layout->disableLayout();
//echo time();
//exit();
$milliseconds = 1573703943;
	    $sign =MD5("appKey=vn_after_sale&imei=866269040651436&language=vi-VN&orderId=orderId1&previlegeCode=screenBrokenFix&quantity=1&timestamp=$milliseconds&key=qDEuEyw3KmjwsIhQppd9yUNSMkMMPFvrs+uyBJkddWg=");
            echo $sign;
            
            $params = array(
                "appKey" => "vn_after_sale",
                "sign" => "$sign",
                "language" => "vi-VN",
                "timestamp" => "$milliseconds",
                "userId" => "",
                "previlegeCode" => "screenBrokenFix",
                "imei" => "866269040651436",
                "quantity"=> "1",
                "orderId"=> "orderId1",
                
            );
            $data_string = json_encode($params);

            $url = 'https://m-member.wanyol.com/vn/api/1.0/member/privilege/common/consume';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		$content = curl_exec($ch);

            echo "<pre>";
            print_r(json_decode($content));
            echo "</pre>";

    }
    public function checkImeiOppoAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'check-imei-oppo.php';
    }
    public function checkDuplicateOppoAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'check-duplicate-oppo.php';
    }
    public function getPurchaseAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'get-purchase.php';
    }
    //CRM APIs
    //1. API description: check warranty status by the warranty card information
    public function crmCheckWarrantyStatusAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-check-warranty-status.php';
    }
    
    //2. API description: check the genuine machine by the imei number
    public function crmCheckGenuineAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-check-genuine.php';
    }
    
    //3. API description: get a list of all mobile phone
    public function crmGetAllMobileAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-get-all-mobile.php';
    }
    
    //4. API description: get a list of components' price by a phoneId
    public function crmGetComponentByPhoneidAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-get-component-by-phoneid.php';
    }
    
    //5. API description: get service center list
    public function crmGetServiceCenterAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-get-service-center.php';
    }
    
    //6. Search matched service center according to user-entered keywords
    public function crmGetServiceCenterByKeywordAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-get-service-center-by-keyword.php';
    }
    
    //7. API description: get the category list
    public function crmGetCategoryAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-get-category.php';
    }
    
    //8. API description: get question list according to a certain category
    public function crmGetQuestionByCategoryAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-get-question-by-category.php';
    }
    
    //9. API description: get the answer of a certain question
    public function crmGetAnswerByQuestionAction(){
        require_once 'api' . DIRECTORY_SEPARATOR . 'crm-get-answer-by-question.php';
    }
    
    
    //End CRM APIs
    
    //1) get latest promo
    public function getLatestPromoAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $res = file_get_contents('php://input');
        $result = json_decode($res,true);
        $username     = $result['username'];
        $password     = $result['password'];
//        $username     = $this->getRequest()->getPost('username');
//        $password     = $this->getRequest()->getPost('password');
        
        if(empty($username) || empty($password)){
            echo    json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
        }
        $auth = $this->checkAuth($username, $password);
        if($auth == true){
            
            $info = array(
                            'status' => 200,
                            'title'  => 'KHUYẾN MÃI HOT NHẤT',
                            'description' => 'Ưu đãi lớn mua Online thêm quà',
                            'photo-url' => 'https://shop.oppomobile.vn/uploads/product_avatar/2018/Nov/23/1542969510526_350x450.jpg',
                            'link-to-order' => 'https://shop.oppomobile.vn',
                            'link-to-see-more'  => 'https://shop.oppomobile.vn',
                            'message'   => 'Success'
                );
            
            echo    json_encode($info);
              
        }else{
            echo    json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
            
        }
    }
    
    //2) get product information based on product name
    public function getProductInfoAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $res = file_get_contents('php://input');
        $result = json_decode($res,true);
        $username     = $result['username'];
        $password     = $result['password'];
        $data         = $result['data'];
        
//        $username     = $this->getRequest()->getPost('username');
//        $password     = $this->getRequest()->getPost('password');
//        $data      = $this->getRequest()->getPost('data');
       
        $auth = $this->checkAuth($username, $password);
        if($auth == true){
            if(empty($data)){
                echo    json_encode(array(
                    'status' => 0,
                    'message' => 'data Ịnvalid'
                ));
            } else {
                $db = Zend_Registry::get('db');
//                $sql = "SELECT p.p_title, op.p_avatar, s.pl_slug, p.p_status FROM oppo_sale_product p "
//                        . "INNER JOIN oppo_cms_product op ON p.p_id = op.p_id "
//                        . "INNER JOIN oppo_cms_product_language s ON p.p_id = s.p_id "
//                        . "WHERE p.p_title LIKE '%' :data '%' and p.pc_id = 4779";
                
                $sql = "SELECT * FROM core_product p WHERE type = 1 and name LIKE '%' :data '%' ";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('data', $data, PDO::PARAM_STR);
                $stmt->execute();

                $list = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;
              
                if(!empty($list)){
                    $info = array();
                    foreach ($list as $key => $value) {
                       $photo_url = empty($list[$key]['p_avatar']) ? 'https://shop.oppomobile.vn/uploads/cms_productmedia/2018/March/22/887_1521702227.png' : 'https://oppomobile.vn/public/files/product/' . $list[$key]['image'];
                        $info[] = array(
                            'product-name'  => $list[$key]['name'],
                            'photo-url' => $photo_url,
                            'description' => $list[$key]['short_description'],
//                            'link-to-order' => 'https://shop.oppomobile.vn/smartphone/' . $list[$key]['pl_slug'],
                            'link-to-order' => $list[$key]['ecom_link'],
//                            'link-to-see-more'  => 'https://shop.oppomobile.vn/smartphone/' . $list[$key]['pl_slug'],
                            'link-to-see-more'  => $list[$key]['ecom_link'],
                            'still-supported'  => ($list[$key]['status'] == 1) ? 'True' : 'False',
                        );
                    }
                
                    echo    json_encode($info);
                }
                
            }
            
        }else{
            echo    json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
        }
    }
    
    //3) get price of accessory based on accessory and product name
    public function getPriceAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $res = file_get_contents('php://input');
        $result = json_decode($res,true);
        $username     = $result['username'];
        $password     = $result['password'];
        $data         = $result['data'];
//        $username     = $this->getRequest()->getParam('username');
//        $password     = $this->getRequest()->getParam('password');
//        $data      = $this->getRequest()->getParam('data');
     
        if(empty($username) || empty($password)){
            echo    json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
        }
        
        if(empty($username) || empty($password)){
            echo    json_encode(array(
                        'status' => 0,
                        'message' => 'Username or password Ịnvalid'
                    ));
        }
        $auth = $this->checkAuth($username, $password);
        if($auth == true){
            if(empty($data)){
                echo json_encode(array(
                        'status' => 0,
                        'message' => 'Data Ịnvalid'
                    ));
            } else {
//                $arrData = json_decode($data,true);
                $arrData = $data;
               
                $arrPrice = array();
                $db = Zend_Registry::get('db');
                $dataJson = array();
                $arrId = array();
                foreach ($data as $k => $value) {
//        $select = $db->select();

//        $col = array(
//            'p.*', 
//            'desc' => 'IFNULL(g.`desc`, p.model)'
//        );
//
//        $select->from(array('p' => CS_DB.'.part'), $col);
//        $select->joinLeft(array('g' => WAREHOUSE_DB.'.good'), 'g.name = p.model OR g.`desc` = p.model', array() );
//        $select->where('p.show_website = ?', 1);  
//        $select->order('desc');  
//echo $select->__toString();
//        $result = $db->fetchAll($select);
        
                    $accessory= $value['accessory'];
                    $product= str_replace("oppo", '', strtolower($value['product'])) ;
//                    $sql = "SELECT p.p_title, s.spp_price,  p.p_status FROM oppo_sale_product p INNER JOIN oppo_sale_product_price s ON p.p_id = s.p_id  WHERE p.p_title LIKE '%' :accessory '%' AND s.spp_price > 0 ";
                    $sql = "SELECT `p`.*, IFNULL(g.`desc`, p.model) AS `desc` FROM `cs_new`.`part` AS `p`
 LEFT JOIN `warehouse`.`good` AS `g` ON g.name = p.model OR g.`desc` = p.model WHERE (p.show_website = 1) and p.name LIKE '%' :accessory '%' AND (g.`desc` LIKE '%' :product '%' OR  p.model LIKE '%' :product '%') ";

                    $stmt = $db->prepare($sql);
                    $stmt->bindParam('accessory',$accessory , PDO::PARAM_STR);
                    $stmt->bindParam('product',TRIM($product) , PDO::PARAM_STR);
                    $stmt->execute();
                    $list = $stmt->fetchAll();
                    $stmt->closeCursor();
                    if(!empty($list)){
                        foreach ($list as $key => $val) {
                            if(!in_array($val['id'], $arrId)){
                                 $dataJson[] = array(
                                'product' => $val['desc'],
                                'accessory' => $val['name'],
                                'price' => $val['price'],
                                'still-supported' => ($val['del'] == 0) ? True : False  ,
                                );
                                 $arrId[] = $val['id'];
                            }
                           
//                            $dataJson[]['product'] = $val['desc'];
//                            $dataJson[]['accessory'] = $val['name'];
//                            $dataJson[]['price'] = $val['price'];
//                            $dataJson[]['still-supported'] = ($val['del'] == 0) ? True : False  ;
                        }
                    }
                
//                    if(!empty($list)){
//                        $arrPrice[] = array(
//                            'product' => $product,
//                            'accessory' => $accessory,
//                            'price' => $list[0]['spp_price'],
//                            'still-supported' => ($list[0]['p_status'] == 1) ? True : False
//
//                        );
//                    }
                }
                echo json_encode($dataJson);
            }
            
        }else{
            echo    json_encode(array(
                        'status' => 0,
                        'message' => 'Wrong password'
                    ));
            
        }
    }
    
     //4) get nearest service center based on province, district info   
    public function getNearestServiceCenterAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $res = file_get_contents('php://input');
        $result = json_decode($res,true);
        $username     = $result['username'];
        $password     = $result['password'];
        $location     = $result['location'];
//        $province     = $result['province'];
//        $district     = $result['district'];
       
//        $username     = $this->getRequest()->getParam('username');
//        $password     = $this->getRequest()->getParam('password');
//        $province      = $this->getRequest()->getParam('province');
//        $district      = $this->getRequest()->getParam('district');
        
        if(empty($username) || empty($password)){
             echo   json_encode(array(
                        'status' => 0,
                        'message' => 'Username or password Ịnvalid'
                    ));
        }
        $auth = $this->checkAuth($username, $password);
        if($auth == true){
//            if(empty($province)){
//                    echo  json_encode(array(
//                        'status' => 0,
//                        'message' => 'Province Ịnvalid'
//                    ));
//            } else {
                $db = Zend_Registry::get('db');
                $list = array();
                foreach ($location as $k => $val){
                     $db = Zend_Registry::get('db');
                    $province = trim($val['province']);
                    $district = trim($val['district']);
                    if(!empty($province) && !empty($district)){
                        $sql = "SELECT p.id, p.address, p.tel FROM cs_new.showroom p INNER JOIN province s ON s.id = p.province_id INNER JOIN regional_market rm on rm.id = p.district_id WHERE s.name LIKE '%' :province '%' AND rm.name LIKE :district and p.center_id IS NOT NULL AND p.id <> 29";
                        $stmt = $db->prepare($sql);
                        $stmt->bindParam('province', $province, PDO::PARAM_STR);
                        $stmt->bindParam('district', $district, PDO::PARAM_STR);
                    }elseif(!empty($province)){
                        $sql = "SELECT p.id, p.address, p.tel FROM cs_new.showroom p INNER JOIN province s ON s.id = p.province_id WHERE s.name LIKE '%' :province '%' and p.center_id IS NOT NULL AND p.id <> 29";
//                        echo $sql;
                        $stmt = $db->prepare($sql);
                        $stmt->bindParam('province', $province, PDO::PARAM_STR);
                    }else{
                        $sql = "SELECT p.id, p.address, p.tel FROM cs_new.showroom p INNER JOIN province s ON s.id = p.province_id INNER JOIN regional_market rm on rm.id = p.district_id WHERE  rm.name LIKE  :district  and p.center_id IS NOT NULL AND p.id <> 29";
                        $stmt = $db->prepare($sql);
                        $stmt->bindParam('district', $district, PDO::PARAM_STR);
                    }

                    $stmt->execute();
                    $list = $stmt->fetchAll();
                    $stmt->closeCursor();
                    $stmt = $db = null;
                    if(!empty($list)){
                        break;
                    }
                }
                
               
                $dataJson = array();
                if(!empty($list)){
                    foreach ($list as $k => $value) {
                        $dataJson[$k]['address'] = $value['address'];
                        $dataJson[$k]['phone'] = $value['tel'];
                        $dataJson[$k]['email'] = 'support.vn@oppo.com';
                        $dataJson[$k]['note'] = 'Mở cửa: 08:00-17:30 mỗi ngày, trừ chủ nhật và ngày lễ';
                    }
                    echo    json_encode($dataJson);
                }
               
                
//            }
            
        }else{
            echo    json_encode(array(
                        'status' => 0,
                        'message' => 'Username or password Ịnvalid'
                    ));
        }
    }
    
    //5) check genuein devices based on imei
    public function checkGenuineAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $res = file_get_contents('php://input');
        $result = json_decode($res,true);
        $username     = $result['username'];
        $password     = $result['password'];
        $entitie      = $result['entitie'];
        
//        $username     = $this->getRequest()->getParam('username');
//        $password     = $this->getRequest()->getParam('password');
//        $entitie      = $this->getRequest()->getParam('entitie');
        
        if(empty($username) || empty($password)){
             echo    json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
        }
        $auth = $this->checkAuth($username, $password);
        if($auth == true){
            if(empty($entitie)){
                echo    json_encode(array(
                    'status' => 0,
                    'message' => 'entitie Ịnvalid'
                ));
            } else {
                $QImei = new Application_Model_Imei();
                $result = '';
                $num = preg_replace("/[^0-9]/", '', $entitie);
                if(strlen($imei)!= 15){
                    $result = $QImei->getImeiInfo($entitie);
                }else{
                    echo    json_encode(array(
                        'status' => 0,
                        'result'  => 'invalid',
                    ));
                }
                if(!empty($result)){
                    echo    json_encode(array(
                        'status' => 200,
                        'result'  => 'valid',
                        'message'   => 'Success'
                    ));
                }else{
                    echo    json_encode(array(
                        'status' => 0,
                        'result'  => 'invalid',
                    ));
                }
                
            }
            
        }else{
            json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
        }
    }
    //6) check the status of device in service center based on warranty number (số phiếu bảo hành)
    public function checkStatusDeviceAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $res = file_get_contents('php://input');
        $result = json_decode($res,true);
        $username     = $result['username'];
        $password     = $result['password'];
        $entitie      = $result['entitie'];
//        $username     = $this->getRequest()->getParam('username');
//        $password     = $this->getRequest()->getParam('password');
//        $entitie      = $this->getRequest()->getParam('entitie');
        
        if(empty($username) || empty($password)){
             echo json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
        }
        $auth = $this->checkAuth($username, $password);
        if($auth == true){
            if(empty($entitie)){
                echo json_encode(array(
                    'status' => 0,
                    'message' => 'entitie Ịnvalid'
                ));
            } else {
                    error_reporting(1);
                    require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
                    $this->wssURI = 'http://cs.opposhop.vn/wss?wsdl';
                    $this->namespace = 'OPPOVN';

                    $client = new nusoap_client($this->wssURI);
                    $client->soap_defencoding = 'UTF-8';
                    $client->decode_utf8 = true;
                    
                    $wsParams = array(
	            'WarrantyID' => $entitie
                    );
                    $result = $client->call("searchIMEIStatusChatBot", $wsParams);
                    $mess = '';
                    if(!empty($result['data'])){
                        if (in_array($result['data'], array('Fixed Waiting Return To Customer')))
                            $mess = 'Máy sửa xong chờ lấy';
                        elseif (in_array($result['data'], array('Fixed Returned To Customer', 'No Fixed Returned To Customer')))
                            $mess = 'Máy sửa xong đã trả';
                        elseif (!in_array($result['data'], array('Warranty Exception')))
                            $mess = 'Máy đang sửa';
                        echo    json_encode(array(
                            'status'   => $mess
                        ));
                         
                    } else {
                         echo    json_encode(array(
                        'status'   => 'Thông tin chưa đúng. Bạn vui lòng nhập chính xác nội dung'
                        ));
                    }
            }
            
        }else{
            echo json_encode(array(
                    'status' => 0,
                    'message' => 'Username or password Ịnvalid'
                ));
        }
    }
    
    public function getServiceCenterAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $res = file_get_contents('php://input');
        $result = json_decode($res,true);
        $username     = $result['username'];
        $password     = $result['password'];
        $long         = $result['long'];
        $lat          = $result['lat'];
        
//        $username     = $this->getRequest()->getParam('username');
//        $password     = $this->getRequest()->getParam('password');
//        $province      = $this->getRequest()->getParam('province');
//        $district      = $this->getRequest()->getParam('district');
        
        if(empty($username) || empty($password)){
             echo   json_encode(array(
                        'status' => 0,
                        'message' => 'Username or password Ịnvalid'
                    ));
        }
        $auth = $this->checkAuth($username, $password);
        if($auth == true){
            if(empty($long) || empty($lat)){
                    echo  json_encode(array(
                        'status' => 0,
                        'message' => 'long or lat Ịnvalid'
                    ));
            } else {
                $db = Zend_Registry::get('db');
                $sql = "SELECT p.id, p.address, p.tel , p.latitude, p.longitude	FROM cs_new.showroom p INNER JOIN province s ON s.id = p.province_id INNER JOIN regional_market rm on rm.id = p.district_id WHERE p.center_id IS NOT NULL";
                $stmt = $db->prepare($sql);
                
                $stmt->execute();
                $list = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;
//                echo "<pre>";
//                print_r($list);
//                echo "</pre>";
                $dataJson = array();
                $min_distance = 100; 
                
                if(!empty($list)){
                    foreach ($list as $k => $value) {
                        $distance_office = My_DistanceGps::getDistance($value['latitude'], $value['longitude'], $lat, $long, "K");
                        if($distance_office <= $min_distance){
                            $min_distance = $distance_office;
                            $min_key = $k;
                        }
//                        echo $min_distance;
                       
                    }
                    
                    echo $distance_office;
                     echo '-';
                        echo $min_key;
                        
                                        echo "<pre>";
                print_r($list);
                echo "</pre>";
                }
                
                
               
                
            }
            
        }else{
            echo    json_encode(array(
                        'status' => 0,
                        'message' => 'Username or password Ịnvalid'
                    ));
        }
    }
    public function checkAuth($u, $p){
        if($u != 'oppo1' || $p != 'oppotester'){
            return false;
        }else{
            return true;
        }
    }
   
}
