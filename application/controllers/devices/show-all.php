<?php

    $QDevices   = new Application_Model_Devices();
    $limitItems = 10;
    $curPage =(int) $this->_request->getParam('page',1);
    $_paginator = array(
                        'itemCountPerPage' =>$limitItems,
                        'pageRange'=>10,
                        'currentPage'=>$curPage
                    );

  
    $dataItems  = $QDevices->getLimitDevices($curPage,$limitItems);
    $totalItems = $QDevices->totalDevices();
    $paginator  = new My_Paginator();
    $paginator  = $paginator->createPaginator($totalItems,$_paginator);

    // send data to view
    $this->view->dataItems    = $dataItems;
    $this->view->stt          = ($curPage == 1) ? 0 :($limitItems*($curPage-1)) ;
    $this->view->limitItems   = $limitItems;
    $this->view->url          = 'http://center_oppo.test/devices/show-all?';
    $this->view->total        = 0;
    $this->view->curPage      = $curPage;
    $this->view->paginator    = $paginator;


