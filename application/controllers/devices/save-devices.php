<?php
    $QDevices = new  Application_Model_Devices();
    // Action for add/update  device -> Disable layout 
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);


    $desc    = $this->_request->getParam('desc');
    $name    = $this->_request->getParam('name');
    $price_5 = $this->_request->getParam('price_5');
    $cat_id  = $this->_request->getParam('cat_id');
    $method  = $this->_request->getParam('method');
    $test    = $this->_request->getParams();
    if($method == 'create'){
        $data = [
            'desc'    => $desc,
            'name'    => $name,
            'price_5' => $price_5,
            'cat_id'  => $cat_id
        ];
        $QDevices = $QDevices->insertDevices($data);
        echo json_encode(['status'=>0]);
    }
    if($method == 'update'){
        $data = [
            'desc'    => $desc,
            'name'    => $name,
            'price_5' => $price_5,
            'cat_id'  => $cat_id
        ];
        $id =(int) $this->_request->getParam('id');
        $QDevices->updateDeviceById($data,$id);
        echo json_encode(['status'=>0]);
    }
  
    