<?php
    $QDecives = new  Application_Model_Devices();
    $id = $this->_request->getParam('id');
    $curPage = $this->_request->getParam('page');
    $device = $QDecives->getDeviceById($id);
    $name           = $device[0]['name'];
    $desc           = $device[0]['desc'];
    $cat_id         = $device[0]['cat_id'];
    $price_for_fpt  = $device[0]['price_for_fpt'];
    
    $this->view->id = $id;
    $this->view->name = $name;
    $this->view->desc = $desc;
    $this->view->cat_id = $cat_id;
    $this->view->price_for_fpt = $price_for_fpt;
    $this->view->curPage = $curPage;