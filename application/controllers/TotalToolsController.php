<?php

class TotalToolsController extends My_Controller_Action
{
	public function imeiLogAction(){
		
		if ($this->_request->getParam('export')) {
			$from_date	= $this->getRequest()->getParam('from_date');
			$to_date	= $this->getRequest()->getParam('to_date');
			$type		= $this->getRequest()->getParam('type');

			$from	= $from_date ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : NULL;
			$from   = $from.' 00:00:00';

			$to		= $to_date ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : NULL;
			$to     = $to.' 23:59:59';

			$db     = Zend_Registry::get('db');

			$sql 	= 'SELECT 
						 a.imei
						,b.sn
						,c.`desc` good
						,d.`name` color
						,store.`name` store
						,CONCAT(e.firstname," ",e.lastname) staff_timing
						,e.`code` code_timing
						,j.`name` title_timing
						,g.`name` area_timing
						,COUNT(a.imei) sl
						,a.time time_timing
						,b.`name` staff_buy
						,e1.`code` code_buy
						,f1.`name` title_buy
						,j1.`name` area_buy
						,b.date_buy 

					FROM check_imei_log a
					JOIN warehouse.staff_order b
						ON a.imei = b.imei
						AND a.time BETWEEN :from_date AND :to_date
					LEFT JOIN store 
						ON store.id = a.store_id
					LEFT JOIN warehouse.good c
						ON c.id = b.good_id
					LEFT JOIN warehouse.good_color d
						ON d.id = b.good_color
					LEFT JOIN staff e
						ON e.id = a.user_id
					LEFT JOIN team f
						ON f.id = e.title
					LEFT JOIN regional_market g 
						ON g.id = e.regional_market
					
					LEFT JOIN area j
						ON j.id = g.area_id


					LEFT JOIN staff e1
						ON e1.id = b.staff_id
					LEFT JOIN team f1
						ON f1.id = e1.title
					LEFT JOIN regional_market g1
						ON g1.id = e1.regional_market
					
					LEFT JOIN area j1
						ON j1.id = g1.area_id
					WHERE a.result = :type

					GROUP BY imei,e.id
					';
			$stmt = $db->prepare($sql);
			$stmt->bindParam("from_date",  $from, PDO::PARAM_STR);
			$stmt->bindParam("to_date",  $to, PDO::PARAM_STR);
        	$stmt->bindParam("type",  $type, PDO::PARAM_STR);
		    $stmt->execute();

		    $data = $stmt->fetchAll();
		    $stmt->closeCursor();
		    $db = $stmt = null;

		    // echo "<pre>";print_r($data);die;
		    
		    require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();

			$heads = array(
				'A' => 'Imei',
				'B' => 'SN',
				'C' => 'Product',
				'D' => 'Color',
				'E' => 'Staff-Timing(1)',
				'F' => 'Code-(1)',
				'G' => 'Title-(1)',
				'H' => 'Area-(1)',
				'I' => 'Times-(1)',
				'J' => 'Time-(1)',
				'K' => 'Staff-Buy(2)',
				'L' => 'Code-(2)',
				'M' => 'Title-(2)',
				'N' => 'Area-(2)',
				'O' => 'Date Buy',
				'P' => 'Store',
			);
			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			foreach($heads as $key => $value)
					$sheet->setCellValue($key.'1', $value);

			$sheet->getStyle('A1:P1')->applyFromArray(array('font' => array('bold' => true)));
			$sheet->getColumnDimension('A')->setWidth(20);
			$sheet->getColumnDimension('B')->setWidth(30);
			$sheet->getColumnDimension('C')->setWidth(10);
			$sheet->getColumnDimension('D')->setWidth(15);
			$sheet->getColumnDimension('E')->setWidth(25);
			$sheet->getColumnDimension('F')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(30);
			$sheet->getColumnDimension('H')->setWidth(20);
			$sheet->getColumnDimension('I')->setWidth(9);
			$sheet->getColumnDimension('J')->setWidth(20);
			$sheet->getColumnDimension('K')->setWidth(25);
			$sheet->getColumnDimension('L')->setWidth(15);
			$sheet->getColumnDimension('M')->setWidth(20);
			$sheet->getColumnDimension('N')->setWidth(15);
			$sheet->getColumnDimension('O')->setWidth(25);
			$sheet->getColumnDimension('P')->setWidth(30);

			foreach($data as $key => $value){
					$sheet->getCell('A'.($key + 2))->setValueExplicit( trim($value['imei']), PHPExcel_Cell_DataType::TYPE_STRING);
					$sheet->getCell('B'.($key + 2))->setValueExplicit( trim($value['sn']), PHPExcel_Cell_DataType::TYPE_STRING);
					$sheet->setCellValue('C'.($key + 2), $value['good']);
					$sheet->setCellValue('D'.($key + 2), $value['color']);
					$sheet->setCellValue('E'.($key + 2), $value['staff_timing']);
					$sheet->setCellValue('F'.($key + 2), $value['code_timing']);
					$sheet->setCellValue('G'.($key + 2), $value['title_timing']);
					$sheet->setCellValue('H'.($key + 2), $value['area_timing']);
					$sheet->setCellValue('I'.($key + 2), $value['sl']);
					$sheet->setCellValue('J'.($key + 2), $value['time_timing']);
					$sheet->setCellValue('K'.($key + 2), $value['staff_buy']);
					$sheet->setCellValue('L'.($key + 2), $value['code_buy']);
					$sheet->setCellValue('M'.($key + 2), $value['title_buy']);
					$sheet->setCellValue('N'.($key + 2), $value['area_buy']);
					$sheet->setCellValue('O'.($key + 2), $value['date_buy']);
					$sheet->setCellValue('P'.($key + 2), $value['store']);
			}

			$filename = 'Imei - '.date('d-m-Y');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
			$objWriter->save('php://output');

			exit;
		}

		
	}
}