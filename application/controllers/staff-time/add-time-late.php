<?php
$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();
$staff_update 	= $this->getRequest ()->getParam ( 'staff_update' );
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();

$late           = $this->getRequest()->getParam('update-late', 0);
$date_late      = $this->getRequest()->getParam('date_late');
$late_from = $this->getRequest()->getParam('late_from');
$late_to = $this->getRequest()->getParam('late_to');
$reason_late    = $this->getRequest()->getParam('reason_late');
$add_reason_late = $this->getRequest()->getParam('add_reason_late');

$add_reason_soon = $this->getRequest()->getParam('add_reason_soon');
$reason_soon = $this->getRequest()->getParam('reason_soon');

$db = Zend_Registry::get('db');
if(!empty($late) AND !empty($late_from) AND !empty($late_to)){
    $db = Zend_Registry::get('db');
    $late_from = DateTime::createFromFormat('d/m/Y', $late_from)->format('Y-m-d');
    $late_to = DateTime::createFromFormat('d/m/Y', $late_to)->format('Y-m-d');
    $status = 1;
    if(empty($add_reason_soon)){
        $add_reason_soon=0;
    }
    if(empty($reason_soon)){
        $reason_soon=null;
    }

    $stmt_late1 = $db->prepare("CALL PR_Add_Time_Late_staff_Fix( :p_staff_id, :p_add_reason_late, :p_satus, :p_late_from,  :p_late_to,  :p_reason, :p_add_reason_soon, :p_reason_soon )");
    $stmt_late1->bindParam('p_staff_id', $staff_update, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_add_reason_late', $add_reason_late, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_satus',$status, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_late_from', My_Util::escape_string($late_from), PDO::PARAM_STR);
    $stmt_late1->bindParam('p_late_to', My_Util::escape_string($late_to), PDO::PARAM_STR);
    $stmt_late1->bindParam('p_reason', My_Util::escape_string($reason_late), PDO::PARAM_STR);
    $stmt_late1->bindParam('p_add_reason_soon', $add_reason_soon, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_reason_soon', My_Util::escape_string($reason_soon), PDO::PARAM_STR);
    $stmt_late1->execute();
//    $sql_insert_late = "INSERT INTO time_late (`staff_id`, `date`, `reason`, `add_reason_late`, `status`, `note`)
//							VALUES (:staff_id, :date_late, :reason_late,:add_reason_late,  1, 'Manager Import')
//						ON DUPLICATE KEY UPDATE reason = VALUES(`reason`),`add_reason_late` = VALUES(`add_reason_late`) , status = VALUES(`status`)";
//
//    $stmt = $db->prepare($sql_insert_late);
//    $stmt->bindParam('staff_id', $staff_update, PDO::PARAM_INT);
//    $stmt->bindParam('date_late', $date_late, PDO::PARAM_STR);
//    $stmt->bindParam('reason_late', $reason_late, PDO::PARAM_STR);
//    $stmt->bindParam('add_reason_late', $add_reason_late, PDO::PARAM_INT);
//    $stmt->execute();
//    $stmt->closeCursor();
//    $stmt = $db = null;
    
    $params = array (
        'month' => 7,
        'year' => 2017,
        'from_date' => '2017-7-1',
        'to_date' => '2017-7-31',
        'staff_id' => $staff_update
    );
    
    $QTime2 = new  Application_Model_Time2();
    echo json_encode (
        array( 'status' => 1,
        'message' => "Xác nhận vào trễ/ Về sớm thành công",
        'data'    => $data   = $QTime2->getInfoStaffView($params)    
    ));
    exit();
}

