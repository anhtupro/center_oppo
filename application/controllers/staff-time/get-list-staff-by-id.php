<?php
$db = Zend_Registry::get('db');
$db->query('SET SESSION group_concat_max_len = 1000000;');
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);

$from_date = date($params['year'] . '-' . $params['month'] . '-1');
$to_date = date($params['year'] . '-' . $params['month'] . '-' . $number_day_of_month);

$select = $db->select()->from(array(
    "st" => "staff"
), array(
    new Zend_Db_Expr($select)
));
$select_staff_id = $db->select()->from(array(
    "st" => "staff"
), array(
    new Zend_Db_Expr("SQL_CALC_FOUND_ROWS st.id as `id`")
));
if (empty($params['month'])) {
    $params['month'] = date('m');
}

$select->joinLeft(array(
    'ad' => 'all_date'
), "month(ad.date) = " . $params['month'] . "
                                AND year(ad.date) = " . $params['year'], array());

$select->joinLeft(array(
    't' => 'time'
), "t.staff_id = st.id
                                AND ad.date = DATE(t.created_at)", array());

$select->join(array(
    'cp' => 'company'
), "st.company_id = cp.id", array());

$select->joinLeft(array(
    'tt' => 'temp_time'
), "st.id = tt.staff_id AND ad.date = tt.date AND tt.status = 0", array());

$select->join(array(
    'rm' => 'regional_market'
), "st.regional_market = rm.id", array());

$select->join(array(
    'ar' => 'area'
), "ar.id = rm.area_id", array());

$select_staff_id->join(array(
    'rm' => 'regional_market'
), "st.regional_market = rm.id", array());

$select_staff_id->joinLeft(array(
    'cgm' => 'company_group_map'
), "st.title = cgm.title", array());

$select_staff_id->joinLeft(array(
    'spt' => 'staff_pending_time'
), "st.id = spt.staff_id AND cgm.company_group <> 6 AND spt.month = " . $params['month'] . " AND spt.year = " . $params['year'], array());

$select->joinLeft(array(
    'spt' => 'staff_pending_time'
), "st.id = spt.staff_id AND spt.month = " . $params['month'] . " AND spt.year = " . $params['year'], array());

$select_staff_id->join(array(
    'ar' => 'area'
), "ar.id = rm.area_id", array());

$select_staff_id->joinLeft(array(
    'str' => new Zend_Db_Expr("(SELECT tcd.staff_id as `staff_id`
                        FROM `trainer_course_detail` tcd
                        JOIN `trainer_course_timing` AS `tct` ON tct.course_detail_id = tcd.id
                        WHERE tcd.staff_id IS NOT NULL
                        AND (tct.date BETWEEN '" . $from_date . "' AND '" . $to_date . "')
                        GROUP BY tcd.staff_id)")
), "str.staff_id = st.id", array());

$select->joinLeft(array(
    'cgm' => 'company_group_map'
), "st.title = cgm.title", array());

$select->joinLeft(array(
    'ada' => 'all_date_advanced'
), "date(t.created_at) = ada.date
                                AND ada.is_off = 1
                                AND (
                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                )
                                AND ada.category_date IS NOT NULL", array());

$select->joinLeft(array(
    'dc' => 'date_category'
), "dc.id = ada.category_date
                            AND ada.category_date IS NOT NULL", array());

$select->joinLeft(array(
    'cid' => 'check_in_detail'
), "date(t.created_at) = cid.check_in_day
                            AND st.code = cid.staff_code", array());

$select->joinLeft(array(
    'dsa' => 'date_staff_advanced'
), "date(t.created_at) = dsa.date
                            AND dsa.staff_id = st.id
                            AND dsa.is_del <> 1", array());

$select->joinLeft(array(
    'std' => 'saturday'
), "std.month = " . $params['month'] . "
                            AND std.year = " . $params['year'] . "
                            AND std.type = 4 AND std.type_id = cgm.company_group", array());

$select_staff_id->where("( (st.joined_at <= '" . $from_date . "'
                OR (st.joined_at between '" . $from_date . "' AND '" . $to_date . "')
                ) AND (st.off_date IS NULL OR st.off_date > '" . $from_date . "') )
                OR str.staff_id IS NOT NULL");

$select_staff_id->where('st.title <> 375');

if (isset($params['area']) && ! empty($params['area'])) {
    $select_staff_id->where("ar.id = ?", $params['area']);
}

if (isset($params['name']) && ! empty($params['name'])) {
    $select_staff_id->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
}

if (isset($params['email']) && ! empty($params['email'])) {
    $select_staff_id->where("st.email = ?", $params['email'] . '@oppomobile.vn');
}

if (isset($params['code']) && ! empty($params['code'])) {
    $select_staff_id->where("st.code = ?", $params['code']);
}

if (isset($params['department']) && ! empty($params['department'])) {
    $select_staff_id->where("st.department = ?", $params['department']);
}

if (isset($params['team']) && ! empty($params['team'])) {
    $select_staff_id->where("st.team = ?", $params['team']);
}

if (isset($params['title']) && ! empty($params['title'])) {
    $select_staff_id->where("st.title = ?", $params['title']);
}

$auth = Zend_Auth::getInstance()->getStorage()->read();
if (! in_array($auth->group_id, array(
    HR_ID,
    ADMINISTRATOR_ID
))) {
    $QCheckin = new Application_Model_CheckIn();
    $list_info = $QCheckin->getListCodePermission($auth->code);
    if (! empty($list_info)) {
        $QCheckin = new Application_Model_CheckIn();
        $list_info = $QCheckin->getListCodePermission($auth->code);
        $where_permission_array = array();
        $where_manager_array = array();
        
        $is_manager = 0;
        $is_leader = 0;
        foreach ($list_info as $key => $val) {
            if ($val['is_leader'] > 0) {
                $is_leader = 1;
            }
            
            if ($val['is_manager'] > 0) {
                $is_manager = 1;
            }
            
            $where_team = ! empty($val['team_id']) ? " AND st.team = " . $val['team_id'] : "";
            $where_team_manager = ! empty($val['team_id']) ? " AND team_id = " . $val['team_id'] : "";
            $where_permission_array[] = "(st.office_id = " . $val['office_id'] . " AND st.department = " . $val['department_id'] . $where_team . " )";
            
            $where_manager_array[] = "(office_id = " . $val['office_id'] . " AND department_id = " . $val['department_id'] . $where_team_manager . " )";
        }
        
        $select_staff_id->where(implode(" OR ", $where_permission_array));
        
        if ($is_leader > 0) {
            $select_staff_id->where('st.id <> ?', $auth->id);
            
            $sql_get_list_manager = "SELECT *
                                                FROM `staff_permission`
                                                WHERE is_manager > 0 AND (" . implode(' OR ', $where_manager_array) . ")";
            $stmt = $db->prepare($sql_get_list_manager);
            $stmt->execute();
            $data_manager_code = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = null;
            
            $array_manager_code = array();
            foreach ($data_manager_code as $val_code_manager) {
                $array_manager_code[] = "'" . $val_code_manager['staff_code'] . "'";
            }
            
            if (! empty($array_manager_code)) {
                $select_staff_id->where('st.code NOT IN (' . implode(",", $array_manager_code) . ")");
            }
        }
    } else {
        if (isset($params['staff_id']) and $params['staff_id']) {
            $select_staff_id->where('st.id = ?', $params['staff_id']);
        } elseif (isset($params['sale']) and $params['sale']) {
            
            $QStore = new Application_Model_Store();
            $array_staff_check = $QStore->get_pg_store_sale($params['sale']);
            $array_staff_check[] = $params['sale'];
            
            if (is_array($array_staff_check) and $array_staff_check) {
                $select_staff_id->where('st.id in (?)', $array_staff_check);
            }
        } elseif (isset($params['leader']) and $params['leader']) {
            
            $QStore = new Application_Model_Store();
            $array_staff_check = $QStore->get_pg_store_sale($params['leader']);
            $array_staff_check[] = $params['leader'];
            
            $array_leader_id = array_merge($array_staff_check, $this->getStaffForTiming($params['leader']));
            
            $select_staff_id->where('st.id in (?)', $array_leader_id);
        } elseif (isset($params['asm']) and $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            if ($list_regions['province'] && is_array($list_regions['province']) && count($list_regions['province']) > 0) {
                $select_staff_id->where('st.regional_market IN (?)', $list_regions['province']);
            } 

            else
                $select_staff_id->where('1=0', 1);
            $select_staff_id->where('st.team in (75,119,294, 397)', '');
        } elseif (isset($params['other']) and $params['other']) {
            $QTimePermission = new Application_Model_TimePermission();
            $list_regions = $QTimePermission->get_region_cache($params['other']);
            
            if ($list_regions && is_array($list_regions) && count($list_regions) > 0) {
                $select->where('t.regional_market IN (?)', $list_regions); // l?c staff thu?c regional_market tr�n
                $select_staff_id->where('st.regional_market IN (?)', $list_regions);
            } else {
                $select_staff_id->where('1=0', 1);
            }
            $list_teams = $QTimePermission->get_team_cache($params['other']);
            if ($list_teams && is_array($list_teams) && count($list_teams) > 0) {
                $select_staff_id->where('st.team IN (?)', $list_teams); // l?c staff thu?c regional_market tr�n
            } else {
                $select_staff_id->where('1=0', 1);
            }
        }
    }
}
if ($limit && empty($params['approve_all'])) {
    $select_staff_id->limitPage($page, $limit);
}

$select_staff_id->order("spt.status desc");
$select_staff_id->order("st.id asc");
$select_staff_id->group('st.id');
$data_staff_id = $db->fetchAll($select_staff_id);

$data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");

$array_staff_id = array();
foreach ($data_staff_id as $key_staff_id => $value_staff_id) {
    $array_staff_id[] = $value_staff_id['id'];
}

$string_array_staff_id = implode(",", $array_staff_id);

return $string_array_staff_id;

if (! empty($array_staff_id)) {
    $select->where('st.id IN (?)', $array_staff_id);
} else {
    $select->where('st.id IN (?)', array(
        - 1
    ));
}

$select->order("spt.status desc");
$select->order("st.id asc");

$select->group("st.id");

echo $select_staff_id->__toString();
if (empty($params['approve_all'])) {
    $data['data'] = $db->fetchAll($select);
} else {
    $data = $db->fetchOne($select);
}
return $data;
