<?php
$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();

set_time_limit(0);
ini_set('memory_limit', '5120M');



require_once 'PHPExcel.php';

$alphaExcel = new My_AlphaExcel();

$PHPExcel = new PHPExcel();
$heads = array(
    $alphaExcel->ShowAndUp() => 'STT',
    $alphaExcel->ShowAndUp() => 'Code',
    $alphaExcel->ShowAndUp() => 'Name',
    $alphaExcel->ShowAndUp() => 'Department',
    $alphaExcel->ShowAndUp() => 'Team',
    $alphaExcel->ShowAndUp() => 'Title',
    $alphaExcel->ShowAndUp() => 'Province',

);


$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

foreach($heads as $key => $value)
{
    $sheet->setCellValue($key.'1', $value);
}

$index = 1;
foreach($data as $key => $value)
{
    $alphaExcel = new My_AlphaExcel();
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $key+1);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['code']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['fullname'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['department'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['province_name'],PHPExcel_Cell_DataType::TYPE_STRING);

    $index++;

}
$filename = 'Staff Permission - ' . date('Y-m-d H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;

