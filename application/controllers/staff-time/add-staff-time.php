<?php
$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();
$staff_update 	= $this->getRequest ()->getParam ( 'staff_update' );
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$QLeaveDetail 	= new Application_Model_LeaveDetail ();
$QTime2 		= new Application_Model_Time2 ();
$QTimeGps       = new Application_Model_TimeGps();
$staff_id       = $userStorage->id;

if (! empty ( $this->getRequest ()->getParam ( 'update-time-office' ) )) {
	$office_time = $this->getRequest ()->getParam ( 'office_time' );
	$reason = $this->getRequest ()->getParam ( 'reason' );
// 	$params_update = array (
// 			'date' => $date,
// 			'shift' => NULL,
// 			'office_time' => $office_time,
// 			'staff_id' => $staff_update,
// 			'reason' => $reason,
// 	);

// 	$QTime2->insertTempTime ( $params_update );
}

if (! empty ( $this->getRequest ()->getParam ( 'update-time' ) )) {
	
	$date          = $this->getRequest ()->getParam ( 'date' );
	$shift_pick    = $this->getRequest ()->getParam ( 'shift' );
	$office_time   = $this->getRequest ()->getParam ( 'office_time' );
	$reason        = $this->getRequest ()->getParam ( 'reason' );
	$add_reason    = $this->getRequest ()->getParam ( 'add_reason' );
	$QTime         = new Application_Model_Time ();
	$QTempTime     = new Application_Model_TempTime();
	$QTimeImageLocation = new Application_Model_TimeImageLocation ();
	$QStaff = new Application_Model_Staff ();
	
	$datetime = date ( 'Y-m-d H:i:s' );
	
	if(!empty($office_time)){ //Office
		$where        = array ();
		$where []     = $QTempTime->getAdapter ()->quoteInto ( 'staff_id = ?', $staff_update );
		$where []     = $QTempTime->getAdapter ()->quoteInto ( 'date = ?', $date );
		$result_time  = $QTempTime->fetchRow ( $where );
		if(empty($result_time)){
		    $data = array(
		        'staff_id' => $staff_update,
		        'date'     => $date,
		        'shift'    => ($office_time == 1) ? 6 : 9,
		        'office_time' => $office_time,
		        'reason'   => $reason,
		        'add_reason' => $add_reason,
		        'created_at'=> $datetime,
		        'note'     => 'Manager Import',
		        'status'   => 1
		    );
		    $QTempTime->insert($data);
            
		    $data_time = array(
		        'staff_id'   => $staff_update,
		        'created_at' => $date,
		        'created_by' => $userStorage->id,
		        'approved_at' => $datetime,
		        'approved_by' => $userStorage->id,
		        'reason'      => $reason,
		        'real_time'   => $office_time,
		        'status'      => 1,
		        'note'     => 'Manager Import'
		    );
		    $QTime->insert($data_time);
    		 echo json_encode (array( 'status' => 1,'message' => "Xác nhận công thành công"));
    		 exit();
		}else{
		    $where        = array ();
		    $where []     = $QTempTime->getAdapter ()->quoteInto ( 'staff_id = ?', $staff_update );
		    $where []     = $QTempTime->getAdapter ()->quoteInto ( 'date = ?', $date );
		    $result_time  = $QTempTime->fetchRow ( $where );
		    $data = array(
		        'staff_id' => $staff_update,
		        'date'     => $date,
		        'shift'    => ($office_time == 1) ? 6 : 9,
		        'office_time' => $office_time,
		        'reason'   => $reason,
		        'add_reason' => $add_reason,
		        'created_at'=> $datetime,
		        'note'     => 'Manager Import',
		        'status'   => 1,
		        'del'      => 0   
		    );
		    $QTempTime->update($data,$where);
		    
		    echo json_encode(array('status'  => 0,'message' => "Ngày này đã được xác nhận công"));
		    exit();
		}
	}else{ // None Office
	    $where        = array ();
	    //$where []     = $QTimeGps->getAdapter ()->quoteInto ( 'staff_id = ?', $staff_update );
	    //$where []     = $QTimeGps->getAdapter ()->quoteInto ( 'check_in_day = ?', $date );
	    //$result_time  = $QTimeGps->fetchRow ( $where );
		
		$where []     = $QTempTime->getAdapter ()->quoteInto ( 'staff_id = ?', $staff_update );
		    $where []     = $QTempTime->getAdapter ()->quoteInto ( 'date = ?', $date );
		    $result_time  = $QTempTime->fetchRow ( $where );
	    
	    if(empty($result_time)){
	        
	        $data = array(
	            'staff_id' => $staff_update,
	            'date'     => $date,
	            'shift'    => $shift_pick,
	            'office_time' => ($shift_pick == 9) ? 0.5 : 1,
	            'reason'   => $reason,
	            'add_reason' => $add_reason,
	            'created_at'=> $datetime,
	            'note'     => 'Manager Import',
	            'status'   => 1
	        );
	        $QTempTime->insert($data);
	        
	          $data_time = array(
		        'staff_id'   => $staff_update,
		        'created_at' => $date,
		        'created_by' => $userStorage->id,
		        'approved_at' => $datetime,
		        'approved_by' => $userStorage->id,
		        'reason'      => $reason,
		        'real_time'   => $office_time,
		        'status'      => 1,
		        'note'     => 'Manager Import'
		    );
	       $id_time = $QTime->insert($data_time);
	        
// 	        $data_insert_image = array (
// 	            'time_id'      => $id_time,
// 	            'staff_id'     => $staff_update,
// 	            'created_at'   => $date,
// 	            'reason_update_mess' => $reason,
// 	            'is_request'   => 1,
// 	            'shift_type'   => $shift_pick
// 	        );
	        	
// 	        $QTimeImageLocation->insert ( $data_insert_image );
	        	
// 	        $data_gps = array(
// 	            'staff_id'     => $staff_update,
// 	            'status'       => 1,
// 	            'check_in_day' => $date,
// 	            'reason'       => $reason,
// 	            'shift'        => $shift_pick,
// 	            'note'         => 'Manager Import' ,
// 	            'is_require_work_day' => 1,
// 	            'is_half_day'  => ($shift_pick == 9) ? 1 : 0 ,
// 	        );
	        	
// 	        $QTimeGps->insert($data_gps);
	        echo json_encode (array( 'status' => 1,'message' => "Xác nhận công thành công"));
	        exit();
	    }else{
			$where        = array ();
		    $where []     = $QTempTime->getAdapter ()->quoteInto ( 'staff_id = ?', $staff_update );
		    $where []     = $QTempTime->getAdapter ()->quoteInto ( 'date = ?', $date );
		    $result_time  = $QTempTime->fetchRow ( $where );
		    $data = array(
		        'staff_id' => $staff_update,
		        'date'     => $date,
		        'shift'    => $shift_pick,
		        'office_time' => ($shift_pick == 9) ? 0.5 : 1,
		        'reason'   => $reason,
		        'add_reason' => $add_reason,
		        'created_at'=> $datetime,
		        'note'     => 'Manager Import',
		        'status'   => 1,
		        'del'      => 0   
		    );
		    $QTempTime->update($data,$where);
		    
			
	        echo json_encode(array('status'  => 0,'message' => "Ngày này đã được xác nhận công"));
	        exit();
	    }
	}
	
// 	try {
// 	    $db = Zend_Registry::get('db');
// 	    $db->beginTransaction();
	    
// 		$staffRowSet = $QStaff->find ( $userStorage->id );
// 		$staff = $staffRowSet->current ();
		
// 		$where = array ();
// 		$where [] = $QTime->getAdapter ()->quoteInto ( 'staff_id = ?', $userStorage->id );
// 		$where[] = $QTime->getAdapter()->quoteInto('off = 0');
// 		$where [] = $QTime->getAdapter ()->quoteInto ( 'DATE(created_at) = ?', $date );
// 		$result_date = $QTime->fetchRow ( $where );
		
// 		if (empty ( $result_date )) {
// 			// cham lan dau
// 			$data_time = array (
// 					'staff_id' => $userStorage->id,
// 					'created_at' => $date,
// 					'regional_market' => $staff ['regional_market'],
// 					'note' => 'yêu cầu bổ sung công ngày: ' . $date . ' ca: ' . $shift_pick 
// 			);
			
// 			$data_time ['shift'] = $shift_pick;
			
// 			$id_time = $QTime->insert ( $data_time );
			
// 			$data_insert_image = array (
// 					'time_id' => $id_time,
// 					'staff_id' => $userStorage->id,
// 					'created_at' => $date,
// 					'reason_update_mess' => $reason,
// 					'is_request' => 1,
// 					'shift_type' => $shift_pick 
// 			);
			
// 			$QTimeImageLocation->insert ( $data_insert_image );
			
// 			$data_gps = array(
// 			    'staff_id'     => $userStorage->id,
// 			    'check_in_day' => $date,
// 			    'reason'       => $reason,
// 			    'shift'        => $shift_pick,
// 			    'note' => 'yêu cầu bổ sung công ngày: ' . $date . ' ca: ' . $shift_pick,
// 			    'is_require_work_day' => 1,
// 				'is_half_day' => ($shift_pick == 9) ? 1 : 0 ,
			    
// 			);
			
// 			$where_gps    = array();
// 			$where_gps[]  = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
// 			$where_gps[]  = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date);
			
// 			$QTimeGps->insert($data_gps, $where_gps);
			
			
// 		} else {
// 			$id_time = $result_date ['id'];
			
// 			$where = array ();
// 			$where [] = $QTimeImageLocation->getAdapter ()->quoteInto ( 'time_id = ?', $id_time );
// 			$result = $QTimeImageLocation->fetchAll ( $where );
			
// 			if (! empty ( $result )) {
// 				if (count ( $result->toArray () ) < 2) {
// 					$data_insert_image = array (
// 							'time_id' => $id_time,
// 							'staff_id' => $userStorage->id,
// 							'created_at' => $date,
// 							'reason_update_mess' => $reason,
// 							'is_request' => 1,
// 							'shift_type' => $shift_pick 
// 					);
					
// 					$QTimeImageLocation->insert ( $data_insert_image );
// 					// exit(json_encode(array(
// 					// 'result' => 1,
// 					// 'message' => $data_insert_image,
// 					// )));
// 				} else {
// 					// exit(json_encode(array(
// 					// 'result' => 1,
// 					// 'message' => 'Ngày '. $date . 'đã đủ công.',
// 					// )));
// 				}
// 			}
// 		}
// 		$db->commit();
// 	} catch ( exception $e ) {
// 	    $db->rollback();
// 	}
}

