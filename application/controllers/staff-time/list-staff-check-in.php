<?php
    $this->_helper->viewRenderer->setRender('list-staff-check-in-clone');
    $not_approve_self = array(LEADER_ID,SALES_ID, PB_SALES_ID, PGPB_ID);
    $page             = $this->getRequest()->getParam('page', 1);
    $name             = $this->getRequest()->getParam('name',null);
    $code             = $this->getRequest()->getParam('code',null);
    $email            = $this->getRequest()->getParam('email',null);
    $export           = $this->getRequest()->getParam('export',0);
    $only_training    = $this->getRequest()->getParam('only_training', '0');
    $month            = $this->getRequest()->getParam('month', date('m'));
    $year             = $this->getRequest()->getParam('year', date('Y'));
    $area             = $this->getRequest()->getParam('area',null);
    $department       = $this->getRequest()->getParam('department',null);
    $team             = $this->getRequest()->getParam('team',null);
    $title            = $this->getRequest()->getParam('title',null);
    $off_long         = $this->getRequest()->getParam('off_long');
    $dev              = $this->getRequest()->getParam('dev',0);

    $export_over_time       = $this->getRequest()->getParam('export_over_time', 0);
    $export_note_contract   = $this->getRequest()->getParam('export_not_contract', 0);
    $export_last_month      = $this->getRequest()->getParam('export_last_month', 0);
    $export_temp_time_late  = $this->getRequest()->getParam('export_temp_time_late', 0);
 //Check locked time
    $QLockTime      = new Application_Model_LockTime();
    $where_lock     = array ();
    $where_lock[]   = $QLockTime->getAdapter ()->quoteInto ( 'month = ?', $month );
    $where_lock[]   = $QLockTime->getAdapter()->quoteInto('year = ? ', $year);
    $result_lock    = $QLockTime->fetchRow ( $where_lock );
    $this->view->locked_time = $result_lock;
    //End Check locked time
    if($off_long == null)
    $off_long = "0,1";

$this->view->off_long = $off_long;

$export_hr        = $this->getRequest()->getParam('export_hr', 0);
$export_hr_total  = $this->getRequest()->getParam('export_hr_total', 0);
$limit            = 20;
$QShift           = new Application_Model_Shift();
$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$QTime            = new Application_Model_Time2();
$note             = $this->getRequest()->getParam('note', null);
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
//$from_date    = $year . '-' . $month . '-01';
//$to_date      = $year . '-' . $month . '-' . $number_day_of_month;
$month_tmp =  ($month < 10) ? '0'. $month : $month;
    $from_date    = $year . '-' . $month_tmp. '-01';
    $to_date      = $year . '-' . $month_tmp . '-' . $number_day_of_month;
    
$QLeaveDetail = new Application_Model_LeaveDetail();
$QTimeLastMonth = new Application_Model_TimeLastMonth();
$QTeam  = new Application_Model_Team();
$staff_title_info = $userStorage->title;
$team_info = $QTeam->find($staff_title_info);
$team_info = $team_info->current();
$group_id = $team_info['access_group'];
$this->view->group_id = $group_id;
if(!empty($export_note_contract))
{
    set_time_limit(0);
    ini_set('memory_limit', '5120M');
    $month_tmp =  ($month < 10) ? '0'. $month : $month;
    $from_date    = $year . '-' . $month_tmp. '-01';
    $to_date      = $year . '-' . $month_tmp . '-' . $number_day_of_month;

    $db           = Zend_Registry::get('db');
    $sql_contract          = "CALL `PR_check_leave_not_contract`(:p_from_date, :p_to_date)";
 
    $stmt_contract = $db->prepare($sql_contract);
    $stmt_contract->bindParam('p_from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
    $stmt_contract->bindParam('p_to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);

    $stmt_contract->execute();
    $data_contract = $stmt_contract->fetchAll();
    $stmt_contract->closeCursor();
    $stmt_contract = $db = null;
    
    require_once 'PHPExcel.php';

    $alphaExcel = new My_AlphaExcel();

    $PHPExcel = new PHPExcel();
    $heads = array(
        $alphaExcel->ShowAndUp() => 'Code',
        $alphaExcel->ShowAndUp() => 'Name',
        $alphaExcel->ShowAndUp() => 'Title',
        $alphaExcel->ShowAndUp() => 'Off date',
        $alphaExcel->ShowAndUp() => 'Quỹ phép',
    );


    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();

    foreach($heads as $key => $value)
    {
        $sheet->setCellValue($key.'1', $value);
    }

    $index = 1;
    foreach($data_contract as $key => $value)
    {
        $alphaExcel = new My_AlphaExcel();

        $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['fullname'] ,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['quyphep'],PHPExcel_Cell_DataType::TYPE_STRING);
        $index++;

    }
    $filename = 'Leave_Not_contract - ' . date('Y-m-d H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');

    exit;

}
if(!empty($export_hr))
{
    set_time_limit(0);
    ini_set('memory_limit', '5120M');

    $db           = Zend_Registry::get('db');
    $sql          = "CALL `PR_Export_Staff_Time_New`(:from_date, :to_date)";

    $stmt = $db->prepare($sql);
    $stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
    $stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);

    $stmt->execute();
    $data_export = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = $db = null;

    require_once 'PHPExcel.php';

    $alphaExcel = new My_AlphaExcel();

    $PHPExcel = new PHPExcel();
    $heads = array(
        $alphaExcel->ShowAndUp() => 'Company',
        $alphaExcel->ShowAndUp() => 'Code',
        $alphaExcel->ShowAndUp() => 'CMND',
        $alphaExcel->ShowAndUp() => 'Name',
        $alphaExcel->ShowAndUp() => 'Department',
        $alphaExcel->ShowAndUp() => 'Team',
        $alphaExcel->ShowAndUp() => 'Title',
        $alphaExcel->ShowAndUp() => 'Area',
        $alphaExcel->ShowAndUp() => 'Join date',
        $alphaExcel->ShowAndUp() => 'Off date',
    );

    for($i = 1; $i <= $number_day_of_month; $i++)
    {
        $heads[$alphaExcel->ShowAndUp()] = $i;
    }

    $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
    $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
    $heads[$alphaExcel->ShowAndUp()] = 'Công training';
    $heads[$alphaExcel->ShowAndUp()] = 'Phép';
    $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
    $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
    $heads[$alphaExcel->ShowAndUp()] = 'Giờ tăng ca';

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();

    foreach($heads as $key => $value)
    {
        $sheet->setCellValue($key.'1', $value);
    }

    $index = 1;
    foreach($data_export as $key => $value)
    {
        $alphaExcel = new My_AlphaExcel();

        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['com_name']);
        //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);
        //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'');
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );

        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value[$i]);
        }

        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congthuong']);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['conggay']);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congtraining']);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phep']+$value['phepcty']);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congchunhat']);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congle']);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['overtime']);
        $index++;

    }
    $filename = 'Export Time - ' . date('Y-m-d H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');

    exit;

}

if(!empty($export_hr_total))
{
    set_time_limit(0);
    ini_set('memory_limit', '5120M');

    $db           = Zend_Registry::get('db');
    $sql          = "CALL `PR_Export_Total_Staff_Time_New`(:from_date, :to_date)";
//    echo $from_date;
//    echo $to_date;
//    exit();
    $stmt = $db->prepare($sql);
    $stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
    $stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
    
    $stmt->execute();
    $data_export = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = $db = null;
    require_once 'PHPExcel.php';

    $alphaExcel = new My_AlphaExcel();

    $PHPExcel = new PHPExcel();
    	
    $heads_month = array(
        'K' => '1',
        'L' => '2',
        'M' => '3',
        'N' => '4',
        'O' => '5',
        'P' => '6',
        'Q' => '7',
        'R' => '8',
        'S' => '9',
        'T' => '10',
        'U' => '11',
        'V' => '12',
        'W' => '13',
        'X' => '14',
        'Y' => '15',
        'Z' => '16',
        'AA' => '17',
        'AB' => '18',
        'AC' => '19',
        'AD' => '20',
        'AE' => '21',
        'AF' => '22',
        'AG' => '23',
        'AH' => '24',
        'AI' => '25',
        'AJ' => '26',
        'AK' => '27',
        'AL' => '28',
        'AM' => '29',
        'AN' => '30',
        'AO' => '31'
    );


    $heads_mid = array_slice($heads_month,0,$number_day_of_month);
    $heads_first = array(
        'A' => 'Company',
        'B' => 'Code',
        'C' => 'CMND',
        'D' => 'Name',
        'E' => 'Department',
        'F' => 'Team',
        'G' => 'Title',
        'H' => 'Area',
        'I' => 'Join date',
        'J' => 'Off date'
    );

    $heads_last = array(
        'AP' => 'Công thường',
        'AQ' => 'Công gãy',
        'AR' => 'Công training',
        'AS' => 'Công chủ nhật',
        'AU' => 'Ngày lễ X2',
        'AV' => 'Ngày lễ X3',
        'AW' => 'Ngày lễ X4',
        'AX' => 'Thứ 7 X2',
        'AY' => 'Phép năm sử dụng trong tháng',
        'AZ' => 'Ứng phép/Hoàn phép',
        'BA' => 'Nghỉ việc riêng được hưởng lương',
        'BB' => 'Tổng giờ tăng ca trong tháng',
        'BC' => 'Ngày nghỉ được hưởng lương',
        'BD' => 'STT',
        'BE' => 'Tên hợp đồng',
        'BF' => 'Ngày điều chỉnh',
    );
    $heads = array_merge($heads_first,$heads_mid,$heads_last);


    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();

  
    foreach($heads as $key => $value)
        $sheet->setCellValue($key.'1', $value);
       
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
       
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();

            $sheet->setCellValue('A' . ($index+1), $value['com_name']);
            $sheet->setCellValueExplicit('B' . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('C' . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);

            $sheet->setCellValue('D' . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue('E' . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue('F' . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue('G' . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue('H' . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue('I' . ($index+1), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'');
                $sheet->setCellValue('J' . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );


            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                $sheet->setCellValue(array_search($i,$heads_mid) . ($index+1), $value[$i]);
            }

            $sheet->setCellValue('AP' . ($index+1), $value['congthuong']);
            $sheet->setCellValue('AQ' . ($index+1), $value['conggay']);
            $sheet->setCellValue('AR' . ($index+1), $value['congtraining']);
            $sheet->setCellValue('AS' . ($index+1), $value['congchunhat']);
            $sheet->setCellValue('AU' . ($index+1), $value['holidayX2']);
            $sheet->setCellValue('AV' . ($index+1), $value['holidayX3']);
            $sheet->setCellValue('AW' . ($index+1), $value['holidayX4']);
            $sheet->setCellValue('AX' . ($index+1), $value['saturdayX2']);
            $sheet->setCellValue('AY' . ($index+1), $value['phepnam']);
            $sheet->setCellValue('AZ' . ($index+1), $value['tamungphepnew']);
            $sheet->setCellValue('BA' . ($index+1), $value['phepcty']);
            $sheet->setCellValue('BB' . ($index+1), $value['overtime']);
            $sheet->setCellValue('BC' . ($index+1), $value['lehuongluong']);
            $sheet->setCellValue('BD' . ($index+1), $value['RowNumber']);
            $sheet->setCellValue('BE' . ($index+1), $value['contract_name']);
            $sheet->setCellValue('BF' . ($index+1), ($value['from_date_transfer'] >= $from_date && $value['from_date_transfer'] <= $to_date && $value['RowNumber'] > 1) ? $value['from_date_transfer'] : '');
            // set dong tren
            if($value['from_date_transfer'] >= $from_date && $value['from_date_transfer'] <= $to_date && $value['RowNumber'] > 1){
                $sheet->setCellValue('BF' . ($index),$value['from_date_transfer']);
            }
            $index++;

        }
        	
        $filename = 'Export Time Total - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        exit;
}

if(in_array($userStorage->group_id, $not_approve_self))
{
    $this->view->not_approve_self = 1;
}
else
{
    $this->view->not_approve_self = 0;
}

$approve_all_by_staff = $this->getRequest()->getParam('approve_all_by_staff');
$deapprove_all_by_staff = $this->getRequest()->getParam('deapprove_all_by_staff');
$date_approve = $this->getRequest()->getParam('date-approve');
$date_none_approve = $this->getRequest()->getParam('date-none-approve');

if(!empty($date_approve))
{
    $staff_approve = $this->getRequest()->getParam('staff_approve');
    $params_approve_date = array(
        'staff_id' => $staff_approve,
        'date' => $date_approve,
        'admin' => $userStorage->id,
        'note' => ''
    );
    $QTime->approveAllTempTimeByStaffDate($params_approve_date);
}


if(!empty($approve_all_by_staff))
{
    $month_edit = $this->getRequest()->getParam('month_edit');
    $year_edit = $this->getRequest()->getParam('year_edit');

    $params_approve_all = array(
        'staff_id' => $approve_all_by_staff,
        'month' => $month_edit,
        'year' => $year_edit,
        'admin' => $userStorage->id,
        'note' => ''
    );
    $QTime->approveAllTempTimeByStaff($params_approve_all);

}

if($this->getRequest()->getParam('approve-none-office') )
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');
    $QStaff = new Application_Model_Staff();
    $currentStaff = $QStaff->find($staff_id);
    $currentStaff = $currentStaff[0];

    $db  = Zend_Registry::get('db');
    $sql = "
                UPDATE time t
                    JOIN staff s ON t.staff_id = s.id
                SET t.status = 1,
                    t.updated_at = '" . date('Y-m-d h:i:s') . "' ,
                    t.approved_at = '" . date('Y-m-d h:i:s') . "' ,
                    t.updated_by = '" . $userStorage->id . "',
                    t.approved_by = '" . $userStorage->id . "',
                t.regional_market = ". $currentStaff->regional_market . "
                WHERE date(t.created_at) = '" . $date ."'
                    AND t.staff_id = " . $staff_id ."
                    AND s.joined_at <= '". $date .
                    "' AND (s.off_date IS NULL OR s.off_date > '" . $date . "')";
    $db->query($sql);
}

if($this->getRequest()->getParam('unapprove-none-office'))
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');

    $sql = "UPDATE `time`
                    SET status = 0, approve_time = 0, approved_by = NULL
                    WHERE DATE(created_at) = :date
                        AND staff_id = :staff_id";
    $db  = Zend_Registry::get('db');

    $stmt = $db->prepare($sql);
    $stmt->bindParam("date", My_Util::escape_string($date), PDO::PARAM_STR);
    $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);

    $stmt->execute();
    $stmt->closeCursor();
    $stmt = $db = null;
}

if($this->getRequest()->getParam('approve-office'))
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');
    if(!empty($staff_id)){
        $QTime->approveOffice($staff_id, $date, $userStorage->id);
    }
    
}

if($this->getRequest()->getParam('approve-office'))
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');
    if(!empty($staff_id)){
        $QTime->approveOffice($staff_id, $date, $userStorage->id);
    }
    
}

if($this->getRequest()->getParam('hr-approve-office'))
{
    $hr_time = $this->getRequest()->getParam('hr_time');
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date_detail');
    if(!empty($staff_id)){
        $QTime->approveOfficeHR($staff_id, $date, $hr_time, $userStorage->id);
    }
    
}

if($this->getRequest()->getParam('approve-temp-time'))
{
    $id_update = $this->getRequest()->getParam('approve-temp-time');
    $QTime->approveTempTime($id_update, $note);
}

if($this->getRequest()->getParam('btn_reject_temp'))
{
    $temp_id_reject = $this->getRequest()->getParam('id_temp_reject');
    $reason_reject = $this->getRequest()->getParam('reason_reject');
    $QTime->rejectTempTime($temp_id_reject, $reason_reject);
}

if($this->getRequest()->getParam('approve-all-temp-time'))
{

    $month_approve_all = $this->getRequest()->getParam('month_approve_all');
    $year_approve_all = $this->getRequest()->getParam('year_approve_all');
    $user_approve_all = $this->getRequest()->getParam('user_approve_all');

    $params_approve_all = array(
        'month' => $month_approve_all,
        'year' => $year_approve_all,
        'staff_id' => $user_approve_all,
        'note' => $note,
    );

    $QTime->approveAllTempTimeByStaff($params_approve_all);
}

if(!empty($this->getRequest()->getParam('submit_approve_all')))
{
    $QLeave = new Application_Model_LeaveDetail();
    $json_temp_id = json_decode($this->getRequest()->getParam('list_temp_id'), true);
    $json_remove_time = json_decode($this->getRequest()->getParam('list_remove_time'), true);
    $json_leave_id = array_unique(json_decode($this->getRequest()->getParam('list_leave_id'), true));
    $json_time_id = array_unique(json_decode($this->getRequest()->getParam('list_time_id'), true));
    $staff_id = $this->getRequest()->getParam('staff_id_approve');

    $params_approve_select = array(
        'temp_id' => $json_temp_id,
        'leave_id' => $json_leave_id,
        'remove_time' => $json_remove_time,
        'staff_id' => $staff_id,
        'admin' => $userStorage->id
    );
    $QTime->approveSelect($params_approve_select);

    $db = Zend_Registry::get('db');
    if(!empty($json_time_id))
    {
        $sql = "UPDATE time set status = 1 where id IN (" . implode(",", $json_time_id) . ")";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
    }

    if(!empty($json_remove_time))
    {
        $sql = "DELETE FROM time where staff_id = " . $staff_id . " AND id IN (" . implode(",", $json_remove_time) . ")";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;

        // $sql = "DELETE FROM time_image_location where  time_id IN (" . implode(",", $json_remove_time) . ")";
        // $stmt = $db->prepare($sql);
        // $stmt->execute();
        // $stmt->closeCursor();
        // $stmt = null;
    }

    foreach($json_leave_id as $val_leave)
    {
        $QLeave->updateStatus(array('status'=>1,'id'=>$val_leave));
    }
    $db = null;
}

if($this->getRequest()->getParam('update-none-office')) // Nếu HR hoặc ASM cập nhật công
{
    $QTime = new Application_Model_Time2();

    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date_detail');
    $shift= $this->getRequest()->getParam('shift');
    $QStaff = new Application_Model_Staff();
    $currentStaff = $QStaff->find($staff_id);
    $currentStaff = $currentStaff[0];
    $db  = Zend_Registry::get('db');
    $select = $db->select()
    ->from(array('t' => 'time'))
    ->where('date(t.created_at) = ?', $date)
    ->where('t.staff_id = ?', $staff_id);
    $check_data = $db->fetchAll($select);
    if(count($check_data) > 0)
    {
        $sql = "
        UPDATE time t
        JOIN staff s ON t.staff_id = s.id
        SET t.shift = $shift,
        t.updated_at = '" . date('Y-m-d h:i:s') . "' ,
                        t.approved_at = '" . date('Y-m-d h:i:s') . "' ,
                        t.updated_by = '" . $userStorage->id . "',
                        t.approved_by = '" . $userStorage->id . "',
                        t.status = 1,
                    t.regional_market = ". $currentStaff->regional_market . "
                    WHERE date(t.created_at) = '" . $date ."'
                        AND t.staff_id = " . $staff_id ."
                        AND s.joined_at <= '". $date .
                        "' AND (s.off_date IS NULL OR s.off_date > '" . $date . "')";

        $db->query($sql);
    }
    else
    {

        if(strtotime($currentStaff->joined_at) <= strtotime($date) && (strtotime($currentStaff->off_date) > strtotime($date) || empty($currentStaff->off_date)))
        {
            $data['shift'] = $shift;
            $data['created_at'] = date($date .' h:i:s');
            $data['approved_at'] = date('Y-m-d h:i:s');
            $data['created_by'] = $userStorage->id;
            $data['approved_by'] = $userStorage->id;
            $data['staff_id'] = $staff_id;
            $data['status'] = 1;
            $QTime->insert($data);
        }
        else
        {
        }
    }
}
    $QTime = new Application_Model_Time2();
    $list_allowance = $QTime->getAllowance();
    $params = array(
        'off' => 1,
        'name' => $name,
        'code' => $code,
        'email' => $email,
        'only_training' => $only_training,
        'month' => $month,
        'year' => $year,
        'area' => $area,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'off_long' => $off_long
    );
    if(empty($code)){
        $code=null;
    }
    if(empty($name)){
        $name=null;
    }
    if(empty($email)){
        $email=null;
    }
    if(empty($area)){
        $area=null;
    }
    if(empty($department)){
        $department=null;
    }
    if(empty($team)){
        $team=null;
    }
    if(empty($title)){
        $title=null;
    }
    $user_id = $userStorage->id;
    $params_day = array(
        'from' => $params['year'] . '-' . $params['month'] . '-01',
        'to' => $params['year'] . '-' . $params['month']  . '-' .$number_day_of_month,
    );
//    $group_id = $userStorage->group_id;
    
    if ($group_id == PGPB_ID){
        $params['staff_id'] = $user_id;
    }elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID){
        $params['asm'] = $user_id;
    }elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID,TRAINING_LEADER_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID))){
        $params['other'] = $user_id;
    }elseif($group_id == LEADER_ID){
        $params['leader'] = $user_id;
    }elseif($group_id == SALES_ADMIN_ID){
        $params['sale_admin'] = $user_id;
    }

    //set quyen cho hong nhung trainer
    elseif ($user_id == 7278 || $user_id == 240 || $user_id == 12719){
        $params['other'] = $user_id;
    }
    //chi van ha noi
    elseif ($user_id == 95){
        $params['other'] = $user_id;
    }
    elseif ($group_id == 22)
    $params['other'] = $user_id;
    elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915 || $user_id ==87)
    $params['asm'] = $user_id;
    else{
        $params['sale'] = $user_id;
    }
    $params['dev'] = $dev;
    
    $public_holyday = $QTime->getPublicHolyday($params);
    
    if($this->getRequest()->getParam('approve-all') )
    {
        $QTime->approveAll($params);
    }
    
    //if ($user_id == SUPERADMIN_ID || in_array($group_id, array( ADMINISTRATOR_ID,HR_ID,HR_EXT_ID)))
		if ($user_id == SUPERADMIN_ID || in_array($group_id, array( ADMINISTRATOR_ID,HR_ID,HR_EXT_ID)))
    {
    
        $from_month = $params_day['from'];
        $to_month = $params_day['to'];
        $db           = Zend_Registry::get('db');
        $stmt = $db->prepare("CALL `PR_get_staff_filter`(:p_from_date, :p_to_date, :p_code, :p_name, :p_email, :p_area, :p_department, :p_team, :p_title)");
        $stmt->bindParam('p_from_date', My_Util::escape_string($from_month), PDO::PARAM_STR);
        $stmt->bindParam('p_to_date', My_Util::escape_string($to_month), PDO::PARAM_STR);
        $stmt->bindParam('p_code', My_Util::escape_string($code), PDO::PARAM_STR);
        $stmt->bindParam('p_name', My_Util::escape_string($name), PDO::PARAM_STR);
        $stmt->bindParam('p_email', My_Util::escape_string($email), PDO::PARAM_STR);
        $stmt->bindParam('p_area', My_Util::escape_string($area), PDO::PARAM_STR);
        $stmt->bindParam('p_department', My_Util::escape_string($department), PDO::PARAM_STR);
        $stmt->bindParam('p_team', My_Util::escape_string($team), PDO::PARAM_STR);
        $stmt->bindParam('p_title', My_Util::escape_string($title), PDO::PARAM_STR);
        $stmt->execute();
        $staffBrandShop = $stmt->fetchAll();
        $stmt->closeCursor();
        $list_of_subordinates = array();
        foreach($staffBrandShop as $key_per => $val_per){
            $list_of_subordinates[] = $val_per['id'];
        }
       
        $total_of_subordinates =  count($list_of_subordinates);
        if( !empty($export_temp_time_late) || !empty($export_over_time) || !empty($export_last_month) || !empty($export) ){
            $list_of_subordinates = array_slice($list_of_subordinates,null,null);
        }
        else{
            $list_of_subordinates = array_slice($list_of_subordinates,($page-1)*$limit,$limit);
        }
        $list_of_subordinates = implode(",",$list_of_subordinates);

    }
//    elseif($user_id == 12719){
//        $from_month = $params_day['from'];
//        $sql_staff = "SELECT id FROM `staff` WHERE office_id = 48 AND team = 397 AND ( off_date IS NULL OR off_date >= '$from_month') ";
//
//        $db   = Zend_Registry::get('db');
//        $stmt = $db->prepare($sql_staff);
//        $stmt->execute();
//        $staffBrandShop = $stmt->fetchAll();
//        $array_personal = array();
//        foreach($staffBrandShop as $key_per => $val_per){
//            $array_personal[] = $val_per['id'];
//        }
//        $list_of_subordinates = implode(",",$array_personal);
//    }
    else{


 
        $db           = Zend_Registry::get('db');
        $stmt = $db->prepare("CALL `PR_get_permission_by_staffcode_filter`(:p_staff_code,:p_from_date,:p_to_date,:p_code,:p_name,:p_email,:p_area,:p_department,:p_team,:p_title)");
        $stmt->bindParam('p_staff_code', My_Util::escape_string($userStorage->code), PDO::PARAM_STR);
        $stmt->bindParam('p_from_date', My_Util::escape_string($params_day['from']), PDO::PARAM_STR);
        $stmt->bindParam('p_to_date', My_Util::escape_string($params_day['to']), PDO::PARAM_STR);
        $stmt->bindParam('p_code', My_Util::escape_string($code), PDO::PARAM_STR);
        $stmt->bindParam('p_name', My_Util::escape_string($name), PDO::PARAM_STR);
        $stmt->bindParam('p_email', My_Util::escape_string($email), PDO::PARAM_STR);
        $stmt->bindParam('p_area', My_Util::escape_string($area), PDO::PARAM_STR);
        $stmt->bindParam('p_department', My_Util::escape_string($department), PDO::PARAM_STR);
        $stmt->bindParam('p_team', My_Util::escape_string($team), PDO::PARAM_STR);
        $stmt->bindParam('p_title', My_Util::escape_string($title), PDO::PARAM_STR);
        $stmt->execute();
        $list_of_subordinates_tmp = $stmt->fetchAll();
        $stmt->closeCursor();
        $list_of_subordinates = array();
        foreach ($list_of_subordinates_tmp as $key=> $value){
            array_push($list_of_subordinates,$value['id']);
        }
       /* if($userStorage->id == 326){
                    $list_of_subordinates = array_diff($list_of_subordinates,array(23154));
        }*/
        $total_of_subordinates =  count($list_of_subordinates);
        if( !empty($export_temp_time_late) || !empty($export_over_time) || !empty($export_last_month) || !empty($export) ){
            $list_of_subordinates = array_slice($list_of_subordinates,null,null);
        }
        else{
            $list_of_subordinates = array_slice($list_of_subordinates,($page-1)*$limit,$limit);        
        }
                
        $list_of_subordinates = implode(",",$list_of_subordinates);

    }
   
    // echo $list_of_subordinates;die;

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $array_ddtm = array(
        24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
    );


    $QCheckIn = new Application_Model_CheckIn();
    $off_date = $QCheckIn->getDateInfo($params_day);
    	
    $db           = Zend_Registry::get('db');
    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $sql          = "CALL `PR_List_Staff_Time_New`(:from_date, :to_date, :p_staff_id, :p_off_long)";
    
    $stmt = $db->prepare($sql);
    $stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
    $stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
    $stmt->bindParam('p_staff_id', My_Util::escape_string($list_of_subordinates), PDO::PARAM_STR);
    $stmt->bindParam('p_off_long', My_Util::escape_string($off_long), PDO::PARAM_STR);
    $stmt->execute();
    $list_staff_view = $stmt->fetchAll();
   
   
    $stmt->closeCursor();
    
    $sql_pending          = "CALL `PR_List_Staff_Time_Approve`(:from_date, :to_date, :p_staff_id)";
    
    $stmt_pending = $db->prepare($sql_pending);
    $stmt_pending->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
    $stmt_pending->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
    $stmt_pending->bindParam('p_staff_id', My_Util::escape_string($list_of_subordinates), PDO::PARAM_STR);
    
    $stmt_pending->execute();
    $list_staff_pending = $stmt_pending->fetchAll();
    $stmt_pending->closeCursor();
    
    $arr_pending = array();
    if(!empty($list_staff_pending)){
        foreach ($list_staff_pending as $k => $val){
            $arr_pending[] = $val['staff_id'];
        }
    }
    
    $array_staff_id = array();
    $array_cmnd = array();

    $max_page = ceil($total_of_subordinates / $limit);
    $page_trainer = $page - $max_page;
    $sum_offset = ($limit * $max_page) - $data['total'];
    $limit_trainer = (($limit - count($total_of_subordinates)) > 0 && count($data['data'] > 0))?($limit - count($data['data'])):$limit;
    foreach ($total_of_subordinates as $key => $value) {
        $array_staff_id[] = $value['id'];
        $array_cmnd[] = $value['cmnd'];
    }

    if($page < $max_page)
    {
        $data_new_staff_trainer['data'] = null;
    }

    if(!empty(implode(",", $array_staff_id)))
    {
        $params['list_staff_id'] = "(" . implode(",", $array_staff_id) . ")";
        $params['list_cmnd'] = "('" . implode("','", $array_cmnd) . "')";
        $data_training = $QTime->getListTrainingByStaffId($params);
        $data_allowance = $QTime->getListAllowanceByStaffId($params);
        $data_leave = $QTime->getListLeaveByStaffId($params);

        $list_training_check_in = $QTime->getListTrainingCheckIn($params);

        $this->view->data_training = $data_training;
        $this->view->data_allowance = $data_allowance;
        $this->view->data_leave = $data_leave;
    }

    $this->view->staffs_trainer = $data_new_staff_trainer['data'];
    $this->view->limit = $limit;
    $this->view->total = $total_of_subordinates ;
            // + $data_new_staff_trainer['total'];
    $this->view->number_day_of_month = $number_day_of_month;
    $this->view->url = HOST . 'staff-time/list-staff-check-in' . ($params ? '?' . http_build_query($params) .
        '&' : '?');

    $this->view->offset = $limit * ($page - 1);

    unset($params['list_staff_id']);
    unset($params['list_cmnd']);
    $this->view->public_holyday = $public_holyday;
    $this->view->shift = $QShift->getShiftByTitle($userStorage->title);
    $this->view->current_user  = $userStorage;
    $QArea = new Application_Model_Area();
    $all_area =  $QArea->fetchAll(null, 'name');
    $this->view->areas = $all_area;
    $this->view->staff_pending = $arr_pending;
	//$params['month']=7;
    $this->view->params = $params;
    $this->view->list_allowance = $list_allowance;
    $this->view->array_ddtm = $array_ddtm;
    $this->view->off_date = $off_date;
    $QCheckIn               = new Application_Model_CheckIn();
    $this->view->list_staff_view = $list_staff_view;
  

    if(isset($export_temp_time_late) and $export_temp_time_late){
         $list_export_staff_id = $list_of_subordinates;
         
          require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $alphaExcel = new My_AlphaExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'STT',
            $alphaExcel->ShowAndUp() => 'KHU VỰC',
            $alphaExcel->ShowAndUp() => 'CODE',
            $alphaExcel->ShowAndUp() => 'TÊN NHÂN VIÊN',
            $alphaExcel->ShowAndUp() => 'PHÒNG BAN',
            $alphaExcel->ShowAndUp() => 'TEAM',
            $alphaExcel->ShowAndUp() => 'CHỨC DANH',
            $alphaExcel->ShowAndUp() => 'NGÀY CÔNG ĐƯỢC BỔ SUNG',
            $alphaExcel->ShowAndUp() => 'NGÀY THỰC HIỆN BỔ SUNG CÔNG',
            $alphaExcel->ShowAndUp() => 'KHOẢNG CÁCH GIỮA NGÀY THỰC HIỆN VÀ NGÀY CÔNG ĐƯỢC BỔ SUNG',
			$alphaExcel->ShowAndUp() => 'LÝ DO',
			
        );
    
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
    
        foreach($heads as $key => $value)
            $sheet->setCellValue($key.'1', $value);
    
            $sheet->getStyle('A1:H1')->applyFromArray(array('font' => array('bold' => true)));
    
    
            $sheet->getColumnDimension('A')->setWidth(10);
            $sheet->getColumnDimension('B')->setWidth(30);
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(15);
            $sheet->getColumnDimension('F')->setWidth(30);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('H')->setWidth(15);
             $db           = Zend_Registry::get('db');
        $sql          = "CALL `PR_temp_time_late`(:from_date, :to_date, :p_staff_id )";
    
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
        $stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
        $stmt->bindParam('p_staff_id', My_Util::escape_string($list_export_staff_id), PDO::PARAM_STR);
        
        $stmt->execute();
        $data_export = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
    
            foreach ($data_export as $key => $value) {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['area']);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['dep']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['team']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['title']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['date'])));
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['created_at'])));
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['day_diff']);
				$sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['reason']);
				
            }
    
            $filename_over = 'TempTimeLate - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
            $objWriter->save('php://output');
            exit;
    }
    if(isset($export_over_time) and $export_over_time)
    {
        $list_export_staff_id = $list_of_subordinates;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $alphaExcel = new My_AlphaExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'STT',
            $alphaExcel->ShowAndUp() => 'KHU VỰC',
            $alphaExcel->ShowAndUp() => 'PHÒNG BAN',
            $alphaExcel->ShowAndUp() => 'TEAM',
            $alphaExcel->ShowAndUp() => 'CHỨC DANH',
            $alphaExcel->ShowAndUp() => 'CODE',
            $alphaExcel->ShowAndUp() => 'TÊN NHÂN VIÊN',
            $alphaExcel->ShowAndUp() => 'NGÀY PHÁT SINH',
        );
    
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
    
        foreach($heads as $key => $value)
            $sheet->setCellValue($key.'1', $value);
    
            $sheet->getStyle('A1:H1')->applyFromArray(array('font' => array('bold' => true)));
    
    
            $sheet->getColumnDimension('A')->setWidth(10);
            $sheet->getColumnDimension('B')->setWidth(30);
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(15);
            $sheet->getColumnDimension('F')->setWidth(30);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('H')->setWidth(15);
            
            $data_export_over = $QLeaveDetail->reportOverTime($list_export_staff_id);
    
            foreach ($data_export_over as $key => $value) {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['areaname']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['depname']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['teamname']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['titlename']);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staffname']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['date'])));
            }
    
            $filename_over = 'OverTime - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
            $objWriter->save('php://output');
            exit;
    }

    if(isset($export_last_month) and $export_last_month){
        
        //Chi tiet bu cong
        $sql_detail = "SELECT st.id, st.code, CONCAT(st.firstname, ' ', st.lastname) AS  'fullname',tlm.date,tlm.`approved_at`,
                    CASE 
                     WHEN IFNULL(tlm.shift,0) = 2 THEN '1'
                     WHEN IFNULL(tlm.shift,0) = 9 THEN '0.5'
                     ELSE '1' END as 'shift'

                    FROM `time_last_month` tlm 
                    inner JOIN staff st ON st.id = tlm.staff_id
                    WHERE ( tlm.`approved_at` BETWEEN :from_date AND :to_date) AND tlm.`status` = 1
                    AND FIND_IN_SET(tlm.staff_id,:list_staff_id)
                    ORDER BY st.id, tlm.date
	";
       $from_date_begin =  date('Y-m-d 00:00:00', strtotime($from_date));
       $from_date_end   =  date('Y-m-d 23:59:59', strtotime($to_date));

        $stmt = $db->prepare($sql_detail);
        $stmt->bindParam('from_date', My_Util::escape_string($from_date_begin), PDO::PARAM_STR);
        $stmt->bindParam('to_date', My_Util::escape_string($from_date_end), PDO::PARAM_STR);
        $stmt->bindParam('list_staff_id', $list_of_subordinates, PDO::PARAM_STR);
        $stmt->execute();

        $list_detail = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        set_time_limit(0);
        ini_set('memory_limit', '5120M');
        $list_export_staff_id = $list_of_subordinates;
       
        $db           = Zend_Registry::get('db');
        $sql          = "CALL `PR_Export_Time_Last_Month`(:from_date, :to_date, :p_staff_id )";
    
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
        $stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
        $stmt->bindParam('p_staff_id', My_Util::escape_string($list_export_staff_id), PDO::PARAM_STR);
        
    
        $stmt->execute();
        $data_export = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
    
        require_once 'PHPExcel.php';
    
        $alphaExcel = new My_AlphaExcel();
    
        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
        );
    
        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
//        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        $heads[$alphaExcel->ShowAndUp()] = 'Ngày lễ X2';
        $heads[$alphaExcel->ShowAndUp()] = 'Ngày lễ X3';
        $heads[$alphaExcel->ShowAndUp()] = 'Ngày lễ X4';
    
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
    
        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }
    
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();
    
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['com_name']);
            //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);
            //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
    
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congthuong']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['conggay']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congtraining']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phep']+$value['phepcty']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congchunhat']);
//            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congle']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['holidayX2']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['holidayX3']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['holidayX4']);
            $index++;
    
        }
        $PHPExcel->setActiveSheetIndex(0);
            //$sheet = $PHPExcel->getActiveSheet();
             $sheet = $PHPExcel->createSheet(1);
             $alpha = 'A';
            $index = 1; 
            $sheet->setCellValue($alpha++ . $index, "Mã NV" );
            $sheet->setCellValue($alpha++ . $index, "Họ Tên" );
            $sheet->setCellValue($alpha++ . $index, "Ngày" );
            $sheet->setCellValue($alpha++ . $index, "Số ngày bù công" );
            $sheet->setCellValue($alpha++ . $index, "Thời gian xác nhận" );
            
                $sheet->getStyle('A1:J1')->applyFromArray(array('font' => array('bold' => true)));
            
                $sheet->getColumnDimension('A')->setWidth(10);
                $sheet->getColumnDimension('B')->setWidth(30);
            
                foreach ($list_detail as $key => $value) {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['fullname']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['date']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['shift']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['approved_at']);
                }
        $filename = 'Export Time - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
    
        exit;
    
    }
    
    if(!empty($export)){
        $list_export_staff_id = $list_of_subordinates;
        $db           = Zend_Registry::get('db');
        $sql          = "CALL `PR_List_Staff_Time_New`('$from_date', '$to_date', '$list_export_staff_id', '$off_long')";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data_export = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
         
        set_time_limit(0);
        ini_set('memory_limit', '5120M');
    
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    
        require_once 'PHPExcel.php';
    
        $alphaExcel = new My_AlphaExcel();
    
        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Company',
            $alphaExcel->ShowAndUp() => 'Code',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Name',
            $alphaExcel->ShowAndUp() => 'Department',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Title',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Join date',
            $alphaExcel->ShowAndUp() => 'Off date',
        );
    
        for($i = 1; $i <= $number_day_of_month; $i++)
        {
            $heads[$alphaExcel->ShowAndUp()] = $i;
        }
    
        $heads[$alphaExcel->ShowAndUp()] = 'Công thường';
        $heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
        $heads[$alphaExcel->ShowAndUp()] = 'Công training';
        $heads[$alphaExcel->ShowAndUp()] = 'Phép';
        $heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
        $heads[$alphaExcel->ShowAndUp()] = 'Công lễ';
        $heads[$alphaExcel->ShowAndUp()] = 'Giờ tăng ca';
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
    
        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }
    
        $index = 1;
        foreach($data_export as $key => $value)
        {
            $alphaExcel = new My_AlphaExcel();
             
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['com_name']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
    
            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value[$i]);
            }
    
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congthuong']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['conggay']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congtraining']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phep']+$value['phepcty']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congchunhat']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congle']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['overtime']);
             
            $index++;
    
        }
        $filename = 'Staff Check In - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    
    
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
    
        exit();
         
    }
