<?php 


    $code   = $this->getRequest()->getParam('code');
    $export   = $this->getRequest()->getParam('export');

    if($code){
        $QStaffPermission = new Application_Model_StaffPermission();
        $params = array(
                "code"      => $code,
                "name"      => '',
                "email"     => '',
                "department" => '',
                "office"    => '',
                "permission" => '',
                'limit'     => '',
            );
        $data = $QStaffPermission->fetchPagination($params);
        $this->view->data = $data['data'];
        
        $db = Zend_Registry::get('db');
        $sql_personal = $db->prepare("SELECT 
                    pp.*,
                    CONCAT(st.firstname, ' ', st.lastname) as `manager_name`,
                    st.code as `manager_code`,
                    CONCAT(st2.firstname, ' ', st2.lastname) as `staff_name`,
                    st2.code as `staff_code`,
                    t1.name as `manager_department`,
                    t2.name as `staff_department`
                FROM `personal_permission` pp
                JOIN `staff` st ON pp.line_manager = st.id
                JOIN `staff` st2 ON pp.staff_id = st2.id
                JOIN `team` t1 ON st.department = t1.id
                JOIN `team` t2 ON st2.department = t2.id
                WHERE st.code = :p_code ");
        $sql_personal->bindParam('p_code', My_Util::escape_string($code), PDO::PARAM_STR); 
        $sql_personal->execute();
        $data_personal = $sql_personal->fetchAll();
        $sql_personal->closeCursor();
        $sql_personal = null;
        $this->view->data_personal = $data_personal;

        $stmt = $db->prepare("CALL `PR_get_permission_by_staffcode`(:p_code)");
        $stmt->bindParam('p_code', My_Util::escape_string($code), PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = null;
     
        $listStafId = array();
        if(!empty($result)){
            foreach ($result as $key => $value) {
                $listStafId[] = $value['id'];
            }
            if(!empty($data_personal)){
                foreach ($data_personal as $key => $value) {
                    $listStafId[] = $value['staff_id'];
                }
            }
            if(!empty($listStafId)){
                $listStafId_str = implode(',', $listStafId);

                $sql_staff = $db->prepare("SELECT st.id, st.code, CONCAT(st.firstname, ' ', st.lastname) AS fullname,
                    de.name as department, 
                    te.name as team, 
                    ti.name as title,
                    rm.`name` AS province_name
                                FROM staff st 
                                INNER JOIN  team ti ON ti.id = st.title
                                INNER JOIN  team de ON de.id = st.department
                                INNER JOIN  team te ON te.id = st.team
                                LEFT JOIN regional_market rm ON st.regional_market = rm.id
                                WHERE st.id IN (:p_listStafId_str)");
                 $sql_staff->bindParam('p_listStafId_str', My_Util::escape_string($listStafId_str), PDO::PARAM_STR); 
//                            $stmt_staff = $db->prepare($sql_staff);
                            $sql_staff->execute();

                            $list_staff = $sql_staff->fetchAll();
                            $sql_staff->closeCursor();
                            $sql_staff = null;
                            $this->view->list = $list_staff;
            }
        }
   
        if ($export){
            $this->exportPermission($list_staff);
        }
        $this->view->code = $code;
    }
    