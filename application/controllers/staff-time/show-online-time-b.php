<?php

$page             = $this->getRequest()->getParam('page', 1);
$name             = $this->getRequest()->getParam('name',null);
$code             = $this->getRequest()->getParam('code',null);
$email            = $this->getRequest()->getParam('email',null);
$export           = $this->getRequest()->getParam('export',0);
$search           = $this->getRequest()->getParam('search',0);
//$month            = $this->getRequest()->getParam('month', date('m'));
$month            = $this->getRequest()->getParam('month');

$week             = $this->getRequest()->getParam('week', date('W'));
$year             = $this->getRequest()->getParam('year', date('Y'));
$area             = $this->getRequest()->getParam('area',null);
$department       = $this->getRequest()->getParam('department',null);
$team             = $this->getRequest()->getParam('team',null);
$title            = $this->getRequest()->getParam('title',null);
$dev              = $this->getRequest()->getParam('dev',0);

//xu li truong hop search va co thong tin ngay thang
if($search==1 and $month!=date('m')){
    //get week number from date
    $ddate= date("d-m-Y", strtotime("first monday $year-$month"));
    $date = new DateTime($ddate);
    $week = $date->format("W");
}

else if($search==1 and $month==date('m')){
    //co seracg va thang trung hien tai thi lay tuan hien tai

    $week = date('W');

}
//ko search gi het thi lay thang hien tai
$month            = $this->getRequest()->getParam('month', date('m'));



//lay ngay thang trong tuan hien tai

$date = new DateTime();
$weekNow = $week;
$yearNow= $year;
$dateNow = date("Y-m-d");// current date
$dateOfWeek=array();

$week_array = $this->getStartAndEndDate($weekNow,$yearNow);
$startWeekDate=$week_array['week_start'];
$endWeekDate=$week_array['week_end'];

//$date = strtotime(date("Y-m-d", strtotime($date)) . " +1 day");
$dateOfWeek[]= date("d/m", strtotime('monday this week', strtotime($startWeekDate)));
$dateOfWeek[]= date("d/m", strtotime('tuesday this week', strtotime($startWeekDate)));
$dateOfWeek[]= date("d/m", strtotime('wednesday this week', strtotime($startWeekDate)));
$dateOfWeek[]= date("d/m", strtotime('thursday this week', strtotime($startWeekDate)));
$dateOfWeek[]= date("d/m", strtotime('friday this week', strtotime($startWeekDate)));
$dateOfWeek[]= date("d/m", strtotime('saturday this week', strtotime($startWeekDate)));
$dateOfWeek[]= date("d/m", strtotime('sunday this week', strtotime($startWeekDate)));


$this->view->dateOfWeek = $dateOfWeek;


$limit            = 20;
$QShift           = new Application_Model_Shift();
$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$QTime            = new Application_Model_Time2();
$note             = $this->getRequest()->getParam('note', null);
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
//$from_date    = $year . '-' . $month . '-01';
//$to_date      = $year . '-' . $month . '-' . $number_day_of_month;
$month_tmp =  ($month < 10) ? '0'. $month : $month;

$QTeam  = new Application_Model_Team();
$staff_title_info = $userStorage->title;
$team_info = $QTeam->find($staff_title_info);
$team_info = $team_info->current();
$group_id = $team_info['access_group'];
$this->view->group_id = $group_id;



$QTime = new Application_Model_Time2();
$list_allowance = $QTime->getAllowance();
$params = array(
    'off' => 1,
    'name' => $name,
    'code' => $code,
    'email' => $email,
    'month' => $month,
    'week' => $week,
    'year' => $year,
    'area' => $area,
    'department' => $department,
    'team' => $team,
    'title' => $title,
);
if(empty($code)){
    $code=null;
}
if(empty($name)){
    $name=null;
}
if(empty($email)){
    $email=null;
}

if(empty($department)){
    $department=null;
}
if(empty($team)){
    $team=null;
}
if(empty($title)){
    $title=null;
}
$user_id = $userStorage->id;
$params_day = array(
    'from' => $params['year'] . '-' . $params['month'] . '-01',
    'to' => $params['year'] . '-' . $params['month']  . '-' .$number_day_of_month,
);
//    $group_id = $userStorage->group_id;



if ($group_id == PGPB_ID){
    $params['staff_id'] = $user_id;
}elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID){
    $params['asm'] = $user_id;
}elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID,TRAINING_LEADER_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID))){
    $params['other'] = $user_id;
}elseif($group_id == LEADER_ID){
    $params['leader'] = $user_id;
}elseif($group_id == SALES_ADMIN_ID){
    $params['sale_admin'] = $user_id;
}

//set quyen cho hong nhung trainer
elseif ($user_id == 7278 || $user_id == 240 || $user_id == 12719){
    $params['other'] = $user_id;
}
//chi van ha noi
elseif ($user_id == 95){
    $params['other'] = $user_id;
}
elseif ($group_id == 22)
    $params['other'] = $user_id;
elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915 || $user_id ==87)
    $params['asm'] = $user_id;
else{
    $params['sale'] = $user_id;
}
$params['dev'] = $dev;

if ($user_id == SUPERADMIN_ID || in_array($group_id, array( ADMINISTRATOR_ID,HR_ID,HR_EXT_ID)))
{


    $from_month = $params_day['from'];
    $to_month = $params_day['to'];

    $db           = Zend_Registry::get('db');
    $stmt = $db->prepare("CALL `PR_get_staff_filter`(:p_from_date, :p_to_date, :p_code, :p_name, :p_email, :p_area, :p_department, :p_team, :p_title)");
    $stmt->bindParam('p_from_date', $from_month, PDO::PARAM_STR);
    $stmt->bindParam('p_to_date', $to_month, PDO::PARAM_STR);
    $stmt->bindParam('p_code', $code, PDO::PARAM_STR);
    $stmt->bindParam('p_name', $name, PDO::PARAM_STR);
    $stmt->bindParam('p_email', $email, PDO::PARAM_STR);
    $stmt->bindParam('p_area', $area, PDO::PARAM_STR);
    $stmt->bindParam('p_department', $department, PDO::PARAM_STR);
    $stmt->bindParam('p_team', $team, PDO::PARAM_STR);
    $stmt->bindParam('p_title', $title, PDO::PARAM_STR);
    $stmt->execute();
    $staffBrandShop = $stmt->fetchAll();
    $stmt->closeCursor();
    $list_of_subordinates = array();
    foreach($staffBrandShop as $key_per => $val_per){
        $list_of_subordinates[] = $val_per['id'];
    }

    if( !empty($export_temp_time_late) || !empty($export_over_time) || !empty($export_last_month) || !empty($export) ){
        $list_of_subordinates = array_slice($list_of_subordinates,null,null);
    }
    else{
        //$list_of_subordinates = array_slice($list_of_subordinates,($page-1)*$limit,$limit);
    }
    $list_of_subordinates = implode(",",$list_of_subordinates);

}

else{

    $from_month = $params_day['from'];
    $to_month = $params_day['to'];

    $db           = Zend_Registry::get('db');
    $stmt = $db->prepare("CALL `PR_get_permission_by_staffcode_filter`(:p_staff_code,:p_from_date,:p_to_date,:p_code,:p_name,:p_email,:p_area,:p_department,:p_team,:p_title)");
    $stmt->bindParam('p_staff_code', $userStorage->code, PDO::PARAM_STR);
    $stmt->bindParam('p_from_date', $from_month, PDO::PARAM_STR);
    $stmt->bindParam('p_to_date', $to_month, PDO::PARAM_STR);
    $stmt->bindParam('p_code', $code, PDO::PARAM_STR);
    $stmt->bindParam('p_name', $name, PDO::PARAM_STR);
    $stmt->bindParam('p_email', $email, PDO::PARAM_STR);
    $stmt->bindParam('p_area', $area, PDO::PARAM_STR);
    $stmt->bindParam('p_department', $department, PDO::PARAM_STR);
    $stmt->bindParam('p_team', $team, PDO::PARAM_STR);
    $stmt->bindParam('p_title', $title, PDO::PARAM_STR);
    $stmt->execute();
    $list_of_subordinates_tmp = $stmt->fetchAll();
    $stmt->closeCursor();
    $list_of_subordinates = array();
    foreach ($list_of_subordinates_tmp as $key=> $value){
        array_push($list_of_subordinates,$value['id']);
    }
    if($userStorage->id == 326){
        $list_of_subordinates = array_diff($list_of_subordinates,array(23154));
    }
    if( !empty($export_temp_time_late) || !empty($export_over_time) || !empty($export_last_month) || !empty($export) ){
        $list_of_subordinates = array_slice($list_of_subordinates,null,null);
    }
    else{
        //$list_of_subordinates = array_slice($list_of_subordinates,($page-1)*$limit,$limit);
    }

    $list_of_subordinates = implode(",",$list_of_subordinates);

}

// echo $list_of_subordinates;die;
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$array_ddtm = array(
    24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
);

$db           = Zend_Registry::get('db');

//get all record to pagination
$sqlGetTotal="select count(distinct (tt.staff_id)) as total from temp_time tt
    inner join staff st on tt.staff_id=st.id
    where 
    tt.add_reason=7 and
    tt.date between '".$startWeekDate."' and '".$endWeekDate."' and find_in_set(st.id,'".$list_of_subordinates."')";
$stmt = $db->prepare($sqlGetTotal);

$stmt->execute();
$totalRecord = $stmt->fetchAll();

$stmt->closeCursor();
//end get all record to pagination

//get data to show in view
$offset=$limit * ($page - 1);

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$sql          = "CALL `PR_show_online_time`(:from_date, :to_date, :p_staff_id, :p_limit, :p_offset)";

$stmt = $db->prepare($sql);

$stmt->bindParam('from_date', $startWeekDate, PDO::PARAM_STR);
$stmt->bindParam('to_date', $endWeekDate, PDO::PARAM_STR);
$stmt->bindParam('p_staff_id', $list_of_subordinates, PDO::PARAM_STR);
$stmt->bindParam('p_limit', $limit, PDO::PARAM_INT);
$stmt->bindParam('p_offset', $offset, PDO::PARAM_INT);

$stmt->execute();
$list_staff_view = $stmt->fetchAll();

$stmt->closeCursor();
//end get data to show in view



$this->view->limit = $limit;
$this->view->total = $totalRecord[0]['total'];
// + $data_new_staff_trainer['total'];
$this->view->number_day_of_month = $number_day_of_month;
$this->view->url = HOST . 'staff-time/show-online-time' . ($params ? '?' . http_build_query($params) .
        '&' : '?');

$this->view->offset = $limit * ($page - 1);

unset($params['list_staff_id']);
unset($params['list_cmnd']);
$this->view->public_holyday = $public_holyday;
$this->view->shift = $QShift->getShiftByTitle($userStorage->title);
$this->view->current_user  = $userStorage;
$QArea = new Application_Model_Area();
$all_area =  $QArea->fetchAll(null, 'name');
$this->view->areas = $all_area;
$this->view->staff_pending = $arr_pending;
//$params['month']=7;
$this->view->params = $params;
$this->view->list_allowance = $list_allowance;
$this->view->array_ddtm = $array_ddtm;
$QCheckIn               = new Application_Model_CheckIn();
$this->view->list_staff_view = $list_staff_view;




if(!empty($export)){
    $from_month = $params_day['from'];

    $to_month = $params_day['to'];

    $sqlGetTotal="select count(distinct (tt.staff_id)) as total from temp_time tt
    inner join staff st on tt.staff_id=st.id
    where 
    tt.add_reason=7 and
    tt.date between '".$from_month."' and '".$to_month."' and find_in_set(st.id,'".$list_of_subordinates."')";
    $stmt = $db->prepare($sqlGetTotal);

    $stmt->execute();
    $result = $stmt->fetchAll();

    $limitExport=$result[0]['total'];
    $offsetExport=0;
    $list_export_staff_id = $list_of_subordinates;
    $db           = Zend_Registry::get('db');
    $sql          = "CALL `PR_show_online_time`(:from_date, :to_date, :p_staff_id, :p_limit, :p_offset)";


    $stmt = $db->prepare($sql);

    $stmt->bindParam('from_date', $from_month, PDO::PARAM_STR);
    $stmt->bindParam('to_date', $to_month, PDO::PARAM_STR);
    $stmt->bindParam('p_staff_id', $list_of_subordinates, PDO::PARAM_STR);
    $stmt->bindParam('p_limit', $limitExport, PDO::PARAM_INT);
    $stmt->bindParam('p_offset', $offsetExport, PDO::PARAM_INT);

    $stmt->execute();
    $data_export = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = $db = null;

    set_time_limit(0);
    ini_set('memory_limit', '5120M');

    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();

    require_once 'PHPExcel.php';

//    $alphaExcel = new My_AlphaExcel();
//    die('dasdasd');

///////////////////////
    //require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'STT',
        'Name',
        'Code',
        'Department',
        'Team',
        'Title'
    );

    for($i = 1; $i <= $number_day_of_month; $i++)
    {
        $heads[] = $i;
    }
    $heads[]='Total';

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;

    try {
        if ($data_export)
            foreach($data_export as $key => $value)
            {
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value['staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value['department'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value['team'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'], PHPExcel_Cell_DataType::TYPE_STRING);

                for($i = 1; $i <= $number_day_of_month; $i++)
                {
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($value[$i], PHPExcel_Cell_DataType::TYPE_STRING);
                }

                $sheet->getCell($alpha++ . $index)->setValueExplicit($value['total'], PHPExcel_Cell_DataType::TYPE_STRING);

                $index++;




            }

    } catch (exception $e) {
        exit;
    }

    $filename  = 'Online-work-report_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
    ////////////////////



}
