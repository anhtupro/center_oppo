<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$month = $this->getRequest()->getParam('month', date('m'));
$year = $this->getRequest()->getParam('year', date('Y'));
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
$from_date = $year . '-' . $month . '-01';
$to_date = $year . '-' . $month . '-' . $number_day_of_month;

$db           = Zend_Registry::get('db');
$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$sql          = "CALL `PR_over_time_warranty`(:from_date, :to_date)";

$stmt = $db->prepare($sql);
$stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
$stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);

$stmt->execute();
$data_export = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = $db = null;

require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Tên nhân viên',
            $alphaExcel->ShowAndUp() => 'Mã nhân viên',
            $alphaExcel->ShowAndUp() => 'Ngày',
            $alphaExcel->ShowAndUp() => 'Giờ vào',
            $alphaExcel->ShowAndUp() => 'Giờ ra',
        );
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        foreach($data_export as $key => $val)
        {
            $alphaExcel = new My_AlphaExcel();
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['staff_name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['staff_code']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($val['check_in_date'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('H:i:s', strtotime($val['check_in_at'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['check_out_at'])?date('H:i:s', strtotime($val['check_out_at'])):'' );

        }

        $filename = 'Staff Time Warranty - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;