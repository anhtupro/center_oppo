<?php

$month = intval($this->getRequest()->getParam('month', date('m')));
$year = intval($this->getRequest()->getParam('year', date('Y')));
if($month<10){
    $month = '0'.$month;
}
$from_date_month = $year . '-' . $month . '-01';
$to_date_month = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);

$this->view->from_date_month = $from_date_month;
$this->view->to_date_month = $to_date_month;

//Check locked time
$QLockTime = new Application_Model_LockTime();
$where_lock = array();
$where_lock[] = $QLockTime->getAdapter()->quoteInto('month = ?', $month);
$where_lock[] = $QLockTime->getAdapter()->quoteInto('year = ? ', $year);
$result_lock = $QLockTime->fetchRow($where_lock);
$this->view->locked_time = $result_lock;
//End Check locked time

$month_present = date('m'); // sau này a Điểm edit lại tháng chốt số vào đây
$year_present = date('Y');
$additional_last_month = 0;

if (($year <= $year_present)) {
    $additional_last_month = 1;
}

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$create_leave = $this->getRequest()->getParam('create_leave');

$flashMessenger = $this->_helper->flashMessenger;
$QLeaveDetail = new Application_Model_LeaveDetail ();
$QTime2 = new Application_Model_Time2 ();
$QTimeLastMonth = new Application_Model_TimeLastMonth();
$QTimeGps = new Application_Model_TimeGps();
$QTempTime = new Application_Model_TempTime();
$QTimeLate = new Application_Model_TimeLate();
$QTeam = new Application_Model_Team ();
$QTempTimeLog = new Application_Model_TempTimeLog ();
$QRegionalMarket = new Application_Model_RegionalMarket();
$QStaffWorkFromHome = new Application_Model_StaffWorkFromHome();

$staff_id = $userStorage->id;
$office_id = $userStorage->office_id;

$team = $userStorage->team;

$titleRowSet = $QTeam->find($userStorage->title);
$policy_group = $titleRowSet->current()['policy_group'];
$title_name = $titleRowSet->current()['name'];
$diligent = $titleRowSet->current()['diligent'];
$this->view->policy_group = $policy_group;
$this->view->title_name = $title_name;


$datetime = date('Y-m-d H:i:s');

$late = $this->getRequest()->getParam('update-late', 0);
$date_late = $this->getRequest()->getParam('date_late');
$late_from = $this->getRequest()->getParam('late_from');
$late_to = $this->getRequest()->getParam('late_to');
$reason_late = $this->getRequest()->getParam('reason_late');
$reason_late = trim($reason_late);

$add_reason_late = $this->getRequest()->getParam('add_reason_late');

$db = Zend_Registry::get('db');
if (!empty($late) AND!empty($late_from) AND!empty($late_to)) {
    try {
        if (empty($reason_late) || empty($add_reason_late)) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('ERROR: Nhập thông tin chưa đầy đủ..!');
            $this->redirect(HOST . "staff-time/staff-view");
        }
        $db = Zend_Registry::get('db');

//        $where_time_late = array();
//        $where_time_late [] = $QTempTime->getAdapter()->quoteInto('staff_id = ?', $staff_id);
//        $where_time_late [] = $QTempTime->getAdapter()->quoteInto('date = ?', $date_late);
//        $result_late = $QTimeLate->fetchRow($where_time_late);
//        $date_time_late = array(
//            'staff_id' => $staff_id,
//            'date' => $date_late,
//            'reason' => $reason_late,
//            'status' =>  ($team == 131 AND $office_id != 49) ? 1 : 0,
//            'add_reason_late' => $add_reason_late,
//            'created_at' => $datetime
//        );
//
//        if (empty($result_late)) {
//
//            $QTimeLate->insert($date_time_late);
//        } else {
//
//            $QTimeLate->update($date_time_late, $where_time_late);
//        }
//        $db->commit();
        $this->redirect(HOST . "staff-time/staff-view");
    } catch (exception $e) {
//        $db->rollback();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $e->getMessage();
        $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        return;
    }
}

$p_year = (in_array($month, array(1, 2)) AND $year == date('Y')) ? $year - 1 : $year;
//Lich su
$db = Zend_Registry::get('db');
$stmt_historyLeave = $db->prepare('CALL PR_year_leave_history (:staff_id, :year, :month) ');
$stmt_historyLeave->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
$stmt_historyLeave->bindParam('year', $p_year, PDO::PARAM_INT);
$stmt_historyLeave->bindParam('month', $month, PDO::PARAM_INT);
$stmt_historyLeave->execute();
$historyLeave = $stmt_historyLeave->fetchAll();
$stmt_historyLeave->closeCursor();
$leaveByMonth = 0;
if (!empty($historyLeave)) {
    foreach ($historyLeave as $val) {
        if ($val['month'] == $month && $val['year'] == $year) {
            $leaveByMonth = $val['number'];
            break;
        }
    }
}

$this->view->leave_by_month = $leaveByMonth;
if (!empty($this->getRequest()->getParam('update-time-office'))) {
    $date = $this->getRequest()->getParam('date');
    $office_time = $this->getRequest()->getParam('office_time');
    $reason = $this->getRequest()->getParam('reason_gps');
    $add_reason = $this->getRequest()->getParam('add_reason');

    try {
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        $reason = trim($reason);
        if (empty($reason) || empty($add_reason)) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('ERROR: Nhập thông tin chưa đầy đủ..!');
            $this->redirect(HOST . "staff-time/staff-view");
        }

        $data_temp_time = array(
            'staff_id' => $staff_id,
            'date' => $date,
            'shift' => ($office_time == 1) ? 6 : 9,
            'office_time' => $office_time,
            'reason' => $reason,
            'add_reason' => $add_reason,
            'status' => (in_array($team, array(131, 611)) AND $office_id != 49) ? 1 : 0,
            'created_at' => $datetime,
        );


        $where_temp_time = array();
        $where_temp_time [] = $QTempTime->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_temp_time [] = $QTempTime->getAdapter()->quoteInto('date = ?', $date);
        $result_date = $QTempTime->fetchRow($where_temp_time);

        $data_temp_time_log = array(
            'staff_id' => $data_temp_time['staff_id'],
            'date' => $data_temp_time['date'],
            'office_time' => $data_temp_time['office_time'],
            'reason' => $data_temp_time['reason'],
            'add_reason' => $data_temp_time['add_reason'],
            'created_at' => $data_temp_time['created_at'],
            'created_by' => $userStorage->id
        );
        if (empty($result_date)) {
            //Insert tbl time
            $data_time = array(
                'staff_id' => $staff_id,
                'created_at' => $datetime,
                'status' => (in_array($team, array(131, 611)) AND $office_id != 49) ? 1 : 0,
                'shift' => ($office_time == 1) ? 6 : 9
            );

            $QTime2->insert($data_time);

            $QTempTimeLog->insert($data_temp_time_log);
            //Insert tbl temp_time
            $QTempTime->insert($data_temp_time);
        } else {

            $QTempTime->update($data_temp_time, $where_temp_time);
            $QTempTimeLog->insert($data_temp_time_log);
        }
        $db->commit();
    } catch (exception $e) {
        $db->rollback();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $e->getMessage();
        $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        return;
    }
}

if (!empty($this->getRequest()->getParam('update-time-none-office'))) {

    $date = $this->getRequest()->getParam('date');
    $shift_pick = $this->getRequest()->getParam('shift');
    $reason = $this->getRequest()->getParam('reason_gps');
    $add_reason = $this->getRequest()->getParam('add_reason');
    $QTime = new Application_Model_Time ();
    $QTimeImageLocation = new Application_Model_TimeImageLocation ();
    $QStaff = new Application_Model_Staff ();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $reason = trim($reason);
    $db = Zend_Registry::get('db');
    try {

        if (empty($staff_id) || empty($reason)) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('ERROR: Nhập thông tin chưa đầy đủ..!');
            $this->redirect(HOST . "staff-time/staff-view?month=$month&year=$year");
        }


        $db->beginTransaction();

        $where_temp_time = array();
        $where_temp_time [] = $QTempTime->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_temp_time [] = $QTempTime->getAdapter()->quoteInto('date = ?', $date);
        $result_date = $QTempTime->fetchRow($where_temp_time);

        $data_temp_time = array(
            'staff_id' => $staff_id,
            'date' => $date,
            'shift' => $shift_pick,
            'office_time' => ($shift_pick == 9) ? 0.5 : 1,
            'reason' => $reason,
            'add_reason' => $add_reason,
            'status' => (in_array($team, array(131, 611)) AND $office_id != 49 ) ? 1 : 0,
            'created_at' => $datetime,
            'approved_at' => (in_array($team, array(131, 611)) AND $office_id != 49) ? $datetime : NULL
        );
        $data_temp_time_log = array(
            'staff_id' => $data_temp_time['staff_id'],
            'date' => $data_temp_time['date'],
            'office_time' => $data_temp_time['office_time'],
            'reason' => $data_temp_time['reason'],
            'add_reason' => $data_temp_time['add_reason'],
            'created_at' => $data_temp_time['created_at'],
            'created_by' => $userStorage->id
        );

        if (empty($result_date)) {
            //Insert tbl time
            $data_time = array(
                'staff_id' => $staff_id,
                'created_at' => $datetime,
                'status' => 0,
                'shift' => $shift_pick
            );

            $QTime2->insert($data_time);

            //Insert tbl temp_time
            $QTempTimeLog->insert($data_temp_time_log);
            $QTempTime->insert($data_temp_time);
        } else {
            $QTempTimeLog->insert($data_temp_time_log);
            $QTempTime->update($data_temp_time, $where_temp_time);
        }

        // 	$QTime2->insertTempTime ( $params_update );
        $db->commit();
    } catch (exception $e) {
        $db->rollback();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $e->getMessage();
        $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        return;
    }
}



/// bổ sung công tháng trước cho khối văn phòng
if (!empty($this->getRequest()->getParam('update-time-office-last-month'))) {
//echo '111';
//    exit();
    $date = $this->getRequest()->getParam('date_last');
    $office_time = $this->getRequest()->getParam('office_time_last');
    $reason = $this->getRequest()->getParam('reason_last');
    $month = $this->getRequest()->getParam('month', date('m'));
    $year = $this->getRequest()->getParam('year', date('Y'));

    try {

        if (empty($staff_id) || empty($reason) || empty($office_time)) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('ERROR: Nhập thông tin chưa đầy đủ..!');
            $this->redirect(HOST . "staff-time/staff-view?month=$month&year=$year");
        }

        //////// CALL PROCEDURE Tính tổng công trong một ngày
        $dbp = Zend_Registry::get('db');
        $stmt_tt = $dbp->prepare("CALL `get_total_time_staff_by_day`(:p_staff_id, :p_work_day)");
        $stmt_tt->bindParam('p_staff_id', My_Util::escape_string($staff_id), PDO::PARAM_STR);
        $stmt_tt->bindParam('p_work_day', My_Util::escape_string($date), PDO::PARAM_STR);
        $stmt_tt->execute();
        $total_by_date = $stmt_tt->fetchAll();
        $stmt_tt->closeCursor();
        $stmt_tt = $dbp = null;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        if ($total_by_date[0]['total_time'] == 1) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage("ERROR: Tổng công phép của ngày " . $date . " đã đủ 1 công..!! ");
            $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        } elseif ($total_by_date[0]['total_time'] == 0.5 && $office_time == 1) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage("ERROR: Tổng công phép của ngày " . $date . " đã có 0.5 công. Chỉ được bổ sung thêm 0.5 công.!! ");
            $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        } else {

            $QTimeLastMonth = new Application_Model_TimeLastMonth();
            $where_TLM = array();
            $where_TLM[] = $QTimeLastMonth->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where_TLM[] = $QTimeLastMonth->getAdapter()->quoteInto('date = ?', $date);
            $QTimeLastMonth_check = $QTimeLastMonth->fetchRow($where_TLM);
            
            $month_required = $QLockTime->getLockTimeLastest();
            if($month_required['month']<10){
                $month_required['month'] = '0'.$month_required['month'];
            } 
            $date_lock_last_str = $month_required['year'].'-'.$month_required['month'].'-'.'01';
            $date_required = new DateTime($date_lock_last_str);
            $date_required->add(new DateInterval('P1M'));
            
            if (empty($QTimeLastMonth_check)) {// insert
                $params_ins = array(
                    'staff_id' => $staff_id,
                    'date' => $date,
                    'shift' => ($office_time == 1) ? 6 : 9,
                    'office_time' => $office_time,
                    'status' => (in_array($team, array(131, 611)) AND $office_id != 49) ? 1 : 0,
                    'reason' => $reason,
                    'created_at' => date('Y-m-d H:i:s'),
                    'approved_at' => (in_array($team, array(131, 611)) AND $office_id != 49) ? date('Y-m-d H:i:s') : NULL,
                    'month_required' => $date_required->format('Y-m-d')
                );

                $QTimeLastMonth->insert($params_ins);
            } else {// update
                if ($QTimeLastMonth_check['status'] == 1) {
                    $flashMessenger = $this->_helper->flashMessenger;
                    $flashMessenger->setNamespace('error')->addMessage("ERROR: Ngày " . $date . " đã được bổ sung công cũ và xác nhận.!! ");
                    $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
                }

                $params_ins = array(
                    'shift' => ($office_time == 1) ? 6 : 9,
                    'office_time' => $office_time,
                    'status' => (in_array($team, array(131, 611)) AND $office_id != 49) ? 1 : 0,
                    'reason' => $reason,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'month_required' => $date_required->format('Y-m-d')
                );

                $QTimeLastMonth->update($params_ins, $where_TLM);
            }
        } //end else    

        $db->commit();
    } catch (exception $e) {
        $db->rollback();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $e->getMessage();
        $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        return;
    }
}

// bổ sung công tháng trước cho nhân viên ngoài văn phòng
if (!empty($this->getRequest()->getParam('update-time-none-office-last-month'))) {

    $date = $this->getRequest()->getParam('date_last');
    $shift_pick = $this->getRequest()->getParam('shift_last');
    $reason = $this->getRequest()->getParam('reason_last');
    //////////////////////////////////

    $month = $this->getRequest()->getParam('month', date('m'));
    $year = $this->getRequest()->getParam('year', date('Y'));

    try {

        if (empty($staff_id) || empty($reason) || empty($shift_pick)) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('ERROR: Nhập thông tin chưa đầy đủ..!');
            $this->redirect(HOST . 'staff-time/staff-view?month=5&year=2017');
        }

        //////// CALL PROCEDURE Tính tổng công trong một ngày
        $dbp = Zend_Registry::get('db');
        $stmt_tt = $dbp->prepare("CALL `get_total_time_staff_by_day`(:p_staff_id, :p_work_day)");
        $stmt_tt->bindParam('p_staff_id', My_Util::escape_string($staff_id), PDO::PARAM_STR);
        $stmt_tt->bindParam('p_work_day', My_Util::escape_string($date), PDO::PARAM_STR);
        $stmt_tt->execute();
        $total_by_date = $stmt_tt->fetchAll();
        $stmt_tt->closeCursor();
        $stmt_tt = $dbp = null;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        if ($total_by_date[0]['total_time'] == 1) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage("ERROR: Tổng công phép của ngày " . $date . " đã đủ 1 công..!! ");
            $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        } elseif ($total_by_date[0]['total_time'] == 0.5 && $shift_pick != 9) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage("ERROR: Tổng công phép của ngày " . $date . " đã có 0.5 công. Chỉ được bổ sung thêm 0.5 công.!! ");
            $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        } else {

            $where_TLM = array();
            $where_TLM[] = $QTimeLastMonth->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where_TLM[] = $QTimeLastMonth->getAdapter()->quoteInto('date = ?', $date);
            $QTimeLastMonth_check = $QTimeLastMonth->fetchRow($where_TLM);

            $month_required = $QLockTime->getLockTimeLastest();
            if($month_required['month']<10){
                $month_required['month'] = '0'.$month_required['month'];
            } 
            $date_lock_last_str = $month_required['year'].'-'.$month_required['month'].'-'.'01';
            $date_required = new DateTime($date_lock_last_str);
            $date_required->add(new DateInterval('P1M'));
            
            if (empty($QTimeLastMonth_check)) {// insert
                $params_ins = array(
                    'staff_id' => $staff_id,
                    'date' => $date,
                    'shift' => $shift_pick,
                    'office_time' => NULL,
                    'status' => (in_array($team, array(131, 611)) AND $office_id != 49 ) ? 1 : 0,
                    'reason' => $reason,
                    'created_at' => date('Y-m-d H:i:s'),
                    'approved_at' => (in_array($team, array(131, 611)) AND $office_id != 49) ? date('Y-m-d H:i:s') : NULL,
                    'month_required' => $date_required->format('Y-m-d')
                );

                $QTimeLastMonth->insert($params_ins);
            } else {// update
                if ($QTimeLastMonth_check['status'] == 1) {
                    $flashMessenger = $this->_helper->flashMessenger;
                    $flashMessenger->setNamespace('error')->addMessage("ERROR: Ngày " . $date . " đã được bổ sung công cũ và xác nhận.!! ");
                    $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
                }

                $params_ins = array(
                    'shift' => $shift_pick,
                    'office_time' => NULL,
                    'status' => (in_array($team, array(131, 611)) AND $office_id != 49) ? 1 : 0,
                    'reason' => $reason,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'month_required' => $date_required->format('Y-m-d')
                );
                $QTimeLastMonth->update($params_ins, $where_TLM);
            }
        } //end else     

        $db->commit();
    } catch (exception $e) {
        $db->rollback();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $e->getMessage();
        $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        return;
    }
}

// Tính tổng số ngày bổ sung công tháng trước  
$p_month_required = $year . "-" . $month . "-01";
$db = Zend_Registry::get('db');
$stmt_t = $db->prepare("CALL `total_work_day_last_month`(:p_month_required, :p_staff_id)");
$stmt_t->bindParam('p_month_required', My_Util::escape_string($p_month_required), PDO::PARAM_STR);
$stmt_t->bindParam('p_staff_id', My_Util::escape_string($staff_id), PDO::PARAM_STR);
$stmt_t->execute();
$data_day_last_month = $stmt_t->fetchAll();
$stmt_t->closeCursor();
$stmt_t = $db = null;

$total_day_last_month = 0;
foreach ($data_day_last_month as $kt => $vla) {
    if (!empty($vla['shift']) && in_array($vla['shift'], array(1, 2, 3, 6))) {
        $total_day_last_month = $total_day_last_month + 1;
    } elseif (!empty($vla['shift']) && $vla['shift'] == 9) {
        $total_day_last_month = $total_day_last_month + 0.5;
    }
}

// Tính tổng số ngày bổ sung công tháng trước đã được appr 
$db = Zend_Registry::get('db');
$stmt_approve = $db->prepare("CALL `total_work_day_last_month_approve`(:p_month_required, :p_staff_id)");
$stmt_approve->bindParam('p_month_required', My_Util::escape_string($p_month_required), PDO::PARAM_STR);
$stmt_approve->bindParam('p_staff_id', My_Util::escape_string($staff_id), PDO::PARAM_STR);
$stmt_approve->execute();
$data_day_last_month_approve = $stmt_approve->fetchAll();
$stmt_approve->closeCursor();
$stmt_approve = $db = null;

$total_day_last_month_approve = 0;
foreach ($data_day_last_month_approve as $kt2 => $vla2) {
    if (!empty($vla2['shift']) && in_array($vla2['shift'], array(1, 2, 3, 6))) {
        $total_day_last_month_approve = $total_day_last_month_approve + 1;
    } elseif (!empty($vla2['shift']) && $vla2['shift'] == 9) {
        $total_day_last_month_approve = $total_day_last_month_approve + 0.5;
    }
}

$this->view->total_day_last_month = $total_day_last_month;
$this->view->total_day_last_month_approve = $total_day_last_month_approve;

if (!empty($create_leave)) {
    
    $is_leave_half = $this->getRequest()->getParam("is-leave-half");
    $from = $this->converDate($this->getRequest()->getParam("from"));
    $to = $this->converDate($this->getRequest()->getParam("to"));
    $date_leave = $this->converDate($this->getRequest()->getParam("date-leave-half"));
    $leave_type = $this->getRequest()->getParam("leave-type");
    $reason_leave = $this->getRequest()->getParam("reason");
    $due_date = $this->converDate($this->getRequest()->getParam('due_date'));
    $auth = Zend_Auth::getInstance()->getStorage()->read();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $db = Zend_Registry::get('db');
    $is_office = $userStorage->is_officer;
    $staff_id = $userStorage->id;

    if ($leave_type == 17) {
        if(empty($due_date)){
            $flashMessenger->setNamespace('error')->addMessage("Vui lòng nhập ngày dự sinh .");
            $this->_redirect("/leave/create");
        }
    }
    if ($leave_type == 2) {
//		if($leave_type == 2 && ($date >= '2019-03-01' || $from >= '2019-03-01')){
//			$flashMessenger->setNamespace('error')->addMessage("Hệ thống chặn đăng ký phép tháng 3 để tính hoàn phép. Thời gian mở lại 10/03/2019");
//			$this->_redirect("/leave/create");
//		}
        //Check locked time
        $where_lock = array();
        if (!empty($from)) {
            if ($from < $userStorage->joined_at) {
                $flashMessenger->setNamespace('error')->addMessage("Ngày phép trước ngày bắt đầu làm việc.");
                $this->_redirect("/leave/create");
            }
            $date_check = date_create($from);
            $month_locked = date_format($date_check, "m");
            $year_locked = date_format($date_check, "Y");
        } elseif (!empty($date_leave)) {

            $date_check = date_create($date_leave);
            $month_locked = date_format($date_check, "m");
            $year_locked = date_format($date_check, "Y");
        }

        $where_lock[] = $QLockTime->getAdapter()->quoteInto('month = ?', $month_locked);
        $where_lock[] = $QLockTime->getAdapter()->quoteInto('year = ? ', $year_locked);
        $result_lock = $QLockTime->fetchRow($where_lock);

        if (!empty($result_lock)) {
            $flashMessenger->setNamespace('error')->addMessage("Tháng này đã lock bảng công.");
            $this->_redirect("/leave/create");
        }
        //End Check locked time
        if(empty($date_check)){
            echo json_encode(array('status' => 0, 'message' => "Check ngày đăng kí phép."));
            exit();
        }
        $date_check_contract = date_format($date_check, "Y-m-d");
        $sql_contract = "SELECT sc.* FROM staff_contract sc
                    INNER JOIN (
                                SELECT MIN( sc.id ) id, sc.staff_id, COUNT( sc.id ) total
                                FROM staff_contract sc
                                WHERE sc.contract_term
                                IN ( 2, 3, 17 ) 
                                AND sc.`is_expired` = 0
                                AND sc.`is_disable` = 0
                                AND sc.staff_id = :p_staff_id
                                GROUP BY sc.staff_id
                    )c ON c.id = sc.id and sc.from_date <= :p_from AND sc.to_date >= :p_from AND sc.`is_expired` = 0 AND sc.`is_disable` = 0";
        $stmt_contract = $db->prepare($sql_contract);
        $stmt_contract->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);
        $stmt_contract->bindParam('p_from', My_Util::escape_string($date_check_contract), PDO::PARAM_STR);
        $stmt_contract->execute();
        $check_data = $stmt_contract->fetchAll();

        if (!empty($check_data)) {
            $flashMessenger->setNamespace('error')->addMessage("Thời gian đăng ký phép năm là thời gian thử việc, bạn vui lòng chọn lại thời gian nghỉ phép năm từ thời điểm ký hợp đồng chính thức trở đi");
            $this->_redirect("/leave/create");
        }
    }

    $stmt = $db->prepare('CALL PR_check_max_day_per_time (:staff_id, :leave_type, :from_date, :to_date) ');
    $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
    $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
    $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
    $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
    $stmt->execute();
    $pass_per_time = $stmt->fetchAll();
    $stmt->closeCursor();

    if ($pass_per_time[0]['pass_per_time'] == 1) {
        $flashMessenger->setNamespace('error')->addMessage("Số ngày nghỉ vượt quá giới hạn của 1 lần nghỉ.");
        $this->_redirect("/leave/create");
    }
    // check ca nam

    if ($date_leave) {
        $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
        $stmt->bindParam('to_date', My_Util::escape_string($date_leave), PDO::PARAM_STR);
        $stmt->bindParam('from_date', My_Util::escape_string($date_leave), PDO::PARAM_STR);
    } else {
        $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
        $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
        $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
    }

    $leave_half_check = ($is_leave_half == 0.5) ? 1 : 0;
    $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
    $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
    $stmt->bindParam('is_leave_half', $leave_half_check, PDO::PARAM_INT);


    $stmt->execute();
    $pass_per_year = $stmt->fetchAll();

    $stmt->closeCursor();
    if ($leave_type == 2) {
        if ($pass_per_year[0]['pass_per_year'] == 1) {
        $flashMessenger->setNamespace('error')->addMessage("Số ngày nghỉ vượt quá giới hạn của 1 năm.(Tối đa ".$pass_per_year[0]['annual']." ngày)");
        $this->_redirect("/leave/create");
        }
    }


    define('MAX_SIZE_UPLOAD', 3145728);

    $name1 = "";
    if (!empty($_FILES['image']['name'])) {
        $ext_arr = array('jpg', 'png', 'JPG', 'PNG', 'jpeg', 'JPEG');

        $ext1 = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

        if (($_FILES['image']['size'] > MAX_SIZE_UPLOAD)) {
            $flashMessenger->setNamespace('error')->addMessage("Kích thước vượt quá 3Mb");
            $this->_redirect("/leave/create");
            return;
        }
        if (!in_array($ext1, $ext_arr)) {
            $flashMessenger->setNamespace('error')->addMessage("Hình sai định dạng png, jpg, jpeg");
            $this->_redirect("/leave/create");
            return;
        }
        $name1 = "leave_" . $userStorage->id . "_" . time() . ".png";
        $file1 = APPLICATION_PATH . "/../public/photo/leave/" . $name1;
        if (!empty($_FILES['image']['name'])) {
            $success1 = @move_uploaded_file($_FILES['image']['tmp_name'], $file1);
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            if ($success1) {
                $s3_lib->uploadS3($file1, "photo/leave/", $name1);
            }
        }
    }

    if ($is_leave_half == 1) {

        if (strtotime($from) > strtotime($to)) {
            $flashMessenger->setNamespace('error')->addMessage("Ngày bắt đầu nghỉ phép không được lơn hơn ngày kết thúc");
            $this->_redirect("/leave/create");
        } else {
            $params = array(
                'from' => $from,
                'to' => $to,
                'leave_type' => $leave_type,
                'is_officer' => $is_office,
                'staff_id' => $staff_id,
                'reason' => $reason_leave,
            );
            if (!empty($name1)) {
                $params['image'] = $name1;
            }
            if (!empty($due_date)) {
                $params['due_date'] = $due_date;
            }
            $data = $QLeaveDetail->insertLeave2($params);
        }
    } else {

        $params = array(
            'date' => $date_leave,
            'leave_type' => $leave_type,
            'is_officer' => $is_office,
            'staff_id' => $staff_id,
            'reason' => $reason_leave,
        );
        if (!empty($name1)) {
            $params['image'] = $name1;
        }
        if (!empty($due_date)) {
            $params['due_date'] = $due_date;
        }
        $data = $QLeaveDetail->insertLeaveHalf2($params);
    }
    if ($data ['status'] == 1) {
        $flashMessenger->setNamespace('success')->addMessage('Thêm phép thành công.');
        $this->_redirect("/leave/list-my-leave");
    } else {
        $flashMessenger->setNamespace('error')->addMessage($data ['message']);
        $this->_redirect("/leave/create");
    }
}

$QLeavInfo = new Application_Model_LeaveInfo ();

$this->view->checkHalfLeave = $QLeaveDetail->checkHalfLeave();
$this->view->stock = $QLeavInfo->getStockById(Zend_Auth::getInstance()->getStorage()->read()->id);
$QLeaveType = new Application_Model_LeaveType ();
$this->view->parent_type = $QLeaveType->getParent();

// end leave

$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
$from_date = $year . "-" . $month . "-01";
$to_date = $year . "-" . $month . "-" . $number_day_of_month;

$db = Zend_Registry::get('db');
$title_id = $userStorage->title;
$sql_le = "SELECT 
				ada.*,
				SUM(IF(ada.public > 0 OR t.id IS NOT NULL, 1, 0)) as `special`
			FROM `all_date_advanced` ada
			LEFT JOIN `team` AS `t` ON ada.type_id = t.policy_group AND t.id = :title
			WHERE 
				ada.category_date <> 2
				AND (ada.date BETWEEN :from_date AND :to_date )
				AND t.id = :title
			GROUP BY ada.date
			HAVING special > 0
	";

$stmt = $db->prepare($sql_le);
$stmt->bindParam('title', $userStorage->title, PDO::PARAM_INT);
$stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
$stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
$stmt->execute();

$list_le = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = $db = null;

$db = Zend_Registry::get('db');
$stmt_late = $db->prepare("CALL `PR_get_late_by_staff`(:p_from_date, :p_to_date, :p_staff_id)");
$stmt_late->bindParam('p_from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
$stmt_late->bindParam('p_to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
$stmt_late->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);

$stmt_late->execute();
$total_late = $stmt_late->fetchAll();
$stmt_late->closeCursor();
$stmt_late = $db = null;

$this->view->staff_late = $total_late[0];
$check_late = 1;

if (in_array($userStorage->deparment, array(321, 160)) || $diligent == 0) {
    $check_late = 0;
}
//echo $check_late;
$this->view->check_late = $check_late;
$array_le = array();

foreach ($list_le as $key => $value) {
    $array_le[] = intval(date('d', strtotime($value['date'])));
}

$this->view->array_le = $array_le;

$params = array(
    'month' => $month,
    'year' => $year,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'staff_id' => $userStorage->id
);

$db = Zend_Registry::get('db');
$sql_check_machine = "SELECT id FROM `check_in_machine_map` WHERE staff_code = :staff_code AND status = 1";
$stmt = $db->prepare($sql_check_machine);
$stmt->bindParam('staff_code', My_Util::escape_string($userStorage->code), PDO::PARAM_STR);
$stmt->execute();
$data_machine_check = $stmt->fetch();
$stmt->closeCursor();

$data = $QTime2->getInfoStaffView($params);
//$this->_helper->viewRenderer->setRender('staff-view-clone');
//start Lay so ngay nhan vien dang ki di lam online vi dich
$db = Zend_Registry::get('db');
$sql_get_online_date = "SELECT id FROM `temp_time` WHERE staff_id = :staff_id AND add_reason = 7
    and MONTH(date) = MONTH(CURRENT_DATE())
AND YEAR(date) = YEAR(CURRENT_DATE())
";

$stmt = $db->prepare($sql_get_online_date);
$stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);

$stmt->execute();
$totalOnlineDate = count($stmt->fetchAll());
$stmt->closeCursor();




//$totalOnlineDate      = count($db->fetchAll($selectOnlineDate));
$this->view->totalOnlineDate = $totalOnlineDate;

//end Lay so ngay nhan vien dang ki di lam online vi dich
$this->view->auth = $userStorage;
$this->view->area_user = $QRegionalMarket->getAreaByProvince($userStorage->regional_market);
$this->view->params = $params;
$this->view->data = $data;
//get staff extend work-from-home
$staff_wfh = $QStaffWorkFromHome->checkStaffWFH($userStorage->id);
if(!empty($staff_wfh)){
    $this->view->staff_wfh = $staff_wfh;   
}

$QArea = new Application_Model_Area ();
$all_area = $QArea->get_cache();

$this->view->data_machine_check = $data_machine_check;

$this->view->areas = $all_area;

$QTeam = new Application_Model_Team ();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$this->view->additional_last_month = $additional_last_month;

// tinh ngay phat
$QCheckIn = new Application_Model_CheckIn();
$params = array(
    'from_date' => $from_date,
    'to_date' => $to_date,
    'month' => $month,
    'year' => $year,
    'department' => $userStorage->department,
    'team' => $userStorage->team,
    'title' => $userStorage->title,
    'code' => $userStorage->code,
    'name' => null,
    'area' => null
);
$data_late = $QCheckIn->listLate($params);
$this->view->total_late = $data_late[0]['total_late'];

$this->view->forget_check_in = $data_late[0]['forget_check_in'];
$this->view->ngay_phat = $data_late[0]['ngay_phat'];
// thêm ở action hiển thị messages

$QReasonTempTime = new Application_Model_ReasonTempTime();
$reason_temp_time = $QReasonTempTime->fetchAll()->toArray();
$this->view->reason_temp_time = $reason_temp_time;
$QReasonTimeLate = new Application_Model_ReasonTimeLate();
$reason_temp_late = $QReasonTimeLate->fetchAll()->toArray();
$this->view->reason_temp_late = $reason_temp_late;

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
