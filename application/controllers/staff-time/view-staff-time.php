<?php 
$this->_helper->layout->disableLayout();
$not_approve_self = array(LEADER_ID,SALES_ID, PB_SALES_ID, PGPB_ID);
$month            = intval($this->getRequest()->getParam('month', date('m')));
$year             = intval($this->getRequest()->getParam('year', date('Y')));
$staff_id         = intval($this->getRequest()->getParam('staff_id'));
$QTime2           = new  Application_Model_Time2();
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$this->view->current_user  = $userStorage;
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
$from_date        = $year . '-' . $month . '-01';
$to_date          = $year . '-' . $month . '-' . $number_day_of_month;


$from_date_month = $year . '-' . $month . '-01';
$to_date_month = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);

$this->view->from_date_month = $from_date_month;
$this->view->to_date_month = $to_date_month;

//Check locked time
$QLockTime = new Application_Model_LockTime();
$where_lock = array();
$where_lock[] = $QLockTime->getAdapter()->quoteInto('month = ?', $month);
$where_lock[] = $QLockTime->getAdapter()->quoteInto('year = ? ', $year);
$result_lock = $QLockTime->fetchRow($where_lock);
$this->view->locked_time = $result_lock;
//End Check locked time

$params = array (
    'month' => $month,
    'year' => $year,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'staff_id' => $staff_id
);
$p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;
//Lich su
$db = Zend_Registry::get('db');
$stmt_historyLeave = $db->prepare('CALL PR_year_leave_history (:staff_id, :year, :month) ');
$stmt_historyLeave->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
$stmt_historyLeave->bindParam('year',$p_year, PDO::PARAM_INT);
$stmt_historyLeave->bindParam('month', $month, PDO::PARAM_INT);
$stmt_historyLeave->execute();
$historyLeave = $stmt_historyLeave->fetchAll();
$stmt_historyLeave->closeCursor();
$leaveByMonth=0;
if(!empty($historyLeave)){
    foreach($historyLeave as $val){
        if($val['month']==$month && $val['year']== $year){
            $leaveByMonth =  $val['number'];
            break;
        }
    }
}


$this->view->leave_by_month = $leaveByMonth;
$db = Zend_Registry::get('db');
        $stmt_late = $db->prepare("CALL `PR_get_late_by_staff`(:p_from_date, :p_to_date, :p_staff_id)");
        $stmt_late->bindParam('p_from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
        $stmt_late->bindParam('p_to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
        $stmt_late->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);
        
        $stmt_late->execute();
        $total_late = $stmt_late->fetchAll();
        $stmt_late->closeCursor();
        $stmt_late = $db = null;
       
        $this->view->staff_late = $total_late[0];
        

$QStaff      = new Application_Model_Staff();
$staffRowSet = $QStaff->find($staff_id);
$staff       = $staffRowSet->current();
$db = Zend_Registry::get('db');
$sql_check_machine= "SELECT id FROM `check_in_machine_map` WHERE staff_code = :staff_code AND status = 1";
$stmt = $db->prepare($sql_check_machine);
$stmt->bindParam('staff_code', My_Util::escape_string($staff['code']), PDO::PARAM_STR);
$stmt->execute();
$data_machine_check = $stmt->fetch();
$stmt->closeCursor();
$db = Zend_Registry::get('db');
$title_id = $staff['title'];
$QTeam = new Application_Model_Team();
    
    $team = $QTeam->find($title_id);
    $team = $team->current();
    $group_id = $team['access_group'];
    $this->view->group_id = $group_id;
$sql_le = "SELECT
				ada.*,
				SUM(IF(ada.public > 0 OR t.id IS NOT NULL, 1, 0)) as `special`
			FROM `all_date_advanced` ada
			LEFT JOIN `team` AS `t` ON ada.type_id = t.policy_group AND t.id = :title
            WHERE
				ada.category_date <> 2
				AND (ada.date BETWEEN :from_date AND :to_date )
                AND t.id = :title
			GROUP BY ada.date
			HAVING special > 0
	";
	
$stmt = $db->prepare($sql_le);
$stmt->bindParam('title', $staff['title'], PDO::PARAM_INT);
$stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
$stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
$stmt->execute();

$list_le = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = $db = null;
$this->view->title_staff = $staff['title'];


$array_le = array();

foreach($list_le as $key => $value)
{
    $array_le[] = intval(date('d', strtotime($value['date'])));
}

$db = Zend_Registry::get('db');
$stmt_late = $db->prepare("CALL `PR_get_late_by_staff`(:p_from_date, :p_to_date, :p_staff_id)");
$stmt_late->bindParam('p_from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
$stmt_late->bindParam('p_to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
$stmt_late->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);

$stmt_late->execute();
$total_late = $stmt_late->fetchAll();
$stmt_late->closeCursor();
$stmt_late = $db = null;

$this->view->staff_late = $total_late[0];
$check_late = 1;

if(in_array($userStorage->deparment, array(321,160)) || $diligent == 0){
    $check_late = 0;
}
//echo $check_late;
$this->view->check_late = $check_late;
$this->view->array_le = $array_le;
$this->view->data_machine_check = $data_machine_check;

// Tính tổng số ngày bổ sung công tháng trước
$p_month_required = $year . "-" . $month . "-01";
$db   = Zend_Registry::get('db');
$stmt_t   = $db->prepare("CALL `total_work_day_last_month`(:p_month_required, :p_staff_id)");
$stmt_t->bindParam('p_month_required', My_Util::escape_string($p_month_required) , PDO::PARAM_STR);
$stmt_t->bindParam('p_staff_id',My_Util::escape_string($staff_id), PDO::PARAM_STR);
$stmt_t->execute();
$data_day_last_month = $stmt_t->fetchAll();
$stmt_t->closeCursor();
$stmt_t = $db = null;

$total_day_last_month = 0;
foreach ($data_day_last_month as $kt => $vla) {
    //                 if(!empty($vla['day_last_month'])){
    //                     $total_day_last_month = $total_day_last_month + $vla['day_last_month'];
    //                 }
    if(!empty($vla['shift'])&& in_array($vla['shift'], array(1,2,3,6))){
        $total_day_last_month = $total_day_last_month + 1;
    }elseif (!empty($vla['shift']) && $vla['shift']==9) {
        $total_day_last_month = $total_day_last_month + 0.5;
    }
}
// Tính tổng số ngày bổ sung công tháng trước đã được appr 
            $db   = Zend_Registry::get('db');
            $stmt_approve   = $db->prepare("CALL `total_work_day_last_month_approve`(:p_month_required, :p_staff_id)");
            $stmt_approve->bindParam('p_month_required', My_Util::escape_string($p_month_required) , PDO::PARAM_STR);
            $stmt_approve->bindParam('p_staff_id', My_Util::escape_string($staff_id), PDO::PARAM_STR);
            $stmt_approve->execute();
            $data_day_last_month_approve = $stmt_approve->fetchAll();
            $stmt_approve->closeCursor(); 
            $stmt_approve = $db = null;
 
            $total_day_last_month_approve = 0;
            foreach ($data_day_last_month_approve as $kt2 => $vla2) {
//                 if(!empty($vla2['day_last_month'])){
//                     $total_day_last_month_approve = $total_day_last_month_approve + $vla2['day_last_month'];
//                 }
                if(!empty($vla2['shift'])&& in_array($vla2['shift'], array(1,2,3,6))){
                    $total_day_last_month_approve = $total_day_last_month_approve + 1;
                }elseif (!empty($vla2['shift'])&&$vla2['shift']==9) {
                    $total_day_last_month_approve = $total_day_last_month_approve + 0.5;
                }
            }
 
            // Tính tổng số ngày phat
            $QCheckIn               = new Application_Model_CheckIn();
            $params_penalty = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'month' => $month,
                'year' => $year,
                'department' => $staff['department'],
                'team' => $staff['team'],
                'title' => $staff['title'],
                'code'  => $staff['code'],
                'name'  =>  null,
                'area'  => null
            );
            $data_penalty = $QCheckIn->listLate($params_penalty);
            $this->view->total_late = $data_penalty[0]['total_late'];
            $this->view->forget_check_in = $data_penalty[0]['forget_check_in'];
            $this->view->ngay_phat = $data_penalty[0]['ngay_phat'];

            $this->view->total_day_last_month = $total_day_last_month;
            $this->view->total_day_last_month_approve = $total_day_last_month_approve;


    
    $data   = $QTime2->getInfoStaffView($params);
    $this->view->auth = $staff_id;
    
    
    $this->view->params = $params;
    $this->view->data = $data;
    $this->view->off_date = $staff['off_date'];
    
    $QLeavInfo = new Application_Model_LeaveInfo ();
    
    $QLeaveType = new Application_Model_LeaveType ();
    $this->view->parent_type = $QLeaveType->getParent ();

$QReasonTempTime = new Application_Model_ReasonTempTime();
$reason_temp_time=$QReasonTempTime->fetchAll()->toArray();
$this->view->reason_temp_time = $reason_temp_time;
$QReasonTimeLate = new Application_Model_ReasonTimeLate();
$reason_temp_late=$QReasonTimeLate->fetchAll()->toArray();
$this->view->reason_temp_late = $reason_temp_late;

?>