<?php 
define('MAX_SIZE_UPLOAD', 3145728);
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$QLeaveDetail = new Application_Model_LeaveDetail ();
$leave_id 	  = $this->getRequest ()->getParam ( 'leave_id' );
$due_date 	  = $this->getRequest ()->getParam ( 'due_date',NULL );
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$db = Zend_Registry::get('db');

if ($this->getRequest()->getMethod() == 'POST') {
	$reponse = array('status' => 1, 'message' => 'Thành công');
	
	if(!empty($_FILES['image']['name'])){
	    $ext_arr = array('jpg','png','JPG','PNG','jpeg','JPEG');
	    
	    $ext1 = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
	    
	    if(($_FILES['image']['size'] > MAX_SIZE_UPLOAD)){
	        $reponse['status']= -5;
	        $reponse['message'] = "Kích thước vượt quá 3Mb";
	        $this->view->mess = $reponse;
	        return ;
	    }
	    if(!in_array($ext1, $ext_arr)){
	        $reponse = array('status' => -3, 'message' => 'Hình sai định dạng png, jpg, jpeg');
	        $this->view->mess = $reponse;
	        return;
	    }
	    $name1= "leave_".$leave_id.'_'.$userStorage->id."_". time().".png";
	    $file1 = APPLICATION_PATH . "/../public/photo/leave/" . $name1;
	    if (!empty($_FILES['image']['name']) ) {
	        $success1 = @move_uploaded_file($_FILES['image']['tmp_name'], $file1);
	    }
	    
	    if($success1){
                require_once "Aws_s3.php";
                $s3_lib = new Aws_s3();
                $s3_lib->uploadS3($file1, "photo/leave/", $name1);
                
	        $data = array(
	            'image'    => $name1,
	           // 'due_date' => $due_date
	        );
	        if(!empty($due_date)){
	            $data['due_date'] = $due_date;
	        }
	        $where = $QLeaveDetail->getAdapter()->quoteInto('id = ?', $leave_id);
	        $QLeaveDetail->update($data,$where);
	    }
	}
	$this->redirect(HOST.'staff-time/staff-view');
	
}


?>