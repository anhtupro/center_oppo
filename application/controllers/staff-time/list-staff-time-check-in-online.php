<?php

$this->_helper->viewRenderer->setRender('list-staff-time-check-in-online');
$page = $this->getRequest()->getParam('page', 1);
$name = $this->getRequest()->getParam('name', null);
$code = $this->getRequest()->getParam('code', null);
$export = $this->getRequest()->getParam('export', 0);
$only_training = $this->getRequest()->getParam('only_training', '0');
$month = $this->getRequest()->getParam('month', null);
$year = $this->getRequest()->getParam('year', date('Y'));
$area = $this->getRequest()->getParam('area', null);
$department = $this->getRequest()->getParam('department', null);
$team = $this->getRequest()->getParam('team', null);
$title = $this->getRequest()->getParam('title', null);
$off_long = $this->getRequest()->getParam('off_long');
$dev = $this->getRequest()->getParam('dev', 0);



$this->view->url = HOST . 'staff-time/list-staff-time-check-in-online' . ($params ? '?' . http_build_query($params) .
        '&' : '?');


$week = $this->getRequest()->getParam('week', 0);

$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;
$total = 0;
$params = array();
if (!empty($month)) {
    $firstDayOfWeek = date('Y-' . $month . '-d', strtotime("monday +" . $week . " week"));
} else {
    $firstDayOfWeek = date('Y-m-d', strtotime("monday +" . $week . " week"));
}
$lastDayOfWeek = date('Y-m-d', strtotime($firstDayOfWeek . " +6 days"));

$params['from_date'] = $firstDayOfWeek;
$params['to_date'] = $lastDayOfWeek;
$params['department'] = $department;
$params['team'] = $team;
$params['title'] = $title;

$QTempTime = new Application_Model_TempTime();
$listStaffTime = $QTempTime->fetchPagination($page, $limit, $total, $params);
$QDepartment = new Application_Model_Team();

$QReserveFee = new Application_Model_ReserveFee();
$department = $QDepartment->get_cache();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$recursiveDeparmentTeamTitle = $QDepartment->get_recursive_cache();


$arrStaffDate = array();
foreach ($listStaffTime as $key => $value) {
    $arrayTemp = null;
    $arrayTemp = explode(',', $value['arrDate']);
    foreach ($arrayTemp as $k => $v) {
        $arrStaffDate[$value['staff_id']][$k] = strtotime($v);
    }
}


$dayName = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
$arrayDate = $arrayDayOfMonth = array();
for ($i = 0; $i <= 6; $i++) {
    $arrayDate[$firstDayOfWeek] = $dayName[date('w', strtotime($firstDayOfWeek))];
    $firstDayOfWeek = date('Y-m-d', strtotime($firstDayOfWeek . " +1 days"));
}
$this->view->params = $params;
$this->view->listStaffTime = $listStaffTime;
$this->view->arrStaffDate = $arrStaffDate;
$this->view->week = $week;
$this->view->month = $month;
$this->view->arrayDate = $arrayDate;

$this->view->userStorage = $userStorage;
$this->view->department = $department;
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
