<?php
$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();

set_time_limit(0);
ini_set('memory_limit', '5120M');
$month = $this->getRequest()->getParam('month', date('m'));
$year = $this->getRequest()->getParam('year', date('Y'));
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);

$from_date    = $year . '-' . $month . '-01';
$to_date      = $year . '-' . $month . '-' . $number_day_of_month;

$db           = Zend_Registry::get('db');
$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$sql          = "CALL `PR_Export_List_Staff_Time_New`(:from_date, :to_date, :staff_id)";

$stmt = $db->prepare($sql);
$stmt->bindParam('from_date', My_Util::escape_string($from_date), PDO::PARAM_STR);
$stmt->bindParam('to_date', My_Util::escape_string($to_date), PDO::PARAM_STR);
$stmt->bindParam('staff_id', My_Util::escape_string($to_date), PDO::PARAM_STR);

$stmt->execute();
$data_export = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = $db = null;

require_once 'PHPExcel.php';

$alphaExcel = new My_AlphaExcel();

$PHPExcel = new PHPExcel();
$heads = array(
    $alphaExcel->ShowAndUp() => 'Company',
    $alphaExcel->ShowAndUp() => 'Code',
    $alphaExcel->ShowAndUp() => 'CMND',
    $alphaExcel->ShowAndUp() => 'Name',
    $alphaExcel->ShowAndUp() => 'Department',
    $alphaExcel->ShowAndUp() => 'Team',
    $alphaExcel->ShowAndUp() => 'Title',
    $alphaExcel->ShowAndUp() => 'Area',
    $alphaExcel->ShowAndUp() => 'Join date',
    $alphaExcel->ShowAndUp() => 'Off date',
);

for($i = 1; $i <= $number_day_of_month; $i++)
{
    $heads[$alphaExcel->ShowAndUp()] = $i;
}

$heads[$alphaExcel->ShowAndUp()] = 'Công thường';
$heads[$alphaExcel->ShowAndUp()] = 'Công gãy';
$heads[$alphaExcel->ShowAndUp()] = 'Công training';
$heads[$alphaExcel->ShowAndUp()] = 'Phép';
$heads[$alphaExcel->ShowAndUp()] = 'Công chủ nhật';
$heads[$alphaExcel->ShowAndUp()] = 'Công lễ';

$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

foreach($heads as $key => $value)
{
    $sheet->setCellValue($key.'1', $value);
}

$index = 1;
foreach($data_export as $key => $value)
{
    $alphaExcel = new My_AlphaExcel();
    
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['com_name']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['cmnd'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), date('d/m/Y', strtotime($value['joined_at']) ));
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($info['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );

    for($i = 1; $i <= $number_day_of_month; $i++)
    {
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value[$i]);
    }
    
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congthuong']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['conggay']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congtraining']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['phep']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congchunhat']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['congle']);
  
    $index++;
    
}
$filename = 'Export Time - ' . date('Y-m-d H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;

