<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$month_lock = $this->getRequest()->getParam('month_lock');
$year_lock = $this->getRequest()->getParam('year_lock');

$QLockTime = new Application_Model_LockTime();
$result = $QLockTime->setLockTimeSalary($month_lock,$year_lock);
if($result){
    $data = [
        'status' => 1 
    ];
    echo json_encode($data);
    exit();
}
?>