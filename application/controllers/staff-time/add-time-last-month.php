<?php
    $this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();
    $date = $this->getRequest()->getParam('date');
    $shift_pick = $this->getRequest()->getParam('shift_last');
    $reason = $this->getRequest()->getParam('reason');
    //////////////////////////////////
    $staff_id 	= $this->getRequest ()->getParam ( 'staff_update' );
    $month = $this->getRequest()->getParam('month', date('m'));
    $year = $this->getRequest()->getParam('year', date('Y'));
    $QTimeLastMonth = new Application_Model_TimeLastMonth(); 
    try {

        if (empty($staff_id) || empty($reason) || empty($shift_pick)) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('ERROR: Nhập thông tin chưa đầy đủ..!');
//            $this->redirect(HOST . 'staff-time/staff-view?month=5&year=2017');
        }

        //////// CALL PROCEDURE Tính tổng công trong một ngày
        $dbp = Zend_Registry::get('db');
        $stmt_tt = $dbp->prepare("CALL `get_total_time_staff_by_day`(:p_staff_id, :p_work_day)");
        $stmt_tt->bindParam('p_staff_id', $staff_id, PDO::PARAM_STR);
        $stmt_tt->bindParam('p_work_day', $date, PDO::PARAM_STR);
        $stmt_tt->execute();
        $total_by_date = $stmt_tt->fetchAll();
        $stmt_tt->closeCursor();
        $stmt_tt = $dbp = null;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        if ($total_by_date[0]['total_time'] == 1) {
             echo json_encode (array( 'status' => 1,'message' => "ERROR: Tổng công phép của ngày " . $date . " đã đủ 1 công..!! "));
        } elseif ($total_by_date[0]['total_time'] == 0.5 && $shift_pick != 9) {
            echo json_encode (array( 'status' => 1,'message' => "ERROR: Tổng công phép của ngày " . $date . " đã có 0.5 công. Chỉ được bổ sung thêm 0.5 công.!! "));
        } else {
            $where_TLM = array();
            $where_TLM[] = $QTimeLastMonth->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where_TLM[] = $QTimeLastMonth->getAdapter()->quoteInto('date = ?', $date);
            $QTimeLastMonth_check = $QTimeLastMonth->fetchRow($where_TLM);
          
            if (empty($QTimeLastMonth_check)) {// insert
                $params_ins = array(
                    'staff_id' => $staff_id,
                    'date' => $date,
                    'shift' => $shift_pick,
                    'office_time' => NULL,
                    'status' => 1,
                    'reason' => $reason,
                    'created_at' => date('Y-m-d H:i:s'),
                    'approved_at' => date('Y-m-d H:i:s'),
                    'note' => 'Admin Import',
                    'month_required' => date('Y-m-1')
                );

                $QTimeLastMonth->insert($params_ins);
            } else {// update
                if ($QTimeLastMonth_check['status'] == 1) {
                    echo json_encode (array( 'status' => 1,'message' => "ERROR: Ngày " . $date . " đã được bổ sung công cũ và xác nhận.!! "));
                }

                $params_ins = array(
                    'shift' => $shift_pick,
                    'office_time' => NULL,
                    'status' => 1 ,
                    'reason' => $reason,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'approved_at' => date('Y-m-d H:i:s'),
                    'note' => 'Admin Import',
                    'month_required' => date('Y-m-1')
                );
                $QTimeLastMonth->update($params_ins, $where_TLM);
            }
             echo json_encode (array( 'status' => 1,'message' => "Thành công.!! "));
        } //end else     

        $db->commit();
    } catch (exception $e) {
        $db->rollback();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $e->getMessage();
        $this->redirect(HOST . "staff-time/staff-view?month=" . $month . "&year=" . $year);
        return;
    }