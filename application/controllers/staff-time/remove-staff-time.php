<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$QTimeGps            = new Application_Model_TimeGps();
$QTimeMachine        = new Application_Model_TimeMachine();
$QTimeLate           = new Application_Model_TimeLate();
$QTempTime           = new Application_Model_TempTime();
$QStaffTimeRemoveLog = new Application_Model_StaffTimeRemoveLog();
$QLockTime           = new Application_Model_LockTime();
$staff_id            = $this->getRequest()->getParam('staff_id');
$json_remove_time    = json_decode($this->getRequest()->getParam('list_remove_time'), true);
$userStorage         = Zend_Auth::getInstance()->getStorage()->read();
$datetime            = date('Y-m-d H:i:s');

if (!empty($staff_id) AND ! empty($json_remove_time)) {
    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $db           = Zend_Registry::get('db');
    $current_date = $json_remove_time[0];
    $current_date = str_replace("'", "", $current_date);
    $date_check   = date_create($current_date);
    $month_locked = date_format($date_check, "m");
    $year_locked  = date_format($date_check, "Y");
    $where_lock[] = $QLockTime->getAdapter()->quoteInto('month = ?', $month_locked);
    $where_lock[] = $QLockTime->getAdapter()->quoteInto('year = ? ', $year_locked);
    $result_lock  = $QLockTime->fetchRow($where_lock);
    if (!empty($result_lock) && $userStorage->department <> 154) {
        echo json_encode(array(
            'status'  => 0,
            'message' => "Đã lock công"
        ));
        exit();
    }
    $db->beginTransaction();
    try {

        $json_remove_time = implode(",", $json_remove_time);
        $json_remove_time = str_replace("'", "", $json_remove_time);
        $json_remove_time = explode(",", $json_remove_time);
        //MCC
        $where_Machine    = array();
        $where_Machine[]  = $QTimeMachine->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_Machine[]  = $QTimeMachine->getAdapter()->quoteInto('time_number <> ?', 0);
        $where_Machine[]  = $QTimeMachine->getAdapter()->quoteInto('check_in_day IN(?)', $json_remove_time);
        $timeMachine      = $QTimeMachine->fetchAll($where_Machine)->toArray();
        if (!empty($timeMachine)) {
            foreach ($timeMachine as $item) {
                $time_number = $item['time_number'];
                $data        = array(
                    'time_number' => 0,
                    'note'        => 'Remove ' . $time_number . ', remove by : ' . $userStorage->id . 'created_at : ' . $datetime,
                );

                $where_machine_item   = array();
                $where_machine_item[] = $QTimeMachine->getAdapter()->quoteInto('staff_id = ?', $staff_id);
                $where_machine_item[] = $QTimeMachine->getAdapter()->quoteInto('check_in_day = ?', $item['check_in_day']);
                $QTimeMachine->update($data, $where_machine_item);
                //luu lock xoa
                $data_log             = [
                    'staff_id'    => $staff_id,
                    'date_remove' => $item['check_in_day'],
                    'type'        => 1, //MCC
                    'create_at'   => $datetime,
                    'create_by'   => $userStorage->id
                ];
                $QStaffTimeRemoveLog->insert($data_log);
            }
        }

        //GPS
        $where_Gps   = array();
        $where_Gps[] = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_Gps[] = $QTimeGps->getAdapter()->quoteInto('time_number <> ?', 0);
        $where_Gps[] = $QTimeGps->getAdapter()->quoteInto('check_in_day IN(?)', $json_remove_time);
        $timeGps     = $QTimeGps->fetchAll($where_Gps)->toArray();
        if (!empty($timeGps)) {
            foreach ($timeGps as $item) {
                $time_number      = $item['time_number'];
                $data             = array(
                    'time_number' => 0,
                    'note'        => 'Remove ' . $time_number . ', remove by : ' . $userStorage->id . 'created_at : ' . $datetime,
                );
                $where_gps_item   = array();
                $where_gps_item[] = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $staff_id);
                $where_gps_item[] = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $item['check_in_day']);
                $QTimeGps->update($data, $where_gps_item);
                //luu lock xoa
                $data_log         = [
                    'staff_id'    => $staff_id,
                    'date_remove' => $item['check_in_day'],
                    'type'        => 2, //GPS
                    'create_at'   => $datetime,
                    'create_by'   => $userStorage->id
                ];
                $QStaffTimeRemoveLog->insert($data_log);
            }
        }

        // Time_late
        $where_late   = array();
        $where_late[] = $QTimeLate->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_late[] = $QTimeLate->getAdapter()->quoteInto('del <> ?', 1);
        $where_late[] = $QTimeLate->getAdapter()->quoteInto('date IN(?)', $json_remove_time);
        $time_late    = $QTimeLate->fetchAll($where_late)->toArray();
        if (!empty($time_late)) {
            foreach ($time_late as $item) {
                /* $data = array(
                  'del' => 1,
                  'note' => 'Remove by : ' . $userStorage->id . ', created_at : ' . $datetime,
                  ); */
                $where_late_item   = array();
                $where_late_item[] = $QTimeLate->getAdapter()->quoteInto('staff_id = ?', $staff_id);
                $where_late_item[] = $QTimeLate->getAdapter()->quoteInto('date = ?', $item['date']);
                $where_late_item[] = $QTimeLate->getAdapter()->quoteInto('del <> ?', 1);
                $where_late_item[] = $QTimeLate->getAdapter()->quoteInto('status = ?', 0);
                $QTimeLate->delete($where_late_item);

                //luu lock xoa
                $data_log = [
                    'staff_id'    => $staff_id,
                    'date_remove' => $item['date'],
                    'type'        => 3, //LATE
                    'create_at'   => $datetime,
                    'create_by'   => $userStorage->id
                ];
                $QStaffTimeRemoveLog->insert($data_log);
            }
        }

        // Temp_time
        $where_temp   = array();
        $where_temp[] = $QTempTime->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_temp[] = $QTempTime->getAdapter()->quoteInto('del <> ?', 1);
        $where_temp[] = $QTempTime->getAdapter()->quoteInto('date IN(?)', $json_remove_time);
        $temp_time    = $QTempTime->fetchAll($where_temp)->toArray();
        if (!empty($temp_time)) {
            foreach ($temp_time as $item) {
                /* $data = array(
                  'del' => 1,
                  'note' => 'Remove by : ' . $userStorage->id . ', created_at : ' . $datetime,
                  ); */
                $where_temp_item   = array();
                $where_temp_item[] = $QTempTime->getAdapter()->quoteInto('staff_id = ?', $staff_id);
                $where_temp_item[] = $QTempTime->getAdapter()->quoteInto('date = ?', $item['date']);
                $where_temp_item[] = $QTempTime->getAdapter()->quoteInto('del <> ?', 1);
                //$where_temp_item[] = $QTempTime->getAdapter()->quoteInto('status = ?', 0);
                $QTempTime->delete($where_temp_item);

                //luu lock xoa
                $data_log = [
                    'staff_id'    => $staff_id,
                    'date_remove' => $item['date'],
                    'type'        => 4, //BSC
                    'create_at'   => $datetime,
                    'create_by'   => $userStorage->id
                ];
                $QStaffTimeRemoveLog->insert($data_log);
            }
        }
        echo json_encode(array(
            'status'  => 1,
            'message' => "success"
        ));
        $db->commit();
    } catch (exception $e) {
        $db->rollback();
        echo "<pre>";
        print_r($e->getMessage());
        echo "</pre>";
    }
}