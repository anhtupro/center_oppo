<?php

$this->_helper->viewRenderer->setNoRender();
$QLeaveDetail = new Application_Model_LeaveDetail ();
$create_leave = $this->getRequest()->getParam('add-leave');
if (!empty($create_leave)) {
    $is_leave_half = My_Util::escape_string($this->getRequest()->getParam("is-leave-half"));
    $from          = My_Util::escape_string($this->converDate($this->getRequest()->getParam("from")));
    $to            = My_Util::escape_string($this->converDate($this->getRequest()->getParam("to")));
    $date_leave    = My_Util::escape_string($this->converDate($this->getRequest()->getParam("date-leave-half")));
    $leave_type    = My_Util::escape_string($this->getRequest()->getParam("leave-type"));
    $reason        = My_Util::escape_string($this->getRequest()->getParam("reason"));
    $staff_update  = intval($this->getRequest()->getParam('staff_update'));
    $due_date      = My_Util::escape_string($this->converDate($this->getRequest()->getParam('due_date')));

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $db     = Zend_Registry::get('db');
    $QStaff = new Application_Model_Staff();
    $QTeam  = new Application_Model_Team();

    $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_update);
    $staff      = $QStaff->fetchRow($whereStaff);
    $joined_at  = $staff['joined_at'];
    $is_office  = $staff['is_officer'];
    $staff_id   = $staff_update;

    $staff_info = $QStaff->find($userStorage->id);
    $staff_info = $staff_info->current();

    $title    = $staff_info['title'];
    $team     = $QTeam->find($title);
    $team     = $team->current();
    $group_id = $team['access_group'];


    if (!empty($date_leave) && isset($date_leave)) {
        $date = $date_leave;
        if ($date_leave < $joined_at) {
            echo json_encode(array('status' => 0, 'message' => "Ngày phép trước ngày bắt đầu làm việc"));
            exit();
        }
    } else if ($from < $joined_at) {
        echo json_encode(array('status' => 0, 'message' => "Ngày phép trước ngày bắt đầu làm việc."));
        exit();
    }
    
    if ($leave_type == 17) {
        if(empty($due_date)){
            $flashMessenger->setNamespace('error')->addMessage("Vui lòng nhập ngày dự sinh .");
            $this->_redirect("/leave/create");
        }
    }
    
    if ($leave_type == 2) {

        //Check locked time
        $QLockTime  = new Application_Model_LockTime();
        $where_lock = array();
        if (!empty($from)) {
            $date_check   = date_create($from);
            $month_locked = date_format($date_check, "m");
            $year_locked  = date_format($date_check, "Y");
        } elseif (!empty($date)) {
            $date_check   = date_create($date);
            $month_locked = date_format($date_check, "m");
            $year_locked  = date_format($date_check, "Y");
        }

        $where_lock[] = $QLockTime->getAdapter()->quoteInto('month = ?', $month_locked);
        $where_lock[] = $QLockTime->getAdapter()->quoteInto('year = ? ', $year_locked);
        $result_lock  = $QLockTime->fetchRow($where_lock);

        if (!empty($result_lock) AND $group_id != HR_ID) {
            echo json_encode(array('status' => 0, 'message' => "Tháng này đã lock bảng công."));
            exit();
        }
        //End Check locked time
        if(empty($date_check)){
            echo json_encode(array('status' => 0, 'message' => "Check ngày đăng kí phép."));
            exit();
        }
        $date_check_contract = date_format($date_check, "Y-m-d");
        //Check Firt contract type in (2, 3, 17)
        $sql_contract  = "SELECT sc.* FROM staff_contract sc
            INNER JOIN (
                        SELECT MIN( sc.id ) id, sc.staff_id, COUNT( sc.id ) total
                        FROM staff_contract sc
                        WHERE sc.contract_term
                        IN ( 2, 3, 17 ) 
                        AND sc.`is_expired` = 0
                        AND sc.`is_disable` = 0
                        AND sc.staff_id = $staff_id
                        GROUP BY sc.staff_id
            )c ON c.id = sc.id and sc.from_date <= '$date_check_contract' AND sc.to_date >= '$date_check_contract' AND sc.`is_expired` = 0 AND sc.`is_disable` = 0";
        $stmt_contract = $db->prepare($sql_contract);
        $stmt_contract->execute();
        $check_data    = $stmt_contract->fetchAll();

        if (!empty($check_data)) {
            echo json_encode(array('status' => 0, 'message' => "Thời gian đăng ký phép năm là thời gian thử việc, bạn vui lòng chọn lại thời gian nghỉ phép năm từ thời điểm ký hợp đồng chính thức trở đi"));
            exit();
        }

        // check phep hop le hay khong
        // check 1 lan nghi
        $stmt          = $db->prepare('CALL PR_check_max_day_per_time (:staff_id, :leave_type, :from_date, :to_date) ');
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
        $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
        $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
        $stmt->execute();
        $pass_per_time = $stmt->fetchAll();
        $stmt->closeCursor();
        if ($pass_per_time[0]['pass_per_time'] == 1) {

            echo json_encode(array('status' => 0, 'message' => "Số ngày nghỉ vượt quá giới hạn của 1 lần nghỉ."));
            exit();
        }
        // check ca nam
        if ($date) {
            $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix_hr (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
            $stmt->bindParam('to_date', My_Util::escape_string($date), PDO::PARAM_STR);
            $stmt->bindParam('from_date', My_Util::escape_string($date), PDO::PARAM_STR);
        } else {
            $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix_hr (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
            $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
            $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
        }

        $leave_half_check = ($is_leave_half == 0.5) ? 1 : 0;
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
        $stmt->bindParam('is_leave_half', $leave_half_check, PDO::PARAM_INT);

        $stmt->execute();
        $pass_per_year = $stmt->fetchAll();
        $stmt->closeCursor();
        if ($leave_type == 2) {
            if ($pass_per_year[0]['pass_per_year'] == 1) {
                echo json_encode(array('status' => 0, 'message' => "Số ngày nghỉ vượt quá giới hạn của 1 năm. (Tối đa ".$pass_per_year[0]['annual']." ngày)"));
                exit();
            }
        }


        if ($is_leave_half == 1) {
            if (strtotime($from) > strtotime($to)) {
                echo json_encode(array('status' => 0, 'message' => "Ngày bắt đầu nghỉ phép không được lơn hơn ngày kết thúc"));
                exit();
            } else {
                $params = array(
                    'from'        => $from,
                    'to'          => $to,
                    'leave_type'  => $leave_type,
                    'is_officer'  => $is_office,
                    'staff_id'    => $staff_id,
                    'reason'      => $reason,
                    'status'      => ($group_id == HR_ID) ? 1 : 0,
                    'note'        => ($group_id == HR_ID) ? 'Manage Import' : 'Admin Import',
                    'created_at'  => date('Y-m-d H:i:s'),
                    'created_by'  => $userStorage->id,
                    'approved_by' => ($group_id == HR_ID) ? $userStorage->id : null,
                    'approved_at' => ($group_id == HR_ID) ? date('Y-m-d H:i:s') : null,
                );
                if (!empty($name1)) {
                    $params['image'] = $name1;
                }
                if (!empty($due_date)) {
                    $params['due_date'] = $due_date;
                }

                $QLeaveDetail = new Application_Model_LeaveDetail();
                $data         = $QLeaveDetail->insertLeave2($params);
            }
        } else {

            $params = array(
                'date'        => $date,
                'leave_type'  => $leave_type,
                'is_officer'  => $is_office,
                'staff_id'    => $staff_id,
                'reason'      => $reason,
                'status'      => ($group_id == HR_ID) ? 1 : 0,
                'note'        => ($group_id == HR_ID) ? 'Manage Import' : 'Admin Import',
                'created_at'  => date('Y-m-d H:i:s'),
                'created_by'  => $userStorage->id,
                'approved_by' => ($group_id == HR_ID) ? $userStorage->id : null,
                'approved_at' => ($group_id == HR_ID) ? date('Y-m-d H:i:s') : null,
            );
            if (!empty($name1)) {
                $params['image'] = $name1;
            }
            if (!empty($due_date)) {
                $params['due_date'] = $due_date;
            }


            $QLeaveDetail = new Application_Model_LeaveDetail();
            $data         = $QLeaveDetail->insertLeaveHalf2($params);
        }
        if ($data['status'] == 1) {
            echo json_encode(array('status' => 1, 'message' => "Đăng ký phép thành công"));
            exit();
        } else {
            echo json_encode(array('status' => 0, 'message' => "Lỗi đăng ký phép"));
            exit();
        }
    } else {


        if ($is_leave_half == 1) {
            $params = array(
                'from'       => $from,
                'to'         => $to,
                'leave_type' => $leave_type,
                'is_officer' => $is_office,
                'staff_id'   => $staff_id,
                'reason'     => $reason,
                'status'     => ($group_id == HR_ID) ? 1 : 0,
                'note'       => ($group_id == HR_ID) ? 'Manage Import' : 'Admin Import',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
            );

            if (!empty($name1)) {
                $params['image'] = $name1;
            }
            if (!empty($due_date)) {
                $params['due_date'] = $due_date;
            }
            $QLeaveDetail = new Application_Model_LeaveDetail();
            $data         = $QLeaveDetail->insertLeave2($params);
        } else {
            $params = array(
                'date'        => $date,
                'leave_type'  => $leave_type,
                'is_officer'  => $is_office,
                'staff_id'    => $staff_id,
                'reason'      => $reason,
                'status'      => ($group_id == HR_ID) ? 1 : 0,
                'note'        => ($group_id == HR_ID) ? 'Manage Import' : 'Admin Import',
                'created_at'  => date('Y-m-d H:i:s'),
                'created_by'  => $userStorage->id,
                'approved_by' => ($group_id == HR_ID) ? $userStorage->id : null,
                'approved_at' => ($group_id == HR_ID) ? date('Y-m-d H:i:s') : null,
            );
            if (!empty($name1)) {
                $params['image'] = $name1;
            }
            if (!empty($due_date)) {
                $params['due_date'] = $due_date;
            }


            $QLeaveDetail = new Application_Model_LeaveDetail();
            $data         = $QLeaveDetail->insertLeaveHalf2($params);
        }
        echo json_encode($data);
        exit();
    }
}