<?php
$Qstaff = new Application_Model_Staff();
$this->_helper->layout->disableLayout();
$month = $this->getRequest()->getParam('month');
$staff_id = $this->getRequest()->getParam('staff_id');
$year = $this->getRequest()->getParam('year');

$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);

$from_date = $year . '-' . $month . '-01 00:00:00';
$to_date = $year . '-' . $month . '-' . $number_day_of_month . ' 23:59:59';

$db = Zend_Registry::get('db');
$stmt = $db->prepare("CALL PR_List_Staff_Time_Approve_By_Id(:staff_id, :p_month, :p_year)");
$stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
$stmt->bindParam('p_month', $month, PDO::PARAM_INT);
$stmt->bindParam('p_year', $year, PDO::PARAM_INT);
$stmt->execute();
$data = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = $db = null;

$where_staff =[] ;
$where_staff[] = $Qstaff->getAdapter()->quoteInto('id = ?',$staff_id);
$staff = $Qstaff->fetchRow($where_staff);
        
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QTeam = new Application_Model_Team();
$staff_title_info = $userStorage->title;

$current_lang = $userStorage->defaut_language;
$trans = Zend_Registry::get('translate');
$list_lang = My_Lang::getLangList();

$this->view->title = $staff_title_info;
$this->view->data = $data;
$this->view->staff = $staff;