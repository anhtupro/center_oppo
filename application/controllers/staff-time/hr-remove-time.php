<?php

$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();
$staff_update 	= $this->getRequest ()->getParam ( 'staff_update' );
$date_update           = $this->getRequest ()->getParam ( 'date' );
$hr_remove_time = $this->getRequest ()->getParam ( 'hr_remove_time' );

$QLeaveDetail 	= new Application_Model_LeaveDetail ();
$QTimeGps       = new Application_Model_TimeGps();
$QTimeMachine   = new Application_Model_TimeMachine();
$QTempTime      = new Application_Model_TempTime();
$QTimeDel       = new Application_Model_TimeDel();
$QLogTimeDel       = new Application_Model_LogTimeDel();

$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$staff_id       = $userStorage->id;

if (!empty ($hr_remove_time) && !empty($staff_update) && !empty($date_update)) {
    $datetime = date ( 'Y-m-d H:i:s' );
    //MCC
    $where_Machine = array();
    $where_Machine[] = $QTimeMachine->getAdapter()->quoteInto('staff_id = ?', $staff_update);
    $where_Machine[] = $QTimeMachine->getAdapter()->quoteInto('check_in_day = ?', $date_update);
    $timeMachine  = $QTimeMachine->fetchRow($where_Machine);
    if(!empty($timeMachine)){
        $time_number = $timeMachine['time_number'];
        $data = array(
            'time_number'   => 0,
            'note'          => 'Hr remove ' . $time_number,
        );
        $QTimeMachine->update($data, $where_Machine);
        
        $whereDel = array();
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('staff_id = ?', $staff_update);
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('date = ?', $date_update);
        $rowDel = $QTimeDel->fetchRow($whereDel);
        
        if(empty($rowDel)){
            $QTimeDel->insert(array(
                'staff_id'      => $staff_update,
                'date'          => $date_update,
                'created_at'    => $datetime,
                'created_by'    => $staff_id
            ));
            
            $QLogTimeDel->insert(array(
                'staff_id'      =>    $staff_update , 
                'date'          =>    $date_update,
                'office_time'   =>    $time_number,
                'del_at'        =>    date("Y-m-d H:i:s"),
                'del_by'        =>    $staff_id , 
                'type'          =>    1  ,
                'data'          =>    json_encode($timeMachine)
            ));
        }
        
    }
    //GPS
    $where_Gps = array();
    $where_Gps[] = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $staff_update);
    $where_Gps[] = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date_update);
    $timeNumber  = $QTimeGps->fetchRow($where_Gps);
    
    if(!empty($timeNumber)){
        $time_number = $timeNumber['time_number'];
        $data = array(
            'time_number'   => 0,
            'note'          => 'Hr remove ' . $time_number,
        );
        $where_Gps->update($data, $where_Gps);
        
        $whereDel = array();
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('staff_id = ?', $staff_update);
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('date = ?', $date_update);
        $rowDel = $QTimeDel->fetchRow($whereDel);
        
        if(empty($rowDel)){
            $QTimeDel->insert(array(
                'staff_id'      => $staff_update,
                'date'          => $date_update,
                'created_at'    => $datetime,
                'created_by'    => $staff_id
            ));
            
            $QLogTimeDel->insert(array(
                'staff_id'      =>    $staff_update , 
                'date'          =>    $date_update,
                'office_time'   =>    $time_number,
                'del_at'        =>    date("Y-m-d H:i:s"),
                'del_by'        =>    $staff_id , 
                'type'          =>    2  ,
                'data'          =>    json_encode($timeNumber)
            ));
        }
    }
    //Temptime
    $where_temp_time = array();
    $where_temp_time[] = $QTempTime->getAdapter()->quoteInto('staff_id = ?', $staff_update);
    $where_temp_time[] = $QTempTime->getAdapter()->quoteInto('date = ?', $date_update);
    $temp_time  = $QTempTime->fetchRow($where_temp_time);
    if(!empty($temp_time)){
        $office_time = $temp_time['office_time'];
        $data = array(
            'office_time'   => 0,
            'note'          => 'Hr remove ' . $office_time,
            'status'        => 2,
            'del'        => 1,
        );
        $QTempTime->update($data, $where_temp_time);
        
        $whereDel = array();
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('staff_id = ?', $staff_update);
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('date = ?', $date_update);
        $rowDel = $QTimeDel->fetchRow($whereDel);
        
        if(empty($rowDel)){
            $QTimeDel->insert(array(
                'staff_id'      => $staff_update,
                'date'          => $date_update,
                'created_at'    => $datetime,
                'created_by'    => $staff_id
            ));
            
            $QLogTimeDel->insert(array(
                'staff_id'      =>    $staff_update , 
                'date'          =>    $date_update,
                'office_time'   =>    $office_time,
                'del_at'        =>    date("Y-m-d H:i:s"),
                'del_by'        =>    $staff_id , 
                'type'          =>    3,
                'data'          =>    json_encode($temp_time)
            ));
        }
    }
    
        $whereDel = array();
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('staff_id = ?', $staff_update);
        $whereDel[] = $QTimeDel->getAdapter()->quoteInto('date = ?', $date_update);
        $rowDel = $QTimeDel->fetchRow($whereDel);
        
        if(empty($rowDel)){
            $QTimeDel->insert(array(
                'staff_id'      => $staff_update,
                'date'          => $date_update,
                'created_at'    => $datetime,
                'created_by'    => $staff_id
            ));
            
            $QLogTimeDel->insert(array(
                'staff_id'      =>    $staff_update , 
                'date'          =>    $date_update,
                'office_time'   =>    0,
                'del_at'        =>    date("Y-m-d H:i:s"),
                'del_by'        =>    $staff_id , 
                'type'          =>    4 // other
            ));
     }
}

