<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;
$team = $userStorage->team;
$office_id = $userStorage->office_id;

$QTempTime = new Application_Model_TempTime();
$QTime2 = new Application_Model_Time2 ();
$QTimeImageLocation = new Application_Model_TimeImageLocation();
$QTempTimeLog = new Application_Model_TempTimeLog();

if (!empty($this->getRequest()->getParam('add_online_time'))) {

    $date = $this->getRequest()->getParam('date');
    $office_time = $this->getRequest()->getParam('office_time');
    
    $reason = "Làm việc online trong mùa dịch";
    $add_reason = 7;

    $QStaff = new Application_Model_Staff ();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $reason = trim($reason);
    $db = Zend_Registry::get('db');
    $datetime = date('Y-m-d H:i:s');

    try {

        $db->beginTransaction();

        $where_temp_time = array();
        $where_temp_time [] = $QTempTime->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_temp_time [] = $QTempTime->getAdapter()->quoteInto('date = ?', $date);
        $result_date = $QTempTime->fetchRow($where_temp_time);

        $data_temp_time = array(
            'staff_id' => $staff_id,
            'date' => $date,
            'shift' => ($office_time == 1) ? 6 : 9,
            'office_time' => $office_time,
            'reason' => $reason,
            'add_reason' => $add_reason,
            'status' => (in_array($team, array(131, 611)) AND $office_id != 49 ) ? 1 : 0,
            'created_at' => $datetime,
            'approved_at' => (in_array($team, array(131, 611)) AND $office_id != 49) ? $datetime : NULL
        );
        $data_temp_time_log = array(
            'staff_id' => $data_temp_time['staff_id'],
            'date' => $data_temp_time['date'],
            'office_time' => $data_temp_time['office_time'],
            'reason' => $data_temp_time['reason'],
            'add_reason' => $data_temp_time['add_reason'],
            'created_at' => $data_temp_time['created_at'],
            'created_by' => $userStorage->id
        );
        $id_staff_temp = 0;
        if (empty($result_date)) {
        //Insert tbl time
            $data_time = array(
                'staff_id' => $staff_id,
                'created_at' => $datetime,
                'status' => (in_array($team, array(131, 611)) AND $office_id != 49) ? 1 : 0,
                'shift' => ($office_time == 1) ? 6 : 9
            );

            $QTime2->insert($data_time);

        //Insert tbl temp_time
            $QTempTimeLog->insert($data_temp_time_log);
            $id_staff_temp = $QTempTime->insert($data_temp_time);
        } else {
            $id_staff_temp = $result_date['id'];
            $QTempTimeLog->insert($data_temp_time_log);
            $QTempTime->update($data_temp_time, $where_temp_time);
        }

//   $QTime2->insertTempTime ( $params_update );
        $db->commit();
        echo json_encode(['status'=> 1,'id_temp'=> $id_staff_temp]) ; 
        exit();
    } catch (exception $e) {
        $db->rollback();
        echo json_encode(['status'=> 0,'message'=> $e->getMessage()]) ; 
        exit();
    }
}