<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;
try {
    
    $tempId = $this->getRequest()->getParam('temp_id');
    $QTempTime = new Application_Model_TempTime();

    $where_detail = $QTempTime->getAdapter()->quoteInto('id = ?', $tempId);
    $QTempTime->delete($where_detail);
    echo json_encode(['status'=>1]);
    exit();
} catch (Exception $exc) {
    echo json_encode(['status'=>0 , 'message'=>$exc->getMessage()]);
    exit();
}

