<?php
$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();

$datetime = date('Y-m-d H:i:s');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$late = $this->getRequest()->getParam('update_late', 0);
$date_late = $this->getRequest()->getParam('date_late');
$late_from = $this->getRequest()->getParam('late_from');
$late_to = $this->getRequest()->getParam('late_to');
$reason_late = $this->getRequest()->getParam('reason_late');
$add_reason_late = $this->getRequest()->getParam('add_reason_late');

$add_reason_soon = $this->getRequest()->getParam('add_reason_soon');
$reason_soon = $this->getRequest()->getParam('reason_soon');
$QTimeLate = new Application_Model_TimeLate();

$reason_late = trim($reason_late);
$db = Zend_Registry::get('db');
if (!empty($late) AND !empty($late_from) AND !empty($late_to)) {

    try {
        if (empty($reason_late) || empty($add_reason_late)) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('ERROR: Nhập thông tin chưa đầy đủ..!');
            $this->redirect(HOST . "staff-time/staff-view");
        }
//        $db->beginTransaction();
        $late_from = DateTime::createFromFormat('d/m/Y', $late_from)->format('Y-m-d');
        $late_to = DateTime::createFromFormat('d/m/Y', $late_to)->format('Y-m-d');
        $staff_id = $userStorage->id;
        $office_id = $userStorage->office_id;
        $team =  $userStorage->team;
    $status = ($team == 131 AND $office_id != 49) ? 1 : 0;
//    $status =  0;
        if(empty($add_reason_soon)){
            $add_reason_soon=0;
        }
        if(empty($reason_soon)){
            $reason_soon=null;
        }
    $stmt_late1 = $db->prepare("CALL PR_Add_Time_Late_staff_Fix( :p_staff_id, :p_add_reason_late, :p_satus, :p_late_from,  :p_late_to,  :p_reason, :p_add_reason_soon, :p_reason_soon )");
    $stmt_late1->bindParam('p_staff_id', $staff_id, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_add_reason_late', $add_reason_late, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_satus',$status, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_late_from', $late_from, PDO::PARAM_STR);
    $stmt_late1->bindParam('p_late_to', $late_to, PDO::PARAM_STR);
    $stmt_late1->bindParam('p_reason', $reason_late, PDO::PARAM_STR);
    $stmt_late1->bindParam('p_add_reason_soon', $add_reason_soon, PDO::PARAM_INT);
    $stmt_late1->bindParam('p_reason_soon', $reason_soon, PDO::PARAM_STR);
    $stmt_late1->execute();
        $QTimeLateLog = new Application_Model_TimeLateLog ();
        $data_time_late_log=array(
            'staff_id'=>$staff_id,
            'from_date'=>$late_from,
            'to_date'=>$late_to,
            'add_reason_late'=>$add_reason_late,
            'reason'=>$reason_late,
            'add_reason_soon'=>$add_reason_soon,
            'reason_soon'=>$reason_soon,
            'created_at'=>date('Y-m-d H:i:s'),
            'created_by'=>$userStorage->id
        );
        $QTimeLateLog->insert($data_time_late_log);
//$this->_redirect("/staff-time/staff-view");
//    $stmt_late1->closeCursor();

 
       $where_time_late = array();
       $where_time_late [] = $QTimeLate->getAdapter()->quoteInto("staff_id = ?", $staff_id);
     $where_time_late [] = $QTimeLate->getAdapter()->quoteInto("date = '$late_from'", $late_from);
       $result_late = $QTimeLate->fetchRow($where_time_late);

        $date_time_late = array(
			'add_reason_late'=>$add_reason_late,
            'reason'=>$reason_late,
            'add_reason_soon'=>$add_reason_soon,
            'reason_soon'=>$reason_soon,
			'status' => 0
        );
		if (!empty($result_late)) {
			$QTimeLate->update($date_time_late, $where_time_late);
		}
//
//        if (empty($result_late)) {
//
//            $QTimeLate->insert($date_time_late);
//        } else {
//
//            $QTimeLate->update($date_time_late, $where_time_late);
//        }

//        $db->commit();
     // $this->redirect(HOST . "staff-time/staff-view");

    } catch (exception $e) {
//        $db->rollback();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $e->getMessage();
        $this->redirect(HOST . "staff-time/staff-view");
        return;
    }
}