<?php
define('START_ROW', 2);
define('ID',0);// cột A;
define('cong_thuong',1);// cột B;
define('cong_training',2);// cột C;
define('cong_chu_nhat',3);// cột D;
define('ngay_le_x2',4);// cột E;
define('ngay_le_x3',5);// cột F;
define('ngay_le_x4',6);// cột G;
define('thu_7_x2',7);// cột H;
define('phep_nam_su_dung_trong_thang',8);// cột I;
define('ung_phep_hoan_phep',9);// cột J;
define('nghi_viec_rieng_duoc_huong_luong',10);// cột K;
define('tong_gio_tang_ca_trong_thang',11);// cột L;
$this->_helper->layout->disableLayout();

$QLogTotalStaffTimeByMonth = new Application_Model_LogTotalStaffTimeByMonth();
set_time_limit(0);
ini_set('memory_limit', -1);

$progress = new My_File_Progress('parent.set_progress');
$progress->flush(0);

$save_folder   = 'mou';
$new_file_path = '';
$requirement   = array(
    'Size'      => array('min' => 5, 'max' => 5000000),
    'Count'     => array('min' => 1, 'max' => 1),
    'Extension' => array('xls', 'xlsx'),
);

try {

    $file = My_File::get($save_folder, $requirement, true);

    if (!$file || !count($file))
        throw new Exception("Upload failed");

    $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
        . DIRECTORY_SEPARATOR . $file['folder'];

    $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
}
catch (Exception $e) {
    $this->view->errors = $e->getMessage();
    return;
}

//read file
include 'PHPExcel/IOFactory.php';
//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel   = $objReader->load($inputFileName);
}
catch (Exception $e) {

    $this->view->errors = $e->getMessage();
    return;
}

//  Get worksheet dimensions
$sheet         = $objPHPExcel->getSheet(0);
$highestRow    = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$data_staff_code = [];

for ($row = 0; $row <= $highestRow; $row++) {

    try {
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        $rowData = isset($rowData[0]) ? $rowData[0] : array();
    }
    catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

    // nothing here
} // END loop through order rows


$progress->flush(30);
$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    for ($row = START_ROW; $row <= $highestRow; $row++) {
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        $rowData = isset($rowData[0]) ? $rowData[0] : array();
        $id = (trim($rowData[ID])!=NULL) ? ($rowData[ID]) : NULL;
        if(!empty($id)) {
            $data_update = array(
                'congthuong' => (trim($rowData[cong_thuong]) != NULL) ? ($rowData[cong_thuong]) : NULL,
                'congtraining' => (trim($rowData[cong_training]) != NULL) ? ($rowData[cong_training]) : NULL,
                'congchunhat' => (trim($rowData[cong_chu_nhat]) != NULL) ? ($rowData[cong_chu_nhat]) : NULL,
                'holidayX2' => (trim($rowData[ngay_le_x2]) != NULL) ? ($rowData[ngay_le_x2]) : NULL,
                'holidayX3' => (trim($rowData[ngay_le_x3]) != NULL) ? ($rowData[ngay_le_x3]) : NULL,
                'holidayX4' => (trim($rowData[ngay_le_x4]) != NULL) ? ($rowData[ngay_le_x4]) : NULL,
                'saturdayX2' => (trim($rowData[thu_7_x2]) != NULL) ? ($rowData[thu_7_x2]) : NULL,
                'phepnam' => (trim($rowData[phep_nam_su_dung_trong_thang]) != NULL) ? ($rowData[phep_nam_su_dung_trong_thang]) : NULL,
                'tamungphepnew' => (trim($rowData[ung_phep_hoan_phep]) != NULL) ? ($rowData[ung_phep_hoan_phep]) : NULL,
                'phepcty' => (trim($rowData[nghi_viec_rieng_duoc_huong_luong]) != NULL) ? ($rowData[nghi_viec_rieng_duoc_huong_luong]) : NULL,
                'overtime' => (trim($rowData[tong_gio_tang_ca_trong_thang]) != NULL) ? ($rowData[tong_gio_tang_ca_trong_thang]) : NULL
            );
//            debug($data_update); exit;
            $where = $QLogTotalStaffTimeByMonth->getAdapter()->quoteInto('id = ?', $id);
            $update=$QLogTotalStaffTimeByMonth->update($data_update, $where);
//            debug($update); exit;
        }

        $percent = round($row * 70 / count($data_staff_code), 1);
        $progress->flush($percent);

    }// end loop
    $db->commit();
    $progress->flush(100);
    $this->view->success = "Import thành công.";
}
catch (Exception $e) {
    $db->rollback();
    $this->view->errors = $e->getMessage();
    return;
}