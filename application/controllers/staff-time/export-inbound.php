<?php 

$month = $this->getRequest()->getParam('month', date('m'));
$year = $this->getRequest()->getParam('year', date('Y'));
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
$from_date = $year . '-' . $month . '-01';
$to_date = $year . '-' . $month . '-' . $number_day_of_month;
$from_date = $this->getRequest()->getParam('from_date', $from_date);
$to_date = $this->getRequest()->getParam('to_date', $to_date);
$export = $this->getRequest()->getParam('export', 0);

$result = $info = array();
error_reporting(1);
require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
$this->wssURI = 'https://ocare.oppomobile.vn/wss?wsdl';
$this->namespace = 'OPPOVN';

$client = new nusoap_client($this->wssURI);
$client->soap_defencoding = 'UTF-8';
$client->decode_utf8 = true;

$wsParams = array(
    'from_date' =>  $from_date,
    'to_date' =>  $to_date,
);
$result_export = $client->call("getInbound", $wsParams);

$data_export = $result_export['return'];
if(!empty($export)){
    require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'ID',
            $alphaExcel->ShowAndUp() => 'IMEI',
            $alphaExcel->ShowAndUp() => 'Create date',
            $alphaExcel->ShowAndUp() => 'Policy No.',
            $alphaExcel->ShowAndUp() => 'Start date',
            $alphaExcel->ShowAndUp() => 'End date',
            $alphaExcel->ShowAndUp() => 'Full name',
            $alphaExcel->ShowAndUp() => 'Address',
            $alphaExcel->ShowAndUp() => 'Email',
            $alphaExcel->ShowAndUp() => 'Phone number',
            $alphaExcel->ShowAndUp() => 'Province',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Model',
            $alphaExcel->ShowAndUp() => 'Serial Number',
            $alphaExcel->ShowAndUp() => 'Partner',
        );
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        foreach($data_export as $key => $val)
        {
            $alphaExcel = new My_AlphaExcel();
            
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['id']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $val['imei'],PHPExcel_Cell_DataType::TYPE_STRING);
            
//            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['imei']);
                        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($val['purchase_date'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['policyno']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['polstartdate']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['polenddate']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['fullname']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['address']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['email']);
            //$sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['phone_number']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $val['phone_number'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['province']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['area_name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['model']);
//            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['serial_number']);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), empty($val['log_partner_id']) ? $val['serial_number'] : '',PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), !empty($val['log_partner_id']) ? 'FPT' : '',PHPExcel_Cell_DataType::TYPE_STRING);
            

        }

        $filename = 'Policy - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
}
