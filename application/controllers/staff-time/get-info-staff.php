<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$code       = $this->getRequest()->getParam('code');
$email      = $this->getRequest()->getParam('email');
$staff_id   = $this->getRequest()->getParam('staff_id');
$db         = Zend_Registry::get('db');

$QStaff = new Application_Model_Staff();

if(!empty($code)){
    $where = $QStaff->getAdapter()->quoteInto('code = ?', $code);
}elseif (!empty($staff_id)){
    $where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
}

$item = $QStaff->fetchRow($where);

$sql_get_location = $db->prepare("SELECT st.`id`,str.id as 'store_id',st.`code`, str.`latitude`,
str.`longitude`, str.`shipping_address`,
lci.`check_in_day`, lci.`latitude` AS 'latitude_in', lci.`longitude` AS 'longitude_in' 
FROM staff st
INNER JOIN store_staff_log sst ON st.id = sst.`staff_id` AND sst.released_at is null AND sst.is_leader IN (0,3)
INNER JOIN store str ON sst.`store_id` = str.`id`
INNER JOIN log_check_in lci ON st.id = lci.staff_id

WHERE
st.code = :p_code
");
$sql_get_location->bindParam('p_code', My_Util::escape_string($code), PDO::PARAM_STR); 
//$stmt_location = $db->prepare($sql_get_location);
$sql_get_location->execute();
$result_location = $sql_get_location->fetchAll();


echo json_encode($result_location);

