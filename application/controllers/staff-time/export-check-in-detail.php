<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$month = $this->getRequest()->getParam('month', date('m'));
$year = $this->getRequest()->getParam('year', date('Y'));
$name  = $this->getRequest()->getParam('name');
$code  = $this->getRequest()->getParam('code');
$email  = $this->getRequest()->getParam('email');
$area = $this->getRequest()->getParam('area');
$department = $this->getRequest()->getParam('department');
$team = $this->getRequest()->getParam('team');
$title = $this->getRequest()->getParam('title');
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
$from_date = $year . '-' . $month . '-01';
$to_date      = $year . '-' . $month . '-' . $number_day_of_month;
 $params = array(
                'off' => 1,
                'name' => $name,
                'code' => $code,
                'email' => $email,
                'month' => $month,
                'year' => $year,
                'area' => $area,
                'department' => $department,
                'team' => $team,
                'title' => $title,
            );
$QTime = new Application_Model_Time2();
$list_export_staff_id = $QTime->getListStaffById($params);

$arr_export = explode(',', $list_export_staff_id);
$data = array();
// foreach ($arr_export as $value) {
//    $data[]['staff_id'] = $value;
//}
$db           = Zend_Registry::get('db');

//$sql_pre = "TRUNCATE TABLE param_tmp_check_in_detail";
//$stmt_pre = $db->prepare($sql_pre);
//$stmt_pre->execute();
//$stmt_pre = null;

//My_Controller_Action::insertAllrow($data, 'tmp_check_in_detail');

$sql1          = "CALL `split_value_into_multiple_rows`('tmp_check_in_detail', :p_staff_id,1)";
$stmt1 = $db->prepare($sql1);
$stmt1->bindParam('p_staff_id', $list_export_staff_id, PDO::PARAM_STR);
$stmt1->execute();
$stmt1->closeCursor();
$stmt1 =  null;

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$sql          = "CALL `PR_Check_In_Detail`(:from_date, :to_date, :p_staff_id)";

$stmt = $db->prepare($sql);
$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
$stmt->bindParam('p_staff_id', $list_export_staff_id, PDO::PARAM_STR);

$stmt->execute();
$data_export = $stmt->fetchAll();
$stmt->closeCursor();
$stmt =  null;

  set_time_limit(0);
error_reporting(0);
ini_set('memory_limit', -1);
require_once 'PHPExcel.php';

        $alphaExcel = new My_AlphaExcel();

        $PHPExcel = new PHPExcel();
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Họ tên',
            $alphaExcel->ShowAndUp() => 'Mã nhân viên',
            $alphaExcel->ShowAndUp() => 'DEPARTMENT',
            $alphaExcel->ShowAndUp() => 'TEAM',
            $alphaExcel->ShowAndUp() => 'TITLE',
            $alphaExcel->ShowAndUp() => 'Area',
            $alphaExcel->ShowAndUp() => 'Ngày vào làm',
            $alphaExcel->ShowAndUp() => 'Ngày off',
            $alphaExcel->ShowAndUp() => 'Ngày',
            $alphaExcel->ShowAndUp() => 'Giờ vào',
            $alphaExcel->ShowAndUp() => 'Giờ ra',
            $alphaExcel->ShowAndUp() => 'Ngày công',
            $alphaExcel->ShowAndUp() => 'Lý do xác nhận công',
            $alphaExcel->ShowAndUp() => 'Lý do xác nhận đi trễ/về sớm',
            
            
        );
        
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }

        foreach($data_export as $key => $val)
        {
            $alphaExcel = new My_AlphaExcel();
            
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['staff_name']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['staff_code']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['dep']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['team']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['title']);
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['area']);
            
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($val['joined_at'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['off_date']) ? date('d/m/Y', strtotime($val['off_date'])) : '');
            
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($val['date'])));
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['check_in_at'])? date('H:i', strtotime($val['check_in_at'])) : '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['check_out_at'])?date('H:i', strtotime($val['check_out_at'])):'' );
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($val['ngaycong']) ? $val['ngaycong'] : '');
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['temp_reason'] );
            $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $val['time_late_reason']);

            
        }

        $filename = 'Check In Detail - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;