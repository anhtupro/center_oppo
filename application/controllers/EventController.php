<?php

class EventController extends My_Controller_Action {

    public function cultureDayAction() {
        $this->_helper->layout->disableLayout();
        $this->userStorage        = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->current_user = $this->userStorage->id;
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/culture-day');

// echo $url;die;
            $this->_redirect($url);
        }
        $event = new Application_Model_Event();
        $id    = $this->userStorage->id;
        $data  = $event->check_staff($id);
// echo "<pre>";print_r($data);die;


        if (empty($data)) {
            echo '<script>
                		parent.alert("RẤT TIẾC VÌ BẠN ĐÃ KHÔNG THAM GIA ĐƯỢC CULTURE DAY NĂM NAY. CHÚC BẠN CUỐI TUẦN VUI VẺ & HẸN BẠN VÀO NĂM SAU NHÉ!");
            		</script>';
        } else {
            if ($data['status'] == 0 && !$this->_request->isPost()) {
//echo 123; die();
                $this->render('/culture-day');
            } else {
                if ($this->_request->isPost()) {
                    $event->update_status($id);
                }
                $team             = $data['team'];
                $this->view->team = $team;


                $QTeamBuilding = new Application_Model_TeamBuilding();
                $where_r[]     = $QTeamBuilding->getAdapter()->quoteInto('team = ?', intval($data['team']));
                $where_r[]     = $QTeamBuilding->getAdapter()->quoteInto('is_mentor = 1', null);
                $result_mentor = $QTeamBuilding->fetchRow($where_r);





                $color       = $result_mentor['team'];
                $avatar      = $result_mentor['team']; //QR code
                $img_mentor  = $result_mentor['staff_id'] . '/' . $result_mentor['photo']; //avarta này
                $name_mentor = $result_mentor['full_name'];

//                switch ($team) {
//                    case 1:
//                        $color       = '1';
//                        $avatar      = 'red';
//                        $img_mentor  = '/event/img/mentor/red.jpg';
//                        $name_mentor = "TRẦN NGỌC THUẬN";
//                        break;
//                    case 2:
//                        $color       = '2';
//                        $avatar      = 'orange';
//                        $img_mentor  = '/event/img/mentor/organe.jpg';
//                        $name_mentor = " LÊ THÀNH NGHĨA";
//                        break;
//                }

                $this->view->color       = $color;
                $this->view->avatar      = $avatar;
                $this->view->name_mentor = $name_mentor;
                $this->view->img_mentor  = $img_mentor;
                $info                    = $event->get_data_team($team);

                $this->view->mentor = $data['is_mentor'];


                $this->view->info = $info;
                $this->render('/team-building-info');
            }
        }
    }

    public function companyTripAction() {
        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QTeamBuilding = new Application_Model_TeamBuilding();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/company-trip');
            $this->_redirect($url);
        }
        $event = new Application_Model_Event();
        $id    = $this->userStorage->id;
        $data  = $event->check_staff($id);

        if (empty($data)) {
            echo '<script>
                         parent.alert("RẤT TIẾC VÌ BẠN ĐÃ KHÔNG CÓ TRONG DANH SÁCH THAM GIA COMPANY TRIP KHỐI H.O NĂM NAY");
            		</script>';
        }
        
        // partner room
        $where = [];
        $where[]      = $QTeamBuilding->getAdapter()->quoteInto('room = ?', intval($data['room']));
        $where[]      = $QTeamBuilding->getAdapter()->quoteInto('staff_id <> ?', $data['staff_id']);
        $result_partner = $QTeamBuilding->fetchAll($where);
        $result_partner = $result_partner->toArray();
        if (!empty($result_partner)) {
            $this->view->partner = $result_partner;
        }
        
        //end partner room
        // get is mentor
        $team                   = $data['team'];
        $where_r[]     = $QTeamBuilding->getAdapter()->quoteInto('team = ?', intval($data['team']));
        $where_r[]     = $QTeamBuilding->getAdapter()->quoteInto('is_mentor = 1', null);
        $result_mentor = $QTeamBuilding->fetchRow($where_r);

        $color                   = $result_mentor['team'];
        $avatar                  = $result_mentor['team']; //QR code
        $img_mentor              = $result_mentor['staff_id'] . '/' . $result_mentor['photo']; //avarta này
        $name_mentor             = $result_mentor['full_name'];
        
        $this->view->mentor      = $data['is_mentor'];
        $this->view->staff_data = $data;
        $this->view->color       = $color;
        $this->view->avatar      = $avatar;
        $this->view->name_mentor = $name_mentor;
        $this->view->img_mentor  = $img_mentor;
        //end get is mentor
        $info                    = $event->get_data_team($team);
        $this->view->info        = $info;
        $this->render('/company-trip-info');
    }

    public function companyTripServicePhuQuocAction() {
        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/company-trip');
            $this->_redirect($url);
        }
        $QTeamBuildingServicePQ = new Application_Model_TeamBuildingServicePQ();
        $id                     = $this->userStorage->id;

        $data = $QTeamBuildingServicePQ->check_staff_service($id);

        if (empty($data)) {
            echo '<script>
                         parent.alert(" RẤT TIẾC VÌ BẠN ĐÃ KHÔNG CÓ TRONG DANH SÁCH THAM GIA COMPANY TRIP ĐỢT NÀY \nVui lòng liên hệ Mr. Hoàng (HR) - 0939393054 nếu có sự nhầm lẫn");
            		</script>';
        }
        $team                   = $data['team'];
        $this->view->team       = $team;
        $this->view->staff_data = $data;

        $where_r[]     = $QTeamBuildingServicePQ->getAdapter()->quoteInto('team = ?', intval($data['team']));
        $where_r[]     = $QTeamBuildingServicePQ->getAdapter()->quoteInto('is_mentor = 1', null);
        $result_mentor = $QTeamBuildingServicePQ->fetchRow($where_r);

        $where_p[]      = $QTeamBuildingServicePQ->getAdapter()->quoteInto('room = ?', intval($data['room']));
        $where_p[]      = $QTeamBuildingServicePQ->getAdapter()->quoteInto('staff_id <> ?', $data['staff_id']);
        $result_partner = $QTeamBuildingServicePQ->fetchRow($where_p);
        if (!empty($result_partner)) {
            $this->view->partner = $result_partner;
        }


        $color                   = $result_mentor['team'];
        $avatar                  = $result_mentor['team']; //QR code
        $img_mentor              = $result_mentor['staff_id'] . '/' . $result_mentor['photo']; //avarta này
        $name_mentor             = $result_mentor['full_name'];
//        $bus_number             = $result_mentor['bus_number'];
        $bus_number              = $data['bus_number'];
        $this->view->color       = $color;
        $this->view->avatar      = $avatar;
        $this->view->name_mentor = $name_mentor;
        $this->view->img_mentor  = $img_mentor;
        $info                    = $QTeamBuildingServicePQ->get_data_team($team);

        $this->view->mentor     = $data['is_mentor'];
        $this->view->info       = $info;
        $this->view->bus_number = $bus_number;

        $this->render('/company-trip-service-pq');
    }

    public function oppoerAction() {
        $week              = 6;
        $flashMessenger    = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();
//          $this->render('/team-building-info');
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/oppoer');
            $this->_redirect($url);
        }

        $game_mode                    = new Application_Model_OppoerMiniGame();
        $where                        = [];
        $where[]                      = $game_mode->getAdapter()->quoteInto('week = ?', $week);
        $users                        = $game_mode->fetchAll($where);
        $this->view->count_user       = count($users);
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;
    }

    public function oppoAction() {
        $flashMessenger               = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;
    }

    public function oppoerPlayAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $user_id        = $userStorage->id;
        if (!in_array($user_id, array(5899, 7671, 6705))) {
            $flashMessenger->setNamespace('error')->addMessage("Đã hết thời gian tham gia.Vui lòng quay lại vào tuần sau nhé!");
            $this->_redirect(HOST . 'event/oppoer');
        }
        $this->view->start = date('Y-m-d H:i:s');

        $week = 6;
        $this->_helper->layout->disableLayout();

//          $this->render('/team-building-info');
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/oppoer');
            $this->_redirect($url);
        }
        $game_mode = new Application_Model_OppoerMiniGame();
        $where     = [];
        $where[]   = $game_mode->getAdapter()->quoteInto('staff_id = ?', $this->userStorage->id);
        $where[]   = $game_mode->getAdapter()->quoteInto('week = ?', $week);
        $user      = $game_mode->fetchRow($where);
        if (!empty($user)) {
            $flashMessenger->setNamespace('error')->addMessage("Bạn đã tham gia Mini Game tuần này rồi");
            $this->_redirect(HOST . 'event/oppoer');
        }

        if ($this->getRequest()->getMethod() == 'POST') {

            $list_answer = $this->getRequest()->getParam('question');
            $start_time  = $this->getRequest()->getParam('start_time');

            $response       = array('status' => 0, 'message' => 'Câu trả lời không hợp lệ');
            $total_score    = 0;
            $answer_model   = new Application_Model_OppoerAnswers();
            $minigame_model = new Application_Model_OppoerMiniGame();
            $answers        = $answer_model->get_cache();
            $wrong_answer   = [];
            if (!empty($list_answer)) {
                foreach ($list_answer as $key => $value) {
                    if ($value['answer'] == $answers[$key]) {
                        $total_score += 1;
                    } else {
                        $wrong_answer[] = $key;
                    }
                }
                $data_insert = array(
                    'staff_id'     => $this->userStorage->id,
                    'week'         => $week,
                    'answer'       => json_encode($list_answer),
                    'score'        => $total_score,
                    'wrong_answer' => json_encode($wrong_answer),
                    'created_at'   => date('Y-m-d H:i:s'),
                    'start_at'     => $start_time
                );
                $id          = $minigame_model->insert($data_insert);
                if ($total_score == 10) {
                    $this->_redirect(HOST . 'event/oppoer-gift');
                } else {
                    $this->_redirect(HOST . 'event/oppoer-result');
                }
            }
            echo json_encode($response);
            exit();
        }
    }

    public function oppoerResultAction() {
        $week              = 6;
        $flashMessenger    = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/oppoer');
            $this->_redirect($url);
        }
        $answer_model   = new Application_Model_OppoerAnswers();
        $minigame_model = new Application_Model_OppoerMiniGame();
        $answers        = $answer_model->get_cache();
        $where          = [];
        $where[]        = $minigame_model->getAdapter()->quoteInto('staff_id = ?', $this->userStorage->id);
        $where[]        = $minigame_model->getAdapter()->quoteInto('week = ?', $week);
        $user           = $minigame_model->fetchRow($where);
        if (empty($user)) {
            $flashMessenger->setNamespace('error')->addMessage("Chưa tham gia mini game tuần này");
            $this->_redirect(HOST . 'event/oppoer');
        }
        $list_wrong = json_decode($user['wrong_answer']);
        if (empty($list_wrong)) {
            $flashMessenger->setNamespace('success')->addMessage("Bạn đã hoàn thành 10/10 câu hỏi mini game tuần này");
            $this->_redirect(HOST . 'event/oppoer');
        }
        $list = [];
        foreach ($list_wrong as $key => $value) {
            $list[] = array(
                'question_id' => $value,
                'aw'          => $answers[$value]
            );
        }
        $this->view->list_question = $list;
    }

    public function oppoerGiftAction() {
        $week              = 6;
        $flashMessenger    = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $minigame_model    = new Application_Model_OppoerMiniGame();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/oppoer');
            $this->_redirect($url);
        }
        $where       = [];
        $where[]     = $minigame_model->getAdapter()->quoteInto('staff_id = ?', $this->userStorage->id);
        $where[]     = $minigame_model->getAdapter()->quoteInto('week = ?', $week);
        $user        = $minigame_model->fetchRow($where);
//        if (empty($user) || $user['score'] < 10) {
//            $flashMessenger->setNamespace('error')->addMessage("Bạn không đủ điều kiện để quay thưởng");
//            $this->_redirect(HOST . 'event/oppoer');
//        }
        $log         = new Application_Model_OppoerMiniGameLog();
        $where_log   = [];
        $where_log[] = $log->getAdapter()->quoteInto('staff_id = ?', $this->userStorage->id);
        $where_log[] = $log->getAdapter()->quoteInto('week = ?', $week);
        $user_log    = $log->fetchRow($where);
        if (!empty($user_log)) {
            $flashMessenger->setNamespace('error')->addMessage("Bạn đã quay thưởng rồi, vui lòng trở lại vào tuần sau.");
            $this->_redirect(HOST . 'event/oppoer');
        }
        if ($this->getRequest()->getMethod() == 'POST') {
            $response = array('status' => 0, 'message' => 'Lỗi dữ liệu không đúng');
            $reward   = $this->getReward();

            if (!empty($reward)) {
                $data_insert         = array(
                    'staff_id'   => $this->userStorage->id,
                    'item_id'    => $reward['id'],
                    'item_name'  => $reward['name'],
                    'week'       => $week,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $log->insert($data_insert);
                $response['status']  = 1;
                $response['message'] = "Thành công";
                $response['item_id'] = $reward['id'];
                if (!empty($reward['refresh'])) {
                    $this->generateReward();
                }
            }
            echo json_encode($response);
            exit();
        }
    }

    public function oppoerCreateAction() {


        $this->generateReward();
        $cache        = Zend_Registry::get('cache');
        $cache_reward = $cache->load('oppoer_reward');
        echo "<pre>";
        print_r($cache_reward);
        die;
    }

    public function checkrewardAction() {
//        $week         = 6;
        $params       = array(
            'round' => MINIGAME_ROUND
        );
        $log_mode     = new Application_Model_MiniGameRewardLog();
        $log          = $log_mode->getLogRewardOfItem($params);
        echo "<pre>";
        print_r($log);
        $cache        = Zend_Registry::get('cache');
        $cache_reward = $cache->load('minigame_reward');
        echo "<pre>";
        print_r($cache_reward);
        die;
    }

    private function changeKey($models, $key) {
        $result = [];
        if (count($models) > 0)
            foreach ($models as $model) {
                $result[$model[$key]] = $model;
            }
        return $result;
    }

    public function generateReward() {
        $week    = 6;
//Reward
        $_reward = array(
            array(
                "id"      => "9",
                "name"    => "Ollie gà nhỏ",
                "percent" => 10,
                "limit"   => 15
            ),
            array(
                "id"      => "3",
                "name"    => "Sạc dự phòng TP709",
                "percent" => 8,
                "limit"   => 12
            ),
            array(
                "id"      => "7",
                "name"    => "Gậy tự sướng ZP105",
                "percent" => 8,
                "limit"   => 12
            ),
            array(
                "id"      => "6",
                "name"    => "Balo Barcelona",
                "percent" => 4,
                "limit"   => 4
            ),
            array(
                "id"      => "1",
                "name"    => "Tai nghe Bluetooth SH1",
                "percent" => 1,
                "limit"   => 2
            ),
            array(
                "id"      => "4",
                "name"    => "Mic Karaoke H17",
                "percent" => 1,
                "limit"   => 1
            ),
            array(
                "id"      => "2",
                "name"    => "Chúc may mắn lần sau",
                "percent" => 50,
                "limit"   => -1
            ),
        );

        $cache_reward = array();
        $cache        = Zend_Registry::get('cache');
        $reward_array = $_reward;
// $limit_array = $cache->load('oppoer_limit');
        $log_mode     = new Application_Model_OppoerMiniGameLog();
        $log          = $log_mode->getLogByItem($week);
        if (empty($log)) {
            $log2 = [];
        } else {
            $log2 = $this->changeKey($log, "item_id");
        }
        $limit_array = $log2;
        if (!empty($reward_array)) {
            foreach ($reward_array as $reward) {
                if (!isset($limit_array[$reward["id"]]) || $reward["limit"] == -1 || $limit_array[$reward["id"]]['count'] < $reward["limit"]) {
                    for ($i = 0; $i < $reward["percent"]; $i++) {
                        $cache_reward[] = $reward;
                    }
                }
            }
        }
        if (!empty($cache_reward)) {
            shuffle($cache_reward);
            $cache->save($cache_reward, "oppoer_reward", array(), null);
//          echo "success";
        }
        return true;
    }

    protected function getReward() {
        $week         = 6;
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
        $cache        = Zend_Registry::get('cache');
        $cache_reward = $cache->load('oppoer_reward');
//       $cache_limit = $cache->load('oppoer_limit');
        $reward       = $cache_reward[rand(0, count($cache_reward) - 1)];

        $log_mode = new Application_Model_OppoerMiniGameLog();
        $log      = $log_mode->getLogByItem($week);
        if (empty($log)) {
            $log2 = [];
        } else {
            $log2 = $this->changeKey($log, "item_id");
        }
        $cache_limit = $log2;
        if (isset($cache_limit[$reward["id"]])) {
            $cache_limit[$reward["id"]]['count'] += 1;
        } else {
            $cache_limit[$reward["id"]] = array('count' => 1, 'item_id' => $reward["id"]);
        }
//Set lai reward neu da dat limit
//        if (!in_array($userStorage->title, array(182, 293, 419, 420)) && $reward['limit'] != -1) {
        if (in_array($userStorage->team, array(141))) {
//            if ($cache_limit[$reward["id"]]['count'] >= (intval($reward['limit']) / 2)) {
            $reward = array(
                "id"      => "2",
                "name"    => "Chúc may mắn lần sau",
                "percent" => 50,
                "limit"   => -1
            );
//            }
        }
        if (in_array($userStorage->id, array(9823))) {
//            if ($cache_limit[$reward["id"]]['count'] >= (intval($reward['limit']) / 2)) {
            $reward = array(
                "id"      => "6",
                "name"    => "Balo Barcelona",
                "percent" => 4,
                "limit"   => 5
            );
//            }
        }

        if ($reward["limit"] != -1 && $cache_limit[$reward["id"]]['count'] >= $reward["limit"]) {
            $reward['refresh'] = 1;
            $this->generateReward();
        }

        return $reward;
    }

    public function lunarNewYear2018Action() {

//        echo "Đã hết thời gian đăng ký, hệ thống đã khóa. !!"; die();

        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/lunar-new-year2018');
            $this->_redirect($url);
        }
        $lunar_model       = new Application_Model_Lunar2018();
        $code              = $this->userStorage->code;
        $data              = $lunar_model->check_staff($code);
        $province_district = new Application_Model_ProvinceDistrict();
//                $rm_mode= new Application_Model_RegionalMarket();
        $list_pro          = $province_district->get_cache();
        asort($list_pro);

        $this->view->list_pro    = $list_pro;
        $this->view->userStorage = $this->userStorage;
        if (empty($data)) {
            $this->render('/not-join');
        } else {

            if ($data['status'] == 1 && FALSE) {
                $list_district             = $province_district->get_district_by_province($data['province']);
                $this->view->list_district = $list_district;
                $this->view->info          = $data;

                $this->render('/show-record');
            } else {
//                $province_mode= new Application_Model_Province();

                $fullname     = $this->getRequest()->getParam('fullname');
                $relationship = $this->getRequest()->getParam('relationship');
                $province     = $this->getRequest()->getParam('province');
                $district     = $this->getRequest()->getParam('district');
                $address      = $this->getRequest()->getParam('address');
                $re_number    = $this->getRequest()->getParam('re-number');
                $se_number    = $this->getRequest()->getParam('se-number');
                if (!empty($fullname) && !empty($relationship) && !empty($province) && !empty($district) && !empty($address) && !empty($re_number) && !empty($se_number)) {
                    $data_insert = array(
                        "fullname"     => $fullname,
                        "relationship" => $relationship,
                        "province"     => $province,
                        "district"     => $district,
                        "address"      => $address,
                        "phone"        => $re_number,
                        "staff_phone"  => $se_number,
                        "status"       => 1,
                    );
                    $where       = $lunar_model->getAdapter()->quoteInto('code =?', $this->userStorage->code);
                    $lunar_model->update($data_insert, $where);
                    $this->render('/lunar-confirm-success');
                } else {
                    $this->render('/create-record');
                }
            }
        }
    }

    public function lunarNewYear2019Action() {

//        echo "Đã hết thời gian đăng ký, hệ thống đã khóa. !!"; die();

        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/lunar-new-year2019');
            $this->_redirect($url);
        }
        $lunar_model       = new Application_Model_Lunar2018();
        $code              = $this->userStorage->code;
        $data              = $lunar_model->check_staff($code);
        $province_district = new Application_Model_ProvinceDistrict();
        $wardmode          = new Application_Model_Ward();
        $list_pro          = $province_district->get_cache();
        asort($list_pro);

        $this->view->list_pro    = $list_pro;
        $this->view->userStorage = $this->userStorage;
        if (empty($data)) {
            $this->render('/not-join');
        } else {
            if ($data['status'] == 1) {
                $list_district             = $province_district->get_district_by_province($data['province']);
                $this->view->list_district = $list_district;
                $listward                  = $wardmode->get_cache_ward_delivery();

                $this->view->list_ward = $listward;
                $this->view->info      = $data;

                $this->render('/show-record');
//                $this->render('/lunar-confirm-success');
            } else {
//                $province_mode= new Application_Model_Province();

                $fullname     = $this->getRequest()->getParam('fullname');
                $relationship = $this->getRequest()->getParam('relationship');
                $province     = $this->getRequest()->getParam('province');
                $district     = $this->getRequest()->getParam('district');
                $ward         = $this->getRequest()->getParam('ward');
                $address      = $this->getRequest()->getParam('address');
                $re_number    = $this->getRequest()->getParam('re-number');
                $se_number    = $this->getRequest()->getParam('se-number');
                if (!empty($fullname) && !empty($relationship) && !empty($province) && !empty($district) && !empty($address) && !empty($re_number) && !empty($se_number)) {
                    $data_insert = array(
                        "fullname"     => $fullname,
                        "relationship" => $relationship,
                        "province"     => $province,
                        "district"     => $district,
                        'ward'         => $ward,
                        "address"      => $address,
                        "phone"        => $re_number,
                        "staff_phone"  => $se_number,
                        "status"       => 1,
                    );
                    $where       = $lunar_model->getAdapter()->quoteInto('code =?', $this->userStorage->code);
                    $lunar_model->update($data_insert, $where);
                    $this->render('/lunar-confirm-success');
                } else {
                    $this->render('/create-record');
                }
            }
        }
    }

    public function yearEndCheckInAction() {
        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $id             = $this->getRequest()->getParam('id');
        $year_end_model = new Application_Model_EndYear();
        $visitor_info   = [];
        if (!empty($id)) {
            $id = base64_decode($id);
            if ($this->getRequest()->getMethod() == 'POST') {
                $data_update = ['status' => 1, 'update_at' => date('y-m-d H:i:s')];
                $where       = $year_end_model->getAdapter()->quoteInto('id =?', $id);
                $year_end_model->update($data_update, $where);
            }
            $visitor_info = $year_end_model->get_staff($id);
            if (!empty($visitor_info)) {
                $this->view->info = $visitor_info;
                $this->render('year-end/view-info');
            } else {
                $this->render('year-end/not-join');
            }
        } else {
            $this->render('year-end/not-join');
        }
    }

    public function genqrAction() {
        echo '<pre>';
        print_r('Die');
        echo '</pre>';
        exit();
        $year_end_model = new Application_Model_EndYear();
        $data           = $year_end_model->fetchAll()->toArray();
        foreach ($data as $key => $value) {
            $paramsQRCODE = array(
                'sn' => $value['id']
            );
            $url          = My_Image_Barcode::renderQR($paramsQRCODE);
        }
        echo "<pre>";
        print_r("Xong");
        die;
    }

    private $hr_signature = '<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" style="font-family: &quot;segoe ui&quot;, &quot;lucida sans&quot;, sans-serif; border-collapse: collapse;">'
            . '<tbody style="">'
            . '<tr style="height: 29.4pt;">'
            . '<td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 29.4pt;">'
            . '<p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;">'
            . '<span style="font-family: arial, sans-serif; color: rgb(4,106,56);">HR Department<br style=""></span>'
            . '<span style="font-size: 10pt; font-family: arial, sans-serif;">Giá trị cốt lõi – “Bổn phận cao hơn chữ tín”</span>'
            . '<span style="font-family: arial, sans-serif; color: rgb(4,106,56);"></span>'
            . '</p>'
            . '</td>'
            . '</tr>'
            . '<tr style="height: 37.5pt;">'
            . '<td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 37.5pt;">'
            . '<p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;">'
            . '<span style="font-size: 10pt; font-family: arial, sans-serif;">T:&nbsp;'
            . '<span class="Object" id="OBJ_PREFIX_DWT357_com_zimbra_phone" style="color: rgb(0, 90, 149); cursor: pointer;">'
            . '<a href="callto:(84-8) 3920 2555" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;" target="_blank">(84-8) 3920 2555</a></span>&nbsp;- Ext: 111</span></p><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><span style="font-size: 10pt; font-family: arial, sans-serif;">F:&nbsp;<span class="Object" id="OBJ_PREFIX_DWT358_com_zimbra_phone" style="color: rgb(0, 90, 149); cursor: pointer;"><a href="callto:(84-8) 3920 4095" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;" target="_blank">(84-8) 3920 4095</a></span><br style="">E:&nbsp;<span class="Object" id="OBJ_PREFIX_DWT359_ZmEmailObjectHandler" style="color: rgb(0, 90, 149); cursor: pointer;"><span class="Object" role="link" id="OBJ_PREFIX_DWT45_ZmEmailObjectHandler">hr@oppo-aed.vn</span></span></span></p></td></tr><tr style="height: 38.9pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><b style=""><span style="font-size: 10pt; font-family: arial, sans-serif;color: rgb(4,106,56);">OPPO Authorized Exclusive Distributor CÔNG TY CỔ PHẦN KỸ THUẬT & KHOA HỌC VĨNH KHANG</span></b><span style="font-size: 10pt; font-family: arial, sans-serif;"><br style="">12<sup style="">th</sup><b style="">&nbsp;</b>Floor, Lim II Tower<br style="">No. 62A CMT8 St., Dist. 3, HCMC, VN</span></p></td></tr><tr style="height: 16.5pt;"><td width="374" style="width: 3.9in; padding: 0in 5.4pt; height: 16.5pt;"><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><span style="color: rgb(31, 73, 125);"><span class="Object" id="OBJ_PREFIX_DWT360_com_zimbra_url" style="color: rgb(0, 90, 149); cursor: pointer;"><span class="Object" role="link" id="OBJ_PREFIX_DWT46_com_zimbra_url"><a href="http://www.oppo.com/vn/" target="_blank" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;"><span style="font-size: 10pt; font-family: arial, sans-serif; color: rgb(4,106,56);">www.oppo.com/vn</span></a></span></span></span></p></td></tr></tbody></table>';

    public function sendqrAction() {
        echo '<pre>';
        print_r('Die');
        echo '</pre>';
        exit();
        $year_end_model = new Application_Model_EndYear();
        $data           = $year_end_model->fetchAll()->toArray();
        $app_config     = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config         = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );
        $transport      = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);
        foreach ($data as $key => $value) {
            if (empty($value['send_qr_status']) && !empty($value['email'])) {
                $link_qr = HOST . 'photo/qrcode-2020-final/' . $value['id'] . '.jpg';
                $link_qr = trim($link_qr);
                $mail    = new My_Mail($app_config->mail->smtp->charset);
                $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);
                $mail->setSubject('[YEP OPPO 2020] SƠ ĐỒ CHỖ NGỒI & CHECK IN');

                $at2              = $mail->createAttachment(file_get_contents(HOST.'img/year_end_2020/layout.png'));
                $at2->type        = 'image/jpeg';
                $at2->disposition = Zend_Mime::DISPOSITION_INLINE;
                $at2->encoding    = Zend_Mime::ENCODING_BASE64;
                $at2->filename    = 'SO_DO_CHO_NGOI.png';
                $at2->id          = md5('yelo');


                $at              = $mail->createAttachment(file_get_contents($link_qr));
                $at->type        = 'image/jpeg';
                $at->disposition = Zend_Mime::DISPOSITION_INLINE;
                $at->encoding    = Zend_Mime::ENCODING_BASE64;
                $at->filename    = $value['code'] . '.jpg';
                $at->id          = md5($value['email']);
                $mai_body        = '<div class="WordSection1">';
                $mai_body .= '<p class="MsoNormal"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Chào anh/chị <b>' . $value['name'] . '</b>,</span></p>
                <p class="MsoNormal"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">
                        Như vậy chỉ còn vài ngày nữa thôi sẽ đến chương trình được mong chờ nhất trong năm “YEAR END PARTY 2020”. Và để <span class="Object" role="link" id="OBJ_PREFIX_DWT577_com_zimbra_date"><span class="Object" role="link" id="OBJ_PREFIX_DWT582_com_zimbra_date">thu</span></span>ận tiện cho các bạn, BTC xin thông báo lại thời gian cũng như gửi mã QR check in để vào chương trình.</span></p>
                <ul style="margin-top: 0cm;" type="disc">
                    <li class="MsoListParagraph" style="margin-left: 0cm;"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Thời gian: </span></li>
                    <ul style="margin-top: 0cm;" type="circle"><li class="MsoListParagraph" style="color: rgb(248, 24, 24); margin-left: 0cm;"><b><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Đón khách & Tham dự các trò chơi dân gian: 16h30 </span></b></li>
                        <li class="MsoListParagraph" style="color: rgb(248, 24, 24); margin-left: 0cm;"><b><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Gala Dinner: 18h00</span></b></li>
                    </ul>
                </ul>
                <ul style="margin-top: 0cm;" type="disc">
                    <li class="MsoListParagraph" style="margin-left: 0cm;"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Timeline sơ bộ: </span></li>
                    <ul style="margin-top: 0cm;" type="circle">
                        <li class="MsoListParagraph" style="color: rgb(248, 24, 24); margin-left: 0cm;"><b><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Check-in nhận số bốc thăm may mắn</span></b></li>
                        <li class="MsoListParagraph" style="color: rgb(248, 24, 24); margin-left: 0cm;"><b><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Tham gia tự do các trò chơi dân gian đổi quà </span></b></li>
                        <li class="MsoListParagraph" style="color: rgb(248, 24, 24); margin-left: 0cm;"><b><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Tiệc Tất Niên </span></b></li>
                        <li class="MsoListParagraph" style="color: rgb(248, 24, 24); margin-left: 0cm;"><b><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Minigame sân khấu - Bốc thăm may mắn</span></b></li>
                    </ul>
                </ul>
                <ul style="margin-top: 0cm;" type="disc">
                    <li class="MsoListParagraph" style="margin-left: 0cm;"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Mã QR đã được gửi trong file đính kèm<i>(Anh/Chị vui lòng chụp lại màn hình QR code check in để nhận door gift và số bốc thăm may mắn)</i> </span>
                    <p class="MsoNormal"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;"><img width="200px" height="200px" style="width: 120px; height: 120px;"  src= "' . $link_qr . '"></span><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;"></span></p>
                    </li>
                </ul>
                <ul style="margin-top: 0cm;" type="disc"><li class="MsoListParagraph" style="margin-left: 0cm;"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Ngoài ra, Anh/Chị có thể xem trước vị trí ngồi của team mình trong sơ đồ bàn tiệc ở file đính kèm</span></li> 
                </ul>
                
                <ul style="margin-top: 0cm;" type="disc">
                    <li class="MsoListParagraph" style="margin-left: 0cm;"><span lang="VI" style="font-size: 11pt; font-family: arial, sans-serif;">Hẹn gặp lại cả nhà vào Chủ nhật tuần này (31/01/2021) tại Nikko Garden!</span></li> 
                </ul>
                <img style="width: 350px;"  src= "' . HOST . 'img/year_end_2020/banner_yeb_2020.jpg">';
                $mai_body .= '</div>';
                $mai_body .= $this->hr_signature;
                $mail->setBodyHtml($mai_body);
                $mailTo      = $value['email'];
                $mail->addTo($mailTo);
                $r           = $mail->send($transport);
                $data_update = ['send_qr_status' => 1];
                $where       = $year_end_model->getAdapter()->quoteInto('id =?', $value['id']);
                $year_end_model->update($data_update, $where);
            }
        }
        echo "<pre>";
        print_r('Xong all');
        die;
    }

    public function companyTripServiceQuyNhonAction() {
        $this->_helper->layout->disableLayout();
        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (empty($this->userStorage)) {
            $url = HOST . 'user/login?b=' . urlencode(HOST . 'event/company-trip');
            $this->_redirect($url);
        }
        $QTeamBuildingServiceQN = new Application_Model_TeamBuildingServiceQN();
        $id                     = $this->userStorage->id;

        $data = $QTeamBuildingServiceQN->check_staff_service($id);

        if (empty($data)) {
            echo '<script>
                         parent.alert(" RẤT TIẾC VÌ BẠN ĐÃ KHÔNG CÓ TRONG DANH SÁCH THAM GIA COMPANY TRIP ĐỢT NÀY \nVui lòng liên hệ Mr. Hoàng (HR) - 0939393054 nếu có sự nhầm lẫn");
            		</script>';
        }
        $team                   = $data['team'];
        $this->view->team       = $team;
        $this->view->staff_data = $data;

        $where_r[]     = $QTeamBuildingServiceQN->getAdapter()->quoteInto('team = ?', intval($data['team']));
        $where_r[]     = $QTeamBuildingServiceQN->getAdapter()->quoteInto('is_mentor = 1', null);
        $result_mentor = $QTeamBuildingServiceQN->fetchRow($where_r);

        $where_p[]      = $QTeamBuildingServiceQN->getAdapter()->quoteInto('room = ?', intval($data['room']));
        $where_p[]      = $QTeamBuildingServiceQN->getAdapter()->quoteInto('staff_id <> ?', $data['staff_id']);
        $result_partner = $QTeamBuildingServiceQN->fetchRow($where_p);
        if (!empty($result_partner)) {
            $this->view->partner = $result_partner;
        }


        $color                   = $result_mentor['team'];
        $avatar                  = $result_mentor['team']; //QR code
        $img_mentor              = $result_mentor['staff_id'] . '/' . $result_mentor['photo']; //avarta này
        $name_mentor             = $result_mentor['full_name'];
//        $bus_number             = $result_mentor['bus_number'];
        $bus_number              = $data['bus_number'];
        $this->view->color       = $color;
        $this->view->avatar      = $avatar;
        $this->view->name_mentor = $name_mentor;
        $this->view->img_mentor  = $img_mentor;
        $info                    = $QTeamBuildingServiceQN->get_data_team($team);
        $this->view->mentor      = $data['is_mentor'];
        $this->view->info        = $info;
        $this->view->bus_number  = $bus_number;

        $this->render('/company-trip-service-qn');
    }

}
