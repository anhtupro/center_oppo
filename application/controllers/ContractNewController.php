<?php

class ContractNewController extends My_Controller_Action {

//    public function indexAction() {
//        $this->_helper->viewRenderer->setNoRender();
//    }
//
//    public function contractTermAction() {
//        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'contract-term.php';
//    }
//
//    public function contractTermCreateAction() {
//        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'contract-term-create.php';
//    }
//
//    public function contractTermSaveAction() {
//        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'contract-term-save.php';
//    }
//
//    public function contractProcessAction() {
//        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'contract-process.php';
//    }
//
//    public function contractProcessCreateAction() {
//        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'contract-process-create.php';
//    }
//
//    public function contractProcessSaveAction() {
//        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'contract-process-save.php';
//    }
//
//    public function contractProposalAction() {
//        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'contract-proposal.php';
//    }
//
//    //PLHĐ do điều chỉnh lương
//    public function appendixContractAction() {
//        
//    }
//
//    //Tái ký hợp đồng hằng tháng
//    public function reSigningAction() {
//        
//    }
//
    //Ký hợp đồng mới
    public function newSignContractAction() {

        require_once 'contract-new' . DIRECTORY_SEPARATOR . 'new-sign-contract.php';
    }

//
//    //HĐ theo transfer
//    public function contractByTransferAction() {
//        
//    }
//
//    public function exportSalaryAction() {
//        
//    }
//
//    public function importSalaryAction() {
//        
//    }
//
//    //Lịch sử HĐ
//    public function staffContractAction() {
//        
//    }
//
//    //Tình trạng xác nhận
//    public function areaApproveAction() {
//        
//    }
//
//    public function testAction() {
//        
//    }
//
//    public function contractTermDisiableAction() {
//        $QModel = new Application_Model_ContractTerm();
//
//        $id = $this->getRequest()->getParam('id');
//
//        $data = array(
//            'status' => 0
//        );
//
//        if ($id) {
//            $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
//
//            $QModel->update($data, $where);
//
//            $flashMessenger = $this->_helper->flashMessenger;
//            $flashMessenger->setNamespace('success')->addMessage('Done!');
//        }
//
//        $back_url = $this->getRequest()->getParam('back_url');
//
//        $this->_redirect(( $back_url ? $back_url : HOST . 'contract-new/contract-term'));
//    }
//
//    public function contractProcessDisiableAction() {
//        $QModel = new Application_Model_ContractProcess();
//
//        $id = $this->getRequest()->getParam('id');
//
//        $data = array(
//            'status' => 0
//        );
//
//        if ($id) {
//            $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
//
//            $QModel->update($data, $where);
//
//            $flashMessenger = $this->_helper->flashMessenger;
//            $flashMessenger->setNamespace('success')->addMessage('Done!');
//        }
//
//        $back_url = $this->getRequest()->getParam('back_url');
//
//        $this->_redirect(( $back_url ? $back_url : HOST . 'contract-new/contract-process'));
//    }

    public function historyAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();

        $db           = Zend_Registry::get('db');
        $export       = $this->getRequest()->getParam('export');
        $name         = $this->getRequest()->getParam('name', '');
        $code         = $this->getRequest()->getParam('code', '');
        $office       = $this->getRequest()->getParam('office', -1);
        $expired_from = $this->getRequest()->getParam('expired_from');
        $expired_to   = $this->getRequest()->getParam('expired_to');

        $signed_from        = $this->getRequest()->getParam('signed_from', '');
        $signed_to          = $this->getRequest()->getParam('signed_to', '');
        $print_type         = $this->getRequest()->getParam('print_type');
        $area_id            = $this->getRequest()->getParam('area_id', NULL);
        $department         = $this->getRequest()->getParam('department', NULL);
        $team               = $this->getRequest()->getParam('team', NULL);
        $title              = $this->getRequest()->getParam('title', NULL);
        $off                = $this->getRequest()->getParam('off', 0);
        $contract_term      = $this->getRequest()->getParam('contract_term', NULL);
        $company_id         = $this->getRequest()->getParam('company_id', NULL);
        $return_letter_from = $this->getRequest()->getParam('return_letter_from');
        $return_letter_to   = $this->getRequest()->getParam('return_letter_to');
        $send_letter_from   = $this->getRequest()->getParam('send_letter_from');
        $send_letter_to     = $this->getRequest()->getParam('send_letter_to');
        $return_letter      = $this->getRequest()->getParam('return_letter');
        $is_upload_file     = $this->getRequest()->getParam('is_upload_file', 0);

        $set_date      = $this->getRequest()->getParam('set_date', date('d/m/Y'));
        $btnSetDate    = $this->getRequest()->getParam('btnSetDate');
        $ids           = $this->getRequest()->getParam('id');
        $btnSetDisable = $this->getRequest()->getParam('btnSetDisable');
        $btnRollback   = $this->getRequest()->getParam('btn_rollback');

        $params = array(
            'name'               => trim($name),
            'code'               => trim($code),
            'expired_from'       => $expired_from,
            'expired_to'         => $expired_to,
            'signed_from'        => $signed_from,
            'signed_to'          => $signed_to,
            'print_type'         => $print_type,
            'office'             => $office,
            'area_id'            => $area_id,
            'department'         => $department,
            'team'               => $team,
            'title'              => $title,
            'off'                => $off,
            'contract_term'      => $contract_term,
            'company_id'         => $company_id,
            'return_letter_from' => $return_letter_from,
            'return_letter_to'   => $return_letter_to,
            'send_letter_from'   => $send_letter_from,
            'send_letter_to'     => $send_letter_to,
            'set_date'           => $set_date,
            'ids'                => $ids,
            'return_letter'      => $return_letter,
            'is_upload_file'     => $is_upload_file,
        );


        if ($btnSetDate) {

            $this->setTimeReturnLetter($params);
        }
        if ($btnSetDisable) {
            $this->_setDisable($params);
        }
        if ($btnRollback) {
            $this->_setRollback($params);
        }
        $limit = 300;
        $total = 0;
        $desc  = $this->getRequest()->getParam('desc', 1);
        $sort  = $this->getRequest()->getParam('sort');
        $page  = $this->getRequest()->getParam('page', 1);

        if ($export) {
            $limit = $page  = 0;
        }

        $staff_contract_model = new Application_Model_StaffContract();
        $list                 = $staff_contract_model->history($page, $limit, $total, $params);
        if ($export) {
            $this->_exportInfo($list);
        }
        $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
        $user_id          = $userStorage->id;
//        if ($user_id == 5899) {
//            echo "<pre>";
//            print_r($list);
//            die;
//        }
        $total            = $db->fetchOne('select @total');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error   = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract-new/history' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;


        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;

        $QContractType             = new Application_Model_ContractTypes();
        $QContractType             = $QContractType->get_cache();
        $this->view->contractTerms = $QContractType;

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all();
        $this->view->provinces = $provinces;

        $QTeam                                   = new Application_Model_Team();
        $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->userStorage                 = $userStorage;


        $area_id_confirm = array();
        $group_id        = $userStorage->group_id;
        if ($group_id != HR_ID) {
            $sql      = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = " . $userStorage->id .
                    " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = " . $userStorage->id;
            $stmt1    = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();
            foreach ($area_tmp as $item) {
                $area_id_confirm[] = $item['area_id'];
            }
            $this->view->area_id_confirm = $area_id_confirm;
        }

        $teams = array();
        if ($department AND count($department) > 0) {
            foreach ($department as $key => $value) {
                $teams += $recursiveDeparmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if ($team AND $department AND count($team) > 0) {
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;
    }

    public function setTimeReturnLetter($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/history' . ($params ? '?' . http_build_query($params) . '&' : '?');
        ;
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        if ($this->getRequest()->isPost()) {
            if (in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID))) {
                $db->beginTransaction();
                try {
                    $ids       = $params['ids'];
                    $date      = My_Date::normal_to_mysql($params['set_date']);
                    $data      = array('return_letter' => $date);
                    $QContract = new Application_Model_StaffContract();
                    $where     = $QContract->getAdapter()->quoteInto('id IN (?)', $ids);
                    $QContract->update($data, $where);
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Cập nhật ngày nhận thành công');
                } catch (Exception $e) {
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $db->rollBack();
                }
            }
            $this->_redirect($back_url);
        }
    }

    public function renewableAction() {

        $flashMessenger        = $this->_helper->flashMessenger;
        $userStorage           = Zend_Auth::getInstance()->getStorage()->read();
        $db                    = Zend_Registry::get('db');
        $export                = $this->getRequest()->getParam('export');
        $name                  = $this->getRequest()->getParam('name', '');
        $code                  = $this->getRequest()->getParam('code', '');
        $office                = $this->getRequest()->getParam('office', -1);
        $signed_to             = $this->getRequest()->getParam('signed_to', date('t/m/Y'));
        $area_id               = $this->getRequest()->getParam('area_id', NULL);
        $department            = $this->getRequest()->getParam('department', NULL);
        $team                  = $this->getRequest()->getParam('team', NULL);
        $title                 = $this->getRequest()->getParam('title', NULL);
        $off                   = $this->getRequest()->getParam('off', 0);
        $contract_term         = $this->getRequest()->getParam('contract_term', NULL);
        $company_id            = $this->getRequest()->getParam('company_id', NULL);
        $area_id_confirm       = $this->getRequest()->getParam('area_id_confirm', 0);
        $set_date              = $this->getRequest()->getParam('set_date', date('d/m/Y'));
        $ids                   = $this->getRequest()->getParam('id');
        $btnApproveByStaff     = $this->getRequest()->getParam('btnApproveByStaff', 0);
        $btnConfirm            = $this->getRequest()->getParam('btnConfirm');
        $month                 = $this->getRequest()->getParam('confirm_date');
        $back_url              = $this->getRequest()->getParam('back_url');
        $updateNextContract    = $this->getRequest()->getParam('update_next');
        $updateNextContractAll = $this->getRequest()->getParam('update_next_all');

        if ($signed_to != '') {
//            if ($signed_to == date('t/m/Y')) {
//                $date_tmp  = My_Date::normal_to_mysql($signed_to);
//                $date_ex   = date_create($date_tmp);
//                date_add($date_ex, date_interval_create_from_date_string("5 days"));
//                $signed_to = date_format($date_ex, "d/m/Y");
//            }
        }
        $params = array(
            'name'              => trim($name),
            'code'              => trim($code),
            'signed_to'         => $signed_to,
            'office'            => $office,
            'area_id'           => $area_id,
            'department'        => $department,
            'team'              => $team,
            'title'             => $title,
            'off'               => $off,
            'contract_term'     => $contract_term,
            'company_id'        => $company_id,
            'month'             => $month,
            'ids'               => $ids,
            'btnApproveByStaff' => $btnApproveByStaff,
            'btnConfirm'        => $btnConfirm,
            'confirm_date'      => date('m/Y'),
            'area_id_confirm'   => $area_id_confirm,
            'back_url'          => $back_url,
        );
        if ($btnConfirm OR $btnApproveByStaff) {
            $this->_confirm($params);
        }
        if ($updateNextContract) {
            $this->_updateNextContract($params);
        }
        if ($updateNextContractAll) {
            $db->beginTransaction();
            try {
                $Qstaff_contact = new Application_Model_StaffContract();
                $Qstaff_contact->UpdateNextContractAll();

                $db->commit();
            } catch (Exception $e) {
                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            }
        }
        $limit = 600;
        $total = 0;
        $desc  = $this->getRequest()->getParam('desc', 1);
        $sort  = $this->getRequest()->getParam('sort');
        $page  = $this->getRequest()->getParam('page', 1);



        $staff_contract_model = new Application_Model_StaffContract();
        if ($export) {
            $limit = $page  = 0;
        }
        $list = $staff_contract_model->renewable($page, $limit, $total, $params);

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $user_id     = $userStorage->id;

        if ($export) {
            $this->_exportRenew($list);
        }
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error   = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract-new/renewable' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;


        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;


        $QContractType             = new Application_Model_ContractTypes();
        $QContractType             = $QContractType->get_cache();
        $this->view->contractTerms = $QContractType;

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all();
        $this->view->provinces = $provinces;

        $QTeam                                   = new Application_Model_Team();
        $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->userStorage                 = $userStorage;


        $area_id_confirm = array();
        $group_id        = $userStorage->group_id;

        if ($group_id != HR_ID) {
            $sql      = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = " . $userStorage->id .
                    " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = " . $userStorage->id;
            $stmt1    = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();
            foreach ($area_tmp as $item) {
                $area_id_confirm[] = $item['area_id'];
            }
            $this->view->area_id_confirm = $area_id_confirm;
        }

        $teams = array();
        if ($department AND count($department) > 0) {
            foreach ($department as $key => $value) {
                $teams += $recursiveDeparmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if ($team AND $department AND count($team) > 0) {
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;
    }

    public function renewableoffAction() {

        $flashMessenger  = $this->_helper->flashMessenger;
        $userStorage     = Zend_Auth::getInstance()->getStorage()->read();
        $db              = Zend_Registry::get('db');
        $export          = $this->getRequest()->getParam('export');
        $name            = $this->getRequest()->getParam('name', '');
        $code            = $this->getRequest()->getParam('code', '');
        $office          = $this->getRequest()->getParam('office', -1);
        $signed_to       = $this->getRequest()->getParam('signed_to', date('t/m/Y'));
        $signed_from     = $this->getRequest()->getParam('signed_from', date('t/m/Y'));
        $area_id         = $this->getRequest()->getParam('area_id', NULL);
        $department      = $this->getRequest()->getParam('department', NULL);
        $team            = $this->getRequest()->getParam('team', NULL);
        $title           = $this->getRequest()->getParam('title', NULL);
        $off             = $this->getRequest()->getParam('off', 0);
        $contract_term   = $this->getRequest()->getParam('contract_term', NULL);
        $company_id      = $this->getRequest()->getParam('company_id', NULL);
        $area_id_confirm = $this->getRequest()->getParam('area_id_confirm', 0);


        $set_date          = $this->getRequest()->getParam('set_date', date('d/m/Y'));
        $ids               = $this->getRequest()->getParam('id');
        $btnApproveByStaff = $this->getRequest()->getParam('btnApproveByStaffOff', 0);
        $btnConfirm        = $this->getRequest()->getParam('btnConfirm');
        $month             = $this->getRequest()->getParam('confirm_date');
        $back_url          = $this->getRequest()->getParam('back_url');

        if ($signed_to != '') {
            if ($signed_to == date('t/m/Y')) {
                $date_tmp  = My_Date::normal_to_mysql($signed_to);
                $date_ex   = date_create($date_tmp);
                date_add($date_ex, date_interval_create_from_date_string("5 days"));
                $signed_to = date_format($date_ex, "d/m/Y");
            }
        }
        if ($signed_from != '') {
            if ($signed_from == date('t/m/Y')) {
                $date_tmp    = My_Date::normal_to_mysql($signed_from);
                $date_ex     = date_create($date_tmp);
                date_add($date_ex, date_interval_create_from_date_string("5 days"));
                $signed_from = date_format($date_ex, "d/m/Y");
            }
        }
        $params = array(
            'name'                 => trim($name),
            'code'                 => trim($code),
            'signed_to'            => $signed_to,
            'signed_from'          => $signed_from,
            'office'               => $office,
            'area_id'              => $area_id,
            'department'           => $department,
            'team'                 => $team,
            'title'                => $title,
            'off'                  => $off,
            'contract_term'        => $contract_term,
            'company_id'           => $company_id,
            'month'                => $month,
            'ids'                  => $ids,
            'btnApproveByStaffOff' => $btnApproveByStaff,
            'confirm_date'         => date('m/Y'),
            'area_id_confirm'      => $area_id_confirm,
            'back_url'             => $back_url,
        );
        if ($btnConfirm OR $btnApproveByStaff) {
            $this->_confirm($params);
        }
        $limit = 600;
        $total = 0;
        $desc  = $this->getRequest()->getParam('desc', 1);
        $sort  = $this->getRequest()->getParam('sort');
        $page  = $this->getRequest()->getParam('page', 1);



        $staff_contract_model = new Application_Model_StaffContract();
        if ($export) {
            $limit = $page  = 0;
        }
        $list = $staff_contract_model->renewableoff($page, $limit, $total, $params);


        if ($export) {
            $this->_exportRenew($list);
        }
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error   = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract-new/renewableoff' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;


        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;


        $QContractType             = new Application_Model_ContractTypes();
        $QContractType             = $QContractType->get_cache();
        $this->view->contractTerms = $QContractType;

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all();
        $this->view->provinces = $provinces;

        $QTeam                                   = new Application_Model_Team();
        $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->userStorage                 = $userStorage;


        $area_id_confirm = array();
        $group_id        = $userStorage->group_id;
        if ($group_id != HR_ID) {
            $sql      = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = " . $userStorage->id .
                    " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = " . $userStorage->id;
            $stmt1    = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();
            foreach ($area_tmp as $item) {
                $area_id_confirm[] = $item['area_id'];
            }
            $this->view->area_id_confirm = $area_id_confirm;
        }

        $teams = array();
        if ($department AND count($department) > 0) {
            foreach ($department as $key => $value) {
                $teams += $recursiveDeparmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if ($team AND $department AND count($team) > 0) {
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;
    }

    public function _confirm($params = NULL) {
        $QStaffContract       = new Application_Model_StaffContract();
        $btnApproveByStaff    = $params['btnApproveByStaff'];
        $btnApproveByStaffOff = $params['btnApproveByStaffOff'];
        $btnConfirm           = $params['btnConfirm'];
        unset($params['btnApproveByStaff']);
        unset($params['btnConfirm']);
        $flashMessenger       = $this->_helper->flashMessenger;
        $db                   = Zend_Registry::get('db');
        $userStorage          = Zend_Auth::getInstance()->getStorage()->read();
        $month                = $params['month'];
        $ids                  = $params['ids'];
        $back_url             = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/renewable' . ($params ? '?' . http_build_query($params) . '&' : '?');

        $area_id_confirm = $params['area_id_confirm'];


        if (in_array($userStorage->group_id, array(ASM_ID, ASMSTANDBY_ID))) {
            if (count($area_id_confirm) == 0) {
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn khu vực xác nhận');
                $this->_redirect($back_url);
            }

            $ids = array(); //reset nếu là asm
        }

        $month = ($month) ? My_Date::normal_to_mysql($month) : NULL;

        if ($month) {

            $start = date('Y-m-01', strtotime($month));

//            $end_tmp = date('Y-m-01', strtotime($month));
//            $date    = new DateTime($end_tmp);
//            $date->add(new DateInterval('P1M'));
            $end = date('Y-m-t', strtotime($month));
        }

        // confirm theo tháng
        if ($btnConfirm) {
            if (!empty($area_id_confirm)) {

                if ($month) {
                    try {
                        $store_params = array($userStorage->id, $start, $end, $area_id_confirm, $id = NULL);
                        // print_r($store_params); die;
                        $code         = $QStaffContract->confirmContract($store_params);
                        if ($code < 0) {
                            $mess = '';
                            if ($code == -1) {
                                $mess = 'ASM da confirm roi';
                            } elseif ($code == -2) {
                                $mess = 'ASM chua nhap benh vien';
                            } elseif ($code == -3) {
                                $mess = 'ASM chua confirm xong(con nguoi chua duoc confirm)';
                            } elseif ($code == -4) {
                                $mess = 'Hospital_id is null';
                            } elseif ($code == -6) {
                                $mess = 'Vui lòng nhập lương';
                            } elseif ($code == -7) {
                                $mess = 'Khu vực chưa xác nhận đề xuất xong';
                            } else {
                                $mess = 'Lỗi';
                            }
                            $flashMessenger->setNamespace('error')->addMessage($mess);
                        } else {
                            $flashMessenger->setNamespace('success')->addMessage('Done');
                            $this->_redirect($back_url);
                        }
                    } catch (Exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect($back_url);
                    }
                }
            } else {
                $flashMessenger->setNamespace('error')->addMessage("Vui lòng chọn khu vực");
            }
        }


        // confirm theo từng nhân viên
        if ($btnApproveByStaff) {


            if (count($ids) > 0) {

                $select = $db->select()
                        ->from(array('a' => 'staff_contract'), array('a.*'))
                        ->where('a.id IN (?)', $ids)
                        ->where('a.status IN (?)', array(0, 1))
                        ->where('a.print_type = ?', 1);
                $result = $db->fetchAll($select);

                foreach ($result as $key => $value) {

                    $store_params = array(
                        $userStorage->id,
                        date('Y-m-01', strtotime($value['from_date'])),
                        date('Y-m-t', strtotime($value['from_date'])),
                        NULL,
                        $value['id']
                    );
//                      if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
//                    echo "<pre>";
//                    print_r($store_params);
//                    die;
//                }
                    $code         = $QStaffContract->confirmContract($store_params);
                    if ($code < 0) {
                        $mess = '';
                        if ($code == -1) {
                            $mess = 'ASM da confirm roi';
                        } elseif ($code == -2) {
                            $mess = 'ASM chua nhap benh vien';
                        } elseif ($code == -3) {
                            $mess = 'ASM chua confirm xong(con nguoi chua duoc confirm)';
                        } elseif ($code == -4) {
                            $mess = 'Hospital_id is null';
                        } elseif ($code == -6) {
                            $mess = 'Vui lòng nhập lương';
                        } elseif ($code == -7) {
                            $mess = 'Khu vực chưa xác nhận đề xuất xong';
                        } else {
                            $mess = 'Lỗi';
                        }
                        $flashMessenger->setNamespace('error')->addMessage($mess);
                        $this->_redirect($back_url);
                    } else {
                        $QStaffContract->UpdateNextContractByStaff($value['staff_id']);
                    }
                }
                $flashMessenger->setNamespace('success')->addMessage('Done');
            } else {
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn nhân viên để approve');
            }
        }
        if ($btnApproveByStaffOff) {
            if (count($ids) > 0) {

                $select = $db->select()
                        ->from(array('a' => 'staff_contract'), array('a.*'))
                        ->where('a.id IN (?)', $ids)
                        ->where('a.status IN (?)', array(0, 1))
                        ->where('a.print_type = ?', 1);
                $result = $db->fetchAll($select);

                foreach ($result as $key => $value) {

                    $store_params = array(
                        $userStorage->id,
                        date('Y-m-01', strtotime($value['from_date'])),
                        date('Y-m-t', strtotime($value['from_date'])),
                        NULL,
                        $value['id']
                    );

                    $code = $QStaffContract->confirmContractOff($store_params);

                    if ($code < 0) {
                        $mess = '';
                        if ($code == -1) {
                            $mess = 'ASM da confirm roi';
                        } elseif ($code == -2) {
                            $mess = 'ASM chua nhap benh vien';
                        } elseif ($code == -3) {
                            $mess = 'ASM chua confirm xong(con nguoi chua duoc confirm)';
                        } elseif ($code == -4) {
                            $mess = 'Hospital_id is null';
                        } elseif ($code == -6) {
                            $mess = 'Vui lòng nhập lương';
                        } elseif ($code == -7) {
                            $mess = 'Khu vực chưa xác nhận đề xuất xong';
                        } else {
                            $mess = 'Lỗi';
                        }
                        $flashMessenger->setNamespace('error')->addMessage($mess);
                        $this->_redirect($back_url);
                    }
                }
                $flashMessenger->setNamespace('success')->addMessage('Done');
            } else {
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn nhân viên để approve');
            }
        }

        $this->_redirect($back_url);
    }

    public function editStaffPreviewAction() {
        $userStorage             = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->userStorage = $userStorage;
        $id                      = $this->getRequest()->getParam('id');
        $db                      = Zend_Registry::get('db');
        $cols                    = array(
            'p.*',
            'province_id' => 'r.province_id',
            'staff_name'  => 'CONCAT(s.firstname," ",s.lastname)',
            'staff_code'  => 's.code'
        );

        $select         = $db->select()
                ->from(array('p' => 'staff_contract'), $cols)
                ->join(array('s' => 'staff'), 'p.staff_id = s.id', array())
                ->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array())
                ->where('p.id = ?', $id)
        ;
        $staff_contract = $db->fetchRow($select);

        if (!empty($staff_contract)) {
            $this->view->staff_contract = $staff_contract;
            $QHospital                  = new Application_Model_Hospital();

//            if ($userStorage->group_id == HR_ID) {
//                $hospitals = $QHospital->get_all();
//            } else {
//                $hospitals = $QHospital->get_all(array('province_id' => $staff_contract['province_id']));
//            }
            $hospitals = $QHospital->get_all_hide_contract(array('province_id' => $staff_contract['province_id']));

            $this->view->hospitals = $hospitals;
        }

        $from_date             = date('d/m/Y');
        $to_date               = date('d/m/Y');
        $this->view->from_date = $from_date;
        $this->view->to_date   = $to_date;

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all();
        $this->view->provinces = $provinces;

        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;

        $this->_helper->layout()->disableLayout(true);
    }

    public function saveProposalAction() {

        $id          = $this->getRequest()->getParam('id');
        $hospital_id = $this->getRequest()->getParam('hospital_id', 0);
        $proposal_id = $this->getRequest()->getParam('proposal_id', 0);
        $note        = $this->getRequest()->getParam('note', NULL);
//        $return_letter = $this->getRequest()->getParam('return_letter', NULL);
        $new_salary  = $this->getRequest()->getParam('new_salary', 0);

        $new_salary     = intval($new_salary);
        $QStaff         = new Application_Model_Staff();
        $QAsmContract   = new Application_Model_StaffContract();
        $db             = Zend_Registry::get('db');
        $flashMessenger = $this->_helper->flashMessenger;
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();

//        $return_letter = ($return_letter) ? My_Date::normal_to_mysql($return_letter) : NULL;

        $data = array(
            'asm_proposal' => $proposal_id,
            'note'         => $note,
        );

        if (in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID)) && $new_salary > 0):
            $data['salary_insurance_temp'] = $new_salary;
        endif;

        if (intval($hospital_id)) {
            $data['hospital_id'] = $hospital_id;
        }
        if (empty($hospital_id) && !in_array($proposal_id, array(2, 3))) {
            $result = array('code' => -1, 'messages' => "Vui lòng chọn bệnh viện");
            $this->_helper->json->sendJson($result);
            return;
        }
        $db->beginTransaction();
        try {

            $select = $db->select()
                    ->from(array('p' => 'staff_contract'), array('p.*'))
                    ->where('p.id = ?', $id)
                    ->where('p.status IN (0,1) OR p.status IS NULL');
            $r      = $db->fetchRow($select);
            if ($r) {

                $where   = array();
                $where[] = $QAsmContract->getAdapter()->quoteInto('id = ?', $id);
                $QAsmContract->update($data, $where);
                $select2 = $db->select()
                        ->from(array('a' => 'staff_contract'), array(
                            'hospital_name' => 'c.name',
                            'proposal'      => 'b.name',
                            'salary'        => 'a.salary_insurance_temp',
                            'a.note'))
                        ->join(array('b' => 'asm_proposal'), 'a.asm_proposal = b.id', array())
                        ->joinLeft(array('c' => 'hospital'), 'c.id = a.hospital_id', array())
                        ->where('a.id = ?', $id);
                $rData   = $db->fetchRow($select2);
                $result  = array('code' => 1, 'messages' => $rData);
                $db->commit();
            } else {
                $result = array('code' => -1, 'messages' => 'Không thể cập nhật sau khi HR xác nhận!');
            }
        } catch (Exception $e) {
            $db->rollBack();
            $result = array('code' => -1, 'messages' => $e->getMessage());
        }

        $this->_helper->json->sendJson($result);
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function printRenewableAction() {
        $flashMessenger    = $this->_helper->flashMessenger;
        $userStorage       = Zend_Auth::getInstance()->getStorage()->read();
        $back_url          = $this->getRequest()->getParam('back_url');
        $db                = Zend_Registry::get('db');
        $export            = $this->getRequest()->getParam('export');
        $name              = $this->getRequest()->getParam('name', '');
        $code              = $this->getRequest()->getParam('code', '');
        $office            = $this->getRequest()->getParam('office', -1);
        $signed_to         = $this->getRequest()->getParam('signed_to', date('d/m/Y', strtotime(date('Y-m-t'))));
        $area_id           = $this->getRequest()->getParam('area_id', NULL);
        $department        = $this->getRequest()->getParam('department', NULL);
        $team              = $this->getRequest()->getParam('team', NULL);
        $title             = $this->getRequest()->getParam('title', NULL);
        $off               = $this->getRequest()->getParam('off', 0);
        $contract_term     = $this->getRequest()->getParam('contract_term', NULL);
        $company_id        = $this->getRequest()->getParam('company_id', NULL);
        $btnSetPrintStatus = $this->getRequest()->getParam('btnSetPrintStatus');
        $ids               = $this->getRequest()->getParam('id');
        $month             = $this->getRequest()->getParam('confirm_date');
        $btnSetDisable     = $this->getRequest()->getParam('btnSetDisable');
        $update_contract   = $this->getRequest()->getParam('update_contract');


//        if ($signed_to != '') {
//            if ($signed_to == date('t/m/Y')) {
//                $date_tmp  = My_Date::normal_to_mysql($signed_to);
//                $date_ex   = date_create($date_tmp);
//                date_add($date_ex, date_interval_create_from_date_string("5 days"));
//                $signed_to = date_format($date_ex, "d/m/Y");
//            }
//        }
        $params = array(
            'name'          => trim($name),
            'code'          => trim($code),
            'signed_to'     => $signed_to,
            'office'        => $office,
            'area_id'       => $area_id,
            'department'    => $department,
            'team'          => $team,
            'title'         => $title,
            'off'           => $off,
            'contract_term' => $contract_term,
            'company_id'    => $company_id,
            'month'         => $month,
            'ids'           => $ids,
            'confirm_date'  => date('m/Y'),
            'back_url'      => $back_url,
        );

        if ($btnSetPrintStatus) {
            $this->_setPrintStatus($params);
        }
        if ($btnSetDisable) {
            $this->_setDisable($params);
        }
        if ($update_contract) {
            $this->_updateContract($params);
        }

        $limit = 300;
        $total = 0;
        $desc  = $this->getRequest()->getParam('desc', 1);
        $sort  = $this->getRequest()->getParam('sort');
        $page  = $this->getRequest()->getParam('page', 1);


        if ($export) {
            $limit = $page  = 0;
        }
        $staff_contract_model = new Application_Model_StaffContract();
        $list                 = $staff_contract_model->renewablePrint($page, $limit, $total, $params);

        if ($export) {
            $this->_exportInfo($list);
        }


        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error   = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract-new/print-renewable' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;


        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;


        $QContractType             = new Application_Model_ContractTypes();
        $QContractType             = $QContractType->get_cache();
        $this->view->contractTerms = $QContractType;

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all();
        $this->view->provinces = $provinces;

        $QTeam                                   = new Application_Model_Team();
        $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->userStorage                 = $userStorage;

        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;
        $area_id_confirm       = array();
        $group_id              = $userStorage->group_id;
        if ($group_id != HR_ID) {
            $sql      = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = " . $userStorage->id .
                    " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = " . $userStorage->id;
            $stmt1    = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();
            foreach ($area_tmp as $item) {
                $area_id_confirm[] = $item['area_id'];
            }
            $this->view->area_id_confirm = $area_id_confirm;
        }

        $teams = array();
        if ($department AND count($department) > 0) {
            foreach ($department as $key => $value) {
                $teams += $recursiveDeparmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if ($team AND $department AND count($team) > 0) {
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;
    }

    public function _setDisable($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/print-renewable';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $ids              = $params['ids'];
            $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
            $db               = Zend_Registry::get('db');
            $QTeam            = new Application_Model_Team();
            $staff_title_info = $userStorage->title;
            $team_info        = $QTeam->find($staff_title_info);
            $team_info        = $team_info->current();
            $group_id         = $team_info['access_group'];

            if (in_array($group_id, array(HR_ID, ADMINISTRATOR_ID))) {
                if (count($ids) > 0) {
                    $str  = implode(',', $ids);
                    $sql  = '
                        UPDATE staff_contract a
                        join staff_salary ss on ss.id=a.salary_id
                        SET a.is_disable = 1,a.print_status = 1 ,a.district_confirm=1,a.district_id=0
                        WHERE a.id IN (' . $str . ') and (a.is_next <> 2 or is_next is null)';
                    $stmt = $db->query($sql);
                    $stmt->execute();
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Updated not current contract to disable status.');
                } else {
                    $flashMessenger->setNamespace('error')->addMessage('Please select item');
                }
            } else {
                $flashMessenger->setNamespace('error')->addMessage('Permission failed');
            }
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }

        $this->_redirect($back_url);
    }

    public function _setPrintStatus($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/print-renewable';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $ids         = $params['ids'];
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db          = Zend_Registry::get('db');
            if (in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID))) {
                if (count($ids) > 0) {

                    $str  = implode(',', $ids);
                    $sql  = '
                        UPDATE staff_contract a
                        SET a.send_letter = ?,
                            a.print_status = 1
                        WHERE a.id IN (' . $str . ') AND  a.title NOT IN (182,183,190,162,163,164)
                        AND (a.print_status IS NULL OR a.print_status = 0)
                    ';
                    $stmt = $db->query($sql, array(date('Y-m-d')));
                    $stmt->execute();
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                } else {
                    $flashMessenger->setNamespace('error')->addMessage('Please select item');
                }
            } else {
                $flashMessenger->setNamespace('error')->addMessage('Permission failed');
            }
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }
    
    public function _setPrintStatusAppendix($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/print-renewable';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $ids         = $params['ids'];
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db          = Zend_Registry::get('db');
            if (in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID))) {
                if (count($ids) > 0) {
                    $str  = implode(',', $ids);
                    // update status print
                    $sql  = '
                        UPDATE staff_contract a
                        SET a.send_letter = ?,
                            a.print_status = 1
                        WHERE a.id IN (' . $str . ') AND  a.title NOT IN (182,183,190,162,163,164)
                        AND (a.print_status IS NULL OR a.print_status = 0)
                    ';
                    $stmt = $db->query($sql, array(date('Y-m-d')));
                    $stmt->execute();
                    
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                } else {
                    $flashMessenger->setNamespace('error')->addMessage('Please select item');
                }
            } else {
                $flashMessenger->setNamespace('error')->addMessage('Permission failed');
            }
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function _setRollback($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/print-renewable';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $ids         = $params['ids'];
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db          = Zend_Registry::get('db');
            if (in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID))) {
                if (count($ids) > 0) {

                    $str  = implode(',', $ids);
                    $sql  = '
                        UPDATE staff_contract a
                        SET 
                            a.print_status = 0
                        WHERE a.id IN (' . $str . ') 
                    ';
                    $stmt = $db->query($sql, array(date('Y-m-d')));
                    $stmt->execute();
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                } else {
                    $flashMessenger->setNamespace('error')->addMessage('Please select item');
                }
            } else {
                $flashMessenger->setNamespace('error')->addMessage('Permission failed');
            }
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function _updateCurrentProbation($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/new-sign-contract';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $Qstaff_contact = new Application_Model_StaffContract();
        $db->beginTransaction();
        try {

//          $str  = implode(',', $ids);
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('c' => 'staff_contract'), array('s.code'))
                    ->join(array('s' => 'staff'), 's.id=c.staff_id ', array())
                    ->where('c.id in (?)', $params['ids']);
            $result = $db->fetchAll($select);
            if (!empty($result)) {
                $list_code = "";
                foreach ($result as $key => $value) {
                    $list_code .= $value['code'] . ",";
                }
            }



            if ($Qstaff_contact->UpdateCurrentContractProbationByStaff(rtrim($list_code, ","))) {
                $Qstaff_contact->UpdateNextContractByListStaff($list_code);
            } else {
                Throw new Exception("Lỗi cập nhật");
            }
            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function _updateContract($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/new-sign-contract';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $Qstaff_contact = new Application_Model_StaffContract();
        $db->beginTransaction();
        try {

            $str    = implode(',', $params['ids']);
            $select = $db->select()
                    ->from(array('c' => 'staff_contract'), array('s.code'))
                    ->join(array('s' => 'staff'), 's.id=c.staff_id ', array())
                    ->where('c.id in (?)', $params['ids']);
            $result = $db->fetchAll($select);
            if (!empty($result)) {
                $list_code = "";
                foreach ($result as $key => $value) {
                    $list_code .= $value['code'] . ",";
                }
            }

            if ($Qstaff_contact->UpdateContractById($str)) {
                $Qstaff_contact->UpdateNextContractByListStaff($list_code);
            } else {
                Throw new Exception("Lỗi cập nhật");
            }
            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function _updateContractProbation($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/new-sign-contract';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $Qstaff_contact = new Application_Model_StaffContract();
        $db->beginTransaction();
        try {

            $str    = implode(',', $params['ids']);
            $select = $db->select()
                    ->from(array('c' => 'staff_contract'), array('s.code'))
                    ->join(array('s' => 'staff'), 's.id=c.staff_id ', array())
                    ->where('c.id in (?)', $params['ids']);
            $result = $db->fetchAll($select);
            if (!empty($result)) {
                $list_code = "";
                foreach ($result as $key => $value) {
                    $list_code .= $value['code'] . ",";
                }
            }

            if ($Qstaff_contact->UpdateContractProbationById($str)) {
                $Qstaff_contact->UpdateNextContractByListStaff($list_code);
            } else {
                Throw new Exception("Lỗi cập nhật");
            }
            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function _updateNextContract($params) {
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract-new/new-sign-contract';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $Qstaff_contact = new Application_Model_StaffContract();
        $db->beginTransaction();
        try {
            $select = $db->select()
                    ->from(array('c' => 'staff_contract'), array('s.code'))
                    ->join(array('s' => 'staff'), 's.id=c.staff_id ', array())
                    ->where('c.id in (?)', $params['ids']);
            $result = $db->fetchAll($select);
            if (!empty($result)) {
                $list_code = "";
                foreach ($result as $key => $value) {
                    $list_code .= $value['code'] . ",";
                }
                $Qstaff_contact->UpdateNextContractByListStaff($list_code);
            }


            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function printAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();

        $back_url = $this->getRequest()->getParam('back_url');

        $ids          = $this->getRequest()->getParam('id');
        $print_status = $this->getRequest()->getParam('print_status', 0);

        $QArea     = new Application_Model_Area();
        $QContract = new Application_Model_ContractTerm();

        $contract_name = $QContract->get_cache();

        $this->view->contract_name = $contract_name;
        $this->view->areas         = $QArea->fetchAll();
        $this->view->back_url      = $back_url;
        $staffs                    = array();

        $db = Zend_Registry::get('db');

        if (is_array($ids) && $ids) {
            $staff_contract_model = new Application_Model_StaffContract();
            $staffs               = $staff_contract_model->printList($ids);
//            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//            $user_id = $userStorage->id;
//            if ($user_id == 5899) {
//                echo "<pre>";
//                print_r($staffs);
//                die;
//            }
            $where                = array();
            $where[]              = $staff_contract_model->getAdapter()->quoteInto('id IN (?)', $ids);
            $where[]              = $staff_contract_model->getAdapter()->quoteInto('print_status = ?', 0);
            $where[]              = $staff_contract_model->getAdapter()->quoteInto('print_type = ?', 1);

            $dataUpdate = array(
                'send_letter'        => date('Y-m-d'),
                'print_status'       => 1,
                'lastest_print_date' => date('Y-m-d H:i:s')
            );

            if ($print_status == 1) {
                $db->beginTransaction();
                try {
                    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                    $user_id     = $userStorage->id;
                    if ($user_id != 5899) {
                        $staff_contract_model->update($dataUpdate, $where);
                    }

                    $db->commit();
                } catch (Exception $e) {
                    $db->rollback();
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $this->_redirect("/contract/staff-preview");
                }
            }
        }
        $this->view->staff = $staffs;

        $QGroup             = new Application_Model_Group();
        $this->view->groups = $QGroup->fetchAll();

        $QModel                     = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel                     = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel                  = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        $QRegionalMarket              = new Application_Model_RegionalMarket();
        $this->view->regional_markets = $QRegionalMarket->get_cache();

        $QOffice             = new Application_Model_Office();
        $this->view->offices = $QOffice->get_cache();

        $QModel            = new Application_Model_Team();
        $this->view->teams = $QModel->fetchAll();

        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
    }

    public function printAppAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();

        $back_url = $this->getRequest()->getParam('back_url');

        $ids          = $this->getRequest()->getParam('id');
        $print_status = $this->getRequest()->getParam('print_status', 0);

        $QArea         = new Application_Model_Area();
        $QContract     = new Application_Model_ContractTerm();
        $QAsmContract  = new Application_Model_AsmContract();
        $contract_name = $QContract->get_cache();

        $this->view->contract_name = $contract_name;
        $this->view->areas         = $QArea->fetchAll();
        $this->view->back_url      = $back_url;
        $staffs                    = array();

        $db = Zend_Registry::get('db');

        if (is_array($ids) && $ids) {
            $staff_contract_model = new Application_Model_StaffContract();
            $staffs               = $staff_contract_model->printListAppendix($ids);
//            echo "<pre>";
//            var_dump($staffs);
//            die;
            $where                = array();
            $where[]              = $staff_contract_model->getAdapter()->quoteInto('id IN (?)', $ids);
            $where[]              = $staff_contract_model->getAdapter()->quoteInto('print_status = ?', 0);
            $where[]              = $staff_contract_model->getAdapter()->quoteInto('print_type = ?', 2);

            $dataUpdate = array(
                'send_letter'        => date('Y-m-d'),
                'print_status'       => 1,
                'lastest_print_date' => date('Y-m-d H:i:s')
            );

            if ($print_status == 1) {
                $db->beginTransaction();
                try {
                    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                    $user_id     = $userStorage->id;
                    if ($user_id != 5899) {
                        $staff_contract_model->update($dataUpdate, $where);
                    }
//                    $staff_contract_model->update($dataUpdate, $where);
                    $db->commit();
                } catch (Exception $e) {
                    $db->rollback();
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $this->_redirect("/contract-new/history");
                }
            }
        }
        $this->view->staff = $staffs;

        $QGroup             = new Application_Model_Group();
        $this->view->groups = $QGroup->fetchAll();

        $QModel                     = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel                     = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel                  = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        $QRegionalMarket              = new Application_Model_RegionalMarket();
        $this->view->regional_markets = $QRegionalMarket->get_cache();

        $QModel            = new Application_Model_Team();
        $this->view->teams = $QModel->fetchAll();

        $QOffice             = new Application_Model_Office();
        $this->view->offices = $QOffice->get_cache();

        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
    }

    public function printAppendixAction() {
        $flashMessenger            = $this->_helper->flashMessenger;
        $userStorage               = Zend_Auth::getInstance()->getStorage()->read();
        $back_url                  = $this->getRequest()->getParam('back_url');
        $db                        = Zend_Registry::get('db');
        $export                    = $this->getRequest()->getParam('export');
        $name                      = $this->getRequest()->getParam('name', '');
        $code                      = $this->getRequest()->getParam('code', '');
        $office                    = $this->getRequest()->getParam('office', -1);
        $signed_to                 = $this->getRequest()->getParam('signed_to', date('t/m/Y'));
        $signed_from               = $this->getRequest()->getParam('signed_from', date('01/m/Y'));
        $area_id                   = $this->getRequest()->getParam('area_id', NULL);
        $department                = $this->getRequest()->getParam('department', NULL);
        $team                      = $this->getRequest()->getParam('team', NULL);
        $title                     = $this->getRequest()->getParam('title', NULL);
        $off                       = $this->getRequest()->getParam('off', 0);
        $contract_term             = $this->getRequest()->getParam('contract_term', NULL);
        $company_id                = $this->getRequest()->getParam('company_id', NULL);
        $btnSetPrintStatus         = $this->getRequest()->getParam('btnSetPrintStatus');
        $ids                       = $this->getRequest()->getParam('id');
        $month                     = $this->getRequest()->getParam('confirm_date');
        $btnSetDisable             = $this->getRequest()->getParam('btnSetDisable');
        $update_contract           = $this->getRequest()->getParam('update_contract');
        $update_contract_probation = $this->getRequest()->getParam('update_contract_probation');


        if ($signed_to != '') {
//            if ($signed_to == date('t/m/Y')) {
//                $date_tmp  = My_Date::normal_to_mysql($signed_to);
//                $date_ex   = date_create($date_tmp);
//                date_add($date_ex, date_interval_create_from_date_string("5 days"));
//                $signed_to = date_format($date_ex, "d/m/Y");
//            }
        }
        if ($signed_from != '') {
            if ($signed_from == date('01/m/Y')) {
                $date_tmp    = My_Date::normal_to_mysql($signed_from);
                $date_ex     = date_create($date_tmp);
//                date_add($date_ex, date_interval_create_from_date_string("5 days"));
                $signed_from = date_format($date_ex, "d/m/Y");
            }
        }
        $params = array(
            'name'          => trim($name),
            'code'          => trim($code),
            'signed_to'     => $signed_to,
            'signed_from'   => $signed_from,
            'office'        => $office,
            'area_id'       => $area_id,
            'department'    => $department,
            'team'          => $team,
            'title'         => $title,
            'off'           => $off,
            'contract_term' => $contract_term,
            'company_id'    => $company_id,
            'month'         => $month,
            'ids'           => $ids,
            'confirm_date'  => date('m/Y'),
            'back_url'      => $back_url,
        );

        if ($btnSetPrintStatus) {
            $this->_setPrintStatusAppendix($params);
        }
        if ($btnSetDisable) {
            $this->_setDisable($params);
        }
        if ($update_contract) {
            $this->_updateContract($params);
            exit();
        }
        if ($update_contract_probation) {
            $this->_updateContractProbation($params);
        }

        $limit = 300;
        $total = 0;
        $desc  = $this->getRequest()->getParam('desc', 1);
        $sort  = $this->getRequest()->getParam('sort');
        $page  = $this->getRequest()->getParam('page', 1);

        if ($export) {
            $limit = $page  = 0;
        }



        $staff_contract_model = new Application_Model_StaffContract();
        $list                 = $staff_contract_model->AppendixPrint($page, $limit, $total, $params);

        if ($export) {
            $this->_exportRenew($list);
//            $this->_exportInfo($list);
        }

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error   = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract-new/print-appendix' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;


        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;


        $QContractType             = new Application_Model_ContractTypes();
        $QContractType             = $QContractType->get_cache();
        $this->view->contractTerms = $QContractType;

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all();
        $this->view->provinces = $provinces;

        $QTeam                                   = new Application_Model_Team();
        $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->userStorage                 = $userStorage;


        $area_id_confirm  = array();
        $staff_title_info = $userStorage->title;
        $team_info        = $QTeam->find($staff_title_info);
        $team_info        = $team_info->current();
        $group_id         = $team_info['access_group'];

//        $group_id        = $userStorage->group_id;
        if ($group_id != HR_ID) {
            $sql      = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = " . $userStorage->id .
                    " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = " . $userStorage->id;
            $stmt1    = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();
            foreach ($area_tmp as $item) {
                $area_id_confirm[] = $item['area_id'];
            }
            $this->view->area_id_confirm = $area_id_confirm;
        }

        $teams = array();
        if ($department AND count($department) > 0) {
            foreach ($department as $key => $value) {
                $teams += $recursiveDeparmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if ($team AND $department AND count($team) > 0) {
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;
    }

    public function editContractAction() {
        $flashMessenger      = $this->_helper->flashMessenger;
        $contract_model      = new Application_Model_StaffContract();
        $team_model          = new Application_Model_Team();
        $company_model       = new Application_Model_Company();
        $contract_term_model = new Application_Model_ContractTypes();
        $contract            = $contract_model->getDetailForEdit($this->getParam('id'));

        if (empty($contract)) {
            $flashMessenger->setNamespace('error')->addMessage("Không tìm thấy hợp đồng hoặc hợp đồng không được phép chỉnh sửa");
            $this->_redirect("/contract-new/renewable");
        }
        $this->view->contract    = $contract;
        $dataTeam                = $team_model->get_cache();
        $this->view->team        = $dataTeam;
        $dataCompany             = $company_model->get_cache();
        $this->view->company     = $dataCompany;
        $dataContract            = $contract_term_model->get_cache();
        $this->view->newContract = $dataContract;

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $list_all_reg    = $QRegionalMarket->nget_all_province_with_area_cache();

        $this->view->regional_market_list = $list_all_reg;

        $parent = $contract_model->getParentContract($contract[0]['staff_id']);

        $this->view->parent = $parent;
    }

    public function editAppendixAction() {
        $flashMessenger      = $this->_helper->flashMessenger;
        $contract_model      = new Application_Model_StaffContract();
        $team_model          = new Application_Model_Team();
        $company_model       = new Application_Model_Company();
        $contract_term_model = new Application_Model_ContractTypes();
        $contract            = $contract_model->getDetailAppendixForEdit($this->getParam('id'));
        if (empty($contract)) {
            $flashMessenger->setNamespace('error')->addMessage("Không tìm thấy hợp đồng hoặc hợp đồng không được phép chỉnh sửa");
            $this->_redirect("/contract-new/renewable");
        }
        $this->view->contract    = $contract;
        $dataTeam                = $team_model->get_cache();
        $this->view->team        = $dataTeam;
        $dataCompany             = $company_model->get_cache();
        $this->view->company     = $dataCompany;
        $dataContract            = $contract_term_model->get_cache();
        $this->view->newContract = $dataContract;

        $parent = $contract_model->getParentContract($contract[0]['staff_id']);

        $this->view->parent = $parent;
    }

    public function createNewContractAction() {

        $flashMessenger      = $this->_helper->flashMessenger;
        $contract_model      = new Application_Model_StaffContract();
        $team_model          = new Application_Model_Team();
        $company_model       = new Application_Model_Company();
        $contract_term_model = new Application_Model_ContractTypes();
        $contract            = $contract_model->getDetailForNew($this->getParam('id'));


        if (empty($contract)) {
            $flashMessenger->setNamespace('error')->addMessage("Hợp đồng không phải là hợp đồng mới nhất của nhân viên");
            $this->_redirect("/contract-new/history");
        }
        $this->view->contract    = $contract;
        $dataTeam                = $team_model->get_cache_title();
        $this->view->team        = $dataTeam;
        $dataCompany             = $company_model->get_cache();
        $this->view->company     = $dataCompany;
        $dataContract            = $contract_term_model->get_cache();
        $this->view->newContract = $dataContract;

        $parent                      = $contract_model->getParentContract($contract[0]['staff_id']);
        $this->view->parent          = $parent;
        $regional_market_model       = new Application_Model_RegionalMarket();
        $regional_market             = $regional_market_model->nget_all_province_cache();
        $this->view->regional_market = $regional_market;


        $district             = $regional_market_model->nget_district_by_province_cache($contract[0]['regional_market']);
        $this->view->district = $district;



        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;
        $QHospital                    = new Application_Model_Hospital();
        $hospitals                    = $QHospital->get_all(array('province_id' => $contract[0]['province_id']));

        $this->view->hospitals = $hospitals;
    }

    public function processCreateAction() {
        $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
        $flashMessenger      = $this->_helper->flashMessenger;
        $userStorage         = Zend_Auth::getInstance()->getStorage()->read();
        $contract_model      = new Application_Model_StaffContract();
        $params_post         = $this->_request->getParams();
        $db                  = Zend_Registry::get('db');
        if ($this->_request->isPost()) {
            try {
                $db->beginTransaction();
                $now = date('Y-m-d H:i:s');

                $Qstaff_model = new Application_Model_Staff();
                $QStaffSalary = new Application_Model_StaffSalary();
                $QStaff       = new Application_Model_Staff();
                $staff_row    = $Qstaff_model->findStaffid($params_post['staff_id']);

                if (empty($staff_row)) {
                    throw new Exception("Không tìm thấy thông tin nhân viên này");
                }
                if (empty($params_post['from_date'])) {
                    throw new Exception("Vui lòng chọn này hiệu lực");
                } else {
                    $from_date          = DateTime::createFromFormat('d/m/Y', $params_post['from_date']);
                    $from_date_formated = $from_date->format('Y-m-d');
                }
                $data_insert = array(
                    'from_date'             => $from_date_formated,
                    'title'                 => intval($params_post['title']),
                    'regional_market'       => intval($params_post['regional_market']),
                    'is_next'               => 2,
                    'staff_id'              => $staff_row['id'],
                    'company_id'            => intval($params_post['company_id']),
                    'contract_term'         => intval($params_post['contract_term']),
                    'salary_insurance_temp' => intval($params_post['salary_insurance_temp']),
                    'work_cost_temp'        => intval($params_post['work_cost_temp']),
                    'salary_total'          => intval($params_post['salary_total']),
                    'salary_bonus'          => intval($params_post['salary_bonus']),
                    'salary_min'            => intval($params_post['salary_min']),
                    'salary_kpi'            => intval($params_post['salary_kpi']),
                    'district_id'           => intval($params_post['district_id']),
                    'system_note'           => 'Manual Created at ' . $now . ' by ' . $userStorage->id,
                    'print_type'            => 1,
                    'print_status'          => 1,
                    'status'                => 2,
                    'note'                  => $params_post['note'],
                    'hospital_id'           => intval($staff_row['hospital_id']),
                    'created_at'            => $now,
                    'created_by'            => $userStorage->id,
                );
                if (in_array($params_post['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER , PGPG Brandshop
                    $salary_policy_data = $contract_model->getSalaryDetailByTitleDistrict(182, $params_post['district_id']);
                } elseif (in_array($params_post['title'], array(183, 164))) { //183, 164, 725, 162, 190, 312
                    $salary_policy_data = $contract_model->getSalaryDetailByTitleDistrict(183, $params_post['district_id']);
                } elseif (in_array($params_post['title'], array(417, 725, 799, 424))) {
                    $salary_policy_data = $contract_model->getSalaryDetailByTitleDistrict($params_post['title'], $params_post['district_id']);
                }
                if (!empty($salary_policy_data)) {
                    $data_insert['salary_policy_detail_id'] = intval($salary_policy_data['id']);
                }
                if (!empty($params_post['send_letter']) && $params_post['send_letter'] != '01-01-1970') {
                    $send_letter                = DateTime::createFromFormat('d/m/Y', $params_post['send_letter']);
                    $data_insert['send_letter'] = $send_letter->format('Y-m-d');
                }

                if (!empty($params_post['to_date']) && $params_post['to_date'] != '01-01-1970') {
                    $to_date                = DateTime::createFromFormat('d/m/Y', $params_post['to_date']);
                    $data_insert['to_date'] = $to_date->format('Y-m-d');
                }
                
                // check thuoc truong hợp edit probation của transfer sinh hđ 2thang + phụ lục
                $data_appendix_pro = $contract_model->getAppendixByProbation($params_post['id']);
                
                // update het luong cu neu co
                $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('staff_id = ?', $staff_row['id']);
                $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('to_date is NULL');
                $data_update_salary   = array('to_date' => $data_insert['from_date']);
                $QStaffSalary->update($data_update_salary, $where_staff_salary);
                // insert luong moi

                $data_salary = array(
                    'insurance_salary' => intval($params_post['salary_insurance_temp']),
                    'work_cost_salary' => intval($params_post['work_cost_temp']),
                    'note'             => 'Manual Created at ' . $now . ' by ' . $userStorage->id,
                    'from_date'        => $data_insert['from_date'],
                    'created_at'       => $now,
                    'created_by'       => $userStorage->id,
                    'staff_id'         => $staff_row['id'],
                );
                $salary_id   = $QStaffSalary->insert($data_salary);
                if (empty($salary_id)) {
                    throw new Exception("Tạo lương mới không thành công");
                }
                $data_insert['salary_id'] = $salary_id;

                if(!empty($data_appendix_pro)){
                    $data_insert['parent_id'] = $data_appendix_pro['parent_id'];
                }
                $contract_new_id      = $contract_model->insert($data_insert);
                //het han hop dong hien tai
                $data_update          = array(
//                    'to_date' => $data_insert['from_date'],
                    'is_next'    => 0,
                    'is_disable' => 1,
                    'is_expired' => 1,
                );
                $where_staff_contract = $contract_model->getAdapter()->quoteInto('id = ?', $params_post['id']);
                $contract_model->update($data_update, $where_staff_contract);

                /// Cập nhật thời gian hợp đồng hiện tại cho bảng staff
                $where_staff       = array();
                $where_staff[]     = $QStaff->getAdapter()->quoteInto('id = ?', $staff_row['id']);
                $data_update_staff = array(
                    'contract_signed_at'  => $data_insert['from_date'],
                    'contract_expired_at' => $data_insert['to_date'],
                    'contract_term'       => $data_insert['contract_term']
                );
                $QStaff->update($data_update_staff, $where_staff);
                if (empty($contract_new_id)) {
                    throw new Exception("Tạo hợp đồng mới không thành công");
                }
                
                if(empty($data_appendix_pro)){
                    // het han next cu
                    $data_update2            = array(
                        'is_expired' => 1
                    );
                    $where_staff_contract2   = array();
                    $where_staff_contract2[] = $contract_model->getAdapter()->quoteInto('staff_id = ?', $staff_row['id']);
                    $where_staff_contract2[] = $contract_model->getAdapter()->quoteInto('is_next = 1');
                    $contract_model->update($data_update2, $where_staff_contract2);
                    // insert hop dong next moi
                    $contract_model->createNextContractByStaff($staff_row['id']);
                }
                $contract_model->UpdateNextContractByStaff($staff_row['id']);
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage("Thành công");
            } catch (Exception $ex) {
                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage($ex->getMessage());
                $this->_redirect("/contract-new/history?code=" . $params_post['code']);
            }
        } else {
            $flashMessenger->setNamespace('error')->addMessage("Sai method");
            $this->_redirect("/contract-new/history?code=" . $params_post['code']);
        }
        $this->_redirect("/contract-new/print-renewable?code=" . $params_post['code']);
    }

    public function processeditAction() {
        $flashMessenger      = $this->_helper->flashMessenger;
        //contract
        $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
        $userStorage         = Zend_Auth::getInstance()->getStorage()->read();
        $contract_model      = new Application_Model_StaffContract();

        if ($this->_request->isPost()) {
            try {
                $now         = date('Y-m-d H:i:s');
                $params_post = $this->_request->getParams();
                $id          = $params_post['id'];
                $edit        = $contract_model->fetchRow($id);
                if (empty($edit)) {
                    throw new Exception("Không tìm thấy hợp đồng này.!");
                }
                $edit_arr = $edit->toArray();


                $data_update = array(
                    'from_date'             => $params_post['from_date'],
                    'regional_market'       => $params_post['regional_market'],
                    'title'                 => $params_post['title'],
                    'contract_term'         => $params_post['contract_term'],
                    'salary_insurance_temp' => intval($params_post['salary_insurance_temp']),
                    'work_cost_temp'        => intval($params_post['work_cost_temp']),
                    'salary_total'          => intval($params_post['salary_total']),
                    'salary_bonus'          => intval($params_post['salary_bonus']),
                    'salary_min'            => intval($params_post['salary_min']),
                    'salary_kpi'            => intval($params_post['salary_kpi']),
                    'system_note'           => 'Edit and Updated at ' . $now . ' by ' . $userStorage->id
                );
                if (in_array($params_post['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER , PGPG Brandshop
                    $salary_policy_data = $contract_model->getSalaryDetailForPGByNew($staff_row['id']);
                } elseif (in_array($params_post['title'], array(183, 164))) { //183, 164, 725, 162, 190, 312
                    $where_PolicyDetail   = array();
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(183));
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $params_post['regional_market']);
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                    $salary_policy_data   = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                } elseif (in_array($params_post['title'], array(417, 725, 799, 424))) {
                    $where_PolicyDetail   = array();
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id = ?', $params_post['title']);
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $params_post['regional_market']);
                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                    $salary_policy_data   = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                }
                if (!empty($salary_policy_data)) {
                    $data_update['salary_policy_detail_id'] = intval($salary_policy_data['id']);
                }
                if (!empty($params_post['from_date']) && $params_post['from_date'] != '01-01-1970') {

                    $from_date                = DateTime::createFromFormat('d/m/Y', $params_post['from_date']);
                    $data_update['from_date'] = $from_date->format('Y-m-d');
                }
                if (!empty($params_post['to_date']) && $params_post['to_date'] != '01-01-1970') {
                    $to_date                = DateTime::createFromFormat('d/m/Y', $params_post['to_date']);
                    $data_update['to_date'] = $to_date->format('Y-m-d');
                }
                $where = $contract_model->getAdapter()->quoteInto("id= ?", $id);
                $contract_model->update($data_update, $where);

                $flashMessenger->setNamespace('success')->addMessage("Cập nhật thành công thông tin hợp đồng");
                $messages_success = $flashMessenger->setNamespace('success')->getMessages();
                $messages_error   = $flashMessenger->setNamespace('error')->getMessages();
            } catch (Exception $ex) {
                $flashMessenger->setNamespace('error')->addMessage($ex->getMessage());
            }
        } else {
            $flashMessenger->setNamespace('error')->addMessage("Sai method");
        }
        $this->redirect('contract-new/renewable');
    }

    public function processeditappendixAction() {
        $flashMessenger      = $this->_helper->flashMessenger;
        //contract
        $userStorage         = Zend_Auth::getInstance()->getStorage()->read();
        $contract_model      = new Application_Model_StaffContract();
        $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
        $salary_model        = new Application_Model_StaffSalary();
        // $edit = $contract->findPrimaykey($this->getParam('id'));
        if ($this->_request->isPost()) {
            try {
                $now          = date('Y-m-d H:i:s');
                $id           = $this->getParam('id');
                $params_post  = $this->_request->getParams();
                $old_appendix = $contract_model->getDetailAppendixForEdit($id);
                if (!empty($old_appendix)) {
                    $data_update = array(
                        'from_date'    => $params_post['from_date'],
                        'title'        => $params_post['title'],
//                    'contract_term'         => $params_post['contract_term'],
//                    'salary_insurance_temp' => intval($params_post['salary_insurance_temp']),
//                    'work_cost_temp'        => intval($params_post['work_cost_temp']),
                        'salary_total' => intval($params_post['salary_total']),
                        'salary_bonus' => intval($params_post['salary_bonus']),
                        'salary_min'   => intval($params_post['salary_min']),
                        'salary_kpi'   => intval($params_post['salary_kpi']),
                        'system_note'  => 'Edit and Updated manualy at ' . $now . ' by ' . $userStorage->id
                    );
                    if (in_array($params_post['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER , PGPG Brandshop
                        $salary_policy_data = $contract_model->getSalaryDetailForPGByNew($staff_row['id']);
                    } elseif (in_array($params_post['title'], array(183, 164))) { //183, 164, 725, 162, 190, 312
                        $where_PolicyDetail   = array();
                        $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(183));
                        $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $params_post['regional_market']);
                        $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                        $salary_policy_data   = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                    } elseif (in_array($params_post['title'], array(417, 725, 799, 424))) {
                        $where_PolicyDetail   = array();
                        $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id = ?', $params_post['title']);
                        $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $params_post['regional_market']);
                        $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                        $salary_policy_data   = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                    }
                    if (!empty($salary_policy_data)) {
                        $data_update['salary_policy_detail_id'] = intval($salary_policy_data['id']);
                    }
                    if (!empty($params_post['from_date']) && $params_post['from_date'] != '01-01-1970') {

                        $from_date                = DateTime::createFromFormat('d/m/Y', $params_post['from_date']);
                        $data_update['from_date'] = $from_date->format('Y-m-d');
                    }
                    if (!empty($params_post['to_date']) && $params_post['to_date'] != '01-01-1970') {
                        $to_date                = DateTime::createFromFormat('d/m/Y', $params_post['to_date']);
                        $data_update['to_date'] = $to_date->format('Y-m-d');
                    }

                    $where       = $contract_model->getAdapter()->quoteInto("id= ?", $id);
                    $contract_model->update($data_update, $where);
                    $data_salary = array(
                        'insurance_salary' => intval($params_post['salary_insurance']),
                        'work_cost_salary' => intval($params_post['work_cost']),
                        'note'             => 'Edit and Updated manualy at ' . $now . ' by ' . $userStorage->id
                    );

                    $where_salary     = $salary_model->getAdapter()->quoteInto("id= ?", $old_appendix[0]['salary_id']);
                    $salary_model->update($data_salary, $where_salary);
                    $flashMessenger->setNamespace('success')->addMessage("Cập nhật thành công thông tin phụ lục");
                    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
                    $messages_error   = $flashMessenger->setNamespace('error')->getMessages();
                } else {
                    throw new Exception("Phụ lục không hợp lệ");
                }
            } catch (Exception $ex) {
                $flashMessenger->setNamespace('error')->addMessage($ex->getMessage());
            }
        } else {
            $flashMessenger->setNamespace('error')->addMessage("Sai method");
        }
        $this->redirect('contract-new/print-appendix');
    }

    public function changeSeniorityAllowanceAction() {
        
    }

    public function saveChangeSeniorityAllowanceAction() {
        // config for template
        define('START_ROW', 2);
        define('STAFF_CODE', 0);
        define('SALARY', 2);
        define('FROM_DATE', 3);
        define('TO_DATE', 4);
        define('TITLE', 5);
        define('INSURANCE_SALARY', 6);
        define('PROVINCEID', 7);
        define('DISTRICTID', 8);
        define('SALARY_TOTAL', 9);
        define('LUNCH', 10);
        define('FUEL', 11);
        define('PHONE', 12);


        $this->_helper->layout->disableLayout();
        $QStaff = new Application_Model_Staff();
        if ($this->getRequest()->getMethod() == 'POST') { // Big IF
            set_time_limit(0);
            ini_set('memory_limit', -1);
            // file_put_contents(APPLICATION_PATH.'/../public/files/mou/lock', '1');
            $progress = new My_File_Progress('parent.set_progress');
            $progress->flush(0);

            $save_folder   = 'mou';
            $new_file_path = '';
            $requirement   = array(
                'Size'      => array('min' => 5, 'max' => 5000000),
                'Count'     => array('min' => 1, 'max' => 1),
                'Extension' => array('xls', 'xlsx'),
            );
            $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
            try {

                $file = My_File::get($save_folder, $requirement, true);

                if (!$file || !count($file))
                    throw new Exception("Upload failed");



                $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                        // . DIRECTORY_SEPARATOR . $userStorage->id
                        . DIRECTORY_SEPARATOR . $file['folder'];

                $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

//                $uniqid = $file['folder'];
//                 My_File_Progress::set('mou', 0);
            } catch (Exception $e) {
                $this->view->errors = $e->getMessage();
                return;
            }
            //read file
            include 'PHPExcel/IOFactory.php';
            //  Read your Excel workbook
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
//                $path_info = pathinfo($inputFileName);
//                $extension = $path_info['extension'];
                $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            } catch (Exception $e) {

                $this->view->errors = $e->getMessage();
                return;
            }

            //  Get worksheet dimensions
            $sheet           = $objPHPExcel->getSheet(0);
            $highestRow      = $sheet->getHighestRow();
            $highestColumn   = $sheet->getHighestColumn();
            /**
             * Danh sách các đơn hàng lỗi
             * @var array
             */
            $error_list      = array();
            $success_list    = array();
            $store_code_list = array();
            $number_of_order = 0;
            $total_value     = 0;
            $total_order_row = 0;
            $order_list      = array();

            $arr_inserts   = array();
            $arr_order_ids = array();

            $_failed         = false;
            $total_row       = array();
            $data_staff_code = [];

            /**
             * @author: hero
             * valid data
             * description
             * $date
             */
            for ($row = START_ROW; $row <= $highestRow; $row++) {

                try {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                    /**
                     * @author: hero
                     * valid data
                     * description
                     * $date
                     */
                    $rowData = isset($rowData[0]) ? $rowData[0] : array();
                    if (empty($rowData[STAFF_CODE])) {
                        throw new Exception("Empty STAFF_CODE, row: " . $row . ".Data:" . implode(",", $rowData));
                    }
//                    if (empty($rowData[SALARY]) && empty($rowData[INSURANCE_SALARY])) {
//                        throw new Exception("Empty SALAY, row: " . $row . ".Data:" . implode(",", $rowData));
//                    }
                    if (empty($rowData[FROM_DATE])) {
                        throw new Exception("Empty FROM_DATE, row: " . $row . ".Data:" . implode(",", $rowData));
                    }
//                     if (empty($rowData[TITLE])) {
//                        throw new Exception("Empty TITLE, row: " . $row . ".Data:" . implode(",", $rowData));
//                    }
//                      if (empty($rowData[PROVINCEID])) {
//                        throw new Exception("Empty PROVINCE ID, row: " . $row . ".Data:" . implode(",", $rowData));
//                    }
//                    if (empty($rowData[TITLE])) {
//                        throw new Exception("Empty TITLE, row: " . $row . ".Data:" . implode(",", $rowData));
//                    }
//                    $from_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[FROM_DATE]));


                    $date_gd = DateTime::createFromFormat('d/m/Y', $rowData[FROM_DATE]);

//                        $from_date      = $date_gd->format('Y-m-d',null);
                    $from_date = date_format($date_gd, "Y-m-d");

                    if (empty($from_date)) {
                        throw new Exception("Wrong date format, row: " . $row . ".Data:" . implode(",", $rowData));
                    }
                    $data_staff_code[] = trim($rowData[STAFF_CODE]);
                } catch (Exception $e) {
                    $this->view->errors = $e->getMessage();
                    return;
                }




                // nothing here
            } // END loop through order rows

            $progress->flush(30);
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            try {
                if (!empty($data_staff_code)) {


                    $select      = $db->select()
                            ->from(array('p' => 'staff'), array('p.code'))
                            ->where('p.code in (?)', $data_staff_code);
//                            ->where('p.status <> 0');
                    $result_code = $db->fetchCol($select);

                    if (count($data_staff_code) <> count($result_code)) {
                        $diff = array_diff($data_staff_code, $result_code);
                        throw new Exception("Nhân viên không tồn tại hoặc đã nghỉ việc code: " . implode($diff, ','));
                    }

                    $QStaff              = new Application_Model_Staff();
                    $QStaffSalary        = new Application_Model_StaffSalary();
                    $QStaffContract      = new Application_Model_StaffContract();
                    $QSalaryPolicyDetail = new Application_Model_SalaryPolicyDetail();
                    $QSalaryByStaff      = new Application_Model_SalaryByStaff;
                    $created_at          = date('Y-m-d H:i:s');
                    $created_by          = $userStorage->id;
                    $arr_none_office     = array(182, 183, 190, 162, 164, 293, 312, 295, 730, 725, 799, 732, 424, 417, 731 , 798, 577);


                    //disable hop dong

                    $select_contract = $db->select()
                            ->from(array('p' => 'v_staff_contract_insurance'), array('p.code'))
//                            ->join(array('s' => 'staff'), 'p.staff_id = s.id', array())
                            ->where('p.code in (?)', $data_staff_code)
//                            ->where('p.is_next =2 and p.is_expired=0')
//                            ->where('p.is_expired=0 and p.is_disable=0 and p.is_next <>1')
                            ->where('p.from_date <= ?', $from_date)
                            ->where('(p.to_date >= ?', $from_date)
                            ->orWhere('p.to_date IS NULL )', NULL)
                            ->group('p.code');

                    $current_contract_array = $db->fetchCol($select_contract);

                    if (count($data_staff_code) <> count($current_contract_array)) {
                        $diff = array_diff($data_staff_code, $current_contract_array);
                        throw new Exception("Nhân viên không có hợp đồng hiện tại: " . implode($diff, ','));
                    }

                    $list_append      = array();
                    $list_salary      = array();
                    $QRegional_market = new Application_Model_RegionalMarket;
                    for ($row = START_ROW; $row <= $highestRow; $row++) {

                        $rowData       = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData       = isset($rowData[0]) ? $rowData[0] : array();
                        $where_staff   = array();
                        $where_staff[] = $QStaff->getAdapter()->quoteInto('code = ?', trim($rowData[STAFF_CODE]));
                        $Staff_Row     = $QStaff->fetchRow($where_staff);
                        $work_cost     = intval($rowData[SALARY]);
//                        $insurance_salary = intval($rowData[INSURANCE_SALARY]);

                        $date_gd   = DateTime::createFromFormat('d/m/Y', $rowData[FROM_DATE]);
                        $from_date = date_format($date_gd, "Y-m-d");
                        if (!empty($rowData[TITLE])) {
                            $Staff_Row['title'] = $rowData[TITLE];
                        }
                        if (!empty($rowData[DISTRICTID])) {
                            $Staff_Row['district'] = $rowData[DISTRICTID];
                            $data_staff_update     = array(
                                'district' => $rowData[DISTRICTID]
                            );
                            $QStaff->update($data_staff_update, $where_staff);
                        }

                        if (!empty($rowData[PROVINCEID])) {
                            $Staff_Row['regional_market'] = $rowData[PROVINCEID];
                        }

                        /**
                         * @author: hero
                         * Điều chỉnh tổng lương
                         * description
                         * 6/27/19 10:07 AM
                         */
                        if (!empty($rowData[SALARY_TOTAL])) {
                            $where_staff_salary   = [];
                            $where_staff_salary[] = $QSalaryByStaff->getAdapter()->quoteInto("staff_id = ? ", $Staff_Row['id']);
                            $where_staff_salary[] = $QSalaryByStaff->getAdapter()->quoteInto("to_date IS NULL");

                            //update tong luong
                            $data_salary_update = array(
                                "to_date" => $from_date
                            );

                            $QSalaryByStaff->update($data_salary_update, $where_staff_salary);

                            // insert luong moi
                            $data_staff_salary_insert = array(
                                'staff_id'     => $Staff_Row['id'],
                                'staff_code'   => trim($rowData[STAFF_CODE]),
                                'total_salary' => intval($rowData[SALARY_TOTAL]),
                                'from_date'    => $from_date,
                                'note'         => 'create by massupload.user_id:' . $created_by . "_at_" . $created_at,
                                'created_at'   => $created_at,
                                'created_by'   => $created_by
                            );
                            if ($rowData[LUNCH] >= 0) {
                                $data_staff_salary_insert['lunch'] = $rowData[LUNCH];
                                $data_staff_salary_insert['fuel']  = $rowData[FUEL];
                                $data_staff_salary_insert['phone'] = $rowData[PHONE];
                            }
                            $QSalaryByStaff->insert($data_staff_salary_insert);
                        }

                        if (!empty($work_cost) || !empty($rowData[INSURANCE_SALARY]) || empty($rowData[SALARY_TOTAL])) {
                            $select_contract = $db->select()
                                    ->from(array('p' => 'staff_contract'), array('p.*', 'v.max_work_cost', 'sale_work_cost' => 'vs.max_work_cost'))
                                    ->joinleft(array('v' => 'v_max_seniority'), 'v.staff_id = p.staff_id', array())
                                    ->joinleft(array('vs' => 'v_max_seniority_sale'), 'vs.staff_id = p.staff_id', array())
                                    ->where('p.staff_id = ?', $Staff_Row['id'])
//                                ->where('p.is_next =2 and p.is_expired=0')
                                    ->where('p.is_expired=0 and p.is_disable=0 and p.is_next <>1')
                                    ->where('p.from_date <= ?', $from_date)
                                    ->where('(p.to_date >= ?', $from_date)
                                    ->orWhere('p.to_date IS NULL )', NULL)
                                    ->order('p.from_date DESC');

                            $current_contract_array = $db->fetchRow($select_contract);
                            if ($current_contract_array['v.max_work_cost'] > $work_cost && in_array($Staff_Row['title'], array(182, 293, 730, 732, 725 , 799))) {
                                $work_cost = intval($current_contract_array['v.max_work_cost']);
                            }
                            if ($current_contract_array['sale_work_cost'] > $work_cost && in_array($Staff_Row['title'], array(183))) {
                                $work_cost = intval($current_contract_array['sale_work_cost']);
                            }

                            $select_contract_parent  = $db->select()
                                    ->from(array('p' => 'staff_contract'), array('p.*'))
                                    ->where('p.staff_id = ?', $Staff_Row['id'])
                                    ->where('p.is_expired=0 and p.is_disable=0 and p.print_type=1', NULL)
                                    ->where('p.is_next =2', NULL)
                                    ->where('p.from_date <= ?', $from_date)
                                    ->where('(p.to_date >= ?', $from_date)
                                    ->orWhere('p.to_date IS NULL )', NULL);
                            $current_contract_parent = $db->fetchRow($select_contract_parent);
//                            $current_contract_parent = $current_contract_array;
                            if (empty($current_contract_parent)) {
                                throw new Exception("Nhân viên không có hợp đồng hiệu lực hiện tại: " . $Staff_Row['code']);
                            }
//                        if (!empty($current_contract_array)) {
                            // tat ca cac truong hop deu phai update luong cu
                            $where_staff_salary   = [];
                            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('staff_id = ?', $Staff_Row['id']);
                            $where_staff_salary[] = $QStaffSalary->getAdapter()->quoteInto('to_date is NULL');
                            $data_update_salary   = array('to_date' => $from_date);



                            $QStaffSalary->update($data_update_salary, $where_staff_salary);

                            if (empty($Staff_Row['district'])) {
                                // neu ko co quan huyen thi lay defauit
                                $where_regional_market    = array();
                                $where_regional_market[]  = $QRegional_market->getAdapter()->quoteInto('parent = ?', $Staff_Row['regional_market']);
                                $where_regional_market[]  = $QRegional_market->getAdapter()->quoteInto('default_contract_district = ?', 1);
                                $regional_market_district = $QRegional_market->fetchRow($where_regional_market);
                                $Staff_Row['district']    = $regional_market_district['id'];
                            }
                            //end update luong cu
                            // end insert luong moi
                            // tinh luong moi
                            if (in_array($Staff_Row['title'], $arr_none_office)) {
                                if (in_array($Staff_Row['title'], array(182, 293, 730, 732))) { //PGPB & SENIOR PROMOTER  (chính sách PG)
                                    $salary_policy_data = $QStaffContract->getSalaryDetailByTitleDistrict(182, $Staff_Row['district']);
                                    if (empty($salary_policy_data)) {
                                        throw new Exception('Không tìm thấy chính sách lương PG cho nhân viên ' . $Staff_Row['code'] . ' này');
                                    }
                                    $salary_policy_filter    = $salary_policy_data['salary_policy_id'];
                                    $PolicyDetail_Row        = $salary_policy_data;
                                    $salary_basic            = intval($PolicyDetail_Row['basic_company_salary']);
                                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);



                                    if (in_array($salary_policy_filter, array(5))) {
                                        $allowance    = $PolicyDetail_Row['allowance'] + $work_cost;
                                        $salary_kpi   = $salary_basic;
                                        $salary_total = $salary_basic + $work_cost;
                                    } else {
                                        if ($salary_insurance_salary >= ($salary_basic + $work_cost)) {
                                            $allowance = 0;
                                        } else {
                                            $allowance = $salary_basic + $work_cost - $salary_insurance_salary;
                                        }
                                        if (($salary_basic + $work_cost) >= $salary_insurance_salary) {
                                            $salary_kpi = 0;
                                        } else {
                                            $salary_kpi = $salary_insurance_salary - $salary_basic - $work_cost;
                                        }
                                        $salary_total = $salary_insurance_salary + $allowance;
                                    }

                                    $allowance1    = intval($PolicyDetail_Row['allowance1']);
                                    $kpi1          = intval($PolicyDetail_Row['salary_kpi1']);
                                    $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                                } elseif (in_array($Staff_Row['title'], array(183, 164, 162))) { //Sale, Sale Access, Sale Leader Access ( chính sách sale)
                                    $where_PolicyDetail   = array();
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(183));
//                                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $Staff_Row['regional_market']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('district_id = ?', $Staff_Row['district']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                                    $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);

                                    $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;
                                    if (!$policy_detail_id) {
                                        throw new Exception('Không tìm thấy chính sách lương SALE cho nhân viên ' . $Staff_Row['code'] . ' này');
                                    }

                                    $salary_basic            = intval($PolicyDetail_Row['basic_company_salary']);
                                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                                    $salary_kpi              = intval($PolicyDetail_Row['salary_kpi']);
                                    $allowance               = intval($PolicyDetail_Row['allowance']) + $work_cost;

                                    $salary_total = $PolicyDetail_Row['total_company_salary'] + $work_cost;

                                    $allowance1    = intval($PolicyDetail_Row['allowance1']);
                                    $kpi1          = intval($PolicyDetail_Row['salary_kpi1']);
                                    $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                                } elseif (in_array($Staff_Row['title'], array(417, 731, 798, 577))) { //Sale, Sale Access, Sale Leader Access ( chính sách sale)
                                    $where_PolicyDetail   = array();
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id = ?', $Staff_Row['title']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('district_id = ?', $Staff_Row['district']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                                    $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);

                                    $policy_detail_id = (isset($PolicyDetail_Row['id']) and $PolicyDetail_Row['id']) ? $PolicyDetail_Row['id'] : 0;
                                    if (!$policy_detail_id) {
                                        throw new Exception('Không tìm thấy chính sách lương PG LEADER cho nhân viên ' . $Staff_Row['code'] . ' này');
                                    }

                                    $salary_basic            = intval($PolicyDetail_Row['basic_company_salary']);
                                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                                    $salary_kpi              = intval($PolicyDetail_Row['salary_kpi']);
                                    $allowance               = intval($PolicyDetail_Row['allowance']);
                                    $salary_total            = $PolicyDetail_Row['total_company_salary'];

                                    $allowance1    = intval($PolicyDetail_Row['allowance1']);
                                    $kpi1          = intval($PolicyDetail_Row['salary_kpi1']);
                                    $salary_total1 = intval($PolicyDetail_Row['total_salary1']);
                                } elseif (in_array($Staff_Row['title'], array(190))) { //Sale leader (chính sách sale leader)
                                    $salary_insurance_salary = 6000000;
                                    $salary_kpi              = 6000000;
                                    $salary_basic            = 6000000;
                                    $allowance               = 0;
                                    $salary_total            = 6000000;

                                    $allowance1    = 0;
                                    $kpi1          = 0;
                                    $salary_total1 = 6000000;
                                } elseif (in_array($Staff_Row['title'], array(725, 799))) {

                                    //Store leader (chính sách store leader )  // lương KPI=6tr, tổng lương 6tr
                                    $where_PolicyDetail   = array();
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', $Staff_Row['title']);
//                                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $Staff_Row['regional_market']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('district_id = ?', $Staff_Row['district']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                                    $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);

                                    $policy_detail_id = intval($PolicyDetail_Row['id']);
                                    if (!$policy_detail_id) {
                                        throw new Exception('Không tìm thấy chính sách lương STORE LEADER cho nhân viên ' . $Staff_Row['code'] . ' này');
                                    }

                                    $salary_basic            = intval($PolicyDetail_Row['basic_company_salary']);
                                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                                    $salary_kpi              = intval($PolicyDetail_Row['salary_kpi']);
                                    $allowance               = intval($PolicyDetail_Row['allowance']);
                                    $salary_total            = $PolicyDetail_Row['total_company_salary'];
                                    $allowance1              = intval($PolicyDetail_Row['allowance1']);
                                    $kpi1                    = intval($PolicyDetail_Row['salary_kpi1']);
                                    $salary_total1           = intval($PolicyDetail_Row['total_salary1']);
                                } elseif (in_array($Staff_Row['title'], array(424))) { // sale leader BS
                                    $where_PolicyDetail   = array();
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('title_id IN (?)', array(424));
//                                $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('province_id = ?', $Staff_Row['regional_market']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('district_id = ?', $Staff_Row['district']);
                                    $where_PolicyDetail[] = $QSalaryPolicyDetail->getAdapter()->quoteInto('to_date is NULL');
                                    $PolicyDetail_Row     = $QSalaryPolicyDetail->fetchRow($where_PolicyDetail);
                                    $policy_detail_id     = intval($PolicyDetail_Row['id']);
                                    if (!$policy_detail_id) {
                                        throw new Exception('Không tìm thấy chính sách lương SALE LEADER cho nhân viên ' . $Staff_Row['code'] . ' này');
                                    }
                                    $salary_basic            = intval($PolicyDetail_Row['basic_company_salary']);
                                    $salary_insurance_salary = intval($PolicyDetail_Row['basic_insurance_salary']);
                                    $salary_kpi              = intval($PolicyDetail_Row['salary_kpi']);
                                    $allowance               = intval($PolicyDetail_Row['allowance']);
                                    $salary_total            = $PolicyDetail_Row['total_company_salary'];
                                    $allowance1              = intval($PolicyDetail_Row['allowance1']);
                                    $kpi1                    = intval($PolicyDetail_Row['salary_kpi1']);
                                    $salary_total1           = intval($PolicyDetail_Row['total_salary1']);
                                }
                                //tao luong moi
                                if (in_array($current_contract_array['contract_term'], array(2, 3, 17))) {
                                    $salary_insurance_salary = 0;
                                }
                                $data_salary = array(
                                    'staff_id'         => $Staff_Row['id'],
                                    'insurance_salary' => $salary_insurance_salary,
                                    'work_cost_salary' => $work_cost,
                                    'from_date'        => $from_date,
                                    'note'             => 'create by massupload.user_id:' . $created_by . "_at_" . $created_at,
                                    'created_at'       => $created_at,
                                    'title_id'         => $Staff_Row['title'],
                                    'created_by'       => $created_by
                                );
                                $salary_id   = $QStaffSalary->insert($data_salary);
//                                $list_salFary[] = $data_salary;
                            } else {
                                // luong khoi none sale cuar Thi
                                //  luong moi theo luong cu
                                //tao luong moi
                                //insert luong moi
                                $data_salary = array(
                                    'from_date'        => $from_date,
                                    'insurance_salary' => $rowData[INSURANCE_SALARY],
                                    'work_cost_salary' => $rowData[SALARY],
                                    'staff_id'         => $Staff_Row['id'],
                                    'created_at'       => $created_at,
                                    'created_by'       => $created_by,
                                    'note'             => 'create by massupload.user_id:' . $created_by . "_at_" . $created_at
                                );

                                $salary_id    = $QStaffSalary->insert($data_salary);
//                                $list_salary[] = $data_salary;
                                $salary_kpi   = 0;
                                $salary_basic = 0;
                                $allowance    = 0;
                                $salary_total = $salary_basic + $allowance;
                            }
                            // end tinh luong moi
                            // tao phu luc 
                            //
                          
                            $data_appendix = array(
                                'staff_id'        => $Staff_Row['id'],
                                'from_date'       => $from_date,
                                'contract_term'   => $current_contract_array['contract_term'],
                                'to_date'         => empty($current_contract_array['to_date']) ? '2050-01-01' : $current_contract_array['to_date'],
                                'status'          => 2,
                                'print_type'      => 2,
                                'created_at'      => $created_at,
                                'created_by'      => $userStorage->id,
                                'regional_market' => $Staff_Row['regional_market'],
                                'title'           => $Staff_Row['title'],
                                'hospital_id'     => empty($Staff_Row['hospital_id']) ? 0 : $Staff_Row['hospital_id'],
                                'note'            => 'create by massupload.user_id:' . $created_by . "_at_" . $created_at,
                                'company_id'      => $current_contract_array['company_id'],
                                'parent_id'       => $current_contract_parent['id'],
                                'salary_total'    => $salary_total,
                                'salary_kpi'      => $salary_kpi,
                                'salary_bonus'    => $allowance,
                                'salary_id'       => $salary_id,
                                'district_id'     => intval($Staff_Row['district']),
                            );

                            if (in_array($current_contract_array['contract_term'], array(2, 3, 17))) {


                                $data_appendix['salary_total'] = $salary_total1;
                                $data_appendix['salary_kpi']   = $kpi1;
                                $data_appendix['salary_bonus'] = $allowance1;
                            }


                            if (!empty($PolicyDetail_Row)) {
                                $data_appendix['salary_policy_detail_id'] = $PolicyDetail_Row['id'];
                            } else {
                                $data_appendix['salary_policy_detail_id'] = 0;
                            }

//                            echo "<pre>";
//                            $QStaffContract->insert($data_appendix);
                            $list_append[] = $data_appendix;




                            $percent = round(50 + $row * 50 / count($data_staff_code), 1);
                            $progress->flush($percent);
                        }
                    }// end loop

                    if (!empty($list_append)) {
                        My_Controller_Action::insertAllrow($list_append, 'staff_contract');
                    }
                    //update 2050 to null
                    $where_staff_contract_null       = [];
                    $where_staff_contract_null[]     = $QStaffContract->getAdapter()->quoteInto('to_date = ?', '2050-01-01');
                    $data_update_staff_contract_null = array('to_date' => NULL);
                    $QStaffContract->update($data_update_staff_contract_null, $where_staff_contract_null);

                    //update next contract
                    $update_next = $QStaffContract->UpdateNextContractByListStaff(implode($data_staff_code, ","));
                    if (!$update_next) {
                        throw new Exception("Update next fail, row: " . $row . ".Data:" . implode(",", $rowData));
                    }
                } else {
                    throw new Exception("Danh sách nhân viên không hợp lệ");
                }
                $db->commit();
                $progress->flush(100);
                $this->view->success = "Thành công.Tất cả nhân viên đã được tạo phụ lục.";
            } catch (Exception $e) {
                $db->rollback();
                $this->view->errors = $e->getMessage();
                return;
            }
        }
    }

    public function _exportInfo($list) {
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        $QContractTerm = new Application_Model_ContractTerm();
        $contractTerms = $QContractTerm->get_cache();

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'STT',
            'CÔNG TY',
            'BỘ PHẬN',
            'MÃ NHÂN VIÊN',
            'HỌ TÊN',
            'CHỨC VỤ',
            'KHU VỰC',
            'TỈNH',
            'QUẬN HUYỆN',
            'HỢP ĐỒNG',
            'TỪ NGÀY',
            'ĐẾN NGÀY',
            'TỔNG LƯƠNG',
            'BẢO HIỂM',
            'PHỤ CẤP KHÁC',
            'KPI',
            'BỆNH VIỆN',
            'GHI CHÚ',
            'NGÀY VÀO LÀM',
            'NGÀY GỬI',
            'NGÀY NHẬN'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index         = 2;
        $stt           = 0;
        $QContractType = new Application_Model_ContractTypes();
        $QContractTerm = $QContractType->get_cache();

        //echo "<pre>";print_r($list);die;
        foreach ($list as $key => $value):
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit(++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['company_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['department']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['full_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['tinh']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['quan_huyen']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($QContractTerm[$value['contract_term']]), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['from_date']) ? date('d/m/Y', strtotime($value['from_date'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['to_date']) ? date('d/m/Y', strtotime($value['to_date'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_total'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_insurance_temp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_bonus'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(-$value['salary_kpi'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['hospital']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['note']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['joined_at']) ? date('d/m/Y', strtotime($value['joined_at'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['send_letter'] != NULL && $value['send_letter'] != '0000-00-00')?date('d/m/Y', strtotime($value['send_letter'])):'', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['return_letter'] != NULL && $value['return_letter'] != '0000-00-00')?date('d/m/Y', strtotime($value['return_letter'])):'', PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;
        $filename  = 'staff_contract' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function _exportRenew($list) {
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);


        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'STT',
            'CÔNG TY',
            'BỘ PHẬN',
            'PHÒNG BAN',
            'MÃ NHÂN VIÊN',
            'ID NHÂN VIÊN',
            'HỌ TÊN',
            'CHỨC VỤ',
            'KHU VỰC',
            'TỈNH',
            'QUẬN HUYỆN',
            'HỢP ĐỒNG BẤT THƯỜNG',
            'HỢP ĐỒNG CHÍNH',
            'TỪ NGÀY',
            'ĐẾN NGÀY',
            'TỔNG LƯƠNG',
            'BẢO HIỂM',
            'PHỤ CẤP KHÁC',
            'KPI',
            'BỆNH VIỆN',
            'GHI CHÚ',
            'HỢP ĐỒNG CŨ',
            'BẮT ĐẦU CŨ',
            'HẾT HẠN CŨ',
            'ĐỊA CHỈ',
            'NƠI SINH',
            'NGÀY SINH',
            'CMND',
            'NGÀY CẤP',
            'NƠI CẤP',
            'NGÀY VÀO LÀM'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index         = 2;
        $stt           = 0;
        $QContractType = new Application_Model_ContractTypes();
        $QContractTerm = $QContractType->get_cache();

        //echo "<pre>";print_r($list);die;
        foreach ($list as $key => $value):
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit(++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['company_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['department']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['team']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['id_nv']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['full_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['tinh']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['quan_huyen']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['print_type'] == 1 ? trim($QContractTerm[$value['contract_term']]) : "Phụ Lục", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($QContractTerm[$value['contract_term']]), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['from_date']) ? date('d/m/Y', strtotime($value['from_date'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['to_date']) ? date('d/m/Y', strtotime($value['to_date'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_total'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_insurance_temp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_bonus'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(-$value['salary_kpi'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['hospital']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['note']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($QContractTerm[$value['old_contract']]), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['old_from']) ? date('d/m/Y', strtotime($value['old_from'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['old_to']) ? date('d/m/Y', strtotime($value['old_to'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['permanent']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['birth_place']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['dob']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['ID_number']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['ID_date']) ? date('d/m/Y', strtotime($value['ID_date'])) : "", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['ID_place']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['joined_at']) ? date('d/m/Y', strtotime($value['joined_at'])) : '', PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;
        $filename  = 'staff_contract' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

}
