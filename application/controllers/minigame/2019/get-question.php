<?php
try {
    $question_id = $this->getRequest()->getParam('question_id');
    $id_master = $this->getRequest()->getParam('id_master');
    $stt = $this->getRequest()->getParam('stt');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QMiniGame2018 = new Application_Model_MiniGame2018();
    $QMiniGame2018Detail = new Application_Model_MiniGame2018Detail();
    $id_staff = $userStorage->id;

    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $started_at = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
//    print "<pre>".print_r($started_at);die;
//    $started_at = date("Y-m-d H:i:s.v");
    $total_question = MINIGAME_TOTAL_QUESTION; // tổng số câu hỏi
    $params = [
        'staff_id' => $staff_id,
        'question_id' => $question_id,
        'id_master' => $id_master,
        'round'=>MINIGAME_ROUND
    ];


    $is_completed = $QMiniGame2018Detail->getisComplete($params);
    $arr_question = [];//mảng các id đã làm xong
    foreach ($is_completed as $key => $value) {
        $arr_question[] = $value['question_id'];
    }

//random ngẫu  nhiên loại trừ những câu đã làm
    do {
        if ($stt < 8) {
                $num = rand(0, 17);
        } elseif ($stt >= 8 and $stt < 12) {
            $num = rand(18, 35);
        } else {
            $num = rand(36, 43);
        }
    } while (in_array($num, $arr_question));

    $xml = simplexml_load_file(APPLICATION_PATH . MINIGAME_QUESTION_FILE);
    $data = json_decode(json_encode($xml, true));

    $question = [];
    $question['question'] = $data->question[$num]; //lấy câu hỏi ngẫu nhiên từ kq random $num

    $get_answer = $QMiniGame2018->getAnswer($data->question[$num]->id, MINIGAME_ROUND);
    $question['answer'] = $get_answer[0];

// var_dump($question['answer']); exit;
    $time_play = MINIGAME_TIME_PLAY; //số thời gian(giây) chơi mỗi câu
    $data_details = [
        'master_id' => $id_master,
        'question_id' => $num,
        'started_at' => $started_at->format('Y-m-d H:i:s.v'),
        'point' => MINIGAME_TIME_PLAY,
        'diff' => $time_play,
        'user_choose' => 0,
        'round' => MINIGAME_ROUND
    ];
//$correct_answer = $QMiniGame2018->getTotalCorrectAnswer($params);
    $QMiniGame2018Detail->insert($data_details);
//$question['point'] = $correct_answer;
    echo json_encode($question);
    exit;
}
catch (exception $e)
{
    echo $e->getMessage();
    exit;
}
