<?php
try {
    $this->_helper->layout->disableLayout();
    $QMiniGame2018 = new Application_Model_MiniGame2018();
    $QMiniGame2018Detail = new Application_Model_MiniGame2018Detail();
    $QMiniGameRewardLog = new Application_Model_MiniGameRewardLog();
    $QMiniGameReward = new Application_Model_MiniGameReward();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    if (empty($userStorage)) {
        $url = HOST . 'user/login?b=' . urldecode('/minigame/index');
        $this->redirect($url);
    }
    $user_id = $userStorage->id;
//die($user_id);
    $user_team = $userStorage->team;
//if (!in_array($user_team, array(133))) {
//    echo "<pre>";
//    print_r("Team bạn chưa được chơi nhé !");
//    die;
//}
// if (!in_array($user_id, array(5899,7671,6705,7,340))) {
//     echo "<pre>";
//     print_r("Chưa được chơi nhé");
//     die;
// }

    $from_time = MINIGAME_FROM_TIME; //thời gian bắt đầu làm
    $to_time = MINIGAME_TO_TIME; //thời gian kết thúc game
    $current_time = date("Y-m-d H:i:s");

    $round = MINIGAME_ROUND; // vòng
    $total_question = MINIGAME_TOTAL_QUESTION; // tổng số câu hỏi
    $key = rand(0, 17); //random câu hỏi đầu tiên
    $time_play = MINIGAME_TIME_PLAY; //số thời gian(giây) chơi mỗi câu

    $staff_id = $userStorage->id;

    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $started_at = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
//    print "<pre>".print_r($started_at);die;
//    $started_at = date("Y-m-d H:i:s.v");
    $time_end = date('Y-m-d H:i:s', strtotime('' . date('Y-m-d H:i:s') . ' + 10 minutes'));
    $xml = simplexml_load_file(APPLICATION_PATH . MINIGAME_QUESTION_FILE);
    $data = json_decode(json_encode($xml, true));
    $params = [
        'staff_id' => $staff_id,
        'round' => $round
    ];
    //load câu đầu tiên, random câu hỏi



    if (!in_array($user_id, array(22813, 6705, 5899)))
        if ($current_time < $from_time) {
            $url = HOST . 'minigame/index?soon=1';
            $this->redirect($url);
        }
    if (!in_array($user_id, array(22813, 6705, 5899)))
    if ($current_time > $to_time) {
        $url = HOST . 'minigame/index?late=1';
        $this->redirect($url);
    }

    $first_question = $data->question[$key];
    $is_done = $QMiniGame2018->getIdStaff($params);//lấy staff đã làm rồi
//    print "<pre>".print_r($data);die;
    $get_answer = $QMiniGame2018->getAnswer($data->question[$key]->id, MINIGAME_ROUND);


    //staff đã làm rồi
    if (!empty($is_done)) {

        //vong quay neu rep dung 13/15 cau
        $get_reward_log = $QMiniGameRewardLog->getLogRewardOfPlayer($params);
        if(empty($get_reward_log))
        {
            //vong quay
            $lst_answers = $QMiniGame2018->getResult($params);
            $total_rights_answer = 0;
            $url_rotation = HOST . 'minigame/rotation';
            foreach ($lst_answers as $k=>$val)
            {
                if($val['is_right']==1)
                    $total_rights_answer++;
            }
            if($total_rights_answer >=13)
                $this->redirect($url_rotation);
        }
        else {
            $url = HOST . 'minigame/result';
            $this->redirect($url);
        }


        $url = HOST . 'minigame/result';
        $this->redirect($url);
    }

    $data_master = [
        'staff_id' => $staff_id,
        'round' => $round,
        'started_at' => $started_at->format('Y-m-d H:i:s.v')
    ];
    $id_master = $QMiniGame2018->insert($data_master);
    $data_details = [
        'master_id' => $id_master,
        'question_id' => $key,
        'started_at' => $started_at->format('Y-m-d H:i:s.v'),
        'point' => MINIGAME_TIME_PLAY,
        'diff' => $time_play,
        'user_choose' => 0,
        'round' => $round
    ];
    $QMiniGame2018Detail->insert($data_details);
    $this->view->question = $first_question;
    $this->view->time_end = $time_end;
    $this->view->id_master = $id_master;
    $this->view->answer = $get_answer[0];


    $this->_helper->viewRenderer->setRender('2019/play');
}
catch(exception $e){
    echo $e->getMessage();
    exit;
}
	 	
 
	

