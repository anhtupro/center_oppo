<?php
try {
    $week = 6;
    $flashMessenger = $this->_helper->flashMessenger;
    $this->_helper->layout->disableLayout();
    $QMiniGame2018 = new Application_Model_MiniGame2018();
    $QMiniGame2018Detail = new Application_Model_MiniGame2018Detail();
    $QMiniGameRewardLog = new Application_Model_MiniGameRewardLog();
    $QMiniGameReward = new Application_Model_MiniGameReward();
    $QMiniGame2018Answer 	= new Application_Model_MiniGameAnswer2018();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//    print "<pre>".print_r($userStorage);die;
    $url = HOST . 'user/login?b=' . urlencode(HOST . 'minigame/index');
    if (empty($userStorage)) {
        $this->_redirect($url);
    }

    $staff_id = $userStorage->id;

    $params = array(
        'staff_id' => $staff_id,
        'round' => MINIGAME_ROUND
    );
    $get_reward_log = $QMiniGameRewardLog->getLogRewardOfPlayer($params);
    $total_rights_answer = 0;
    $err = 0;
    if (!empty($get_reward_log))
//    $this->redirect($url);
        $err = 1;
    else {
        //vong quay
        $lst_answers = $QMiniGame2018->getResult($params);
        foreach ($lst_answers as $k => $val) {
            if ($val['is_right'] == 1)
                $total_rights_answer++;
        }

//    die('a'.$total_rights_answer.'b');
        if ($total_rights_answer < 13)
//        $this->redirect(HOST . 'minigame/index');
            $err = 2;
    }

    if ($this->getRequest()->getMethod() == 'POST' and $err == 0) {
        $response = array('status' => 0, 'message' => 'Lỗi dữ liệu không đúng');
        $reward = $this->getNewRotationReward($staff_id);
//    print "<pre>".print_r($reward);die;
        if (!empty($reward)) {
//            $data_insert = array(
//                'staff_id' => $userStorage->id,
//                'reward' => $reward['id'],
//                'round' => MINIGAME_ROUND
//            );
//            $QMiniGameRewardLog->insert($data_insert);
            $response['status'] = 1;
            $response['message'] = "Thành công";
            $response['item_id'] = $reward['id'];
            $response['name'] = $reward['name'];
//            if (!empty($reward['refresh'])) {
//                $this->generateReward();
//            }
        }
        echo json_encode($response);
        exit();
    }

    $xml = simplexml_load_file(APPLICATION_PATH . MINIGAME_QUESTION_FILE);
    $question = json_decode(json_encode($xml, true));

    $data = $QMiniGame2018->getResult($params);
//    print "<pre>".print_r($data);die;
    $lst_answer = $QMiniGame2018Answer->getListAnswer($params);

    $lst_correct_answers = array();
//    print "<pre>".print_r($lst_answer);die;
    foreach ($lst_answer as $key => $value) {
        $lst_correct_answers[$value['question_id']] = $value['answer'];
    }

// foreach ($question->question as $key => $value) {
// 	foreach ($data as $k => $val) {
// 		if($value->id == $val['question_id']){

// 		}
// 	}
// }

    $this->view->data = $data;
    $this->view->question = $question;
    $this->view->lst_correct_answers = $lst_correct_answers;

//die($this->getRequest()->getMethod());
    $this->view->error = $err;
    $this->_helper->viewRenderer->setRender('2019/rotation');
}
catch (exception $e)
{
    print "<pre>".print_r($e);die;
}

