<?php
$this->_helper->layout->disableLayout();
$QMiniGame2018 			= new Application_Model_MiniGame2018();
$QMiniGame2018Detail 	= new Application_Model_MiniGame2018Detail();
$QMiniGame2018Answer 	= new Application_Model_MiniGameAnswer2018();
$QMiniGameRewardLog = new Application_Model_MiniGameRewardLog();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
$staff_id 	= $userStorage->id;
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
if(empty($userStorage)){
    $url = HOST . 'user/login?b='.urldecode('/minigame/result');
    $this->redirect($url);
}

$params = [
    'staff_id'	=> $staff_id,
    'round' => MINIGAME_ROUND
];

    //vong quay neu rep dung 13/15 cau
    $get_reward_log = $QMiniGameRewardLog->getLogRewardOfPlayer($params);
    if(empty($get_reward_log))
    {
        //vong quay
        $lst_answers = $QMiniGame2018->getResult($params);
        $total_rights_answer = 0;
        $url_rotation = HOST . 'minigame/rotation';
        foreach ($lst_answers as $k=>$val)
        {
            if($val['is_right']==1)
                $total_rights_answer++;
        }
        if($total_rights_answer >=13)
            $this->redirect($url_rotation);
    }



$xml 	= simplexml_load_file(APPLICATION_PATH.MINIGAME_QUESTION_FILE);
$question 	= json_decode(json_encode($xml, true));
$data = $QMiniGame2018->getResult($params);

$lst_answer = $QMiniGame2018Answer->getListAnswer($params);

$lst_correct_answers = array();
foreach ($lst_answer as $key => $value) {
    $lst_correct_answers[$value['question_id']] = $value['answer'];
}

// foreach ($question->question as $key => $value) {
// 	foreach ($data as $k => $val) {
// 		if($value->id == $val['question_id']){

// 		}
// 	}
// }

$this->view->data = $data;
$this->view->question = $question;
$this->view->lst_correct_answers = $lst_correct_answers;
// /var_dump($question->question[1]->id); exit;

$this->_helper->viewRenderer->setRender('2019/result');