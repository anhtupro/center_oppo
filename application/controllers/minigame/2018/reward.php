<?php
$this->_helper->layout->disableLayout();
$QMiniGame2018 			= new Application_Model_MiniGame2018();
$QMiniGame2018Detail 	= new Application_Model_MiniGame2018Detail();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
$staff_id 	= $userStorage->id;
$round = 1;
$params = [
	'round'		=> $round,
 	'staff_id'	=> $staff_id
 ];


 $data = $QMiniGame2018->getReward($params);
 $this->view->data = $data;