<?php
$this->_helper->layout->disableLayout();
$QMiniGame2018 			= new Application_Model_MiniGame2018();
$QMiniGame2018Detail 	= new Application_Model_MiniGame2018Detail();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();

if(empty($userStorage)){
	$url = HOST . 'user/login?b='.urldecode('/iamoppers/index');
	 		$this->redirect($url);
}
$user_id     = $userStorage->id;

$user_team   = $userStorage->team;
//if (!in_array($user_team, array(133))) {
//    echo "<pre>";
//    print_r("Team bạn chưa được chơi nhé !");
//    die;
//}
// if (!in_array($user_id, array(5899,7671,6705,7,340))) {
//     echo "<pre>";
//     print_r("Chưa được chơi nhé");
//     die;
// }

$from_time		= "2019-11-19 03:00:00"; //thời gian bắt đầu làm
$to_time		= "2019-11-19 05:15:00"; //thời gian kết thúc game
$current_time 	= date("Y-m-d H:i:s");
$round = 1; // vòng
$total_question = 10; // tổng số câu hỏi
$key = rand(0 ,9); //random câu hỏi đầu tiên
$time_play = 60; //số thời gian(giây) chơi mỗi câu

$staff_id 	= $userStorage->id;
$started_at = date("Y-m-d H:i:s");
$time_end 	= date('Y-m-d H:i:s', strtotime(''.date('Y-m-d H:i:s').' + 10 minutes') );
$xml 	= simplexml_load_file(APPLICATION_PATH.'/../public/xml/data-xml.xml');
$data 	= json_decode(json_encode($xml, true));
$params = [
 	'staff_id'	=> $staff_id
 ];
 //load câu đầu tiên, random câu hỏi


$first_question 		= $data->question[$key]; 
$is_done = $QMiniGame2018->getIdStaff($params);//lấy staff đã làm rồi
$get_answer = $QMiniGame2018->getAnswer($data->question[$key]->id);

	if($current_time < $from_time ){
		$url = HOST . 'iamoppers/index?soon=1';
	 		$this->redirect($url);
	}
	if($current_time > $to_time ){
		$url = HOST . 'iamoppers/index?late=1';
	 		$this->redirect($url);
	}

	//staff đã làm rồi
	if(!empty($is_done)){
			$url = HOST . 'iamoppers/result';
	 		$this->redirect($url);
	 }

	$data_master = [
		'staff_id'		=> $staff_id,
		'round'			=> $round,
		'started_at'	=> $started_at
	];
	$id_master = $QMiniGame2018->insert($data_master);
	$data_details = [
		'master_id' 	=> $id_master,
		'question_id'	=> $key,
		'started_at'	=> $started_at,
		'point'			=> 0,
		'diff'			=> $time_play,
		'user_choose'	=> 0
	];
	$QMiniGame2018Detail->insert($data_details);
	$this->view->question 	= $first_question;
	$this->view->time_end 	= $time_end;
	$this->view->id_master 	= $id_master;
	$this->view->answer 	= $get_answer[0];
 
		

	 	
 
	

