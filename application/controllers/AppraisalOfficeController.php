<?php

class AppraisalOfficeController extends My_Controller_Action
{
    public function init()
    {
        
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $action_name = $this->getRequest()->getActionName();
        
        if(!in_array($userStorage->id, [20915, 7564, 5899, 8807, 7, 6705, 22716, 14766]) AND $userStorage->team != 14 AND $userStorage->department != 156 AND $userStorage->department != 154){
            //exit;
        }
        
        $list_admmin = [8807, 7, 340, 2584, 6705, 26612];
        
        $is_admin = (in_array($userStorage->id, $list_admmin)) ? 1 : 0;
        $is_head = $QToDo->checkIsHead($userStorage->id);
        $is_staff = $QToDo->checkIsStaff($userStorage->id);
        
        if(in_array($userStorage->id, $list_admmin)){
            $level = 1;
        }
        else{
            if($is_head == 1){
                $level = 2;
            }
            elseif($is_staff == 1){
                $level = 3;
            }
            else{
                if(!in_array($action_name, ['index-staff', 'save-field', 'save-task', 'delete-task', 'delete-field'])){
                    echo 'Error Priviledge';exit;
                }
            }
        }
        
        $storage = [
            'staff_id' => $userStorage->id,
            'level'    => $level,
        ];
        
        $this->list_admmin = $list_admmin;
        $this->storage = $storage;
        
        $this->is_admin = $is_admin;
        $this->is_head = $is_head;
        $this->is_staff = $is_staff;
        
        
    }
    
    public function indexAction()
    {
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'index.php';
    }
    
    public function indexStaffAction()
    {
        $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $params = [
            'staff_id' => $userStorage->id
        ];
        $all_member = $QAppraisalOfficeMember->getAllMemberApproved($params);
        
        $plan = $QAppraisalOfficePlan->getListPlan($params);
        
        $plan_active = $QAppraisalOfficePlan->checkActivePlan($userStorage->id);
        
        $member = [];
        
        foreach($all_member as $key=>$value){
            $member[$value['aop_name']] = $value;
        }
        
        
        $this->view->params = $params;
        $this->view->member = $member;
        $this->view->plan = $plan;
        $this->view->plan_active = $plan_active;
        
    }
    
    #region AppraisalPlan
    public function planAction()
    {
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        $plans = $QAppraisalOfficePlan->fetchAll(['aop_is_deleted = 0', 'aop_type = 2'], 'aop_id ASC')->toArray();
        
        for ($i = 0; $i < count($plans); $i++) {
            $plans[$i]['aop_from'] = date('d/m/Y', strtotime($plans[$i]['aop_from']));
            $plans[$i]['aop_to'] = date('d/m/Y', strtotime($plans[$i]['aop_to']));
        }
        $plans = json_encode($plans);

        $QDSurvey = new Application_Model_DynamicSurvey();
        $surveys = $QDSurvey->getActiveSurveyByRef('appraisal_office');
        foreach ($surveys as &$survey) {
            $survey['ds_name'] = trim($survey['ds_name']);
        }
        $surveys = json_encode($surveys);
        $this->view->data = [
            'plans' => $plans,
            'surveys' => $surveys
        ];
    }
    
    public function planPrdAction()
    {
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        $QDSurvey = new Application_Model_DynamicSurvey();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        
        $plans = $QAppraisalOfficePlan->fetchAll(['aop_is_deleted = ?' => 0, 'is_prd = ?' => 1], 'aop_id ASC')->toArray();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;
        $is_head = $QToDo->checkIsHead($userStorage->id);
        $is_staff = $QToDo->checkIsStaff($userStorage->id);
        
        for ($i = 0; $i < count($plans); $i++) {
            $plans[$i]['aop_from'] = date('d/m/Y', strtotime($plans[$i]['aop_from']));
            $plans[$i]['aop_to'] = date('d/m/Y', strtotime($plans[$i]['aop_to']));
        }
        $plans = json_encode($plans);

        
        $surveys = $QDSurvey->getActiveSurveyByRef('appraisal_office');
        foreach ($surveys as &$survey) {
            $survey['ds_name'] = trim($survey['ds_name']);
        }
        $surveys = json_encode($surveys);
        $this->view->data = [
            'plans' => $plans,
            'surveys' => $surveys,
            'is_admin' => $is_admin,
            'is_head' => $is_head,
            'is_staff' => $is_staff
        ];
    }

    public function getPlanAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        $plans = $QAppraisalOfficePlan->fetchAll('aop_is_deleted = 0', 'aop_id DESC')->toArray();
        for ($i = 0; $i < count($plans); $i++) {
            $plans[$i]['aop_from'] = date('d/m/Y', strtotime($plans[$i]['aop_from']));
            $plans[$i]['aop_to'] = date('d/m/Y', strtotime($plans[$i]['aop_to']));
        }
        echo json_encode($plans);
        return;
    }

    public function savePlanAction()
    {
        
        $db              = Zend_Registry::get('db');
        $db->beginTransaction();
        
        try {
            
            
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            /** @var Zend_Controller_Request_Http $request */
            $request = $this->getRequest();
            if (!$request->isPost()) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Wrong method submitted!'
                ]);
                return;
            }

            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;

            $data = $this->getRequest()->getParam('data');
            $data = json_decode($data, true);
            $data['aop_from'] = implode('-', array_reverse(explode('/', $data['aop_from'])));
            $data['aop_to'] = implode('-', array_reverse(explode('/', $data['aop_to'])));
            $data['aop_updated_at'] = date('Y/m/d h:i:s');
            $data['aop_updated_by'] = $userId;
            

            $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
            
            if (isset($data['aop_id']) && $data['aop_id'] != 0) {
                
                $plan = $QAppraisalOfficePlan->fetchRow(['aop_id = ?' => $data['aop_id']]);
                $maxPlanId = $QAppraisalOfficePlan->getMaxPlanId($data['aop_id']);
                
                if($plan['aop_status'] == 0 && $plan['aop_init'] == 0) {
                    
                    $QToDo = new Application_Model_AppraisalOfficeToDo();
                    
                    if($data['aop_type'] == 1){
                        $QToDo->initPlanPrd($data['aop_id']);
                        
                        //Copy Member
                        $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
                        $QAppraisalOfficeField = new Application_Model_AppraisalOfficeField();
                        $QAppraisalOfficeTask = new Application_Model_AppraisalOfficeTask();
                        
                        
                        $QAppraisalOfficeMemberRoot = new Application_Model_AppraisalOfficeMemberRoot();
                        $QAppraisalOfficeFieldRoot = new Application_Model_AppraisalOfficeFieldRoot();
                        $QAppraisalOfficeTaskRoot = new Application_Model_AppraisalOfficeTaskRoot();
                        
                        //Lấy danh sách member plan này
                        $member = $QAppraisalOfficeMember->fetchAll(['aop_id = ?' => $data['aop_id']]);
                        
                        foreach($member as $key=>$value){
                            
                            //Lấy member root sau đó lấy field gần nhất insert vào member plan hiện tại
                            $member_root = $QAppraisalOfficeMemberRoot->fetchRow(['aom_staff_id = ?' => $value['aom_staff_id'], 'aom_is_deleted = ?' => 0]);
                            if(!empty($member_root)){
                                $field = $QAppraisalOfficeFieldRoot->fetchAll(['aom_id = ?' => $member_root['aom_id'], 'aof_is_deleted = ?' => 0]);
                            
                                foreach($field as $k=>$v){
                                    $insert_field = [
                                        'aom_id' => $value['aom_id'],
                                        'aof_name' => $v['aof_name'],
                                        'aof_created_at' => date('Y-m-d H:i:s'),
                                        'aof_created_by' => $v['aof_created_by'],
                                    ];
                                    $id_field = $QAppraisalOfficeField->insert($insert_field);
                                    $task = $QAppraisalOfficeTaskRoot->fetchAll(['aof_id = ?' => $v['aof_id'], 'aot_is_deleted = ?' => 0]);
                                    foreach($task as $i=>$j){
                                        $insert_task = [
                                            'aof_id' => $id_field,
                                            'aot_target' => $j['aot_target'],
                                            'aot_task' => $j['aot_task'],
                                            'aot_skill' => $j['aot_skill'],
                                            'aot_ratio' => $j['aot_ratio'],
                                            'aot_created_at' => date('Y-m-d H:i:s'),
                                            'aot_created_by' => $j['aot_created_by'],
                                        ];
                                        $QAppraisalOfficeTask->insert($insert_task);
                                    }

                                }
                            }
                            
                        }
                        //END Copy member
                        
                        //tạo survey
                        $QNotification = new Application_Model_Notification();
                        $id_noti = $QNotification->insert([
                                'title' => 'Đánh giá PRD',
                                'content' => 'Bạn vui lòng <a style="color: red; text-decoration: underline" href="/appraisal-office/plan-prd?plan_id">click vào link</a> để thực hiện đánh giá năng lực',
                                'category_id' => 4,
                                'pop_up' => 1,
                                'type'  => 1
                            ]
                        );
                        $data['aop_notification_id'] = $id_noti;
                        //
                        
                    }
                    elseif($data['aop_type'] == 2){
                        $QToDo->initPlan($data['aop_id']);
                        //tạo survey
                        $QNotification = new Application_Model_Notification();
                        $id_noti = $QNotification->insert([
                                'title' => 'Đánh giá năng lực (Khối văn phòng)',
                                'content' => 'Bạn vui lòng <a style="color: red; text-decoration: underline" href="/appraisal-office/to-do-survey?plan_id='.$data['aop_id'].'">click vào link</a> để thực hiện đánh giá năng lực',
                                'category_id' => 4,
                                'pop_up' => 1,
                                'type'  => 1
                            ]
                        );
                        $data['aop_notification_id'] = $id_noti;
                        //
                        
                    }
                    
                    
                    
                    $data['aop_init'] = 1;
                    
                    //Update survey_id
                    $appraisal_office_plan = $QAppraisalOfficePlan->fetchRow($QAppraisalOfficePlan->getAdapter()->quoteInto('aop_id = ?', $data['aop_id']));
                    
                    $QAppraisalOfficeToDo = new Application_Model_AppraisalOfficeToDo();
                    $where_todo = [];
                    $where_todo[] = $QAppraisalOfficePlan->getAdapter()->quoteInto('fk_plan = ?', $data['aop_id']);
                    $where_todo[] = $QAppraisalOfficePlan->getAdapter()->quoteInto('type = ?', 1);
                    
                    $QAppraisalOfficeToDo->update(['survey_id' => $appraisal_office_plan['aop_survey_id']], $where_todo);
                    
                    $where_todo = [];
                    $where_todo[] = $QAppraisalOfficePlan->getAdapter()->quoteInto('fk_plan = ?', $data['aop_id']);
                    $where_todo[] = $QAppraisalOfficePlan->getAdapter()->quoteInto('type = ?', 2);
                    
                    $QAppraisalOfficeToDo->update(['survey_id' => $appraisal_office_plan['aop_survey_leader_id']], $where_todo);
                    
                    
                }
                
                $QAppraisalOfficePlan->update($data, $QAppraisalOfficePlan->getAdapter()->quoteInto('aop_id = ?', $data['aop_id']));
                echo json_encode([
                    'status' => 0,
                    'data' => 0
                ]);
                $db->commit();
                return;
            } else {
                unset($data['aop_id']);
                $data['aop_created_at'] = date('Y/m/d h:i:s');
                $data['aop_created_by'] = $userId;
                $result = $QAppraisalOfficePlan->insert($data);
                echo json_encode([
                    'status' => 0,
                    'data' => intval($result)
                ]);
                $db->commit();
                return;
            }
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
            $db->rollback();
            return;
        }
    }
    
    public function savePlanPrdAction()
    {
        
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            /** @var Zend_Controller_Request_Http $request */
            $request = $this->getRequest();
            if (!$request->isPost()) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Wrong method submitted!'
                ]);
                return;
            }

            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;

            $data = $this->getRequest()->getParam('data');
            $data = json_decode($data, true);
            $data['aop_from'] = implode('-', array_reverse(explode('/', $data['aop_from'])));
            $data['aop_to'] = implode('-', array_reverse(explode('/', $data['aop_to'])));
            $data['aop_updated_at'] = date('Y/m/d h:i:s');
            $data['aop_updated_by'] = $userId;
            

            $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
            if (isset($data['aop_id']) && $data['aop_id'] != 0) {
                
                if($data['aop_status'] == 1 && $plan['aop_status'] == 0 && $plan['aop_init'] == 0) {
                }
                
                $QAppraisalOfficePlan->update($data, $QAppraisalOfficePlan->getAdapter()->quoteInto('aop_id = ?', $data['aop_id']));
                echo json_encode([
                    'status' => 0,
                    'data' => 0
                ]);
                return;
            } else {
                unset($data['aop_id']);
                $data['aop_created_at'] = date('Y/m/d h:i:s');
                $data['aop_created_by'] = $userId;
                $data['is_prd'] = 1;
                
                
                $result = $QAppraisalOfficePlan->insert($data);
                echo json_encode([
                    'status' => 0,
                    'data' => intval($result)
                ]);
                return;
            }
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
            return;
        }
    }

    public function deletePlanAction()
    {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            /** @var Zend_Controller_Request_Http $request */
            $request = $this->getRequest();
            if (!$request->isPost()) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Wrong method submitted!'
                ]);
                return;
            }
            $aop_id = $this->getRequest()->getParam('aop_id');
            $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
            if (isset($aop_id)) {
                $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
                $data = [
                    'aop_is_deleted' => true,
                    'aop_updated_at' => date('Y/m/d h:i:s'),
                    'aop_updated_by' => $userId
                ];
                $QAppraisalOfficePlan->update($data, $QAppraisalOfficePlan->getAdapter()->quoteInto('aop_id = ?', $aop_id));
                echo json_encode([
                    'status' => 0,
                    'data' => 0
                ]);
                return;
            } else {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Wrong plan ID submitted!'
                ]);
                return;
            }
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
            return;
        }
    }

    public function getPlanDetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');

        try {
            $QPlan = new Application_Model_AppraisalOfficePlan();
            $plan = $QPlan->fetchRow($QPlan->getAdapter()->quoteInto('aop_id = ?', $planId));
            if($plan && $plan['aop_type'] == 1) {
                $sql  = "SELECT t.id, t.name department_name, p.aom_head_department_id, p.aom_staff_id, concat(s.firstname, ' ', s.lastname) head_name, p.aom_head_department_id head_id,
COUNT( DISTINCT IF(f.total_star = 1, f.aom_staff_id, NULL)) star_1, COUNT( DISTINCT IF(f.total_star = 2, f.aom_staff_id,NULL)) star_2, 
COUNT( DISTINCT IF(f.total_star = 3,f.aom_staff_id,NULL)) star_3, COUNT( DISTINCT IF(f.total_star = 4,f.aom_staff_id,NULL)) star_4,
COUNT( DISTINCT IF(f.total_star = 5,f.aom_staff_id,NULL)) star_5
FROM appraisal_office_member p
LEFT JOIN staff s ON s.id = p.aom_head_department_id
LEFT JOIN team t ON t.id = s.department
LEFT JOIN (
	SELECT p.aom_id, p.aop_id, p.aom_head_department_id, p.aom_staff_id, p.aom_head_approved, p.aom_staff_approved, p.aom_is_deleted,
	t.aot_ratio, t.aot_self_appraisal, t.aot_head_department_appraisal,
	SUM(star.ratio*t.aot_ratio)/100 total_point,
	CASE WHEN SUM(star.ratio*t.aot_ratio)/100 >= 110 THEN 5
	WHEN SUM(star.ratio*t.aot_ratio)/100 >= 100 THEN 4
	WHEN SUM(star.ratio*t.aot_ratio)/100 >= 90 THEN 3
	WHEN SUM(star.ratio*t.aot_ratio)/100 >= 70 THEN 2
	WHEN SUM(star.ratio*t.aot_ratio)/100 >= 0 THEN 1
	END total_star
	FROM appraisal_office_member p
	LEFT JOIN staff s ON s.id = p.aom_staff_id
	LEFT JOIN appraisal_office_field f ON f.aom_id = p.aom_id
	LEFT JOIN appraisal_office_task t ON t.aof_id = f.aof_id
	LEFT JOIN appraisal_office_star star ON star.star = t.aot_head_department_appraisal
	WHERE p.aop_id = :plan_id AND p.aom_is_deleted = 0 AND s.off_date IS NULL
	GROUP BY p.aom_staff_id
) f ON f.aom_head_department_id =  s.id
WHERE p.aop_id = :plan_id
GROUP BY s.id
ORDER BY t.id";
            } else if($plan && $plan['aop_type'] == 2) {
                $sql = "select distinct t.name                                         department_name,
                td.from_staff                                head_id,
                concat(s.firstname, ' ', s.lastname)           head_name,
                group_concat(rd.fk_dsa) result
from appraisal_office_plan p
       join appraisal_office_to_do td
            on p.aop_id = td.fk_plan and p.aop_type = 2 and p.aop_is_deleted = 0 and p.aop_id = :plan_id
       join staff s on td.from_staff = s.id
       join team t on s.department = t.id
       left join appraisal_office_capacity c
                 on c.fk_aop = td.fk_plan and c.fk_staff = td.staff_to and c.aoc_is_deleted = 0
       left join dynamic_survey_result r on c.fk_dsr = r.dsr_id
       left join dynamic_survey_result_detail rd on rd.fk_dsr = r.dsr_id
       left join dynamic_survey_question q on q.dsq_id = rd.fk_dsq
       left join appraisal_office_skill sk on q.dsq_ref = sk.aos_id
group by td.from_staff
order by p.aop_from";
            }

            /** @var Zend_Db_Adapter_Abstract $db */
            $db   = Zend_Registry::get('db');
            $stmt = $db->prepare($sql);
            $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => 'Không thể cập nhật thông tin! Vui lòng thử lại sau.' . $ex->getMessage()
            ]);
            return;
        }
        $result = [];
        
        
        foreach ($data as $datum) {
            if(!array_key_exists($datum['department_name'], $result)) {
                $result[$datum['department_name']] = [];
            }
            $result[$datum['department_name']][] = [
                'head_id' => $datum['head_id'],
                'head_name' => $datum['head_name'],
                'result' => $datum['result'] ? array_map('intval', explode(',', $datum['result'])) : [],
                'star_1' => $datum['star_1'],
                'star_2' => $datum['star_2'],
                'star_3' => $datum['star_3'],
                'star_4' => $datum['star_4'],
                'star_5' => $datum['star_5']
            ];
        }
        
        echo json_encode([
            'status' => 0,
            'data' => $result
        ]);
    }
    #endregion AppraisalPlan

    #region SetPRDMultiple
    public function setPrdMultipleAction()
    {
        $plan_id = $this->getRequest()->getParam('plan_id');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $staff_from = $userStorage->id;
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $QStaff = new Application_Model_Staff();
        
        $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $plan_id]);
        $this->view->staff = $QStaff->fetchRow(['id = ?' => $staff_from]);
    }
    
    public function setPrdMultipleAllAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $review_staff = $userStorage->id;
        $planId = $this->getRequest()->getParam('plan_id');
        $star = $this->getRequest()->getParam('star');
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $QStaff = new Application_Model_Staff();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QField = new Application_Model_AppraisalOfficeField();
        
        $list_staff = $QToDo->getListStaffView($planId, $review_staff, $star);
        $get_field = $QField->getFieldPrd($planId, $review_staff);
        $get_task = $QField->getTaskPrd($planId, $review_staff);
        
        $field = [];
        foreach($get_field as $key=>$value){
            $field[$value['aom_staff_id']][] = $value;
        }
        
        $task = [];
        foreach($get_task as $key=>$value){
            $task[$value['aom_staff_id']][$value['aof_id']][] = $value;
        }
        
        $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $planId]);
        $this->view->staff = $QStaff->fetchRow(['id = ?' => $review_staff]);
        $this->view->list_staff = $list_staff;
        $this->view->field = $field;
        $this->view->task = $task;
        $this->view->viewStar = $star;
    }
    
    public function viewPrdAction(){
        
        $review_staff = $this->getRequest()->getParam('review-staff');
        $planId = $this->getRequest()->getParam('plan-id');
        $star = $this->getRequest()->getParam('star');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $review_staff = !empty($review_staff) ? $review_staff : $userStorage->id;
        
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $QStaff = new Application_Model_Staff();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QField = new Application_Model_AppraisalOfficeField();
        
        $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;
        $is_head = $QToDo->checkIsHead($userStorage->id);
        $is_staff = $QToDo->checkIsStaff($userStorage->id);
        
        if($is_admin == 1){
            $list_staff = $QToDo->getListStaffView($planId, $review_staff, NULL, $star);
        }
        elseif($is_head == 1){
            $list_staff = $QToDo->getListStaffView($planId, $userStorage->id, NULL, $star);
        }
        elseif($is_staff == 1){
            $head_of_staff = $QToDo->getHeadOfStaff($userStorage->id);
            
            if(empty($head_of_staff)){
                echo "not head";exit;
            }
            $list_staff = $QToDo->getListStaffView($planId, $head_of_staff, $review_staff, NULL);
        }
        
        $get_field = $QField->getFieldPrd($planId, $review_staff);
        $get_task = $QField->getTaskPrd($planId, $review_staff);
        
        
        $field = [];
        foreach($get_field as $key=>$value){
            $field[$value['aom_staff_id']][] = $value;
        }
        
        $task = [];
        foreach($get_task as $key=>$value){
            $task[$value['aom_staff_id']][$value['aof_id']][] = $value;
        }
        
        $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $planId]);
        //$this->view->staff = $QStaff->fetchRow(['id = ?' => $review_staff]);
        $this->view->list_staff = $list_staff;
        $this->view->field = $field;
        $this->view->task = $task;
        $this->view->viewStar = $star;
    }
    
    public function viewPrdDetailsAction(){
        
        $staff_id = $this->getRequest()->getParam('staff_id');
        $plan_id = $this->getRequest()->getParam('plan_id');
        
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $QStaff = new Application_Model_Staff();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QField = new Application_Model_AppraisalOfficeField();
        
        $list_staff = $QToDo->getListStaffView($plan_id, NULL, $staff_id, NULL);
            
        $get_field = $QField->getFieldPrd($plan_id, NULL);
        $get_task = $QField->getTaskPrd($plan_id, NULL);
        
        
        $field = [];
        foreach($get_field as $key=>$value){
            $field[$value['aom_staff_id']][] = $value;
        }
        
        $task = [];
        foreach($get_task as $key=>$value){
            $task[$value['aom_staff_id']][$value['aof_id']][] = $value;
        }
        
        $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $plan_id]);
        //$this->view->staff = $QStaff->fetchRow(['id = ?' => $review_staff]);
        $this->view->list_staff = $list_staff;
        $this->view->field = $field;
        $this->view->task = $task;
        $this->view->viewStar = $star;
    }

    public function getPrdMultipleAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('plan_id');
        $reviewStaff = $this->getRequest()->getParam('review_staff');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $userId = $userStorage->id;
        if($userStorage->title == 470) {
            //$userId = $reviewStaff;
        }
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $listStaff = $QToDo->getListStaffPrd($planId, $userId);
        
        $QMember = new Application_Model_AppraisalOfficeMember();
        $members = $QMember->fetchAll(sprintf('aom_head_department_id = %s and aom_is_deleted = 0 and aop_id = %s', $userId,$planId))->toArray();
        
        
        $fields = [];
        if (count($members) > 0) {
            $memberIds = [];
            foreach ($members as $member) {
                $memberIds[] = $member['aom_id'];
            }
            $QField = new Application_Model_AppraisalOfficeField();
            $fields = $QField->fetchAll(sprintf(
                'aom_id in (%s) and aof_is_deleted = 0',
                implode(', ', $memberIds)
            ))->toArray();
        }

        $tasks = [];
        if (count($fields) > 0) {
            $fieldIds = [];
            foreach ($fields as $field) {
                $fieldIds[] = $field['aof_id'];
            }
            $QTask = new Application_Model_AppraisalOfficeTask();
            $tasks = $QTask->fetchAll(sprintf(
                'aof_id in (%s) and aot_is_deleted = 0',
                implode(', ', $fieldIds)
            ))->toArray();
        }

        $crossReviews = [];
        if (count($tasks) > 0) {
            $taskIds = [];
            foreach ($tasks as $task) {
                $taskIds[] = $task['aot_id'];
            }

            /** @var Zend_Db_Adapter_Abstract $db */
            $db = Zend_Registry::get('db');
            $stmt = $db
                ->select()
                ->from(['cr' => 'appraisal_office_cross_review'], ['cr.*'])
                ->join(['s' => 'staff'], 'cr.aocr_staff_id = s.id', ['s.id', 's.code', 's.lastname', 's.firstname', 's.email', 's.department', 's.team', 's.title'])
                ->where(sprintf(
                    'aot_id in (%s) and aocr_is_deleted = 0',
                    implode(', ', $taskIds)
                ));
            $crossReviews = $db->fetchAll($stmt);
        }
        echo json_encode([
            'plan_id' => $planId,
            'head_id' => $userId,
            'staffs' => $listStaff,
            'members' => $members,
            'fields' => $fields,
            'tasks' => $tasks,
            'cross_reviews' => $crossReviews
        ]);
    }

    public function saveTaskAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        try {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $taskId = $this->getRequest()->getParam('aot_id');
            $data = [
                'aof_id' => $params['aof_id'],
                'aot_target' => $params['aot_target'],
                'aot_target_review' => (!empty($params['aot_target_review']) AND $params['aot_target_review'] != 'null') ? $params['aot_target_review'] : NULL,
                'aot_task' => $params['aot_task'],
                'aot_skill' => $params['aot_skill'],
                'aot_ratio' => $params['aot_ratio'],
                'aot_self_appraisal' => $params['aot_self_appraisal'],
                'aot_note' => !empty($params['aot_note']) ? $params['aot_note'] : NULL,
                'aot_head_department_appraisal' => (!empty($params['aot_head_department_appraisal']) AND $params['aot_head_department_appraisal'] != 'null') ? $params['aot_head_department_appraisal'] : 0,
                'aot_head_department_note' => !empty($params['aot_head_department_note']) ? $params['aot_head_department_note'] : NULL,
                'aot_updated_at' => date('Y-m-d H:i:s'),
                'aot_updated_by' => $userId
            ];

            $QTask = new Application_Model_AppraisalOfficeTask();
            if ($taskId == 0) {
                $data['aot_created_at'] = date('Y-m-d H:i:s');
                $data['aot_created_by'] = $userId;
                $taskId = $QTask->insert($data);
            } else {
                $QTask->update($data, $QTask->getAdapter()->quoteInto('aot_id = ? and aot_is_deleted = 0', $taskId));
            }
            (new Application_Model_AppraisalOfficeMember())->updateInitRatioByField($data['aof_id']);
            echo json_encode([
                'status' => 0,
                'data' => $taskId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function deleteTaskAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        try {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $taskId = $this->getRequest()->getParam('aot_id');
            $QTask = new Application_Model_AppraisalOfficeTask();
            $task = $QTask->fetchRow($QTask->getAdapter()->quoteInto('aot_id = ? and aot_is_deleted = 0', $taskId));
            if ($task) {
                $QTask->update([
                    'aot_is_deleted' => 1,
                    'aot_updated_at' => date('Y-m-d H:i:s'),
                    'aot_updated_by' => $userId
                ], $QTask->getAdapter()->quoteInto('aot_id = ? and aot_is_deleted = 0', $taskId));

                $QCrossReview = new Application_Model_AppraisalOfficeCrossReview();
                $QCrossReview->deleteByTaskDeleted();

                (new Application_Model_AppraisalOfficeMember())->updateInitRatioByField($task['aof_id']);
                echo json_encode([
                    'status' => 0,
                    'data' => ''
                ]);
            } else {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Cannot found this task. Please reload page and try again!'
                ]);
            }
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage() . $ex->getTraceAsString()
            ]);
        }
    }

    public function saveCrossReviewAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Giao thức truy cập không được hỗ trợ!'
            ]);
            return;
        }

        try {
            $params = $this->getRequest()->getParams();
            $headId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $crId = $params['aocr_id'];
            $data = [
                'aot_id' => $params['aot_id'],
                'aocr_cross_review' => $params['aocr_cross_review'],
                'aocr_note' => $params['aocr_note'],
                'aocr_staff_id' => $params['aocr_staff_id'],
                'aocr_updated_at' => date('Y-m-d H:i:s'),
                'aocr_updated_by' => $headId
            ];

            $QCrossReview = new Application_Model_AppraisalOfficeCrossReview();
            if ($crId == 0) {
                $data['aocr_created_at'] = date('Y-m-d H:i:s');
                $data['aocr_created_by'] = $headId;

                $crDuplicate = $QCrossReview->fetchRow([
                    $QCrossReview->getAdapter()->quoteInto('aot_id = ?', $params['aot_id']),
                    $QCrossReview->getAdapter()->quoteInto('aocr_staff_id = ?', $params['aocr_staff_id']),
                    $QCrossReview->getAdapter()->quoteInto('aocr_is_deleted = ?', 0),
                ]);
                if ($crDuplicate == null) {
                    $crId = $QCrossReview->insert($data);
                } else {
                    echo json_encode([
                        'status' => 1,
                        'message' => 'Đã có người đánh giá này!'
                    ]);
                    return;
                }
            } else {
                $QCrossReview->update($data, $QCrossReview->getAdapter()->quoteInto('aocr_id = ? and aocr_is_deleted = 0', $crId));
            }
            echo json_encode([
                'status' => 0,
                'data' => $crId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function deleteCrossReviewAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Giao thức truy cập không được hỗ trợ!'
            ]);
            return;
        }

        $crId = $this->getRequest()->getParam('aocr_id');
        $headId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $data = [
            'aocr_is_deleted' => 1,
            'aocr_updated_at' => date('Y-m-d H:i:s'),
            'aocr_updated_by' => $headId,
        ];
        $QCrossReview = new Application_Model_AppraisalOfficeCrossReview();
        $QCrossReview->update($data, $QCrossReview->getAdapter()->quoteInto('aocr_id = ? and aocr_is_deleted = 0', $crId));
        echo json_encode([
            'status' => 0,
            'data' => ''
        ]);
    }

    public function confirmDoneAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Giao thức truy cập không hợp lệ!'
            ]);
            return;
        }

        try {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $planId = $this->getRequest()->getParam('plan_id');
            $headId = $this->getRequest()->getParam('head_id');
            if ($userId != $headId) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Người yêu cầu không hợp lệ. Vui lòng tải lại trang!'
                ]);
                return;
            }

            $data = [
                'aom_head_approved' => 1,
                'aom_updated_by' => $userId,
                'aom_updated_at' => date('Y-m-d h:i:s')
            ];

            $QMember = new Application_Model_AppraisalOfficeMember();
            $QMember->update($data, [
                $QMember->getAdapter()->quoteInto('aop_id = ? and aom_is_deleted = 0', $planId),
                $QMember->getAdapter()->quoteInto('aom_head_department_id = ? and aom_init_ratio = 100', $headId)
            ]);
            echo json_encode([
                'status' => 0,
                'data' => ''
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage() . $ex->getTraceAsString()
            ]);
        }
    }

    public function headApprovedPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');
        $headId = $this->getRequest()->getParam('head_id');
        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        if($headId != $userId) {
            echo json_encode([
                'status' => 1,
                'data' => 'Thông tin người dùng không hợp lệ! Vui lòng thử lại.'
            ]);
            return;
        }
        $QMember = new Application_Model_AppraisalOfficeMember();
        $result = $QMember->update([
            'aom_head_approved' => 2,
            'aom_updated_at' => date('Y-m-d H:i:s'),
            'aom_updated_by' => $userId
        ], [
            $QMember->getAdapter()->quoteInto('aop_id = ?', $planId),
            $QMember->getAdapter()->quoteInto('aom_head_department_id = ?', $headId),
            $QMember->getAdapter()->quoteInto('aom_head_approved = ?', 1),
        ]);
        if($result > 0) {
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    #endregion SetPRDMultiple

    #region SetPRD
    public function setPrdAction()
    {
        $planId = $this->getRequest()->getParam('plan_id');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $review_staff = $userStorage->id;
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $QStaff = new Application_Model_Staff();
        $QAppraisalOfficeField = new Application_Model_AppraisalOfficeField();
        
        $field_null = $QAppraisalOfficeField->getFieldNull($planId, $review_staff);
        foreach($field_null as $key=>$value){
            $QAppraisalOfficeField->update(['aof_is_deleted' => 1], ['aof_id = ?' => $value['aof_id']]);
        }
        
        $this->view->plan = $QPlan->fetchRow(['aop_id = ?' => $planId]);
        $this->view->staff = $QStaff->fetchRow(['id = ?' => $review_staff]);
    }

    public function getPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $planId = $this->getRequest()->getParam('plan_id');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $userStorage->id;
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QPlan = new Application_Model_AppraisalOfficePlan();
        
        $staff = $QToDo->getStaffMember($planId, $userId);
        if(count($staff) > 0) {
            $staff = $staff[0];
        }

        $QMember = new Application_Model_AppraisalOfficeMember();
        $member = $QMember->fetchRow(sprintf(
            'aom_staff_id = %s and aom_is_deleted = 0 and aop_id = %s',
            $userId,
            $planId
        ));

        $fields = [];
        if ($member) {
            $member = $member->toArray();
            $QField = new Application_Model_AppraisalOfficeField();
            $fields = $QField->fetchAll(sprintf(
                'aom_id = %s and aof_is_deleted = 0',
                $member['aom_id']
            ))->toArray();
        }

        $tasks = [];
        if (count($fields) > 0) {
            $fieldIds = [];
            foreach ($fields as $field) {
                $fieldIds[] = $field['aof_id'];
            }
            $QTask = new Application_Model_AppraisalOfficeTask();
            $tasks = $QTask->fetchAll(sprintf(
                'aof_id in (%s) and aot_is_deleted = 0',
                implode(', ', $fieldIds)
            ))->toArray();
        }

        $crossReviews = [];
        if (count($tasks) > 0) {
            $taskIds = [];
            foreach ($tasks as $task) {
                $taskIds[] = $task['aot_id'];
            }

            /** @var Zend_Db_Adapter_Abstract $db */
            $db = Zend_Registry::get('db');
            $stmt = $db
                ->select()
                ->from(['cr' => 'appraisal_office_cross_review'], ['cr.*'])
                ->join(['s' => 'staff'], 'cr.aocr_staff_id = s.id', ['s.id', 's.code', 's.lastname', 's.firstname', 's.email', 's.department', 's.team', 's.title'])
                ->where(sprintf(
                    'aot_id in (%s) and aocr_is_deleted = 0',
                    implode(', ', $taskIds)
                ));
            $crossReviews = $db->fetchAll($stmt);
        }
        
        //$QHeadDepartment = new Application_Model_HeadOfDepartment();
        $headId = $QToDo->getStaffApproveByStaff($userId);
        
        $last_plan = $QPlan->getLastPlan($planId);
        
        echo json_encode([
            'plan_id' => $planId,
            'head_id' => $headId['from_id'],
            'staff' => $staff,
            'member' => $member,
            'fields' => $fields,
            'tasks' => $tasks,
            'cross_reviews' => $crossReviews,
            'last_plan_id' => $last_plan['aop_id'],
            'last_plan_name' => $last_plan['aop_name'],
        ]);
    }
    
    public function getPrdApproveAction()
    {
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'get-prd-approve.php';
    }

    public function saveFieldAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $headId = $this->getRequest()->getParam('head_id');
        $staffId = $this->getRequest()->getParam('staff_id');
        $planId = $this->getRequest()->getParam('aop_id');
        $memberId = $this->getRequest()->getParam('aom_id');
        $fieldId = $this->getRequest()->getParam('aof_id');
        $fieldName = $this->getRequest()->getParam('aof_name');
        $memberData = [
            'aop_id' => $planId,
            'aom_head_department_id' => $headId,
            'aom_head_approved' => 0,
            'aom_staff_approved' => 0,
            'aom_staff_id' => $staffId,
            'aom_created_at' => date('Y-m-d H:i:s'),
            'aom_created_by' => $userId,
            'aom_updated_at' => date('Y-m-d H:i:s'),
            'aom_updated_by' => $userId,
        ];
        $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
        if ($memberId == 0) {
            $memberId = $QAppraisalOfficeMember->insert($memberData);
            $memberData['aom_id'] = $memberId;
        }
        $field = [
            'aom_id' => $memberId,
            'aof_name' => $fieldName,
            'aof_updated_at' => date('Y-m-d H:i:s'),
            'aof_updated_by' => $userId
        ];
        $QAppraisalOfficeField = new Application_Model_AppraisalOfficeField();
        if ($fieldId == 0) {
            $field['aof_created_at'] = date('Y-m-d H:i:s');
            $field['aof_created_by'] = $userId;
            $fieldId = $QAppraisalOfficeField->insert($field);
            $field['aof_id'] = $fieldId;
        } else {
            $QAppraisalOfficeField->update(
                $field,
                $QAppraisalOfficeField->getAdapter()->quoteInto('aof_id = ?', $fieldId)
            );
        }
        $QAppraisalOfficeMember->updateInitRatioByField($fieldId);
        echo json_encode([
            'status' => 0,
            'data' => [
                'member' => $memberData,
                'field_id' => $fieldId
            ]
        ]);
    }

    public function deleteFieldAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $fieldId = $this->getRequest()->getParam('aof_id');
        $QAppraisalOfficeField = new Application_Model_AppraisalOfficeField();
        $field = $QAppraisalOfficeField->fetchRow($QAppraisalOfficeField->getAdapter()->quoteInto('aof_id = ? and aof_is_deleted = 0', $fieldId));
        if ($field) {
            $QAppraisalOfficeTask = new Application_Model_AppraisalOfficeTask();
            $QAppraisalOfficeTask->update([
                'aot_is_deleted' => 1,
                'aot_updated_at' => date('Y-m-d H:i:s'),
                'aot_updated_by' => $userId
            ], $QAppraisalOfficeTask->getAdapter()->quoteInto('aof_id = ? and aot_is_deleted = 0', $fieldId));

            $QAppraisalOfficeCrossReview = new Application_Model_AppraisalOfficeCrossReview();
            $QAppraisalOfficeCrossReview->deleteByTaskDeleted();

            $result = $QAppraisalOfficeField->update([
                'aof_is_deleted' => 1,
                'aof_updated_at' => date('Y-m-d H:i:s'),
                'aof_updated_by' => $userId
            ], $QAppraisalOfficeField->getAdapter()->quoteInto('aof_id = ? and aof_is_deleted = 0', $fieldId));
            $memberId = $field['aom_id'];
            $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMember();
            $QAppraisalOfficeMember->updateInitRatioByField($fieldId);
            if ($result > 0) {
                $fieldOther = $QAppraisalOfficeField
                    ->fetchAll($QAppraisalOfficeField->getAdapter()->quoteInto('aom_id = ? and aof_is_deleted = 0', $memberId))->toArray();
                if (count($fieldOther) === 0) {
                    $QAppraisalOfficeMember->update([
                        'aom_is_deleted' => 1,
                        'aom_updated_at' => date('Y-m-d H:i:s'),
                        'aom_updated_by' => $userId
                    ], $QAppraisalOfficeField->getAdapter()->quoteInto('aom_id = ? and aom_is_deleted = 0', $memberId));
                }
                echo json_encode([
                    'status' => 0,
                    'data' => ''
                ]);
                return;
            }
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Field not exits. Please reload page and try again!'
        ]);
    }

    public function staffApprovedPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');
        $memberId = $this->getRequest()->getParam('aom_id');
        $QMember = new Application_Model_AppraisalOfficeMember();
        $member = $QMember->fetchRow([
            $QMember->getAdapter()->quoteInto('aop_id = ?', $planId),
            $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId),
        ]);
        if($member) {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $member = $member->toArray();
            $member['aom_staff_approved'] = 1;
            $member['aom_updated_at'] = date('Y-m-d H:i:s');
            $member['aom_updated_by'] = $userId;
            $QMember->update($member, $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId));
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    
    public function headApprovedSetPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');
        $memberId = $this->getRequest()->getParam('aom_id');
        $QMember = new Application_Model_AppraisalOfficeMember();
        $member = $QMember->fetchRow([
            $QMember->getAdapter()->quoteInto('aop_id = ?', $planId),
            $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId),
        ]);
        if($member) {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $member = $member->toArray();
            $member['aom_head_approved'] = 1;
            $member['aom_updated_at'] = date('Y-m-d H:i:s');
            $member['aom_updated_by'] = $userId;
            $QMember->update($member, $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId));
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    
    public function headRollbackPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');
        $memberId = $this->getRequest()->getParam('aom_id');
        $QMember = new Application_Model_AppraisalOfficeMember();
        $member = $QMember->fetchRow([
            $QMember->getAdapter()->quoteInto('aop_id = ?', $planId),
            $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId),
        ]);
        if($member) {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $member = $member->toArray();
            $member['aom_head_approved'] = 0;
            $member['aom_staff_approved'] = 0;
            $member['aom_updated_at'] = date('Y-m-d H:i:s');
            $member['aom_updated_by'] = $userId;
            $QMember->update($member, $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId));
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    #endregion SetPRD
    
    #region ApprovePRD
    public function staffApprovedStarPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');
        $memberId = $this->getRequest()->getParam('aom_id');
        $QMember = new Application_Model_AppraisalOfficeMember();
        $member = $QMember->fetchRow([
            $QMember->getAdapter()->quoteInto('aop_id = ?', $planId),
            $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId),
        ]);
        if($member) {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $member = $member->toArray();
            $member['aom_staff_approved'] = 1;
            $member['aom_updated_at'] = date('Y-m-d H:i:s');
            $member['aom_updated_by'] = $userId;
            $QMember->update($member, $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId));
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    
    public function headApprovedStarPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');
        $memberId = $this->getRequest()->getParam('aom_id');
        $QMember = new Application_Model_AppraisalOfficeMember();
        $member = $QMember->fetchRow([
            $QMember->getAdapter()->quoteInto('aop_id = ?', $planId),
            $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId),
        ]);
        if($member) {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $member = $member->toArray();
            $member['aom_head_approved'] = 2;
            $member['aom_updated_at'] = date('Y-m-d H:i:s');
            $member['aom_updated_by'] = $userId;
            $QMember->update($member, $QMember->getAdapter()->quoteInto('aom_id = ?', $memberId));
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    #endregion ApprovePRD

    #region CrossReview
    public function crossReviewAction()
    {

    }

    public function getStaffCrossReviewAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $planId = $this->getRequest()->getParam('plan_id');
        $db = Zend_Registry::get('db');
        $stmt = $db
            ->select()
            ->from(['m' => 'appraisal_office_member'], ['m.aom_staff_id'])
            ->join(
                ['f' => 'appraisal_office_field'],
                sprintf('m.aop_id = %s and m.aom_id = f.aom_id and m.aom_is_deleted = 0 and f.aof_is_deleted = 0 and aom_head_approved > 0', $planId),
                []
            )
            ->join(
                ['t' => 'appraisal_office_task'],
                'f.aof_id = t.aof_id and t.aot_is_deleted = 0'
            )
            ->join(
                ['cr' => 'appraisal_office_cross_review'],
                sprintf('t.aot_id = cr.aot_id and cr.aocr_is_deleted = 0 and cr.aocr_staff_id = %s', $userId)
            );
        $data = $db->fetchAll($stmt);
        $staffIds = [];
        foreach ($data as $cr) {
            $staffIds[] = $cr['aom_staff_id'];
        }
        $staffs = [];
        if(count($staffIds) > 0) {
            $QStaff = new Application_Model_Staff();
            $staffs = $QStaff->fetchAll(
                $QStaff->select()
                    ->from($QStaff, ['id', 'code', 'email', 'firstname', 'lastname', 'department', 'team', 'title'])
                    ->where(sprintf('id in (%s) and off_date is null', implode(',', $staffIds)))
            )->toArray();
        }
        echo json_encode([
            'head_id' => $userId,
            'data' => $data,
            'staffs' => $staffs
        ]);
    }

    public function saveStaffCrossReviewAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        try {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $crId = $request->getParam('aocr_id');
            $crCr = $request->getParam('aocr_cross_review');
            $crNote = $request->getParam('aocr_note');
            $QCrossReview = new Application_Model_AppraisalOfficeCrossReview();

            $data = [
                'aocr_cross_review' => $crCr,
                'aocr_note' => $crNote,
                'aocr_updated_at' => date('Y-m-d H:i:s'),
                'aocr_updated_by' => $userId
            ];
            $QCrossReview->update($data, $QCrossReview->getAdapter()->quoteInto('aocr_id = ? and aocr_is_deleted = 0', $crId));
            echo json_encode([
                'status' => 0,
                'data' => ''
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 0,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function crossReviewApprovedPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $planId = $this->getRequest()->getParam('aop_id');
        $headId = $this->getRequest()->getParam('head_id');

        try {
            /** @var Zend_Db_Adapter_Abstract $db */
            $db   = Zend_Registry::get('db');
            $sql  = "update appraisal_office_member m
  join appraisal_office_field f on m.aom_id = f.aom_id and m.aom_is_deleted = 0 and m.aom_head_approved = 1 and m.aop_id = :plan_id
  join appraisal_office_task t on f.aof_id = t.aof_id and f.aof_is_deleted = 0
  join appraisal_office_cross_review cr on t.aot_id = cr.aot_id and t.aot_is_deleted = 0
set cr.aocr_approved = 1
where cr.aocr_staff_id = :staff_id and cr.aocr_is_deleted = 0 and cr.aocr_approved = 0";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
            $stmt->bindParam('staff_id', $headId, PDO::PARAM_INT);
            $result = $stmt->execute();
            $stmt->closeCursor();
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => 'Không thể cập nhật thông tin! Vui lòng thử lại sau.'
            ]);
            return;
        }

        if($result) {
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    #endregion CrossReview

    public function skillAction()
    {
        $QSkill = new Application_Model_AppraisalOfficeSkill();
        $skills = $QSkill->fetchAll([
            $QSkill->getAdapter()->quoteInto('aos_is_deleted = ?', 0)
        ], 'aos_id desc')->toArray();
        $this->view->skills = json_encode($skills);
    }

    public function saveSkillAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $skillId = $this->getRequest()->getParam('aos_id');
        $skillName = $this->getRequest()->getParam('aos_name');
        $skillStatus = $this->getRequest()->getParam('aos_status');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QSkill = new Application_Model_AppraisalOfficeSkill();
            $skill = [
                'aos_name' => $skillName,
                'aos_status' => $skillStatus,
                'aos_updated_at' => date('Y-m-d H:i:s'),
                'aos_updated_by' => $userStorage->id,
            ];
            if($skillId == 0) {
                $skill['aos_created_at'] = date('Y-m-d H:i:s');
                $skill['aos_created_by'] = $userStorage->id;
                $skillId = $QSkill->insert($skill);
            } else {
                $QSkill->update($skill, $QSkill->getAdapter()->quoteInto('aos_id = ?', $skillId));
            }
            echo json_encode([
                'status' => 0,
                'data' => $skillId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    }

    public function deleteSkillAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $skillId = $this->getRequest()->getParam('aos_id');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QSkill = new Application_Model_AppraisalOfficeSkill();
            $skill = [
                'aos_is_deleted' => 1,
                'aos_updated_at' => date('Y-m-d H:i:s'),
                'aos_updated_by' => $userStorage->id,
            ];
            $QSkill->update($skill, $QSkill->getAdapter()->quoteInto('aos_id = ?', $skillId));
            $QLevel = new Application_Model_AppraisalOfficeLevel();
            $level = [
                'aol_is_deleted' => 1,
                'aol_updated_at' => date('Y-m-d H:i:s'),
                'aol_updated_by' => $userStorage->id,
            ];
            $QLevel->update($level, $QLevel->getAdapter()->quoteInto('fk_aos = ?', $skillId));

            echo json_encode([
                'status' => 0,
                'data' => $skillId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    }

    public function getLevelAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $skillId = $this->getRequest()->getParam('aos_id');
        $QLevel = new Application_Model_AppraisalOfficeLevel();
        $levels = $QLevel->fetchAll([
            $QLevel->getAdapter()->quoteInto('fk_aos = ?', $skillId),
            $QLevel->getAdapter()->quoteInto('aol_is_deleted = ?', 0),
        ], 'aol_point asc')->toArray();
        echo json_encode([
            'status' => 0,
            'data' => $levels
        ]);
    }

    public function saveLevelAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $levelId = $this->getRequest()->getParam('aol_id');
        $skillId = $this->getRequest()->getParam('fk_aos');
        $levelName = $this->getRequest()->getParam('aol_name');
        $levelStatus = $this->getRequest()->getParam('aol_status');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QLevel = new Application_Model_AppraisalOfficeLevel();
            $level = [
                'aol_name' => $levelName,
                'fk_aos' => $skillId,
                'aol_status' => $levelStatus,
                'aol_updated_at' => date('Y-m-d H:i:s'),
                'aol_updated_by' => $userStorage->id,
            ];
            if($levelId == 0) {
                $level['aol_created_at'] = date('Y-m-d H:i:s');
                $level['aol_created_by'] = $userStorage->id;
                $levelId = $QLevel->insert($level);
            } else {
                $QLevel->update($level, $QLevel->getAdapter()->quoteInto('aol_id = ?', $levelId));
            }
            $QLevel->reindexPoint($skillId);

            echo json_encode([
                'status' => 0,
                'data' => $levelId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    }

    public function deleteLevelAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $levelId = $this->getRequest()->getParam('aol_id');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QLevel = new Application_Model_AppraisalOfficeLevel();
            $level = $QLevel->fetchRow($QLevel->getAdapter()->quoteInto('aol_id = ?', $levelId));
            if(!$level) {
                echo json_encode([
                    'status' => 1,
                    'data' => 'Không tìm thấy mục tiêu này! Vui lòng thử lại.'
                ]);
                return;
            }
            $skillId = $level['fk_aos'];
            $level = [
                'aol_is_deleted' => 1,
                'aol_updated_at' => date('Y-m-d H:i:s'),
                'aol_updated_by' => $userStorage->id,
            ];
            $QLevel->update($level, $QLevel->getAdapter()->quoteInto('aol_id = ?', $levelId));
            $QLevel->reindexPoint($skillId);
            echo json_encode([
                'status' => 0,
                'data' => $levelId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    }

    public function createSurveyAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name = $this->getRequest()->getParam('survey_name');
        $title = $this->getRequest()->getParam('survey_title');
        $db = null;
        try {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QDSurvey = new Application_Model_DynamicSurvey();
            $dsId = $QDSurvey->insert([
                'ds_name' => $name,
                'ds_title' => $title,
                'ds_ref' => 'appraisal_office',
                'ds_status' => 1,
                'ds_created_at' => date('Y-m-d H:i:s'),
                'ds_created_by' => $userStorage->id
            ]);
            $QSkill = new Application_Model_AppraisalOfficeSkill();
            $QLevel = new Application_Model_AppraisalOfficeLevel();
            $QDSQuestion = new Application_Model_DynamicSurveyQuestion();
            $QDSAnswer = new Application_Model_DynamicSurveyAnswer();
            $skills = $QSkill->fetchAll($QSkill->getAdapter()->quoteInto('aos_status = ? and aos_is_deleted = 0', 0));
            $template = json_decode('{"title":"Question","type":"range","id":"range","defaultValue":1,"display_label":true,"help":"","required":1,"checked_choices":["1"],"current_field":false}', true);
            foreach ($skills as $skill) {
                $template['title'] = $skill['aos_name'];
                $dsqId = $QDSQuestion->insert([
                    'fk_ds' => $dsId,
                    'dsq_title' => $skill['aos_name'],
                    'dsq_ref' => $skill['aos_id'],
                    'dsq_type' => 'range',
                    'dsq_data' => json_encode($template),
                    'dsq_created_at' => date('Y-m-d H:i:s'),
                    'dsq_created_by' => $userStorage->id
                ]);
                $levels = $QLevel->fetchAll($QLevel->getAdapter()->quoteInto('fk_aos = ? and aol_status = 0 and aol_is_deleted = 0', $skill['aos_id']), 'aol_point');
                foreach ($levels as $index => $level) {
                    $dsaId = $QDSAnswer->insert([
                        'fk_dsq' => $dsqId,
                        'dsa_ref' => $level['aol_id'],
                        'dsa_title' => $level['aol_name'],
                        'dsa_value' => $level['aol_point'],
                        'dsa_sort_id' => $index + 1,
                        'dsa_created_at' => date('Y-m-d H:i:s'),
                        'dsa_created_by' => $userStorage->id,
                    ]);
                }
            }
            $db->commit();
            echo json_encode([
                'status' => 0,
                'message' => 'Success',
            ]);
        } catch (Exception $ex) {
            $db->rollback();
            echo json_encode(array('status' => 1, 'message' => $ex->getMessage()));
        }
    }

    public function viewStaffAction()
    {
        try {
            $year = date('Y');
            $staffId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $QAO = new Application_Model_AppraisalOffice();
            $dataQuarter = $QAO->getStaffQuarter($staffId, $year);
            $dataCapacityByYear = $QAO->getStaffCapacityByYear($staffId, $year);
            $dataCapacityCompare = $QAO->getStaffCapacityCompare($staffId, $year);
            $this->view->dataQuarter = json_encode($dataQuarter);
            $this->view->dataCapacityByYear = json_encode($dataCapacityByYear);
            $this->view->dataCapacityCompare = json_encode($dataCapacityCompare);
        } catch (\Exception $ex) {
            $this->view->dataQuarter = [];
            $this->view->dataCapacityByYear = [];
            $this->view->dataCapacityCompare = [];
        }
    }

    public function viewManagerAction()
    {
        try {
            $year = date('Y');
            $staff = Zend_Auth::getInstance()->getStorage()->read()->id;
            $QAO = new Application_Model_AppraisalOffice();
            $dataQuarter = $QAO->getManagerQuarter($year, $staff->id, $staff->title);
            $dataCapacity = $QAO->getManagerCapacity($year, $staff->id, $staff->title);
            $this->view->dataQuarter = json_encode($dataQuarter);
            $this->view->dataCapacity = json_encode($dataCapacity);
        } catch (\Exception $ex) {
            $this->view->dataQuarter = [];
            $this->view->dataCapacity = [];
        }
    }

    public function getQuarterAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $year = $this->getRequest()->getParam('year');
            $staffId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $QAO = new Application_Model_AppraisalOffice();
            $dataQuarter = $QAO->getStaffQuarter($staffId, $year);
            echo json_encode([
                'status' => 0,
                'data' => $dataQuarter
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function getCapacityAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $year = $this->getRequest()->getParam('year');
            $staffId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $QAO = new Application_Model_AppraisalOffice();
            $dataCapacityByYear = $QAO->getStaffCapacityByYear($staffId, $year);
            $dataCapacityCompare = $QAO->getStaffCapacityCompare($staffId, $year);
            echo json_encode([
                'status' => 0,
                'dataCapacityByYear' => $dataCapacityByYear,
                'dataCapacityCompare' => $dataCapacityCompare
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function getQuarterManagerAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $year = $this->getRequest()->getParam('year');
            $staff = Zend_Auth::getInstance()->getStorage()->read();
            $QAO = new Application_Model_AppraisalOffice();
            $dataQuarter = $QAO->getManagerQuarter($year, $staff->id, $staff->title);
            echo json_encode([
                'status' => 0,
                'data' => $dataQuarter
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function getCapacityManagerAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $year = $this->getRequest()->getParam('year');
            $staff = Zend_Auth::getInstance()->getStorage()->read();
            $QAO = new Application_Model_AppraisalOffice();
            $dataCapacity = $QAO->getManagerCapacity($year, $staff->id, $staff->title);
            echo json_encode([
                'status' => 0,
                'dataCapacity' => $dataCapacity,
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function viewResultAction()
    {
        $fromName = $this->getRequest()->getParam('from_name', '');
        $toName = $this->getRequest()->getParam('to_name', '');
        $fromTitle = $this->getRequest()->getParam('from_title', 0);
        $toTitle = $this->getRequest()->getParam('to_title', 0);
        $planId = $this->getRequest()->getParam('plan_id', 0);
        $export = $this->getRequest()->getParam('export', 0);
        $page = $this->getRequest()->getParam('page', 1);
        
        $data = [
            'from_name' => $fromName,
            'to_name' => $toName,
            'from_title' => $fromTitle,
            'to_title' => $toTitle,
            'plan_id' => $planId,
        ];

        if($export > 0) {
            My_Report_Trainer::appraisalSaleResult($data, $export);
        }

        $QView = new Application_Model_AppraisalOfficeView();
        $total = 0;
        $result = $QView->getResult($data, $total, $page, $export);
        
        
        $this->view->url = HOST . 'appraisal-office/view-result' . ( $data ? '?' . http_build_query($data) . '&' : '?' );
        $this->view->data = json_encode($result);
        $this->view->params = $data;
        $this->view->export = $export;
        $this->view->page = $page;
        $this->view->total = $total;
    }
    
    public function toDoSurveyAction()
    {
        $planID = $this->getRequest()->getParam('plan_id');
        
        $user = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $user->id;

        $QView = new Application_Model_AppraisalOfficeView();
        $todo = $QView->getToDoSurvey($userId, $planID);
        
        $staffIds = [];
        foreach ($todo['superior'] as $item) {
            $staffIds[] = $item['id'];
        }
        foreach ($todo['subordinate'] as $item) {
            $staffIds[] = $item['id'];
        }
        foreach ($todo['self'] as $item) {
            $staffIds[] = $item['id'];
        }
        $QStaff = new Application_Model_Staff();

        $staffs = [];
        if(count($staffIds) > 0) {
            $staffs = $QStaff->fetchAll(
                $QStaff->select()
                    ->from($QStaff, ['id', 'code', 'email', 'firstname', 'lastname', 'title'])
                    ->where(sprintf('id in (%s) and off_date is null', implode(',', $staffIds)))
            )->toArray();
        }

        $QMember = new Application_Model_AppraisalOfficeMember();
        $member = $QMember->getByPlanAndStaff($planID, $userId);
        
        
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $userTitleId = $QToDo->getTitleById($userId, $planID);
        
        $data = [
            'plan' => $planID,
            'staff_id' => $userId,
            'staff_title' => $userTitleId,
            'staff_title_id' => $userTitleId,
            'to_do' => $todo,
            'member' => $member,
            'staffs' => $staffs
        ];
        $this->view->data = self::escapeJsonString(json_encode($data));
    } // Done
    
    
    protected function escapeJsonString($value)
    {
        $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
        $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
        $result = str_replace($escapers, $replacements, $value);
        return $result;
    } // Done
    
    
    public function getResultDetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $planId = $this->getRequest()->getParam('plan_id');
        $fromStaff = $this->getRequest()->getParam('from_staff');
        $toStaff = $this->getRequest()->getParam('to_staff');

        $QView = new Application_Model_AppraisalOfficeView();
        $result = $QView->getResultDetail($planId, $fromStaff, $toStaff);

        echo json_encode([
            'status' => 0,
            'data' => $result
        ]);
    }
    
    public function organizationChartAction()
    {
        $QAppraisalOfficeToDo = new Application_Model_AppraisalOfficeToDo();
        
        $to_do = $QAppraisalOfficeToDo->fetchAll();
        
    }
    
    public function resultChartAction()
    {
        $planId = $this->getRequest()->getParam('plan_id');
        
        $QView = new Application_Model_AppraisalOfficeView();
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        
        $result = $QView->getResultChart($planId);
        $office_plan = $QAppraisalOfficePlan->fetchRow(["aop_id = ?" => $planId]);
        
        $data = [];
        $name = [];
        
        foreach($result as $key=>$value){
            $data[$value['fk_dsq']][$value['fk_dsa']] = $value['total'];
            $name[$value['fk_dsq']] = $value['dsq_title'];
        }
        
        $this->view->name = $name;
        $this->view->data = $data;
        $this->view->office_plan = $office_plan;
        
    }
    
    public function listPrdAction()
    {
        $sort = $this->getRequest()->getParam('sort', '');
        $desc = $this->getRequest()->getParam('desc', 1);
        $sn = $this->getRequest()->getParam('sn');
        $from_name = $this->getRequest()->getParam('from_name');
        $from_code = $this->getRequest()->getParam('from_code');
        $to_name = $this->getRequest()->getParam('to_name');
        $to_code = $this->getRequest()->getParam('to_code');
        $department = $this->getRequest()->getParam('department');
        $team = $this->getRequest()->getParam('team');
        $title = $this->getRequest()->getParam('title');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;
        
        $QTeam = new Application_Model_Team();
        $QAppraisalOfficeToDo = new Application_Model_AppraisalOfficeToDo();
        
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
        $teams            = $QTeam->get_cache();
        

        $params = array(
            'sn' => $sn,
            'from_name' => $from_name,
            'to_name' => $to_name,
            'department' => $department,
            'team' => $team,
            'title' => $title,
            'staff_head_id' => $userStorage->id,
            'is_admin' => $is_admin,
            'from_code' => $from_code,
            'to_code'   => $to_code
        );

        $params['sort'] = $sort;
        $params['desc'] = $desc;


        $QMember = new Application_Model_AppraisalOfficeMemberRoot();
        $page = $this->getRequest()->getParam('page', 1);
        $limit = 20;
        $total = 0;
        $result = $QMember->fetchPagination($page, $limit, $total, $params);
        
        
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
	$this->view->teams = $teams;
        $this->view->staffs = $result;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->params = $params;
        $this->view->desc = $desc;
        $this->view->sort = $sort;
        $this->view->url = HOST . 'appraisal-office/list-prd' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
        $this->view->offset = $limit * ($page - 1);
    }
    
    public function prdStaffDetailsAction()
    {
        $staff_id = $this->getRequest()->getParam('staff_id');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $review_staff = $userStorage->id;
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QStaff = new Application_Model_Staff();
        
        $is_admin = (in_array($userStorage->id, $this->list_admmin)) ? 1 : 0;
        $is_head = $QToDo->checkIsHead($userStorage->id);
        
        if(!$is_admin AND !$is_head){
            $staff_id = $userStorage->id;
        }
        
        $this->view->staff = $QStaff->fetchRow(['id = ?' => $staff_id]);
    }
    
    public function getPrdByStaffAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $staff_id = $this->getRequest()->getParam('staff_id');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $userStorage->id;
        
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $staff = $QToDo->getStaffById($staff_id);

        
        if(count($staff) > 0) {
            $staff = $staff[0];
        }

        $QMember = new Application_Model_AppraisalOfficeMemberRoot();
        $member = $QMember->fetchRow(['aom_staff_id = ?' => $staff_id]);

        $fields = [];
        if ($member) {
            $member = $member->toArray();
            $QField = new Application_Model_AppraisalOfficeFieldRoot();
            $fields = $QField->fetchAll(sprintf(
                'aom_id = %s and aof_is_deleted = 0',
                $member['aom_id']
            ))->toArray();
        }

        $tasks = [];
        if (count($fields) > 0) {
            $fieldIds = [];
            foreach ($fields as $field) {
                $fieldIds[] = $field['aof_id'];
            }
            $QTask = new Application_Model_AppraisalOfficeTaskRoot();
            $tasks = $QTask->fetchAll(sprintf(
                'aof_id in (%s) and aot_is_deleted = 0',
                implode(', ', $fieldIds)
            ))->toArray();
        }

        $crossReviews = [];
        if (count($tasks) > 0) {
            $taskIds = [];
            foreach ($tasks as $task) {
                $taskIds[] = $task['aot_id'];
            }

            /** @var Zend_Db_Adapter_Abstract $db */
            $db = Zend_Registry::get('db');
            $stmt = $db
                ->select()
                ->from(['cr' => 'appraisal_office_cross_review'], ['cr.*'])
                ->join(['s' => 'staff'], 'cr.aocr_staff_id = s.id', ['s.id', 's.code', 's.lastname', 's.firstname', 's.email', 's.department', 's.team', 's.title'])
                ->where(sprintf(
                    'aot_id in (%s) and aocr_is_deleted = 0',
                    implode(', ', $taskIds)
                ));
            $crossReviews = $db->fetchAll($stmt);
        }
        
        //$QHeadDepartment = new Application_Model_HeadOfDepartment();
        //$headId = $QHeadDepartment->getHeadIdByDepartment($staff['department']);
        
        $headId = 6705;
        
        
        echo json_encode([
            'plan_id' => $planId,
            'head_id' => $headId,
            'staff' => $staff,
            'member' => $member,
            'fields' => $fields,
            'tasks' => $tasks,
            'cross_reviews' => $crossReviews
        ]);
    }
    
    public function saveFieldRootAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $headId = $this->getRequest()->getParam('head_id');
        $staffId = $this->getRequest()->getParam('staff_id');
        $planId = $this->getRequest()->getParam('aop_id');
        $memberId = $this->getRequest()->getParam('aom_id');
        $fieldId = $this->getRequest()->getParam('aof_id');
        $fieldName = $this->getRequest()->getParam('aof_name');
        $memberData = [
            'aop_id' => $planId,
            'aom_head_department_id' => $headId,
            'aom_head_approved' => 0,
            'aom_staff_approved' => 0,
            'aom_staff_id' => $staffId,
            'aom_created_at' => date('Y-m-d H:i:s'),
            'aom_created_by' => $userId,
            'aom_updated_at' => date('Y-m-d H:i:s'),
            'aom_updated_by' => $userId,
        ];
        $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMemberRoot();
        if ($memberId == 0) {
            $memberId = $QAppraisalOfficeMember->insert($memberData);
            $memberData['aom_id'] = $memberId;
        }
        $field = [
            'aom_id' => $memberId,
            'aof_name' => $fieldName,
            'aof_updated_at' => date('Y-m-d H:i:s'),
            'aof_updated_by' => $userId
        ];
        $QAppraisalOfficeField = new Application_Model_AppraisalOfficeFieldRoot();
        if ($fieldId == 0) {
            $field['aof_created_at'] = date('Y-m-d H:i:s');
            $field['aof_created_by'] = $userId;
            $fieldId = $QAppraisalOfficeField->insert($field);
            $field['aof_id'] = $fieldId;
        } else {
            $QAppraisalOfficeField->update(
                $field,
                $QAppraisalOfficeField->getAdapter()->quoteInto('aof_id = ?', $fieldId)
            );
        }
        $QAppraisalOfficeMember->updateInitRatioByField($fieldId);
        echo json_encode([
            'status' => 0,
            'data' => [
                'member' => $memberData,
                'field_id' => $fieldId
            ]
        ]);
    }
    
    public function deleteFieldRootAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $fieldId = $this->getRequest()->getParam('aof_id');
        $QAppraisalOfficeField = new Application_Model_AppraisalOfficeFieldRoot();
        $field = $QAppraisalOfficeField->fetchRow($QAppraisalOfficeField->getAdapter()->quoteInto('aof_id = ? and aof_is_deleted = 0', $fieldId));
        if ($field) {
            $QAppraisalOfficeTask = new Application_Model_AppraisalOfficeTaskRoot();
            $QAppraisalOfficeTask->update([
                'aot_is_deleted' => 1,
                'aot_updated_at' => date('Y-m-d H:i:s'),
                'aot_updated_by' => $userId
            ], $QAppraisalOfficeTask->getAdapter()->quoteInto('aof_id = ? and aot_is_deleted = 0', $fieldId));

            $QAppraisalOfficeCrossReview = new Application_Model_AppraisalOfficeCrossReview();
            $QAppraisalOfficeCrossReview->deleteByTaskDeleted();

            $result = $QAppraisalOfficeField->update([
                'aof_is_deleted' => 1,
                'aof_updated_at' => date('Y-m-d H:i:s'),
                'aof_updated_by' => $userId
            ], $QAppraisalOfficeField->getAdapter()->quoteInto('aof_id = ? and aof_is_deleted = 0', $fieldId));
            $memberId = $field['aom_id'];
            $QAppraisalOfficeMember = new Application_Model_AppraisalOfficeMemberRoot();
            $QAppraisalOfficeMember->updateInitRatioByField($fieldId);
            if ($result > 0) {
                $fieldOther = $QAppraisalOfficeField
                    ->fetchAll($QAppraisalOfficeField->getAdapter()->quoteInto('aom_id = ? and aof_is_deleted = 0', $memberId))->toArray();
                if (count($fieldOther) === 0) {
                    $QAppraisalOfficeMember->update([
                        'aom_is_deleted' => 1,
                        'aom_updated_at' => date('Y-m-d H:i:s'),
                        'aom_updated_by' => $userId
                    ], $QAppraisalOfficeField->getAdapter()->quoteInto('aom_id = ? and aom_is_deleted = 0', $memberId));
                }
                echo json_encode([
                    'status' => 0,
                    'data' => ''
                ]);
                return;
            }
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Field not exits. Please reload page and try again!'
        ]);
    }
    
    public function deleteTaskRootAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        try {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $taskId = $this->getRequest()->getParam('aot_id');
            $QTask = new Application_Model_AppraisalOfficeTaskRoot();
            $task = $QTask->fetchRow($QTask->getAdapter()->quoteInto('aot_id = ? and aot_is_deleted = 0', $taskId));
            if ($task) {
                $QTask->update([
                    'aot_is_deleted' => 1,
                    'aot_updated_at' => date('Y-m-d H:i:s'),
                    'aot_updated_by' => $userId
                ], $QTask->getAdapter()->quoteInto('aot_id = ? and aot_is_deleted = 0', $taskId));

                $QCrossReview = new Application_Model_AppraisalOfficeCrossReview();
                $QCrossReview->deleteByTaskDeleted();

                (new Application_Model_AppraisalOfficeMember())->updateInitRatioByField($task['aof_id']);
                echo json_encode([
                    'status' => 0,
                    'data' => ''
                ]);
            } else {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Cannot found this task. Please reload page and try again!'
                ]);
            }
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage() . $ex->getTraceAsString()
            ]);
        }
    }
    
    public function saveTaskRootAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        try {
            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
            $taskId = $this->getRequest()->getParam('aot_id');
            $data = [
                'aof_id' => $params['aof_id'],
                'aot_target' => $params['aot_target'],
                'aot_task' => $params['aot_task'],
                'aot_skill' => $params['aot_skill'],
                'aot_ratio' => $params['aot_ratio'],
                'aot_self_appraisal' => $params['aot_self_appraisal'],
                'aot_note' => !empty($params['aot_note']) ? $params['aot_note'] : NULL,
                'aot_head_department_appraisal' => $params['aot_head_department_appraisal'],
                'aot_head_department_note' => !empty($params['aot_head_department_note']) ? $params['aot_head_department_note'] : NULL,
                'aot_updated_at' => date('Y-m-d H:i:s'),
                'aot_updated_by' => $userId
            ];

            $QTask = new Application_Model_AppraisalOfficeTaskRoot();
            if ($taskId == 0) {
                $data['aot_created_at'] = date('Y-m-d H:i:s');
                $data['aot_created_by'] = $userId;
                $taskId = $QTask->insert($data);
            } else {
                $QTask->update($data, $QTask->getAdapter()->quoteInto('aot_id = ? and aot_is_deleted = 0', $taskId));
            }
            (new Application_Model_AppraisalOfficeMemberRoot())->updateInitRatioByField($data['aof_id']);
            echo json_encode([
                'status' => 0,
                'data' => $taskId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function uploadPrdAction()
    {
        
    }
    
    public function saveUploadPrdAction(){
        
        include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
        
	error_reporting(0);
	set_time_limit(0);

	$current_month = date('m', strtotime(date('Y-m')." -1 month"));
	$current_year  = date('Y', strtotime(date('Y-m')." -1 month"));

	echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';
	echo '<div class="theme-showcase">';
	//================Process Uploaded File====================
	If($_FILES["file"]["tmp_name"] != ""){
            if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
                    //die("Không phải file XLSX !");
            }
            if ($_FILES["file"]["error"] > 0)
            {
            die("Return Code: " . $_FILES["file"]["error"] . "<br />");
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], "files/appraisal-ofice/data.xlsx");
	}
	//===================================================
	
	//===================Main=======================
        
	$inputfile = 'files/appraisal-ofice/data.xlsx';
	
        $xlsx = new SimpleXLSX($inputfile);
	$data = $xlsx->rows();
        
        
        
	// Đếm số cột, dựa vào số tiêu đề ở dòng 0, nếu 23 cột là headoffice, còn 25 cột là bảo hành, type = 3 đổi lại thành 27 cột để không 
	$i = 0;
	$num_cols = 0;
	while ($data[0][$i++] <> '') {
	  $num_cols++;
	}
	
        define('fullname',              0);
        define('code',                  1);
        define('linh_vuc',	 	2);
        define('tieu_chuan', 		3);
        define('nhiem_vu', 		4);
        define('ky_nang', 		5);
        define('ty_le', 		6);

	$i = 1;
	$num_rows = 0;
	while ($data[$i++][3] <> '') {
	  $num_rows++;
	}
        
        $all_data = [];
        
        $fullname = NULL;
        $code = NULL;
        $linh_vuc = NULL;
	for($i = 1; $i <= $num_rows; $i++){
            
            $fullname = !empty($data[$i][fullname]) ? $data[$i][fullname] : $fullname;
            $code = !empty($data[$i][code]) ? trim($data[$i][code]) : trim($code);
            $linh_vuc = !empty($data[$i][linh_vuc]) ? $data[$i][linh_vuc] : $linh_vuc;
            $tieu_chuan = $data[$i][tieu_chuan];
            $nhiem_vu = $data[$i][nhiem_vu];
            $ky_nang = $data[$i][ky_nang];
            $ty_le = $data[$i][ty_le];
            
            $all_data[] = [
                'fullname' => $fullname,
                'code'  => $code,
                'linh_vuc' => $linh_vuc,
                'tieu_chuan' => $tieu_chuan,
                'nhiem_vu' => $nhiem_vu,
                'ky_nang' => $ky_nang,
                'ty_le' => $ty_le,
            ];
	}
        
        $task = [];
        foreach($all_data as $key=>$value){
            $task[$value['code']][$value['linh_vuc']][] = $value;
        }
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $QAppraisalOfficeMemberRoot = new Application_Model_AppraisalOfficeMemberRoot();
        $QAppraisalOfficeFieldRoot = new Application_Model_AppraisalOfficeFieldRoot();
        $QAppraisalOfficeTaskRoot = new Application_Model_AppraisalOfficeTaskRoot();
        
        $all_member = $QAppraisalOfficeMemberRoot->getAllMember();
        
        
        if ($this->getRequest()->getMethod() == 'POST') {
        
            $db = Zend_Registry::get('db');
            $db->beginTransaction();

            try {

                foreach($task as $key=>$value){
                    $aom_id = $all_member[$key];
                    
                    if(empty($aom_id)){
                        echo "Nhân viên không tồn tại!:".$key;
                        $db->rollback();
                        exit;
                    }
                    $field_update = ['aof_is_deleted' => 1];
                    
                    $where_field_update = $QAppraisalOfficeFieldRoot->getAdapter()->quoteInto('aom_id = ?', $aom_id);
                    $QAppraisalOfficeFieldRoot->update($field_update,$where_field_update);

                    foreach($value as $k=>$v){
                        $field_insert = [
                            'aom_id' => $aom_id,
                            'aof_name' => $k,
                            'aof_created_at' => date('Y-m-d H:i:s'),
                            'aof_created_by' => $userStorage->id,
                        ];


                        $field_id = $QAppraisalOfficeFieldRoot->insert($field_insert);

                        foreach($v as $i=>$j){

                            $ty_le = $j['ty_le']*100;

                            if($ty_le <= 100 AND $ty_le > 0){
                                $task_insert = [
                                    'aof_id'        => $field_id,
                                    'aot_target'    => $j['tieu_chuan'],
                                    'aot_task'      => $j['nhiem_vu'],
                                    'aot_skill'     => $j['ky_nang'],
                                    'aot_ratio'     => $ty_le
                                ];
                                $task_id = $QAppraisalOfficeTaskRoot->insert($task_insert);
                            }
                            else{
                                echo "Tỷ lệ không chính xác!";
                                $db->rollback();
                                exit;
                            }
                        }
                    }
                    echo "Hoàn Thành: ".$key."<br>";
                }
                $db->commit();

            } catch (Exception $e) {
                $db->rollback();
                echo 'Caught exception: ', $e->getMessage(), "\n";exit;
            }

            unlink($inputfile);

            
        }
        
        $this->_helper->layout->disableLayout();

    }
    
    public function uploadPrdNoResultAction()
    {
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        
        $plan = $QAppraisalOfficePlan->fetchAll(['aop_to >= ?' => date('Y-m-d'), 'aop_type = ?' => 1, 'aop_is_deleted = ?' => 0]);
        
        $this->view->plan = $plan;
    }
    
    public function uploadPrdResultAction()
    {
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        
        $plan = $QAppraisalOfficePlan->fetchAll(['aop_to <= ?' => date('Y-m-d'), 'aop_type = ?' => 1, 'aop_is_deleted = ?' => 0]);
        
        $this->view->plan = $plan;
    }
    
    public function saveUploadPrdResultAction(){
        require_once 'appraisal-office'.DIRECTORY_SEPARATOR.'save-upload-prd-result.php';
    }
    
    public function reAppraisalPrdAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $aom_id = $this->getRequest()->getParam('aom_id');
        
        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        
        $QMember = new Application_Model_AppraisalOfficeMember();
        $result = $QMember->update([
            'aom_staff_approved' => 0,
        ], [
            $QMember->getAdapter()->quoteInto('aom_id = ?', $aom_id),
        ]);
        if($result > 0) {
            echo json_encode([
                'status' => 0,
                'data' => 'Thành công!'
            ]);
            return;
        }
        echo json_encode([
            'status' => 1,
            'data' => 'Không tìm thấy thông tin! Vui lòng thử lại sau.'
        ]);
    }
    
    public function toDoRootAction(){
        
        $staff_id = $this->getRequest()->getParam('staff_id');
        $from_staff = $this->getRequest()->getParam('from_staff');
        $is_prd = $this->getRequest()->getParam('is_prd');
        
        
        $flashMessenger     = $this->_helper->flashMessenger;
        $QStaff = new Application_Model_Staff();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QToDoRoot = new Application_Model_AppraisalOfficeToDoRoot();
        $QAppraisalOfficeMemberRoot = new Application_Model_AppraisalOfficeMemberRoot();
        
        $all_staff = $QToDo->getStaffOffice();
        $staff = $QToDo->getStaffApproveByStaff($staff_id);
        
        
        if ($this->getRequest()->isPost()){
            
            $db = Zend_Registry::get('db');
            
            try {
                $db->beginTransaction();
                
                $from = $QStaff->fetchRow(['id = ?' => $from_staff]);
                
                if($from){
                    $data = [
                        'from_staff' => $from['id'],
                        'from_title' => $from['title'],
                        'is_prd'     => !empty($is_prd) ? $is_prd : 0
                    ];
                    
                    $data_member = [
                        'aom_head_department_id' => $from['id'],
                    ];
                    
                    $where = [];
                    $where[] = $QToDoRoot->getAdapter()->quoteInto('to_staff = ?', $staff['id']);
                    $QToDoRoot->update($data, $where);
                    
                    $where = [];
                    $where[] = $QAppraisalOfficeMemberRoot->getAdapter()->quoteInto('aom_staff_id = ?', $staff['id']);
                    $QAppraisalOfficeMemberRoot->update($data_member, $where);
                }
                
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Done!');
                $this->redirect('/appraisal-office/to-do-root?staff_id='.$staff_id);
                
                
            } catch (Exception $e) {
                
                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage('Có lỗi!');
                $this->redirect('/appraisal-office/to-do-root?staff_id='.$staff_id);
            }
            
        }
        
        $this->view->staff = $staff;
        $this->view->all_staff = $all_staff;
        
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages;

        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages_error;
        
    }
    
    public function createToDoRootAction(){
        
        $to_staff_id = $this->getRequest()->getParam('to_staff');
        $from_staff_id = $this->getRequest()->getParam('from_staff');
        
        
        $flashMessenger     = $this->_helper->flashMessenger;
        $QStaff = new Application_Model_Staff();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QToDoRoot = new Application_Model_AppraisalOfficeToDoRoot();
        $QAppraisalOfficeMemberRoot = new Application_Model_AppraisalOfficeMemberRoot();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $params = [
            'to_staff_id' => $to_staff_id,
            'from_staff_id' => $from_staff_id
        ];
        
        $staff_not_prd = $QToDo->getStaffOfficeNotPrd();
        $all_staff = $QToDo->getStaffOffice();
        
        if ($this->getRequest()->isPost()){
            
            $db = Zend_Registry::get('db');
            
            try {
                $db->beginTransaction();
                
                $from_staff = $QStaff->fetchRow(['id = ?' => $from_staff_id]);
                
                $to_staff = $QStaff->fetchRow(['id = ?' => $to_staff_id]);
                
                $staff_to_do = $QToDoRoot->fetchRow(['to_staff = ?' => $to_staff_id]);
                
                
                if($staff_to_do){
                    $db->rollback();
                    $flashMessenger->setNamespace('error')->addMessage('Người này đã có PRD. Vui lòng kiểm tra lại!');
                    $this->redirect('/appraisal-office/create-to-do-root');
                }
                else{
                    
                    $data = [
                        'from_staff' => $from_staff['id'],
                        'from_title' => $from_staff['title'],
                        'to_staff' => $to_staff['id'],
                        'to_title' => $to_staff['title'],
                        'type' => 1,
                        'is_del' => 0,
                    ];
                    
                    $QToDoRoot->insert($data);
                    
                    $data_member = [
                        'aom_contact_id' => NULL,
                        'aom_head_department_id' => $from_staff['id'],
                        'aom_staff_id' => $to_staff['id'],
                        'aom_head_approved' => 0,
                        'aom_staff_approved' => 0,
                        'aom_init_ratio' => 100,
                        'aom_created_at' => date('Y-m-d H:i:s'),
                        'aom_created_by' => $userStorage->id,
                        'aom_updated_at' => date('Y-m-d H:i:s'),
                        'aom_updated_by' => $userStorage->id,
                        'aom_is_deleted' => 0,
                    ];
                    
                    $QAppraisalOfficeMemberRoot->insert($data_member);
                }
                
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Done!');
                $this->redirect('/appraisal-office/create-to-do-root');
                
                
            } catch (Exception $e) {
                
                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage('Có lỗi!'. $e->getMessage());
                $this->redirect('/appraisal-office/create-to-do-root');
            }
            
        }
        
        $this->view->staff_not_prd = $staff_not_prd;
        $this->view->all_staff = $all_staff;
        $this->view->params = $params;
        
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages;

        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages_error;
        
    }
    
    public function preSetPrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'pre-set-prd.php';
    }
    
    public function viewResultStaffAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'view-result-staff.php';
    }
    
    public function viewPrdDetailsStaffAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'view-prd-details-staff.php';
    }
    
    public function preStarPrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'pre-star-prd.php';
    }
    
    public function starPrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'star-prd.php';
    }
    
    public function preListPrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'pre-list-prd.php';
    }
    
    public function preListPrdAdminAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'pre-list-prd-admin.php';
    }
    
    public function listApprovePrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'list-approve-prd.php';
    }
    
    public function listApprovePrdAdminAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'list-approve-prd-admin.php';
    }
    
    public function approvePrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'approve-prd.php';
    }
    
    public function preApprovePrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'pre-approve-prd.php';
    }
    
    public function listStarPrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'list-star-prd.php';
    }
    
    public function starApprovePrdAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'star-approve-prd.php';
    }
    
    public function viewResultHeadAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'view-result-head.php';
    }
    
    public function viewResultAdminAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'view-result-admin.php';
    }
    
    public function adminAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'admin.php';
    }
    
    public function loadStaffOldAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'load-staff-old.php';
    }
    
    public function saveRecommendAction(){
        require_once 'appraisal-office' . DIRECTORY_SEPARATOR . 'save-recommend.php';
    }
    
}
