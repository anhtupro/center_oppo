<?php
$flashMessenger              = $this->_helper->flashMessenger;
$messages                    = $flashMessenger->setNamespace('success')->getMessages();
$this->view->message_success = $messages;
$messages_error              = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages        = $messages_error;

$userStorage                 = Zend_Auth::getInstance()->getStorage()->read();
$QLog                        = new Application_Model_Log();
$QStaff 					 = new Application_Model_Staff();
$ip                          = $this->getRequest()->getServer('REMOTE_ADDR');
$db                          = Zend_Registry::get('db');

$page                        = $this->getRequest()->getParam('page', 1);
$sort                        = $this->getRequest()->getParam('sort', '');
$desc                        = $this->getRequest()->getParam('desc', 1);
$type                        = $this->getRequest()->getParam('type');
$from_date                   = $this->getRequest()->getParam('from_date');
$to_date                     = $this->getRequest()->getParam('to_date');

$QTrainerCourse = new Application_Model_TrainerCourse();
$QTrainerCourseType = new Application_Model_TrainerCourseType();
$Area = new Application_Model_Area();

$trainer_course_type = $QTrainerCourseType->get_cache();
$this->view->trainer_course_type = $trainer_course_type;
$this->view->area = $Area->get_cache();


$arr_created_by = $QTrainerCourse->getTrainerByLeader($userStorage->id);
$params             = array_filter(array(
    'sort'          =>$sort,
    'desc'          =>$desc,
    'type'          =>$type,
    'from_date'     =>$from_date,
    'to_date'       =>$to_date,
    'created_by'    =>$arr_created_by
));

$full_rights_trainer = unserialize(FULL_RIGHTS_TRAINER);
$key_project = unserialize(TRAINER_KEY_PROJECT);

if(in_array($userStorage->title,$full_rights_trainer) 
	|| in_array($userStorage->id,$key_project) 
	|| $userStorage->group_id == ADMINISTRATOR_ID 
        || $userStorage->id == 14682
	||  $userStorage->group_id == HR_ID){
    unset($params['created_by']);
}

$limit                      = LIMITATION;
$total                      = 0;
$rows                       = $QTrainerCourse->fetchPagination($page, $limit, $total, $params);

$this->view->desc           = $desc;
$this->view->sort           = $sort;
$this->view->params         = $params;
$this->view->list           = $rows;
$this->view->limit          = $limit;
$this->view->total          = $total;
$this->view->url            = HOST.'trainer/course'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset         = $limit*($page-1);
$this->view->cached_staff   = $QStaff->get_cache();
$this->view->userStorage    = $userStorage;