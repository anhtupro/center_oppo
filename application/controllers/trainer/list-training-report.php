<?php
$flashMessenger              = $this->_helper->flashMessenger;
$messages                    = $flashMessenger->setNamespace('success')->getMessages();
$this->view->message_success = $messages;
$messages_error              = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages        = $messages_error;

$userStorage                 = Zend_Auth::getInstance()->getStorage()->read();
$QLog                        = new Application_Model_Log();
$ip                          = $this->getRequest()->getServer('REMOTE_ADDR');
$db                          = Zend_Registry::get('db');

$page                        = $this->getRequest()->getParam('page', 1);
$sort                        = $this->getRequest()->getParam('sort', '');
$desc                        = $this->getRequest()->getParam('desc', 1);
$date                        = $this->getRequest()->getParam('date',null);
$trainer_dealer                   = $this->getRequest()->getParam('trainer_dealer',null);
$store                       = $this->getRequest()->getParam('store',null);
$area                        = $this->getRequest()->getParam('area',null);
$province                    = $this->getRequest()->getParam('province',null);
$type                        = $this->getRequest()->getParam('type',null);
$from_date                   = $this->getRequest()->getParam('from_date',date('Y-m-01'));
$to_date                     = $this->getRequest()->getParam('to_date',date('Y-m-t'));
$export                      = $this->getRequest()->getParam('export');

$QArea                       = new Application_Model_Area();
$this->view->cachedArea      = $QArea->get_cache();

$QRegionalMarket                  = new Application_Model_RegionalMarket();
$this->view->cachedRegionalMarket = $QRegionalMarket->get_cache();

$QTrainerDealer                   = new Application_Model_TrainerDealer();
$this->view->trainer_dealer_cache = $QTrainerDealer->get_cache();

$QStaffTrainingReport        = new Application_Model_StaffTrainingReport();
$staff_id                    = $userStorage->id;
$QStore                      = new Application_Model_Store();
$this->view->cachedStore     = $QStore->get_cache();

$QStaffTrainer               = new Application_Model_StaffTrainer();
$areaTrainer                 = $QStaffTrainer->getAreaTrainer($staff_id);
$regional_market             = $areaTrainer['district'];
$area_right                  = $areaTrainer['area'];
$province_right              = $areaTrainer['province'];
$this->view->areaTrainer     = $areaTrainer['area'];
$this->view->provinceTrainer = $areaTrainer['province'];

$nameDealer                  = $QStaffTrainingReport->getDealerArea($regional_market);
$this->view->name_dealer     = $nameDealer;

// get all staff id ma do quan ly
$arrayRightStaffID           = $QStaffTrainingReport->getStaffIDFromAreaAsm(array_keys($area_right),$userStorage);

if($dealer){
    //get store
    $whereStore     = array();
    $whereStore[]   = $QStore->getAdapter()->quoteInto('d_id = ?',$dealer);
    $whereStore[]   = $QStore->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
    $rows           = $QStore->fetchAll($whereStore);

    $arrayStore = array();
    if($rows->count())
    {
        foreach($rows as $key => $value)
        {
            $arrayStore[$value['id']] = $value['name'];
        }
    }

    $this->view->list_store = $arrayStore;
}

$params             = array_filter(array(
    'staff_id'      =>$arrayRightStaffID,
    'date'          =>$date,
    'trainer_dealer'        =>$trainer_dealer,
    'store'         =>$store,
    'type'          =>$type,
    'area'          =>$area,
    'area_right'    =>$area_right, 
    'province'      =>$province,
    'from_date'     =>$from_date,
    'to_date'       =>$to_date,
    'sort'          =>$sort,
    'desc'          =>$desc
));


$limit                      = LIMITATION;
$total                      = 0;
if($export){
    $result  = $QStaffTrainingReport->fetchPagination(NULL, NULL, $total, $params); 
    $this->_exportListTrainingReport($result,$params);
    exit;
}
$rows                       = $QStaffTrainingReport->fetchPagination($page, $limit, $total, $params);

$this->view->desc           = $desc;
$this->view->sort           = $sort;
$this->view->params         = $params;
$this->view->list           = $rows;
$this->view->limit          = $limit;
$this->view->total          = $total;
$this->view->url            = HOST.'trainer/list-training-report'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset         = $limit*($page-1);
