<?php 
    $staff_id = $this->getRequest()->getParam('staff_id');
    $submit = $this->getRequest()->getParam('submit');

    $title = $this->getRequest()->getParam('title');
    $scores = $this->getRequest()->getParam('scores');

    $QStaff = new Application_Model_Staff();
    $QSpAssessmentScores = new Application_Model_SpAssessmentScores();

    $flashMessenger = $this->_helper->flashMessenger;
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $params = array(
        'staff_id' => $staff_id
    );

    $info_staff = $QStaff->getInfoStar($staff_id);

    $other_scores = $QSpAssessmentScores->getOtherScores($staff_id);

    $this->view->info_staff = $info_staff;
    $this->view->other_scores = $other_scores;

    if ($submit) {

        try {
            $db = Zend_Registry::get('db');
            
            $data = [
                'title' => $title,
                'staff_id' => $staff_id,
                'scores' => $scores,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id
            ];

            $QSpAssessmentScores->insert($data);
            
            
            $flashMessenger->setNamespace('success')->addMessage('Điều chỉnh điểm thành công !');
            
            $back_link = '/trainer/other-scores?staff_id='.$staff_id;
            
            $this->redirect($back_link);

        } catch (exception $e) {
            $b->rollBack();

            $flashMessenger->setNamespace('error')->addMessage("Điều chỉnh điểm không thành công::".$e->getMessage());
            
            $back_link = '/trainer/other-scores?staff_id='.$staff_id;
            
            $this->redirect($back_link);
        }

    }

    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();

    $this->view->messages_success = $messages_success;
    $this->view->messages = $messages_error;


    

    

    
    
    