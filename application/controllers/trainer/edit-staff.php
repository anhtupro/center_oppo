<?php

$id        			   = $this->getRequest()->getParam('id');

$QTrainerCourseDetail  = new Application_Model_TrainerCourseDetail();
$QTrainerCourse  = new Application_Model_TrainerCourse();
$where = $QTrainerCourse->getAdapter()->quoteInto('id = ?', $id);
$this->view->course = $QTrainerCourse->fetchRow($where);

$params = array(
	'course_id' => $id
);


$staff = array();


$staff_new = $QTrainerCourseDetail->getStaffNew($params);
$staff_old = $QTrainerCourseDetail->getStaffOld($params);

$this->view->staff_new = $staff_new;
$this->view->staff_old = $staff_old;

// get area and province
$QArea = new Application_Model_Area();
$this->view->area = $QArea->get_cache();

// cached team
$QTeam = new Application_Model_Team();
$this->view->cached_team = $QTeam->get_cache();

// get all title of team sale and training
$teamID         = unserialize(TEAM_FOR_ASSIGN_STAFF_TRAINER_COURSE);
$QTeam          = new Application_Model_Team();
$whereJobTile   = array();
$whereJobTile[] = $QTeam->getAdapter()->quoteInto('parent_id IN (?)', $teamID);
$whereJobTile[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
$jobTitle       = $QTeam->fetchAll($whereJobTile);

if($jobTitle)
{
    $this->view->jobtitle = $jobTitle;
}

// get all province
$QRegionalMarket = new Application_Model_RegionalMarket();
$province = $QRegionalMarket->get_cache();
$this->view->province = $province;

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;






















