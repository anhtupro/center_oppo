<?php
$id                   = $this->getRequest()->getParam('id');
$store_staff_log_id   = $this->getRequest()->getParam('store_staff_log_id');
$note                 = $this->getRequest()->getParam('note');
$approve_type                 = $this->getRequest()->getParam('approve_type');


$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger     = $this->_helper->flashMessenger;

$params = array(
    'id'                => $id,
    'store_staff_log_id' => $store_staff_log_id
);

$QStaff                 = new Application_Model_Staff();
$QStoreStaffLogApprove  = new Application_Model_StoreStaffLogApprove();

$info = $QStoreStaffLogApprove->getStoreStaffInfo($id);


$store_image = $QStoreStaffLogApprove->getStoreImage($info['store_id']);

if($this->getRequest()->getMethod()=='POST')
{
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        $where = $QStoreStaffLogApprove->getAdapter()->quoteInto('store_staff_log_id = ?',$store_staff_log_id);
        $approve = $QStoreStaffLogApprove->fetchRow($where);

        if($approve){
            $flashMessenger->setNamespace('error')->addMessage('Gán shop đã được xác nhận hoặc từ chối.');
            $this->_redirect('/trainer/store-staff-details?id='.$id);
        }
        
        $data = [
            'store_staff_log_id' => $id,
            'approve_by'         => $userStorage->id,
            'approve_at'         => date('Y-m-d H:i:s'),
            'note'               => $note,
            'type'               => $approve_type
        ];
        
        $QStoreStaffLogApprove->insert($data);
        
        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Hoàn thành!');
    } catch (Exception $e) {
        $db->rollback();
        $e->getMessage();
    }
    $this->_redirect('/trainer/store-staff-details?id='.$id);
}


$this->view->info = $info;
$this->view->store_image = $store_image;

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;



