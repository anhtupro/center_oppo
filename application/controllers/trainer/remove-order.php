<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$sn               = $this->getRequest()->getParam('sn');

$back_url           = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER']:'/trainer/view-order?sn='.$sn;

$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');

try
{

    $db->beginTransaction();

    $id               = $this->getRequest()->getParam('id');
    $QTrainerOrder    = new Application_Model_TrainerOrder();
    $whereID          = array();
    $whereID[]        = $QTrainerOrder->getAdapter()->quoteInto('id = ?',$id);
    $whereID[]        = $QTrainerOrder->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
    $row_order        = $QTrainerOrder->fetchRow($whereID);

    if($row_order)
    {
        $dataDel = array(
            'del'=>1
        );

        $QTrainerOrder->update($dataDel,$whereID);

        //Update lại inventory FROM
        if(isset($row_order['order_from']) and $row_order['order_from'] != 0){
            $QTrainerInventory    = new Application_Model_TrainerInventory();
            $where_inventory      = array();
            $where_inventory[]    = $QTrainerInventory->getAdapter()->quoteInto('trainer_id = ?', $row_order['order_from']);
            $where_inventory[]    = $QTrainerInventory->getAdapter()->quoteInto('object = ?', $row_order['object']);
            $data_inventory = array(
                'date_out' => date('Y-m-d H:i:s'),
                'status'   => 3
            );
            $QTrainerInventory->update($data_inventory, $where_inventory);
        }
        //End inventory

        //Update lại inventory TO
        if(isset($row_order['order_to']) and $row_order['order_to'] != 0 and isset($row_order['order_to_confirm_at']) and !is_null($row_order['order_to_confirm_at'])){
            $QTrainerInventory    = new Application_Model_TrainerInventory();
            $where_inventory      = array();
            $where_inventory[]    = $QTrainerInventory->getAdapter()->quoteInto('trainer_id = ?', $row_order['order_to']);
            $where_inventory[]    = $QTrainerInventory->getAdapter()->quoteInto('order_id = ?', $row_order['id']);
            $where_inventory[]    = $QTrainerInventory->getAdapter()->quoteInto('object = ?', $row_order['object']);
            $data_inventory = array(
                'date_out' => date('Y-m-d H:i:s'),
                'status'   => 3
            );
            $QTrainerInventory->update($data_inventory, $where_inventory);
        }
        //End inventory
    }

    $db->commit();
    $flashMessenger ->setNamespace('success')->addMessage('Done');
    $this->_redirect($back_url);
}
catch (Exception $e)
{
    $db->rollback();
    $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect( $back_url);
}
