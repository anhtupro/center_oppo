<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');
$QTrainerOrderOut = new Application_Model_TrainerOrderOut();
if($this->getRequest()->getMethod() == 'POST')
{
    $order_id = $this->getRequest()->getParam('order_id');
    $list = $this->getRequest()->getParam('list');
    $date = $this->getRequest()->getParam('date');
    $params = array(
        'order_id'  => intval($order_id),
        'list'      => $list,
        'date'      => $date,
        'ip'        => $ip,
        'userStorage'=>$userStorage
    );

    $result = $QTrainerOrderOut->save($params);
    $this->_helper->json->sendJson($result);
}
