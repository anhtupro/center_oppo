<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger     = $this->_helper->flashMessenger;

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');
$back_url           = HOST.'trainer/course';
$QTrainerCourse     = new Application_Model_TrainerCourse();

if($this->getRequest()->getMethod()=='POST')
{
    try
    {
        $db->beginTransaction();
        $type               = $this->getRequest()->getParam('type');
        $area_id            = $this->getRequest()->getParam('area_id');
        $from_date          = $this->getRequest()->getParam('from_date');
        $to_date            = $this->getRequest()->getParam('to_date');
        $note               = $this->getRequest()->getParam('note');
        $id                 = $this->getRequest()->getParam('id');

        $data = array(
            'type'      => $type,
            'area_id'   => $area_id,
            'from_date' => $from_date,
            'to_date'   => $to_date,
            'note'      => $note
        );

        
        if(isset($id) and $id)
        {
            // update
            $whereTrainerCourse = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$id);
            $row                = $QTrainerCourse->fetchRow($whereTrainerCourse);
            if($row)
            {
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['updated_by'] = $userStorage->id;
                $QTrainerCourse->update($data,$whereTrainerCourse);
            }

            // to do log
            $info = array('UPDATE COURSE','new'=>$data,'old'=>$row);
            $QLog->insert( array (
                'info'          => json_encode($info),
                'user_id'       => $userStorage->id,
                'ip_address'    => $ip,
                'time'          => date('Y-m-d H:i:s'),
            ) );
        }
        else
        {
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $userStorage->id;
            $QTrainerCourse->insert($data);

            // to do log
            $info = array('INSERT COURSE','new'=>$data,'old'=>$row);
            $QLog->insert( array (
                'info'          => json_encode($info),
                'user_id'       => $userStorage->id,
                'ip_address'    => $ip,
                'time'          => date('Y-m-d H:i:s'),
            ) );
        }

        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Done');

    }
    catch (Exception $e)
    {
        $db->rollback();
        $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    }
}

$this->redirect($back_url);
