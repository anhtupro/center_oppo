<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$flashMessenger = $this->_helper->flashMessenger;
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$QLog           = new Application_Model_Log();
$ip             = $this->getRequest()->getServer('REMOTE_ADDR');

$back_url = HOST.'trainer/list-order';

try
{
    $QTrainerOrder = new Application_Model_TrainerOrder();

    $date       = $this->getRequest()->getParam('date');
    $confirm    = $this->getRequest()->getParam('confirm');
    $id         = $this->getRequest()->getParam('id', array());
    $asset_id   = $this->getRequest()->getParam('asset_id', array());
    $order_from = $this->getRequest()->getParam('order_from', array());
    $order_to   = $this->getRequest()->getParam('order_to', array());
    $imei_code  = $this->getRequest()->getParam('imei_code', array());
    $note       = $this->getRequest()->getParam('note', array());

    $sn = date ('YmdHis') . substr ( microtime (), 2, 4 );

    foreach($asset_id as $key => $value){
        $data = array(
            'sn'          => $sn,
            'date'        => $date,
            'asset_id'    => $value,
            'order_from'  => !empty($order_from[$key]) ? $order_from[$key] : null,
            'order_to'    => !empty($order_to[$key]) ? $order_to[$key] : null,
            'object'      => $imei_code[$key],
            'note'        => $note[$key]
        );

        if(isset($confirm) and $confirm){
            $data['confirm_by'] = $userStorage->id;
            $data['confirm_at'] = date('Y-m-d H:i:s');
        }

        if(isset($id[$key]) and $id[$key]){
            $where = $QTrainerOrder->getAdapter()->quoteInto('id = ?', $id[$key]);
            $QTrainerOrder->update($data, $where);
        }
        else{
            $data['created_by'] = $userStorage->id;
            $data['created_at'] = date('Y-m-d H:i:s');
            $result = $QTrainerOrder->insert($data);
        }
    }

    $flashMessenger->setNamespace('success')->addMessage('Done!');
    $this->redirect($back_url);
    
}
catch (Exception $e)
{
    $result = array('code'=>-1,'message'=>$e->getMessage());
    $this->_helper->json->sendJson($result);
}
exit;