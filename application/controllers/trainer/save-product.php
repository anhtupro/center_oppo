<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger     = $this->_helper->flashMessenger;

$back_url           = HOST.'trainer/list-product';

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db = Zend_Registry::get('db');

$QTrainerProduct  = new Application_Model_TrainerProduct();

if($this->getRequest()->getMethod()=='POST')
{
    try
    {
        $db->beginTransaction();
        $id = $this->getRequest()->getParam('id');
        $name = $this->getRequest()->getParam('name');
        $code  = $this->getRequest()->getParam('code');
        $description = $this->getRequest()->getParam('description');

        $data  = array(
            'name'      => $name,
            'code'      => $code,
            'description' => $description,
            'created_at'  => date('Y-m-d H:i:s'),
            'created_by'  => $userStorage->id
        );


        if(is_numeric($id)){
            //UPDATE
            $where = array();
            $where[] = $QTrainerProduct->getAdapter()->quoteInto('code = ?', $code);
            $where[] = $QTrainerProduct->getAdapter()->quoteInto('id <> ?', $id);
            $rowProduct   = $QTrainerProduct->fetchRow($where);

            if($rowProduct){
                $flashMessenger->setNamespace('error')->addMessage('This CODE Product has existed');
                $this->redirect(HOST.'trainer/create-product?id='.$id);
            }

            $data['updated_by'] = $userStorage->id;
            $data['updated_at'] = date('Y-m-d H:i:s');

            $where = $QTrainerProduct->getAdapter()->quoteInto('id = ?', $id);
            $QTrainerProduct->update($data, $where);
            
            $db->commit();
            $flashMessenger ->setNamespace('success')->addMessage('Done');
            $this->redirect(HOST.'trainer/create-product?id='.$id);
        }
        else{
            //INSERT
            $where = $QTrainerProduct->getAdapter()->quoteInto('code = ?', $code);
            $rowProduct   = $QTrainerProduct->fetchRow($where);

            if($rowProduct){
                $flashMessenger->setNamespace('error')->addMessage('This CODE Product has existed ');
                $this->redirect($back_url);
            }

            $QTrainerProduct->insert($data);

            $db->commit();
            $flashMessenger ->setNamespace('success')->addMessage('Done');
            $this->redirect($back_url);
        }

    }
    catch(Exception $e)
    {
        $db->rollback();
        $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    }
}

$this->redirect($back_url);
