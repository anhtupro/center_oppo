<?php 
$flashMessenger         = $this->_helper->flashMessenger;
$QTrainerCourse = new Application_Model_TrainerCourse();
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$name = $this->getRequest()->getParam('name');

$page = $this->getRequest()->getParam('page');
$desc = $this->getRequest()->getParam('desc',1);
$sort = $this->getRequest()->getParam('sort','id');
$params = array(
    'name'      => trim($name),
    'from_date' => trim($from_date),
    'to_date'   => trim($to_date),
    'desc'      => $desc,
    'sort'      => $sort
);
$total = 0;
$limit = LIMITATION;
$list = $QTrainerCourse->fetchPaginationCourseFinished($page,$limit,$total,$params);
$this->view->list           = $list;
$this->view->desc           = $desc;
$this->view->sort           = $sort;
$this->view->params         = $params;
$this->view->limit          = $limit;
$this->view->total          = $total;
$this->view->url            = HOST.'trainer/course-finished'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset         = $limit*($page-1);

$messages = $flashMessenger->setNamespace('success')->getMessages();
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
$this->view->messages_error = $messages_error;