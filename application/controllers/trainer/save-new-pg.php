<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');
$back_url           = HOST.'trainer/list-new-staff';
$QStaffTraining     = new Application_Model_StaffTraining();

if($this->getRequest()->getMethod()=='POST')
{
    try
    {
        $db->beginTransaction();
        $id                     = $this->getRequest()->getParam('id_new_staff',null);
        $firstname              = $this->getRequest()->getParam('firstname');
        $lastname               = $this->getRequest()->getParam('lastname');
        $cmnd                   = $this->getRequest()->getParam('cmnd');
        $gender                 = $this->getRequest()->getParam('gender');
        $regional_market        = $this->getRequest()->getParam('regional_market');

        $data  = array(
            'firstname'       => $firstname,
            'lastname'        => $lastname,
            'gender'          => $gender,
            'cmnd'            => $cmnd,
            'team'            => SALES_TEAM,
            'title'           => PGPB_TITLE,
            'regional_market' => $regional_market
        );
        if(isset($id) and $id)
        {
            $whereNewStaff   =  array();
            $whereNewStaff[] = $QStaffTraining->getAdapter()->quoteInto('id = ?',$id);
            $whereNewStaff[] = $QStaffTraining->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
            $rowNewStaff     = $QStaffTraining->fetchRow($whereNewStaff);
            if($rowNewStaff)
            {
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['updated_by'] = $userStorage->id;
                $QStaffTraining->update($data,$whereNewStaff);
                // to do log
                $info = array('UPDATE-NEW-PG-FOR-TRAINING','old'=>$rowNewStaff,'new'=>$data);
                $QLog->insert( array (
                    'info'          => json_encode($info),
                    'user_id'       => $userStorage->id,
                    'ip_address'    => $ip,
                    'time'          => date('Y-m-d H:i:s'),
                ) );
            }
            else
            {
                throw new Exception('Staff Is Not Found !');
            }
        }
        else
        {
            $whereStaffTraining = $QStaffTraining->getAdapter()->quoteInto('cmnd = ?',$cmnd);
            $rowStaffTraining   = $QStaffTraining->fetchRow($whereStaffTraining);

            if(!$rowStaffTraining)
            {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_by'] = $userStorage->id;
                $idInsert = $QStaffTraining->insert($data);

                // to do log
                $info = array('INSERT-NEW-PG-FOR-TRAINING','new'=>$data);
                $QLog->insert( array (
                    'info'          => json_encode($info),
                    'user_id'       => $userStorage->id,
                    'ip_address'    => $ip,
                    'time'          => date('Y-m-d H:i:s'),
                ) );
            }
            else
            {
                throw new Exception('User is created !');
            }
        }

        $db->commit();

        $flashMessenger ->setNamespace('success')->addMessage('Done');
        $this->redirect($back_url);

    }
    catch(Exception $e)
    {
        $db->rollback();
        $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect($back_url);
    }
}
