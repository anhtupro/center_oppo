<?php
$back_url                   = '/trainer/orders';
$this->view->back_url       = $back_url;
$flashMessenger             = $this->_helper->flashMessenger;
$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$whereTypeAsset             = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
$type_asset                 = $QTrainerTypeAsset->fetchAll($whereTypeAsset);
$arrayTypeAsset             = array();

$type = unserialize(CATEGORY_ASSET_TRAINER);
$this->view->type = $type;
$db = Zend_Registry::get('db');
if($type_asset->count())
{
    foreach($type_asset as $key => $value)
    {
        $arrayTypeAsset[$value['id']] = $value['name'];
    }
}

$this->view->type_asset     = $arrayTypeAsset;

try
{
    $id                  = $this->getRequest()->getParam('id');
    $QTrainerOrder       = new Application_Model_TrainerOrder();
    $QTrainerOrderDetail = new Application_Model_TrainerOrderDetail();
    $QTrainerOrderImei   = new Application_Model_TrainerOrderImei();

    $QTrainerOrderOut  = new Application_Model_TrainerOrderOut();
    $this->view->list_trainer = $QTrainerOrderOut->getStaffTrainer();

    if($id){
        $order = $QTrainerOrder->find($id)->current();
        $selectDetail = $db->select()
            ->from(array("p"=>'trainer_order_detail'),array('p.*'))
            ->where('p.del IS NULL OR p.del = 0')
            ->where('order_id = ?',$id);
        $orderDetail = $db->fetchAll($selectDetail);

        $listImei = array();
        foreach($orderDetail as $value):
            if($value['type'] == 1){
                $selectImei = $db->select()
                    ->from(array('p'=>'trainer_order_imei'),array('p.*'))
                    ->where('p.del IS NULL OR p.del = 0')
                    ->where('p.type = ?',1)
                    ->where('p.order_detail_id = ?',$value['id']);
                $rows = $db->fetchAll($selectImei);
                $listImei[$value['id']] = $rows;
            }
        endforeach;
        $this->view->order = $order;
        $this->view->orderDetail = $orderDetail;
        $this->view->listImei = $listImei;
        $selectCreated = $db->select()
            ->from(array('staff'),array('CONCAT(firstname," ",lastname)'))
            ->where('id = ?',$order->created_by);
        $this->view->created_by = $db->fetchOne($selectCreated);

    }
}
catch (Exception $e)
{
    $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    echo '<script>setTimeout(function(){parent.location.href="'.HOST.'trainer/orders"})</script>';
}

