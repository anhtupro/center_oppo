<?php
$name = $this->getRequest()->getParam('name');

$QTrainerInventory = new Application_Model_TrainerInventory();

$params = array(
	'name'  => $name
);
$data = $QTrainerInventory->getInventoryByAsset($params);

$this->view->params = $params;
$this->view->data = $data;