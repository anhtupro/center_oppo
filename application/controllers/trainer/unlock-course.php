<?php 
$id             = $this->getRequest()->getParam('id');
$QTrainerCourse = new Application_Model_TrainerCourse();
$flashMessenger = $this->_helper->flashMessenger;
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$db             = Zend_Registry::get('db');

if(!$id){
    $flashMessenger->setNamespace('error')->addMessage('Please select course to unlock');
    $this->_redirect('/trainer/course');
}

if(!in_array($userStorage->group_id,array(HR_ID,ADMINISTRATOR_ID))){
    $flashMessenger->setNamespace('error')->addMessage('Permission denied');
    $this->_redirect('/trainer/course');
}

if(in_array($userStorage->group_id,array(HR_ID,ADMINISTRATOR_ID))){
    $data = array(
        'locked' => NULL,
        'locked_at' => NULL,
        'locked_by' => NULL
    );
    $where = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$id);
    $db->beginTransaction();
    try{
        $QTrainerCourse->update($data,$where);
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Done');
        $this->_redirect('/trainer/course');
    }catch (Exception $e){
        $db->rollBack();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->_redirect('/trainer/course');
    }
}
$this->_redirect('/trainer/course');
exit;