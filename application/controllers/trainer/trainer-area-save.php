<?php

$id = $this->getRequest()->getParam('id');
$back_url = HOST.'trainer/trainer-area';
$flashMessenger = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if (!$id) {
    $this->view->messages = $flashMessenger->setNamespace('error')->addMessage('Invalid ID');
    $this->_redirect($back_url);
} else {
    $QStaff = new Application_Model_Staff();
    $QTeam  = new Application_Model_Team();
    $staff = $QStaff->find($id);
    $staff = $staff->current();
    $title = $staff['title'];
    $team = $QTeam->find($title);
    $team = $team->current();
    if (!$staff
//             || !in_array($staff['group_id'], My_Staff_Group::$allow_in_area_view)
             //|| !in_array($team['access_group'], My_Staff_Group::$allow_in_area_view)
            || !is_null($staff['off_date'])
            || $staff['status'] != My_Staff_Status::On
            || !in_array($userStorage->id, unserialize(TRAINER_KEY_PROJECT)))  {
        PC::debug($staff->toArray());
        $this->view->messages = $flashMessenger->setNamespace('error')->addMessage('This staff cannot be assigned to Area views');
        $this->_redirect($back_url);
    }

    //chi duoc gan cho trainer
    if(!in_array($team['access_group'],array(TRAINING_TEAM_GROUP_ID, TRAINING_LEADER_ID))){
        //$flashMessenger->setNamespace('error')->addMessage('Only training team');
        //$this->_redirect($back_url);
    }

    $QAsm = new Application_Model_Asm();

    $area = $this->getRequest()->getParam('area');
    $area = is_array($area) ? array_unique( array_filter( $area ) ) : array();

    $region = $this->getRequest()->getParam('region');
    $region = is_array($region) ? array_unique( array_filter( $region ) ) : array();
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try{
        $where = $QAsm->getAdapter()->quoteInto('staff_id = ?', $id);
        $QAsm->delete($where);

        if (isset($area) && $area) {
            foreach ($area as $value) {
                $data = array(
                    'staff_id' => $id,
                    'area_id'  => $value,
                    'type'     => My_Region::Area,
                    );

                $QAsm->insert($data);
            }
        }

        if (isset($region) && $region) {
            foreach ($region as $value) {
                $data = array(
                    'staff_id' => $id,
                    'area_id'  => $value,
                    'type'     => My_Region::Province,
                    );

                $QAsm->insert($data);
            }
        }

        // log
        $QLog = new Application_Model_Log();
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = 'ASM - Update('.$id.') - Area('.implode(',', $area).') - Province (' . implode(',', $region) . ')' ;

        $QLog->insert( array (
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ) );

        $cache = Zend_Registry::get('cache');
        $cache->remove('asm_cache');
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Success');

    }catch (Exception $e){
        $db->rollBack();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
    $this->_redirect($back_url);

}