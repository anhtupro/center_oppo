<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger     = $this->_helper->flashMessenger;
$userStorage           = Zend_Auth::getInstance()->getStorage()->read();
$QLog                  = new Application_Model_Log();
$ip                    = $this->getRequest()->getServer('REMOTE_ADDR');
$db                    = Zend_Registry::get('db');
$back_url              = HOST.'trainer/add-time';
$QTrainerCourse        = new Application_Model_TrainerCourse();
$QTrainerCourseDetail  = new Application_Model_TrainerCourseDetail();
$QNewStaff             = new Application_Model_StaffTraining();
$cached_new_staff      = $QNewStaff->get_cache();
$QStaff                = new Application_Model_Staff();
$cached_staff          = $QStaff->get_cache();
$QTrainerCourseTiming  = new Application_Model_TrainerCourseTiming();

if($this->getRequest()->getMethod()=='POST')
{
    try
    {
        $db->beginTransaction();
        
        $staff_check_in        = $this->getRequest()->getParam('staff_check_in');
        $date                  = $this->getRequest()->getParam('date');
        $course_id             = $this->getRequest()->getParam('course_id');        

        if(!isset($date) || !count($staff_check_in))
            throw new  Exception("Date Or STaff Check In  NOT INPUT !");

        // check khoa hoc do coi co bi block chua 
        $whereTrainerCourse       = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$course_id);
        $checked                  = $QTrainerCourse->fetchRow($whereTrainerCourse);
        $Locked                   = $checked['locked'];
        if(isset($Locked) and $Locked == 1)
        {
            throw new Exception("Course Is Locked !");            
        }

        // lay ra nhung thang đa cham cong 
        $whereTrainerCourseTiming   = array();
        $whereTrainerCourseTiming[] = $QTrainerCourseTiming->getAdapter()->quoteInto('date = ?',$date);
        $whereTrainerCourseTiming[] = $QTrainerCourseTiming->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
        $rowsTiming                 = $QTrainerCourseTiming->fetchAll($whereTrainerCourseTiming);
        $arrayStaffHaveCheckin      = array();
        if($rowsTiming->count())
        {
            foreach ($rowsTiming as $key => $value) {
                $arrayStaffHaveCheckin[] = $value['course_detail_id'];
            }
            // gia ta da co nhung cai course_detail_id --> gio mình phải lấy được nhung course_detail_id cua khóa học đó 
            foreach($arrayStaffHaveCheckin as $key => $item )
            {
              $whereStaffHaveCheckInCourse   = array();
              $whereStaffHaveCheckInCourse[] = $QTrainerCourseDetail->getAdapter()->quoteInto('id = ?',$item);
              $whereStaffHaveCheckInCourse[] = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$course_id);
              $rowStaffHaveCheckInCousre     = $QTrainerCourseDetail->fetchRow($whereStaffHaveCheckInCourse);
              if(!$rowStaffHaveCheckInCousre)
              {
                unset($arrayStaffHaveCheckin[$key]);
              }
            }
            // minh se coi nhung thang nao khac se update lai del
            $arrayDiffCheckIn = array_diff($arrayStaffHaveCheckin, $staff_check_in); 
            // update lai thanh del => 1
            $dataUpdateDel = array('del' => 1);
            foreach ($arrayDiffCheckIn as $key => $value) {
               $whereTrainerCourseTimingUpdateDel   = array();
               $whereTrainerCourseTimingUpdateDel[] = $QTrainerCourseTiming->getAdapter()->quoteInto('date = ?',$date);
               $whereTrainerCourseTimingUpdateDel[] = $QTrainerCourseTiming->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
               $whereTrainerCourseTimingUpdateDel[] = $QTrainerCourseTiming->getAdapter()->quoteInto('course_detail_id = ?',$value);
               $QTrainerCourseTiming->update($dataUpdateDel,$whereTrainerCourseTimingUpdateDel);
            }
           
        }

        // minh insert nhung thang moi vao 
        $arrayDiffNewCheckIn = array_diff($staff_check_in, $arrayStaffHaveCheckin);


        foreach ($arrayDiffNewCheckIn as $key => $value) {

            $dataInsert = array();
            $dataInsert['date']                = $date;
            $dataInsert['course_detail_id']    = $value;
            $dataInsert['created_at']          = date('Y-m-d H:i:s');
            $dataInsert['created_by']          = $userStorage->id;
            
            $QTrainerCourseTiming->insert($dataInsert);
        }

    

        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Done');
        $this->redirect('/trainer/course');
    }
    catch (Exception $e)
    {
        $db->rollback();
        $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/trainer/course');
    }
}

$this->redirect($back_url);
