<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger     = $this->_helper->flashMessenger;

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');
$back_url           = HOST.'trainer/type-asset';
$QTrainerTypeAsset  = new Application_Model_TrainerTypeAsset();
$QColor = new Application_Model_GoodColorCombined();

$QGood = new Application_Model_Good();
if($this->getRequest()->getMethod()=='POST')
{
    try
    {
        $db->beginTransaction();
        $asset = $this->getRequest()->getParam('asset');
        $type  = $this->getRequest()->getParam('type');
        $good_id = $this->getRequest()->getParam('good_id',NULL);
        $color_id = $this->getRequest()->getParam('color_id',NULL);
        $id    = $this->getRequest()->getParam('id');
        $asset = trim($asset);
        if(!$asset){
            $flashMessenger->setNamespace('error')->addMessage('asset name is require');
            $this->_redirect('/trainer/type-asset');
        }

        if(!$type){
            if(!$id){
                $flashMessenger->setNamespace('error')->addMessage('type  is require');
                $this->_redirect('/trainer/type-asset');
            }
        }

        if($type == CATEGORY_ASSET_TRAINER_PHONE){
            if(!$good_id){
                $flashMessenger->setNamespace('error')->addMessage('product name is require');
                $this->_redirect('/trainer/type-asset');
            }

            if(!$color_id){
                $flashMessenger->setNamespace('error')->addMessage('color name is require');
                $this->_redirect('/trainer/type-asset');
            }

           $asset = $QColor->getGoodColorName($good_id,$color_id);
           if(!$asset){
               $flashMessenger->setNamespace('error')->addMessage('Please select product and color');
               $this->_redirect('/trainer/type-asset');
           }
        }else{
            if(!$id) {
                $good_id = null;
                $color_id = null;
            }
        }

        $data  = array(
            'name'      => $asset,
            'type'      => $type,
            'good_id'   => $good_id,
            'color_id'  => $color_id
        );

        //Kiểm dã tạo chưa
        $whereAsset = $QTrainerTypeAsset->getAdapter()->quoteInto('id = ?',$id);
        $rowAsset   = $QTrainerTypeAsset->fetchRow($whereAsset);

        if(!$rowAsset)
        {
            if($type == CATEGORY_ASSET_TRAINER_PHONE){
                $whereAsset2 = array();
                $whereAsset2[] = $QTrainerTypeAsset->getAdapter()->quoteInto('good_id = ?',$good_id);
                $whereAsset2[] = $QTrainerTypeAsset->getAdapter()->quoteInto('color_id = ?',$color_id);
                $rowAsset2 = $QTrainerTypeAsset->fetchRow($whereAsset2);
                if($rowAsset2){
                    $flashMessenger->setNamespace('error')->addMessage('This Product has existed ');
                    $this->_redirect('/trainer/type-asset');
                }
            }
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $userStorage->id;
            $QTrainerTypeAsset->insert($data);

            // to do log
            $info = array('INSERT-TYPE-ASSET','new'=>$data);
            $QLog->insert( array (
                'info'          => json_encode($info),
                'user_id'       => $userStorage->id,
                'ip_address'    => $ip,
                'time'          => date('Y-m-d H:i:s'),
            ) );
        }
        else
        {
            if($good_id){
                $whereAsset2 = array();
                $whereAsset2[] = $QTrainerTypeAsset->getAdapter()->quoteInto('good_id = ?',$good_id);
                $whereAsset2[] = $QTrainerTypeAsset->getAdapter()->quoteInto('color_id = ?',$color_id);
                $whereAsset2[] = $QTrainerTypeAsset->getAdapter()->quoteInto('id != ?',$id);
                $rowAsset2 = $QTrainerTypeAsset->fetchRow($whereAsset2);
                if($rowAsset2){
                    $flashMessenger->setNamespace('error')->addMessage('This Product has existed ');
                    $this->_redirect('/trainer/type-asset');
                }
            }

            unset($data['type']);
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = $userStorage->id;
            $QTrainerTypeAsset->update($data,$whereAsset);

            // to do log
            $info = array('UPDATE-TYPE-ASSET','new'=>$data,'old'=>$rowAsset);
            $QLog->insert( array (
                'info'          => json_encode($info),
                'user_id'       => $userStorage->id,
                'ip_address'    => $ip,
                'time'          => date('Y-m-d H:i:s'),
            ) );
        }

        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Done');
    }
    catch(Exception $e)
    {
        $db->rollback();
        $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    }
}

$this->redirect($back_url);
