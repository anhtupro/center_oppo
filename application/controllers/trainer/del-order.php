<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$back_url                   = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER']:'/trainer/list-order';

$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');

try
{

    $db->beginTransaction();

    $sn               = $this->getRequest()->getParam('sn');
    $QTrainerOrder    = new Application_Model_TrainerOrder();
    $whereID          = array();
    $whereID[]        = $QTrainerOrder->getAdapter()->quoteInto('sn = ?',$sn);
    $whereID[]        = $QTrainerOrder->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
    $rowID            = $QTrainerOrder->fetchRow($whereID);

    if($rowID)
    {
        $dataDel = array(
            'del'=>1
        );

        if($rowID['confirm_by'])
            throw new Exception('Order Is Confirmed - Not Delete !');

        $QTrainerOrder->update($dataDel,$whereID);

        //Update lại inventory
        $QTrainerInventory    = new Application_Model_TrainerInventory();
        $where_inventory      = array();
        $where_inventory[]    = $QTrainerInventory->getAdapter()->quoteInto('trainer_id = ?', $rowID['order_from']);
        $where_inventory[]    = $QTrainerInventory->getAdapter()->quoteInto('object = ?', $rowID['object']);
        $data_inventory = array(
            'date_out' => null,
            'status'   => 1
        );
        $QTrainerInventory->update($data_inventory, $where_inventory);
        //End inventory
    }

    $db->commit();
    $flashMessenger ->setNamespace('success')->addMessage('Done');
    $this->_redirect($back_url);
}
catch (Exception $e)
{
    $db->rollback();
    $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect( $back_url);
}
