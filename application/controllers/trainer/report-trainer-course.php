<?php
// lay ra danh sách các course
$QTrainerCourse          = new Application_Model_TrainerCourse();
$QTrainerCourseType      = new Application_Model_TrainerCourseType();
$this->view->trainer_course_type = $QTrainerCourseType->get_cache();
$QLog                    = new Application_Model_Log();
$ip                      = $this->getRequest()->getServer('REMOTE_ADDR');
$userStorage             = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger          = $this->_helper->flashMessenger;
$listCourse              = $QTrainerCourse->getCourse();

$this->view->list_course = $listCourse;


if($this->getRequest()->getMethod() == 'POST')
{
	try
	{

		$from_date             = $this->getRequest()->getParam('from_date'); 
		$to_date               = $this->getRequest()->getParam('to_date');  
		$course_id             = $this->getRequest()->getParam('course_id'); 
		$lock                  = $this->getRequest()->getParam('lock');
		$export                = $this->getRequest()->getParam('export');
		$picture               = $this->getRequest()->getParam('picture');
		$submit_form           = $this->getRequest()->getParam('submit_form');
		$image_dir             = HOST .'photo/trainer/course/'. $course_id;
		$this->view->image_dir = $image_dir;
		$course_detail_id      = $this->getRequest()->getParam('course_detail_id');
		$result                = $this->getRequest()->getParam('result');
		$del_img               = $this->getRequest()->getParam('del_img');

	    $db = Zend_Registry::get('db');

	    $paramsSP = array(
	        $from_date ? $from_date: NULL,
	        $to_date ? $to_date: NULL,
	        $course_id ? $course_id: NULL
	    );

	    // chen ket qua neu co ket qua 
	    if(is_array($result) and count($result) and count($course_detail_id))
	    {
	    	$QTrainerCourseDetail = new Application_Model_TrainerCourseDetail();
	    	foreach ($course_detail_id as $key => $value) {
	    		$whereCourseDetailID = $QTrainerCourseDetail->getAdapter()->quoteInto('id = ?',$value);
	    		$rowCourseDetail     = $QTrainerCourseDetail->fetchRow($whereCourseDetailID); 
	    		if($rowCourseDetail)
	    		{
	    			$dataUpdate = array('result' => $result[$key]);
	    			$QTrainerCourseDetail->update($dataUpdate,$whereCourseDetailID);
	    		}
	    	}
	    }

	    //Thêm hình ảnh
	    $whereCourseLock = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$course_id);
	    $rowCourse       = $QTrainerCourse->fetchRow($whereCourseLock);
	    if($submit_form AND $rowCourse){
	    	$arrFiles = array();

	    	//remove image
			if(is_array($del_img) and count($del_img)){
				foreach($del_img as $_v){
					@unlink($uploaded_dir . DIRECTORY_SEPARATOR.$_v);
				}	
			}

			// xử lý upload file
			$upload = new Zend_File_Transfer();
			$upload->setOptions(array('ignoreNoFile'=>true));

    		if (function_exists('finfo_file')){
            	$upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));
    		}

        	$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
        	$upload->addValidator('ExcludeExtension', false, 'php,sh,exe');

    		$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .DIRECTORY_SEPARATOR . 'public' 
    		. DIRECTORY_SEPARATOR . 'photo' .DIRECTORY_SEPARATOR . 'trainer' . DIRECTORY_SEPARATOR .'course' 
    		. DIRECTORY_SEPARATOR . $course_id;

            if (!is_dir($uploaded_dir)){
            	@mkdir($uploaded_dir, 0777, true);	
            }

        	$upload->setDestination($uploaded_dir);    
			$files = $upload->getFileInfo();

			if(count($files) > 0):
				foreach ($files as $file => $fileInfo):

					if(!$upload->isUploaded($file)){
						continue;
					}	

					$new_name = '';
	            	if(!$upload->isValid($file)){
	            		$errors = $upload->getErrors();
	                	$sError = null;
		                if ($errors and isset($errors[0]))
		                {
		                    switch ($errors[0]) {
		                        case 'fileUploadErrorIniSize':
		                            $sError = 'File size is too large';
		                            break;
		                        case 'fileMimeTypeFalse':
		                            $sError = 'The file(s) you selected weren\'t the type we were expecting';
		                            break;
		                         default:
		                         	$sError = 'File upload error';
		                            break;
		                    }
		                    throw new Exception($sError);
		                }
	            	}else{
	            		$upload->receive($file);
	    
						$path_info = pathinfo($upload->getFileName($file));
						$filename  = $path_info['filename'];
						$extension = $path_info['extension'];
						$old_name  = $filename . '.'.$extension;
						$new_name  = md5($filename . uniqid('', true)) . '.'.$extension;

			            if (is_file($uploaded_dir . DIRECTORY_SEPARATOR . $old_name)){
			                rename($uploaded_dir . DIRECTORY_SEPARATOR . $old_name, $uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
			                $arrFiles[] = $new_name;
			            } 
			            
	            	}
				endforeach;
			endif;

            $old_images = array();
			$old_images = json_decode($rowCourse['picture']);
			
			if(json_last_error() != JSON_ERROR_NONE){
				$old_images = array();
			}

			if(!is_array($old_images) OR !count($old_images)){
				$old_images = array();
			}

    		//remove image
			if(is_array($del_img) and count($del_img)){
                if(is_array($old_images) and count($old_images)){
                    $old_images = array_diff($old_images,$del_img);
                }

			}

			// update if delete or insert
			if( ( is_array($del_img) and count($del_img) ) OR count($arrFiles) ){

				$dataUpdateLocked = array();
				$dataUpdateLocked['picture'] = json_encode(array_merge($old_images,$arrFiles));
	    		$QTrainerCourse->update($dataUpdateLocked,$whereCourseLock);

	    		// to do log 
		        $info = array('UPDATE LOCKED COURSE','new' => $dataUpdateLocked,'old'=>$rowCourse);
		        $QLog->insert(array(
		            'info'       => json_encode($info),
		            'user_id'    => $userStorage->id,
		            'ip_address' => $ip,
		            'time'       => date('Y-m-d H:i:s')
		        ));
			}
				
	    }//END UPLOAD FILE

	    // neu co lock khoa hoc do 
	    if(isset($lock) and $lock)
	    {
	    	// gio minh co khoa hoc do minh update cai truong cua khoa hoc do thanh da lock thoi 
	    	if($rowCourse and $rowCourse['locked'] != 1 )
	    	{
				// check neu khoa hoc chua het ngay ma lock thi van Exception
				$arrFiles = array();
				if($submit_form){
		    		$dataUpdateLocked = array(
						'locked'    => 1,
						'locked_at' => date('Y-m-d H:i:s'),
						'locked_by' => $userStorage->id		
	    			);
		    		
		    		// to do locked     		
		    		$QTrainerCourse->update($dataUpdateLocked,$whereCourseLock);

		    		// to do log 
			        $info = array('UPDATE LOCKED COURSE','new' => $dataUpdateLocked,'old'=>$rowCourse);

			        $QLog->insert(array(
			            'info'       => json_encode($info),
			            'user_id'    => $userStorage->id,
			            'ip_address' => $ip,
			            'time'       => date('Y-m-d H:i:s')
			        ));
		        }
	    	}// End check row
	    }
	   
		// get locked 
		$whereCourseGetLock        = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$course_id);
		$rowCourseGetLock          = $QTrainerCourse->fetchRow($whereCourseGetLock);
		$this->view->currentCourse = $rowCourseGetLock;
		if($rowCourseGetLock)
		{
			$is_locked = $rowCourseGetLock['locked'];
		}

		$params = array(
			'from_date' => $from_date ? $from_date : null,
			'to_date'   => $to_date   ? $to_date   : null,
			'lock'      => $lock ? $lock : $is_locked,
			'course_id' => $course_id
	    	);

		$this->view->params = $params;
		
		// call store
	    $stmt = $db->query("CALL p_training_course_report(?,?,?)", $paramsSP);
	    $result =  $stmt->fetchAll();
		
	    if(count($result))
	    {
	    	foreach ($result as $key => $value) {
	    		unset($result[$key]['cmnd']);
	    	}
	   		$this->view->list = $result;
	    }

	    if(isset($export) and $export == 1)
	    {
	    	// bat dau thuc hien data 
		    $name = "Report Trainer Cousrse".date('Ymd').".csv";
		    $fp = fopen('php://output', 'w');

			header('Content-Type: text/csv; charset=utf-8');
			echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
			header('Content-Disposition: attachment; filename='.$name);

		    $arrayPUT = array();
		    $k = 0;
		    foreach ($result as $key => $value) {
					
	    		$k = $k+1;
					
		    	if($k == 1)
		    	{
		    		fputcsv($fp,array_keys($result[$key]));
		    	}
					
					$resultT                = unserialize(RESULT_TRANIING_NEWSTAFF_COURSE);				
					$value                  = $result[$key]['RESULT']; 
					$valueResult            = $resultT[$value];				
				    $result[$key]['RESULT'] = $valueResult;
					
		    	$arrayPUT =  $result[$key];				
					
		    	fputcsv($fp, $arrayPUT);      
		    }

	        fclose($fp);
	        exit;
	    }
	}
	catch(Exception $e)
	{
		$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $back_url = '/trainer/report-trainer-course';
        $this->_redirect($back_url);

	}

}
