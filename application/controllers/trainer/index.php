<?php

$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$name            = $this->getRequest()->getParam('name');
$area_id         = $this->getRequest()->getParam('area_id',null);
$big_area_id     = $this->getRequest()->getParam('big_area_id',null);
$regional_market = $this->getRequest()->getParam('regional_market',null);
$code            = $this->getRequest()->getParam('code');
$email           = $this->getRequest()->getParam('email');
$date            = $this->getRequest()->getParam('date');
$month           = $this->getRequest()->getParam('month');
$year            = $this->getRequest()->getParam('year');
$title           = $this->getRequest()->getParam('title');
$sell_out_from   = $this->getRequest()->getParam('sell_out_from');
$sell_out_to     = $this->getRequest()->getParam('sell_out_to');

$from_date       = $this->getRequest()->getParam('from_date');
$to_date         = $this->getRequest()->getParam('to_date');
$export          = $this->getRequest()->getParam('export');

$joined_at_from  = $this->getRequest()->getParam('joined_at_from');
$joined_at_to    = $this->getRequest()->getParam('joined_at_to');

$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger  = $this->_helper->flashMessenger;

$TITLE_PGPB_SALE    = unserialize(LIST_SALE_PGS);

if($title)
{
    $TITLE_PGPB_SALE    =  array($title);
    $this->view->title  = $title;
}

$total              = 0;
$limit              = LIMITATION;

$area_id_login      = array();

$QStaffTrainer          = new Application_Model_StaffTrainer();
    $QAsm                   = new Application_Model_Asm();
    $QArea              = new Application_Model_Area();
    $QBigArea              = new Application_Model_BigArea();


$params = array_filter(array(
    'name'              => $name,
    'title'             => $TITLE_PGPB_SALE,
    'big_area_id'       => $big_area_id,
    'area_id'           => $area_id,
    'regional_market'   => $regional_market,
    'code'              => $code,
    'email'             => $email,
    'date'              => $date,
    'month'             => $month,
    'year'              => $year,
    'off'               => My_Staff_Status::On,
    'from_date'         => $from_date,
    'to_date'           => $to_date,
    'export'            => $export,
    'joined_at_from'    => $joined_at_from,
    'joined_at_to'      => $joined_at_to,
));
$params['sort']                 = $sort;
$params['desc']                 = $desc;

if( $userStorage->team == TRAINING_TEAM
    || in_array($userStorage->group_id,array(ADMINISTRATOR_ID,ASM_ID,ASMSTANDBY_ID ))) {
    $area_id_login = $QStaffTrainer->getAreaTrainer($userStorage->id);
}

if(isset($area_id_login['province']) and count($area_id_login['province']))
{

    $params['regional_market_right']     = array_keys($area_id_login['province']);

    $this->view->regional_markets  = $area_id_login['province'];
}

if(isset($area_id_login['area']) and count($area_id_login['area']) )
{

    $params['area_trainer_right']   = array_keys($area_id_login['area']);


    if(!empty($big_area_id)){ //for seacrhing view
        $area_trainer = $QArea->getAreaByBigArea($big_area_id,array_keys($area_id_login['area']));
    }else{
        $area_trainer = $area_id_login['area'];
    }
}
$this->view->area_trainer = $area_trainer;



$QTeam = new Application_Model_Team();
$recursiveDepartmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->recursiveDepartmentTeamTitle = $recursiveDepartmentTeamTitle;

$staffs = $QStaffTrainer->fetchPaginationStaff($page, $limit, $total, $params);

if($export and $export == 1) {
    //$data = $QStaffTrainer->getListExportPgsSale($params);
    $query = $staffs;
    $this->reportListPgsSale($query);
}

$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->staffs     = $staffs;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'trainer/index' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);


//$this->view->list_big_area       = $list_big_area;
//$this->view->big_area       = $QBigArea->get_big_area_by_area(array_keys($area_id_login['area']));

$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages   = $messages;
$messages_error         = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;