<?php
$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages       = $messages;
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;

$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$page                       = $this->getRequest()->getParam('page', 1);
$name                       = $this->getRequest()->getParam('name');
$type 						= $this->getRequest()->getParam('type');


$params = array_filter(array(
    'name' => $name,
    'type' => $type
));

$limit = LIMITATION;
$total = 0;

$assets = $QTrainerTypeAsset->fetchPagination($page, $limit, $total, $params);
$QGood = new Application_Model_Good();
$goods = $QGood->get_cache();
$this->view->goods = $goods;

$this->view->assets     = $assets;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'trainer/type-asset' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);






