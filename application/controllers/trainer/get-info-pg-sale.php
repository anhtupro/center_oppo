<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$staff_id            = $this->getRequest()->getParam('staff_id');
$QStaffTrainer       = new Application_Model_StaffTrainer();
$QStaffPoint         = new Application_Model_StaffPoint();
$QStaffRewardWarning = new Application_Model_StaffRewardWarning();
$QStaff = new Application_Model_Staff();
$db = Zend_Registry::get('db');
// -----------------------------------------------------------------
$result = array(
    'knowledge',
    'skill',
    'attitude',
    'facebook_link'
);

if($staff_id)
{

    $profile         = $QStaffTrainer->getInfoStaff($staff_id);
    $type_evaluation = unserialize(TRAINER_EVALUATION);
    $staff = $QStaff->find($staff_id)->current();    
    $result['staff']['staff_name'] = $staff['firstname'].' '.$staff['lastname'];
    $result['staff']['gender']     = $staff['gender'] = 1 ? 'Male' : 'Felmale';
    $result['staff']['joind_at']   = date('d/m/Y',strtotime($staff['joined_at']));

    if($profile)
    {
        foreach ($profile as $key => $value)
        {
            $result['knowledge']      = $type_evaluation[$value['knowledge']];
            $result['skill']          = $type_evaluation[$value['skill']];
            $result['attitude']       = $type_evaluation[$value['attitude']];
            $result['facebook_id']    = $value['facebook_id'];
        }
    }

    $staffPoint         = $QStaffPoint->getInfoStaff($staff_id);

    if($staffPoint)
    {
        foreach($staffPoint as $key => $val)
        {
            $result['point']['point'][]      = $val['point'];
            $result['point']['month'][]      = $val['month'];
            $result['point']['year'][]       = $val['year'];
        }
    }
    $staffRewardWarning = $QStaffRewardWarning->getInfoStaff($staff_id);
    $typeRewardWarning  =  unserialize(TYPE_PG);

    if($staffRewardWarning)
    {
        foreach($staffRewardWarning as $key => $item)
        {
            $result['reward_warning']['type'][]    = $typeRewardWarning[$item['type']];
            $result['reward_warning']['content'][] = $item['content'];
            $result['reward_warning']['month'][]   = $item['month'];
            $result['reward_warning']['year'][]    = $item['year'];
        }
    }

    $sellout = My_Kpi::fetchGrid($staff_id,date('Y-m-01'),date('Y-m-t'));
    $sellout = array_shift($sellout);
    $total = 0;
    if(count($sellout)){
        foreach($sellout as $item):
            $total += $item['quantity'];
        endforeach;
    }
    
    $result['sellout'] = $total;

    //
    $select_store = $db->select()
        ->from(array('a'=>'store_staff'),array('store_name'=>'b.name'))
        ->join(array('b'=>'store'),'a.store_id = b.id AND a.staff_id',array())
        ->where('a.staff_id = ?',$staff_id);
    $stores = $db->fetchAll($select_store);
    if(count($stores) > 0){
        foreach($stores as $value){
            $result['store'][] = $value['store_name'];
        }
    }

    echo json_encode($result);
}