<?php

$id = $this->getRequest()->getParam('id');
$db = Zend_Registry::get('db');
$QTrainerOrder          = new Application_Model_TrainerOrder();
$QTrainerInventory      = new Application_Model_TrainerInventory();

$flashMessenger         = $this->_helper->flashMessenger;
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
if($this->getRequest()->isPost()){
	if(count($id)){
		try{
			$db->beginTransaction();
			foreach($id as $key=>$value){
				$where_order = $QTrainerOrder->getAdapter()->quoteInto('id = ?', $value);
				$order       = $QTrainerOrder->fetchRow($where_order);
				if($order['order_to'] != $userStorage->id){
					$flashMessenger->setNamespace('error')->addMessage('Error NOT trainer!');
					$this->_redirect('/trainer/trainer-take-asset');
				}

				$where = array();
				$where[] = $QTrainerOrder->getAdapter()->quoteInto('id = ?', $value);
				$where[] = $QTrainerOrder->getAdapter()->quoteInto('order_to = ?', $userStorage->id);
				$data = array(
					'order_to_confirm_at'=> date('Y-m-d H:i:s')
				);
				$QTrainerOrder->update($data, $where);

				//Update Stock trainer
				$stock = $QTrainerInventory->updateInventory($value);
				if($stock['status'] != 1){
					$flashMessenger->setNamespace('error')->addMessage($stock['message']);
					$this->_redirect('/trainer/trainer-take-asset');
				}
				//END - Update Stock trainer

				$flashMessenger->setNamespace('success')->addMessage('Done');
			}
			$db->commit();
		}catch (Exception $e){
			$db->rollBack();
			$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
		}
		$this->_redirect('/trainer/trainer-take-asset');

	}else{
		$flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn vật phẩm');
	}
}
$this->_redirect('/trainer/trainer-take-asset');