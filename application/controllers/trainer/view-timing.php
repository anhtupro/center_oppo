<?php 
$staff_id = $this->getRequest()->getParam('staff_id');
$back_url = $this->getRequest()->getParam('back_url','/manage/asm');
$month = $this->getRequest()->getParam('month',date('m/Y'));
$params = array(
    'month' => $month
);
$this->view->params = $params;
$flashMessenger = $this->_helper->flashMessenger;
$QStaff = new Application_Model_Staff();
$staff = $QStaff->find($staff_id)->current();
if(!$staff){
    $flashMessenger->setNamespace('error')->addMessage('Cant not find this staff');
    $this->_redirect($back_url);
}

$this->view->staff = $staff;
$month = My_Date::normal_to_mysql($month);
$from = $month;
$to = date('Y-m-t',strtotime($from));
$db = Zend_Registry::get('db');
$cols = array(
    'a.*',
    'staff_name' => 'CONCAT(b.firstname," ",b.lastname)'
);
$select = $db->select()
        ->from(array('a'=>'time'),$cols)
        ->join(array('b'=>'staff'),'a.staff_id = b.id',array())
        ->where('a.staff_id = ?',$staff_id)
        ->where('DATE(a.created_at) >= ?',$from)
        ->where('DATE(a.created_at) <= ?',$to)
        ->order('created_at ASC')
        ;
$times = $db->fetchAll($select);
$this->view->times = $times;

$trainer_works = unserialize(TRAINER_WORK);
$this->view->trainer_works = $trainer_works;