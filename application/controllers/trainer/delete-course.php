<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger     = $this->_helper->flashMessenger;

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');
$back_url           = HOST.'trainer/course';
$QTrainerCourse     = new Application_Model_TrainerCourse();

try
{
    $db->beginTransaction();
    $id                     = $this->getRequest()->getParam('id');

    if(isset($id) and $id)
    {
        $whereTrainerCourse   =  array();
        $whereTrainerCourse[] = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$id);
        $whereTrainerCourse[] = $QTrainerCourse->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
        $rowTrainerCourse     = $QTrainerCourse->fetchRow($whereTrainerCourse);
        if($rowTrainerCourse)
        {
            $data = array(
                'del' => 1
            );
            $QTrainerCourse->update($data,$whereTrainerCourse);
            // to do log
            $info = array('DEL-COURSE','old'=>$rowTrainerCourse,'new'=>$data);
            $QLog->insert( array (
                'info'          => json_encode($info),
                'user_id'       => $userStorage->id,
                'ip_address'    => $ip,
                'time'          => date('Y-m-d H:i:s'),
            ) );
        }
        else
        {
            throw new Exception('Course Is Not Found !');
        }
    }
    else
    {
        throw new Exception(' Not Found  Course ! ');
    }

    $db->commit();
    $flashMessenger ->setNamespace('success')->addMessage('Done');
}
catch(Exception $e)
{
    $db->rollback();
    $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
}


$this->redirect($back_url);