<?php

$id = $this->getRequest()->getParam('id');
$back_url = HOST.'trainer/trainer-area';
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;
$QStaff = new Application_Model_Staff();
$QTeam  = new Application_Model_Team();
if (!$id) {
    $flashMessenger->setNamespace('error')->addMessage('Invalid ID');
    $this->_redirect($back_url);
}else {
    //dánh sách duoc phep
    $allow_all = $QStaff->staffTrainerAllowReport();
    $allow_all[] = $id;

    if(!in_array($userStorage->id,$allow_all)){
        $flashMessenger->setNamespace('error')->addMessage('Not permission');
        $this->_redirect($back_url);
    }

    $QStaff = new Application_Model_Staff();
    $staff = $QStaff->find($id);
    $staff = $staff->current();
    
    $title = $staff['title'];
    $team = $QTeam->find($title);
    $team = $team->current();

    if (!$staff 
           // || !in_array($staff['group_id'], My_Staff_Group::$training_allow_area_view)
        //|| !in_array($team['access_group'], My_Staff_Group::$allow_in_area_view)
            || !is_null($staff['off_date']) 
            || $staff['status'] != My_Staff_Status::On
            || !in_array($userStorage->id, unserialize(TRAINER_KEY_PROJECT))
        ) {
        $flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages = $flashMessenger->setNamespace('error')->addMessage('This staff cannot be assigned to Area views');

        $this->_redirect($back_url);
    }
    
    $QAsm = new Application_Model_Asm();
    $where = array();
    $where[] = $QAsm->getAdapter()->quoteInto('staff_id = ?', $id);
    $where[] = $QAsm->getAdapter()->quoteInto('type = ?', My_Region::Area);
    $this->view->asm = $QAsm->fetchAll($where);

    $where = array();
    $where[] = $QAsm->getAdapter()->quoteInto('staff_id = ?', $id);
    $where[] = $QAsm->getAdapter()->quoteInto('type = ?', My_Region::Province);
    $this->view->asm_region = $QAsm->fetchAll($where);

    $this->view->id = $id;

    $QArea = new Application_Model_Area();
    $this->view->areas = $QArea->get_cache();

    $QRegion = new Application_Model_RegionalMarket();
    $this->view->regions = $QRegion->get_cache_all();

    $this->view->staff = $staff;


}