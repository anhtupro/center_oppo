<?php 
	$this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $userStorage        = Zend_Auth::getInstance()->getStorage()->read();
    if($this->getRequest()->getMethod() == 'POST')
    {
        $id = $this->getRequest()->getParam('id');

        $QStaffEventReport     = new Application_Model_StaffEventReport();
        $whereStaffEventReport = $QStaffEventReport->getAdapter()->quoteInto('id = ?', $id);
        $row                   = $QStaffEventReport->fetchRow($whereStaffEventReport);
        if($row)
        {
            // date - visitor - staff id - dealer id - store id - picture - note

            $data = '<table class="table table-responsive">';
            // date
            $date = $row['date'];
            $data = $data.'<tr><td>Date:</td><td>'.$date.'</td></tr>';
            // visitor
            $visitors   = $row['visitors'];
            $data = $data.'<tr><td>Visitors:</td><td>'.$visitors.'</td></tr>';
            // dealer id
            $QStaffTrainingReport        = new Application_Model_StaffTrainingReport();
            $QStaffTrainer               = new Application_Model_StaffTrainer();
            $areaTrainer                 = $QStaffTrainer->getAreaTrainer($userStorage->id);
            $regional_market             = $areaTrainer['district'];
            $dealers                     = $QStaffTrainingReport->getDealerArea($regional_market);
            $nameDealer                  = $dealers[$row['dealer_id']];
            $data = $data.'<tr><td>Dealer Name:</td><td>'.$nameDealer.'</td></tr>';
            // store
            $QStore         = new Application_Model_Store();
            $cachedStore    = $QStore->get_cache();
            $store          = $cachedStore[$row['store_id']];
            $data = $data.'<tr><td>Store:</td><td>'.$store.'</td></tr>';
            // note
            $note = $row['note'];
            $data = $data.'<tr><td>Note:</td><td>'.$note.'</td></tr>';
            // picture
            $allPicture           = json_decode($row['picture'],true);
            $direct               = HOST.'public/photo/trainer/'.$allPicture['user_id'].'/'.$allPicture['direct'].'/';
            $data = $data.'<tr><td colspan="2" style="text-align:center"><b>Picture</b></td></tr>';
            $data = $data.'<tr><td colspan="2">';
            foreach($allPicture as $key => $value)
            {
                if($key == 'direct' || $key =='user_id')
                {
                    continue;
                }
                else if ($value)
                {
                    $data = $data.'<a data-featherlight="image" href="'.$direct.$value.'"><img src="'.$direct.$value.'" class="img-rounded" style="width:80px; height:80px; padding:10px;"/></a>';
                }
            }
            $data = $data.'</td></tr>';
            $data = $data.'</table>';

            echo $data;

        }
    }

?>