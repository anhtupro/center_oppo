<?php
$this->_helper->layout->disableLayout();
$db = Zend_Registry::get('db');
//remove cache
$cache = Zend_Registry::get('cache');
$cache->remove('regional_market_cache');
$cache->remove('area_cache');

$idNumber  = $this->getRequest()->getParam('id_number');
$course_id = $this->getRequest()->getParam('course_id');

$idNumber  = preg_replace("/(^[\r\n]*|[\r\n]+)[\r\t]*[\s\n]+/", "\n", $idNumber);
$idNumber  = explode("\n", $idNumber);
$idNumber  = array_unique($idNumber);

$result = array();

// cached team
$QTeam = new Application_Model_Team();
$this->view->cached_team = $QTeam->get_cache();

// get all title of team sale and training

$teamID         = unserialize(TEAM_FOR_ASSIGN_STAFF_TRAINER_COURSE);
$QTeam          = new Application_Model_Team();
$whereJobTile   = array();
$whereJobTile[] = $QTeam->getAdapter()->quoteInto('parent_id IN (?)', $teamID);
$whereJobTile[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
$jobTitle       = $QTeam->fetchAll($whereJobTile);

if($jobTitle)
{
    $this->view->jobtitle = $jobTitle;
}

// get area and province
$QArea = new Application_Model_Area();
$this->view->area = $QArea->get_cache();

// get all province
$QRegionalMarket = new Application_Model_RegionalMarket();
$province = $QRegionalMarket->get_cache();
$this->view->province = $province;

$error = null;

try
{
    // get locked 
    $QTrainerCourse     = new Application_Model_TrainerCourse();
    $whereCourseGetLock = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$course_id);
    $rowCourseGetLock   = $QTrainerCourse->fetchRow($whereCourseGetLock);
    if($rowCourseGetLock)
    {
        $is_locked = $rowCourseGetLock['locked'];
        $this->view->course_is_lock = $is_locked;
    }


    if (is_array($idNumber)) {
        $IdNumberLengths = unserialize(ID_NUMBER_LENGTH);
        foreach ($idNumber as $key => $value) {
            $value          = trim($value);
            if( in_array(strlen($value),$IdNumberLengths) == false OR !is_int(intval($value))    )
            {
                $error = $value. ' IS NOT TRUE FORMAT';
                throw new Exception ($error);
            }

            $QStaff               = new Application_Model_Staff();
            $QStaffTraining       = new Application_Model_StaffTraining();
            $QTrainerCourseDetail = new Application_Model_TrainerCourseDetail();

            $selectStaff = $db->select()
                                ->from(array('a'=>'staff'),array('a.*'))
                                ->where('ID_number = ?',$value)
                                ->where('a.status != ?',0)
                                ->order('a.id DESC')
                                ->limit(1);
            $rowStaff = $db->fetchRow($selectStaff);

            if($rowStaff)
            {
                $result['new_staff'][]          = 0;
                $result['id'][]                 = $rowStaff['id']? $rowStaff['id']: null;
                $result['cmnd'][]               = $value;
                $result['code'][]               = $rowStaff['code']? $rowStaff['code']: null;
                $result['firstname'][]          = $rowStaff['firstname']? $rowStaff['firstname']: null;
                $result['lastname'][]           = $rowStaff['lastname'] ? $rowStaff['lastname'] : null;
                $result['team'][]               = $rowStaff['team'] ? $rowStaff['team'] : null;
                $result['title'][]              = $rowStaff['title'] ? $rowStaff['title']: null ;
                $result['regional_market'][]    = $rowStaff['regional_market'] ? $rowStaff['regional_market'] : null;
                $whereArea                      = $QRegionalMarket->getAdapter()->quoteInto('id = ?',$rowStaff['regional_market']);
                $rowArea                        = $QRegionalMarket->fetchRow($whereArea);
                $result['area_id'][]            = $rowArea['area_id']? $rowArea['area_id']:null;

            }
            else
            {
                // get info new staff
                $whereStaffTraining             = array();
                $whereStaffTraining[]           = $QStaffTraining->getAdapter()->quoteInto('cmnd = ?',$value);
                $whereStaffTraining[]           = $QStaffTraining->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
                $rowStaffTraining               = $QStaffTraining->fetchRow($whereStaffTraining);

                $result['new_staff'][]          = 1;
                $result['id'][]                 = $rowStaffTraining['id']? $rowStaffTraining['id']: null;
                $result['cmnd'][]               = $value;
                $result['code'][]               = null;
                $result['firstname'][]          = $rowStaffTraining['firstname']? $rowStaffTraining['firstname']: null;
                $result['lastname'][]           = $rowStaffTraining['lastname'] ? $rowStaffTraining['lastname'] : null;
                $result['team'][]               = $rowStaffTraining['team'] ? $rowStaffTraining['team'] : null;
                $result['title'][]              = $rowStaffTraining['title'] ? $rowStaffTraining['title']: null ;
                $result['regional_market'][]    = $rowStaffTraining['regional_market'] ? $rowStaffTraining['regional_market'] : null;
                // get area_id
                $whereAreaNT                    = $QRegionalMarket->getAdapter()->quoteInto('id = ?',$rowStaffTraining['regional_market']);
                $rowAreaNT                      = $QRegionalMarket->fetchRow($whereAreaNT);
                $result['area_id'][]            = $rowAreaNT['area_id']? $rowAreaNT['area_id']:null;
               
            }
        }
        
        $this->view->list = $result;
        
        
    }
}
catch (Exception $e)
{
    $this->view->error = $error;
}
