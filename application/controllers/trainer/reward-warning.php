<?php
// dau tien minh lay staff_id
$staffID           = $this->getRequest()->getParam('staff_id');
$ip                = $this->getRequest()->getServer('REMOTE_ADDR');
$userStorage       = Zend_Auth::getInstance()->getStorage()->read();
$QStaff            = new Application_Model_Staff();
$whereStaff        = $QStaff->getAdapter()->quoteInto('id = ?',$staffID);
$rowStaff          = $QStaff->fetchRow($whereStaff);
$this->view->staff = $rowStaff;
$url_after_submit  = HOST."trainer/reward-warning?staff_id=".$staffID;

$back_url = $_SERVER['HTTP_REFERER'];
$this->view->back_url = $back_url;

$userStorage                = Zend_Auth::getInstance()->getStorage()->read();
$QLog                       = new Application_Model_Log();

$flashMessenger              = $this->_helper->flashMessenger;
$messages                    = $flashMessenger->setNamespace('success')->getMessages();
$this->view->message_success = $messages;
$messages_error              = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages        = $messages_error;

$QStaffRewardWarning = new Application_Model_StaffRewardWarning();
$QRewardWarningType  = new Application_Model_RewardWarningType();
$QStaffTrainer       = new Application_Model_StaffTrainer();

$reward_warning_type = $QRewardWarningType->get_cache($where);
$reward_type         = $QRewardWarningType->get_cache_reward($where);

$this->view->reward_warning_type = $reward_warning_type;
$this->view->reward_type         = $reward_type;

try
{
    $checkPGSALE = $QStaffTrainer->checkPGPBSALE($staffID);
    if($checkPGSALE)
    {
        throw new Exception($checkPGSALE);
    }

    $checkAreaPGSALEISAreaTrainer = $QStaffTrainer->checkAreaPGSALEISAreaTrainer($staffID,$userStorage->id);

    if($checkAreaPGSALEISAreaTrainer)
    {
        throw new Exception($checkAreaPGSALEISAreaTrainer);
    }
}
catch (Exception $e)
{
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect(HOST . 'trainer');
}

// get history
$page               = $this->getRequest()->getParam('page', 1);
$params             = array(
    'staff_id'      => $staffID
);
$limit              = LIMITATION;
$total              = 0;
$rows               = $QStaffRewardWarning->fetchPagination($page, $limit, $total, $params);

$this->view->params         = $params;
$this->view->staffHistory   = $rows;
$this->view->limit          = $limit;
$this->view->total          = $total;
$this->view->url            = HOST.'trainer/reward-warning'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset         = $limit*($page-1);

$image_url = '/photo/trainer/reward_warning/';
$this->view->image_url = $image_url;
// save and update
if($this->getRequest()->getMethod()=='POST')
{
    ini_set('upload_max_filesize', '10M');
    $db = Zend_Registry::get('db');
    $db->beginTransaction();

    try {

        $type              = $this->getRequest()->getParam('type');
        $warning_type      = $this->getRequest()->getParam('warning_type');
        $warning_type_warning      = $this->getRequest()->getParam('warning_type_warning');
        
        $content           = $this->getRequest()->getParam('content');
        $date              = $this->getRequest()->getParam('date');
        $date              = str_replace('/', '-', $date);
        $date              = date("Y-m-d", strtotime($date) );
        /*
        $monthYear = $this->getRequest()->getParam('month_year');

        $month = explode("-", $monthYear)[0];
        $year  = explode("-", $monthYear)[1];

        if(!$type || !$content)
        {
            throw new Exception('Can not found Type Or Content');
        }

        if(!$monthYear)
        {
            throw new Exception('Can not found Month Or Year');
        }
        */
        
        $save_folder = 'trainer'.DIRECTORY_SEPARATOR.'reward_warning';
        $requirement = array(
                'Size'      => array('min' => 50, 'max' => 50000000),
                'Count'     => array('min' => 1, 'max' => 1),
                'Extension' => array('jpg', 'png','jpeg','doc','docx','pdf'),
        );
    
        $data = array(
            'staff_id' => $staffID,
            'type'     => $type,
            'content'  => $content,
            'date'     => $date,
            'warning_type' => !empty($warning_type) ? $warning_type : $warning_type_warning,
            //'month'    => $month,
            //'year'     => $year
        );

        if(file_exists($_FILES['file']['tmp_name'])) {
            $result_file  = My_File::get2($save_folder,$requirement);
            
            $file_name = $result_file['filename'];
            
            //Move file to S3
            require_once 'Aws_s3.php';
            $s3_lib = new Aws_s3();
            
            $file_location = 'photo'. DIRECTORY_SEPARATOR .'trainer'. DIRECTORY_SEPARATOR .'reward_warning'. DIRECTORY_SEPARATOR .$file_name;
            $detination_path = 'photo/trainer/reward_warning/';
            $destination_name = $file_name;
            
            $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
            
            if($upload_s3['message'] != 'ok'){
                echo json_encode([
                    'status' => 0,
                    'message' => "Upload S3 không thành công",
                ]);
                return;
            }
            // END - Move file to S3
            
            $data['file'] = $file_name;
        }

        // insert
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $userStorage->id;

        $QStaffRewardWarning->insert($data);

        // to do log
        $info = array('Insert Reward Warning PG', 'new' => $data);
        $QLog->insert(array(
            'info'       => json_encode($info),
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));


        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Done!');
        $this->_redirect($url_after_submit);

    }
    catch (Exception $e)
    {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->_redirect($url_after_submit);
    }
}