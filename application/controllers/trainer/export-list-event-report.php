<?php 
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
        'stt',
        'date report',
        'Trainer',
        'area',
        'from date',
        'to date',
        'dealer',
        'store',
        'visitors',
        'event type',
        'human',
        'fee',
        'Role',
        'sale out before',
        'sale out after',
        'note'
    );

    

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    foreach ($heads as $key)
    {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index = 2;
    
    $QArea      = new Application_Model_Area();
    $cachedArea = $QArea->get_cache();

    $QRegionalMarket      = new Application_Model_RegionalMarket();
    $cachedRegionalMarket = $QRegionalMarket->get_cache();
    
    $QStore               = new Application_Model_Store();
    $cachedStore          = $QStore->get_cache();

    $intCount = 1;

    foreach ($data as $_key => $item)
    {
        $alpha = 'A';
        $date      = date('d/m/Y',strtotime($item['date']));
        $from_date = (isset($item['from_date']) AND $item['from_date']) ? date('d/m/Y',strtotime($item['from_date'])) : '';
        $to_date   = (isset($item['to_date']) AND $item['to_date']) ? date('d/m/Y',strtotime($item['to_date'])) : '';
        $dealer    = $item['dealer_name'];
        $store     = $cachedStore[$item['store_id']];

        $sheet->getCell($alpha++ . $index)->setValueExplicit($intCount++,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($date,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['staff_name'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['area_name'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($from_date,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($to_date,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($dealer,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($store,PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['visitors'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['event_name'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['quantity_human'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['fee'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['function'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(intval($item['sell_out_before']),PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(intval($item['sell_out_after']),PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($item['note'],PHPExcel_Cell_DataType::TYPE_STRING);
        $index++;
    }
    
    

    $filename = ' Training event report ' . date('d-m-Y H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;

?>