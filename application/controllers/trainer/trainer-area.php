<?php

$QArea               = new Application_Model_Area();
$QStaff              = new Application_Model_Staff();
$QAsm                = new Application_Model_Asm();
$QRegion             = new Application_Model_RegionalMarket();
$db                  = Zend_Registry::get('db');
$this->view->areas   = $QArea->get_cache();
$this->view->regions = $QRegion->get_cache();
$params              = array(
    'group_id' => My_Staff_Group::$training_allow_area_view,
);
$page                = $this->getRequest()->getParam('page', 1);
$limit               = 0;
$total               = 0;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$key_project = unserialize(TRAINER_KEY_PROJECT);
$key_project[] = SUPERADMIN_ID;

$this->view->key_project = $key_project;

// danh sách được xem tất cả
$allow_all = $QStaff->staffTrainerAllowReport();

if(in_array($userStorage->id, $allow_all)){

    $asm         = $QAsm->fetchPagination($page, $limit, $total, $params);
    $list        = array();
    
    foreach ($asm as $key => $value) {

        if (in_array($value['title'], unserialize(TRAINER_TITLE_ID_LIST))){

            if (in_array($userStorage->id, $allow_all)) {

                if (!isset($list[$value['group_id']]))
                    $list[$value['group_id']] = array();

                if (!isset($list[$value['group_id']][$value['id']]))
                    $list[$value['group_id']]
                    [$value['id']] = array(
                        'firstname'  => $value['firstname'],
                        'lastname'   => $value['lastname'],
                        'email'      => $value['email'],
                        'title_name' => $value['title_name'],
                        'area'       => array(),
                    );

                $list[$value['group_id']]
                [$value['id']]
                ['area'][] = $value['area_id'];

            } else {
                if (!isset($list[$value['group_id']]) AND $value['id'] == $userStorage->id)
                    $list[$value['group_id']] = array();

                if (!isset($list[$value['group_id']][$value['id']]) AND $value['id'] == $userStorage->id)
                    $list[$value['group_id']]
                    [$value['id']] = array(
                        'firstname' => $value['firstname'],
                        'lastname' => $value['lastname'],
                        'email' => $value['email'],
                        'title_name' => $value['title_name'],
                        'area' => array(),
                    );

                if ($value['id'] == $userStorage->id) {
                    $list[$value['group_id']]
                    [$value['id']]
                    ['area'][] = $value['area_id'];
                }
            }
        }

    }


}

if($userStorage->title == TITLE_TRAINER_LEADER || $userStorage->title == INTERNAL_COMMUNICATION_SPECIALIST || in_array($userStorage->id, $allow_all)):
    $cols = array(
        'staff_id' => 'a.id',
        'staff_name' => 'CONCAT(a.firstname," ",a.lastname)',
        'area_name' => 'd.name',
        'email' => 'a.email'
    );
    $selectTrainer = $db->select()
        ->from(array('a'=>'staff'),$cols)
        //->join(array('b'=>'regional_market'),'a.regional_market = b.id',array())
        ->join(array('b'=>'asm'),'a.id = b.staff_id',array())
        ->join(array('c'=>'asm'),'c.area_id = b.area_id',array())
        ->join(array('d'=>'area'),'d.id = b.area_id',array())
        ->where('c.staff_id = ?',$userStorage->id)
        ->where('a.title IN (?)', unserialize(TRAINER_LEADER_ID_LIST))
        ->where('a.status IN (?)',array(1,2,3))
        ->group(array('b.area_id','a.id'))
        ->order('d.name')

    ;
	
    $list_trainer = $db->fetchAll($selectTrainer);
$this->view->list_trainer = $list_trainer;
endif;


$this->view->list             = $list;
$flashMessenger               = $this->_helper->flashMessenger;
$this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->userStorage      = $userStorage;

