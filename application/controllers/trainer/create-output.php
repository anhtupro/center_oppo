<?php
$back_url                   = '/trainer/orders';
$this->view->back_url       = $back_url;
$flashMessenger             = $this->_helper->flashMessenger;
$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$whereTypeAsset             = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
$type_asset                 = $QTrainerTypeAsset->fetchAll($whereTypeAsset);
$arrayTypeAsset             = array();

$type = unserialize(CATEGORY_ASSET_TRAINER);
$this->view->type = $type;
$db = Zend_Registry::get('db');
if($type_asset->count())
{
    foreach($type_asset as $key => $value)
    {
        $arrayTypeAsset[$value['id']] = $value['name'];
    }
}

$this->view->type_asset     = $arrayTypeAsset;
// danh sach trainer lay tu staff
$QTrainerOrderOut  = new Application_Model_TrainerOrderOut();
$this->view->list_trainer = $QTrainerOrderOut->getStaffTrainer();

try
{
    $id                  = $this->getRequest()->getParam('id');
    $QTrainerOrderOutDetail = new Application_Model_TrainerOrderOutDetail();
    $QTrainerOrderImei   = new Application_Model_TrainerOrderImei();
    if($id){
        $order = $QTrainerOrderOut->find($id)->current();
        $selectDetail = $db->select()
            ->from(array("p"=>'trainer_order_out_detail'),array('p.*'))
            ->where('p.del IS NULL OR p.del = 0')
            ->where('order_out_id = ?',$id);
        $orderDetail = $db->fetchAll($selectDetail);

        $listImei = array();
        foreach($orderDetail as $value):
            if($value['type'] == 1){
                $selectImei = $db->select()
                    ->from(array('p'=>'trainer_order_imei'),array('p.*'))
                    ->where('p.del IS NULL OR p.del = 0')
                    ->where('p.type = ?',2)
                    ->where('p.order_detail_id = ?',$value['id']);
                $rows = $db->fetchAll($selectImei);
                $listImei[$value['id']] = $rows;
            }
        endforeach;
        $this->view->order = $order;
        $this->view->orderDetail = $orderDetail;
        $this->view->listImei = $listImei;

    }
}
catch (Exception $e)
{
    $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    echo '<script>setTimeout(function(){parent.location.href="'.HOST.'trainer/orders"})</script>';
}



