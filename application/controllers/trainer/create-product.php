<?php

$id = $this->getRequest()->getParam('id');

$QTrainerProduct  = new Application_Model_TrainerProduct();
$where = $QTrainerProduct->getAdapter()->quoteInto('id = ?',$id);
$rowProduct   = $QTrainerProduct->fetchRow($where);

$this->view->rowProduct = $rowProduct;

$flashMessenger = $this->_helper->flashMessenger;

$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages;

$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;



