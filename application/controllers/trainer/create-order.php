<?php

$sn = $this->getRequest()->getParam('sn');
$confirm = $this->getRequest()->getParam('confirm');

$params = array(
    'confirm' => $confirm
);

if(isset($sn) and $sn){
    $QTrainerOrder = new Application_Model_TrainerOrder();
    $where         = $QTrainerOrder->getAdapter()->quoteInto('sn = ?', $sn);

    $trainer_order = $QTrainerOrder->fetchAll($where);
    $this->view->trainer_order = $trainer_order;
}



$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$whereTypeAsset             = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
$type_asset                 = $QTrainerTypeAsset->fetchAll($whereTypeAsset);

$this->view->type_asset = $type_asset;

$QTrainerOrderOut  = new Application_Model_TrainerOrderOut();
$this->view->list_trainer = $QTrainerOrderOut->getStaffTrainer();
$this->view->off = $QTrainerOrderOut->getStaffTrainerOff();

$this->view->params = $params;
// Messages
$flashMessenger = $this->_helper->flashMessenger;

$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages;

$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;


