<?php
$flashMessenger              = $this->_helper->flashMessenger;
$messages                    = $flashMessenger->setNamespace('success')->getMessages();
$this->view->message_success = $messages;
$messages_error              = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages        = $messages_error;

$userStorage                 = Zend_Auth::getInstance()->getStorage()->read();
$QLog                        = new Application_Model_Log();
$ip                          = $this->getRequest()->getServer('REMOTE_ADDR');
$db                          = Zend_Registry::get('db');

$QArea                       = new Application_Model_Area();
$this->view->cached_area     = $QArea->get_cache();

$QRegionalMarket                     = new Application_Model_RegionalMarket();
$this->view->cached_regional_market  = $QRegionalMarket->get_cache();

$cache = Zend_Registry::get('cache');
$cache->remove('asm_cache');
$cache->remove('regional_market_cache');

$page                        = $this->getRequest()->getParam('page', 1);
$name                        = $this->getRequest()->getParam('name');
$cmnd                        = $this->getRequest()->getParam('cmnd');
$regional_market             = $this->getRequest()->getParam('regional_market');


$QStaffTrainer        = new Application_Model_StaffTrainer();
$QRegionalMarket      =  new Application_Model_RegionalMarket();
$cachedRegionalMarket = $QRegionalMarket->get_cache();

$areaStaffTrainer               = $QStaffTrainer->getAreaTrainer($userStorage->id);
$this->view->province_trainer   = $areaStaffTrainer['province'];
$regional_market_right          = $areaStaffTrainer['province'];

$QStaffTraining              = new Application_Model_StaffTraining();

$params              = array_filter(array(
    'name'                  => $name,
    'cmnd'                  => $cmnd,
    'regional_market'       => $regional_market,
    'regional_market_right' => array_keys($regional_market_right)
));


$limit                      = LIMITATION;
$total                      = 0;
$rows                       = $QStaffTraining->fetchPagination($page, $limit, $total, $params);

$this->view->params         = $params;
$this->view->list           = $rows;
$this->view->limit          = $limit;
$this->view->total          = $total;
$this->view->url            = HOST.'trainer/list-new-staff'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset         = $limit*($page-1);
$this->view->staff_id       = $userStorage->id;
