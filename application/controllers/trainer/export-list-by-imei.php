<?php 
	require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
        'stt'                   =>'STT',
        'asset',
        'imei',
        'note',
        'staff name',
        'staff code'
    );



    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    foreach ($heads as $key)
    {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }
    $index = 2;

    try
    {
        if ($data)
        {
            $intCount = 1;
            foreach ($data as $_key => $item)
            {

                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['asset_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['imei'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['note'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['staff_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($item['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
        }
    }
    catch (exception $e)
    {

    }

    $filename = ' Trainer asset report ' . date('d-m-Y H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
?>