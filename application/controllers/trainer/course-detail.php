<?php
//remove cache
$cache = Zend_Registry::get('cache');
$cache->remove('staff_all_cache');
$cache->remove('staff_training_all_cache');

// get cache
$QStaff              = new Application_Model_Staff();
$cachedStaff         = $QStaff->get_all_cache();
$QStaffTraining      = new Application_Model_StaffTraining();
$cachedStaffTraining = $QStaffTraining->get_all_cache();
$db = Zend_Registry::get('db');
// hien thi thong tin cua course
$id 	     = $this->getRequest()->getParam('id');
$QCourse     = new Application_Model_TrainerCourse();
$whereCourse = $QCourse->getAdapter()->quoteInto('id = ?',$id);
$rowCourse   = $QCourse->fetchRow($whereCourse);

if($rowCourse)
$this->view->course = $rowCourse;

//----------------------------------------------------------------------------------------------
//-------------------------- Get ALl List Have Assign ------------------------------------------
//----------------------------------------------------------------------------------------------

$arrayCMND                 = array();
$QTrainerCourseDetail      = new Application_Model_TrainerCourseDetail();
$whereTrainerCouseDetail   = array();
$whereTrainerCouseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$id);
$whereTrainerCouseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
$rowsTrainerCourseDetail   = $QTrainerCourseDetail->fetchAll($whereTrainerCouseDetail);

if($rowsTrainerCourseDetail->count())
{
	$this->view->totalAssign = $rowsTrainerCourseDetail->count();

	foreach ($rowsTrainerCourseDetail as $key => $value) {

		if( isset($value['staff_id']) and $value['staff_id'])
		{
			$arrayCMND[]=$cachedStaff[$value['staff_id']]['ID_number'];
		}
		elseif(isset($value['new_staff_id']) and $value['new_staff_id'])
		{
            $arrayCMND[]=$cachedStaffTraining[$value['new_staff_id']]['cmnd'];
		}
	}
	$stringCMND = NULL;
	foreach ($arrayCMND as $item) {
		$stringCMND = $stringCMND . $item."\n";
	}

	$this->view->list_cmnd = $stringCMND;
	
	// check khoa hoc do co khoa chua thi khong cho check luon 

	$QTrainerCourse           = new Application_Model_TrainerCourse();
	$whereTrainerCourse       = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$id);
	$checked                  = $QTrainerCourse->fetchRow($whereTrainerCourse);
	$Locked                   = $checked['locked'];
	if(isset($Locked) and $Locked == 1)
	{
			$this->view->course_locked = $Locked;
	}
}
