<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$id                 = $this->getRequest()->getParam('id');
$db                 = Zend_Registry::get('db');
$db->beginTransaction();
try
{
    $QTrainerOrderOut          = new Application_Model_TrainerOrderOut();

    if($this->getRequest()->getMethod() == 'POST')
    {
           $data = array(
               'confirmed_at' => date('Y-m-d H:i:s'),
               'confirmed_by' => $userStorage->id,
               'status' => 2
           );
        $where = $QTrainerOrderOut->getAdapter()->quoteInto('id = ?',$id);
        $QTrainerOrderOut->update($data,$where);
        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Done');
        $this->_redirect('/trainer/orders-out');
    }
}
catch (Exception $e)
{
    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect('/trainer/orders-out');

//    echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
//    echo '<script>window.parent.unblockUI();</script>';
//    echo '<div class="alert alert-error">Failed - '.$e->getMessage().'</div>';

}