<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger     = $this->_helper->flashMessenger;

$id_course         = $this->getRequest()->getParam('id_course');

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');

$QTrainerCourse     = new Application_Model_TrainerCourse();
$QStaffTraining     = new Application_Model_StaffTraining();

if($this->getRequest()->getMethod()=='POST')
{
    try
    {
        $db->beginTransaction();

        $ids               = $this->getRequest()->getParam('ids');
        $del               = $this->getRequest()->getParam('del');
        $cmnd              = $this->getRequest()->getParam('cmnd');
        $firstname         = $this->getRequest()->getParam('firstname');
        $lastname          = $this->getRequest()->getParam('lastname');
        $team              = $this->getRequest()->getParam('team');
        $title             = $this->getRequest()->getParam('title');
        $regional_market   = $this->getRequest()->getParam('regional_market');

        foreach ($ids as $key => $value) {
            $data = array(
                'cmnd'              => $cmnd[$key],
                'firstname'         => $firstname[$key],
                'lastname'          => $lastname[$key],
                'team'              => $team[$key],
                'title'             => $title[$key],
                'regional_market'   => $regional_market[$key]
            );

            //Check cmnd
            $IdNumberLengths = unserialize(ID_NUMBER_LENGTH);
            $cmnd_          = trim($cmnd[$key]);
            if( in_array(strlen($cmnd_),$IdNumberLengths) == false OR !is_int(intval($cmnd_))    )
            {
                $flashMessenger ->setNamespace('error')->addMessage($cmnd_. ' IS NOT TRUE FORMAT');
                $back_url           = HOST.'trainer/edit-staff?id='.$id_course;
                $this->redirect($back_url);
            }
            //end Check cmnd

            $where = $QStaffTraining->getAdapter()->quoteInto('id = ?', $value);
            if($del[$key] == 2){
				$data['del'] = 1;
                $QStaffTraining->update($data, $where);
            }
            else{
                $QStaffTraining->update($data, $where);
            }
            
        }

        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Done');
    }
    catch (Exception $e)
    {
        $db->rollback();
        $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    }
}

$back_url           = HOST.'trainer/edit-staff?id='.$id_course;
$this->redirect($back_url);
