<?php


$id                   = $this->getRequest()->getParam('id');
$note                 = $this->getRequest()->getParam('note');
$approve_type                 = $this->getRequest()->getParam('approve_type');


$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger     = $this->_helper->flashMessenger;

$params = array(
    'id'                => $id,
);

$QStaff                 = new Application_Model_Staff();
$QSpAssessmentApprove  = new Application_Model_SpAssessmentApprove();

$info = $QSpAssessmentApprove->getStaffInfo($params);
// if($_GET['dev']){
//     echo "<pre>";
//     print_r($info);
// }


if($this->getRequest()->getMethod()=='POST')
{
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        $where = [];
        $where[] = $QSpAssessmentApprove->getAdapter()->quoteInto('id = ?',$id);
        $where[] = $QSpAssessmentApprove->getAdapter()->quoteInto('confirmed_at IS NULL');
        $approve = $QSpAssessmentApprove->fetchRow($where);

        if(!empty($approve['confirmed_at'])){
            $flashMessenger->setNamespace('error')->addMessage('Pgs Lên sao đã được xác nhận!.');
            $this->_redirect('/trainer/assessment-approve-details?id='.$id);
        }
        
        $data = [
            'confirmed_at' => date('Y-m-d'),
            'confirmed_by' => $userStorage->id,
            'note'         => $note
        ];
        
        $QSpAssessmentApprove->update($data, $where);
        
        
        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Hoàn thành!');
    } catch (Exception $e) {
        $db->rollback();
        $e->getMessage();
    }
    $this->_redirect('/trainer/assessment-approve-details?id='.$id);
}


$this->view->info = $info;

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;



