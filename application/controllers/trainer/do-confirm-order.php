<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');

try
{

    $db->beginTransaction();

    $QTrainerOrder                 = new Application_Model_TrainerOrder();
    $QTrainerOrderDetail           = new Application_Model_TrainerOrderDetail();
    $QTrainerOrderImei             = new Application_Model_TrainerOrderImei();
    $QTrainerInventoryAsset        = new Application_Model_TrainerInventoryAsset();
    $QTrainerInventoryHistoryAsset = new Application_Model_TrainerInventoryHistoryAsset();

    $QTrainerOrderOutDetail = new Application_Model_TrainerOrderOutDetail();

    if($this->getRequest()->getMethod() == 'POST')
    {
        $order_id     = $this->getRequest()->getParam('id');
        $currentTime  = date('Y-m-d H:i:s');
        $whereOrder   = array();
        $whereOrder[] = $QTrainerOrder->getAdapter()->quoteInto('id = ?',$order_id);
        $whereOrder[] = $QTrainerOrder->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
        $rowOrder     = $QTrainerOrder->fetchRow($whereOrder);

        if($rowOrder)
        {

            $params = array(
                'order_id' => $order_id
            );

            $list_order_imei = $QTrainerOrderImei->getListOrderImei($params);

            if($rowOrder['confirmed_at'])
                $flashMessenger->setNamespace('error')->addMessage('This SN has confirmed');
            /*
                        // lay sn detail
                        $whereOrderDetail   = array();
                        $whereOrderDetail[] = $QTrainerOrderDetail->getAdapter()->quoteInto('order_id = ?',$order_id);
                        $whereOrderDetail[] = $QTrainerOrderDetail->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
                        $rowsOrderDetail    = $QTrainerOrderDetail->fetchAll($whereOrderDetail);

                        foreach($rowsOrderDetail as $key => $value)
                        {

                            // kiem tra xem coi trong inventory no co chua neu co roi thi cong vao khong thi insert vao
                            $whereInventoryAsset = $QTrainerInventoryAsset->getAdapter()->quoteInto('asset_id = ?',$value['asset_id']);
                            $rowInventoryAsset   = $QTrainerInventoryAsset->fetchRow($whereInventoryAsset);

                            if($rowInventoryAsset)
                            {
                                // update + so luong
                                $newQuantity = intval($rowInventoryAsset['total_quantity']) +  intval($value['quantity']);
                                $dataUpdate= array(
                                    'total_quantity' => $newQuantity,
                                );
                                $where = $QTrainerInventoryAsset->getAdapter()->quoteInto('id = ?',$rowInventoryAsset->id);
                                $QTrainerInventoryAsset->update($dataUpdate,$whereID);

                                //  INVENTORY HISTORY GHI LẠI
                                $params = array(
                                    'asset_id'      => $rowInventoryAsset['asset_id'],
                                    'quantity_in'   => $value['quantity'],
                                    'ip'            => $ip,
                                    'userStorage'   => $userStorage,
                                    'time'          => $currentTime
                                );
                                $resultHistory = $QTrainerInventoryHistoryAsset->updateInventoryHistory($params);
                                if($resultHistory['code']!=0)
                                    throw new Exception($resultHistory['message']);
                            }
                            else
                            {
                                // insert -- hot cai dau ai cung chhe buon qua
                                $dataInsert = array(
                                    'asset_id'       => $value['asset_id'],
                                    'total_quantity' => $value['quantity']
                                );

                                //  INVENTORY HISTORY GHI LẠI
                                $params = array(
                                    'asset_id'      => $value['asset_id'],
                                    'quantity_in'   => $value['quantity'],
                                    'ip'            => $ip,
                                    'userStorage'   => $userStorage,
                                    'time'          => $currentTime
                                );

                                $resultHistory = $QTrainerInventoryHistoryAsset->updateInventoryHistory($params);

                                if($resultHistory['code']!=0)
                                    throw new Exception($resultHistory['message']);

                                $QTrainerInventoryAsset->insert($dataInsert);


                                // to do log
            //                    $info = array('INSERT_INVENTORY_ASSET','asset_id'=>$value['asset_id'],'value'=>array('new'=>$dataInsert));
            //                    $QLog->insert( array (
            //                        'info'          => json_encode($info),
            //                        'user_id'       => $userStorage->id,
            //                        'ip_address'    => $ip,
            //                        'time'          => $currentTime,
            //                    ) );

                            }

                        }
            */
            // bat dau update status cua sn do lai nha bồ ken

            foreach($list_order_imei as $key=>$value){
                $data_order_out = array(
                    'order_in_id' => $value['order_in_id'],
                    'asset_id'    => $value['asset_id'],
                    'quantity'    => 1,
                    'output_to_staff_id' => $value['output_to_staff_id'],
                    'note'        => $value['note'],
                    'type'        => $value['type'],
                    'imei'        => $value['imei'],
                    'type_order'  => 1,
                    'trainer_confirm' => ($value['output_to_staff_id'] == WAREHOUSE_TRAINER) ? $value['output_to_staff_id'] : null,
                    'trainer_confirm_at' => ($value['output_to_staff_id'] == WAREHOUSE_TRAINER) ? $currentTime : null
                );

                $QTrainerOrderOutDetail->insert($data_order_out);
            }

            $dataUpdateStatusSn = array(
                'status'      => 2,
                'confirmed_at'=> $currentTime,
                'confirmed_by'=> $userStorage->id
            );
            $QTrainerOrder->update($dataUpdateStatusSn,$whereOrder);
        }
        else
        {
            $flashMessenger->setNamespace('error')->addMessage('SN not found');
            $this->_redirect('/trainer/orders');
        }

        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Done');
        $this->_redirect('/trainer/orders');
    }
}
catch (Exception $e)
{
    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect('/trainer/orders');
}