<?php

$name             = $this->getRequest()->getParam('name');
$code             = $this->getRequest()->getParam('code');
$result                 = $this->getRequest()->getParam('result');
$area_id                = $this->getRequest()->getParam('area_id');
$is_approved             = $this->getRequest()->getParam('is_approved',NULL);
$has_image             = $this->getRequest()->getParam('has_image',NULL);

$userStorage            = Zend_Auth::getInstance()->getStorage()->read();

$QSpAssessmentApprove = new Application_Model_SpAssessmentApprove();
$QAsm = new Application_Model_Asm();
$QArea = new Application_Model_Area();

$params = array(
    'name'              			 => $name,
    'code'               			 => $code,
    'result'                   		 => $result,
    'area_id'                  		 => $area_id,
    'is_approved'              	     => $is_approved,
    'has_image'                  	 => $has_image,
);

$area = $QAsm->get_cache($userStorage->id);
$params['area_list'] = $area['area'];

$params['sort'] = $sort;
$params['desc'] = $desc;

$params['channel'] = [2363, 2316];

$page           = $this->getRequest()->getParam('page', 1);
$limit          = LIMITATION;
$total          = 0;

$result         = $QSpAssessmentApprove->fetchPagination($page, $limit, $total, $params);

$this->view->list    = $result;
$this->view->limit   = $limit;
$this->view->total   = $total;
$this->view->params  = $params;
$this->view->desc    = $desc;
$this->view->sort    = $sort;
$this->view->url     = HOST.'trainer/assessment-approve'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset  = $limit*($page-1);
$this->view->area_trainer = $area['area'];
$this->view->area = $QArea->get_cache();


$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;