<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$flashMessenger        = $this->_helper->flashMessenger;
$userStorage           = Zend_Auth::getInstance()->getStorage()->read();
$QLog                  = new Application_Model_Log();
$ip                    = $this->getRequest()->getServer('REMOTE_ADDR');
$db                    = Zend_Registry::get('db');
$back_url              = HOST.'trainer/course';
$QTrainerCourse        = new Application_Model_TrainerCourse();
$QTrainerCourseDetail  = new Application_Model_TrainerCourseDetail();
$QNewStaff             = new Application_Model_StaffTraining();
$cached_new_staff      = $QNewStaff->get_cache();
$QStaff                = new Application_Model_Staff();
$cached_staff          = $QStaff->get_cache();
$QStaffTraining        = new Application_Model_StaffTraining();
echo '<link href="/css/bootstrap.min.css" rel="stylesheet">';
if($this->getRequest()->getMethod()=='POST')
{
    try
    {
        $db->beginTransaction();
        $course_id       = $this->getRequest()->getParam('course_id');
        $ids             = $this->getRequest()->getParam('ids');
        $new_staff       = $this->getRequest()->getParam('new_staff');
        $cmnd            = $this->getRequest()->getParam('cmnd');
        $code            = $this->getRequest()->getParam('code');
        $firstname       = $this->getRequest()->getParam('firstname');
        $lastname        = $this->getRequest()->getParam('lastname');
        $team            = $this->getRequest()->getParam('team');
        $title           = $this->getRequest()->getParam('title');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $result          = $this->getRequest()->getParam('result');

        // check course
        if(!$course_id)
            throw new Exception('Can not found Course !');

        if(!count($ids))
            throw new Exception(" Can Not found Staff ");


        // check post len thieu cai nao la bao loi lien - cai noi
        if(!$new_staff || !$cmnd || !$code || !$firstname || !$lastname || !$team || !$title || !$regional_market)
        {
            throw new Exception('Input value not true !');
        }

        
        /* Gio lay ra nhung id dang dang ky khoa hoc*/
        $arrayStaffID                     = array();
        $arrayNewStaffID                  = array();
        $whereStaffTrainngCourseDetail    = array();
        $whereStaffTrainngCourseDetail[]  = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$course_id);
        $whereStaffTrainngCourseDetail[]  = $QTrainerCourseDetail->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
        $rowsStaffTrainingCourseDetail    = $QTrainerCourseDetail->fetchAll($whereStaffTrainngCourseDetail);

        if($rowsStaffTrainingCourseDetail->count())
        {
            // check coi khoa hoc do da lock chua - neu da lock roi ma post len du lieu nua thi loi lien 
            $totalStaffAssignCourse = count($rowsStaffTrainingCourseDetail);
            $totalStaffPost         = count($ids);
            $whereTrainerCourseLock = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$course_id);
            $rowCourseLock          = $QTrainerCourse->fetchRow($whereTrainerCourseLock);
            if($rowCourseLock)
            {
              $checked = $rowCourseLock['locked'];
              if(isset($checked) and $checked == 1)
              {
                if($totalStaffPost != $totalStaffAssignCourse)
                throw new Exception('Course Is Locked - Can not add Staff !');
              }
            }
            
            // roi minh chay binh thuong thoi      
            foreach ($rowsStaffTrainingCourseDetail as $key => $value) {
                if(isset($value['staff_id']) and $value['staff_id'])
                    $arrayStaffID[] = $value['staff_id'];
                elseif (isset($value['new_staff_id']) and $value['new_staff_id']) {
                    $arrayNewStaffID[] = $value['new_staff_id'];
                }
            }
        }

        // gio thang post len thang nao la thang moi - thang nao la thang cu.
        // array value nhan vien moi
        $arrayPostNewStaff = array(); // post len nhan vien moi da co id
        $arrrayPostStaff   = array(); // post len  nhan vien cu
        $arrayInfoNewStaff = array(); // nhan vien moi hoan toan
        if(is_array($ids))
        {
            foreach($ids as $key => $value)
            {
                if($new_staff[$key] == 1)
                {
                    // nhan vien moi da co id
                    if(isset($value) and $value)
                    {
                      $arrayPostNewStaff[] = $value;
                    }
                    else {
                      // nhan vien moi chua co id
                      $arrayInfoNewStaff[] = array(
                         'cmnd'           => $cmnd[$key],
                         'firstname'      => $firstname[$key],
                         'lastname'       => $lastname[$key],
                         'team'           => $team[$key],
                         'title'          => $title[$key],
                         'regional_market'=> $regional_market[$key]

                        );
                    }
                }
                elseif ($new_staff[$key] == 0) {
                    // nhan vien cu
                    $arrrayPostStaff[] = $value;
                }
            }
        }

        // minh so sanh coi nhung thang nao khong co trong nhung thang do
        $arrayDiffNewStaff = array_diff($arrayNewStaffID,$arrayPostNewStaff);
        $arrayDiffStaff    = array_diff($arrayStaffID,$arrrayPostStaff);

        // gio minh update nhung thang khac do thanh del => 1

        foreach ($arrayDiffNewStaff as $key => $value) {
            $whereDiffNewStaffCourseDetail   = array();
            $whereDiffNewStaffCourseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('new_staff_id = ?',$value);
            $whereDiffNewStaffCourseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$course_id);
            $rowDiffNewStaffCourseDetail     = $QTrainerCourseDetail->fetchRow($whereDiffNewStaffCourseDetail);
            if($rowDiffNewStaffCourseDetail)
            {
                $dataUpdateDiffNewStaff = array(
                        'del'=>1
                    );

                $QTrainerCourseDetail->update($dataUpdateDiffNewStaff,$whereDiffNewStaffCourseDetail);
                // to do log
                $info = array("UPDATE COURSE DETAIL",'new'=>$dataUpdateDiffNewStaff,'old'=>$rowDiffNewStaffCourseDetail);
                $QLog->insert( array (
                    'info'          => json_encode($info),
                    'user_id'       => $userStorage->id,
                    'ip_address'    => $ip,
                    'time'          => date('Y-m-d H:i:s'),
                ) );
            }
        }

        foreach ($arrayDiffStaff as $key => $value) {
            $whereDiffStaffCourseDetail   = array();
            $whereDiffStaffCourseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('staff_id = ?',$value);
            $whereDiffStaffCourseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$course_id);
            $rowDiffStaffCourseDetail     = $QTrainerCourseDetail->fetchRow($whereDiffStaffCourseDetail);
            if($rowDiffStaffCourseDetail)
            {
                $dataUpdateDiffStaff = array(
                        'del'=>1
                    );

                $QTrainerCourseDetail->update($dataUpdateDiffStaff,$whereDiffStaffCourseDetail);
                // to do log
                $info = array("UPDATE COURSE DETAIL",'new'=>$dataUpdateDiffStaff,'old'=>$rowDiffStaffCourseDetail);
                $QLog->insert( array (
                    'info'          => json_encode($info),
                    'user_id'       => $userStorage->id,
                    'ip_address'    => $ip,
                    'time'          => date('Y-m-d H:i:s'),
                ) );
            }
        }

        // luu nhung thang moi xuong
        $arraySaveNewStaffID     = array();
        $arraySaveStaffID        = array();
        $arrayDiffSaveNewStaffID = array_diff($arrayPostNewStaff, $arrayNewStaffID);
        $arrayDiffSaveStaffID    = array_diff($arrrayPostStaff, $arrayStaffID);

        foreach ($arrayDiffSaveNewStaffID as $key => $value)
        {
          if(!isset($cmnd[$key]) or !$cmnd[$key])
              throw new Exception('Can Not Save !');

          // thieu truong hop check no co chua
          $whereTrainingCouseDetailNewStaff     = array();
          $whereTrainingCouseDetailNewStaff[]   = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$course_id);
          $whereTrainingCouseDetailNewStaff[]   = $QTrainerCourseDetail->getAdapter()->quoteInto('new_staff_id = ?',$value);
          $rowTrainingCouseDetailNewStaff       = $QTrainerCourseDetail->fetchRow($whereTrainingCouseDetailNewStaff);

          if($rowTrainingCouseDetailNewStaff)
          {
              $columDel = $rowTrainingCouseDetailNewStaff['del'];

              if(isset($columDel) and $columDel)
              {
                  $dataUpdateDetailNewStaff = array(
                          'del'=>0
                      );

                  $QTrainerCourseDetail->update($dataUpdateDetailNewStaff,$whereTrainingCouseDetailNewStaff);
                  // to do log
                  $info = array("UPDATE COURSE DETAIL",'new'=>$dataUpdateDetailOldStaff,'old'=>$rowTrainingCouseDetailOldStaff);
                  $QLog->insert(
                  array (
                      'info'          => json_encode($info),
                      'user_id'       => $userStorage->id,
                      'ip_address'    => $ip,
                      'time'          => date('Y-m-d H:i:s'),
                  ) );

               }
             }
             else
             {
               // roi chung ta chen vao course detail
               $dataCourseDetailNewStaff = array(
                   'new_staff_id' => $value,
                   'course_id'    => $course_id
               );

               $QTrainerCourseDetail->insert($dataCourseDetailNewStaff);
               // to do log
               $info = array("INSERT COURSE DETAIL",'new'=>$dataCourseDetailNewStaff);
               $QLog->insert( array (
                   'info'          => json_encode($info),
                   'user_id'       => $userStorage->id,
                   'ip_address'    => $ip,
                   'time'          => date('Y-m-d H:i:s'),
               ) );
             }
        }

        // gio luu nhung tang cu xuong

        foreach ($arrayDiffSaveStaffID as $key => $value) {

            $whereTrainingCouseDetailOldStaff     = array();
            $whereTrainingCouseDetailOldStaff[]   = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$course_id);
            $whereTrainingCouseDetailOldStaff[]   = $QTrainerCourseDetail->getAdapter()->quoteInto('staff_id = ?',$value);
            $rowTrainingCouseDetailOldStaff       = $QTrainerCourseDetail->fetchRow($whereTrainingCouseDetailOldStaff);

            if($rowTrainingCouseDetailOldStaff)
            {
                $colDel = $rowTrainingCouseDetailOldStaff['del'];

                if(isset($colDel) and $colDel)
                {
                    $dataUpdateDetailOldStaff = array(
                            'del'=>0
                        );

                    $QTrainerCourseDetail->update($dataUpdateDetailOldStaff,$whereTrainingCouseDetailOldStaff);
                    // to do log
                    $info = array("UPDATE COURSE DETAIL",'new'=>$dataUpdateDetailOldStaff,'old'=>$rowTrainingCouseDetailOldStaff);
                    $QLog->insert( array (
                        'info'          => json_encode($info),
                        'user_id'       => $userStorage->id,
                        'ip_address'    => $ip,
                        'time'          => date('Y-m-d H:i:s'),
                    ) );
                    }
            }
            else
            {
                $dataCourseDetailStaff = array(
                    'staff_id'     => $value,
                    'course_id'    => $course_id
                );

                $QTrainerCourseDetail->insert($dataCourseDetailStaff);
                // to do log
                $info = array("INSERT COURSE DETAIL",'new'=>$dataCourseDetailNewStaffTraining);
                $QLog->insert( array (
                    'info'          => json_encode($info),
                    'user_id'       => $userStorage->id,
                    'ip_address'    => $ip,
                    'time'          => date('Y-m-d H:i:s'),
                ) );
            }
        }

        // dong thoi nhung thang moi post len chung ta phai luu xuong

        foreach ($arrayInfoNewStaff as $key => $value)
        {
            // chen vao cai noi
            // hinh nhu cai noi hom nay bi be - kho chiu qua
            $dataInsertNewStaffTraining = array(
                'cmnd'              => $value['cmnd'],
                'firstname'         => $value['firstname'],
                'lastname'          => $value['lastname'],
                'team'              => $value['team'],
                'title'             => $value['title'],
                'regional_market'   => $value['regional_market'],
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => $userStorage->id
            );

            $idInsertStaffTraining = $QStaffTraining->insert($dataInsertNewStaffTraining);
            // to do log
            $info = array("INSERT STAFF TRAINING",'new'=>$dataInsertNewStaffTraining);
            $QLog->insert( array (
                'info'          => json_encode($info),
                'user_id'       => $userStorage->id,
                'ip_address'    => $ip,
                'time'          => date('Y-m-d H:i:s'),
            ) );

            // roi chung ta chen vao course detail
            $dataCourseDetailNewStaffTraining = array(
                'new_staff_id' => $idInsertStaffTraining,
                'course_id'    => $course_id
            );

            $QTrainerCourseDetail->insert($dataCourseDetailNewStaffTraining);
            // to do log
            $info = array("INSERT COURSE DETAIL",'new'=>$dataCourseDetailNewStaffTraining);
            $QLog->insert( array (
                'info'          => json_encode($info),
                'user_id'       => $userStorage->id,
                'ip_address'    => $ip,
                'time'          => date('Y-m-d H:i:s'),
            ) );
        }

        // save xuong ket qua cua nhung thang do 
        if(is_array($ids) and isset($result) and count($result))
        {
            foreach ($ids as $key => $value) {

                if(!isset($value) OR !$value)
                {
                    throw new Exception("Can not found id for result ! ");                    
                }

                $whereUpdateResultCourseDetail   = array();
                $whereUpdateResultCourseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('course_id = ?',$course_id);

                if($new_staff[$key] == 1)
                {
                    
                    $whereUpdateResultCourseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('new_staff_id = ?',$value);
                    $rowFetch                        = $QTrainerCourseDetail->fetchRow($whereUpdateResultCourseDetail);
                    if($rowFetch)
                    {
                        $dataUpdate  = array('result' => $result[$key]);
                        $QTrainerCourseDetail->update($dataUpdate,$whereUpdateResultCourseDetail);
                        // to do log 
                        $info = array("UPDATE COURSE DETAIL",'new'=>$dataUpdate,'old'=>$rowFetch);
                        $QLog->insert( array (
                            'info'          => json_encode($info),
                            'user_id'       => $userStorage->id,
                            'ip_address'    => $ip,
                            'time'          => date('Y-m-d H:i:s'),
                        ) );

                    }

                }
                elseif ($new_staff[$key] == 0)
                {   
                    $whereUpdateResultCourseDetail[] = $QTrainerCourseDetail->getAdapter()->quoteInto('staff_id = ?',$value);
                    $rowFetchOLD                     = $QTrainerCourseDetail->fetchRow($whereUpdateResultCourseDetail);
                    if($rowFetchOLD)
                    {
                        $dataUpdate  = array('result' => $result[$key]);
                        $QTrainerCourseDetail->update($dataUpdate,$whereUpdateResultCourseDetail);
                        // to do log 
                        $info = array("UPDATE COURSE DETAIL",'new'=>$dataUpdate,'old'=>$rowFetchOLD);
                        $QLog->insert( array (
                            'info'          => json_encode($info),
                            'user_id'       => $userStorage->id,
                            'ip_address'    => $ip,
                            'time'          => date('Y-m-d H:i:s'),
                        ) );

                    }
                }

                


            }
        }

        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Done');
        echo '<script>setTimeout(function(){parent.location.href="/trainer/course"})</script>';
    }
    catch (Exception $e)
    {
        $db->rollback();
        echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
        echo '<script>window.parent.unblockUI();</script>';
        echo '<script>window.parent.scroll2Top();</script>';
        echo '<div class="alert alert-error">Failed - '.$e->getMessage().'</div>';
    }
}
