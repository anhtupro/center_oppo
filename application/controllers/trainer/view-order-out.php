<?php
$back_url                   = '/trainer/orders-out';
$this->view->back_url       = $back_url;
$flashMessenger             = $this->_helper->flashMessenger;
$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$whereTypeAsset             = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
$type_asset                 = $QTrainerTypeAsset->getAsset(array());
$arrayTypeAsset             = array();

$type = unserialize(CATEGORY_ASSET_TRAINER);
$this->view->type = $type;
$db = Zend_Registry::get('db');
$this->view->type_asset     = $type_asset;

$QTrainerOrderOut  = new Application_Model_TrainerOrderOut();
$this->view->list_trainer = $QTrainerOrderOut->getStaffTrainer();

try
{
    $id                     = $this->getRequest()->getParam('id');
    $QTrainerOrderOut       = new Application_Model_TrainerOrderOut();
    $QTrainerOrderOutDetail = new Application_Model_TrainerOrderOutDetail();
    $QTrainerOrderImei      = new Application_Model_TrainerOrderImei();
    if($id){
        $order = $QTrainerOrderOut->find($id)->current();
        $selectDetail = $db->select()
            ->from(array("p"=>'trainer_order_out_detail'),array('p.*'))
            ->where('p.del IS NULL OR p.del = 0')
            ->where('p.order_out_id = ?',$id);
        $orderDetail = $db->fetchAll($selectDetail);

        $listImei = array();
        foreach($orderDetail as $value):
            if($value['type'] == 1){
                $selectImei = $db->select()
                    ->from(array('p'=>'trainer_order_imei'),array('p.*'))
                    ->where('p.del IS NULL OR p.del = 0')
                    ->where('p.type = ?',TRAINER_ORDER_OUT)
                    ->where('p.order_detail_id = ?',$value['id']);
                $rows = $db->fetchAll($selectImei);
                $listImei[$value['id']] = $rows;
            }
        endforeach;
        $this->view->order = $order;
        $this->view->orderDetail = $orderDetail;
        $this->view->listImei = $listImei;
        $selectCreated = $db->select()
            ->from(array('staff'),array('CONCAT(firstname," ",lastname)'))
            ->where('id = ?',$order->created_by);
        $this->view->created_by = $db->fetchOne($selectCreated);

    }
}
catch (Exception $e)
{
    $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect('/trainer/orders-out');
}

