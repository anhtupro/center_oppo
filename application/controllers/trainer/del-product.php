<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$back_url                   = HOST.'trainer/list-product';

$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QLog               = new Application_Model_Log();
$ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
$db                 = Zend_Registry::get('db');

try
{

    $db->beginTransaction();

    $id               = $this->getRequest()->getParam('id');
    $QTrainerProduct  = new Application_Model_TrainerProduct();
    $whereID          = array();
    $whereID[]        = $QTrainerProduct->getAdapter()->quoteInto('id = ?',$id);
    $whereID[]        = $QTrainerProduct->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
    $rowID            = $QTrainerProduct->fetchRow($whereID);

    if($rowID)
    {
        $data = array(
            'updated_by' => $userStorage->id, 
            'updated_at' => date('Y-m-d H:i:s'),
            'del'=>1
        );
        $QTrainerProduct->update($data, $whereID);
    }

    $db->commit();
    $flashMessenger ->setNamespace('success')->addMessage('Done');
    $this->_redirect($back_url);
}
catch (Exception $e)
{
    $db->rollback();
    $flashMessenger ->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect( $back_url);
}
