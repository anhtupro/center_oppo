<?php
$inventory_id = $this->getRequest()->getParam('id');
$order_to = $this->getRequest()->getParam('order_to');
$note = $this->getRequest()->getParam('note');

$db = Zend_Registry::get('db');
$flashMessenger = $this->_helper->flashMessenger;
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$back_url = HOST.'trainer/trainer-take-asset';

$QTrainerOrder = new Application_Model_TrainerOrder();
$QInventory    = new Application_Model_TrainerInventory();

$where = $QInventory->getAdapter()->quoteInto('id = ?', $inventory_id);
$inventory = $QInventory->fetchRow($where);

try{
	if($inventory){
		$db->beginTransaction();

		$sn = date ('YmdHis') . substr ( microtime (), 2, 4 );
		$data = array(
			'sn'       => $sn,
			'asset_id' => $inventory['asset_id'],
			'date'     => date('Y-m-d'),
			'order_from' => $inventory['trainer_id'],
			'order_to' => $order_to,
			'object'   => $inventory['object'],
			'note'     => $note,
			'created_by' => $userStorage->id,
			'created_at' => date('Y-m-d H:i:s')
		);
		$id = $QTrainerOrder->insert($data);

		if($id){
			$data_inventory = array(
				'date_out' => date('Y-m-d H:i:s'),
				'status'   => 2
			);
			$QInventory->update($data_inventory, $where);
		}

		$db->commit();

		$flashMessenger->setNamespace('success')->addMessage('Done!');
		$this->redirect($back_url);

	}//inventory
}catch (Exception $e){
	$flashMessenger->setNamespace('success')->addMessage($e->getMessage());
	$this->redirect($back_url);
}