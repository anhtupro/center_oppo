<?php 
	$this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $userStorage        = Zend_Auth::getInstance()->getStorage()->read();
    if($this->getRequest()->getMethod() == 'POST')
    {
        $id = $this->getRequest()->getParam('id');

        $QTrainerDealer     = new Application_Model_TrainerDealer();
        $trainer_dealer     = $QTrainerDealer->get_cache();

        $QStaffTrainingReport     = new Application_Model_StaffTrainingReport();
        $whereStaffTrainingReport = $QStaffTrainingReport->getAdapter()->quoteInto('id = ?', $id);
        $row                      = $QStaffTrainingReport->fetchRow($whereStaffTrainingReport);
        if($row)
        {
            /* lay du lieu ve dc roi */
            $type = $row['type'];
            $data = '<table class="table table-responsive">';
            switch($type)
            {
                case TRAINING_REPORT_INTERNALLY :
                {
                    // internal thi date - area - province - note - quantity trainee
                    // date
                    $data = $data.'<tr><td>TYPE:</td><td>'.TRAINING_REPORT_INTERNALLY_NAME.'</td></tr>';
                    $date = $row['date'];
                    $data = $data.'<tr><td>Date:</td><td>'.$date.'</td></tr>';
                    // area
                    $QArea      = new Application_Model_Area();
                    $AreaCached = $QArea->get_cache();
                    $area = $AreaCached[$row['area']];
                    $data = $data.'<tr><td>Area:</td><td>'.$area.'</td></tr>';
                    // province
                    $QRegionalMarket                 = new Application_Model_RegionalMarket();
                    $getCachedProvinceRegionalMarket = $QRegionalMarket->nget_all_province_cache();
                    $province                        = $getCachedProvinceRegionalMarket[$row['province']];
                    $data = $data.'<tr><td>Province:</td><td>'.$province.'</td></tr>';
                    // note
                    $note = $row['note'];
                    $data = $data.'<tr><td>Note:</td><td>'.$note.'</td></tr>';
					$trainer_dealer_v = (isset($row['trainer_dealer']) and $row['trainer_dealer']) ? $trainer_dealer[$row['trainer_dealer']] : '';
                    $data = $data.'<tr><td>Dealer:</td><td>'.$trainer_dealer_v.'</td></tr>';
                    // quantity trainees
                    $quantity_trainees = $row['quantity_trainees'];
                    $data = $data.'<tr><td>Quantity Trainees:</td><td>'.$quantity_trainees.'</td></tr>';

                    break;

                }
                case TRAINING_REPORT_PARTNERS:
                {
                    // partners thi co date - dealer - store- note - quantity trainees
                    // date
                    $data = $data.'<tr><td>TYPE:</td><td>'.TRAINING_REPORT_PARTNERS_NAME.'</td></tr>';
                    $date = $row['date'];
                    $data = $data.'<tr><td>Date:</td><td>'.$date.'</td></tr>';
                    // dealer name
                    $QStaffTrainer               = new Application_Model_StaffTrainer();
                    $areaTrainer                 = $QStaffTrainer->getAreaTrainer($userStorage->id);
                    $regional_market             = $areaTrainer['district'];
                    $dealers                     = $QStaffTrainingReport->getDealerArea($regional_market);
                    $data = $data.'<tr><td>Số buổi học:</td><td>'.$row['quantity_session'].'</td></tr>';
                    // note
                    $note = $row['note'];
                    $data = $data.'<tr><td>Note:</td><td>'.$note.'</td></tr>';
					$trainer_dealer_v = (isset($trainer_dealer[$row['trainer_dealer']]) and $trainer_dealer[$row['trainer_dealer']]) ? $trainer_dealer[$row['trainer_dealer']] : '';
                    $data = $data.'<tr><td>Dealer:</td><td>'.$trainer_dealer_v.'</td></tr>';
                    // quantity trainees
                    $quantity_trainees = $row['quantity_trainees'];
                    $data = $data.'<tr><td>Quantity Trainees:</td><td>'.$quantity_trainees.'</td></tr>';
                    break;
                }
                case TRAINING_REPORT_NEW_STAFF:
                {
                    // voi new staff thi co from date - to date - Quantity Participant -  Quantity Disqualified - Quantity Remain
                    // from date
                    $data = $data.'<tr><td>TYPE:</td><td>'.TRAINING_REPORT_NEW_STAFF_NAME.'</td></tr>';
                    $from_date = $row['from_date'];
                    $data = $data.'<tr><td>From Date:</td><td>'.$from_date.'</td></tr>';
                    // to date
                    $to_date   = $row['to_date'];
                    $data = $data.'<tr><td>From Date:</td><td>'.$to_date.'</td></tr>';
                    // Quantity Participant
                    $quantity_participant = $row['quantity_participant'];
                    $data = $data.'<tr><td>Quantity Participant:</td><td>'.$quantity_participant.'</td></tr>';
                    //Quantity Disqualified
                    $quantity_disqualified = $row['quantity_disqualified'];
                    $data = $data.'<tr><td>Quantity Disqualified:</td><td>'.$quantity_disqualified.'</td></tr>';
                    // Quantity Remain
                    $quantity_remain = $row['quantity_remain'];
                    $data = $data.'<tr><td>Quantity Remain:</td><td>'.$quantity_remain.'</td></tr>';
                    break;
                }
            }

            // gio toi hinh anh ne -- cai nay moi cang ah
            $allPicture              = json_decode($row['picture'],true);
            // get image
            $direct                  = HOST.'photo/trainer/'.$allPicture['user_id'].'/'.$allPicture['direct'].'/';

            // get list picture training
            foreach($allPicture as $key => $value)
            {
                if(preg_match('/picture_list_pg/',$key))
                {
                    $arrayPictureList[$key] = $direct.$value;
                }
                if(preg_match('/picture_training/',$key))
                {
                    $arrayPictureTraining[$key] = $direct.$value;
                }
            }

            // dua data ra ban table --- picture list pg
            $data = $data.'<tr><td colspan="2" style="text-align:center"><b>Picture List PG/PB</b></td></tr>';
            $data = $data.'<tr><td colspan="2">';
            foreach($arrayPictureList as $k => $picture)
            {
                $data = $data.'<a data-featherlight="image" href="'.$picture.'"><img src="'.$picture.'" class="img-rounded" style="width:80px; height:80px; padding:10px;"/></a>';
            }
            $data = $data.'</td></tr>';
            // picture training
            $data = $data.'<tr><td colspan="2" style="text-align:center"><b>Picture Training</b></td></tr>';
            $data = $data.'<tr><td colspan="2">';
            foreach($arrayPictureTraining as $k => $picture)
            {
                $data = $data.'<a data-featherlight="image" href="'.$picture.'"><img src="'.$picture.'" class="img-rounded" style="width:80px; height:80px; padding:10px;"/></a>';
            }
            $data = $data.'</td></tr>';
            $data = $data.'</table>';
            echo $data;

        }

    }

?>