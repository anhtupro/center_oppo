<?php
$sort                   = $this->getRequest()->getParam('sort', '');
$desc                   = $this->getRequest()->getParam('desc', 1);
$asset_id               = $this->getRequest()->getParam('asset_id');
$page                   = $this->getRequest()->getParam('page', 1);
$export_imei            = $this->getRequest()->getParam('export_imei',0);
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();

$params = array(
    'asset_id'          => $asset_id
);

$params['sort'] = $sort;
$params['desc'] = $desc;

$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$QTrainerInventoryAsset     = new Application_Model_TrainerInventoryAsset();
$limit                      = LIMITATION;
$total                      = 0;

if($export_imei){
    $result = $QTrainerInventoryAsset->getStatusByImei();
    $this->_exportListByImei($result);
}
$result                     = $QTrainerInventoryAsset->fetchPagination($page, $limit, $total, $params);

$this->view->list    = $result;
$this->view->limit   = $limit;
$this->view->total   = $total;
$this->view->params  = $params;
$this->view->desc    = $desc;
$this->view->sort    = $sort;
$this->view->url     = HOST.'trainer/inventory-asset'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset  = $limit*($page-1);

$type_asset = $QTrainerTypeAsset->getAsset(array());
$this->view->type_asset     = $type_asset;

$type = unserialize(CATEGORY_ASSET_TRAINER);
$this->view->type = $type;

$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages       = $messages;
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;