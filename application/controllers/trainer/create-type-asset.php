<?php
/**
 * Created by PhpStorm.
 * User: hac
 * Date: 8/25/2015
 * Time: 12:29 AM
 */
$id = $this->getRequest()->getParam('id');
$QTrainerTypeAsset = new Application_Model_TrainerTypeAsset();
$QColor = new Application_Model_GoodColorCombined();
$QGood = new Application_Model_Good();

$goods = $QGood->get_cache2();
$this->view->goods = $goods;
if($id){
    $row = $QTrainerTypeAsset->find($id)->current();
    if($row){
        $this->view->row = $row;
        if($row['type'] == CATEGORY_ASSET_TRAINER_PHONE){
            $colors = $QColor->getColor($row['good_id']);
            $this->view->colors = $colors;
        }
    }
    $this->view->id = $id;
}else{
    $first = 0;
    foreach($goods as $key => $value):
        $first = $key;
        break;
    endforeach;
    $colors = $QColor->getColor($first);
    $this->view->colors = $colors;
}





