<?php
$flashMessenger     = $this->_helper->flashMessenger;
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();

$groupID = $userStorage->group_id;
$this->view->group_id = $groupID;

if($this->getRequest()->getMethod() == 'POST')
{
	$month  	= $this->getRequest()->getParam('month');
	$export 	= $this->getRequest()->getParam('export');
	$lock_by_hr = $this->getRequest()->getParam('lock_by_hr');
    $lock_by_timing = $this->getRequest()->getParam('lock_by_timing');

	// bat dau explode cai đó ra
	$explode = explode("-",$month);
	$_month  = $explode[0];
	$_year   = $explode[1];
	$user_id = $userStorage->id;

	

	try
	{
		if(!$_year || !$_month || !$user_id)
			throw new Exception(" Can not found Month Or Year Or User !");
        //lock timing HR
        if(isset($lock_by_timing) and $lock_by_timing)
        {
            $QLocing = new Application_Model_HrTimingLock();
            $currentTime = date('Y-m-d h:i:s');

            // kiểm tra xem da lock thang nay chua

            $whereLocking = array();
            $whereLocking[] = $QLocing->getAdapter()->quoteInto('month = ?' , $_month);
            $whereLocking[] = $QLocing->getAdapter()->quoteInto('year  = ?' , $_year);

            $locking        = $QLocing->fetchRow($whereLocking);

            if($locking)
            {
                throw new Exception("This month is lock , please try again !");
            }


            $dataLocking = array(
                'month' => $_month,
                'year'  => $_year,
                'created_at' => $currentTime,
                'created_by' => $userStorage->id,
                'locked_at'  => $currentTime,
                'locked_by'  => $userStorage->id
            );

            $QLocing->insert($dataLocking);
        }
        else
        {
            

            $db = Zend_Registry::get('db');

            $paramsSP = array(
                $_year ? $_year: NULL,
                $_month ? $_month: NULL,
                $user_id ? $user_id: NULL
            );

            if(isset($lock_by_hr) and $lock_by_hr == 1)
            {
                
                $stmlLockHR   = $db->prepare("CALL p_training_course_lock_by_hr(".($_year ? $_year : NULL).",".($_month ? $_month : NULL).",".($user_id).",".ENABLE_STORE_TRAINER_LOCK_BY_MONTH.")");
                $stmlLockHR->execute();
            }


            if(isset($lock_by_hr_timing) and $lock_by_hr_timing == 1)
            {
                $stmlLockHR   = $db->prepare("CALL p_training_course_lock_by_hr(".($_year ? $_year : NULL).",".($_month ? $_month : NULL).",".($user_id).",".ENABLE_STORE_TRAINER_LOCK_BY_MONTH.")");
                $stmlLockHR->execute();
            }


            // get coi data thang do tranfer chua
            $resultTranferLock 		  = null;
            $QTrainerCourseLock       = new Application_Model_TrainerCourseLock();
            $whereTrainerCourseLock   = array();
            $whereTrainerCourseLock[] = $QTrainerCourseLock->getAdapter()->quoteInto('month = ?',$_month);
            $whereTrainerCourseLock[] = $QTrainerCourseLock->getAdapter()->quoteInto('year = ?',$_year);
            $rowTranferLock 		  = $QTrainerCourseLock->fetchRow($whereTrainerCourseLock);

            if($rowTranferLock)
            {
                $resultTranferLock = $rowTranferLock['transfer_lock'];
            }


            $param = array(
                'month' 	=> $month,
                'lock_by_hr'=> $resultTranferLock ? $resultTranferLock : null
            );

            $this->view->param = $param;
          
            $stmt     = $db->query("CALL p_training_course_by_month(".($_year ? $_year : NULL).",".($_month ? $_month : NULL).",".($user_id).")");

            $result   = $stmt->fetchAll();

            foreach ($result as $key => $value) {
                unset($result[$key]['cmnd']);
            }

            $this->view->list = $result;
        }
      
       
       if(isset($export) and $export)
        {
            set_time_limit(0);
            ini_set('memory_limit', '5120M');
        
            require_once 'PHPExcel.php';
        
            $alphaExcel = new My_AlphaExcel();
        
            $PHPExcel = new PHPExcel();
            $headExcel =  $result[0];
           
            $heads = array();
            foreach ($headExcel as $k => $val){
                $heads[] = array(
                    $alphaExcel->ShowAndUp() => $k,
                );
            }
          
           
            $headsFix = array();
            foreach ($heads as $key => $value){
                $headsFix[array_keys($value)[0]] = array_values($value)[0];
                
            }
            
        
            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
           
            foreach($headsFix as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }
            $index = 1;
            foreach($result as $key => $value)
            {
                $alphaExcel = new My_AlphaExcel();
                foreach($headsFix as $k => $val)
                {
                   if($val != 'result'){
                       $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value[$val],PHPExcel_Cell_DataType::TYPE_STRING);
                   }else{
   			    	$arrayPUT =  $result[$key];
   			    	$text = '';
                       if($arrayPUT['result'] == RESULT_PASS_TRAINING_NEWSTAFF_COURSE){
                           $text = RESULT_PASS_TRAINING_NEWSTAFF_COURSE_NAME;
                       }elseif($arrayPUT['result'] == RESULT_FAIL_TRAINING_NEWSTAFF_COURSE){
                           $text = RESULT_FAIL_TRAINING_NEWSTAFF_COURSE_NAME;
                       }elseif($arrayPUT['result'] == RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE){
                           $text = RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE_NAME;
                       }
                       $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $text,PHPExcel_Cell_DataType::TYPE_STRING);
                       
                   }
                    
                }
                $index++;
            }
            $filename = 'Report Trainer Cousrse Month' . date('Ymd');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        
        
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
        
            exit;
        
        }
    //    if(isset($export) and $export)
//         {
//         	// bat dau xuat excel 
//         	$name = "Report Trainer Cousrse Month".date('Ymd').".csv";
// 	        $fp = fopen('php://output', 'w');

// 		      header('Content-Type: text/csv; charset=utf-8');
// 		      echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
// 		      header('Content-Disposition: attachment; filename='.$name);

// 			    $arrayPUT = array();
// 			    $k = 0;
// 			    foreach ($result as $key => $value) {
// 		    		$k = $k+1;
// 			    	if($k == 1)
// 			    	{

// 			    		fputcsv($fp,array_keys($result[$key]));
// 			    	}

// 			    	$arrayPUT =  $result[$key];
//                     if($arrayPUT['result'] == RESULT_PASS_TRAINING_NEWSTAFF_COURSE){
//                         $arrayPUT['result'] = RESULT_PASS_TRAINING_NEWSTAFF_COURSE_NAME;
//                     }elseif($arrayPUT['result'] == RESULT_FAIL_TRAINING_NEWSTAFF_COURSE){
//                         $arrayPUT['result'] = RESULT_FAIL_TRAINING_NEWSTAFF_COURSE_NAME;
//                     }elseif($arrayPUT['result'] == RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE){
//                         $arrayPUT['result'] = RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE_NAME;
//                     }
// 			    	fputcsv($fp, $arrayPUT);      
// 			    }

// 	        fclose($fp);
// 	        exit;
//         }
  			
	}
	catch(Exception $e)
	{
		echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
    	echo '<div class="alert alert-error">Failed - '.$e->getMessage().'</div>';
	}

}

