<?php

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$user_id        = $userStorage->id;

$cache = Zend_Registry::get('cache');
$cache->remove('staff_training_cache');
$cache->remove('staff_cache');

$QLog           				= new Application_Model_Log();
$ip             			    = $this->getRequest()->getServer('REMOTE_ADDR');

$flashMessenger                 = $this->_helper->flashMessenger;
$messages                       = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success   = $messages;
$messages                       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages           = $messages;


// date
$this->view->date     = date('Y-m-d');
$this->view->staff_id = $user_id;

// lay ra danh sách các course
$QTrainerCourse    = new Application_Model_TrainerCourse();
$QTrainerCourseType    = new Application_Model_TrainerCourseType();

$listCourse = $QTrainerCourse->getCourse(null,date('Y-m-d'));

$this->view->list_course = $listCourse;
$this->view->trainer_course_type = $QTrainerCourseType->get_cache();