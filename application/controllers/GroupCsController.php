<?php
class GroupCsController extends My_Controller_Action
{
    public function indexAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $limit = LIMITATION;
        $total = 0;

        $params = array();

        $QGroup = new Application_Model_GroupCs();
        $groups = $QGroup->fetchPagination($page, $limit, $total, $params);

        $this->view->groups = $groups;

        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST.'group-cs/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
    }

    public function createAction(){

        $QWS    = new Application_Model_WS();

        $acl    = $QWS->getListActions();
        
        if ($_GET['dev'] == 1) {
            echo "<pre>";
            var_dump($acl);
            exit;
        }


        $menus  = $QWS->getListMenus();

        $id = $this->getRequest()->getParam('id');

        $group_menus = null;

        if ($id) {
            $QGroup = new Application_Model_GroupCs();

            $groupRowset    = $QGroup->find($id);
            $group          = $groupRowset->current();

            $group_menus = $group->menu ? explode(',', $group->menu) : null;

            $this->view->group_accesses = json_decode($group->access);
            $this->view->group          = $group;
        }

        foreach ($menus as $menu)
            $this->add_row($menu['id'], $menu['parent_id'], $menu['title']);

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();

        $this->view->acl        = $acl;
        $this->view->menus      = $this->generate_list($group_menus);
        $this->view->messages   = $messages;
    }

    private function generate_list($group_menus) {
        return $this->ul(0, '', $group_menus);
    }

    function ul($parent = 0, $attr = '', $group_menus = null) {
        static $i = 1;
        $indent = str_repeat("\t\t", $i);
        if (isset($this->data[$parent])) {
            if ($attr) {
                $attr = ' ' . $attr;
            }
            $html = "\n$indent";
            $html .= "<ul$attr>";
            $i++;
            foreach ($this->data[$parent] as $row) {
                $child = $this->ul($row['id'], '', $group_menus);
                $html .= "\n\t$indent";
                $html .= '<li>';
                $html .= '<input value="'.$row['id'].'" '.( ($group_menus and in_array($row['id'] , $group_menus)) ? 'checked' : '' ).' type="checkbox" id="menus_'.$row['id'].'" name="menus[]"><label>'.$row['label'].'</label>';
                if ($child) {
                    $i--;
                    $html .= $child;
                    $html .= "\n\t$indent";
                }
                $html .= '</li>';
            }
            $html .= "\n$indent</ul>";
            return $html;
        } else {
            return false;
        }
    }

    private function add_row($id, $parent, $label) {
        $this->data[$parent][] = array('id' => $id, 'label' => $label);
    }

    public function saveAction(){

        if ($this->getRequest()->getMethod() == 'POST'){
            $group = new Application_Model_GroupCs();

            $id             = $this->getRequest()->getParam('id');
            $name           = $this->getRequest()->getParam('name');
            $default_page   = $this->getRequest()->getParam('default_page');
            $raw_access     = $this->getRequest()->getParam('access');
            $menus          = $this->getRequest()->getParam('menus');

            $access = array();
            if (is_array($raw_access)){
                foreach ($raw_access as $module=>$item)
                    foreach ($item as $controller=>$item_2)
                        foreach ($item_2 as $action=>$item_3){
                            if ($item_3)
                                $access[] = $module.'::'.$controller.'::'.$action;
                        }
            }

            $data = array(
                'name'          => $name,
                'default_page'  => $default_page,
                'menu'          => ( is_array($menus) ? implode(',', $menus) : null ),
                'access'        => json_encode($access),
            );

            if ($id){
                $where = $group->getAdapter()->quoteInto('id = ?', $id);

                $group->update($data, $where);

                My_Request::sync_table(array('table' => 'staff_cs_group', 'id' => $id, 'type' => 'update'));

            } else {
                $id = $group->insert($data);

                My_Request::sync_table(array('table' => 'staff_cs_group', 'id' => $id, 'type' => 'insert'));
            }

            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('Done!');
        }
        $this->_redirect(HOST.'group-cs');
    }

    public function delAction(){
        $id = $this->getRequest()->getParam('id');

        My_Request::sync_table(array('table' => 'staff_cs_group', 'id' => $id, 'type' => 'delete'));

        $group = new Application_Model_GroupCs();
        $where = $group->getAdapter()->quoteInto('id = ?', $id);
        $group->delete($where);

        $this->_redirect('/group-cs');
    }
}