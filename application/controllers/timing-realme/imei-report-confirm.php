<?php

/**
 * Bảng mã lỗi
 * 		các mã sau được trả về khi gọi function AJAX này
 * 		dùng để dịch ra ở client cho người ta dễ hiểu
 * @return  1 OK
 * @return  -100 Không phải truy vấn kiểu AJAX
 * @return  0 Thiếu thông tin
 * @return  -1 Không tồn tại thông báo trùng IMEI này
 * @return  -2 Thông báo này đã được xử lý
 * @return  -3 Bạn không có quyền sử dụng chức năng này
 * @return  -4 Không tìm thấy chấm công của người Bị báo cáo (chấm công không tồn tại hoặc đã bị xóa)
 * @return  -5 [description] chỗ này cần check lại, nhưng nếu dùng transaction thì ok, không lo bị nóng
 * @return  -6 Thiếu ID người được nhận
 * @return  -7 Không tìm thấy thông tin khách hàng của người Bị báo cáo
 * @return  -8 Không tìm thấy chấm công của người Bị báo cáo
 * @return  -9 Không tìm thấy thông tin người báo cáo
 * @return  -10 Không tìm thấy thông tin người Bị báo cáo
 * @return  -11 Không tìm thấy chấm công của người báo cáo
 * @return  -12 loi update data
 * @return  -1000 Chặn ASM báo cáo
 * @return  -9000 Éo biết vì sao lỗi (try-catch-exception)
 */
$this->_helper->viewRenderer->setNoRender(true);
$this->_helper->layout->disableLayout();

if (!$this->getRequest()->isXmlHttpRequest()) {
    exit('-100'); // not
}

$id        = $this->getRequest()->getParam('id');
$staff_win = $this->getRequest()->getParam('staff_win');
$note      = $this->getRequest()->getParam('note');
$now       = date("Y-m-d H:i:s");
if ($id) {
    // try {
    $QDuplicatedImei  = new Application_Model_DuplicatedImei();
    $QTiming          = new Application_Model_Timing();
    $QTimingLog       = new Application_Model_TimingLog();
    $QTimingSale      = new Application_Model_TimingSale();
    $QTimingSaleLog   = new Application_Model_TimingSaleLog();
    $QTimingSaleTrash = new Application_Model_TimingSaleTrash();

    // check $id
    $di = $QDuplicatedImei->find($id);
    $di = $di->current();

    if (!$di) { // không tồn tại dòng này
        echo '-1';
        exit;
    }

    if ($di['solved'] == 1) { // đã xử lý rồi mà -_-
        echo '-2';
        exit;
    }

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $group_id               = $userStorage->group_id;
    $to_insert_timing_sales = null;
    $to_delete_timing_sales = null;
    $flag                   = false;

    // chia thao tác: tùy sếp hay asm mà xử lý
    if ($group_id == BOARD_ID || $group_id == SALES_EXT_ID || $userStorage->id == SUPERADMIN_ID) {
        if (!$staff_win) { // thiếu thông tin
            echo '-6';
            exit;
        }

        $timing_sales_first = $QTimingSale->find($di['timing_sales_first']);
        $timing_sales_first = $timing_sales_first->current();

        if (!$timing_sales_first) {
            echo '-7';
            exit;
        }
        $timing_sales_first = $timing_sales_first->toArray();
        $timing_first       = $QTiming->find($timing_sales_first['timing_id']);
        $timing_first       = $timing_first->current();

        if (!$timing_first) {
            echo '-8';
            exit;
        }
        if ($staff_win == $di['staff_id']) {
            $is_insert_timing_sale_log  = FALSE;
            $is_updated_duplicate       = FALSE;
            // backup timing_sale to timing_sale_log
            $timing_sales_first['note'] = $userStorage->email . " solved duplicate imei at " . date("Y-m-d H:i:s");
            $is_insert_timing_sale_log  = $QTimingSaleLog->insert($timing_sales_first);
            if (!empty($is_insert_timing_sale_log)) {
                // update duplicate imei with log timing_sale id
                $where_update_duplicate = $QDuplicatedImei->getAdapter()->quoteInto('id = ?', $id);
                $data_update_duplicate  = [
                    'timing_sale_log_id' => $is_insert_timing_sale_log['id_main']
                ];
                $is_updated_duplicate   = $QDuplicatedImei->update($data_update_duplicate, $where_update_duplicate);

                if ($is_updated_duplicate) {
                    // xóa timing sales gốc
                    $where                  = $QTimingSale->getAdapter()->quoteInto('id = ?', $timing_sales_first['id']);
                    $to_delete_timing_sales = $QTimingSale->delete($where);
                    // get imei info
                    $model                  = $QDuplicatedImei->get_model($di['imei']);
                    // insert new timing vs timing sale
                    $data_timing            = [
                        'staff_id'    => $di['staff_id'],
                        'shift'       => 2,
                        'from'        => $di['date'],
                        'to'          => $di['date'],
                        'store'       => $di['store_id'],
                        'note'        => "Duplicated Inserted " . $now,
                        'status'      => 1,
                        'created_at'  => $di['date'],
                        'created_by'  => $userStorage->id,
                        'approved_at' => $di['date'],
                        'approved_by' => $userStorage->id
                    ];
                    $id_timing              = $QTiming->insert($data_timing);
                    if (!empty($id_timing)) {
                        $data_timing_sale = [
                            'product_id'    => $model['product_id'],
                            'model_id'      => $model['color_id'],
                            'customer_name' => $di['customer_name'],
                            'phone_number'  => $di['customer_phone'],
                            'address'       => $di['customer_address'],
                            'imei'          => $di['imei'],
                            'timing_id'     => $id_timing,
                        ];
                        $id_timing_sale   = $QTimingSale->insert($data_timing_sale);
                        $data             = array_filter(array(
                            'timing_sales'  => $timing_sale_id,
                            'staff_win'     => $staff_win,
                            'director_note' => $note,
                            'solved'        => 1,
                            'solved_at'     => date('Y-m-d H:i:s'),
                            'solved_by'     => $userStorage->id,
                        ));
                    } else {
                        echo '-12';
                        exit;
                    }
                } else {
                    echo '-12';
                    exit;
                }
            } else {
                echo '-12';
                exit;
            }
        } else {
            $data = array_filter(array(
                'staff_win'     => $staff_win,
                'director_note' => $note,
                'solved'        => 1,
                'solved_at'     => date('Y-m-d H:i:s'),
                'solved_by'     => $userStorage->id,
            ));
        }
    } elseif (in_array($group_id, array(ASM_ID, ASMSTANDBY_ID, SALES_ADMIN_ID))) {
        $data = array_filter(array(
            'asm_check' => 1,
            'asm_at'    => date('Y-m-d H:i:s'),
            'asm_id'    => $userStorage->id,
        ));
    } else {
        exit('-3');
    }

    $where = $QDuplicatedImei->getAdapter()->quoteInto('id = ?', $id);
    $QDuplicatedImei->update($data, $where);
    echo '1'; // update thành công
    exit;
}

echo '0'; // không có id lấy gì mà check

exit;
