<?php

$listname           = $this->getRequest()->getParam('pic');
$month          = $this->getRequest()->getParam('month',date('m/Y'));
//$listname = '70,6884';

if(!empty($listname) && !empty($month) ){    
    
    if (empty($month) || !date_create_from_format('m/Y', $month))
        $month = date('m/Y');
    
                
    $month_obj = date_create_from_format('m/Y', $month);
    
    $from_month_now = $month_obj->format('Y/m/01');
    $to_month_now = $month_obj->format('Y/m/t');
    
    
    $from_month_last = strtotime ( '-1 month' , strtotime ( $from_month_now ) ) ;
    $from_month_last = date ( 'Y-m-d' , $from_month_last );
    
    $to_month_last = strtotime ( '-1 day' , strtotime ( $from_month_now ) ) ;
    $to_month_last = date ( 'Y-m-d' , $to_month_last );

    $arr_staff_id = explode(',', $listname);
      
    $from_month_last =  date('Y-m-d', strtotime($from_month_last));  
    $to_month_last =  date('Y-m-d', strtotime($to_month_last));  
    $from_month_now =  date('Y-m-d', strtotime($from_month_now));  
    $to_month_now =  date('Y-m-d', strtotime($to_month_now));   
    
    $last_month =  $to_month_last;
    $last_month =  date('m/Y', strtotime($last_month)); 

    
    $QImeiKpi = new Application_Model_ImeiKpi();
    $ListSale = $QImeiKpi->getListSale($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now );
    $ListPG = $QImeiKpi->getListPG($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now );
    $ListPBSale = $QImeiKpi->getListPBSale($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now );
    $ListLeader = $QImeiKpi->getListLeader($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now );

    $this->view->ListSale   = $ListSale;
    $this->view->ListPG     = $ListPG;
    $this->view->ListPBSale   = $ListPBSale;
    $this->view->ListLeader   = $ListLeader;

    $this->view->arr_staff_id  = $arr_staff_id;
    
    
    $this->view->lastmonth = $last_month;
    
    $staff_name = $QImeiKpi->getListNameSalePG($listname);
    $this->view->staff_name = $staff_name;

} // end if
    $this->view->month     = $month;