<?php
$from            = $this->getRequest()->getParam('from');
$to              = $this->getRequest()->getParam('to');
$phone_number    = $this->getRequest()->getParam('phone_number');
$name            = $this->getRequest()->getParam('name');
$area            = $this->getRequest()->getParam('area');
$regional_market = $this->getRequest()->getParam('regional_market');
$staff_id = $this->getRequest()->getParam('staff_id');

$params = array(
    'from'            => $from,
    'to'              => $to,
    'phone_number'    => $phone_number,
    'name'            => $name,
    'area'            => $area,
    'regional_market' => $regional_market,
    'staff_id' => $staff_id,
);

$QTiming = new Application_Model_Timing();
$analytics = $count = 0;
$sales   = $QTiming->report_by_staff($params, $analytics, $count);
$this->view->sales = $sales;

$QStore = new Application_Model_Store();
$this->view->store_cached = $QStore->get_cache();

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setRender('partials/short_report');