<?php
$from_date       = $this->getRequest()->getParam('from_date', date('01/m/Y'));
$to_date         = $this->getRequest()->getParam('to_date', date('d/m/Y'));
$imei            = $this->getRequest()->getParam('imei');
$result          = $this->getRequest()->getParam('result', 3);
$export          = $this->getRequest()->getParam('export', 0);
$dev             = $this->getRequest()->getParam('dev');

$page = $this->getRequest()->getParam('page', 1);
$sort = $this->getRequest()->getParam('sort', 'p.created_at');
$desc = $this->getRequest()->getParam('desc', 1);

$this->view->desc        = $desc;
$this->view->current_col = $sort;
$limit = LIMITATION;
$total = 0;

$QArea = new Application_Model_Area();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$group_id = $userStorage->group_id;
$uid = $userStorage->id;

$params = array_filter(array(
    'imei'                  => $imei,
    'from_date'             => $from_date,
    'to_date'               => $to_date,
    'result'                => $result,
    'export'                => $export,
    'dev'                   => $dev,
));



$params['sort']   = $sort;
$params['desc']   = $desc;
$QDuplicatedImei  = new Application_Model_ImeiDuplicatedTgdd();
$dup_imeis        = $QDuplicatedImei->fetchPagination($page, $limit, $total, $params);


// if ($uid == 5899) {
// 	echo "<pre>";print_r($QDuplicatedImei->get_timing_sales_info_cache());die;
// }


//cap nhat status
if ($this->getRequest()->getMethod() == 'POST') {
    $dupId       = $this->getRequest()->getParam('dupId');
    $result       = $this->getRequest()->getParam('app-result');

    foreach ($dupId as $key => $value){
        $where = array();
        $where[] = $QDuplicatedImei->getAdapter()->quoteInto('id = ?', $value);
        $data = array(
            'result'     => $result,
            'solved_id'  =>$uid,
        );

        $QDuplicatedImei->update($data, $where);
    }
    
    $this->_flashMessenger->setNamespace('success')->addMessage('Done!');

    echo '<script>parent.location.href="' . '/timing/imei-duplicated-tgdd-approve'  . '"</script>';
    exit;
}


if(!empty($export)){

    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'STT',
        'Imei',
        'Staff Code',
        'Staff Name',
        'Store',
        'Customer Name',
        'Customer Phone',
        'Customer Address',
        'Note',
        'Created At',
        'Result',
        'Solved Code',
        'Solved Name'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;

    try {
        if ($dup_imeis)
            foreach ($dup_imeis as $_key => $_order) {

                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['staff_code'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['store_id'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['customer_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['customer_phone'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['customer_address'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['note'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['created_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['str_result'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['solved_code'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['solved_name'], PHPExcel_Cell_DataType::TYPE_STRING);


                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename  = 'Imei_Dupicated_TGDD_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;




}




$this->view->dup_imeis    = $dup_imeis;
$this->view->imei_model   = $imei_model;
$this->view->params       = $params;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->url          = HOST.'timing/imei-duplicated-tgdd-approve'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset       = $limit*($page-1);
$this->view->timing_sales = $timing_sales;

$this->view->user_group   = $group_id;
