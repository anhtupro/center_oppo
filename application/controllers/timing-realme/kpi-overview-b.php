<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!$userStorage || !isset($userStorage->id)) $this->_redirect(HOST);

$staff_id = $userStorage->id;
$from     = $this->getRequest()->getParam('from', date('01/m/Y'));
$to       = $this->getRequest()->getParam('to', date('d/m/Y'));

$QStaff = new Application_Model_Staff();
$this->view->staffs = $QStaff->get_all_cache();
$this->view->params = array(
    'from'     => $from,
    'to'       => $to,
    'staff_id' => $staff_id,
);

$where = $QStaff->getAdapter()->quoteInto('id = ?', intval($staff_id));
$staff  = $QStaff->fetchRow($where);

if (!$staff) return;

$sellout = My_Kpi::fetchGrid(
    $staff_id,
    date_create_from_format("d/m/Y", $from)->format("Y-m-d"),
    date_create_from_format("d/m/Y", $to)->format("Y-m-d")
);
$this->view->sellout = $sellout;


$QGood = new Application_Model_Good();
$this->view->goods = $QGood->get_cache();
$QGoodColor = new Application_Model_GoodColor();
$this->view->good_colors = $QGoodColor->get_cache();

$db = Zend_Registry::get('db');
$cols = array(
    'total'        => new Zend_Db_Expr('(COUNT(DISTINCT a.imei_sn))'),
    'total_active' => new Zend_Db_Expr('(SUM(CASE WHEN a.status = 1 AND DATE(a.activation_date) = DATE("2016-03-20") THEN 1 ELSE 0 END))'),
    'kpi_active'   => new Zend_Db_Expr(170000),
    'kpi_normal'   => new Zend_Db_Expr(70000)
);

$select_f1 = $db->select()
    ->from(array('a'=>'imei_kpi'),$cols)
    ->where('a.pg_id = ?',$staff_id)
    ->where('a.pg_id <> a.sale_id AND a.pg_id <> a.leader_id',0)

    //->where('a.status = 1')
    ->where('DATE(a.timing_date) = ?','2016-03-20')
    ->where('a.good_id = ?',402)
    ->group('a.pg_id');
;
//echo $select_f1;
//exit;
$sellout_f1 = $db->fetchRow($select_f1);
$this->view->sellout_f1 = $sellout_f1;