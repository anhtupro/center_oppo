<?php

if (defined("LOCK_TIMING") && LOCK_TIMING) {
    $this->_helper->viewRenderer->setRender('lock');
    return;
}

$id          = $this->getRequest()->getParam('id');
$is_rm       = $this->getRequest()->getParam('is_rm', 0);
$QTiming     = new Application_Model_Timing();
$QTime       = new Application_Model_Time();
$QTimingSale = new Application_Model_TimingSale();
$QTimingSaleDetail = new Application_Model_TimingSaleDetail();
$installment_model = new Application_Model_ImeiInstallment();
if ($is_rm == 1) {

    $QTiming     = $QTime       = new Application_Model_TimingRm();
    $QTimingSale = new Application_Model_TimingSaleRm();
}
$timingRowset = $QTiming->find($id);
$timing       = $timingRowset->current();

$flashMessenger = $this->_helper->flashMessenger;

if (!$timing) {
    $flashMessenger->setNamespace('error')->addMessage('Invalid Report!');
    $this->_redirect('/timing');
}

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

//check timing if not approved yet can edit
if ($timing->approved_by) {
    $flashMessenger->setNamespace('error')->addMessage('This Report was approved already!');
    $this->_redirect('/timing');
}

//check quyền view
//check có phải là quản lý của store
if ($userStorage->id != $timing->staff_id and $userStorage->group_id != ADMINISTRATOR_ID) {
    $QStoreStaffLog = new Application_Model_StoreStaffLog();
    $QLeaderLog     = new Application_Model_StoreLeaderLog();

    if (!$QStoreStaffLog->belong_to($userStorage->id, $timing['store'], $timing['from'], true)) {

        if ($userStorage->group_id == SALES_ID)
            $salesTeamErr = 'Bạn không phải người quản lý của Store!';
        elseif ($userStorage->group_id == LEADER_ID && $QLeaderLog->is_leader($userStorage->id, $timing['store'], $timing['from']))
            $salesTeamErr = 'Bạn không phải người quản lý của Store. Bạn chỉ được xem, không được sửa.';
        else
            $salesTeamErr = 'Bạn không phải người quản lý của Store.';


        $flashMessenger->setNamespace('error')->addMessage($salesTeamErr);
        $back_url = $this->getRequest()->getServer('HTTP_REFERER');
        $this->_redirect(($back_url ? $back_url : '/timing'));
    }
}

$date             = explode(' ', $timing->from);
$this->view->from = substr($date[1], 0, -3);
$tem              = explode('-', $date[0]);
$this->view->date = $tem[2] . '/' . $tem[1] . '/' . $tem[0];
$to               = explode(' ', $timing->to);
$this->view->to   = substr($to[1], 0, -3);

$dateObj = new DateTime($timing->from);


$this->view->timing = $timing;

$QShift             = new Application_Model_Shift();
$_shifts            = $QShift->get_cache();
$this->view->shifts = $_shifts;

// lấy shift từ chấm công 
// viết cho nó chạy thôi, ai thích từ optimize lại nhóe
$where_time[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $timing['staff_id']);
$where_time[] = $QTime->getAdapter()->quoteInto('DAY(created_at) = ?', $dateObj->format('d'));
$where_time[] = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $dateObj->format('m'));
$where_time[] = $QTime->getAdapter()->quoteInto('YEAR(created_at) = ?', $dateObj->format('Y'));
$resultTiming = $QTime->fetchRow($where_time);
if (isset($resultTiming) and $resultTiming) {
    //tạm thời dùng cho sale
    $shift                    = $resultTiming['shift'];
    $this->view->shiftTiming  = $_shifts[$shift];
    $this->view->resultTiming = $resultTiming->toArray();
}


//get staff info
$QStaff            = new Application_Model_Staff();
$where             = $QStaff->getAdapter()->quoteInto('id = ?', $timing->staff_id);
$this->view->staff = $QStaff->fetchRow($where);

$where        = $QTimingSale->getAdapter()->quoteInto('timing_id = ?', $id);
$timing_sales = $QTimingSale->fetchAll($where);

$QGoodColor         = new Application_Model_GoodColor();
$QGoodColorCombined = new Application_Model_GoodColorCombined();
$data               = array();
$QTimingFileWarranty = new Application_Model_TimingFileWarranty();
if ($timing_sales->count()) {
    foreach ($timing_sales as $item) {
        
        
        //timing sale detail
       $where = $QTimingSaleDetail->getAdapter()->quoteInto('timing_sale_id = ?', $item->id);
       $timing_sales_detail = $QTimingSaleDetail->fetchAll($where);
       $timing_sales_detail = $timing_sales_detail->toArray();
       
       //timing file warranty
       $where = $QTimingFileWarranty->getAdapter()->quoteInto('timing_sale_id = ?', $item->id);
       $timing_file_warranty = $QTimingFileWarranty->fetchAll($where);
       $timing_file_warranty = $timing_file_warranty->toArray();
        
        $where             = $QGoodColorCombined->getAdapter()->quoteInto('good_id = ?', $item->product_id);
        $goodColorCombined = $QGoodColorCombined->fetchAll($where);

        $goodColor = $temp      = array();
        foreach ($goodColorCombined as $jd)
            $temp[]    = $jd->good_color_id;

        if ($temp) {
            $where     = $QGoodColor->getAdapter()->quoteInto('id IN (?)', $temp);
            $goodColor = $QGoodColor->fetchAll($where, 'name');
        }

        $where_installment = $installment_model->getAdapter()->quoteInto('timing_sale_id = ?', $item->id);
        $installment_info  = $installment_model->fetchRow($where_installment);
  
        if (!empty($installment_info)) {
            $installment_info = $installment_info->toArray();
            
        }
        $data_temp = array(
            'id'            => $item->id,
            'photo'         => $item->photo,
            'product_id'    => $item->product_id,
            'model_id'      => $item->model_id,
            'models'        => $goodColor->toArray(),
            'customer_name' => $item->customer_name,
            'phone_number'  => $item->phone_number,
            'address'       => $item->address,
            'imei'          => $item->imei,
            'timing_sale_detail' => $timing_sales_detail,
            'amortization'  => $item->amortization,
            'timing_file_warranty' => $timing_file_warranty,
        );
        if (!empty($installment_info)) {
            $data_temp['installment_id']       = $installment_info['id'];
            $data_temp['installment_company']  = $installment_info['installment_company'];
            $data_temp['installment_contract'] = $installment_info['contract_number'];
        }
        $data[]=$data_temp;
//        echo "<pre>";
//        print_r($data);
//        die;
       
    }
}

$this->view->timing_sales = $data;

//timing accessories
$QTimingAccessories             = new Application_Model_TimingAccessories();
$where                          = $QTimingAccessories->getAdapter()->quoteInto('timing_id = ?', $id);
$timing_accessories             = $QTimingAccessories->fetchAll($where);
$this->view->timing_accessories = $timing_accessories;

//get accessories
$QAccessories            = new Application_Model_Good();
$where                   = $QAccessories->getAdapter()->quoteInto('cat_id = ?', ACCESS_CAT_ID);
$Accessories             = $QAccessories->fetchAll($where, 'desc');
$this->view->accessories = $Accessories;

//get product
$QGood             = new Application_Model_Good();
$where             = $QGood->getAdapter()->quoteInto('cat_id = ?', PHONE_CAT_ID);
$goods             = $QGood->fetchAll($where, 'desc');
$this->view->goods = $goods;

// load all model
$QGoodColor = new Application_Model_GoodColor();
$result     = $QGoodColor->fetchAll();

$data = null;
if ($result->count()) {
    foreach ($goods as $good) {
        $colors = explode(',', $good->color);

        $temp = array();
        foreach ($result as $item) {
            if (in_array($item['id'], $colors)) {
                $temp[] = array(
                    'id'    => $item->id,
                    'model' => $item->name,
                );
            }
        }
        $data[$good['id']] = $temp;
    }
}
$this->view->good_colors = json_encode($data);


//get store
$QStore         = new Application_Model_Store();
$QStoreStaffLog = new Application_Model_StoreStaffLog();
$staff_id_edit  = false;
if ($id && isset($timing['staff_id']))
    $staff_id_edit  = $timing['staff_id'];
else
    $staff_id_edit  = $userStorage->id;

$store_id_list = $QStoreStaffLog->get_stores($staff_id_edit, date('Y-m-d', strtotime($timing['from'])));
$where         = array();

if (is_array($store_id_list) && count($store_id_list) > 0)
    $where[] = $QStore->getAdapter()->quoteInto('id IN (?)', $store_id_list);
elseif (!in_array($userStorage->group_id, array(ADMINISTRATOR_ID)))
    $where[] = $QStore->getAdapter()->quoteInto('1=0', 1);

$this->view->stores = $QStore->fetchAll($where, 'name');
//

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$this->_helper->viewRenderer->setRender('create');
