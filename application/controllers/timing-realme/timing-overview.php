<?php
// exit('Chức năng tạm thời bảo trì, vui lòng quay lại sau');
$params = $this->_request->getParams();

$limit = 30;
$page = $this->getRequest()->getParam('page', 1);
$total = 0;

$from = $this->getRequest()->getParam('from');
$to = $this->getRequest()->getParam('to');
$dev = $this->getRequest()->getParam('dev');
$imei_sn = $this->getRequest()->getParam('imei_sn');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if(empty($from) && empty($to))
{
	$from = date("01/m/Y");
	$to = date("d/m/Y");
}

$params = array_filter(array(
	'from' => $from,
	'to' => $to,
	'dev' => $dev,
    'imei_sn' => TRIM($imei_sn)
));

//$QTimingSale = new Application_Model_TimingSale();
// $data = $QTimingSale->selectKpiOverview($page, $limit, $total, $params);
$QTimingSale = new Application_Model_TimingSaleRealme();

if (in_array($userStorage->title, [SALES_TITLE])) {
    $data_sale = $QTimingSale->selectKpiOverviewSale($page, $limit, $total, $params);
} else {
    $data_pg = $QTimingSale->selectKpiOverview($page, $limit, $total, $params);
}

$this->view->data_pg = $data_pg;
$this->view->data_sale = $data_sale;
$this->view->userStorage = $userStorage;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST.'timing-realme/timing-overview'.( $params ? '?'.http_build_query($params).'&' : '?' );

$this->view->params = $params;