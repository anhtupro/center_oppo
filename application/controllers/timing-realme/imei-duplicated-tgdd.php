<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$ImeiDuplicatedTgdd = new Application_Model_ImeiDuplicatedTgdd();
$staff_id = $userStorage->id;

$db = Zend_Registry::get('db');
if ($this->getRequest()->getMethod() == 'POST') {
    set_time_limit(0);

    $date = $this->getRequest()->getParam('date');
    $store = $this->getRequest()->getParam('store');
    $note = $this->getRequest()->getParam('note');
    $imeis = $this->getRequest()->getParam('imei');
    $customer_names = $this->getRequest()->getParam('customer_name');
    $customer_phone = $this->getRequest()->getParam('customer_phone');
    $customer_address = $this->getRequest()->getParam('customer_address');
    $warranty_file = $this->getRequest()->getParam('warranty_file_src');


    //kiem tra imei co ton tai hay khong trong warehouse
    $errorImei='';
    if (isset($imeis) && $imeis) {
        foreach ($imeis as $key => $value) {
            if (!$this->_checkImeiWarehouse($value)){
                $errorImei=$errorImei." ".$value;
            }
        }
        if(!empty($errorImei)){
            echo '<script>
                parent.alert("Imei '.$errorImei.' không hợp lệ");
            </script>';
            exit();
        }
    }

    //Kiem tra imei nay da duoc bao lan truoc hay chua
    $errorImei='';
    $QImei              = new Application_Model_ImeiDuplicatedTgdd();
    if (isset($imeis) && $imeis) {
        foreach ($imeis as $key => $value) {
            $where=array();
            $where[] = $QImei->getAdapter()->quoteInto('imei = ?', $value);
            $where[] = $QImei->getAdapter()->quoteInto('staff_id = ?', $staff_id);

            $data = $QImei->fetchRow($where);

            if(count($data)>0){
                $errorImei=$errorImei." ".$value;
            }

        }
        if(!empty($errorImei)){
            echo '<script>
                parent.alert("Imei '.$errorImei.'đã được bạn báo rồi, xin kiểm tra lại");
            </script>';
            exit();
        }
    }





    //khong cho bao ngay som hon hien tai
    foreach ($date as $key=>$value){

        $temp = explode('/', $date[$key]);

        //khong cho bao som hon ngay hien tai
        if (strtotime($temp[2] . '-' . $temp[1] . '-' . $temp[0]) > strtotime(date('Y-m-d'))) {
            echo '<script>
                parent.alert("Bạn không thể báo cáo trước ngày hiện tại! Vui lòng xem lại ngày.");
            </script>';
            exit;
        }

        // khong cho bao so tu cu hon 2 thang
        if (strtotime($temp[2] . '-' . $temp[1] . '-' . $temp[0])+strtotime("+ 2 months") < strtotime(date('Y-m-d'))) {
            echo '<script>
                parent.alert("Bạn không thể báo cáo số vào thời gian lớn hơn 2 tháng trước! Vui lòng xem lại ngày.");
            </script>';
            exit;
        }
    }

    if (isset($imeis) && $imeis) {
        foreach ($imeis as $key => $value) {
        //xu li hinh anh
            $img_warranty='';

            if($warranty_file[$key]&& !empty($warranty_file[$key]) ){

                $img_warranty = str_replace('data:image/jpeg;base64,', '', $warranty_file[$key]);
                $img_warranty = str_replace(' ', '+', $img_warranty);
                $data = base64_decode($img_warranty);
                $name_warranty = "image_warranty_"."_". time().".jpg";
                $file1 = APPLICATION_PATH . "/../public/photo/imei-duplicated-tgdd/" . $name_warranty;
                $success = file_put_contents($file1, $data);
                if($success){
                    $img_warranty=LINK_WARRANTY . $name_warranty;
                }
            }

            $data = array(
                'imei' => $value,
                'staff_id' => $staff_id,
                'store_id' => intval($store[$key]),
                'customer_name' => ($customer_names[$key]),
                'customer_phone' => $customer_phone[$key],
                'customer_address' => trim($customer_address[$key]),
                'img_warranty' => $img_warranty,
                'note' => trim($note[$key]),
                'created_at' => $this->_formatDate($date[$key]),
            );
            $ImeiDuplicatedTgdd->insert($data);
        }
    }

//    $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');
//

    $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');

    echo '<script>parent.location.href="' . '/timing/imei-duplicated-tgdd'  . '"</script>';
    exit;
}






