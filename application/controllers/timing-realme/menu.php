<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$lang = $userStorage->defaut_language;
$id = $this->getRequest()->getParam('id');
//lay menu trong group
if (isset($userStorage->menu) && $userStorage->menu) {
    $groupMenu = $userStorage->menu;

} else {

    $QMenu = new Application_Model_Menu();
    $where = $QMenu->getAdapter()->quoteInto('group_id = ?', $group_id);
    $menu = $QMenu->fetchAll($where, array('parent_id', 'position'));

}
//lay menu trong staff privielege

$QMenu2 = new Application_Model_Menu2();
$staffMenu = $QMenu2->getMenuPermission();

//lay menu cap 2
$db          = Zend_Registry::get('db');

$select = $db->select()->from(array('m' => 'menu'), array(
    'id'=>'m.id'
));

$select->joinLeft(array('m1' => 'menu'), 'm1.parent_id = m.id', array())
    ->where('m.parent_id =?',$id)
    ->group('m.id');

$secondLevelMenuId = $db->fetchAll($select);
//lay ra danh sach ID
$listId=array();
foreach ($secondLevelMenuId as $Id){
    $listId[]=$Id['id'];
}


$this->view->groupMenu=$groupMenu;
$this->view->listId=$listId;
$this->view->staffMenu=$staffMenu;
$this->view->lang=$lang;




