<?php

set_time_limit(0);

$page               = $this->getRequest()->getParam('page', 1);
$sort               = $this->getRequest()->getParam('sort', 'product_count');
$desc               = $this->getRequest()->getParam('desc', 1);
$from               = $this->getRequest()->getParam('from', date('01/m/Y'));
$to                 = $this->getRequest()->getParam('to', date('d/m/Y'));
$name               = $this->getRequest()->getParam('name');
$search             = $this->getRequest()->getParam('search', 1);
$area               = $this->getRequest()->getParam('area');
$province           = $this->getRequest()->getParam('province');
$district           = $this->getRequest()->getParam('district');
$store              = $this->getRequest()->getParam('store');
$has_pg             = $this->getRequest()->getParam('has_pg');
$sales_from         = $this->getRequest()->getParam('sales_from');
$sales_to           = $this->getRequest()->getParam('sales_to');
$distributor_id     = $this->getRequest()->getParam('distributor_id');
$store_level        = $this->getRequest()->getParam('store_level');
$channel            = $this->getRequest()->getParam('channel', -1);
$goods              = $this->getRequest()->getParam('goods');
$distributor_ka     = $this->getRequest()->getParam('distributor_ka');
$include_0_sell_out = $this->getRequest()->getParam('include_0_sell_out');
$export_ka          = $this->getRequest()->getParam('export_ka');
$QStoreStaffLog     = new Application_Model_StoreStaffLog();
$QGood              = new Application_Model_Good();

if (!empty(date_create_from_format("d/m/Y", $from))) {
//    $from_date = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
} else {
    $from = date('01/m/Y');
}
if (!empty(date_create_from_format("d/m/Y", $to))) {
//    $to = date_create_from_format("d/m/Y", $to)->format("Y-m-d");
} else {
    $to = date('d/m/Y');
}
$from_date = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
$to_date = date_create_from_format("d/m/Y", $to)->format("Y-m-d");
$dateDiff = My_Date::date_diff($from_date, $to_date);

if ($dateDiff > 92) {
    echo '<script>
            alert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 90 ngày.");
            palert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 90 ngày.");
        </script>';
    exit;
}
$params = array(
    'page'               => $page,
    'sort'               => $sort,
    'desc'               => $desc,
    'from'               => $from,
    'to'                 => $to,
    'name'               => $name,
    'area'               => $area,
    'province'           => $province,
    'district'           => $district,
    'store'              => $store,
    'has_pg'             => $has_pg,
    'sales_from'         => $sales_from,
    'sales_to'           => $sales_to,
    'distributor_id'     => $distributor_id,
    'store_level'        => $store_level,
    'channel'            => $channel,
    'goods'              => $goods,
    'search'             => $search,
    'include_0_sell_out' => $include_0_sell_out,
    'distributor_ka'     => $distributor_ka
);

$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$params['title'] = $userStorage->title;
//if(!empty(date_create_from_format("d/m/Y", $from))){
//    $dateFrom =  date_create_from_format("d/m/Y", $from)->format("Y-m-d");
//}else{
//    $dateFrom=date('01/m/Y');
//}
//if(!empty(date_create_from_format("d/m/Y", $to))){
//$dateTo   =  date_create_from_format("d/m/Y", $to)->format("Y-m-d");
//} else {
//    $dateTo=date('d/m/Y');
//}
//$dateDiff = My_Date::date_diff($dateFrom,$dateTo);
//
//if($dateDiff > 92 ){
//    echo '<script>
//            alert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 60 ngày.");
//            palert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 60 ngày.");
//        </script>';
//    exit;
//}

if (!$userStorage || !isset($userStorage->id))
    $this->_redirect(HOST);
$group_id = $userStorage->group_id;


// if(empty($_GET['dev']) && $userStorage->id != 341 && $userStorage->id != 5899)
//     exit('Chức năng này tạm khóa');
if (in_array($userStorage->title, array(418, 412)) || $userStorage->id == 910 || $userStorage->id == 4266) {
    // BRAND SHOP MANAGER
    $params['is_brand_shop'] = 1;
}

$QArea = new Application_Model_Area();
$areas = $QArea->get_cache();

$QRegionalMarket = new Application_Model_RegionalMarket();

if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm                = new Application_Model_Asm();
    $asm_cache           = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();
} elseif ($userStorage->title == SALES_TITLE) {
    // lấy cửa hàng của sale


    $store_cache = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));



    $params['store_list'] = $store_cache;
    $params['sale_id']    = $userStorage->id;
} elseif ($userStorage->title == LEADER_TITLE) {
    // lấy cửa hàng của sale
    $QStoreLeaderLog    = new Application_Model_StoreLeaderLog();
    $store_cache_leader = $QStoreLeaderLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $store_cache = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
    $store_list  = array_merge($store_cache_leader, $store_cache);

    $params['store_list'] = $store_list;
    $params['leader_id']  = $userStorage->id;
} elseif ($userStorage->title == 424) {

    $QStoreStaffLog        = new Application_Model_StoreStaffLog();
    $store_cache_sales_brs = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $store_list = $store_cache_sales_brs;

    $params['store_list'] = $store_list;
} elseif ($userStorage->title == 418) {

    $QStoreStaffLog        = new Application_Model_StoreStaffLog();
    $store_cache_sales_brs = $QStoreStaffLog->get_stores_brandshop($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $store_list           = $store_cache_sales_brs;
    $params['store_list'] = $store_list;
}

if ($area) {
    $where                        = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

if ($province) {
    $where                 = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $province);
    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}

if ($district) {
    //get store
    $QStore             = new Application_Model_Store();
    $where              = $QStore->getAdapter()->quoteInto('district = ?', $district);
    $this->view->stores = $QStore->fetchAll($where, 'name');
}

if (in_array($userStorage->group_id, array(KA_TEAM)) && !in_array($userStorage->code, array(14110288))) {
    $params['is_ka_team']   = $userStorage->id;
    $this->view->is_ka_team = $userStorage->id;

    $QStaffChannel = new Application_Model_StaffChannel();
    $name_chart    = $QStaffChannel->get_staff_channel($userStorage->id);
    $channel       = array();
    foreach ($name_chart as $key => $value) {
        $channel[] = $key;
    }
    $params['channel_ka'] = $channel;
}

$limit  = LIMITATION;
$total  = $total2 = 0;

$QImeiKpi = new Application_Model_ImeiKpi();

if (!empty($export_ka)) {
    $data = $QImeiKpi->getAreaKaGood($params);
    $this->_exportAreaKaGood($data);
}

if (isset($search) && $search) {

    $sales = $QImeiKpi->fetchStore($page, $limit, $total, $params);

    $params['get_total_sales'] = true;
    $total_sales               = $QImeiKpi->fetchStore(null, null, $total2, $params);

    //Lấy S.O 6 tháng gần nhất
    $list_store = [];
    foreach ($sales as $key => $value) {
        $list_store[] = $value['store_id'];
    }

    $QKpiMonth      = new Application_Model_KpiMonth();
    $sell_out_month = $QKpiMonth->getSelloutMonthStore($list_store);
    //END
}
unset($params['get_total_sales']);
unset($params['asm']);
unset($params['store_list']);
unset($params['area_list']);
unset($params['sale_id']);
unset($params['leader_id']);
unset($params['title']);
unset($params['is_ka_team']);

$QDistributor                  = new Application_Model_Distributor();
$this->view->distributor_cache = $QDistributor->get_cache();
//echo "<pre>";print_r($this->view->distributor_cache);die;

$QLoyaltyPlan                 = new Application_Model_LoyaltyPlan();
$store_level_list             = $QLoyaltyPlan->get_cache();
$this->view->store_level_list = $store_level_list;

$channel_list = array(
    1 => 'KA',
    0 => 'IND'
);



$this->view->channel_list = $channel_list;
if (isset($search) && $search) {
    $this->view->total_quantity  = $total_sales['total_quantity'];
    $this->view->total_activated = $total_sales['total_activated'];
    $this->view->sales           = $sales;
}

$month6 = intval(date('m', strtotime(date('Y-m') . " -6 month")));
$month5 = intval(date('m', strtotime(date('Y-m') . " -5 month")));
$month4 = intval(date('m', strtotime(date('Y-m') . " -4 month")));
$month3 = intval(date('m', strtotime(date('Y-m') . " -3 month")));
$month2 = intval(date('m', strtotime(date('Y-m') . " -2 month")));
$month1 = intval(date('m', strtotime(date('Y-m') . " -1 month")));
$month0 = intval(date('m', strtotime(date('Y-m') . " 0 month")));

$list_month = array(
    $month1,
    $month2,
    $month3,
    $month4,
    $month5,
    $month6
);

$this->view->list_month     = $list_month;
$this->view->sell_out_month = $sell_out_month;
$this->view->offset         = $limit * ($page - 1);
$this->view->total          = $total;
$this->view->limit          = $limit;
$this->view->url            = HOST . 'timing/analytics-store' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->desc           = $desc;
$this->view->current_col    = $sort;
$this->view->to             = $to;
$this->view->from           = $from;
$this->view->areas          = $areas;
$this->view->params         = $params;
$this->view->goods          = $QGood->get_cache();
$this->view->distributor_ka = $QDistributor->getChannelKa($params);
$this->view->title          = $userStorage->title;

// ADD
// Search Distributor Using Ajax

if ($this->getRequest()->isXmlHttpRequest()) {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $SearchBox = $this->getRequest()->getParam('txt');

    //echo $SearchBox;die;
    //echo "<pre>";print_r($param);die;

    $db = Zend_Registry::get('db');

    $sql  = 'SELECT id,title FROM warehouse.distributor WHERE title LIKE "%' . $SearchBox . '%"';
    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll();
    $stmt->closeCursor();
    $db   = $stmt = null;

    $this->view->dealer_ajax = $data;
    $this->render('partials/search-dealer-ajax');
    // exit();
    // echo "<pre>";print_r($data);die;
}


