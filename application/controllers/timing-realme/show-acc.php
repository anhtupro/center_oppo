<?php

$area            = $this->getRequest()->getParam('area_id');
$from            = $this->getRequest()->getParam('from', date('01/m/Y') );
$to              = $this->getRequest()->getParam('to', date('d/m/Y') );

$params = array(
    'area' => $area,
    'from' => $from,
    'to'   => $to
);

$QMarket         = new Application_Model_Market();
$data = $QMarket->showSellInByArea($params);
$this->view->data = $data;

$this->_helper->layout()->disablelayout(true);
?>