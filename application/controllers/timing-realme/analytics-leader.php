<?php

set_time_limit(0);

$sort        = $this->getRequest()->getParam('sort');
$desc        = $this->getRequest()->getParam('desc', 1);
$export      = $this->getRequest()->getParam('export', 0);
$from        = $this->getRequest()->getParam('from', date('01/m/Y'));
$to          = $this->getRequest()->getParam('to', date('d/m/Y'));
$area        = $this->getRequest()->getParam('area',NULL);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$db          = Zend_Registry::get('db');
$list_area   = [];
if (!in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '14.161.22.196', '112.109.92.6', '115.78.166.171'))) {
//    echo "Chức năng đang được bảo trì.";
//    exit;
}


$QArea      = new Application_Model_Area();
$area_cache = $QArea->get_cache();
$QAsm       = new Application_Model_Asm();
$asm_cache  = $QAsm->get_cache();
$params     = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'area'   => $area,
    'export' => $export,
    'title'  => $title
);
// echo "<pre>";print_r($params);die;
// Xuáº¥t excel
if (isset($export) && $export) {
    $this->_forward('leader', 'sales-report', null, array('params' => $params));
    return;
}
$area_list = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();

$area_array      = [$area];
if (!empty($area_list)) {
  
    if (!empty($area)) {
        $filter_area = array_intersect($area_list, $area_array);
    } else {
        $filter_area = $area_list;
    }
} else {
    if (!empty($area)) {
        $filter_area = $area_array;
    } else {
        $filter_area = $area_list;
    }
}

$from_f     = date_create_from_format("d/m/Y", $from)->format("Y-m-d 00:00:00");
$to_f       = date_create_from_format("d/m/Y", $to)->format("Y-m-d 23:59:59");
$staff_id   = $userStorage->title == LEADER_TITLE ? $userStorage->id : -1;
$sql_params = [
    $from_f,
    $to_f,
    implode($filter_area, ","),
    $staff_id
];

$sql        = 'CALL sp_report_leader_asm(?,?,?,?,@total_value)';
$stmt       = $db->query($sql, $sql_params);
$list       = $stmt->fetchAll();

$stmt->closeCursor();
$total = $db->fetchOne('select @total_value');
$this->view->total_value        = $total;

$this->view->sales       = $list;
$this->view->total_value = $total;



if (empty($area_list)) {
    if (My_Staff_Permission_Area::view_all($userStorage->id) || in_array($userStorage->id, array(341,24125))) {
        $list_area = $area_cache;
    }
} else {
    foreach ($area_list as $key => $value) {
        $list_area[$value] = $area_cache[$value];
    }
}

$this->view->params = $params;
$this->view->areas  = $list_area;
$this->view->url    = HOST . 'timing/analytics-area' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->to     = $to;
$this->view->from   = $from;
$this->view->userStorage =$userStorage;

//if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
//    
//}
//
//if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
//    $area_list                  = array();
//    $list_regions               = $QAsm->get_cache($userStorage->id);
//    $list_regions               = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
//    $this->view->viewed_area_id = $list_regions;
//}
//
//// echo "<pre>";print_r($list_regions);die;
//
//$QArea                   = new Application_Model_Area();
//$QImeiKpi                = new Application_Model_ImeiKpi();
//$this->view->userStorage = $userStorage;
//
//$data     = $QImeiKpi->fetchLeader($params);
//$data_asm = $QImeiKpi->fetchRegionShareAsm($params);
//
//$params['get_total_sales'] = true;
//
//
//
//
//$params_area = array(
//    'from' => $from,
//    'to'   => $to,
//);
//
//$params_area['get_total_sales'] = true;
//$total_sales                    = $QImeiKpi->fetchArea($params_area);
//
//// echo "<pre>";print_r($total_sales);die;
////get total money
//$total_money     = $total_sales['total_value'];
//$sales           = array();
//$data_area_sum   = array();
//$data_asm_region = array();
//
//if ($userStorage->id == 5899) {
////     echo "<pre>";print_r($total_money);
//}
//
//foreach ($data_asm as $asm_region) {
//    $data_asm_region[$asm_region['sales_region_id']] = $asm_region;
//}
//
//
//// echo "<pre>";print_r($asm_region);die;
////tÃ­nh point
//// if($userStorage->id == 5899){
////     echo "<pre>";print_r($QImeiKpi->getValue80(null,null,$params));die;
//// }
//
//foreach ($data as $item) {
//    $val = $item;
//    if ($item['type'] == 1 AND isset($data_asm_region[$item['region_id']])) {
//        $val['total_quantity']  = $data_asm_region[$item['region_id']]['total_quantity'];
//        $val['total_activated'] = $data_asm_region[$item['region_id']]['total_activated'];
//        $val['total_value']     = $data_asm_region[$item['region_id']]['total_value'];
//    }
//
//    if (empty($item['staff_to_date'])) {
//        $point = ( $total_money > 0 and ( $val ['region_shared'] / 100) > 0 ) ?
//                round(($val ['total_value'] / $total_money) * 60 / ($val ['region_shared'] / 100), 2) : 0;
//    } else {
//        $total_money_from_to = $QImeiKpi->getValue80($item['staff_from_date'], $item['staff_to_date'], $params);
//        $point               = round(($val ['total_value'] / $total_money_from_to) * 60 / ($val ['region_shared'] / 100), 2);
//    }
//
//    $val['point'] = $point;
//    // $val['total_value_from_to'] = $QImeiKpi->getValue80($item['staff_from_date'],$item['staff_to_date'],$params);
//    $sales[]      = $val;
//    if ($userStorage->id == 5899) {
////     echo "<pre>";print_r($val);die;
//    }
//}
//
//// if($userStorage->id == 5899){
////     echo "<pre>";print_r($sales);die;
//// }
//
//unset($params['kpi']);
//unset($params['get_total_sales']);
//
//$this->view->areas                 = $QArea->get_cache();
//$this->view->total_sales           = $total_sales['total_quantity'];
//$this->view->total_sales_activated = $total_sales['total_activated'];
//$this->view->sales                 = $sales;
//$this->view->url                   = HOST . 'timing/analytics-area' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
//$this->view->desc                  = $desc;
//$this->view->current_col           = $sort;
//$this->view->to                    = $to;
//$this->view->from                  = $from;
//$this->view->params                = $params;
//$this->view->data_area_sum         = $data_area_sum;

