<?php

$id          = $this->getRequest()->getParam('id');
$is_rm       = $this->getRequest()->getParam('is_rm', 0);
$Timing      = new Application_Model_TimingRealme();
$QTimingSale = new Application_Model_TimingSaleRealme();
if ($is_rm == 1) {

    $QTiming =$Timing    = new Application_Model_TimingRm();
    $QTimingSale = new Application_Model_TimingSaleRm();
}
try {
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $user_id     = $userStorage->id;
    if (!$id)
        $this->_redirect(HOST . 'timing');
    $id          = intval($id);

    // chỉ sales mới đc approve (trừ admin)
    if (!in_array($userStorage->group_id, array(SALES_ID, LEADER_ID, ASM_ID, ASMSTANDBY_ID, PB_SALES_ID)) && $userStorage->group_id != ADMINISTRATOR_ID && $userStorage->group_id != SUPERADMIN_ID) {
        echo '-69';
        exit;
    }

    // $QTime = new Application_Model_Time();
    // $check_status = $QTime->checkInStatus(intval($user_id));
    //chua checkin thi k dc approve
    // if(!$check_status)
    // {
    //    echo '-79';
    //    exit;
    // }


    $where = $Timing->getAdapter()->quoteInto('id = ?', $id);
    $item  = $Timing->fetchRow($where);
    $from = $item['from'];
    //se bo comment sau
   // $time_limit_timing = 86400 * IMEI_APPROVE_TIMING_EXPIRE;

    $time_limit_timing = 86400 * 100;

    // if (!My_Staff_Title::isPg($userStorage->title)) {
    //     if ((date('d') > TIME_LIMIT_TIMING_SALE || date('d') == 1))
    //         $time_limit_timing = TIME_LIMIT_TIMING_SALE;
    //     else
    //         $time_limit_timing = 86400 * IMEI_APPROVE_TIMING_EXPIRE;
    // }
    //prevent approve after 02 day
    if (strtotime(date('Y-m-d')) - strtotime(date('Y-m-d', strtotime($from))) > $time_limit_timing && $userStorage->group_id != ASM_ID) {
        echo 2;
        exit;
        /*
          $happyTime = defined('HAPPY_TIME') ? unserialize(HAPPY_TIME) : null;
          $currentTime = date('Y-m-d H:i:s');
          if (
          !
          (
          $happyTime
          and
          $currentTime >= $happyTime['editFrom']
          and
          $currentTime <= $happyTime['editTo']
          and
          $from >= $happyTime['from']
          and
          $from <= $happyTime['to']
          )
          ){
          echo 2;
          exit;
          }
         */
    }

    if (!$item) {
        echo 3;
        exit;
    } else {
        // check có phải là quản lý của store
        // tại ngày chấm công của timing
        /* $QStoreStaffLog = new Application_Model_StoreStaffLog();
          $QStoreLeaderLog = new Application_Model_StoreLeaderLog();

          // co phai quan ly cua hang
          $isSaleOfStore = $QStoreStaffLog->belong_to($userStorage->id, $item['store'], $item['from'], true);

          // co phai leader cua hang
          $isLeaderOfStore = $QStoreLeaderLog->is_leader($userStorage->id, $item['store'], $item['from']);

          if (
          ! $isSaleOfStore // neu khong phai sale
          and ! $isLeaderOfStore // va khong phai leader thi phan
          ) {
          echo 4;
          exit;
          }
         */
    }

    $data = array(
        'status'      => 1,
        'approved_at' => date('Y-m-d H:i:s'),
        'approved_by' => $userStorage->id,
    );
    $Timing->update($data, $where);

    $paramsTime = array(
        'shift'         => $item['shift'],
        'timing_at'     => $item['from'],
        'status'        => 1,
        'id'            => $item['staff_id'],
        'approved_at'   => date('Y-m-d H:i:s'),
        'approved_by'   => $userStorage->id,
        'check_approve' => 1
    );

    $where = $QTimingSale->getAdapter()->quoteInto('timing_id = ?', $item['id']);
    $ts    = $QTimingSale->fetchAll($where);

    $imei_string = '';
    $imei_arr    = [];

//    if ($ts)
//        foreach ($ts as $key => $value)
//            $imei_arr[] = $value['imei'];
//
//    if ($imei_arr && count($imei_arr)) {
//        $imei_string = implode(',', $imei_arr);
//        $db = Zend_Registry::get('db');
//        $sql = "call sp_imei_kpi_approve(?)";
//        $db->query($sql, array($imei_string));
//    }
    // $resultTime = $QTime->saveApi($paramsTime);
//    if($resultTime['code'] != 1)
//    {
//          echo '-89';
//          exit;
//    }


    echo 0;
    exit;
} catch (Exception $e) {

    echo $e->getMessage();
    exit;
}