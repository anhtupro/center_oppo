<?php 

set_time_limit(0);
error_reporting(0);
ini_set('display_error', 0);
$filename = 'IMEIs Checking - '.date('d-m-Y H-i-s');
// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename.'.csv');
echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
$output = fopen('php://output', 'w');

$head = array(
    'No.',
    'IMEI',
    'DEALER',
    'STORE',
    'SELL OUT DATE',
    'ACTIVATED_AT'
    );

fputcsv($output, $head);
if(is_array($imeis) and count($imeis) > 0){
    $str_imeis = implode(',',$imeis);
}

$sql = "
    SELECT a.imei_sn, c.`name` store_name, DATE_FORMAT(b.timing_date,'%Y-%m-%d') sellout_date, 
    DATE_FORMAT(a.activated_date,'%Y-%m-%d') activated_date, d.title as dealer_name
    FROM warehouse.imei a
    LEFT JOIN imei_kpi b ON a.imei_sn = b.imei_sn
    LEFT JOIN store c ON c.id = b.store_id
    LEFT JOIN warehouse.distributor d ON d.id = a.distributor_id
    WHERE a.imei_sn IN (".$str_imeis.")
    ORDER BY d.title ASC;
";

$sql_not_exist = "
    SELECT a.imei_sn
    FROM warehouse.imei a
    WHERE a.imei_sn  IN (".$str_imeis.")
    ORDER BY d.title ASC;
";

$db     = Zend_Registry::get('db');
$stmt   = $db->query($sql);
$result = $stmt->fetchAll();

$i      = 0;
foreach($result as $item){
    $row = array();
    $i++;
    $row[] = $i;
    $row[] = $item['imei_sn'];
    $row[] = $item['dealer_name'];
    $row[] = $item['store_name'];
    $row[] = $item['sellout_date'];
    $row[] = $item['activated_date'];
    fputcsv($output, $row);
}

exit;

?>