<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QTiming = new Application_Model_Timing();

if ($this->getRequest()->getMethod() == 'POST'){
    $id = $this->getRequest()->getParam('id');
    $timing_sales_ids = $this->getRequest()->getParam('timing_sales_ids');

    //validate image
    $upload = new Zend_File_Transfer_Adapter_Http();
    $upload->addValidator('Size', false, 10*1024*1024);
    $upload->addValidator('Extension', false, array('jpg', 'jpeg', 'gif', 'png'));

    $photos = $upload->getFileInfo();
    $photos_key = is_array($photos) ? array_keys($photos) : null;
    $photos = is_array($photos) ? array_values($photos) : null;
    if ($photos){
        if (!$upload->isValid()){
            echo '<script>
                            parent.palert("Please input valid file.");
                        </script>';
            exit;
        }
    }

    $data = array();

    if ($id){
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $userStorage->id;

        $where = $QTiming->getAdapter()->quoteInto('id = ?', $id);
        $QTiming->update($data, $where);

        $QLog = new Application_Model_Log();
        $ts = "";
        if (isset($timing_sales_ids)) {
            foreach ($timing_sales_ids as $k => $v) {
                $ts .= $v . ",";
            }

            $ts = trim($ts, ",");
        }

        $ts = trim($ts, ",");
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = "TIMING Photo - Update (".$id.") - Timing sales (".$ts.")";
        //todo log
        $QLog->insert( array (
            'info' => $info,
            'user_id' => $userStorage->id,
            'ip_address' => $ip,
            'time' => date('Y-m-d H:i:s'),
        ) );
    }

    //insert timing sale
    $QTimingSale = new Application_Model_TimingSale();

    foreach ($timing_sales_ids as $k=>$timing_sales_id){

        if (isset($photos[$k]) and $photos[$k] and $timing_sales_id ) {

            $uploaded_dir = APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'photo'.DIRECTORY_SEPARATOR.'timing_sales'.DIRECTORY_SEPARATOR.$timing_sales_id;
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $upload->setDestination($uploaded_dir);

            try {
                $upload->receive($photos_key[$k]);
                $file_name = $photos[$k]['name'];
                $data      = array('photo' => $file_name);
                $where = $QTimingSale->getAdapter()->quoteInto('id = ?', $timing_sales_id);
                $QTimingSale->update($data, $where);
            } catch (Zend_File_Transfer_Exception $e) {
                echo '<script>
                            parent.palert("Please input valid file.");
                        </script>';
                exit;
            }
        }
    }

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');

}

$back_url = $this->getRequest()->getParam('back_url');

echo '<script>parent.location.href="'.( $back_url ? $back_url : '/timing' ).'"</script>';
exit;