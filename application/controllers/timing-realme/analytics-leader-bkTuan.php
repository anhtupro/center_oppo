<?php
$sort        = $this->getRequest()->getParam('sort');
$desc        = $this->getRequest()->getParam('desc', 1);
$export      = $this->getRequest()->getParam('export', 0);
$from        = $this->getRequest()->getParam('from', date('01/m/Y') );
$to          = $this->getRequest()->getParam('to', date('d/m/Y') );
$area        = $this->getRequest()->getParam('area');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$title       = $this->getRequest()->getParam('title');
$params = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'area'   => $area,
    'export' => $export,
    'title'  => $title
);

// Xuất excel
if ( isset($export) && $export ) {
    $this->_forward('leader', 'sales-report', null, array('params' => $params));
    return;
}

if ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    
    $area_list    = array();
    $QAsm         = new Application_Model_Asm();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();

    //danh sách khu vực được xem
    $this->view->viewed_area_id = $list_regions;
    
}

$QArea                   = new Application_Model_Area();
$QImeiKpi                = new Application_Model_ImeiKpi();
$this->view->userStorage = $userStorage;
$data                    = $QImeiKpi->fetchLeader($params);
$data_asm                = $QImeiKpi->fetchRegionShareAsm($params);

$params['get_total_sales'] = true;
$params_area = array(
    'from'   => $from,
    'to'     => $to
);

$params_area['get_total_sales'] = true;
$total_sales                    = $QImeiKpi->fetchArea($params_area);

//get total money
$total_money     = $total_sales['total_value'];
$sales           = array();
$data_area_sum   = array();
$data_asm_region = array();


foreach($data_asm as $asm_region){
    $data_asm_region[$asm_region['sales_region_id']] = $asm_region;
}

// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($data_asm_region);
//     exit;
// }

//tính point
foreach($data as $item){
    $val = $item;
    if($item['type'] == 1 AND isset($data_asm_region[$item['region_id']])) {
        $val['total_quantity']  = $data_asm_region[ $item['region_id'] ]['total_quantity'];
        $val['total_activated'] = $data_asm_region[ $item['region_id'] ]['total_activated'];
        $val['total_value']     = $data_asm_region[ $item['region_id'] ]['total_value'];
    }

    $point        = ( $total_money > 0 and ($val ['region_shared']/100) > 0 ) ?  
                        round ( ($val ['total_value']/$total_money) * 60 / ($val ['region_shared']/100), 2 ) : 0;    
    $val['point'] = $point;
    $sales[]      = $val;
}

// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($val);
//     exit;
// }

unset($params['kpi']);
unset($params['get_total_sales']);

$this->view->areas                 = $QArea->get_cache();
$this->view->total_sales           = $total_sales['total_quantity'];
$this->view->total_sales_activated = $total_sales['total_activated'];
$this->view->sales                 = $sales;
$this->view->url                   = HOST.'timing/analytics-area'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->desc                  = $desc;
$this->view->current_col           = $sort;
$this->view->to                    = $to;
$this->view->from                  = $from;
$this->view->params                = $params;
$this->view->data_area_sum         = $data_area_sum;

