<?php

$this->_helper->layout->disableLayout();
$timing_sales_id = $this->getRequest()->getParam('timing_sales_id');
$is_check = $this->getRequest()->getParam('is_check', 0);
$export = $this->getRequest()->getParam('export', 0);

if ($is_check) {
    $imei = trim($this->getRequest()->getParam('value', ''));
    $imei = explode("\n", $imei);

} else {
    $imei = $this->getRequest()->getParam('value');
}

if (!defined("IMEI_ACTIVATION_EXPIRE"))
    define("IMEI_ACTIVATION_EXPIRE", 3);

if($is_check){
    $imei = trim($this->getRequest()->getParam('value', null));

    if($imei){
        $imei = $str = preg_replace('/^[ \t]*[\r\n]+/m', '', $imei);
        $imei = explode("\n", $imei);
//        $imei = '('.implode('),(', $imei).')';
//        $imei = preg_replace("/[^0-9,\(\)]/", null, $imei);

  //      $imei = "'" .implode("', '",$imei). "'" ;



        $db = Zend_Registry::get('db');
        $db->query('
            DROP  TABLE IF EXISTS _check_imei_info;

            CREATE TABLE _check_imei_info(
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                imei_sn varchar(40) NOT NULL
            ) ENGINE=INNODB DEFAULT CHARSET UTF8 COLLATE UTF8_UNICODE_CI
        ');

        try{
            $insertImei= array();

            for($i=0;$i<count($imei);$i++){
                $tempImei      = array(
                    'imei_sn'     => $imei[$i]
                );
                $insertImei[]=$tempImei;
            }



            My_Controller_Action::insertAllrow($insertImei, '_check_imei_info');

        } catch(Exception $e){
            $db = null;
            echo -1;
            exit;
        }

        $data = $db->query('
            SELECT
			t.staff_id pg_id,
			s.`code` pg_code,
			ci.imei_sn imei_sn,
			CONCAT(s.firstname," ",s.lastname) pg_name,
			s.email pg_email,
			st.`name` store_name,
			t.`from` timing_date,
			wi.activated_date activated_date,
			wi.out_date out_date,
			IFNULL(wi.imei_sn, 0) imei_status,
			s.phone_number pg_phone_number,
			ts.customer_name customer_name,
			ts.phone_number customer_phone_number,
			ts.address customer_address,
			null customer_email,
			g.name product_code,
			g.desc product_name,
			t.created_at,
			ida.note note,
						CONCAT(si1.firstname," ",si1.lastname) consutant_kpi,
                        CONCAT(si.firstname," ",si.lastname) pg_kpi,
                         CONCAT(ssi.firstname," ",ssi.lastname) sale_kpi,
                         sti.`name` store_name_kpi,
                         tsl.note log_note,
                         lk.note lock_note
		FROM _check_imei_info ci
		LEFT JOIN timing_sale ts ON ci.imei_sn = ts.imei
		LEFT JOIN timing t ON t.id = ts.timing_id
		LEFT JOIN staff s ON t.staff_id = s.id
		LEFT JOIN store st ON t.store = st.id
                LEFT JOIN imei_kpi  i ON i.imei_sn COLLATE utf8_unicode_ci = ci.imei_sn
				LEFT JOIN imei_kpi_staff  iks ON i.imei_sn = iks.imei_sn
                LEFT JOIN staff si ON i.pg_id = si.id
				LEFT JOIN staff si1 ON iks.staff_id = si1.id
                LEFT JOIN staff ssi ON i.sale_id = ssi.id
                LEFT JOIN store sti ON i.store_id = sti.id
		LEFT JOIN '.WAREHOUSE_DB.'.imei wi ON   wi.imei_sn COLLATE utf8_unicode_ci = ci.imei_sn
		LEFT JOIN '.WAREHOUSE_DB.'.good g ON wi.good_id = g.id
		LEFT jOIN imei_du_an ida ON ida.imei_sn = ci.imei_sn
                LEFT JOIN (select l.`imei`,group_concat(l.`note`) as note from timing_sale_log l group by l.`imei`) tsl ON tsl.imei = ci.imei_sn
                LEFT JOIN locked_imei lk ON lk.imei_sn = ci.imei_sn
		ORDER BY ci.id ASC
        ')->fetchAll();
        $db = null;
//        if(!empty($export)){
//            set_time_limit(0);
//            ini_set('memory_limit', -1);
//            error_reporting(~E_ALL);
//            ini_set('display_error', 0);
//
//            require_once 'PHPExcel.php';
//            $PHPExcel = new PHPExcel();
//
//            $heads = array(
//                'A' => 'No.',
//                'B' => 'IMEI',
//                'C' => 'Product Name',
//                'D' => 'Timing Staff',
//                'E' => 'Email',
//                'F' => 'Phone Number',
//                'G' => 'Timing Date',
//                'H' => 'Active Date',
//                'I' => 'Shop',
//                'J' => 'Customer Name',
//                'K' => 'Customer Phone',
//                'L' => 'Customer Address',
//                'M' => 'Customer Email',
//                'N' => 'Created At',
//                'O' => 'Note',
//                'P' => 'PG KPI',
//                'Q' => 'Sale KPI',
//                'R' => 'Store KPI',
//                'S' => 'Log note',
//                'T' => 'Locked',
//            );
//            $PHPExcel->setActiveSheetIndex(0);
//            $sheet = $PHPExcel->getActiveSheet();
//
//            foreach ($heads as $key => $value)
//                $sheet->setCellValue($key . '1', $value);
//            $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));
//
//            $sheet->getColumnDimension('A')->setWidth(5);
//            $sheet->getColumnDimension('B')->setWidth(10);
//            $sheet->getColumnDimension('C')->setWidth(17);
//            $sheet->getColumnDimension('D')->setWidth(17);
//            $sheet->getColumnDimension('E')->setWidth(30);
//            $sheet->getColumnDimension('F')->setWidth(35);
//            $sheet->getColumnDimension('G')->setWidth(15);
//            $sheet->getColumnDimension('H')->setWidth(20);
//            $sheet->getColumnDimension('I')->setWidth(20);
//            $sheet->getColumnDimension('J')->setWidth(40);
//            $sheet->getColumnDimension('K')->setWidth(20);
//            $sheet->getColumnDimension('L')->setWidth(15);
//            $sheet->getColumnDimension('M')->setWidth(20);
//            $sheet->getColumnDimension('N')->setWidth(20);
//            $sheet->getColumnDimension('O')->setWidth(30);
//            $sheet->getColumnDimension('P')->setWidth(30);
//            $sheet->getColumnDimension('Q')->setWidth(30);
//            $sheet->getColumnDimension('R')->setWidth(30);
//            $sheet->getColumnDimension('S')->setWidth(30);
//            $sheet->getColumnDimension('T')->setWidth(30);
//
//
//            foreach ($data as $key => $value) {
//                $sheet->setCellValue('A' . ($key + 2), $key + 1);
//                $sheet->setCellValue('B' . ($key + 2), $value['imei_sn']);
//                $sheet->setCellValue('C' . ($key + 2), ($value['product_code'] ? $value['product_code'] : 'n/a'));
//                $sheet->setCellValue('D' . ($key + 2), ($value['pg_name'] ? $value['pg_name'] : null));
//                $sheet->setCellValue('E' . ($key + 2), ($value['pg_email'] ? $value['pg_email'] : null));
//                $sheet->setCellValue('F' . ($key + 2), ($value['pg_phone_number'] ? $value['pg_phone_number'] : null));
//                $sheet->setCellValue('G' . ($key + 2), ($value['imei_status'] ? date('d-m-Y H:i:s', strtotime($value['timing_date'])) : 'IMEI không tồn tại'));
//                $sheet->setCellValue('H' . ($key + 2), ($value['activated_date'] ? date('d-m-Y H:i:s', strtotime($value['activated_date'])) : null));
//                $sheet->setCellValue('I' . ($key + 2), ($value['store_name'] ? $value['store_name'] : null));
//                $sheet->setCellValue('J' . ($key + 2), ($value['customer_name'] ? $value['customer_name'] : null));
//                $sheet->setCellValue('K' . ($key + 2), ($value['customer_phone_number'] ? $value['customer_phone_number'] : null));
//                $sheet->setCellValue('L' . ($key + 2), ($value['customer_address'] ? $value['customer_address'] : null));
//                $sheet->setCellValue('M' . ($key + 2), ($value['customer_email'] ? $value['customer_email'] : null));
//                $sheet->setCellValue('N' . ($key + 2), ($value['created_at'] ? $value['created_at'] : null));
//                $sheet->setCellValue('O' . ($key + 2), ($value['note'] ? $value['note'] : 'n/a'));
//                $sheet->setCellValue('P' . ($key + 2), ($value['pg_kpi'] ? $value['pg_kpi'] : null));
//                $sheet->setCellValue('Q' . ($key + 2), ($value['sale_kpi'] ? $value['sale_kpi'] : null));
//                $sheet->setCellValue('R' . ($key + 2), ($value['store_name_kpi'] ? $value['store_name_kpi'] : null));
//                $sheet->setCellValue('S' . ($key + 2), ($value['log_note'] ? $value['log_note'] : null));
//                $sheet->setCellValue('T' . ($key + 2), ($value['lock_note'] ? $value['lock_note'] : null));
//            }
//
//            $filename = 'imei-checking-' . date('d-m-Y H-i-s');
//            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
//            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
//            $objWriter->save('php://output');
//            exit;
//        }



        $this->view->data = $data;



    }
} else 
{ // Check AJAX khi chấm công

    $info = array();
    $imei = trim($imei);
    $return = $this->checkImei($imei, $timing_sales_id, $info);

    if (intval($imei) > 0 && strlen($imei) == 15) {
        $QCheckImeiLog = new Application_Model_CheckImeiLog();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        // $info .= " - IMEIs (".serialize($imeis).")";
        //
        //todo log
        $QCheckImeiLog->insert(array(
            'result' => $return,
            // 'info' => '',
            'imei' => $imei,
            'user_id' => $userStorage->id,
            'ip_address' => $ip,
            'time' => date('Y-m-d H:i:s'),
        ) );
    }

    if ($return==1) {
        // nó đã chấm ở một lúc nào đó
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 0,
                "message" => "Bạn đã báo cáo IMEI này rồi, vào lúc [" . date('d/m/Y H:i:s', strtotime($info['date'])) . "] Tại cửa hàng [".$info['store']."]",
            )
        );

    } else if ($return==2) {

        //not existed in list sales out
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 0,
                "message" => "IMEI không tồn tại. Vui lòng xem kỹ lại IMEI hoặc liên hệ bộ phận Kỹ thuật.",
            )
        );

    } else if ($return==3 || $return==5) {

        // bị thằng khác chấm trước rồi
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 0,
                "message" => 'IMEI '.$imei.' này đã được ['.@$info['staff'].'] báo cáo ở cửa hàng ['.$info['store'].'] vào lúc ['.date('d/m/Y H:i:s', strtotime($info['date'])).']
                            <a href="#" data-timing-sales-id="'.$info['timing_sales_id'].'" data-imei="'.$imei.'"
                            data-case="IMEI '.$imei.' này đã được ['.@$info['staff'].'] báo cáo ở cửa hàng ['.$info['store'].'] vào lúc ['.date('d/m/Y H:i:s', strtotime($info['date'])).']"
                            data-checksum="'.sha1(md5($imei).$info['timing_sales_id']).'"
                            data-staff-first="'.@$info['staff_id'].'"
                            class="send_notify">Bấm vào đây</a> để gửi yêu cầu xử lý.',
            )
        );

    } /*elseif ($return==5) {

        // tồn tại trong list tháng 12 về trước
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => "IMEI này đã được bán cách đây hơn 01 tháng. Bởi [" . $info['staff'].'] ở cửa hàng ['. $info['store'].'] vào lúc ['.date('d/m/Y H:i:s', strtotime($info['date'])).']. Công ty không tính doanh số đối với các máy đã bán ra thị trường hơn 01 tháng. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } */ elseif ($return == 4) {
        // tồn tại trong bảng imei acti
        $QImeiHappyTime = new Application_Model_ImeiHappyTime();
        $data = $QImeiHappyTime->checkImei($imei);
        if(empty($data))
        {
            echo json_encode(array(
                'value' => $imei,
                'valid' => 0,
                'message' => 'IMEI này đã được bán cách đây hơn '.IMEI_ACTIVATION_EXPIRE.' ngày, vào ngày ['.$info['activated_at'].']. Công ty không tính doanh số đối với các máy đã bán ra thị trường hơn '.IMEI_ACTIVATION_EXPIRE.' ngày. Vui lòng liên hệ ASM nếu có thắc mắc.',
            ));
        }
    } elseif ($return == 6) {
        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy tặng khách hàng, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } elseif ($return == 7) {

        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy demo, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    }  elseif ($return == 8) {
        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy xuất cho nhân viên, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } elseif ($return == 9) {
        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy mượn, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } elseif ($return == 999) {
        $QBlock = new Application_Model_BlockCheckingImei();
        $data = array(
            'staff_id' => $userStorage->id,
            'date' => date('Y-m-d'),
        );

        try {
            @$QBlock->insert($data);
        } catch (Exception $e) {

        }

        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        session_destroy();

        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => sprintf('Bạn đã thử sai hơn %s lần trong 1 ngày.', IMEI_CHECKING_FAILED_IN_1_DAYS),
        ));
    } else {
        //success
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 1,
                "message" => ""
            )
        );
    }

    exit;
}
