<?php

//(imei);
//(date);
//(store_id_oppo);
//(customer_name);
//(customer_phone);
//(customer_address);
//(shift);
//1 active qua 7 ngay
//2 thong tin khach hang chua day du
//3 Không báo dup tháng trước sau ngày 2 mỗi tháng
//4Không báo dup tháng trước sau ngày 2 mỗi tháng
//5 thanh cong

if ($this->getRequest()->getMethod() == 'POST') {
    $imei = $this->getRequest()->getParam('imei');
    $date = $this->getRequest()->getParam('date');
    $store_id_oppo = $this->getRequest()->getParam('store_id_oppo');
    $customer_name = $this->getRequest()->getParam('customer_name');
    $customer_phone = $this->getRequest()->getParam('customer_phone');
    $customer_address = $this->getRequest()->getParam('customer_address');
    $shift = $this->getRequest()->getParam('shift');

    $QDuplicatedImeiRealme = new Application_Model_DuplicatedImeiRealme();
    $QDuplicatedImeiRm = new Application_Model_DuplicatedImeiRm();
    $QDuplicatedImei = new Application_Model_DuplicatedImei();

//trang here
    $new_date = DateTime::createFromFormat('d/m/Y', $date);
    $new_date1 = $new_date->format('Y-m-d'); //trang

    $tmp = explode('/', $date);
    $formatedDate = $tmp[2] . '-' . $tmp[1] . '-' . $tmp[0] . ' 00:00:00';
    $params = array(
        'imei_check' => TRIM($imei),
        'month_check' => $tmp[1],
    );
    $imei = TRIM($imei);
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    try {
        //check active 7 ngay
//        $sql = "SELECT i.id FROM realme_warehouse.imei i WHERE DATEDIFF(NOW(),i.activated_date) > 7 AND i.imei_sn = :imei";
//        $stmt = $db->prepare($sql);
//        $stmt->bindParam('imei', $imei, PDO::PARAM_INT);
//        $stmt->execute();
//        $data = $stmt->fetchAll();
//
//        if (!empty($data)  ) {
//            echo 1;
//            exit();
//        } else

        if  (empty($customer_name) || empty($customer_phone) || empty($customer_address)) {

            echo 2;
            exit();
        } else {
            //Loại imei báo dup cùng shop trong cùng tháng

            $checkSecondImei = $QDuplicatedImeiRm->checkSecondImei($params, $store_id_oppo);

            if (!empty($checkSecondImei)) {
                echo 3;
                exit();
            }

            //Không báo dup tháng trước sau ngày 2 mỗi tháng
//            $getDateNow = date("Y/m/d");
//            $monthNowRev = date('m', strtotime(date($getDateNow) . " -1 month"));
//            $monthNow = date('m', strtotime(date($getDateNow)));
            //            if ((in_array($tmp[0], array(1, 2)) && $monthNowRev == $tmp[1]) || ($tmp[0] >= 3 && $tmp[1] == $monthNow) || (in_array($tmp[0], array(1, 2)) && $tmp[1] == $monthNow)) {
//
//            } else {
//
//                return 4;
//                exit();
//            }

            // tu check  Không báo dup tháng trước sau ngày 2 mỗi tháng
            $current_month = date('m');
            $current_day = date('d');
            if ($current_month != $tmp[1] && $current_day > 2) {
                return 4;
                exit();
            }



            // get area pg bao so
            $sql = " SELECT a.id as area_id FROM realme_hr.staff st INNER JOIN realme_hr.regional_market rm ON rm.id = st.regional_market INNER JOIN realme_hr.area a on a.id = rm.area_id where st.id = :id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $userStorage->id, PDO::PARAM_INT);
            $stmt->execute();
            $area_id = $stmt->fetch();

            //check dup lan 2
            $sql = "SELECT d.id as duplicate_id FROM realme_hr.duplicated_imei d INNER JOIN realme_hr.staff st on st.id = d.staff_id INNER JOIN realme_hr.regional_market rm ON rm.id = st.regional_market INNER JOIN realme_hr.area a on a.id = rm.area_id WHERE d.imei = :imei AND a.id = :area_id AND st.id <> :staff_id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('imei', $imei, PDO::PARAM_INT);
            $stmt->bindParam('area_id', $area_id['area_id'], PDO::PARAM_INT);
            $stmt->bindParam('staff_id', $userStorage->id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll();
            if (!empty($data) && count($data) == 1) {
                $sql = "SELECT s.id,s.title,CASE WHEN s.title = 179 THEN 1 WHEN s.title = 181 THEN 2 WHEN s.title = 308 THEN 3 WHEN s.title = 191 THEN 3 END AS order_title FROM realme_hr.staff s INNER JOIN realme_hr.asm a ON s.id = a.staff_id WHERE a.area_id = :area_id AND s.off_date IS NULL AND s.department = 152  AND s.team = 75  AND s.title IN(179,181,308,191) ORDER BY order_title";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('area_id', $area_id['area_id'], PDO::PARAM_INT);
                $stmt->execute();
                $data1 = $stmt->fetchAll();
                if (!empty($data1)) {
                    $QDuplicatedImeiRm->update(array('asm_check' => 1, 'asm_id' => $data1[0]['id'], 'asm_at' => date('Y-m-d')), array('imei = ?' => $imei));
                }
                //update imei 1
                //$QDuplicatedImeiRm->update(array('asm_check' => 1,'asm_id'=>), $sql)
            }

            $getIdPgTimingSaleRm = $QDuplicatedImeiRm->getIdPgTimingSaleRm($imei);

            $id_duplicated_imei = $QDuplicatedImeiRm->insert(array(
                'staff_id' => 28943, //staff_id ao
                'imei' => $imei,
                'date' => $new_date1,
                'shift' => $shift,
                'store_id' => $QDuplicatedImeiRm->getStoreRmFromOppo($store_id_oppo), //store maping
                'customer_name' => $customer_name,
                'customer_phone' => $customer_phone,
                'customer_address' => $customer_address,
                'created_at' => date('Y-m-d H:i:s'),
                'timing_sales_first' => $getIdPgTimingSaleRm[0]['timing_sales_first'],
                'staff_id_first' => $getIdPgTimingSaleRm[0]['staff_id_first']
                    )
            );

            $QDuplicatedImeiRealme->insert(array(
                'staff_id_oppo' => $userStorage->id,
                'imei_oppo' => $imei,
                'date' => $new_date1,
                'shift' => $shift,
                'store_id_oppo' => $store_id_oppo,
                'customer_name' => $customer_name,
                'customer_phone' => $customer_phone,
                'customer_address' => $customer_address,
                'created_at' => date('Y-m-d H:i:s'),
                'duplicated_imei_id' => $id_duplicated_imei
            ));

            //echo '<script>parent.alert("Báo duplicate thành công");</script>';
            $db->commit();
            echo 5;
            exit();
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
        $db->rollback();
    }
}
