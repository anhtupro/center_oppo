<?php

$params = $this->_request->getParams();
$ik     = new Application_Model_NewImeiKpiRealme();
$dev = $this->getRequest()->getParam('dev');
$from = $this->getRequest()->getParam('from');
$to = $this->getRequest()->getParam('to');

$from = str_replace('/', '-', $from);
$to = str_replace('/', '-', $to);

$from = date('Y-m-d 00:00:00', strtotime($from));
$to = date('Y-m-d 23:59:59', strtotime($to));

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!$userStorage || !isset($userStorage->id))
    $this->_redirect(HOST);

$data = array(
    'staff_id' => $userStorage->id,
    'from'     => $from,
    'to'       => $to
);

//if (!empty($params['filter'])) {
//    $data['from'] = $params['from'] ? DateTime::createFromFormat('d/m/Y', $params['from'])->format('Y-m-d') : date('Y-m-01 00:00:00');
//    $data['to']   = $params['to'] ? DateTime::createFromFormat('d/m/Y', $params['to'])->format('Y-m-d') : date('Y-m-d H:i:s');
//}

//if(!empty($dev)){
//}



if($userStorage->title == 545){
    $data = $ik->Get_All_CONSULTANT($data);
}else{
    $data = $ik->Get_All($data);
}


$this->view->res      = $data;
$this->view->params   = $params;
$this->view->userData = $userStorage;
