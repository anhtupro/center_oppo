<?php
$id = $this->getRequest()->getParam('id');

$QTiming = new Application_Model_Timing();
$timingRowset = $QTiming->find($id);
$timing = $timingRowset->current();

if ($timing){
    $date = explode(' ', $timing->from);
    $this->view->from = substr($date[1], 0, -3);
    $tem = explode('-', $date[0]);
    $this->view->date = $tem[2].'/'.$tem[1].'/'.$tem[0];
    $to = explode(' ', $timing->to);
    $this->view->to = substr($to[1], 0, -3);

    $this->view->timing = $timing;

    //get staff info
    $QStaff = new Application_Model_Staff();
    $where = $QStaff->getAdapter()->quoteInto('id = ?', $timing->staff_id);
    $this->view->staff = $QStaff->fetchRow($where);

    //timing sale
    $QTimingSale = new Application_Model_TimingSale();
    $where = $QTimingSale->getAdapter()->quoteInto('timing_id = ?', $id);
    $timing_sales = $QTimingSale->fetchAll($where);

    $QModel = new Application_Model_Model();
    $QCustomer = new Application_Model_Customer();
    $data = array();
    if ($timing_sales->count()){
        foreach ($timing_sales as $item){
            $where = $QModel->getAdapter()->quoteInto('product_id = ?', $item->product_id);
            $models = $QModel->fetchAll($where);

            $where = $QCustomer->getAdapter()->quoteInto('id = ?', $item->customer_id);
            $customer = $QCustomer->fetchRow($where);

            $data[] = array(
                'id' => $item->id,
                'photo' => $item->photo,
                'product_id' => $item->product_id,
                'model_id' => $item->model_id,
                'models' => $models,
                'customer' => $customer,
                'imei' => $item->imei,
            );
        }
    }
    $this->view->timing_sales = $data;

    // load all model
    $QProduct = new Application_Model_Product();
    $products = $QProduct->fetchAll();
    $this->view->products = $products;
    $result = $QModel->fetchAll();

    $data = null;
    if ($result->count()) {
        foreach ($products as $key => $product) {
            $temp = array();
            foreach ($result as $item){
                if ($item['product_id'] == $product['id']) {
                    $temp[] = array(
                        'id' => $item->id,
                        'model' => $item->model,
                    );
                }
            }
            $data[$product['id']] = $temp;
        }
    }
    $this->view->model = json_encode($data);
    //
}

//get product
$QProduct = new Application_Model_Product();
$this->view->products = $QProduct->fetchAll();

//get store
$QStore = new Application_Model_Store();
$where = $QStore->getAdapter()->quoteInto('regional_market = ?', $this->view->staff->regional_market);
$this->view->stores = $QStore->fetchAll($where, 'name');

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$this->_helper->viewRenderer->setRender('edit-photo');