<?php

$sort      = $this->getRequest()->getParam('sort');
$desc      = $this->getRequest()->getParam('desc', 1);
$export    = $this->getRequest()->getParam('export', 0);
$dealer_id = $this->getRequest()->getParam('dealer_id', 0);
$from      = $this->getRequest()->getParam('from', date('01/m/Y'));
$to        = $this->getRequest()->getParam('to', date('d/m/Y'));
$area      = $this->getRequest()->getParam('area');


$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$staff_id         = $userStorage->id;
$params           = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'area'   => $area,
    'export' => $export,
);
$QTeam            = new Application_Model_Team();
$staff_title_info = $userStorage->title;
$team_info        = $QTeam->find($staff_title_info);
$team_info        = $team_info->current();
$group_id         = $team_info['access_group'];

$db          = Zend_Registry::get('db');
$date_target = $this->getRequest()->getParam('to', date('d/m/Y'));
$date_target = date_create_from_format("d/m/Y", $date_target)->format("Y-m-d");
$sql_target  = "SELECT area_id, SUM(`sell_out`) AS 'target' 
							FROM `target_sales` 
							WHERE `month` = MONTH('$date_target') 
										AND `year` = YEAR('$date_target')
										AND area_id IN (SELECT area_id FROM asm WHERE staff_id = :staff_id)
							GROUP BY area_id			";
$stmt_target = $db->prepare($sql_target);
$stmt_target->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
$stmt_target->execute();
$info_target = $stmt_target->fetchAll();
$stmt_target->closeCursor();
$QTeam       = New Application_Model_Team();
$title       = $userStorage->title;
$team        = $QTeam->find($title);
$team_group  = $team->current();
$group_id    = $team['access_group'];
if (in_array($userStorage->id, array(5899, 341, 765, 24125,24129)) || in_array($group_id, array(
            ADMINISTRATOR_ID,
            HR_ID,
            HR_EXT_ID,
            BOARD_ID,
            SALES_ADMIN_ID))) {
    $sql_target  = "SELECT area_id, SUM(`sell_out`) AS 'target' 
							FROM `target_sales` 
							WHERE `month` = MONTH('$date_target') 
										AND `year` = YEAR('$date_target')
										
							GROUP BY area_id			";
    $stmt_target = $db->prepare($sql_target);
//    $stmt_target->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
    $stmt_target->execute();
    $info_target = $stmt_target->fetchAll();
    $stmt_target->closeCursor();
}
$targetArea = array();
if (!empty($info_target[0])) {

    foreach ($info_target as $target) {
        $targetArea[$target['area_id']] = $target['target'];
    }
}
$this->view->targetArea = $targetArea;

if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $area_list                  = array();
    $QAsm                       = new Application_Model_Asm();
    $list_regions               = $QAsm->get_cache($userStorage->id);
    $list_regions               = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $this->view->viewed_area_id = $list_regions;
}

$QArea                   = new Application_Model_Area();
$QImeiKpi                = new Application_Model_ImeiKpi();
$this->view->userStorage = $userStorage;
$data                    = $QImeiKpi->fetchArea($params);
if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171', '171.244.18.76', '171.244.18.86'))) {
//    echo "<pre>";
//    print_r($data);
//    die;
        }
if ($dealer_id) {
    $params['dealer_id'] = $dealer_id;
    $dealer              = $QImeiKpi->fetchArea($params);

    $data_dealer = [];
    foreach ($dealer as $key => $value) {
        $data_dealer[$value['area_id']] = $value;
    }
    $this->_exportAreaKa($data, $data_dealer);
}

$params['get_total_sales'] = true;

$total_sales       = $QImeiKpi->fetchArea($params);
$total_money       = $total_sales['total_value'];
$total_area        = $total_sales['total_quantity'];
$total_area_active = $total_sales['total_area_active'];

// if ($staff_id == 5899) {
// echo $total_area; echo $total_area_active ;die;
//   // echo $total_value_kpi;
// }
//get total money
// $total_money = 0;
// $total_sales = 0;
// foreach ($data as $key => $value) {
//     if ($list_regions) {
//         if(in_array($value['area_id'], $list_regions)){
//             $total_money += $value['total_value'];
//             $total_sales += $value['total_quantity'];
//             $total_sales_activated += $value['total_activated'];
//         }
//     }
//     else{
//         $total_money += $value['total_value'];
//         $total_sales += $value['total_quantity'];
//         $total_sales_activated += $value['total_activated'];
//     }
// }

$sales = array();
// foreach($sales as $value){
//      $total_sales += $value['total_quantity'];
//      $total_sales_activated += $value['total_activated'];
// }
//   $ti_le_active_ca_nuoc = round( (($total_sales - $total_sales_activated) / $total_sales ) * 100,1) ;
//    $sales['ti_le_active_ca_nuoc'] = $ti_le_active_ca_nuoc; 
//tính point
// if ($staff_id == 5899) {
//   echo "<pre>";print_r( $data);
//   // echo $total_value_kpi;
//   die;
// }


foreach ($data as $item) {
    $point                              = ( $total_money > 0 and ( $item ['region_share'] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($item ['region_share'] / 100), 2) : 0;
    $val                                = $item;
    $val['point_cu']                    = $point;
    $val['ti_le_chua_active']           = round(( ( $item['total_quantity'] - $item['total_activated'] ) / $item['total_quantity'] ) * 100, 1);
    $val['ti_le_active_ca_nuoc']        = round((($total_area - $total_area_active) / $total_area ) * 100, 1);
    $active_vuot                        = round(( ( $item['total_quantity'] - $item['total_activated'] ) / $item['total_quantity'] ) * 100, 1) - round((($total_area - $total_area_active) / $total_area ) * 100, 1);
    $val['active_vuot']                 = $active_vuot;
    $val['value_exclude_special_model'] = $val['total_value'] - $val['value_a71k'];
     $val['value_exclude_special_model'] = $val['total_value'] - $val['value_a71k'];
      $val['value_exclude_special_model'] = $val['total_value'] - $val['value_a71k'];
    if ($active_vuot <= 0) {
        $val['value_kpi']                       = $item ['total_value'];
        $val['value_kpi_exclude_special_model'] = $val['value_exclude_special_model'];
        $val['value_kpi_a71k_model']            = $item['value_a71k'];
    } else {
        $val['value_kpi']                       = $item ['total_value'] * ( (100 - $active_vuot) / 100 );
        $val['value_kpi_exclude_special_model'] = $val ['value_exclude_special_model'] * ( (100 - $active_vuot) / 100 );
        $val['value_kpi_a71k_model']            = $item ['value_a71k'] * ( (100 - $active_vuot) / 100 );
    }

    $sales[] = $val;
}
// $total_money = 0;    
// $total_sales = 0;


$sales_final = array();
$total_value = 0;
foreach ($sales as $key => $value) {
    $total_value_kpi += $value['value_kpi'];
    $total_value     += $value ['total_value'];
}




foreach ($sales as $item) {
    $point_moi     = round(($item['value_kpi'] / $total_value_kpi) * 60 / ($item['region_share'] / 100), 2);
    $val           = $item;
    $val['point']  = $point_moi;
    $sales_final[] = $val;
}


unset($params['kpi']);
unset($params['get_total_sales']);
usort($sales_final, array($this, 'cmp'));

$this->view->areas           = $QArea->get_cache();
$this->view->total_value_kpi = $total_value_kpi;
$this->view->total_value     = $total_value;

$this->view->total_sales           = $total_sales['total_quantity'];
$this->view->total_sales_activated = $total_sales['total_activated'];
$this->view->sales                 = $sales_final;
$this->view->url                   = HOST . 'timing/analytics-area' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->desc                  = $desc;
$this->view->current_col           = $sort;
$this->view->to                    = $to;
$this->view->from                  = $from;
$this->view->params                = $params;





