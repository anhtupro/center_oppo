<?php

$export    = $this->getRequest()->getParam('export');
$area      = $this->getRequest()->getParam('area');
$from      = $this->getRequest()->getParam('from', date('01/m/Y'));
$to        = $this->getRequest()->getParam('to', date('d/m/Y'));

$dev        = $this->getRequest()->getParam('dev');

    //die($from);

$db = Zend_Registry::get('db');

$select1 = $db->select()->from(array('ik' => 'imei_kpi'), array(new Zend_Db_Expr
('MONTH(ik.timing_date) AS month'),
    new Zend_Db_Expr
    ('YEAR(ik.timing_date) AS year'),
    new Zend_Db_Expr
    ('COUNT(ik.imei_sn) AS total_quantity'),
    new Zend_Db_Expr
    ('SUM(ik.value) AS total_value'),
    new Zend_Db_Expr
    ('SUM(ik.value)*0.003*0.8 AS KPI_of_asm')
    ));
$select1->joinInner(array('a' => 'area'), 'ik.area_id = a.id', array('area_name' =>
    'a.name'));

//sale, pg query
$select2 = $db->select()->from(array('ik' => 'imei_kpi'), array(
    'imei_sn'=>'ik.imei_sn',
    'kpi_sale'=>'ik.kpi_sale',
    'kpi_store_leader'=>'ik.kpi_store_leader',
    'kpi_pg'=>'ik.kpi_pg',
    'timing_date'=>'ik.timing_date'
));
$select2->joinInner(array('a' => 'area'), 'a.id = ik.area_id', array(
    'area_name'=>'a.name'
    ));
$select2->joinInner(array('st' => 'store'), 'st.id = ik.store_id', array(
    'store_name'=>'st.name'
));
$select2->joinInner(array('g' => 'yadea_warehouse.good'), 'g.id = ik.good_id', array(
    'good_name'=>'g.name'
));
$select2->joinLeft(array('s' => 'staff'), 's.id = ik.pg_id', array(
    'pg_code'=>'s.code',
    'pg_name'=>new Zend_Db_Expr('CONCAT(s.firstname," ",s.lastname)')
));
$select2->joinLeft(array('s1' => 'staff'), 's1.id = ik.sale_id', array(
    'sale_code'=>'s1.code',
    'sale_name'=>new Zend_Db_Expr('CONCAT(s1.firstname," ",s1.lastname)')
));

$select2->joinLeft(array('s2' => 'staff'), 's2.id = ik.leader_id', array(
    'store_leader_code'=>'s2.code',
    'store_leader_name'=>new Zend_Db_Expr('CONCAT(s2.firstname," ",s2.lastname)')
));
$select2->joinLeft(array('ii'=>'imei_installment'),'ii.imei_sn=ik.imei_sn',
    array(
        new Zend_Db_Expr('CASE
    WHEN ii.installment_company =1 THEN "FE CREDIT"
    WHEN ii.installment_company =2 THEN "HOME CREDIT"
    WHEN ii.installment_company =3 THEN "HD CREDIT"
    END AS installment_company')
    ));

if (isset($from) && $from){
    $fromDate = $this->getRequest()->getParam('from', date('d/m/Y'));
    $fromDate = date_create_from_format("d/m/Y", $fromDate)->format("Y-m-d 00:00:00");
    $select1->where("ik.timing_date >= ?",  $fromDate);
    $select2->where("ik.timing_date >= ?",  $fromDate);
}

if (isset($to) && $to){
    $toDate = $this->getRequest()->getParam('to', date('d/m/Y'));
    $toDate = date_create_from_format("d/m/Y", $toDate)->format("Y-m-d 23:59:59");
    $select1->where("ik.timing_date <= ?",  $toDate);
    $select2->where("ik.timing_date <= ?",  $toDate);
}

if (isset($area) && $area){
    $select1->where("ik.area_id = ?",  $area);
    $select2->where("ik.area_id = ?",  $area);

}

$select1->group('a.id');
//$select1->group('month');
//$select1->group('year');

$select1->order('area_name DESC');
$select1->order('year DESC');
$select1->order('month DESC');

$select2->group('ik.imei_sn');
$select2->order('timing_date DESC');

if($dev){
    echo "hhhh";
    $qr=$select1->__toString();

    $qr2=$select2->__toString();
    print_r($qr);
    echo "<hr>";
    print_r($qr2);
    die('');
}
$asmData = $db->fetchAll($select1);
$salePGData = $db->fetchAll($select2);

//export timing
if (isset($export) and $export){

    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();



    $heads    = array(
        'Id',
        'Tên khu vực',
        'Tổng số',
        'Năm',
        'Tháng',
        'Tổng doanh thu',
        'KPI of ASM (80% doanh thu * 0.3%)',
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;


    try {
        if ($asmData)
            foreach ($asmData as $_key => $_order) {

                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['total_quantity'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['year'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['month'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['total_value'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['KPI_of_asm'], PHPExcel_Cell_DataType::TYPE_STRING);

                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $PHPExcel->getActiveSheet()->setTitle('Data ASM');

    //export second sheet
    $PHPExcel->createSheet();

    $heads    = array(
        'STT',
        'Imei_sn',
        'Good name',
        'Store name',
        'Area',
        'PG Code',
        'PG Name',
        'Sale Code',
        'Sale Name',
        'Store Leader Code',
        'Store Leader Name',
        'KPI PG',
        'KPI Sale',
        'KPI store leader',
        'Timing at',
        'Installment Company'
    );

    $PHPExcel->setActiveSheetIndex(1);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;


    try {
        if ($salePGData)
            foreach ($salePGData as $_key => $_order) {

                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['imei_sn'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['good_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['store_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['pg_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['pg_name'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['sale_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['sale_name'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['store_leader_code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['store_leader_name'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['kpi_pg'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['kpi_sale'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['kpi_store_leader'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['timing_date'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['installment_company'], PHPExcel_Cell_DataType::TYPE_STRING);

                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $PHPExcel->getActiveSheet()->setTitle('Data PGS SALE');



    $filename  = 'ASM_REPORT_BY_AREA' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
    $flashMessenger->setNamespace('success')->addMessage('Thành công.');
}




