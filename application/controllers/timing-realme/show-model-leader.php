<?php  
	
	$staff_id	= $this->getRequest()->getParam('staff_id');
	$from		= $this->getRequest()->getParam('from');
	$to			= $this->getRequest()->getParam('to');
	$area_id	= $this->getRequest()->getParam('area_id');
	$sellout	= $this->getRequest()->getParam('sellout');
	$staff_name	= $this->getRequest()->getParam('staff_name');
	// echo $sellout;die;

	$params = array(
		'from'			=> $from,
		'to'			=> $to,
		'area_id'		=> $area_id,
		'staff_id'		=> $staff_id,
		'sellout'		=> $sellout,
		'staff_name'	=> $staff_name
	);

	$QImeiKpi				= new Application_Model_ImeiKpi2();
	$result['data']			= $QImeiKpi->getModelLeader($params);
	
	$this->view->sum		= count($result['data']);
	$this->view->staff_name	= $staff_name;
	$this->view->sellout	= $sellout;
	$this->view->result		= $result['data'];

	$this->_helper->layout()->disablelayout(true);
	// echo "<pre>";print_r($result);die;
?>