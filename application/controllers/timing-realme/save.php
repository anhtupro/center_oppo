<?php
echo 'Chức này này không còn hoạt động'; die;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();


$ip = $this->getRequest()->getServer('REMOTE_ADDR');
$QTiming = new Application_Model_TimingRealme();
$QTimingSale = new Application_Model_TimingSaleRealme();
$QWebImei = new Application_Model_WebImeiRealme();
$QStoreMapping = new Application_Model_StoreMapping();
//$QApp_installing   = new Application_Model_AppInstallingImei();
$installment_model = new Application_Model_ImeiInstallmentRealme();
if (!defined("IMEI_ACTIVATION_EXPIRE"))
    define("IMEI_ACTIVATION_EXPIRE", 3);

$db = Zend_Registry::get('db');
if ($this->getRequest()->getMethod() == 'POST') {
    set_time_limit(0);

    $id = $this->getRequest()->getParam('id');
    $shift = $this->getRequest()->getParam('shift', 1);
    $date = $this->getRequest()->getParam('date');
    $store = $this->getRequest()->getParam('store');
    $start = $this->getRequest()->getParam('timepicker_start');
    $end = $this->getRequest()->getParam('timepicker_end');
    $note = $this->getRequest()->getParam('note');
    $products = $this->getRequest()->getParam('product');
    $models = $this->getRequest()->getParam('model');
    $imeis = $this->getRequest()->getParam('imei');
    $customer_names = $this->getRequest()->getParam('customer_name');
    $phone_numbers = $this->getRequest()->getParam('phone_number');
    $emails = $this->getRequest()->getParam('email');
    $addresses = $this->getRequest()->getParam('address');
    $amortization = $this->getRequest()->getParam('amortization');
    $customer_ids = $this->getRequest()->getParam('customer_ids');
    $timing_sales_ids = $this->getRequest()->getParam('timing_sales_ids');
    $photos = $this->getRequest()->getParam('photo');
    $back_url = $this->getRequest()->getParam('back_url');
    $timing_type = $this->getRequest()->getParam('timing_type', 1);
    $app_install = $this->getRequest()->getParam('app_installing', 0);

//    if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
    $installment_status = $this->getRequest()->getParam('installment_status');
    $installment_company = $this->getRequest()->getParam('installment_company');
    $installment_contract = $this->getRequest()->getParam('installment_contract');
    $installment_id = $this->getRequest()->getParam('installment_id');
    foreach ($installment_status as $key => $value) {
        if (empty($customer_names[$key]) || empty($phone_numbers[$key]) || empty($addresses[$key])) {
            echo '<script>
                parent.alert("Vui lòng nhập đầy đủ thông tin khách hàng.");
                parent.palert("Vui lòng nhập đầy đủ thông tin khách hàng.");
                </script>';
            exit;
        }
        if ($value == 1) { // co tick tra gop
            if (empty($installment_company[$key]) || empty($installment_contract[$key])) {
                echo '<script
                parent.alert("Vui lòng nhập đầy đủ thông tin trả góp.");
                parent.palert("Vui lòng nhập đầy đủ thông tin trả góp.");
                </script>';
                exit;
            }
        }
    }
    $timing_type = 1; // default oppo
//    }
    //Phụ kiện
    $accessories = $this->getRequest()->getParam('accessories');
    $num_accessories = $this->getRequest()->getParam('num_accessories');
    $timing_accessories_ids = $this->getRequest()->getParam('timing_accessories_id');

    // Check biến $start và $end trong trường hợp có giá trị rỗng từ 2 biến trên
    // Nếu cả hai rỗng thì sẽ lấy thời gian submit. Nếu một trong hai rỗng thì sẽ lấy giá trị không rỗng gán cho giá trị rỗng.
    // Giá trị của phút được làm trong theo bội số của 5 như định dạng
//check timing_type to change model


    if (empty($start)) {
        $current_hour = date("H", time());
        $current_minute = date("i", time());
        $current_minute = sprintf("%02d\n", ($current_minute) - fmod($current_minute, 5));
        $start = $current_hour . ":" . $current_minute;
    }

    $temp = explode('/', $date);
    $temp2 = explode(':', $start);
    $temp3 = explode(':', $end);

    // check ko cho báo số chéo từ ngày 01/9/2020
    $date_check = date('Y-m-d', strtotime($temp[2] . '-' . $temp[1] . '-' . $temp[0])); // timing_date

    if ($date_check >= date('Y-m-d', strtotime('2020-09-01'))) {
        echo '<script>
                parent.alert("Không thể báo số Realme sau ngày 01/09/2020");
                parent.palert("Không thể báo số Realme sau ngày 01/09/2020");
            </script>';
        exit;
    }

    $store_id_realme = $QStoreMapping->getStoreRealme($store);
    if (!$store) {
        echo '<script>
                parent.alert("Vui lòng nhập cửa hàng.");
                parent.palert("Vui lòng nhập cửa hàng.");
            </script>';
        exit;
    }
    if (! $store_id_realme) {
        echo '<script>
                parent.alert("Store chưa được liên kết vối hệ thống Realme hoặc store đã bị đóng. Vui lòng liên hệ Admin để được hỗ trợ");
                parent.palert("Store chưa được liên kết vối hệ thống Realme hoặc store đã bị đóng. Vui lòng liên hệ Admin để được hỗ trợ");
            </script>';
        exit;
    }



    $QStore = new Application_Model_Store();
    $store_cache = $QStore->get_cache();

    if (!isset($store_cache[$store])) {
        echo '<script>
                parent.alert("Cửa hàng không hợp lệ.");
                parent.palert("Cửa hàng không hợp lệ.");
            </script>';
        exit;
    }

    // nếu ở TGDD thì set các IMEI là null -> như vậy thì vẫn chấm công được nhưng ếu có IMEI nào
    if (defined("PREVENT_TIMING_AT_TGDD") && PREVENT_TIMING_AT_TGDD && preg_match('/^TGDĐ[\s]?-/', $store_cache[$store]))
        $imeis = $models = $products = $customer_names = $phone_numbers = $emails = $customer_ids = $timing_sales_ids = $photos = null;

    // if (    ! ( isset($temp[2]) and intval($temp[2]) >= 2013 and intval($temp[2]) <= 2020
    //     and isset($temp[1]) and intval($temp[1]) <= 12 and intval($temp[1]) >= 1
    //     and isset($temp[0]) and intval($temp[0]) <= 31 and intval($temp[0]) >= 1
    //     and isset($temp2[0]) and intval($temp2[0]) <= 23 and intval($temp2[0]) >= 0
    //     and isset($temp2[1]) and intval($temp2[1]) <= 59 and intval($temp2[1]) >= 0
    //     and isset($temp3[0]) and intval($temp3[0]) <= 23 and intval($temp3[0]) >= 0
    //     and isset($temp3[1]) and intval($temp3[1]) <= 59 and intval($temp3[1]) >= 0
    // )
    // ){
    //     echo '<script>
    //             parent.alert("Vui lòng chọn đúng định dạng ngày báo cáo.");
    //             parent.palert("Vui lòng chọn đúng định dạng ngày báo cáo.");
    //         </script>';
    //     exit;
    // }
    $new_date = DateTime::createFromFormat('d/m/Y', $date);
    if (!$new_date) {
        echo '<script>alert("Vui lòng chọn đúng định dạng ngày báo cáo.");</script>';
        exit;
    }
    $new_date = $new_date->format('Y-m-d') . date(' H:i:s');


    //prevent report soon
    if (strtotime($temp[2] . '-' . $temp[1] . '-' . $temp[0]) > strtotime(date('Y-m-d'))) {
        echo '<script>
                parent.palert("Bạn không thể báo cáo cho trước ngày hiện tại! Vui lòng xem lại ngày.");
                parent.alert("Bạn không thể báo cáo cho trước ngày hiện tại! Vui lòng xem lại ngày.");
            </script>';
        exit;
    }

    $time_limit_timing = TIME_LIMIT_TIMING;

    // if (!My_Staff_Title::isPg($userStorage->title)) {
    //     if ((date('d') > TIME_LIMIT_TIMING_SALE || date('d') == 1))
    //         $time_limit_timing = TIME_LIMIT_TIMING_SALE;
    //     else
    //         $time_limit_timing = (date('d') - 1) * 60*60*24;
    // }
    // prevent report after 1 day (pg) or 6 days (sale/leader)
    if (strtotime(date('Y-m-d')) - strtotime($temp[2] . '-' . $temp[1] . '-' . $temp[0]) > $time_limit_timing) {
        $day_limit = ceil($time_limit_timing / 86400);

        echo '<script>
                parent.palert("Bạn chỉ có thể báo cáo cho hôm nay và ' . $day_limit . ' ngày trước.");
                parent.alert("Bạn chỉ có thể báo cáo cho hôm nay và ' . $day_limit . ' ngày trước.");
            </script>';
        exit;
    }

    // validate đuôi hình không được viết hoa
    // if($photos){
    //     $extension = array('jpg','jpeg','png','gif');
    //     foreach ($photos as $key => $value) {
    //         $userfile_extn = substr($value, strrpos($value, '.')+1);
    //         if (!in_array($userfile_extn,$extension)){
    //             echo '<script>
    //                 parent.alert("Đuôi file hình [-- '.$value.' --] không được viết hoa , chỉ cho phép đuôi dạng [-- jpg , jpeg , png , gif --] . Vui lòng check lại file hình.");
    //                 parent.palert("Đuôi file hình [-- '.$value.' --] không được viết hoa , chỉ cho phép đuôi dạng [-- jpg , jpeg , png , gif --] .Vui lòng check lại file hình.");
    //             </script>';
    //         exit;
    //         }
    //     }
    // }


    if ($imeis && $timing_type != 1) {
        $QTimimigCheck = new Application_Model_TimingCheckRealme();
        foreach ($imeis as $key => $imei) {
            $imei = TRIM($imei);
            $staff_id = $userStorage->id;
            $checkImeiDuAn = $QTimimigCheck->checkImeiDuAn($imei);

            if (isset($checkImeiDuAn) && $checkImeiDuAn && $checkImeiDuAn['flag'] == 0) {

                $QTimimigCheck->insertImeiDuAnLog($staff_id, $imei, $store);
                echo '<script>
                    parent.palert("Imei này thuộc diện ' . $checkImeiDuAn['note'] . ' . Công ty không tính doanh số với các imei thuộc diện này. Vui lòng liên hệ Tech team nếu có thắc mắc.");
                    parent.alert("Imei này thuộc diện ' . $checkImeiDuAn['note'] . ' . Công ty không tính doanh số với các imei thuộc diện này. Vui lòng liên hệ Tech team nếu có thắc mắc.");
                </script>';

                exit;
            }
        }
    }

    $imei_list_info = array();
    if (isset($imeis) && $imeis) {
        foreach ($imeis as $k => $imei) {
            $info = array();
            $imei = TRIM($imei);


            //check trong db cua oppo
            if ($timing_type == 2) {
                $return = $this->checkImeiRm($imei, (isset($timing_sales_ids[$k]) ? $timing_sales_ids[$k] : null), $info);
            } else {
                $return = $this->checkImeiRealme($imei, (isset($timing_sales_ids[$k]) ? $timing_sales_ids[$k] : null), $info);
            }
            //echo '<pre>'; print_r($return); die;

            $QCheckImeiLogRealme = new Application_Model_CheckImeiLogRealme();
            //todo log
            $QCheckImeiLogRealme->insert(array(
                'result' => $return,
                // 'info' => '',
                'imei' => $imei,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
                'store_id' => $store
            ));
            // echo $return;die;

            if ($return == 1) {
                echo '<script>
                            parent.palert("Bạn đã báo cáo IMEI: ' . $imei . ' rồi, vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Tại cửa hàng [' . $info['store'] . ']");
                            parent.alert("Bạn đã báo cáo IMEI: ' . $imei . ' rồi, vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Tại cửa hàng [' . $info['store'] . ']");
                        </script>';
                exit;
            } elseif ($return == 2) {
                echo '<script>
                            parent.palert("IMEI ' . $imei . ' không tồn tại. Vui lòng xem kỹ lại IMEI hoặc liên hệ bộ phận Kỹ thuật.");
                            parent.alert("IMEI ' . $imei . ' không tồn tại. Vui lòng xem kỹ lại IMEI hoặc liên hệ bộ phận Kỹ thuật.");
                        </script>';
                exit;
            } elseif ($return == 3 || $return == 5) {
                // Tu thay doi do lưu dữ liệu sai
//                echo '<script>
//                                parent.palert("IMEI '
//                    . $imei . ' này đã được báo cáo lúc ['
//                    . date('d/m/Y H:i:s', strtotime($info['date']))
//                    . '] <a href=\"#\" data-staff-first=\"'
//                    . $info['staff_id'] . '\" data-timing-sales-id=\"'
//                    . $info['timing_sales_id'] . '\" data-imei=\"'
//                    . $imei . '\" data-case=\"IMEI '
//                    . $imei . ' này đã được báo cáo lúc ['
//                    . date('d/m/Y H:i:s', strtotime($info['date'])) . ']\" data-checksum=\"' . sha1(md5($imei) . $info['timing_sales_id']) . '\" class=\"send_notify\">Bấm vào đây</a> để gửi yêu cầu xử lý.");
//                                parent.alert("IMEI ' . $imei . ' đã  báo cáo vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Cuộn lên đầu trang để chọn chức năng gửi báo cáo trùng IMEI.");
//                            </script>';
//                exit;

                echo '<script>
                                parent.palert("IMEI '
                    . $imei . ' này đã được báo cáo lúc ['
                    . date('d/m/Y H:i:s', strtotime($info['date']))
                    . ']");
                                parent.alert_confirm("IMEI ' . $imei . ' đã  báo cáo vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Trong hệ thống Realme. Bạn có muốn báo trùng không ?.","' . $imei . '","' . $date . '",' . $store . ',"' . $customer_names[$k] . '","' . $phone_numbers[$k] . '","' . $addresses[$k] . '",' . $shift . ');
                </script>';
                exit;

            } elseif ($return == 4) {
                $QImeiHappyTime = new Application_Model_ImeiHappyTimeRealme();
                $dataCheckImei = $QImeiHappyTime->checkImei($imei);

                if (empty($dataCheckImei)) {
//				if ($dataCheckImei!=0) {
                    echo '<script>
                                parent.palert("IMEI ' . $imei . ' đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày, vào ngày [' . $info['activated_at'] . ']. Công ty không tính doanh số đối với các máy đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                                parent.alert("IMEI ' . $imei . ' đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày, vào ngày [' . $info['activated_at'] . ']. Công ty không tính doanh số đối với các máy đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                            </script>';
                    exit;
                }
            } elseif ($return == 6) {
                echo '<script>
                            parent.palert("IMEI ' . $imei . ' của máy tặng khách hàng hoặc khóa chấm công, thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales nếu có thắc mắc.");
                            parent.alert("IMEI ' . $imei . ' của máy tặng khách hàng hoặc khóa chấm công, thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales nếu có thắc mắc.");
                        </script>';
                exit;
            } elseif ($return == 7) {
                echo '<script>
                            parent.palert("IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                            parent.alert("IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                        </script>';
                exit;
            } elseif ($return == 8) {
                echo '<script>
                            parent.palert("IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                            parent.alert("IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                        </script>';
                exit;
            } elseif ($return == 9) {
                echo '<script>
                            parent.palert("IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                            parent.alert("IMEI ' . $imei . ' thuộc diện không tính KPI. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                        </script>';
                exit;
            } elseif ($return == 5) {
                echo '<script>
                            parent.palert("IMEI ' . $imei . ' đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Bởi [' . $info['staff'] . '] ở cửa hàng [' . $info['store'] . '] vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . ']. Công ty không tính doanh số đối với các máy đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                            parent.alert("IMEI ' . $imei . ' đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Bởi [' . $info['staff'] . '] ở cửa hàng [' . $info['store'] . '] vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . ']. Công ty không tính doanh số đối với các máy đã kích hoạt hơn ' . IMEI_ACTIVATION_EXPIRE . ' ngày. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                        </script>';
                exit;
            } elseif ($return == 13) {
                echo '<script>
                             parent.palert("IMEI ' . $imei . ' chưa được set mapping vui lòng liên hệ Tech Team.");
                            parent.alert("IMEI ' . $imei . ' chưa được set mapping vui lòng liên hệ Tech Team.");
                        </script>';
                exit;
            } elseif ($return == 14) {
                echo '<script>
                             parent.palert("IMEI ' . $imei . ' của Realme không tồn tại.");
                            parent.alert("IMEI ' . $imei . ' của Realme không tồn tại.");
                        </script>';
                exit;
            } elseif ($return == 999) {
                $QBlock = new Application_Model_BlockCheckingImeiRealme();
                $data = array(
                    'staff_id' => $userStorage->id,
                    'date' => date('Y-m-d'),
                );

                try {
                    @$QBlock->insert($data);
                } catch (Exception $e) {

                }

                $auth = Zend_Auth::getInstance();
                $auth->clearIdentity();
                session_destroy();

                echo '<script>
                            parent.palert("' . sprintf('Bạn đã thử sai hơn %s lần trong 1 ngày.', IMEI_CHECKING_FAILED_IN_1_DAYS) . '");
                            parent.alert("' . sprintf('Bạn đã thử sai hơn %s lần trong 1 ngày.', IMEI_CHECKING_FAILED_IN_1_DAYS) . '");
                        </script>';
                exit;
            }


            //check trong bd realme
            //chi check imei da duoc bao so chua trong DB real me, con nua se check ben duoi theo db oppo
            $return = $this->checkTimingRealme($imei, (isset($timing_sales_ids[$k]) ? $timing_sales_ids[$k] : null), $info);
            print_r($info);
            if ($return == 3 || $return == 5) {
                // Tu dong lai do luu sai du lieu
                /*echo '<script>
                                parent.palert("IMEI '
                    . $imei . ' này đã được báo cáo lúc ['
                    . date('d/m/Y H:i:s', strtotime($info['date']))
                    . '] <a href=\"#\" data-staff-first=\"'
                    . $info['staff_id'] . '\" data-timing-sales-id=\"'
                    . $info['timing_sales_id'] . '\" data-imei=\"'
                    . $imei . '\" data-case=\"IMEI '
                    . $imei . ' này đã được báo cáo lúc ['
                    . date('d/m/Y H:i:s', strtotime($info['date'])) . ']\" data-checksum=\"' . sha1(md5($imei) . $info['timing_sales_id']) . '\" class=\"send_notify\">Bấm vào đây</a> để gửi yêu cầu xử lý.");
                                parent.alert("IMEI ' . $imei . ' đã  báo cáo vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Trong hệ thống Realme. Cuộn lên đầu trang để chọn chức năng gửi báo cáo trùng IMEI.");
                            </script>';*/
                echo '<script>
                                parent.palert("IMEI '
                    . $imei . ' này đã được báo cáo lúc ['
                    . date('d/m/Y H:i:s', strtotime($info['date']))
                    . ']");
                                parent.alert_confirm("IMEI ' . $imei . ' đã  báo cáo vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Trong hệ thống Realme. Bạn có muốn báo trùng không ?.","' . $imei . '","' . $date . '",' . $store . ',"' . $customer_names[$k] . '","' . $phone_numbers[$k] . '","' . $addresses[$k] . '",' . $shift . ');
                </script>';
                exit;
            }
            $QCheckImeiLogRealmeDB = new Application_Model_CheckImeiLogRealmeDB();
            //todo log
            $QCheckImeiLogRealmeDB->insert(array(
                'result' => $return,
                // 'info' => '',
                'imei' => $imei,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
                'store_id' => $store
            ));

            //end check trond db realme
        }
    }

    $db->beginTransaction();

    if ($imeis && $timing_type != 2) {

        $QTimimigCheck = new Application_Model_TimingCheckRealme();
        foreach ($imeis as $key => $imei) {
            $imei = My_Util::escape_string($imei);
            echo $imei;
            $imei = TRIM($imei);
            $checkImei = $QTimimigCheck->checkImeiDealer($imei, $store);
            //print_r($checkImei['status']);
            echo $checkImei['status'];
            $checkImeiTA = $QTimimigCheck->checkImeiTranAnh($imei, $store);
//            $checkImeiBrs = $QTimimigCheck->checkImeiBrandshop($imei, $store);
            //$checkImeiVinPro = $QTimimigCheck->checkImeiVinpro($imei);

            if (!empty($checkImeiVinPro)) {
                echo '<script>
                    parent.palert("IMEI ' . $imei . ' thuộc cửa hàng VinPro. Bạn không thể báo Imei này ở cửa hàng không thuộc VinPro . Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                    parent.alert("IMEI ' . $imei . ' thuộc cửa hàng VinPro. Bạn không thể báo Imei này ở cửa hàng không thuộc VinPro . Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                </script>';
                exit;
            }

            // echo "<pre>";print_r($checkImeiBrs);die;

//            if (isset($checkImeiBrs) && $checkImeiBrs && $checkImeiBrs['check_sell_in'] == 1 && $checkImeiBrs['check_brs'] == 1 && $checkImeiBrs['is_brand_shop'] != 1 && $checkImeiBrs['type'] != 1) {
//                echo '<script>
//                    parent.palert("IMEI ' . $imei . ' thuộc cửa hàng Brandshop. Bạn không thể báo Imei này ở cửa hàng không thuộc Brandshop . Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
//                    parent.alert("IMEI ' . $imei . ' thuộc cửa hàng Brandshop. Bạn không thể báo Imei này ở cửa hàng không thuộc Brandshop . Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
//                </script>';
//                exit;
//            }

            $list_good_sell_to_area = [48, 50, 51, 52];
            $is_sell_to_area = $QTiming->isImeiSellToArea($imei, $list_good_sell_to_area);
            if (isset($checkImeiTA) && $checkImeiTA && $checkImeiTA['dealer'] == 1 && $checkImeiTA['store'] == 1) {
                # code...
            } else {
                if ($is_sell_to_area) {
                    $is_right_dealer = $QTiming->isRightDealerSellToArea($imei, $store);

                    if (! $is_right_dealer) {
                        echo '<script>
                            parent.palert("IMEI ' . $imei . ' không thuộc Dealer khu vực. Vui lòng liên hệ ASM/ hoặc Sales Admin nếu có thắc mắc");
                            parent.alert("IMEI ' . $imei . ' không thuộc Dealer khu vực. Vui lòng liên hệ ASM/ hoặc Sales Admin nếu có thắc mắc");
                        </script>';
                        exit;
                    }
                } elseif (empty($checkImei['status'])) {
                    echo '<script>
                            parent.palert("IMEI ' . $imei . ' không thuộc cửa hàng này. Có thể đây là máy mượn/chuyển giữa các cửa hàng. Công ty không tính doanh số với các máy được báo cáo sai cửa hàng. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                            parent.alert("IMEI ' . $imei . ' không thuộc cửa hàng này. Có thể đây là máy mượn/chuyển giữa các cửa hàng. Công ty không tính doanh số với các máy được báo cáo sai cửa hàng. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc.");
                        </script>';
                    exit;
                }
            }
        }
    } else {
        foreach ($imeis as $key => $imei) {
            $imei = TRIM($imei);
            $where_imei_dealer = $QWebImei->getAdapter()->quoteInto('imei_sn = ?', $imei);
            $imei_dealer = $QWebImei->fetchRow($where_imei_dealer);
            if (!empty($imei_dealer)) {
                $imei_dealer = $imei_dealer->toArray();
            }
            if (!in_array($store_info['d_parent_id'], array($imei_dealer['distributor_id'], $imei_dealer['d_parent'])) && !in_array($store_info['d_id'], array($imei_dealer['distributor_id'], $imei_dealer['d_parent']))
            ) {
                echo '<script>
                    parent.palert("IMEI ' . $imei . ' không thuộc cửa hàng này. Có thể đây là máy mượn/chuyển giữa các cửa hàng. Công ty không tính doanh số với các máy được báo cáo sai cửa hàng. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc!!");
                    parent.alert("IMEI ' . $imei . ' không thuộc cửa hàng này. Có thể đây là máy mượn/chuyển giữa các cửa hàng. Công ty không tính doanh số với các máy được báo cáo sai cửa hàng. Vui lòng liên hệ ASM/Sales Admin nếu có thắc mắc!!");
                </script>';
                exit;
            }
        }
    }

    // call store check ----------------------

    $formatedDate = (isset($temp[2]) ? $temp[2] : '1970') . '-' . (isset($temp[1]) ? $temp[1] : '01') . '-' . (isset($temp[0]) ? $temp[0] : '01');
    $staff_id_edit = false; // dùng để check staff đứng shop
    $QStoreStaffLog = new Application_Model_StoreStaffLog();
    // $QTime          = new Application_Model_Time();
    //kiểm tra approved hay chưa khi edit
    if ($id) {
        $where = $QTiming->getAdapter()->quoteInto('id = ?', $id);
        $_timing = $QTiming->fetchRow($where);

        if (!$_timing) {
            echo '<script>
                    parent.palert("Không tìm thấy báo cáo bạn muốn sửa!");
                </script>';
            exit;
        }

        if ($_timing['approved_at']) {
            echo '<script>
                    parent.palert("Báo cáo này đã được xác nhận, bạn không thể sửa!");
                </script>';
            exit;
        }

        // Kiểm tra xem nó phải sales quản lý hay không,
        //      trường hợp edit mà người edit khác với người chấm công
        if ($userStorage->id != $_timing['staff_id'] && !empty($_timing['store']) && !empty($_timing['from']) && !$QStoreStaffLog->belong_to($userStorage->id, $_timing['store'], $_timing['from'], true)) {
            echo '<script>
                    parent.palert("Vào ngày ' . date('Y-m-d', strtotime($_timing['from']))
                . ' bạn không phải là Sales của Store '
                . (isset($store_cache[$_timing['store']]) && $store_cache[$_timing['store']] ? ('[' . $store_cache[$_timing['store']] . ']') : ' này')
                . ' Bạn không có quyền chỉnh sửa chấm công này.");
                </script>';
            exit;
        }

        // nếu sales edit chấm công thì lấy user id = staff_id của chấm công
        if ($userStorage->id != $_timing['staff_id'])
            $staff_id_edit = $_timing['staff_id'];
    }

    // nếu không phải thì lấy user id hiện tại
    if (!$staff_id_edit)
        $staff_id_edit = $userStorage->id;

    // Kiểm tra xem staff này vào ngày chấm công có thuộc store tương ứng chưa.
    // Nếu chưa thì chặn không cho chấm công
    if (!$QStoreStaffLog->belong_to($staff_id_edit, $store, $formatedDate)) {
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id_edit);
        $staff_check = $QStaff->fetchRow($where);
        $staff_name_check = @$staff_check['firstname'] . ' ' . @$staff_check['lastname'];

        echo '<script>
                parent.palert("Vào ngày ' . $formatedDate . ', ' . $staff_name_check . ' không thuộc Store '
            . (isset($store_cache[$store]) && $store_cache[$store] ? ('[' . $store_cache[$store] . ']') : ' này')
            . '. Để chấm công, ' . $staff_name_check . ' phải là PG/PB hoặc Sales ở Store này. Vui lòng liên hệ Sales Admin để được hỗ trợ.");
                parent.alert("Vào ngày ' . $formatedDate . ', ' . $staff_name_check . ' không thuộc Store '
            . (isset($store_cache[$store]) && $store_cache[$store] ? ('[' . $store_cache[$store] . ']') : ' này')
            . '. Để chấm công, ' . $staff_name_check . ' phải là PG/PB hoặc Sales ở Store này. Vui lòng liên hệ Sales Admin để được hỗ trợ.");
            </script>';
        exit;
    }


    /* neu la sale oppo thi kiem tra shop do co sale real me khong
     * neu co thi khong cho bao so
     * phat sinhh khi cho sale cua oppo ban may realme
     *
     * */
//    if($userStorage->title==183){
    if (in_array($userStorage->title, [183, 190])) {
        if ($QStoreStaffLog->checkRealmeSale($store, $formatedDate)) {
            $QStaff = new Application_Model_Staff();
            $where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id_edit);
            $staff_check = $QStaff->fetchRow($where);
            $staff_name_check = @$staff_check['firstname'] . ' ' . @$staff_check['lastname'];

            echo '<script>
                parent.palert("Vào ngày ' . $formatedDate . ' đã có Sale của Realme phụ trách cửa hàng này nên bạn không được báo số. Đây là quy định của công ty, liên hệ Sale Admin hoặc ASM nếu có thắc mắc.");
                parent.alert("Vào ngày ' . $formatedDate . ' đã có Sale của Realme phụ trách cửa hàng này nên bạn không được báo số. Đây là quy định của công ty, liên hệ Sale Admin hoặc ASM nếu có thắc mắc.");

            </script>';
            exit;
        }
    }


    //
    //check reported or not yet
    // check ngoại lệ region
    $QCasual = new Application_Model_CasualWorker();
    $casuals = $QCasual->get_cache();

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $where = array();
    $where[] = $QTiming->getAdapter()->quoteInto('shift = ?', $shift);
    $where[] = $QTiming->getAdapter()->quoteInto('date(`from`) = ?', $formatedDate);
    $where[] = $QTiming->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);

    if ($id)
        $where[] = $QTiming->getAdapter()->quoteInto('id <> ?', $id);

    if ($userStorage->group_id == SALES_ID || $userStorage->group_id == LEADER_ID ||
        (isset($casuals[$userStorage->id]) && $casuals[$userStorage->id]['status'] == 1)
    )
        $where[] = $QTiming->getAdapter()->quoteInto('store = ?', $store);

    $checked_existed = $QTiming->fetchRow($where);

    $formatedFrom = $start . ':00';
    $formatedTo = $end . ':00';

    $data = array(
        'shift' => $shift ? $shift : 1,
        'from' => $new_date,
        'to' => $formatedDate . ' ' . $formatedTo,
        'note' => trim($note),
        'store' => $store,
    );


    if ($imeis) {
        $count_vl = array_count_values($imeis);
        foreach ($count_vl as $key => $value) {
            if ($value > 1) {
                echo '<script>
                            parent.palert("IMEI: ' . $key . ' bị nhập 2 lần trong cùng một ca.");
                            parent.alert("IMEI: ' . $key . ' bị nhập 2 lần trong cùng một ca.");
                        </script>';
                exit;
            }
        }
    }

    if (isset($imeis) && $imeis)
        foreach ($imeis as $k => $imei)
            $imeis[$k] = trim($imei);

    // Kiểm tra product_id và model có hợp lệ hay không
    // Kiểm tra IMEI với product/model có khớp/thiếu hay không
    if (is_array($products) and is_array($models)) {


        $QGood = new Application_Model_GoodRealme();
        $products_list = $QGood->get_cache();

        $QGoodColor = new Application_Model_GoodColorRealme();
        $color_list = $QGoodColor->get_cache();

        foreach ($products as $k => $product) {

            $where_product_del = array();
            $where_product_del[] = $QGood->getAdapter()->quoteInto('id = ?', $product);
            $good = $QGood->fetchRow($where_product_del);
            if ($good and $good['del'] == 1) {
                echo '<script>
                        parent.palert("Sản phẩm thứ ' . ($k + 1) . ' không hợp lệ (đã bị khóa). Vui lòng chọn lại sản phẩm từ danh sách.");
                        parent.alert("Sản phẩm thứ ' . ($k + 1) . ' không hợp lệ (đã bị khóa). Vui lòng chọn lại sản phẩm từ danh sách.");
                    </script>';
                exit;
            }

            // Trường hợp product_id không hợp lệ
            if (!is_numeric($product) || $product < 1) {
                echo '<script>
                            parent.palert("Sản phẩm thứ ' . ($k + 1) . ' không hợp lệ. Vui lòng chọn lại sản phẩm từ danh sách.");
                            parent.alert("Sản phẩm thứ ' . ($k + 1) . ' không hợp lệ. Vui lòng chọn lại sản phẩm từ danh sách.");
                    	</script>';
                exit;
            }

            // kiểm tra có product nào thiếu IMEI ko
            if (empty($imeis[$k])) {
                echo '<script>
                            parent.palert("Sản phẩm thứ ' . ($k + 1) . ' thiếu IMEI. Vui lòng bổ sung.");
                            parent.alert("Sản phẩm thứ ' . ($k + 1) . ' thiếu IMEI. Vui lòng bổ sung.");
                    	</script>';
                exit;
            }

            // Kiểm tra có product/model nào không khớp IMEI ko
            $where = $QWebImei->getAdapter()->quoteInto('imei_sn = ?', TRIM($imeis[$k]));
            $imei = $QWebImei->fetchRow($where);

            // Product ko hợp lệ
            if ($imei['good_id'] != $product) {
                echo '<script>
                            parent.palert("Sản phẩm thứ ' . ($k + 1) . ', IMEI [' . $imeis[$k] . '] không phải của máy [' . $products_list[$product] . ']. Vui lòng chọn lại sản phẩm từ danh sách.");
                            parent.alert("Sản phẩm thứ ' . ($k + 1) . ', IMEI [' . $imeis[$k] . '] không phải của máy [' . $products_list[$product] . ']. Vui lòng chọn lại sản phẩm từ danh sách.");
                        </script>';
                exit;
            }

            // kiểm tra model

            if (!is_numeric($models[$k]) || $models[$k] < 1) {
                echo '<script>
                            parent.palert("Sản phẩm thứ ' . ($k + 1) . ' màu sắc không hợp lệ. Vui lòng chọn lại màu sắc từ danh sách.");
                            parent.alert("Sản phẩm thứ ' . ($k + 1) . ' màu sắc không hợp lệ. Vui lòng chọn lại màu sắc từ danh sách.");
                        </script>';
                exit;
            }
            if ($timing_type != 2) {
                // Kiểm tra màu

                if ($imei['good_color'] != $models[$k]) {
                    echo '<script>
                            parent.palert("Sản phẩm thứ ' . ($k + 1) . ', IMEI [' . $imeis[$k] . '] không phải màu [' . $color_list[$models[$k]] . ']. Vui lòng chọn lại màu từ danh sách.");
                            parent.alert("Sản phẩm thứ ' . ($k + 1) . ', IMEI [' . $imeis[$k] . '] không phải màu [' . $color_list[$models[$k]] . ']. Vui lòng chọn lại màu từ danh sách.");
                        </script>';
                    exit;
                }
            }
        }
    }
    /*echo '<pre>'; print_r(123); die;
    die;*/
    $QLog = new Application_Model_LogRealme();
    $ip = $this->getRequest()->getServer('REMOTE_ADDR');

    $ts = "";
    if (isset($timing_sales_ids)) {
        foreach ($timing_sales_ids as $k => $v) {
            $ts .= $v . ",";
        }

        $ts = trim($ts, ",");
    }

    $diff_ids = array();
    $diff_accessories_ids = array();
    $QTimingAccessories = new Application_Model_TimingAccessoriesRealme();
    if ($id) {

        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $userStorage->id;

        $data['status'] = 1;
        $data['approved_by'] = $userStorage->id;
        $data['approved_at'] = date('Y-m-d H:i:s');

        $where = $QTiming->getAdapter()->quoteInto('id = ?', $id);
        $QTiming->update($data, $where);

        //check old timing sale
        $where = $QTimingSale->getAdapter()->quoteInto('timing_id = ?', $id);
        $old_timing = $QTimingSale->fetchAll($where);

        foreach ($old_timing as $item) {

            if (!in_array($item->id, $timing_sales_ids) or !$timing_sales_ids) {

                $diff_ids[] = $item->id;

            }
        }

        //check old timing accossories
        $where = $QTimingAccessories->getAdapter()->quoteInto('timing_id = ?', $id);
        $old_assessories = $QTimingAccessories->fetchAll($where);


        foreach ($old_assessories as $item) {

            if (!in_array($item->id, $timing_accessories_ids) or !$timing_accessories_ids) {

                $diff_accessories_ids[] = $item->id;
            }
        }

        $info = "TIMING - Update (" . $id . ") - Timing sale (" . $ts . ") - Info (" . serialize($data) . ")";
    } else {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $userStorage->id;
        $data['staff_id'] = $userStorage->id;

        $data['status'] = 1;
        $data['approved_by'] = $userStorage->id;
        $data['approved_at'] = date('Y-m-d H:i:s');


        $id = $QTiming->insert($data);
        $info = "TIMING - Add (" . $id . ") - Timing sale (" . $ts . ") - Info (" . serialize($data) . ")";
    }

    $info .= " - IMEIs (" . serialize($imeis) . ")";

    //todo log
    $QLog->insert(array(
        'info' => $info,
        'user_id' => $userStorage->id,
        'ip_address' => $ip,
        'time' => date('Y-m-d H:i:s'),
    ));

    //insert timing sale
    if (is_array($products) and is_array($models)) {
        foreach ($products as $k => $product) {
            if ($product and isset($models[$k]) and $models[$k]) {
                try {
                    //insert timing sale
                    if (isset($timing_sales_ids[$k]) and $timing_sales_ids[$k]) {
                        $timing_sales_id = $timing_sales_ids[$k];
                        $where = $QTimingSale->getAdapter()->quoteInto('id = ?', $timing_sales_id);

                        $QTimingSale->update(array(
                            'model_id' => $models[$k],
                            'product_id' => $product,
                            'timing_id' => $id,
                            'imei' => (isset($imeis[$k]) ? $imeis[$k] : null),
                            'customer_name' => ((isset($customer_names[$k]) and $customer_names[$k]) ? $customer_names[$k] : null),
                            'phone_number' => ((isset($phone_numbers[$k]) and $phone_numbers[$k]) ? $phone_numbers[$k] : null),
                            'address' => ((isset($addresses[$k]) and $addresses[$k]) ? $addresses[$k] : null),
                            'amortization' => ($amortization[$k] ? $amortization[$k] : 0)
                        ), $where);
                        $where_install[] = $installment_model->getAdapter()->quoteInto('imei_sn = ?', $imeis[$k]);
                        $where_install[] = $installment_model->getAdapter()->quoteInto('timing_sale_id = ?', $timing_sales_ids[$k]);

                        $data_installment = array(
                            'imei_sn' => (isset($imeis[$k]) ? $imeis[$k] : null),
                            'contract_number' => $installment_contract[$k],
                            'installment_company' => $installment_company[$k],
                            'customer_name' => ((isset($customer_names[$k]) and $customer_names[$k]) ? $customer_names[$k] : null),
                            'customer_phone' => ((isset($phone_numbers[$k]) and $phone_numbers[$k]) ? $phone_numbers[$k] : null),
                            'customer_address' => ((isset($addresses[$k]) and $addresses[$k]) ? $addresses[$k] : null),
                        );
                        $installment_model->update($data_installment, $where_install);
                    } else {
                        if (isset($models[$k]) and $models[$k]) {

                            $timing_sales_id = $QTimingSale->insert(array(
                                'model_id' => $models[$k],
                                'product_id' => $product,
                                'timing_id' => $id,
                                'imei' => (isset($imeis[$k]) ? TRIM($imeis[$k]) : null),
                                'customer_name' => ((isset($customer_names[$k]) and $customer_names[$k]) ? $customer_names[$k] : null),
                                'phone_number' => ((isset($phone_numbers[$k]) and $phone_numbers[$k]) ? $phone_numbers[$k] : null),
                                'address' => ((isset($addresses[$k]) and $addresses[$k]) ? $addresses[$k] : null),
                                'amortization' => ($amortization[$k] ? $amortization[$k] : 0)
                            ));

                            $data_installment = array(
                                'imei_sn' => (isset($imeis[$k]) ? TRIM($imeis[$k]) : null),
                                'timing_date' => $new_date,
                                'store_id' => $store,
                                'staff_id' => $userStorage->id,
                                'contract_number' => $installment_contract[$k],
                                'installment_company' => $installment_company[$k],
                                'timing_sale_id' => $timing_sales_id,
                                'customer_name' => ((isset($customer_names[$k]) and $customer_names[$k]) ? $customer_names[$k] : null),
                                'customer_phone' => ((isset($phone_numbers[$k]) and $phone_numbers[$k]) ? $phone_numbers[$k] : null),
                                'customer_address' => ((isset($addresses[$k]) and $addresses[$k]) ? $addresses[$k] : null),
                                'created_at' => date('Y-m-d H:i:s')
                            );
                            $installment_model->insert($data_installment);
                        }
                    }
//                    if ($app_install <> 0) {
//                        $where_app = $QApp_installing->getAdapter()->quoteInto('imei_sn = ?', (isset($imeis[$k]) ? $imeis[$k] : ''));
//                        $imei_app  = $QApp_installing->fetchAll($where_app);
//                        $imei_app  = $imei_app->toArray();
//                        if (empty($imei_app)) {
//                            $data_app = array(
//                                'imei_sn'      => (isset($imeis[$k]) ? $imeis[$k] : null),
//                                'timing_id'    => $id,
//                                'created_date' => date('Y-m-d H:i:s'),
//                                'app_id'       => $app_install,
//                                'staff_id'     => $userStorage->id
//                            );
//                            $QApp_installing->insert($data_app);
//                        }
//                    }
                } catch (Exception $e) {
                    $db->rollback();
//                    echo $e->getMessage();
//                    die;
//                    echo '<pre>';
//                    print_r($products);
//                    die;

//                    echo '<pre>';
//                    print_r($return);
//                    die;


                    $QImeiKpi = new Application_Model_ImeiKpiRealme();
                    $where = $QImeiKpi->getAdapter()->quoteInto('imei_sn = ?', TRIM($imeis[$k]));
                    $infoImei = $QImeiKpi->fetchRow($where);


                    $QStore = new Application_Model_Store();
                    $where = $QStore->getAdapter()->quoteInto('id = ?', $infoImei['store_id']);
                    $infoStore = $QStore->fetchRow($where);

                    $QStaff = new Application_Model_Staff();
                    $where = $QStaff->getAdapter()->quoteInto('id = ?', $infoImei['pg_id']);
                    $infoStaff = $QStaff->fetchRow($where);
                    $where = $QTimingSale->getAdapter()->quoteInto('imei = ?', TRIM($imeis[$k]));
                    $infoTimingSale = $QTimingSale->fetchRow($where);

                    // echo $infoTimingSale['id'];die;
                    if (isset($infoImei) && $infoImei) {
                        //Tu dong lai test dang lưu duplicate sai
//                        echo '<script>
//                                    parent.palert("IMEI '
//                            . $imeis[$k] . ' này đã được báo cáo lúc ['
//                            . $infoImei['timing_date']
//                            . '] <a href=\"#\" data-staff-first=\"'
//                            . $infoImei['pg_id'] . '\" data-timing-sales-id=\"'
//                            . $infoTimingSale['id'] . '\" data-imei=\"'
//                            . $imeis[$k] . '\" data-case=\"IMEI '
//                            . $imeis[$k] . ' này đã được báo cáo lúc ['
//                            . $infoImei['timing_date'] . ']\" data-checksum=\"' . sha1(md5($imeis[$k]) . $infoTimingSale['id']) . '\" class=\"send_notify\">Bấm vào đây</a> để gửi yêu cầu xử lý.");
//                                    parent.alert("IMEI ' . $imeis[$k] . ' đã  báo cáo vào lúc [' . $infoImei['timing_date'] . '] Cuộn lên đầu trang để chọn chức năng gửi báo cáo trùng IMEI.");
//                                </script>';
//                        exit;

                        echo '<script>
                                parent.palert("IMEI '
                            . $imei . ' này đã được báo cáo lúc ['
                            . date('d/m/Y H:i:s', strtotime($info['date']))
                            . ']");
                                parent.alert_confirm("IMEI ' . $imei . ' đã  báo cáo vào lúc [' . date('d/m/Y H:i:s', strtotime($info['date'])) . '] Trong hệ thống Realme. Bạn có muốn báo trùng không ?.","' . $imei . '","' . $date . '",' . $store . ',"' . $customer_names[$k] . '","' . $phone_numbers[$k] . '","' . $addresses[$k] . '",' . $shift . ');
                </script>';
                        exit;

                    } else {
                        if ($e->getCode() == 23000) {
                            echo '<script>
                                        parent.palert("Sản phẩm thứ ' . ($k + 1) . ', IMEI [' . $imeis[$k] . '] bị trùng. Vui lòng kiểm tra lại.");
                                        parent.alert("Sản phẩm thứ ' . ($k + 1) . ', IMEI [' . $imeis[$k] . '] bị trùng. Vui lòng kiểm tra lại.");
                                    </script>';
                            exit;
                        } else {
                            echo '<script>
                                        parent.palert("' . $e->getMessage() . '");
                                        parent.alert("' . $e->getMessage() . '");
                                    </script>';
                            exit;
                        }
                    }
                }


                if (isset($photos[$k]) and $photos[$k]
                    and $timing_sales_id
                ) {

                    try {
                        $month = substr($formatedDate, 0, -3);

                        //move from temp to folder
                        $located_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'timing_sales_new' . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $formatedDate . DIRECTORY_SEPARATOR . $timing_sales_id;
                        if (!is_dir($located_dir))
                            @mkdir($located_dir, 0777, true);

                        $tem = explode('_located_', $photos[$k]);

                        if (isset($tem[1]) and $tem[1]) {
                            $uniqid = $tem[0];
                            $file_name = strtolower($tem[1]);

                            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'timing_sales_new' . DIRECTORY_SEPARATOR;
                            $uploaded_dir .= 'temp' . DIRECTORY_SEPARATOR . $userStorage->id . DIRECTORY_SEPARATOR . $uniqid . DIRECTORY_SEPARATOR . $file_name;


                            if (is_file($uploaded_dir)) {

                                $info = pathinfo($uploaded_dir);

                                $new_file_name = md5($timing_sales_id . FILENAME_SALT) . '.' . $info['extension'];

                                copy($uploaded_dir, $located_dir . DIRECTORY_SEPARATOR . $new_file_name);
                            }

//                            //remove other file
//                            $files = glob($located_dir . '*'); // get all file names
//                            foreach ($files as $file) { // iterate files
//                                if (is_file($file) and ! strstr($file, $new_file_name))
//                                    unlink($file); // delete file
//                            }

                            $data = array('photo' => $new_file_name);

                            $where = $QTimingSale->getAdapter()->quoteInto('id = ?', $timing_sales_id);

                            $QTimingSale->update($data, $where);
                        }
                    } catch (Exception $e) {
                        echo '<script>
                                    parent.palert("Please input valid file.");
                                </script>';
                        exit;
                    }
                }
            }
        }

        //unlink temp folder
        try {
            $dirPath = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'timing_sales_new' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $userStorage->id . DIRECTORY_SEPARATOR;
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
                $path->isFile() ? @unlink($path->getPathname()) : @rmdir($path->getPathname());
            }
            @rmdir($dirPath);
        } catch (Exception $e) {

        }
    }

    //insert timing accessories
    if (is_array($accessories) and is_array($num_accessories)) {
        foreach ($accessories as $k => $value) {
            if ($accessories and isset($num_accessories[$k]) and $num_accessories[$k]) {
                try {
                    //Nếu chưa approved thì cho sửa
                    if (!$_timing->approved_accessories_by) {
                        if (isset($timing_accessories_ids[$k]) and $timing_accessories_ids[$k]) {
                            $timing_accessories_id = $timing_accessories_ids[$k];
                            $where = $QTimingAccessories->getAdapter()->quoteInto('id = ?', $timing_accessories_id);

                            $QTimingAccessories->update(array(
                                'good_id' => $value,
                                'quantity' => $num_accessories[$k],
                                'timing_id' => $id,
                            ), $where);
                        } else {
                            $timing_accessories_id = $QTimingAccessories->insert(array(
                                'good_id' => $value,
                                'quantity' => $num_accessories[$k],
                                'timing_id' => $id,
                            ));
                        }
                    }
                } catch (Exception $e) {
                    echo '<script>
                                parent.palert("' . $e->getMessage() . '");
                                parent.alert("' . $e->getMessage() . '");
                            </script>';
                    exit;
                }
            }
        }
    }

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}

if ($diff_ids) {
    $old_date = explode(' ', $old_timing_2['from']);
    $old_date = $old_date[0];
    $old_month = substr($old_date, 0, -3);

    //delete unused timing sales
    $where = $QTimingSale->getAdapter()->quoteInto('id IN (?)', $diff_ids);
    $QTimingSale->delete($where);

    //delete photo folder
    foreach ($diff_ids as $un_timing_sales_id) {
        //unlink folder
        try {
            $dirPath = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'timing_sales_new' . DIRECTORY_SEPARATOR . $old_month . DIRECTORY_SEPARATOR . $old_date . DIRECTORY_SEPARATOR . $un_timing_sales_id . DIRECTORY_SEPARATOR;
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
                $path->isFile() ? @unlink($path->getPathname()) : @rmdir($path->getPathname());
            }
            rmdir($dirPath);
        } catch (Exception $e) {

        }
    }
}

if ($diff_accessories_ids) {
    //delete unused timing accessories
    //Nếu chưa approved thì cho xóa
    if (!$_timing->approved_accessories_by) {
        $where = $QTimingAccessories->getAdapter()->quoteInto('id IN (?)', $diff_accessories_ids);
        $QTimingAccessories->delete($where);
    }
}

$db->commit();

echo '<script>parent.location.href="' . ($back_url ? $back_url : '/timing-realme') . '"</script>';
exit;
