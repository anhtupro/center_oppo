<?php
$staff_id = $this->getRequest()->getParam('staff_id');
$from     = $this->getRequest()->getParam('from', date('01/m/Y'));
$to       = $this->getRequest()->getParam('to', date('d/m/Y'));

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!$userStorage || !isset($userStorage->id))
    $this->_redirect(HOST);

$QAsm = new Application_Model_Asm();
$asm = $QAsm->get_cache($userStorage->id);
if (!isset($asm)
    || !isset($asm['province'])
    || !is_array($asm['province'])
    || !count($asm['province']))
    $this->_redirect(HOST);

$provinces = $asm['province'];
$QStaff = new Application_Model_Staff();
$this->view->staffs = $QStaff->fetchForSearch($provinces);
$this->view->params = array(
    'from'     => $from,
    'to'       => $to,
    'staff_id' => $staff_id,
);

if (!$staff_id) return;

$where = $QStaff->getAdapter()->quoteInto('id = ?', intval($staff_id));
$staff  = $QStaff->fetchRow($where);

if (!$staff) return;
$check = false;

foreach ($provinces as $key => $value) {
    if ($staff['regional_market'] == $value)
        $check = true;
}

if (!$check) return;

$result = My_Kpi::fetchByMonth(
    $staff_id,
    date_create_from_format("d/m/Y", $from)->format("Y-m-d"),
    date_create_from_format("d/m/Y", $to)->format("Y-m-d")
);

$data = array();
$title_list = array();
$db = Zend_Registry::get('db');

foreach ($result as $_type => $query) {
    $_result = $db->query($query);

    foreach ($_result as $_sellout) {
        $title_list[ $_sellout['year'] ][ $_sellout['month'] ] = 1;
        $data[ $_type ][ $_sellout['good_id'] ][ $_sellout['year'] ][ $_sellout['month'] ] = array(
            'quantity'      => $_sellout['quantity'],
            'activated'     => $_sellout['activated'],
            'non_activated' => $_sellout['non_activated'],
            'total_value'   => $_sellout['total_value'],
        );
    }
}

foreach ($data as $_type => $_data) {
    foreach ($_data as $_good_id => $_years) {
        foreach ($title_list as $year => $months) {
            if (!isset($_years[ $year ])) {
                foreach ($months as $month => $value) {
                    $data[ $_type ][ $_good_id ][ $year ][ $month ] = array(
                        'quantity'      => 0,
                        'activated'     => 0,
                        'non_activated' => 0,
                        'total_value'   => 0,
                    );
                }
            }
        }

        foreach ($_years as $_year => $_months) {

            foreach ($title_list[ $_year ] as $month => $value) {
                if (!isset($_months[ $month ])) {
                    $data[ $_type ][ $_good_id ][ $_year ][ $month ] = array(
                        'quantity'      => 0,
                        'activated'     => 0,
                        'non_activated' => 0,
                        'total_value'   => 0,
                    );
                }
            }
        }
    }

}

$QGood = new Application_Model_Good();
$QGoodColor = new Application_Model_GoodColor();
$this->view->sellout     = $data;
$this->view->title_list  = $title_list;
$this->view->goods       = $QGood->get_cache();
$this->view->good_colors = $QGoodColor->get_cache();
