<?php

    class DateController extends My_Controller_Action
    {

        public function saturdaySettingAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $company_group =$this->getRequest()->getParam('company_group');
            $month = $this->getRequest()->getParam('month', date('m'));
            $year = $this->getRequest()->getParam('year', date('Y'));
            $number = $this->getRequest()->getParam('number', 0);
            $submit = $this->getRequest()->getParam('submit', 0);
            
            $params = array(
                'month' => $month,
                'year'  => $year,
            );
            $QAllDateAdvanced = new Application_Model_AllDateAdvanced();
            $QSaturday       = new Application_Model_Saturday();
            $this->view->params = $params;
            
            $where   = array ();
            $where[] = $QSaturday->getAdapter ()->quoteInto ( 'month = ?', $month );
            $where[] = $QSaturday->getAdapter()->quoteInto('year = ?', $year);
            $result_staturday = $QSaturday->fetchRow ( $where );
            $this->view->number = !empty($result_staturday) ? $result_staturday->toArray()['number'] : $number;
            if(!empty($submit))
            {
                
               
                if(!empty($result_staturday)){
                    $data_update = array(
                        'number' => $number
                    );
                    $QSaturday->update($data_update, $where);
                }else{
                    $data_insert = array(
                        'month' => $month,
                        'year'  => $year,
                        'number' => $number
                    );
                    $QSaturday->insert($data_insert);
                }
                $flashMessenger->setNamespace('success')->addMessage("Done");
                $this->_redirect('/date/list-date-setting?month=' . $params['month'] . '&year=' . $params['year']);
            }

            $QCompanyGroup = new Application_Model_CompanyGroup();
            $listCompanyGroup = $QCompanyGroup->getAll();
            $this->view->listCompanyGroup = $listCompanyGroup;
        }

        public function listSaturdayAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $month = $this->getRequest()->getParam('month', date('m'));
            $year = $this->getRequest()->getParam('year', date('Y'));

            $params = array(
                'month' => $month,
                'year' => $year,
            );

            $this->view->params = $params;

            $QAllDateAdvanced = new Application_Model_AllDateAdvanced();
            $data =$QAllDateAdvanced->getSaturday($params);

            $this->view->data = $data;
            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }

        // public function dateSettingAction()
        // {
        //     $flashMessenger = $this->_helper->flashMessenger;
            
        //     $date_edit = $this->getRequest()->getParam('date_edit');
        //     $edit = $this->getRequest()->getParam('edit');
        //     $date_edit_param = !empty($date_edit) ? DateTime::createFromFormat('d/m/Y', $date_edit)->format('Y-m-d') : null;
        //     $submit = $this->getRequest()->getParam('submit');

        //     $QAllDateAdvanced = new Application_Model_AllDateAdvanced();
        //     if(!empty($submit))
        //     {
        //         $date = $this->getRequest()->getParam('date');
        //         $from_date = $this->getRequest()->getParam('from_date');
        //         $to_date = $this->getRequest()->getParam('to_date');

        //         $public = $this->getRequest()->getParam('public');
        //         $multiple = $this->getRequest()->getParam('multiple');

        //         $date_param = !empty($date) ? DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d') : null;
        //         $from_date_param = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
        //         $to_date_param = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;

        //         $category = $this->getRequest()->getParam('category');
        //         if(in_array($category, array(2,3)))
        //         {
        //             $company_group = $this->getRequest()->getParam('company_group');
        //             $staff_titles = $this->getRequest()->getParam('staff_titles');
        //             $params_insert = array(
        //                 'company_group' => $company_group,
        //                 'category' => $category,
        //                 'date' => $date_param,
        //             );
        //             $QAllDateAdvanced->_insertDateAdvanced($params_insert);
        //             $flashMessenger->setNamespace('success')->addMessage("Thêm dữ liệu thành công");
        //             $this->_redirect('/date/list-date-setting');
        //         }
        //         else
        //         {
        //             $company_group = $this->getRequest()->getParam('company_group');
        //             $staff_titles = $this->getRequest()->getParam('staff_titles');
        //             $params_insert = array(
        //                 'company_group' => $company_group,
        //                 'category' => $category,
        //                 'from_date' => $from_date_param,
        //                 'to_date' => $to_date_param,
        //                 'public' => $public,
        //                 'multiple' => $multiple,
        //             );
        //             if(strtotime($params['from_date']) <= strtotime($params['to_date']))
        //             {
        //                 $QAllDateAdvanced->_insertMultiDateAdvanced($params_insert);
        //                 $flashMessenger->setNamespace('success')->addMessage("Thêm dữ liệu thành công");
        //                 $this->_redirect('/date/list-date-setting');
        //             }
        //             else
        //             {
        //                 $QAllDateAdvanced->_insertMultiDateAdvanced($params_insert);
        //                 $flashMessenger->setNamespace('error')->addMessage("From date phải bé hơn To date");
        //             }
                    
        //         }
        //     }
            
        //     $category_edit = $this->getRequest()->getParam('category_edit');
        //     $params = array(
        //         'date' => $date_edit_param,
        //         'category' => $category_edit,
        //     );

        //     $array_group = $QAllDateAdvanced->getGroup($params);

        //     $QDateCategory = new Application_Model_DateCategory();
        //     $allDateCategory = $QDateCategory->getAllSelect();
        //     $this->view->allDateCategory = $allDateCategory;

        //     $QCompanyGroup = new Application_Model_CompanyGroup();
        //     $listCompanyGroup = $QCompanyGroup->getAll();
        //     $this->view->listCompanyGroup = $listCompanyGroup;
        //     $this->view->group_selected = $array_group;
        //     $this->view->category_selected = $category_edit;
        //     $this->view->date = $date_edit;

        //     $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        //     $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        // }

        public function dateSettingAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $date = $this->getRequest()->getParam('date');
            $submit = $this->getRequest()->getParam('submit', 0);
            $QAllDateAdvanced = new Application_Model_AllDateAdvanced();
            
            if(!empty($date))
            {
                $list = $QAllDateAdvanced->getListByDate($date);
                $this->view->list = $list;
                $this->view->date = $date;
            }

            if(!empty($submit))
            {
                $from_date = $this->getRequest()->getParam('from_date');
                $to_date = $this->getRequest()->getParam('to_date');
                $json_input = $this->getRequest()->getParam('json_input');

                $from_date = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
                $to_date = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;
                
                $input = json_decode($json_input, true);
                
                $array_group_edit = array();
                foreach ($input as $key => $value) 
                {
                    $array_group_edit[] = $value['type_id'];
                }

                $params = array(
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'list_type_id' => $array_group_edit,
                    'input' => $input,
                );

                $QAllDateAdvanced->insertDate($params);
                $flashMessenger->setNamespace('success')->addMessage("Thêm dữ liệu thành công");
                $this->_redirect('/date/list-date-setting');
            }
            

            $QCompanyGroup = new Application_Model_CompanyGroup();
            $listCompanyGroup = $QCompanyGroup->getAll();
            $this->view->listCompanyGroup = $listCompanyGroup;

            $QDateCategory = new Application_Model_DateCategory();
            $allDateCategory = $QDateCategory->getAllSelect();
            $this->view->allDateCategory = $allDateCategory;
        }

        public function checkDateSettingAction()
        {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            if($this->getRequest()->isXmlHttpRequest())
            {
                $from_date = $this->getRequest()->getParam('from_date');
                $to_date = $this->getRequest()->getParam('to_date');
                $json_input = $this->getRequest()->getParam('json_input');

                $from_date = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
                $to_date = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;
                
                $input = json_decode($json_input, true);
                $array_group_edit = array();
                foreach ($input as $key => $value) 
                {
                    $array_group_edit[] = $value['type_id'];
                }

                $params = array(
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'list_type_id' => $array_group_edit,
                );
                $QAllDateAdvanced = new Application_Model_AllDateAdvanced();
                $result = $QAllDateAdvanced->checkEdit($params);

                echo json_encode($result); die;
            }
        }

        public function listDateSettingAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            
            $month = $this->getRequest()->getParam('month', date('m'));
            $year = $this->getRequest()->getParam('year', date('Y'));

            $params = array(
                'month' => $month,
                'year' => $year,
            );

            $QAllDateAdvanced = new Application_Model_AllDateAdvanced();

            $data = $QAllDateAdvanced->getListDateSetting($params);

            $params['month'] = $month;
            $params['year'] = $year;

            $this->view->params = $params;
            $this->view->data = $data;

            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }

        public function listDateStaffSettingAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $from_date = $this->getRequest()->getParam('from_date', date('1/m/Y'));
            $to_date = $this->getRequest()->getParam('to_date', date('d/m/Y'));
            $name = $this->getRequest()->getParam('name');
            $code = $this->getRequest()->getParam('code');
            $page = $this->getRequest()->getParam('page', 1);

            $department = $this->getRequest()->getParam('department');
            $team = $this->getRequest()->getParam('team');
            $title = $this->getRequest()->getParam('title');
            
            $from_date_param = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
            $to_date_param = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;

            $params = array(
                'from_date' => $from_date_param,
                'to_date' => $to_date_param,
                'name' => $name,
                'code' => $code,
                'department' => $department,
                'team' => $team,
                'title' => $title,
                'limit' => 10,
            );

            $params['offset'] = ($page-1)*$params['limit'];

            $QAllDateStaffAdvanced = new Application_Model_DateStaffAdvanced();
            $data = $QAllDateStaffAdvanced->fetchPagination($params);

            $params['from_date'] = $from_date;
            $params['to_date'] = $to_date;

            $this->view->params = $params;
            $this->view->data = $data['data'];
            $this->view->total = $data['total'];
            $this->view->limit = $params['limit'];
            $this->view->offset = $params['offset'];
            $this->view->url = $this->view->url = HOST . 'date/list-date-staff-setting' . ($params ? '?' . http_build_query($params) . '&' : '?');

            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        }

        public function delDateStaffSettingAction()
        {
            $id = $this->getRequest()->getParam('id');

            $QAllDateStaffAdvanced = new Application_Model_DateStaffAdvanced();
            $QAllDateStaffAdvanced->deleteSetting($id);
            $this->_redirect("/date/list-date-staff-setting");
        }

        public function dateStaffSettingAction()
        {
            $submit = $this->getRequest()->getParam('submit');
            $flashMessenger             = $this->_helper->flashMessenger;
            if(!empty($submit))
            {
                $json_input = $this->getRequest()->getParam('json_input');
                $data = json_decode($json_input);

                $array_value = array();
                foreach($data as $key => $value)
                {
                    
                    $array_date = explode("/", $value->date);

                    $date = $array_date[2] . '-' . $array_date[1] . '-' . $array_date[0];
                    $is_del = $value->is_del;
                    $array_value[] = "(" . implode(", ", array("'" .$date . "'", $value->staff_id, $is_del)) . ")";
                    
                }
                if(!empty($array_value))
                {
                    $sql = implode(",", $array_value);
                    
                    $QAllDateStaffAdvanced = new Application_Model_DateStaffAdvanced();
                    $QAllDateStaffAdvanced->_insert($sql);
                    $flashMessenger->setNamespace('success')->addMessage("Thêm dữ liệu thành công");
                    $this->_redirect("/date/date-staff-setting");
                }
                else
                {
                    $flashMessenger->setNamespace('error')->addMessage("Không có dữ liệu");
                    $this->_redirect("/date/date-staff-setting");
                }
            }

            $QDateCategory = new Application_Model_DateCategory();
            $allDateCategory = $QDateCategory->getAllSelect();

            $this->view->allDateCategory = $allDateCategory;
            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }

        public function dateCategoryAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $QDateCategory = new Application_Model_DateCategory();
            $data = $QDateCategory->getAll();

            $this->view->data = $data;
            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }

        public function dateCategoryCreateAction()
        {
            $id = $this->getRequest()->getParam('id');
            $submit = $this->getRequest()->getParam('submit');
            $code = $this->getRequest()->getParam('code');
            $code_half = $this->getRequest()->getParam('code_half');
            $name = $this->getRequest()->getParam('name');
            $flashMessenger = $this->_helper->flashMessenger;
            $QDateCategory = new Application_Model_DateCategory();
            if(!empty($submit))
            {
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'code_half' => $code_half,
                );

                if(!empty($id))
                {
                    $where = $QDateCategory->getAdapter()->quoteInto('id = ?', $id);
                    $QDateCategory->update($data, $where);
                }
                else
                {
                    $QDateCategory->insert($data);
                }
                $flashMessenger->setNamespace('success')->addMessage("Đã xử lý xong");
                $this->_redirect("/date/date-category");
            }

            if(!empty($id))
            {
                $data = $QDateCategory->find($id);
                $this->view->data = $data[0];
            }
            
        }
        public function deleteDateSettingAction()
        {
            $QAllDateAdvanced  = new Application_Model_AllDateAdvanced();

            $id = $this->getRequest()->getParam('id');
            $where  = $QAllDateAdvanced->getAdapter()->quoteInto('id = ?', $id);
            
            $QAllDateAdvanced->delete($where);

        }
        
    }