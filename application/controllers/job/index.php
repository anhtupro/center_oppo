<?php
$page = $this->getRequest()->getParam('page', 1);
$title = $this->getRequest()->getParam('title');
$location = $this->getRequest()->getParam('location');
$area = $this->getRequest()->getParam('area');
$content = $this->getRequest()->getParam('content');
$status = $this->getRequest()->getParam('status');
$category = $this->getRequest()->getParam('category');
$sort = $this->getRequest()->getParam('sort');
$desc = $this->getRequest()->getParam('desc', 1);
$export = $this->getRequest()->getParam('export');
$reset_view = $this->getRequest()->getParam('reset_view');

if(isset($reset_view) and $reset_view == 1){
	
	//Lấy lượt view
	$ch = curl_init();
	$url = "http://vieclam.oppomobile.vn/index/reset-view-data";
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST, 1);         
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
	curl_setopt($ch,CURLOPT_TIMEOUT, 20);
	$response = curl_exec($ch);
	$data = json_decode($response,true);
	curl_close($ch);
	//end
	
	set_time_limit( 0 );
	error_reporting( 0 );
	ini_set('display_error', 0);
	ini_set('memory_limit', -1);
	
	$this->_redirect('/job');
}

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id = $userStorage->id;
$user_group = $userStorage->group_id;

$QAsm = new Application_Model_Asm();

$QRegionalMarket = new Application_Model_RegionalMarket();
$regional_market = $QRegionalMarket->get_cache();

$QArea = new Application_Model_Area();
$areas = $QArea->get_cache();

$QStaff = new Application_Model_Staff();
$staff = $QStaff->get_all_cache();

$QCat = new Application_Model_JobCategory();
$this->view->category_all = $QCat->get_all();

$limit = LIMITATION;
$total = 0;

$params = array_filter(array(
	'title' => $title,
	'location'=>$location,
	'area'=>$area,
	'status'=>$status,
	'content' => $content,
	'category' => $category,
	'sort' => $sort,
	'desc' => $desc,
));

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
// echo "<pre>";
// print_r($userStorage);
// echo "</pre>";
if (in_array(
	$userStorage->title,
	array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM, SALE_SALE_ASM_STANDBY)
) OR $userStorage->group_id == SALES_ADMIN_ID OR $userStorage->group_id == 22) {
	
	$cachedASM = $QAsm->get_cache($userStorage->id);
	$tem = $cachedASM['province'];
	if ($tem)
		$list_province_ids = $tem;
	else
		$list_province_ids = -1;
	
	$params['list_province_ids'] = $list_province_ids;

	$params['cv_area'] = $cachedASM['area'];
}
elseif($userStorage->title == SALES_LEADER_TITLE OR $userStorage->title == PG_LEADER_TITLE){

	$where = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $userStorage->regional_market);

	$regional_market = $QRegionalMarket->fetchRow($where);

	$params['area'] = [$regional_market['area_id']];

}
elseif($userStorage->group_id == HR_ID){

}
elseif($userStorage->group_id == TRAINING_TEAM_ID){
	$QAsm = new Application_Model_Asm();
	$cachedASM = $QAsm->get_cache($userStorage->id);
	$tem = $cachedASM['province'];
	if ($tem)
		$list_province_ids = $tem;
	else
		$list_province_ids = -1;
	
	$params['list_province_ids'] = $list_province_ids;
	$params['cv_area'] = $cachedASM['area'];
	$params['job_title'] = PGPB_TITLE;
}elseif($userStorage->group_id == EMPLOYEE_ID){
    	$params['category']=5;
	
	$arrayRSM=array(3424,$userStorage->regional_market);
	$arraytemp=array();
	foreach ($arrayRSM as $key => $value) {
		$where= $QRegionalMarket->getAdapter()->quoteInto('id = ?', $value);
		$regional_market = $QRegionalMarket->fetchRow($where);
		$arraytemp[$key]=$regional_market['area_id'];
	}
	$params['area'] = $arraytemp;
}else{
	$QAsm = new Application_Model_Asm();
	$cachedASM = $QAsm->get_cache($userStorage->id);
	$tem = $cachedASM['province'];
	if ($tem)
		$list_province_ids = $tem;
	else
		$list_province_ids = -1;
	
	$params['list_province_ids'] = $list_province_ids;

	$params['cv_area'] = $cachedASM['area'];
}

//if: nếu là ngocduyen.le chỉ được phép xem các tin tuyển dụng của ngocduyen.le đăng
if(in_array($userStorage->id,array('7278'))){
	$params['created_by'] = 7278;
}
//end


$params['sort'] = $sort;
$params['desc'] = $desc;
$params['filter_display'] = 1;

$QModel = new Application_Model_Job();

$jobs = $QModel->fetchPagination($page, $limit, $total, $params);
$this->view->params = $params;
$this->view->sort = $sort;
$this->view->desc = $desc;
$this->view->job = $jobs;
$this->view->regional_market = $regional_market;
$this->view->areas = $areas;
$this->view->staff = $staff;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'job/' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;

if ($this->getRequest()->isXmlHttpRequest()) {
	$this->_helper->layout->disableLayout();

	$this->_helper->viewRenderer->setRender('partials/list');
} else
	$this->_helper->viewRenderer->setRender('index');

//EXPORT

if(isset($export) and $export == 1)
{
	//Lấy lượt view
	$ch = curl_init();
	$url = "http://vieclam.oppomobile.vn/index/get-view-data";
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST, 1);         
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
	curl_setopt($ch,CURLOPT_TIMEOUT, 20);
	$response = curl_exec($ch);
	$data = json_decode($response,true);
	curl_close($ch);
	//end
    ini_set("memory_limit", -1);
    ini_set("display_error", 0);
    error_reporting(~E_ALL);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();


    $heads = array(
		'#',
		'Vị trí',
		'Số CV nộp vào',
		'Lượt view',
	);


    $PHPExcel->setActiveSheetIndex(0);
    $sheet    = $PHPExcel->getActiveSheet();

    $alpha    = 'A';
    $index    = 1;
    foreach($heads as $key)
    {
        $sheet->setCellValue($alpha.$index, $key);
        $alpha++;
    }
    $index    = 2;

    $i = 1;


	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

	$config = $config->toArray();

	$con    = mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

	mysqli_set_charset($con,'utf8');

	// Check connection
	if (mysqli_connect_errno())
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS
				p.id,
				`p`.*, `b`.`id` AS `job_regional_id`,
				GROUP_CONCAT(c.`name`) AS `regional_market_list`,
				GROUP_CONCAT(DISTINCT d.`name`) AS `area_list`,
				`e`.`name` AS `name_category`,
				cv.count_cv AS `count_cv`
			FROM
				`job` AS `p`
			LEFT JOIN `job_regional` AS `b` ON p.id = b.job_id
			LEFT JOIN `regional_market` AS `c` ON b.regional_market_id = c.id
			LEFT JOIN `area` AS `d` ON c.area_id = d.id
			LEFT JOIN `team` AS `e` ON p.category = e.id
			LEFT JOIN 
			(
			SELECT a.id,a.title,count(b.id) AS count_cv
			FROM job as a
			LEFT JOIN cv as b On b.job_id = a.id
			GROUP BY a.id,a.title
			) as cv ON cv.id = p.id
			GROUP BY
				`p`.`id`
			ORDER BY
				`p`.`created_at` DESC";
	
	$result = mysqli_query($con,$sql);
	$stt = 0;
	$sum_view = 0;
	while($row = @mysqli_fetch_array($result))
	{
		$stt++;
		$sum_view += $data[$row['id']];
        $alpha    = 'A';
        $sheet->setCellValue($alpha++.$index, $i++);
        $sheet->setCellValue($alpha++.$index, isset($row['title']) ? $row['title'] :NULL);
        $sheet->setCellValue($alpha++.$index, isset($row['count_cv'] ) ? $row['count_cv'] : "0");
        $sheet->setCellValue($alpha++.$index, isset($data[$row['id']] ) ? $data[$row['id']] : NULL);


        $index++;

	}
	

    $filename = 'Report_Job_'.date('d/m/Y');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

    $objWriter->save('php://output');

    exit;
}