<?php

$id = $this->getRequest()->getParam('id');
$from_approve = $this->getRequest()->getParam('from_approve');
$approve = $this->getRequest()->getParam('approve');

$QModel = new Application_Model_Job();
$QJobObject = new Application_Model_JobObject();
$QJobEmailPersonCharge = new Application_Model_JobEmailPersonCharge();

$QJobCategory = new Application_Model_JobCategory();
$this->view->job_cat = $QJobCategory->get_cache();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id = $userStorage->id;
$user_group = $userStorage->group_id;
$this->view->userStorage = $userStorage;


if (in_array($userStorage->title, array(SALE_SALE_ASM))) {
    $QAsm = new Application_Model_Asm();
    $cachedASM = $QAsm->get_cache($userStorage->id);
    $tem = $cachedASM['area'];
    $this->view->areas = $tem;
} else {
    $QArea = new Application_Model_Area();
    $tem = $QArea->get_cache();
    $this->view->areas_all = $tem;
}

$QArea = new Application_Model_Area();
$this->view->areas_catche = $QArea->get_cache();

$QTeam = new Application_Model_Team();
$whereDepartment[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', 0);
$whereDepartment[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
$listDepartment = $QTeam->fetchAll($whereDepartment);
$this->view->listDepartment = $listDepartment;

$QRegionalMarket = new Application_Model_RegionalMarket();
$this->view->all_province_cache = $QRegionalMarket->get_cache();
if ($userStorage->department != DEPARTMENT_HR AND $userStorage->id != 5899) {
    $staffSpecial = 1;
    /* get team + title SALES */
    $where_team = array();
    $where_team[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $userStorage->department);
    $where_team[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
    $team_view = $QTeam->fetchAll($where_team);
    $this->view->team_view = $team_view;

    $where_team = array();
    $where_team[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $userStorage->team);
    $where_team[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
    $team_title = $QTeam->fetchAll($where_team);
    $this->view->team_title = $team_title;
    /* end get team and title */
} else {
    $staffSpecial = 0;
}
echo $userStorage->department;
$this->view->staffSpecial = $staffSpecial;


if (isset($id) and $id) {
    $QJobRegional = new Application_Model_JobRegional();
    $job_regional = $QJobRegional->get_regional_by_id($id);
    $this->view->job_regional = $job_regional;

    $jobCurrent = $QModel->find($id);
    $job = $jobCurrent->current();
    $this->view->job = $job;
//    debug($job);exit;

    $whereTitle = $QTeam->getAdapter()->quoteInto('id = ?', $job['category']);
    $title = $QTeam->fetchRow($whereTitle);
    $this->view->title = $title;


    /* get team + title */
    $where_team = array();
    $where_team[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $job['department']);
    $where_team[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
    $team_view = $QTeam->fetchAll($where_team);
    $this->view->team_view = $team_view;

    $where_team[] = array();
    $where_team[] = $QTeam->getAdapter()->quoteInto('parent_id = ?', $job['team']);
    $where_team[] = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
    $team_title = $QTeam->fetchAll($where_title);
    $this->view->team_title = $team_title;
    /* end get team and title */

    $rowset = $QRegionalMarket->find($job->regional_market_id);

    if ($rowset) {
        $this->view->regional_market = $regional_market = $rowset->current();
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);
        $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

        $rowset = $QArea->find($regional_market['area_id']);
        $this->view->area = $rowset->current();
    }

    $QJobObject = new Application_Model_JobObject();
    $where = $QJobObject->getAdapter()->quoteInto('job_id = ?', $id);
    $old_objects = $QJobObject->fetchAll($where);

    $old_province_objects = array();

    foreach ($old_objects as $_key => $_value) {
        switch ($_value['type']) {
            case 1:
                $old_province_objects[] = $_value['object_id'];
                break;
            default:
                throw new Exception("Invalid object type");
                break;
        }
    }
    //Truyền file ảnh
    $QJobFile = new Application_Model_JobFile();
    $QFileUploadLog = new Application_Model_FileUploadLog();
    $where = array(
        'job_id=?' => $id,
        'del=?' => 0
    );
    
    //email person in charge
    $where_person = [];
    $where_person[] = $QJobEmailPersonCharge->getAdapter()->quoteInto("job_id = ?",$id);
    $persion_charg_tmp = $QJobEmailPersonCharge->fetchAll($where_person);
    $persion_charg = [];
    foreach($persion_charg_tmp as $value){
        $persion_charg[] = $value->email;
    }
    $this->view->persion_charg = implode(';', $persion_charg);

    $file = $QJobFile->fetchRow($where);
//    debug($file);exit;
    $where = $QFileUploadLog->getAdapter()->quoteInto("filename = ?", $file['name']);
    $userStorage = $QFileUploadLog->fetchRow($where);

    
    $this->view->user = $userStorage[staff_id];
    $this->view->files = $file;
    $this->view->old_province_objects = $old_province_objects;
}

if ($this->getRequest()->getMethod() == 'GET') {
    if (isset($approve) and ( $approve == 1)) {
        $data = array(
            'status' => 1
        );
        $where_approve = $QModel->getAdapter()->quoteInto('id = ?', $id);
        $QModel->update($data, $where_approve);
        My_Controller::redirect(HOST . 'job');
    }
}

if ($this->getRequest()->getMethod() == 'POST') {
    $id = $this->getRequest()->getParam('id');
    $title = $this->getRequest()->getParam('title');
    $content = $this->getRequest()->getParam('content');
    $description = $this->getRequest()->getParam('description');

    $desc_skill = $this->getRequest()->getParam('desc_skill');
    $from = $this->getRequest()->getParam('from', null);
    $to = $this->getRequest()->getParam('to', null);
    $status = $this->getRequest()->getParam('status', 0);
    $hot = $this->getRequest()->getParam('hot', 0);
    $area_id = $this->getRequest()->getParam('area_id', 0);
    $regional_market = $this->getRequest()->getParam('regional_market');
    $regionals = $this->getRequest()->getParam('regionals');
    $all = $this->getRequest()->getParam('all');
    $province_objects = $this->getRequest()->getParam('province');
    $category = $this->getRequest()->getParam('category');

    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $job_title = $this->getRequest()->getParam('job_title');
    $job_type = $this->getRequest()->getParam('job_type');
    $job_group = $this->getRequest()->getParam('job_group');


    $salary_text = $this->getRequest()->getParam('salary_text');
    $person_in_charge = $this->getRequest()->getParam('person-in-charge');


    $all = intval($all);

    $title = trim($title);
    $currentTime = date('Y-m-d h:i:s');

    $from = explode('/', $from);
    $from = $from[2] . '-' . $from[1] . '-' . $from[0];

    $to = explode('/', $to);
    $to = $to[2] . '-' . $to[1] . '-' . $to[0];
    //File
    $key = $this->getRequest()->getParam('key');
    $file_id = $this->getRequest()->getParam('file_id');
    $hash = $this->getRequest()->getParam('hash');
    $nfile_id = $this->getRequest()->getParam('nfile_id');
    //--------------------------------------------------------
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $userId = $userStorage->id;
    $group_id = $userStorage->group_id;

    $db = Zend_Registry::get('db');


    $db->beginTransaction();
    try {
        // kiểm tra job title

        if (empty($title))
            throw new exception('Please input job title');

        if (empty($title))
            throw new exception('Please input title');

        // lấy nhóm hiển thị đã lưu trước đó
        $where = $QJobObject->getAdapter()->quoteInto('job_id = ?', $id);
        $old_objects = $QJobObject->fetchAll($where);

        $old_province_objects = array();

        foreach ($old_objects as $_key => $_value) {
            switch ($_value['type']) {
                case My_Job::PROVINCE:
                    $old_province_objects[] = $_value['object_id'];
                    break;

                default:
                    throw new Exception("Invalid object type");
                    break;
            }
        }


        $data = array(
            'title' => $title,
            'content' => $content,
            'description' => $description,
            'email' => null,
            'hot' => $hot,
            'desc_skill' => $desc_skill,
            'from' => $from,
            'to' => $to,
            'area_id' => intval($area_id),
            'regional_market_id' => intval($regional_market),
            'created_at' => $currentTime,
            'created_by' => $user_id,
            'all' => $all > 1 ? 1 : 0,
            'department' => (isset($department) and $department) ? $department : null,
            'team' => (isset($team) and $team) ? $team : null,
            'job_title' => (isset($job_title) and $job_title) ? $job_title : null,
            'category' => (isset($category) and $category) ? $category : null,
            'job_type' => (isset($job_type) and $job_type) ? $job_type : null,
            'job_group' => (isset($job_group) and $job_group) ? $job_group : null,
            'salary_text' => (isset($salary_text) and $salary_text) ? $salary_text : null,
        );

        if ($approve == 1) {
            $data['status'] = 1;
        }

        if ($id) {
            $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
            $QModel->update($data, $where);
        } else {
            $id = $QModel->insert($data);
        }

        $Ws = new Application_Model_Job();

        $params_ws = [
            'id' => $id
        ];

        // insert email person in charge
        $where_charge = [];
        $where_charge[] = $QJobEmailPersonCharge->getAdapter()->quoteInto('job_id = ?', $id);
        $QJobEmailPersonCharge->delete($where_charge);
        if(!empty($person_in_charge)){
            $mail_in_charge = explode(';',$person_in_charge);
            foreach($mail_in_charge as $value){
                if(!filter_var(trim($value), FILTER_VALIDATE_EMAIL)){
                    throw new exception('Invalid email format');
                }
                $QJobEmailPersonCharge->insert([
                    'job_id' => $id , 
                    'email' => trim($value)
                ]);
            }
        }
        // end insert email person in charge

        //transfer data job_regional
        $WsRe = new Application_Model_JobRegional();
        $dataRe = $WsRe->get_regional();
        $temp_regional = array();
        if ($data) {
            foreach ($dataRe as $item) {
                $temp_regional[] = array(
                    'id' => $item['id'],
                    'job_id' => $item['job_id'],
                    'regional_market_id' => $item['job_id'],
                );
            }
        }

        $resultRe = $WsRe->insertWebservice($temp_regional);



        //Insert multil area
        $QJobRegional = new Application_Model_JobRegional();
        $where = $QJobRegional->getAdapter()->quoteInto('job_id = ?', $id);
        $QJobRegional->delete($where);

        if ($regionals) {
            $regionals_arr = explode(',', $regionals);
            foreach ($regionals_arr as $k => $c) {
                $data = array('job_id' => $id, 'regional_market_id' => $c);
                $QJobRegional->insert($data);
            }
        }

        ////////////////////////////////////////////////////////////////
        $add_area_objects = array_diff($province_objects, $old_province_objects);
        $remove_area_objects = array_diff($old_province_objects, $province_objects);

        foreach ($add_area_objects as $_key => $_value) {
            $data = array(
                'job_id' => intval($id),
                'object_id' => intval($_value),
                'type' => My_Job::PROVINCE,
            );

            $QJobObject->insert($data);
        }
        /////////////////////////////////////////////////////////////////////////

        foreach ($remove_area_objects as $_key => $_value) {
            $where = array();
            $where[] = $QJobObject->getAdapter()->quoteInto('notification_id = ?', intval($id));
            $where[] = $QJobObject->getAdapter()->quoteInto('object_id = ?', intval($_value));
            $where[] = $QJobObject->getAdapter()->quoteInto('type = ?', 1);

            $QJobObject->delete($where);
        }
        ////////////////////////////////////////////////////////////////////////////////

        $QLog = new Application_Model_Log();
        $info = 'Insert job' . $title;
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');

        $QLog->insert(array(
            'info' => $info,
            'user_id' => $userId,
            'ip_address' => $ip,
            'time' => $currentTime,
        ));

        $QFileLog = new Application_Model_FileUploadLog();
        $QJobFile = new Application_Model_JobFile();

        if (isset($file_id)) { //Upload image

            $where = $QFileLog->getAdapter()->quoteInto("id= ?", $file_id);
            $log = $QFileLog->fetchRow($where);

            $data = array(
                'job_id' => $id,
                'file_display_name' => $log['real_file_name'],
                'name' => $log['filename'],
                'unique_folder' => $log['folder'],
                'del' => 0,
            );
            //Deleting all old images
            $where = $QJobFile->getAdapter()->quoteInto("job_id  =   ?", $id);
            $QJobFile->update(array('del' => 1), $where);


            $QJobFile->insert($data); //Insert new image
        }
        ////////////////////////////////////////////////////////////////

        $db->commit();

        $flashMessenger = $this->_helper->flashMessenger;
        $flashMessenger->setNamespace('success')->addMessage('success');

        if ($from_approve == 1) {
            My_Controller::redirect(HOST . 'job/approve?id=' . $id);
        } else {
            My_Controller::redirect(HOST . 'job');
        }
    } catch (Exception $e) {
        $db->rollback();
        My_Controller::palert($e->getMessage());
    }
}