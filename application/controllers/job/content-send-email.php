<?php 

$hr_signature = '
                    <h3 style="color: #00b050;margin-bottom: 0;">Recruitment Team</h3>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">T (84-8) 39202555 - ext: 111</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">F (84-8) 39202555 - ext: 120</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">E <a href="mailto:recruitment@oppomobile.vn">recruitment@oppomobile.vn</a></p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">12th Floor, Lim II Tower, No. 62A CMT8 St., Ward 6, Dist. 3, HCMC, VN</p>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 0;"><a href="http://vieclam.oppomobile.vn/">www.vieclam.oppomobile.vn</a></p>';

/////////////////////////
/////// CONFIG
/////////////////////////
    error_reporting(~E_ALL);
    ini_set("display_error", 0);
    set_time_limit(0);
    $mailfrom = 'recruitment@oppomobile.vn'; // Config this
    $mailfromname = "OPPO tuyển dụng"; // Config this
    $subject = 'CƠ HỘI NGHỀ NGHIỆP VỚI OPPO';
    $content = '<div><p class="">
                Thân gửi '. $name.',
                <br><br>Rất cảm ơn bạn đã quan tâm đến môi trường làm việc tại OPPO Việt Nam';
    $content .= '<br><br>OPPO Việt Nam thân gửi bạn thông tin về các cơ hội nghề nghiệp phù hơp với nguyện vọng của bạn.';
    $content .= '<br><br>Vị trí: '. $title;
    $content .= '<br><br>Thông tin chi tiết: '. $link;

    $content .= '<br><br>Trân trọng,' . $hr_signature;
    $mailto = array();
    
    $mailto[] = $email;
    $res = $this->sendmail($mailto, $subject, $content);
    return $res;
