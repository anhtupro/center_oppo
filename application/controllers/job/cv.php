<?php
	
	$page     	= $this->getRequest()->getParam('page', 1);
	$name     	= trim($this->getRequest()->getParam('name'));
	$content  	= $this->getRequest()->getParam('content');
	$location 	= $this->getRequest()->getParam('location');
	$dob_from 	= $this->getRequest()->getParam('dob_from');
	$dob_to   	= $this->getRequest()->getParam('dob_to');
	$status   	= $this->getRequest()->getParam('status');
	$address  	= $this->getRequest()->getParam('address');
	$job      	= $this->getRequest()->getParam('job');
	$phone      = $this->getRequest()->getParam('phone');
	$email      = trim($this->getRequest()->getParam('email'));
	$source     = $this->getRequest()->getParam('source');
	$province   = $this->getRequest()->getParam('province');
	$district   = $this->getRequest()->getParam('district');
	$export  	= $this->getRequest()->getParam('export');
	$job_category = $this->getRequest()->getParam('job_category');
	$language_other = $this->getRequest()->getParam('language_other');

	$is_potential = $this->getRequest()->getParam('is_potential',null);

	$created_at_from  	= $this->getRequest()->getParam('created_at_from');
	$created_at_to  	= $this->getRequest()->getParam('created_at_to');

	$schools    = $this->getRequest()->getParam('schools');
	$brand      = $this->getRequest()->getParam('brand');
	$qualification      = $this->getRequest()->getParam('qualification');
	
	$sort     	= $this->getRequest()->getParam('sort');
	$desc     	= $this->getRequest()->getParam('desc', 1);
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();


	$QRegionalMarket = new Application_Model_RegionalMarket();


	if($created_at_from){
		$created_at_from = str_replace('/', '-', $created_at_from);
	    $created_at_from = date("Y-m-d", strtotime($created_at_from) );
	}

	if($created_at_to){
		$created_at_to = str_replace('/', '-', $created_at_to);
	    $created_at_to = date("Y-m-d 23:59:59", strtotime($created_at_to) );
	}
        

	$limit = LIMITATION;
	$total = 0;

	$params = array_filter(array(
		'name'    		=> $name,
		'location'		=> $location,
		'dob_from'		=> $dob_from,
		'dob_to'  		=> $dob_to,
		'status'  		=> $status,
		'address' 		=> $address,
		'content' 		=> $content,
		'job'     		=> $job,
		'phone'   		=> $phone,
		'email'   		=> $email,
		'sort'    		=> $sort,
		'desc'    		=> $desc,
		'schools' 		=>$schools,
		'brand'   		=>$brand,
		'qualification'	=>$qualification,
		'source'  		=> $source,
		'province' 		=> $province,
		'district' 		=> $district,
		'export'		=> $export,
		'created_at_from'	=> $created_at_from,
		'created_at_to'	    => $created_at_to,
		'job_category'  => $job_category,
		'language_other'  => $language_other,
		'is_potential'  => $is_potential,
	));
	$params['sort'] = $sort;
	$params['desc'] = $desc;
	$params['filter_display'] = 1;

	if (in_array(
		$userStorage->title,
		array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM, SALE_SALE_ASM_STANDBY, LEADER_TITLE)
	) OR $userStorage->group_id == SALES_ADMIN_ID OR $userStorage->group_id == LEADER_ID OR $userStorage->group_id == 22) {
		$QAsm = new Application_Model_Asm();
		$cachedASM = $QAsm->get_cache($userStorage->id);
		$tem = $cachedASM['province'];
		if ($tem)
			$list_province_ids = $tem;
		else
			$list_province_ids = -1;
		
		$params['list_province_ids'] = $list_province_ids;

		$params['cv_area'] = $cachedASM['area'];
	}
	elseif($userStorage->group_id == HR_ID){
		
	}
	elseif($userStorage->group_id == TRAINING_TEAM_ID){
		$QAsm = new Application_Model_Asm();
		$cachedASM = $QAsm->get_cache($userStorage->id);
		$tem = $cachedASM['province'];
		if ($tem)
			$list_province_ids = $tem;
		else
			$list_province_ids = -1;
		
		$params['list_province_ids'] = $list_province_ids;
		$params['cv_area'] = $cachedASM['area'];
		$params['job_title'] = PGPB_TITLE;
	}elseif($userStorage->group_id == 21){ //group service
            $params['job_category'] = 8; // cham soc khach hang
    }elseif($userStorage->group_id == EMPLOYEE_ID){
    	$params['job_category']=5;
		$arrayRSM=array($userStorage->regional_market);
		$arraytemp=array();
		foreach ($arrayRSM as $key => $value) {
			$where= $QRegionalMarket->getAdapter()->quoteInto('id = ?', $value);
			$regional_market = $QRegionalMarket->fetchRow($where);
			$arraytemp[$key]=$regional_market['area_id'];
		}
		$params['cv_area'] = $arraytemp;
    }
	else{
		$QAsm = new Application_Model_Asm();
		$cachedASM = $QAsm->get_cache($userStorage->id);
		$tem = $cachedASM['province'];
		if ($tem)
			$list_province_ids = $tem;
		else
			$list_province_ids = -1;
		
		$params['list_province_ids'] = $list_province_ids;

		$params['cv_area'] = $cachedASM['area'];
	}
	//Lọc 
	$QCvLanguage = new Application_Model_CvLanguage();
	$CvLanguage=$QCvLanguage->fetchAll();
	// echo "<pre>";
	// print_r($CvLanguage);
	// echo "</pre>";

	$QModel = new Application_Model_Cv();
	$cv_suggest = $QModel->fetchPagination_suggest($page, $limit, $total, $params);
	$cv = $QModel->fetchPagination($page, $limit, $total, $params);

	if(isset($export) and $export == 1){
		$this->_exportExcelCv($cv);
	}

	$QRegionalMarket = new Application_Model_RegionalMarket();
	$regional_market = $QRegionalMarket->get_cache();
	$this->view->regional_market = $regional_market;

	$province_district = $QRegionalMarket->get_province(); 
	$this->view->province_district = $province_district;
	$this->view->province_cache = $QRegionalMarket->get_province_cache();

	$QProvince = new Application_Model_Province();

	$this->view->province = $QProvince->fetchAll();


	$this->view->params        = $params;
	$this->view->sort          = $sort;
	$this->view->desc          = $desc;
	$this->view->CvLanguage    = $CvLanguage;


	$this->view->cv            = $cv;
	$this->view->cv_suggest    = $cv_suggest;
	
	$this->view->limit  = $limit;
	$this->view->total  = $total;
	$this->view->url    = HOST.'job/cv'.( $params ? '?'.http_build_query($params).'&' : '?' );
	$this->view->offset = $limit*($page-1);

	$flashMessenger       = $this->_helper->flashMessenger;
	$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages_success = $messages_success;

	$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
	$this->view->messages_error = $messages_error;

	if($this->getRequest()->isXmlHttpRequest()) {
		$this->_helper->layout->disableLayout();

		$this->_helper->viewRenderer->setRender('partials/list_cv');
	} else
		$this->_helper->viewRenderer->setRender('cv');
		
	
	$QJob = new Application_Model_Job();


	$today = date("Y-m-d");

	$db = Zend_Registry::get('db');
	$cols = array(
		'p.*',
		'p.title',
		'region' => 'GROUP_CONCAT(r.name)'
	);
	$select = $db->select()
		->from(array('p'=>'job'), $cols)
		->joinLeft(array('j'=>'job_regional'), 'j.job_id = p.id',array())
		->joinLeft(array('r'=>'regional_market'), 'r.id = j.regional_market_id',array());

	//$select->where('status = ?', 1);
	//$select->where('DATE(p.to) >= ?', DATE($today));
	$select->group('p.id');

	$job = $db->fetchAll($select);
	$this->view->job = $job;
	$this->view->auth = $userStorage;
	$QCvSource = new Application_Model_CvSource();
	$source    = $QCvSource->fetchAll();
	$this->view->source = $source;

	$QJobCat = new Application_Model_JobCategory();
	$this->view->list_job = $QJobCat->get_cache();
