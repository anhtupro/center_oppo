<?php
	
	$name     = $this->getRequest()->getParam('name');
    $dob   	  = $this->getRequest()->getParam('dob');
	
    $date_dob = DateTime::createFromFormat('d/m/Y', $dob);
    if($date_dob){
		$dob = $date_dob->format('Y-m-d');
	}

    $QCv = new Application_Model_Cv();
    $QJob = new Application_Model_Job();
    $job = $QJob->get_job();

    $QCvStatus = new Application_Model_CvStatus();
    $job_status = $QCvStatus->get_cache();

    $where = array();
    $where[] = $QCv->getAdapter()->quoteInto('DATE(dob) = ?', $dob);
    $where[] = $QCv->getAdapter()->quoteInto('name LIKE ?', '%'.$name.'%');
    $cv = $QCv->fetchAll($where, 'created_at DESC');

    $data = array();
    foreach ($cv as $key => $value) {

    	$status_name = null;
    	$status = $value['status'];
    	if($status > 0 ){
    		$status_name = !empty($job_status[$value['status']]) ? $job_status[$value['status']] : "Chưa xem";
    	}
    	else{
    		$status_name = 'Chưa xem';
    	}

    	$data[] = array(
    		'id' 				=>	$value['id'],
    		'name'  			=>	$value['name'],
    		'gender'  			=>	$value['gender'],
    		'address' 			=> $value['address'],
    		'phone'	  			=> $value['phone'],
    		'email'	 			=> $value['email'],
    		'source'  			=> $value['source'],
    		'salary'  			=> $value['salary'],
    		'education'  		=> $value['education'],
    		'education_branch'  => $value['education_branch'],
    		'education_type'  	=> $value['education_type'],
    		'education_order'  	=> $value['education_order'],
    		'job_id'			=> $value['job_id'],
    		'status'			=> $value['status'],
    		'job'				=> $job[$value['job_id']],
    		'status_name'		=> $status_name,
    		'created_at'		=> date("d/m/Y", strtotime($value['created_at'])),
            'note'             => $value['note'],

    	);
    }

    if(count($cv) > 0)
		echo json_encode($data);

	exit;


