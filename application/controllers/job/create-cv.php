<?php
	
	$id   = $this->getRequest()->getParam('id');
	$date = date('Y-m-d H:i:s');

	$QRegion 	  = new Application_Model_RegionalMarket();
	$QJob    	  = new Application_Model_Job();
	$QCvSource    = new Application_Model_CvSource();
	$QCv 		  = new Application_Model_Cv();
	$QCvRegion    = new Application_Model_CvRegion();

	$where = $QCvRegion->getAdapter()->quoteInto("cv_id = ?", $id);
	$cv_region = $QCvRegion->fetchAll($where);
	$this->view->cv_region = $cv_region;

	$where = $QJob->getAdapter()->quoteInto("id = ?", $id);
	$cv = $QCv->fetchRow($where);
	$this->view->cv = $cv;

	$list_province = $QRegion->get_province_cache();
	
	$province = array();
	foreach($list_province as $key=>$value){
		if(isset($province[$value['province_code']])){
			$province[$value['province_code']]['district'][] = array(
				'district_center' => $value['district_center'],
				'district_id'     => $value['district_id']
				);
		}
		else{
			$province[$value['province_code']]['name'] = $value['province_name'];
		}
	}
	$this->view->province = $province;
	$where = array();
	
	if(empty($id)){
	$where[] = $QJob->getAdapter()->quoteInto("`status` = ?", 1);
	$where[] = $QJob->getAdapter()->quoteInto("`from` <= ?", $date);
	$where[] = $QJob->getAdapter()->quoteInto("`to` >= ?", $date);
	}

	$this->view->job = $QJob->fetchAll($where);
        
        $whereSource = [];
	$whereSource[] 		   = $QCvSource->getAdapter()->quoteInto("`status` = ?", 1);
        $this->view->cv_source = $QCvSource->fetchAll($whereSource);

     
                //get other language
        $QCvLanguage = new Application_Model_CvLanguage();
        $CvLanguage=$QCvLanguage->getLanguageByID($id);

    $this->view->CvLanguage = $CvLanguage;


	//path_info
	$job_url = HOST_VIECLAM.'cv/';
    $job_url_avatar = HOST_VIECLAM.'cv_image/';
    $job_url_source = HOST . 'photo' .
            DIRECTORY_SEPARATOR . 'cv' . DIRECTORY_SEPARATOR . $cv['id'];
    
    $path_info = '';
    if(isset($cv['cv_name']) and $cv['cv_name'])
    {
        $temp = explode('_' , $cv['cv_name']);
        $path_info  = $job_url;
        $path_info .= 'temp' . DIRECTORY_SEPARATOR;
        $path_info .= $temp[0] . DIRECTORY_SEPARATOR . $temp[2];

        if(isset($cv['source']) and $cv['source'] != 5){
            $path_info = $job_url_source . DIRECTORY_SEPARATOR . $cv['cv_name'];
        }
    }
    $this->view->path_info = $path_info;


