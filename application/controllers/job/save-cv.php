<?php

$flashMessenger = $this->_helper->flashMessenger;
$back_url = HOST . 'job/cv';

$id = $this->getRequest()->getParam('id');
$name = $this->getRequest()->getParam('name');
$id_number = $this->getRequest()->getParam('id_number');
$gender = $this->getRequest()->getParam('gender');
$dob = $this->getRequest()->getParam('dob');
$is_potential = $this->getRequest()->getParam('potential');
$region_address1 = $this->getRequest()->getParam('region_address1');
$region_address2 = $this->getRequest()->getParam('region_address2');
$salary = $this->getRequest()->getParam('salary');
$phone = $this->getRequest()->getParam('phone');
$email = $this->getRequest()->getParam('email');
$source = $this->getRequest()->getParam('source');
$job_id = $this->getRequest()->getParam('job_id');

$education = $this->getRequest()->getParam('education');
$education_branch = $this->getRequest()->getParam('education_branch');
$education_type = $this->getRequest()->getParam('education_type');
$language_other = $this->getRequest()->getParam('language_other');

$region = $this->getRequest()->getParam('region', array());
$region2 = $this->getRequest()->getParam('region2', array());

if (!empty($dob)) {
    //$date_dob = DateTime::createFromFormat('d/m/Y', $dob);
    //$dob = $date_dob->format('Y-m-d H:i:s');

    $dob = explode('/', $dob);
    $dob = $dob[2] . '-' . $dob[1] . '-' . $dob[0];

    $time = strtotime($dob);
    $dob = date('Y-m-d', $time);
}

$QCv = new Application_Model_Cv();
$where_cv = $QCv->getAdapter()->quoteInto('id = ?', $id);
$cv = $QCv->fetchRow($where_cv);
$QCvLanguage = new Application_Model_CvLanguage();
$QCvRegion = new Application_Model_CvRegion();

$data = array(
    'name' => $name,
    'id_number' => !empty($id_number) ? $id_number : null,
    'gender' => !empty($gender) ? $gender : null,
    'dob' => !empty($dob) ? $dob : null,
    'is_potential' => !empty($is_potential) ? $is_potential : null,
    'region_address1' => !empty($region_address1) ? $region_address1 : null,
    'region_address2' => !empty($region_address2) ? $region_address2 : null,
    'salary' => !empty($salary) ? $salary : null,
    'phone' => !empty($phone) ? $phone : null,
    'email' => !empty($email) ? $email : null,
    'source' => !empty($source) ? $source : null,
    'job_id' => !empty($job_id) ? $job_id : null,
    'education' => !empty($education) ? $education : null,
    'education_branch' => !empty($education_branch) ? $education_branch : null,
    'education_type' => !empty($education_type) ? $education_type : null,
    'created_at' => date('Y-m-d H:i:s')
);

try {
    if (isset($cv) and count($cv) > 0) {
        $QCv->update($data, $where_cv);

        $where_region = $QCvRegion->getAdapter()->quoteInto('cv_id = ?', $id);
        $QCvRegion->delete($where_region);

        $where = $QCvLanguage->getAdapter()->quoteInto("cv_id = ? ", $id);
        $QCvLanguage->delete($where);
    } else {
        $id = $QCv->insert($data);

        if (isset($job_id) && !empty($job_id)) {
            $QCv->informToPersonChargAction($name, $email, $id, $job_id);
        }
    }
    if (!empty($language_other)) {
        foreach ($language_other as $k => $v) {
            $data_language = array(
                "cv_id" => $id,
                "language" => $k,
                "level" => $v,
            );
            $QCvLanguage->insert($data_language);
        }
    }
} catch (Exception $e) {
    echo "Có lỗi xảy ra";
    exit;
}


if (!empty($region)) {
    foreach ($region as $key => $value) {
        $data_region = array(
            'region_id' => !empty($value) ? $value : null,
            'district_id' => !empty($region2[$key]) ? $region2[$key] : null,
            'cv_id' => $id
        );
        $id_region = $QCvRegion->insert($data_region);
    }
}


//UPLOAD CV
$target_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
        DIRECTORY_SEPARATOR . 'cv' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR;

if (!is_dir($target_dir))
    @mkdir($target_dir, 0777, true);


$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$cv_name = basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if file already exists
/*
  if (file_exists($target_file)) {
  $error = "Sorry, file already exists.";
  $uploadOk = 0;
  }
 */
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    $error = "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if ($imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "pdf") {
    $error = "Sorry, only doc, docx & pdf files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $flashMessenger->setNamespace('error')->addMessage($error);
    // if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $where = $QCv->getAdapter()->quoteInto('id = ?', $id);
        $QCv->update(array('cv_name' => $cv_name), $where);

        //Move file to S3
        require_once 'Aws_s3.php';
        $s3_lib = new Aws_s3();

        $file_location = 'photo' .
                DIRECTORY_SEPARATOR . 'cv' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $cv_name;
        $detination_path = 'photo/cv/' . $id . '/';
        $destination_name = $cv_name;

        $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
        if ($upload_s3['message'] != 'ok') {
            echo json_encode([
                'status' => 0,
                'message' => "Upload S3 không thành công",
            ]);
            return;
        }
        // END - Move file to S3

        $flashMessenger->setNamespace('success')->addMessage('Done');
    } else {
        $flashMessenger->setNamespace('error')->addMessage('Error upload file!');
    }
}
//END UPLOAD CV


$back_url = HOST . 'job/view-cv?id=' . $id;

$this->_helper->layout->disableLayout();
$this->redirect($back_url);



