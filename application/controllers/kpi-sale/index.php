<?php
    $QPgpb_sales = new Application_Model_KpiSale();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $userId      = $userStorage->id;
    $userTitle   = $userStorage->title;
    $arrayUser   = ['title'=>$userTitle, 'id'=>$userId];

    $curPage     = $this->_request->getParam('page',1); 
    ($curPage == 0) ? $curPage =1 : $curPage;
    $params      = $this->_request->getParams();
    $dateStart   = $this->_request->getParam('date-start'); 
    $dateEnd     = $this->_request->getParam('date-end'); 
    $productName = $this->_request->getParam('product-name'); 
    $staffName   = $this->_request->getParam('staff-name'); 
    $email       = $this->_request->getParam('email');
    $submit      = $this->_request->getParam('submit');
    $export      = $this->_request->getParam('export');
    $controller  = $this->_request->getControllerName();

    $curTime     = date('d/m/Y-H:i:s',time());
   
    
    $curParams   = ltrim($_SERVER['REQUEST_URI'],'/'.$controller.'?');
    $limitItems  = LIMITATION;
    $data        = $QPgpb_sales->selectSalesProduct($arrayUser,$curPage,$limitItems,$params);
    if(!empty($export)){
        $QPgpb_sales->exportFileExcel($data['totalDetail'],$controller.'-'.$curTime,$controller);
    }
    $_paginator = array(
        'itemCountPerPage' =>$limitItems,
        'pageRange'=>10,
        'currentPage'=>$curPage
    );
    $check = $QPgpb_sales->isExsitsStoreId(29605);
    $paginator = new My_Paginator();
    $paginator = $paginator->createPaginator($data['totalItems'],$_paginator);

    //Import file Excel
    if($this->_request->isPost()){
        // Delete biến POST nếu dữ liệu trùng 
        echo '
            <script>
                        if ( window.history.replaceState ) {
                            window.history.replaceState( null, null, window.location.href );
                        }
            </script>
        ';
        $file = new Zend_File_Transfer_Adapter_Http();
        $upload = $file->getFileInfo();
        $file = $upload['file-upload']['tmp_name'];
        if($file){
            $data = $QPgpb_sales->importFileExcel($file);
            if($data['flag']){
                $this->view->success = $data['message'];
            }else{
                $this->view->error = $data['message'];
            }
        }

      
    }
    

    $this->view->stt         = ($curPage == 1) ? 0 :($limitItems*($curPage-1))  ;
    $this->view->data        = $data['detailPerPage'];
    $this->view->paginator   = $paginator;
    $this->view->curPage     = $curPage;
    $this->view->curParams   = $curParams;
    $this->view->dateStart   = $dateStart;
    $this->view->dateEnd     = $dateEnd;
    $this->view->productName = $productName;
    $this->view->staffName   = $staffName;
    $this->view->email       = $email;
    $this->view->export      = $export;
    $this->view->submit      = $submit;



