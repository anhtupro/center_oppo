<?php

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $QPgpb_sales  = new Application_Model_KpiSale();
    $dataColExcel =[
        'store id',
        'Ngày bán(Năm-Tháng-Ngày)',
        'Mã Nhân viên bán hàng',
        'Mã Sản phẩm',
        'Số lượng'
    ];

    $QPgpb_sales->templateExcel('File_Template',$dataColExcel);