<?php   
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $QDevices    = new Application_Model_Devices();
        $QPgpb_sales = new Application_Model_KpiSale();
    
        $userCode = $userStorage->code;
        $userId   = $userStorage->id;
        // amount_sale: (2) ["1", "1"]
        // controller: "kpi-sale"
        // date_sale: (2) ["2021-07-20", "2021-07-08"]
        // method: "create"
        // module: "default"
        // product_id: (2) ["37", "38"]
        // store: (2) ["29607", "29606"]

        
        $params   = $this->_request->getParams();
        $method = $this->_request->getParam('method');
        
        if($method == 'create'){
            $arrAmountSale = $params['amount_sale'];
            $arrDateSale   = $params['date_sale'];
            $arrProductId  = $params['product_id'];
            $arrStore      = $params['store'];
            foreach($arrAmountSale as $key => $value){
                $bonus = $QDevices->selectBonusByIdDevice($arrProductId[$key],['bonus_amount']);
                $bonus = $bonus['bonus_amount'];
                $data['staff_id']   = $userId;
                $data['store_id']   = $arrStore[$key];
                $data['product_id'] = $arrProductId[$key];
                $data['pgpb_sales_date']   = $arrDateSale[$key];
                $data['pgpb_sales_amount'] = $arrAmountSale[$key];
                $data['pgpb_sales_total']  = $bonus*$arrAmountSale[$key];
                $QPgpb_sales->insertData($data);
            }
            echo json_encode(['status'=>0]);
        }
    