<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $QPgpb_sales = new Application_Model_KpiSale();


    $data =  $this->_request->getParams();
    if($data['method'] == 'detail-product'){
        $idStaff    = $data['id'];
        $data = $QPgpb_sales->analyticsProduct($idStaff);
        echo json_encode($data['detail']);
    } 
    