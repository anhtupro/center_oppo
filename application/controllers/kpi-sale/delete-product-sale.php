<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $QPgpb_sales  = new Application_Model_KpiSale();
    $idPgpbSales  = $this->_request->getParam('id');
    $controller   = $this->_request->getControllerName();
    $action       = $this->_request->getActionName(); 
    $curParams    = basename($_SERVER['REQUEST_URI'],'&id='.$idPgpbSales);
    $curParams    =  str_replace('delete-product-sale?',"",$curParams);
    
    $QPgpb_sales->deletePgpbSalesById($idPgpbSales);
   
    $this->redirect('/kpi-sale?pa'.$curParams);
    

