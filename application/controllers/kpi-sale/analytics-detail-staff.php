<?php
    $QPgpb_sales = new Application_Model_KpiSale();
    $limitItems  = LIMITATION;
    $curPage     = $this->_request->getParam('page',1);
    ($curPage == 0) ? $curPage =1 : $curPage;


    $data  = $QPgpb_sales->analyticsStaff($curPage,$limitItems);
    $_paginator = array(
                        'itemCountPerPage'=>$limitItems,
                        'rangePage'       =>10,
                        'currentPage'     =>$curPage   
                );
    $paginator = new My_Paginator();
    $paginator = $paginator->createPaginator($data['totalItems'],$_paginator);


    $this->view->stt       = ($curPage == 1) ? 0 :($limitItems*($curPage-1));
    $this->view->data      = $data;
    $this->view->paginator = $paginator;