<?php
    $QPgpb_sales = new Application_Model_KpiSale();
    $QDevices    = new Application_Model_Devices();
    $page        = $this->_request->getParam('page',1);
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $userId      = $userStorage->id; 
    
    $product = $QDevices->getAllDevices();
    $store   = $QPgpb_sales->selectStoreByStaff($userId);

    $this->view->page    = $page;
    $this->view->store   = $store;
    $this->view->product = $product;
 

