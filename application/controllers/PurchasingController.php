<?php

class PurchasingController extends My_Controller_Action
{
    
    public function ajaxGetSupplierAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'ajax-get-supplier.php';
    }
    
    public function ajaxLoadTypeAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'ajax-load-type.php';
    }
    
    public function ajaxGetSupplierPriceAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'ajax-get-supplier-price.php';
    }
    
    public function categoryAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.'category.php';
    }
    public function categoryCreateAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.'category-create.php';

    }
    public function categorySaveAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.'category-save.php';

    }
    public function categoryDelAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.'category-del.php';

    }
    ///
    public function codeAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code'.DIRECTORY_SEPARATOR.'code.php';
    }
    public function codeCreateAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code'.DIRECTORY_SEPARATOR.'code-create.php';

    }
    public function codeSaveAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code'.DIRECTORY_SEPARATOR.'code-save.php';

    }
    public function codeDelAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code'.DIRECTORY_SEPARATOR.'code-del.php';

    }
    ////
    
    //////
    public function supplierAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'supplier'.DIRECTORY_SEPARATOR.'supplier.php';
    }
    public function supplierCreateAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'supplier'.DIRECTORY_SEPARATOR.'supplier-create.php';

    }
    public function supplierSaveAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'supplier'.DIRECTORY_SEPARATOR.'supplier-save.php';

    }
    public function supplierDelAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'supplier'.DIRECTORY_SEPARATOR.'supplier-del.php';
    }
    public function supplierDelFileAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'supplier'.DIRECTORY_SEPARATOR.'supplier-del-file.php';
    }
    ////

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $back_url = "purchasing/purchasing-request";
        echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1)</script>'; 
    }
    
    public function purchasingRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'purchasing-request.php';
    }
    
    public function viewRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'view-request.php';
    }
    
    public function viewRequestMobileAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'view-request-mobile.php';
    }

    public function createPurchasingRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'create-purchasing-request.php';

    }
    
    public function savePurchasingRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-purchasing-request.php';
    }

    public function purchasingRequestDelFileAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'purchasing-request-del-file.php';

    }
    
    public function delPurchasingRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'del-purchasing-request.php';
    }

    public function confirmPurchasingRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'confirm-purchasing-request.php';
    }
    
    public function approvePurchasingRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'approve-purchasing-request.php';
    }
    
    public function savePurchasingRequestContractAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-purchasing-request-contract.php';
    }
    
    // ko dùng nữa
//    public function confirmRequestAction()
//    {
//        require_once 'purchasing'.DIRECTORY_SEPARATOR.'confirm-request.php';
//    }
//
//    public function saveConfirmRequestAction()
//    {
//        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-confirm-request.php';
//    }

    public function createOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'create-order.php';

    }

    public function saveOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-order.php';

    }
    
        /*
     * thong july 18 2020
     * */
    public function manageTypeAction(){
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'manager'.DIRECTORY_SEPARATOR.'manage-type.php';
    } 

    public function updateTypeAction(){
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'manager'.DIRECTORY_SEPARATOR.'update-type.php';
    } 

    public function hideTypeAction(){
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'manager'.DIRECTORY_SEPARATOR.'hide-type.php';
    }
    /***************************************/

    public function saveConfirmOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-confirm-order.php';

    }

    public function orderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'order.php';

    }

    public function confirmOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'confirm-order.php';

    }

    public function viewOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'view-order.php';

    }

    public function codeSupplierAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code-supplier'.DIRECTORY_SEPARATOR.'code-supplier.php';

    }
    public function codeSupplierCreateAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code-supplier'.DIRECTORY_SEPARATOR.'code-supplier-create.php';

    }

    public function codeSupplierSaveAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code-supplier'.DIRECTORY_SEPARATOR.'code-supplier-save.php';

    }

    public function codeSupplierDelAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'code-supplier'.DIRECTORY_SEPARATOR.'code-supplier-del.php';

    }

    public function editOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'edit-order.php';

    }

    public function saveEditOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-edit-order.php';

    }

    public function delOrderAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'del-order.php';
    }
    
    public function listRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'list-request.php';
    }

    //////// COST chi phí
    
    public function costAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'cost.php';
    }
    
    public function createCostAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'create-cost.php';
    }
    public function viewCostAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'view-cost.php';
    }
    public function delCostAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'del-cost.php';
    }
    
    
    public function saveConfirmCostAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-confirm-cost.php';
    }
    
    public function purchasingOrderDelFileAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'purchasing-order-del-file.php';
    }
    
    public function savePurchasingCostAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-purchasing-cost.php';
    }
        
    public function purchasingCostDelFileAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'purchasing-cost-del-file.php';
    }
    
    public function searchPmodelAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $keyword = $this->getRequest()->getParam('keyword');

        /* SEarch */
        $db = Zend_Registry::get('db');

        $searchSplit = explode(' ', $keyword);

        $searchQueryItems = array();
        foreach ($searchSplit as $searchTerm) {
            $searchQueryItems[] = "name LIKE '%" . ($searchTerm) . "%'";
        }

        $query = 'SELECT * FROM pmodel_code' . (!empty($searchQueryItems) ? ' WHERE ' . implode(' AND ', $searchQueryItems) : '');
        $pmodel_code = $db->fetchAll($query);

        /* END SEarch */

        $data_pmodel = [];
        foreach ($pmodel_code as $key => $value) {
            $data_pmodel[$value['id']] = array(
                'id'     => $value['id'],
                'name'   => $value['name']
            );
        }

        $data = [];
        $data['code'] = 1;
        $data['data'] = $data_pmodel;

        echo json_encode($data);
    }

    public function searchPriceAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $pmodel_code_id        = $this->getRequest()->getParam('pmodel_code_id');

        $QPmodelCode   = new Application_Model_PmodelCode();
        
        $params = array(
            'pmodel_code_id'    => $pmodel_code_id
        );

        $price = $QPmodelCode->getPrice($params);

        $data = [];
        $data['code'] = 1;
        $data['price'] = $price['price'];

        echo json_encode($data);
    }
    
    public function printRequestAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'print-request.php';
    
    }
    
    public function printPoAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'print-po.php';
    
    }

    public function printCostAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'print-cost.php';
    
    }

    public function showInfoAction()
    {
        
        $pmodel_code_id = $this->getRequest()->getParam('pmodel_code_id');
        $QPmodelCode   = new Application_Model_PmodelCode();

        $data = $QPmodelCode->getCodeSupplier($pmodel_code_id);

        $html = '<table class="table table-bordered">';
        $html .= '<tr><th>Product</th><th>Supplier</th><th>Price</th><tr>';

        foreach($data as $key=>$value){
            $html .= '<tr><td>'.$value['code'].' - '.$value['name'].'</td><td>'.$value['supplier_name'].'</td><td>'.number_format($value['price']).'</td><tr>';
        }
        echo $html;exit;


    }
    
    ///////Chi phí dự trù cho sale
    public function estimatesAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'estimates.php';
    }
    //save popup 1: lưu chi phí dự trù
    public function saveEstimatesAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-estimates.php';
    }
    
    public function getDataEstimatesAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'get-data-estimates.php';
    }
    //save popup 2: lưu chi phí thực tế
    public function saveEstimatesRealityAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'save-estimates-reality.php';
    }
    
    public function getDataEstimatesRealityAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'get-data-estimates-reality.php';
    }
    
    public function estimatesDelFileAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'estimates-del-file.php';
    }
    
    public function createSubmissionSupplierAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'create-submission-supplier.php';
    }
    
    public function iframeSubmissionSupplierAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'iframe-submission-supplier.php';
    }
    
    public function printSubmissionSupplierAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'print-submission-supplier.php';
    }
    
    public function purchasingReviewSupplierAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'purchasing-review-supplier.php';
    }
    
    public function pruchasingReviewSupplierSaveAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'pruchasing-review-supplier-save.php';
    }


    public function listPurchasingRatingAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'list-purchasing-rating.php';

    }
    public function purchasingRatingAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'purchasing-rating.php';

    }
    public function savePurchasingRatingAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'save-purchasing-rating.php';
        
    }
    public function exportListPurchasingRating($data,$export)
    {
        $this->_helper->viewRenderer->setNoRender(true);
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'export-list-purchasing-rating.php';

    }
    public function purchasingRatingDelAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'purchasing-rating-del.php';

    }
    public function listRatingHistoryAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'list-rating-history.php';

    }
    public function listRatingDetailAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'list-rating-detail.php';

    }
    public function deletePurchasingRatingFileAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'rating'.DIRECTORY_SEPARATOR.'delete-purchasing-rating-file.php';

    }
    
    
    public function createPurchasingRequestNewAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'create-purchasing-request-new.php';

    }
    
    public function purchasingRequestNewAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'purchasing-request-new.php';

    }
    public function projectManagingAction()
    {
        require_once 'purchasing' . DIRECTORY_SEPARATOR . 'project-managing.php';
    }
    
    public function mapSupplierAction()
    {
        require_once 'purchasing' . DIRECTORY_SEPARATOR . 'map-supplier.php';
    }
    
    public function mapCategoryAction()
    {
        require_once 'purchasing' . DIRECTORY_SEPARATOR . 'map-category.php';
    }
    
    public function mapSupplierListAction()
    {
        require_once 'purchasing' . DIRECTORY_SEPARATOR . 'map-supplier-list.php';
    }
    
    public function mapCategoryListAction()
    {
        require_once 'purchasing' . DIRECTORY_SEPARATOR . 'map-category-list.php';
    }
    
    public function projectCreateAction()
    {
        $this->_helper->layout->disableLayout();

        if ( ! $this->getRequest()->isXmlHttpRequest() ) {
            exit('0');
        }
        $project_name = $this->getRequest()->getParam('project_name');
        $project_remark = $this->getRequest()->getParam('project_remark');
        $data=array(
            'name' => $project_name,
            'remark' => $project_remark
        );

        $QPurchasingProject = new Application_Model_PurchasingProject();
        $res=$QPurchasingProject->insert($data);
        if(!empty($res)){
            echo $res;
            exit;
        }
        exit('-1');
    }
    
    public function projectUpdateAction()
    {
        $this->_helper->layout->disableLayout();

        if ( ! $this->getRequest()->isXmlHttpRequest() ) {
            exit('0');
        }
        $project_id = $this->getRequest()->getParam('project_id');
        $project_name = $this->getRequest()->getParam('project_name');
        $project_remark = $this->getRequest()->getParam('project_remark');
        $data=array(
            'name' => $project_name,
            'remark' => $project_remark
        );

        $QPurchasingProject = new Application_Model_PurchasingProject();
        $where=$QPurchasingProject->getAdapter()->quoteInto('id = ?',$project_id);
        $res=$QPurchasingProject->update($data,$where);
        if(!empty($res)){
            echo $res;
            exit;
        }
        exit('-1');
    }
    
    public function projectDeleteAction()
    {
        $this->_helper->layout->disableLayout();

        if ( ! $this->getRequest()->isXmlHttpRequest() ) {
            exit('0');
        }
        $project_id = $this->getRequest()->getParam('project_id');
        $data=array(
            'del'=> 1
        );

        $QPurchasingProject = new Application_Model_PurchasingProject();
        $where=$QPurchasingProject->getAdapter()->quoteInto('id = ?',$project_id);
        $res=$QPurchasingProject->update($data,$where);
        if(!empty($res)){
            echo $res;
            exit;
        }
        exit('-1');
    }

    public function projectUpdatePositionAction()
    {
        $this->_helper->layout->disableLayout();
        $arrayProjectIndex = $this->getRequest()->getParam('array_project');
        foreach ($arrayProjectIndex as $arr_two_project) {
            if(is_array($arr_two_project) AND $arr_two_project[0] != $arr_two_project[1]){
                $this->projectSwapPosition($arr_two_project[0],$arr_two_project[1]);
            }
        }
        echo 1;exit;
    }

    public function getProjectInformationAction()
    {
        $this->_helper->layout->disableLayout();

        if ( ! $this->getRequest()->isXmlHttpRequest() ) {
            exit('0');
        }
        $project_id = $this->getRequest()->getParam('project_id');

        $QPurchasingProject = new Application_Model_PurchasingProject();
        $where = $QPurchasingProject->getAdapter()->quoteInto('id = ?', $project_id);
        $res=$QPurchasingProject->fetchRow($where);

        if(!empty($res)){
            echo json_encode($res->toArray());
            exit;
        }
        exit('-1');
    }
    
    public function menuAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $lang = $userStorage->defaut_language;
        $id = $this->getRequest()->getParam('id');
//lay menu trong group
        if (isset($userStorage->menu) && $userStorage->menu) {
            $groupMenu = $userStorage->menu;

        } else {
            $QMenu = new Application_Model_Menu();
            $where = $QMenu->getAdapter()->quoteInto('group_id = ?', $group_id);
            $menu = $QMenu->fetchAll($where, array('parent_id', 'position'));
        }
//lay menu trong staff privielege



        $QMenu2 = new Application_Model_Menu2();
        $staffMenu = $QMenu2->getMenuPermission();
//lay menu cap 2
        $db          = Zend_Registry::get('db');

        $select = $db->select()->from(array('m' => 'menu'), array(
            'id'=>'m.id'
        ));
        $select->joinLeft(array('m1' => 'menu'), 'm1.parent_id = m.id', array())
            ->where('m.parent_id =?',$id)
            ->group('m.id');

        $secondLevelMenuId = $db->fetchAll($select);
//lay ra danh sach ID menu cap 3
        $listId=array();
        foreach ($secondLevelMenuId as $Id){
            $listId[]=$Id['id'];
        }



//lay danh sach id cua menu cap 3
        $hasChildIDs=array();

        foreach ($listId as $id){
            $select = $db->select()->from('menu', array('id'))
                ->where('parent_id=?',$id);
            $result = $db->fetchAll($select);
            if(count($result)>0){
                $hasChildIDs[]=$id;
            }
        }



        $this->view->groupMenu=$groupMenu;

        
        $this->view->listId=$listId;
        $this->view->id=$this->getRequest()->getParam('id');
        $this->view->staffMenu=$staffMenu;
        $this->view->lang=$lang;
        $this->view->hasChildIDs=$hasChildIDs;

    }

    
    public function ajaxSubmitPurchasingRatingAction()
    {
        $this->_helper->layout->disableLayout();
        require_once 'purchasing' . DIRECTORY_SEPARATOR .'rating'.DIRECTORY_SEPARATOR. 'ajax-submit.php';
    }
    public function ajaxActiveRatingAction()
    {
        $this->_helper->layout->disableLayout();
        require_once 'purchasing' . DIRECTORY_SEPARATOR .'rating'.DIRECTORY_SEPARATOR. 'ajax-active-rating.php';
    }
    public function ajaxDeleteRatingAction()
    {
        $this->_helper->layout->disableLayout();
        require_once 'purchasing' . DIRECTORY_SEPARATOR .'rating'.DIRECTORY_SEPARATOR. 'ajax-delete-rating.php';
    }
    
    public function rejectPurchasingRequestAction() {
        
        require_once 'purchasing' . DIRECTORY_SEPARATOR . 'reject-purchasing-request.php';
        
    }
    
    /**********Staff Rating*************/
    public function timeListAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'staff-rating'.DIRECTORY_SEPARATOR.'time-list.php';
    }
    
    public function timeRatingAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'staff-rating'.DIRECTORY_SEPARATOR.'time-rating.php';
    }
    
    public function setEditPrAction()
    {
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'set-edit-pr.php';
    } 

    public function loadBudgetAction(){
        require_once 'purchasing'.DIRECTORY_SEPARATOR.'load-budget.php';
    }
}
