<?php

class KpiReportController extends My_Controller_Action{

	public function init(){
		$this->view->act = $this->getRequest()->getActionName();
	}

	public function kpiSettingViewAction(){
		$pars = array_merge(
			array(
				'product_name' => null,
				'policy' => null,
				'from_date' => null,
				'to_date' => null,
				'title' => null,
				'page' => 1
			),
			$this->_request->getParams()
		);

		$pars['product_name'] = trim($pars['product_name']);
		$pars['limit'] = 25;
		$pars['offset'] = ($pars['page'] - 1) * $pars['limit'];
		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		$pars['policy'] = empty($pars['policy'])?null:$pars['policy'];
		$pars['title'] = empty($pars['title'])?null:$pars['title'];


		$gkl = new Application_Model_GoodKpiLog2;
		// Start export
		if(!empty($pars['export'])){
			unset($pars['limit'], $pars['offset']);

			$res = $gkl->GetAll($pars);

			require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();

			$heads = array(
				'A' => 'Policy',
				'B' => 'Title',
				'C' => 'Product',
				'D' => 'Color',
				'E' => 'KPI',
				'F' => 'Effect',
				'G' => 'Expire'
			);
			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			foreach($heads as $key => $value)
				$sheet->setCellValue($key.'1', $value);
			$sheet->getStyle('A1:G1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(30);
			$sheet->getColumnDimension('B')->setWidth(10);
			$sheet->getColumnDimension('C')->setWidth(15);
			$sheet->getColumnDimension('D')->setWidth(10);
			$sheet->getColumnDimension('E')->setWidth(10);
			$sheet->getColumnDimension('F')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(15);

			foreach($res['data'] as $key => $value){
				$sheet->setCellValue('A'.($key + 2), $value['policy']);
				$sheet->setCellValue('B'.($key + 2), $value['title']);
				$sheet->setCellValue('C'.($key + 2), $value['product']);
				$sheet->setCellValue('D'.($key + 2), $value['color']);
				$sheet->setCellValue('E'.($key + 2), $value['kpi']);
				$sheet->setCellValue('F'.($key + 2), date('d/m/Y', $value['from_date']));
				$sheet->setCellValue('G'.($key + 2), ($value['to_date'] ? date('d/m/Y', $value['to_date']) : null));
			}

			$filename = 'Report KPI_'.date('d-m-Y');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
			$objWriter->save('php://output');

			exit;
		}
		// End export

		$this->view->kpi = $gkl->GetAll($pars);
		$QPolicy = new Application_Model_Policy();
		$policy = $QPolicy->selectPolicy();
		$this->view->policy = $policy['data'];

		unset($pars['module'], $pars['controller'], $pars['action'], $pars['page']);
		$pars['url'] = HOST.'kpi-report/kpi-setting-view?'.http_build_query($pars).'&';
		$this->view->pars = $pars;
		// echo '<pre>';print_r($pars);die;
	}

	public function kpiByPolicyAction(){
		$pars = array_merge(
			array(
				'from_date' => date('1/m/Y', strtotime("-1 months")),
				'to_date' => date('t/m/Y' , strtotime("-1 months")),
				'policy' => null,
				'title' => null,
				'name' => null,
				'code' => null,
				'area' => null,
				'page' => 1,
			),
			$this->_request->getParams()
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		
		$this->view->from_date = $pars['from_date'];
		$this->view->to_date = $pars['to_date'];

		if($this->_request->getParam('export'))
		{
			$QPolicy = new Application_Model_Policy();
			$all_policy = $QPolicy->selectPolicy()['data'];

			require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'A' => 'Name',
                'B' => 'Code',
                'C' => 'Area',
				'D' => 'Title',
            );
			$array_ez = range('E', 'Z');
			$temp = 0;
			foreach($all_policy as $key => $value)
			{
				$heads[$array_ez[$key]] = $value['title'];
				$temp = $key+1;
			}
			$heads[$array_ez[$temp]] = 'Total';
			$PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:'.$array_ez[$temp].'1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(30);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(20);
			$sheet->getColumnDimension('D')->setWidth(10);
			foreach($all_policy as $key => $value)
			{
				$heads[$array_ez[$key]] = $value['title'];
				$sheet->getColumnDimension($array_ez[$key])->setWidth(30);
			}
			$sheet->getColumnDimension($array_ez[$temp])->setWidth(20);

			$ik = new Application_Model_ImeiKpi2();
			$data_export =  $ik->GetByTitle(0, 10, $pars);;

			foreach($data_export['data'] as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['staff_name']);
				$sheet->setCellValue('B' . ($key+2), $value['code']);
				$sheet->setCellValue('C' . ($key+2), $value['area_name']);
				$sheet->setCellValue('D' . ($key+2), $value['title']);
				$total_kpi = 0;
				foreach($all_policy as $key_policy => $value_policy)
				{
					if($value_policy['id'] == $value['policy_id'])
					{
						$data_kpi = $value['kpi'];
					}
					else
					{
						$data_kpi = null;
					}
					$total_kpi += $data_kpi;
					$sheet->setCellValue($array_ez[$key_policy] . ($key+2), $data_kpi);
				}
				$sheet->setCellValue($array_ez[$temp] . ($key+2), $total_kpi);
			}

			$filename = 'KPI By Policy - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
		}
		
		$this->view->pars = $pars;
		$total = 0;
		$limit = 50;
		$offset = ($pars['page']-1)*$limit;
		$ik = new Application_Model_ImeiKpi2();
		// $ik->ReportByPolicy(10, 0, $pars); die;
		$data = $ik->GetByTitle($limit, $offset, $pars);
		// print_r($data['data']); die;
		$this->view->data = $data['data'];

		$QPolicy = new Application_Model_Policy();
		$policy = $QPolicy->selectPolicy();
		$this->view->policy = $policy['data'];

		$this->view->limit = $limit;
        $this->view->offset = $pars['page'];
        $this->view->total = $data['total'];

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));
		unset($pars['page']);
	
        $this->view->url = HOST . 'kpi-report/kpi-by-policy' . ($pars ? '?' . http_build_query($pars) . '&' : '?');

		$QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;
	}

	public function groupByPolicyAction()
	{
		$pars = array_merge (
			array(
				'from_date' => date('1/m/Y', strtotime("-1 months")),
				'to_date' => date('t/m/Y' , strtotime("-1 months")),
				'policy' => null,
				'title' => null,
				'page' => 1
			),
			$this->_request->getParams()
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;

		if($this->_request->getParam('export'))
		{
			require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'A' => 'Title',
                'B' => 'Policy',
				'C' => 'Sell out',
                'D' => 'Total',
            );

			$PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:D1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(20);
            $sheet->getColumnDimension('B')->setWidth(30);
			$sheet->getColumnDimension('C')->setWidth(30);
            $sheet->getColumnDimension('D')->setWidth(30);

			$ik = new Application_Model_ImeiKpi2();
			$data_export =  $ik->GroupByPolicy(null, null, $pars);
			
			foreach($data_export['data'] as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['title']);
				$sheet->setCellValue('B' . ($key+2), $value['policy']);
				$sheet->setCellValue('C' . ($key+2), $value['sell_out']);
				$sheet->setCellValue('D' . ($key+2), $value['kpi']);
			}

			$filename = 'KPI Group By Policy - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
		}

		$QPolicy = new Application_Model_Policy();
		$policy = $QPolicy->selectPolicy();
		$this->view->policy = $policy['data'];

		$total = 0;
		$limit = 50;
		$offset = ($pars['page']-1)*$limit; 
		$ik = new Application_Model_ImeiKpi2();
		$data = $ik->GroupByPolicy($limit, $offset, $pars);
		$this->view->pars = $pars;
		$this->view->data = $data['data'];

		$this->view->total = $data['total'];

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));
		unset($pars['page']);

        $this->view->url = HOST . 'kpi-report//group-by-policy' . ($pars ? '?' . http_build_query($pars) . '&' : '?');
	}

	public function byStaffAction()
	{
		$pars = array_merge(
			array(
				'from_date' => date('1/m/Y', strtotime("-1 months")),
				'to_date' => date('t/m/Y' , strtotime("-1 months")),
				'name' => null,
				'code' => null,
				'title' => null,
				'area' => null,
				'page' => 1,
			),
			$this->_request->getParams()
		);
		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		
		$this->view->from_date_ajax = $pars['from_date'] . " 00:00:00";
		$this->view->to_date_ajax = $pars['to_date'] . " 23:59:59";
		
		if($this->_request->getParam('export'))
		{

			require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'A' => 'Name',
                'B' => 'Code',
                'C' => 'Area',
				'D' => 'KPI PG',
				'E' => 'Sell out PG',
				'F' => 'KPI Sale',
				'G' => 'Sale out sale',
				'H' => 'KPI PB Sale',
				'I' => 'Sale out sale',
				'J' => 'Title',
				'K' => 'Total',
            );

			$PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:L1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(35);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(20);
			$sheet->getColumnDimension('D')->setWidth(15);
			$sheet->getColumnDimension('E')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(15);
			$sheet->getColumnDimension('H')->setWidth(15);
			$sheet->getColumnDimension('I')->setWidth(15);
			$sheet->getColumnDimension('J')->setWidth(15);
			$sheet->getColumnDimension('K')->setWidth(15);
			$sheet->getColumnDimension('L')->setWidth(15);

			$ik = new Application_Model_ImeiKpi2();
			
			$data_export =  $ik->GetAllByStaff(null, null, $pars);
			foreach($data_export['data'] as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['staff_name']);
				$sheet->setCellValue('B' . ($key+2), $value['code']);
				$sheet->setCellValue('C' . ($key+2), $value['area']);
				$sheet->setCellValue('D' . ($key+2), $value['kpi_pg']);
				$sheet->setCellValue('E' . ($key+2), $value['sell_out_pg']);
				$sheet->setCellValue('F' . ($key+2), $value['kpi_sale']);
				$sheet->setCellValue('G' . ($key+2), $value['sell_out_sale']);
				$sheet->setCellValue('H' . ($key+2), $value['kpi_pb_sale']);
				$sheet->setCellValue('I' . ($key+2), $value['sell_out_pb_sale']);
				$sheet->setCellValue('J' . ($key+2), $value['title_name']);
				$sheet->setCellValue('K' . ($key+2), $value['kpi_pg']+$value['kpi_sale']+$value['kpi_pb_sale']);
				
			}

			$filename = 'KPI Group By Staff - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
		}
		
		$QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;

		$total = 0;
		$limit = 50;
		$offset = ($pars['page']-1)*$limit; 
		$ik = new Application_Model_ImeiKpi2();
		$data = $ik->GetAllByStaff($limit, $offset, $pars);
		$this->view->pars = $pars;
		$this->view->data = $data;
		$this->view->limit = $limit;
        $this->view->offset = $pars['page'];
        $this->view->total = $data['total'];

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));
		unset($pars['page']);

        $this->view->url = HOST . 'kpi-report/by-staff' . ($pars ? '?' . http_build_query($pars) . '&' : '?');


        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
	}

	public function ajaxDetailKpiByPolicyAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->isXmlHttpRequest())
		{
			$pars = $this->_request->getParams();
			$QImeiKpi2 = new Application_Model_ImeiKpi2();

			$data = $QImeiKpi2->getDetailKpiByPolicy($pars);
			echo json_encode($data); die;
		}
	}

	public function ajaxDetailKpiStaffByPolicyAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

		$table_name = $this->getRequest()->getParam('table_name');
		$staff_id = $this->getRequest()->getParam('staff_id');
		$from_date = $this->getRequest()->getParam('from_date');
		$to_date = $this->getRequest()->getParam('to_date');
		
		$pars = array(
			'table_name' => $table_name,
			'staff_id' => $staff_id,
			'from_date' => $from_date,
			'to_date' => $to_date,
		);
		
		$QImeiKpi2 = new Application_Model_ImeiKpi2();

		$data = $QImeiKpi2->getDetailKpiStaffByPolicy($pars);
		echo json_encode($data); die;
	}

	public function exportStaffAction()
	{
		$export = $this->getRequest()->getParam('export');
		$from_date = $this->getRequest()->getParam('from_date', date('1/m/Y', strtotime("-1 months")));
		$to_date = $this->getRequest()->getParam('to_date', date('t/m/Y' , strtotime("-1 months")));
		$code = $this->getRequest()->getParam('code');

		$pars = array(
			'from_date' => $from_date,
			'to_date' => $to_date,
			'code' => $code,
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : date('Y-m-1');
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : date('Y-m-d');
		

		if($export)
		{
			set_time_limit(0);
			$QImeiKpi2 = new Application_Model_ImeiKpi2();
			$data = $QImeiKpi2->DetailStaff($pars);

			require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'A' => 'Name',
				'B' => 'Code',
                'C' => 'Area',
                'D' => 'Title',
				'E' => 'Good name',
				'F' => 'Color',
				'G' => 'Policy',
				'H' => 'Sell out',
				'I' => 'KPI',
				'J' => 'Total',
            );

			$PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:J1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(35);
            $sheet->getColumnDimension('B')->setWidth(25);
            $sheet->getColumnDimension('C')->setWidth(25);
			$sheet->getColumnDimension('D')->setWidth(15);
			$sheet->getColumnDimension('E')->setWidth(15);
			$sheet->getColumnDimension('F')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(15);
			$sheet->getColumnDimension('H')->setWidth(20);
			$sheet->getColumnDimension('I')->setWidth(20);
			$sheet->getColumnDimension('J')->setWidth(20);

			foreach($data as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['staff_name']);
				$sheet->setCellValue('B' . ($key+2), $value['code']);
				$sheet->setCellValue('C' . ($key+2), $value['area_name']);
				$sheet->setCellValue('D' . ($key+2), $value['title']);
				$sheet->setCellValue('E' . ($key+2), $value['good_name']);
				$sheet->setCellValue('F' . ($key+2), $value['color_name']);
				$sheet->setCellValue('G' . ($key+2), $value['policy_title']);
				$sheet->setCellValue('H' . ($key+2), $value['sell_out']);
				$sheet->setCellValue('I' . ($key+2), $value['kpi']);
				$sheet->setCellValue('J' . ($key+2), $value['kpi']*$value['sell_out']);
			}

			$filename = 'KPI Detail Staff - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
		}

		$pars['from_date'] = $from_date;
		$pars['to_date'] = $to_date;
		$this->view->pars = $pars;
	}

	public function storeByStaffAction()
	{
		$staff_id = $this->getRequest()->getParam('staff_id');
		$from_date = $this->getRequest()->getParam('from_date', date('1/m/Y', strtotime("-1 months")));
		$to_date = $this->getRequest()->getParam('to_date', date('t/m/Y' , strtotime("-1 months")));

		$pars = array(
			'staff_id' => $staff_id,
			'from_date' => $from_date,
			'to_date' => $to_date,
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		
		if(!empty($staff_id))
		{
			$ik = new Application_Model_ImeiKpi2();
			$data = $ik->storeByStaff($pars);
			$this->view->data = $data;
		}

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));

		$this->view->pars = $pars;
	}

	public function ajaxSearchStaffAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->isXmlHttpRequest())
		{
			$input = $this->getRequest()->getParam('input');

			$ik = new Application_Model_ImeiKpi2();
			$data = $ik->SearchStaff($input);
			echo json_encode($data); die;
		}
	}

}