<?php
    class HappyTimeController extends My_Controller_Action
    {
        public function indexAction()
        {
            $page = $this->getRequest()->getParam('page', 1);
            $imei_sn = $imei_sn_input = $this->getRequest()->getParam('imei_sn', null);
            $date = $this->getRequest()->getParam('created_at', null);
            $imei_sn = empty($imei_sn)?null: TRIM($imei_sn);

            if($imei_sn!= null)
            {
                $imei_sn = explode(PHP_EOL, $imei_sn);
                $imei_list = empty($imei_sn)?null:trim('(' . implode(",", $imei_sn) . ')');
            }
            else{
                $imei_list = null;
            }

            $params = array(
                'limit' => 50,
                'imei_sn' => $imei_list,
            );

            $params['created_at'] = !empty($date) ? DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d') : null;
            $params['offset'] = ($page - 1) * $params['limit'];

            $QImeiHappyTime = new Application_Model_ImeiHappyTime();
            $data = $QImeiHappyTime->fetchPagination($params);

            $params['created_at'] = $date;
            $params['imei_sn'] = $imei_sn_input;

            $this->view->params = $params;

            $this->view->data = $data['data'];
            $this->view->total = $data['total'];
            $this->view->limit = $params['limit'];
            $this->view->offset = $params['offset'];
            $this->view->url = HOST . 'happy-time' . ($pars ? '?' . http_build_query($params) . '&' : '?');


            $flashMessenger = $this->_helper->flashMessenger;
            $this->view->messages_error= $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages      = $flashMessenger->setNamespace('success')->getMessages();
        }

        public function updateAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $id = $this->getRequest()->getParam('id');
            $status = $this->getRequest()->getParam('status');

            $params = array(
                'id' => $id,
                'status' => $status
            );

            $QImeiHappyTime = new Application_Model_ImeiHappyTime();
            $QImeiHappyTime->updateValue($params);
            $flashMessenger->setNamespace('success')->addMessage('Thêm Imei Happy Time thành công.');
            $this->_redirect("/happy-time");
        }

        public function addAction()
        {
            $submit = $this->getRequest()->getPost('submit', 0);
            $imei_sn = $this->getRequest()->getPost('imei_sn');
            $flashMessenger = $this->_helper->flashMessenger;
            
            if(!empty($submit))
            {
                $imei_sn = trim($imei_sn);
                $imei = explode(PHP_EOL, $imei_sn);

                $QImeiHappyTime = new Application_Model_ImeiHappyTime();
                if(!empty($imei))
                {
                    $values = array();
                    foreach($imei as $key => $val)
                    {
                        $values[] = "(" . $val . ", 1)"; 
                    }
                    $QImeiHappyTime->insertValue(implode(",", $values));
                    $flashMessenger->setNamespace('success')->addMessage('Thêm Imei Happy Time thành công.');
                    $this->_redirect("/happy-time");
                }
            }
        }
    }