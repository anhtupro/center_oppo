<?php
$flashMessenger = $this->_helper->flashMessenger;
try {
    $id = $this->getRequest()->getParam('id');

    if (!$id) {
        throw new Exception("Invalid ID");
    }

    $QStaff = new Application_Model_Staff();
    $staff = $QStaff->find($id);
    $staff = $staff->current();


    if (!$staff) {
        throw new Exception("Invalid ID");
    }

    $is_leader = 6;

    $pic = $this->getRequest()->getParam('store_ids');



    if (!$pic && ($pic && strlen($pic) == 0)) {
        // gỡ hết store của leader
    } else {
        $store_ids = explode(',', $pic);

        $store_ids = is_array($store_ids) ? array_filter($store_ids) : array();
        $store_ids = array_unique($store_ids);

        $joined_dates = $this->getRequest()->getParam('joined_dates', '{}');
        $joined_dates = json_decode($joined_dates, true);


        $QStaff         = new Application_Model_Staff();
        $QStore          = new Application_Model_Store();
        $QStoreLeader    = new Application_Model_StoreStaff();
        $QStoreLeaderLog = new Application_Model_StoreStaffLog();
        $QRegion         = new Application_Model_RegionalMarket();
        $QCasual         = new Application_Model_CasualWorker();
        $QLog            = new Application_Model_Log();
        $QDistributor = new Application_Model_Distributor();

        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();

        // print_r($this->userStorage->id);die;
        $QLogStore            = new Application_Model_LogStore();

        $region_cache    = $QRegion->get_cache_all();

        // lấy store cũ thuộc leader này
        $where = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null and is_leader = 6 and staff_id = ?', $id);
        $stores = $QStoreLeaderLog->fetchAll($where);

        // print_r($stores);die;

        $old_ids = array();

        foreach ($stores as $key => $store) {
            $old_ids[] = intval($store['store_id']);
        }

        // định dạng store id hiện tại
        foreach ($store_ids as $key => $s_id) {
            $store_ids[$key] = intval( trim( $s_id ) );
        }


        // print_r($old_ids);die;

        $del_ids = array_diff($old_ids, $store_ids);
        $del_ids = is_array($del_ids) ? array_filter($del_ids) : array();

        $new_ids = array_diff($store_ids, $old_ids);
        $new_ids = is_array($new_ids) ? array_filter($new_ids) : array();
        $time = time();
        

        // cập nhật d_id cho các store mới thêm
        foreach ($new_ids as $k => $s_id) {

            
        // Kiểm tra cửa hàng này có sales branshop nào đứng chưa, trường hợp gán cho sales

            $store_check = $QStore->find($s_id);
            $store_check = $store_check->current();

            if ($store_check['is_brand_shop'] != 1) {
                echo '<script>
                        parent.palert("Store '.$store_check['name'].' không thuộc Brandshop , bạn chỉ được quản lí những store thuộc Brandshop.");
                        parent.alert("Store '.$store_check['name'].' không thuộc Brandshop , bạn chỉ được quản lí những store thuộc Brandshop.");
                    </script>';
                exit;
            }


            $where = array();
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('store_id = ?', $s_id);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('is_leader = ?', 6);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null');
            $staff_store_check = $QStoreLeaderLog->fetchRow($where);

            if ($staff_store_check) {
                

                echo '<script>
                        parent.palert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] (<a href=\"'.HOST.'manage/sales-pg-view?id='.$staff_store_check['staff_id'].'\" target=\"_blank\">'
                            .$store_check['name'] .'</a>) đã có Sales Leader Brandshop phụ trách. Vui lòng kiểm tra lại.");

                        parent.alert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] ('
                            .$store_check['name'] .') đã có Sales Leader Brandshop phụ trách. Vui lòng kiểm tra lại.");
                    </script>';
                exit;
            }
            

            $data = array(
                'parent'    => $parent,
                'is_leader' => $is_leader,
                'store_id'  => $s_id,
                'staff_id'  => $id,
                'joined_at' => isset($joined_dates[$s_id]) && strtotime($joined_dates[$s_id]) ? strtotime($joined_dates[$s_id]) : $time,
                );

            $QStoreLeaderLog->insert($data);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $id,
                'store_id'  => $s_id,
                'is_leader' => $is_leader,
                'action'    => "ADD",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }

        // print_r($del_ids);die;

        foreach ($del_ids as $s_id) {

            $where = array();
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('store_id = ?', $s_id);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('joined_at IS NOT NULL', 1);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at IS NULL', 1);

            $data = array('released_at' => $time);
            $QStoreLeaderLog->update($data, $where);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $id,
                'store_id'  => $s_id,
                'is_leader' => $is_leader,
                'action'    => "DELETE",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }

        // print_r($data_log);die;
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = 'SALES BRANDSHOP - Bind('.$id.') - Stores('.$pic.')';

        //todo log
        $QLog->insert( array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ) );
    }

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');

    echo '<script>parent.location.href="'. HOST.'manage/sales-brandshop"</script>';
    exit;

} catch(Exception $ex) {
    echo '<script>
            parent.palert("'.$ex->getMessage().'");
            parent.alert("'.$ex->getMessage().'");
        </script>';
    exit;
}

exit;