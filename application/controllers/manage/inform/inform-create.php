<?php

        $id = $this->getRequest()->getParam('id');
        
        if ($id)
        {
            $QModel = new Application_Model_Inform();
            $rowset = $QModel->find($id);
            $inform = $rowset->current();
        
            if (!$inform)
                $this->_redirect(HOST . 'manage/inform');
        
            $this->view->inform = $inform;
        
            $QStaff = new Application_Model_Staff();
        
            $QInformTitle = new Application_Model_InformTitle();
            $where = $QInformTitle->getAdapter()->quoteInto('id = ?', $id);
            $old_objects = $QInformTitle->fetchAll($where);
            
            $old_team_objects = array();
        
            $QInformFile = new Application_Model_InformFile();
            $where = $QInformFile->getAdapter()->quoteInto('inform_id = ?', $id);
            $files = $QInformFile->fetchAll($where);
        
        
            foreach ($old_objects as $_key => $_value)
            {
             switch ($_value['type']) {
            case My_Notification::DEPARTMENT:
                $old_department_objects[] = $_value['object_id'];
                break;

            case My_Notification::STAFF:
                $old_staff_objects[] = $_value['object_id'];                    
                break;

            case My_Notification::AREA:
                $old_area_objects[] = $_value['object_id'];                    
                break;

            case My_Notification::TEAM:
                $old_team_objects[] = $_value['object_id'];                    
                break;

            case My_Notification::TITLE:
                $old_title_objects[] = $_value['object_id'];                    
                break;

            case My_Notification::COMPANY:
                $old_company_objects[] = $_value['object_id'];
                break;

            case My_Notification::ALL_STAFF:
                $all_staff = $_value['object_id'];                    
                break;

            case My_Notification::OFFICER:
                $officer = $_value['object_id'];                    
                break;

            default:
                throw new Exception("Invalid object type");
                break;
        }
            }
            $this->view->old_team_objects = $old_team_objects;
        
            $this->view->userStorage = Zend_Auth::getInstance()->getStorage()->read();
        }
        
        $QTeam = new Application_Model_Team();
        $QArea                 = new Application_Model_Area();
        $QInformCategory = new Application_Model_InformCategory();
        
        $this->view->team_cache            = $QTeam->get_recursive_cache();
        $this->view->area_cache            = $QArea->get_cache();
        $this->view->old_title_objects     = $old_title_objects;
        $this->view->old_area_objects      = $old_area_objects;
        $this->view->files = $files;
//         $this->view->team_cache = $QTeam->get_cache_team();
        
        $this->view->category_string_cache = $QInformCategory->get_cache();
        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;
        
        //back url
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
        
        $this->_helper->viewRenderer->setRender('inform/create');
