<?php
$page = $this->getRequest()->getParam('page', 1);
$title_search = $this->getRequest()->getParam('title');
$content = $this->getRequest()->getParam('content');
$category = $this->getRequest()->getParam('category');
$sort = $this->getRequest()->getParam('sort');
$desc = $this->getRequest()->getParam('desc', 1);
$status = $this->getRequest()->getParam('status',1);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QArea = new Application_Model_Area();
$id = $userStorage->id;
$QStaff = new Application_Model_Staff();
$QArea = new Application_Model_Area();
$staffRowset = $QStaff->find($id);
$staff = $staffRowset->current();


if(empty($userStorage->id)){
    $this->redirect(HOST . 'user/login');
}
//get area & province
$QRegionalMarket = new Application_Model_RegionalMarket();
$rowset = $QRegionalMarket->find($staff->regional_market);
$QInformCategory = new Application_Model_InformCategory();
$this->view->category_string_cache = $QInformCategory->get_cache();
if ($rowset)
{
    $regional_market = $rowset->current();
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);

    $QRegionalMarket->fetchAll($where);

    $rowset = $QArea->find($regional_market['area_id']);
    $area_id = $rowset->current()->id;
}

$limit = LIMITATION;
$total = 0;
$title = $userStorage->title;
$params = array(
    'category'=> $category,
    'content' => $content,
    'sort'    => $sort,
    'desc'    => $desc,
    'status'  => $status,
    'area_id'   => $area_id,
    'title'   => $title_search,


//     'staff_id'  => $id
);
$params['sort'] = $sort;
$params['desc'] = $desc;
$params['group_cat'] = 1;
$QTeam  = new Application_Model_Team();
$QStaff = new Application_Model_Staff();
$staff = $QStaff->find($id);
$staff = $staff->current();

$title = $staff['title'];
$team = $QTeam->find($title);
$team = $team->current();
// $group_id = $userStorage->group_id;

$group_id = $team['access_group'];

$db         = Zend_Registry::get('db');


    $this->_helper->viewRenderer->setRender('inform/inform-edit');
    unset($params['group_cat']);
    $QModel = new Application_Model_Inform();
    if (!in_array($group_id, array(ADMINISTRATOR_ID, HR_ID, HR_EXT_ID, BOARD_ID))) {
        $params['allow_inform'] = 1;
        $params['title_id_all'] = $title;
        $params['area_id_all'] = $area_id;
    }
    $inform = $QModel->fetchPagination($page, $limit, $total, $params);
    $QStaff = new Application_Model_Staff();
    $QInformCategory = new Application_Model_InformCategory();
    $inform_category = $QInformCategory->get_cache();
    $staff = $QStaff->get_cache();
    $informs = array();
    $this->view->inform_category = $inform_category;
    $this->view->informs = $inform;




$this->view->params = $params;
$this->view->sort = $sort;
$this->view->desc = $desc;
$this->view->staff = $staff;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'manage/inform/' . ($params ? '?' . http_build_query($params) .
        '&' : '?');
$this->view->offset = $limit * ($page - 1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;

$arr_status = array(
    -1 => 'ALL',
    1 => 'ENABLED',
    0 => 'DISABLED'
);

$this->view->arr_status = $arr_status;
