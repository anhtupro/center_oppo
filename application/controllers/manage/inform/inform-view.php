<?php

$id = $this->getRequest()->getParam('id');
$flashMessenger = $this->_helper->flashMessenger;
// $this->_helper->viewRenderer->setNoRender();
try
{
    if (!$id)
        throw new Exception("Invalid ID");
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
  
    $group_id = $userStorage->group_id;
    $QInform = new Application_Model_Inform();
    $inform = $QInform->find($id);
    $inform = $inform->current();

    $QInformFile = new Application_Model_InformFile();
    $where = $QInformFile->getAdapter()->quoteInto('inform_id = ?', $id);
    $files = $QInformFile->fetchAll($where);

    if (!$inform)
        throw new Exception("Invalid ID");
	
	$count_view = $inform->toArray()['count_view'];
	$data = array(
        'count_view' => $count_view+1
    );
    $where_update = $QInform->getAdapter()->quoteInto('id = ?', $id);
    $QInform->update($data, $where_update);
	
    $QInformTitle = new Application_Model_InformTitle();
    $check = $QInformTitle->check_view($id);

    if ($QInformTitle->check_view($id) || in_array($group_id, array(
        ADMINISTRATOR_ID,
        HR_ID,
        HR_EXT_ID,
        BOARD_ID)))
    {
        $this->view->files = $files;
        $this->view->userStorage = $userStorage;
        $this->view->inform = $inform;
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
        $this->_helper->viewRenderer->setRender('inform/view');
    } else
    {
        throw new Exception("You cannot view this");
    }
}
catch (exception $e)
{
        $flashMessenger->setNamespace('success')->addMessage("You cannot view this");
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;

    $this->_redirect(HOST . 'manage/inform');
}
