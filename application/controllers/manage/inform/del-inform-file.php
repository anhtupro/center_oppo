<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$id = $this->getRequest()->getParam('id');

try {
    // xoa file
    $QInformFile = new Application_Model_InformFile();
    $where = array();
    $where[] = $QInformFile->getAdapter()->quoteInto('id = ?', $id);
    $file = $QInformFile->fetchRow($where);


    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . 'inform'
            . DIRECTORY_SEPARATOR . $file['unique_folder'];

    $file_path = $uploaded_dir . DIRECTORY_SEPARATOR . $file['name'];
    unlink($file_path);
    
    //xoa du lieu
    $QInformFile->delete($where);
    echo json_encode(['status'=>1,'message'=>'Success']);
    exit();
} catch (Exception $exc) {
    echo json_encode(['status'=>0,'message'=>'Fail']);
    exit();
}
