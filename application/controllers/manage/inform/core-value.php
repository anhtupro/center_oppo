<?php
    $page = $this->getRequest()->getParam('page', 1);
    // $title = $this->getRequest()->getParam('title');
    $content = $this->getRequest()->getParam('content');
    $sort = $this->getRequest()->getParam('sort');
    $desc = $this->getRequest()->getParam('desc', 1);
    $status = $this->getRequest()->getParam('status', 1);
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $id = $userStorage->id;
    $QStaff = new Application_Model_Staff();
    $QArea = new Application_Model_Area();
    $staffRowset = $QStaff->find($id);
    $staff = $staffRowset->current();
    //get area & province
    $QRegionalMarket = new Application_Model_RegionalMarket();
    $rowset = $QRegionalMarket->find($staff->regional_market);

    if ($rowset)
    {
        $regional_market = $rowset->current();
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);
    
        $QRegionalMarket->fetchAll($where);
    
        $rowset = $QArea->find($regional_market['area_id']);
        $area_id = $rowset->current()->id;
    }
    
    $group_id = $userStorage->group_id;
    $title = $userStorage->title;
    $limit = LIMITATION;
    $total = 0;
    $category_id = 3;
    $params = array(
        'title' => $title,
        'content' => $content,
        'sort' => $sort,
        'desc' => $desc,
        'status' => $status,
        'category_id' => $category_id,
        'area_id'   => $area_id,
        'staff_id'  => $id
    );
    $params['sort'] = $sort;
    $params['desc'] = $desc;
    $params['group_cat'] = 1;
    
    $QModel = new Application_Model_Inform();
    $inform = $QModel->getListFaq($page, $limit, $total, $params);
 
    $QStaff = new Application_Model_Staff();
    $QInformCategory = new Application_Model_InformCategory();
    $inform_category = $QInformCategory->get_cache();
    $staff = $QStaff->get_cache();
    $informs = array();
    $this->view->inform_category = $inform_category;
    $this->view->informs = $inform;
    $this->view->params = $params;
    $this->view->sort = $sort;
    $this->view->desc = $desc;
    $this->view->staff = $staff;
    
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->_helper->viewRenderer->setRender('inform/list-policy');
    $this->view->offset = $limit * ($page - 1);
    
    $flashMessenger = $this->_helper->flashMessenger;
    $messages = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
    
    $arr_status = array(
        - 1 => 'ALL',
        1 => 'ENABLED',
        0 => 'DISABLED'
    );
    
    $this->view->arr_status = $arr_status;
