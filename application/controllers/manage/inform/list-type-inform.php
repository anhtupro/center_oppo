<?php
$parent=$this->getRequest()->getParam('parent');
if($parent==1)
{
    $typeInform="Thông Tin OPPO Việt Nam";
}
elseif ($parent==2){
    $typeInform="Chính Sách & Hướng Dẫn";

}
elseif ($parent==4){
    $typeInform="'OPPO Là Nhà'";
}
$QCategory=new Application_Model_InformCategory();
$where= $QCategory->getAdapter()->quoteInto("parent=?",$parent);
$listCategory=$QCategory->fetchAll($where);

$this->view->typeInform=$typeInform;
$this->view->listCategory=$listCategory;
$this->_helper->viewRenderer->setRender('inform/list-type-inform');