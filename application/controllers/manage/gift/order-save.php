<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$id                     = $this->getRequest()->getParam('id');
$mid                    = $this->getRequest()->getParam('mid');
$area_id                = $this->getRequest()->getParam('area_id');
$num_gift               = $this->getRequest()->getParam('num_gift');
$list_gift              = $this->getRequest()->getParam('list_gift');
$rejected               = $this->getRequest()->getParam('rejected');

$QGift                  = new Application_Model_Gift();
$QGiftOrder             = new Application_Model_GiftOrder();
$QGiftOrderFlow         = new Application_Model_GiftOrderFlow();
$QGiftMainDetails       = new Application_Model_GiftMainDetails();
$QGiftOrderDetails      = new Application_Model_GiftOrderDetails();

$db = Zend_Registry::get('db');

try {
    $db->beginTransaction();
    // Check xem đã chọn khu vực hay chưa?
    if(empty($area_id)){
        echo json_encode([
            'status'    => 0,
            'message'   => "Bạn chưa chọn khu vực quản lý",
        ]);
        return;
    }

    $arr_gift = json_decode($list_gift,true);
    $arr_num  = json_decode($num_gift,true);

    // Check xem đã có đủ thông tin order quà tặng hay chưa
    if(empty($arr_gift) || empty($arr_num)){
        echo json_encode([
            'status'    => 0,
            'message'   => "Bạn chưa chọn quà tặng hoặc nhập số lượng quà tặng",
        ]);
        return;
    }

    $data = array(
        'main_id'       => $mid,
        'area_id'       => $area_id,
        'created_by'    => $userStorage->id,
        'created_at'    => date('Y-m-d H:i:s'),
    );

    $arr_temp = array();
    foreach ($arr_gift as $k1 => $v1):
        $arr_temp[$v1] = $arr_num[$k1];
    endforeach;

    if($id){

        // nếu đã bị reject thì không cần trả tồn nữa, vì reject đã tự động trả tồn
        if(!$rejected):
            // Lấy lượng tồn hiện tại
            $data_inventory1 = $QGiftMainDetails->getInventory($mid);
            $arr_inventory1  = array();

            foreach ($data_inventory1 as $lan1):
                $arr_inventory1[$lan1['id']] = $lan1['inventory'];
            endforeach;
            // End

            // lấy số lượng đã đặt hiện tại
            $whereOrderOld2     = $QGiftOrderDetails->getAdapter()->quoteInto('order_id = ?', $id);
            $order_old          = $QGiftOrderDetails->fetchAll($whereOrderOld2)->toArray();

            // trả ngược về kho tồn
            foreach ($order_old as $k => $v):
                $whereTraTon    = $QGift->getAdapter()->quoteInto('id = ?', $v['gift_id']);
                $QGift->update(
                    array(
                        'inventory' => $arr_inventory1[$v['gift_id']] + $v['num']
                    ), $whereTraTon
                );
            endforeach;
        endif;
        // Lấy lượng tồn hiện tại sau khi trả tồn cũ
        $data_inventory2    = $QGiftMainDetails->getInventory($mid);
        $arr_inventory2     = array();
        foreach ($data_inventory2 as $lan2):
            $arr_inventory2[$lan2['id']] = $lan2['inventory'];
        endforeach;
        // End

        foreach ($arr_temp as $k => $v):
            if($v > $arr_inventory2[$k]):
                echo json_encode([
                    'status'    => 0,
                    'message'   => "Trong danh sách chứa quà tặng đã hết hàng trong kho, vui lòng kiểm tra lại",
                ]);
                return;
            endif;
        endforeach;

        // xóa hết order cũ
        $whereOrderOld3     = $QGiftOrderDetails->getAdapter()->quoteInto('order_id = ?', $id);
        $QGiftOrderDetails->delete($whereOrderOld3);

        // Update lại thông tin đơn đặt hàng
        $whereOrder = $QGiftOrder->getAdapter()->quoteInto('id = ?', $id);
        $QGiftOrder->update($data, $whereOrder);

        // Xóa hết luồng duyệt hiện tại
        $whereFlowOld = $QGiftOrderFlow->getAdapter()->quoteInto('order_id = ?', $id);
        $QGiftOrderFlow->delete($whereFlowOld);

        // Update lại lượng tồn mới
        foreach ($arr_temp as $key => $value):
            $QGiftOrderDetails->insert(
                array(
                    'order_id'  => $id,
                    'gift_id'   => $key,
                    'num'       => $value
                )
            );

            $whereGift    = $QGift->getAdapter()->quoteInto('id = ?', $key);
            // update lại tồn
            $QGift->update(
                array(
                    'inventory' => $arr_inventory2[$key] - $value
                ), $whereGift
            );
        endforeach;

    }else{

        // Check xem đã có đơn đặt quà tặng cho khu vực này chưa? nếu có thì báo lỗi
        $whereOrderOld      = array();
        $whereOrderOld[]    = $QGiftOrder->getAdapter()->quoteInto('main_id = ?', $mid);
        $whereOrderOld[]    = $QGiftOrder->getAdapter()->quoteInto('area_id = ?', $area_id);
        $data_old           = $QGiftOrder->fetchAll($whereOrderOld)->toArray();

        if(!empty($data_old)){
            echo json_encode([
                'status'    => 0,
                'message'   => "Đã có đơn đặt quà tặng cho khu vực này! Vui lòng kiểm tra lại",
            ]);
            return;
        }
        //end check

        // Begin check xem lượng quà đặt có nhỏ hơn lượng tồn trong kho không
        $data_inventory = $QGiftMainDetails->getInventory($mid);
        $arr_inventory  = array();

        foreach ($data_inventory as $k2 => $v2):
            $arr_inventory[$v2['id']] = $v2['inventory'];
        endforeach;

        foreach ($arr_temp as $k => $v):
            if($v > $arr_inventory[$k]):
                echo json_encode([
                    'status'    => 0,
                    'message'   => "Trong danh sách chứa quà tặng đã hết hàng trong kho, vui lòng kiểm tra lại",
                ]);
                return;
            endif;
        endforeach;
        // End check

        $id = $QGiftOrder->insert($data);
        foreach ($arr_temp as $key => $value):
            $QGiftOrderDetails->insert(
                array(
                    'order_id'  => $id,
                    'gift_id'   => $key,
                    'num'       => $value
                )
            );

            $whereGift    = $QGift->getAdapter()->quoteInto('id = ?', $key);
            // update lại tồn
            $QGift->update(
                array(
                    'inventory' => $arr_inventory[$key] - $value
                ), $whereGift
            );
        endforeach;
    }

    $QGiftOrderFlow->insert(array(
        'order_id'      => $id,
        'status'        => 'Đang chờ ASM duyệt',
        'step'          => 0,
        'created_by'    => $userStorage->id,
        'created_at'    => date('Y-m-d H:i:s'),
    ));

    $db->commit();
    echo json_encode([
        'status' => 1,
    ]);
    return;
} catch (Exception $e) {

    $db->rollback();
    echo json_encode([
        'status' => 0,
        'message' => $e->getMessage(),
    ]);
    return;
}