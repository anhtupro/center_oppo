<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$page        = $this->getRequest()->getParam("page", 1);
$sort        = $this->getRequest()->getParam("sort", "");
$desc        = $this->getRequest()->getParam("desc", 1);
$QGiftMain   = new Application_Model_GiftMain();
$QTeam       = new Application_Model_Team();

$info_team      = $QTeam->fetchRow(['id = ?' => $userStorage->title]);
if($info_team->access_group ==  My_Staff_Group::SALES_ADMIN){
    $params['sales_admin']      = 1;
    $this->view->is_sales_admin = 1;
}

$limit          = LIMITATION;
$total          = 0;
$params         = [];
$params["sort"] = $sort;
$params["desc"] = $desc;
$result         = $QGiftMain->fetchPagination($page, $limit, $total, $params);
$this->view->list   = $result;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->params = $params;
$this->view->desc   = $desc;
$this->view->sort   = $sort;
$this->view->url    = HOST . "/manage/gift-list" . ( $params ? "?" . http_build_query($params) . "&" : "?" );
$this->view->offset = $limit * ($page - 1);
$this->_helper->viewRenderer->setRender('gift/list');