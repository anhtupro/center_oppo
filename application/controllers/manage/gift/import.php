<?php
$flashMessenger = $this->_helper->flashMessenger;
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$page           = $this->getRequest()->getParam("page", 1);
$sort           = $this->getRequest()->getParam("sort", "");
$desc           = $this->getRequest()->getParam("desc", 1);
$QGift          = new Application_Model_Gift();

$limit = LIMITATION;
$total = 0;
$params         = [];
$params["sort"] = $sort;
$params["desc"] = $desc;
$result             = $QGift->fetchPagination($page, $limit, $total, $params);
$this->view->list   = $result;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->params = $params;
$this->view->desc   = $desc;
$this->view->sort   = $sort;
$this->view->url    = HOST . "/manage/gift-import" . ( $params ? "?" . http_build_query($params) . "&" : "?" );
$this->view->offset = $limit * ($page - 1);

if($this->getRequest()->isPost()) {
    $db = Zend_Registry::get('db');
    $db->beginTransaction();

    try {

        $id             = $this->getRequest()->getParam('id');
        $name           = $this->getRequest()->getParam('name');
        $inventory      = $this->getRequest()->getParam('inventory');
        $link           = $this->getRequest()->getParam('link');
        $desc           = $this->getRequest()->getParam('desc');
        $data   = array(
            'name'          => $name,
            'desc'          => $desc,
            'inventory'     => $inventory, // lượng tồn hàng
            'storage'       => $inventory, // Tổng tồn hàng
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $userStorage->id,
        );

        if($id){
            $whereGift = $QGift->getAdapter()->quoteInto('id = ?', $id);
            $QGift->update($data, $whereGift);
        }else{
            $id = $QGift->insert($data);
        }

        $filename = $_FILES['file']['name'];
        //BEGIN Upload file
        if(!empty($filename) ){
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                DIRECTORY_SEPARATOR . 'gift' . DIRECTORY_SEPARATOR . $id;

            if(!is_dir($uploaded_dir)){
                @mkdir($uploaded_dir, 0777, true);
            }
            $ext_arr = array('jpg','png','JPG','PNG','jpeg','JPEG');
            $ext1 = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext1, $ext_arr)){
                $flashMessenger->setNamespace('error')->addMessage("File không đúng định dạng");
                $this->redirect(HOST . '/manage/gift-import');
            }
            $name_pic = "Pic_" . $id . "_" . time() . "." . $ext1;
            $file = $uploaded_dir . DIRECTORY_SEPARATOR . $name_pic;
            @move_uploaded_file($_FILES['file']['tmp_name'], $file);
            $where = NULL;
            $where = $QGift->getAdapter()->quoteInto("id = ?", $id);
            $QGift->update(['file' => $name_pic], $where);
        }
        //END Upload file

        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Done');
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
    $this->redirect($link);
}


$this->_helper->viewRenderer->setRender('gift/import');

