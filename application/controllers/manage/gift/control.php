<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$id                     = $this->getRequest()->getParam('id');
$note                   = $this->getRequest()->getParam('note', NULL);
$act                    = $this->getRequest()->getParam('act');
$inventory              = $this->getRequest()->getParam('inventory');
$step                   = $this->getRequest()->getParam('step');

$QGiftOrder             = new Application_Model_GiftOrder();
$QGiftMain              = new Application_Model_GiftMain();
$QGiftOrderFlow         = new Application_Model_GiftOrderFlow();
$QGiftOrderDetails      = new Application_Model_GiftOrderDetails();
$QGiftMainDetails       = new Application_Model_GiftMainDetails();
$QGift                  = new Application_Model_Gift();
$db = Zend_Registry::get('db');

try {
    $db->beginTransaction();

    $whereFlow   = array();
    $whereFlow[] = $QGiftOrderFlow->getAdapter()->quoteInto('order_id = ?', $id);
    $whereFlow[] = $QGiftOrderFlow->getAdapter()->quoteInto('has_approved = ?', 0);
    $QGiftOrderFlow->update(array('has_approved' => 1, 'approved_by' => $userStorage->id), $whereFlow);

    if($act == 1){

        if($step == 1){
            $status = "Đang chờ Admin tổng xác nhận";
        }elseif ($step == 2){
            $status = "Đang chuyển hàng xuống khu vực";
        }elseif ($step == 3){
            $status = "Hoàn thành";
        }

        $data_insert = array(
            'order_id'      => $id,
            'status'        => $status,
            'created_by'    => $userStorage->id,
            'created_at'    => date('Y-m-d H:i:s'),
            'step'          => $step,
        );
        $QGiftOrderFlow->insert($data_insert);

    }elseif ($act == 2){

        if($step == 1){
            $status = "Đã bị ASM từ chối";
        }elseif ($step == 2){
            $status = "Đã bị Admin tổng từ chối";
        }

        $data_insert = array(
            'order_id'      => $id,
            'status'        => $status,
            'note'          => $note,
            'has_rejected'  => 1,
            'created_by'    => $userStorage->id,
            'created_at'    => date('Y-m-d H:i:s'),
            'step'          => $step,
        );
        $QGiftOrderFlow->insert($data_insert);

        // Bị Reject thì phải trả tồn lại
        $whereOrderDetails  = $QGiftOrderDetails->getAdapter()->quoteInto('order_id = ?', $id);
        $data_order_de      = $QGiftOrderDetails->fetchAll($whereOrderDetails)->toArray();

        $whereOrder         = $QGiftOrder->getAdapter()->quoteInto('id = ?', $id);
        $data_order         = $QGiftOrder->fetchRow($whereOrder)->toArray();

        $data_inventory     = $QGiftMainDetails->getInventory($data_order['main_id']);

        $arr_inventory      = array();
        foreach ($data_inventory as $k2 => $v2):
            $arr_inventory[$v2['id']] = $v2['inventory'];
        endforeach;

        foreach ($data_order_de as $k => $v):
            $where    = $QGift->getAdapter()->quoteInto('id = ?', $v['gift_id']);

            $QGift->update(
                array(
                    'inventory' => $arr_inventory[$v['gift_id']] + $v['num']
                ), $where
            );
        endforeach;
        // end

    }elseif ($act == 3){
        $whereMain = $QGiftMain->getAdapter()->quoteInto('id = ?', $id);
        $QGiftMain->update(array('del' => 1), $whereMain);
    }elseif ($act == 4){
        $whereGift  = $QGift->getAdapter()->quoteInto('id = ?', $id);
        $_arr       = $QGift->fetchRow($whereGift)->toArray();

        $QGift->update(
            array(
                'storage'   => $_arr['storage'] + $inventory,
                'inventory' => $_arr['inventory'] + $inventory
            ), $whereGift
        );
    }else{
        echo json_encode([
            'status' => 0,
            'message' => "Không tìm thấy tác vụ thích hợp",
        ]);
        return;
    }

    $db->commit();
    echo json_encode([
        'status' => 1,
    ]);
    return;
} catch (Exception $e) {

    $db->rollback();
    echo json_encode([
        'status' => 0,
        'message' => $e->getMessage(),
    ]);
    return;
}