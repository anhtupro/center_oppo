<?php
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QGiftMain          = new Application_Model_GiftMain();
$QGiftOrder         = new Application_Model_GiftOrder();
$QGiftOrderDetails  = new Application_Model_GiftOrderDetails();
$QGiftMainDetails   = new Application_Model_GiftMainDetails();
$QTeam              = new Application_Model_Team();
$QArea              = new Application_Model_Area();
$QAsm               = new Application_Model_Asm();

$id                 = $this->getRequest()->getParam("id");
$cat                = $this->getRequest()->getParam("cat");
$rejected           = $this->getRequest()->getParam("rejected");

if(in_array($userStorage->group, array(SALES_EXT_ID)) || in_array($userStorage->title, [SALES_ASSISTANT_TITLE])){
    $this->redirect(HOST . "manage/gift-list");
}

$info_team          = $QTeam->fetchRow(['id = ?' => $userStorage->title]);
$area_cache         = $QArea->get_cache();

if($info_team->access_group ==  My_Staff_Group::ASM){
    $this->redirect(HOST . "manage/gift-order");
}elseif($info_team->access_group == ADMINISTRATOR_ID){
    $area = $area_cache;
}elseif ($info_team->access_group ==  My_Staff_Group::SALES_ADMIN){
    $area_list      = $QAsm->get_cache($userStorage->id)['area'];
    foreach ($area_list as $key => $value) {
        $area[$value] = $area_cache[$value];
    }
}
$this->view->area   = $area;
if(!$cat){
    $this->redirect(HOST . "manage/gift-list");
}

$whereMain          = array();
$whereMain[]        = $QGiftMain->getAdapter()->quoteInto('from_date < ?', date('Y-m-d H:i:s'));
$whereMain[]        = $QGiftMain->getAdapter()->quoteInto('to_date   > ?', date('Y-m-d H:i:s'));
$whereMain[]        = $QGiftMain->getAdapter()->quoteInto('id = ?', $cat);
$info               = $QGiftMain->fetchAll($whereMain)->toArray();

if(!$info){
    echo 'Đợt đặt hàng quà tặng đã kết thúc hoặc chưa được mở duyệt, vui lòng liên hệ Sales Admin Tổng để biết chi tiết!';exit;
}

$this->view->info       = $info[0];
$this->view->staff_id   = $userStorage->id;
$this->view->gifts      = $QGiftMainDetails->getInventory($info[0]['id']);

if($id) {
    $list_order             = $QGiftOrderDetails->fetchAllData($id);
    $order = array();
    foreach ($list_order as $ko => $vo):
        $order[$vo['id']] = $vo['num'];
    endforeach;

    $whereOrder         = $QGiftOrder->getAdapter()->quoteInto('id = ?', $id);
    $this->view->data   = $QGiftOrder->fetchRow($whereOrder);
    $this->view->order  = $order;
    $this->view->id     = $id;
    $this->view->rejected = $rejected ? 1 : 0;
}

$this->_helper->viewRenderer->setRender('gift/index');