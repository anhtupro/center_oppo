<?php
$id                 = $this->getRequest()->getParam("id");
$act                = $this->getRequest()->getParam("act", 1);

$QGiftOrder         = new Application_Model_GiftOrder();
$GiftMainDetails    = new Application_Model_GiftMainDetails();
$QGiftOrderDetails  = new Application_Model_GiftOrderDetails();
$QTeam              = new Application_Model_Team();

if($act == 1){

    $info       = $QGiftOrder->getInfo($id);
    $list_order = $QGiftOrderDetails->fetchAllData($id);
    $this->view->info                           = $info;
    $this->view->list_order                     = $list_order;
    $this->_helper->viewRenderer->setRender('gift/modal');

}elseif ($act == 2){

    $inventory = $GiftMainDetails->getInventory($id);
    $this->view->list_order                     = $inventory;
    $this->_helper->viewRenderer->setRender('gift/modal-inventory');

}

$this->view->act        = $act;
$this->_helper->layout->disableLayout();