<?php
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
// param `cat` được gửi từ Admin tổng sang để xem danh sách trong đợt đó
$cat            = $this->getRequest()->getParam("cat");
$staff_id       = $this->getRequest()->getParam("staff_id");
$view_all       = $this->getRequest()->getParam("view_all");
$page           = $this->getRequest()->getParam("page", 1);
$sort           = $this->getRequest()->getParam("sort", "");
$desc           = $this->getRequest()->getParam("desc", 1);
$QGiftOrderFlow     = new Application_Model_GiftOrderFlow();
$QGiftOrder         = new Application_Model_GiftOrder();
$QTeam              = new Application_Model_Team();
$QAsm               = new Application_Model_Asm();

$info_team          = $QTeam->fetchRow(['id = ?' => $userStorage->title]);
$params             = [
    'cat'       => $cat,
    'staff_id'  => $staff_id,
    'view_all'  => $view_all
];

if($info_team->access_group == My_Staff_Group::ASM){
    $params['asm']      = 1;
    $params['area_id']  = $QAsm->get_cache($userStorage->id)['area'];
}elseif ($info_team->access_group ==  My_Staff_Group::SALES_ADMIN){
    $params['area_id']  = $QAsm->get_cache($userStorage->id)['area'];
    $this->view->is_local_admin = 1;
}

$limit              = LIMITATION;
$total              = 0;
$params["sort"]     = $sort;
$params["desc"]     = $desc;
$result             = $QGiftOrder->fetchPagination($page, $limit, $total, $params);
$more_info          = $QGiftOrderFlow->fetchAllData();

$this->view->recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->more_info  = $more_info;
$this->view->list       = $result;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->params     = $params;
$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->url        = HOST . "/manage/gift-order" . ( $params ? "?" . http_build_query($params) . "&" : "?" );
$this->view->offset     = $limit * ($page - 1);
$this->view->title      = $userStorage->title;

$this->_helper->viewRenderer->setRender('gift/order');