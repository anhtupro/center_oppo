<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();

$id                     = $this->getRequest()->getParam('id');
$name                   = $this->getRequest()->getParam('name');
$desc                   = $this->getRequest()->getParam('desc');
$from_date              = $this->getRequest()->getParam('from_date');
$to_date                = $this->getRequest()->getParam('to_date');
$list_gift              = $this->getRequest()->getParam('list_gift');
$stop_action            = $this->getRequest()->getParam('stop_action');

$QGiftMain              = new Application_Model_GiftMain();
$QGiftOrder             = new Application_Model_GiftOrder();
$QGiftMainDetails       = new Application_Model_GiftMainDetails();

$db = Zend_Registry::get('db');

try {
    $db->beginTransaction();

    $data = array(
        'name'          => $name,
        'desc'          => $desc,
        'from_date'     => $from_date,
        'to_date'       => $to_date,
        'created_by'    => $userStorage->id,
        'created_at'    => date('Y-m-d H:i:s'),
    );
    if($id){

        $whereMain = $QGiftMain->getAdapter()->quoteInto('id = ?', $id);
        $QGiftMain->update($data, $whereMain);

        if(!$stop_action){
            $whereMainDetails = $QGiftMainDetails->getAdapter()->quoteInto('main_id = ?', $id);
            $QGiftMainDetails->delete($whereMainDetails);
        }

    }else{
        $id = $QGiftMain->insert($data);
    }


    if(!$stop_action){
        foreach (json_decode($list_gift,true) as $key => $value):
            $QGiftMainDetails->insert(array(
                'main_id'       => $id,
                'gift_id'       => $value,
            ));
        endforeach;
    }


    $db->commit();
    echo json_encode([
        'status' => 1,
    ]);
    return;
} catch (Exception $e) {

    $db->rollback();
    echo json_encode([
        'status' => 0,
        'message' => $e->getMessage(),
    ]);
    return;
}

