<?php
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$id                     = $this->getRequest()->getParam('id');

$QGift                  = new Application_Model_Gift();
$QGiftMain              = new Application_Model_GiftMain();
$QGiftOrder             = new Application_Model_GiftOrder();
$QGiftMainDetails       = new Application_Model_GiftMainDetails();

if($id){
    $whereMain = $QGiftMain->getAdapter()->quoteInto('id = ?', $id);
    $info      = $QGiftMain->fetchRow($whereMain);

    if(empty($info)){
        echo 'Không tìm thấy dữ liệu, vui lòng quay trở lại';exit;
    }

    $whereMainDetails   = $QGiftMainDetails->getAdapter()->quoteInto('main_id = ?', $id);
    $gifts              = $QGiftMainDetails->fetchAll($whereMainDetails)->toArray();

    $whereOrder         =  $QGiftOrder->getAdapter()->quoteInto('main_id = ?', $id);
    $exist_order        = $QGiftOrder->fetchAll($whereOrder)->toArray();
    $this->view->stop_action = $exist_order ? 1 : 0;

    $inventory = $QGiftMainDetails->getInventory($id);
    $list_gift = array();
    foreach ($inventory as $k => $v):
        $list_gift[] = $v['id'];
    endforeach;

    $this->view->inventory  = $inventory;
    $this->view->list_gift  = $list_gift;
    $this->view->data       = $info->toArray();
}
$all_gift               = $QGift->fetchAll();
$this->view->all_gift   = $all_gift;
$this->_helper->viewRenderer->setRender('gift/create');