<?php
$flashMessenger = $this->_helper->flashMessenger;
try {
    $id = $this->getRequest()->getParam('id');

    if (!$id) {
        throw new Exception("Invalid ID");
    }

    $QStaff = new Application_Model_Staff();
    $staff = $QStaff->find($id);
    $staff = $staff->current();

    // echo "<pre>" ;print_r($staff);die;
    if (!$staff) {
        throw new Exception("Invalid ID");
    }

    if($staff['status'] == 0 && $staff['off_date'] != null){
        echo '<script>
                parent.palert("Trong danh sách ,'.$staff['firstname'] .' '.$staff['lastname'] .' đã không còn làm việc tại Oppo , Bạn không được add shop cho nhân viên đã nghĩ việc.");
                parent.alert("Trong danh sách , '.$staff['firstname'] .' '.$staff['lastname'] .' đã không còn làm việc tại Oppo , Bạn không được add shop cho nhân viên đã nghĩ việc.");
            </script>';
        exit;
    }


    $is_leader = 10;
    if (($staff['title'] == PGPB_TITLE || $staff['title'] == PG_BRANDSHOP || $staff['title'] == SENIOR_PROMOTER_BRANDSHOP || $staff['title'] == CHUYEN_VIEN_BAN_HANG_TITLE ) && $staff['group_id'] == PGPB_ID)
        $is_leader = 0;
    else if ($staff['title'] == SALES_TITLE && $staff['group_id'] == SALES_ID)
        $is_leader = 1;
    else if ($staff['title'] == PG_LEADER_TITLE && $staff['group_id'] == PG_LEADER_ID)
        $is_leader = 5;
    else if ($staff['title'] == STORE_LEADER && $staff['group_id'] == STORE_LEADER_ID)
        $is_leader = 3;
    else if ($staff['title'] == LEADER_TITLE && $staff['group_id'] == LEADER_ID)
        $is_leader = 1;

    // echo $is_leader;die;

    // $is_leader = $staff['group_id'] == SALES_ID ? 1 : ($staff['group_id'] == PGPB_ID ? 0 : 5);

    if ($is_leader == 10){
        echo '<script>
                parent.palert("Title hoặc Group bạn đang được gán sai , liên hệ Tech team nếu có thắc mắc .");
                parent.alert("Title hoặc Group bạn đang được gán sai , liên hệ Tech team nếu có thắc mắc .");
            </script>';
        exit;
    }

    $pic = $this->getRequest()->getParam('store_ids');



    if (!$pic && ($pic && strlen($pic) == 0)) {
        // gỡ hết store của leader
    } else {
        $store_ids = explode(',', $pic);

        $store_ids = is_array($store_ids) ? array_filter($store_ids) : array();
        $store_ids = array_unique($store_ids);
         // echo "<pre>" ;print_r($store_ids);die;
        $joined_dates = $this->getRequest()->getParam('joined_dates', '{}');
        $joined_dates = json_decode($joined_dates, true);


        $QStaff         = new Application_Model_Staff();
        $QStore          = new Application_Model_Store();
        $QStoreLeader    = new Application_Model_StoreStaff();
        $QStoreLeaderLog = new Application_Model_StoreStaffLog();
        $QRegion         = new Application_Model_RegionalMarket();
        $QCasual         = new Application_Model_CasualWorker();
        $QLog            = new Application_Model_Log();
        $QDistributor = new Application_Model_Distributor();

        $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();

        // log mới Tuấn
        $QLogStore            = new Application_Model_LogStore();

        $region_cache    = $QRegion->get_cache_all();

        // lấy store cũ thuộc leader này
        $where = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null and staff_id = ?', $id);
        $stores = $QStoreLeaderLog->fetchAll($where);

        // echo "pre>";print_r($stores);die;

        


        $old_ids = array();

        foreach ($stores as $key => $store) {
            $old_ids[] = intval($store['store_id']);
        }

        // định dạng store id hiện tại
        foreach ($store_ids as $key => $s_id) {
            $store_ids[$key] = intval( trim( $s_id ) );
        }



        $del_ids = array_diff($old_ids, $store_ids);
        $del_ids = is_array($del_ids) ? array_filter($del_ids) : array();

        $new_ids = array_diff($store_ids, $old_ids);
        $new_ids = is_array($new_ids) ? array_filter($new_ids) : array();

        // echo "<pre>";print_r($new_ids);die;

        // echo $id;die;
        $time = time();


        

        // cập nhật d_id cho các store mới thêm
        foreach ($new_ids as $k => $s_id) {

            // lấy distributor để kiểm tra có thuộc TGDĐ_Ka hoặc VTA_Ka 
            $where_1 = $QStore->getAdapter()->quoteInto('id = ?',$s_id);
            $rs = $QStore->fetchRow($where_1);
            // echo "<pre>";print_r($rs['is_brand_shop']);die;
            
            $staff = $QStaff->find($id);
            $staff = $staff->current();


            $store = $QStore->find($s_id);
            $store = $store->current();

            
            if (!$staff) {
                echo '<script>
                        parent.palert("Trong danh sách , người thứ ['.($k+1).'] hiện không có trên hệ thống.");
                        parent.alert("Trong danh sách , người thứ ['.($k+1).'] hiện không có trên hệ thống.");
                    </script>';
                exit;
            }

            
            // if ($staff['regional_market'] != $store['regional_market']) {

            //         echo '<script>
            //                 parent.palert("Người thứ ['.($k+1).'] '
            //                     .$staff['firstname'] .' '.$staff['lastname'] .' | '
            //                     . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' có hồ sơ ở tỉnh khác với tỉnh của Store [--'.$store['name'].'--]. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

            //                 parent.alert("Người thứ ['.($k+1).'] '
            //                     .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' có hồ sơ ở tỉnh khác với tỉnh của Store [--'.$store['name'].'--]. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
            //             </script>';
            //         exit;
            // }    

            if ($rs['is_brand_shop'] != 1 && ($staff['group_id'] == STORE_LEADER_ID || $staff['title'] == STORE_LEADER)) {
                echo '<script>
                        parent.palert("Người thứ ['.($k+1).'] '
                            .$staff['firstname'] .' '.$staff['lastname'] .' | '
                            . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' không thuộc nhóm Store Leader.");

                        parent.alert("Người thứ ['.($k+1).'] '
                            .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' không thuộc nhóm Store Leader.");
                    </script>';
                exit;
            } 

            


            $where_2 = $QDistributor->getAdapter()->quoteInto('id = ?', $rs['d_id']);
            $distributor_check = $QDistributor->fetchRow($where_2);

            // check TGDĐ này đã có PG chưa nếu có rồi đá đít thằng gán ra , có rồi ai cho add mà add cha.

            if ($is_leader == 0 && ((isset($distributor_check['parent']) && $distributor_check['parent'] && $distributor_check['parent'] == 2316)
                || (isset($distributor_check['id']) && $distributor_check['id'] && $distributor_check['id'] == 2316)
                || (isset($distributor_check['title']) && preg_match('/^TGDĐ -/', $distributor_check['title']))
            )) {
                $where   = array();
                
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('store_id = ?', $s_id);
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('is_leader = ?',0);
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null');
                $pg_check = $QStoreLeaderLog->fetchRow($where);

                
                if ($pg_check) {
                    $store_check = $QStore->find($s_id);
                    $store_check = $store_check->current();
                    // echo "<pre>";print_r($store_check);die;
                    echo '<script>
                            parent.palert("Mỗi store thuộc chuỗi TGDĐ chỉ có tối đa 1 PG. <br />(<a href=\"'.HOST.'manage/sales-pg-view?id='.$pg_check['staff_id'].'\" target=\"_blank\">'
                                .$store_check['name'] .'</a>) đã có PG phụ trách. Vui lòng kiểm tra lại.");

                            parent.alert("Mỗi store thuộc chuỗi TGDĐ chỉ có tối đa 1 PG - ('
                                .$store_check['name'] .') đã có PG phụ trách. Vui lòng kiểm tra lại.");
                        </script>';
                    exit;
                }
            }

            // check VTA này đã có PG chưa nếu có rồi đá đít thằng gán ra , có rồi ai cho add mà add cha.

            if ($is_leader == 0 && ((isset($distributor_check['parent']) && $distributor_check['parent'] && $distributor_check['parent'] == 2317)
                || (isset($distributor_check['id']) && $distributor_check['id'] && $distributor_check['id'] == 2317)
                || (isset($distributor_check['title']) && preg_match('/^VTA -/', $distributor_check['title']))
            )) {
                $where   = array();
                
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('store_id = ?', $s_id);
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('is_leader = ?',0);
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null');
                $pg_check = $QStoreLeaderLog->fetchAll($where);

                
                
                if (count($pg_check) > 2 ) {
                    $store_check = $QStore->find($s_id);
                    $store_check = $store_check->current();
                    
                    echo '<script>
                            parent.palert("Mỗi store thuộc chuỗi VTA chỉ có tối đa 3 PGs. <br />(<a href=\"'.HOST.'manage/store-edit?id='.$s_id.'\" target=\"_blank\">'
                                .$store_check['name'] .'</a>) đã có 3 PGs phụ trách. Vui lòng kiểm tra lại.");

                            parent.alert("Mỗi store thuộc chuỗi VTA chỉ có tối đa 3 PGs - ('
                                .$store_check['name'] .') đã có 3 PGs phụ trách. Vui lòng kiểm tra lại.");
                        </script>';
                    exit;
                }
            }

            // check shop thuộc brand shop
            if ( $is_leader == 0 && $store['is_brand_shop'] == 1 ) {
                if ( $staff['title'] == PG_BRANDSHOP || $staff['title'] == SENIOR_PROMOTER_BRANDSHOP ){ 
                } else {
                    echo '<script>
                        parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
                                .$staff['firstname'] .' '.$staff['lastname'] .' | '
                                . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' hiện không thuộc PGs Brandshop.");
                        parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
                                .$staff['firstname'] .' '.$staff['lastname'] .' | '
                                . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' hiện không thuộc PGs Brandshop.");
                    </script>';
                    exit;
                }
            }


            
            // Kiểm tra cửa hàng này có sales nào đứng chưa, trường hợp gán cho sales
            if ($is_leader == 1) {
                $where = array();
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('store_id = ?', $s_id);
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('is_leader = ?', 1);
                $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null');
                $staff_store_check = $QStoreLeaderLog->fetchRow($where);

                if ($staff_store_check) {
                    $store_check = $QStore->find($s_id);
                    $store_check = $store_check->current();

                    echo '<script>
                            parent.palert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] (<a href=\"'.HOST.'manage/sales-pg-view?id='.$staff_store_check['staff_id'].'\" target=\"_blank\">'
                                .$store_check['name'] .'</a>) đã có Sales phụ trách. Vui lòng kiểm tra lại.");

                            parent.alert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] ('
                                .$store_check['name'] .') đã có Sales phụ trách. Vui lòng kiểm tra lại.");
                        </script>';
                    exit;
                }
            }



            

            if($is_leader == 1 && $staff['title'] == LEADER_TITLE  ){
                $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
                $where = array();
                $where = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null and store_id = ?', $s_id);
                $dataLeader = $QStoreLeaderLog->fetchRow($where);

                $staffLeader = $QStaff->find($dataLeader['staff_id']);
                $staffLeader = $staffLeader->current();

                if ($staff['id'] != $dataLeader['staff_id']){
                    echo '<script>
                            parent.palert("Shop này đã được gán Sales Leader '.$staffLeader['firstname'] .' '.$staffLeader['lastname'] .' - Code : '.$staffLeader['code'].' đứng gián tiếp , không được gán Sales Leader khác đứng trực tiếp.");
                            parent.alert("Shop này đã được gán Sales Leader '.$staffLeader['firstname'] .' '.$staffLeader['lastname'] .' - Code : '.$staffLeader['code'].' đứng gián tiếp , không được gán Sales Leader khác đứng trực tiếp.");
                        </script>';
                    exit;
                }

            }

            // $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
            // $where           = array();
            // $where           = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null and store_id = ?', $s_id);
            // $data            = $QStoreLeaderLog->fetchRow($where);

            // $staffLeader = $QStaff->find($data['staff_id']);
            // $staffLeader = $staffLeader->current();

            // if ($is_leader == 1 && $staff['title'] == LEADER_TITLE && $data) {
            //     echo '<script>
            //         parent.palert("Shop này đã được gán Sales Leader '.$staffLeader['firstname'] .' '.$staffLeader['lastname'] .' - Code : '.$staffLeader['code'].' đứng gián tiếp , không được gán Sales Leader khác đứng trực tiếp.");
            //         parent.alert("Shop này đã được gán Sales Leader '.$staffLeader['firstname'] .' '.$staffLeader['lastname'] .' - Code : '.$staffLeader['code'].' đứng gián tiếp , không được gán Sales Leader khác đứng trực tiếp.");
            //     </script>';
            //     exit;
            // }

            //nếu là mấy thằng bê đê PB_SALE
            // if ($is_leader == 2) {
            //     $where = array();
            //     $where[] = $QStoreLeader->getAdapter()->quoteInto('staff_id <> ?', $id);
            //     $where[] = $QStoreLeader->getAdapter()->quoteInto('store_id = ?', $s_id);
            //     $where[] = $QStoreLeader->getAdapter()->quoteInto('is_leader IN (?)', array(1,2));
            //     $staff_store_check = $QStoreLeader->fetchRow($where);
            //     if ($staff_store_check) {
            //         $store_check = $QStore->find($s_id);
            //         $store_check = $store_check->current();
            //         echo '<script>
            //                 parent.palert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] (<a href=\"'.HOST.'manage/sales-pg-view?id='.$staff_store_check['staff_id'].'\" target=\"_blank\">'
            //                     .$store_check['name'] .'</a>) đã có nhân vien phụ trách (PB_SALES không được gán shop đã có nhân viên). Vui lòng kiểm tra lại.");

            //                 parent.alert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] ('
            //                     .$store_check['name'] .') đã có nhân phụ trách (PB_SALES không được gán shop đã có nhân viên). Vui lòng kiểm tra lại.");
            //             </script>';
            //         exit;
            //     }
            // }

            // if($is_leader == 0){
            //     $where             = array();
            //     $where[]           = $QStoreLeader->getAdapter()->quoteInto('staff_id <> ?', $id);
            //     $where[]           = $QStoreLeader->getAdapter()->quoteInto('store_id = ?', $s_id);
            //     $where[]           = $QStoreLeader->getAdapter()->quoteInto('is_leader = ?', 0);
            //     $staff_store_check = $QStoreLeader->fetchRow($where);
            //     if ($staff_store_check) {
            //         $store_check = $QStore->find($s_id);
            //         $store_check = $store_check->current();
            //         echo '<script>
            //                 parent.palert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] (<a href=\"'.HOST.'manage/sales-pg-view?id='.$staff_store_check['staff_id'].'\" target=\"_blank\">'
            //                     .$store_check['name'] .'</a>) đã có PGPB phụ trách. Vui lòng kiểm tra lại.");
            //                 parent.alert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] ('
            //                     .$store_check['name'] .') đã có PGPB phụ trách. Vui lòng kiểm tra lại.");
            //             </script>';
            //         exit;
            //     }
            // }

            // if($is_leader == 0){
            //     $where             = array();
            //     $where[]           = $QStoreLeader->getAdapter()->quoteInto('staff_id <> ?', $id);
            //     $where[]           = $QStoreLeader->getAdapter()->quoteInto('store_id = ?', $s_id);
            //     $where[]           = $QStoreLeader->getAdapter()->quoteInto('is_leader IN (?)', array(2));
            //     $staff_store_check = $QStoreLeader->fetchRow($where);
            //     if ($staff_store_check) {
            //         $store_check = $QStore->find($s_id);
            //         $store_check = $store_check->current();
            //         echo '<script>
            //                 parent.palert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] (<a href=\"'.HOST.'manage/sales-pg-view?id='.$staff_store_check['staff_id'].'\" target=\"_blank\">'
            //                     .$store_check['name'] .'</a>) đã có PB_SALES phụ trách. Vui lòng kiểm tra lại.");
            //                 parent.alert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] ('
            //                     .$store_check['name'] .') đã có PB_SALES phụ trách. Vui lòng kiểm tra lại.");
            //             </script>';
            //         exit;
            //     }
            // }
            
            // kiểm tra vùng và ngoại lệ
            // $where = $QRegion->getAdapter()->quoteInto('area_id IN (?)', array(HCMC1, HCMC2, HCMC3, HCMC4, HCMC5, HN1, HN2, HN3, HN4));
            // $hcm_hn_regions = $QRegion->fetchAll($where);
            // $hcm_hn_regions_arr = array();

            // foreach ($hcm_hn_regions as $key => $value) {
            //     $hcm_hn_regions_arr[] = $value['id'];
            // }

            // echo "<pre>";print_r($hcm_hn_regions_arr);die;
            // $store_check = $QStore->find($s_id);
            // $store_check = $store_check->current();
            // if (!$store_check) {
            //     echo '<script>
            //             parent.palert("Store thứ '.($k+1) .' không có thật, vui lòng kiểm tra lại.");

            //             parent.alert("Store thứ '.($k+1) .' không có thật, vui lòng kiểm tra lại.");
            //         </script>';
            //     exit;
            // }
            
            // if ( $staff['regional_market'] != $store_check['regional_market'] && !$is_leader) {
            //     if ( ! (in_array($store_check['regional_market'], $hcm_hn_regions_arr) 
            //         && in_array($staff['regional_market'], $hcm_hn_regions_arr)) ) {

            //         // check ngoại lệ region
            //         $casuals = $QCasual->get_cache();

            //         if (isset( $casuals[ $staff['id'] ] ) 
            //             && $casuals[ $staff['id'] ]['status'] == 1
            //             && $casuals[ $staff['id'] ]['area_id'] == @$region_cache[ $store_check['regional_market'] ]['area_id']) {
                        
            //         } else {

            //             echo '<script>
            //                     parent.palert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] ('
            //                         .$store_check['name'] .') nằm khác tỉnh so với hồ sơ của nhân viên. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

            //                     parent.alert("Trong danh sách cửa hàng, cửa hàng thứ ['.($k+1).'] ('
            //                         .$store_check['name'].') nằm khác tỉnh so với hồ sơ của nhân viên. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
            //                 </script>';
            //             exit;
            //         }
            //     }
            // }
            

            $QRegion = new Application_Model_RegionalMarket();
            $where = $QRegion->getAdapter()->quoteInto('area_id IN (?)', array(HCMC1, HCMC2, HCMC3, HCMC4, HCMC5, HN1, HN2, HN3, HN4));
            $hcm_hn_regions = $QRegion->fetchAll($where);
            $hcm_hn_regions_arr = array();

            foreach ($hcm_hn_regions as $key => $value) {
                $hcm_hn_regions_arr[] = $value['id'];
            }

            // echo $store['regional_market'];die;
            // echo "<pre>";print_r($region_cache);die;

            if ($staff['regional_market'] != $store['regional_market'] 
                AND (! (in_array($staff['regional_market'],array(4189,3433)) 
                        AND in_array($store['regional_market'],array(4189,3433)))) ) {

                if ( ! (in_array($store['regional_market'], $hcm_hn_regions_arr) 
                        && in_array($staff['regional_market'], $hcm_hn_regions_arr)) ) {

                    // check ngoại lệ region
                    $QCasual = new Application_Model_CasualWorker();
                    $casuals = $QCasual->get_cache();


                    if (isset( $casuals[ $staff['id'] ] )
                        && $casuals[ $staff['id'] ]['status'] == 1
                        && $casuals[ $staff['id'] ]['area_id'] == @$region_cache[ $store['regional_market'] ]['area_id']) {

                    } else {

                        echo '<script>
                                parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
                                    .$staff['firstname'] .' '.$staff['lastname'] .' | '
                                    . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

                                parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
                                    .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
                            </script>';
                        exit;
                    }
                }
            }

            if (isset($joined_dates[$s_id]) && !strtotime($joined_dates[$s_id])) {
                $tmp_store = $QStore->find($s_id);
                $tmp_store = $tmp_store->current();

                echo '<script>
                        parent.palert("Store thứ '.($k+1).' ['.$tmp_store['name'] .'] có ngày bắt đầu quản lý không đúng, vui lòng kiểm tra lại.");

                        parent.alert("Store thứ '.($k+1).' ['.$tmp_store['name'] .'] có ngày bắt đầu quản lý không đúng, vui lòng kiểm tra lại.");
                    </script>';
                exit;
            }

            $data = array(
                'store_id'  => $s_id,
                'staff_id'  => $id,
                'is_leader' => $is_leader,
                );

            $parent = $QStoreLeader->insert($data);

            $data = array(
                'parent'    => $parent,
                'is_leader' => $is_leader,
                'store_id'  => $s_id,
                'staff_id'  => $id,
                'joined_at' => isset($joined_dates[$s_id]) && strtotime($joined_dates[$s_id]) ? strtotime($joined_dates[$s_id]) : $time,
                );

            $QStoreLeaderLog->insert($data);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $id,
                'store_id'  => $s_id,
                'is_leader' => $is_leader,
                'action'    => "ADD",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }

        foreach ($del_ids as $s_id) {

            $today = date('Y-m-d');
            $staff_info = $QStaff->find($id);
            $staff_info = $staff_info->current();

            if ($is_leader == 0 && My_Staff::checkIfHaveTiming($id, $today, $s_id)) {
                echo '<script>
                        parent.palert("Bạn vừa gỡ PG/PB '
                            .@$staff_info['firstname'] .' '.@$staff_info['lastname'] .' | '
                            . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff_info['email']) .' ra khỏi shop. PG/PB này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo PG/PB kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), PG/PB có thể chấm công bình thường.");

                        parent.alert("Bạn vừa gỡ PG/PB '
                            .@$staff_info['firstname'] .' '.@$staff_info['lastname'] .' | '
                            . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff_info['email']) .' ra khỏi shop. PG/PB này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo PG/PB kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), PG/PB có thể chấm công bình thường.");
                    </script>';
                exit;
            }


            $where = array();
            $where[] = $QStoreLeader->getAdapter()->quoteInto('store_id = ?', $s_id);
            $where[] = $QStoreLeader->getAdapter()->quoteInto('staff_id = ?', $id);
            $QStoreLeader->delete($where);

            $where = array();
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('store_id = ?', $s_id);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('joined_at IS NOT NULL', 1);
            $where[] = $QStoreLeaderLog->getAdapter()->quoteInto('released_at IS NULL', 1);

            $data = array('released_at' => $time);
            $QStoreLeaderLog->update($data, $where);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $id,
                'store_id'  => $s_id,
                'is_leader' => $is_leader,
                'action'    => "DELETE",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }

        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = 'SALES/PG - Bind('.$id.') - Stores('.$pic.')';

        //todo log
        $QLog->insert( array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ) );
    }

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');

    echo '<script>parent.location.href="'. HOST.'manage/sales-pg"</script>';
    exit;

} catch(Exception $ex) {
    echo '<script>
            parent.palert("'.$ex->getMessage().'");
            parent.alert("'.$ex->getMessage().'");
        </script>';
    exit;
}

exit;