<?php
$id = $this->getRequest()->getParam('id');

$flashMessenger = $this->_helper->flashMessenger;

try {

    if (!$id) {
        throw new Exception('Invalid ID');
    }

    // check Asm/ leader role
    $QStaff = new Application_Model_Staff();
    $staff = $QStaff->find($id);
    $staff = $staff->current();

    if (!$staff) {
        throw new Exception('Invalid ID');
    }

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    if (!$userStorage) {
        $this->_redirect(HOST);
    }

    /**
     * Phân quyền view theo group
     */
    if ( $userStorage->id == SUPERADMIN_ID
            || in_array(
                $userStorage->group_id,
                array(ADMINISTRATOR_ID, HR_ID, HR_EXT_ID, BOARD_ID, SALES_EXT_ID)
            )
           || $userStorage->id == 2123 || $userStorage->id == 12719 // dieu.le
    ) { }

    elseif ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id) ) {
        $params['asm'] = $userStorage->id;

        $QAsm = new Application_Model_Asm();
        $asm_cache = $QAsm->get_cache();

        if (!isset($asm_cache[ $userStorage->id ])
            || !isset($asm_cache[ $userStorage->id ]['province'])
            || !in_array($staff['regional_market'], $asm_cache[ $userStorage->id ]['province'])
        ) throw new Exception("You can manage only staffs in your areas.");
    }

    elseif ( in_array($userStorage->group_id, array(PGPB_ID, SALES_ID)) ) {
        if ($userStorage->id != $id)
            throw new Exception("You can view only your stores.");
    }

    // elseif ($userStorage->group_id == LEADER_ID)
        // $params['leader'] = $userStorage->id;

    else
        $this->_redirect(HOST);

    // get leader's stores
    $QStoreStaffLog = new Application_Model_StoreStaffLog();
    $page = $this->getRequest()->getParam('page', 1);
    $limit = null;
    $params = array(
        'staff_id' => $id,
    );
    $total = 0;
    $this->view->store_staffs = $QStoreStaffLog->fetchPagination($page, $limit, $total, $params);

//    $QStore = new Application_Model_Store();
//    $this->view->stores = $QStore->get_cache();

    $QRegion = new Application_Model_RegionalMarket();
    $this->view->regions = $QRegion->get_cache_all();
    $QArea = new Application_Model_Area();
    $this->view->areas = $QArea->get_cache();

    $this->view->staff   = $staff;
} catch(Exception $ex) {
    $flashMessenger->setNamespace('error')->addMessage($ex->getMessage());
    $this->_redirect(HOST.'manage/sales-pg');
}

$this->_helper->viewRenderer->setRender('sales-pg/view');