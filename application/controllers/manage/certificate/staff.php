<?php
$flashMessenger         = $this->_helper->flashMessenger;
$messages               = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages   = $messages;
$messages_success       = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
$id                     = $this->getRequest()->getParam('id');
$del                    = $this->getRequest()->getParam('del', '');
$code                   = $this->getRequest()->getParam('code');
$QCertificateStaff      = new Application_Model_CertificateStaff();
if ($del)
{    
    $where = $QCertificateStaff->getAdapter()->quoteInto('id = ?', $del);
    $QCertificateStaff->delete($where);
    $flashMessenger->setNamespace('success')->addMessage('Deleted');
    $this->redirect('/manage/certificate-staff?id='.$id);
}

$staff                  = $QCertificateStaff->getStaffInfo();
$params = array(
    'id' => $id,
    'code' => $code,
    'name' => $name
);

$page   = $this->getRequest()->getParam('page', 1);
$limit  = 10;
$result = $QCertificateStaff->fetchPagination($page, $limit, $total, $params);    
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url          = HOST . 'manage/certificate-staff' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);
$this->view->staff        = $staff;
$this->view->id           = $id;

$save_folder   = 'certificate-data';
$requirement   = array(
    'Size'      => array('min' => 5, 'max' => 5000000),
    'Count'     => array('min' => 1, 'max' => 1),
    'Extension' => array('xls', 'xlsx'),
);
$db = Zend_Registry::get('db');
$db->beginTransaction();
if($this->getRequest()->isPost()) {    
    try {
        $file = My_File::get($save_folder, $requirement, true); 
        include 'PHPExcel/IOFactory.php';        
        $url  = DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . $file['filename'];
        $inputFileName  = '.' . $url;        
        try {
            $inputFileType  = PHPExcel_IOFactory::identify($inputFileName);
            $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel    = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        $sheet          = $objPHPExcel->getSheet(0); 
        $highestRow     = $sheet->getHighestRow(); 
        $highestColumn  = $sheet->getHighestColumn();         
        for ($row = 2; $row <= $highestRow; $row++){    
            $rowData[] = $sheet->rangeToArray('B' . $row,
                                            NULL,
                                            TRUE,
                                            TRUE);
        }
        foreach ($rowData as $key => $value) {
            $dt[] = $value[0];
        }
        foreach ($dt as $k => $v) {
            $dta = array(
                'certificate_id'    => $id,
                'code'              => $v[0]
            );
            $QCertificateStaff->insert($dta);
        }        
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Done');     
    }catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/manage/certificate-staff');
    }
    $this->redirect('/manage/certificate-staff?id='.$id);
}
$this->_helper->viewRenderer->setRender('certificate/staff');