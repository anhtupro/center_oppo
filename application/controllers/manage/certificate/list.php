<?php
$QCertificate = new Application_Model_Certificate();
$params = array(

    );

$page   = $this->getRequest()->getParam('page', 1);
$limit  = 10;

$result = $QCertificate->fetchPagination($page, $limit, $total, $params);    
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url          = HOST . 'certificate/certificate-list' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);

$this->_helper->viewRenderer->setRender('certificate/list');
