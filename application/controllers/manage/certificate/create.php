<?php
$flashMessenger     = $this->_helper->flashMessenger;
$messages           = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
$messages_success   = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
$id         = $this->getRequest()->getParam('id');
$del        = $this->getRequest()->getParam('del', 0);
$name       = $this->getRequest()->getParam('name');
$from       = date_create($this->getRequest()->getParam('from'));
$to         = date_create($this->getRequest()->getParam('to'));
$QCertificate  = new Application_Model_Certificate();
$params = array(
    'id'  => $id,
);
$sql = "SELECT MAX(id) as m_id FROM certificate";
$db = Zend_Registry::get('db');
$stmt = $db->prepare($sql);
$stmt->execute();
$exist_value = $stmt->fetchAll();
if($id > $exist_value[0]['m_id']){
    $this->redirect('/manage/certificate-create');
}
if($id){    
    $certificate   = $QCertificate->getData($params);
    $this->view->certificate = $certificate;    
}
$save_folder   = 'certificate';
$requirement   = array(
    'Size'      => array('min' => 5, 'max' => 5000000),
    'Count'     => array('min' => 1, 'max' => 1),
    'Extension' => array('png', 'jpg'),
);
$db = Zend_Registry::get('db');
$db->beginTransaction();
if($this->getRequest()->isPost()) {    
    try {
        if (empty($name)) {
            throw new Exception(' Vui lòng không để trống các trường bắt buộc');
        }   
        if($id){           
            if($del == '0'){
                $current = $QCertificate->getAdapter()->quoteInto('del = ?', '0');
                $QCertificate->update(array('del' => 1), $where);
            }
            $data = array(//array_filter(
                'name'    => trim($name),                
                'from'    => date_format($from,"Y-m-d H:i:s"),
                'to'      => date_format($to,"Y-m-d H:i:s"),
                'del'     => $del
            );    
            $where = $QCertificate->getAdapter()->quoteInto('id = ?', $id);
            $QCertificate->update($data, $where);
            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Updated');  
            $this->redirect('/manage/certificate-create?id='.$id);
        }else{
            $file = My_File::get($save_folder, $requirement, true);   
            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['filename'];     
            $data = array(//array_filter(
                'name'    => trim($name),
                'url'     => DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . $file['filename'], 
                'from'    => date_format($from, "Y-m-d H:i:s"),
                'to'      => date_format($to,   "Y-m-d H:i:s"),
            );  
            
            //Move file to S3
            require_once 'Aws_s3.php';
            $s3_lib = new Aws_s3();
            
            $file_location = "files" . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . $file['filename'];
            $detination_path = 'files/'.$save_folder.'/'.$file['folder'].'/';
            $destination_name = $file['filename'];
            
            
            $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
            
            
            if($upload_s3['message'] != 'ok'){
                echo json_encode([
                    'status' => 0,
                    'message' => "Upload S3 không thành công",
                ]);
                
                $flashMessenger->setNamespace('error')->addMessage("Upload S3 không thành công::".$upload_s3['message']);
                $this->redirect('/manage/certificate-create');
                
                return;
            }
            // END - Move file to S3
            
            $id_n  = $QCertificate->insert($data);
            $id_m    = (string)(intval($id_n) - 1);
            $where = $QCertificate->getAdapter()->quoteInto('id = ?', $id_m);
            $QCertificate->update(array('del' => '1'), $where);
            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');    
            $this->redirect('/manage/certificate-create?id='.$id_n);
        }  
    }catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/manage/certificate-create');
    }    
}
$this->view->edit_url = $id;
$this->_helper->viewRenderer->setRender('certificate/create');