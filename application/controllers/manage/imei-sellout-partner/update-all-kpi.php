<?php
$imei_list            = $this->getRequest()->getParam('imei_list', null);

$status               = $this->getRequest()->getParam('status');
$product              = $this->getRequest()->getParam('product');
$color                = $this->getRequest()->getParam('model');
$store                = $this->getRequest()->getParam('store_id');
$from                 = $this->getRequest()->getParam('from', date('01/m/Y'));
$to                   = $this->getRequest()->getParam('to', date('d/m/Y'));
$imei                 = $this->getRequest()->getParam('imei', null);
$area_id              = $this->getRequest()->getParam('area_id');
$regional_market      = $this->getRequest()->getParam('regional_market');
$district             = $this->getRequest()->getParam('district');
$sales_team           = $this->getRequest()->getParam('sales_team');
$staff                = $this->getRequest()->getParam('staff_id', null);
$shop_type            = $this->getRequest()->getParam('shop_type');
$kpi                  = $this->getRequest()->getParam('kpi');
$export               = $this->getRequest()->getParam('export');
$status_imei          = $this->getRequest()->getParam('status_imei');
$page                 = $this->getRequest()->getParam('page');
$imei_sn              = $this->getRequest()->getParam('imei_sn');
$pg_id                = $this->getRequest()->getParam('pg_id');
//$app_install          = $this->getRequest()->getParam('app_installing', 0);
$installment_company  = $this->getRequest()->getParam('installment_company');
$installment_contract = $this->getRequest()->getParam('installment_contract');
$installment_status   = $this->getRequest()->getParam('installment_status',0);
$imei_installment   = $this->getRequest()->getParam('imei_installment',null);

//$imei_installment = preg_replace("$[\[\]]$",'',$imei_installment);
$array_installment = json_decode($imei_installment, true);


$params = array(
    'product'         => intval($product),
    'color'           => intval($color),
    'store_id'        => trim($store),
    'from'            => trim($from),
    'to'              => trim($to),
    'imei'            => $imei,
    'status'          => intval($status),
    'shop_type'       => intval($shop_type),
    'kpi'             => intval($kpi),
    'area_id'         => intval($area_id),
    'regional_market' => intval($regional_market),
    'district'        => intval($district),
    'status_imei'     => intval($status_imei),
    'page'            => $page
);
try {
    $flashMessenger = $this->_helper->flashMessenger;

    if (!$imei_list)
        throw new Exception("Invalid imei");
    
    $QLogSelloutPartner  = new Application_Model_LogSelloutPartner();
    $QImeiSelloutPartner = new Application_Model_ImeiSelloutPartner();
    $QImeiInstallment    = new Application_Model_ImeiInstallment();
    $userStorage         = Zend_Auth::getInstance()->getStorage()->read();
    
    $user_id             = $userStorage->id;
    $array_imei =  explode(",",$imei_list);

    if (empty($pg_id)) {
        $data = array('pg_id' => $user_id);
    } else
        $data     = array('pg_id' => $pg_id);
    
    $is_chot = 2; // is_chot khac 2 moi cho bao so
    $where = array();
    $where[] = $QImeiSelloutPartner->getAdapter()->quoteInto("imei_sn IN ($imei_list)", true);
    $where[] = $QImeiSelloutPartner->getAdapter()->quoteInto("is_chot != ?", $is_chot);
    $QImeiSelloutPartner->update($data, $where);
    
    $array_imei = explode(",",$imei_list);
   
    if(!empty($array_imei)){
        $data_imei_log = array();
        foreach ($array_imei as $key => $value) {
            $data_log = array(
                    'user_id'    => $user_id,
                    'imei_sn'    => $value,
                    'pg_id'      => empty($pg_id) ? $user_id : $pg_id,
                    'action'     => "update all kpi",
                    'created_at' => date('Y-m-d H:i:s')
                );
            $data_imei_log[] = $data_log;
        }
        
        if (!empty($data_imei_log)) {
            My_Controller_Action::insertAllrow($data_imei_log, 'sellout_partner_log');
        }
    }
    
    if(!empty($array_installment)){
        
        $data_installment = array();    
        foreach ($array_installment as $k => $val) {
            $installment = array(
                    'imei_sn'            => $val['imei'],
                    'staff_id'            => empty($pg_id) ? $user_id : $pg_id,
                    'contract_number'     => $val['contract'],
                    'installment_company' => $val['company'],
                    'is_ka_link'          => 1,
                    'created_at'          => date('Y-m-d H:i:s')
                );
            $data_installment[] = $installment;
        }
        
        if (!empty($data_installment)) {
            My_Controller_Action::insertAllrow($data_installment, 'imei_installment');
        }
        
    }

    $flashMessenger->setNamespace('success')->addMessage('Success');
    $this->_redirect(HOST . 'manage/imei-sellout-partner/' . ( $params ? '?' . http_build_query($params) . '&' : '?' ));
} catch (Exception $e) {
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect(HOST . 'manage/imei-sellout-partner/' . ( $params ? '?' . http_build_query($params) . '&' : '?' ));
}