<?php 
$imei			= $this->getRequest()->getParam('imei');
$pg_id			= $this->getRequest()->getParam('pg_id');
$userStorage	= Zend_Auth::getInstance()->getStorage()->read();
$user_id             = $userStorage->id;
$QImeiSelloutPartner = new Application_Model_ImeiSelloutPartner();

$QLogSelloutPartner = new Application_Model_LogSelloutPartner();

$data_log = array(
				'user_id'		=> $user_id,
				'imei_sn'		=> $imei,
				'pg_id'			=> $pg_id,
				'action'		=> "remove",
				'created_at'	=> date('Y-m-d H:i:s')
);

$data = array('pg_id' => null );
$where = $QImeiSelloutPartner->getAdapter()->quoteInto('imei_sn = ?', $imei);
$QImeiSelloutPartner->update($data, $where);
$QLogSelloutPartner->insert($data_log);

$this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOST.'manage/imei-sellout-partner');