<?php 
// $imei			= $this->getRequest()->getParam('imei');
// $userStorage	= Zend_Auth::getInstance()->getStorage()->read();
// $user_id             = $userStorage->id;
// $QImeiSelloutPartner = new Application_Model_ImeiSelloutPartner();

// $data = array('pg_id' => $user_id );
// $where = $QImeiSelloutPartner->getAdapter()->quoteInto('imei_sn = ?', $imei);
// $QImeiSelloutPartner->update($data, $where);

// $this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOST.'manage/imei-sellout-partner');

$status          = $this->getRequest()->getParam('status');
$product         = $this->getRequest()->getParam('product');
$color           = $this->getRequest()->getParam('model');
$store           = $this->getRequest()->getParam('store_id');
$from            = $this->getRequest()->getParam('from', date('01/m/Y') );
$to              = $this->getRequest()->getParam('to', date('d/m/Y') );
$imei            = $this->getRequest()->getParam('imei',null);
$area_id         = $this->getRequest()->getParam('area_id');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$sales_team      = $this->getRequest()->getParam('sales_team');
$staff           = $this->getRequest()->getParam('staff_id',null);
$shop_type       = $this->getRequest()->getParam('shop_type');
$kpi             = $this->getRequest()->getParam('kpi');
$export          = $this->getRequest()->getParam('export');
$status_imei     = $this->getRequest()->getParam('status_imei');

$imei_sn			= $this->getRequest()->getParam('imei_sn');


$params = array(
    'product'         => intval($product),
    'color'           => intval($color),
    'store_id'        => trim($store),
    'from'            => trim($from),
    'to'              => trim($to),
    'imei'            => $imei,
    'status'          => intval($status),
    'shop_type'       => intval($shop_type),
    'kpi'             => intval($kpi),
    'area_id'         => intval($area_id),
    'regional_market' => intval($regional_market),
    'district'        => intval($district),
    'status_imei'     => intval($status_imei),
);

try {
    $flashMessenger = $this->_helper->flashMessenger;

    if (!$imei_sn) throw new Exception("Invalid imei");
    
    $userStorage	= Zend_Auth::getInstance()->getStorage()->read();
$user_id             = $userStorage->id;
$QImeiSelloutPartner = new Application_Model_ImeiSelloutPartner();

$data = array('pg_id' => $user_id );
$where = $QImeiSelloutPartner->getAdapter()->quoteInto('imei_sn = ?', $imei_sn);
$QImeiSelloutPartner->update($data, $where);


    $flashMessenger->setNamespace('success')->addMessage('Success');
    $this->_redirect(HOST.'manage/imei-sellout-partner/'.( $params ? '?'.http_build_query($params).'&' : '?' ));
} catch (Exception $e) {
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect(HOST.'manage/imei-sellout-partner/'.( $params ? '?'.http_build_query($params).'&' : '?' ));
}