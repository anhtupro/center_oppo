<?php 
$imei			= $this->getRequest()->getParam('imei');
$userStorage	= Zend_Auth::getInstance()->getStorage()->read();
$user_id             = $userStorage->id;
$QImeiSelloutPartner = new Application_Model_ImeiSelloutPartner();

$data = array('pg_id' => null );
$where = $QImeiSelloutPartner->getAdapter()->quoteInto('imei_sn = ?', $imei);
$QImeiSelloutPartner->update($data, $where);

$this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOST.'manage/imei-sellout-partner');