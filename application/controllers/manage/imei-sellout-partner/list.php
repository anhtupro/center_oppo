<?php

$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$status          = $this->getRequest()->getParam('status');
$product         = $this->getRequest()->getParam('product');
$color           = $this->getRequest()->getParam('model');
$store           = $this->getRequest()->getParam('store_id');
$from            = $this->getRequest()->getParam('from', date('01/m/Y'));
$to              = $this->getRequest()->getParam('to', date('d/m/Y'));
$imei            = $this->getRequest()->getParam('imei', null);
$area_id         = $this->getRequest()->getParam('area_id');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$sales_team      = $this->getRequest()->getParam('sales_team');
$staff           = $this->getRequest()->getParam('staff_id', null);
$shop_type       = $this->getRequest()->getParam('shop_type');
$kpi             = $this->getRequest()->getParam('kpi');
$export          = $this->getRequest()->getParam('export');
$status_imei     = $this->getRequest()->getParam('status_imei');

$QImeiSelloutPartner          = new Application_Model_ImeiSelloutPartner();
$flashMessenger               = $this->_helper->flashMessenger;
$messages                     = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages;
// $string = "0";
// print_r(explode(',',$string));die;


$page              = $this->getRequest()->getParam('page', 1);
// $sort        = $this->getRequest()->getParam('sort', 'name');
// $desc        = $this->getRequest()->getParam('desc', 0);
$limit             = 100;
$total             = 0;
$total_fail        = 0;
$params['area_id'] = array();

$user_id             = $userStorage->id;
$this->view->user_id = $user_id;
$title               = $userStorage->title;
// echo $title;die;
$this->view->title   = $title;

// if ($user_id != 5899 and $user_id != 341 ) {
//   echo 'Hệ thống đang được khóa để đổ số TGDĐ , Vui lòng trở lại khi có thông báo !';exit;
// }
// echo "<pre>";print_r($user_id);die;
$params                  = array(
    'product'         => intval($product),
    'color'           => intval($color),
    'store_id'        => trim($store),
    'from'            => trim($from),
    'to'              => trim($to),
    'imei'            => $imei,
    'status'          => intval($status),
    'shop_type'       => intval($shop_type),
    'kpi'             => intval($kpi),
    'area_id'         => intval($area_id),
    'regional_market' => intval($regional_market),
    'district'        => intval($district),
    'status_imei'     => intval($status_imei),
    'page'            => $page
);
$QStaff                  = new Application_Model_Staff();
$staff_cache             = $QStaff->get_all_cache();
$this->view->staff_cache = $staff_cache;
// echo "<pre>";print_r($staff);die;
$QTeam                   = new Application_Model_Team();
$where_group_areas       = array();
$where_group_areas       = $QTeam->getAdapter()->quoteInto('id = ?', $title);

$group_areas              = $QTeam->fetchRow($where_group_areas, 'policy_group');
$this->view->policy_group = $group_areas->policy_group;

if ($group_areas->policy_group == 6 || $group_areas->policy_group == 8) {
    $QAsm       = new Application_Model_Asm();
    $where_area = array();
    $where_area = $QAsm->getAdapter()->quoteInto('staff_id = ?', $user_id);

    $areas = $QAsm->fetchAll($where_area, 'area_id');

    foreach ($areas as $key => $value) {
        $area_[] = $value['area_id'];
    }
    if (in_array($params['area_id'], $area_)) {
        $params['area_id'] = $params['area_id'];
    } else
        $params['area_id'] = $area_;
} else {
    if ($title == PGPB_TITLE || $title == CHUYEN_VIEN_BAN_HANG_TITLE || $title == SALES_TITLE) {
        $params['staff_id'] = intval($user_id);
    } else
        $params['staff_id'] = $staff;
}




// if ($title == SALE_SALE_SALE_ADMIN || $title == SALE_SALE_ASM || $title == 308 ) {
// }
// echo "<pre>";print_r($params);die;


if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm       = new Application_Model_Asm();
    $where_area = array();
    $where_area = $QAsm->getAdapter()->quoteInto('staff_id = ?', $user_id);

    $areas = $QAsm->fetchAll($where_area, 'area_id');

    foreach ($areas as $key => $value) {
        $area_[] = $value['area_id'];
    }
    if (in_array($params['area_id'], $area_)) {
        $params['area_id'] = $params['area_id'];
    } else
        $params['area_id'] = $area_;
}
// if ($user_id  == 257) {
//  echo "<pre>";print_r($params);die;
// }

if ($export) {
    // if ($user_id == 5899) {
    $QImeiSelloutPartner->export($QImeiSelloutPartner->fetchPagination($page, null, $total, $params));
}

$imeis      = $QImeiSelloutPartner->fetchPagination($page, $limit, $total, $params);
$imeis_fail = $QImeiSelloutPartner->fetchImeiFailPagination($page, $limit, $total_fail, $params);


// echo "<pre>";print_r($imeis);
// echo "<pre>";print_r($user_id);
// if ($imeis[0]['pg_id'] == $user_id ) {
//     echo 1;
// } else echo 2;
// die;

if ($user_id == SUPERADMIN_ID || in_array($group_id, array(ADMINISTRATOR_ID, BOARD_ID, SALES_EXT_ID))) {
    
}
if ($userStorage->title != PGPB_TITLE) {
    # code...
}
//get goods
$QGood     = new Application_Model_Good();
$where_g   = array();
$where_g[] = $QGood->getAdapter()->quoteInto('cat_id IN (?)', [PHONE_CAT_ID, IOT_OPPO_CAT_ID]);
// tạm thời ko cho chấm one plus
$where_g[] = $QGood->getAdapter()->quoteInto('id  != ?', 424);
$goods     = $QGood->fetchAll($where_g, 'desc');


$this->view->goods = $goods;

// load all model
$QGoodColor = new Application_Model_GoodColor();
$result     = $QGoodColor->fetchAll();

$QArea             = new Application_Model_Area();
$QRegionalMarket   = new Application_Model_RegionalMarket();
$this->view->areas = $QArea->fetchAll(null, 'name');

if ($area_id) {
    $where                        = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}


if ($regional_market) {
    //get district
    $where                 = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}

if ($area_id) {
    $where                        = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

$data = null;
if ($result->count()) {
    foreach ($goods as $good) {
        $colors = explode(',', $good->color);
        $temp   = array();
        foreach ($result as $item) {
            if (in_array($item['id'], $colors)) {
                $temp[] = array(
                    'id'    => $item->id,
                    'model' => $item->name,
                );
            }
        }
        $data[$good['id']] = $temp;
    }
}
$begin_passby_tgdd = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
$to_passby_tgdd = date_create_from_format("d/m/Y", $to)->format("Y-m-d");
unset($params['area_id']);
// echo "<pre>";print_r($data[$product]);
$this->view->colors      = $data[$product];
$this->view->good_colors = json_encode($data);
$this->view->params      = $params;

// $this->view->imei        = (!in_array($user_id, array(5899, 341)) AND  $begin_passby_tgdd >= '2020-01-01') ? array() :  $imeis ;
$this->view->show_export = 1;
$staff_id_test = 26485;
if(!in_array($user_id, array(5899, 341, $staff_id_test)) AND  ($begin_passby_tgdd > '2020-01-31' || $to_passby_tgdd > '2020-01-31') ) {
	$this->view->show_export = 0;
}
$this->view->imei        = (!in_array($user_id, array(5899, 341, $staff_id_test)) AND  ($begin_passby_tgdd > '2020-01-31' 
 || $to_passby_tgdd > '2020-01-31'
) 
) ? array() :  $imeis ;
$this->view->staff_id = $user_id;
$this->view->show_export = 1;
 $this->view->imei        = $imeis ;
$this->view->imei_fail   = $imeis_fail;
$this->view->limit       = $limit;
$this->view->total       = $total;
$this->view->total_fail  = $total_fail;
$this->view->param       = $params ? '?' . http_build_query($params) . '&' : '?';
$this->view->url         = HOST . 'manage/imei-sellout-partner/' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset      = $limit * ($page - 1);

$this->_helper->viewRenderer->setRender('imei-sellout-partner/list');


