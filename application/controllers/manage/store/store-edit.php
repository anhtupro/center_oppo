<?php
$id = $this->getRequest()->getParam('id');

$QArea = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();
$this->view->id_store = $id;
$QRegionalMarket = new Application_Model_RegionalMarket();

if ($id) {
    $QModel = new Application_Model_Store();
    $rowset = $QModel->find($id);
    $store = $rowset->current();

    // echo $store['is_brand_shop'];die;
    $this->view->brandshop = $store['is_brand_shop'];
    // echo "<pre>";print_r($store);die;
    $this->view->store = $store;

    if (isset($store['district'])) {
        $district_cache = $QRegionalMarket->get_district_cache();
        $province_cache = $QRegionalMarket->get_cache_all();

        $province_cache_id = isset( $province_cache[ $district_cache[ $store['district'] ]['parent'] ] ) ? $district_cache[ $store['district'] ]['parent'] : 0;

        if ( $province_cache_id ) {
            $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $province_cache_id);
            $this->view->store_province = $province_cache_id;
            $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');

            $area_cache_id = isset( $province_cache[ $province_cache_id ]['area_id'] ) ? $province_cache[ $province_cache_id ]['area_id'] : 0;

            if ( $area_cache_id ) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_cache_id);
                $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
                $this->view->store_area = $area_cache_id;
            } // END if area_cache_id
        } // END if province_cache_id
    } // END if $store['district']

    //get staff assign
    $QStoreStaff = new Application_Model_StoreStaffLog();
    $where = array();
	$where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
	$where[] = $QStoreStaff->getAdapter()->quoteInto('released_at is null and is_leader = ?', 0);
    $data = $QStoreStaff->fetchAll($where);
    $pgTgdd = $QStoreStaff->pgTgdd($id);
    $this->view->pgTgdd = $pgTgdd;
// echo "<pre>";print_r($pgTgdd);die;
    $where_pgtgdd[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    $where_pgtgdd[] = $QStoreStaff->getAdapter()->quoteInto('released_at is null and is_leader = ?', 0);
    $where_pgtgdd[] = $QStoreStaff->getAdapter()->quoteInto('is_kpi = ?', 1);
    $dataPgTgdd = $QStoreStaff->fetchRow($where_pgtgdd);
    // echo "<pre>";print_r($dataPgTgdd);die;
    $id_kpi = 1;
    if(!empty($dataPgTgdd)) 
        $id_kpi =  $dataPgTgdd['id'] ;

    $this->view->id_kpi = $id_kpi;
    // echo "<pre>";print_r($id_kpi);die;
    if ($data->count()){
        $tem = array();
        foreach ($data as $item)
            $tem[] = $item->staff_id;
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $tem);
        $this->view->staffs = $QStaff->fetchAll($where);

    }

     //get store leader assign
    $QStoreStaff = new Application_Model_StoreStaffLog();
    $where = array();
    $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    $where[] = $QStoreStaff->getAdapter()->quoteInto('released_at is null and is_leader = ?', 3);
    $data = $QStoreStaff->fetchAll($where);

    if ($data->count()){
        $tem = array();
        foreach ($data as $item)
            $tem[] = $item->staff_id;
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $tem);
        $this->view->store_leader = $QStaff->fetchAll($where);
        // echo "<pre>";print_r($QStaff->fetchAll($where));die;
    }


    //get pgs leader assign
    $QStoreStaff = new Application_Model_StoreStaffLog();
    $where = array();
    $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    $where[] = $QStoreStaff->getAdapter()->quoteInto('released_at is null and is_leader = ?', 5);
    $data = $QStoreStaff->fetchAll($where);
	    if ($data->count()){
        $tem = array();
        foreach ($data as $item)
            $tem[] = $item->staff_id;
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $tem);
        $this->view->pg_leader = $QStaff->fetchAll($where);
        // echo "<pre>";print_r($QStaff->fetchAll($where));die;
    }

    $whereConsult = array();
    $whereConsult[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    $whereConsult[] = $QStoreStaff->getAdapter()->quoteInto('released_at is null and is_leader = ?', 7);
    $data = $QStoreStaff->fetchAll($whereConsult);
	    if ($data->count()){
        $tem = array();
        foreach ($data as $item)
            $tem[] = $item->staff_id;
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $tem);
        $this->view->pg_Consult = $QStaff->fetchAll($where);
        // echo "<pre>";print_r($QStaff->fetchAll($where));die;
    }



    //leader
    $QStoreStaff = new Application_Model_StoreStaffLog();
	$where = array();
	$where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
	$where[] = $QStoreStaff->getAdapter()->quoteInto('released_at is null and is_leader IN (?)', array(1,2));
	$data = $QStoreStaff->fetchAll($where);
	
	if ($data->count()){
		$tem = array();
		
		foreach ($data as $item)
			$tem[] = $item->staff_id;
		
		$QStaff = new Application_Model_Staff();
		$where = $QStaff->getAdapter()->quoteInto('id IN (?)', $tem);
		$this->view->leaders = $QStaff->fetchAll($where);
	}
}

$QDistributor = new Application_Model_Distributor();
$where = $QDistributor->getAdapter()->quoteInto('del IS NULL OR del = ?', 0);
$this->view->distributors = $QDistributor->fetchAll($where, 'title');

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$this->view->group_id = $userStorage->group_id;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$this->_helper->viewRenderer->setRender('store/create');