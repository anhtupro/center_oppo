<?php
$id = $this->getRequest()->getParam('id');
$this->view->group_id = Zend_Auth::getInstance()->getStorage()->read()->group_id;
$QArea = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();

if ($id) {
    $QModel = new Application_Model_Store();
    $rowset = $QModel->find($id);
    $store = $rowset->current();
    
    $this->view->store = $store;

    $rowset = $QRegionalMarket->find($store->regional_market);
    $regional_market = $rowset->current();

    $rowset = $QArea->find($regional_market->area_id);
    $area = $rowset->current();

    $this->view->area = $area;

    //get staff assign
    $QStoreStaff = new Application_Model_StoreStaff();
    $QStaff = new Application_Model_Staff();

    $where = array();
    $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader IN (?)', array(1,2));

    $data = $QStoreStaff->fetchAll($where);

    if ($data->count()){
        $tem = array();

        foreach ($data as $item)
            $tem[] = $item->staff_id;

        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $tem);
        $this->view->leaders = $QStaff->fetchAll($where);
    }

    $where = array();
    $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?', 0);

    $data = $QStoreStaff->fetchAll($where);

    if ($data->count()){
        $tem = array();

        foreach ($data as $item)
            $tem[] = $item->staff_id;

        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $tem);
        $this->view->staffs = $QStaff->fetchAll($where);
    }
}

$QDistributor = new Application_Model_Distributor();
$where = $QDistributor->getAdapter()->quoteInto('del IS NULL OR del = ?', 0);
$this->view->distributors = $QDistributor->fetchAll($where, 'title');

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$this->_helper->viewRenderer->setRender('store/create');