<?php
$page            = $this->getRequest()->getParam('page', 1);
$name            = $this->getRequest()->getParam('name');
$address         = $this->getRequest()->getParam('address');
$area_id         = $this->getRequest()->getParam('area_id');
$staff_email     = $this->getRequest()->getParam('staff_email');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$d_id            = $this->getRequest()->getParam('d_id');
$partner_id      = $this->getRequest()->getParam('partner_id');
$sort            = $this->getRequest()->getParam('sort', 'created_at');

$have_pg         = $this->getRequest()->getParam('have_pg', 0);
$have_sales      = $this->getRequest()->getParam('have_sales', 0);
$no_staff        = $this->getRequest()->getParam('no_staff', 0);

$export          = $this->getRequest()->getParam('export', 0);
$desc            = $this->getRequest()->getParam('desc', 1);
$gps             = $this->getRequest()->getParam('gps');
$export_not_gps  = $this->getRequest()->getParam('export_not_gps');
$export_not_gps_detail  = $this->getRequest()->getParam('export_not_gps_detail');
if($export_not_gps_detail){
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $params['asm'] = $userStorage->id;
    $QAsm = new Application_Model_Asm();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $list_district = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();
    $list_regional_market = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();
     foreach ($list_regional_market as $k => $param){
        $str_insert .= $param . ',';
        
    }
      $db     = Zend_Registry::get('db');
    $str_rows = rtrim($str_insert,',');
    
    
    $sql_gps = "
        SELECT st.id , st.`name` as 'store_name',a.`name`,
            ss.code, 
            CONCAT(ss.firstname,' ',ss.lastname) AS 'staff_name',
            ss.phone_number          
                            FROM  `store` st
            								LEFT JOIN store_staff_log s1 ON s1.store_id = st.id AND s1.released_at is null AND s1.is_leader = 1
            								LEFT JOIN staff ss ON ss.id = s1.staff_id			
                            LEFT JOIN regional_market rm ON st.`regional_market` = rm.`id` 
                            LEFT JOIN area a ON rm.area_id = a.id
                            WHERE (st.del <> 1 OR st.del IS NULL)
                            AND (
								st.longitude IS NULL
			OR st.latitude IS NULL OR st.latitude = '' OR st.longitude = ''
							) 
            								AND st.regional_market IN ($str_rows)
                            ORDER BY a.id
                    ";
    
 $stmt = $db->prepare($sql_gps);
    
    $stmt->execute();
    
    $data = $stmt->fetchAll();
    
       
}

if($export_not_gps){
    $db     = Zend_Registry::get('db');
    
    $sql  = "SELECT a.`name` , COUNT( st.id ) AS 'slg'
                FROM  `store` st
                LEFT JOIN regional_market rm ON st.`regional_market` = rm.`id` 
                LEFT JOIN area a ON rm.area_id = a.id
                WHERE (st.del <> 1 OR st.del IS NULL)
                AND  (
			st.longitude IS NULL
			OR st.latitude IS NULL OR st.latitude = '' OR st.longitude = ''
		)
                GROUP BY ( a.id )
                ORDER BY COUNT( st.id ) DESC ";
    $stmt = $db->prepare($sql);
    
    $stmt->execute();
    
    $data = $stmt->fetchAll();
    
    
    set_time_limit(0);
    ini_set('memory_limit', '5120M');
    
    $stmt->execute();
    $data_export = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = $db = null;
    
    require_once 'PHPExcel.php';
    
    $alphaExcel = new My_AlphaExcel();
    
    $PHPExcel = new PHPExcel();
    $heads = array(
        $alphaExcel->ShowAndUp() => 'Khu vực',
        $alphaExcel->ShowAndUp() => 'Số lượng',
    );
    
    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    
    foreach($heads as $key => $value)
    {
        $sheet->setCellValue($key.'1', $value);
    }
    
    $index = 1;
    foreach($data_export as $key => $value)
    {
        $alphaExcel = new My_AlphaExcel();
    
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name']);
        $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['slg']);
        $index++;
    
    }
    $filename = 'Export Time - ' . date('Y-m-d H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    
    exit;
    
    
    $stmt->closeCursor();
    $db = $stmt = null;
}

//
$limit = LIMITATION;
$total = 0;
// PC::db($partner_id);
$params = array(
    'name'            => $name,
    'address'         => $address,
    'area_id'         => $area_id,
    'regional_market' => $regional_market,
    'district'        => $district,
    'd_id'            => $d_id,
    'partner_id'      => $partner_id,
    'staff_email'     => $staff_email,
    'have_pg'         => $have_pg,
    'no_staff'        => $no_staff,
    'have_sales'      => $have_sales,
    'export'          => $export,
    'gps'             => $gps,
);

$params['sort'] = $sort;
$params['desc'] = $desc;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id = $userStorage->group_id;

$title = $userStorage->title;
$this->view->title = $title;

if ($title == SALES_TITLE || $title == LEADER_TITLE) {
    $params['sale-gps'] = $userStorage->id;
}


if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $params['asm'] = $userStorage->id;
} elseif ($group_id == SALES_ID) {
    $params['sales_store'] = $userStorage->id;
} elseif ($group_id == LEADER_ID) {
    $params['leader_province'] = $userStorage->id;
}

$QArea = new Application_Model_Area();
$this->view->areas = $QArea->fetchAll(null, 'name');

if ($area_id) {
    $QRegionalMarket = new Application_Model_RegionalMarket();
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regional_market) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$QModel = new Application_Model_Store();
$stores = $QModel->fetchPagination($page, $limit, $total, $params);

if ($export && $export == 1) {
    $loader = new Zend_Loader_PluginLoader();
    $loader->addPrefixPath('Export_', 'My/Application/Export2CSV');
    $sales = $loader->load('Sales');
    Export_Sales::store_list($stores);


  



    // echo "<pre>";print_r($stores);die;

}

// echo "<pre>";print_r($sellout);die;

$QDistributor = new Application_Model_Distributor();
$this->view->distributor_cache = $QDistributor->get_cache();

$this->view->params = $params;
$this->view->sort = $sort;
$this->view->desc = $desc;
$this->view->stores = $stores;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST.'manage/store/'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset = $limit*($page-1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;

if($this->getRequest()->isXmlHttpRequest()) {
    $this->_helper->layout->disableLayout();

    $this->_helper->viewRenderer->setRender('store/partials/list');
} else
    $this->_helper->viewRenderer->setRender('store/index');


// ADD
// Search Distributor Using Ajax

if($this->getRequest()->isXmlHttpRequest())
{
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $SearchBox = $this->getRequest()->getParam('txt');


    $db     = Zend_Registry::get('db');

    $sql  = 'SELECT id,title FROM warehouse.distributor WHERE title LIKE "%'.$SearchBox.'%"';
    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll();
    $stmt->closeCursor();
    $db = $stmt = null;

    $this->view->dealer_ajax = $data;
    $this->render('store/partials/search-dealer-ajax');
}
