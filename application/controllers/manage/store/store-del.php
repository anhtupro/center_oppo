<?php
$id = $this->getRequest()->getParam('id');
if ($id) $id = intval($id);
else $this->_redirect(HOST.'manage/store');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id = $userStorage->id;


// log mới Tuấn


$db = Zend_Registry::get('db');
$db->beginTransaction();


$QLogStore            = new Application_Model_LogStore();
$QModel = new Application_Model_Store();

$data = array('del' => 1, 'partner_id' => 0);
$where = $QModel->getAdapter()->quoteInto('id = ?', $id);
$QModel->update($data, $where);

$data_log = array(
                'user_id'   => $user_id,
                'staff_id'  => NULL,
                'store_id'  => $id,
                'is_leader' => null,
                'action'    => "DEL SHOP",
                'created_at' => date('Y-m-d H:i:s')
                );
$QLogStore->insert($data_log);

try {
    $sql = 'CALL sp_remove_staff_from_deleted_store(?)';
    $db->query($sql, array($id));
} catch (Exception $e) {
    $db->rollback();
}

$db->commit();
//remove cache
$cache = Zend_Registry::get('cache');
$cache->remove('store_cache');
$cache->remove('store_staff_log_cache');
$cache->remove('store_assigned_cache');

$this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOST.'manage/store');