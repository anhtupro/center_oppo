<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$id         = $this->getRequest()->getParam('id');
$latitude   = $this->getRequest()->getParam('latitude');
$longitude  = $this->getRequest()->getParam('longitude');
$address    = $this->getRequest()->getParam('address');
$error_gps    = $this->getRequest()->getParam('error_gps');

$db         = Zend_Registry::get('db');
$QStore         = new Application_Model_Store();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if (!empty($id) && !empty($latitude) && !empty($longitude)){
    $where = $QStore->getAdapter()->quoteInto('id = ?', $id);
    if($error_gps){
        $data = array(
            'error_gps'      => 1,
			'error_gps_by'      => $userStorage->id
			
            );
    }else{
        $data = array(
            'latitude'      => $latitude,
            'longitude'     => $longitude,
            'address_gps'   => $address,
            'update_gps_by' => $userStorage->id,
            'update_gps_at' => date('Y-m-d H:i:s'),
        );
    }
    
    $QStore->update($data, $where);
}

