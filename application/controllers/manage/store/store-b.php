<?php
$page            = $this->getRequest()->getParam('page', 1);
$name            = $this->getRequest()->getParam('name');
$address         = $this->getRequest()->getParam('address');
$area_id         = $this->getRequest()->getParam('area_id');
$staff_email     = $this->getRequest()->getParam('staff_email');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$d_id            = $this->getRequest()->getParam('d_id');
$partner_id      = $this->getRequest()->getParam('partner_id');
$sort            = $this->getRequest()->getParam('sort', 'created_at');

$have_pg         = $this->getRequest()->getParam('have_pg', 0);
$have_sales      = $this->getRequest()->getParam('have_sales', 0);
$no_staff        = $this->getRequest()->getParam('no_staff', 0);

$export          = $this->getRequest()->getParam('export', 0);
$desc            = $this->getRequest()->getParam('desc', 1);
//
$limit = LIMITATION;
$total = 0;
// PC::db($partner_id);
$params = array(
    'name'            => $name,
    'address'         => $address,
    'area_id'         => $area_id,
    'regional_market' => $regional_market,
    'district'        => $district,
    'd_id'            => $d_id,
    'partner_id'      => $partner_id,
    'staff_email'     => $staff_email,
    'have_pg'         => $have_pg,
    'no_staff'        => $no_staff,
    'have_sales'      => $have_sales,
    'export'          => $export,
);

$params['sort'] = $sort;
$params['desc'] = $desc;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id = $userStorage->group_id;

if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $params['asm'] = $userStorage->id;
} elseif ($group_id == SALES_ID) {
    $params['sales_store'] = $userStorage->id;
} elseif ($group_id == LEADER_ID) {
    $params['leader_province'] = $userStorage->id;
}

$QArea = new Application_Model_Area();
$this->view->areas = $QArea->fetchAll(null, 'name');

if ($area_id) {
    $QRegionalMarket = new Application_Model_RegionalMarket();
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');

    if ($regional_market) {
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
        $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    }
}

$QModel = new Application_Model_Store();
$stores = $QModel->fetchPagination($page, $limit, $total, $params);

if ($export && $export == 1) {
    $loader = new Zend_Loader_PluginLoader();
    $loader->addPrefixPath('Export_', 'My/Application/Export2CSV');
    $sales = $loader->load('Sales');
    Export_Sales::store_list($stores);
}

$QDistributor = new Application_Model_Distributor();
$this->view->distributor_cache = $QDistributor->get_cache();

$this->view->params = $params;
$this->view->sort = $sort;
$this->view->desc = $desc;
$this->view->stores = $stores;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST.'manage/store/'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset = $limit*($page-1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;

if($this->getRequest()->isXmlHttpRequest()) {
    $this->_helper->layout->disableLayout();

    $this->_helper->viewRenderer->setRender('store/partials/list');
} else
    $this->_helper->viewRenderer->setRender('store/index');


// ADD
// Search Distributor Using Ajax

if($this->getRequest()->isXmlHttpRequest())
{
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $SearchBox = $this->getRequest()->getParam('txt');


    $db     = Zend_Registry::get('db');

    $sql  = 'SELECT id,title FROM warehouse.distributor WHERE title LIKE "%'.$SearchBox.'%"';
    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll();
    $stmt->closeCursor();
    $db = $stmt = null;

    $this->view->dealer_ajax = $data;
    $this->render('store/partials/search-dealer-ajax');
}
