<?php
$id = $this->getRequest()->getParam('id');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id = $userStorage->id;
$QModel = new Application_Model_Store();
$data = array('del' => null);
$where = $QModel->getAdapter()->quoteInto('id = ?', $id);
$QModel->update($data, $where);
$QLogStore            = new Application_Model_LogStore();

$data_log = array(
                'user_id'   => $user_id,
                'staff_id'  => NULL,
                'store_id'  => $id,
                'is_leader' => null,
                'action'    => "UNDEL SHOP",
                'created_at' => date('Y-m-d H:i:s')
                );
$QLogStore->insert($data_log);
//remove cache
$cache = Zend_Registry::get('cache');
$cache->remove('store_cache');
$cache->remove('store_assigned_cache');

$this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOST.'manage/store');