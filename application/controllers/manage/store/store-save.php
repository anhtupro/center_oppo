<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$db = Zend_Registry::get('db');

if ($this->getRequest()->getMethod() == 'POST'){
    
    $db->beginTransaction();
    $QStaff         = new Application_Model_Staff();
    $QStore         = new Application_Model_Store();
    $QStoreStaff    = new Application_Model_StoreStaff();
    $QStoreStaffLog = new Application_Model_StoreStaffLog();
    $QLog           = new Application_Model_Log();
    $QRegion        = new Application_Model_RegionalMarket();
    $region_cache   = $QRegion->get_cache_all();

    $id               = $this->getRequest()->getParam('id');
    $name             = $this->getRequest()->getParam('name');
//    $store_id         = $this->getRequest()->getParam('store_id');
    $store_code       = $this->getRequest()->getParam('store_code');
    $company_name     = $this->getRequest()->getParam('company_name');
    $company_address  = $this->getRequest()->getParam('company_address');
    $shipping_address = $this->getRequest()->getParam('shipping_address');
    $phone_number     = $this->getRequest()->getParam('phone_number');
    $regional_market  = $this->getRequest()->getParam('regional_market');
    $district         = $this->getRequest()->getParam('district');
    $agency_name      = $this->getRequest()->getParam('agency_name');
    $contact_point    = $this->getRequest()->getParam('contact_point');
    $contact_phone    = $this->getRequest()->getParam('contact_phone');
    $contact_email    = $this->getRequest()->getParam('contact_email');
    $mst              = $this->getRequest()->getParam('mst');
    $rank             = $this->getRequest()->getParam('rank', 1);
    $d_id             = $this->getRequest()->getParam('d_id');
    $pic              = $this->getRequest()->getParam('pic');
    $pic1             = $this->getRequest()->getParam('pic1');
    $partner_id       = $this->getRequest()->getParam('partner_id', 0);
    $short_name       = $this->getRequest()->getParam('short_name');
    $latitude         = $this->getRequest()->getParam('latitude');
    $longitude        = $this->getRequest()->getParam('longitude');
    $is_brand_shop    = $this->getRequest()->getParam('is_brand_shop');

    $area    = $this->getRequest()->getParam('area_id');


    $this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QLogStore            = new Application_Model_LogStore();

    // add title store leader for brandshop - 16/04/2017 - by abcxyz

    $pic2             = $this->getRequest()->getParam('pic2');

    // add pg leader
    $pic3             = $this->getRequest()->getParam('pic3');
    


    // check dealer
    $QDistributor = new Application_Model_Distributor();
    $where = $QDistributor->getAdapter()->quoteInto('id = ?', intval($d_id));
    $distributor_check = $QDistributor->fetchRow($where);

    // echo "<pre>";print_r($distributor_check);die;

    // if (!$d_id || !intval($d_id) || !$distributor_check) {
    //     echo '<script>
    //             parent.palert("Dealer không tồn tại.");
    //             parent.alert("Dealer không tồn tại.");
    //         </script>';
    //     exit;
    // }

    // if ($partner_id) {
    //     $where = array();
    //     $where[] = $QStore->getAdapter()->quoteInto('partner_id = ?', intval($partner_id));

    //     if ($id)
    //         $where[] = $QStore->getAdapter()->quoteInto('id <> ?', intval($id));
    //     if ($d_id)
    //         $where[] = $QStore->getAdapter()->quoteInto('d_id = ?', intval($d_id));

    //     $store_check = $QStore->fetchRow($where);

    //     if ($store_check) {
    //         echo '<script>
    //                 parent.palert("Partner ID ứng với DEALER này đã tồn tại.");
    //                 parent.alert("Partner ID ứng với DEALER này đã tồn tại.");
    //             </script>';
    //         exit;
    //     }
    // }

    $data = array(
        'name'             => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $name)),
        'store_id'         => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $store_id)),
        'store_code'       => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $store_code)),
        'company_name'     => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $company_name)),
        'company_address'  => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $company_address)),
        'shipping_address' => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $shipping_address)),
        'phone_number'     => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $phone_number)),
        'regional_market'  => intval($regional_market),
        'district'         => intval($district),
        'agency_name'      => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $agency_name)),
        'contact_point'    => $contact_point,
        'contact_phone'    => $contact_phone,
        'contact_email'    => $contact_email,
        'mst'              => $mst,
        'rank'             => intval($rank),
        'd_id'             => intval($d_id),
        'partner_id'       => $partner_id,
        'short_name'       => trim(preg_replace(array('/\s{2,}/', '/[\t\n]+/'), ' ', $short_name)),
        'latitude'         => $latitude,
        'longitude'        => $longitude,
        'is_brand_shop'    => $is_brand_shop
    );



    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $ip = $this->getRequest()->getServer('REMOTE_ADDR');
    $info = 'STORE - ';

    // chỉ cho phép admin update thông tin store
    if (in_array($userStorage->group_id, array(ADMINISTRATOR_ID, SALES_EXT_ID))) {
        if ($id){
            $where = $QStore->getAdapter()->quoteInto('id = ?', $id);

            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = $userStorage->id;

            $QStore->update($data, $where);
            $info .= 'Update('.$id.') - Info ('.serialize($data).') ';

        } else {

            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $userStorage->id;

            $id = $QStore->insert($data);
            $info .= 'Insert('.$id.') - Info ('.serialize($data).') ';
        }
    }

    // echo $pic; $pic1 ; die;

    // Cắt chuỗi trả về, đưa ra mảng
    $list_staff        = $pic ? explode(',', $pic) : array();
    $list_leader       = $pic1 ? explode(',', $pic1) : array();
    $list_store_leader = $pic2 ? explode(',', $pic2) : array();
    $list_pg_leader    = $pic3 ? explode(',', $pic3) : array();

    // echo "<pre>";print_r($list_staff);die;

    // Đảm bảo dữ liệu là mảng các số, không có khoảng trắng dư
    $list_staff        = array_filter($list_staff);
    $list_leader       = array_filter($list_leader);
    $list_store_leader = array_filter($list_store_leader);
    $list_pg_leader    = array_filter($list_pg_leader);

    // echo "<pre>";print_r($list_pg_leader);die;
    
    foreach ($list_staff as $k => $s) {
        $list_staff[$k] = intval( trim( $s ) );
    }

    foreach ($list_leader as $k =>  $s) {
        $list_leader[$k] = intval( trim( $s ) );
    }

    foreach ($list_store_leader as $k =>  $s) {
        $list_store_leader[$k] = intval( trim( $s ) );
    }

    foreach ($list_pg_leader as $k =>  $s) {
        $list_pg_leader[$k] = intval( trim( $s ) );
    }

    // check mỗi store chỉ có 1 leader
    if (count( $list_leader ) > 1) {
        echo '<script>
                parent.palert("Mỗi store chỉ có thể có duy nhất 01 Sales.");
                parent.alert("Mỗi store chỉ có thể có duy nhất 01 Sales.");
            </script>';
        exit;
    }
    if (count( $list_store_leader ) > 1) {
        echo '<script>
                parent.palert("Mỗi store thuộc Brandshop chỉ có thể có duy nhất 01 Store Leader.");
                parent.alert("Mỗi store thuộc Brandshop chỉ có thể có duy nhất 01 Store Leader.");
            </script>';
        exit;
    }

    // check mỗi store chỉ có 1 pg
    if (count( $list_staff ) > 1 && ((isset($distributor_check['parent']) && $distributor_check['parent'] && $distributor_check['parent'] == 2316)
            || (isset($distributor_check['id']) && $distributor_check['id'] && $distributor_check['id'] == 2316)
            || (isset($distributor_check['title']) && preg_match('/^TGDĐ -/', $distributor_check['title']))
            || (isset($name) && preg_match('/^TGDĐ -/', $name))
    )) {
        echo '<script>
                parent.palert("Mỗi store thuộc chuỗi TGDĐ chỉ có tối đa 01 PG.");
                parent.alert("Mỗi store thuộc chuỗi TGDĐ chỉ có tối đa 01 PG.");
            </script>';
        exit;
    }


    if (count( $list_staff ) > 3 && ((isset($distributor_check['parent']) && $distributor_check['parent'] && $distributor_check['parent'] == 2317)
            || (isset($distributor_check['id']) && $distributor_check['id'] && $distributor_check['id'] == 2317)
            || (isset($distributor_check['title']) && preg_match('/^VTA -/', $distributor_check['title']))
            || (isset($name) && preg_match('/^VTA -/', $name))
    )) {
        echo '<script>
                parent.palert("Mỗi store thuộc chuỗi VTA chỉ có tối đa 03 PGs.");
                parent.alert("Mỗi store thuộc chuỗi VTA chỉ có tối đa 03 PGs.");
            </script>';
        exit;
    }


    // store leader 
    
    $where = array();
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at is null and is_leader = ?', 3);
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('store_id = ?', $id);
 

    $old = $QStoreStaffLog->fetchAll($where);
    $old_members = array();

    if ($old->count()) {
        foreach ($old as $item) {
            $old_members[] = $item->staff_id;
        }
    }

    
    $join_store_leader = array_diff($list_store_leader, $old_members);
    $join_store_leader = is_array($join_store_leader) ? array_filter($join_store_leader) : null;


    $release_store_leader = array_diff($old_members, $list_store_leader);
    $release_store_leader = is_array($release_store_leader) ? array_filter($release_store_leader) : null;


    // có phải store leader hay không ?
    foreach ($list_store_leader as $k => $staff_id ){
        $staff = $QStaff->find($staff_id);
        $staff = $staff->current();

        // echo "<pre>";print_r($staff);die;

        if (
//                $staff['group_id'] != STORE_LEADER_ID || 
                $staff['title'] != STORE_LEADER) {
            echo '<script>
                    parent.palert("Trong danh sách Store Leader, người thứ ['.($k+1).'] '
                        .$staff['firstname'] .' '.$staff['lastname'] .' | '
                        . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' không thuộc nhóm Store Leader.");

                    parent.alert("Trong danh sách Store Leader, người thứ ['.($k+1).'] '
                        .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' không thuộc nhóm Store Leader.");
                </script>';
            exit;
        }

        // if ($staff['regional_market'] != $regional_market) {

        //             echo '<script>
        //                     parent.palert("Trong danh sách Store Leader, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

        //                     parent.alert("Trong danh sách Store Leader, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
        //                 </script>';
        //             exit;
        // } 
    }

        $where = array();
        $where[] = $QStoreStaffLog->getAdapter()->quoteInto('is_leader = ?', 3);
        $where[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at IS NULL');
        $where[] = $QStoreStaffLog->getAdapter()->quoteInto('staff_id IN (?)',$staff_id);
        $where[] = $QStoreStaffLog->getAdapter()->quoteInto('store_id <> ?',$id);

        $check_store_leader = $QStoreStaffLog->fetchRow($where);
        if($check_store_leader) {
            echo '<script>
                    parent.palert("Trong danh sách Store Leader, người hiện tại đã được gán vị trí Store Leader nên không thể gán thêm được nữa.");
                    parent.alert("Trong danh sách Store Leader, người hiện tại đã được gán vị trí Store Leader nên không thể gán thêm được nữa.");
                </script>';
            exit;   
        }
    // end store leader
   
    //PGs leader 
    
    $where = array();
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at is null and is_leader = ?', 5);
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('store_id = ?', $id);

    $old = $QStoreStaffLog->fetchAll($where);
    $old_members = array();

    if ($old->count()) {
        foreach ($old as $item) {
            $old_members[] = $item->staff_id;
        }
    }

    $join_pg_leader = array_diff($list_pg_leader, $old_members);
    $join_pg_leader = is_array($join_pg_leader) ? array_filter($join_pg_leader) : null;


    $release_pg_leader = array_diff($old_members, $list_pg_leader);
    $release_pg_leader = is_array($release_pg_leader) ? array_filter($release_pg_leader) : null;

    // PG leader 27/07/2017
        
    // có phải pg leader hay không ?
    foreach ($list_pg_leader as $k => $staff_id ){
        $staff = $QStaff->find($staff_id);
        $staff = $staff->current();

        if ( $staff['title'] != PG_LEADER_TITLE) {
            echo '<script>
                    parent.palert("Trong danh sách PGs Leader, người thứ ['.($k+1).'] '
                        .$staff['firstname'] .' '.$staff['lastname'] .' | '
                        . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' không thuộc nhóm PGs Leader.");

                    parent.alert("Trong danh sách PGs Leader, người thứ ['.($k+1).'] '
                        .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' không thuộc nhóm PGs Leader.");
                </script>';
            exit;
        }

        // echo $regional_market; echo $staff['regional_market'];die;

        // if ($staff['regional_market'] != $regional_market) {

        //             echo '<script>
        //                     parent.palert("Trong danh sách PGs Leader, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

        //                     parent.alert("Trong danh sách PGs Leader, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
        //                 </script>';
        //             exit;
        // }
    }


    // end

    $where = array();
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at is null and is_leader = ?', 0);
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('store_id = ?', $id);

    $old = $QStoreStaffLog->fetchAll($where);
    $old_members = array();

    if ($old->count()) {
        foreach ($old as $item) {
            $old_members[] = $item->staff_id;
        }
    }

    // staff
    $join_staffs = array_diff($list_staff, $old_members);
    $join_staffs = is_array($join_staffs) ? array_filter($join_staffs) : null;

    $release_staffs = array_diff($old_members, $list_staff);
    $release_staffs = is_array($release_staffs) ? array_filter($release_staffs) : null;

    // leader
    $where = array();
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at is null and is_leader = ?', 1);
    $where[] = $QStoreStaffLog->getAdapter()->quoteInto('store_id = ?', $id);

    $old = $QStoreStaffLog->fetchAll($where);
    $old_members = array();

    if ($old->count()) {
        foreach ($old as $item) {
            $old_members[] = $item->staff_id;
        }
    }
    // echo "<pre>";print_r($list_leader);
    // echo "<pre>";print_r($old_members);die; 
    $join_leaders    = array_diff($list_leader, $old_members);
    // echo "<pre>";print_r($join_leaders);die; 
    
    $join_leaders    = is_array($join_leaders) ? array_filter($join_leaders) : null;
    

    $release_leaders = array_diff($old_members, $list_leader);
    $release_leaders = is_array($release_leaders) ? array_filter($release_leaders) : null;

    $today = date('Y-m-d');

    // check leader phải là sales
    foreach ($list_leader as $staff_id) {
        // check id staff có hợp lệ ko
            
        // echo $id;die;
        // echo "<pre>";print_r($staff_id);die;    

        $staff = $QStaff->find($staff_id);
        $staff = $staff->current();
        // $QStoreStaffLog = new Application_Model_StoreStaffLog();
        // $slg_sales =  $QStoreStaffLog->checkSales($staff_id);
        // $_staff = $QStoreStaffLog->get_staff_by_sales_leader($id);

        // $QStoreLeaderLog = new Application_Model_StoreLeaderLog ();
        // $_leader = $QStoreLeaderLog->get_staff_by_sales_leader($id);
        // echo $staff['title'];die;
        // echo "<pre>";print_r($_leader);die;    

        // echo $area;die;
        // echo $slg_sales['n'];die;

        // if (!$staff) {
        //     echo '<script>
        //             parent.palert("Trong danh sách Sales, người hiện tại không có trên hệ thống.");
        //             parent.alert("Trong danh sách Sales, người hiện tại không có trên hệ thống.");
        //         </script>';
        //     exit;
        // }

        


        // if ( ! in_array( $staff['title'], array( SALES_TITLE, LEADER_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, SALES_TRAINEE_TITLE) ) ) {
        //     echo '<script>
        //             parent.palert("Trong danh sách Sales, người hiện tại không thuộc nhóm Sales hoặc Leader.");
        //             parent.alert("Trong danh sách Sales, người hiện tại không thuộc nhóm Sales hoặc Leader.");
        //         </script>';
        //     exit;
        // }

        $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
        $where = array();
        $where = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null and store_id = ?', $id);
        $data = $QStoreLeaderLog->fetchRow($where);

        $staffLeader = $QStaff->find($data['staff_id']);
        $staffLeader = $staffLeader->current();
        // echo "<pre>";print_r($data);
        // echo $staff['title'];
        // echo $staff_id;
        // die;
        // if ($data &&  $staff['title'] == LEADER_TITLE  && ($staff_id != $data['staff_id'])) {
        //     echo '<script>
        //             parent.palert("Shop này đã được gán Sales Leader '.$staffLeader['firstname'] .' '.$staffLeader['lastname'] .' - Code : '.$staffLeader['code'].' đứng gián tiếp , không được gán Sales Leader khác đứng trực tiếp.");
        //             parent.alert("Shop này đã được gán Sales Leader '.$staffLeader['firstname'] .' '.$staffLeader['lastname'] .' - Code : '.$staffLeader['code'].' đứng gián tiếp , không được gán Sales Leader khác đứng trực tiếp.");
        //         </script>';
        //     exit;
        // }


        // if ($staff['regional_market'] != $regional_market) {

        //             echo '<script>
        //                     parent.palert("Trong danh sách Sales, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

        //                     parent.alert("Trong danh sách Sales, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
        //                 </script>';
        //             exit;
        // }
           

        // if ($staff['title'] == LEADER_TITLE && in_array( $staff_id, array( $_leader,$_staff ) ) ){
        //     echo '<script>
        //             parent.palert("Chính sách công ty mỗi Sales Leader chỉ được gán duy nhất tại 1 shop để đứng trực tiếp hoặc gián tiếp. Nếu đã được gán trực tiếp thì không thể gán gián tiếp và ngược lại.");
        //             parent.alert("Chính sách công ty mỗi Sales Leader chỉ được gán duy nhất tại 1 shop để đứng trực tiếp hoặc gián tiếp. Nếu đã được gán trực tiếp thì không thể gán gián tiếp và ngược lại.");
        //         </script>';
        //     exit;
        // }

        // if ($slg_sales['n'] >= 5 ){
        //     echo '<script>
        //             parent.palert("Chính sách công ty mỗi Sales Leader chỉ có thể đứng 5 shop trực tiếp , người hiện tại đang được gán hơn 5 shop trực tiếp.");
        //             parent.alert("Chính sách công ty mỗi Sales Leader chỉ có thể đứng 5 shop trực tiếp , người hiện tại đang được gán hơn 5 shop trực tiếp.");
        //         </script>';
        //     exit;
        // }

    } // end foreach - check sales

    // duyệt danh sách staff
    foreach ($list_staff as $k => $staff_id) {
        // check id staff có hợp lệ ko
        $staff = $QStaff->find($staff_id);
        $staff = $staff->current();

        $store = $QStore->find($id);
        $store = $store->current();

       

        // if ( $store['is_brand_shop'] == 1 ) {
        //     if ( $staff['title'] == PG_BRANDSHOP || $staff['title'] == SENIOR_PROMOTER_BRANDSHOP ){ 
        //     } else {
        //         echo '<script>
        //             parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                     .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                     . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' hiện không thuộc PGs Brandshop.");
        //             parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                     .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                     . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' hiện không thuộc PGs Brandshop.");
        //         </script>';
        //         exit;
        //     }
        // }
        

        // if (!$staff) {
        //     echo '<script>
        //             parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] hiện không có trên hệ thống.");
        //             parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] hiện không có trên hệ thống.");
        //         </script>';
        //     exit;
        // }

        // //check staff này có thuộc store nào khác hay không
        // $QStoreStaff = new Application_Model_StoreStaff();
        // $sql = "call sp_check_num_store_pg(?, ?)";
        // $result = $db->query($sql, array(intval($staff_id), intval($id)));
        // $result = $result->fetchAll();

        // if ( !($result && isset($result[0][0]) && $result[0][0] == 0 || SALES_EXT_ID == $userStorage->group_id) ) {
        //     $store_check = '';
        //     foreach ($result as $key => $_store)
        //         $store_check .= sprintf("<br />[%d] <a href='%smanage/store-edit?id=%d' target='_blank'>%s</a>, ", $_store['id'], HOST, $_store['id'], $_store['name']);

        //     $store_check = trim(trim($store_check), ',');

        //     if ($store_check) {
        //         echo '<script>
        //                 parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                     .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                     . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' đang thuộc <strong>'.count($result).'</strong> cửa hàng, bằng số cửa hàng tối đa cho phép.<br />Các cửa hàng hiện tại là: '.$store_check.'.");

        //                 parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                     .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' đang thuộc '.count($result).' cửa hàng, bằng số cửa hàng tối đa cho phép. Cuộn lên đầu trang để xem chi tiết.");
        //             </script>';
        //         exit;
        //     }
        // }

        // check staff phải là pg/pb
        // if ( $staff['title'] != 182 || $staff['title'] != 293) {
        //     echo '<script>
        //             parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                 .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                 . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' không thuộc nhóm PG/PB.");

        //             parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                 .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' không thuộc nhóm PG/PB.");
        //         </script>';
        //     exit;
        // }


        // check region
        // ngoại trừ HN với HCM
        $QRegion = new Application_Model_RegionalMarket();
        $where = $QRegion->getAdapter()->quoteInto('area_id IN (?)', array(HCMC1, HCMC2, HCMC3, HCMC4, HCMC5, HN1, HN2, HN3, HN4));
        $hcm_hn_regions = $QRegion->fetchAll($where);
        $hcm_hn_regions_arr = array();

        foreach ($hcm_hn_regions as $key => $value) {
            $hcm_hn_regions_arr[] = $value['id'];
        }

        // if ($staff['regional_market'] != $regional_market 
        //     AND (! (in_array($staff['regional_market'],array(4189,3433)) 
        //             AND in_array($regional_market,array(4189,3433)))) ) {

        //     if ( ! (in_array($regional_market, $hcm_hn_regions_arr) 
        //             && in_array($staff['regional_market'], $hcm_hn_regions_arr)) ) {

        //         // check ngoại lệ region
        //         $QCasual = new Application_Model_CasualWorker();
        //         $casuals = $QCasual->get_cache();


        //         if (isset( $casuals[ $staff['id'] ] )
        //             && $casuals[ $staff['id'] ]['status'] == 1
        //             && $casuals[ $staff['id'] ]['area_id'] == @$region_cache[ $regional_market ]['area_id']) {

        //         } else {

        //             echo '<script>
        //                     parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

        //                     parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
        //                 </script>';
        //             exit;
        //         }
        //     }
        // }
        
        // if ($staff['regional_market'] != $regional_market) {
        //     if (isset( $casuals[ $staff['id'] ] )
        //             && $casuals[ $staff['id'] ]['status'] == 1
        //             && $casuals[ $staff['id'] ]['area_id'] == @$region_cache[ $regional_market ]['area_id']) {

        //         } else {

        //             echo '<script>
        //                     parent.palert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '
        //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', $staff['email']) .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");

        //                     parent.alert("Trong danh sách PG/PB, người thứ ['.($k+1).'] '
        //                         .$staff['firstname'] .' '.$staff['lastname'] .' | '. $staff['email'] .' có hồ sơ ở tỉnh khác với tỉnh của Store. Vui lòng liên hệ Phòng Nhân sự nếu cần điều chỉnh.");
        //                 </script>';
        //             exit;
        //         }

                    
        // }
           

    } // end foreach - check list staff

    // add store leader 
    
    if ($join_store_leader) {
        foreach ($join_store_leader as $staff_id) {
            $data = array(
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 3,
                'joined_at' => time(),
                );

            // ghi log
            $QStoreStaffLog->insert($data);

            unset($data['joined_at']);

            $QStoreStaff->insert($data);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 3,
                'action'    => "ADD",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }
    }
   

    // if ($release_store_leader) {
    //     foreach ($release_store_leader as $staff_id) {
    //         $staff = $QStaff->find($staff_id);
    //         $staff = $staff->current();
    //         $id_leader = 3;   
            
    //         if (My_Staff::checkIfHaveTiming($staff_id, $today, $id)) {
    //             echo '<script>
    //                     parent.palert("Bạn vừa gỡ Store Leader '
    //                         .@$staff['firstname'] .' '.@$staff['lastname'] .' | '
    //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff['email']) .' ra khỏi shop. Store Leader này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo Store Leader kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), Store Leader có thể chấm công bình thường.");

    //                     parent.alert("Bạn vừa gỡ Store Leader '
    //                         .@$staff['firstname'] .' '.@$staff['lastname'] .' | '
    //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff['email']) .' ra khỏi shop. Store Leader này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo Store Leader kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), Store Leader có thể chấm công bình thường.");
    //                 </script>';
    //             exit;
    //         }





    //         $where   = array();
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?',3);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('released_at IS NULL', 1);

    //         $data = array(
    //             'released_at' => time(),
    //             );

    //         // ghi log
    //         $QStoreStaffLog->update($data, $where);
    //         $where   = array();
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?', 3);
    //         $QStoreStaff->delete($where);

    //         $data_log = array(
    //             'user_id'   => $this->userStorage->id,
    //             'staff_id'  => $staff_id,
    //             'store_id'  => $id,
    //             'is_leader' => 3,
    //             'action'    => "DELETE",
    //             'created_at' => date('Y-m-d H:i:s')
    //             );
    //         $QLogStore->insert($data_log);
    //     }
    // }

     // add PGs leader 
    
    if ($join_pg_leader) {
        foreach ($join_pg_leader as $staff_id) {
            $data = array(
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 5,
                'joined_at' => time(),
                );

            // ghi log
            $QStoreStaffLog->insert($data);

            unset($data['joined_at']);

            $QStoreStaff->insert($data);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 5,
                'action'    => "ADD",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }
    }
    //end 
    

    // remove pgs leader
    
    if ($release_pg_leader) {
        foreach ($release_pg_leader as $staff_id) {
            $staff = $QStaff->find($staff_id);
            $staff = $staff->current();
            $id_leader = 5;   
            
        

            $where   = array();
            $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
            $where[] = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?',5);
            $where[] = $QStoreStaff->getAdapter()->quoteInto('released_at IS NULL', 1);

            $data = array(
                'released_at' => time(),
                );

            // ghi log
            $QStoreStaffLog->update($data, $where);
            $where   = array();
            $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
            $where[] = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?', 5);
            $QStoreStaff->delete($where);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 5,
                'action'    => "DELETE",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }
    }

    //end


    if ($join_staffs) {
        foreach ($join_staffs as $staff_id) {
            $data = array(
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 0,
                'joined_at' => time(),
                );

            // ghi log
            $QStoreStaffLog->insert($data);

            unset($data['joined_at']);

            $QStoreStaff->insert($data);

            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 0,
                'action'    => "ADD",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }
    }

    // if ($release_staffs) {
    //     foreach ($release_staffs as $staff_id) {
    //         if (in_array($staff_id, $release_staffs) && My_Staff::checkIfHaveTiming($staff_id, $today, $id)) {
    //             $staff = $QStaff->find($staff_id);
    //             $staff = $staff->current();

    //             echo '<script>
    //                     parent.palert("Bạn vừa gỡ PG/PB '
    //                         .@$staff['firstname'] .' '.@$staff['lastname'] .' | '
    //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff['email']) .' ra khỏi shop. PG/PB này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo PG/PB kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), PG/PB có thể chấm công bình thường.");

    //                     parent.alert("Bạn vừa gỡ PG/PB '
    //                         .@$staff['firstname'] .' '.@$staff['lastname'] .' | '
    //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff['email']) .' ra khỏi shop. PG/PB này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo PG/PB kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), PG/PB có thể chấm công bình thường.");
    //                 </script>';
    //             exit;
    //         }

    //         $where = array();
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?', 0);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('released_at IS NULL', 1);

    //         $data = array(
    //             'released_at' => time(),
    //             );

    //         // ghi log
    //         $QStoreStaffLog->update($data, $where);

    //         $where = array();
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?', 0);

    //         $QStoreStaff->delete($where);

    //         $data_log = array(
    //             'user_id'   => $this->userStorage->id,
    //             'staff_id'  => $staff_id,
    //             'store_id'  => $id,
    //             'is_leader' => 0,
    //             'action'    => "DELETE",
    //             'created_at' => date('Y-m-d H:i:s')
    //             );
    //         $QLogStore->insert($data_log);
    //     }
    // }

    // echo "<pre>";print_r($join_leaders);die;
    if ($join_leaders) {
        foreach ($join_leaders as $staff_id) {
            $staff = $QStaff->find($staff_id);
            $staff = $staff->current();

            $id_leader = 1;   
            

            $data = array(
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => $id_leader,
                'joined_at' => time(),
            );
            // echo "<pre>";print_r($data);die;
            // ghi log
            $QStoreStaffLog->insert($data);
            unset($data['joined_at']);
            $QStoreStaff->insert($data);
            $data_log = array(
                'user_id'   => $this->userStorage->id,
                'staff_id'  => $staff_id,
                'store_id'  => $id,
                'is_leader' => 1,
                'action'    => "ADD",
                'created_at' => date('Y-m-d H:i:s')
                );
            $QLogStore->insert($data_log);
        }
    }

    // if ($release_leaders) {
    //     foreach ($release_leaders as $staff_id) {
    //         $staff = $QStaff->find($staff_id);
    //         $staff = $staff->current();
    //         $id_leader = 1;   
    //         if($staff->title == PB_SALES_TITLE){
    //             $id_leader = 2;    
    //         }

    //         if (My_Staff::checkIfHaveTiming($staff_id, $today, $id)) {
    //             echo '<script>
    //                     parent.palert("Bạn vừa gỡ Sale '
    //                         .@$staff['firstname'] .' '.@$staff['lastname'] .' | '
    //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff['email']) .' ra khỏi shop. Sale này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo Sale kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), Sale có thể chấm công bình thường.");

    //                     parent.alert("Bạn vừa gỡ Sale '
    //                         .@$staff['firstname'] .' '.@$staff['lastname'] .' | '
    //                         . preg_replace('/'.EMAIL_SUFFIX.'/', '', @$staff['email']) .' ra khỏi shop. Sale này đã chấm công tại shop vào ngày hôm nay. Việc gỡ shop cùng ngày đã có chấm công là không hợp lệ. Trước khi gỡ shop, bạn vui lòng báo Sale kiểm tra lại, điều chỉnh/xóa ngày chấm công hôm nay, đồng thời dời các IMEI đã báo sang ngày hôm qua. Sau khi gán lại shop mới (nếu có), Sale có thể chấm công bình thường.");
    //                 </script>';
    //             exit;
    //         }

    //         $where   = array();
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('store_id = ?', $id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('is_leader = ?', 1);
    //         $where[] = $QStoreStaff->getAdapter()->quoteInto('released_at IS NULL', 1);


    //         $data = array(
    //             'released_at' => time(),
    //             );

    //         // ghi log
    //         $QStoreStaffLog->update($data, $where);
            
    //         $data_log = array(
    //             'user_id'   => $this->userStorage->id,
    //             'staff_id'  => $staff_id,
    //             'store_id'  => $id,
    //             'is_leader' => 1,
    //             'action'    => "DELETE",
    //             'created_at' => date('Y-m-d H:i:s')
    //             );
    //         $QLogStore->insert($data_log);
    //     }
    // }




    if ($pic1) {
        $info .= ' - Sales('.$pic1.')';
    }

    if ($pic) {
        $info .= ' - PG('.$pic.')';
    }

    if ($pic2) {
        $info .= ' - Store Leader('.$pic2.')';
    }

    if ($pic3) {
        $info .= ' - PGs Leader('.$pic3.')';
    }

    //todo log
    $QLog->insert( array (
        'info'       => $info,
        'user_id'    => $userStorage->id,
        'ip_address' => $ip,
        'time'       => date('Y-m-d H:i:s'),
    ) );

    //remove cache
    $cache = Zend_Registry::get('cache');
    $cache->remove('store_cache');
    $cache->remove('store_staff_log_cache');
    $cache->remove('store_assigned_cache');

    $db->commit();

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}

$back_url = $this->getRequest()->getParam('back_url');

echo '<script>parent.location.href="'. ( !empty( $back_url ) ? ($back_url) : (HOST.'manage/store') ).'"</script>';