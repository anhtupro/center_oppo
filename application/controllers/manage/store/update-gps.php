<?php
$id = $this->getRequest()->getParam('id');

if ($id) {

    $QModel = new Application_Model_Store();
    $QLogModel = new Application_Model_LogStoreGps();

    $rowset = $QModel->find($id);

    $store = $rowset->current();
    $dataStore = $store->toArray();
    $this->view->store = $store;

    if($this->getRequest()->isPost()){
        $latitude = $this->getRequest()->getPost('latitude');
        $longitude = $this->getRequest()->getPost('longitude');
        if(!empty($latitude) && !empty($longitude)){
            
            $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
            $arrOld = array(
                'latitude' => $dataStore['latitude'],
                'longitude' => $dataStore['longitude'],
            );
            
            $data_update = array(
                'latitude' => trim($latitude),
                'longitude' => trim($longitude)
            );
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QModel->update($data_update, $where);
            $dataLog = array(
                'old' => json_encode($arrOld),
                'new' => json_encode($data_update),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id
            );
            $QLogModel->insert($dataLog);
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('Done!');
            $this->_redirect(HOST.'manage/store');
        }
    }
}
$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$this->view->group_id = $userStorage->group_id;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$this->_helper->viewRenderer->setRender('store/update-gps');