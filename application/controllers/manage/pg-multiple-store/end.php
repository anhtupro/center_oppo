<?php
try {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setRender('pg-multiple-store/end');
    $db = Zend_Registry::get('db');

    $id = $this->getRequest()->getParam('id');

    if (!$id) throw new Exception("Invalid ID");

    $id = intval($id);
    $QPgMultipleStore = new Application_Model_PgMultipleStore();
    $where = $QPgMultipleStore->getAdapter()->quoteInto('id = ?', $id);
    $pg = $QPgMultipleStore->fetchRow($where);

    if (!$pg) throw new Exception("No data");

    $db->beginTransaction();

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    if (!$userStorage || !isset($userStorage->id) || !$userStorage->id)
        throw new Exception("Invalid user");

    $data = array(
        'removed_at' => date('Y-m-d H:i:s'),
        'removed_by' => $userStorage->id,
        'status' => 0
    );

    $QPgMultipleStore->update($data, $where);
    $this->view->result = 'success';
    $db->commit();
} catch (Exception $e) {
    $this->view->error = $e->getMessage();
    $db->rollback();
}
