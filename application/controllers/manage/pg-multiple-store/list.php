<?php 
$QPg = new Application_Model_PgMultipleStore();
$page = $this->getRequest()->getParam('page', 1);
$email = $this->getRequest()->getParam('email');
$limit = LIMITATION;
$total = 0;
$params = array('email' => $email);

$QPgMultipleStore = new Application_Model_PgMultipleStore();
$this->view->list = $QPgMultipleStore->fetchPagination($page, $limit, $total, $params);
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->page   = $page;
$this->view->offset = $limit*($page-1);
$this->view->params = $params;
$this->view->url    = HOST.'manage/pg-multiple-store'.( $params ? '?'.http_build_query($params).'&' : '?' );

if ($page * $limit > ceil($total/$limit)*$limit) {
    $page = ceil($total/$limit);
    $this->_redirect(HOST.'manage/pg-multiple-store?page='.$page.( $params ? '&'.http_build_query($params).'&' : '&' ));
}

$this->_helper->viewRenderer->setRender('pg-multiple-store/list');
