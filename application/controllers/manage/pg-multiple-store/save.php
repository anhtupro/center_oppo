<?php
try {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setRender('pg-multiple-store/save');
    $db = Zend_Registry::get('db');
    $db->beginTransaction();

    $pg = $this->getRequest()->getParam('pg');
    $number_of_store = $this->getRequest()->getParam('number_of_store');

    if(!$pg) throw new Exception("Please choose a PG");
    if (!$number_of_store || !is_numeric($number_of_store)) throw new Exception("Number of store much larger than 0 (zero)");

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    if (!$userStorage || !isset($userStorage->id)) throw new Exception("Invalid user");

    $pg = intval($pg);
    $number_of_store = intval($number_of_store);

    $QStaff = new Application_Model_Staff();
    $where = $QStaff->getAdapter()->quoteInto('id = ?', $pg);
    $staff_check = $QStaff->fetchRow($where);

    if ( !$staff_check || !in_array($staff_check['title'], My_Staff_Title::getPg()) )
        throw new Exception("This is not PG");

    $QPg = new Application_Model_PgMultipleStore();
    $where = array();
    $where[] = $QPg->getAdapter()->quoteInto('staff_id = ?', $pg);
    $where[] = $QPg->getAdapter()->quoteInto('removed_at IS NULL OR removed_at = 0', 1);
    $date = date('Y-m-d H:i:s');
    $data = array(
        'removed_at' => $date,
        'removed_by' => $userStorage->id,
        'status' => 0,
    );

    $QPg->update($data, $where);

    $data = array(
        'staff_id'        => $pg,
        'number_of_store' => $number_of_store,
        'approved_from'   => $date,
        'approved_by'     => $userStorage->id,
        'status'          => 1,
    );
    $QPg->insert($data);

    $this->view->result = 'success';
    $db->commit();
} catch (Exception $e) {
    $this->view->error = $e->getMessage();
    $db->rollback();
}
