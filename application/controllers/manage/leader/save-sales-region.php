<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$params = $this->_request->getParams();

$data = array(
	'id' => $params['id'],
	'parent' => $params['parent'],
	'shared' => $params['shared']
);

$sr = new Application_Model_SalesRegion();
$srl = new Application_Model_SalesRegionLog();
$res = $sr->_update($data);
if($res)
	$srl->_insert(array(
		'id' => $data['id'],
		'staff_id' => $userStorage->id,
		'action' => 'update',
		'date' => date('Y-m-d H:i:s')
	));

exit(json_encode(array('status' => $res ? true : false)));