<?php 
$this->_helper->layout()->disablelayout(true);
$this->_helper->viewRenderer->setRender('leader/leader-region-save');
if($this->getRequest()->isPost()){
	$from_date       = $this->getRequest()->getParam('from_date');
	$to_date         = $this->getRequest()->getParam('to_date');
	$current_staff   = $this->getRequest()->getParam('current_staff');
	$new_staff       = $this->getRequest()->getParam('new_staff_id');
	$remove          = $this->getRequest()->getParam('remove',0);
	$sales_region_id = $this->getRequest()->getParam('sales_region_id');
	$back_url        = $this->getRequest()->getParam('back_url','/manage/leader-region');

	$params = array(
		'from_date'       => $from_date,
		'to_date'         => $to_date,
		'current_staff'   => $current_staff,
		'new_staff'       => $new_staff,
		'remove'          => $remove,
		'sales_region_id' => $sales_region_id
	);

	$QSalesRegionStaffLog = new Application_Model_SalesRegionStaffLog();
	$db = Zend_Registry::get('db');
	$db->beginTransaction();
	try {

		$result = $QSalesRegionStaffLog->save($params);
		if($result['code'] === 0){
			$db->commit();
            echo '<script>window.parent.document.getElementById("palert").innerHTML = "<div class=\'alert alert-success\'>Success!</div>";</script>';
            echo '<script>setTimeout(function(){parent.location.href="' . $back_url .'"}, 1000)</script>';
            exit;
		}else{
			throw new Exception($result['message']);
		}
		
	} catch (Exception $e) {
		$db->rollBack();
        echo '<script>window.parent.document.getElementById("palert").innerHTML = "<div class=\'alert alert-error\'>Failed - '.$e->getMessage().'</div>";</script>';
        exit;
		
	}
	
	exit;
}