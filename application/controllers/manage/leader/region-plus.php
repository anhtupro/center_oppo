<?php 
	
$sr = new Application_Model_SalesRegion();
$this->view->sr = $sr->findPrimaykey($this->getParam('sales_region_id'));

if($this->_request->isPost()){
   
	$params = $this->_request->getParams();
	$params['type'] = !empty($params['type']) ? $params['type'] : 1;
	$sr = new Application_Model_SalesRegion();

	if($this->_request->isXmlHttpRequest()){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$res = $sr->GetTotalShared(array('parent' => intval($params['parent'])));
		echo json_encode($res);
		exit;
	}

	if(
		!$params['name'] ||
		!$params['parent'] ||
		!$params['type'] ||
		!$params['from_date']
	)
		exit('error');

	if(!$params['shared']) $params['shared'] = 0;
	
	$params['from_date'] = DateTime::createFromFormat('d/m/Y', trim($params['from_date']));
    $params['from_date'] = $params['from_date'] ? $params['from_date']->format('Y-m-d 00:00:00') : null;
   	// echo "<pre>";print_r($params);die;
	$res = $sr->_insert($params);
	 	 
	if(!$res){
		$this->view->error = "Shared is greater than limit!";
		$this->view->data = $params;
	}
	else{
		$srs = new Application_Model_SalesRegionStaffLog();
		$datai['sales_region_id'] = $res;
		$datai['from_date'] =  $params['from_date'];
		$datai['created_at'] = date('Y-m-d H:i:s');
		$datai['staff_id'] = $this->_request->getParam('s_result');
		$datai['type'] = $this->_request->getParam('is_asm');
		$datai['type'] = !empty($datai['type']) ? $datai['type'] : 0;
		$datai['created_by'] = Zend_Auth::getInstance()->getStorage()->read()->id;
		$re = $srs->insert($datai);
		if ($re){
//			$fm = $this->_helper->FlashMessenger();
//			$fm->setNamespace('success')->addMessage('Success');
			$this->_redirect('/manage/leader-region');
		}
		else{
			$res = $sr->_delete(array('id' => $params['sales_region_id']));
		}
	}
}

$area = new Application_Model_Area();
$this->view->area = $area->GetAll();
$this->view->copy = true;
$this->_helper->viewRenderer->setRender('leader/leader-region-plus');