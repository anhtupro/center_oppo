<?php 

	$sales_region_id = $this->getRequest()->getParam('sales_region_id');
	$flashMessenger  = $this->_helper->flashMessenger;
	$back_url        = $this->getRequest()->getParam('back_url','/manage/leader-region');
	$db              = Zend_Registry::get('db');
	$cols = array(
		'sales_region_name' => 'a.name',
		'staff_name'        => 'CONCAT(c.firstname," ",c.lastname)',
		'staff_id'          => 'c.id',
		'sales_region_id'   => 'a.id',
		'b.to_date',
		'parent'			=> 'a.parent',
		'shared'			=> 'a.shared',
		'to_date2'			=> 'a.to_date'
	);

	$select = $db->select()
		->from(array('a'=>'sales_region'),$cols)
		->joinLeft(array('b'=>'sales_region_staff_log'),'a.id = b.sales_region_id',array())
		->joinLeft(array('c'=>'staff'),'c.id = b.staff_id')
		->where('a.id = ?',$sales_region_id)
		->order('b.from_date DESC')
		->limit(1)
		;
	$result = $db->fetchRow($select);
	$result['to_date2'] = $result['to_date2'] ? date('d/m/Y', strtotime($result['to_date2'])) : null;

	if(!$result){
		$flashMessenger->setNameSpace('error')->addMessage('Sales region not exist');;
		$this->redirect($back_url);
	}

	if($result){
		$this->view->result = $result;

		$cols_2 = array(
			'sales_region_name' => 'a.name',
			'staff_name'        => 'CONCAT(c.firstname," ",c.lastname)',
			'staff_id'          => 'c.id',
			'sales_region_id'   => 'a.id',
			'from_date'			=> 'b.from_date',
			'to_date'			=> 'b.to_date',
		);

		$select_log = $db->select()
			->from(array('a'=>'sales_region'),$cols_2)
			->joinLeft(array('b'=>'sales_region_staff_log'),'a.id = b.sales_region_id',array())
			->joinLeft(array('c'=>'staff'),'c.id = b.staff_id')
			->where('a.id = ?',$sales_region_id)
			->order('b.from_date DESC')
		;	

		$logs = $db->fetchAll($select_log);
		$this->view->logs = $logs;
	}
	$this->view->back_url = $back_url;
	$this->_helper->viewRenderer->setRender('leader/leader-region-edit');
?>