<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$params = $this->_request->getParams();

$sr = new Application_Model_SalesRegion();
$srl = new Application_Model_SalesRegionLog();
$srl->_insert(array(
	'id' => $params['sales_region_id'],
	'staff_id' => $userStorage->id,
	'action' => 'delete',
	'date' => date('Y-m-d H:i:s')
));
$res = $sr->_delete(array('id' => $params['sales_region_id']));

$fm = $this->_helper->FlashMessenger();
if($res) $fm->setNamespace('success')->addMessage('Success');
else $fm->setNamespace('error')->addMessage('Error');

$this->_redirect('/manage/leader-region');