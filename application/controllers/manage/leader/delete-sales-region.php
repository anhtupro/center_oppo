<?php 

if($this->_request->isPost()){
	$flashMessenger  = $this->_helper->flashMessenger;
	$back_url        = $this->getRequest()->getParam('back_url','/manage/leader-region');
	$params = array_merge(
		array(
			'id_sales_region' => null,
			'edit_to_date'    => null
		),
		$this->_request->getParams()
	);
	$QSalesRegionStaffLog = new Application_Model_SalesRegionStaffLog();
	$re = $QSalesRegionStaffLog->findPrimaykey($params['id_sales_region']);
	$from_date = strtotime($re->from_date);
	$toDate                          = DateTime::createFromFormat('d/m/Y', $params['edit_to_date']);
    $toDate                          = $toDate ? $toDate->format('Y-m-d 00:00:00') : null;
    $to_date = strtotime($toDate);
	if($to_date < $from_date)
	{
		$flashMessenger->setNameSpace('error')->addMessage('Error, To date can not greater than From date');
		$this->redirect($back_url);
	}
	else
	{
		$re->to_date = $toDate;
		$re->save();
		$flashMessenger->setNameSpace('success')->addMessage('Success, Removed ');
		$this->redirect($back_url);
	}
		
		
}