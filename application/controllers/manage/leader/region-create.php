<?php

if($this->_request->isPost()){
	$params = $this->_request->getParams();
	$params['type'] = !empty($params['type']) ? $params['type'] : 1;
	$sr = new Application_Model_SalesRegion();

	if($this->_request->isXmlHttpRequest()){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$parent = $this->_request->getParam('parent');
		$res = $sr->GetTotalShared(array('parent' => intval($parent)));
		echo json_encode($res);
		exit;
	}

	if(
		!$params['name'] ||
		!$params['parent'] ||
		!$params['shared'] ||
		// !$params['type'] ||
		!$params['from_date']
	)
		exit('error');

	$params['from_date'] = DateTime::createFromFormat('d/m/Y', $params['from_date'])->format('Y-m-d');
	$params['to_date'] = !empty($params['to_date']) ? DateTime::createFromFormat('d/m/Y', $params['to_date'])->format('Y-m-d') : null;

	$res = $sr->_insert($params);
	if(!$res){
		$this->view->error = "Shared is greater than limit!";
		$params['from_date'] = DateTime::createFromFormat('Y-m-d', $params['from_date'])->format('d/m/Y');
		$params['to_date'] = !empty($params['to_date']) ? DateTime::createFromFormat('Y-m-d', $params['from_date'])->format('d/m/Y') : null;
		$this->view->data = $params;
	}
	else{
		$fm = $this->_helper->FlashMessenger();
		$fm->setNamespace('success')->addMessage('Success');
		$this->_redirect('/manage/leader-region-edit?sales_region_id='.$res);
	}
}

$area = new Application_Model_Area();
$this->view->area = $area->GetAll();
$this->_helper->viewRenderer->setRender('leader/leader-region-create');