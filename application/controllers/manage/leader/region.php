<?php
/**
 * @author buu.pham
 * @create 2015-09-30T16:51:33+07:00
 * @filename region.php
 */
$page   = $this->getRequest()->getParam('page', 1);
$limit  = 100;
$total  = 0;
$params = array();


$fm = $this->_helper->flashMessenger;

$QSalesRegionStaffLog = new Application_Model_SalesRegionStaffLog();

$this->view->staffs   = $QSalesRegionStaffLog->fetchPagination($page, $limit, $total, $params);
// echo '<pre>';print_r($this->view->staffs);die;
$this->view->limit    = $limit;
$this->view->total    = $total;
$this->view->page     = $page;
$this->view->offset   = $limit*($page-1);
$this->view->params   = $params;
$this->view->url      = HOST.'manage/leader-region'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->message  = $fm;
$this->_helper->viewRenderer->setRender('leader/region');