<?php
$id = $this->getRequest()->getParam('id');

if ($id) {
    $QModel = new Application_Model_Area();
    $rowset = $QModel->find($id);

    $area = $rowset->current();
    $this->view->area = $area;
    $QStaff = new Application_Model_Staff();


    if ($area->asm_id) {
        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $area->asm_id);
        $this->view->asms = $QStaff->fetchAll($where);
    }
    if ($area->rsm_id) {

        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $area->rsm_id);
        $this->view->rsms = $QStaff->fetchAll($where);

    }



}
//List region
$QRegion= new Application_Model_Region();


$region=$QRegion->get_cache();

$this->view->region=$region;
$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$this->_helper->viewRenderer->setRender('area/create');