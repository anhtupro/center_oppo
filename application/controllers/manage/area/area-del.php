<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$id = $this->getRequest()->getParam('id');

$QModel = new Application_Model_Area();
//$al = new Application_Model_AreaLog();
//$al->_insert(array(
//    'id' => $id,
//    'staff_id' => $userStorage->id,
//    'action' => 'delete',
//    'date' => date('Y-m-d H:i:s')
//));

$where = $QModel->getAdapter()->quoteInto('id = ?', $id);
$QModel->delete($where);

//remove cache
$cache = Zend_Registry::get('cache');
$cache->remove('area_cache');
$cache->remove('area_index_cache');
$cache->remove('area_cache2');
$cache->remove('area_ASM_cache');
$cache->remove('regional_market_HCM_cache');
$cache->remove('asm_cache');

$this->_redirect('/manage/area');