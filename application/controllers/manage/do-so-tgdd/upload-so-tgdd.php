<?php
$upload   = $this->getRequest()->getParam('upload');
$flashMessenger = $this->_helper->flashMessenger;
$random_do_so = date("YmdHis");
$QKpiByModel = new Application_Model_KpiByModel();
$QKpiByModelFile = new Application_Model_KpiByModelFile();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$this->view->staff_id = $userStorage->id;

include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
	$db   = Zend_Registry::get('db');
        

if(!empty($upload)){
	if($_FILES['file_tgdd']['name'] != NULL) {
		try {
//            $db->beginTransaction();

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
							DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'tgdd-do-so';

			if (!is_dir($uploaded_dir))
			@mkdir($uploaded_dir, 0777, true);
			$tmpFilePath = $_FILES['file_tgdd']['tmp_name'];

			if ($tmpFilePath != ""){
				$old_name 	= $_FILES['file_tgdd']['name'];
				$tExplode 	= explode('.', $old_name);
				$extension  = end($tExplode);
				$new_name = 'TGDD-' . md5(uniqid('', true)) . '.' . $extension;
				$newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
				if(move_uploaded_file($tmpFilePath, $newFilePath)) {
					$url= 'tgdd-do-so' . DIRECTORY_SEPARATOR . $new_name;
				}else{
					$url = NULL;
				}
			}else{
				$url = NULL;
			}
			//đọc file excel
			$inputfile = $url;
			
			$xlsx = new SimpleXLSX($inputfile);
			$data = $xlsx->rows();
			foreach ($data as $key => $value) {
				$key_ = $key + 1;
				if($data[$key_][0] != "" || $data[$key_][1] != "" || $data[$key_][5] != "" || $data[$key_][7] != "" || $data[$key_][9] != "") {
					$tgdd = [
						'timing_date' => trim($data[$key_][0]),
						'partner_id' => trim($data[$key_][1]),
						'model' => trim($data[$key_][5]),
						'color_name' => trim($data[$key_][7]),
						'qty' => trim($data[$key_][9]),
						'random_do_so' => $random_do_so,
					];
					$QKpiByModelFile->insert($tgdd);
				} else {
					$check = $key_ - 1;
					if($check != $key) {
						$db->rollBack();
						$flashMessenger->setNamespace('error')->addMessage("File TGDD lỗi !!!");
						$this->redirect(HOST . 'manage/do-so-tgdd');
					}
				}
			}
			//unlink($url);

			
			$QKpiByModelFile->updateBasic($random_do_so);
                        
			$QKpiByModelFile->updateGoodId($random_do_so);


            $checkColor = $QKpiByModelFile->checkColor($random_do_so);

            if(empty($checkColor)) {
				$QKpiByModelFile->updateColorId($random_do_so);
                                
			} else {

                $getColor = array();
				foreach($checkColor as $color) {
					array_push($getColor, $color['desc']);
				}
				$setColor = implode(', ', $getColor);
      
//                $QKpiByModelFile->deleteModelFile($random_do_so);


                $flashMessenger->setNamespace('error')->addMessage( "Màu model: " . $setColor . " đang bị sai,  check lại file excel !!!");
				$this->redirect(HOST . 'manage/do-so-tgdd');
			}


            $db->beginTransaction();


            $QKpiByModelFile->insertData($random_do_so);

            $data_date = $QKpiByModelFile->getMinMaxDate($random_do_so);

            $QKpiByModelFile->callKpiByModelStaff(date('Y-m-d', strtotime($data_date['min_date'])) , date('Y-m-d', strtotime($data_date['max_date'])));
            
            $db->commit();


			$flashMessenger->setNamespace('success')->addMessage( "Import thành công !!!");
			$this->redirect(HOST . 'manage/do-so-tgdd');
		} catch (Exception $e) {
			$db->rollBack();
			$flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
			$this->redirect(HOST . 'manage/do-so-tgdd');
		}
	} else {
		$flashMessenger->setNamespace('error')->addMessage( "File TGDD không tồn tại !!!");
		$this->redirect(HOST . 'manage/do-so-tgdd');
	}
} else {
	$flashMessenger->setNamespace('error')->addMessage( "File TGDD không tồn tại !!!");
	$this->redirect(HOST . 'manage/do-so-tgdd');
}
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
		$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
		$this->view->messages_error = $messages_error;

	}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
	$messages          = $flashMessenger->setNamespace('success')->getMessages();
	$this->view->messages = $messages;
}