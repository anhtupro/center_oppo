<?php
$flashMessenger               = $this->_helper->flashMessenger;
$messages                     = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages;

$this->_helper->viewRenderer->setRender('do-so-tgdd/index');