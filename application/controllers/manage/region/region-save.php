<?php

if ($this->getRequest()->getMethod() == 'POST'){

    $QModel = new Application_Model_Region();

    $id = $this->getRequest()->getParam('id');
    $asm_id=$this->getRequest()->getParam('pic_name');
    $rsm_id=$this->getRequest()->getParam('pic_name_rsm');
    $name = $this->getRequest()->getParam('name');

    $data = array(
        'name' => $name,
        'id'=>$id,
        'asm_id'=>$asm_id,
        'rsm_id'=>$rsm_id
    );

    if ($id){
        $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
        $QModel->update($data, $where);

    } else {
        $QModel->insert($data);
    }




    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}
// if not use $back_url must create file region-save.phtml in views

$back_url = $this->getRequest()->getParam('back_url');

$this->_redirect( ( $back_url ? $back_url : HOST.'manage/region' ) );