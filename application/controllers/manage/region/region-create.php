<?php
$id = $this->getRequest()->getParam('id');


//Tồn tại region
if ($id) {

    $QModel = new Application_Model_Region();
    $religionRowset = $QModel->find($id);
    $region = $religionRowset->current();

    $this->view->region = $region;

    $QStaff= new Application_Model_Staff();

    if($region->asm_id)
    {
        // select * from staff where id=idASM
        $where =$QStaff->getAdapter()->quoteInto('id IN (?)',$region->asm_id);
        $this->view->asm=$QStaff->fetchAll($where);
    }
    if($region->rsm_id)
    {
        // select * from staff where id=idRSM
        $where=$QStaff->getAdapter()->quoteInto('id IN (?)',$region->rsm_id);
        $this->view->rsm=$QStaff->fetchAll($where);
    }


}
$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
//send data to view
$this->_helper->viewRenderer->setRender('region/create');