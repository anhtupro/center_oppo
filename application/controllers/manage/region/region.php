<?php
$page = $this->getRequest()->getParam('page', 1);
$params = array();
$limit = LIMITATION;
$total = 0;
$params = array();

$QModel = new Application_Model_Region();
$regions = $QModel->fetchPagnation($page, $limit, $total, $params);

$this->view->regions = $regions;

$this->view->limit = $limit;
$this->view->total = $total;


$this->view->url = HOST.'manage/region/'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset = $limit*($page-1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->_helper->viewRenderer->setRender('region/index');