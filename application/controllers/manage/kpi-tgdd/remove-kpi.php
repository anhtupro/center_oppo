<?php 
$id		= $this->getRequest()->getParam('id');
$pg_id		= $this->getRequest()->getParam('pg_id');
$userStorage	= Zend_Auth::getInstance()->getStorage()->read();
$user_id        = $userStorage->id;
$QKpiByStaff = new Application_Model_KpiByStaff();
$QLogKpiByStaff = new Application_Model_LogKpiByStaff();
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
if(!empty($id) && !empty($pg_id)){
   
    $where      = array();
    $where[]    = $QKpiByStaff->getAdapter()->quoteInto('kpi_by_model_id = ?', $id);
    $where[]    = $QKpiByStaff->getAdapter()->quoteInto('pg_id = ?', $pg_id);
    
    $row        = $QKpiByStaff->fetchRow($where);
    
    if($row){
        $result = $row->toArray();
        $data_log = array(
            'kpi_by_model_id' => $id,
            'pg_id'           => $pg_id,
            'kpi_pg'          => 0,
            'qty'             => $result['qty'],
            'created_at'      => date("Y-m-d h:i:s"),
            'created_by'      => $userStorage->id,
            'note'            => 'Delete',
        );
        $QKpiByStaff->delete($where);
        $QLogKpiByStaff->insert($data_log);
    }
    
    
    
}

// $this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOST.'manage/kpi-tgdd');