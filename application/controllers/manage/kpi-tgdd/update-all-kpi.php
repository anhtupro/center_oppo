<?php
$QKpiByModel = new Application_Model_KpiByModel();
$data_kpi_by_model    = $this->getRequest()->getParam('data_kpi_by_model', null);
$status               = $this->getRequest()->getParam('status');
$product              = $this->getRequest()->getParam('product');
$color                = $this->getRequest()->getParam('model');
$store                = $this->getRequest()->getParam('store_id');
$from                 = $this->getRequest()->getParam('from', date('01/m/Y'));
$to                   = $this->getRequest()->getParam('to', date('d/m/Y'));
$imei                 = $this->getRequest()->getParam('imei', null);
$area_id              = $this->getRequest()->getParam('area_id');
$regional_market      = $this->getRequest()->getParam('regional_market');
$district             = $this->getRequest()->getParam('district');
$sales_team           = $this->getRequest()->getParam('sales_team');
$staff                = $this->getRequest()->getParam('staff_id', null);
$shop_type            = $this->getRequest()->getParam('shop_type');
$kpi                  = $this->getRequest()->getParam('kpi');
$export               = $this->getRequest()->getParam('export');
$status_imei          = $this->getRequest()->getParam('status_imei');
$page                 = $this->getRequest()->getParam('page');
$imei_sn              = $this->getRequest()->getParam('imei_sn');
$pg_id                = $this->getRequest()->getParam('pg_id');
$data_kpi_by_model    = json_decode($data_kpi_by_model, true);

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$params = array(
    'product'         => intval($product),
    'color'           => intval($color),
    'store_id'        => trim($store),
    'from'            => trim($from),
    'to'              => trim($to),
    'imei'            => $imei,
    'status'          => intval($status),
    'shop_type'       => intval($shop_type),
    'kpi'             => intval($kpi),
    'area_id'         => intval($area_id),
    'regional_market' => intval($regional_market),
    'district'        => intval($district),
    'status_imei'     => intval($status_imei),
    'page'            => $page
);
// check nếu bật status_locked = 1 rồi thì ko cho báo nữa
foreach ($data_kpi_by_model as $data) {
    $kpi_by_model_id = $data['id'];
    $kpi_by_model_row = $QKpiByModel->fetchRow(['id = ?'=> $kpi_by_model_id]);
    if ($kpi_by_model_row['status_locked'] == 1) {
        echo json_encode(array(
            'code' => 1,
            'msg' => 'Đã chốt số không thể chấm thêm!'
        ));
        exit();
    }
}

try {
    $flashMessenger = $this->_helper->flashMessenger;

    if (!$data_kpi_by_model)
        throw new Exception("Bạn chưa Chọn đủ thông tin");
    
    $userStorage         = Zend_Auth::getInstance()->getStorage()->read();
    $QKpiByStaff         = new Application_Model_KpiByStaff();
    $user_id             = $userStorage->id;
    $is_chot = 2; // is_chot khac 2 moi cho bao so
    $db                  = Zend_Registry::get('db');
    if(!empty($data_kpi_by_model)){
        
        // loc input id => value
        $result_input = array ();
        foreach($data_kpi_by_model as $k => $item){
            if (isset($result_input[$item['id']])){
                $result_input[$item['id']] += $item['qty'];
            }else{
                $result_input[$item['id']] = $item['qty'];
            }
        }
        foreach ($data_kpi_by_model as $key => $value) {
            if($value['qty'] > 0){
//                if(   $user_id  == 5899){
                    $id = intval($value['id']);
                    $select = $db->select()
                        ->from(array('p' => 'kpi_by_model'))
                        ->joinLeft(array('kbs' => 'kpi_by_staff'), 'kbs.kpi_by_model_id = p.id', 
                             array(
                            'staff_qty' => new Zend_Db_Expr('SUM(kbs.qty)'),
                            'quantity'  => new Zend_Db_Expr('MAX(p.qty)')
                                 ) );
                    $select->where('p.id = ?', $id);
                    $select->group('p.id');
                    $result = $db->fetchRow($select);
                    $pg_allow = $result['pg_allow'];
                    if($pg_allow != '0'){
                        $quantity = $result['quantity']; // tong so ban ra
                    
                        $quantity_staff = !empty($result['staff_qty']) ? $result['staff_qty'] : 0; // tong so da bao
                    
                        // neu (tong so da bao + tong so nhap tu input) > tong so ban ra thì stop
                        if(($result_input[$id] + $quantity_staff) > $quantity){ 
                            echo json_encode(array(
                                    'code' => 0,
                                    'msg' => 'Số lượng bạn chọn vượt quá số lượng bán ra của shop'
                                ));
                            exit();
                        }
                        $array_pg = explode(',', substr($pg_allow, 1, -1));
                        if(!in_array($value['pg_id'], $array_pg)){
                            echo json_encode(array(
                                'code' => 0,
                                'msg' => 'PG không đúng'
                            ));
                        exit();
                        }
                        
                        
                        $kpi_staff = array(
                            'kpi_by_model_id'    => $value['id'],
                            'qty'                => $value['qty'],
                            'pg_id'              => $value['pg_id'],
                            'created_at'         => date('Y-m-d H:i:s'),
                            'created_by'         => $userStorage->id
                        );

                        $data_kpi_staff[] = $kpi_staff;

                        $log_kpi_staff = $kpi_staff;
                        $log_kpi_staff['note'] = 'Insert';
                        $log_data_kpi_staff[] = $log_kpi_staff;
                    }
                    
//                }
            }
            
        } 

        if (!empty($data_kpi_staff)) {
            My_Controller_Action::insertAllrow($data_kpi_staff, 'kpi_by_staff');
            My_Controller_Action::insertAllrow($log_data_kpi_staff, 'log_kpi_by_staff');
            echo json_encode(array(
                   'code' => 200,
                   'msg' => 'Done'
               ));
             exit();
        }
    }
   
    

    $flashMessenger->setNamespace('success')->addMessage('Success');
    $this->_redirect(HOST . 'manage/kpi-tgdd/' . ( $params ? '?' . http_build_query($params) . '&' : '?' ));
} catch (Exception $e) {
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect(HOST . 'manage/kpi-tgdd/' . ( $params ? '?' . http_build_query($params) . '&' : '?' ));
}