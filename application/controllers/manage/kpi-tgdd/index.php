<?php

$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$status          = $this->getRequest()->getParam('status');
$product         = $this->getRequest()->getParam('product');
$color           = $this->getRequest()->getParam('model');
$store           = $this->getRequest()->getParam('store_id');
$from            = $this->getRequest()->getParam('from', date('01/m/Y'));
$to              = $this->getRequest()->getParam('to', date('d/m/Y'));
$imei            = $this->getRequest()->getParam('imei', null);
$area_id         = $this->getRequest()->getParam('area_id');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$sales_team      = $this->getRequest()->getParam('sales_team');
$staff           = $this->getRequest()->getParam('staff_id', null);
$shop_type       = $this->getRequest()->getParam('shop_type');
$kpi             = $this->getRequest()->getParam('kpi');
$export          = $this->getRequest()->getParam('export');
$export_detail   = $this->getRequest()->getParam('export_detail');
$status_imei     = $this->getRequest()->getParam('status_imei');
$dev     		= $this->getRequest()->getParam('dev');
$dev1     		= $this->getRequest()->getParam('dev1');
$area_id    		= $this->getRequest()->getParam('area_id');


	
$QImeiSelloutPartner          = new Application_Model_ImeiSelloutPartner();
$flashMessenger               = $this->_helper->flashMessenger;
$messages                     = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages;

$page              = $this->getRequest()->getParam('page', 1);
// $sort        = $this->getRequest()->getParam('sort', 'name');
// $desc        = $this->getRequest()->getParam('desc', 0);

$limit             = 50;
$total             = 0;
$total_fail        = 0;
$params['area_id'] = array();

$user_id             = $userStorage->id;
$this->view->user_id = $user_id;
$title               = $userStorage->title;
// echo $title;die;
$this->view->title   = $title;

$params                  = array(
    'product'         => intval($product),
    'color'           => intval($color),
    'store_id'        => trim($store),
    'from'            => trim($from),
    'to'              => trim($to),
    'imei'            => $imei,
    'status'          => intval($status),
    'shop_type'       => intval($shop_type),
    'kpi'             => intval($kpi),
    'area_id_search'         => intval($area_id),
    'regional_market' => intval($regional_market),
    'district'        => intval($district),
    'status_imei'     => intval($status_imei),
    'page'            => $page, 
    'staff_id'        => $staff, 
	'dev'        => $dev,
	'dev1'        => $dev1,
	
	// 'area_id'        => $area_id
    
);
$QStaff                  = new Application_Model_Staff();
$staff_cache             = $QStaff->get_all_cache();
//echo "<pre>";
//print_r($staff_cache[23668]);
//echo "</pre>";
$this->view->staff_cache = $staff_cache;
// echo "<pre>";print_r($staff);die;
$QTeam       = new Application_Model_Team();
$QKpiByModel = new Application_Model_KpiByModel();
$QKpiByStaff = new Application_Model_KpiByStaff();
$where_group_areas       = array();
$where_group_areas       = $QTeam->getAdapter()->quoteInto('id = ?', $title);

$group_areas              = $QTeam->fetchRow($where_group_areas, 'policy_group');
$this->view->policy_group = $group_areas->policy_group;
$list_area = '';
if ($group_areas->policy_group == 6 || $group_areas->policy_group == 8) {
    $QAsm       = new Application_Model_Asm();
    $where_area = array();
    $where_area = $QAsm->getAdapter()->quoteInto('staff_id = ?', $user_id);

    $areas = $QAsm->fetchAll($where_area, 'area_id');

    foreach ($areas as $key => $value) {
        $list_area .= $value['area_id']. ',';
        $area_[] = $value['area_id'];
    }
    if (in_array($params['area_id'], $area_)) {
        $params['area_id'] = $params['area_id'];
    } else
        $params['area_id'] = $area_;
} else {
    if ($title == PGPB_TITLE || $title == CHUYEN_VIEN_BAN_HANG_TITLE || $title == SALES_TITLE) {
        $params['staff_id'] = intval($user_id);
    } else
        $params['staff_id'] = $staff;
}




// if ($title == SALE_SALE_SALE_ADMIN || $title == SALE_SALE_ASM || $title == 308 ) {
// }
// echo "<pre>";print_r($params);die;


if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm       = new Application_Model_Asm();
    $where_area = array();
    $where_area = $QAsm->getAdapter()->quoteInto('staff_id = ?', $user_id);

    $areas = $QAsm->fetchAll($where_area, 'area_id');
    
    foreach ($areas as $key => $value) {
        $area_[] = $value['area_id'];
        $list_area .= $value['area_id']. ',';
    }
    if (in_array($params['area_id'], $area_)) {
        $params['area_id'] = $params['area_id'];
    } else
        $params['area_id'] = $area_;
}
// if ($user_id  == 257) {
//  echo "<pre>";print_r($params);die;
// }

if ($export_detail) {

        // $data_export = $QKpiByModel->fetchPagination($page, null, $total, $params);
    $data_export = $QKpiByModel->fetchPaginationDetail($page, null, $total, $params);
    
    require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'STT',
            'B' => 'KHU VỰC',
            'C' => 'TỈNH',
            'D' => 'MÃ kho/Partner ID',
            'E' => 'CỬA HÀNH',
            'F' => 'SẢN PHẨM',
            'G' => 'MÀU SẮC',
            'H' => 'NGÀY BÁN',
//            'I' => 'SỐ LƯỢNG BÁN RA',
            'I' => 'THÔNG TIN NVBH',
            'J' => 'NVBH CHẤM SỐ',
            'K' => 'S.O TÍNH KPI/NVBH',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach ($heads as $key => $value)
            $sheet->setCellValue($key . '1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(15);
        $sheet->getColumnDimension('I')->setWidth(80);
        $sheet->getColumnDimension('J')->setWidth(80);
        $sheet->getColumnDimension('K')->setWidth(10);
//        $sheet->getColumnDimension('L')->setWidth(10);
//        $sheet->getColumnDimension('M')->setWidth(30);


        foreach ($data_export as $key => $value) {

            if ($value['pg_allow'] != "0" || !empty($value['pg_allow']) || $value['pg_allow'] != 0){
                $array_pg = explode(',', substr($value['pg_allow'], 1, -1));
            } 

//            if(is_array($array_pg) ){
//                    $arrInfo        = explode(',',$value['info_pg_id']);
//                    $arrValue       = explode(',',$value['info_qty']);
//                    $arrInfoValue   = array_combine($arrInfo,$arrValue);
//            }
//            $sheet->setCellValue('A' . ($key + 2), $value['area']);
            $sheet->setCellValue('B' . ($key + 2), $value['area']);
            $sheet->setCellValue('C' . ($key + 2), $value['province']);
            $sheet->setCellValue('D' . ($key + 2), $value['partner_id']);
            $sheet->setCellValue('E' . ($key + 2), $value['store']);
            $sheet->setCellValue('F' . ($key + 2), $value['model']);
            $sheet->setCellValue('G' . ($key + 2), $value['color']);
            $sheet->setCellValue('H' . ($key + 2), date('d/m/Y', strtotime($value['timing_date'])));
//            $sheet->setCellValue('I' . ($key + 2), $value['quantity']);
            if(is_array($array_pg) && $value['pg_allow'] != "0"){
                $str_name = '';
                $str_name_staff = '';
                $staff_qty = 0;
                foreach ($array_pg as $v_pg){
                    $str_name .= $staff_cache[$v_pg]['name'] . ' ' .$staff_cache[$v_pg]['code'] . ' , ';
                     
                }
                $sheet->setCellValue('I' . ($key + 2), rtrim($str_name,', '));
                if(!empty($value['pg_id'])){
                    $str_name_staff = $staff_cache[$value['pg_id']]['name'] . ' ' .$staff_cache[$value['pg_id']]['code'] ;
                }
                $sheet->setCellValue('J' . ($key + 2), rtrim($str_name_staff, ', ') );
                $sheet->setCellValue('K' . ($key + 2), $value['staff_qty'] );
                
            }
            
        }
        $filename  = 'Sellout Detail -' . date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        exit;
}

if ($export) {
    $data_export = $QKpiByModel->fetchPagination($page, null, $total, $params);
//    $data_export = $QKpiByModel->fetchPagination($page, $limit, $total, $params);
    require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'STT',
            'B' => 'KHU VỰC',
            'C' => 'TỈNH',
            'D' => 'MÃ kho/Partner ID',
            'E' => 'CỬA HÀNH',
            'F' => 'SẢN PHẨM',
            'G' => 'MÀU SẮC',
            'H' => 'NGÀY BÁN',
            'I' => 'SỐ LƯỢNG BÁN RA',
            'J' => 'THÔNG TIN NVBH',
            'K' => 'NVBH CHẤM SỐ',
//            'L' => 'S.O TÍNH KPI/NVBH',
//            'M' => 'ID',
//            'N' => 'pg_allow',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach ($heads as $key => $value)
            $sheet->setCellValue($key . '1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('I')->setWidth(30);
        $sheet->getColumnDimension('J')->setWidth(80);
        $sheet->getColumnDimension('K')->setWidth(80);
//        $sheet->getColumnDimension('L')->setWidth(30);
//        $sheet->getColumnDimension('M')->setWidth(30);


        foreach ($data_export as $key => $value) {

            if ($value['pg_allow'] != "0" || !empty($value['pg_allow']) || $value['pg_allow'] != 0){
                $array_pg = explode(',', substr($value['pg_allow'], 1, -1));
            } 
                
            if(is_array($array_pg) ){
                    $arrInfo        = explode(',',$value['info_pg_id']);
                    $arrValue       = explode(',',$value['info_qty']);
                    $arrInfoValue   = array_combine($arrInfo,$arrValue);
            }
//            $sheet->setCellValue('A' . ($key + 2), $value['area']);
            $sheet->setCellValue('B' . ($key + 2), $value['area']);
            $sheet->setCellValue('C' . ($key + 2), $value['province']);
            $sheet->setCellValue('D' . ($key + 2), $value['partner_id']);
            $sheet->setCellValue('E' . ($key + 2), $value['store']);
            $sheet->setCellValue('F' . ($key + 2), $value['model']);
            $sheet->setCellValue('G' . ($key + 2), $value['color']);
            $sheet->setCellValue('H' . ($key + 2), date('d/m/Y', strtotime($value['timing_date'])));
            $sheet->setCellValue('I' . ($key + 2), $value['quantity']);
            if(is_array($array_pg) && $value['pg_allow'] != "0"){
                $str_name = '';
                foreach ($array_pg as $v_pg){
                    $str_name .= $staff_cache[$v_pg]['name'] . ' ' .$staff_cache[$v_pg]['code'] . ' , ';
                     
                }
                
                $sheet->setCellValue('J' . ($key + 2), rtrim($str_name,', '));
            }
            
            if(is_array($array_pg) && $value['pg_allow'] != "0"){
               
                    $str_name_staff = '';
                foreach ($array_pg as $v_pg){
                    if ( !empty($arrInfoValue[$v_pg])){
                        $str_name_staff .= $staff_cache[$v_pg]['name'] . ' ' .$staff_cache[$v_pg]['code'] . ' SL: ' . $arrInfoValue[$v_pg] . ' , ';
                    }
                }
                
                $sheet->setCellValue('K' . ($key + 2), rtrim($str_name_staff, ', ') );
            }
            
            
//            $sheet->setCellValue('M' . ($key + 2), $value['id']);
//            $sheet->setCellValue('N' . ($key + 2), $value['pg_allow']);
//            $sheet->setCellValue('J' . ($key + 2), $value['']);
//            $sheet->setCellValue('K' . ($key + 2), $value['customer_phone']);
        }
        $filename  = 'Sellout Tổng -' . date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        exit;
}

$imeis      = $QKpiByModel->fetchPagination($page, $limit, $total, $params);

$result_quantity = $QKpiByModel->getTotalQuantity($params);
$total_quantity = $result_quantity['total_quantity'];
$quantity_selected = $result_quantity['quantity_selected'];

    $p_from_date = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
    $p_to_date = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

    $p_from_date = $p_from_date . ' 00:00:00';
    $p_to_date   = $p_to_date . ' 23:59:59';
    
    if(in_array($userStorage->id, array(5899, 341)) && !empty($area_id) ){
         $list_area .= $area_id;
		 $params['area_id'] = $area_id;
    }
	


    if($list_area <> ''){
        $list_area = rtrim($list_area, ',');
    }


$this->view->total_quantity = $total_quantity;
$this->view->quantity_selected = $quantity_selected;
if ($userStorage->title != PGPB_TITLE) {
    # code...
}
//get goods
$QGood     = new Application_Model_Good();
$where_g   = array();
$where_g[] = $QGood->getAdapter()->quoteInto('cat_id IN (?)', [PHONE_CAT_ID, IOT_OPPO_CAT_ID]);
$where_g[] = $QGood->getAdapter()->quoteInto('is_timing = ?', 1);

// tạm thời ko cho chấm one plus
$where_g[] = $QGood->getAdapter()->quoteInto('id  != ?', 424);
$goods     = $QGood->fetchAll($where_g, 'desc');


$this->view->goods = $goods;

// load all model
$QGoodColor = new Application_Model_GoodColor();
$result     = $QGoodColor->fetchAll();

$QArea             = new Application_Model_Area();
$QRegionalMarket   = new Application_Model_RegionalMarket();
$this->view->areas = $QArea->fetchAll(null, 'name');

if ($area_id) {
    $where                        = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}


if ($regional_market) {
    //get district
    $where                 = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}

if ($area_id) {
    $where                        = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

$data = null;
if ($result->count()) {
    foreach ($goods as $good) {
        $colors = explode(',', $good->color);
        $temp   = array();
        foreach ($result as $item) {
            if (in_array($item['id'], $colors)) {
                $temp[] = array(
                    'id'    => $item->id,
                    'model' => $item->name,
                );
            }
        }
        $data[$good['id']] = $temp;
    }
}
$begin_passby_tgdd = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
$to_passby_tgdd = date_create_from_format("d/m/Y", $to)->format("Y-m-d");
 // unset($params['area_id']);
  if(!in_array($userStorage->id, array(5899, 341))  ){
         unset($params['area_id']);
    }
// echo "<pre>";print_r($data[$product]);

$this->view->colors      = $data[$product];
$this->view->good_colors = json_encode($data);
$this->view->params      = $params;
// $this->view->imei        = (!in_array($user_id, array(5899, 341)) AND  $begin_passby_tgdd >= '2020-01-01') ? array() :  $imeis ;
$this->view->show_export = 1;
$staff_id_test = 16595;
/*if(!in_array($user_id, array(5899, 341, $staff_id_test)) AND  ($begin_passby_tgdd > '2020-01-31' || $to_passby_tgdd > '2020-01-31') ) {
	$this->view->show_export = 0;
}
 * 
 */
 // $this->view->imei        = !in_array($user_id, array(5899, 341, $staff_id_test, 26095, 16595, 8153)) ? array() :  $imeis ;

  $this->view->imei        = $imeis ;
$this->view->imei_fail   = $imeis_fail;
$this->view->limit       = $limit;
$this->view->total       = $total;
$this->view->total_fail  = $total_fail;
$this->view->param       = $params ? '?' . http_build_query($params) . '&' : '?';
$this->view->url         = HOST . 'manage/kpi-tgdd/' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset      = $limit * ($page - 1);
$this->view->staff_id    = $userStorage->id;

$this->_helper->viewRenderer->setRender('kpi-tgdd/index');


