<?php
/**
 * Xuất các report cho HR, dành cho view ở local
 */
class HrReportController extends My_Controller_Action
{
    
    /**
     * Giao diện chính; chọn thời gian để xuất các loại báo cáo
     */
    public function indexAction()
    {
        $from_date = $this->getRequest()->getParam('from_date', date('01/m/Y'));
        $to_date   = $this->getRequest()->getParam('to_date', date('d/m/Y'));
        $export    = $this->getRequest()->getParam('export');


        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'      => $from,
            'to'        => $to,
        );

        $this->view->params = $params;

        if ( isset( $export ) && intval( $export ) > 0 ) {
            if ( $export == 1 ) {
                My_Report_Hr::kpiPg($from, $to);
                exit;
            }

            if ( $export == 2 ) {
                My_Report_Hr::kpiSale($from, $to);
                exit;
            }

            if ( $export == 3 ) {
                My_Report_Hr::kpiLeader($from, $to);
                exit;
            }
        }
    }


    public function pgKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);
        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area_id');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        // echo "<pre>";print_r($param);die; 
        // echo $area;die;

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );
        // 
        
        $this->view->params = $params;

        if (isset( $export ) && intval( $export )){
            
            My_Report_Hr::kpiPgNew($from, $to, $params);
        }
    }
	
	public function pgKpiAccessoriesAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);

        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );


        $this->view->params = $params;

        if (isset( $export ) && intval( $export )){
            My_Report_Hr::kpiPgAccessories($from, $to, $params);
        }
    }

    public function saleKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);

        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiSaleNew($from, $to, $params);
    }

     public function pbSaleKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);

        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiPbSale($from, $to, $params);
    }

    public function leaderKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 0);
        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiLeaderNew($from, $to, $params);
    }

    public function storeLeaderKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 0);
        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiStoreLeader($from, $to, $params);
    }

    public function pgImeiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 0);
        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::pgImei($from, $to, $params);
    }
    public function accessoriesKpiAction()
    {
        $from_date = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date   = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export    = $this->getRequest()->getParam('export', 0);

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0] . " 23:59:59";

        $params = array(
            'from' => $from,
            'to'   => $to,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiAccessories($from, $to);
    }

    public function timingAction()
    {
        $from_date = $this->getRequest()->getParam('from_date', date('01/m/Y'));
        $to_date   = $this->getRequest()->getParam('to_date', date('d/m/Y'));
        $export    = $this->getRequest()->getParam('export', 0);

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from_date' => $from_date,
            'to_date'   => $to_date,
            'from'      => $from,
            'to'        => $to,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiPg($from, $to);
    }

    /**
     * Dùng để gán các hằng số dùng tính KPI;
     *		-- bảng KPI của sales
     * 		giá trị từng biến có thể thay đổi bởi HR;
     * 		các biến mới chỉ Technology tạo thêm được
     *		-- riêng bên product thì dùng chung bảng (thêm cột pg_kpi)
     *		chỉ HR thấy các số KPI, còn bên khác chỉ thấy/sửa giá, tên sản phẩm...
     */
    public function settingsAction()
    {
        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
        if (empty($from)) $from = date('01/m/Y');

        $to = $this->getRequest()->getParam('to', date('d/m/Y'));
        if (empty($to)) $to = date('d/m/Y');

        $type   = $this->getRequest()->getParam('type');
        $cat   = $this->getRequest()->getParam('cat');
        $name   = $this->getRequest()->getParam('name');
        $product_name   = $this->getRequest()->getParam('product_name');
        $policy = $this->getRequest()->getParam("policy_id");
        $area = $this->getRequest()->getParam("area_id");
        $export = $this->getRequest()->getParam("export");
        $page   = $this->getRequest()->getParam('page', 1);
        $limit  = LIMITATION;
        $total  = 0;
        $params = array('from' => $from, 'to' => $to, 'type' => $type, 'cat' => $cat, 'name' => $name, 'policy_id' => $policy, 'area_id' => $area);

    	$QGoogKpiLog = new Application_Model_GoodKpiLog();
        $log = $QGoogKpiLog->kpiSetting($page, $limit, $total, $params);
        $this->view->logs = $log; 
        $QGood = new Application_Model_Good();
        $this->view->goods = $QGood->get_cache();
       
        $good_all = $QGood->get_cache_all(null);
        $this->view->good_all = $good_all;
        $QGoodColor = new Application_Model_GoodColor();
        $good_colors = $QGoodColor->get_cache();
        $this->view->good_colors = $good_colors;
        $QGoodCat = new Application_Model_GoodCategory();
        $this->view->cat = $QGoodCat->get_cache();
        $QPolicy = new Application_Model_Policy();
        $this->view->policy = $QPolicy->selectPolicy(array());
        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;
        // echo "<pre>";print_r( $QGoogKpiLog->fetchByMonth($page, $limit, $total, $params) );die;
        if ($export) {
           $params['from'] = null;
            $params['to'] = null;
             // echo "<pre>";print_r($params);die;
           $data_export =  $QGoogKpiLog->kpiSetting($page, null, $total, $params);

           

            set_time_limit(0);
            error_reporting(0);
            ini_set('memory_limit', -1);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();


            $heads = array(
                /*'No.',*/
                'Good',
                'Policy',
                'Color',
                'Area',
                'Dealer Type',
                'Kpi',
                'From',
                'To',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            $alpha = 'A';
            $index = 1;
            foreach ($heads as $key) {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }

            $sheet->getStyle('A1:K1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(35);
            $sheet->getColumnDimension('B')->setWidth(15);
            $sheet->getColumnDimension('C')->setWidth(15);
            $sheet->getColumnDimension('D')->setWidth(15);
            $sheet->getColumnDimension('E')->setWidth(15);
            $sheet->getColumnDimension('F')->setWidth(25);
            $sheet->getColumnDimension('G')->setWidth(30);
            $sheet->getColumnDimension('H')->setWidth(30);
            
            $index = 2;
            $stt = 0;

            foreach($data_export as $key => $log){
                $dealer_type = '';
                if ($log['dealer_type'] == 1) {
                               $dealer_type = 'KA';
                }elseif ($log['dealer_type'] == 2) {
                    $dealer_type = 'IND';
                }
                $alpha = 'A';
                $sheet->getCell($alpha++.$index)->setValueExplicit( isset($good_all[ $log['good_id'] ]) ? $good_all[ $log['good_id'] ] : '', PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++.$index)->setValueExplicit( trim(isset($log['policy_title']) ? $log['policy_title'] : ''), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++.$index)->setValueExplicit( isset($good_colors[ $log['color_id'] ])  ? $good_colors[ $log['color_id'] ] : '' , PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++.$index)->setValueExplicit( trim($log['area_name']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++.$index)->setValueExplicit( trim($dealer_type), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++.$index)->setValueExplicit( trim($log['kpi']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++.$index)->setValueExplicit( trim((isset($log['from'])) ? date('d/m/Y', strtotime($log['from'])) : ''), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++.$index)->setValueExplicit( trim((isset($log['to'])) ? date('d/m/Y', strtotime($log['to'])) : ''), PHPExcel_Cell_DataType::TYPE_STRING);
    
                $index++;
            } 
            

            $filename = 'KPI Setting - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }
        $this->view->params = $params;
        
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'hr-report/settings' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset = $limit * ($page - 1);

    	$flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
   		$this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
    }

    public function settingsCreateAction()
    {
        $QGood                   = new Application_Model_Good();
        $this->view->goods       = $QGood->get_cache();
        $QGoodCat                = new Application_Model_GoodCategory();
        $this->view->cat         = $QGoodCat->get_cache();
        $QGoodColor              = new Application_Model_GoodColor();
        $this->view->good_colors = $QGoodColor->get_cache();

        $this->view->good_all = $QGood->get_cache_all(null);

        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();

        $this->view->areas = $areas;

        $this->view->refer       = My_Url::refer('hr-report/settings');

        $flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
    }
    
    public function loadProductAction()
    {
        $cat_id = $this->getRequest()->getParam('cat_id');
        $QGood = new Application_Model_Good();

        if ($cat_id) {
            $where = array();
            $where[] = $QGood->getAdapter()->quoteInto('cat_id = ?',    $cat_id);
            $where[] = $QGood->getAdapter()->quoteInto('del = ?',       0);
            $goods = $QGood->fetchAll($where, 'name');

            echo json_encode(array('goods' => $goods->toArray()));
            exit;

        }
        else{
            echo json_encode(array());
            exit;
        }
    }

    public function settingsEditAction()
    {
        $id = $this->getRequest()->getParam('id');
        $refer = My_Url::refer('hr-report/settings');
        $db =  Zend_Registry::get('db');
        try {
            if (!$id) throw new Exception("Invalid ID", 1);

            $id = intval($id);
            $QGoodKpiLog = new Application_Model_GoodKpiLog();
            $where       = $QGoodKpiLog->getAdapter()->quoteInto('id = ?', $id);
            $log_check   = $QGoodKpiLog->fetchRow($where);

            if (!$log_check) throw new Exception("Wrong ID", 2);
            
            $QGoodLog = new Application_Model_Good();
            $where    = $QGoodLog->getAdapter()->quoteInto('id = ?', $log_check->good_id);
            $good     = $QGoodLog->fetchRow($where);
            
            $this->view->log_cat = $good->cat_id;
            $this->view->log     = $log_check;
            $this->view->refer   = $refer;

            $QGood = new Application_Model_Good();
            $this->view->goods = $QGood->get_cache();
            $this->view->good_all = $QGood->get_cache_all($good->cat_id);
            
            $QGoodCat = new Application_Model_GoodCategory();
            $this->view->cat = $QGoodCat->get_cache();
            
            $QGoodColor = new Application_Model_GoodColor();
            $this->view->good_colors = $QGoodColor->get_cache();

            $QArea = new Application_Model_Area();
            $areas = $QArea->get_cache();
            $this->view->areas = $areas;

            $QDistributor = new Application_Model_Distributor();
            if($log_check['distributor_id']){

                //$arr_distr         = explode(',',$log_check['distributor_id']);
                $arr_distr[]         = $log_check['distributor_id'];
                $params['list_id'] = $arr_distr;
                $total             = 0;

                $arr_distributors             = $QDistributor->fetchPagination(1,0,$total,$params);
                $this->view->arr_distributors = $arr_distributors;
            }


            $this->_helper->viewRenderer->setRender('settings-create');
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage(sprintf("[%s] %s", $e->getCode(), $e->getMessage()));
            $this->_redirect($refer);
        }
    }


    public function settingsSaveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if($this->getRequest()->isPost()){
            $good_id             = $this->getRequest()->getParam('good_id');
            $color_id            = $this->getRequest()->getParam('color_id');
            $from                = $this->getRequest()->getParam('from');
            $to                  = $this->getRequest()->getParam('to');
            $kpi                 = $this->getRequest()->getParam('kpi', 0);
            $type                = $this->getRequest()->getParam('type');
            $id                  = $this->getRequest()->getParam('id');
            $refer               = HOST.'hr-report/settings';
            $distributor_id_text = $this->getRequest()->getParam('distributor_id_text');
            $area_id             = $this->getRequest()->getParam('area_id');
            $flashMessenger      = $this->_helper->flashMessenger;
            $QGoodKpiLog         = new Application_Model_GoodKpiLog();
            $db                  = Zend_Registry::get('db');

            $db->beginTransaction();
            try {
                if ($id) {
                    $where = $QGoodKpiLog->getAdapter()->quoteInto('id = ?', intval($id));
                    $log_check = $QGoodKpiLog->fetchRow($where);
                    if (!$log_check) throw new Exception("Invalid log", 7);
                }

                if (!$type || !isset(My_Kpi_Object::$name[$type])) throw new Exception("Invalid KPI For", 8);

                if (!$from || !date_create_from_format('d/m/Y', $from))
                    throw new Exception("Invalid From date", 1);

                $from = date_create_from_format('d/m/Y', $from)->format('Y-m-d');

                if ($to && !date_create_from_format('d/m/Y', $to)){
                    throw new Exception("Invalid To date", 2);
                }

                if ($to){
                    $to = date_create_from_format('d/m/Y', $to)->format('Y-m-d');
                }

                if (!$good_id) throw new Exception("Invalid product", 3);
            

                if ($color_id) {
                    $QGoodColor = new Application_Model_GoodColor();
                    $colors = $QGoodColor->get_cache();
                    if (!isset($colors[ $color_id ])) throw new Exception("Invalid color", 5);
                }

                $where        = array();
                $where_date   = array();
                $where[]      = $QGoodKpiLog->getAdapter()->quoteInto('good_id = ?', intval($good_id));
                $where[]      = $QGoodKpiLog->getAdapter()->quoteInto('type = ?', intval($type));
                $where_date[] = $QGoodKpiLog->getAdapter()->quoteInto('from_date <= ? AND to_date >= ?', $from);

                if ($to){
                    $where_date[] = $QGoodKpiLog->getAdapter()->quoteInto('from_date <= ? AND to_date >= ?', $to);
                }

                $where[] = implode(' OR ', $where_date);

                if ($color_id){
                    $where[] = $QGoodKpiLog->getAdapter()->quoteInto('color_id = ?', intval($color_id));
                }

                if ($id){
                    $where[] = $QGoodKpiLog->getAdapter()->quoteInto('id <> ?', intval($id));
                }

                $log_check = $QGoodKpiLog->fetchRow($where);

                if ($log_check) throw new Exception("Conflict dates", 6);

                $data = array(
                    'good_id'        => intval($good_id),
                    'color_id'       => intval($color_id),
                    'from_date'      => $from,
                    'to_date'        => empty($to) ? null : $to,
                    'kpi'            => floatval( $kpi ),
                    'type'           => intval( $type ),
                    'area_id'        => $area_id,
                    'distributor_id' => intval($distributor_id_text),
                );

                $result = $QGoodKpiLog->save($data,$id);
                if($result['code'] !== 0){
                    throw new Exception($result['message'],$result['code']);
                }

                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage($result['message']);
                My_Message::redirect($refer,0);

            } catch (Exception $e) {
                $db->rollBack();
                $message = sprintf("[%s] %s", $e->getCode(), $e->getMessage());
                My_Message::jsAlert($message,'error');
            
            }
        }
        exit; 
    }

    public function settingsDeleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $QGoodKpiLog = new Application_Model_GoodKpiLog();
        $where = $QGoodKpiLog->getAdapter()->quoteInto('id = ?', intval($id));
        $QGoodKpiLog->delete($where);
        $this->_redirect(My_Url::refer('hr-report/settings'));
    }

    public function saveSettingsAction()
    {
    	if ($this->getRequest()->getMethod() == 'POST') {
    		// update cho PG
            $flashMessenger = $this->_helper->flashMessenger;

    		$QGood = new Application_Model_Good();
            $goods = $QGood->get_cache();

            $QGoodKpi = new Application_Model_GoodKpi();

            foreach ($goods as $id => $desc) {
                $pg_kpi     = $this->getRequest()->getParam('p_'.$id, 0);
                $sales_kpi  = $this->getRequest()->getParam('s_'.$id, 0);
                $leader_kpi = $this->getRequest()->getParam('l_'.$id, 0);
                $pg_kpi     = intval($pg_kpi);
                $sales_kpi  = intval($sales_kpi);
                $leader_kpi = intval($leader_kpi);

                $data = array(
                    'pg_kpi'     => $pg_kpi,
                    'sales_kpi'  => $sales_kpi,
                    'leader_kpi' => $leader_kpi,
                );

                $where = $QGoodKpi->getAdapter()->quoteInto('good_id = ?', $id);
                $it = $QGoodKpi->fetchRow($where);

                try {
                    if ($it){
                    $QGoodKpi->update($data, $where);
                    } else {
                        $data['good_id'] = $id;
                        $QGoodKpi->insert($data);
                    }
                } catch (Exception $e) {
                    $flashMessenger->setNamespace('error')->addMessage(sprintf("[%s] %s", $e->getCode(), $e->getMessage()));
                    $this->_redirect(HOST.'hr-report/settings');
                }
    		}

       		$messages = $flashMessenger->setNamespace('success')->addMessage('Done!');
    	}

    	$this->_redirect(HOST.'hr-report/settings');
    }

    private function _error($fm){
        $fm->setNamespace('error')->addMessage('Error');
        $this->_redirect('/hr-report/new-settings-create');
    }

    private function _error1($fm){
        $fm->setNamespace('error')->addMessage('Error');
        $this->_redirect('/hr-report/kpi-setting-area-new');
    }

    public function newSettingsCreateAction(){
        $fm = $this->_helper->FlashMessenger();

        if($this->_request->isPost()){
            $params = $this->_request->getParams();
            $params = $this->validate_data($params);

            if($params){
                $data_insert = array();
                $from_date =  date("Y-m-d", strtotime($params['from']) - 86400);

                // Lấy các input
                $product_id = $params['product_id'];
                $color_id = (!empty($params['color_id']) ? $params['color_id'] : 0);
                $kpi = $params['kpi'];
                // $kpi_type = $this->get_kpi_type($params['team']);
                $kpi_type = $params['team'];
                $from = $params['from'];
                $to = ($params['to'] ? '"'.$params['to'].'"' : 'null');
                $policy_id = $params['policy_id'];
                $distributor = ($params['distributor'] ? '"'.$params['distributor'].'"' : 'null'); // Chưa có distributor nên tạm thời rỗng

                // Lấy full thông tin policy từ id_policy đế lấy danh sách các khu vực của policy
                $PolicyModel = new Application_Model_Policy();
                $params_select = array("id" => $policy_id);
                $data_policy = $PolicyModel->selectById($params_select);

                // echo "<pre>";print_r($data_policy);die;

                // array chứa danh sách id của các khu vực thuộc chính sách.
                if(!empty($data_policy['data']['area']))
                {
                    $array_id_area_policy = explode(",", $data_policy['data']['area']);
                }
                else
                {
                    $array_id_area_policy = '';
                }

               // echo "<pre>";print_r($array_id_area_policy);die;

               // echo "<pre>";print_r($kpi_type);die;

                foreach ($kpi_type as $key => $kpi_type_value)
                {
                    $kpi_type_id = $this->get_kpi_type($kpi_type_value);
                    if(!empty($array_id_area_policy))
                    {
                        foreach($array_id_area_policy as $key => $value)
                        {
                            $data_insert[] = '('.
                                        $product_id.', '.
                                        $color_id.', '.
                                        $kpi.', '.
                                        $kpi_type_id.', '.
                                        '"'.$from.'", '.
                                        $to.', '.
                                        '"'.date('Y-m-d H:i:s').'", '.
                                        $value.', '.
                                        $distributor.',' .
                                        $policy_id
                                        .')';
                        }
                    }
                    else
                    {
                        
                        $data_insert[] = '('.
                                        $product_id.', '.
                                        $color_id.', '.
                                        $kpi.', '.
                                        $kpi_type_id.', '.
                                        '"'.$from.'", '.
                                        $to.', '.
                                        '"'.date('Y-m-d H:i:s').'", '.
                                        'null'.', '.
                                        $distributor.',' .
                                        $policy_id
                                        .')';
                    }
                }

                // echo "<pre>";print_r($data_insert);die;
                
                // foreach($params['group'] as $key => $value){
                //     if($value['kpi']){
                //         if(!empty($value['area'])){
                //             foreach($value['area'] as $k => $v){
                //                 if(!empty($value['distributor'])){
                //                     foreach($value['distributor'] as $kd => $vd){
                //                         $data[] = '('.
                //                             $params['product_id'].', '.
                //                             (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                //                             $value['kpi'].', '.
                //                             $this->get_kpi_type($params['team']).', '.
                //                             '"'.$params['from'].'", '.
                //                             ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                //                             '"'.date('Y-m-d H:i:s').'", '.
                //                             $v.', '.
                //                             $vd.', '.
                //                             $params['policy_id']
                //                         .')';
                //                     }
                //                 }
                //                 else{
                //                     $data[] = '('.
                //                         $params['product_id'].', '.
                //                         (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                //                         $value['kpi'].', '.
                //                         $this->get_kpi_type($params['team']).', '.
                //                         '"'.$params['from'].'", '.
                //                         ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                //                         '"'.date('Y-m-d H:i:s').'", '.
                //                         $v.', '.
                //                         '0'.', '.
                //                         $params['policy_id']
                //                     .')';
                //                 }
                //             }
                //         }
                //         else{
                //             if(!empty($value['distributor'])){
                //                 foreach($value['distributor'] as $kd => $vd){
                //                     $data[] = '('.
                //                         $params['product_id'].', '.
                //                         (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                //                         $value['kpi'].', '.
                //                         $this->get_kpi_type($params['team']).', '.
                //                         '"'.$params['from'].'", '.
                //                         ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                //                         '"'.date('Y-m-d H:i:s').'", '.
                //                         '-1, '.
                //                         $vd.', '.
                //                         $params['policy_id']
                //                     .')';
                //                 }
                //             }
                //             else{
                //                 // Neu ca 2 deu null thi cho chay vo 40 khu vuc
                //                 $listArea = (new Application_Model_Area())->get_cache();
                //                 foreach ($listArea as $keyArea => $valueArea) {
                //                     $data[] = '('.
                //                     $params['product_id'].', '.
                //                     (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                //                     $value['kpi'].', '.
                //                     $this->get_kpi_type($params['team']).', '.
                //                     '"'.$params['from'].'", '.
                //                     ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                //                     '"'.date('Y-m-d H:i:s').'", ' . $keyArea . ', 0,' . $params['policy_id'] . ')';
                //                 }
                                
                //             }
                //         }
                //     }
                // }
                $value = implode(',', $data_insert);

                // echo "<pre>";print_r($value);die;

                // $value = implode('<br>', $data);
                // echo '<pre>';
                // echo($value);die;
                
                if($value){
                    $res = (new Application_Model_GoodKpiLog())->_insert($value, $from_date);
                    if($res){
                        $fm->setNamespace('success')->addMessage('Success');
                        $this->_redirect('/hr-report/settings');
                    }
                }
                else $this->_error($fm);
            }
            else $this->_error($fm);
        }

        $QProduct = new Application_Model_Good();
        $QArea = new Application_Model_Area();
        $team = new Application_Model_Team();
        $policy = new Application_Model_Policy();

        $this->view->areas = $QArea->get_cache();
        $this->view->listProduct = $QProduct->get_cache();
        
        $this->view->team = $team->getSpecialist(implode(',', array(PGPB_TITLE, SALES_TITLE, LEADER_TITLE, PB_SALES_TITLE)));
        $this->view->policy = $policy->selectAllPolicy();
        $this->view->message = $fm;
        // var_dump($fm->setNamespace('error')->hasMessages());die;
    }

    public function ajaxKpiSettingAction()
    {
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
        	$this->_helper->layout()->disableLayout();
        
        	$product_id = $this->_request->getParam('product_id');
            $QGoodColor = new Application_Model_GoodColor();
            $GoodColor = $QGoodColor->getById($product_id);
        
        	if(!empty($GoodColor)){
                $this->view->GoodColor = $GoodColor;
        	}
    
        }
    }
    
    public function getFilterDistributorAction(){
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $this->_helper->layout()->disableLayout();
            $search = trim(addslashes($this->_request->getParam('search')));
            $data = (new Application_Model_Distributor())->getFilter($search);
            if(!empty($data)){
                $this->view->data = $data;
            }
        }
    }

    private function get_kpi_type($id){
        $array = array(
            182 => 1,
            183 => 2,
            190 => 3,
            312 => 4
        );
        return $array[$id];
    }

    private function validate_data($params){
        if(
            !$params['product_id'] ||
            !$params['team'] ||
            !$params['from'] ||
            !$params['policy_id'] ||
            empty($params['policy_id'])
        )
            return false;
        return $params;
    }

    // add 22.5.2017 
    public function kpiSettingAction(){
        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
        if (empty($from)) $from = date('01/m/Y');

        $to = $this->getRequest()->getParam('to', date('d/m/Y'));
        if (empty($to)) $to = date('d/m/Y');

        $is_tgdd         = $this->getRequest()->getParam('is_tgdd');
        $dealer_type         = $this->getRequest()->getParam('dealer_type');
        $type         = $this->getRequest()->getParam('type');
        $name         = $this->getRequest()->getParam('name');
        $product_name = $this->getRequest()->getParam('product_name');
        $policy       = $this->getRequest()->getParam("policy_id");
        $area         = $this->getRequest()->getParam("area_id");
        $page   = $this->getRequest()->getParam('page', 1);
        $limit  = LIMITATION;
        $total  = 0;
        $params = array('from' => $from, 'to' => $to, 'type' => $type, 'cat' => $cat, 'name' => $name, 'policy_id' => $policy, 'area_id' => $area,
                        'dealer_type' => $dealer_type,'product_name' => $product_name,'is_tgdd' => $is_tgdd);

        $QGoogKpiLog = new Application_Model_KpiSetting();

        // $models = 
        $this->view->logs = $QGoogKpiLog->kpi_setting($page, $limit, $total, $params);
        $QGood = new Application_Model_Good();
        $this->view->goods = $QGood->get_cache();
        $this->view->good_all = $QGood->get_cache_all(null);
        $QGoodColor = new Application_Model_GoodColor();
        $this->view->good_colors = $QGoodColor->get_cache();
        $QGoodCat = new Application_Model_GoodCategory();
        $this->view->cat = $QGoodCat->get_cache();
        $QPolicy = new Application_Model_Policy();
        $this->view->policy = $QPolicy->selectPolicy(array());
        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;
        $this->view->dealer_type = $dealer_type;
        $this->view->type = $type;
        $this->view->is_tgdd = $is_tgdd;

        $this->view->params = $params;
        
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'hr-report/kpi-setting' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset = $limit * ($page - 1);

        $flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
    }

    public function kpiSettingAreaAction() {

        $policy       = $this->getRequest()->getParam("policy_id");

        $db             = Zend_Registry::get('db');
        $sql            = '
        SELECT p.id,p1.title policy,a.`name` area
        FROM policy_area p
        JOIN area a
            ON a.id = p.area_id
        JOIN policy p1
            ON p1.id = p.policy_id
        
        ';
        if (isset($policy) && $policy) {
            $sql = $sql . "WHERE policy_id =" .$policy;
        }
        $stmt = $db->prepare($sql);
        $stmt->execute();
        

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        // echo "<pre>";print_r($data);die;
        $this->view->policy_area = $data;
        $QPolicy = new Application_Model_Policy();
        $this->view->policy = $QPolicy->selectPolicy(array());

    }

    public function kpiSettingAreaDeleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $db             = Zend_Registry::get('db');
        $sql            = '
            DELETE FROM policy_area WHERE id = '.$id.'
        ';
        $db->query($sql);
        $db  = null;
        $this->_redirect(My_Url::refer('hr-report/kpi-setting-area'));
    }

    public function kpiSettingAreaNewAction() {
        
        $db = Zend_Registry::get('db');

        $QPolicy = new Application_Model_Policy();
        $this->view->policy = $QPolicy->selectPolicy1(array());
        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;
        $QPolicyArea = new Application_Model_PolicyArea();

      

        if($this->_request->isPost() ){
            $policy   = $this->getRequest()->getParam('policy_id');
            $area     = $this->getRequest()->getParam('area_id');
            $data_insert = array();
            foreach($area as $k => $v ){
                $data_insert[] = '('.
                                        $policy.', '.
                                        $v
                                        .')';
                        
            }   

            $value = implode(',', $data_insert);
            
            if($value){
                
                    try {
                        $res = $QPolicyArea->_insert($value);
                        if($res){
                            $this->_redirect('/hr-report/kpi-setting-area?policy_id='.$policy.'');
                            // $db->commit();
                        }
                    } catch (Exception $e){ 
                        // $db->rollback();
                        if ($e->getCode() == 23000) {
                        echo '<script>
                                parent.palert("Mỗi store thuộc Brandshop chỉ có thể có duy nhất 01 Store Leader.");
                                parent.alert("Mỗi store thuộc Brandshop chỉ có thể có duy nhất 01 Store Leader.");
                            </script>';
                        exit;
                        } else {
                            echo '<script>
                                        
                                        parent.alert("'.$e->getMessage().'");
                                    </script>';
                            exit;
                        }
                    }
                    
                }
                
            }
        
            

            
         
        }

       
    

    public function kpiSettingNewAction() {
        $QPolicy = new Application_Model_Policy();
        $this->view->policy = $QPolicy->selectPolicy2(array());
        $QProduct = new Application_Model_Good();
        $this->view->listProduct = $QProduct->get_cache();
        
        if($this->_request->isPost() ){
            
        }
        // $this->view->message = $fm;
    }

    public function getAreaByPolicyAction()
    {
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $this->_helper->layout()->disableLayout();
        
            $product_id = $this->_request->getParam('product_id');
            $QGoodColor = new Application_Model_GoodColor();
            $GoodColor = $QGoodColor->getById($product_id);
        
            if(!empty($GoodColor)){
                $this->view->GoodColor = $GoodColor;
            }
    
        }
    }
}