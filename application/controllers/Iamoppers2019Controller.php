<?php
class Iamoppers2019Controller extends My_Controller_Action
{

	public function indexAction(){
        include 'iamoppers'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'index.php';
	}
	public function playAction(){
        include 'iamoppers'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'play.php';
	}
	public function submitQuestionAction(){
        include 'iamoppers'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'submit-question.php';
	}
	public function getQuestionAction(){
        include 'iamoppers'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'get-question.php';
	}
	public function resultAction(){
        include 'iamoppers'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'result.php';
	}
	public function rewardAction(){
        include 'iamoppers'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'reward.php';
	}
}