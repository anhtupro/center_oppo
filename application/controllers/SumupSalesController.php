<?php

class SumupSalesController extends My_Controller_Action
{
    public function createAction()
    {
        $force = $this->getRequest()->getParam('force');
        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $QBrand = new Application_Model_Brand();
        $QStoreStaffLog = new Application_Model_StoreStaffLog();
        $QSumupSalesDaily = new Application_Model_SumupSalesDaily();

        $histories = $QSumupSalesDaily->getHistory($auth->id, date('Y-m-d'));
        $brands = $QBrand->fetchAll();
        $stores = $QStoreStaffLog->getStoreByStaff($auth->id);

        if ($force === 'true') {
            $date = [
                date('Y-m-d', strtotime("-1 days")),
                date('Y-m-d', strtotime("-2 days")),
                date('Y-m-d', strtotime("-3 days")),
                date('Y-m-d', strtotime("-4 days")),
                date('Y-m-d', strtotime("-5 days")),
                date('Y-m-d', strtotime("-6 days")),
                date('Y-m-d', strtotime("-7 days"))
            ];
            //check báo số chi tiết
            $QSumupSalesDaily = new Application_Model_SumupSalesDaily();
            $remainDetailReportBrand = $QSumupSalesDaily->getListNeedToReport($auth->id, $date);

            //check báo số tổng
            $QSumupSalesDailyTotal = new Application_Model_SumupSalesDailyTotal();
            $remainTotalReportBrand = $QSumupSalesDailyTotal->getListNeedToReport($auth->id, $date);

            $this->view->remainDetailReportBrand = $remainDetailReportBrand;
            $this->view->remainTotalReportBrand = $remainTotalReportBrand;
            $this->view->force = $force;
        }

        $this->view->auth = $auth;
        $this->view->brands = $brands;
        $this->view->stores = $stores;
        $this->view->histories = json_encode($histories);


    }

    public function saveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $store = $this->getRequest()->getParam('store');
        $brand = $this->getRequest()->getParam('brand');
        $product = $this->getRequest()->getParam('product');
        $quantity = $this->getRequest()->getParam('quantity');
        $product_color = $this->getRequest()->getParam('color');
        $note = $this->getRequest()->getParam('note');
        $date = $this->getRequest()->getParam('date');
        $date = $date ? date('Y-m-d', strtotime($date)) : date('Y-m-d');

        $QSumupSalesDaily = new Application_Model_SumupSalesDaily();
        $where = [];
        $where [] = $QSumupSalesDaily->getAdapter()->quoteInto('store_id = ?', $store);
        $where [] = $QSumupSalesDaily->getAdapter()->quoteInto('brand_id = ?', $brand);
        $where [] = $QSumupSalesDaily->getAdapter()->quoteInto('date = ?', $date);
        $row = $QSumupSalesDaily->fetchRow($where);
        if ($row) {
            echo json_encode([
                'status' => 1,
                'message' => 'Bạn đã báo cáo hãng này tại shop rồi! Vui lòng chọn shop/hãng khác'
            ]);
            return;
        }

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        for ($i = 0; $i < count($product); $i++) {
            // check không cho báo số hãng khác 0
            if ($product[$i] == 0 && $quantity[$i] != 0) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Bạn vui lòng báo số từng model'
                ]);
                return;
            }
            $QSumupSalesDaily->insert([
                'good_id' => $product[$i],
                'brand_id' => $brand,
                'quantity' => $quantity[$i],
                'color' => $product_color[$i],
                'date' => $date,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $auth->id,
                'note' => $note,
                'store_id' => $store
            ]);
        }
        if (!$product) {
            $QSumupSalesDaily->insert([
                'store_id' => $store,
                'brand_id' => $brand,
                'date' => $date,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $auth->id,
                'quantity' => 0
            ]);
        }
        $history = $QSumupSalesDaily->getHistory($auth->id, date('Y-m-d'));

        $db->commit();

        echo json_encode([
            'status' => 0,
            'data' => $history
        ]);
    }

    public function saveTotalAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $store = $this->getRequest()->getParam('store');
        $totalQuantity = $this->getRequest()->getParam('total_quantity');
        $date = $this->getRequest()->getParam('date');
        $date = $date ? date('Y-m-d', strtotime($date)) : date('Y-m-d');

        $QSumupSalesDailyTotal = new Application_Model_SumupSalesDailyTotal();
        $where = [];
        $where [] = $QSumupSalesDailyTotal->getAdapter()->quoteInto('store_id = ?', $store);
        $where [] = $QSumupSalesDailyTotal->getAdapter()->quoteInto('date = ?', $date);

        $row = $QSumupSalesDailyTotal->fetchRow($where);
        if ($row) {
            echo json_encode([
                'status' => 1,
                'message' => 'Hôm nay bạn đã báo cáo tổng số shop này rồi. Vui lòng chọn shop khác'
            ]);
            return;
        }

        try {
            $QSumupSalesDailyTotal->insert([
                'store_id' => $store,
                'total_quantity' => $totalQuantity,
                'date' => $date,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $auth->id
            ]);

            echo json_encode([
                'status' => 0
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                'status' => 1,
                'message' => $e->getMessage()
            ]);
        }

    }

    public function getProductByBrandAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        try {
            $brand_id = $this->getRequest()->getParam('brand_id');
            $QPhoneGood = new Application_Model_PhoneGood();
            $QBrand = new Application_Model_Brand();
            $where = [
                'brand_id = ?' => $brand_id,
                'is_deleted = ?' => 0
            ];
            $products = $QPhoneGood->fetchAll($where, 'ordering ASC')->toArray();
            $brand = $QBrand->fetchRow($QBrand->getAdapter()->quoteInto('id = ?', $brand_id));

            echo json_encode([
                'status' => 0,
                'products' => $products,
                'brand' => $brand['name']
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                'status' => 1,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function manageAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $export = $this->getRequest()->getParam('export');
        $store = $this->getRequest()->getParam('store');
        $store_id_number = $this->getRequest()->getParam('store_id_number');
        $staff_code = $this->getRequest()->getParam('staff_code');
        $from_date = $this->getRequest()->getParam('from_date');
        $from_date = $from_date ? date('Y-m-d', strtotime($from_date)) : NULL;
        $to_date = $this->getRequest()->getParam('to_date');
        $to_date = $to_date ? date('Y-m-d', strtotime($to_date)) : NULL;
        $limit = 20;
        $params = [
            'staff_code' => $staff_code,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'store_id' => $store,
            'store_id_number' => $store_id_number
        ];
        $QStore = new Application_Model_Store();
        $list_store = $QStore->fetchAll(
            $QStore->select()
                ->from(['s' => 'store'], ['s.id', 's.name'])
                ->where('s.is_sumup_daily = ?', 1)
        );
        $QSumupSalesDaily = new Application_Model_SumupSalesDaily();
        $statistics_stores = $QSumupSalesDaily->fetchPagination($page, $limit, $total, $params);

        $this->view->list_store = $list_store;
        $this->view->params = $params;
        $this->view->stores = $statistics_stores;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->offset = $limit * ($page - 1);
        $this->view->url = HOST . 'sumup-sales/manage' . '?';

        // export
        if ($export === 'Export') {
            include_once 'PHPExcel.php';

            $QPhoneGood = new Application_Model_PhoneGood();
            $statistics = $QSumupSalesDaily->getStatistics($params);

            $where = [
                'is_deleted = ?' => 0
            ];
            $models = $QPhoneGood->fetchAll($where, ['ordering_group_export ASC', 'ordering ASC'])->toArray();

            $objExcel = new PHPExcel();
            $objExcel->setActiveSheetIndex(0);
            $sheet = $objExcel->getActiveSheet();

            $sheet->setCellValue('A1', 'Dấu thời gian');
            $sheet->setCellValue('B1', 'Date(y/m/d)');
            $sheet->setCellValue('C1', 'Họ và Tên');
            $sheet->setCellValue('D1', 'Khu vực');
            $sheet->setCellValue('E1', 'Shop ID');
            $sheet->setCellValue('F1', 'Shop tổng');
            $sheet->setCellValue('G1', 'OPPO tổng');
            $sheet->setCellValue('H1', 'SAMSUNG tổng');
            $sheet->setCellValue('I1', 'VIVO tổng');
            $sheet->setCellValue('J1', 'HUAWEI tổng');
            // phone detail
            $char = 'K';
            foreach ($models as $model) {
                $sheet->setCellValue($char . 1, $model['name']);
                ++$char;
            }

            // insert data to excel
            $rowCount = 2;
            foreach ($statistics as $statistic) {
                $sheet->setCellValue('A' . $rowCount, $statistic['created_at']);
                $sheet->setCellValue('B' . $rowCount, $statistic['date']);
                $sheet->setCellValue('C' . $rowCount, $statistic['staff_name']);
                $sheet->setCellValue('D' . $rowCount, $statistic['area']);
                $sheet->setCellValue('E' . $rowCount, $statistic['store_id']);
                $sheet->setCellValue('F' . $rowCount, $statistic['total'] ? $statistic['total'] : 0);
                $sheet->setCellValue('G' . $rowCount, $statistic['OPPO'] ? $statistic['OPPO'] : 0);
                $sheet->setCellValue('H' . $rowCount, $statistic['SAMSUNG'] ? $statistic['SAMSUNG'] : 0);
                $sheet->setCellValue('I' . $rowCount, $statistic['VIVO'] ? $statistic['VIVO'] : 0);
                $sheet->setCellValue('J' . $rowCount, $statistic['HUAWEI'] ? $statistic['HUAWEI'] : 0);

                // set detail phone data
                $char = 'K';
                foreach ($models as $model) {
                    $sheet->setCellValue($char . $rowCount, $statistic[$model['name']] ? $statistic[$model['name']] : 0);
                    ++$char;
                }
                ++$rowCount;
            }

            //style sheet
            $style_border = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $sheet->getStyle('A0:' . $char . $rowCount)->applyFromArray($style_border);

            for ($i = 'A'; $i < 'Z'; ++$i) {
                $sheet->getStyle($i . 1)->getFont()->setBold(true);
            }

            //download
            $filename = 'Daily report';
            $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }
    }

    public function testAction()
    {

    }

    public function removeAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $store_id = $this->getRequest()->getParam('store_id');
        $date = $this->getRequest()->getParam('date');

        $QSumupSalesDaily = new Application_Model_SumupSalesDaily();
        $QSumupSalesDailyTotal = new Application_Model_SumupSalesDailyTotal();

        $where = [];
        $where [] = $QSumupSalesDaily->getAdapter()->quoteInto('date = ?', $date);
        $where [] = $QSumupSalesDaily->getAdapter()->quoteInto('store_id = ?', $store_id);

        try {
            $QSumupSalesDaily->delete($where);
            $QSumupSalesDailyTotal->delete($where);

            echo json_encode([
                'status' => 0
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                'status' => 1
            ]);
            return;
        }


    }

    public function createProductAction()
    {
        $QBrand = new Application_Model_Brand();
        $listBrand = $QBrand->fetchAll()->toArray();

        $this->view->listBrand = $listBrand;

    }

    public function saveCreateProductAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $QPhoneGood = new Application_Model_PhoneGood();

        $listBrand = $this->getRequest()->getParam('brand_id');
        $listProductName = $this->getRequest()->getParam('product_name');

        for ($i = 0; $i < count($listBrand); ++$i) {
            switch ($listBrand[$i]) {
                case 1:
                    $groupExport = 1;
                    break;
                case 2:
                    $groupExport = 2;
                    break;
                case 3:
                    $groupExport = 4;
                    break;
                case 4:
                    $groupExport = 3;
                    break;
            }

            $QPhoneGood->insert([
                'name' => $listProductName[$i],
                'brand_id' => $listBrand[$i],
                'ordering' => 0,
                'is_deleted' => 0,
                'ordering_group_export' => $groupExport
            ]);
        }

        echo json_encode([
            'status' => 0
        ]);
    }

    public function listBrandAction()
    {
        $QBrand = new Application_Model_Brand();
        $listBrand = $QBrand->fetchAll()->toArray();

        $this->view->listBrand = $listBrand;
    }

    public function editBrandProductAction()
    {
        $brandId = $this->getRequest()->getParam('brand_id');

        $QPhoneGood = new Application_Model_PhoneGood();

        $listProduct = $QPhoneGood->fetchAll(['brand_id = ?' => $brandId], 'ordering ASC')->toArray();

        $this->view->listProduct = $listProduct;

    }

    public function saveEditBrandProductAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $QPhoneGood = new Application_Model_PhoneGood();


        $listOrdering = $this->getRequest()->getParam('ordering');
        $listIsDeleted = $this->getRequest()->getParam('is_deleted');

        foreach ($listOrdering as $id => $ordering) {
            $where = ['id = ?' => $id];
            $QPhoneGood->update([
                'ordering' => $ordering,
                'is_deleted' => ($listIsDeleted[$id] != Null) ? 0 : 1
            ],$where);
        }

        echo json_encode([
            'status' => 0
        ]);


    }


}