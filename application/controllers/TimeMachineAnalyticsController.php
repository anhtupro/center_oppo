<?php

class TimeMachineAnalyticsController extends My_Controller_Action
{

    public function luoiAction()
    {
        $export = $this->getRequest()->getParam('export');
        $from_date = $this->getRequest()->getParam('from_date', date('1/m/Y'));
        $to_date = $this->getRequest()->getParam('to_date', date('d/m/Y'));

        $params = array(
        );

        $params['from_date'] = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
        $params['to_date'] = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;

        $QCheckIn = new Application_Model_CheckInAnalytics();
        $data = $QCheckIn->xuat_luoi($params);
        
        $category = array();
        foreach ($data as $key => $value) {
            if(!isset($category[$value['code']]['name'][$value['staff_name']])){
                $category[$value['code']]['name'] = $value['staff_name'];
            }

            if(!isset($category[$value['code']]['total_time'])){
                $category[$value['code']]['total_time'] = (floatval($value['cong'])>0)?number_format($value['cong']*$value['time_per_day'], 2):0;
            }
            else{
                $category[$value['code']]['total_time'] += (floatval($value['cong'])>0)?number_format($value['cong']*$value['time_per_day'], 2):0;
            }
        }

        $params = array(
            'from_date' => $from_date,
            'to_date' => $to_date,
        );
        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->category = $category;
    }
}
