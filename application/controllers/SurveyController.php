<?php

class SurveyController extends My_Controller_Action {

    public function indexAction() {
        try {

            $survey_id = $this->getRequest()->getParam('survey_id');

            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $QSurveyToken = new Application_Model_SurveyToken();
            $where = array();
            $where[] = $QSurveyToken->getAdapter()->quoteInto('survey_id = ?', $survey_id);
            $where[] = $QSurveyToken->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);

            $survey_token = $QSurveyToken->fetchRow($where);
            $this->view->survey_id = $survey_id;
            if (!$survey_token) {

                require_once 'My' . DIRECTORY_SEPARATOR . 'jsonRPCClient.php';

                $myJSONRPCClient = new JsonRPCClient(URL_SURVEY . 'admin/remotecontrol');
                $participantData = array('email' => $userStorage->email);
                $sessionKey = $myJSONRPCClient->get_session_key(SURVEY_ADMIN, SURVEY_PASS);

                $rs = $myJSONRPCClient->add_participants($sessionKey, $survey_id, $participantData, true);

                if (!isset($rs['email']['token']) or ! $rs['email']['token'])
                    throw new Exception('Cannot create token');

                $QSurveyToken->insert(array(
                    'survey_id' => $survey_id,
                    'staff_id' => $userStorage->id,
                    'token' => $rs['email']['token'],
                ));

                $myJSONRPCClient->release_session_key($sessionKey);
            }
			
            
			
            $this->view->currentToken = isset($survey_token['token']) ? $survey_token['token'] : (isset($rs['email']['token']) ? $rs['email']['token'] : '');

            $back_url = $this->getRequest()->getServer('HTTP_REFERER');
            $this->view->back_url = $back_url ? $back_url : '/index';
        } catch (Exception $e) {
            $this->view->messages = array($e->getMessage());
        }
    }

    public function listAction() {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QSurveyResponse = new Application_Model_SurveyResponse();
        $where = array();
        $where[] = $QSurveyResponse->getAdapter()->quoteInto('survey_id = ?', 2);
        $where[] = $QSurveyResponse->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
        $this->view->check = $QSurveyResponse->fetchRow($where);

        $this->_helper->viewRenderer->setRender('list-pg');
    }

    public function submitAction() {
        $this->_helper->layout->disableLayout();

        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id))
                throw new Exception("Invalid user", 1);

            $options = $this->getRequest()->getParam('option');
            if (!$options || !is_array($options) || !count($options))
                throw new Exception("Bạn phải điền ít nhất 01 đề xuất", 2);

            foreach ($options as $key => $option)
                $options[$key] = My_String::trim(My_String::UnicodeToHop2DungSan($option));

            $options = array_filter($options);
            if (!count($options))
                throw new Exception("Bạn phải điền ít nhất 01 đề xuất", 3);
            if (count($options) > 3)
                throw new Exception("Bạn chỉ được điền tối đa 03 đề xuất", 4);

            $QSurveyResponse = new Application_Model_SurveyResponse();

            foreach ($options as $key => $option) {
                $data = array(
                    'staff_id' => $userStorage->id,
                    'survey_id' => 1,
                    'survey_queston_id' => 1,
                    'response' => $option,
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $QSurveyResponse->insert($data);
            }

            $this->view->result = "Gửi thành công";
        } catch (Exception $e) {
            $this->view->error = sprintf("[%s] %s", $e->getCode(), $e->getMessage());
        }
    }

    public function submitPgAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setRender('submit');

        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id))
                throw new Exception("Invalid user", 1);

            $q1 = $this->getRequest()->getParam('q1');
            $q2 = $this->getRequest()->getParam('q2');
            $q3 = $this->getRequest()->getParam('q3');
            $q4 = $this->getRequest()->getParam('q4');
            $q5 = $this->getRequest()->getParam('q5');
            $q6 = $this->getRequest()->getParam('q6');

            if (!$q1)
                throw new Exception("Bạn chưa chọn câu 1", 1);
            $q1 = intval($q1);

            if (!$q2)
                throw new Exception("Bạn chưa chọn câu 2", 2);
            $q2 = intval($q2);

            if (!$q3)
                throw new Exception("Bạn chưa chọn câu 3", 3);
            $q3 = intval($q3);

            if (!$q4)
                throw new Exception("Bạn chưa chọn câu 4", 4);
            $q4 = intval($q4);

            if (!$q5)
                throw new Exception("Bạn chưa chọn câu 5", 5);
            $q5 = intval($q5);

            if ($q6)
                $q6 = My_String::trim(My_String::UnicodeToHop2DungSan($q6));

            $QSurveyResponse = new Application_Model_SurveyResponse();
            $options = array($q1, $q2, $q3, $q4, $q5, $q6);

            foreach ($options as $key => $option) {
                $data = array(
                    'staff_id' => $userStorage->id,
                    'survey_id' => 2,
                    'survey_queston_id' => $key + 1,
                    'response' => $option,
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $QSurveyResponse->insert($data);
            }

            $this->view->result = "Gửi thành công";
        } catch (Exception $e) {
            $this->view->error = sprintf("[%s] %s", $e->getCode(), $e->getMessage());
        }
    }

    public function responseAction() {
        $page = $this->getRequest()->getParam('page', 1);
        $limit = LIMITATION;
        $total = 0;
        $params = array();
		
		
			
        $QSurveyResponse = new Application_Model_SurveyResponse();
        $this->view->responses = $QSurveyResponse->fetchPagination($page, $limit, $total, $params);
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->page = $page;
        $this->view->offset = $limit * ($page - 1);
        $this->view->params = $params;
        $this->view->url = HOST . 'survey/response' . ( $params ? '?' . http_build_query($params) . '&' : '?' );

        $QTeam = new Application_Model_Team();
        $this->view->team = $QTeam->get_cache();
    }

    public function doAction() {
        try {
            $survey_id = $this->getRequest()->getParam('survey_id');
            if (!$survey_id || !is_numeric($survey_id) || !intval($survey_id))
                throw new Exception("Invalid ID");

            $survey_id = intval($survey_id);

            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            if (!$userStorage || !isset($userStorage->id))
                $this->_redirect(HOST);

            $QSurveyResponse = new Application_Model_SurveyResponse();
            $where = array();
            $where[] = $QSurveyResponse->getAdapter()->quoteInto('survey_id = ?', $survey_id);
            $where[] = $QSurveyResponse->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $check = $QSurveyResponse->fetchRow($where);

            // if ($check) $this->_redirect(HOST);

            $QSurvey = new Application_Model_Survey();
            $where = $QSurvey->getAdapter()->quoteInto('id = ?', $survey_id);
            $survey = $QSurvey->fetchRow($where);

            if (!$survey)
                throw new Exception("Invalid survey");
            $this->view->survey = $survey;

            // load questions and answers
            $params = array('survey_id' => $survey_id);
            $QSurveyQuestion = new Application_Model_SurveyQuestion();
            $result = $QSurveyQuestion->random($params, null, false);

            $questions = array();

            if (!$result)
                throw new Exception("No question in this survey");

            foreach ($result as $key => $value) {
                if (!isset($questions[$value['question_id']])) {
                    $questions[$value['question_id']] = array(
                        'content' => $value['question'],
                        'response_type' => $value['response_type'],
                        'required' => $value['required'],
                        'options' => array(),
                    );
                }
                $questions[$value['question_id']]['options'][$value['option_id']] = $value['option'];
            }

            $this->view->questions = $questions;
			
			
			
        } catch (Exception $e) {
            $this->view->error = $e->getMessage();
        }
    }

    public function randomAction() {

        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $QSurveyImageByChannel=new Application_Model_SurveyImageByChannel();
            $QStaffMainNew=new Application_Model_StaffMainNew();


			
            $survey_id 	= $this->getRequest()->getParam('survey_id');


            $QStaff=new Application_Model_Staff();

            // $channel_id=$QStaff->getChannelId($userStorage->id);

            $params['staff_id']=$userStorage->id;
            $channel_id=$QStaffMainNew->getListChannelByPgsId($params);

            $params=null;
            $params['survey']=$survey_id;
            $params['channel']=$channel_id;


            $listImageSurvey=$QSurveyImageByChannel->getSurveyImageByChannel($params);
            // echo "<pre>";
            // print_r($listImageSurvey) ;
                    $this->view->listImageSurvey=$listImageSurvey;
            $dev 		= $this->getRequest()->getParam('dev');
            if (!$survey_id || !is_numeric($survey_id) || !intval($survey_id))
                throw new Exception("Invalid ID");

            $survey_id = intval($survey_id);


            if (!$userStorage || !isset($userStorage->id))
                $this->_redirect(HOST);

            $QSurveyResponse = new Application_Model_SurveyResponse();
            $where = array();
            $where[] = $QSurveyResponse->getAdapter()->quoteInto('survey_id = ?', $survey_id);
            $where[] = $QSurveyResponse->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $check = $QSurveyResponse->fetchRow($where);
            // if ($check) $this->_redirect(HOST);

            $QSurvey = new Application_Model_Survey();
            $where = $QSurvey->getAdapter()->quoteInto('id = ?', $survey_id);
            $survey = $QSurvey->fetchRow($where);

            if (!$survey)
                throw new Exception("Invalid survey");
            $this->view->survey = $survey;

            // load questions and answers
            $params = array('survey_id' => $survey_id);
            $QSurveyQuestion = new Application_Model_SurveyQuestion();
            $number_question = 3;
            if ($survey_id == 13) {
                $number_question = 1;
            }
            $result = $QSurveyQuestion->random($params, $number_question, true);
            if($_GET['dev']){
                echo "<pre>";
                print_r($result);
            }

            $questions = array();

            if (!$result)
                throw new Exception("No question in this survey");

            foreach ($result as $key => $value) {
                if (!isset($questions[$value['question_id']])) {
                    $questions[$value['question_id']] = array(
                        'content' => $value['question'],
                        'options' => array(),
                        'response_type' => $value['response_type'],
                    );
                }

                $questions[$value['question_id']]['options'][$value['option_id']] = $value['option'];
            }

            $this->view->questions = $questions;
            $this->view->dev = $dev;
        } catch (Exception $e) {
            $this->view->error = $e->getMessage();
			
        }
    }

    public function saveAction() {
        try {
            $this->_helper->layout->disableLayout();

            $id = $this->getRequest()->getParam('id');
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id))
                throw new Exception("Invalid user");

            if (!$id || !is_numeric($id) || !intval($id))
                throw new Exception("Invalid ID");

            $id = intval($id);
            $count = 1;

            $QSurveyQuestion = new Application_Model_SurveyQuestion();
            $QSurveyQuestionOption = new Application_Model_SurveyQuestionOption();
            $QSurveyResponse = new Application_Model_SurveyResponse();
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            $responses = $check_order = array();

            $where = $QSurveyQuestion->getAdapter()->quoteInto('survey_id = ?', $id);
            $all_question = $QSurveyQuestion->fetchAll($where);


            foreach ($all_question as $key => $value){
                $responses[$value['id']] = $this->getRequest()->getParam('q_' . $value['id'], array());
                $check_order[$value['id']] = $this->getRequest()->getParam('order_' . $value['id'], array());
            }    

            foreach ($responses as $_id => $value) {
                if(is_array($value)){//trường hợp 1 câu hỏi có nhiều checkbox
                    foreach ($value as $k1 => $vl_option) {

                        $data = array(
                            'staff_id' => $userStorage->id,
                            'survey_id' => $id,
                            'survey_question_id' => $_id,
                            'response' => $vl_option,
                            'created_at' => date('Y-m-d H:i:s'),
                        );                        
                        if(!empty($check_order[$_id][$k1])){
                            $data['checkorder'] = $check_order[$_id][$k1];// dùng cho câu hỏi chọn nhiều đáp án. có sắp xếp. (type=checkorder)
                        }

                        $QSurveyResponse->insert($data);
                    }
                }else{//trường hợp có 1 đáp án
                    $data = array(
                        'staff_id' => $userStorage->id,
                        'survey_id' => $id,
                        'survey_question_id' => $_id,
                        'response' => $value,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $QSurveyResponse->insert($data);
                }
            }

            
            
            $db->commit();

            $this->view->result = "Chào mừng bạn trở lại hệ thống!";
        } catch (Exception $e) {
            $db->rollback();
            $this->view->error = $e->getMessage();
        }
    }

    public function saveRandomAction() {
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $this->_helper->layout->disableLayout();
            $id = $this->getRequest()->getParam('id');
            $question_ids = $this->getRequest()->getParam('question_id');
            $dev = $this->getRequest()->getParam('dev');
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id))
                throw new Exception("Invalid user");

            if (!$id || !is_numeric($id) || !intval($id))
                throw new Exception("Invalid ID");

            $id = intval($id);
            $count = 1;

            $QSurveyQuestionOption = new Application_Model_SurveyQuestionOption();
            $QSurveyResponse = new Application_Model_SurveyResponse();

            $responses = array();

		
                foreach ($question_ids as $question_id) {
                    $responses[$question_id] = $this->getRequest()->getParam('q_' . $question_id);
                }

                foreach ($responses as $key => $value) {
                    $where = array();
                    $where[] = $QSurveyQuestionOption->getAdapter()->quoteInto('survey_question_id = ?', intval($key));
                    $where[] = $QSurveyQuestionOption->getAdapter()->quoteInto('TRIM(`option`) = ?', trim($value));
                    $where[] = $QSurveyQuestionOption->getAdapter()->quoteInto('`true` = ?', 1);
                    $check = $QSurveyQuestionOption->fetchRow($where);
			
                    if (!$check) {
                        $this->view->reset = 1;
                        if(!$dev){
                        	throw new Exception("Chưa đúng, thử lại nào!");
                        }
                        
                    }

                    $data = array(
                        'staff_id' => $userStorage->id,
                        'survey_id' => $id,
                        'survey_question_id' => $key,
                        'response' => $value,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $QSurveyResponse->insert($data);
                }

            $db->commit();

            $this->view->result = "Chào mừng bạn trở lại hệ thống!";
        } catch (Exception $e) {
            $db->rollback();
            $this->view->error = $e->getMessage();
        }
    }

    public function createAction() {
        
    }

    public function saveSurveyAction() {
        $this->_helper->layout->disableLayout();
        $post_data = $this->getRequest()->getParam('data');
        $questions = $this->getRequest()->getParam('questions');
        $QSurveyQuestion = new Application_Model_SurveyQuestion();
        $QSurveyQuestionOption = new Application_Model_SurveyQuestionOption();

        $post_data = $post_data['questions'];
        foreach ($questions as $key => $value) {
            $data = array(
                'question' => $value['question'],
                'response_type' => 'radio',
            );

            $survey_question_id = $QSurveyQuestion->insert($data);

            if (isset($value['options']) && is_array($value['options'])) {

                foreach ($value['options'] as $_key => $option) {
                    $data = array(
                        'survey_question_id' => $survey_question_id,
                        'option' => $option,
                    );

                    $QSurveyQuestionOption->inset($data);
                }
            }
        }

        exit('1');
    }
    
    
    public function devAction() {
        $db = Zend_Registry::get('db');

        try {
            $this->_helper->layout->disableLayout();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $id = $this->getRequest()->getParam('survey_id');

            $QSurveyResponse = new Application_Model_SurveyResponse();

            $where = array();
            $where[] = $QSurveyResponse->getAdapter()->quoteInto('staff_id = ?',  $userStorage->id);
            $where[] = $QSurveyResponse->getAdapter()->quoteInto('survey_id = ?', intval($id));
            $where[] = $QSurveyResponse->getAdapter()->quoteInto('DATE(created_at) = ?', date ( 'Y-m-d'));
            $check = $QSurveyResponse->fetchRow($where);

            if(empty($check)){
                            
                $data_dev = array(
                    'staff_id' => $userStorage->id,
                    'survey_id' => $id,
                    'survey_question_id' => 1,
                    'response' => "DEV",
                    'created_at' => date('Y-m-d H:i:s'),
                );     
               $QSurveyResponse->insert($data_dev);
               
            }
            $this->redirect(HOST);
            exit;
        } catch (Exception $e) {
            $this->view->error = $e->getMessage();
        }
    }
    
    public function formBuilderCreateAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-create.php';
    }
    
    public function formBuilderSaveAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-save.php';
    }
    
    public function formBuilderViewAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-view.php';
    }
	
    public function formBuilderPreviewAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-preview.php';
    }
	
    public function formBuilderListAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-list.php';
    }
    
    public function downloadFileAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'download-file.php';
    }
    public function saveCollectAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'save-collect.php';
    }
	
	public function formBuilderEditAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-edit.php';
    }
	
	public function formBuilderSubmitAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-submit.php';
    }
	
	public function formSubmitSaveAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-submit-save.php';
    }
		
	public function formBuilderActiveAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-active.php';
    }
		
	public function formBuilderEndAction(){
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-end.php';
    }
	
    public function addSurveyAccess($params){
        
    	$db = Zend_Registry::get('db');

    	//List All Staff_id
    	$user_ids		= array();
    	if($params['all_staff'] == 1){
    		   $select_Staff 	= $db->select() ->from(array('p' => 'staff'),array('p.id'))
    		   									->where('p.status = 1');
    		       
    		   $user_ids 		=	$db->fetchAll($select_Staff);
    	}
    	
//     	List Staff_id with Title
    	$user_ids_with_title = array();
    	if(!empty($params['title_objects'])){
    		$select_ids_Title 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Title->where('p.title IN (?)',$params['title_objects'])
    						->where('p.status = 1');
    		$user_ids_with_title 	=	$db->fetchAll($select_ids_Title);
    	}
    	
    	
//     	List Staff_id with area
    	$user_ids_with_area = array();
    	if(!empty($params['area_objects'])){
    		$select_ids_Area 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Area->joinInner(array('rm' => 'regional_market'), 'p.regional_market=rm.id', array());
    		$select_ids_Area->where('rm.area_id IN (?)',$params['area_objects'])
    						->where('p.status = 1');
    		$user_ids_with_area 	=	$db->fetchAll($select_ids_Area);
    		 
    	}
    	
    	//     	List Staff_id with Company
    	$user_ids_with_company = array();
        $user_company=[];
    	if(!empty($params['company_objects'])){
    		$select_ids_Company 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Company->where('p.company_id IN (?)',$params['company_objects'])
    							->where('p.status = 1');
    		$user_ids_with_company 	=	$db->fetchAll($select_ids_Company);
                if(!empty($user_ids_with_company))
                foreach ($user_ids_with_company as $k => $list_user_id){
    			$user_company[]		= $list_user_id['id'];
    		}
    	}
    	//     	List Staff_id width pic
    	$user_ids_with_pic = array();
    	if(!empty($params['staff'])){
    		$user_ids_with_pic 	=	$params['staff'];
    	}
    	$list_users=array();
    	if($params['type'] == 1){
    		$list_user_ids =(array_merge($user_ids,$user_ids_with_title,$user_ids_with_area)) ;
    		
    		foreach ($list_user_ids as $k => $list_user_id){
    			$list_users[]		= $list_user_id['id'];
    		}
    	}
    	//$list_user_ids =(array_merge($user_ids,$user_ids_with_title,$user_ids_with_area)) ;
    	
    	if($params['type'] == 2){
    		if($user_ids_with_title){
    			foreach ($user_ids_with_title as $k => $val){
    				$titles[]		= $val['id'];
    			}
    		}
    		
    		if($user_ids_with_area){
    			foreach ($user_ids_with_area as $k => $val){
    				$areas[]		= $val['id'];
    			}
    		}
    		
    		$list_users = array_intersect($titles,$areas);
    	}
    	
    	
            
    	$list_user_ids = array_unique(array_merge($list_users,$user_ids_with_pic));
       
    	if(!empty($params['company_objects'])){
            $list_user_ids=array_intersect($list_user_ids,$user_company);
        }
    	
    	
    	if($list_user_ids){
               
                foreach ($list_user_ids as $user){
                    $data['staff_id'] = $user;
                    $data['survey_id'] = $params['survey_id'];
                    $all_staff[] = $data;
                }
				/*
				
				
			                // delete before insert    
                $QSurveyAccess = new Application_Model_SurveyAccess();    
                $survey_id = $params['survey_id'];
                $where = $QSurveyAccess->getAdapter()->quoteInto('survey_id = ?', $survey_id);   
                $QSurveyAccess->delete($where);	
                */
    		My_Controller_Action::insertAllrow($all_staff, 'survey_access');
    	}else{
    		throw new Exception("Cannot Save, Please choose Staff ");
    	}
    	
    
    }

    public function formBuilderCopyAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-copy.php';
    }

       // a toan
    public function massUploadSurveyAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'mass-upload-survey.php';  
    }
    public function saveMassUploadSurveyAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'save-mass-upload-survey.php';      
    }
    public function setTitleAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'set-title.php';      
        
    }
    public function saveSetTitleAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'save-set-title.php';      
    }
    public function listSurveyAction()
    {
        $QSurvey = new Application_Model_Survey();
        $page = $this->getRequest()->getParam('page', 1);
        $limit = 10;

        $list_survey =  $QSurvey->fetchPagination($page, $limit, $total, $params);

        $this->view->list_survey = $list_survey;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->offset = $limit * ($page - 1);
        $this->view->url = HOST . 'survey/list-survey' . '?';

        $flashMessenger  = $this->_helper->flashMessenger;
        $successes             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->successes = $successes;

        $this->renderScript('survey/list-survey.phtml');    
    }

    public function toggleStatusAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $status = $this->getRequest()->getParam('status');
        $surveyId = $this->getRequest()->getParam('survey_id');

        $QSurvey = new Application_Model_Survey();

        if (! $surveyId) {
            echo json_encode([
                'status' => 1,
                'message' => 'Thiếu ID survey!'
            ]);
            return;
        }

        // if ($status == 1) {
        //     $surveyIsOn = $QSurvey->fetchRow(['status = ?' => 1]);

        //     if ($surveyIsOn) {
        //         echo json_encode([
        //             'status' => 1,
        //             'message' => 'Survey ' . $surveyIsOn['title'] . ' đang chạy, vui lòng tắt trước khi bật Survey khác.'
        //         ]);
        //         return;
        //     }
        // }

        $QSurvey->update([
            'status' => $status ? $status : 0
        ], ['id = ?' => $surveyId]);

        echo json_encode([
            'status' => 0
        ]);

    }

    public function formBuilderDownloadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $survey_history_id = $this->getRequest()->getParam('survey_history_id');


        $QSurveyFile = new Application_Model_SurveyFile();
        $where_file = [];
        $where_file [] = $QSurveyFile->getAdapter()->quoteInto('survey_history_id = ?', $survey_history_id);
        $files = $QSurveyFile->fetchAll($where_file, null, null, 'file')->toArray();


        foreach ($files as $file) {
            $arrFile [] = $file['url'];
        }

        $pathDir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'survey' . DIRECTORY_SEPARATOR. 'trainer' .DIRECTORY_SEPARATOR. $survey_history_id;


        $zip_file_temp = tempnam("/tmp", '');
        $zip = new ZipArchive();
        $zip->open($zip_file_temp, ZipArchive::OVERWRITE);
        $dir = opendir($pathDir);

        while ($file = readdir($dir)) {
            if (is_file($pathDir . DIRECTORY_SEPARATOR . $file) ) {
                $zip->addFile($pathDir . DIRECTORY_SEPARATOR . $file, $file);
            }
        }
        $zip->close();

        //download file
        $download_filename = 'Survey' . '.zip';
        header("Content-Type: application/zip");
        header("Content-Length: " . filesize($zip_file_temp));
        header("Content-Disposition: attachment; filename=\"" . $download_filename . "\"");

        readfile($zip_file_temp);
        unlink($excel_file_temp);
        unlink($zip_file_temp);
        exit;
    }

    public function formBuilderSetTitleAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-set-title.php';
    }

    public function formBuilderSaveSetTitleAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-save-set-title.php';
    }

    public function formBuilderMassUploadStaffAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-mass-upload-staff.php';
    }
    public function formBuilderSaveMassUploadStaffAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-save-mass-upload-staff.php';
    }

    public function formBuilderExportStaffAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-export-staff.php';
    }

    public function formBuilderSetTimeAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-set-time.php';
    }

    public function formBuilderSaveSetTimeAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'form-builder-save-set-time.php';
    }
    public function imageSurveyAction()
    {
        require_once 'survey' . DIRECTORY_SEPARATOR . 'image-survey.php';
    }






}
