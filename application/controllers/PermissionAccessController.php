<?php

class PermissionAccessController extends My_Controller_Action
{
    //Group Basic Level 1
    public function listGroupBasicAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'list-group-basic.php';
    }
    
    public function createGroupBasicAction()
    {
        require_once 'permission-access'.DIRECTORY_SEPARATOR.'create-group-basic.php';
    }

    public function saveGroupBasicAction()
    {
        require_once 'permission-access'.DIRECTORY_SEPARATOR.'save-group-basic.php';
    }

    public function delGroupBasicAction(){
        require_once 'permission-access'.DIRECTORY_SEPARATOR.'del-group-basic.php';
    }

    //Group Level 2
    public function listGroupAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'list-group.php';
    }

    public function createGroupAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'create-group.php';
    }

    public function saveGroupAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'save-group.php';
    }

    public function delGroupAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'del-group.php';
    }

    //Create group mapping staff
    public function createGroupStaffAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'create-group-staff.php';
    }

    public function saveGroupStaffAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'save-group-staff.php';
    }

    //Create group mapping title
    public function createGroupTitleAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'create-group-title.php';
    }

    public function saveGroupTitleAction()
    {
        require_once 'permission-access' . DIRECTORY_SEPARATOR . 'save-group-title.php';
    }

    private function generate_list($group_menus) {
        return $this->ul(0, '', $group_menus);
    }

    function ul($parent = 0, $attr = '', $group_menus = null) {
        static $i = 1;
        $indent = str_repeat("\t\t", $i);
        if (isset($this->data[$parent])) {
            if ($attr) {
                $attr = ' ' . $attr;
            }
            $html = "\n$indent";
            $html .= "<ul$attr>";
            $i++;
            foreach ($this->data[$parent] as $row) {
                $child = $this->ul($row['id'], '', $group_menus);
                $html .= "\n\t$indent";
                $html .= '<li>';
                $html .= '<input value="'.$row['id'].'" '.( ($group_menus and in_array($row['id'] , $group_menus)) ? 'checked' : '' ).' type="checkbox" id="menus_'.$row['id'].'" name="menus[]"><label>'.$row['label'].'</label>';
                if ($child) {
                    $i--;
                    $html .= $child;
                    $html .= "\n\t$indent";
                }
                $html .= '</li>';
            }
            $html .= "\n$indent</ul>";
            return $html;
        } else {
            return false;
        }
    }

    private function add_row($id, $parent, $label) {
        $this->data[$parent][] = array('id' => $id, 'label' => $label);
    }
}
