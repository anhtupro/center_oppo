<?php
/**
 * Created by PhpStorm.
 * User: Hac
 * Date: 19/07/2016
 * Time: 1:55 PM
 * descripton: file này được tạo ra để upload file excel vào hệ thống trong trường hợp ko có tool
 */

class ImportController extends My_Controller_Action
{

    public function transferPgAction(){
        $check = $this->getRequest()->getParam('check',0);
        if($check == 0 || date('Y-m-d') != '2016-07-20'){
            echo 'vui lòng check điều kiện';
            exit;
        }

        $db = Zend_Registry::get('db');
        $sql = "
            SELECT a.code, b.id as staff_id, c.`name`, c.id as regional_market, c.area_id as area_id, a.date as from_date,
                d.id as title, e.id as team, f.id as department, b.company_id, b.group_id
            FROM upgrade_senior a
            inner join staff b ON a.`code` = b.`code` COLLATE utf8_unicode_ci
            inner join regional_market c ON c.id = b.regional_market
            inner join team d ON d.id = 293
            inner join team e ON e.id = d.parent_id
            inner JOIN team f ON f.id = e.parent_id
            WHERE b.title = 182 and a.code NOT IN (14050153)
            ";
        $stmt = $db->query($sql);
        $result  = $stmt->fetchAll();

        try{
            foreach($result as $item){
                $transfer_data = array();
                $transfer_data['staff_id']    = $item['staff_id'];
                $transfer_data['from_date']   = $item['from_date'];
                $transfer_data['transfer_id'] = 0;
                $transfer_data['info'] = array(
                    My_Staff_Info_Type::Area       => $item['area_id'],
                    My_Staff_Info_Type::Region     => $item['regional_market'],
                    My_Staff_Info_Type::Company    => $item['company_id'],
                    My_Staff_Info_Type::Department => $item['department'],
                    My_Staff_Info_Type::Team       => $item['team'],
                    My_Staff_Info_Type::Group      => $item['group_id'],
                    My_Staff_Info_Type::Title      => $item['title']);
                $option = 'add';
                $result = My_Staff_Log::transfer($item['staff_id'], $transfer_data, $option);
                if($result['status']==0){
                    echo $item['code'].'<br/>';
                    //throw new Exception($result['message'].'-'. $item['code']);
                }

            }

            echo 'Done';
            exit;
        }catch (Exception $e){

            echo $e->getMessage();
            exit;
        }

    }
}