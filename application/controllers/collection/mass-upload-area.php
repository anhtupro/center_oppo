<?php

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->view->messages_success = $messages_success;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$id_parent = My_Util::escape_string($this->getRequest()->getParam('id'));
$QMassUpload = new Application_Model_MassUpload();
$QMassUploadData = new Application_Model_MassUploadData();
$QMassUploadAccess = new Application_Model_MassUploadAccess();
$QAsm = new Application_Model_Asm();
$QArea = new Application_Model_Area();
$QTeam = new Application_Model_Team();

if (!is_numeric($id_parent)) {
    $this->redirect('/collection/mass-download-data');
}
$data_massupload = $QMassUpload->fetchRow(['id = ?' => $id_parent]);
if (empty($data_massupload) || $data_massupload->status_final == 1) {
    $this->redirect('/collection/mass-download-data');
}
$info_team = $QTeam->fetchRow(['id = ?' => $userStorage->title]);
$data_permission = $QMassUploadAccess->fetchRow(['id_massupload = ?' => $id_parent , 'id_group_access = ?' => $info_team->access_group]);
if(empty($data_permission) && $info_team->access_group <> ADMINISTRATOR_ID){
    $this->redirect('/collection/mass-download-data');
}
if ($this->getRequest()->getMethod() == 'POST') {
    $save_folder = 'template/mass-upload-area';
    $new_file_path = '';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 5000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        // xoa neu truoc do da upload
        $where = [];
        $where[] = $QMassUpload->getAdapter()->quoteInto('parent_id = ?', $id_parent);
        $where[] = $QMassUpload->getAdapter()->quoteInto('created_by = ?', $userStorage->id);
        $where[] = $QMassUpload->getAdapter()->quoteInto('del = ?', 0);
        $QMassUpload->update(['del' => 1], $where);
        
        $file = My_File::get($save_folder, $requirement, true);
        if (!$file || !count($file)) {
            throw new Exception("Upload failed");
        }

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . $file["filename"];
        $clone_path = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . "template_" . $file["filename"];

        //Clone file
        copy($uploaded_dir, $clone_path);
        include 'PHPExcel/IOFactory.php';
        $url = DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . $file['filename'];
        $url_template = DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . "template_" . $file["filename"];
        $inputFileType = PHPExcel_IOFactory::identify($clone_path);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($clone_path);
        $index = 9;
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $objPHPExcel->getActiveSheet()->removeRow($index, $highestRow);
        $inputFileName = '.' . $url;
        try {
            $inputFileType_ = PHPExcel_IOFactory::identify($inputFileName);
            $objReader_ = PHPExcel_IOFactory::createReader($inputFileType_);
            $objPHPExcel_ = $objReader_->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        $sheet_ = $objPHPExcel_->getSheet(0);
        if($objPHPExcel_->getSheetCount() != 1){
            throw new Exception("The number of sheet is only one!");
        }
        $highestRow_ = $sheet_->getHighestRow();
        $highestColumn = $sheet_->getHighestColumn();
        for ($row = 9; $row <= $highestRow_; $row++) {
            $rowData[] = $sheet_->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    NULL,
                    TRUE,
                    TRUE);
        }

        foreach ($rowData as $key => $value) {
            $dt[] = $value[0];
        }
        $params = array(
            'parent_id' => $id_parent,
            'url' => $url,
            'url_template' => $url_template,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id,
        );
        $id = $QMassUpload->insert($params);

        $data_massupload = [];
        $list_area_upload = [];
        foreach ($dt as $k => $v) {
            /*if (empty($v[1])) {
                throw new Exception("Check file upload. Please delete the blank rows!");
            }*/
            if(!empty($v[1])){
                $list_area_upload[] = $v[1];
                if(empty(json_encode($v, JSON_UNESCAPED_UNICODE))){
                    throw new Exception("Vui lòng chuyển thông tin file upload sang kiểu TEXT và không bao gồm công thức tính !");
                }
                $mass_data = array(
                    'massupload_id' => $id,
                    'area' => $v[1],
                    'json' => json_encode($v, JSON_UNESCAPED_UNICODE),
                );
                $mass_data['json'] = str_replace("'", "\'", $mass_data['json']);
                $data_massupload[] = $mass_data;
            }
        }

        $area_cache = $QArea->get_cache();
        $area = [];
        if ($info_team->access_group == My_Staff_Group::ASM || $info_team->access_group == My_Staff_Group::SALES_ADMIN) {
            $area_list = $QAsm->get_cache($userStorage->id)['area'];
            foreach ($area_list as $key => $value) {
                $area[$value] = $area_cache[$value];
            }
        } else {
            $area = $area_cache;
        }
        
        if (!empty(array_diff($list_area_upload, $area))) {
            throw new Exception("Check area upload !");
        }
        My_Controller_Action::insertAllrow($data_massupload, 'massupload_data');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
        $objWriter->save($clone_path);
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Đã tải file thành công');
    } catch (Exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/collection/mass-upload-area?id=' . $id_parent);
    }
    $this->redirect('/collection/mass-upload-area?id=' . $id_parent);
}

$this->view->data_upload = $data_massupload;
