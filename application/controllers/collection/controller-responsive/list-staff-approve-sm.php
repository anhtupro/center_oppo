<?php
    $this->_helper->viewRenderer->setRender('view-responsive/list-staff-approve-clone');
    $not_approve_self = array(LEADER_ID,SALES_ID, PB_SALES_ID, PGPB_ID);
    $page       = $this->getRequest()->getParam('page', 1);
    $name       = $this->getRequest()->getParam('name');
    $code       = $this->getRequest()->getParam('code');
    $email      = $this->getRequest()->getParam('email');
    $export     = $this->getRequest()->getParam('export');
    $only_training = $this->getRequest()->getParam('only_training', '0');
    $month      = $this->getRequest()->getParam('month', date('m'));
    $year       = $this->getRequest()->getParam('year', date('Y'));
    $area       = $this->getRequest()->getParam('area');
    $department = $this->getRequest()->getParam('department');
    $team       = $this->getRequest()->getParam('team');
    $title      = $this->getRequest()->getParam('title');
    $note       = $this->getRequest()->getParam('note');
    $limit      = 20;
    //Check locked time
    $QLockTime      = new Application_Model_LockTime();
    $where_lock     = array ();
    $where_lock[]   = $QLockTime->getAdapter ()->quoteInto ( 'month = ?', $month );
    $where_lock[]   = $QLockTime->getAdapter()->quoteInto('year = ? ', $year);
    $result_lock    = $QLockTime->fetchRow ( $where_lock );
    $this->view->locked_time = $result_lock;
    //End Check locked time
    
    $QShift     = new Application_Model_Shift();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QTime      = new Application_Model_Time2();
    $QStaff     = new Application_Model_Staff();
    $QTeam      = new Application_Model_Team();
    $note       = $this->getRequest()->getParam('note', null);
    $QLeaveDetail = new Application_Model_LeaveDetail();
	
    $QTimeGps = new Application_Model_TimeGps();
    $QTimeLastMonth = new Application_Model_TimeLastMonth();
		
    $export     = $this->getRequest()->getParam('export');
    $approved   = $this->getRequest()->getParam('approved', 0);
    $notapproved = $this->getRequest()->getParam('notapproved', 0);
    $hr_approved = $this->getRequest()->getParam('hr_approved', 0);
    $delete      = $this->getRequest()->getParam('delete');
    $approve_all_by_staff   = $this->getRequest()->getParam('approve_all_by_staff');
    $deapprove_all_by_staff = $this->getRequest()->getParam('deapprove_all_by_staff');
    $date_approve           = $this->getRequest()->getParam('date-approve');
    $date_none_approve      = $this->getRequest()->getParam('date-none-approve');
    $date_approve_last = $this->getRequest()->getParam('date-approve-last'); 
    $date_none_approve_last = $this->getRequest()->getParam('date-none-approve-last'); 
		
    $QTimeGps = new Application_Model_TimeGps();
    $QTimeLastMonth = new Application_Model_TimeLastMonth();
    $approve_all_by_staff_last = $this->getRequest()->getParam('approve_all_by_staff_last');
    $deapprove_all_by_staff_last = $this->getRequest()->getParam('deapprove_all_by_staff_last');
    
    $QTeam  = new Application_Model_Team();
    $staff_title_info = $userStorage->title;
    $team_info = $QTeam->find($staff_title_info);
    $team_info = $team_info->current();
    $group_id = $team_info['access_group'];
    $this->view->group_id = $group_id;

    $this->view->auth_id = $userStorage->id;
    if(in_array($userStorage->group_id, $not_approve_self))
    {
        $this->view->not_approve_self = 1;
    }
    else
    {
        $this->view->not_approve_self = 0;
    }

    $this->view->auth_id = $userStorage->id;

    
    
    if(!empty($date_approve))
    {
        $staff_approve = $this->getRequest()->getParam('staff_approve');
        $params_approve_date = array(
            'staff_id' => $staff_approve,
            'date' => $date_approve,
            'admin' => $userStorage->id,
            'note' => ''
        );
        $QTime->approveAllTempTimeByStaffDate($params_approve_date);
    }

    if(!empty($date_none_approve))
    {

        $staff_approve = $this->getRequest()->getParam('staff_approve');
        $params_approve_date = array(
            'staff_id' => $staff_approve,
            'date' => $date_none_approve,
            'admin' => $userStorage->id,
            'note' => ''
        );
        $QTime->nonapproveAllTempTimeByStaffDate($params_approve_date);
    }

    if(!empty($deapprove_all_by_staff))
    {
        $month_edit = $this->getRequest()->getParam('month_edit');
        $year_edit = $this->getRequest()->getParam('year_edit');

        $params_approve_all = array(
            'staff_id' => $deapprove_all_by_staff,
            'month' => $month_edit,
            'year' => $year_edit,
            'admin' => $userStorage->id,
            'note' => ''
        );
        $QTime->deapproveAllTempTimeByStaff($params_approve_all);
    }

    if(!empty($approved))
    {
        $params_approved = array(
            'status' => 1,
            'id' => $approved,
            'note' => $note,
        );
        $QLeaveDetail->updateStatus($params_approved);
        
        $where_info_leave     = $QLeaveDetail->getAdapter()->quoteInto("id = $approved");
        $res_leave =  $QLeaveDetail->fetchRow($where_info_leave);
        if(!empty($res_leave)){
            $staff_id = $res_leave['staff_id'];
            if($res_leave['total'] > 13 AND $res_leave['leave_type'] == 17 AND $res_leave['from_date'] >= date('Y-m-d')){
               
                $data_update_staff = array(
                        'status'    => 3,
                ); 
                
                if(!empty($staff_id)){
                    $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                    $QStaff->update($data_update_staff,$where_staff);
                }
                
            }
            if($res_leave['total'] > 13 AND !in_array($res_leave['leave_type'], array(2,17)) AND $res_leave['from_date'] >= date('Y-m-d')){
                 
                $data_update_staff = array(
                    'status'    => 2,
                );
            
                if(!empty($staff_id)){
                    $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                    $QStaff->update($data_update_staff,$where_staff);
                }
            
            }
        }
    }

    if(!empty($notapproved))
    {
        $params_notapproved = array(
            'status' => 2,
            'id' => $notapproved,
            'note' => $note,
        );
        $QLeaveDetail->updateStatus($params_notapproved);
    }

    if(!empty($hr_approved))
    {
        $params_hr_approved = array(
            'status' => 1,
            'id' => $hr_approved,
        );
        $QLeaveDetail->updateHrStatus($params_hr_approved);
    }

    if(!empty($approve_all_by_staff))
    {
        $month_edit = $this->getRequest()->getParam('month_edit');
        $year_edit = $this->getRequest()->getParam('year_edit');

        $params_approve_all = array(
            'staff_id' => $approve_all_by_staff,
            'month' => $month_edit,
            'year' => $year_edit,
            'admin' => $userStorage->id,
            'note' => ''
        );
        $QTime->approveAllTempTimeByStaff($params_approve_all);
//         $QTime->approveAllLateTimeByStaff($params_approve_all);
    }
         /// approve all by staff công tháng trước
        if(!empty($approve_all_by_staff_last))
        {    
            $month_edit = $this->getRequest()->getParam('month_edit');
            $year_edit = $this->getRequest()->getParam('year_edit'); 
            $work_day_last = $this->getRequest()->getParam('work_day_last'); 
            $month_required = $year_edit . '-' . $month_edit . '-01';
            $work_day_last_string = $work_day_last[$approve_all_by_staff_last]; 
            
            // update bảng  temp_time             
            $where_app_Lasttime = array ();
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter ()->quoteInto ( "staff_id = ?", $approve_all_by_staff_last);
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("status = 0");
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("DATE(date) IN ($work_day_last_string)"); 
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto( "DATE(month_required) = ?", $month_required );
            $result_appr_LastTime = $QTimeLastMonth->fetchAll($where_app_Lasttime);

            
            if(!empty($result_appr_LastTime[0])){
                $appr_LastTime = array();
                $appr_LastTime = array(
                    'status' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),            
                    'approved_at' => date('Y-m-d H:i:s'),
                    'approved_by' => $userStorage->id
                );
                $QTimeLastMonth->update($appr_LastTime,$where_app_Lasttime);
            }
            
        }// End appr all

        
        /// Huỷ yêu cầu bổ sung công tháng trước
        if(!empty($deapprove_all_by_staff_last))
        {
            $month_edit = $this->getRequest()->getParam('month_edit');
            $year_edit = $this->getRequest()->getParam('year_edit'); 
            $work_day_last = $this->getRequest()->getParam('work_day_last'); 
            $month_required = $year_edit . '-' . $month_edit . '-01';
            $work_day_last_string = $work_day_last[$deapprove_all_by_staff_last]; 
            
            // update bảng  temp_time             
            $where_app_Lasttime = array ();
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter ()->quoteInto ( "staff_id = ?", $deapprove_all_by_staff_last);
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("status = 0");
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("DATE(date) IN ($work_day_last_string)"); 
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto( "DATE(month_required) = ?", $month_required );
            $result_appr_LastTime = $QTimeLastMonth->fetchAll($where_app_Lasttime);

            
            if(!empty($result_appr_LastTime[0])){
                $appr_LastTime = array();
                $appr_LastTime = array(
                    'status' => 2,         
                    'approved_at' => date('Y-m-d H:i:s'),
                    'approved_by' => $userStorage->id
                );
                $QTimeLastMonth->update($appr_LastTime,$where_app_Lasttime);
            }
            
        }//End Huỷ yêu cầu bổ sung công tháng trước
		
		/// approve all by staff công tháng trước từng ngày
        if(!empty($date_approve_last))
        {               
            $staff_approve = $this->getRequest()->getParam('staff_approve');
                        
            $month_edit = $this->getRequest()->getParam('month_edit');
            $year_edit = $this->getRequest()->getParam('year_edit'); 
            $work_day_last = $this->getRequest()->getParam('work_day_last');  
            
            // update bảng  temp_time             
            $where_app_Lasttime = array ();
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter ()->quoteInto ( "staff_id = ?", $staff_approve);
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("status = 0");
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("DATE(date) = ?", $date_approve_last); 
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto( "DATE(month_required) = ?", date('Y-m-01') );
            $result_appr_LastTime = $QTimeLastMonth->fetchAll($where_app_Lasttime);
 
            if(!empty($result_appr_LastTime[0])){
                $appr_LastTime = array();
                $appr_LastTime = array(
                    'status' => 1,          
                    'approved_at' => date('Y-m-d H:i:s'),
                    'approved_by' => $userStorage->id
                );
                $QTimeLastMonth->update($appr_LastTime,$where_app_Lasttime);
            }
            
        }// End appr 
        
        ///  none approve all by staff công tháng trước từng ngày
        if(!empty($date_none_approve_last))
        {               
            $staff_approve = $this->getRequest()->getParam('staff_approve');
                        
            $month_edit = $this->getRequest()->getParam('month_edit');
            $year_edit = $this->getRequest()->getParam('year_edit'); 
            $work_day_last = $this->getRequest()->getParam('work_day_last');  
            
            // update bảng  temp_time             
            $where_app_Lasttime = array ();
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter ()->quoteInto ( "staff_id = ?", $staff_approve);
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("status = 0");
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto("DATE(date) = ?", $date_none_approve_last); 
            $where_app_Lasttime[] = $QTimeLastMonth->getAdapter()->quoteInto( "DATE(month_required) = ?", date('Y-m-01')  );
            $result_appr_LastTime = $QTimeLastMonth->fetchAll($where_app_Lasttime);

            if(!empty($result_appr_LastTime[0])){
                $appr_LastTime = array();
                $appr_LastTime = array(
                    'status' => 2,          
                    'approved_at' => date('Y-m-d H:i:s'),
                    'approved_by' => $userStorage->id
                );
                $QTimeLastMonth->update($appr_LastTime,$where_app_Lasttime);
            }
            
        }    
    //Check phân quyền
        $params = array(
            'off' => 1,
            'name' => $name,
            'code' => $code,
            'email' => $email,
            'only_training' => $only_training,
            'month' => $month,
            'year' => $year,
            'area' => $area,
            'department' => $department,
            'team' => $team,
            'title' => $title,
        );
        if(empty($code)){
            $code=null;
        }
        if(empty($name)){
            $name=null;
        }
        if(empty($email)){
            $email=null;
        }
        if(empty($area)){
            $area=null;
        }
        if(empty($department)){
            $department=null;
        }
        if(empty($team)){
            $team=null;
        }
        if(empty($title)){
            $title=null;
        }
        $user_id = $userStorage->id;
//        $group_id = $userStorage->group_id;

        if (in_array($user_id, array(SUPERADMIN_ID, 2584, 8807, 16269, 22716))  || in_array($group_id, array(ADMINISTRATOR_ID,
//            HR_ID,
//            HR_EXT_ID,
BOARD_ID))  )
        {
        }
        elseif ($group_id == PGPB_ID){
            $params['staff_id'] = $user_id;
        }elseif (($group_id == ASM_ID || $group_id == ASMSTANDBY_ID) and  $user_id != 326){
            $params['asm'] = $user_id;
        }elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID,TRAINING_LEADER_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID))){
            $params['other'] = $user_id;
        }elseif($group_id == LEADER_ID){
            $params['leader'] = $user_id;
        }elseif($group_id == SALES_ADMIN_ID){
        $params['sale_admin'] = $user_id;
        }

        //set quyen cho hong nhung trainer
        elseif ($user_id == 7278 ){
            $params['other'] = $user_id;
        }
        //chi van ha noi
        elseif ($user_id == 95){
            $params['other'] = $user_id;
        }
        elseif ($group_id == 22)
        $params['other'] = $user_id;
        elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915)
        $params['asm'] = $user_id;
        else{
            $params['sale'] = $user_id;
        }

        if($this->getRequest()->getParam('approve-all') )
        {
            $QTime->approveAll($params);
        }

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);


        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $array_ddtm = array(
            24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 6705, 43, 13, 40, 41, 4
        );

        $params_day = array(
            'from' => $params['year'] . '-' . $params['month'] . '-01',
            'to' => $params['year'] . '-' . $params['month'] .'-'. $number_day_of_month,
        );
       
        if($userStorage->id == 5899){
            $list_tech_team = $QTime->getListTechTeam($params_day);
            $list_tech = array();
            foreach ($list_tech_team as $k=> $val){
                $list_tech[]=$val['id'];
            
            }
			$list_tech[] = 18941;
			$list_tech[] = 18672;
			$list_tech[] = 20915;
			$list_tech[] = 22782;
			$list_tech[] = 23587;
			$list_tech[] = 23794;
            $list_of_subordinates = implode(",",$list_tech);
			
        }elseif($userStorage->id == 240 ){
            $sql_check_personal = "SELECT staff_id
                                    FROM `personal_permission`
                                    WHERE line_manager = " . $userStorage->id;
            $db   = Zend_Registry::get('db');
            $stmt = $db->prepare($sql_check_personal);
            $stmt->execute();
            $staff_personal = $stmt->fetchAll();

            $array_personal = array();
            foreach($staff_personal as $key_per => $val_per){
                $array_personal[] = $val_per['staff_id'];
            }
            $list_trainer_team = $QTime->getListTrainingTeam($params_day);
            $list_trainer = array();
            foreach ($list_trainer_team as $k=> $val){
                    $list_trainer[]=$val['id'];           
            }
            $listStaffTrain = array_merge($list_trainer, $array_personal );
            //$list_of_subordinates = implode(",",$list_trainer);
            $list_of_subordinates = implode(",",$listStaffTrain);
            $params_day = array(
            'from' => $params['year'] . '-' . $params['month'] . '-01',
            'to' => $params['year'] . '-' . $params['month'] .'-'. $number_day_of_month,
        );
        }elseif($userStorage->id == 12719){
            $from_month = $params_day['from'];
            $sql_staff = "SELECT id FROM `staff` WHERE office_id = 48 AND team = 397 AND ( off_date IS NULL OR off_date >= '$from_month') ";
            
            $db   = Zend_Registry::get('db');
            $stmt = $db->prepare($sql_staff);
            $stmt->execute();
            $staffBrandShop = $stmt->fetchAll();
            $array_personal = array();
            foreach($staffBrandShop as $key_per => $val_per){
                $array_personal[] = $val_per['id'];
            }
            $list_of_subordinates = implode(",",$array_personal);
        }
        else{
            if($userStorage->code){
				$QStaffPermission = new Application_Model_StaffPermission();
					$is_all_office    = 1;
					$is_approve = 0;
					$wherePermissionAllOffice    = array();
					$wherePermissionAllOffice[]  = $QStaffPermission->getAdapter()->quoteInto('staff_code = ?', $userStorage->code);
					$wherePermissionAllOffice[]  = $QStaffPermission->getAdapter()->quoteInto('is_leader > 0 or is_manager > 0');
					$wherePermissionAllOffice[]  = $QStaffPermission->getAdapter()->quoteInto('is_all_office = ? ', $is_all_office);
					$rowPermissionAllOffice = $QStaffPermission->fetchRow($wherePermissionAllOffice);
					if(isset($rowPermissionAllOffice)){
						$infoPermissionAllOffice = $rowPermissionAllOffice->toArray();
						$is_approve = $infoPermissionAllOffice['is_approve'];
						
					}
                $QCheckin = new Application_Model_CheckIn();
                $list_info = $QCheckin->getListCodePermission($userStorage->code);
                $is_manager = 0;
                foreach ($list_info as $key => $val) {
                    if ($val['is_manager'] > 0) {
                        $is_manager = 1;
                    }
                }
               
                if($is_manager == 0 AND $is_approve == 0){
                    $params['not_myself'] = 1;
                }
            }
            
// 			if(in_array($userStorage->title, array(183,399,403))){
//                     $params['not_myself'] = 1;
//                 }

//            $list_of_subordinates = $QTime->getListStaffById($params);
            //Group HR_ID,HR_EXT_ID,BOARD_ID
            if(in_array($group_id, array(HR_ID,HR_EXT_ID,BOARD_ID))){
                $from_month = $params_day['from'];
                $to_month = $params_day['to'];

//                $sql_staff = "SELECT id FROM `staff` WHERE  ( off_date IS NULL OR off_date >= '$from_month' and off_date <= '$to_month') ";
//
//                $db   = Zend_Registry::get('db');
//                $stmt = $db->prepare($sql_staff);
//                $stmt->execute();
//                $staffBrandShop = $stmt->fetchAll();
//                $list_of_subordinates = array();
//                foreach($staffBrandShop as $key_per => $val_per){
//                    $list_of_subordinates[] = $val_per['id'];
//                }
//                $list_of_subordinates = implode(",", $list_of_subordinates);

                $db           = Zend_Registry::get('db');
                $stmt = $db->prepare("CALL `PR_get_staff_filter`(:p_from_date, :p_to_date, :p_code, :p_name, :p_email, :p_area, :p_department, :p_team, :p_title)");
                $stmt->bindParam('p_from_date', $from_month, PDO::PARAM_STR);
                $stmt->bindParam('p_to_date', $to_month, PDO::PARAM_STR);
                $stmt->bindParam('p_code', $code, PDO::PARAM_STR);
                $stmt->bindParam('p_name', $name, PDO::PARAM_STR);
                $stmt->bindParam('p_email', $email, PDO::PARAM_STR);
                $stmt->bindParam('p_area', $area, PDO::PARAM_STR);
                $stmt->bindParam('p_department', $department, PDO::PARAM_STR);
                $stmt->bindParam('p_team', $team, PDO::PARAM_STR);
                $stmt->bindParam('p_title', $title, PDO::PARAM_STR);
                $stmt->execute();
                $staffBrandShop = $stmt->fetchAll();
                $list_of_subordinates = array();
                foreach($staffBrandShop as $key_per => $val_per){
                    $list_of_subordinates[] = $val_per['id'];
                }
                $list_of_subordinates = implode(",", $list_of_subordinates);
//if( $userStorage->id== 22716 ){
//    echo $list_of_subordinates;
//}
            }
            else{
//                $db           = Zend_Registry::get('db');
//                $stmt = $db->prepare("CALL `PR_get_permission_by_staffcode`(:p_code)");
//                $stmt->bindParam('p_code', $userStorage->code, PDO::PARAM_STR);
//                $stmt->execute();
//                $list_of_subordinates_tmp = $stmt->fetchAll();
//                $stmt->closeCursor();
//                $list_of_subordinates = array();
//                foreach ($list_of_subordinates_tmp as $key=> $value){
//                    array_push($list_of_subordinates,$value['id']);
//                }
                $db           = Zend_Registry::get('db');
                $stmt = $db->prepare("CALL `PR_get_permission_by_staffcode_filter`(:p_staff_code,:p_from_date,:p_to_date,:p_code,:p_name,:p_email,:p_area,:p_department,:p_team,:p_title)");
                $stmt->bindParam('p_staff_code', $userStorage->code, PDO::PARAM_STR);
                $stmt->bindParam('p_from_date', $params_day['from'], PDO::PARAM_STR);
                $stmt->bindParam('p_to_date', $params_day['to'], PDO::PARAM_STR);
                $stmt->bindParam('p_code', $code, PDO::PARAM_STR);
                $stmt->bindParam('p_name', $name, PDO::PARAM_STR);
                $stmt->bindParam('p_email', $email, PDO::PARAM_STR);
                $stmt->bindParam('p_area', $area, PDO::PARAM_STR);
                $stmt->bindParam('p_department', $department, PDO::PARAM_STR);
                $stmt->bindParam('p_team', $team, PDO::PARAM_STR);
                $stmt->bindParam('p_title', $title, PDO::PARAM_STR);
                $stmt->execute();
                $list_of_subordinates_tmp = $stmt->fetchAll();
                $stmt->closeCursor();
                $list_of_subordinates = array();
                foreach ($list_of_subordinates_tmp as $key=> $value){
                    array_push($list_of_subordinates,$value['id']);
                }
                $db   = Zend_Registry::get('db');
                $select = $db->select()
                    ->from(array('sp'=>'staff_permission'),array('st.id'))
                    ->join(array('st'=>'staff'),'st.code = sp.staff_code',array())
                    ->where('sp.is_approve = ?',1)
                    ->where('sp.staff_code = ?',$userStorage->code)
                ;
                $data_approve = $db->fetchRow($select);
                if(empty($data_approve)){
                    $list_of_subordinates = array_diff($list_of_subordinates,array($userStorage->id));
                }
                $list_of_subordinates = implode(",", $list_of_subordinates);
            }
        }

        $db           = Zend_Registry::get('db');
        $sql          = "CALL `PR_List_Staff_Time_Approve`(:from_date, :to_date, :p_staff_id)";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params_day['from'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params_day['to'], PDO::PARAM_STR);
        $stmt->bindParam('p_staff_id', $list_of_subordinates, PDO::PARAM_STR);
        
        $stmt->execute();
        $list_staff_view = $stmt->fetchAll();
        $stmt->closeCursor();
        //$stmt = $db = null;
		
		 $p_month_required = $params_day['from'];	
		 
        
		$stmt_last   = $db->prepare("CALL `get_list_staff_approve_last_month`(:p_month_required, :p_staff_id)");
            $stmt_last ->bindParam('p_month_required', $p_month_required , PDO::PARAM_STR);
            $stmt_last ->bindParam('p_staff_id', $list_of_subordinates, PDO::PARAM_STR);
            $stmt_last ->execute();
	    $data_last = $stmt_last->fetchAll();
		 $stmt_last->closeCursor();
	    $this->view->staffs_last = $data_last;
       
        unset($params['list_staff_id']);
        unset($params['list_cmnd']);
       
        $QArea = new Application_Model_Area();
        $this->view->staffs = $list_staff_view;
        $this->view->staffs_cache = $QStaff->get_all_cache();
        $this->view->titles_cache = $QTeam->get_cache_title();
        $this->view->areas = $QArea->fetchAll(null, 'name');;
        
        $this->view->params = $params;
        $this->view->array_ddtm = $array_ddtm;
        
        $params_leave = $params;
        
        $params_leave['from'] = $year . '-' . $month . '-01';
        $params_leave['to'] = $year . '-' . $month . '-' . $number_day_of_month;
        $params_leave['status'] = 0;
        $params_leave['list_staff'] = $list_of_subordinates;
        $QLeaveDetail = new Application_Model_LeaveDetail();
        
        $data_leave = $QLeaveDetail->_selectAdmin(null, $page, $params_leave);
        $this->view->leave = $data_leave;
        //Export - List staff pending
        if(!empty($export)){
            require_once 'PHPExcel.php';

            $alphaExcel = new My_AlphaExcel();

            $PHPExcel = new PHPExcel();
            $heads = array(
                $alphaExcel->ShowAndUp() => 'Mã nhân viên',
                $alphaExcel->ShowAndUp() => 'Họ tên',
				$alphaExcel->ShowAndUp() => 'Department',
                $alphaExcel->ShowAndUp() => 'Team',
                $alphaExcel->ShowAndUp() => 'Chức vụ',
				$alphaExcel->ShowAndUp() => 'Area',
                $alphaExcel->ShowAndUp() => 'Những ngày cần xác nhận công',
                $alphaExcel->ShowAndUp() => 'Những ngày cần xác nhận đi trễ về sớm do công tác ngoài',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }

            $index = 1;
            foreach($list_staff_view as $key => $value)
            {
                $alphaExcel = new My_AlphaExcel();

                $day_pending = !empty($value['temp_time']) ? $value['temp_time'] : $value['gps_time']; 
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['dep'] ,PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['team'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['area_name'],PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $day_pending,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['late_time'],PHPExcel_Cell_DataType::TYPE_STRING);

                $index++;
            }
            $filename = 'Export Pending Approve - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');

            exit;
        }
        
