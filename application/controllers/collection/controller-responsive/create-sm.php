<?php
$this->_helper->viewRenderer->setRender('view-responsive/create');
$flashMessenger = $this->_helper->flashMessenger;
$QLeaveDetail = new Application_Model_LeaveDetail();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if($this->getRequest()->isPost()){
    $is_leave_half = $this->getRequest()->getParam("is-leave-half");
    $from = $this->converDate($this->getRequest()->getParam("from"));
    $to = $this->converDate($this->getRequest()->getParam("to"));
    $date = $this->converDate($this->getRequest()->getParam("date-leave-half"));
    $leave_type = $this->getRequest()->getParam("leave-type");
    $reason = $this->getRequest()->getParam("reason");
    $due_date 	    = $this->getRequest ()->getParam ( 'due_date' );
    $auth = Zend_Auth::getInstance()->getStorage()->read();
    $is_office = $auth->is_officer;
    $staff_id = $auth->id;
    $db = Zend_Registry::get('db');

    $QStaff = new Application_Model_Staff();

    if($from<$userStorage->joined_at){
        $flashMessenger->setNamespace('error')->addMessage("Ngày phép trước ngày bắt đầu làm việc.");
        $this->_redirect("/leave/create");
    }

    if($leave_type == 2){
        //Check locked time
        $QLockTime      = new Application_Model_LockTime();
        $where_lock     = array ();
        if(!empty($from)){

            $date_check = date_create($from);
            $month_locked = date_format($date_check,"m");
            $year_locked  = date_format($date_check,"Y");

        }elseif (!empty($date)){

            $date_check = date_create($date);
            $month_locked = date_format($date_check,"m");
            $year_locked  = date_format($date_check,"Y");
        }

        $where_lock[]   = $QLockTime->getAdapter ()->quoteInto ( 'month = ?', $month_locked );
        $where_lock[]   = $QLockTime->getAdapter()->quoteInto('year = ? ', $year_locked);
        $result_lock    = $QLockTime->fetchRow ( $where_lock );

        if(!empty($result_lock)){
            $flashMessenger->setNamespace('error')->addMessage("Tháng này đã lock bảng công.");
            $this->_redirect("/leave/create");
        }
        //End Check locked time
        //Check Firt contract type in (2, 3, 17)
        $sql_contract = "SELECT sc.* FROM staff_contract sc
                    INNER JOIN (
                                SELECT MIN( sc.id ) id, sc.staff_id, COUNT( sc.id ) total
                                FROM staff_contract sc
                                WHERE sc.contract_term
                                IN ( 2, 3, 17 ) 
                                AND sc.`is_expired` = 0
                                AND sc.`is_disable` = 0
                                AND sc.staff_id = $staff_id
                                GROUP BY sc.staff_id
                    )c ON c.id = sc.id and sc.from_date <= '$from' AND sc.to_date >= '$from' AND sc.`is_expired` = 0 AND sc.`is_disable` = 0";
        $stmt_contract = $db->prepare($sql_contract);
        $stmt_contract->execute();
        $check_data = $stmt_contract->fetchAll();

        if(!empty($check_data)){
            $flashMessenger->setNamespace('error')->addMessage("Thời gian đăng ký phép năm là thời gian thử việc, bạn vui lòng chọn lại thời gian nghỉ phép năm từ thời điểm ký hợp đồng chính thức trở đi");
            $this->_redirect("/leave/create");
        }

//                $stmt = $db->prepare('CALL PR_check_max_day_per_time (:staff_id, :leave_type, :from_date, :to_date) ');
    }
//            if($leave_type == 2 && ($date >= '2019-03-01' || $from >= '2019-03-01')){
//                $flashMessenger->setNamespace('error')->addMessage("Hệ thống chặn đăng ký phép tháng 3 để tính hoàn phép. Thời gian mở lại 03/03/2019");
//                $this->_redirect("/leave/create");
//            }
    // check phep hop le hay khong
    // check 1 lan nghi
    $stmt = $db->prepare('CALL PR_check_max_day_per_time (:staff_id, :leave_type, :from_date, :to_date) ');
    $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
    $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
    $stmt->bindParam('from_date', $from, PDO::PARAM_STR);
    $stmt->bindParam('to_date', $to, PDO::PARAM_STR);
    $stmt->execute();
    $pass_per_time = $stmt->fetchAll();
    $stmt->closeCursor();
    if ($pass_per_time[0]['pass_per_time'] == 1) {
        $flashMessenger->setNamespace('error')->addMessage("Số ngày nghỉ vượt quá giới hạn của 1 lần nghỉ.");
        $this->_redirect("/leave/create");
    }
    // check ca nam
    if($date){
        $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
        $stmt->bindParam('to_date', $date, PDO::PARAM_STR);
        $stmt->bindParam('from_date', $date, PDO::PARAM_STR);
    }else{
        $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
        $stmt->bindParam('to_date', $to, PDO::PARAM_STR);
        $stmt->bindParam('from_date', $from, PDO::PARAM_STR);
    }

    $leave_half_check = ($is_leave_half == 0.5) ? 1 : 0 ;
    $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
    $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
    $stmt->bindParam('is_leave_half', $leave_half_check , PDO::PARAM_INT);

    $stmt->execute();
    $pass_per_year = $stmt->fetchAll();
    $stmt->closeCursor();
    if($leave_type == 2){
        if ($pass_per_year[0]['pass_per_year'] == 1) {
            $flashMessenger->setNamespace('error')->addMessage("Số ngày nghỉ vượt quá giới hạn của 1 năm. (Tối đa 12 ngày)");
            $this->_redirect("/leave/create");
        }
    }


    define('MAX_SIZE_UPLOAD', 3145728);

    $name1 = "";
    if(!empty($_FILES['image']['name'])){
        $ext_arr = array('jpg','png','JPG','PNG','jpeg','JPEG');
        $ext1 = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
        if(($_FILES['image']['size'] > MAX_SIZE_UPLOAD)){
            $flashMessenger->setNamespace('error')->addMessage("Kích thước vượt quá 3Mb");
            $this->_redirect("/leave/create");
            return ;
        }
        if(!in_array($ext1, $ext_arr)){
            $flashMessenger->setNamespace('error')->addMessage("Hình sai định dạng png, jpg, jpeg");
            $this->_redirect("/leave/create");
            return;
        }
        $name1= "leave_".$userStorage->id."_". time().".png";
        $file1 = APPLICATION_PATH . "/../public/photo/leave/" . $name1;
        if (!empty($_FILES['image']['name']) ) {
            $success1 = @move_uploaded_file($_FILES['image']['tmp_name'], $file1);
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            if ($success1) {
                $s3_lib->uploadS3($file1, "photo/leave/", $name1);
            }
        }

    }

    if($is_leave_half == 1){
        if(strtotime($from) > strtotime($to)){
            $flashMessenger->setNamespace('error')->addMessage("Ngày bắt đầu nghỉ phép không được lơn hơn ngày kết thúc");
            $this->_redirect("/leave/create");
        }else{
            $params = array(
                'from' => $from,
                'to' => $to,
                'leave_type' => $leave_type,
                'is_officer' => $is_office,
                'staff_id' => $staff_id,
                'reason' => $reason,
            );
            if(!empty($name1)){
                $params['image'] = $name1;
            }
            if(!empty($due_date)){
                $params['due_date'] = $due_date;
            }
            $QLeaveDetail = new Application_Model_LeaveDetail();
            $data = $QLeaveDetail->insertLeave2($params);
        }
    }else{
        $params = array(
            'date' => $date,
            'leave_type' => $leave_type,
            'is_officer' => $is_office,
            'staff_id' => $staff_id,
            'reason' => $reason,
        );
        if(!empty($name1)){
            $params['image'] = $name1;
        }
        if(!empty($due_date)){
            $params['due_date'] = $due_date;
        }
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $data = $QLeaveDetail->insertLeaveHalf2($params);
    }
    if($data['status'] == 1){
        $flashMessenger->setNamespace('success')->addMessage('Thêm phép thành công.');
        $this->_redirect("/leave/list-my-leave");
    }else{
        $flashMessenger->setNamespace('error')->addMessage($data['message']);
        $this->_redirect("/leave/create");
    }
}

$QLeavInfo = new Application_Model_LeaveInfo();
$this->view->checkHalfLeave = $QLeaveDetail->checkHalfLeave();
$this->view->stock = $QLeavInfo->getStockById(Zend_Auth::getInstance()->getStorage()->read()->id);
$QLeaveType = new Application_Model_LeaveType();
$this->view->parent_type = $QLeaveType->getParent();
$this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $flashMessenger->setNamespace('success')->getMessages();