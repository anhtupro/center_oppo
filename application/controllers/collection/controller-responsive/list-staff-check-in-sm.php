<?php
$this->_helper->viewRenderer->setRender('view-responsive/list-staff-check-in-clone');
$not_approve_self = array(LEADER_ID,SALES_ID, PB_SALES_ID, PGPB_ID);
$page             = $this->getRequest()->getParam('page', 1);
$name             = $this->getRequest()->getParam('name',null);
$code             = $this->getRequest()->getParam('code',null);
$email            = $this->getRequest()->getParam('email',null);
$export           = $this->getRequest()->getParam('export',0);
$only_training    = $this->getRequest()->getParam('only_training', '0');
$month            = $this->getRequest()->getParam('month', date('m'));
$year             = $this->getRequest()->getParam('year', date('Y'));
$area             = $this->getRequest()->getParam('area',null);
$department       = $this->getRequest()->getParam('department',null);
$team             = $this->getRequest()->getParam('team',null);
$title            = $this->getRequest()->getParam('title',null);
$off_long         = $this->getRequest()->getParam('off_long');
$dev              = $this->getRequest()->getParam('dev',0);

$export_over_time       = $this->getRequest()->getParam('export_over_time', 0);
$export_note_contract   = $this->getRequest()->getParam('export_not_contract', 0);
$export_last_month      = $this->getRequest()->getParam('export_last_month', 0);
$export_temp_time_late  = $this->getRequest()->getParam('export_temp_time_late', 0);
//Check locked time
$QLockTime      = new Application_Model_LockTime();
$where_lock     = array ();
$where_lock[]   = $QLockTime->getAdapter ()->quoteInto ( 'month = ?', $month );
$where_lock[]   = $QLockTime->getAdapter()->quoteInto('year = ? ', $year);
$result_lock    = $QLockTime->fetchRow ( $where_lock );
$this->view->locked_time = $result_lock;
//End Check locked time
if($off_long == null)
    $off_long = "0,1";

$this->view->off_long = $off_long;
$export_hr        = $this->getRequest()->getParam('export_hr', 0);
$export_hr_total  = $this->getRequest()->getParam('export_hr_total', 0);
$export_hr_total_final = $this->getRequest()->getParam('export_hr_total_final', 0);
$limit            = 20;
$QShift           = new Application_Model_Shift();
$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$QTime            = new Application_Model_Time2();
$note             = $this->getRequest()->getParam('note', null);
$number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($month), $year);
$month_tmp =  ($month < 10) ? '0'. $month : $month;
$from_date    = $year . '-' . $month_tmp. '-01';
$to_date      = $year . '-' . $month_tmp . '-' . $number_day_of_month;

$QLeaveDetail = new Application_Model_LeaveDetail();
$QTimeLastMonth = new Application_Model_TimeLastMonth();
$QTeam  = new Application_Model_Team();
$staff_title_info = $userStorage->title;
$team_info = $QTeam->find($staff_title_info);
$team_info = $team_info->current();
$group_id = $team_info['access_group'];
$this->view->group_id = $group_id;
if(!empty($export_note_contract)) {
    require_once 'export'.DIRECTORY_SEPARATOR.'export-note-contract.php';
}
if(!empty($export_hr)){
    require_once 'export'.DIRECTORY_SEPARATOR.'export-hr.php';
}
if(!empty($export_hr_total)){
    require_once 'export'.DIRECTORY_SEPARATOR.'export-hr-total.php';
}
if(!empty($export_hr_total_final)){
    require_once 'export'.DIRECTORY_SEPARATOR.'export-hr-total-final.php';
}
if(in_array($userStorage->group_id, $not_approve_self)) {
    $this->view->not_approve_self = 1;
}
else {
    $this->view->not_approve_self = 0;
}
$approve_all_by_staff = $this->getRequest()->getParam('approve_all_by_staff');
$deapprove_all_by_staff = $this->getRequest()->getParam('deapprove_all_by_staff');
$date_approve = $this->getRequest()->getParam('date-approve');
$date_none_approve = $this->getRequest()->getParam('date-none-approve');
if(!empty($date_approve)) {
    $staff_approve = $this->getRequest()->getParam('staff_approve');
    $params_approve_date = array(
        'staff_id' => $staff_approve,
        'date' => $date_approve,
        'admin' => $userStorage->id,
        'note' => ''
    );
    $QTime->approveAllTempTimeByStaffDate($params_approve_date);
}
if(!empty($approve_all_by_staff))
{
    $month_edit = $this->getRequest()->getParam('month_edit');
    $year_edit = $this->getRequest()->getParam('year_edit');

    $params_approve_all = array(
        'staff_id' => $approve_all_by_staff,
        'month' => $month_edit,
        'year' => $year_edit,
        'admin' => $userStorage->id,
        'note' => ''
    );
    $QTime->approveAllTempTimeByStaff($params_approve_all);

}
if($this->getRequest()->getParam('approve-none-office'))
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');
    $QStaff = new Application_Model_Staff();
    $currentStaff = $QStaff->find($staff_id);
    $currentStaff = $currentStaff[0];

    $db  = Zend_Registry::get('db');
    $sql = "
                    UPDATE time t
                        JOIN staff s ON t.staff_id = s.id
                    SET t.status = 1,
                        t.updated_at = '" . date('Y-m-d h:i:s') . "' ,
                        t.approved_at = '" . date('Y-m-d h:i:s') . "' ,
                        t.updated_by = '" . $userStorage->id . "',
                        t.approved_by = '" . $userStorage->id . "',
                    t.regional_market = ". $currentStaff->regional_market . "
                    WHERE date(t.created_at) = '" . $date ."'
                        AND t.staff_id = " . $staff_id ."
                        AND s.joined_at <= '". $date .
        "' AND (s.off_date IS NULL OR s.off_date > '" . $date . "')";
    $db->query($sql);
}
if($this->getRequest()->getParam('unapprove-none-office'))
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');

    $sql = "UPDATE `time`
                        SET status = 0, approve_time = 0, approved_by = NULL
                        WHERE DATE(created_at) = :date
                            AND staff_id = :staff_id";
    $db  = Zend_Registry::get('db');

    $stmt = $db->prepare($sql);
    $stmt->bindParam("date", $date, PDO::PARAM_STR);
    $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);

    $stmt->execute();
    $stmt->closeCursor();
    $stmt = $db = null;
}
if($this->getRequest()->getParam('approve-office'))
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');
    $QTime->approveOffice($staff_id, $date, $userStorage->id);
}
if($this->getRequest()->getParam('approve-office'))
{
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date');
    $QTime->approveOffice($staff_id, $date, $userStorage->id);
}
if($this->getRequest()->getParam('hr-approve-office'))
{
    $hr_time = $this->getRequest()->getParam('hr_time');
    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date_detail');
    $QTime->approveOfficeHR($staff_id, $date, $hr_time, $userStorage->id);
}
if($this->getRequest()->getParam('approve-temp-time'))
{
    $id_update = $this->getRequest()->getParam('approve-temp-time');
    $QTime->approveTempTime($id_update, $note);
}
if($this->getRequest()->getParam('btn_reject_temp'))
{
    $temp_id_reject = $this->getRequest()->getParam('id_temp_reject');
    $reason_reject = $this->getRequest()->getParam('reason_reject');
    $QTime->rejectTempTime($temp_id_reject, $reason_reject);
}
if($this->getRequest()->getParam('approve-all-temp-time'))
{

    $month_approve_all = $this->getRequest()->getParam('month_approve_all');
    $year_approve_all = $this->getRequest()->getParam('year_approve_all');
    $user_approve_all = $this->getRequest()->getParam('user_approve_all');

    $params_approve_all = array(
        'month' => $month_approve_all,
        'year' => $year_approve_all,
        'staff_id' => $user_approve_all,
        'note' => $note,
    );

    $QTime->approveAllTempTimeByStaff($params_approve_all);
}
if(!empty($this->getRequest()->getParam('submit_approve_all')))
{
    $QLeave = new Application_Model_LeaveDetail();
    $json_temp_id = json_decode($this->getRequest()->getParam('list_temp_id'), true);
    $json_remove_time = json_decode($this->getRequest()->getParam('list_remove_time'), true);
    $json_leave_id = array_unique(json_decode($this->getRequest()->getParam('list_leave_id'), true));
    $json_time_id = array_unique(json_decode($this->getRequest()->getParam('list_time_id'), true));
    $staff_id = $this->getRequest()->getParam('staff_id_approve');

    $params_approve_select = array(
        'temp_id' => $json_temp_id,
        'leave_id' => $json_leave_id,
        'remove_time' => $json_remove_time,
        'staff_id' => $staff_id,
        'admin' => $userStorage->id
    );
    $QTime->approveSelect($params_approve_select);

    $db = Zend_Registry::get('db');
    if(!empty($json_time_id))
    {
        $sql = "UPDATE time set status = 1 where id IN (" . implode(",", $json_time_id) . ")";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
    }

    if(!empty($json_remove_time))
    {
        $sql = "DELETE FROM time where staff_id = " . $staff_id . " AND id IN (" . implode(",", $json_remove_time) . ")";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;

        // $sql = "DELETE FROM time_image_location where  time_id IN (" . implode(",", $json_remove_time) . ")";
        // $stmt = $db->prepare($sql);
        // $stmt->execute();
        // $stmt->closeCursor();
        // $stmt = null;
    }

    foreach($json_leave_id as $val_leave)
    {
        $QLeave->updateStatus(array('status'=>1,'id'=>$val_leave));
    }
    $db = null;
}
// if HR or ASM update success
if($this->getRequest()->getParam('update-none-office')){
    $QTime = new Application_Model_Time2();
    $QStaff = new Application_Model_Staff();

    $staff_id = $this->getRequest()->getParam('staff_id');
    $date = $this->getRequest()->getParam('date_detail');
    $shift= $this->getRequest()->getParam('shift');
    $currentStaff = $QStaff->find($staff_id);
    $currentStaff = $currentStaff[0];
    $db  = Zend_Registry::get('db');
    $select = $db->select()
        ->from(array('t' => 'time'))
        ->where('date(t.created_at) = ?', $date)
        ->where('t.staff_id = ?', $staff_id);
    $check_data = $db->fetchAll($select);
    if(count($check_data) > 0) {
        $sql = "
            UPDATE time t
            JOIN staff s ON t.staff_id = s.id
            SET t.shift = $shift,
            t.updated_at = '" . date('Y-m-d h:i:s') . "' ,
                            t.approved_at = '" . date('Y-m-d h:i:s') . "' ,
                            t.updated_by = '" . $userStorage->id . "',
                            t.approved_by = '" . $userStorage->id . "',
                            t.status = 1,
                        t.regional_market = ". $currentStaff->regional_market . "
                        WHERE date(t.created_at) = '" . $date ."'
                            AND t.staff_id = " . $staff_id ."
                            AND s.joined_at <= '". $date .
            "' AND (s.off_date IS NULL OR s.off_date > '" . $date . "')";
        $db->query($sql);
    }
    else {
        if(strtotime($currentStaff->joined_at) <= strtotime($date) && (strtotime($currentStaff->off_date) > strtotime($date) || empty($currentStaff->off_date))) {
            $data['shift'] = $shift;
            $data['created_at'] = date($date .' h:i:s');
            $data['approved_at'] = date('Y-m-d h:i:s');
            $data['created_by'] = $userStorage->id;
            $data['approved_by'] = $userStorage->id;
            $data['staff_id'] = $staff_id;
            $data['status'] = 1;
            $QTime->insert($data);
        }
        else {
        }
    }
}
$QTime = new Application_Model_Time2();
$list_allowance = $QTime->getAllowance();
$params = array(
    'off' => 1,
    'name' => $name,
    'code' => $code,
    'email' => $email,
    'only_training' => $only_training,
    'month' => $month,
    'year' => $year,
    'area' => $area,
    'department' => $department,
    'team' => $team,
    'title' => $title,
    'off_long' => $off_long
);
if(empty($code)){
    $code=null;
}
if(empty($name)){
    $name=null;
}
if(empty($email)){
    $email=null;
}
if(empty($area)){
    $area=null;
}
if(empty($department)){
    $department=null;
}
if(empty($team)){
    $team=null;
}
if(empty($title)){
    $title=null;
}
$user_id = $userStorage->id;
$params_day = array(
    'from' => $params['year'] . '-' . $params['month'] . '-01',
    'to' => $params['year'] . '-' . $params['month']  . '-' .$number_day_of_month,
);
//    $group_id = $userStorage->group_id;

if ($group_id == PGPB_ID){
    $params['staff_id'] = $user_id;
}elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID){
    $params['asm'] = $user_id;
}elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID,TRAINING_LEADER_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID))){
    $params['other'] = $user_id;
}elseif($group_id == LEADER_ID){
    $params['leader'] = $user_id;
}elseif($group_id == SALES_ADMIN_ID){
    $params['sale_admin'] = $user_id;
}
//set quyen cho hong nhung trainer
elseif ($user_id == 7278 || $user_id == 240 || $user_id == 12719){
    $params['other'] = $user_id;
}
//chi van ha noi
elseif ($user_id == 95){
    $params['other'] = $user_id;
}
elseif ($group_id == 22){
    $params['other'] = $user_id;
}
elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915 || $user_id ==87){
    $params['asm'] = $user_id;
}
else{
    $params['sale'] = $user_id;
}
$params['dev'] = $dev;
$public_holyday = $QTime->getPublicHolyday($params);
if($this->getRequest()->getParam('approve-all')){
    $QTime->approveAll($params);
}
if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
        ADMINISTRATOR_ID,
        HR_ID,
        HR_EXT_ID
    ))){
    $from_month = $params_day['from'];
    $to_month = $params_day['to'];
    $db           = Zend_Registry::get('db');
    $stmt = $db->prepare("CALL `PR_get_staff_filter`(:p_from_date, :p_to_date, :p_code, :p_name, :p_email, :p_area, :p_department, :p_team, :p_title)");
    $stmt->bindParam('p_from_date', $from_month, PDO::PARAM_STR);
    $stmt->bindParam('p_to_date', $to_month, PDO::PARAM_STR);
    $stmt->bindParam('p_code', $code, PDO::PARAM_STR);
    $stmt->bindParam('p_name', $name, PDO::PARAM_STR);
    $stmt->bindParam('p_email', $email, PDO::PARAM_STR);
    $stmt->bindParam('p_area', $area, PDO::PARAM_STR);
    $stmt->bindParam('p_department', $department, PDO::PARAM_STR);
    $stmt->bindParam('p_team', $team, PDO::PARAM_STR);
    $stmt->bindParam('p_title', $title, PDO::PARAM_STR);
    $stmt->execute();
    $staffBrandShop = $stmt->fetchAll();
    $stmt->closeCursor();
    $list_of_subordinates = array();
    foreach($staffBrandShop as $key_per => $val_per){
        $list_of_subordinates[] = $val_per['id'];
    }
    $total_of_subordinates =  count($list_of_subordinates);
    if( !empty($export_temp_time_late) || !empty($export_over_time) || !empty($export_last_month) || !empty($export) ){
        $list_of_subordinates = array_slice($list_of_subordinates,null,null);
    }
    else{
        $list_of_subordinates = array_slice($list_of_subordinates,($page-1)*$limit,$limit);
    }
    $list_of_subordinates = implode(",",$list_of_subordinates);

}
elseif($user_id == 12719){
    $from_month = $params_day['from'];
    $sql_staff = "SELECT id FROM `staff` WHERE office_id = 48 AND team = 397 AND ( off_date IS NULL OR off_date >= '$from_month') ";
    $db   = Zend_Registry::get('db');
    $stmt = $db->prepare($sql_staff);
    $stmt->execute();
    $staffBrandShop = $stmt->fetchAll();
    $array_personal = array();
    foreach($staffBrandShop as $key_per => $val_per){
        $array_personal[] = $val_per['id'];
    }
    $list_of_subordinates = implode(",",$array_personal);
}else{
    $db           = Zend_Registry::get('db');
    $stmt = $db->prepare("CALL `PR_get_permission_by_staffcode_filter`(:p_staff_code,:p_from_date,:p_to_date,:p_code,:p_name,:p_email,:p_area,:p_department,:p_team,:p_title)");
    $stmt->bindParam('p_staff_code', $userStorage->code, PDO::PARAM_STR);
    $stmt->bindParam('p_from_date', $params_day['from'], PDO::PARAM_STR);
    $stmt->bindParam('p_to_date', $params_day['to'], PDO::PARAM_STR);
    $stmt->bindParam('p_code', $code, PDO::PARAM_STR);
    $stmt->bindParam('p_name', $name, PDO::PARAM_STR);
    $stmt->bindParam('p_email', $email, PDO::PARAM_STR);
    $stmt->bindParam('p_area', $area, PDO::PARAM_STR);
    $stmt->bindParam('p_department', $department, PDO::PARAM_STR);
    $stmt->bindParam('p_team', $team, PDO::PARAM_STR);
    $stmt->bindParam('p_title', $title, PDO::PARAM_STR);
    $stmt->execute();
    $list_of_subordinates_tmp = $stmt->fetchAll();
    $stmt->closeCursor();
    $list_of_subordinates = array();
    foreach ($list_of_subordinates_tmp as $key=> $value){
        array_push($list_of_subordinates,$value['id']);
    }
    $total_of_subordinates =  count($list_of_subordinates);
    echo $total_of_subordinates;
    if( !empty($export_temp_time_late) || !empty($export_over_time) || !empty($export_last_month) || !empty($export) ){
        $list_of_subordinates = array_slice($list_of_subordinates,null,null);
    }
    else{
        $list_of_subordinates = array_slice($list_of_subordinates,($page-1)*$limit,$limit);
    }
    $list_of_subordinates = implode(",",$list_of_subordinates);
}
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$array_ddtm = array(
    24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
);
$QCheckIn = new Application_Model_CheckIn();
$off_date = $QCheckIn->getDateInfo($params_day);

$db           = Zend_Registry::get('db');
$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$sql          = "CALL `PR_List_Staff_Time_New`(:from_date, :to_date, :p_staff_id, :p_off_long)";
$stmt = $db->prepare($sql);
$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
$stmt->bindParam('p_staff_id', $list_of_subordinates, PDO::PARAM_STR);
$stmt->bindParam('p_off_long', $off_long, PDO::PARAM_STR);
$stmt->execute();
$list_staff_view = $stmt->fetchAll();
$stmt->closeCursor();

$sql_pending          = "CALL `PR_List_Staff_Time_Approve`(:from_date, :to_date, :p_staff_id)";
$stmt_pending = $db->prepare($sql_pending);
$stmt_pending->bindParam('from_date', $from_date, PDO::PARAM_STR);
$stmt_pending->bindParam('to_date', $to_date, PDO::PARAM_STR);
$stmt_pending->bindParam('p_staff_id', $list_of_subordinates, PDO::PARAM_STR);
$stmt_pending->execute();
$list_staff_pending = $stmt_pending->fetchAll();
$stmt_pending->closeCursor();

$arr_pending = array();
if(!empty($list_staff_pending)){
    foreach ($list_staff_pending as $k => $val){
        $arr_pending[] = $val['staff_id'];
    }
}
$array_staff_id = array();
$array_cmnd = array();

$max_page = ceil($total_of_subordinates / $limit);
$page_trainer = $page - $max_page;
$sum_offset = ($limit * $max_page) - $data['total'];
$limit_trainer = (($limit - count($total_of_subordinates)) > 0 && count($data['data'] > 0))?($limit - count($data['data'])):$limit;
foreach ($total_of_subordinates as $key => $value) {
    $array_staff_id[] = $value['id'];
    $array_cmnd[] = $value['cmnd'];
}
if($page < $max_page) {
    $data_new_staff_trainer['data'] = null;
}
if(!empty(implode(",", $array_staff_id))) {
    $params['list_staff_id'] = "(" . implode(",", $array_staff_id) . ")";
    $params['list_cmnd'] = "('" . implode("','", $array_cmnd) . "')";
    $data_training = $QTime->getListTrainingByStaffId($params);
    $data_allowance = $QTime->getListAllowanceByStaffId($params);
    $data_leave = $QTime->getListLeaveByStaffId($params);
    $list_training_check_in = $QTime->getListTrainingCheckIn($params);

    $this->view->data_training = $data_training;
    $this->view->data_allowance = $data_allowance;
    $this->view->data_leave = $data_leave;
}
$this->view->staffs_trainer = $data_new_staff_trainer['data'];
$this->view->limit = $limit;
$this->view->total = $total_of_subordinates ;
$this->view->number_day_of_month = $number_day_of_month;
$this->view->url = HOST . 'staff-time/list-staff-check-in' . ($params ? '?' . http_build_query($params) .
        '&' : '?');
$this->view->offset = $limit * ($page - 1);
unset($params['list_staff_id']);
unset($params['list_cmnd']);
$this->view->public_holyday = $public_holyday;
$this->view->shift = $QShift->getShiftByTitle($userStorage->title);
$this->view->current_user  = $userStorage;

$QArea = new Application_Model_Area();
$QCheckIn               = new Application_Model_CheckIn();

$all_area =  $QArea->fetchAll(null, 'name');
$this->view->areas = $all_area;
$this->view->staff_pending = $arr_pending;
$this->view->params = $params;
$this->view->list_allowance = $list_allowance;
$this->view->array_ddtm = $array_ddtm;
$this->view->off_date = $off_date;
$this->view->list_staff_view = $list_staff_view;

if(!empty($export_temp_time_late)){
    require_once 'export'.DIRECTORY_SEPARATOR.'export-temp-time-late.php';
}
if(!empty($export_over_time)){
    require_once 'export'.DIRECTORY_SEPARATOR.'export-over-time.php';
}
if(!empty($export_last_month)){
    require_once 'export'.DIRECTORY_SEPARATOR.'export-last-month.php';
}
if(!empty($export)){
    require_once 'export'.DIRECTORY_SEPARATOR.'export.php';
}
