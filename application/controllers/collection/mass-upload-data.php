<?php

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->view->messages_success = $messages_success;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QMassUpload = new Application_Model_MassUpload();
$QMassUploadData = new Application_Model_MassUploadData();
$QMassUploadAccess = new Application_Model_MassUploadAccess();

$name = $this->getRequest()->getParam('name');
$group_access = $this->getRequest()->getParam('group-access');
$allow_upload_area = $this->getRequest()->getParam('allow_upload_area', 0);

$db = Zend_Registry::get('db');
$db->beginTransaction();
if ($this->getRequest()->getMethod() == 'POST') {
    $save_folder = 'template';
    $save_export = 'export';
    $new_file_path = '';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 5000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
    try {
        $file = My_File::get($save_folder, $requirement, true);
        if (!$file || !count($file)) {
            throw new Exception("Upload failed");
        }

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . $file["filename"];
        $clone_path = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . "template_" . $file["filename"];

        //Clone file
        copy($uploaded_dir, $clone_path);
        include 'PHPExcel/IOFactory.php';
        $url = DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR . "template" . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . $file['filename'];
        $url_template = DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR . "template" . DIRECTORY_SEPARATOR . $file['folder'] . DIRECTORY_SEPARATOR . "template_" . $file["filename"];
        $inputFileType = PHPExcel_IOFactory::identify($clone_path);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($clone_path);
        $index = 9;
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $objPHPExcel->getActiveSheet()->removeRow($index, $highestRow);
        $inputFileName = '.' . $url;
        try {
            $inputFileType_ = PHPExcel_IOFactory::identify($inputFileName);
            $objReader_ = PHPExcel_IOFactory::createReader($inputFileType_);
            $objPHPExcel_ = $objReader_->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        if($objPHPExcel_->getSheetCount() != 1){
            throw new Exception("The number of sheet is only one!");
        }
        $sheet_ = $objPHPExcel_->getSheet(0);
        $highestRow_ = $sheet_->getHighestRow();
        $highestColumn = $sheet_->getHighestColumn();
        for ($row = 9; $row <= $highestRow_; $row++) {
            $rowData[] = $sheet_->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    NULL,
                    TRUE,
                    TRUE);
        }

        foreach ($rowData as $key => $value) {
            $dt[] = $value[0];
        }
        $params = array(
            'name' => $name,
            'url' => $url,
            'url_template' => $url_template,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id,
            'status_upload_area' => $allow_upload_area
        );
        $id = $QMassUpload->insert($params);

        if (!empty($group_access)) {
            foreach ($group_access as $val) {
                $data_access = [
                    'id_massupload' => $id,
                    'id_group_access' => $val
                ];
                $QMassUploadAccess->insert($data_access);
            }
        }
        $data_massupload = [];
        foreach ($dt as $k => $v) {
            if (empty($v[1])) {
                throw new Exception("Check file upload. Please delete the blank rows !");
            }
            $mass_data = array(
                'massupload_id' => $id,
                'area' => $v[1],
                'json' => json_encode($v, JSON_UNESCAPED_UNICODE),
            );
            $data_massupload[] = $mass_data;
        }

        My_Controller_Action::insertAllrow($data_massupload, 'massupload_data');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
        $objWriter->save($clone_path);
        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage('Đã tải file thành công');
    } catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect('/collection/mass-upload-data');
    }
    $this->redirect('/collection/mass-upload-data');
}



