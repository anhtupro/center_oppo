<?php

set_time_limit(0);
error_reporting(0);
ini_set('memory_limit', -1);
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();
$heads = array(
    'STT',
    'CODE',
    'FULL NAME',
    'DEPARTMENT',
    'TEAM',
    'TITLE',
    'AREA',
    'JOINED AT',
    'GENDER',
    'STATUS',
    'OFF',
    'KV phản hồi
Các case loại, không tặng: nghỉ việc/ chuẩn bị nghỉ việc; thai sản/ tạm off qua 20/10/2020 mới trở lại làm việc'
);

$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();


$sheet->mergeCells('A1:L1');
$sheet->getCell('A1')->setValue('DANH SÁCH TẶNG QUÀ 20/10 NĂM 2020- BỘ PHẬN SALES KHU VỰC');
$sheet->mergeCells('A2:L2');
$sheet->getCell('A2')->setValue('Đối tượng: Tất cả nhân viên Nữ đang làm việc ở công ty tính đến ngày 20/10/2020 (không bao gồm chức danh PG Partime)');





$alpha = 'A';
$index = 3;
foreach ($heads as $key) {
    $sheet->setCellValue($alpha . $index, $key);
    $alpha++;
}
$index = 4;
$stt = 0;
foreach($list_gift as $key => $value):

    if($value['area']==$area){
        $alpha = 'A';

        $phpColor = new PHPExcel_Style_Color();
        $phpColor->setRGB('FF0000');  

       
        // if(trim($value['status']) == 'Temporary_Off'){
        //      $sheet->getStyle('A1')->getFont()->setColor( $phpColor );
        // }
        $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['department']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['team']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['joined_at']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['gender']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['status']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['off_date']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['description']), PHPExcel_Cell_DataType::TYPE_STRING);
       
       if(trim($value['status']) != 'On'){
             $sheet->getStyle('A'.$index.':J'.$index.'')->getFont()->setColor( $phpColor );
        }

        $index++;
    }

endforeach;

// $heads_clone = array(
//     'AREA'=>'THÊM',
//     'AN GIANG Count'=>6,
//     'BAC NINH Count'=>11,
//     'BINH DINH Count'=>21,
//     'BINH PHUOC Count'=>3,
//     'BINH THUAN Count'=>7,
//     'BUON ME THUOT Count'=>5,
//     'DONG NAI 1 Count'=>7,
//     'DONG NAI 2 Count'=>6,
//     'DONG THAP Count'=>6,
//     'GIA LAI Count'=>5,
//     'HCMC 1 Count'=>2,
//     'HCMC 3 Count'=>27,
//     'HCMC 4 Count'=>3,
//     'HCMC 5 Count'=>1,
//     'HUE Count'=>10,
//     'LAM DONG Count'=>2,
//     'NAM DINH Count'=>6,
//     'QUANG NINH Count'=>9,
//     'THAI NGUYEN Count'=>23,
//     'THANH HOA Count'=>15,
//     'TIEN GIANG Count'=>2,
//     'VINH LONG Count'=>5,
//     'VUNG TAU Count'=>6,
//     'Grand Count'=>188,

// );

// $index+=3;
// $sheet->getCell('B'.$index)->setValueExplicit( 'Đặt thêm', PHPExcel_Cell_DataType::TYPE_STRING);

// foreach ($heads_clone as $key =>$value) {
//     $alpha = 'C';
//     $sheet->getCell($alpha++.$index)->setValueExplicit( $key, PHPExcel_Cell_DataType::TYPE_STRING);
//     $sheet->getCell($alpha++.$index)->setValueExplicit( $value, PHPExcel_Cell_DataType::TYPE_STRING);
//     $index++;
// }





$filename = 'list_gift_' .$area.'_'. date('d-m-Y');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;
