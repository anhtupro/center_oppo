<?php
$export = $this->getRequest()->getParam('export');
$area = $this->getRequest()->getParam('area');
$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
$QAsm         = new Application_Model_Asm();
$list_regions = $QAsm->get_cache($userStorage->id);
$area         = $this->getRequest()->getParam('area');
$export       = $this->getRequest()->getParam('export');
$export_all       = $this->getRequest()->getParam('export_all');
$list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
$str_area = implode(",", $list_regions); 

if(in_array($userStorage->id, array(5899, 340)) ){
    $sql = "SELECT *  FROM gift_lunar_new_year";
}else{
    $sql = "SELECT g.*  FROM gift_lunar_new_year g WHERE g.area IN (
                             SELECT name FROM area WHERE id IN ($str_area)
                    )
                  ";
}
$db = Zend_Registry::get('db');
$stmt = $db->prepare($sql);
$stmt->execute();
$list_gift = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = null;
$list_area=array();
if(!empty($export) && !empty($area)) {
    $this->exportListLunarNewYear($list_gift,$area);
}
foreach($list_gift as $key =>$value){
    if(!in_array($value['area'], $list_area)){
        array_push($list_area,$value['area']);
    }
}

$this->view->list_area      = $list_area;
