<?php
    $area_list    = array();
    $db             = Zend_Registry::get('db');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id       = $userStorage->id; 
    $QAsm         = new Application_Model_Asm();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $area         = $this->getRequest()->getParam('area');
    $export       = $this->getRequest()->getParam('export');
    $export_all       = $this->getRequest()->getParam('export_all');
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $str_area = implode(",", $list_regions); 
    $title = $userStorage->title;
    $QTeam  = new Application_Model_Team();
    $team = $QTeam->find($title);
    $team_info = $team->current();
    $group_id  = $team_info['access_group'];
    
    if(!empty($area) AND !empty($export)){
         if ( in_array($group_id,array(ASM_ID))) {
               $sql_area  = "SELECT * FROM area WHERE id IN ($str_area) AND name = '$area' ";
               $stmt_area = $db->prepare($sql_area);
               $stmt_area->execute();
               $area_check = $stmt_area->fetchAll();
               $stmt_area->closeCursor();
               if(empty($area_check)){
                    $this->_redirect("/collection/bonus-admin");
               }
         }
        $sql_detail = "SELECT * FROM bonus_admin_2019 WHERE area = '$area'  ";
        
        $stmt_detail = $db->prepare($sql_detail);
        $stmt_detail->execute();
        $data = $stmt_detail->fetchAll();
        $stmt_detail->closeCursor();
     
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);

    include 'PHPExcel/IOFactory.php';
    //\htdocs\sell\public\templete\target
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'templete' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'up_bonus_admin_2019.xlsx';

        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($path);
        $sheet         = $objPHPExcel->getActiveSheet();
     
        $index = 11;
        $stt   = 1;
     
if(!empty($data))
        foreach ($data as $key => $value):
            $alpha   = 'B';
        
//            $sheet->getCell($alpha++ . $index)->setValueExplicit($stt, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
//            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['thamnien'],0), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['thoigian'],0), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['luong']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['ketqua'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['sothang'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['note'], PHPExcel_Cell_DataType::TYPE_STRING);
            $stt++;
            $index++;
        endforeach;

        $filename  = $area . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        $objWriter->save('php://output');
        exit;    
        
    }
    
    if(!empty($export_all)){
         
        $sql_detail = "SELECT * FROM bonus_admin_2019 WHERE 1 ";
        
        $stmt_detail = $db->prepare($sql_detail);
        $stmt_detail->execute();
        $data = $stmt_detail->fetchAll();
        $stmt_detail->closeCursor();
        
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);

    include 'PHPExcel/IOFactory.php';
    //\htdocs\sell\public\templete\target
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'templete' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'DSTANGLUONGTHAMNIEN_admin_2019_pre.xlsx';

        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($path);
        $sheet         = $objPHPExcel->getActiveSheet();

        $index = 11;
        $stt   = 1;
        print_r($data);
        exit();
        if(!empty($data))
        foreach ($data as $key => $value):
            $alpha   = 'B';
        
            $sheet->getCell($alpha++ . $index)->setValueExplicit($key+1, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['thamnien'],0), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['thoigian'],0), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['luong']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['ketqua'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['sothang'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['note'], PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;

        $filename  = $area . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        $objWriter->save('php://output');
        exit;    
        
    }
    
  
    if ( in_array($group_id,array(HR_ID, ADMINISTRATOR_ID))) {
            $sql = "SELECT *, count(*) soluong FROM bonus_admin_2019 group by area ";
    }else{
            $sql = "SELECT *, count(*) soluong FROM bonus_admin_2019 
              WHERE area IN (
                          SELECT name FROM area WHERE id IN ($str_area)
                             ) group by area ";
    }
    
    
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $info = $stmt->fetchAll();
    $stmt->closeCursor();
    $this->view->viewed_area_id = $list_regions;
    $this->view->info = $info;

    