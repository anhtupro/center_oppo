<?php
$submit         = $this->getRequest()->getParam('submit');
$code = trim($this->getRequest()->getParam('staff_code', array()));
if(!empty($submit)){
    $from         = $this->getRequest()->getParam('from');
    $to         = $this->getRequest()->getParam('to');
    $office_time        = $this->getRequest()->getParam('office_time');
    $add_reason        = $this->getRequest()->getParam('add_reason');
    $reason        = $this->getRequest()->getParam('reason');
    $from = str_replace('/', '-', $from);
    $to = str_replace('/', '-', $to);

    $from=date("Y-m-d", strtotime($from));
    $to=date("Y-m-d", strtotime($to));
    define('START_ROW', 2);
    define('Code',0);// cột A;

    $this->_helper->layout->disableLayout();
    $flashMessenger = $this->_helper->flashMessenger;
    $QTempTime = new Application_Model_TempTime();
    $QStaff            = new Application_Model_Staff();
    $arr_code_area = explode("\n", $code);
    if(empty($arr_code_area[0])){
        $arr_code_area = array();
    }
    set_time_limit(0);
    ini_set('memory_limit', -1);

//    $progress = new My_File_Progress('parent.set_progress');
//    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );

    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];

        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
    }
    catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

//read file
    include 'PHPExcel/IOFactory.php';
//  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    }
    catch (Exception $e) {

        $this->view->errors = $e->getMessage();
        return;
    }


//  Get worksheet dimensions
    $sheet         = $objPHPExcel->getSheet(0);
    $highestRow    = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $data_staff_code = [];

    for ($row = 0; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
        }
        catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        // nothing here
    } // END loop through order rows

$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
//    $progress->flush(30);
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        $code_array=array();
        $id_array=array();
        for ($row = START_ROW; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            $staff_code = (trim($rowData[Code])!=NULL) ? ($rowData[Code]) : NULL;

            if(!empty($staff_code)) {
                $where_staff                       = $QStaff->getAdapter()->quoteInto("code = '$staff_code'", null);
                $staff_by_code             = $QStaff->fetchRow($where_staff);
                if(!empty($staff_by_code)) {
                    array_push($id_array, $staff_by_code['id']);
                }
                array_push($code_array,$staff_code);
            }
        }// end loop
        $string_id=implode(",",$id_array);
        if(!empty($arr_code_area)) {
            $code_array = array_merge($arr_code_area,$code_array);
        }
        
       
        $string_code="'".implode("','",$code_array) ."'";
        $str_where = "'" .implode("', '",$data_staff_code). "'" ;
        
        $sql_delete = "DELETE tt.*
FROM
	temp_time tt
        INNER JOIN staff st on st.`id` = tt.staff_id
WHERE
	st.`code` IN ($string_code) 
	AND date BETWEEN '$from' 
	AND '$to' ";

        $stmt = $db->prepare($sql_delete);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
        $approved_by = $userStorage->id; 
        $shift = ($office_time == 1) ? 6 : 9;
        $sql1 = "INSERT INTO temp_time (`staff_id`,
`date`,
`status` ,  
 `shift` ,      
`office_time`,
`add_reason`,
`reason`,
`created_at`,
`approved_at`,
`approved_by`,
`del`,
`passby`, 
`note`
)
	SELECT
		st.id,
		ad_tmp.date,1,$shift,$office_time,$add_reason,'$reason', NOW(),NOW(),$approved_by,0,0, 'mass upload'
	FROM
		`staff` st
		CROSS JOIN ( SELECT * FROM `all_date` ad WHERE ad.`date` BETWEEN '$from' AND '$to' ) ad_tmp 
		 WHERE st.code IN($string_code)  
                ";

        $stmt1 = $db->prepare($sql1);
        $stmt1->execute();
        $stmt1->closeCursor();
        $stmt1 = null;
        $flashMessenger->setNamespace('success')->addMessage('thành công.');
        $db->commit();    

        $this->_redirect(HOST . 'collection/upload-temp-time');
        
    }
    catch (Exception $e) {
        $db->rollback();
        $this->view->errors = $e->getMessage();

        return;
    }
}




$QReasonTempTime = new Application_Model_ReasonTempTime();
$reason_temp_time=$QReasonTempTime->fetchAll()->toArray();
$this->view->reason_temp_time = $reason_temp_time;
$flashMessenger = $this->_helper->flashMessenger;
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->view->messages_error = $messages_error;