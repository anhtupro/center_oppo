<?php
    $export = $this->getRequest()->getParam('export');
    $from_date = $this->getRequest()->getParam('from_date', date('1/m/Y', strtotime("-1 months")));
    $to_date = $this->getRequest()->getParam('to_date', date('t/m/Y' , strtotime("-1 months")));
    $code = $this->getRequest()->getParam('code');
    $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
    $QAsm         = new Application_Model_Asm();
//    $list_regions = $QAsm->get_cache($user_id);
    
    $user_id = $userStorage->id;
    $list_regions = $QAsm->get_cache(6074);
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $str_area = implode(",", $list_regions); 

    $pars = array(
            'from_date' => $from_date,
            'to_date' => $to_date,
            'code' => $code,
    );

    $pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : date('Y-m-1');
    $pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : date('Y-m-d');
    $pars['from_date'] = $pars['from_date'].' 00:00:00';
    $pars['to_date'] = $pars['to_date'].' 23:59:59';


		$pars['from_date'] = $from_date;
		$pars['to_date'] = $to_date;
		$this->view->pars = $pars;$export = $this->getRequest()->getParam('export');
		$from_date = $this->getRequest()->getParam('from_date', date('1/m/Y', strtotime("-1 months")));
		$to_date = $this->getRequest()->getParam('to_date', date('t/m/Y' , strtotime("-1 months")));
		$code = $this->getRequest()->getParam('code');

		$pars = array(
			'from_date' => $from_date,
			'to_date' => $to_date,
			'code' => $code,
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : date('Y-m-1');
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : date('Y-m-d');
		$pars['from_date'] = $pars['from_date'].' 00:00:00';
		$pars['to_date'] = $pars['to_date'].' 23:59:59';

		if($export)
		{
			
			set_time_limit(0);
			
                        $sql = "
			SELECT
				CONCAT(s.firstname,' ',s.lastname) staff_name
				, kpi.kpi
				, kpi.sell_out
				, s.code as `code`
				, p.title policy_title
				, g.`desc` good_name
				, kpi.title
				, a.`name` area_name
				, '' color_name
				, t.`name` team
				, total
                                , t1.name as store_name
                                
			FROM (

				SELECT pg_id staff_id, area_id, 'PGPB' title, good_id, policy_id_pg policy, kpi_pg kpi, COUNT(imei_sn) sell_out, (kpi_pg * COUNT(imei_sn)) total, store_id
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND pg_id > 0 AND kpi_pg > 0
				GROUP BY staff_id, policy_id_pg, good_id, kpi

				UNION ALL

				SELECT sale_id staff_id, area_id, 'Sales' title, good_id, policy_id_sale policy, kpi_sale kpi, COUNT(imei_sn) sell_out, (kpi_sale * COUNT(imei_sn)) total, store_id
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND sale_id > 0 AND kpi_sale > 0
				GROUP BY staff_id, policy_id_sale, good_id, kpi

				UNION ALL

				SELECT store_leader_id staff_id, area_id, 'Store Leader' title, good_id, policy_id_store_leader policy, kpi_store_leader kpi, COUNT(imei_sn) sell_out, (kpi_store_leader * COUNT(imei_sn)) total, store_id
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND store_leader_id > 0 AND kpi_store_leader > 0
				GROUP BY staff_id, policy_id_store_leader, good_id, kpi

				UNION ALL

				SELECT sales_brs_id staff_id, area_id, 'Sale Leader Brs' title, good_id, policy_id_sales_brs policy, kpi_sales_brs kpi, COUNT(imei_sn) sell_out, (kpi_sales_brs * COUNT(imei_sn)) total, store_id
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND sales_brs_id > 0 AND kpi_sales_brs > 0
				GROUP BY staff_id, policy_id_sales_brs, good_id, kpi
                                    
                                UNION ALL
                                
                                SELECT iks.staff_id , kl.area_id, 'CONSULTANT Brs' title, kl.good_id , 16 policy , iks.kpi, COUNT(iks.imei_sn) sell_out , ( iks.kpi * COUNT(iks.imei_sn)) total , kl.`store_id`  
                                FROM imei_kpi_staff iks       
                                JOIN kpi_log kl ON iks.imei_sn = kl.imei_sn
                                WHERE iks.staff_id > 0 and iks.kpi > 0 and iks.timing_date BETWEEN :from_date AND :to_date
                                GROUP BY iks.staff_id, iks.type, kl.good_id, iks.kpi
			)
			AS `kpi`
			JOIN staff s ON s.id = kpi.staff_id
			JOIN policy p ON p.id = kpi.policy
			JOIN team t ON t.id = s.team
                        JOIN store t1 ON t1.id = `kpi`.store_id
			JOIN warehouse.good g ON g.id = kpi.good_id
			JOIN area a ON a.id = kpi.area_id
                        WHERE  a.id IN ($str_area)
			GROUP BY kpi.staff_id, title, kpi.good_id, kpi.policy, kpi.kpi
		";

		if(!empty($code))
		{
			$sql .= " HAVING s.code = '" . $pars['code'] . "'";
		}
                $db = Zend_Registry::get('db');
		$stmt = $db->prepare($sql);
		$pars['from_date'] .= " 00:00:00";
		$pars['to_date'] .= " 23:59:59";
		$stmt->bindParam('from_date', $pars['from_date'] , PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);

		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();
		$stmt = null;
           
			require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();
			$heads = array(
				'A' => 'Code',
				'B' => 'Name',
				'C' => 'Area',
				'D' => 'Title',
				'E' => 'Good name',
				'F' => 'Store name',
//				'G' => 'Policy',
				'H' => 'Sell out',
				'I' => 'KPI',
				'J' => 'Total S.O',
//				'L' => 'Team'
			);

			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			foreach($heads as $key => $value)
				$sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:J1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(35);
			$sheet->getColumnDimension('B')->setWidth(25);
			$sheet->getColumnDimension('C')->setWidth(25);
			$sheet->getColumnDimension('D')->setWidth(15);
			$sheet->getColumnDimension('E')->setWidth(15);
			$sheet->getColumnDimension('F')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(15);
			$sheet->getColumnDimension('H')->setWidth(20);
			$sheet->getColumnDimension('I')->setWidth(20);
			$sheet->getColumnDimension('J')->setWidth(20);

			foreach($data as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['code']);
				$sheet->setCellValue('B' . ($key+2), $value['staff_name']);
				$sheet->setCellValue('C' . ($key+2), $value['area_name']);
				$sheet->setCellValue('D' . ($key+2), $value['title']);
				$sheet->setCellValue('E' . ($key+2), $value['good_name']);
				$sheet->setCellValue('F' . ($key+2), $value['store_name']);
//				$sheet->setCellValue('G' . ($key+2), $value['policy_title']);
				$sheet->setCellValue('H' . ($key+2), $value['sell_out']);
				$sheet->setCellValue('I' . ($key+2), $value['kpi']);
				$sheet->setCellValue('J' . ($key+2), $value['total']);
//				$sheet->setCellValue('K' . ($key+2), $value['team']);
			}

			$filename = 'KPI Detail Staff - ' . date('Y-m-d H-i-s');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;
		}

		$pars['from_date'] = $from_date;
		$pars['to_date'] = $to_date;
		$this->view->pars = $pars;