<?php
$id            = $this->getRequest()->getParam('id');
$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
$QTeam              = new Application_Model_Team();
$info_team          = $QTeam->fetchRow(['id = ?' => $userStorage->title]);
function stripVN($str) {
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    return $str;
}
set_time_limit(0);
error_reporting(0);
ini_set('memory_limit', -1);
require_once 'PHPExcel.php';
$QMassUpload        = new Application_Model_MassUpload();  
$QMassUploadData    = new Application_Model_MassUploadData();
$info               = $QMassUpload->fetchRow(['id = ?' => $id]);
if($info_team->access_group == My_Staff_Group::ASM || $info_team->access_group ==  My_Staff_Group::SALES_ADMIN){
    $path           = '.' .$info['url_template'];
}else{
    $path           = '.' .$info['url'];
}
$str            = stripVN($info['name']);
$name           = str_replace(' ', '_', $str);
$inputFileType  = PHPExcel_IOFactory::identify($path);
$objReader      = PHPExcel_IOFactory::createReader($inputFileType);
$PHPExcel       = $objReader->load($path);
if(empty($area)){
    $area[] = '0';
}
$params = array(
    'area' => $area,
    'id'   => $id,
);
if($info_team->access_group == My_Staff_Group::ASM || $info_team->access_group ==  My_Staff_Group::SALES_ADMIN){
    $d          = $QMassUploadData->getData($params);    
    $length_    = count($d);  
    for($j = 0; $j < $length_; $j++){
        $data[] = json_decode($d[$j]["json"]);
    }   
    $sheet  = $PHPExcel->getActiveSheet();
    $index  = 9;
    $stt    = 0;
    $length = count($data[0]);   
    foreach($data as $key => $value):       
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);        
            for($i = 1; $i < $length + 1; $i++){
                $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
            }
            $index++;           
    endforeach;
    $rename = "";
    foreach($area as $char){
        $rename .= $char . '_';
    }        
    $filename =  $name  . $rename . date('d-m-Y');
}else {
    $filename =  $name . date('d-m-Y');
}
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;
