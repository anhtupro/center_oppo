<?php

$id = $this->getRequest()->getParam('id');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QTeam = new Application_Model_Team();
$info_team = $QTeam->fetchRow(['id = ?' => $userStorage->title]);

function stripVN($str) {
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    return $str;
}

set_time_limit(0);
error_reporting(0);
ini_set('memory_limit', -1);
require_once 'PHPExcel.php';
$QMassUpload = new Application_Model_MassUpload();
$QMassUploadData = new Application_Model_MassUploadData();
$info = $QMassUpload->fetchRow(['id = ?' => $id]);

$path = '.' . $info['url_template'];
$str = stripVN($info['name']);
$name = str_replace(' ', '_', $str);
$inputFileType = PHPExcel_IOFactory::identify($path);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$PHPExcel = $objReader->load($path);
$params = array(
    'id_parent' => $id,
);
//export
$d = $QMassUploadData->getDataReview($params);
$length_ = count($d);
$data = array();
foreach ($d as $value){
    $data_temp['json'] = json_decode($value['json']);
    $data_temp['created_at'] = $value['created_at'];
    $data_temp['created_by'] = $value['code'].'-'.$value['full_name'];
    $data[] = $data_temp;
}
$sheet = $PHPExcel->getActiveSheet();
$index = 9;
$stt = 0;
$length = count($data[0]['json']);
foreach ($data as $key => $value):
    $alpha = 'A';
    $sheet->getCell($alpha++ . $index)->setValueExplicit(++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
    for ($i = 1; $i < $length + 1; $i++) {
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['json'][$i]), PHPExcel_Cell_DataType::TYPE_STRING);
    }
    if($index == 9){
        $temp_alpha = $alpha;
    }
    $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['created_by']), PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['created_at']), PHPExcel_Cell_DataType::TYPE_STRING);
    $index++;
endforeach;
if(!empty($temp_alpha)){
    $sheet->getCell($temp_alpha++ . 8)->setValueExplicit('Upload by', PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->getCell($temp_alpha++ . 8)->setValueExplicit('Upload at', PHPExcel_Cell_DataType::TYPE_STRING);   
}
$filename = 'Result_data_review' . $name . '_' . date('d-m-Y');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
// end export

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;