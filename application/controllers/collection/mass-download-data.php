<?php
$start_row     = 9;
$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
$id            = $this->getRequest()->getParam('id');
$del           = $this->getRequest()->getParam('del', 0);
$export        = $this->getRequest()->getParam('export',0);
$export_result = $this->getRequest()->getParam('export-result',0);
$name          = $this->getRequest()->getParam('name');
$status          = $this->getRequest()->getParam('status',0);
$QMassUpload        = new Application_Model_MassUpload();
$QMassUploadData    = new Application_Model_MassUploadData();
$QTeam              = new Application_Model_Team();
$QArea          = new Application_Model_Area();
$QAsm           = new Application_Model_Asm();
$info_team          = $QTeam->fetchRow(['id = ?' => $userStorage->title]);

$params = array(
        'name'              => trim($name),        
        'del'               => $del,
        'access_group'      => $info_team->access_group,
        'id_user'           => $userStorage->id,
        'status'            => $status
    );

$page   = $this->getRequest()->getParam('page', 1);
$limit  = 10;
$result = $QMassUpload->fetchPagination($page, $limit, $total, $params);   
$this->view->title        = $userStorage->title;
$this->view->id           = $userStorage->id;
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url = HOST . 'collection/mass-download-data' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);

//$status_access
$area_cache = $QArea->get_cache();
if($info_team->access_group == ADMINISTRATOR_ID){
    $status_access = 1;
    $area = $area_cache;
}else if($info_team->access_group == 7){
    $status_access = 2;
    $area = $area_cache;
}else if($info_team->access_group == My_Staff_Group::ASM || $info_team->access_group ==  My_Staff_Group::SALES_ADMIN){
    $status_access = 3;
    $area_list      = $QAsm->get_cache($userStorage->id)['area'];
    foreach ($area_list as $key => $value) {
        $area[$value] = $area_cache[$value];
    }
}
$this->view->status_access       = $status_access;
if($id){
    if(!empty($export)) {
        $this->massExportData($area);//$area
    }
    
    if(!empty($export_result) && ($info_team->access_group != My_Staff_Group::ASM && $info_team->access_group !=  My_Staff_Group::SALES_ADMIN)) {
        $this->massExportDataReview();
    }
}