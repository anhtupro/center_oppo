<?php
$export = $this->getRequest()->getParam('export');
$area = $this->getRequest()->getParam('area');
$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
$QAsm         = new Application_Model_Asm();
$list_regions = $QAsm->get_cache($userStorage->id);
$area         = $this->getRequest()->getParam('area');
$export       = $this->getRequest()->getParam('export');
$export_all       = $this->getRequest()->getParam('export_all');
$list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
$str_area = implode(",", $list_regions); 

if(in_array($userStorage->id, array(5899, 2584,22716,7,8807)) ){
    $sql = "SELECT *  FROM salary_increase_dot1_2020 ORDER BY area ASC";
}else{
    $sql = "SELECT g.*  FROM salary_increase_dot1_2020 g WHERE g.area IN (
                             SELECT name FROM area WHERE id IN ($str_area)
                    ) ORDER BY g.area ASC
                  ";
}
$db = Zend_Registry::get('db');
$stmt = $db->prepare($sql);
$stmt->execute();
$list_salary_increase = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = null;
$list_area=array();


if(in_array($userStorage->id, array(5899, 2584,22716,7,8807)) ){
    $sql_salary_increase_total = "SELECT *  FROM salary_increase_total_dot1_2020 ORDER BY khu_vuc ASC";
}else{
    $sql_salary_increase_total = "SELECT *  FROM salary_increase_total_dot1_2020 s WHERE s.khu_vuc IN (
                             SELECT name FROM area WHERE id IN ($str_area)
                    ) ORDER BY s.khu_vuc ASC";
}

$stmt = $db->prepare($sql_salary_increase_total);
$stmt->execute();
$list_salary_increase_total = $stmt->fetchAll();

if(!empty($export) && !empty($area)) {
    $this->exportSalaryIncrease20($list_salary_increase,$area);
}
foreach($list_salary_increase_total as $key =>$value){
    if(!in_array($value['area'], $list_area)){
        array_push($list_area,$value);
    }
}


//debug($list_area); exit;

$this->view->list_area      = $list_area;
