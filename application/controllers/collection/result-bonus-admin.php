<?php
    $area_list    = array();
    $db             = Zend_Registry::get('db');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id       = $userStorage->id; 
    $QAsm         = new Application_Model_Asm();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $area         = $this->getRequest()->getParam('area');
    $export       = $this->getRequest()->getParam('export');
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $str_area = implode(",", $list_regions); 
    $title = $userStorage->title;
    $QTeam  = new Application_Model_Team();
    $team = $QTeam->find($title);
    $team_info = $team->current();
    $group_id  = $team_info['access_group'];
    
    if( !in_array($group_id,array(HR_ID, ADMINISTRATOR_ID, ASM_ID))){
        $this->_redirect(HOST);
    }
    
    if(!empty($area) AND !empty($export)){ 
        
         if ( in_array($group_id,array(ASM_ID))) {
               $sql_area  = "SELECT * FROM area WHERE id IN ($str_area) AND name = '$area' ";
               $stmt_area = $db->prepare($sql_area);
               $stmt_area->execute();
               $area_check = $stmt_area->fetchAll();
               $stmt_area->closeCursor();
               if(empty($area_check)){
                    $this->_redirect("/collection/result-bonus-admin");
               }
         }
        $sql_detail = "SELECT * FROM `backup`.kq_bonus_admin_2019 WHERE area = '$area' ";
      
        $stmt_detail = $db->prepare($sql_detail);
        $stmt_detail->execute();
        $data = $stmt_detail->fetchAll();
        $stmt_detail->closeCursor();
       
        //////////////////
       
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);

    include 'PHPExcel/IOFactory.php';
    //\htdocs\sell\public\templete\target
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'templete' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'theme_kq_bonus_admin_20199.xlsx';

        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($path);
        $sheet         = $objPHPExcel->getActiveSheet();

        $index = 6;
        $stt   = 0;
        $color    = 'c4bd97';
        if(!empty($data))
        foreach ($data as $key => $value):
            $alpha   = 'A';
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
//            $sheet->getCell($alpha++ . $index)->setValueExplicit($key+1, PHPExcel_Cell_DataType::TYPE_STRING);
//            if(!empty($value['ghichu'])){
//                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
//                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
//           
//            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['congty'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            } 
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tinh'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['loaihd'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['ngayky'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['name'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['dep'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['team'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
           $sheet->getCell($alpha++ . $index)->setValueExplicit($value['join'], PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
           
            }
            
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['thamnien'],1), PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['thamnien1'],1), PHPExcel_Cell_DataType::TYPE_STRING);
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['tamoff'],1), PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(round($value['b'],1), PHPExcel_Cell_DataType::TYPE_STRING);
            
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['luongsau']), PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['luongtruoc']), PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['sothang'], PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['thuongthang13']), PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['month'], PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['bonus']), PHPExcel_Cell_DataType::TYPE_STRING);
            
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong']), PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tongthunhap']), PHPExcel_Cell_DataType::TYPE_STRING);
            
            if(!empty($value['ghichu'])){
                 $sheet->getStyle($alpha . $index)->applyFromArray(array('fill' => array('type'  =>
                            PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => $color))));
            }
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['binhquan']), PHPExcel_Cell_DataType::TYPE_STRING);
           
          
            
            $index++;
        endforeach;

        $filename  = $area . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        $objWriter->save('php://output');
        exit;    
    }
    
  
    if ( in_array($group_id,array(HR_ID, ADMINISTRATOR_ID))) {
            $sql = "SELECT * FROM `backup`.kq_bonus_admin_2019 ";
    }else{
            $sql = "SELECT * FROM `backup`.kq_bonus_admin_2019
              WHERE area IN (
                          SELECT name FROm area WHERE id IN ($str_area)
                             ) group by area ";
    }
    
    
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $info = $stmt->fetchAll();
    $stmt->closeCursor();
    $this->view->viewed_area_id = $list_regions;
    $this->view->info = $info;

    