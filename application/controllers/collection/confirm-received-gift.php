<?php

$QLunar2021 = new Application_Model_Lunar2021();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$status = 0;
$where = [];
$where[] = $QLunar2021->getAdapter()->quoteInto("code = ? ", $userStorage->code);
$where[] = $QLunar2021->getAdapter()->quoteInto("status_delivery = ? ", 1);
$data_check = $QLunar2021->fetchRow($where);

if ($QLunar2021->check_staff_lunar($userStorage->id) && empty($data_check)) {
    $status = 1;
}
$this->view->status = $status;
