<?php
    $id = $this->getRequest()->getParam('id');
    $QStaff            = new Application_Model_Staff();
    $staffRowset       = $QStaff->find($id);
    $staff             = $staffRowset->current();
    $this->view->staff = $staff;
    
    $QCompany              = new Application_Model_Company();
    $this->view->companies = $QCompany->get_cache();
    $QArea            = new Application_Model_Area();
    $this->view->areas = $QArea->get_cache();
    $this->view->staff_id = $id;
    $QRegionalMarket                = new Application_Model_RegionalMarket();
    $this->view->all_province_cache = $QRegionalMarket->get_cache();
    
    if($id){
        $rowset = $QRegionalMarket->find($staff->regional_market);

        if ($rowset) {
            $this->view->regional_market = $regional_market             = $rowset->current();
            $where                       = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);

            $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

            $rowset           = $QArea->find($regional_market['area_id']);
            $this->view->area = $rowset->current();
            
             //get teams
            $QTeam                       = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
            $this->view->teamsCached                 = $QTeam->get_cache();
        }
    }