<?php
    $export         = $this->getRequest()->getParam('export');
    
    if(!empty($export)){
        $from         = $this->getRequest()->getParam('from');
        $to         = $this->getRequest()->getParam('to');
        $from = str_replace('/', '-', $from);
        $to = str_replace('/', '-', $to);

        $from=date("Y-m-d 00:00:00", strtotime($from));
        $to=date("Y-m-d 23:59:59", strtotime($to));
        
        if($from <= '2019-01-01 00:00:00'){
            echo 'Thời gian không hợp lệ';
            exit();
        }
        $db             = Zend_Registry::get('db');
        $sql = "SELECT ik.imei_sn,
                    st.name as shopname, 
                    ts.customer_name, 
                    ts.phone_number, 
                    g.desc as good_name,
                    re.name as area, 
                    ik.timing_date

                    FROM (SELECT * FROM imei_kpi ik WHERE ik.timing_date BETWEEN '$from' AND  '$to' ) ik 
                    INNER JOIN store st on st.id = ik.store_id
                    INNER JOIN timing_sale ts on ts.imei = ik.imei_sn
                    INNER JOIN warehouse.good g on g.id = ik.good_id
                    INNER JOIN area re on re.id = ik.area_id
                    WHERE ik.timing_date BETWEEN '$from' AND  '$to'  AND IFNULL(st.is_brand_shop,0) = 1";
        
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = null;
        if(!empty($data)){
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();
            $heads = array(
                $alphaExcel->ShowAndUp() => 'STT',
                $alphaExcel->ShowAndUp() => 'Imei',
                $alphaExcel->ShowAndUp() => 'Shop Name',
                $alphaExcel->ShowAndUp() => 'Customer Name',
                $alphaExcel->ShowAndUp() => 'Address',
                $alphaExcel->ShowAndUp() => 'Good Name',
                $alphaExcel->ShowAndUp() => 'Area',
                $alphaExcel->ShowAndUp() => 'timing',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);

            $sheet->getStyle('A1:Q1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(10);
            $sheet->getColumnDimension('B')->setWidth(30);
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(15);
            $sheet->getColumnDimension('F')->setWidth(30);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('H')->setWidth(15);

            foreach ($data as $key => $value) {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['imei_sn'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['shopname']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['customer_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['address']);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['phone_number'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['good_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['area']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['timing_date']);

            }

            $filename = 'Info - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }
    }