<?php

set_time_limit(0);
error_reporting(0);
ini_set('memory_limit', -1);
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();

//$objExcel = new PHPExcel();
$path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'gift_final.xlsx';

//$PHPExcel->createSheet();

$inputFileType = PHPExcel_IOFactory::identify($path);
$objReader     = PHPExcel_IOFactory::createReader($inputFileType);

$PHPExcel   = $objReader->load($path);

$sheet = $PHPExcel->getActiveSheet();
$index = 5;
$stt = 0;

foreach($list_gift as $key => $value):
    if($value['area']==$area){
        $alpha = 'A';

        $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['full_name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['department']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['team']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['contract_signed_at']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['contract_term']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['contract_expired_at']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['joined_at']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['condition']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['description']), PHPExcel_Cell_DataType::TYPE_STRING);

        $index++;
    }

endforeach;



$filename = 'list_gift_lunar_new_year_' .$area.'_'. date('d-m-Y');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');


exit;
