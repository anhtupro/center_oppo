<?php

//echo "Đã hết thời gian đăng ký, hệ thống đã khóa. !!"; die();

$this->_helper->layout->disableLayout();
$this->userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QLunar2021 = new Application_Model_Lunar2021();
$QProvinceDistrict = new Application_Model_ProvinceDistrict();
$Qward = new Application_Model_Ward;

if (empty($this->userStorage)) {
    $url = HOST . 'user/login?b=' . urlencode(HOST . 'collection/lunar-new-year2021');
    $this->_redirect($url);
}
$this->view->userStorage = $this->userStorage;
if ($QLunar2021->check_staff_lunar($this->userStorage->id) == 0) {
    $this->render('/not-join2021');
} else {
    $code = $this->userStorage->code;
    $data = $QLunar2021->check_staff($code);
    if (!empty($data)) {
        $list_district = $QProvinceDistrict->get_district_by_province($data['province']);
        $wards = $Qward->nget_ward_delivery_by_district($data['district']);
        $this->view->data = $data;
        $this->view->list_district = $list_district;
        $this->view->list_ward = $wards;
    }
    $province_district = new Application_Model_ProvinceDistrict();
    $wardmode = new Application_Model_Ward();
    $list_pro = $province_district->get_cache();
    asort($list_pro);
    $this->view->list_pro = $list_pro;

    if ($this->getRequest()->isPost()) {
        $fullname = $this->getRequest()->getParam('fullname');
        $relationship = $this->getRequest()->getParam('relationship');
        $province = $this->getRequest()->getParam('province');
        $district = $this->getRequest()->getParam('district');
        $ward = $this->getRequest()->getParam('ward');
        $address = $this->getRequest()->getParam('address');
        $re_number = $this->getRequest()->getParam('re-number');
        $se_number = $this->getRequest()->getParam('se-number');
        if (!empty($fullname) && !empty($relationship) && !empty($province) && !empty($district) && !empty($address) && !empty($re_number) && !empty($se_number)) {
            $data_insert = array(
                "fullname" => $fullname,
                "relationship" => $relationship,
                "province" => $province,
                "district" => $district,
                'ward' => $ward,
                "address" => $address,
                "phone" => $re_number,
                "staff_phone" => $se_number,
                "status" => 1,
            );
            if (empty($data)) {
                $data_insert['code'] = $this->userStorage->code;
                $QLunar2021->insert($data_insert);
            } else {
                $where = $QLunar2021->getAdapter()->quoteInto('code =?', $this->userStorage->code);
                $QLunar2021->update($data_insert, $where);
            }
            $this->_redirect(HOST . 'collection/lunar-confirm-success2021');
        }
    }
    $this->render('/create-record2021');
}
