<?php

set_time_limit(0);
error_reporting(0);
ini_set('memory_limit', -1);
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();

//$objExcel = new PHPExcel();
$path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'dstangluongdot12020.xlsx';

//$PHPExcel->createSheet();

$inputFileType = PHPExcel_IOFactory::identify($path);
$objReader     = PHPExcel_IOFactory::createReader($inputFileType);

$PHPExcel   = $objReader->load($path);

$sheet = $PHPExcel->getActiveSheet();
$index = 9;
$stt = 0;

foreach($list_salary_increase as $key => $value):
    if($value['area']==$area){
        $alpha = 'A';

        $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['province']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['district']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['nhom_tham_nien']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['truoc']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sau']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['tang_10']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['tang_20']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['dieu_chinh_len_chuyen_vien_ban_hang']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim(number_format($value['luong_co_ban_chuc_danh_khu_vuc'])), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim(number_format($value['luong_nha_nuoc'])), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim(number_format($value['muc_tang'])), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim(number_format($value['luong_cu'])), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim(number_format($value['luong_moi'])), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( round(trim($value['ti_le_chenh_lech']),1) .'%', PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['oct']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['nov']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['dec']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['jan']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['feb']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['mar']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sell_out_binh_quan_6thang']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['group_sell_out']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['ghi_chu']), PHPExcel_Cell_DataType::TYPE_STRING);
        $index++;
    }

endforeach;



$filename = 'list_salary_increase_' .$area.'_'. date('d-m-Y');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');


exit;
