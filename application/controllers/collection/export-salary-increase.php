<?php

set_time_limit(0);
error_reporting(0);
ini_set('memory_limit', -1);
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();

//$objExcel = new PHPExcel();
$path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'dstangluong.xlsx';

//$PHPExcel->createSheet();

$inputFileType = PHPExcel_IOFactory::identify($path);
$objReader     = PHPExcel_IOFactory::createReader($inputFileType);

$PHPExcel   = $objReader->load($path);

$sheet = $PHPExcel->getActiveSheet();
$index = 9;
$stt = 0;

foreach($list_salary_increase as $key => $value):
    if($value['area']==$area){
        $alpha = 'A';

        $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['province']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['district']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['tham_nien']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['nhom_tham_nien']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['truoc']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sau']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['tang_10']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['tang_20']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['tang_1000']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['dieu_chinh_len_chuyen_vien_ban_hang']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['luong_nha_nuoc']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['muc_tang']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['luong_cu']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['luong_moi']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( round(trim($value['ti_le_chenh_lech'])*100,1) .'%', PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['apr']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['may']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['jun']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['jul']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['aug']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sep']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sell_out_6_thang']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['group_sell_out']), PHPExcel_Cell_DataType::TYPE_STRING);
        if(trim($value['ghi_chu'])<1){
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['ghi_chu'])*100 . '%', PHPExcel_Cell_DataType::TYPE_STRING);
        }
        else{
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['ghi_chu']), PHPExcel_Cell_DataType::TYPE_STRING);

        }

        $index++;
    }

endforeach;



$filename = 'list_salary_increase_' .$area.'_'. date('d-m-Y');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');


exit;
