<?php
$area_list    = array();
$db             = Zend_Registry::get('db');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id       = $userStorage->id;
$QAsm         = new Application_Model_Asm();
$list_regions = $QAsm->get_cache($userStorage->id);

$area         = $this->getRequest()->getParam('area');

$export       = $this->getRequest()->getParam('export');
$export_all       = $this->getRequest()->getParam('export_all');
$list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
$str_area = implode(",", $list_regions);
$title = $userStorage->title;
$QTeam  = new Application_Model_Team();
$team = $QTeam->find($title);
$team_info = $team->current();
$group_id  = $team_info['access_group'];

if(!empty($area) AND !empty($export)){

    $sql_detail = "SELECT p.*,c.name as company_name FROM phep_nam_2019 p inner join staff s on p.code=s.code
        inner join company c on s.company_id=c.id where p.area='$area'";
    
    $stmt_detail = $db->prepare($sql_detail);
    $stmt_detail->execute();
    $data = $stmt_detail->fetchAll();
    $stmt_detail->closeCursor();

    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);

    include 'PHPExcel/IOFactory.php';
    //\htdocs\sell\public\templete\target
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'phep-nam.xlsx';

    $inputFileType = PHPExcel_IOFactory::identify($path);
    $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel   = $objReader->load($path);
    $sheet         = $objPHPExcel->getActiveSheet();

    $index = 7;
    $intCount   = 1;

    if(!empty($data))
        foreach ($data as $key => $value):
            $alpha   = 'B';

            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['department'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['team'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['join_at'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['off_date'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['company_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $intCount++);

            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['name'], PHPExcel_Cell_DataType::TYPE_STRING);

            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_quy_phep']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_phep_da_su_dung']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_phep_ton']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['luong_tinh_hoan_phep']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_hoan_phep']), PHPExcel_Cell_DataType::TYPE_STRING);

            $stt++;
            $index++;
        endforeach;

    $filename  = $area . date('Y-m-d H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    header('Set-Cookie: fileLoading=true');
    $objWriter->save('php://output');
    exit;

}

if(!empty($export_all)){

    $sql_detail = "SELECT p.*,c.name as company_name FROM phep_nam_2019 p inner join staff s on p.code=s.code
    inner join company c on s.company_id=c.id ";

    $stmt_detail = $db->prepare($sql_detail);
    $stmt_detail->execute();
    $data = $stmt_detail->fetchAll();
    $stmt_detail->closeCursor();

    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);

    include 'PHPExcel/IOFactory.php';
    //\htdocs\sell\public\templete\target
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'target' . DIRECTORY_SEPARATOR . 'phep-nam.xlsx';

    $inputFileType = PHPExcel_IOFactory::identify($path);
    $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel   = $objReader->load($path);
    $sheet         = $objPHPExcel->getActiveSheet();

    $index = 7;
    $intCount   = 1;

    if(!empty($data))
        foreach ($data as $key => $value):
            $alpha   = 'B';

            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['department'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['team'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['join_at'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['off_date'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['company_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $intCount++);

            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['name'], PHPExcel_Cell_DataType::TYPE_STRING);

            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_quy_phep']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_phep_da_su_dung']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_phep_ton']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['luong_tinh_hoan_phep']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($value['tong_hoan_phep']), PHPExcel_Cell_DataType::TYPE_STRING);

            $stt++;
            $index++;
        endforeach;

    $filename  = $area . date('Y-m-d H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    header('Set-Cookie: fileLoading=true');
    $objWriter->save('php://output');
    exit;

}

$db          = Zend_Registry::get('db');

  
    if ( in_array($group_id,array(HR_ID, ADMINISTRATOR_ID))) {
            $sql = "SELECT * FROM phep_nam_2019 group by area order by area ";
    }else{
            $sql = "SELECT * FROM phep_nam_2019
              WHERE area IN (
                          SELECT name FROm area WHERE id IN ($str_area)
                             ) group by area order by area";
    }
    
    
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $info = $stmt->fetchAll();
    $stmt->closeCursor();
    $this->view->viewed_area_id = $list_regions;
    $this->view->info = $info;

	
	
$select = $db->select()->from(array('p' => 'phep_nam_2019'), array(
    'area',
    'tong_quy_phep' => new Zend_Db_Expr('sum(tong_quy_phep)'),
    'tong_phep_da_su_dung'=> new Zend_Db_Expr('sum(tong_phep_da_su_dung)')))
    ->joinInner(array('s' => 'staff'), 's.code=p.code', array())
    ->joinInner(array('c' => 'company'), 'c.id=s.company_id', array('company_name'=>'p.name'));
	
if ( in_array($group_id,array(HR_ID, ADMINISTRATOR_ID))) {

        $select->group('area');
}
else{
    $select->joinInner(array('r'=>'regional_market'),'r.id=s.regional_market',array())
        ->joinInner(array('a'=>'area'),'a.id=r.area_id',array())
        ->where('a.id IN (?)',$str_area)
        ->group('area');
}
$info = $db->fetchAll($select);

$teams            = $QTeam->get_cache();
$this->view->teams = $teams;

$this->view->viewed_area_id = $list_regions;
// $this->view->info = $info;

$this->view->staff_id = $staff_id;

