<?php 
// config for template

define('START_ROW',11);
define('code', 0);
define('ketqua', 6);
define('sothang', 7);
define('note', 8);

$datetime = date('Y-m-d H:i:s');
$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
$this->_helper->layout->disableLayout();
 $this->_helper->viewRenderer->setNoRender();
$QBonusPgs2019 = new Application_Model_BonusPgs2019();

if ($this->getRequest()->getMethod() == 'POST') { // Big IF
 
    set_time_limit(0);
    ini_set('memory_limit', -1);
    // file_put_contents(APPLICATION_PATH.'/../public/files/mou/lock', '1');
    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
    
    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
     
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }
    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {    

        $this->view->errors = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet           = $objPHPExcel->getSheet(0);
    $highestRow      = $sheet->getHighestRow();
    $highestColumn   = $sheet->getHighestColumn();

    $dataMassUpload  = array();
    $data_staff_code = array();

    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
         
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
          
            if(!empty($rowData[code])){
           define('code', 1);

                
                $data_staff_code[] = trim($rowData[code]);      
                
                $data = array(
                    'ketqua'        => trim($rowData[ketqua]),
                    'sothang'        => trim($rowData[sothang]),
                    'note'      => trim($rowData[note]),
                    'code'      => trim($rowData[code])
                );
                $dataMassUpload[] = $data;
            }
         
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }
        // nothing here
    } // END loop through order rows

    $progress->flush(30);
    $db = Zend_Registry::get('db');
    // $db->beginTransaction();
   
    try {
        if (!empty($dataMassUpload)) {
                    $select = $db->select()
                            ->from(array('p' => 'staff'), array('p.code'))
                            ->where('p.code in (?)', $data_staff_code);
                    $result_code = $db->fetchCol($select);
                    
                    if (count($data_staff_code) <> count($result_code)) {
                        $diff = array_diff($data_staff_code, $result_code);
                        throw new Exception("Nhân viên không tồn tại hoặc đã nghỉ việc hoặc trong file bị trùng code: " . implode($diff, ','));
                    }
//                    My_Controller_Action::insertAllrow($dataMassUpload, 'mass_upload_insurance');
                    
                    foreach ($dataMassUpload as $k => $upload){
                        $data_staff = array();         
                        
                        if(!empty($upload['code'])){
                            $data = array(
                                'ketqua' => $upload['ketqua'],
                                'sothang' => $upload['sothang'],
                                'note' => $upload['note']
                            );
                            $staff_code = $upload['code'];
                       
                            $where_upload = $QBonusPgs2019->getAdapter()->quoteInto('code = ?', $staff_code);
                            $QBonusPgs2019->update($data, $where_upload);
                        }
                       
                    }
                 
               //    My_Controller_Action::insertAllrow($dataMassUpload, 'mass_upload_insurance');
//            exit();
        } else {
            throw new Exception("Danh sách nhân viên không hợp lệ");

        }
        // $db->commit();
        
        $progress->flush(99);
        
        $progress->flush(100);
        echo 'Upload thành công!';
//        die;
    } catch (Exception $e) {
        // $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}
