<?php
   
    $QCompany              = new Application_Model_Company();
    $this->view->companies = $QCompany->get_cache();
    $QArea            = new Application_Model_Area();
    $QTeam            = new Application_Model_Team();  
    $this->view->areas = $QArea->get_cache();
    $this->view->staff_id = $id;
    $QRegionalMarket                = new Application_Model_RegionalMarket();
    $this->view->all_province_cache = $QRegionalMarket->get_cache();
    
    $arrCols                         = array(
            'note'           => 's.note',
            'sl.transfer_id',
            'staff_id'       => 's.staff_id',
            'staff_code'     => 'st.`code`',
            'sl.object',
            's.from_date',
            'info_types'     => new Zend_Db_Expr('GROUP_CONCAT(sl.info_type)'),
            'current_values' => new Zend_Db_Expr('GROUP_CONCAT(sl.current_value)'),
        );
    $db = Zend_Registry::get('db');
        $select                          = $db->select()
                ->from(array('s' => 'staff_transfer_tmp'), $arrCols)
                ->join(array('st' => 'staff'), 'st.id = s.staff_id', array())
                ->joinLeft(array('sl' => 'staff_log_detail_tmp'), 's.id = sl.transfer_id', array())
                ->where("s.`status` = 1")
                ->where("s.`finish` = 0")
                ->where("IFNULL(sl.`info_type`,0) <> 3")
                ->group('sl.transfer_id')
                ->order('s.from_date DESC')
        ;
        $result                          = $db->fetchAll($select);

        $this->view->resultStaffTransfer = $result;
  
        $where                        = $QRegionalMarket->getAdapter()->quoteInto('1 = 1', null);
        $this->view->regional_markets = $QRegionalMarket->fetchAll($where);
            
        $recursiveDeparmentTeamTitle  = $QTeam->get_recursive_cache();
       
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
        $this->view->teamsCached      = $QTeam->get_cache_team();
        $this->view->titlesCached      = $QTeam->get_cache_title();
