<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$total              = $this->getRequest()->getParam('total');
$time_create        = $this->getRequest()->getParam('time_create');
$insurance_id       = $this->getRequest()->getParam('insurance_id');

$QInsuranceStaff    = new Application_Model_InsuranceStaff();
$userStorage 	    = Zend_Auth::getInstance ()->getStorage ()->read ();

if(!empty($time_create) AND ($total == 1) AND !empty($insurance_id) ){
    $where             = array();
    $where[]           = $QInsuranceStaff->getAdapter()->quoteInto('id = ?' , $insurance_id);
    
    $insurance_detail  = $QInsuranceStaff->find($insurance_id)->current();
   
    $time_create_old   = $insurance_detail->toArray()['locked_at'];
    $note_old          = $insurance_detail->toArray()['note'];
    $updated_by        =  $userStorage->id;  
        $datetime = date ( 'Y-m-d H:i:s' );
        $data = array(
            'locked_at'  => DateTime::createFromFormat('d/m/Y', $time_create)->format('Y-m-d'),
            'created_at' => DateTime::createFromFormat('d/m/Y', $time_create)->format('Y-m-d'),
            'note'       => $note_old . "Update time_create: $time_create_old created_at: $datetime By: $updated_by "
        );
        
        $QInsuranceStaff->update($data, $where);
        
}else{
   return array('code'=>1,'message'=>'Done');
}
