<?php 
    $id            = $this->getRequest()->getParam('id');
    $name          = $this->getRequest()->getParam('name');
    $note          = $this->getRequest()->getParam('note');
    $code          = $this->getRequest()->getParam('code');
    $province      = $this->getRequest()->getParam('province');
    $QHospital     = new Application_Model_Hospital();
    $flashMessenger  = $this->_helper->flashMessenger;
    if($id){
        $hospital  = $QHospital->find($id)->current();
        $this->view->hospital = $hospital;
    }                    
    $QProvince     = new Application_Model_Province();
    $provinces     = $QProvince->get_all();
    
    if ( $this->getRequest()->getMethod() == 'POST' ) {
       
        if($province){
           $whereProvince  =  $QProvince->getAdapter()->quoteInto('id = ? ', $province);
           $provinceInfo   =  $QProvince->fetchRow($whereProvince);
           $province_code  =  $provinceInfo->toArray()['code'];
           $province_name  =  $provinceInfo->toArray()['name'];
           if(!empty($province_code) && !empty($province_name) && !empty($name) && !empty($code)){
             $data = array(
                        'name'              => $name,
                        'code'              => $code,
                        'province_code'     => $province_code,
                        'province_name'     => $province_name,
                        'province_id'       => $province,
                        'note'              => $note
                    );
                 
                if($id){
                    $whereHospital  =  $QHospital->getAdapter()->quoteInto('id = ? ', $id);  
                    $QHospital->update($data, $whereHospital);
                }else{
                    $QHospital->insert($data);
                }   
                $flashMessenger->setNamespace('success')->addMessage('Done.');
                $this->_redirect("/insurance/list-hospital");
           }
           
        }
      
    }
    
    $this->view->provinces	= $provinces;