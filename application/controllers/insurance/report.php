<?php 
	$flashMessenger = $this->_helper->flashMessenger;
        $date_ins       = $this->getRequest()->getParam('date_ins',NULL);
        $date_approved  = $this->getRequest()->getParam('date_approved',NULL);
        $name           = $this->getRequest()->getParam('name');
        $company_id     = $this->getRequest()->getParam('company_id');
        $code           = $this->getRequest()->getParam('code');
        $ins_number     = $this->getRequest()->getParam('ins_number');
        $type           = $this->getRequest()->getParam('type',-1);
        $locked         = $this->getRequest()->getParam('locked',-1);
        $month          = $this->getRequest()->getParam('month',date('m/Y'));
        $unit_code_id   = $this->getRequest()->getParam('unit_code_id');
        $before         = $this->getRequest()->getParam('before',1);
        $total          = 0;
        $page           = $this->getRequest()->getParam('page',1);
        $desc           = $this->getRequest()->getParam('desc',0);
        $sort           = $this->getRequest()->getParam('sort','locked');
        $limit          = 500;
        $report_tang    = $this->getRequest()->getParam('report_tang',0);
        $report_giam    = $this->getRequest()->getParam('report_giam',0);
        $report_chotso  = $this->getRequest()->getParam('report_chotso',0);
        $s_qdnv         = $this->getRequest()->getParam('s_qdnv');
        $get_qdnv       = $this->getRequest()->getParam('get_qdnv');
        $get_qdnkl       = $this->getRequest()->getParam('get_qdnkl');
        $back_url = $this->getRequest()->getParam('back_url','/insurance/report');

        $QInsurance = new Application_Model_Insurance();
        $params = array(
            'date_ins'      => $date_ins,
            'date_approved' => $date_approved,
            'name'          => $name,
            'company_id'    => $company_id,
            'unit_code_id'  => intval($unit_code_id),
            'code'          => $code,
            'ins_number'    => $ins_number,
            'type'          => intval($type),
            'locked'        => intval($locked),
            'month'         => $month,
            'before'        => intval($before),
            's_qdnv'        => trim($s_qdnv),
        );

        if($report_tang){
            $this->reportTangAction($params);
            exit;
        }

        if($report_giam){
            $this->reportGiamAction($params);
            exit;
        }

        if($report_chotso){
            $this->reportChotSoAction($params);
            exit;
        }

        if($get_qdnv){
            $result_qdnv = $this->getQdnv($month);
            if($result_qdnv['code'] == -1){
                $flashMessenger->setNamespace('error')->addMessage($result_qdnv['message']);
            }else{
                $flashMessenger->setNamespace('success')->addMessage('Done');
            }
            $this->redirect($back_url);
        }

        if($get_qdnkl){
            $result_qdnv = $this->getQdNghiKhongLuong($month);
            if($result_qdnv['code'] == -1){
                $flashMessenger->setNamespace('error')->addMessage($result_qdnv['message']);
            }else{
                $flashMessenger->setNamespace('success')->addMessage('Done');
            }
            $this->redirect($back_url);
        }
        
      
        $list = $QInsurance->fetchPaginationReport($page,$limit,$total,$params);
        $this->view->list = $list;

        $dates = $QInsurance->getDateIns($month);
        $this->view->dates = $dates;

        $dates_approved = $QInsurance->getDateInsApproved($month);
        $this->view->dates_approved = $dates_approved;

        $QCompanies            = new Application_Model_Company();
        $companies             = $QCompanies->get_cache();
        $this->view->companies = $companies;

        $QUnitCode            = new Application_Model_UnitCode();
        $unit_codes             = $QUnitCode->get_all();
        $this->view->unit_codes = $unit_codes;
        
        $QStatusOption         = new Application_Model_StatusOption();
        $options               = $QStatusOption->get_all();
        $this->view->options   = $options;

        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->params           = $params;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->url              = HOST.'insurance/report/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset           = $limit*($page-1);
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;

        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $this->view->uri = $uri;
?>