<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';

$userStorage           = Zend_Auth::getInstance()->getStorage()->read();
$QPInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
$QLeaveDetail          = new Application_Model_LeaveDetail();
$QStaff                = new Application_Model_Staff();

$staff_id = $this->getRequest()->getParam('staff_id');

$insurance_number     = $this->getRequest()->getParam('insurance_number', NULL);
$have_book            = $this->getRequest()->getParam('have_book', NULL);
$close_book           = $this->getRequest()->getParam('close_book', NULL);
$return_ins_book_time = $this->getRequest()->getParam('return_ins_book_time', NULL);
$qdnv                 = $this->getRequest()->getParam('qdnv', NULL);
$code_BHYT_ins        = $this->getRequest()->getParam('code_BHYT_ins', NULL);
$province_hospital    = $this->getRequest()->getParam('province_hospital', NULL);
$hospital_id          = $this->getRequest()->getParam('hospital_id', NULL);
$have_BHYT_time       = $this->getRequest()->getParam('have_BHYT_time', NULL);
$note_ins             = $this->getRequest()->getParam('note_ins', NULL);
$QInsuranceLogInfo    = new Application_Model_InsuranceLogInfo();
$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($staff_id) {

    try {

        $where     = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
        $Staff_Row = $QStaff->fetchRow($where);

        if (empty($Staff_Row)) {
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
            echo '<div class="alert alert-error">Lỗi : ID truyền vào không tồn tại.</div>';
            exit;
        } else {
            // chuyển định dạng ngày
            if (!empty($have_book)) {
                $month_obj = date_create_from_format('d/m/Y', $have_book);
                $have_book = $month_obj->format('Y-m-d');
            }
            if (!empty($close_book)) {
                $month_obj  = date_create_from_format('d/m/Y', $close_book);
                $close_book = $month_obj->format('Y-m-d');
            }
            if (!empty($return_ins_book_time)) {
                $month_obj            = date_create_from_format('d/m/Y', $return_ins_book_time);
                $return_ins_book_time = $month_obj->format('Y-m-d');
            }
            if (!empty($have_BHYT_time)) {
                $month_obj            = date_create_from_format('d/m/Y', $have_BHYT_time);
                $have_BHYT_time = $month_obj->format('Y-m-d');
            }
            $beforeStaff = $QStaff->find($staff_id)->current();
            $data_update = array(
                'updated_by'           => $userStorage->id,
                'updated_at'           => date('Y-m-d H:i:s'),
                'insurance_number'     => $insurance_number ? $insurance_number : null,
                'have_book'            => $have_book ? $have_book : null,
                'close_book'           => $close_book ? $close_book : null,
                'return_ins_book_time' => $return_ins_book_time ? $return_ins_book_time : null,
                'qdnv'                 => $qdnv ? $qdnv : null,
                'code_BHYT_ins'        => $code_BHYT_ins ? $code_BHYT_ins : null,
//                'province_hospital'    => $province_hospital ? $province_hospital : null,
                'hospital_id'          => $hospital_id ? $hospital_id : null,
                'have_BHYT_time'       => $have_BHYT_time ? $have_BHYT_time : null,
                'note_ins'             => $note_ins ? $note_ins : null,
            );

            $r1 = $QStaff->update($data_update, $where);
            
            $afterStaff = $QStaff->find($staff_id)->current();
            $before = $beforeStaff->toArray();
            $after = $afterStaff->toArray();
            $QInsuranceLogInfo->save($before,$after,$staff_id,$userStorage->id);
        }//End else


        if (empty($r1)) {
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
            echo '<div class="alert alert-error">Lỗi : Insert Không thành công.</div>';
            exit;
        }

        $db->commit();
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
        echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
        echo '<div class="alert alert-success">Done</div>';
        /// load lại trang
//        $back_url = " report-list";
//        echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
    } catch (Exception $e) {
        $db->rollBack();
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
        echo '<div class="alert alert-error">Lỗi : ' . $e->getMessage() . '</div>';
    }
    exit;
} else {

    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : Thiếu Staff_ID truyền vào.</div>';
    exit;
}
