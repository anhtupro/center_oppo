<?php 
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $QInsurance  = new Application_Model_Insurance();
    $db          = Zend_Registry::get('db');
    $companies   = array(1, 2, 3);
    $date        = My_Date::normal_to_mysql($month);
    $from_create = new DateTime($date);
    $from_create = $from_create->sub(new DateInterval('P1M'));
    $from        = $from_create->format('Y-m-16');
    
    $to_create  = new DateTime($date);
    $to         = $to_create->format('Y-m-15');
    $month_year = $to_create->format('my');
    $staff_ids           = $this->getRequest()->getParam('staff_ids');
    $action_insurance    = $this->getRequest()->getParam('action_insurance');
    
    $QStaff     = new Application_Model_Staff();
    
    if($staff_ids AND $action_insurance == '#giam'){
        $db->beginTransaction();
        
        try{
            foreach($companies as $company_id){
                $cols = array(
                    'a.qdnv',
                    'stt' => 'CASE WHEN a.qdnv IS NOT NULL AND a.qdnv != ""  THEN  SUBSTRING_INDEX(a.qdnv,"/",1)  ELSE 0 END',
                );
        
                $select = $db->select()
                ->from(array('a'=>'staff'),$cols)
                ->where('a.qdnv IS NOT NULL AND a.qdnv != ""')
                ->where("a.qdnv LIKE '%$month_year%' ")
                ->where('a.company_id = ?',$company_id)
                ->order('CASE WHEN a.qdnv IS NOT NULL AND a.qdnv != "" THEN  SUBSTRING_INDEX(a.qdnv,"/",1) ELSE 0 END DESC')
                ->limit(1)
                ;
        
                $row = $db->fetchRow($select);
                $stt = 1;
                if($row){
                    $stt = intval($row['stt']) + 1;
                }
        
                foreach($staff_ids as $item){
                    $where_staff      = $QStaff->getAdapter()->quoteInto('id = ?' , $item);
                    $row_staff        = $QStaff->fetchRow($where_staff);
                    $company_staff    = $row_staff->toArray()['company_id'];
                    $qdnv_staff       = $row_staff->toArray()['qdnv'];
                    if(($company_staff == $company_id) AND empty($qdnv_staff) ){
                        $data = array(
                            'qdnv' => str_pad($stt,3,'0',STR_PAD_LEFT).'/'.$month_year.'/QĐNV'
                        );
                        $stt++;
                        $where = $QStaff->getAdapter()->quoteInto('id = ?',$item);
                        $QStaff->update($data,$where);
                    }
        
                }
            }
            $db->commit();
            return array('code'=>1,'message'=>'Done');
        }catch (Exception $e){
            $db->rollBack();
            return array('code'=>1,'message'=>$e->getMessage());
        }
    }else{
        echo json_encode(array('status'=>0,'message'=>'Vui lòng chọn đúng nhân viên nghỉ việc'));
    }
    
    
?>