<?php 
	$unit_code_id     = $this->getRequest()->getParam('unit_code_id');
        $export           = $this->getRequest()->getParam('export');
        $export_detail    = $this->getRequest()->getParam('export_detail');
        $export_type      = $this->getRequest()->getParam('export_type');
        $code             = $this->getRequest()->getParam('code');
        $month            = $this->getRequest()->getParam('month',date('m/Y'));

        $params = array(
            'month'         => $month,
            'export_type'   => $export_type,
            'unit_code_id'  => $unit_code_id,
            'code'          => $code
        );
         
        if( isset($params['month']) AND $params['month'] ){
            $date = My_Date::normal_to_mysql($params['month']);
        }else{
            $date    = date('Y') . '-' . date('m') . '-01';
        }

        $db             = Zend_Registry::get('db');

        $QListInsurance = new Application_Model_LockInsurance();
        $list_LockInssurance = $QListInsurance->getListLock();
        
        $show_lock = 1;
        if(!empty($list_LockInssurance)){

            foreach ($list_LockInssurance as $item){
                if($item['lock_time'] == $date){
                    $show_lock = 0;
                }
            }
        }
        if(!empty($export) || !empty($export_type)){
             //Trich nop
        $stmt = $db->prepare('CALL PR_Insurance_Payable (:r , :pUnitCodeId) ');
        $stmt->bindParam('r', $date, PDO::PARAM_STR);
        $stmt->bindParam('pUnitCodeId', intval($unit_code_id), PDO::PARAM_INT);
        $stmt->execute();
        $payable = $stmt->fetchAll();
        $stmt->closeCursor();
        }

        //tham gia nguoc
        $stmt_join_back = $db->prepare('CALL PR_Insurance_Join_Back (:r, :pUnitCodeId) ');
        $stmt_join_back->bindParam('r', $date, PDO::PARAM_STR);
        $stmt_join_back->bindParam('pUnitCodeId', intval($unit_code_id), PDO::PARAM_INT);
        $stmt_join_back->execute();
        $join_back = $stmt_join_back->fetchAll();
        $stmt_join_back->closeCursor();

        //dieu chinh nguoc
        $stmt_adjust_back = $db->prepare('CALL PR_Insurance_Adjust_Back (:r, :pUnitCodeId) ');
        $stmt_adjust_back->bindParam('r', $date, PDO::PARAM_STR);
        $stmt_adjust_back->bindParam('pUnitCodeId', intval($unit_code_id), PDO::PARAM_INT);
        $stmt_adjust_back->execute();
        $adjust_back = $stmt_adjust_back->fetchAll();
        $stmt_adjust_back->closeCursor();

        //giam
        $stmt_decreate_back = $db->prepare('CALL PR_Insurance_Decreate_Back (:r, :pUnitCodeId) ');
        $stmt_decreate_back->bindParam('r', $date, PDO::PARAM_STR);
        $stmt_decreate_back->bindParam('pUnitCodeId', intval($unit_code_id), PDO::PARAM_INT);
        $stmt_decreate_back->execute();
        $decreate = $stmt_decreate_back->fetchAll();
        $stmt_decreate_back->closeCursor();
        
        //return
        $stmt_decreate_return = $db->prepare('CALL PR_Insurance_Decreate_Return (:r, :pUnitCodeId) ');
        $stmt_decreate_return->bindParam('r', $date, PDO::PARAM_STR);
        $stmt_decreate_return->bindParam('pUnitCodeId', intval($unit_code_id), PDO::PARAM_INT);
        $stmt_decreate_return->execute();
        $recreate = $stmt_decreate_return->fetchAll();
        $stmt_decreate_return->closeCursor();
        
        $ins_rate = '32%';
        if(!empty($export_detail) AND !empty($export_detail) ){
            //detail
            $stmt_detail = $db->prepare('CALL PR_Insurance_Having (:r, :pUnitCodeId) ');
            $stmt_detail->bindParam('r', $date, PDO::PARAM_STR);
            $stmt_detail->bindParam('pUnitCodeId', intval($unit_code_id), PDO::PARAM_INT);
            $stmt_detail->execute();
            $resDetail = $stmt_detail->fetchAll();
            $stmt_detail->closeCursor();
            
                        set_time_limit(0);
            ini_set('memory_limit', '5120M');
            require_once 'PHPExcel.php';
            $alphaExcel = new My_AlphaExcel();

            $PHPExcel = new PHPExcel();
            $heads = array(
                $alphaExcel->ShowAndUp() => 'Mã đơn vị',
                $alphaExcel->ShowAndUp() => 'Mã NV',
                $alphaExcel->ShowAndUp() => 'Họ Tên',
                $alphaExcel->ShowAndUp() => 'Tỷ Lệ Đóng',
                $alphaExcel->ShowAndUp() => 'Mức Lương',
                $alphaExcel->ShowAndUp() => 'Từ Tháng',
                $alphaExcel->ShowAndUp() => 'Đến Tháng',
                $alphaExcel->ShowAndUp() => 'Loại ',
                $alphaExcel->ShowAndUp() => 'Số Tháng Trích Nộp',
                $alphaExcel->ShowAndUp() => 'Thành Tiền',

            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }

            $index = 1;
            foreach($resDetail as $key => $value)
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['unit_code_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $ins_rate,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['salary'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['fromMonth'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['toMonth'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['option_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['number_of_month'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['amount'],PHPExcel_Cell_DataType::TYPE_STRING);

                $index++;
            }
            $filename = 'Insurance_detail- ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit();
        }
      
        
        

        
        
        $data_export = array();
        if($export_type == 1){
            $data_export = $payable;
        }elseif ($export_type == 2){
            $data_export = $join_back;
        }elseif ($export_type == 3){
            $data_export = $adjust_back;
        }elseif ($export_type == 4){
             $ins_rate = '4.5%';
            $data_export = $decreate;
        }elseif ($export_type == 5){
            $data_export = $recreate;
        }
         
        if(!empty($export) AND !empty($export_type) ){
            set_time_limit(0);
            ini_set('memory_limit', '5120M');
            require_once 'PHPExcel.php';
            $alphaExcel = new My_AlphaExcel();

            $PHPExcel = new PHPExcel();
            $heads = array(
                $alphaExcel->ShowAndUp() => 'Mã đơn vị',
                $alphaExcel->ShowAndUp() => 'Mã NV',
                $alphaExcel->ShowAndUp() => 'Họ Tên',
                $alphaExcel->ShowAndUp() => 'Tỷ Lệ Đóng',
                $alphaExcel->ShowAndUp() => 'Mức Lương',
                $alphaExcel->ShowAndUp() => 'Mức Lương Cũ',
                $alphaExcel->ShowAndUp() => 'Từ Tháng',
                $alphaExcel->ShowAndUp() => 'Đến Tháng',
                $alphaExcel->ShowAndUp() => 'Số Tháng Trích Nộp',
                $alphaExcel->ShowAndUp() => 'Thành Tiền',

            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }

            $index = 1;
            foreach($data_export as $key => $value)
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['unit_code_name'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $ins_rate,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['salary'],PHPExcel_Cell_DataType::TYPE_STRING);
                $old_salary = ($export_type == 3) ? $value['old_salary'] : '';
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $old_salary ,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $date,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $date,PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['number_of_month'],PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['amount'],PHPExcel_Cell_DataType::TYPE_STRING);

                $index++;
            }
            $filename = 'Trich_nop - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit();
        }
         
        $this->view->payable = $payable;
        $this->view->join_back = $join_back;
        $this->view->adjust_back = $adjust_back;
        $this->view->decreate = $decreate;
        $this->view->recreate = $recreate;
        $this->view->params = $params;
        $this->view->lock_time = $list_LockInssurance;
        $this->view->time_lock = $date;
        $this->view->show_lock = $show_lock;
        
        $QUnitCode = new Application_Model_UnitCode();
        $this->view->unit_codes = $QUnitCode->get_all();

        $flashMessenger               = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;
