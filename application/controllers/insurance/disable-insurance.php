<?php 

$flashMessenger = $this->_helper->flashMessenger;
$QInsurance     = new Application_Model_Insurance();
$delete         = $this->getRequest()->getParam('delete');
$tang           = $this->getRequest()->getParam('tang');
$giam           = $this->getRequest()->getParam('giam');
$dieuchinh      = $this->getRequest()->getParam('dieuchinh');
$chedo          = $this->getRequest()->getParam('chedo');
$sauchedo       = $this->getRequest()->getParam('sauchedo');
$disable        = $this->getRequest()->getParam('disable');



$month          = $this->getRequest()->getParam('month',date('m/Y'));
$unit_code_id   = $this->getRequest()->getParam('unit_code_id');
$staff_ids      = $this->getRequest()->getParam('staff_ids');
$leave_ids      = $this->getRequest()->getParam('leave_ids');

// $leave_after_ids = $this->getRequest()->getParam('leave_after_ids');

$ids            = $this->getRequest()->getParam('ids');
$qdnv           = $this->getRequest()->getParam('qdnv');
$back_url       = $this->getRequest()->getParam('back_url','/insurance/report-list');
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$db             = Zend_Registry::get('db');
$QInsuranceInfo = new Application_Model_InsuranceInfo();
$QInsuranceStaff = new Application_Model_InsuranceStaff();
$QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

    if( isset($month) AND $month ){
        $date = My_Date::normal_to_mysql($month);
    }else{
        $flashMessenger->setNamespace('error')->addMessage("Please enter month to export!");
        // $this->_redirect($back_url);
    }
   
    $paramsStore = array(
        $date,
        $unit_code_id,
        NULL,
        '',
        ''
    );
    
    $date_ins = date('Y-m-d');
    $datetime = date('Y-m-d H:i:s');
    
    if(!empty($disable) AND !empty($tang) AND !empty($staff_ids)){
        
        $stmt_inCreateInfo = $db->prepare('CALL PR_Increate_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
        $stmt_inCreateInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
        $stmt_inCreateInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
        $stmt_inCreateInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
        
        
        $stmt_inCreateInfo->execute();
        $inCreateInfo = $stmt_inCreateInfo->fetchAll();
        $stmt_inCreateInfo->closeCursor();
        
        $db->beginTransaction();
        try{
            foreach($inCreateInfo as $item){
                $data       = array(
                    'staff_id'      => $item['staff_id'],
                    'type'          => $item['type'],
                    'option'        => $item['option'],
                    'time_effective'=> $item['time_effective'],
                    'time_alter'    => $item['time_alter'],
                    'note'          => 'Disable_new',
                    'created_at'    => $datetime,
                    'created_by'    => $userStorage->id,
                    'passby'        => 1,
                    'system_note'   => 'system auto generate'
                );
                $QInsuranceStaffBasic->insert($data);
                
            }

            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
            $this->_redirect($back_url);
        }catch (Exception $e){
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $db->rollBack();
            $this->_redirect($back_url);
        }
    }elseif(!empty($disable) AND !empty($giam) AND !empty($staff_ids)){
        
        $stmt_deCreateInfo = $db->prepare('CALL PR_Decreate_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
        $stmt_deCreateInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
        $stmt_deCreateInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
        $stmt_deCreateInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
        
        
        $stmt_deCreateInfo->execute();
        $deCreateInfo = $stmt_deCreateInfo->fetchAll();
        $stmt_deCreateInfo->closeCursor();
        
        foreach($deCreateInfo as $item_de){
            $data_de       = array(
                'staff_id'      => $item_de['staff_id'],
                'type'          => $item_de['type'],
                'option'        => $item_de['option'],
                'time_effective'=> $item_de['time_effective'],
                'time_alter'    => $item_de['time_alter'],
                'note'          => 'Disable_new',
                'created_at'    => $datetime,
                'created_by'    => $userStorage->id,
                'passby'        => 1,
                'system_note'   => 'system auto generate'
            );
            
                $QInsuranceStaffBasic->insert($data_de);
        }
    }elseif (!empty($disable) AND !empty($dieuchinh) AND !empty($staff_ids)){
        //Adjust
        $stmt_adjustInfo = $db->prepare('CALL PR_Adjust_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
        $stmt_adjustInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
        $stmt_adjustInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
        $stmt_adjustInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
        
        $stmt_adjustInfo->execute();
        $adjust_info = $stmt_adjustInfo->fetchAll();
        $stmt_adjustInfo->closeCursor();
        
            foreach($adjust_info as $item_adj){
            $data_adj       = array(
                'staff_id'      => $item_adj['staff_id'],
                'type'          => $item_adj['type'],
                'option'        => $item_adj['option'],
                'time_effective'=> $item_adj['time_effective'],
                'time_alter'    => $item_adj['time_alter'],
                'note'          => 'Disable_new',
                'created_at'    => $datetime,
                'created_by'    => $userStorage->id,
                'passby'        => 1,
                'system_note'   => 'system auto generate'
            );
           $QInsuranceStaffBasic->insert($data_adj);
           
        }
   }elseif (!empty($disable) AND !empty($chedo) AND !empty($staff_ids)){
       //Dieu Chinh
       $stmt_DieuchinhInfo = $db->prepare('CALL PR_Change_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pStaffId) ');
       $stmt_DieuchinhInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
       $stmt_DieuchinhInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
       $stmt_DieuchinhInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
       $stmt_DieuchinhInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
       $stmt_DieuchinhInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
       $stmt_DieuchinhInfo->bindParam('pStaffId', $staff_ids, PDO::PARAM_STR);
       
       $stmt_DieuchinhInfo->execute();
       $dieuchinh_info = $stmt_DieuchinhInfo->fetchAll();
       $stmt_DieuchinhInfo->closeCursor();
       
               foreach($dieuchinh_info as $chedo){
                   $data       = array(
                       'staff_id'      => $chedo['staff_id'],
                       'type'          => $chedo['type'],
                       'option'        => $chedo['option'],
                       'time_effective'=> $chedo['time_effective'],
                       'time_alter'    => $chedo['time_alter'],
                       'note'          => 'Disable_new',
                       'created_at'    => $datetime,
                       'created_by'    => $userStorage->id,
                       'passby'        => 1,
                       'system_note'   => 'system auto generate'
                   );
                
                $QInsuranceStaffBasic->insert($data);
        }
   }elseif (!empty($lock) AND !empty($sauchedo) AND !empty($leave_ids)){
       $stmt_SauChedoInfo = $db->prepare('CALL PR_After_Leave_Insurance_Info (:r, :pUnitCodeId, :pDateIns, :pName, :pCode, :pLeaveId) ');
       $stmt_SauChedoInfo->bindParam('r', $paramsStore[0], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pUnitCodeId', $paramsStore[1], PDO::PARAM_INT);
       $stmt_SauChedoInfo->bindParam('pDateIns', $paramsStore[2], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pName', $paramsStore[3], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pCode', $paramsStore[4], PDO::PARAM_STR);
       $stmt_SauChedoInfo->bindParam('pLeaveId', $leave_ids, PDO::PARAM_STR);
       
       $stmt_SauChedoInfo->execute();
       $sauchedo_info = $stmt_SauChedoInfo->fetchAll();
       $stmt_SauChedoInfo->closeCursor();
       
       foreach($sauchedo_info as $chedo_after){
           $data_chedo_after   = array(
               'staff_id'      => $chedo_after['staff_id'],
               'type'          => $chedo_after['type'],
               'option'        => $chedo_after['option'],
               'time_effective'=> $chedo_after['time_effective'],
               'time'          => $chedo_after['time_ins'],
               'time_ins'      => $date_ins,
               'time_alter'    => $chedo_after['time_alter'],
               'note'          => 'ON_note_new',
               'created_at'    => $datetime,
               'created_by'    => $userStorage->id,
               'company_id'    => $chedo_after['company_id'],
               'unit_code_id'  => $chedo_after['unit_code_id'],
               'rate'          => $chedo_after['INS_RATE'],
               'department'    => $chedo_after['department'],
               'team'          => $chedo_after['team'],
               'title'         => $chedo_after['title'],
               'del'           => 0,
               'locked'        => 1,
               'salary'        => $chedo_after['salary'],
               'old_salary'    => $chedo_after['old_salary'],
               'regional_market' => $chedo_after['regional_market'],
               'locked_at'     => $datetime,
               'locked_by'     => $userStorage->id,
               'time_created_at'  => $datetime,
               'money_out'     => $chedo_after['salary']
           );
       echo "<pre>";
       print_r($data_chedo_after);
       echo "</pre>";
       
        }
   }
// }else{
//     $flashMessenger->setNamespace('error')->addMessage('Please select item to process');
//     $this->_redirect($back_url);
// }
// $flashMessenger->setNamespace('error')->addMessage('Error');
// $this->_redirect($back_url);
