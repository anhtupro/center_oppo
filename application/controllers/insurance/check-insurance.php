<?php 
    $staff_id = $this->getRequest()->getParam('staff_id');
    $code     = $this->getRequest()->getParam('code');
    $QStaff   = new Application_Model_Staff();
    $QInsurance = new Application_Model_Insurance();
    $QTeam      = new Application_Model_Team();
    $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
    $team_info = $QTeam->find($userStorage->title);
    $team_info = $team_info->current();
    $group_id = $team_info['access_group'];
    
    if(!in_array($group_id, array(HR_ID, ADMINISTRATOR_ID, SALES_ADMIN_ID))){
        $flashMessenger = $this->_helper->flashMessenger;
        $flashMessenger->setNamespace('error')->addMessage('No permission');
        $backUrl = HOST;
        $this->_redirect($backUrl);
    }
        $db = Zend_Registry::get('db');
    
    if(!empty($code)){
        $where           = $QStaff->getAdapter()->quoteInto("code = ? " , $code );
        $result          = $QStaff->fetchRow($where);
        $staff_id        = $result->toArray()['id'];
        if(in_array($group_id, array(SALES_ADMIN_ID))){
        $QAsm = new Application_Model_Asm();
        $cachedASM = $QAsm->get_cache($userStorage->id);
        $list_province_ids = $cachedASM['province'];
       
        $where_Staff  = $db->select()
                        ->from(array('a'=>'staff'),array('a.id'))
                        ->where('a.regional_market IN (?)',$list_province_ids)
                        ->where('a.team in (75,119,294, 397)', '');
        $resultStaff  = $db->fetchAll($where_Staff);
        $listStaff = array();
        foreach ($resultStaff  as $val){
            $listStaff[]  = $val['id'];
        }
        
        if(!in_array($staff_id, $listStaff)){
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('Bạn không quản lý nhân viên này');
            $backUrl = HOST;
            $this->_redirect($backUrl . '/insurance/check-insurance');
        }
    }
    
        $qdnv            = $result->toArray()['qdnv'];
        $this->view->qdnv = $qdnv;
        $title           = $result->toArray()['title'];
        
    }
    $params = array(
        'code'     => $code,
    );
    $this->view->params = $params;
    

    $cols = array(
        'a.*',
        'title_name'      => 'CONCAT(b.name_vn," - ",h.name," - ",k.name)',
        'team_name'       => 'c.name',
        'department_name' => 'd.name',
        'regional_name'   => 'e.name',
        'area_name'       => 'f.name',
        'gender_name'     => 'IF(i.gender = 1,"NAM","NỮ")',
        'status_name'     => '(CASE WHEN a.status = 0 THEN "OFF" WHEN a.status = 1 THEN "ON" WHEN a.status = 2 THEN "TEMP OFF" ELSE "CHILDBEARING" END)',
        'id_number_ins'   => 'IFNULL(i.ID_number,a.ID_number)',
        'dob_ins'         => 'IFNULL(i.dob,a.dob)',
        'firstname_ins'   => 'IFNULL(i.firstname,a.firstname)',
        'lastname_ins'    => 'IFNULL(i.lastname,a.lastname)',
    );
    if(!empty($staff_id)){
        $select = $db->select()
        ->from(array('a'=>'staff'),$cols)
        ->joinLeft(array('i'=>'insurance_info'),'i.staff_id = a.id',array())
        ->join(array('b'=>'team'),'a.title = b.id',array())
        ->join(array('c'=>'team'),'b.parent_id = c.id',array())
        ->join(array('d'=>'team'),'c.parent_id = d.id',array())
        ->join(array('e'=>'regional_market'),'e.id = a.regional_market',array())
        ->join(array('f'=>'area'),'f.id = e.area_id',array())
        ->joinLeft(array('h'=>'province'),'e.province_id = h.id',array())
        ->joinLeft(array('k'=>'area_nationality'),'k.id = h.area_national_id',array())
        ->where('a.id = ?',$staff_id)
        ->group('a.id')
        ;
    $staff = $db->fetchRow($select);
    $this->view->staff = $staff;
    }
    
    
    $QUnitCode = new Application_Model_UnitCode();
    $unit_codes = $QUnitCode->get_cache();
    $this->view->unit_codes = $unit_codes;

    $increase_text = '';
    if(isset($increase['time_alter']) AND $increase['time_alter']){
        $date_increase = date_create($increase['time_alter']);
        if( intval( date('d',strtotime($increase['time_alter']) ) ) > 15 ){
            $date_increase->add(new DateInterval('P20D'));
        }
        $increase_text = $date_increase->format('m/Y');
    }

   
    $QProvince = new Application_Model_Province();
    $provinces = $QProvince->get_all2();
    $this->view->provinces = $provinces;

    $QHospital = new Application_Model_Hospital();
    $provinceHospital = 0;
    if(isset($staff['hospital_id']) AND $staff['hospital_id']){
        $rowHospital = $QHospital->find($staff['hospital_id'])->current();
        $provinceHospital = $rowHospital['province_id'];
    }
    $currentHospitals = $QHospital->get_all(array('province_id'=> $provinceHospital));
    $this->view->currentHospitals = $currentHospitals;
    $this->view->provinceHospital = $provinceHospital;

    $flashMessenger = $this->_helper->flashMessenger;
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
    $this->view->messages_success = $messages_success;