<?php 
// config for template
define('START_ROW', 2);
define('company', 1);
define('code', 2);
define('name', 3);
define('title', 4);
define('department', 5);
define('team', 6);
define('dob', 7);
define('gender', 8);
define('insurance_number', 9);
define('have_book', 10);
define('close_book', 11);
define('return_ins_book_time', 12);
define('qdnv', 13);
define('code_BHYT_ins', 14);
define('province_code', 15);
define('hospital_code', 16);
define('have_BHYT_time', 17);
define('note_ins', 18);
$datetime = date('Y-m-d H:i:s');
$userStorage   = Zend_Auth::getInstance()->getStorage()->read();
$QInsuranceLogInfo    = new Application_Model_InsuranceLogInfo();
$this->_helper->layout->disableLayout();

$QStaff         = new Application_Model_Staff();
$QHospital      = new Application_Model_Hospital();
$hospital_cache = $QHospital->get_cache_by_code(); 
if ($this->getRequest()->getMethod() == 'POST') { // Big IF
  
    set_time_limit(0);
    ini_set('memory_limit', -1);
    // file_put_contents(APPLICATION_PATH.'/../public/files/mou/lock', '1');
    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
    
    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }
    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {    

        $this->view->errors = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet           = $objPHPExcel->getSheet(0);
    $highestRow      = $sheet->getHighestRow();
    $highestColumn   = $sheet->getHighestColumn();
  
    $error_list      = array();
    $success_list    = array();
    $store_code_list = array();
    $number_of_order = 0;
    $total_value     = 0;
    $total_order_row = 0;
    $order_list      = array();

    $arr_inserts   = array();
    $arr_order_ids = array();

    $_failed         = false;
    $total_row       = array();
    $data_staff_code = [];
    $dataMassUpload  = array();
    $data_staff_code = array();
    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            
            if(!empty($rowData[code])){
                $data_staff_code[] = trim($rowData[code]);      
                
                $data = array(
                    'company'               => trim($rowData[company]),
                    'code'                  => trim($rowData[code]),
                    'name'                  => trim($rowData[name]),
                    'title'                 => trim($rowData[title]),
                    'department'            => trim($rowData[department]),
                    'team'                  => trim($rowData[team]),
                    'dob'                   => trim($rowData[dob]),
                    'gender'                => trim($rowData[gender]),
                    'insurance_number'      => trim($rowData[insurance_number]),
                    'have_book'             => trim($rowData[have_book]),
                    'close_book'            => trim($rowData[close_book]),
                    'return_ins_book_time'  => trim($rowData[return_ins_book_time]),
                    'qdnv'                  => trim($rowData[qdnv]),
                    'code_BHYT_ins'         => trim($rowData[code_BHYT_ins]),
                    'province_code'              => trim($rowData[province_code]),
                    'hospital_code'         => trim($rowData[hospital_code]),
                    'have_BHYT_time'        => trim($rowData[have_BHYT_time]),
                    'note_ins'              => trim($rowData[note_ins]),
                    'created_at'            => $datetime,
                    'created_by'            => $userStorage->id
                );
                $dataMassUpload[] = $data;
            }
         
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }
        // nothing here
    } // END loop through order rows
    
    $progress->flush(30);
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    
    try {
        if (!empty($dataMassUpload)) {
                    $select = $db->select()
                            ->from(array('p' => 'staff'), array('p.code'))
                            ->where('p.code in (?)', $data_staff_code);
                    $result_code = $db->fetchCol($select);

                    if (count($data_staff_code) <> count($result_code)) {
                        $diff = array_diff($data_staff_code, $result_code);
                        throw new Exception("Nhân viên không tồn tại hoặc đã nghỉ việc hoặc trong file bị trùng code: " . implode($diff, ','));
                    }
//                    My_Controller_Action::insertAllrow($dataMassUpload, 'mass_upload_insurance');
                    foreach ($dataMassUpload as $k => $upload){
                        $data_staff = array();         
                        if(!empty($upload['insurance_number'])){
                            $data_staff['insurance_number'] = $upload['insurance_number'];
                        }
                        if(!empty($upload['have_book'])){
                            $time_obj = date_create_from_format('d/m/Y', $upload['have_book']);
                            $data_staff['have_book'] = $time_obj->format('Y-m-d');
                        }
                        if(!empty($upload['close_book'])){
                            $time_obj = date_create_from_format('d/m/Y', $upload['close_book']);
                            $data_staff['close_book'] = $time_obj->format('Y-m-d');
                        }
                        if(!empty($upload['return_ins_book_time'])){
                            $time_obj = date_create_from_format('d/m/Y', $upload['return_ins_book_time']);
                            $data_staff['return_ins_book_time'] = $time_obj->format('Y-m-d');
                        }
                        if(!empty($upload['qdnv'])){
                            $data_staff['qdnv'] = $upload['qdnv'];
                        }
                        if(!empty($upload['code_BHYT_ins'])){
                            $data_staff['code_BHYT_ins'] = $upload['code_BHYT_ins'];
                        }
                        if(!empty($upload['hospital_code']) && !empty($upload['province_code'])){

                            $hospital_id = $hospital_cache[$upload['province_code']][$upload['hospital_code']]['id'];
                            if($hospital_id){
                                $data_staff['hospital_id']   = $hospital_id;
                            }
                            
                        }
                        if(!empty($upload['have_BHYT_time'])){
                            $time_obj = date_create_from_format('d/m/Y', $upload['have_BHYT_time']);
                            $data_staff['have_BHYT_time'] = $time_obj->format('Y-m-d');
                        }
                        if(!empty($upload['note_ins'])){
                            $data_staff['note_ins'] = $upload['note_ins'];
                        }
                        if(!empty($data_staff)){
                            echo $upload['code'];
                            $where_Row = $db->select()
                                            ->from(array('a'=>'staff'),array('a.id'))
                                            ->where('a.code = ?',$upload['code']);
                            $Staff_Row = $db->fetchRow($where_Row);
                            $staff_id  = $Staff_Row['id'];
                            $beforeStaff = $QStaff->find($staff_id)->current();
                            $where_update     = $QStaff->getAdapter()->quoteInto('code = ?', $upload['code']);
                            $r1 = $QStaff->update($data_staff, $where_update);
                            $afterStaff = $QStaff->find($staff_id)->current();
                            $before = $beforeStaff->toArray();
                            $after = $afterStaff->toArray();
                            $QInsuranceLogInfo->save($before,$after,$staff_id,$userStorage->id);
                        }
                    }
               //    My_Controller_Action::insertAllrow($dataMassUpload, 'mass_upload_insurance');
//            exit();
        } else {
            throw new Exception("Danh sách nhân viên không hợp lệ");
        }
        $db->commit();
        $progress->flush(99);
        
        $progress->flush(100);
//      
    } catch (Exception $e) {
        $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}
