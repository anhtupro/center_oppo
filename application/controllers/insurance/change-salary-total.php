<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$total              = $this->getRequest()->getParam('total');
$salary             = $this->getRequest()->getParam('salary');
$insurance_id       = $this->getRequest()->getParam('insurance_id');

$QInsuranceStaff    = new Application_Model_InsuranceStaff();
$userStorage 	    = Zend_Auth::getInstance ()->getStorage ()->read ();

if(!empty($salary) AND ($total == 1) AND !empty($insurance_id) ){
    $where             = array();
    $where[]           = $QInsuranceStaff->getAdapter()->quoteInto('id = ?' , $insurance_id);
    
    
    $insurance_detail = $QInsuranceStaff->find($insurance_id)->current();
    $salary_old       = $insurance_detail->toArray()['salary'];
    
        $datetime = date ( 'Y-m-d H:i:s' );
        $data = array(
            'salary'  => $salary,
            'note'    => "Update to_date_old: $salary_old created_at: $datetime"
        );
        
        $QInsuranceStaff->update($data, $where);
        
}else{
   return array('code'=>1,'message'=>'Done');
}
