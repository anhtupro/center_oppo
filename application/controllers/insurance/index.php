<?php 
	$unit_code_id         = $this->getRequest()->getParam('unit_code_id');
        $code                 = $this->getRequest()->getParam('code');
        $name                 = $this->getRequest()->getParam('name');
        $email                = $this->getRequest()->getParam('email');
        $area                 = $this->getRequest()->getParam('area');
        $off                  = $this->getRequest()->getParam('off');
        $region               = $this->getRequest()->getParam('region');
        $status               = $this->getRequest()->getParam('status',1);
        $insurance_number     = $this->getRequest()->getParam('insurance_number');
        $have_book            = $this->getRequest()->getParam('have_book', -1);
        $close_book           = $this->getRequest()->getParam('close_book', -1);
        $return_book_ins_time = $this->getRequest()->getParam('return_book_ins_time', -1);
        $return_card_ins_time = $this->getRequest()->getParam('return_card_ins_time', -1);
        $take_card_from_ins   = $this->getRequest()->getParam('take_card_from_ins', -1);
        $take_card_from_staff = $this->getRequest()->getParam('take_card_from_staff', -1);
        $delivery_card_ins    = $this->getRequest()->getParam('delivery_card_ins', -1);
        $regional_market      = $this->getRequest()->getParam('regional_market');
        $tags                 = $this->getRequest()->getParam('tags');
        $export               = $this->getRequest()->getParam('export', 0);
        $page                 = $this->getRequest()->getParam('page', 1);
        $sort                 = $this->getRequest()->getParam('sort', 'id');
        $desc                 = $this->getRequest()->getParam('desc', 0);
        $c45                  = $this->getRequest()->getParam('c45',1);
        $report_c45           = $this->getRequest()->getParam('report_c45');
        $limit                = 20;
        $total                = 0;
        $params = array(
            'sort'                 => $sort,
            'desc'                 => $desc,
            'unit_code_id'         => $unit_code_id,
            'code'                 => $code,
            'name'                 => $name,
            'off'                  => $off,
            'email'                => $email,
            'area'                 => $area,
            'region'               => $region,
            'status'               => $status,
            'insurance_number'     => $insurance_number,
            'have_book'            => $have_book,
            'close_book'           => $close_book,
            'return_book_ins_time' => $return_book_ins_time,
            'return_card_ins_time' => $return_card_ins_time,
            'take_card_from_ins'   => $take_card_from_ins,
            'delivery_card_ins'    => $delivery_card_ins,
            'take_card_from_staff' => $take_card_from_staff,
            'export'               => $export,
            'regional_market'      => $regional_market,
            'c45'                  => $c45
        );


        $QRegionalMarket = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache();
        $this->view->regional_markets = $regional_markets;

        $QInsurance = new Application_Model_Insurance();
        $status = $QInsurance->fetchPagination($page, $limit, $total, $params);

        if($export){
            //$this->reportTong();
            $this->_export($status);
        }

        if($report_c45){
            $QInsurance = new Application_Model_Insurance();
            $result = $QInsurance->c45($params);
            $this->_exportc45($result);
        }

        $this->view->staffs = $status;
        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST.'insurance/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);

        $QArea = new Application_Model_Area();
        $this->view->area_cache = $QArea->get_cache();

        $QRegion = new Application_Model_RegionalMarket();
        $this->view->all_region_cache = $QRegion->get_cache();

        $QTeam              = new Application_Model_Team();
        $this->view->teams  = $QTeam->get_cache();
        $this->view->titles = $QTeam->get_all('vn');

        $QUnitCode = new Application_Model_UnitCode();
        $this->view->unit_codes = $QUnitCode->get_all();

        $flashMessenger               = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;

?>