<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$total              = $this->getRequest()->getParam('total');
$time_alter         = $this->getRequest()->getParam('time_alter');
$insurance_id       = $this->getRequest()->getParam('insurance_id');

$QInsuranceStaff    = new Application_Model_InsuranceStaff();
$userStorage 	    = Zend_Auth::getInstance ()->getStorage ()->read ();

if(!empty($time_alter) AND ($total == 1) AND !empty($insurance_id) ){
    $where             = array();
    $where[]           = $QInsuranceStaff->getAdapter()->quoteInto('id = ?' , $insurance_id);
    
    
    $insurance_detail  = $QInsuranceStaff->find($insurance_id)->current();
    $time_alter_old    = $insurance_detail->toArray()['time_alter'];
    $updated_by        =  $userStorage->id;  
        $datetime = date ( 'Y-m-d H:i:s' );
        $data = array(
            'time_alter'  => DateTime::createFromFormat('d/m/Y', $time_alter)->format('Y-m-d'),
            'note'        => "Update time_alter: $time_alter_old created_at: $datetime By: $updated_by "
        );
        
        $QInsuranceStaff->update($data, $where);
        
}else{
   return array('code'=>1,'message'=>'Done');
}
