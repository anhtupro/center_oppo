<?php 
    $QInsurance  = new Application_Model_Insurance();
    $db          = Zend_Registry::get('db');
    $companies   = array(1, 2);
    $date        = My_Date::normal_to_mysql($month);
    $from_create = new DateTime($date);
    $from_create = $from_create->sub(new DateInterval('P1M'));
    $from        = $from_create->format('Y-m-16');

    $to_create  = new DateTime($date);
    $to         = $to_create->format('Y-m-15');
    $month_year = $to_create->format('my');
    $db->beginTransaction();
    try{
        foreach($companies as $company_id){
            $cols = array(
                'a.qdnv',
                'stt' => 'CASE WHEN a.qdnv IS NOT NULL AND a.qdnv != ""  THEN  SUBSTRING_INDEX(a.qdnv,"/",1)  ELSE 0 END',
            );

            $select = $db->select()
                ->from(array('a'=>'pre_status'),$cols)
                ->where('a.`option` = ?',3)
                ->where('time_alter >= ?',$from)
                ->where('time_alter <= ?',$to)
                ->where('qdnv IS NOT NULL AND qdnv != ""')
                ->where('company_id = ?',$company_id)
                ->where('locked = ?',1)
                ->where('del IS NULL OR del = ?',0)
                ->order('CASE WHEN a.qdnv IS NOT NULL AND a.qdnv != "" THEN  SUBSTRING_INDEX(a.qdnv,"/",1) ELSE 0 END DESC')
                ->limit(1)
            ;
            $row = $db->fetchRow($select);
            $stt = 1;
            if($row){
                $stt = intval($row['stt']) + 1;
            }

            $select_off = $db->select()
                ->from(array('a'=>'pre_status'),array('a.*'))
                ->where('a.`option` = ?',3)
                ->where('time_alter >= ?',$from)
                ->where('time_alter <= ?',$to)
                ->where('company_id = ?',$company_id)
                ->where('qdnv IS NULL OR qdnv = ""')
                //->where('locked = ?',1)
                ->order('a.time_effective ASC')
            ;
            // echo $select_off;
            // exit;
            $result = $db->fetchAll($select_off);

            foreach($result as $item){
                $data = array(
                    'qdnv' => str_pad($stt,3,'0',STR_PAD_LEFT).'/'.$month_year.'/QĐNV'
                );
                $stt++;
                $where = $QInsurance->getAdapter()->quoteInto('id = ?',$item['id']);
                $QInsurance->update($data,$where);

            }
        }
        $db->commit();
        return array('code'=>1,'message'=>'Done');
    }catch (Exception $e){
        $db->rollBack();
        return array('code'=>1,'message'=>$e->getMessage());
    }
?>