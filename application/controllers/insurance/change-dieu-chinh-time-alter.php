<?php 
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$staff_id           = $this->getRequest()->getParam('staff_id');
$dieuchinh               = $this->getRequest()->getParam('dieuchinh');
$time_effective     = $this->getRequest()->getParam('time_effective');
$time_alter         = $this->getRequest()->getParam('time_alter');
$option_type        = $this->getRequest()->getParam('option_type');


$QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
$userStorage 	= Zend_Auth::getInstance ()->getStorage ()->read ();
$type = 1;
if(!empty($staff_id) AND ($dieuchinh == 1) AND !empty($time_effective) AND !empty($time_alter)){
    $where             = array();
    $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('staff_id = ?' , $staff_id );
    $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('time_effective = ?' , $time_effective);
    $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('`type` = ?', $type);
    if($option_type == 7){
        $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('`option` = ?',$option_type);
    }else{
        $where[]           = $QInsuranceStaffBasic->getAdapter()->quoteInto('`option` IN (5,6)');
    }
    
    $result            = $QInsuranceStaffBasic->fetchRow($where);
    $data = array(
        'staff_id'          => $staff_id,
        'type'              => $type,
        'option'            => $option_type,
        'time_effective'    => $time_effective,
        'time_alter'        => DateTime::createFromFormat('d/m/Y', $time_alter)->format('Y-m-d'),
        'created_at'        => date ( 'Y-m-d H:i:s' ),
        'created_by'        => $userStorage->id
    );
    
    if(empty($result)){
         $QInsuranceStaffBasic->insert($data);
     }else{
         $QInsuranceStaffBasic->update($data, $where);
     }
}else{
   return array('code'=>1,'message'=>'Done');
}
