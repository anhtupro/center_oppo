<?php 

$staff_id        = $this->getRequest()->getParam('staff_id');
$type            = $this->getRequest()->getParam('type');
$option          = $this->getRequest()->getParam('option');
$time_effective  = $this->getRequest()->getParam('time_effective');
$time_alter      = $this->getRequest()->getParam('time_alter');
$note            = $this->getRequest()->getParam('note');
$unit_code_id    = $this->getRequest()->getParam('unit_code_id');
$department      = $this->getRequest()->getParam('department');
$team            = $this->getRequest()->getParam('team');
$title           = $this->getRequest()->getParam('title');
$salary          = $this->getRequest()->getParam('salary');
$old_salary          = $this->getRequest()->getParam('old_salary');
$del             = 0;
$locked = 1;
$rate = 32;
$company_id = ($unit_code_id == 2 ) ? 2 : 1;
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$date_ins = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');

$data = array(
    'staff_id'        => intval($staff_id),
    'type'            => intval($type),
    'option'          => intval($option),
    'time_effective'  => My_Date::normal_to_mysql($time_effective),
    'time_alter'      => My_Date::normal_to_mysql($time_alter),
    'time_ins'        => $date_ins,
    'note'            => trim($note),
    'created_at'      => $datetime,
    'created_by'      => $userStorage->id,
    'department'      => intval($department),
    'team'            => intval($team),
    'title'           => intval($title),
    'company_id'      => $company_id,
    'unit_code_id'    => intval($unit_code_id),
    'salary'          => intval($salary),
    'old_salary'      => intval($old_salary),
    'time_created_at' => My_Date::normal_to_mysql($time_alter),
    'rate'            => $rate,
    'locked'          => $locked,
    'del'             => 0,
    'locked_at'       => $datetime,
    'locked_by'       => $userStorage->id,
    'time_created_at' => $datetime,
    'system_note'     => 'add Data'
);
$QInsuranceStaff    = new Application_Model_InsuranceStaff();

$db = Zend_Registry::get('db');
$flashMessenger = $this->_helper->flashMessenger;
$db->beginTransaction();
try{
    //locked data khi thêm mới
    $QInsuranceStaff->insert($data);
    $db->commit();
    $flashMessenger->setNamespace('success')->addMessage('Done');
}catch(Exception $e){
    $db->rollBack();
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
}
$this->_redirect('/insurance/report-list');
