<?php 
	$unit_code_id         = $this->getRequest()->getParam('unit_code_id');
        $code                 = $this->getRequest()->getParam('code');
        $name                 = $this->getRequest()->getParam('name');
        $email                = $this->getRequest()->getParam('email');
        $area                 = $this->getRequest()->getParam('area');
        $off                  = $this->getRequest()->getParam('off');
        $region               = $this->getRequest()->getParam('region');
        $status               = $this->getRequest()->getParam('status',1);
        $insurance_number     = $this->getRequest()->getParam('insurance_number');
        $have_book            = $this->getRequest()->getParam('have_book', -1);
        $close_book           = $this->getRequest()->getParam('close_book', -1);
        $return_book_ins_time = $this->getRequest()->getParam('return_book_ins_time', -1);
        $return_card_ins_time = $this->getRequest()->getParam('return_card_ins_time', -1);
        $take_card_from_ins   = $this->getRequest()->getParam('take_card_from_ins', -1);
        $take_card_from_staff = $this->getRequest()->getParam('take_card_from_staff', -1);
        $delivery_card_ins    = $this->getRequest()->getParam('delivery_card_ins', -1);
        $regional_market      = $this->getRequest()->getParam('regional_market');
        $tags                 = $this->getRequest()->getParam('tags');
        $export               = $this->getRequest()->getParam('export', 0);
        $export_salary               = $this->getRequest()->getParam('export_salary', 0);
        
        $page                 = $this->getRequest()->getParam('page', 1);
        $sort                 = $this->getRequest()->getParam('sort', 'id');
        $desc                 = $this->getRequest()->getParam('desc', 0);
        $report_c45           = $this->getRequest()->getParam('report_c45');
        $limit                = 20;
        $total                = 0;
        $params = array(
            'sort'                 => $sort,
            'desc'                 => $desc,
            'unit_code_id'         => $unit_code_id,
            'code'                 => $code,
            'name'                 => $name,
            'off'                  => $off,
            'email'                => $email,
            'area'                 => $area,
            'region'               => $region,
            'status'               => $status,
            'insurance_number'     => $insurance_number,
            'have_book'            => $have_book,
            'close_book'           => $close_book,
            'return_book_ins_time' => $return_book_ins_time,
            'return_card_ins_time' => $return_card_ins_time,
            'take_card_from_ins'   => $take_card_from_ins,
            'delivery_card_ins'    => $delivery_card_ins,
            'take_card_from_staff' => $take_card_from_staff,
            'export'               => $export,
            'regional_market'      => $regional_market,
            'c45'                  => 1
        );


        $QRegionalMarket = new Application_Model_RegionalMarket();
        $regional_markets = $QRegionalMarket->get_cache();
        $this->view->regional_markets = $regional_markets;

        $QInsuranceStaff = new Application_Model_InsuranceStaff();
        $status = $QInsuranceStaff->fetchPagination($page, $limit, $total, $params);
        $this->view->staffs = $status;
        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST.'insurance/insurance-list/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);

        $QArea = new Application_Model_Area();
        $this->view->area_cache = $QArea->get_cache();

        $QRegion = new Application_Model_RegionalMarket();
        $this->view->all_region_cache = $QRegion->get_cache();

        $QTeam              = new Application_Model_Team();
        $this->view->teams  = $QTeam->get_cache();
        $this->view->titles = $QTeam->get_all('vn');

        $QUnitCode = new Application_Model_UnitCode();
        $this->view->unit_codes = $QUnitCode->get_all();

        $flashMessenger               = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;

        
        if($export==1){
		// no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
		
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Công Ty',
            'Mã nhân viên',
            'Họ và tên',
            'Chức vụ',
            'Bộ phận',
            'Team',
            'Ngày sinh',
            'Giới tính (Nữ x)',
            'Số sổ BXHH',
            'Thời gian cấp sổ/Nhận sổ',
            'Thời gian chốt sổ',
            'Thời gian trả sổ',
            'Số Quyết định nghỉ việc',
            'Mã thẻ BHYT',
            'Tỉnh đăng ký KCB',
            'Mã tỉnh',
            'Bệnh viện đăng ký KCB',
            'Mã bệnh viện',
            'Thời gian phát thẻ BHYT',
            'Ghi chú'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $intCount = 1;       

      
        try
        {
            if ($status)
                foreach ($status as $_key => $_order)
                {

                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['unit_code_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['staff_code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);   
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['staff_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['title_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);   
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['department_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['team_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['dob'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $gender = ($_order['gender'] == 0) ? 'X' : '';
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($gender,
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['insurance_number'],
                        PHPExcel_Cell_DataType::TYPE_STRING);       
                     if($_order['off_date'] AND $_order['off_date'] != '0000-00-00'){
                        $off_date = date('d/m/Y',strtotime($_order['off_date']));
                    }
                    if($_order['from_date'] AND $_order['from_date'] != '0000-00-00'){
                        $from_date = date('d/m/Y',strtotime($_order['from_date']));
                    }
    
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($_order['have_book'])  ? date('d/m/Y',strtotime($_order['have_book']))  : '',
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($_order['close_book']) ? date('d/m/Y',strtotime($_order['close_book']))  : '' ,
                        PHPExcel_Cell_DataType::TYPE_STRING);    
                    $sheet->getCell($alpha++ . $index)->setValueExplicit( !empty($_order['return_ins_book_time']) ?  date('d/m/Y',strtotime($_order['return_ins_book_time'])) : '',
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['qdnv'],
                        PHPExcel_Cell_DataType::TYPE_STRING);  
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code_BHYT_ins'],
                        PHPExcel_Cell_DataType::TYPE_STRING);  
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['province_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);  
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['province_code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);  
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['hospital_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);  
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['hospital_code'],
                        PHPExcel_Cell_DataType::TYPE_STRING); 
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(date('d/m/Y',strtotime($_order['have_BHYT_time'])),
                        PHPExcel_Cell_DataType::TYPE_STRING);  
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['note'],
                        PHPExcel_Cell_DataType::TYPE_STRING);                      
                    
                    $index++;
                    
                }
        }
        catch (exception $e)
        {
            exit;
        }

        $filename = 'Insurance_list_' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
     
    if($export_salary==1){
        $db = Zend_Registry::get('db');
        
        $sql = "SELECT uc.unit_code,st.`code`,st.ID_number, CONCAT(st.firstname, ' ', st.lastname) AS 'staff_name',
                dp.name dep, te.name team, ti.name title, st.off_date, v.`insurance_salary` , v.`from_date`
                FROM `v_salary_now` v
                INNER JOIN staff st ON v.`staff_id` = st.`id`
                LEFT JOIN team ti ON st.title = ti.id
                LEFT JOIN team dp ON st.department = dp.id
                LEFT JOIN team te ON te.id = st.team
                LEFT JOIN unit_code uc ON uc.id = st.company_id
	";
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        
        $data = $stmt->fetchAll();
      
        // no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
    
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'CÔNG TY',
            'Mã NV',
            'CMND',
            'HỌ TÊN',
            'PHÒNG BAN',
            'BỘ PHẬN',
            'CHỨC VỤ',
            'OFF DATE',
            'LƯƠNG BH',
            'NGÀY HIỆU LỰC',
        );
    
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
    
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        $intCount = 1;
        try
        {
            if ($status)
                foreach ($data as $_key => $_order)
                {
                    $off_date = $from_date = $status_staff = "";
    
                    if($_order['off_date'] AND $_order['off_date'] != '0000-00-00'){
                        $off_date = date('d/m/Y',strtotime($_order['off_date']));
                    }
                    if($_order['from_date'] AND $_order['from_date'] != '0000-00-00'){
                        $from_date = date('d/m/Y',strtotime($_order['from_date']));
                    }
    
    
                    $alpha = 'A';
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['unit_code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['ID_number'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['staff_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['dep'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['team'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['title'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($off_date,
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['insurance_salary'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($from_date,
                        PHPExcel_Cell_DataType::TYPE_STRING);
                  
                    $index++;
    
                }
        }
        catch (exception $e)
        {
            exit;
        }
    
        $filename = 'Insurance_salary_' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
