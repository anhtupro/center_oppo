<?php 
        $db = Zend_Registry::get('db');
        $cols = array('id','staff_name' => 'CONCAT(s.firstname," ",s.lastname," - ",s.code," - ",t.name)');
        $select = $db->select()
            ->from(array('s' => 'staff'), $cols)
            ->join(array('t' =>'team'), 't.id = s.team', array())
            ->where("s.off_date IS NULL OR s.off_date > '2018-01-01' ")    
            ->order('s.firstname');
        $staffs = $db->fetchAll($select);
        $this->view->staffs = $staffs;

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $regionals = $QRegionalMarket->get_cache();
        $this->view->regionals = $regionals;

        $QUnitCode = new Application_Model_UnitCode();
        $unit_codes = $QUnitCode->get_cache();
        $this->view->unit_codes = $unit_codes;

        $QTeam = new Application_Model_Team();
        $this->view->departments = $QTeam->get_department();

        $teams = $QTeam->get_cache_team();
        $this->view->teams = $teams;

        $titles = $QTeam->get_cache_title();
        $this->view->titles = $titles;

        $QInsuranceOption      = new Application_Model_InsuranceOption();
        $options               = $QInsuranceOption->get_cache();
        $this->view->options   = $options;
        

        $flashMessenger = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;