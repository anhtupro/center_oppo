<?php 

set_time_limit(0);
ini_set('memory_limit', '5120M');

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();

require_once 'PHPExcel.php';

$alphaExcel = new My_AlphaExcel();

$PHPExcel = new PHPExcel();
$heads = array(
    $alphaExcel->ShowAndUp() => 'Mã đơn vị',
    $alphaExcel->ShowAndUp() => 'Mã NV',
    $alphaExcel->ShowAndUp() => 'Tên',
    $alphaExcel->ShowAndUp() => 'Lương cũ',
    $alphaExcel->ShowAndUp() => 'Chức danh cũ',
    $alphaExcel->ShowAndUp() => 'Lương mới',
    $alphaExcel->ShowAndUp() => 'Chức danh mới',
    $alphaExcel->ShowAndUp() => 'Loại',
    
);

$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

foreach($heads as $key => $value)
{
    $sheet->setCellValue($key.'1', $value);
}

$index = 1;
foreach($adjust as $key => $value)
{
    $alphaExcel = new My_AlphaExcel();
     
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['unit_code_name']);
    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+1), $value['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['staff_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['old_salary'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['old_job_title'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['salary'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['job_title'],PHPExcel_Cell_DataType::TYPE_STRING);
//     $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), !empty($value['time_effective'])?date('d/m/Y', strtotime($value['time_effective'])):'');
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), 'DC',PHPExcel_Cell_DataType::TYPE_STRING);
    
    $index++;

}
$filename = 'Điều chỉnh - ' . date('Y-m-d H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit();




