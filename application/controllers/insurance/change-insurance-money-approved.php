<?php 
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$money_approved  = $this->getRequest()->getParam('money_approved',NULL);
	$leave_id = $this->getRequest()->getParam('leave_id',NULL);
	$QLeaveDetail = new Application_Model_LeaveDetail();
	
	if(!empty($money_approved) AND !empty($leave_id)){
	    $where = $QLeaveDetail->getAdapter()->quoteInto('id = ?', $leave_id);
	    $data = array(
	        'money_approved' => $money_approved
	    );
	    $QLeaveDetail->update($data, $where);
	}