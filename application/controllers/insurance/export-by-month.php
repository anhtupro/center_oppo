<?php 
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        
        $lock_time             = $this->getRequest()->getParam('lock_time');
        //Trich nop
        $db             = Zend_Registry::get('db');
    
        $sql1 = "SELECT st.id as 'staff_id',
                     uc.unit_code AS 'unit_code_name',
        			 st.`code`,
        			 CONCAT(st.firstname, ' ', st.lastname) AS 'fullname',
        			 li.*,
        			 CASE 
        					WHEN li.`lock_type` = 1 THEN 'Trích nộp hàng tháng'
        					WHEN li.`lock_type` = 2 THEN 'Trích nộp do tham gia ngược từ quá khứ'
        					WHEN li.`lock_type` = 3 THEN 'Trích nộp điều chỉnh lương ngược từ quá khứ'
        					WHEN li.`lock_type` = 4 THEN 'Truy thu BHYT do nhân viên nghỉ việc'
                                                WHEN li.`lock_type` = 5 THEN 'Hoàn trả tiền BH do báo giảm ở quá khứ'
                                                
        					END AS 'lock_name',
        			 CASE 
        					WHEN li.`lock_type` = 4 THEN '4.5%'
        					WHEN li.`lock_type` <> 4 THEN '32%'
        					END AS 'INS_RATE'
                FROM lock_insurance li 
                LEFT JOIN staff st ON li.staff_id = st.id
                LEFT JOIN unit_code uc ON uc.id = li.unit_code_id
                        WHERE lock_time = '$lock_time' AND li.`unit_code_id` = 1 ";
                        
        $stmt1 = $db->prepare($sql1);
        $stmt1->execute();
        $data1 = $stmt1->fetchAll();
        $stmt1->closeCursor();
        $stmt1 = null;
        
        $sql2 = "SELECT st.id as 'staff_id',
                     uc.unit_code AS 'unit_code_name',
        			 st.`code`,
        			 CONCAT(st.firstname, ' ', st.lastname) AS 'fullname',
        			 li.*,
        			 CASE 
        					WHEN li.`lock_type` = 1 THEN 'Trích nộp hàng tháng'
        					WHEN li.`lock_type` = 2 THEN 'Trích nộp do tham gia ngược từ quá khứ'
        					WHEN li.`lock_type` = 3 THEN 'Trích nộp điều chỉnh lương ngược từ quá khứ'
        					WHEN li.`lock_type` = 4 THEN 'Truy thu BHYT do nhân viên nghỉ việc'
                                                WHEN li.`lock_type` = 5 THEN 'Hoàn trả tiền BH do báo giảm ở quá khứ'
        					END AS 'lock_name',
        			 CASE 
        					WHEN li.`lock_type` = 4 THEN '4.5%'
        					WHEN li.`lock_type` <> 4 THEN '32%'
        					END AS 'INS_RATE'
                FROM lock_insurance li 
                LEFT JOIN staff st ON li.staff_id = st.id
                LEFT JOIN unit_code uc ON uc.id = li.unit_code_id
                        WHERE lock_time = '$lock_time' AND li.`unit_code_id` = 2 ";
                        
        $stmt2 = $db->prepare($sql2);
        $stmt2->execute();
        $data2 = $stmt2->fetchAll();
        $stmt2->closeCursor();
        $stmt2 = null;
        
        if(!empty($data1)){
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();
            
            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            
            $heads = array(
                $alphaExcel->ShowAndUp() => 'Công ty',
                $alphaExcel->ShowAndUp() => 'Mã NV',
                $alphaExcel->ShowAndUp() => 'Họ Tên',
                $alphaExcel->ShowAndUp() => 'Tỷ lệ đóng',
                $alphaExcel->ShowAndUp() => 'Mức lương',
                $alphaExcel->ShowAndUp() => 'Từ tháng',
                $alphaExcel->ShowAndUp() => 'Đến tháng',
                $alphaExcel->ShowAndUp() => 'Số tháng trích nộp',
                $alphaExcel->ShowAndUp() => 'Thành tiền',
                $alphaExcel->ShowAndUp() => 'Loại trích nộp',
            );
            
            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);
                
                $sheet->getStyle('A1:J1')->applyFromArray(array('font' => array('bold' => true)));
            
            
                $sheet->getColumnDimension('A')->setWidth(10);
                $sheet->getColumnDimension('B')->setWidth(30);
            
                foreach ($data1 as $key => $value) {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['unit_code_name']);
//                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['code']);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['fullname']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['INS_RATE']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['salary']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['from_month']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['to_month']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['number_of_month']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  $value['amount']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  $value['lock_name']);
                }
        }
        
        if(!empty($data2)){
            
            $PHPExcel->setActiveSheetIndex(0);
            //$sheet = $PHPExcel->getActiveSheet();
             $sheet = $PHPExcel->createSheet(1);
             $alpha = 'A';
            $index = 1; 
            $sheet->setCellValue($alpha++ . $index, "Công ty" );
            $sheet->setCellValue($alpha++ . $index, "Mã NV" );
            $sheet->setCellValue($alpha++ . $index, "Họ Tên" );
            $sheet->setCellValue($alpha++ . $index, "Tỷ lệ đóng" );
            $sheet->setCellValue($alpha++ . $index, "Mức lương" );
            $sheet->setCellValue($alpha++ . $index, "Từ tháng" );
            $sheet->setCellValue($alpha++ . $index, "Đến tháng" );
            $sheet->setCellValue($alpha++ . $index, "Số tháng trích nộp" );
            $sheet->setCellValue($alpha++ . $index, "Thành tiền" );
            $sheet->setCellValue($alpha++ . $index, "Loại trích nộp" );
            
                $sheet->getStyle('A1:J1')->applyFromArray(array('font' => array('bold' => true)));
            
                $sheet->getColumnDimension('A')->setWidth(10);
                $sheet->getColumnDimension('B')->setWidth(30);
            
                foreach ($data2 as $key => $value) {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['unit_code_name']);
//                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['code']);
                     $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['fullname']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['INS_RATE']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['salary']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['from_month']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['to_month']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['number_of_month']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  $value['amount']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  $value['lock_name']);
                }
        }

        $filename_over = 'Insurance - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
        $objWriter->save('php://output');