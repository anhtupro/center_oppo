<?php 
$page = $this->getRequest()->getParam('page',1);
$code = $this->getRequest()->getParam('code');
$leave_ids      = $this->getRequest()->getParam('leave_ids');
$restore        = $this->getRequest()->getParam('restore');

$total = 0;
$limit = 20;

$params = array(
    'code' => $code,
);
$QInsurance     = new Application_Model_Insurance();
$QLeaveDetail   = new Application_Model_LeaveDetail();

$db             = Zend_Registry::get('db');

//Restore Insurance
$stmt_restore = $db->prepare('CALL PR_Restore_Insurance (:pCode, :pLeaveId) ');
$stmt_restore->bindParam('pCode', $code, PDO::PARAM_STR);
$stmt_restore->bindParam('pLeaveId', $leave_ids, PDO::PARAM_STR);

$stmt_restore->execute();
$list = $stmt_restore->fetchAll();
$stmt_restore->closeCursor();

if(($restore == 1) AND !empty($list) AND !empty($leave_ids)){
    $sql = "update leave_detail SET insurance_passby = 0 WHERE id IN ($leave_ids) ";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $stmt->closeCursor();
    $stmt = $db = null;
    
}

$this->view->list = $list;
$this->view->params = $params;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->url    = HOST.'insurance/recycle-bin-list/'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset = $limit*($page-1);