<?php

$QPaySlipsTemp  = new Application_Model_PaySlipsTemp();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$email = $userStorage->email;

$params = array(
	'email'	=> $email
);

$data = $QPaySlipsTemp->getData($params);

$this->view->data = $data;

$this->view->params = $params;

