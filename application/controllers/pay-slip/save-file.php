<?php

	error_reporting(0);
        
        @ignore_user_abort(1);
        @set_time_limit(0);
        @ini_set('memory_limit', -1);
        @ini_set('max_execution_time', 0);
        ini_set('upload_max_filesize', "1000M");
        ini_set('post_max_size', "1000M");

	$current_month = date('m', strtotime(date('Y-m')." -1 month"));
	$current_year  = date('Y', strtotime(date('Y-m')." -1 month"));

	$QPaySlips     = new Application_Model_PaySlips();

	echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';
	echo '<div class="theme-showcase">';
	//================Process Uploaded File====================
	If($_FILES["file"]["tmp_name"] != ""){
		if(strstr('xlsx',$_FILES["file"]["type"] ) == FALSE){
			//die("Không phải file XLSX !");
		}
		if ($_FILES["file"]["error"] > 0)
	    {
	    	die("Return Code: " . $_FILES["file"]["error"] . "<br />");
	    }
		move_uploaded_file($_FILES["file"]["tmp_name"], "files/normal/data.xlsx");
	}
	//===================================================
	
	//===================Main=======================
	$inputfile = 'files/normal/data.xlsx';
        
	//$subject = $_POST['email_subject'];
        $subject = "PHIẾU LƯƠNG T13 VÀ THƯỞNG 2019";
        
	global $mailfrom;
	global $mailfromname;


	$mailfrom = 'nguyettrinh.dang@oppomobile.vn'; // Config this
	//$mailfrom = 'hr.saleleader@oppomobile.vn'; // Config this
	$mailfromname = "HR OPPOMobile"; // Config this

	//$tpl = $_POST['email_content'];
        $tpl = '<p>Phòng HCNS gửi bạn thông tin về Phiếu lương tháng 13 và thưởng năm 2019.</p>

            <p><strong>Đây là email từ hệ thống, yêu cầu giữ bảo mật tuyệt đối với những thông tin của phiếu lương. Nghiêm cấm chia sẻ đường link phiếu lương trong mail dưới mọi hình thức.</strong></p>

            <p>Vui lòng nhấn vào đường dẫn đính kèm, đăng nhập hệ thống center OPPO (thông tin đăng nhập: thông tin email cá nhân, Mật khẩu: mật khẩu đăng nhập hệ thống center) để xem Phiếu lương chi tiết:</p>

            <p>{1}</p>

            <p>***Lưu ý***:&nbsp;</p>

            <ul>
                    <li>Mọi thắc mắc về phiếu lương, vui lòng liên hệ Ms. Trinh –&nbsp;<a href="callto:(028) 39202555">(028) 39202555</a>&nbsp;– 105 hoặc gửi phản hồi qua địa chỉ email –&nbsp;<a href="mailto:nguyettrinh.dang@oppomobile.vn" target="_blank">nguyettrinh.dang@oppomobile.vn</a>&nbsp;để được giải đáp.</li>
                    <li>Trường hợp quên mật khẩu đăng nhập center, vui lòng liên hệ Ms. Yến Nhi&nbsp;<a href="callto:(028) 39202555">(028) 39202555</a>&nbsp;– 105) để được hỗ trợ.</li>
            </ul>

            <p>Trân trọng cám ơn!</p>';
        
	$xlsx = new SimpleXLSX($inputfile);

	$data = $xlsx->rows();

	// Đếm số cột, dựa vào số tiêu đề ở dòng 0, nếu 23 cột là headoffice, còn 25 cột là bảo hành, type = 3 đổi lại thành 27 cột để không 
	$i = 0;
	$num_cols = 0;
	while ($data[0][$i++] <> '') {
	  $num_cols++;
	}

	$type = 0;
        
	if($num_cols == 23){
		define('email', 						0);
		define('ho_ten', 						1);
		define('chuc_vu',	 					2);
		define('ngay_cong_dinh_muc', 			3);
		define('ngay_cong_binh_thuong', 		4);
		define('ngay_cong_thu_bay', 			5);
		define('ngay_cong_di_lam_ngay_le', 		6);
		define('phep_nam', 						7);
		define('tong_cong_tinh_luong', 			8);
		define('tong_luong_khong_phu_cap',		9);
		define('luong_theo_ngay_cong',		 	10);
		define('tro_cap',		 				11);
		define('kpi',		 					12);
		define('quyet_toan_phep_nam',		 	13);
		define('tong_luong',		 			14);
		define('luong_bhxh',		 			15);
		define('muoi_phan_tram',		 		16);
		define('cac_khoan_tru_khac',		 	17);
		define('thue_tncn',		 				18);
		define('thue_nop_them',		 			19);
		define('tong_tru',		 				20);
		define('luong_thuc_nhan',		 		21);
                define('ghi_chu',		 		22);

		$type = 1;
	}
	elseif($num_cols == 25){
		define('email', 						0);
		define('ho_ten', 						1);
		define('chuc_vu',	 					2);
		define('ngay_cong_dinh_muc', 			3);
		define('ngay_cong_binh_thuong', 		4);
		define('ngay_cong_chu_nhat', 			5);
		define('ngay_cong_di_lam_ngay_le', 		6);
		define('phep_nam', 						7);
		define('tong_cong_tinh_luong', 			8);
		define('tong_gio_tang_ca',		 		9);
		define('tong_luong_khong_phu_cap',		10);
		define('luong_ngay_cong_tang_ca',		11);
		define('tro_cap',		 				12);
		define('kpi',		 					13);
		define('bu_luong',		 				14);
		define('quyet_toan_phep_nam',		 	15);
		define('tong_luong',				 	16);
		define('luong_bhxh',		 			17);
		define('muoi_phan_tram',		 		18);
		define('cac_khoan_tru_khac',		 	19);
		define('thue_tncn',		 				20);
		define('thue_nop_them',		 			21);
		define('tong_tru',		 				22);
		define('luong_thuc_nhan',		 		23);
                define('ghi_chu',		 		24);

		$type = 2;
	}
	elseif($num_cols == 27){
		define('email', 						0);
		define('ho_ten', 						1);
		define('chuc_vu',	 					2);
		define('ngay_cong_dinh_muc', 			3);
		define('ngay_cong_binh_thuong', 		4);
		define('ngay_cong_chu_nhat', 			5);
		define('ngay_cong_di_lam_ngay_le', 		6);
		define('phep_nam', 						7);
		define('tong_cong_tinh_luong', 			8);
		define('tong_gio_tang_ca',		 		9);
		define('tong_luong_khong_phu_cap',		10);
		define('luong_theo_ngay_cong',			11);
		define('luong_ngay_cong_tang_ca',		12);
		define('tro_cap',		 				13);
		define('kpi',		 					14);
		define('bu_luong',		 				15);
		define('tong_luong',		 			16);
		define('luong_bhxh',		 			17);
		define('muoi_phan_tram',		 		18);
		define('cac_khoan_tru_khac',		 	19);
		define('thue_tncn',		 				20);
		define('truy_thu_thang_truoc',		 	21);
		define('tong_tru',		 				22);
		define('luong_thuc_nhan',		 		23);
		define('ghi_chu',		 				24);
		

		$type = 3;
	}
        elseif($num_cols == 7){
		define('email', 				0);
		define('ho_ten', 				1);
		define('ttncn_khau_tru',	 		2);
		define('ttncn_thu_tuc', 			3);
                define('ttncn_nop',                             4);
                define('ttncn_hoan_tra', 			5);
                define('ttncn_nop_them', 			6);
		
		$type = 4;
	}
        elseif($num_cols == 6){
            
                //define('email', 			0);
		//define('ho_ten', 			1);
		//define('bhxh_muc_moi',	 	2);
		//define('bhxh_muc_cu', 		3);
                //define('bhxh_chenh_lech',             4);
                //define('bhxh_so_tien_truy_thu', 	5);
		
		//$type = 5;
            
                define('email', 				0);
                define('ho_ten', 				1);
		define('chuc_vu', 				2);
		define('time_month_13',                         3);
		define('time_happy_1',                          4);
                define('time_happy_2',                          5);
		
		$type = 6;
            
        }
        elseif($num_cols == 5){
                define('email', 				0);
                define('ho_ten', 				1);
		define('chuc_vu', 				2);
		define('time_month_13',                         3);
		define('time_happy_1',                          4);
		
		$type = 7;
        }
        elseif($num_cols == 9){
                define('email', 				0);
                define('ho_ten', 				1);
                define('chuc_vu', 				2);
                
                define('month_13_muc_luong_co_so', 		3);
		define('month_13_so_thang_di_lam', 		4);
		define('month_13_muc_luong',                    5);
		define('month_13_muc_thuong',                   6);
                define('month_13_thue_thu_nhap',                7);
                define('month_13_thuc_nhan',                    8);
		
		$type = 8;
        }
        elseif($num_cols == 10){
                define('email', 				0);
                define('ho_ten', 				1);
                define('chuc_vu', 				2);
                
                define('month_13_muc_luong_co_so', 		3);
		define('month_13_so_thang_di_lam', 		4);
		define('month_13_muc_luong',                    5);
		define('month_13_muc_thuong',                   6);
                define('month_13_thue_thu_nhap',                7);
                define('month_13_thuc_nhan',                    8);
		
		$type = 9;
        }
        
        
	// Đếm số dòng, dựa vào số email ở cột 0; bỏ dòng 0 ra (tính từ dòng 1)
	$i = 1;
	$num_rows = 0;
	while ($data[$i++][0] <> '') {
	  $num_rows++;
	}


	// $i = 1; // index counter - skip 0 is header row of xlsx
	for($i = 1; $i <= $num_rows; $i++){
            
            //$subject = $subject;
            //$tpl = $tpl;
            
		//$numcol = count($data[$i]);

		if($data[$i][email] <> ''){

			$email 								= $data[$i][email];
			$ho_ten 							= $data[$i][ho_ten];
			$chuc_vu 							= $data[$i][chuc_vu];
			$ngay_cong_dinh_muc 				= $data[$i][ngay_cong_dinh_muc];
			$ngay_cong_binh_thuong 				= $data[$i][ngay_cong_binh_thuong];
			$ngay_cong_thu_bay 					= $data[$i][ngay_cong_thu_bay];
			$ngay_cong_chu_nhat 				= $data[$i][ngay_cong_chu_nhat];
			$ngay_cong_di_lam_ngay_le 			= $data[$i][ngay_cong_di_lam_ngay_le];
			$phep_nam 							= $data[$i][phep_nam];
			$ung_phep 							= $data[$i][ung_phep];
			$tong_cong_tinh_luong 				= $data[$i][tong_cong_tinh_luong];
			$tong_gio_tang_ca 					= $data[$i][tong_gio_tang_ca];
			$tong_luong_khong_phu_cap 			= $data[$i][tong_luong_khong_phu_cap];
			$luong_ngay_cong_tang_ca 			= $data[$i][luong_ngay_cong_tang_ca];
			$tong_luong 						= $data[$i][tong_luong];
			$luong_theo_ngay_cong 				= $data[$i][luong_theo_ngay_cong];
			$tro_cap 							= $data[$i][tro_cap];
			$kpi 								= $data[$i][kpi];
			$quyet_toan_phep_nam				= $data[$i][quyet_toan_phep_nam];
			$bu_luong 							= $data[$i][bu_luong];
			$tong_luong 						= $data[$i][tong_luong];
			$luong_bhxh 						= $data[$i][luong_bhxh];
			$muoi_phan_tram 					= $data[$i][muoi_phan_tram];
			$cac_khoan_tru_khac 				= $data[$i][cac_khoan_tru_khac];
			$thue_tncn 							= $data[$i][thue_tncn];
			$thue_nop_them 						= $data[$i][thue_nop_them];
			$tong_tru 							= $data[$i][tong_tru];
			$luong_thuc_nhan 					= $data[$i][luong_thuc_nhan];
			$truy_thu_thang_truoc 				= $data[$i][truy_thu_thang_truoc];
			$ghi_chu			 		= $data[$i][ghi_chu];
                        
                        $ttncn_khau_tru			 		= $data[$i][ttncn_khau_tru];
                        $ttncn_thu_tuc			 		= $data[$i][ttncn_thu_tuc];
                        $ttncn_nop			 		= $data[$i][ttncn_nop];
                        $ttncn_hoan_tra			 		= $data[$i][ttncn_hoan_tra];
                        $ttncn_nop_them			 		= $data[$i][ttncn_nop_them];
                        
                        $bhxh_muc_moi			 		= $data[$i][bhxh_muc_moi];
                        $bhxh_muc_cu			 		= $data[$i][bhxh_muc_cu];
                        $bhxh_chenh_lech			 	= $data[$i][bhxh_chenh_lech];
                        $bhxh_so_tien_truy_thu			 	= $data[$i][bhxh_so_tien_truy_thu];
                        
                        $time_month_13			 	= $data[$i][time_month_13];
                        $time_happy_1			 	= $data[$i][time_happy_1];
                        $time_happy_2			 	= $data[$i][time_happy_2];
                        
                        $month_13_muc_luong_co_so		= $data[$i][month_13_muc_luong_co_so];
                        $month_13_so_thang_di_lam		= $data[$i][month_13_so_thang_di_lam];
                        $month_13_muc_luong			= $data[$i][month_13_muc_luong];
                        $month_13_muc_thuong			= $data[$i][month_13_muc_thuong];
                        $month_13_thue_thu_nhap			= $data[$i][month_13_thue_thu_nhap];
                        $month_13_thuc_nhan			= $data[$i][month_13_thuc_nhan];

			$month 								= $current_month;
			$year 								= $current_year;

            $sn = date ('YmdHis') . substr ( microtime (), 2, 4 );
            $key = md5($maso_nv.$sn.rand(0,1000000));
            $tpl_row = str_replace("{1}", HOST."pay-slip/view-pay?key=".$key, $tpl);

			$data_pay = array(
				'email' 					=> $email,
				'ho_ten' 					=> $ho_ten,
				'chuc_vu' 					=> $chuc_vu,
                                'code' 						=> $this->enString($email, $key),
				
//                                'ngay_cong_dinh_muc' 			=> $this->enString($ngay_cong_dinh_muc, $key),
//				'ngay_cong_binh_thuong' 		=> $this->enString($ngay_cong_binh_thuong, $key),
//				'ngay_cong_thu_bay' 			=> $this->enString($ngay_cong_thu_bay, $key),
//				'ngay_cong_chu_nhat' 			=> $this->enString($ngay_cong_chu_nhat, $key),
//				'ngay_cong_di_lam_ngay_le' 		=> $this->enString($ngay_cong_di_lam_ngay_le, $key),
//				'phep_nam' 						=> $this->enString($phep_nam, $key),
//				'ung_phep' 						=> $this->enString($ung_phep, $key),
//				'tong_cong_tinh_luong' 			=> $this->enString($tong_cong_tinh_luong, $key),
//				'tong_gio_tang_ca' 				=> $this->enString($tong_gio_tang_ca, $key),
//				'tong_luong_khong_phu_cap' 		=> $this->enString($tong_luong_khong_phu_cap, $key),
//				'luong_ngay_cong_tang_ca' 		=> $this->enString($luong_ngay_cong_tang_ca, $key),
//				'tong_luong' 					=> $this->enString($tong_luong, $key),
//				'luong_theo_ngay_cong' 			=> $this->enString($luong_theo_ngay_cong, $key),
//				'tro_cap' 						=> $this->enString($tro_cap, $key),
//				'kpi' 							=> $this->enString($kpi, $key),
//				'quyet_toan_phep_nam' 			=> $this->enString($quyet_toan_phep_nam, $key),
//				'bu_luong' 						=> $this->enString($bu_luong, $key),
//				'tong_chi_phi_luong' 			=> $this->enString($tong_chi_phi_luong, $key),
//				'luong_bhxh' 					=> $this->enString($luong_bhxh, $key),
//				'muoi_phan_tram' 				=> $this->enString($muoi_phan_tram, $key),
//				'cac_khoan_tru_khac' 			=> $this->enString($cac_khoan_tru_khac, $key),
//				'thue_tncn' 					=> $this->enString($thue_tncn, $key),
//				'thue_nop_them' 				=> $this->enString($thue_nop_them, $key),
//				'tong_tru' 						=> $this->enString($tong_tru, $key),
//				'luong_thuc_nhan' 				=> $this->enString($luong_thuc_nhan, $key),
//				'truy_thu_thang_truoc' 			=> $this->enString($truy_thu_thang_truoc, $key),
//				'ghi_chu' 						=> $this->enString($ghi_chu, $key),
//                            
//                                'ttncn_khau_tru' 				=> $this->enString($ttncn_khau_tru, $key),
//                                'ttncn_thu_tuc' 				=> $this->enString($ttncn_thu_tuc, $key),
//                                'ttncn_nop'                                     => $this->enString($ttncn_nop, $key),
//                                'ttncn_hoan_tra'                                => $this->enString($ttncn_hoan_tra, $key),
//                                'ttncn_nop_them'                                => $this->enString($ttncn_nop_them, $key),
//                            
//                                'bhxh_muc_moi'                                  => $this->enString($bhxh_muc_moi, $key),
//                                'bhxh_muc_cu'                                   => $this->enString($bhxh_muc_cu, $key),
//                                'bhxh_chenh_lech'                               => $this->enString($bhxh_chenh_lech, $key),
//                                'bhxh_so_tien_truy_thu'                         => $this->enString($bhxh_so_tien_truy_thu, $key),
//                            
//                                'time_month_13'                                 => $this->enString($time_month_13, $key),
//                                'time_happy_1'                                  => $this->enString($time_happy_1, $key),
//                                'time_happy_2'                                  => $this->enString($time_happy_2, $key),
                                
                                'month_13_muc_luong_co_so'                      => $this->enString($month_13_muc_luong_co_so, $key),
                                'month_13_so_thang_di_lam'                      => $this->enString($month_13_so_thang_di_lam, $key),
                                'month_13_muc_luong'                            => $this->enString($month_13_muc_luong, $key),
                                'month_13_muc_thuong'                           => $this->enString($month_13_muc_thuong, $key),
                                'month_13_thue_thu_nhap'                        => $this->enString($month_13_thue_thu_nhap, $key),
                                'month_13_thuc_nhan'                            => $this->enString($month_13_thuc_nhan, $key),

				'month' 					=> $month,
				'year' 						=> $year,
				'status' 					=> 1,
				'type'						=> $type,
				'created_at'				=> date('Y-m-d H:i:s')
			);

			$where = array();
            $where[] = $QPaySlips->getAdapter()->quoteInto('email = ?', $email);
            $where[] = $QPaySlips->getAdapter()->quoteInto('month = ?', $current_month);
            $where[] = $QPaySlips->getAdapter()->quoteInto('year = ?', $current_year);
            $where[] = $QPaySlips->getAdapter()->quoteInto('type = ?', $type);
            $where[] = $QPaySlips->getAdapter()->quoteInto('status = ?', 1);
            $check_sendmail = $QPaySlips->fetchAll($where);
            
            
            if(count($check_sendmail) == 0 OR $email == 'nguyettrinh.dang@oppomobile.vn' OR $email == 'thehien.hoang@oppomobile.vn' OR $email == 'anhthoai.vo@oppomobile.vn'){
                $sendmail = $this->sendmail($email, $mailfrom, $mailfromname, $subject, $tpl_row);
                if($sendmail['ResponseCode'] == 1){
                        $QPaySlips->insert($data_pay);
                }
                echo $sendmail['Description'];
            }
            echo "Done";
            }
	}

	unlink($inputfile); // Delete file upload after send mail list done

	/*
	echo '<div class="alert alert-info"> <strong>Hoàn thành!</strong> ';
	echo 'Đã gửi email cho toàn bộ danh sách.';
	echo '</div><a class="link" href="index.php"><button type="button" class="btn btn-lg btn-success">Gửi tiếp</button></a></div>';
	*/

	//=================Function Area===================
    $this->_helper->layout->disableLayout();
