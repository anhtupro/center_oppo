<?php

$month  	= $this->getRequest()->getParam('month');
$year  		= $this->getRequest()->getParam('year');

$QPaySlipsTemp  = new Application_Model_PaySlipsTemp();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$email = $userStorage->email;

$params = array(
	'email'		=> $email,
	'month'		=> $month,
	'year'		=> $year
);

$data = $QPaySlipsTemp->getDataView($params);

$this->view->data = $data[0];


