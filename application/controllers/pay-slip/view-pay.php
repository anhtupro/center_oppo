<?php


$key = $this->getRequest()->getParam('key');

$QPaySlips   = new Application_Model_PaySlips();
$QPaySlipsLog   = new Application_Model_PaySlipsLog();
$QStaff     = new Application_Model_Staff();
$flashMessenger       = $this->_helper->flashMessenger;

$current_month = null;
$current_year  = null;


if ($this->getRequest()->getMethod() == 'POST'){

    $email     = $this->getRequest()->getParam('user_name');
    $password  = $this->getRequest()->getParam('password');
    $key_post  = $this->getRequest()->getParam('key_post');
    
    if (!preg_match('/@/', $email)) {
        $email .= '@oppo-aed.vn';
    }
	
    $email_check = substr($email, 0, strpos($email, '@'));

    $where = array();
    $where[] = $QStaff->getAdapter()->quoteInto('email = ?', $email);
    $where[] = $QStaff->getAdapter()->quoteInto('password = ?', md5($password));
    $staff   = $QStaff->fetchAll($where);
    
    $where_pay = $QPaySlips->getAdapter()->quoteInto("TRIM(email) = ?", $email);
    $result    = $QPaySlips->fetchAll($where_pay);
    
	
    if(isset($staff) and count($staff)){
        $data = array();
        foreach ($result as $k => $v) {

            if($this->deString($v['code'], $key_post) == $v['email']){
                $data = array(
                    'email'                     => $v['email'],
                    'stt'                       => $v['stt'],
                    'maso_nv'                   => $v['maso_nv'],
                    'ho_ten'                    => $v['ho_ten'],
                    'bo_phan'                   => $v['bo_phan'],
                    'chuc_vu'                   => $v['chuc_vu'],
					'type'                   	=> $v['type'],
                    'tong_luong'                => $this->deString($v['tong_luong'], $key_post),
                    'ngay_cong_dinh_muc'        => $this->deString($v['ngay_cong_dinh_muc'], $key_post),
                    'ngay_cong_binh_thuong'     => $this->deString($v['ngay_cong_binh_thuong'], $key_post),
                    'ngay_cong_thu_bay'         => $this->deString($v['ngay_cong_thu_bay'], $key_post),
                    'ngay_cong_chu_nhat'        => $this->deString($v['ngay_cong_chu_nhat'], $key_post),
                    'ngay_nghi_huong_luong'     => $this->deString($v['ngay_nghi_huong_luong'], $key_post),
                    'ngay_cong_di_lam_ngay_le'  => $this->deString($v['ngay_cong_di_lam_ngay_le'], $key_post),
                    'phep_nam'                  => $this->deString($v['phep_nam'], $key_post),
                    'ung_phep'                  => $this->deString($v['ung_phep'], $key_post),
                    'tong_cong_tinh_luong'      => $this->deString($v['tong_cong_tinh_luong'], $key_post),
                    'cac_khoan_cong'            => $this->deString($v['cac_khoan_cong'], $key_post),
                    'luong_theo_ngay_cong'      => $this->deString($v['luong_theo_ngay_cong'], $key_post),
                    'tro_cap'                   => $this->deString($v['tro_cap'], $key_post),
                    'ho_tro_cong_viec'          => $this->deString($v['ho_tro_cong_viec'], $key_post),
                    'kpi'                       => $this->deString($v['kpi'], $key_post),
                    'bu_luong'                  => $this->deString($v['bu_luong'], $key_post),
                    'tong_chi_phi_luong'        => $this->deString($v['tong_chi_phi_luong'], $key_post),
                    'tru_luong'                 => $this->deString($v['tru_luong'], $key_post),
                    'luong_bhxh'                => $this->deString($v['luong_bhxh'], $key_post),
                    'muoi_phan_tram'            => $this->deString($v['muoi_phan_tram'], $key_post),
                    'nop_doan_phi'              => $this->deString($v['nop_doan_phi'], $key_post),
                    'cac_khoan_tru_khac'        => $this->deString($v['cac_khoan_tru_khac'], $key_post),
                    'thue_tncn'                 => $this->deString($v['thue_tncn'], $key_post),
                    'truy_thu_bao_hiem'         => $this->deString($v['truy_thu_bao_hiem'], $key_post),
                    'truy_dong_nguoc_bh'        => $this->deString($v['truy_dong_nguoc_bh'], $key_post),
                    'tong_tru'                  => $this->deString($v['tong_tru'], $key_post),
                    'thanh_toan_lan3'           => $this->deString($v['thanh_toan_lan3'], $key_post),
                    'ky_nhan'                   => $this->deString($v['ky_nhan'], $key_post),
                    'truy_thu_thang_truoc'      => $this->deString($v['truy_thu_thang_truoc'], $key_post),
                    'ghi_chu'                   => $this->deString($v['ghi_chu'], $key_post),
                    'code'                      => $this->deString($v['code'], $key_post),
					
					'tong_gio_tang_ca'           => $this->deString($v['tong_gio_tang_ca'], $key_post),
					'tong_luong_khong_phu_cap'   => $this->deString($v['tong_luong_khong_phu_cap'], $key_post),
					'luong_ngay_cong_tang_ca'    => $this->deString($v['luong_ngay_cong_tang_ca'], $key_post),
					'quyet_toan_phep_nam'        => $this->deString($v['quyet_toan_phep_nam'], $key_post),
					'thue_nop_them'              => $this->deString($v['thue_nop_them'], $key_post),
					'luong_thuc_nhan'            => $this->deString($v['luong_thuc_nhan'], $key_post),
					'created_at'                 => $this->deString($v['created_at'], $key_post),
                    
                    'ttncn_khau_tru'           => $this->deString($v['ttncn_khau_tru'], $key_post),
                    'ttncn_thu_tuc'            => $this->deString($v['ttncn_thu_tuc'], $key_post),
                    'ttncn_nop'                => $this->deString($v['ttncn_nop'], $key_post),
                    'ttncn_hoan_tra'           => $this->deString($v['ttncn_hoan_tra'], $key_post),
                    'ttncn_nop_them'           => $this->deString($v['ttncn_nop_them'], $key_post),
                    
                    'bhxh_muc_moi'             => $this->deString($v['bhxh_muc_moi'], $key_post),
                    'bhxh_muc_cu'              => $this->deString($v['bhxh_muc_cu'], $key_post),
                    'bhxh_chenh_lech'          => $this->deString($v['bhxh_chenh_lech'], $key_post),
                    'bhxh_so_tien_truy_thu'    => $this->deString($v['bhxh_so_tien_truy_thu'], $key_post),
                    
                    'time_month_13'            => $this->deString($v['time_month_13'], $key_post),
                    'time_happy_1'             => $this->deString($v['time_happy_1'], $key_post),
                    'time_happy_2'             => $this->deString($v['time_happy_2'], $key_post),
                    
                    'month_13_muc_luong_co_so'        => $this->deString($v['month_13_muc_luong_co_so'], $key_post),
                    'month_13_so_thang_di_lam'       => $this->deString($v['month_13_so_thang_di_lam'], $key_post),
                    'month_13_muc_luong'             => $this->deString($v['month_13_muc_luong'], $key_post),
                    'month_13_muc_thuong'            => $this->deString($v['month_13_muc_thuong'], $key_post),
                    'month_13_thue_thu_nhap'         => $this->deString($v['month_13_thue_thu_nhap'], $key_post),
                    'month_13_thuc_nhan'             => $this->deString($v['month_13_thuc_nhan'], $key_post),
                    'maso_nhanvien'                  => $this->deString($v['maso_nhanvien'], $key_post),
                    'so_thang_tam_ung'               => $this->deString($v['so_thang_tam_ung'], $key_post),
				
					
                    'month'                     => $v['month'],
                    'year'                      => $v['year']
                );
				
				$current_month = $v['month'];
				$current_year = $v['year'];
				
				//Save data log
                $ip = $_SERVER['REMOTE_ADDR'];
                $data_log = array(
                    'ip'    => $ip,
                    'time'  => date('Y-m-d h:i:s'),
                    'month' => $v['month'],
                    'year'  => $v['year'],
                    'code'  => $v['email'],
                );
                $QPaySlipsLog->insert($data_log);
                //
            }
        }

        $this->view->data = $data;
        
    }
    else{
        $flashMessenger->setNamespace('error')->addMessage('Tài khoản hoặc mật khẩu không đúng!');
        $this->redirect('pay-slip/view-pay?key='.$key_post);
    }
}

$this->view->current_month = $current_month;
$this->view->current_year = $current_year;

$this->view->key = $key;

$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;
