<?php
class InternalController extends My_Controller_Action
{
    
    public function init()
    {
        $this->_helper->layout->setLayout('internal');
    }
    
    public function indexAction()
    {
        
    }
    
    public function planAction(){
        $QAppraisalOfficePlan = new Application_Model_AppraisalOfficePlan();
        $plans = $QAppraisalOfficePlan->fetchAll(['aop_is_deleted = 0', 'aop_type = 2'], 'aop_id ASC')->toArray();
        
        for ($i = 0; $i < count($plans); $i++) {
            $plans[$i]['aop_from'] = date('d/m/Y', strtotime($plans[$i]['aop_from']));
            $plans[$i]['aop_to'] = date('d/m/Y', strtotime($plans[$i]['aop_to']));
        }
        $plans = json_encode($plans);

        $QDSurvey = new Application_Model_DynamicSurvey();
        $surveys = $QDSurvey->getActiveSurveyByRef('appraisal_office');
        foreach ($surveys as &$survey) {
            $survey['ds_name'] = trim($survey['ds_name']);
        }
        $surveys = json_encode($surveys);
        $this->view->data = [
            'plans' => $plans,
            'surveys' => $surveys
        ];
    }
    
    public function listSurveyAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $QDynamicSurvey = new Application_Model_DynamicSurvey();
        $page = $this->getRequest()->getParam('page', 1);
        $params = array(
            'staff_id' => $userStorage->id
        );
        $limit = 10;
        $list = $QDynamicSurvey->fetchPagination($page, $limit, $total, $params);
        $this->view->list = $list;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->offset = $limit * ($page - 1);
        $this->view->url = HOST . 'dynamic-survey/list' . '?';
    }
    
    public function listStaffDetailsAction()
    {
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $category = $this->getRequest()->getParam('category');
        $skill = $this->getRequest()->getParam('skill');
        
        $level_arr = [
            'Level 1' => 1,
            'Level 2' => 2,
            'Level 3' => 3,
            'Level 4' => 4,
            'Level 5' => 5,
        ];
        
        $level = $level_arr[$category];
        
        $data = [
            $level,
            $skill
        ];
        echo json_encode($data);
        return;
        
    }
    
    public function listLessonAction(){
        
    }
    
}




















