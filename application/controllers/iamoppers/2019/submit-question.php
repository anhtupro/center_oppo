<?php
try {
    $submit = $this->getRequest()->getParam('submit');
    $time_end = $this->getRequest()->getParam('time_end');
    $question_id = $this->getRequest()->getParam('question_id');
    $id_master = $this->getRequest()->getParam('id_master');
    $user_choose = $this->getRequest()->getParam('user_choose', 0);

    $submited_at = date('Y-m-d H:i:s', strtotime('' . date('Y-m-d H:i:s') . ' -1 seconds')); //thời gian lúc vừa submit
    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
    $milisecond = $d->format("u"); // lấy milisecond


    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QMiniGame2018 = new Application_Model_MiniGame2018();
    $QMiniGame2018Detail = new Application_Model_MiniGame2018Detail();
    $QMiniGameAnswer2018 = new Application_Model_MiniGameAnswer2018();
    $id_staff = $userStorage->id;
    $params = [
        'staff_id' => $staff_id,
        'question_id' => $question_id,
        'id_master' => $id_master
    ];
    //buffer point
//  $current_time = date("Y-m-d H:i:s");
//  $from_one     = "2018-12-07 12:00:00";
//  $from_two     = "2018-12-07 12:05:00";
//  $from_three   = "2018-12-07 12:10:00";


    if (!empty($submit)) {
        // lấy đáp án câu hỏi
        $answer = $QMiniGameAnswer2018->getAnswer($params);
        //lấy time bắt đầu câu hỏi từ database
        $time_started = $QMiniGame2018Detail->getTimeStartQuestion($params);
        //Tính điểm dựa vào số giây làm, mỗi giây 10 điểm
        $date1 = date_create($time_started[0]['started_at']);
        $date2 = date_create($submited_at);
        $diff_ = date_diff($date1, $date2);
        $diff = $diff_->format("%s");//tính số giây làm


        //buffer vào làm sớm - muộn
        // if($current_time <= $from_two){
        //   $point = 300 -($diff*15)-(round($milisecond/1000000,1)*15); //tính điểm trừ luôn mili giây
        // }elseif($current_time > $from_two && $current_time <= $from_three){
        //   $point = 250 -($diff*12.5)-(round($milisecond/1000000,1)*12.5); //tính điểm trừ luôn mili giây
        // }else{
        //    $point = 200 -($diff*10)-(round($milisecond/1000000,1)*10); //tính điểm trừ luôn mili giây
        // }


        //tính point theo số giây, tam bỏ chỉ tính theo số giây làm thôi
//    $total_point = 200; //tổng điểm mỗi câu
//    $total_point_second = 5; // số điểm mỗi giây (mỗi câu 40s * 5point = 200point)
//  	$point = $total_point -($diff*$total_point_second)-(round($milisecond/1000000,1)*$total_point_second);
        //tổng điểm trừ số giây (mỗi giây 15điểm) - mili giây(số mili *30)

        $point = $diff;
//  	if($user_choose==0 || $point < 0 || $answer[0]['answer'] != $user_choose){
//  		$point = 0;
//  	}

        $right = 0;
        if ($user_choose == 0 || $answer[0]['answer'] != $user_choose) {
            $right = 1;
        }
        $where = [];
        $where[] = $QMiniGame2018Detail->getAdapter()->quoteInto('master_id = ?', $id_master);
        $where[] = $QMiniGame2018Detail->getAdapter()->quoteInto('question_id = ?', $question_id);
        $data = [
            'master_id' => $id_master,
            'question_id' => $question_id,
            'user_choose' => $user_choose,
            'submit_at' => $submited_at,
            'point' => $point,
            'diff' => $diff,
            'submit_milisecond' => $milisecond,
            'is_right' => $right

        ];
        $QMiniGame2018Detail->update($data, $where);
        exit;
    }
}
catch (exception $e) {
    echo $e->getMessage();
    exit;
}