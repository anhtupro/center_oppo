<?php
$this->_helper->layout->disableLayout();
$QMiniGame2018 			= new Application_Model_MiniGame2018();
$QMiniGame2018Detail 	= new Application_Model_MiniGame2018Detail();
$QMiniGame2018Answer 	= new Application_Model_MiniGameAnswer2018();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
$staff_id 	= $userStorage->id;
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
if(empty($userStorage)){
    $url = HOST . 'user/login?b='.urldecode('/iamoppers/result');
    $this->redirect($url);
}
$params = [
    'staff_id'	=> $staff_id
];
$xml 	= simplexml_load_file(APPLICATION_PATH.'/../public/xml/data-xml.xml');
$question 	= json_decode(json_encode($xml, true));
$data = $QMiniGame2018->getResult($params);

$lst_answer = $QMiniGame2018Answer->getListAnswer($params);

$lst_correct_answers = array();
foreach ($lst_answer as $key => $value) {
    $lst_correct_answers[$value['question_id']] = $value['answer'];
}

// foreach ($question->question as $key => $value) {
// 	foreach ($data as $k => $val) {
// 		if($value->id == $val['question_id']){

// 		}
// 	}
// }

$this->view->data = $data;
$this->view->question = $question;
$this->view->lst_correct_answers = $lst_correct_answers;
// /var_dump($question->question[1]->id); exit;

$this->_helper->viewRenderer->setRender('2019/result');