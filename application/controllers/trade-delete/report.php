<?php


$QTradeDelete = new Application_Model_TradeDelete();
$QArea        = new Application_Model_Area();

$category = $QTradeDelete->getCategory();
$area = $QArea->get_cache();

$params = array();

$result = $QTradeDelete->getDataArea($params);


$data_area = array();
foreach($result as $key=>$value){
    $data_area[$value['id']][$value['good_id']] = $value['total'];
}

$this->view->data_area = $data_area;
$this->view->category = $category;
$this->view->area = $area;


