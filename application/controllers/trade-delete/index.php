<?php

$page 					= $this->getRequest()->getParam('page', 1);
$imei 					= $this->getRequest()->getParam('imei', null);
$store_code			    = $this->getRequest()->getParam('store_code', null);
$store_name 		    = $this->getRequest()->getParam('store_name', null);
$del 					= $this->getRequest()->getParam('del');
$lock 					= $this->getRequest()->getParam('lock');
$area_id 				= $this->getRequest()->getParam('area_id');
$export                 = $this->getRequest()->getParam('export');

$QStore				 	= new Application_Model_Store();
$userStorage    		= Zend_Auth::getInstance()->getStorage()->read();

$QArea				 	= new Application_Model_Area();
$this->view->areas 		= $QArea->fetchAll(null, 'name');

$params = array(
    'page' 		 => $page,
    'imei' 		 => $imei,
    'limit' 	 => $limit,
    'store_code' => $store_code,
    'store_name' => $store_name,
    'offset'     => $offset,
    'get_store'  => 1,
    'area_id'	 => $area_id
);

if($userStorage->title == SALES_TITLE){
	$params['staff_id'] = $userStorage->id;
}
elseif($userStorage->title == 173){

}
elseif($userStorage->id != SUPERADMIN_ID){
	echo "You have no permission to access this.";exit;
}

$page               = $this->getRequest()->getParam('page', 1);
$limit              = LIMITATION;
$total              = 0;
$data = $QStore->fetchPaginationStoreStaff($page, $limit, $total, $params);

if($export and $export == 1){
    $data = $QStore->fetchPaginationStoreStaffExport($page, $limit, $total, $params);
    echo "<pre>";
    var_dump($data);exit;

    $this->reportStoreStaffExport($data);
}

$this->view->params = $params;
$this->view->data   = $data;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->url    = HOST.'trade-delete'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset = $limit*($page-1);

