<?php

$QStore = new Application_Model_Store();

$object_id = $this->getRequest()->getParam('object_id');
$lock = $this->getRequest()->getParam('lock');
$add = $this->getRequest()->getParam('add');
$del_id = $this->getRequest()->getParam('del_id');
$del = $this->getRequest()->getParam('del');

if(empty($object_id))
{
	$this->_redirect('/trade-delete');
	exit;
}

if(!empty($lock))
{
	$this->lockImeiStore($object_id);
}

if(!empty($del_id))
{
	$this->deleteGood($del_id);
}

if(!empty($del))
{
	$this->deleteImei($del);
}

if(!empty($add))
{
	$object_id = $this->getRequest()->getParam('object_id');
	$good_id = $this->getRequest()->getParam('good_id');
	$number = $this->getRequest()->getParam('number');
	$params = array(
		'object_id' => $object_id,
		'good_id' => $good_id,
		'number' => $number
	);
	$this->addGood($params);
}

$data_imei = $this->getImeiByStore($object_id);
$data_good = $this->getGoodByStore($object_id);
$this->view->category = $this->getCategory();
$this->view->object_id = $object_id;
$this->view->data_imei = $data_imei;
$this->view->data_good = $data_good;

$where = $QStore->getAdapter()->quoteInto("id = ?", $object_id);
$this->view->store = $QStore->fetchRow($where);