<?php
class CostingController extends My_Controller_Action
{
	 public function createCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'create-costing.php';
    }
    public function confirmCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'confirm-costing.php';
    }
    public function listCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'list-costing.php';
    }
    public function saveCreateCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'save-create-costing.php';
    }
    public function ajaxCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'ajax-costing.php';
    }
     public function editCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'edit-costing.php';
    }
    public function thanhtoanAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'thanhtoan.php';
    }
    public function viewCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'view-costing.php';
    }
    public function reportCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'report-costing.php';
    }
    public function reportTypeCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'report-type-costing.php';
    }
    public function reportSellCostAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'report-sell-cost.php';
    }
    public function reportDetailsCostingAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'report-details-costing.php';
    }
    public function listTypeAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'list-type.php';
    }
    public function createTypeApproveAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'create-type-approve.php';
    }
     public function editTypeAction()
    {
        require_once 'costing' . DIRECTORY_SEPARATOR . 'edit-type.php';
    }

}