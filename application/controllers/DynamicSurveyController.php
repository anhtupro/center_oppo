<?php
/**
 * Created by PhpStorm.
 * User: lphuctai
 * Date: 09/04/2019
 * Time: 11:54
 */

class DynamicSurveyController extends My_Controller_Action
{
    public function createAction()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function editAction()
    {
        $surveyId = $this->getRequest()->getParam('survey_id');
        
        $QDSurvey = new Application_Model_DynamicSurvey();
        $QDSurveyQuestion = new Application_Model_DynamicSurveyQuestion();
        $QDSurveyAnswer = new Application_Model_DynamicSurveyAnswer();
        
        $dataFields = [];
        if ($surveyId) {
            $survey = $QDSurvey->fetchRow([
                $QDSurvey->getAdapter()->quoteInto('ds_id = ?', $surveyId),
                $QDSurvey->getAdapter()->quoteInto('ds_status = ? ', 1),
                $QDSurvey->getAdapter()->quoteInto('ds_is_deleted = ? ', 0),
            ]);
            if ($survey) {
                $survey = $survey->toArray();

                // Get list Question
                $questions = $QDSurveyQuestion->fetchAll([
                    $QDSurveyQuestion->getAdapter()->quoteInto('fk_ds = ?', $surveyId),
                    $QDSurveyQuestion->getAdapter()->quoteInto('dsq_is_deleted = ?', 0),
                ])->toArray();

                // Get list Answer
                $questionIds = [];
                foreach ($questions as $question) {
                    $questionIds[] = $question['dsq_id'];
                }
                $answers = $QDSurveyAnswer->fetchAll([
                    $QDSurveyAnswer->getAdapter()->quoteInto('fk_dsq in (?)', $questionIds),
                    $QDSurveyAnswer->getAdapter()->quoteInto('dsa_is_deleted = ?', 0),
                ])->toArray();

                // Build field data
                foreach ($questions as $question) {
                    $qTmp = json_decode($question['dsq_data'], true);
                    $qTmp['title'] = str_replace('"', '\"', $qTmp['title']);
                    foreach ($answers as $answer) {
                        if ($answer['fk_dsq'] == $question['dsq_id']) {
                            $aTmp = [
                                'title' => str_replace('"', '\"', $answer['dsa_title']),
                                'value' => $answer['dsa_id'],
                            ];
                            if (!array_key_exists('choices', $qTmp)) {
                                $qTmp['choices'] = [];
                            }
                            $qTmp['choices'][] = $aTmp;
                        }
                    }
                    if (count($qTmp['choices']) > 0) {
                        $qTmp['id'] = 'question_choose_' . $question['dsq_id'];
                        $qTmp['name'] = 'question_choose_' . $question['dsq_id'];
                    } else {
                        $qTmp['id'] = 'question_fill_' . $question['dsq_id'];
                        $qTmp['name'] = 'question_fill_' . $question['dsq_id'];
                    }
                    $dataFields[] = $qTmp;
                }
            }
        }
        $this->view->data_fields = json_encode($dataFields, JSON_HEX_QUOT);
        
        $this->view->dynamic_survey = $survey;
        $this->_helper->layout->disableLayout();
    }

    public function saveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        

        $title = $this->getRequest()->getParam('title');
        $surveyId = $this->getRequest()->getParam('survey_id');
        $fields = json_decode($this->getRequest()->getParam('fields'), true);
        
        $QDSurvey = new Application_Model_DynamicSurvey();
        $QDSurveyQuestion = new Application_Model_DynamicSurveyQuestion();
        $QDSurveyAnswer = new Application_Model_DynamicSurveyAnswer();
        
        
        
        $db = null;
        try {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            
            if ($surveyId) {
                $survey = $QDSurvey->fetchRow([
                    $QDSurvey->getAdapter()->quoteInto('ds_id = ?', $surveyId),
                    $QDSurvey->getAdapter()->quoteInto('ds_status = ? ', 1),
                    $QDSurvey->getAdapter()->quoteInto('ds_is_deleted = ? ', 0),
                ]);
            }
            
            if(!empty($survey)){
                $dsId = $survey['ds_id'];
                $QDSurveyQuestion->delete(["fk_ds = ?" => $dsId]);
            }
            else{
                $dsId = $QDSurvey->insert([
                    'ds_name' => $title,
                    'ds_status' => 1,
                    'ds_created_at' => date('Y-m-d H:i:s'),
                    'ds_created_by' => $userStorage->id
                ]);
            }
            
            
            foreach ($fields as $field) {
                $dsqId = $QDSurveyQuestion->insert([
                    'fk_ds' => $dsId,
                    'dsq_title' => $field['dsq_title'],
                    'dsq_type' => $field['dsq_type'],
                    'dsq_data' => json_encode($field['dsq_data']),
                    'dsq_created_at' => date('Y-m-d H:i:s'),
                    'dsq_created_by' => $userStorage->id
                ]);
                if (array_key_exists('choices', $field) && count($field['choices']) > 0) {
                    foreach ($field['choices'] as $index => $choice) {
                        $dsaId = $QDSurveyAnswer->insert([
                            'fk_dsq' => $dsqId,
                            'dsa_title' => $choice['title'],
                            'dsa_value' => $choice['value'],
                            'dsa_sort_id' => $index + 1,
                            'dsa_created_at' => date('Y-m-d H:i:s'),
                            'dsa_created_by' => $userStorage->id,
                        ]);
                    }

                }
            }
            $db->commit();
            echo json_encode([
                'status' => 0,
                'message' => 'Success',
            ]);
        } catch (Exception $ex) {
            $db->rollback();
            echo json_encode(array('status' => 1, 'message' => $ex->getMessage()));
        }
    }

    public function submitAction()
    {
        $this->_helper->layout->disableLayout();

        $surveyId = $this->getRequest()->getParam('survey_id');
        $actionType = $this->getRequest()->getParam('action_type');
        $data = $this->getRequest()->getParam('data');
        
        
        $QStaff = new Application_Model_Staff();
        
        $surveyTitle = '';
        $dataFields = [];
        if ($surveyId) {
            $QDSurvey = new Application_Model_DynamicSurvey();
            $survey = $QDSurvey->fetchRow([
                $QDSurvey->getAdapter()->quoteInto('ds_id = ?', $surveyId),
                $QDSurvey->getAdapter()->quoteInto('ds_status = ? ', 1),
                $QDSurvey->getAdapter()->quoteInto('ds_is_deleted = ? ', 0),
            ]);
            if ($survey) {
                $survey = $survey->toArray();
                $surveyTitle = $survey['ds_title'];

                // Get list Question
                $QDSurveyQuestion = new Application_Model_DynamicSurveyQuestion();
                $questions = $QDSurveyQuestion->fetchAll([
                    $QDSurveyQuestion->getAdapter()->quoteInto('fk_ds = ?', $surveyId),
                    $QDSurveyQuestion->getAdapter()->quoteInto('dsq_is_deleted = ?', 0),
                ])->toArray();

                // Get list Answer
                $questionIds = [];
                foreach ($questions as $question) {
                    $questionIds[] = $question['dsq_id'];
                }
                $QDSurveyAnswer = new Application_Model_DynamicSurveyAnswer();
                $answers = $QDSurveyAnswer->fetchAll([
                    $QDSurveyAnswer->getAdapter()->quoteInto('fk_dsq in (?)', $questionIds),
                    $QDSurveyAnswer->getAdapter()->quoteInto('dsa_is_deleted = ?', 0),
                ])->toArray();

                // Build field data
                foreach ($questions as $question) {
                    $qTmp = json_decode($question['dsq_data'], true);
                    $qTmp['title'] = str_replace('"', '\"', $qTmp['title']);
                    foreach ($answers as $answer) {
                        if ($answer['fk_dsq'] == $question['dsq_id']) {
                            $aTmp = [
                                'title' => str_replace('"', '\"', $answer['dsa_title']),
                                'value' => $answer['dsa_id'],
                            ];
                            if (!array_key_exists('choices', $qTmp)) {
                                $qTmp['choices'] = [];
                            }
                            $qTmp['choices'][] = $aTmp;
                        }
                    }
                    if (count($qTmp['choices']) > 0) {
                        $qTmp['id'] = 'question_choose_' . $question['dsq_id'];
                        $qTmp['name'] = 'question_choose_' . $question['dsq_id'];
                    } else {
                        $qTmp['id'] = 'question_fill_' . $question['dsq_id'];
                        $qTmp['name'] = 'question_fill_' . $question['dsq_id'];
                    }
                    $dataFields[] = $qTmp;
                }
            }
        }
        
        $data_de = json_decode($data);
        $to_staff = $QStaff->fetchRow([
                    $QStaff->getAdapter()->quoteInto('id = ?', $data_de->to_staff),
                ])->toArray();
        
        
        
        $this->view->survey_title = $surveyTitle;
        $this->view->data_fields = json_encode($dataFields);
        $this->view->survey_id = $surveyId;
        $this->view->actionType = $actionType;
        $this->view->data = $data;
        $this->view->to_staff = $to_staff;
    }

    public function previewLiveAction()
    {
        $this->_helper->layout->disableLayout();

        $surveyId = $this->getRequest()->getParam('survey_id');
        $surveyTitle = '';
        $dataFields = [];
        if ($surveyId) {
            $QDSurvey = new Application_Model_DynamicSurvey();
            $survey = $QDSurvey->fetchRow([
                $QDSurvey->getAdapter()->quoteInto('ds_id = ?', $surveyId),
                $QDSurvey->getAdapter()->quoteInto('ds_status = ? ', 1),
                $QDSurvey->getAdapter()->quoteInto('ds_is_deleted = ? ', 0),
            ]);
            if ($survey) {
                $survey = $survey->toArray();
                $surveyTitle = $survey['ds_title'];

                // Get list Question
                $QDSurveyQuestion = new Application_Model_DynamicSurveyQuestion();
                $questions = $QDSurveyQuestion->fetchAll([
                    $QDSurveyQuestion->getAdapter()->quoteInto('fk_ds = ?', $surveyId),
                    $QDSurveyQuestion->getAdapter()->quoteInto('dsq_is_deleted = ?', 0),
                ])->toArray();

                // Get list Answer
                $questionIds = [];
                foreach ($questions as $question) {
                    $questionIds[] = $question['dsq_id'];
                }
                $QDSurveyAnswer = new Application_Model_DynamicSurveyAnswer();
                $answers = $QDSurveyAnswer->fetchAll([
                    $QDSurveyAnswer->getAdapter()->quoteInto('fk_dsq in (?)', $questionIds),
                    $QDSurveyAnswer->getAdapter()->quoteInto('dsa_is_deleted = ?', 0),
                ])->toArray();

                // Build field data
                foreach ($questions as $question) {
                    $qTmp = json_decode($question['dsq_data'], true);
                    $qTmp['title'] = str_replace('"', '\"', $qTmp['title']);
                    foreach ($answers as $answer) {
                        if ($answer['fk_dsq'] == $question['dsq_id']) {
                            $aTmp = [
                                'title' => str_replace('"', '\"', $answer['dsa_title']),
                                'value' => $answer['dsa_id'],
                            ];
                            if (!array_key_exists('choices', $qTmp)) {
                                $qTmp['choices'] = [];
                            }
                            $qTmp['choices'][] = $aTmp;
                        }
                    }
                    if (count($qTmp['choices']) > 0) {
                        $qTmp['id'] = 'question_choose_' . $question['dsq_id'];
                        $qTmp['name'] = 'question_choose_' . $question['dsq_id'];
                    } else {
                        $qTmp['id'] = 'question_fill_' . $question['dsq_id'];
                        $qTmp['name'] = 'question_fill_' . $question['dsq_id'];
                    }
                    $dataFields[] = $qTmp;
                }
            }
        }
        $this->view->survey_title = $surveyTitle;
        $this->view->data_fields = json_encode($dataFields, JSON_HEX_QUOT);
        $this->view->survey_id = $surveyId;
    }

    public function saveSubmitAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        $surveyId = $request->getParam('survey_id');
        $action = $request->getParam('action_type');
        $data = $request->getParam('data');
        $params = $request->getUserParams();

        $db = null;
        try {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $QDSurvey = new Application_Model_DynamicSurveyResult();
            $dsId = $QDSurvey->insert([
                'fk_ds' => $surveyId,
                'fk_staff' => $userStorage->id,
                'dsr_created_at' => date('Y-m-d H:i:s'),
                'dsr_created_by' => $userStorage->id
            ]);

            $QDSurveyResultDetail = new Application_Model_DynamicSurveyResultDetail();
            foreach ($params as $key => $param) {
                $keyData = explode('_', $key);
                if (count($keyData) == 3 && $keyData[0] == 'question') {
                    $resultDetail = [
                        'fk_dsr' => $dsId,
                        'fk_dsq' => $keyData[2],
                    ];
                    if ($keyData[1] == 'choose') { // Choice answer
                        if (is_array($param)) {
                            $resultDetail['fk_dsa'] = json_encode($param);
                        } else {
                            $resultDetail['fk_dsa'] = $param;
                        }
                        $resultDetail['dsrd_text'] = null;
                    } else { // Fill value
                        $resultDetail['dsrd_text'] = $param;
                        $resultDetail['fk_dsa'] = null;
                    }
                    $QDSurveyResultDetail->insert($resultDetail);
                }
            }
            if($action && $data) {
                $data = json_decode($data, true);
                self::pluginProcessAdapter($surveyId, $action, $dsId, $data);
            }
            $db->commit();
            echo json_encode([
                'status' => 0,
                'message' => 'Success',
                'plan_id' => $data['plan_id'],
                'action_type'   => $action
            ]);
        } catch (Exception $ex) {
            $db->rollback();
            echo json_encode([
                'status' => -1,
                'message' => $ex->getMessage()
            ]);
            return;
        }
    }

    public function previewAction()
    {
        $this->_helper->layout->disableLayout();
        $form_html = $this->getRequest()->getParam('form_html');
        $description = $this->getRequest()->getParam('description_preview');
        $form_name = $this->getRequest()->getParam('title_preview');

        $this->view->form_html = $form_html;
        $this->view->description = $description;
        $this->view->form_name = $form_name;
    }

    public function listAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $QDynamicSurvey = new Application_Model_DynamicSurvey();
        $page = $this->getRequest()->getParam('page', 1);
        $params = array(
            'staff_id' => $userStorage->id
        );
        $limit = 10;
        $list = $QDynamicSurvey->fetchPagination($page, $limit, $total, $params);
        $this->view->list = $list;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->offset = $limit * ($page - 1);
        $this->view->url = HOST . 'dynamic-survey/list' . '?';
    }

    const TITLE_MANAGER_ID = 462;
    const TITLE_RSM_ID = 308;
    const TITLE_ASM_ID = 179;
    const TITLE_ASM_STANDBY_ID = 181;
    const TITLE_SALE_LEADER_ID = 190;
    const TITLE_SALE_ID = 183;
    const TITLE_PG_ID = 182;

    /**
     * @param $surveyId
     * @param $action
     * @param $resultId
     * @param $data
     * @throws Zend_Auth_Storage_Exception
     */
    private function pluginProcessAdapter($surveyId, $action, $resultId, $data)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if ($action == 'appraisal_office') {
            if(!array_key_exists('plan_id', $data) || !$data['plan_id']) {
                return;
            }
            $QCapacity = new Application_Model_DynamicSurveyCapacity();
            $capacity = $QCapacity->fetchAll([
                $QCapacity->getAdapter()->quoteInto('fk_aop = ?', $data['plan_id']),
                $QCapacity->getAdapter()->quoteInto('fk_staff = ?', $userStorage->id),
                $QCapacity->getAdapter()->quoteInto('fk_staff_be_appraisal = ?', $data['to_staff']),
                $QCapacity->getAdapter()->quoteInto('aoc_is_deleted = ?', 0),
            ])->toArray();
            if(count($capacity) == 0) {
                $QCapacity->insert([
                    'fk_aop' => $data['plan_id'],
                    'fk_staff' => $userStorage->id,
                    'fk_staff_be_appraisal' => $data['to_staff'],
                    'fk_dsr' => $resultId,
                    'aoc_created_at' => date('Y-m-d H:i:s'),
                    'aoc_created_by' => $userStorage->id
                ]);
            }
        } elseif ($action == 'appraisal_sale') {
            if(!array_key_exists('plan_id', $data) || !$data['plan_id'] || $userStorage->id != $data['from_staff']) {
                return;
            }
            $QMember = new Application_Model_AppraisalSaleMember();
            $member = $QMember->fetchAll([
                $QMember->getAdapter()->quoteInto('asp_id = ?', $data['plan_id']),
                $QMember->getAdapter()->quoteInto('asm_staff_id = ?', $userStorage->id),
                $QMember->getAdapter()->quoteInto('asm_staff_be_appraisal = ?', $data['to_staff']),
                $QMember->getAdapter()->quoteInto('asm_is_deleted = ?', 0),
            ])->toArray();
            if(count($member) == 0) {
                $QQuestion = new Application_Model_DynamicSurveyQuestion();
                $select = $QQuestion->select()
                    ->from($QQuestion, ['count(*) as count'])
                    ->where('fk_ds = ?', $surveyId)
                    ->where('dsq_is_deleted = ?', 0)
                    ->where('dsq_type in (?)', ['range', 'radio', 'select', 'checkbox']);
                $count = $QQuestion->fetchAll($select)[0]->count;

                $QResultDetail = new Application_Model_DynamicSurveyResultDetail();
                $QAnswer = new Application_Model_DynamicSurveyAnswer();
                $select = $QResultDetail->select()
                    ->from($QResultDetail, [])
                    ->join('dynamic_survey_answer', 'fk_dsa = dsa_id', ['sum(dsa_value) as total'])
                    ->where('fk_dsr = ?', $resultId)
                    ->where('fk_dsa is not null', null)
                    ->setIntegrityCheck(false);
                $total = $QResultDetail->fetchAll($select)->toArray()[0]['total'];

                $promote = $QQuestion->fetchAll(
                    $QQuestion->select()
                    ->from($QQuestion, [])
                    ->join('dynamic_survey_result_detail', 'dsq_id = fk_dsq',  ['dsrd_text as promote'])
                    ->where('dsq_type = ?', 'textarea')
                    ->where('dsq_title = ?', 'Điểm cần phát huy')
                    ->where('fk_ds = ?', $surveyId)
                    ->where('fk_dsr = ?', $resultId)
                    ->where('dsrd_text is not null', null)
                    ->setIntegrityCheck(false)
                )->toArray()[0]['promote'];

                $improve = $QQuestion->fetchAll(
                    $QQuestion->select()
                    ->from($QQuestion, [])
                    ->join('dynamic_survey_result_detail', 'dsq_id = fk_dsq', ['dsrd_text as improve'])
                    ->where('dsq_type = ?', 'textarea')
                    ->where('dsq_title = ?', 'Điểm cần cải thiện')
                    ->where('fk_ds = ?', $surveyId)
                    ->where('fk_dsr = ?', $resultId)
                    ->where('dsrd_text is not null', null)
                    ->setIntegrityCheck(false)
                )->toArray()[0]['improve'];

                $newMember = [
                    'asp_id' => $data['plan_id'],
                    'asm_staff_id' => $data['from_staff'],
                    'asm_staff_title' => $data['from_staff_title'],
                    'asm_staff_be_appraisal' => $data['to_staff'],
                    'asm_staff_be_appraisal_title' => $data['to_staff_title'],
                    'fk_dsr' => $resultId,
                    'asm_type' => $data['type_appraisal'],
                    'asm_total_point' => $total,
                    'asm_count_question' => $count,
                    'asm_promote' => str_replace('"', '\"', $promote),
                    'asm_improve' => str_replace('"', '\"', $improve),
                    'asm_created_at' => date('Y-m-d H:i:s'),
                    'asm_created_by' => $userStorage->id,
                    'asm_updated_at' => date('Y-m-d H:i:s'),
                    'asm_updated_by' => $userStorage->id,
                ];

                $QRegion = new Application_Model_Region();
                $QArea = new Application_Model_Area();
                if($data['from_staff_title'] == self::TITLE_ASM_ID) {
                    $newMember['asm_from_cache'] = $QArea->getAreaByAsm($data['from_staff']);
                }
                if($data['from_staff_title'] == self::TITLE_RSM_ID) {
                    $newMember['asm_from_cache'] = $QRegion->getRegionByRsm($data['from_staff']);
                }
                if($data['to_staff_title'] == self::TITLE_ASM_ID) {
                    $newMember['asm_to_cache'] = $QArea->getAreaByAsm($data['to_staff']);
                }
                if($data['to_staff_title'] == self::TITLE_RSM_ID) {
                    $newMember['asm_to_cache'] = $QRegion->getRegionByRsm($data['to_staff']);
                }
                $QMember->insert($newMember);
            }
        }
    }// done
    
    
}