<?php
class CapacityController extends My_Controller_Action
{
    public function init()
    {
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        
        if(!in_array($userStorage->id, [20915, 7564, 5899, 8807, 7, 6705, 22716, 14766]) AND $userStorage->team != 14 AND $userStorage->department != 156 AND $userStorage->department != 154){
            //exit;
        }
        
        $list_admmin = [6705, 11967, 26612];
        
        $is_admin = (in_array($userStorage->id, $list_admmin)) ? 1 : 0;
        $is_head = $QToDo->checkIsHead($userStorage->id);
        $is_staff = $QToDo->checkIsStaff($userStorage->id);
        
        if(in_array($userStorage->id, $list_admmin)){
            $level = 1;
        }
        else{
            if($is_head == 1){
                $level = 2;
            }
            elseif($is_staff == 1){
                $level = 3;
            }
            else{
                echo 'Error Priviledge';exit;
            }
        }
        
        $storage = [
            'staff_id' => $userStorage->id,
            'level'    => $level,
        ];
        
        
        $this->list_admmin = $list_admmin;
        $this->storage = $storage;
        
        $this->is_admin = $is_admin;
        $this->is_head = $is_head;
        $this->is_staff = $is_staff;
        
    }
    
    public function indexAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'index.php';
    }
    
    public function dictionaryAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'dictionary.php';
    }
    
    public function createDictionaryAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'create-dictionary.php';
    }
    
    public function viewDictionaryAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'view-dictionary.php';
    }
    
    public function createCapacityAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'create-capacity.php';
    }
    
    public function createCapacityDetailsAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'create-capacity-details.php';
    }
    
    public function createCapacityDetailsNewAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'create-capacity-details-new.php';
    }
    
    public function getFieldAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $title_id = $this->getRequest()->getParam('title_id');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $userStorage->id;
        $QCapacity = new Application_Model_Capacity();
        $QCapacityDictionary = new Application_Model_CapacityDictionary();
        $QCapacityDictionaryLevel = new Application_Model_CapacityDictionaryLevel();
        
        $fields = $QCapacity->getField($title_id);
        
        if($fields[0]['capacity_id']){
            $capacity = $QCapacity->fetchRow(['id = ?' => $fields[0]['capacity_id']])->toArray();
        }
        else{
            $capacity = [];
        }
        
        $dictionary = $QCapacity->getDictionary($title_id);
        $science = $QCapacity->getScience($title_id);
        $dictionary_science = $QCapacity->getDictionaryScience();
        
        $dictionary_all = $QCapacityDictionary->fetchAll(['is_deleted = ? OR is_deleted IS NULL' => 0], 'title ASC')->toArray();
        $dictionary_level_all = $QCapacityDictionaryLevel->fetchAll(null, 'level DESC')->toArray();
        
        echo json_encode([
            'title_id' => $title_id,
            'fields' => $fields,
            'dictionary' => $dictionary,
            'dictionary_all' => $dictionary_all,
            'dictionary_level_all' => $dictionary_level_all,
            'science'   => $science,
            'dictionary_science' => $dictionary_science,
            'capacity' => $capacity
        ]);
    }
    
    public function getFieldCapacityDetailsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $staff_id = $this->getRequest()->getParam('staff_id');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $userStorage->id;
        
        $QStaff = new Application_Model_Staff();
        $QCapacity = new Application_Model_Capacity();
        $QCapacityField = new Application_Model_CapacityField();
        $QCapacityFieldDictionary = new Application_Model_CapacityFieldDictionary();
        $QCapacityScience = new Application_Model_CapacityScience();
        $QCapacityDictionary = new Application_Model_CapacityDictionary();
        $QCapacityDictionaryLevel = new Application_Model_CapacityDictionaryLevel();
        
        $staff = $QStaff->fetchRow(['id = ?' => $staff_id]);
        
        $capacity_details = $QCapacity->fetchRow(['staff_id = ?' => $staff_id]);
        
        if(!$capacity_details){
            $title_id = $staff->title;
            
            $data = [
                'staff_id' => $staff_id,
                'is_deleted' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userId,
            ];
            $capacity_id = $QCapacity->insert($data);
            
            $fields = $QCapacity->getField($title_id);
            foreach($fields as $key=>$value){
                
                $data_field = [
                    'capacity_id' => $capacity_id,
                    'title' => $value['title'],
                    'kpi' => $value['kpi'],
                    'desc' => $value['desc'],
                    'ratio' => $value['ratio'],
                ];
                $field_id = $QCapacityField->insert($data_field);
                
                $dictionary = $QCapacityFieldDictionary->fetchAll(['field_id = ?' => $value['field_id'], 'is_deleted = ?' => 0]);
                
                
                foreach($dictionary as $k=>$v){
                    $data_dictionary = [
                        'field_id' => $field_id,
                        'dictionary_id' => $v['dictionary_id'],
                        'level' => $v['level'],
                        'share' => $v['share'],
                    ];
                    $QCapacityFieldDictionary->insert($data_dictionary);
                }
                
                $dictionary_science = $QCapacityScience->fetchAll(['capacity_field_id = ?' => $value['field_id'], 'is_deleted = ?' => 0]);
                
                foreach($dictionary_science as $k=>$v){
                    $data_science = [
                        'capacity_field_id' => $field_id,
                        'desc' => $v['desc'],
                        'share' => $v['share'],
                        'level' => $v['level'],
                    ];
                    $QCapacityScience->insert($data_science);
                }
                
            }
            
        }
            
        $fields = $QCapacity->getFieldByStaff($staff_id);

        $dictionary = $QCapacity->getDictionaryByStaff($staff_id);
        $science = $QCapacity->getScienceByStaff($staff_id);
        $dictionary_science = $QCapacity->getDictionaryScience();

        $dictionary_all = $QCapacityDictionary->fetchAll(['is_deleted = ? OR is_deleted IS NULL' => 0])->toArray();
        $dictionary_level_all = $QCapacityDictionaryLevel->fetchAll(null, 'level DESC')->toArray();

        echo json_encode([
            'title_id' => $title_id,
            'fields' => $fields,
            'dictionary' => $dictionary,
            'dictionary_all' => $dictionary_all,
            'dictionary_level_all' => $dictionary_level_all,
            'science'   => $science,
            'dictionary_science' => $dictionary_science,
            'is_capacity_details' => 1
        ]);
        
    }
    
    public function saveFieldAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $QCapacityField = new Application_Model_CapacityField();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            $title_id = $params['title_id'];
            $field_id = $params['field_id'];
            
            $data = [
                'title' => $params['title'],
                'desc' => $params['desc'],
                'kpi' => $params['kpi'],
                'ratio' => $params['ratio'],
            ];
            
            if(!empty($field_id)){
                $QCapacityField->update($data, ['id = ?' => $field_id]);
            }
            else{
                $capacity = $QCapacity->fetchRow(['title_id = ?' => $title_id]);
            
                if($capacity){
                    $capacity_id = $capacity['id'];
                    
                    $data['capacity_id'] = $capacity_id;
                    $field_id = $QCapacityField->insert($data);
                }
                else{
                    $capacity_id = $QCapacity->insert([
                        'title_id' => $title_id, 
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id
                    ]);
                    
                    $data['capacity_id'] = $capacity_id;
                    $field_id = $QCapacityField->insert($data);
                }
            }
            
            echo json_encode([
                'status' => 0,
                'field_id' => $field_id
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function saveFieldCapacityDetailsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $QCapacityField = new Application_Model_CapacityField();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            $staff_id = $params['staff_id'];
            $field_id = $params['field_id'];
            
            $data = [
                'title' => $params['title'],
                'kpi' => $params['kpi'],
                'desc' => $params['desc'],
                'ratio' => $params['ratio'],
            ];
            
            if(!empty($field_id)){
                $QCapacityField->update($data, ['id = ?' => $field_id]);
            }
            else{
                $capacity = $QCapacity->fetchRow(['staff_id = ?' => $staff_id]);
            
                if($capacity){
                    $capacity_id = $capacity['id'];
                    
                    $data['capacity_id'] = $capacity_id;
                    $field_id = $QCapacityField->insert($data);
                }
                else{
                    $capacity_id = $QCapacity->insert([
                        'staff_id' => $staff_id, 
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userStorage->id
                    ]);
                    
                    $data['capacity_id'] = $capacity_id;
                    $field_id = $QCapacityField->insert($data);
                }
            }
            
            echo json_encode([
                'status' => 0,
                'field_id' => $field_id
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function deleteFieldAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $QCapacityField = new Application_Model_CapacityField();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            $field_id = $params['field_id'];
            
            $data = [
                'is_deleted' => 1,
            ];
            
            if(!empty($field_id)){
                $QCapacityField->update($data, ['id = ?' => $field_id]);
            }
            
            echo json_encode([
                'status' => 0,
                'field_id' => $field_id
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function saveDictionaryAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $QCapacityFieldDictionary = new Application_Model_CapacityFieldDictionary();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            $data = [
                'field_id' => $params['field_id'],
                'dictionary_id' => $params['dictionary_id'],
                'level' => $params['level'],
                'share' => $params['share'],
            ];
            
            if(!empty($params['dictionary_field_id'])){
                $field_dictionary_id = $params['dictionary_field_id'];
                $QCapacityFieldDictionary->update($data, ['id = ?' => $field_dictionary_id]);
            }
            else{
                $field_dictionary_id = $QCapacityFieldDictionary->insert($data);
            }
            
            
            
            echo json_encode([
                'status' => 0,
                'field_dictionary_id' => $field_dictionary_id
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function saveScienceAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $QCapacityScience = new Application_Model_CapacityScience();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            $data = [
                'capacity_field_id' => $params['field_id'],
                'desc' => $params['desc'],
                'share' => $params['share'],
                'level' => $params['level'],
            ];
            
            if(!empty($params['capacity_science_id'])){
                $capacity_science_id = $params['capacity_science_id'];
                $QCapacityScience->update($data, ['id = ?' => $capacity_science_id]);
            }
            else{
                $capacity_science_id = $QCapacityScience->insert($data);
            }
            
            
            echo json_encode([
                'status' => 0,
                'capacity_science_id' => $capacity_science_id
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function deleteDictionaryAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $QCapacityFieldDictionary = new Application_Model_CapacityFieldDictionary();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            if(!empty($params['dictionary_field_id'])){
                $data = [ 
                    'is_deleted' => 1,
                ];
                $where = NULL;
                $where = $QCapacityFieldDictionary->getAdapter()->quoteInto('id = ?', $params['dictionary_field_id']);
                $QCapacityFieldDictionary->update($data, $where);
                
                echo json_encode([
                    'status' => 0,
                    'dictionary_field_id' => $params['dictionary_field_id']
                ]);
                
            }
            else{
                echo json_encode([
                    'status' => 1,
                    'message' => 'Empty dictionary_field_id'
                ]);
            }
            
            
            
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function listCapacityAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'list-capacity.php';
    }
    
    public function listStaffAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'list-staff.php';
    }
    
    public function appraisalAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'appraisal.php';
    }
    
    public function appraisalNewAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'appraisal-new.php';
    }
    
    public function appraisalStaffAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'appraisal-staff.php';
    }
    
    public function getCapacityAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        $capacity_id = $params['capacity_id'];
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            if(!empty($capacity_id)){
                
                $capacity = $QCapacity->getInfoCapacity($capacity_id);
                
                $capacity_level = $QCapacity->getListLevel($capacity_id);
                
                $capacity_science = $QCapacity->getListScience($capacity_id);
                
                echo json_encode([
                    'status' => 0,
                    'capacity' => $capacity,
                    'capacity_level' => $capacity_level,
                    'capacity_science' => $capacity_science
                ]);
                
            }
            else{
                echo json_encode([
                    'status' => 1,
                    'message' => 'Empty dictionary_field_id'
                ]);
            }
            
            
            
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function getAppraisalCapacityAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        $staff_id = $params['staff_id'];
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            
            if(!empty($staff_id)){
                
                $capacity_staff = $QCapacity->getInfoStaffCapacity($staff_id);
                $staff = $QCapacity->getInfoStaff($staff_id);
                
                echo json_encode([
                    'status' => 0,
                    'capacity_staff' => $capacity_staff,
                    'staff' => $staff
                ]);
                
            }
            else{
                echo json_encode([
                    'status' => 1,
                    'message' => 'Empty dictionary_field_id'
                ]);
            }
            
            
            
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    public function getDictionaryAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'get-dictionary.php';
    }
    
    public function saveAppraisalAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'save-appraisal.php';
    }
    
    public function preAppraisalStaffAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'pre-appraisal-staff.php';
    }
    
    public function preAppraisalAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'pre-appraisal.php';
    }
    
    public function preStaffCapacityAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'pre-staff-capacity.php';
    }
    
    public function saveAppraisalStaffAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'save-appraisal-staff.php';
    }
    
    public function staffCapacityAction(){
        //Xem kết quả trưởng bộ phận đánh giá
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'staff-capacity.php';
    }
    
    public function resultStaffCapacityAction(){
        //Xem kết quả tự đánh giá nhân viên
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'result-staff-capacity.php';
    }
    
    public function staffViewCapacityAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'staff-view-capacity.php';
    }
    
    public function viewCapacityAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'view-capacity.php';
    }
    
    public function appraisalResultAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'appraisal-result.php';
    }
    
    public function appraisalStaffResultAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'appraisal-staff-result.php';
    }
    
    public function resultAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'result.php';
    }
    
    public function saveAppraisalNewAction(){
        require_once 'capacity' . DIRECTORY_SEPARATOR . 'save-appraisal-new.php';
    }
    
    public function lockCapacityAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if (!$request->isPost()) {
            echo json_encode([
                'status' => 1,
                'message' => 'Wrong method submitted!'
            ]);
            return;
        }

        $params = $this->getRequest()->getParams();
        
        try {
            
            $QCapacity = new Application_Model_Capacity();
            
            $capacity_id = $params['capacity_id'];
            $is_lock = $params['is_lock'];
            
            $data = [
                'is_lock' => (int)$is_lock,
            ];
            
            if(!empty($capacity_id)){
                $QCapacity->update($data, ['id = ?' => $capacity_id]);
            }
            
            echo json_encode([
                'status' => 0,
                'capacity_id' => $capacity_id
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }
    
    
}
