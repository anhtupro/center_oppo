<?php

class ContracteditController extends My_Controller_Action
{
	
    public function indexAction()
    {
    	//
    }
    public function listAction(){
       $contract                 = new Application_Model_Contract();
       $dataContract             = $contract->listAll();
       $this->view->listContract = $dataContract;
    }
    public function editAction(){
       $contract                = new Application_Model_Contract();
       $id_contract             = $contract->findPrimaykey($this->getParam('id'));
       $staff_id                = $id_contract->staff_id; 
       $parent                  = $contract->getParentContract($staff_id);
       $this->view->parent      = $parent;  
       $this->view->contract    = $contract->findPrimaykey($this->getParam('id'));
       $dataContract            = $contract->listNewContract();
       $this->view->newContract = $dataContract;
       $dataTeam                = $contract->listTeam();
       $this->view->team        = $dataTeam;    
       $dataCompany             = $contract->listCompany();
       $this->view->company     = $dataCompany;
     }
    public function processeditAction(){
        //contract
        $contract = new Application_Model_Contract();
        $edit = $contract->findPrimaykey($this->getParam('id'));
        if($this->_request->isPost()){
          $params = array_merge(
            array(
              'new_contract'       => null,
              'from_date'          => null,
              'to_date'            => null,
              'status'             => null,
              'title'              => null,
              'print_type'         => null,
              'print_status'       => null,
              'salary'             => null,
              'total_salary'       => null,
              'kpi_text'           => null,
              'probation_salary'   => null,
              'allowance_1'        => null,
              'allowance_2'        => null,
              'allowance_3'        => null,
              'kpi_text_probation' => null,
              'parent_id'          => null,
              'company_id'          => null,
              'work_cost'          => null,
              'staff_id'           => null
            ),
            $this->_request->getParams()
          );
          $params1 = array_merge(
            array(
              'contract_signed_at'  => null,
              'contract_expired_at' => null,
              'contract_term'       => null,
            ),
            $this->_request->getParams()
          );
         //staff
         $id_staff                        = $edit->staff_id ;
         $staff                           = new Application_Model_Staff();
         $edit_staff                      = $staff->findStaffid($id_staff);
         //contract
         $fromDate                        = DateTime::createFromFormat('d/m/Y', $params['from_date']);
         $fromDate                        = $fromDate ? $fromDate->format('Y-m-d 00:00:00') : null;
         $toDate                          = DateTime::createFromFormat('d/m/Y', $params['to_date']);
         $toDate                          = $toDate ? $toDate->format('Y-m-d 00:00:00') : null;
         $print_type                      = $params['print_type'];
         $edit->new_contract              = $params['new_contract'];  
         $edit->from_date                 = $fromDate;
         $edit->to_date                   = $toDate;
         $edit->status                    = $params['status'];
         $edit->title                     = $params['title'];
         $edit->print_type                = $print_type;
         $edit->print_status              = $params['print_status'];
         $edit->salary                    = $params['salary'] ? $params['salary'] : null;
         $edit->total_salary              = $params['salary_total'] ? $params['salary_total'] : null;
         $edit->kpi_text                  = $params['kpi_text'];
         $edit->probation_salary          = $params['probation_salary'] ? $params['probation_salary'] : null;
         $edit->allowance_1               = $params['allowance_1'] ? $params['allowance_1'] :null;
         $edit->allowance_2               = $params['allowance_2']? $params['allowance_2'] : null;
         $edit->allowance_3               = $params['allowance_3'] ? $params['allowance_3'] :null;
         $edit->kpi_text_probation        = $params['kpi_text_probation'];
         $edit->parent_id                 = $params['parent_id'];
         $edit->company_id                 = $params['company_id'];
         $edit->work_cost                 = $params['work_cost'] ? $params['work_cost'] : null;
         $now                             = time(); 
         $time_from_date                  = strtotime($fromDate); 
         $time_to_date                    = strtotime($toDate); 
         $edit_staff->contract_signed_at  = $fromDate;
         $edit_staff->contract_expired_at = $toDate;
         $edit_staff->contract_term       = $params['new_contract'];
         $edit->save($params);
         if($time_from_date <= $now && $now <= $time_to_date && $print_type ==1)
         {
           $edit_staff->save($params1);
           $this->redirect('contract/staff-contract');
         }
        else
            $this->redirect('contract/staff-contract');
         }
    }
}

