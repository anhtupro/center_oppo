<?php
class TrainerController extends My_Controller_Action
{
    public function editProfileAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'edit-profile.php';
    }
    /*list-pg*/
    public function indexAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'index.php';
    }

    /*mangage point pg*/
    public function pointAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'point.php';
    }

    public function rewardWarningAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'reward-warning.php';
    }

    /*Del history reward warning with ajax*/
    public function delHistoryAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR. 'del-history.php';
    }

    public function trainingReportAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'training-report.php';
    }

    public function listTrainingReportAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'list-training-report.php';
    }

    public function listEventReportAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'list-event-report.php';
    }

    /*ajax get shop*/
    public function getShopAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->getMethod()=='POST')
        {
            $dealer_id      = $this->getRequest()->getParam('dealer_id');
            $QStore         = new Application_Model_Store();
            $whereStore     = array();
            $whereStore[]   = $QStore->getAdapter()->quoteInto('d_id = ?',$dealer_id);
            $whereStore[]   = $QStore->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
            $rows           = $QStore->fetchAll($whereStore);

            $arrayShop = array();
            if($rows->count())
            {
                foreach($rows as $key => $value)
                {
                    $arrayShop[$value['id']] = $value['name'];
                }
            }
            echo json_encode($arrayShop);
        }
    }

    public function delTrainingReportAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'del-training-report.php';
    }

    public function eventReportAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'event-report.php';
    }

    public function delEventReportAction(){

        require_once 'trainer' . DIRECTORY_SEPARATOR . 'del-event-report.php';
    }

    public function listTeamBuildingReportAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'list-team-building-report.php';
    }

    public function teamBuildingReportAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'team-building-report.php';
    }

    public function delTeamBuildingReportAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'del-team-building-report.php';
    }

    /* ajax get province staff */
    public function getProvinceAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->getMethod()=='POST')
        {
            $cache = Zend_Registry::get('cache');
            $cache->remove('asm_cache');
            $cache->remove('regional_market_cache');

            $staff_id             = $this->getRequest()->getParam('staff_id');
            $area                 = $this->getRequest()->getParam('area');
            $QStaffTrainer        = new Application_Model_StaffTrainer();
            $QRegionalMarket      =  new Application_Model_RegionalMarket();
            $getProvinceArea      = $QRegionalMarket->get_region_cache($area);
            $cachedRegionalMarket = $QRegionalMarket->get_cache();

            $result = array();

            if($staff_id)
            {
                $AreaTrainer     = $QStaffTrainer->getAreaTrainer($staff_id);

                $province        = array_keys($AreaTrainer['province']);

                $array_intersect = array_intersect($province,$getProvinceArea);

                foreach($array_intersect as $item )
                {
                    $result[$item] = $cachedRegionalMarket[$item];
                }

                echo json_encode($result);
            }
        }
    }

    /*ajax get all info PG or SALE*/
    public function getInfoPgSaleAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'get-info-pg-sale.php';

    }

    public function typeAssetAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'type-asset.php';
    }

    /*function save asset*/
    public function saveAssetAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-asset.php';

    }

    /*function del type asset*/
    public function delAssetAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'del-asset.php';
    }

    /*function create input order*/
    public function createInputAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'create-input.php';
    }

    /*function save order*/
    public function saveOrderAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-order.php';
    }

    /*function order*/
    public function ordersAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'orders.php';
    }

    /*function view order*/
    public function viewOrderAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'view-order.php';
    }

    /*function confirm order*/
    public function confirmOrderAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'confirm-order.php';
    }

    /*do confirm order*/
    public function doConfirmOrderAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'do-confirm-order.php';
    }

    /*del order*/
    public function delOrderAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'del-order.php';
    }

    /*function inventory asset*/
    public function inventoryAssetAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'inventory-asset.php';
    }

    /*function create output order*/
    public function createOutputAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'create-output.php';
    }

    /*function save order out*/
    public function saveOrderOutAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-order-out.php';
    }

    /*function list order out*/
    public function ordersOutAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'orders-out.php';
    }

    /*function view order*/
    public function viewOrderOutAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'view-order-out.php';
    }

    /*function confirm order out*/
    public function confirmOrderOutAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'confirm-order-out.php';
    }

    /*function do confirm order out*/
    public function doConfirmOrderOutAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'do-confirm-order-out.php';
    }

    /*function del order out */
    public function delOrderOutAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'del-order-out.php';
    }

    public function inventoryHistoryAssetAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'inventory-history-asset.php';
    }

    /* ajax view training report */
    public function getInfoTrainingReportAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'get-info-training-report.php';
    }

    /* ajax view event report */
    public function getInfoEventReportAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'get-info-event-report.php';
        
    }

    /* ajax view team building report */
    public function getInfoTeamBuildingReportAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $userStorage        = Zend_Auth::getInstance()->getStorage()->read();
        if($this->getRequest()->getMethod() == 'POST')
        {
            $id = $this->getRequest()->getParam('id');

            $QStaffTeamBuildingReport     = new Application_Model_StaffTeamBuildingReport();
            $whereStaffTeamBuildingReport = $QStaffTeamBuildingReport->getAdapter()->quoteInto('id = ?', $id);
            $row                          = $QStaffTeamBuildingReport->fetchRow($whereStaffTeamBuildingReport);
            if($row)
            {
                // date - description - note - picture

                $data = '<table class="table table-responsive">';
                // date
                $date = $row['date'];
                $data = $data.'<tr><td>Date:</td><td>'.$date.'</td></tr>';
                // description
                $description   = $row['description'];
                $data = $data.'<tr><td>Description:</td><td>'.$description.'</td></tr>';
                // note
                $note = $row['note'];
                $data = $data.'<tr><td>Note:</td><td>'.$note.'</td></tr>';
                // picture
                $allPicture           = json_decode($row['picture'],true);
                $direct               = HOST.'public/photo/trainer/'.$allPicture['user_id'].'/'.$allPicture['direct'].'/';
                $data = $data.'<tr><td colspan="2" style="text-align:center"><b>Picture</b></td></tr>';
                $data = $data.'<tr><td colspan="2">';
                foreach($allPicture as $key => $value)
                {
                    if($key == 'direct' || $key =='user_id')
                    {
                        continue;
                    }
                    else if ($value)
                    {
                        $data = $data.'<a data-featherlight="image" href="'.$direct.$value.'"><img src="'.$direct.$value.'" class="img-rounded" style="width:80px; height:80px; padding:10px;"/></a>';
                    }
                }
                $data = $data.'</td></tr>';
                $data = $data.'</table>';

                echo $data;

            }

        }
    }

    /* function save new staff --- PG - for time check in pg  */
    public function saveNewPgAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-new-pg.php';
    }

    /* function for view list new staff for time check in training */
    public function listNewStaffAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'list-new-staff.php';
    }

    /* Ajax get info new staff */
    public function getInfoNewStaffAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if($this->getRequest()->getMethod() == 'POST')
        {
            $id                 = $this->getRequest()->getParam('id');
            $QStaffTraining     = new Application_Model_StaffTraining();
            $whereStaffTraining = $QStaffTraining->getAdapter()->quoteInto('id = ?',$id);
            $row                = $QStaffTraining->fetchRow($whereStaffTraining);
            $result             = array();
            if($row)
            {
               foreach($row as $key => $value)
               {
                   $result[$key] = $value;
               }
               echo json_encode($result);
            }

        }
    }

    public function deleteNewStaffAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'delete-new-staff.php';
    }

    /* Tạo Khóa Học mà trainer tạo */
    public function courseAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'course.php';
    }

    /*  Save Course */
    public function saveCourseAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-course.php';
    }

    /* ajax get info course */
    public function getInfoCourseAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if($this->getRequest()->getMethod() == 'POST')
        {
            $id                 = $this->getRequest()->getParam('id');
            $QTrainerCourse     = new Application_Model_TrainerCourse();
            $whereTrainerCourse = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$id);
            $row                = $QTrainerCourse->fetchRow($whereTrainerCourse);
            $result             = array();
            if($row)
            {
                foreach($row as $key => $value)
                {
                    $result[$key] = $value;
                }
                echo json_encode($result);
            }

        }
    }

    /* delete course  */
    public function deleteCourseAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'delete-course.php';
    }

    /* Course detail - assgin staff and check in */
    public  function  courseDetailAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'course-detail.php';
    }

    public function saveCourseDetailAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-course-detail.php';
    }

    /* add timing check in for pg pb  */
    public function addTimeAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'add-time.php';
    }

    /*  Ajax get list staff assign course  */
    public function getStaffCourseDetailAction()
    {
        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender();

        if($this->getRequest()->getMethod()=='POST')
        {
            $date           = $this->getRequest()->getParam('date');

            $course_id      = $this->getRequest()->getParam('course_id');
            
            // dau tien minh lay nhung thang da check in
            $QTrainerCourseTiming = new Application_Model_TrainerCourseTiming();

            $listStaffCheckin     = $QTrainerCourseTiming->getStaffCheckin($date,$course_id);
            // ta co danh sach nhan vien da check roi 
            // lay danh sach nhan vien da dang ki 
            $QTrainerCourseDetail = new Application_Model_TrainerCourseDetail();

            $listStaffAssign      = $QTrainerCourseDetail->getStaffAssign($course_id); 

            $arrayStaffAssign     = $listStaffAssign['ids'];

            $arrayDiffStaffAssign = array_diff($arrayStaffAssign,$listStaffCheckin); 

            $staffNotCheckIn      = $QTrainerCourseDetail->getInfoStaffAssign($arrayDiffStaffAssign);

            $arrayHaveCheckIn     = $QTrainerCourseDetail->getInfoStaffAssign($listStaffCheckin);

            echo json_encode(array('not_check_in'=>$staffNotCheckIn,'check_in'=>$arrayHaveCheckIn));
        }
    }

    /* save add timing check in for pg pb  */
    public function saveAddTimeAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-add-time.php';
    }

    public function checkIdNumberAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'check-id-number.php';
    }

    public function reportTrainerCourseAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'report-trainer-course.php';
    }

    public function reportTrainerMonthAction()
    {
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'report-trainer-month.php';
    }

    /*  ajax get course */
    public function getCourseAction()
    {

        $QTrainerCourseType = new Application_Model_TrainerCourseType();
        $trainer_course_type = $QTrainerCourseType->get_cache();

        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender();

        if($this->getRequest()->getMethod()=='POST')
        {
            $date           = $this->getRequest()->getParam('date');

            $QTrainerCourse = new Application_Model_TrainerCourse();

            $listCourse     = $QTrainerCourse->getCourse(null,$date);

            $arrayResult    = array();

            if( count($listCourse))
            {
                foreach ($listCourse as $key => $value) {
                    $arrayResult[$value['id']] = $trainer_course_type[$value['type']]." - ".$value['note'];
                }
            } 

            echo json_encode($arrayResult);          
            
        }
    }

    /* function ajax check course is lock*/
    function checkLockDataCourseAction()
    {
        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender();

        if($this->getRequest()->getMethod()=='POST')
        {
            $course_id                = $this->getRequest()->getParam('course_id');

            $QTrainerCourse           = new Application_Model_TrainerCourse();

            $result                   = 0;

            if(isset($course_id) and $course_id)
            {
                $whereTrainerCourse = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$course_id);
                $checked            = $QTrainerCourse->fetchRow($whereTrainerCourse);
                if($checked)
                {
                    $result      = $checked['locked'];
                }
                
            }

            echo $result;
        }
    }

    /*** function ajax use for report course **/
    function autoLockCourseAction()
    {
        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender();

        $userStorage        = Zend_Auth::getInstance()->getStorage()->read();
        $QLog               = new Application_Model_Log();
        $ip                 = $this->getRequest()->getServer('REMOTE_ADDR');

        if($this->getRequest()->getMethod()=='POST')
        {
            $course_id                = $this->getRequest()->getParam('id');

            $QTrainerCourse           = new Application_Model_TrainerCourse();

            $result                   = 0;

            if(isset($course_id) and $course_id)
            {
                $whereTrainerCourse = $QTrainerCourse->getAdapter()->quoteInto('id = ?',$course_id);
                $checked            = $QTrainerCourse->fetchRow($whereTrainerCourse);
                if($checked)
                {
                    $locked = $checked['locked'];

                    if($locked != 1)
                    {
                        // bat dau update khoa hoc do 
                        $dataUpdate = array(
                            'locked'    => 1,
                            'locked_at' => date('Y-m-d H:i:s'),
                            'locked_by' => $userStorage->id
                            );

                        $QTrainerCourse->update($dataUpdate,$whereTrainerCourse);
                        // to do log
                        $info = array('UPDATE LOCKED COURSE','new'=>$dataUpdate,'old'=>$checked);
                        $QLog->insert( array (
                            'info'          => json_encode($info),
                            'user_id'       => $userStorage->id,
                            'ip_address'    => $ip,
                            'time'          => date('Y-m-d H:i:s'),
                        ) );

                        $result = 1;
                    }
                }
                
            }

            echo $result;
        }
    }

    function createTypeAssetAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'create-type-asset.php';
        $id = $this->getRequest()->getParam('id');
        if($id){
            $this->view->id = $id;
        }
    }

    public function checkImeiAction(){
        $asset_id = $this->getRequest()->getParam('asset_id');
        $imei = $this->getRequest()->getParam('imei');
        $QTrainerOrder = new Application_Model_TrainerOrder();
        $asset_id = intval($asset_id);
        $imei = trim($imei);
        $result = $QTrainerOrder->checkImei($imei,$asset_id);
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->json->sendJson($result);
        exit;
    }

    public function inventoryAssetDetailAction(){
        $asset_id = $this->getRequest()->getParam('asset_id');
        $QTrainerInventory = new Application_Model_TrainerInventoryAsset();
        $params = array(
            'asset_id' => $asset_id,
            'available_stock' => 0
        );
        $list = $QTrainerInventory->getInventoryImei($params);
        $this->view->list = $list;
    }

    public function listTrainerConfirmedAction(){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QTrainerOrderOut = new Application_Model_TrainerOrderOut();
        $list = $QTrainerOrderOut->getOrderConfirmByTrainer($userStorage->id,1);
        $this->view->list = $list;
    }

    public function trainerConfirmAction(){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QTrainerOrderOut = new Application_Model_TrainerOrderOut();
        $list_pending  = $QTrainerOrderOut->getOrderConfirmByTrainer($userStorage->id,0);
        $this->view->list_pending = $list_pending;
    }

    public function saveTrainerConfirmAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-trainer-confirm.php';
    }

    public function listInventoryByStaffAction(){
        $name = $this->getRequest()->getParam('name');
        $export = $this->getRequest()->getParam('export');

        $QTrainerInventory = new Application_Model_TrainerInventory();

        $params = array(
            'name'  => $name
        );
        $data = $QTrainerInventory->getInventoryByStaff($params);

        if($export == 1){
            $data_ex = $QTrainerInventory->getAllInventory($params);
            $this->_exportList($data_ex, $params);
        }

        $this->view->params = $params;
        $this->view->data = $data;
    }

    public function listInventoryByAssetAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'list-inventory-by-asset.php';
    }

    //ajax
    public function getInventoryByUserAction(){
        $this->_helper->layout()->disableLayout(true);
        $staff_id = $this->getRequest()->getParam('staff_id');
        $type_id = $this->getRequest()->getParam('type_id');
        $QInventory = new Application_Model_TrainerInventory();

        $params = array(
            'staff_id'      => $staff_id,
            'type_id'    => $type_id
        );

        if($type_id == 1){
            $list  = $QInventory->fetchInventoryDetailsByUser($params);
        }
        elseif($type_id == 2){
            $list  = $QInventory->fetchCurrentDetailsByUser($params);
        }
        
        $this->view->type_id = $type_id;
        $this->view->list = $list;

    }

    public function getInventoryByAssetAction(){
        $this->_helper->layout()->disableLayout(true);
        $asset_id = $this->getRequest()->getParam('asset_id');
        $type_id = $this->getRequest()->getParam('type_id');
        $QInventory = new Application_Model_TrainerInventory();

        $params = array(
            'asset_id'      => $asset_id,
            'type_id'    => $type_id
        );

        if($type_id == 1){
            $list  = $QInventory->fetchInventoryDetailsByAsset($params);
        }
        elseif($type_id == 2){
            $list  = $QInventory->fetchCurrentDetailsByAsset($params);
        }
        
        $this->view->type_id = $type_id;
        $this->view->list = $list;

    }

    public function trainerTakeAssetAction(){

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QInventory = new Application_Model_TrainerInventory();

        $params = array(
            'trainer_id' => $userStorage->id
        );

        $list  = $QInventory->fetchInventoryByUser($params);
        $this->view->list = $list;

        $QTrainerOrder = new Application_Model_TrainerOrder();
        $list_pending  = $QTrainerOrder->getOrderConfirmByTrainer($userStorage->id,0);
        $this->view->list_pending = $list_pending;

        $QTrainerOrderOut  = new Application_Model_TrainerOrderOut();
        $this->view->list_trainer = $QTrainerOrderOut->getStaffTrainer();

        $flashMessenger         = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;
        $this->view->messages_error = $messages_error;
    }

    private function _exportListTrainingReport($data,$params){
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'stt'                   =>'STT',
            'type'                  =>'TYPE',
            'date'                  =>'DATE',
            'area'                  =>'AREA',
            'province'              =>'PROVINCE',
            'dealer'                =>'DEALER',
            'store'                 =>'STORE',
            'sell_out_before'       =>'SELL OUT BEFORE',
            'sell_out_after'        =>'SELL OUT AFTER',
            'from_date'             =>'FROM DATE',
            'to_date'               =>'TO DATE',
            'quantity_participant'  =>'quantity participant',
            'quantity_disqualified' =>'quantity disqualified',
            'quantity_remain'       =>'quantity remain',
            'quantity_trainees'     =>'quantity trainees',
            'note'                  =>'NOTE',
            );

        if(isset($params['type']) AND $params['type'] == 2){
            unset($heads['province']);
            unset($heads['from_date']);
            unset($heads['to_date']);
            unset($heads['quantity_participant']);
            unset($heads['quantity_disqualified']);
            unset($heads['quantity_remain']);
        }

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        $types = unserialize(TYPE_TRAINING_REPORT);
        $QArea                       = new Application_Model_Area();
        $cachedArea      = $QArea->get_cache();

        $QRegionalMarket                  = new Application_Model_RegionalMarket();
        $cachedRegionalMarket = $QRegionalMarket->get_cache();

        $QStore                      = new Application_Model_Store();
        $cachedStore     = $QStore->get_cache();

        try
        {
            if ($data)
            {
                $intCount = 1;
                foreach ($data as $_key => $item)
                {
                    $type     = ( isset($types[$item['type']])) ? $types[$item['type']] : '';
                    $date     = ( isset($item['date']) ) ? date('d/m/Y',strtotime($item['date'])) : '';
                    $area     = ( isset($cachedArea[$item['area']]) ) ? $cachedArea[$item['area']] : '';
                    $province = ( isset($cachedRegionalMarket[$item['province']])) ? $cachedRegionalMarket[$item['province']] : '';
                    $dealer   = ( isset($item['dealer_name']) ) ? $item['dealer_name'] : '';
                    $store    = ( isset($cachedStore[$item['store_id']])) ? $cachedStore[$item['store_id']] : '';

                    if(isset($item['sellout']['before'])){
                        $sellout_before = $item['sellout']['before'];
                    }else{ 
                        $sellout_before = '';
                    }

                    if(isset($item['sellout']['after'])){
                        $sellout_after = $item['sellout']['after'];
                    }else{ 
                        $sellout_after = '';
                    }

                    $from_date             = (isset( $item['from_date']) and ( $item['from_date'])) ? date('d/m/Y',strtotime($item['from_date'])) : '';
                    $to_date               = (isset( $item['to_date']) and ( $item['to_date'])) ? date('d/m/Y',strtotime($item['to_date'])) : '';
                    $quantity_participant  = (isset( $item['quantity_participant']) and ( $item['quantity_participant']))? $item['quantity_participant'] :'';
                    $quantity_disqualified = (isset( $item['quantity_disqualified']) and ( $item['quantity_disqualified']))? $item['quantity_disqualified'] :'';
                    $quantity_remain       = (isset( $item['quantity_remain']) and ( $item['quantity_remain']))? $item['quantity_remain'] :'';    
                    $quantity_trainees     = (isset( $item['quantity_trainees']) and ( $item['quantity_trainees']))? $item['quantity_trainees'] :'';    
                    $note                  = (isset( $item['note']) and ( $item['note'])) ? $item['note'] : '';

                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($type,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($date,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($area,PHPExcel_Cell_DataType::TYPE_STRING);

                    if(isset($params['type']) AND $params['type'] != 2){
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($province,PHPExcel_Cell_DataType::TYPE_STRING);
                    }

                    $sheet->getCell($alpha++ . $index)->setValueExplicit($dealer,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($store,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($sellout_before,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($sellout_after,PHPExcel_Cell_DataType::TYPE_STRING);

                    if(isset($params['type']) AND $params['type'] != 2){
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($from_date,PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($to_date,PHPExcel_Cell_DataType::TYPE_STRING);    
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($quantity_participant,PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($quantity_disqualified,PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getCell($alpha++ . $index)->setValueExplicit($quantity_remain,PHPExcel_Cell_DataType::TYPE_STRING);
                    }    
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($quantity_trainees,PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($note,PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
                }
            }
        }
        catch (exception $e)
        {

        }

        $filename = ' Training report ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function getCourseByCodeAction(){
        $id_number  = $this->getRequest()->getParam('id_number');
        $code       = $this->getRequest()->getParam('code');
        $from_date = $this->getRequest()->getParam('from_date');
        $to_date = $this->getRequest()->getParam('to_date');
        $code       = trim($code);
        $id_number  = trim($id_number);
        $db = Zend_Registry::get('db');
        $id_number_tmp = $id_number;


        if($code){
            $select = $db->select()
                ->from(array('a'=>'staff'),array('a.*'))
                ->where('a.code = ?',$code);
            $row = $db->fetchRow($select);
            if($row){
                $id_number_tmp = $row['ID_number'];
            }
        }

        $selectCourse = $db->select()
            ->from(array('a'=>'trainer_course_detail'),array('b.id','b.name','f.title', 'b.note'))
            ->join(array('b'=>'trainer_course'),'a.course_id = b.id',array())
            ->joinLeft(array('c'=>'staff'),'c.id = a.staff_id',array())
            ->joinLeft(array('d'=>'staff_training'),'d.id = a.new_staff_id',array())
            ->joinLeft(array('f' => 'trainer_course_type'), 'b.type = f.id',array())
            ->where('b.del IS NULL')
            ->group('a.course_id')
            ->order('a.course_id DESC');

        if($id_number_tmp){
            $selectCourse->where('c.ID_number = ? OR d.cmnd = ?',$id_number_tmp);
        }

        if(isset($from_date) AND $from_date AND isset($to_date) AND $to_date ){
            $selectCourse->where(" (b.from_date >= '$from_date' AND b.to_date <= '$to_date')
                OR ('$from_date' BETWEEN b.from_date AND b.to_date)
                OR ('$to_date' BETWEEN b.from_date AND b.to_date)
                ");
        }

        $result = $db->fetchAll($selectCourse);
        $this->_helper->json->sendJson($result);
        $this->_helper->layout()->disableLayout(true);
        exit;
    }

    public function courseFinishedAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'couse-finished.php';
    }

    public function unlockCourseAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'unlock-course.php';
    }

    public function  viewTimeAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'view-timing.php';
    }

    public function listNotConfirmAction(){
        $page = $this->getRequest()->getParam('page',1);
        $limit = NULL;
        $params = array(
            'sort' => 'staff_name',
            'desc' => '0'
        );
        $QTrainerInventory = new Application_Model_TrainerInventoryAsset();
        $list = $QTrainerInventory->fetchNotConfirm($page,$limit,$total,$params);
        $this->view->list = $list;
    }

    public function trainerAreaAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'trainer-area.php';
    }

    public function trainerAreaEditAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'trainer-area-edit.php';
    }

    public function trainerAreaSaveAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'trainer-area-save.php';
    }


    private function _exportEventReport($data){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'export-list-event-report.php';
    }

    private function _exportListByImei($data){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'export-list-by-imei.php';   
        
    }

    public function createProductAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'create-product.php';
    }

    public function listProductAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'list-product.php';
    }

    public function saveProductAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-product.php';
    }

    public function delProductAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'del-product.php';
    }

    public function createOrderAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'create-order.php';
    }

    public function updateRewardWarningAction(){
        $warning_type               = $this->getRequest()->getParam('warning_type');
        $staff_id                   = $this->getRequest()->getParam('staff_id');
        $staff_reward_warning_id    = $this->getRequest()->getParam('staff_reward_warning_id');

        $QStaffRewardWarning = new Application_Model_StaffRewardWarning();

        $where   = [];
        $where[] = $QStaffRewardWarning->getAdapter()->quoteInto('id = ?', $staff_reward_warning_id);
        $where[] = $QStaffRewardWarning->getAdapter()->quoteInto('staff_id = ?', $staff_id);

        $staff_reward_warning = $QStaffRewardWarning->fetchRow($where);

        $flashMessenger              = $this->_helper->flashMessenger;
        
        if($staff_reward_warning){

            $data = [
                'warning_type' => $warning_type
            ];
            $QStaffRewardWarning->update($data, $where);

            $flashMessenger->setNamespace('success')->addMessage('Success');
            $back_url = HOST.'trainer/reward-warning?staff_id='.$staff_id;
            $this->redirect($back_url);
        }
        else{
            $flashMessenger->setNamespace('error')->addMessage('Error');
            $back_url = HOST.'trainer/reward-warning?staff_id='.$staff_id;
            $this->redirect($back_url);
        }
        

    } 

    public function getInfoAssetAction(){
        $asset = $this->getRequest()->getParam('asset');

        $QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
        $whereTypeAsset             = $QTrainerTypeAsset->getAdapter()->quoteInto('id = ?',$asset);
        
        $db = Zend_Registry::get('db');
        
        $type_asset = $QTrainerTypeAsset->fetchRow($whereTypeAsset);

        if(!empty($type_asset)){
            $data = array(
                'imei_code' => $imei_code,
                'descreption' => $type_order
            );
            echo json_encode($data);exit;
        }
    }

    public function listOrderAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'list-order.php';
    }

    public function tranferOrderAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'tranfer-order.php';
    }

    public function removeOrderAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'remove-order.php';
    }

    private function _exportList($data,$params){
       ini_set('memory_limit', '-1');

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            '#',
            'FULL NAME',
            'MODEL',
            'IMEI',
            'DATE IN',
            'NOTE'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;


        $i = 1;

        foreach ($data as $item) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $i++);
            $sheet->setCellValue($alpha++ . $index, $item['fullname']);
            $sheet->setCellValue($alpha++ . $index, $item['model']);
            $sheet->setCellValue($alpha++ . $index, $item['object']);
            $sheet->setCellValue($alpha++ . $index, $item['date_in']);
            $sheet->setCellValue($alpha++ . $index, $item['note']);

            $index++;

        }

        $filename = 'IMEI_LIST_' . date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }
	
	public function  editStaffAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'edit-staff.php';
    }

    public function  saveEditStaffAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'save-edit-staff.php';
    }

    public function reportListPgsSale($query){

        $db = Zend_Registry::get('db');
        $stmt = $db->query($query);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();

            set_time_limit(0);
            error_reporting(0);
            ini_set('memory_limit', -1);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();

            $PHPExcel->setActiveSheetIndex(0);
            $sheet    = $PHPExcel->getActiveSheet();
            $index    = 1;
            
            $heads = array(
                '#',
                'Code',
                'Fullname',
                'Email',
                'Department',
                'Team',
                'Title',
                'Joined At',
                'Phone Number',
                //'Status',
                //'Sellout',
                //'Reward',
                //'Warning',
            );
            
            $alpha    = 'A';
        
            foreach($heads as $key)
            {
                $sheet->setCellValue($alpha.$index, $key);
                $alpha++;
            }

            $index    = 2;
        $i = 1;

            foreach($list as $item){
                $alpha = 'A';
               
                $sheet->setCellValue($alpha++.$index, $i++);
                $sheet->setCellValue($alpha++.$index, $item['code']);
                $sheet->setCellValue($alpha++.$index, $item['fullname']);
                $sheet->setCellValue($alpha++.$index, $item['email']);
                $sheet->setCellValue($alpha++.$index, $item['department_name']);
                $sheet->setCellValue($alpha++.$index, $item['team_name']);
                $sheet->setCellValue($alpha++.$index, $item['title_name']);
                $sheet->setCellValue($alpha++.$index, $item['joined_at']);
                $sheet->setCellValue($alpha++.$index, $item['phone_number']);
                //$sheet->setCellValue($alpha++.$index, $item['status']);
                //$sheet->setCellValue($alpha++.$index, $item['sellout']);
                //$sheet->setCellValue($alpha++.$index, $item['reward']);
                //$sheet->setCellValue($alpha++.$index, $item['warning']);

                $index++;

            }
            
            $filename = 'List PGs - '.date('d/m/Y');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
    }
    
    public function storeStaffAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'store-staff.php';
    }
    
    public function storeStaffDetailsAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'store-staff-details.php';
    }
    
    public function assessmentApproveAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'assessment-approve.php';
    }
    
    public function assessmentApproveDetailsAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'assessment-approve-details.php';
    }

    public function menuAction(){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $lang = $userStorage->defaut_language;
        $id = $this->getRequest()->getParam('id');
//lay menu trong group
        if (isset($userStorage->menu) && $userStorage->menu) {
            $groupMenu = $userStorage->menu;

        } else {
            $QMenu = new Application_Model_Menu();
            $where = $QMenu->getAdapter()->quoteInto('group_id = ?', $group_id);
            $menu = $QMenu->fetchAll($where, array('parent_id', 'position'));
        }
//lay menu trong staff privielege



        $QMenu2 = new Application_Model_Menu2();
        $staffMenu = $QMenu2->getMenuPermission();
//lay menu cap 2
        $db          = Zend_Registry::get('db');

        $select = $db->select()->from(array('m' => 'menu'), array(
            'id'=>'m.id'
        ));
        $select->joinLeft(array('m1' => 'menu'), 'm1.parent_id = m.id', array())
            ->where('m.parent_id =?',$id)
            ->group('m.id');

        $secondLevelMenuId = $db->fetchAll($select);
//lay ra danh sach ID menu cap 3
        $listId=array();
        foreach ($secondLevelMenuId as $Id){
            $listId[]=$Id['id'];
        }



//lay danh sach id cua menu cap 3
        $hasChildIDs=array();

        foreach ($listId as $id){
            $select = $db->select()->from('menu', array('id'))
                ->where('parent_id=?',$id);
            $result = $db->fetchAll($select);
            if(count($result)>0){
                $hasChildIDs[]=$id;
            }
        }



        $this->view->groupMenu=$groupMenu;

        
        $this->view->listId=$listId;
        $this->view->id=$this->getRequest()->getParam('id');
        $this->view->staffMenu=$staffMenu;
        $this->view->lang=$lang;
        $this->view->hasChildIDs=$hasChildIDs;
    }

    public function otherScoresAction(){
        require_once 'trainer' . DIRECTORY_SEPARATOR . 'other-scores.php';
    }
    
}
