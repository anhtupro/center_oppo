<?php

class HrManagementController extends My_Controller_Action
{
    
    public function init() {
        
        
        $this->not_company = [7, 5];
        $this->not_department = [525, 321, 479, 321];
    }


    public function indexAction(){
        // $year_from  = date("Y",strtotime('-1 year'));
        // $year_to    = date("Y");
        // $month_from = date('m', strtotime('-1 month'));
        // $month_to   = date("m");
        

        $year_from  = date("Y",strtotime('-2 year'));
        $year_to    = date("Y",strtotime('-1 year'));
        $month_from = date('m', strtotime('-2 month'));
        $month_to   = date('m', strtotime('-1 month'));
        
        $this->view->year_from  = $year_from;
        $this->view->year_to    = $year_to;

        $this->view->month_from  = $month_from;
        $this->view->month_to    = $month_to;
        
        $department      = $this->getRequest()->getParam('department');
        $team            = $this->getRequest()->getParam('team');
        $title           = $this->getRequest()->getParam('title');

        $params = array_filter(array(
            'department' => $department,
            'team' => $team,
            'title' => $title,
        ));
        $this->view->params = $params;



        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;

        $QRegionalMarket = new Application_Model_RegionalMarket();

        if ($area_id)
        {
            if (is_array($area_id) && count($area_id))
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

            $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
        }

        

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $teams            = $QTeam->get_cache();
        $this->view->teams = $teams;

        $QHrManagement          = new Application_Model_HrManagement();
        
        $QTitleMappingReport    = new Application_Model_TitleMappingReport();
        $cacheTitle             = $QTitleMappingReport->get_cache();
        $this->view->cacheTitle = $cacheTitle;
        
        $QTeamReport            = new Application_Model_TeamReport();
        $cacheTeam              = $QTeamReport->get_cache();
        $this->view->cacheTeam  = $cacheTeam;


        

        $data_from                  = $QHrManagement->headcount_staffallcompany($year_from);
        $this->view->allCompanyFrom = $data_from;
        
        $data_to                    = $QHrManagement->headcount_staffallcompany($year_to);
        $this->view->allCompanyTo   = $data_to;
        $temp = array();

        foreach ($data_from as $key => $value) {
                    $inc = 0;
                $temp[]= "{x:".$key." +columnMetrics[0].center,y:".$value['Headcount']."},";
        } 

       
        $from                = $data_from[0]['Monthtex'].'.'.$year_from;
        $keys                = array_keys($data_from);
        $last                = end($keys);
        $to                  = $data_from[$last]['Monthtex'].'.'.$year_from;

       
        $this->view->from    = $from;
        $this->view->to      = $to;
        $this->view->number  = $data_from[$last]['Headcount'] - $data_from[0]['Headcount'];
        $this->view->percent  = round ( ( $data_from[$last]['Headcount'] - $data_from[0]['Headcount'] ) / $data_from[0]['Headcount'] * 100 ,1 ) ;
        
        $from1               = $data_to[0]['Monthtex'].'.'.$year_to;
        $keys                = array_keys($data_to);
        $last                = end($keys);
        $to1                 = $data_to[$last]['Monthtex'].'.'.$year_to;
        

        
        $this->view->from1    = $from1;
        $this->view->to1      = $to1;
        $this->view->number1  =  $data_to[$last]['Headcount'] - $data_to[0]['Headcount'];
        $this->view->percent1 = round ( ( $data_to[$last]['Headcount'] - $data_to[0]['Headcount'] ) / $data_to[0]['Headcount'] * 100 ,1 ) ;


        $allCompanySeniority_from            = $QHrManagement->get_all_company_seniority($year_from);
        $this->view->allCompanySeniorityFrom = $allCompanySeniority_from;
        // echo "<pre>";print_r($allCompanySeniority_from );die;    
        $allCompanySeniority_to              = $QHrManagement->get_all_company_seniority($year_to);
        $this->view->allCompanySeniorityTo   = $allCompanySeniority_to;

        foreach ($allCompanySeniority_to as $k => $v) {
                                // echo "['".$v['seniority'].' '.'('.$v['Headcount1'].')'."',".$v['Headcount']."],";
                                $test .= "['".$v['seniority']."',".$v['Headcount']."],";
                     }
$this->view->test = $test;

        $data_transfer_from                  = $QHrManagement->get_transfer_by_year($year_from);
        $this->view->transferFrom = $data_transfer_from;
        
        $data_transfer_to                    = $QHrManagement->get_transfer_by_year($year_to);
        $this->view->transferTo   = $data_transfer_to;
        // echo "<pre>";print_r($data_transfer_from);die;
        // $group1 = $QHrManagement->get_group($month,$year,$month1,$year1,$group = 1);
        // // echo "<pre>";print_r($group1);die;

        // $group1Name = array();

        // foreach ($group1 as $key => $value) {
        //     $group1Name[] = $value['name'];
        // }       


        // foreach ($group1 as $k => $v) {
        //     if ($k % 2 == 0) {
        //         $even1[] = $v;
        //     }
        //     else {
        //         $odd1[] = $v;
        //     }
        // }
        // $group1Name             = array_unique($group1Name);

        // $this->view->even1      = $even1 ;
        // $this->view->odd1       = $odd1 ;
        // $this->view->group1Name = $group1Name ;
        // $this->view->group1From = $group1[0]['Monthtex'];
        // $this->view->group1To   = $group1[1]['Monthtex'];


        // $group2     = $QHrManagement->get_group($month,$year,$month1,$year1,$group = 2);
        // $group2Name = array();

        // foreach ($group2 as $key => $value) {
        //     $group2Name[] = $value['name'];
        // }       


        // foreach ($group2 as $k => $v) {
        //     if ($k % 2 == 0) {
        //         $even2[] = $v;
        //     }
        //     else {
        //         $odd2[] = $v;
        //     }
        // }
        // $group2Name             = array_unique($group2Name);
        // $this->view->even2      = $even2 ;
        // $this->view->odd2       = $odd2 ;
        // $this->view->group2Name = $group2Name ;
        // $this->view->group2From = $group2[0]['Monthtex'];
        // $this->view->group2To   = $group2[1]['Monthtex'];


        // $group3     = $QHrManagement->get_group($month,$year,$month1,$year1,$group = 3);
        // $group3Name = array();

        // foreach ($group3 as $key => $value) {
        //     $group3Name[] = $value['name'];
        // }       


        // foreach ($group3 as $k => $v) {
        //     if ($k % 2 == 0) {
        //         $even3[] = $v;
        //     }
        //     else {
        //         $odd3[] = $v;
        //     }
        // }
        // $group3Name             = array_unique($group3Name);
        // $this->view->even3      = $even3 ;
        // $this->view->odd3       = $odd3 ;
        // $this->view->group3Name = $group3Name ;
        // $this->view->group3From = $group3[0]['Monthtex'];
        // $this->view->group3To   = $group3[1]['Monthtex'];


        // $group4     = $QHrManagement->get_group_title($month,$year,$month1,$year1,4,182);
        // // echo "<pre>";print_r($group4);die;

        // $group4Name = array();

        // foreach ($group4 as $key => $value) {
        //     $group4Name[] = $value['name'];
        // }       


        // foreach ($group4 as $k => $v) {
        //     if ($k % 2 == 0) {
        //         $even4[] = $v;
        //     }
        //     else {
        //         $odd4[] = $v;
        //     }
        // }
        // $group4Name             = array_unique($group4Name);
        // $this->view->even4      = $even4 ;
        // $this->view->odd4       = $odd4 ;
        // $this->view->group4Name = $group4Name ;
        // $this->view->group4From = $group4[0]['Monthtex'];
        // $this->view->group4To   = $group4[1]['Monthtex'];

        // $group4s     = $QHrManagement->get_group_title($month,$year,$month1,$year1,4,183);
        // // echo "<pre>";print_r($group4s);die;

        // $group4sName = array();

        // foreach ($group4s as $key => $value) {
        //     $group4sName[] = $value['name'];
        // }       


        // foreach ($group4s as $k => $v) {
        //     if ($k % 2 == 0) {
        //         $even4s[] = $v;
        //     }
        //     else {
        //         $odd4s[] = $v;
        //     }
        // }
        // $group4sName             = array_unique($group4sName);
        // $this->view->even4s      = $even4s ;
        // $this->view->odd4s       = $odd4s ;
        // $this->view->group4sName = $group4sName ;
        // $this->view->group4sFrom = $group4s[0]['Monthtex'];
        // $this->view->group4sTo   = $group4s[1]['Monthtex'];



        // $staff_se_pg             = $QHrManagement->get_seniority($month,$year,$month1,$year1,$title = 182);
        
        // $staff_se_pg_final = array();
        
        // // // echo  $staff_se_pg[0]['Headcount'];die;
            
            
        // $staff_se_pg_final[3] = array('color' => '#baddcd','even' => $staff_se_pg[0]['Headcount'],'odd' => $staff_se_pg[1]['Headcount']);
        // $staff_se_pg_final[6] = array('color' => '#86f9bc','even' => $staff_se_pg[2]['Headcount'],'odd' => $staff_se_pg[3]['Headcount']);
        // $staff_se_pg_final[12] = array('color' => '#2ed386','even' => $staff_se_pg[4]['Headcount'],'odd' => $staff_se_pg[5]['Headcount']);
        // $staff_se_pg_final[24] = array('color' => '#23bc75','even' => $staff_se_pg[6]['Headcount'],'odd' => $staff_se_pg[7]['Headcount']);
        // $staff_se_pg_final[25] = array('color' => '#1F8657','even' => $staff_se_pg[8]['Headcount'],'odd' => $staff_se_pg[9]['Headcount']);
        // // // echo "<pre>";print_r($staff_se_pg_final);die;
        
        // $this->view->seFrom = $staff_se_pg[0]['Monthtex'];
        // $this->view->seTo   = $staff_se_pg[1]['Monthtex'];

        // $rank = array(3 => ' &#8804 3 month',6 => ' &#8804 6 month',12 =>' &#8804 12 month',24 => ' &#8804 24 month',25 => '&#8805 24 month');
        // $this->view->rank = $rank;
        // $this->view->staff_se_pg_final = $staff_se_pg_final;

        // $staff_se_sale             = $QHrManagement->get_seniority($month,$year,$month1,$year1,$title = 183);
        // // // echo "<pre>";print_r($staff_se_sale);die;
        // $staff_se_sale_final = array();
        
        // // // echo  $staff_se_sale[0]['Headcount'];die;
            
            
        // $staff_se_sale_final[3] = array('color' => '#baddcd','even' => $staff_se_sale[0]['Headcount'],'odd' => $staff_se_sale[1]['Headcount']);
        // $staff_se_sale_final[6] = array('color' => '#86f9bc','even' => $staff_se_sale[2]['Headcount'],'odd' => $staff_se_sale[3]['Headcount']);
        // $staff_se_sale_final[12] = array('color' => '#2ed386','even' => $staff_se_sale[4]['Headcount'],'odd' => $staff_se_sale[5]['Headcount']);
        // $staff_se_sale_final[24] = array('color' => '#23bc75','even' => $staff_se_sale[6]['Headcount'],'odd' => $staff_se_sale[7]['Headcount']);
        // $staff_se_sale_final[25] = array('color' => '#1F8657','even' => $staff_se_sale[8]['Headcount'],'odd' => $staff_se_sale[9]['Headcount']);
            
        
        // $this->view->seFroms = $staff_se_sale[0]['Monthtex'];
        // $this->view->seTos   = $staff_se_sale[1]['Monthtex'];

        // $this->view->staff_se_sale_final = $staff_se_sale_final;
        

        // // $new_staff_from             = $QHrManagement->get_new_staff($month1,$year1);
        // // $this->view->new_staff_from = $new_staff_from;
        // // $this->view->staffFrom      = $new_staff_from[0]['Monthtex'];
        

        // // $new_staff_to             = $QHrManagement->get_new_staff($month,$year);
        // // $this->view->new_staff_to = $new_staff_to;
        // // $this->view->staffTo      = $new_staff_to[0]['Monthtex'];
        
       

        $turnover_all             = $QHrManagement->get_staff_turnover_all($year_to);
        $this->view->turnover_all = $turnover_all;

    
        
        // // $transfer_report             = $QHrManagement->get_transfer_report($month1,$year1);
        // // $this->view->transfer_report = $transfer_report ;

        // // $transfer_report_2             = $QHrManagement->get_transfer_report($month,$year);
        // // $this->view->transfer_report_2 = $transfer_report_2 ;


        $turnover_all_dept_from =  $QHrManagement->get_turnover_all_dept($year_from);
        $turnover_all_dept =  $QHrManagement->get_turnover_all_dept($year_to);
        foreach ($turnover_all_dept as $key => $value) {
            $this->view->turnover_all_dept_total += $value['percent']; 
        }

        // echo "<pre>";print_r($turnover_all_dept_from);die;
        $this->view->turnover_all_dept = $turnover_all_dept ;
        $this->view->turnover_all_dept_from = $turnover_all_dept_from ;
        // $compare_pg_from             =  $QHrManagement->get_compare(date('Y', strtotime('-1 year')));
        // $this->view->compare_pg_from = $compare_pg_from ;
        
        // $compare_pg_to               =  $QHrManagement->get_compare($year);
        // $this->view->compare_pg_to   = $compare_pg_to ;
       
       


        
    }

    public function staffAllCompanyAjaxAction(){
    	$this->_helper->layout->disableLayout();
    	if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){

			$year_from			= $this->getRequest()->getParam('year_from');
            $year_to          = $this->getRequest()->getParam('year_to');
			$QHrManagement	= new Application_Model_HrManagement();

			$allCompany		= $QHrManagement->headcount_staffallcompany($year_from);

            $allCompany1     = $QHrManagement->headcount_staffallcompany($year_to);
            // echo "<pre>";print_r($allCompany);
            // echo "<pre>";print_r($allCompany1);
            // die;
            // 
            // 
            
            for($index=2;$index<=2;$index++){
                $columnMetrics = array();
                for ($j=0;$j<$index;$j++) {

                    $categoryWidth    = 1;
                    $groupPadding     = $categoryWidth * .2;
                    $groupWidth       = $categoryWidth - 2 * $groupPadding;
                    $pointOffsetWidth = $groupWidth / $index;
                    $pointPadding     = $pointOffsetWidth * .1;
                    $pointWidth       = $pointOffsetWidth - 2 * $pointPadding; 
                    $colIndex         = $j;
                    $pointXOffset     = $pointPadding + ($groupPadding + $colIndex * $pointOffsetWidth - ($categoryWidth / 2));

                    $columnMetrics[] = array(
                        'width'  => $pointWidth,
                        'offset' => $pointXOffset,
                        'center' => $pointXOffset + ($pointWidth) / 2.0
                    );
                }
            }

        // echo "<pre>";print_r($columnMetrics);die;

            $x_from = $columnMetrics[0]['center'];
            $x_to   = $columnMetrics[1]['center'];


            $data = array();
            foreach ($allCompany as $key => $value) {
                $data['column_from'][] =  intval($value['Headcount']);
            }

            foreach ($allCompany1 as $key => $value) {
                $data['column_to'][] =  intval($value['Headcount']);
            }

            foreach ($allCompany as $key => $value) {
                $point_x = $x_from + $key;
                $point_y = $value['Headcount'];

                $data['line_from'][]  = array('x'=>$point_x,'y'=>intval($point_y));
            }

            foreach ($allCompany1 as $key => $value) {
                $point_x = $x_to + $key;
                $point_y = $value['Headcount'];

                $data['line_to'][]  = array('x'=>$point_x,'y'=>intval($point_y));
            }
            // echo "<pre>";print_r($data);die;			
            // 
            
            $from   = $allCompany[0]['Monthtex'].'.'.$year_from;
            $keys   = array_keys($allCompany);
            $last   = end($keys);
            $to     = $allCompany[$last]['Monthtex'].'.'.$year_from;
            
            $number = $allCompany[$last]['Headcount'] - $allCompany[0]['Headcount'];
            
            $from1  = $allCompany1[0]['Monthtex'].'.'.$year_to;
            $keys   = array_keys($allCompany1);
            $last   = end($keys);
            $to1    = $allCompany1[$last]['Monthtex'].'.'.$year_to;
            
            $number1 = $allCompany1[$last]['Headcount'] - $allCompany1[0]['Headcount'];
            $percent = round($number / $allCompany[0]['Headcount'] * 100,1);
            $percent1 = round($number1 / $allCompany1[0]['Headcount'] * 100,1);
            $data['string'] = $from.' - '.$to.' ( '.$percent.'% &asymp; '.$number.' HC)<br>'.$from1.' - '.$to1.' ( '.$percent1.'% &asymp; '.$number1.' HC)<br>';



            echo json_encode($data);
	        exit;

    	}
    }

    public function staffGroupAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year_from  = $this->getRequest()->getParam('year_from');
            $month_from = $this->getRequest()->getParam('month_from');
            $year_to    = $this->getRequest()->getParam('year_to');
            $month_to   = $this->getRequest()->getParam('month_to');
            $group      = $this->getRequest()->getParam('group');

            // echo 123;die;

            $QHrManagement = new Application_Model_HrManagement();
            $group         = $QHrManagement->get_group($month_from,$year_from,$month_to,$year_to,$group);

            

            $data = array();
            $data['from'] = $group[1]['Monthtex'];
            $data['to'] = $group[0]['Monthtex'];

            foreach ($group as $k => $v) {
                if ($k % 2 == 0) {
                    $data['event'][] = intval($v['Headcount']);
                }
                else {
                    $data['odd'][] = intval($v['Headcount']);
                }
            }

            $team = array();
            foreach ($group as $key => $value) {
                $team[] = $value['team'];
            }  
            $team             = array_unique($team);
            // echo "<pre>";print_r($team);die;
            foreach ($team as $key => $value) {
                $data['team'] .= "".$value." - ";
                $data['categories_team'][] = "".$value."";
            }  
            // echo "<pre>";print_r($data);die;
            echo json_encode($data);
            exit;
            
        }

    }

    public function staffGroupTitleAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year       = $this->getRequest()->getParam('year');
            $month      = $this->getRequest()->getParam('month');
            $year1      = $this->getRequest()->getParam('year1');
            $month1     = $this->getRequest()->getParam('month1');
            $title      = $this->getRequest()->getParam('title');
            $department = $this->getRequest()->getParam('department');
            $team       = $this->getRequest()->getParam('team');
            $area       = $this->getRequest()->getParam('area');
            $regional_market       = $this->getRequest()->getParam('regional_market');
        


            $params = array(
                'year_from'       => $year1,
                'month_from'      => $month1,
                'year_to'         => $year,
                'month_to'        => $month,
                'title'           => isset($title) ? $title : NULL,
                'department'      => $department,
                'team'            => isset($team) ? $team : NULL,
                'area'            => isset($area) ? $area : NULL,
                'regional_market' => isset($regional_market) ? $regional_market : NULL
            );
               


            $QHrManagement = new Application_Model_HrManagement();
            $group         = $QHrManagement->get_group_title($params);

            

            $data         = array();
            $group[0]['Monthtex'];
            $lastElement = end($group);

           
            $string_to = $month.$year;
            $string_from = $month1.$year1;
            // echo $string_from ;die;
            foreach ($group as $k => $v) {
                if ($k % 2 == 0) {
                    $data['serial'][$v['name']][] = intval($v['headcount']);
                }
                else {
                    $data['serial'][$v['name']][] = intval($v['headcount']);
                }
            }

            // foreach ($group as $k => $v) {
                
            //         $data[] = array('name' => $v['name'],'headcount' => $v['headcount'],'tmp' =>$v['tmp']); 
            // }
            $dataFinal = array();
            // $final = array();
            // foreach ($data as $key => $value) {
            //     if($value['tmp'] == $string_to)
            //         $dataFinal[$string_to][] = array('name' => $value['name'],'headcount' => $value['headcount']); 
            //     else
            //         $dataFinal[$string_from][] = array('name' => $value['name'],'headcount' => $value['headcount']); 
            // }
            
            // foreach ($dataFinal[$string_from] as $key => $value) {
            //     $final['from'][] = array('name' => $value['name'],'headcount' => $value['headcount']); 
            // }
            // foreach ($dataFinal[$string_to] as $key => $value) {
            //     $final['to'][] = array('name' => $value['name'],'headcount' => $value['headcount']); 
            // }

            // echo "<pre>";print_r($final);die;
            // $dataFinal = array();
            foreach ($data['serial'] as $key => $value) {
                $dataFinal['serial'][] = array('name' => $key,'headcount' => $value); 
                # code...
            }
            
            $dataFinal['categories'] = array($group[0]['Monthtex'],$lastElement['Monthtex']);
            // echo "<pre>";print_r($dataFinal);
            // echo "<pre>";print_r($dataFinal);
            // $data = array(
            //     array("name" => "SALES-ASM","headcount" => array(33,39)),
            //     array("name" => "TRADE MARKETING-ASSISTANT","headcount" => array(4,7)),
            //     array("name" => "ONLINE SALES-ASSISTANT","headcount" => array(2,3)),
            // );

            // $data['date'] = array($group[1]['Monthtex'],$group[0]['Monthtex']);
            // // echo "<pre>";print_r($data);
            
            echo json_encode($dataFinal);
            exit;
            
        }

    }

    public function getSeniorityAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');
            $month = $this->getRequest()->getParam('month');
            $year1  = $this->getRequest()->getParam('year1');
            $month1 = $this->getRequest()->getParam('month1');
            $title = $this->getRequest()->getParam('title');
            // echo $title;die;

        $QHrManagement  = new Application_Model_HrManagement();
        $staff_seniority             = $QHrManagement->get_seniority($year,$month,$title);

        $staff_seniority             = $QHrManagement->get_seniority($year,$month,$title);


        // echo "<pre>";print_r($staff_seniority);die;

        $data = array();
        
        $data['se1'] = array( floatval(number_format($staff_seniority[0]['Headcount'],1)),floatval(number_format($staff_seniority[1]['Headcount'],1)));    
        $data['se2'] = array( floatval(number_format($staff_seniority[2]['Headcount'],1)),floatval(number_format($staff_seniority[3]['Headcount'],1)));
        $data['se3'] = array( floatval(number_format($staff_seniority[4]['Headcount'],1)),floatval(number_format($staff_seniority[5]['Headcount'],1)));
        $data['se4'] = array( floatval(number_format($staff_seniority[6]['Headcount'],1)),floatval(number_format($staff_seniority[7]['Headcount'],1)));
        $data['se5'] = array( floatval(number_format($staff_seniority[8]['Headcount'],1)),floatval(number_format($staff_seniority[9]['Headcount'],1)));  

        $data['date'] = array( trim($staff_seniority[0]['Monthtex']),trim($staff_seniority[1]['Monthtex'])); 
       
        echo json_encode($data);
        exit;
            
        }
    }

    public function getStaffNewAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');
            $month = $this->getRequest()->getParam('month');


        $QHrManagement = new Application_Model_HrManagement();
        $staff_new     = $QHrManagement->get_new_staff($month,$year);
        
        
        echo json_encode($staff_new);
        exit;
            
        }
    }

    public function getAllAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');
            


        $QHrManagement = new Application_Model_HrManagement();
        $all          = $QHrManagement->get_staff_turnover_all($year);

        
        $data = array();
            foreach ($all as $key => $value) {
                $data['Monthtex'][] = trim($value['Monthtex']);
                $data['transfer'][] = intval($value['transfer']);
                $data['Off'][] = intval($value['Off']);
                $data['Newstaff'][] = intval($value['Newstaff']);
            }
    
        echo json_encode($data);
        exit;

        }
        
    }

    public function getTransferAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');
            $month  = $this->getRequest()->getParam('month');
            

        $QHrManagement   = new Application_Model_HrManagement();
        $transfer_report = $QHrManagement->get_transfer_report($month,$year);
    
        echo json_encode($transfer_report);
        exit;

        }
        
    }
    public function getTurnoverByTitleAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year_from  = $this->getRequest()->getParam('year_from');
            $year_to    = $this->getRequest()->getParam('year_to');
            $title      = $this->getRequest()->getParam('title');
            $department = $this->getRequest()->getParam('department');
            $team       = $this->getRequest()->getParam('team');
            

        $turnover = array();
        $turnover['ytd_from'] = 0;
        $turnover['ytd_to'] = 0;

        $QHrManagement = new Application_Model_HrManagement();
        $turnover_from =  $QHrManagement->get_turnover_by_title($year_from,$title,$department,$team);
        $turnover_to   =  $QHrManagement->get_turnover_by_title($year_to,$title,$department,$team);

        foreach ($turnover_from as $key => $value) {
            $turnover['from'][] = array(trim($value['headcount'].'-OFF:'.$value['off']),round($value['percent'],1));
            $turnover['ytd_from'] = $turnover['ytd_from'] + round($value['percent'],1);
        }

        foreach ($turnover_to as $key => $value) {
            $turnover['to'][] = array(trim($value['headcount'].' - OFF :'.$value['off']),round($value['percent'],1));
            $turnover['ytd_to'] = $turnover['ytd_to'] + round($value['percent'],1);
        }

        echo json_encode($turnover);die;
        exit;

        

        }
        
    }

    public function getTurnoverByTeamAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');
            $team  = $this->getRequest()->getParam('team');

        $QHrManagement   = new Application_Model_HrManagement();
        $turnover_team =  $QHrManagement->get_turnover_by_team($year,$team);

        echo json_encode($turnover_team);
        exit;

        }
        
    }
        
    public function getCompareByTitleAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year_from = $this->getRequest()->getParam('year_from');
            $year_to   = $this->getRequest()->getParam('year_to');
            $title     = $this->getRequest()->getParam('title');

        $turnover_team = array();

        $QHrManagement   = new Application_Model_HrManagement();
        $from =  $QHrManagement->get_compare_title($year_from,$title);
        $to   =  $QHrManagement->get_compare_title($year_to,$title);

        foreach ($from as $key => $value) {
            $turnover_team['from'][] = round($value['TurnoverRate'],1);
        }

        foreach ($to as $key => $value) {
            $turnover_team['to'][] = round($value['TurnoverRate'],1);
        }

        echo json_encode($turnover_team);
        exit;

        }
        
    }
    public function getCompareByTeamAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year_from = $this->getRequest()->getParam('year_from');
            $year_to   = $this->getRequest()->getParam('year_to');
            $team     = $this->getRequest()->getParam('team');

        $turnover_team = array();

        $QHrManagement   = new Application_Model_HrManagement();
        $from =  $QHrManagement->get_compare_team($year_from,$team);
        $to   =  $QHrManagement->get_compare_team($year_to,$team);

        foreach ($from as $key => $value) {
            $turnover_team['from'][] = array("OFF - 12141",round($value['TurnoverRate'],1));
        }

        foreach ($to as $key => $value) {
            $turnover_team['to'][] = array("OFF - 15141",round($value['TurnoverRate'],1));
        }

        echo json_encode($turnover_team);
        exit;

        }
        
    }

    public function getTurnoverByAllDeptAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');

        $QHrManagement   = new Application_Model_HrManagement();
        $turnover_title =  $QHrManagement->get_turnover_all_dept($year);

        echo json_encode($turnover_title);
        exit;

        }
        
    }

    public function getTotalOffByTitleAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');
            $title  = $this->getRequest()->getParam('title');

        $QHrManagement   = new Application_Model_HrManagement();
        $turnover_title =  $QHrManagement->get_total_off_by_title($year,$title);

        echo json_encode($turnover_title);
        die;
        exit;

        }
        
    }

    public function getTotalOffByTeamAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year  = $this->getRequest()->getParam('year');
            $team  = $this->getRequest()->getParam('team');

        $QHrManagement   = new Application_Model_HrManagement();
        $turnover_title =  $QHrManagement->get_total_off_by_team($year,$team);

        echo json_encode($turnover_title);
        die;
        exit;

        }
        
    }

    public function getSeniorityAllCompanyAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year_from  = $this->getRequest()->getParam('year_from');
            $year_to  = $this->getRequest()->getParam('year_to');


        $QHrManagement   = new Application_Model_HrManagement();
        $from =  $QHrManagement->get_all_company_seniority($year_from);

        $QHrManagement   = new Application_Model_HrManagement();
        $to =  $QHrManagement->get_all_company_seniority($year_to);

        $data = array();
            foreach ($from as $key => $value) {
                $data[$year_from][]  = array('name' => trim($value['seniority']),'y' => round($value['Headcount'],1));
        }


        foreach ($to as $key => $value) {
                $data[$year_to][]  = array('name' => trim($value['seniority']),'y' => round($value['Headcount'],1));
        }

        // echo "<pre>";print_r($data);die;
        echo json_encode($data);
        // die;
        exit;

        }
        
    }


    public function getSeniorityAllAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year       = $this->getRequest()->getParam('year');
            $month      = $this->getRequest()->getParam('month');
            $year1      = $this->getRequest()->getParam('year1');
            $month1     = $this->getRequest()->getParam('month1');
            $title      = $this->getRequest()->getParam('title');
            $department = $this->getRequest()->getParam('department');
            $team       = $this->getRequest()->getParam('team');
        


            $params_from = array(
                'year'  => $year1,
                'month' => $month1,
                'title'      => isset($title) ? $title : NULL,
                'department' => $department,
                'team'       => isset($team) ? $team : NULL

            );

            $params_to = array(
                'year'    => $year,
                'month'   => $month,
                'title'      => isset($title) ? $title : NULL,
                'department' => $department,
                'team'       => isset($team) ? $team : NULL

            );

            $QHrManagement   = new Application_Model_HrManagement();
            $from =  $QHrManagement->get_seniority_all($params_from);

            $QHrManagement   = new Application_Model_HrManagement();
            $to =  $QHrManagement->get_seniority_all($params_to);




            $data = array();
            foreach ($from as $key => $value) {
                    $data[$month1.'-'.$year1][]  = array(trim($value['seniority']),round($value['Headcount'],1));
            }


            foreach ($to as $key => $value) {
                    $data[$month.'-'.$year][]  = array(trim($value['seniority']),round($value['Headcount'],1));
            }

            // echo "<pre>";print_r($data);die;
            echo json_encode($data);
            // die;
            exit;

       

        }
        
    }
    public function getTransferByYearAjaxAction(){
        $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year_from  = $this->getRequest()->getParam('year_from');
            $year_to  = $this->getRequest()->getParam('year_to');
            

        $data = array();

        $QHrManagement   = new Application_Model_HrManagement();
        $from = $QHrManagement->get_transfer_by_year($year_from);
        $to = $QHrManagement->get_transfer_by_year($year_to);

        foreach ($from as $key => $value) {
            $data['transfer_from'][] = intval($value['transfer']);

        }

        foreach ($to as $key => $value) {
            $data['transfer_to'][] = intval($value['transfer']);
        }
        $data['from'] = trim($year_from);
        $data['to'] = trim($year_to);
        echo json_encode($data);
        exit;

        }
        
    }

    public function getTransferStaffAjaxAction(){
       $this->_helper->layout->disableLayout();
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $year       = $this->getRequest()->getParam('year');
            $month      = $this->getRequest()->getParam('month');
            $year1      = $this->getRequest()->getParam('year1');
            $month1     = $this->getRequest()->getParam('month1');
            $title      = $this->getRequest()->getParam('title');
            $department = $this->getRequest()->getParam('department');
            $team       = $this->getRequest()->getParam('team');
        


            $params = array(
                'year_from'  => $year1,
                'month_from' => $month1,
                'year_to'    => $year,
                'month_to'   => $month,
                'title'      => isset($title) ? $title : NULL,
                'department' => $department,
                'team'       => isset($team) ? $team : NULL

            );
               


            $QHrManagement = new Application_Model_HrManagement();
            $group         = $QHrManagement->get_staff_transfer_all($params);

            

            $data         = array();
            $group[0]['Monthtex'];
            $lastElement = end($group);

           

            foreach ($group as $k => $v) {
                if ($k % 2 == 0) {
                    $data['serial'][$v['name']][] = intval($v['transfer']);
                }
                else {
                    $data['serial'][$v['name']][] = intval($v['transfer']);
                }
            }

            $dataFinal = array();
            foreach ($data['serial'] as $key => $value) {
                $dataFinal['serial'][] = array('name' => $key,'transfer' => $value); 
                # code...
            }

            $dataFinal['categories'] = array($group[0]['Monthtex'],$lastElement['Monthtex']);
            // echo "<pre>";print_r($dataFinal);
            // $data = array(
            //     array("name" => "SALES-ASM","headcount" => array(33,39)),
            //     array("name" => "TRADE MARKETING-ASSISTANT","headcount" => array(4,7)),
            //     array("name" => "ONLINE SALES-ASSISTANT","headcount" => array(2,3)),
            // );

            // $data['date'] = array($group[1]['Monthtex'],$group[0]['Monthtex']);
            // // echo "<pre>";print_r($data);
            
            echo json_encode($dataFinal);
            exit;
            
        }
    }

    public function staffAction(){
        require_once 'hr-management' . DIRECTORY_SEPARATOR . 'staff.php';
    }

    private function month_range($first, $last, $step = '+1 month', $output_format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
    
        while( $current <= $last ) {
    
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
    
        return $dates;
    }

    private function list_month_by_year($year, $step = '+1 month', $output_format = 'Y-m-d'){

        if($year == date('Y')){
            $dates = array();
            $current = strtotime(date('Y-01-01'));
            $last = strtotime(date('Y-m-d'));

            while( $current <= $last ) {
                $da = date($output_format, $current);
                $dates[] = date('Y-m-t', strtotime($da));
                $current = strtotime($step, $current);
            }
        
            return $dates;
        }
        else{
            $dates = array();
            $current = strtotime(date($year.'-01-01'));
            $last = strtotime(date($year.'-12-01'));

            while( $current <= $last ) {
    
                $da = date($output_format, $current);
                $dates[] = date('Y-m-t', strtotime($da));
                $current = strtotime($step, $current);
            }
        
            return $dates;
        }
    }

    private function get_list_year($from_date, $to_date){
        $from_year = date('Y', strtotime($from_date));
        $to_year = date('Y', strtotime($to_date));

        $list_year = [];
        while($from_year <= $to_year){
            $list_year[] = (int)$from_year;
            $from_year = $from_year+1;
        }

        return $list_year;

    }


}
