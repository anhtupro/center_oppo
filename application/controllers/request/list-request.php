<?php
require_once 'Mobile_Detect.php';
$detect_device = new Mobile_Detect;
$category = $this->getRequest()->getParam('category', null);
$type = $this->getRequest()->getParam('type', null);
$company_id = $this->getRequest()->getParam('id_company', null);
$channel_search_id = $this->getRequest()->getParam('id_channel', null);
$content = $this->getRequest()->getParam('content', null);
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$status= $this->getRequest()->getParam('status', null);
$updateStatusPR= $this->getRequest()->getParam('updatesatuspr', null);

$flashMessenger = $this->_helper->flashMessenger;
$Qstaff = new Application_Model_Staff();
$QstaffChannel = new Application_Model_StaffChannel();
$QRequestCategory=new Application_Model_RequestCategory();
$QRequest = new Application_Model_Request();
$QCompany = new Application_Model_Company();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$total = 0;
$limit = 10;
$offset = ($page - 1) * $limit;
$title=$Qstaff->getStaffInfo($userStorage->id);
$channel=$QstaffChannel->get_staff_channel($userStorage->id);
$id_channel=array();
foreach($channel as $key => $value){
    $id_channel[$key]=$key;
}

$params = array(
    'team_name' => $title["team_name"],
    'title_name' => $title["title_name"],
    'channel' => $id_channel,
    'channel_id' => $id_channel_search,
    'type' => $type,
    'company_id' => $id_company,
    'category' => $category,
    'content' => $content,
    "staff_id"  => $userStorage->id,
    "status"  => $status,
);
$listRequest = $QRequest->fetchPagination($page, $limit, $total, $params);
$category = $QRequestCategory->getCategory();
$staff = $Qstaff->get_cache();
$company = $QCompany->get_cache();

$channel = $QstaffChannel->get_channel();

$this->view->category = $category;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

$this->view->IS_MOBILE=$detect_device->isMobile();
$this->view->company = $company;
$this->view->channel = $channel;
$this->view->offset = $offset;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST."request/list-request?";
$this->view->listRequest = $listRequest;
$this->view->userStorage = $userStorage;
$this->view->staff = $staff;




