<?php

$id = $this->getRequest()->getParam('id');
$submit = $this->getRequest()->getParam('submit');
$submit_update_payment = $this->getRequest()->getParam('submit_update_payment');
$submit_success = $this->getRequest()->getParam('submit_success');
$note = $this->getRequest()->getParam('note_confirm');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QStaffChannel = new Application_Model_StaffChannel();

$flashMessenger = $this->_helper->flashMessenger;
$QRequestApprove = new Application_Model_RequestApprove();
//get request info
$QRequest = new Application_Model_Request();
$Qstaff = new Application_Model_Staff();

$requestDetail = $QRequest->findRequestByID($id);
$title = $Qstaff->getStaffInfo($userStorage->id);
$check_approve=$QRequest->checkStaffApproving($id,$userStorage->id);
if(!empty($check_approve[0])){
    $IS_APPROVE=0;
}else{
    $IS_APPROVE=1;
}

if ($title["team_name"] == "FINANCE" AND $userStorage->id == 17) {
    $IS_ACCOUNTANT = 1;
} else {
    $IS_ACCOUNTANT = 0;
}

$staff = $Qstaff->get_cache();
//get file in request

$QRequestFile = new Application_Model_RequestFile();
$QRequestPayment = new Application_Model_RequestPayment();
$cost_real_sum=$QRequestPayment->getRealCost($id);
$cost_real_sum=$cost_real_sum["cost_real_sum"];
$this->view->cost_real_sum = $cost_real_sum;

$where = $QRequestFile->getAdapter()->quoteInto("request_id =?", $id);
$requestFile = $QRequestFile->fetchAll($where);



try{
    if ($submit) {
        //Approval: Requester -> Leader/ Key Manager  -> Director -> Requester -> Upload Payment notes  -> Accountant -> Confirm Payment -> Finish.
        //Approval: Requester -> Sale manager  -> Director -> Requester -> Upload Payment notes  -> Accountant -> Confirm Payment-> Finish.
        foreach ($requestDetail as $key => $value) {
            if (!empty($value["channel_id"])) {
                $flow_approve = array(
                    0 => "cho key account manager duyet",
                    1 => 'cho ceo duyet',
                    2 => 'cho bank accountant duyet',
                );
            } else {
                $flow_approve = array(
                    0 => 'cho sales manager duyet',
                    1 => 'cho ceo duyet',
                    2 => 'cho bank accountant duyet',
                );
            }
        }
        foreach ($flow_approve as $key => $value) {
            foreach ($requestDetail as $k => $v) {
                if ($flow_approve[$key] == $v['status']) {
                    $status = $flow_approve[$key + 1];
                    break;
                }
            }
        }
        switch ($status) {
            case "cho ceo duyet":
                $id_noti=765;
                break;
            case 'cho bank accountant duyet':
                $id_noti=17;
                break;
        }

        //$QAppNoti = new Application_Model_AppNoti();
        //$data = [
        //    'title' => "Bạn có 1 thông báo từ OPPO Test Request",
        //    'message' => "Test Request",
        //    'link' => "/request/list-request",
        //    'staff_id' => $id_noti,
        //];
        //$data_res = $QAppNoti->sendNotification($data);

        $data = array(
            'status' => $status,
        );
            $where = $QRequest->getAdapter()->quoteInto('id = ?', $id);
            $QRequest->update($data, $where);

        //insert into request_approve
        $data = array(
            'request_id' => $id,
            'created_at' => date("Y-m-d h:i:s"),
            'note' => $note,
            'staff_id' => $userStorage->id,
        );
            $QRequestApprove->insert($data);


        $total = count($_FILES['file']['name']);

        for ($i = 0; $i < $total; $i++) {
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id;
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);
            $tmpFilePath = $_FILES['file']['tmp_name'][$i];
            if ($tmpFilePath != "") {
                $old_name = $_FILES['file']['name'][$i];
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = md5(uniqid('', true)) . $id . '-staff_id' . $userStorage->id .".".$extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url = 'files' .
                        DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
                } else {
                    $url = NULL;
                }
            } else {
                $url = NULL;
            }

            $detail_file = [
                'request_id' => $id,
                'url' => $url,
                'name' => $_FILES['file']['name'][$i],
                'hash_name' => $new_name,
                'created_at' => date("Y-m-d h:i:s"),
                'staff_id' => $userStorage->id,
            ];
            if (!empty($url)) {
                    $QRequestFile->insert($detail_file);
            }
        }
        $flashMessenger->setNamespace('success')->addMessage('Xét duyệt đề xuất thành công !');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->redirect(HOST . 'request/list-request');
    }
    if ($submit_update_payment) {
        $invoice_number = $this->getRequest()->getParam('invoice_number',null);
        $invoice_date = $this->getRequest()->getParam('invoice_date',null);
        $payment_date = $this->getRequest()->getParam('payment_date');
        $payment_cost = $this->getRequest()->getParam('payment_cost');
        $payment_cost=str_replace(",","",$payment_cost);
        foreach ($payment_cost as $key => $value) {
            $data = array();
            if($key > 0){
                $invoice_number=null;
                $invoice_date=null;
            }
            $data = array(
                "request_id" => $id,
                "invoice_number" => $invoice_number,
                "invoice_date" => $invoice_date,
                "payment_date" => $payment_date[$key],
                "payment_cost" => $payment_cost[$key],
                "created_at" => date('Y-m-d H:i:s'),
                "created_by" => $userStorage->id,
            );

                $QRequestPayment->insert($data);

        }
        $flashMessenger->setNamespace('success')->addMessage('Cập nhật thanh toán thành công !');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->redirect(HOST . 'request/confirm-request?id='.$id);
    }
    if ($submit_success) {
        //insert into request_approve
        $data = array(
            'request_id' => $id,
            'created_at' => date("Y-m-d h:i:s"),
            'note' => $note,
            'staff_id' => $userStorage->id,
        );
            $QRequestApprove->insert($data);

        $data = array();
        $data = array(
            "is_approve" => 1,
            'status' => "hoan tat",
        );

            $where = $QRequest->getAdapter()->quoteInto("id=?", $id);
            $QRequest->update($data, $where);

        $flashMessenger->setNamespace('success')->addMessage('Xác nhận hoàn tất đề xuất thành công !');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->redirect(HOST . 'request/list-request');
    }
}catch (Exception $e){
    $flashMessenger->setNamespace('error')->addMessage('Cập nhật đề xuất thất bại !');
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;
    $this->redirect(HOST . 'request/confirm-request?id='.$id);
}
$channel = $QStaffChannel->get_channel();
$where = $QRequestApprove->getAdapter()->quoteInto("request_id=?", $id);
$requestApprove = $QRequestApprove->fetchAll($where);
$where = null;
$where = $QRequestPayment->getAdapter()->quoteInto("request_id=?", $id);
$requestPayment = $QRequestPayment->fetchAll($where);

$this->view->channel = $channel;
$this->view->title = $title;
$this->view->requestPayment = $requestPayment;
$this->view->IS_ACCOUNTANT = $IS_ACCOUNTANT;
$this->view->IS_APPROVE = $IS_APPROVE;
$this->view->requestFile = $requestFile;
$this->view->userStorage = $userStorage;
$this->view->requestApprove = $requestApprove;
$this->view->requestDetail = $requestDetail;
$this->view->staff = $staff;

// exit;
if (!empty($flashMessenger->setNamespace('error')->getMessages())) {
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}

?>