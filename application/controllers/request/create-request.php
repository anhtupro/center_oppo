<?php
$QCompany=new Application_Model_Company();
$QStaffChannel=new Application_Model_StaffChannel();
$QRequestCategory=new Application_Model_RequestCategory();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger     = $this->_helper->flashMessenger;

$company = $QCompany->get_cache();
$channel = $QStaffChannel->get_channel();
$category = $QRequestCategory->getCategory();
$this->view->company = $company;
$this->view->channel = $channel;
$this->view->category = $category;
$this->view->userStorage = $userStorage;



if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
    $messages             	= $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages 	= $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
    $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}
?>