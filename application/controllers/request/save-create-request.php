<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$company = $this->getRequest()->getParam('company');
$id = $this->getRequest()->getParam('id');
$channel = $this->getRequest()->getParam('channel', null);
$category = $this->getRequest()->getParam('category');
$type = $this->getRequest()->getParam('type');
$content = $this->getRequest()->getParam('content');
$cost_temp = $this->getRequest()->getParam('cost_temp');
$cost_temp=str_replace(",","",$cost_temp);
$cost_change = $this->getRequest()->getParam('cost_change');
$cost_execute = $this->getRequest()->getParam('cost_execute');
$note = $this->getRequest()->getParam('note');
$date_offer = $this->getRequest()->getParam('date_offer');

$QRequest = new Application_Model_Request();
$QRequestFile = new Application_Model_RequestFile();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;
$created_at = date('Y-m-d H:i:s');
//$date_offer = date_create_from_format("d/m/Y", $date_offer)->format("Y-m-d");

//Approval: Requester -> Director -> Requester -> Upload Payment notes  -> Accountant -> Confirm Payment -> Finish.
//Approval: Requester -> Sale manager  -> Director -> Requester -> Upload Payment notes  -> Accountant -> Confirm Payment-> Finish.

if (!empty($channel)) {
    $flow_approve = array(
        0 => "cho key account manager duyet",
        1 => 'cho ceo duyet',
        2 => 'cho bank accountant duyet',
    );
}else{
    $flow_approve = array(
        0 => 'cho sales manager duyet',
        1 => 'cho ceo duyet',
        2 => 'cho bank accountant duyet',
    );
}

$status = $flow_approve[0];

switch ($status) {
    case "cho key account manager duyet":
        $id_noti=14125;
        break;
    case 'cho sales manager duyet':
        $id_noti=326;
        break;
}


//$QAppNoti = new Application_Model_AppNoti();
//$data = [
//    'title' => "Bạn có 1 thông báo từ OPPO Test Request",
//    'message' => "Test Request",
//    'link' => "/request/list-request",
//    'staff_id' => $id_noti,
//];
//$data_res = $QAppNoti->sendNotification($data);

$data = array(
    'company_id'    => $company,
    'category_id'   => $category,
    'channel_id'=> $channel,
    'cost_temp' => $cost_temp,
    'number_change' => $cost_change,
    'cost_real' => $cost_execute,
    'content'   => $content,
    'type'      => $type,
    'note'      => $note,
    'created_by' => $userStorage->id,
    'created_at' => $created_at,
    'is_approve' => 0,
    'date_offer' => $date_offer,
    'del' => 0,
    'status' => $status,
);

try {
    $id = $QRequest->insert($data);
    $total = count($_FILES['file']['name']);

    for ($i = 0; $i < $total; $i++) {
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id;

        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);
        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
        if ($tmpFilePath != "") {
            $old_name = $_FILES['file']['name'][$i];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode);
            $new_name = md5(uniqid('', true)) . $id . '-staff_id' . $userStorage->id."." .$extension;
            $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                $url = 'files' .
                    DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
            } else {
                $url = NULL;
            }
        } else {
            $url = NULL;
        }

        $detail_file = [
            'request_id' => $id,
            'url' => $url,
            'name' => $_FILES['file']['name'][$i],
            'hash_name' => $new_name,
            'created_at' => $created_at,
            'staff_id' => $userStorage->id,
        ];
        if (!empty($url)) {
            $QRequestFile->insert($detail_file);
        }
    }

    $flashMessenger->setNamespace('success')->addMessage('Đề xuất chi phí thành công !');
    $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
        echo json_encode([
        'status' => 1,
    ]);
    return;
    // $this->redirect(HOST . 'request/list-request');

} catch (exception $e) {
        echo json_encode([
        'status' => 0,
        'message' => "Tạo đề xuất không thành công"
    ]);
    return;
    // $flashMessenger=$flashMessenger->setNamespace('error')->addMessage('Lỗi tạo đề xuất ! ');
    // $messages             = $flashMessenger->setNamespace('error')->getMessages();
    // $this->view->messages = $messages;
    // $this->_redirect(HOST . 'request/create-request');
}
?>