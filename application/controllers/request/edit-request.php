<?php

//$note = $this->getRequest()->getParam('note');
//$company = $this->getRequest()->getParam('company');
$id = $this->getRequest()->getParam('id');
//$channel = $this->getRequest()->getParam('channel', null);
//$category = $this->getRequest()->getParam('category');
//$content = $this->getRequest()->getParam('content');
$submit = $this->getRequest()->getParam('submit');
$note = $this->getRequest()->getParam('note');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;
$QRequestApprove = new Application_Model_RequestApprove();
//get request info
$QRequest = new Application_Model_Request();
$requestDetail = $QRequest->findRequestByID($id);
$Qstaff = new Application_Model_Staff();
$staff = $Qstaff->get_cache();
$QRequestCompany = new Application_Model_RequestCompany();
$QRequestChannel = new Application_Model_RequestChannel();
$QRequestCategory = new Application_Model_RequestCategory();
$QRequestPayment = new Application_Model_RequestPayment();

//get file in request


$QRequestFile = new Application_Model_RequestFile();
$where = $QRequestFile->getAdapter()->quoteInto("request_id =?", $id);
$requestFile = $QRequestFile->fetchAll($where);

//$type_approve = 0;
//if (empty($staff_approve_process)) {
//    $type_approve = 1;
//}

if ($submit) {
    $data = array();
    $data = array(
//        'id_company' => $company,
//        'id_category' => $category,
//        'id_channel' => $channel,
//        'content' => $content,
//        'note' => $note,
        'updated_at' => date('Y-m-d H:i:s'),
    );

    $where = $QRequest->getAdapter()->quoteInto('id = ?', $id);

    $QRequest->update($data, $where);

    $total = count($_FILES['file']['name']);

    for ($i = 0; $i < $total; $i++) {
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id;

        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);
        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
        if ($tmpFilePath != "") {
            $old_name = $_FILES['file']['name'][$i];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode);
//            $new_name = 'UPLOAD-REQUEST-'.md5(uniqid('', true))  . $id . '-staff_id' . $userStorage->id . $old_name . '.' . $extension;
            $new_name =md5(uniqid('', true)) . $id . '-staff_id' . $userStorage->id."." .$extension;
            $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
            	
                $url = 'files' .
                    DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
            } else {
                $url = NULL;
            }
        } else {
            $url = NULL;
        }

        $detail_file = [
            'request_id' => $id,
            'url' => $url,
            'name' => $_FILES['file']['name'][$i],
            'hash_name' => $new_name,
            'created_at' => date("Y-m-d h:i:s"),
            'staff_id' => $userStorage->id,
        ];
        if (!empty($url)) {
            $QRequestFile->insert($detail_file);
        }
    }

    $flashMessenger->setNamespace('success')->addMessage('Cập nhật đề xuất thành công !');
    $this->redirect(HOST . 'request/list-request');
}
$cost_real_sum=$QRequestPayment->getRealCost($id);
$cost_real_sum=$cost_real_sum["cost_real_sum"];
$this->view->cost_real_sum = $cost_real_sum;

$where = $QRequestPayment->getAdapter()->quoteInto("request_id=?", $id);
$requestPayment = $QRequestPayment->fetchAll($where);

$company = $QRequestCompany->getCompany();
$channel = $QRequestChannel->getChannel();
$category = $QRequestCategory->getCategory();

$flashMessenger = $this->_helper->flashMessenger;
$this->view->requestPayment = $requestPayment;
$this->view->company = $company;
$this->view->channel = $channel;
$this->view->category = $category;
$this->view->requestFile = $requestFile;
$this->view->requestDetail = $requestDetail;
$this->view->staff = $staff;


if (!empty($flashMessenger->setNamespace('error')->getMessages())) {
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}

?>