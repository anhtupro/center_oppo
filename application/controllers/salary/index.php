<?php

$month                  = $this->getRequest()->getParam('month', null);
$year                   = $this->getRequest()->getParam('year', null);
$generate_salary        = $this->getRequest()->getParam('generate_salary', null);
$export_kpi_sale        = $this->getRequest()->getParam('export_kpi', null);
$db                     = Zend_Registry::get('db');
$export_salary          = $this->getRequest()->getParam('export_salary', null);
$export_salary_by_staff = $this->getRequest()->getParam('export_salary_by_staff', null);
$export_salary_total = $this->getRequest()->getParam('export_salary_total', null);
$generated              = 0;
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger         = $this->_helper->flashMessenger;
if (empty($month) || empty($year)) {
    $month_last = strtotime('-1 month', strtotime(date('Y-m-d')));

    $month = date('m', $month_last);
    $year  = date('Y', $month_last);
} else {
    if (!is_numeric($month) || !is_numeric($year)) {
        echo "<pre>";
        print_r("Tháng năm không đúng định dạng");
        die;
    }
}
$params['month'] = $month;
$params['year']  = $year;
$sql_check_kpi   = "SELECT * FROM information_schema.tables WHERE table_schema = 'salary' AND table_name = 'kpi_" . $params['month'] . "_" . $params['year'] . "' LIMIT 1;";
$stmt            = $db->query($sql_check_kpi);
$kpi_table       = $stmt->fetchAll();
$stmt->closeCursor();
$kpi_check       = 0;
if (!empty($kpi_table)) {
    $kpi_check = 1;
}
$this->view->kpi_check = $kpi_check;

if (!empty($kpi_check)) {
    $generated = 1;
}
$version_synchronization = 0;
if ($generated) {
    $sql_check_version = "select s.code,t.session_id from salary.kpi_" . $params['month'] . "_" . $params['year'] . " s
 join(
SELECT max(`session_id`) as session_id FROM hr.`mass_upload_salary` WHERE `month`=" . $params['month'] . " and year=" . $params['year'] . ") t on s.`session_id`=IFNULL(t.session_id,0) limit 0,1;";

    $stmt    = $db->query($sql_check_version);
    $version = $stmt->fetch();
    $stmt->closeCursor();

    if (!empty($version)) {
        $version_synchronization = 1;
    }
}
$this->view->version = $version_synchronization;


if (!empty($generate_salary)) {

    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);
    try {

        $month_ini  = new DateTime("first day of last month");
        $month_end  = new DateTime("last day of last month");
        $from_date  = $month_ini->format('Y-' . $month . '-d') . ' 00:00:00';
        $to_date    = $month_end->format('Y-' . $month . '-d') . ' 23:59:59';
        $sql_params = array(
            $from_date,
            $to_date,
        );
        $sql        = 'CALL p_salary_report(?,?)';
        $stmt       = $db->query($sql, $sql_params);
//        $list       = $stmt->fetchAll();
        $stmt->closeCursor();
        
//        $sql2        = 'CALL p_salary_report_by_staff(?,?)';
//        $stmt2       = $db->query($sql2, $sql_params);
//        $stmt2->closeCursor();
//        $sql2  = 'CALL p_salary_report_training(?,?)';
//        $stmt2 = $db->query($sql2, $sql_params);
//        $list2 = $stmt->fetchAll();
//        $stmt2->closeCursor();



        $flashMessenger->setNamespace('success')->addMessage('Tạo bảng lương tháng ' . $month_ini->format('m/Y') . ' thành công.Bạn có thể export bảng lương và kpi.');
        $QLog = new Application_Model_Log();
        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = "Salary created data";
        //todo log
        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));
    } catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
}
if ((!empty($export_salary) || !empty($export_salary_total) || !empty($export_salary_by_staff)) && $generated && $version_synchronization) {
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);
    try {
        if (!empty($export_salary)) {
            $sql2     = "select * from salary.report_" . $params['month'] . "_" . $params['year'];
            $title    = "BẢNG  LƯƠNG THÁNG " . $params['month'] . " NĂM " . $params['year'];
            $filename = 'salary_report_' . date('d-m-Y');
            $sqlc     = "select * from salary.times_" . $params['month'] . "_" . $params['year'];
        } else if(!empty($export_salary_by_staff)) {
            $sql2     = "select * from salary.report_by_staff_" . $params['month'] . "_" . $params['year'];
            $title    = "BẢNG LƯƠNG ASM,ASM STANDBY, SALE AMIN,SALE LEADER THÁNG  " . $params['month'] . " NĂM " . $params['year'];
            $filename = 'salary_report_ASM_' . date('d-m-Y');
            if(($params['month']<=10 and $params['year']=2019) || ($params['year'] <2019)){
            $sqlc     = "select * from salary.times_by_staff_" . $params['month'] . "_" . $params['year'];
            }else{
                  $sqlc     = "select * from salary.times_" . $params['month'] . "_" . $params['year'];
            }
        }else{
            $sql2     = "select * from salary.report_final_" . $params['month'] . "_" . $params['year'];
            $title    = "BẢNG LƯƠNG THÁNG  " . $params['month'] . " NĂM " . $params['year'];
            $filename = 'salary_report_total_' . date('d-m-Y');
             $sqlc     = "select * from salary.times_" . $params['month'] . "_" . $params['year'];
        }
        $stmt2 = $db->query($sql2, $sql_params);
        $list2 = $stmt2->fetchAll();
        $stmt2->closeCursor();
        $list3 = [];
//        $sql3  = "select * from salary.report_training" . $params['month'] . "_" . $params['year'];
//        $stmt3 = $db->query($sql3, $sql_params);
//        $list3 = $stmt3->fetchAll();
//        $stmt3->closeCursor();

        $list_all = array_merge($list2, $list3);

        include 'PHPExcel/IOFactory.php';
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'export_template.xlsx';

        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($path);
        $sheet         = $objPHPExcel->getActiveSheet();

        $index = 6;
        $stt   = 1;


        $sheet->getCell("F2")->setValueExplicit($title, PHPExcel_Cell_DataType::TYPE_STRING);
        foreach ($list_all as $key => $value):
            $alpha   = 'A';
            $alpha_1 = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['company']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['khuvu']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['tinh']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['quan_huyen']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($stt++), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['fullname']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['department']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['team']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['current_salary_total'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['current_insurance_salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['current_salary_bonus'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['current_work_cost_salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['com'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['phone'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['xang'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['hotrocongviec_import'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['hotro_cagay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['congdinhmuc'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['congthuong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['congchunhat'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['thu7tinhluong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['nghihuongluong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['congdilamle'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['congtraining'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['phep'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['ungphep'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['giocongtangca'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['giocongchunhat'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_before_by_working_days'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['salary_current_by_working_days'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luongtangca'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi1'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_sale'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['thuongkhac'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_phukien'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_chuyenphat'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['buluong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tong_cong_luong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['bhxh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['bhyt'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['bhtn'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['congdoan'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tru_khac_import'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['thue_tncn'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['truythu_bhyt'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['quyettoan_thue'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tam_ung_luong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tong_giam_tru'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong_thuc_nhan'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(" ", PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tax_salary'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['nguoiphuthuoc'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['giamtru'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong_chiu_thue'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luongdongbaohiem'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luongcongdoan'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong13'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['com_saudieuchinh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['phone_saudieuchinh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['xang_saudieuchinh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['ngaydieuchinh'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(" ", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong_truocdieuchinh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['com_truocdieuchinh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['phone_truocdieuchinh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['xang_truocdieuchinh'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cong_truoc'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cong_sau'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cong_tinh_luong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cong_thucte'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['joined_at'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['off_date'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['conggay_total'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi1'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi2'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_phone_pg_leader'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_phone_sales'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_phone_leader'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_phone_management'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_acce_sale_pb_sale'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_acce_pgpb'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_acce_ka'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_acce_leader'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_digital'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['hr_basic']) ? $value['hr_basic'] : '', PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['hr_var']) ? $value['hr_var'] : '', PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['hr_21']) ? $value['hr_21'] : '', PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['hr_cd']) ? $value['hr_cd'] : '', PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['hr_cp']) ? $value['hr_cp'] : '', PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['hr_cost']) ? $value['hr_cost'] : '', PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha . $index)->setValueExplicit($value['transfer_note'], PHPExcel_Cell_DataType::TYPE_STRING);


            if ($value['hr_check'] == 4) {
                $objPHPExcel->getActiveSheet()
                        ->getStyle($alpha_1 . $index . ":" . $alpha . $index)
                        ->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => '00af00')
                                    )
                ));
            } else
            if ($value['hr_check'] == 3) {
                $objPHPExcel->getActiveSheet()
                        ->getStyle($alpha_1 . $index . ":" . $alpha . $index)
                        ->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'E05CC2')
                                    )
                ));
            } else
            if ($value['hr_check'] == 2) {
                $objPHPExcel->getActiveSheet()
                        ->getStyle($alpha_1 . $index . ":" . $alpha . $index)
                        ->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'ffff00')
                                    )
                ));
            }
            if ($value['code'] == '') {
                $objPHPExcel->getActiveSheet()
                        ->getStyle($alpha_1 . $index . ":" . $alpha . $index)
                        ->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => '875f5f')
                                    )
                ));
            }

            $index++;
        endforeach;

        ///
//        $sqlc   = "select * from salary.times_" . $params['month'] . "_" . $params['year'];
        $stmtc  = $db->query($sqlc);
        $list_c = $stmtc->fetchAll();
        $stmtc->closeCursor();

        $objPHPExcel->createSheet(1);
        $objPHPExcel->setActiveSheetIndex(1); // This is the first required line
        $sheet1 = $objPHPExcel->getActiveSheet();
        $sheet1->setTitle("BẢNG CÔNG TÍNH LƯƠNG ");

        $heads_c = array(
            'STT',
            'Mã số NV',
            'Công thường (đưa về định mức)',
            'Công chủ nhật',
            'Thứ 7 tính lương nhân đôi',
            'Ngày nghỉ được hưởng lương',
            'Nghỉ việc riêng được hưởng lương',
            'Lễ x2',
            'Lễ x3',
            'Lễ x4',
            'Công training',
            'Phép năm',
            'Ứng phép',
            'Giờ công tăng ca',
            'Giờ công chủ nhật ',
            '  ',
            'Tổng ngày công tính lương',
            'Ngày công tính lương TĐC',
            'Ngày công tính lương SĐC',
            '  ',
            'Ngày phép TĐC',
            'NGÀY CÔNG TĐC CHƯA CỘNG PHÉP',
        );
        $alpha_c = 'A';
        $index_c = 1;
        foreach ($heads_c as $key) {
            $sheet1->setCellValue($alpha_c . $index_c, $key);
            $alpha_c++;
        }
        $index_c++;
        $stt_13 = 1;
        foreach ($list_c as $key => $value) {
            $alpha_c = 'A';
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($stt_13++), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['actual_working_days']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['congchunhat']) + trim($value['congchunhat2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['saturdayX2']) + trim($value['saturdayX2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['nghihuongluong']) + trim($value['nghihuongluong2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['phep_cty']) + trim($value['phep_cty2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['total_le_x2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['total_le_x3']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['total_le_x4']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['congtranning']) + trim($value['congtranning2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['phep']) + trim($value['phep2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['tamungphepnew']) + trim($value['tamungphepnew2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['total_payment_day']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['before_working_days_after_cal']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(trim($value['current_working_days_after_cal']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit($value['phep_cty2'] + $value['phep2'] + $value['tamungphepnew2'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_c++ . $index_c)->setValueExplicit($value['before_working_days_after_cal'] - ($value['phep_cty2'] + $value['phep2'] + $value['tamungphepnew2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $index_c++;
        }


        $filename  = 'salary_report' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        $objWriter->save('php://output');
        exit;
    } catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
}


if (!empty($export_kpi_sale) && $generated && $version_synchronization) {
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);

    try {
        $sql = 'select * from salary.kpi_' . $params['month'] . "_" . $params['year'];

        $stmt     = $db->query($sql, array());
        $list_kpi = $stmt->fetchAll();


        include 'PHPExcel/IOFactory.php';
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'kpi_sale_pg_template.xlsx';

        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($path);
        $sheet         = $objPHPExcel->getActiveSheet();
        $stt           = 0;
        $index         = 5;
        $alpha_1       = 'A';
        $title         = $sheet->getCell("A1")->getValue();
        $title .= " " . $params['month'] . " NĂM " . $params['year'];
        $sheet->getCell("A1")->setValueExplicit($title, PHPExcel_Cell_DataType::TYPE_STRING);
        foreach ($list_kpi as $key => $value) {
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit(++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['fullname']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['khuvu']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['tinh']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['store_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['total_store']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['bonus_kpi']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['1m_kpi_days_after']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['sellout']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['kpi']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['pg_kpi_pg_after_1m']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['actual_kpi_pg']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['kpi_pg_1']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['kpi_pg_2']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['total_sale_store_leader']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['sum_total']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(" ", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(" ", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['policy_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['final_tgdd'] == 0 ? "N.TGDD" : "TGDD", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['diff_total_vs_insurance']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['diff_total_vs_insurance_before']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['salary_total_after_cal']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['insurance_salary']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['salary_total_before_cal']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['before_insurance_salary']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['congtruoc']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['congsau']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['actual_working_days']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['protect_kpi']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($value['joined_at'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['end_2_month'] == "2013-01-01" ? " " : $value['end_2_month']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha . $index)->setValueExplicit($value['transfer_note'], PHPExcel_Cell_DataType::TYPE_STRING);
            if ($value['hr_check'] == 4) {
                $objPHPExcel->getActiveSheet()
                        ->getStyle($alpha_1 . $index . ":" . $alpha . $index)
                        ->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => '00af00')
                                    )
                ));
            } else
            if ($value['hr_check'] == 3) {
                $objPHPExcel->getActiveSheet()
                        ->getStyle($alpha_1 . $index . ":" . $alpha . $index)
                        ->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'E05CC2')
                                    )
                ));
            } else
            if ($value['hr_check'] == 2) {
                $objPHPExcel->getActiveSheet()
                        ->getStyle($alpha_1 . $index . ":" . $alpha . $index)
                        ->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'ffff00')
                                    )
                ));
            }
            $index++;
        }
        /**
         * @author: hero
         * kpi asm standby
         * description
         * date
         */
        $sql_asm  = "select * from salary.kpi_asm_standby_" . $params['month'] . "_" . $params['year'];
        $stmtc    = $db->query($sql_asm);
        $list_asm = $stmtc->fetchAll();
        $stmtc->closeCursor();

        $objPHPExcel->createSheet(1);
        $objPHPExcel->setActiveSheetIndex(1); // This is the first required line
        $sheet1 = $objPHPExcel->getActiveSheet();
        $sheet1->setTitle("KPI ASM STANDBY");

        $heads_asm = array(
            'STT',
            'Mã số NV',
            'Họ tên',
            'Khu vực làm việc',
            'Tỉnh',
            'Chức vụ',
            'Ngày vào làm',
            'Transfer sang asm standby',
            'Công ty',
            'Point',
            'Tổng sellout',
            'Doanh thu 80%',
            'Tỉ lệ chưa active vượt(%)',
            'Doanh thu theo sellout sau khi trừ active vượt',
            'Kpi theo doanh thu',
            'Kpi theo bảng lương  ',
        );
        $alpha_asm = 'A';
        $index_asm = 1;
        foreach ($heads_asm as $key) {
            $sheet1->setCellValue($alpha_asm . $index_asm, $key);
            $alpha_asm++;
        }
        $index_asm++;
        $stt_13 = 1;

        foreach ($list_asm as $key => $value) {
            $alpha_asm = 'A';
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($stt_13++), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['asm_sb_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['area_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['province_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit("ASM STANDBY", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['joined_at']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['from_date']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['company']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['total_quantity']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(intval(number_format($value['value_80'])), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(trim($value['diff_area_total']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(intval($value['value_80']) * (1 - $value['diff_area_total'] / 100), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(intval($value['value_80']) * 0.002 * (1 - $value['diff_area_total'] / 100), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet1->getCell($alpha_asm++ . $index_asm)->setValueExplicit(intval($value['value_80']) * 0.002 * (1 - $value['diff_area_total'] / 100) - 6000000, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $index_asm++;
        }
        // enn kpi asm standby


        /**
         * @author: hero
         * kpi sale leader
         * description
         * date
         */
        $sql_ld  = "select * from salary.kpi_leader_" . $params['month'] . "_" . $params['year'] . " where type=0 and staff_title ='LEADER'";
        $stmtc   = $db->query($sql_ld);
        $list_ld = $stmtc->fetchAll();
        $stmtc->closeCursor();

        $objPHPExcel->createSheet(2);
        $objPHPExcel->setActiveSheetIndex(2); // This is the first required line
        $sheet2 = $objPHPExcel->getActiveSheet();
        $sheet2->setTitle("KPI SALE LEADER");

        $heads_leader = array(
            'Cống Ty',
            'Mã số NV',
            'Họ tên',
            'Khu vực làm việc',
            'Tổng sell out',
            'Doanh thu 80%',
            'Point',
            'Mức kpi',
            'KPI theo sell out 80%',
            'Tỷ lệ máy chưa active',
            'Cấn trừ máy chưa active',
            'KPI sale leader',
            'Cộng khác',
            'Tổng thu nhập',
            'Khu vực',
            'Tỉnh',
            'Note',
            'Lương CB',
            'KPI sau khi trừ lương CB',
        );
        $alpha_leader = 'A';
        $index_leader = 1;
        foreach ($heads_leader as $key) {
            $sheet2->setCellValue($alpha_leader . $index_leader, $key);
            $alpha_leader++;
        }
        $index_leader++;
        $stt_ld = 1;

        foreach ($list_ld as $key => $value) {
            $alpha_leader = 'A';
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['staff_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['region_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['total_quantity']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['total_value']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['point']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['point_doanh_thu']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['value_kpi']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['value_kpi']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['value_kpi']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['area_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(' ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(6000000, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet2->getCell($alpha_leader++ . $index_leader)->setValueExplicit(trim($value['value_kpi'] - 6000000), PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $index_leader++;
        }
        // enn kpi asm standby

        $filename  = 'kpi-sale-pg-' . $params['month'] . '-' . $params['year'] . "-created-at-" . date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        $objWriter->save('php://output');
        exit;
    } catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
}
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$messages_error               = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;
$this->view->params           = $params;
