<?php

// config for template
define('START_ROW', 9);


define('STT', 1); // cột B;	
define('MNV', 2); // cột C;	
define('HO_TEN', 3); // cột D;	
define('CHUC_VU', 4); // cột E;	
define('KHU_VUC', 5); // cột F;	
define('TINH', 6); // cột G;	
define('SHOP', 7); // cột H;	
define('TONG_SHOP_PHU_TRACH', 8); // cột I;	
define('KPI_CONG_THEM', 9); // cột J;	
define('NGAY_CONG_DUOC_HO_TRO_KPI_1TR_DAY_WORK', 10); // cột K;	
define('NHOM_SELL_OUT', 11); // cột L;	
define('TONG_SELL_OUT', 12); // cột M;	
define('TONG_KPI_THEO_SELL_OUT', 13); // cột N;	
define('KPI_SAU_KHOAN_HO_TRO_THU_NHAP_TOI_THIEU_CAC_KHOAN_CONG_KHAC', 14); // cột O;	
define('KPI_THUC_NHAN', 15); // cột P;	
define('KPI_1', 16); // cột Q;	
define('KPI_2', 17); // cột R;	
define('BU_KPI_KPI_SERVICE_BRANDSHOP', 18); // cột S;	
define('COT_T', 19); // cột T;	
define('COT_U', 20); // cột U;	
define('CHINH_SACH_LUONG', 21); // cột V;	
define('KENH_PHU_TRACH_SHOP_THUONG_TGDD_SHOP_TGDD_TGDD', 22); // cột W;	
define('CHENH_LECH_LUONG_SAU_DIEU_CHINH_VOI_LUONG_NHA_NUOC', 23); // cột X;	
define('CHENH_LECH_LUONG_TRUOC_DIEU_CHINH_VOI_LUONG_NHA_NUOC', 24); // cột Y;	
define('LUONG_TAI_THOI_DIEM_NGAY_CUOI_CUNG_CUA_THANG', 25); // cột Z;	
define('LUONG_NHA_NUOC_TAI_THOI_DIEM_NGAY_CUOI_CUNG_CUA_THANG', 26); // cột AA;	
define('LUONG_TRUOC_DIEU_CHINH', 27); // cột AB;	
define('LUONG_NHA_NUOC_TRUOC_DIEU_CHINH', 28); // cột AC;	
define('NGAY_CONG_TRUOC_DIEU_CHINH', 29); // cột AD;	
define('NGAY_CONG_SAU_DIEU_CHINH', 30); // cột AE;	
define('TONG_NGAY_CONG_TINH_LUONG', 31); // cột À;	
define('KPI_BAO_VE_DANH_CHO_CHUC_DANH_PGPB', 32); // cột AG;	
define('JOINED_AT', 33); // cột AH;	
define('JOIN_AT_TT', 34); // cột AI;


$this->_helper->layout->disableLayout();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QStaff      = new Application_Model_Staff();

$created_at = date('Y-m-d H:i:s');
$created_by = $userStorage->id;
$session_id = $userStorage->id . date('YmdHis') . substr(microtime(), 2, 4);



if ($this->getRequest()->getMethod() == 'POST') { // Big IF
    $month = $this->getRequest()->getParam('month');
    $year  = $this->getRequest()->getParam('year');

    set_time_limit(0);
    ini_set('memory_limit', -1);

    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );

    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                . DIRECTORY_SEPARATOR . $file['folder'];

        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {

        $this->view->errors = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet         = $objPHPExcel->getSheet(0);
    $highestRow    = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $data_staff_code = [];

    /**
     * @author: hero
     * valid data
     * description
     * $date
     */
    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        // nothing here
    } // END loop through order rows


    $progress->flush(30);
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        $now         = date('Y-m-d H:i:s');
        // if (!empty($data_staff_code)) {
        $data_insert = [];
        if (empty($month) || empty($year)) {
            throw new Exception("Vui lòng chọn Tháng và Năm.");
        }


        $count = 0;
        for ($row = START_ROW; $row <= $highestRow; $row++) {

          
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            if (empty($rowData[MNV])) {
                continue;
            }
              $count++;
//            // validate data
////            $joined_at= date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[JOINED_AT]));
//            $date_t    = DateTime::createFromFormat('d/m/Y', trim($rowData[JOINED_AT]));
//            $joined_at = $date_t->format('Y-m-d');
//            
//            $joined_at_59 = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[JOIN_AT_TT]));
//          
//
//            if (!My_Util::validateDate($joined_at, 'Y-m-d')) {
//                throw new Exception("Invalid Joined at, row: " . $row . ".Data:" . implode(",", $rowData));
//            }
//            if (!My_Util::validateDate($joined_at_59, 'Y-m-d')) {
//                throw new Exception("Invalid join_at_59, row: " . $row . ".Data:" . implode(",", $rowData));
//            }

            // end validate

            $data          = array(
                'stt'                                                         => (trim($rowData[STT]) != NULL) ? ($rowData[STT]) : NULL, // Cột B
                'mnv'                                                         => (trim($rowData[MNV]) != NULL) ? ($rowData[MNV]) : NULL, // Cột C
                'ho_ten'                                                      => (trim($rowData[HO_TEN]) != NULL) ? ($rowData[HO_TEN]) : NULL, // Cột D
                'chuc_vu'                                                     => (trim($rowData[CHUC_VU]) != NULL) ? ($rowData[CHUC_VU]) : NULL, // Cột E
                'khu_vuc'                                                     => (trim($rowData[KHU_VUC]) != NULL) ? ($rowData[KHU_VUC]) : NULL, // Cột F
                'tinh'                                                        => (trim($rowData[TINH]) != NULL) ? ($rowData[TINH]) : NULL, // Cột G
                'shop'                                                        => (trim($rowData[SHOP]) != NULL) ? ($rowData[SHOP]) : NULL, // Cột H
                'tong_shop_phu_trach'                                         => (trim($rowData[TONG_SHOP_PHU_TRACH]) != NULL) ? ($rowData[TONG_SHOP_PHU_TRACH]) : NULL, // Cột I
                'kpi_cong_them'                                               => (trim($rowData[KPI_CONG_THEM]) != NULL) ? intval($rowData[KPI_CONG_THEM]) : NULL, // Cột J
                'ngay_cong_duoc_ho_tro_kpi_1tr_day_work'                      => (trim($rowData[NGAY_CONG_DUOC_HO_TRO_KPI_1TR_DAY_WORK]) != NULL) ? intval($rowData[NGAY_CONG_DUOC_HO_TRO_KPI_1TR_DAY_WORK]) : NULL, // Cột K
                'nhom_sell_out'                                               => (trim($rowData[NHOM_SELL_OUT]) != NULL) ? intval($rowData[NHOM_SELL_OUT]) : NULL, // Cột L
                'tong_sell_out'                                               => (trim($rowData[TONG_SELL_OUT]) != NULL) ? intval($rowData[TONG_SELL_OUT]) : NULL, // Cột M
                'tong_kpi_theo_sell_out'                                      => (trim($rowData[TONG_KPI_THEO_SELL_OUT]) != NULL) ? intval($rowData[TONG_KPI_THEO_SELL_OUT]) : NULL, // Cột N
                'kpi_sau_khoan_ho_tro_thu_nhap_toi_thieu_cac_khoan_cong_khac' => (trim($rowData[KPI_SAU_KHOAN_HO_TRO_THU_NHAP_TOI_THIEU_CAC_KHOAN_CONG_KHAC]) != NULL) ? intval($rowData[KPI_SAU_KHOAN_HO_TRO_THU_NHAP_TOI_THIEU_CAC_KHOAN_CONG_KHAC]) : NULL, // Cột O
                'kpi_thuc_nhan'                                               => (trim($rowData[KPI_THUC_NHAN]) != NULL) ? intval($rowData[KPI_THUC_NHAN]) : NULL, // Cột P
                'kpi_1'                                                       => (trim($rowData[KPI_1]) != NULL) ? intval($rowData[KPI_1]) : NULL, // Cột Q
                'kpi_2'                                                       => (trim($rowData[KPI_2]) != NULL) ? intval($rowData[KPI_2]) : NULL, // Cột R
                'bu_kpi_kpi_service_brandshop'                                => (trim($rowData[BU_KPI_KPI_SERVICE_BRANDSHOP]) != NULL) ? intval($rowData[BU_KPI_KPI_SERVICE_BRANDSHOP]) : NULL, // Cột S
                'chinh_sach_luong'                                            => (trim($rowData[CHINH_SACH_LUONG]) != NULL) ? ($rowData[CHINH_SACH_LUONG]) : NULL, // Cột V
                'kenh_phu_trach_shop_thuong_tgdd_shop_tgdd_tgdd'              => (trim($rowData[KENH_PHU_TRACH_SHOP_THUONG_TGDD_SHOP_TGDD_TGDD]) != NULL) ? ($rowData[KENH_PHU_TRACH_SHOP_THUONG_TGDD_SHOP_TGDD_TGDD]) : NULL, // Cột W
                'chenh_lech_luong_sau_dieu_chinh_voi_luong_nha_nuoc'          => (trim($rowData[CHENH_LECH_LUONG_SAU_DIEU_CHINH_VOI_LUONG_NHA_NUOC]) != NULL) ? intval($rowData[CHENH_LECH_LUONG_SAU_DIEU_CHINH_VOI_LUONG_NHA_NUOC]) : NULL, // Cột X
                'chenh_lech_luong_truoc_dieu_chinh_voi_luong_nha_nuoc'        => (trim($rowData[CHENH_LECH_LUONG_TRUOC_DIEU_CHINH_VOI_LUONG_NHA_NUOC]) != NULL) ? intval($rowData[CHENH_LECH_LUONG_TRUOC_DIEU_CHINH_VOI_LUONG_NHA_NUOC]) : NULL, // Cột Y
                'luong_tai_thoi_diem_ngay_cuoi_cung_cua_thang'                => (trim($rowData[LUONG_TAI_THOI_DIEM_NGAY_CUOI_CUNG_CUA_THANG]) != NULL) ? intval($rowData[LUONG_TAI_THOI_DIEM_NGAY_CUOI_CUNG_CUA_THANG]) : NULL, // Cột Z
                'luong_nha_nuoc_tai_thoi_diem_ngay_cuoi_cung_cua_thang'       => (trim($rowData[LUONG_NHA_NUOC_TAI_THOI_DIEM_NGAY_CUOI_CUNG_CUA_THANG]) != NULL) ? intval($rowData[LUONG_NHA_NUOC_TAI_THOI_DIEM_NGAY_CUOI_CUNG_CUA_THANG]) : NULL, // Cột AA
                'luong_truoc_dieu_chinh'                                      => (trim($rowData[LUONG_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[LUONG_TRUOC_DIEU_CHINH]) : NULL, // Cột AB
                'luong_nha_nuoc_truoc_dieu_chinh'                             => (trim($rowData[LUONG_NHA_NUOC_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[LUONG_NHA_NUOC_TRUOC_DIEU_CHINH]) : NULL, // Cột AC
                'ngay_cong_truoc_dieu_chinh'                                  => (trim($rowData[NGAY_CONG_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[NGAY_CONG_TRUOC_DIEU_CHINH]) : NULL, // Cột AD
                'ngay_cong_sau_dieu_chinh'                                    => (trim($rowData[NGAY_CONG_SAU_DIEU_CHINH]) != NULL) ? intval($rowData[NGAY_CONG_SAU_DIEU_CHINH]) : NULL, // Cột AE
                'tong_ngay_cong_tinh_luong'                                   => (trim($rowData[TONG_NGAY_CONG_TINH_LUONG]) != NULL) ? intval($rowData[TONG_NGAY_CONG_TINH_LUONG]) : NULL, // Cột À
                'kpi_bao_ve_danh_cho_chuc_danh_pgpb'                          => (trim($rowData[KPI_BAO_VE_DANH_CHO_CHUC_DANH_PGPB]) != NULL) ? intval($rowData[KPI_BAO_VE_DANH_CHO_CHUC_DANH_PGPB]) : NULL, // Cột AG
                'joined_at'                                                   => (trim($rowData[JOINED_AT]) != NULL) ? ($rowData[JOINED_AT]) : NULL,  // Cột AH
                'join_at_59'                                                  => (trim($rowData[JOIN_AT_TT]) != NULL) ? ($rowData[JOIN_AT_TT]) : NULL, // Cột B // Cột AI
                'month'                                                       => $month,
                'year'                                                        => $year,
                'created_by'                                                  => $userStorage->id,
                'created_at'                                                  => $now,
                'session_id'                                                  => $session_id,
            );
            $data_insert[] = $data;
            $percent       = round($count * 70 / ($highestRow - START_ROW), 1);
            $progress->flush($percent);
        }// end loop


        My_Controller_Action::insertAllrow($data_insert, 'mass_upload_kpi_full');

        $db->commit();
        $progress->flush(100);
        $this->view->success = "Bảng kpi đã Import thành công.";
    } catch (Exception $e) {
        $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}