<?php

// config for template
define('START_ROW',6);

define('CONG_TY', 0); // cột A;	
define('KHU_VUC', 1); // cột B;	
define('TINH', 2); // cột C;	
define('STT', 3); // cột D;	
define('MA_SO_NV', 4); // cột E;	
define('HO_TEN', 5); // cột F;	
define('BO_PHAN', 6); // cột G;	
define('BO_PHAN_TEAM', 7); // cột H;	
define('CHUC_VU', 8); // cột I;	
define('TONG_LUONG', 9); // cột J;	
define('LUONG_CO_BAN_LUONG_THAM_GIA_BH', 10); // cột K;	
define('LUONG_CHI_PHI_PHUC_VU_CONG_VIEC', 11); // cột L;	
define('LUONG_THAM_NIEN', 12); // cột M;	
define('TC_TIEN_COM', 13); // cột N;	
define('TC_DIEN_THOAI', 14); // cột O;	
define('TC_TIEN_XANG', 15); // cột P;	
define('TC_HO_TRO_CONG_VIEC', 16); // cột Q;	
define('TC_HO_TRO_LAM_CA_GAY', 17); // cột R;	
define('GC_NGAY_CONG_DINH_MUC', 18); // cột S;	
define('GC_NGAY_CONG_BINH_THUONG', 19); // cột T;	
define('GC_NGAY_CONG_CHU_NHAT', 20); // cột U;	
define('GC_NGAY_CONG_THU_7_TINH_LUONG_NHAN_DOI', 21); // cột V;	
define('GC_NGAY_NGHI_DUOC_HUONG_LUONG', 22); // cột W;	
define('GC_NGAY_CONG_DI_LAM_NGAY_LE', 23); // cột X;	
define('GC_NGAY_CONG_TRAINING', 24); // cột Y;	
define('GC_PHEP_NAM', 25); // cột Z;	
define('GC_UNG_PHEP_HOAN_TRA_PHEP_DO_NGHI_VIEC', 26); // cột AA;	
define('GIO_CONG_TANG_CA_BT', 27); // cột AB;	
define('GIO_CONG_CHU_NHAT', 28); // cột AC;	
define('LUONG_THEO_NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC', 29); // cột AD;	
define('LUONG_THEO_NGAY_CONG_SAU_DIEU_CHINH_THU_VIEC', 30); // cột AE;	
define('LUONG_TANG_CA', 31); // cột À;	
define('KPI_SALE_SAO', 32); // cột AG;	
define('KPI_SALE', 33); // cột AH;	
define('THUONG_KHAC', 34); // cột AI;	
define('KPI_PHU_KIEN_DIGITAL', 35); // cột AJ;	
define('KPI_CHUYEN_PHAT_VTRI_KPI_SERVICE', 36); // cột AK;	
define('BU_LUONG_CAC_KHOAN_CONG_KHAC_QUYET_TOAN_PHEP_NAM', 37); // cột AL;	
define('TONG_CONG_CHI_PHI_TIEN_LUONG', 38); // cột AM;	
define('TRICH_NOP_BHXH_8PT', 39); // cột AN;	
define('TRICH_NOP_BHYT_1_5PT', 40); // cột AO;	
define('TRICH_NOP_BHTN_1PT', 41); // cột AP;	
define('TRICH_NOP_DOAN_PHI_CD', 42); // cột AQ;	
define('CAC_KHOAN_TRU_KHAC', 43); // cột AR;	
define('THUE_TNCN', 44); // cột AS;	
define('TRUY_THU_BAO_HIEM_Y_TE_TRUY_DONG_NGUOC_BH', 45); // cột AT;	
define('QUYET_TOAN_THUE_TNCN', 46); // cột AU;	
define('TAM_UNG_LUONG', 47); // cột AV;	
define('TONG_CAC_KHOAN_GIAM_TRU', 48); // cột AW;	
define('THUC_NHAN', 49); // cột AX;	
define('KY_NHAN', 50); // cột AY;	
define('LUONG_TINH_THUE', 51); // cột AZ;	
define('SO_NGUOI_PHU_THUOC_TRONG_THANG', 52); // cột BA;	
define('GIAM_TRU_BAN_THAN_NGUOI_PHU_THUOC', 53); // cột BB;	
define('PHAN_LUONG_CHIU_THUE_TNCN_SAU_GIAM_TRU_BAN_THAN_NPT', 54); // cột BC;	
define('LUONG_BHXH', 55); // cột BD;	
define('CONG_DOAN', 56); // cột BE;	
define('LUONG_T13', 57); // cột BF;	
define('PHU_CAP_COM_SAU_DIEU_CHINH', 58); // cột BG;	
define('PHU_CAP_DIEN_THOAI_SAU_DIEU_CHINH', 59); // cột BH;	
define('PHU_CAP_XANG_XE_SAU_DIEU_CHINH', 60); // cột BI;	
define('NGAY_DIEU_CHINH', 61); // cột BJ;	
define('CHUC_DANH_TRUOC_DIEU_CHINH', 62); // cột BK;	
define('LUONG_TRUOC_DIEU_CHINH', 63); // cột BL;	
define('PHU_CAP_COM_TRUOC_DIEU_CHINH', 64); // cột BM;	
define('PHU_CAP_DIEN_THOAI_TRUOC_DIEU_CHINH', 65); // cột BN;	
define('PHU_CAP_XANG_XE_TRUOC_DIEU_CHINH', 66); // cột BO;	
define('NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC', 67); // cột BP;	
define('NGAY_CONG_SAU_CHINH_THU_VIEC', 68); // cột BQ;	
define('NGAY_CONG_TINH_LUONG', 69); // cột BR;	
define('NGAY_CONG_THUC_TE', 70); // cột BS;	
define('JOINED_AT', 71); // cột BT;	
define('OFF_DATE', 72); // cột BU;	
define('NGAY_CONG_CA_GAY', 73); // cột BV;	
define('KPI_PHONE_PGS_1', 74); // cột BW;	
define('KPI_PGS_2', 75); // cột BX;	
define('KPI_PG_LEADER', 76); // cột BY;	
define('KPI_PHONE_SALES', 77); // cột BZ;	
define('KPI_PHONE_LEADER', 78); // cột CA;	
define('KPI_PHONE_MANAGEMENT', 79); // cột CB;	
define('KPI_ACCE_SALE_PB_SALE', 80); // cột CC;	
define('KPI_ACCE_PGPB', 81); // cột CD;	
define('KPI_ACCE_KA', 82); // cột CE;	
define('KPI_ACCE_LEADER', 83); // cột CF;	
define('KPI_DIGITAL', 84); // cột CG;	
define('BASIC_SALARY', 85); // cột CH;	
define('VARIABLE', 86); // cột CI;	
define('PT_BH_NSDLD', 87); // cột CJ;	
define('PT_KP_CD', 88); // cột CK;	
define('CP_CONG_THEM', 89); // cột CL;	
define('HR_COST', 90); // cột CM;

$this->_helper->layout->disableLayout();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QStaff      = new Application_Model_Staff();

$created_at = date('Y-m-d H:i:s');
$created_by = $userStorage->id;
$session_id =  date('YmHis') . substr(microtime(), 2, 3);

if ($this->getRequest()->getMethod() == 'POST') { // Big IF
    $month = $this->getRequest()->getParam('month');
    $year  = $this->getRequest()->getParam('year');

    set_time_limit(0);
    ini_set('memory_limit', -1);

    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
  
    try {

        $file = My_File::get($save_folder, $requirement, true);
        
        if (!$file || !count($file))
            throw new Exception("Upload failed");

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                . DIRECTORY_SEPARATOR . $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];    
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {

        $this->view->errors = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet         = $objPHPExcel->getSheet(0);
    $highestRow    = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $data_staff_code = [];

    /**
     * @author: hero
     * valid data
     * description
     * $date
     */
    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        // nothing here
    } // END loop through order rows


    $progress->flush(30);
    $db          = Zend_Registry::get('db');
    $db->beginTransaction();
    $data_insert = [];

    try {

        // if (!empty($data_staff_code)) {

        if (empty($month) || empty($year)) {
            throw new Exception("Vui lòng chọn Tháng và Năm.");
        }

        $count = 0;
        for ($row = START_ROW; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            if (empty($rowData[MA_SO_NV])) {
                continue;
            }
            $count++;
            $data = array(
                'cong_ty'                                             => (trim($rowData[CONG_TY]) != NULL) ? ($rowData[CONG_TY]) : NULL, // Cột A
                'khu_vuc'                                             => (trim($rowData[KHU_VUC]) != NULL) ? ($rowData[KHU_VUC]) : NULL, // Cột B
                'tinh'                                                => (trim($rowData[TINH]) != NULL) ? ($rowData[TINH]) : NULL, // Cột C
                'stt'                                                 => (trim($rowData[STT]) != NULL) ? ($rowData[STT]) : NULL, // Cột D
                'ma_so_nv'                                            => (trim($rowData[MA_SO_NV]) != NULL) ? ($rowData[MA_SO_NV]) : NULL, // Cột E
                'ho_ten'                                              => (trim($rowData[HO_TEN]) != NULL) ? ($rowData[HO_TEN]) : NULL, // Cột F
                'bo_phan'                                             => (trim($rowData[BO_PHAN]) != NULL) ? ($rowData[BO_PHAN]) : NULL, // Cột G
                'bo_phan_team'                                        => (trim($rowData[BO_PHAN_TEAM]) != NULL) ? ($rowData[BO_PHAN_TEAM]) : NULL, // Cột H
                'chuc_vu'                                             => (trim($rowData[CHUC_VU]) != NULL) ? ($rowData[CHUC_VU]) : NULL, // Cột I
                'tong_luong'                                          => (trim($rowData[TONG_LUONG]) != NULL) ? intval($rowData[TONG_LUONG]) : NULL, // Cột J
                'luong_co_ban_luong_tham_gia_bh'                      => (trim($rowData[LUONG_CO_BAN_LUONG_THAM_GIA_BH]) != NULL) ? intval($rowData[LUONG_CO_BAN_LUONG_THAM_GIA_BH]) : NULL, // Cột K
                'luong_chi_phi_phuc_vu_cong_viec'                     => (trim($rowData[LUONG_CHI_PHI_PHUC_VU_CONG_VIEC]) != NULL) ? intval($rowData[LUONG_CHI_PHI_PHUC_VU_CONG_VIEC]) : NULL, // Cột L
                'luong_tham_nien'                                     => (trim($rowData[LUONG_THAM_NIEN]) != NULL) ? intval($rowData[LUONG_THAM_NIEN]) : NULL, // Cột M
                'tc_tien_com'                                         => (trim($rowData[TC_TIEN_COM]) != NULL) ? intval($rowData[TC_TIEN_COM]) : NULL, // Cột N
                'tc_dien_thoai'                                       => (trim($rowData[TC_DIEN_THOAI]) != NULL) ? intval($rowData[TC_DIEN_THOAI]) : NULL, // Cột O
                'tc_tien_xang'                                        => (trim($rowData[TC_TIEN_XANG]) != NULL) ? intval($rowData[TC_TIEN_XANG]) : NULL, // Cột P
                'tc_ho_tro_cong_viec'                                 => (trim($rowData[TC_HO_TRO_CONG_VIEC]) != NULL) ? intval($rowData[TC_HO_TRO_CONG_VIEC]) : NULL, // Cột Q
                'tc_ho_tro_lam_ca_gay'                                => (trim($rowData[TC_HO_TRO_LAM_CA_GAY]) != NULL) ? intval($rowData[TC_HO_TRO_LAM_CA_GAY]) : NULL, // Cột R
                'gc_ngay_cong_dinh_muc'                               => (trim($rowData[GC_NGAY_CONG_DINH_MUC]) != NULL) ? intval($rowData[GC_NGAY_CONG_DINH_MUC]) : NULL, // Cột S
                'gc_ngay_cong_binh_thuong'                            => (trim($rowData[GC_NGAY_CONG_BINH_THUONG]) != NULL) ? intval($rowData[GC_NGAY_CONG_BINH_THUONG]) : NULL, // Cột T
                'gc_ngay_cong_chu_nhat'                               => (trim($rowData[GC_NGAY_CONG_CHU_NHAT]) != NULL) ? intval($rowData[GC_NGAY_CONG_CHU_NHAT]) : NULL, // Cột U
                'gc_ngay_cong_thu_7_tinh_luong_nhan_doi'              => (trim($rowData[GC_NGAY_CONG_THU_7_TINH_LUONG_NHAN_DOI]) != NULL) ? intval($rowData[GC_NGAY_CONG_THU_7_TINH_LUONG_NHAN_DOI]) : NULL, // Cột V
                'gc_ngay_nghi_duoc_huong_luong'                       => (trim($rowData[GC_NGAY_NGHI_DUOC_HUONG_LUONG]) != NULL) ? intval($rowData[GC_NGAY_NGHI_DUOC_HUONG_LUONG]) : NULL, // Cột W
                'gc_ngay_cong_di_lam_ngay_le'                         => (trim($rowData[GC_NGAY_CONG_DI_LAM_NGAY_LE]) != NULL) ? intval($rowData[GC_NGAY_CONG_DI_LAM_NGAY_LE]) : NULL, // Cột X
                'gc_ngay_cong_training'                               => (trim($rowData[GC_NGAY_CONG_TRAINING]) != NULL) ? intval($rowData[GC_NGAY_CONG_TRAINING]) : NULL, // Cột Y
                'gc_phep_nam'                                         => (trim($rowData[GC_PHEP_NAM]) != NULL) ? intval($rowData[GC_PHEP_NAM]) : NULL, // Cột Z
                'gc_ung_phep_hoan_tra_phep_do_nghi_viec'              => (trim($rowData[GC_UNG_PHEP_HOAN_TRA_PHEP_DO_NGHI_VIEC]) != NULL) ? intval($rowData[GC_UNG_PHEP_HOAN_TRA_PHEP_DO_NGHI_VIEC]) : NULL, // Cột AA
                'gio_cong_tang_ca_bt'                                 => (trim($rowData[GIO_CONG_TANG_CA_BT]) != NULL) ? intval($rowData[GIO_CONG_TANG_CA_BT]) : NULL, // Cột AB
                'gio_cong_chu_nhat'                                   => (trim($rowData[GIO_CONG_CHU_NHAT]) != NULL) ? intval($rowData[GIO_CONG_CHU_NHAT]) : NULL, // Cột AC
                'luong_theo_ngay_cong_truoc_dieu_chinh_thu_viec'      => (trim($rowData[LUONG_THEO_NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC]) != NULL) ? intval($rowData[LUONG_THEO_NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC]) : NULL, // Cột AD
                'luong_theo_ngay_cong_sau_dieu_chinh_thu_viec'        => (trim($rowData[LUONG_THEO_NGAY_CONG_SAU_DIEU_CHINH_THU_VIEC]) != NULL) ? intval($rowData[LUONG_THEO_NGAY_CONG_SAU_DIEU_CHINH_THU_VIEC]) : NULL, // Cột AE
                'luong_tang_ca'                                       => (trim($rowData[LUONG_TANG_CA]) != NULL) ? intval($rowData[LUONG_TANG_CA]) : NULL, // Cột À
                'kpi_sale_sao'                                        => (trim($rowData[KPI_SALE_SAO]) != NULL) ? intval($rowData[KPI_SALE_SAO]) : NULL, // Cột AG
                'kpi_sale'                                            => (trim($rowData[KPI_SALE]) != NULL) ? intval($rowData[KPI_SALE]) : NULL, // Cột AH
                'thuong_khac'                                         => (trim($rowData[THUONG_KHAC]) != NULL) ? intval($rowData[THUONG_KHAC]) : NULL, // Cột AI
                'kpi_phu_kien_digital'                                => (trim($rowData[KPI_PHU_KIEN_DIGITAL]) != NULL) ? intval($rowData[KPI_PHU_KIEN_DIGITAL]) : NULL, // Cột AJ
                'kpi_chuyen_phat_vtri_kpi_service'                    => (trim($rowData[KPI_CHUYEN_PHAT_VTRI_KPI_SERVICE]) != NULL) ? intval($rowData[KPI_CHUYEN_PHAT_VTRI_KPI_SERVICE]) : NULL, // Cột AK
                'bu_luong_cac_khoan_cong_khac_quyet_toan_phep_nam'    => (trim($rowData[BU_LUONG_CAC_KHOAN_CONG_KHAC_QUYET_TOAN_PHEP_NAM]) != NULL) ? intval($rowData[BU_LUONG_CAC_KHOAN_CONG_KHAC_QUYET_TOAN_PHEP_NAM]) : NULL, // Cột AL
                'tong_cong_chi_phi_tien_luong'                        => (trim($rowData[TONG_CONG_CHI_PHI_TIEN_LUONG]) != NULL) ? intval($rowData[TONG_CONG_CHI_PHI_TIEN_LUONG]) : NULL, // Cột AM
                'trich_nop_bhxh_8pt'                                  => (trim($rowData[TRICH_NOP_BHXH_8PT]) != NULL) ? intval($rowData[TRICH_NOP_BHXH_8PT]) : NULL, // Cột AN
                'trich_nop_bhyt_1_5pt'                                => (trim($rowData[TRICH_NOP_BHYT_1_5PT]) != NULL) ? intval($rowData[TRICH_NOP_BHYT_1_5PT]) : NULL, // Cột AO
                'trich_nop_bhtn_1pt'                                  => (trim($rowData[TRICH_NOP_BHTN_1PT]) != NULL) ? intval($rowData[TRICH_NOP_BHTN_1PT]) : NULL, // Cột AP
                'trich_nop_doan_phi_cd'                               => (trim($rowData[TRICH_NOP_DOAN_PHI_CD]) != NULL) ? intval($rowData[TRICH_NOP_DOAN_PHI_CD]) : NULL, // Cột AQ
                'cac_khoan_tru_khac'                                  => (trim($rowData[CAC_KHOAN_TRU_KHAC]) != NULL) ? intval($rowData[CAC_KHOAN_TRU_KHAC]) : NULL, // Cột AR
                'thue_tncn'                                           => (trim($rowData[THUE_TNCN]) != NULL) ? intval($rowData[THUE_TNCN]) : NULL, // Cột AS
                'truy_thu_bao_hiem_y_te_truy_dong_nguoc_bh'           => (trim($rowData[TRUY_THU_BAO_HIEM_Y_TE_TRUY_DONG_NGUOC_BH]) != NULL) ? intval($rowData[TRUY_THU_BAO_HIEM_Y_TE_TRUY_DONG_NGUOC_BH]) : NULL, // Cột AT
                'quyet_toan_thue_tncn'                                => (trim($rowData[QUYET_TOAN_THUE_TNCN]) != NULL) ? intval($rowData[QUYET_TOAN_THUE_TNCN]) : NULL, // Cột AU
                'tam_ung_luong'                                       => (trim($rowData[TAM_UNG_LUONG]) != NULL) ? intval($rowData[TAM_UNG_LUONG]) : NULL, // Cột AV
                'tong_cac_khoan_giam_tru'                             => (trim($rowData[TONG_CAC_KHOAN_GIAM_TRU]) != NULL) ? intval($rowData[TONG_CAC_KHOAN_GIAM_TRU]) : NULL, // Cột AW
                'thuc_nhan'                                           => (trim($rowData[THUC_NHAN]) != NULL) ? intval($rowData[THUC_NHAN]) : NULL, // Cột AX
                'ky_nhan'                                             => (trim($rowData[KY_NHAN]) != NULL) ? intval($rowData[KY_NHAN]) : NULL, // Cột AY
                'luong_tinh_thue'                                     => (trim($rowData[LUONG_TINH_THUE]) != NULL) ? intval($rowData[LUONG_TINH_THUE]) : NULL, // Cột AZ
                'so_nguoi_phu_thuoc_trong_thang'                      => (trim($rowData[SO_NGUOI_PHU_THUOC_TRONG_THANG]) != NULL) ? intval($rowData[SO_NGUOI_PHU_THUOC_TRONG_THANG]) : NULL, // Cột BA
                'giam_tru_ban_than_nguoi_phu_thuoc'                   => (trim($rowData[GIAM_TRU_BAN_THAN_NGUOI_PHU_THUOC]) != NULL) ? intval($rowData[GIAM_TRU_BAN_THAN_NGUOI_PHU_THUOC]) : NULL, // Cột BB
                'phan_luong_chiu_thue_tncn_sau_giam_tru_ban_than_npt' => (trim($rowData[PHAN_LUONG_CHIU_THUE_TNCN_SAU_GIAM_TRU_BAN_THAN_NPT]) != NULL) ? intval($rowData[PHAN_LUONG_CHIU_THUE_TNCN_SAU_GIAM_TRU_BAN_THAN_NPT]) : NULL, // Cột BC
                'luong_bhxh'                                          => (trim($rowData[LUONG_BHXH]) != NULL) ? intval($rowData[LUONG_BHXH]) : NULL, // Cột BD
                'cong_doan'                                           => (trim($rowData[CONG_DOAN]) != NULL) ? intval($rowData[CONG_DOAN]) : NULL, // Cột BE
                'luong_t13'                                           => (trim($rowData[LUONG_T13]) != NULL) ? intval($rowData[LUONG_T13]) : NULL, // Cột BF
                'phu_cap_com_sau_dieu_chinh'                          => (trim($rowData[PHU_CAP_COM_SAU_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_COM_SAU_DIEU_CHINH]) : NULL, // Cột BG
                'phu_cap_dien_thoai_sau_dieu_chinh'                   => (trim($rowData[PHU_CAP_DIEN_THOAI_SAU_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_DIEN_THOAI_SAU_DIEU_CHINH]) : NULL, // Cột BH
                'phu_cap_xang_xe_sau_dieu_chinh'                      => (trim($rowData[PHU_CAP_XANG_XE_SAU_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_XANG_XE_SAU_DIEU_CHINH]) : NULL, // Cột BI
                'ngay_dieu_chinh'                                     => (trim($rowData[NGAY_DIEU_CHINH]) != NULL) ? ($rowData[NGAY_DIEU_CHINH]) : NULL, // Cột BJ
                'chuc_danh_truoc_dieu_chinh'                          => (trim($rowData[CHUC_DANH_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[CHUC_DANH_TRUOC_DIEU_CHINH]) : NULL, // Cột BK
                'luong_truoc_dieu_chinh'                              => (trim($rowData[LUONG_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[LUONG_TRUOC_DIEU_CHINH]) : NULL, // Cột BL
                'phu_cap_com_truoc_dieu_chinh'                        => (trim($rowData[PHU_CAP_COM_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_COM_TRUOC_DIEU_CHINH]) : NULL, // Cột BM
                'phu_cap_dien_thoai_truoc_dieu_chinh'                 => (trim($rowData[PHU_CAP_DIEN_THOAI_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_DIEN_THOAI_TRUOC_DIEU_CHINH]) : NULL, // Cột BN
                'phu_cap_xang_xe_truoc_dieu_chinh'                    => (trim($rowData[PHU_CAP_XANG_XE_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_XANG_XE_TRUOC_DIEU_CHINH]) : NULL, // Cột BO
                'ngay_cong_truoc_dieu_chinh_thu_viec'                 => (trim($rowData[NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC]) != NULL) ? intval($rowData[NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC]) : NULL, // Cột BP
                'ngay_cong_sau_chinh_thu_viec'                        => (trim($rowData[NGAY_CONG_SAU_CHINH_THU_VIEC]) != NULL) ? intval($rowData[NGAY_CONG_SAU_CHINH_THU_VIEC]) : NULL, // Cột BQ
                'ngay_cong_tinh_luong'                                => (trim($rowData[NGAY_CONG_TINH_LUONG]) != NULL) ? intval($rowData[NGAY_CONG_TINH_LUONG]) : NULL, // Cột BR
                'ngay_cong_thuc_te'                                   => (trim($rowData[NGAY_CONG_THUC_TE]) != NULL) ? intval($rowData[NGAY_CONG_THUC_TE]) : NULL, // Cột BS
                'joined_at'                                           => (trim($rowData[JOINED_AT]) != NULL) ? ($rowData[JOINED_AT]) : NULL, // Cột BT
                'off_date'                                            => (trim($rowData[OFF_DATE]) != NULL) ? ($rowData[OFF_DATE]) : NULL, // Cột BU
                'ngay_cong_ca_gay'                                    => (trim($rowData[NGAY_CONG_CA_GAY]) != NULL) ? intval($rowData[NGAY_CONG_CA_GAY]) : NULL, // Cột BV
                'kpi_phone_pgs_1'                                     => (trim($rowData[KPI_PHONE_PGS_1]) != NULL) ? intval($rowData[KPI_PHONE_PGS_1]) : NULL, // Cột BW
                'kpi_pgs_2'                                           => (trim($rowData[KPI_PGS_2]) != NULL) ? intval($rowData[KPI_PGS_2]) : NULL, // Cột BX
                'kpi_pg_leader'                                       => (trim($rowData[KPI_PG_LEADER]) != NULL) ? intval($rowData[KPI_PG_LEADER]) : NULL, // Cột BY
                'kpi_phone_sales'                                     => (trim($rowData[KPI_PHONE_SALES]) != NULL) ? intval($rowData[KPI_PHONE_SALES]) : NULL, // Cột BZ
                'kpi_phone_leader'                                    => (trim($rowData[KPI_PHONE_LEADER]) != NULL) ? intval($rowData[KPI_PHONE_LEADER]) : NULL, // Cột CA
                'kpi_phone_management'                                => (trim($rowData[KPI_PHONE_MANAGEMENT]) != NULL) ? intval($rowData[KPI_PHONE_MANAGEMENT]) : NULL, // Cột CB
                'kpi_acce_sale_pb_sale'                               => (trim($rowData[KPI_ACCE_SALE_PB_SALE]) != NULL) ? intval($rowData[KPI_ACCE_SALE_PB_SALE]) : NULL, // Cột CC
                'kpi_acce_pgpb'                                       => (trim($rowData[KPI_ACCE_PGPB]) != NULL) ? intval($rowData[KPI_ACCE_PGPB]) : NULL, // Cột CD
                'kpi_acce_ka'                                         => (trim($rowData[KPI_ACCE_KA]) != NULL) ? intval($rowData[KPI_ACCE_KA]) : NULL, // Cột CE
                'kpi_acce_leader'                                     => (trim($rowData[KPI_ACCE_LEADER]) != NULL) ? intval($rowData[KPI_ACCE_LEADER]) : NULL, // Cột CF
                'kpi_digital'                                         => (trim($rowData[KPI_DIGITAL]) != NULL) ? intval($rowData[KPI_DIGITAL]) : NULL, // Cột CG
                'basic_salary'                                        => (trim($rowData[BASIC_SALARY]) != NULL) ? intval($rowData[BASIC_SALARY]) : NULL, // Cột CH
                'variable'                                            => (trim($rowData[VARIABLE]) != NULL) ? intval($rowData[VARIABLE]) : NULL, // Cột CI
                '21_5pt_bh_nsdld'                                     => (trim($rowData[PT_BH_NSDLD]) != NULL) ? intval($rowData[PT_BH_NSDLD]) : NULL, // Cột CJ
                '2pt_kp_cd'                                           => (trim($rowData[PT_KP_CD]) != NULL) ? intval($rowData[PT_KP_CD]) : NULL, // Cột CK
                'cp_cong_them'                                        => (trim($rowData[CP_CONG_THEM]) != NULL) ? intval($rowData[CP_CONG_THEM]) : NULL, // Cột CL
                'hr_cost'                                             => (trim($rowData[HR_COST]) != NULL) ? intval($rowData[HR_COST]) : NULL, // Cột CM
                'month'                                               => $month,
                'year'                                                => $year,
                'created_by'                                          => $userStorage->id,
                'created_at'                                          => date('Y-m-d H:i:s'),
                'session_id'                                          => $session_id
            );
           
            $data_insert[] = $data;
            $percent       = round($count * 70 / ($highestRow - START_ROW), 1);
            $progress->flush($percent);
        }// end loop
        //
          My_Controller_Action::insertAllrow($data_insert, 'mass_upload_salary_full');

        $db->commit();
        $progress->flush(100);
          unlink($inputFileName);
        $this->view->success = "Bảng lương đã Import thành công.";
    } catch (Exception $e) {
        $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}