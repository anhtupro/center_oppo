<?php

$this->_helper->layout->setLayout('layout_bi_empty');
$db            = Zend_Registry::get('db');
//$month = $this->getRequest()->getParam('month', date('m', strtotime("-1 month")));
$area          = $this->getRequest()->getParam('area', null);
//$sql_max_month = "SELECT  max(month) as month from mass_upload_salary_full where year=" . date('Y');
$sql_max_month = "select max(month) as `month`,m_year.year fROM mass_upload_salary_full m
join( select max(year) `year` from  mass_upload_salary_full) m_year
on m_year.year= m.year";
$stmt          = $db->query($sql_max_month);
$max_month     = $stmt->fetch(PDO::FETCH_ASSOC);
$stmt->closeCursor();
//$current_year  = date('Y');
$current_year  = $max_month['year'];

$to_d = DateTime::createFromFormat('Y-n-d', $current_year . '-' . $max_month['month'] . '-01');

$to_date_bi = $to_d->format('Y-m-t');

$from_date_bi  = $to_d->add(date_interval_create_from_date_string('-2 months'))->format('Y-m-01');
$first_day          = DateTime::createFromFormat('Y-m-d',  $max_month['year'] . '-01-01');
$first_day_off_year = $first_day->format('Y-m-01');
if ($from_date_bi < $first_day_off_year) {
    $from_date_bi = $first_day_off_year;
}

$months     = My_Date::get_months($from_date_bi, $to_date_bi);
$year          = $this->getRequest()->getParam('year', $max_month['year']);
$export_detail = $this->getRequest()->getParam('export_detail', null);


$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QArea       = new Application_Model_Area();
$area_cache  = $QArea->get_cache();

$QAsm      = new Application_Model_Asm();
$asm_cache = $QAsm->get_cache();
$area_list = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();


$params = array('area_id' => current($area_list));
if (!empty($area_list)) {
    if (in_array($area, $area_list)) {
        $params['area_id'] = $area;
    }
} else {
    $params['area_id'] = $area;
}
$data_sellout = $data_salary  = $data_averg   = [];
if (empty($area_list)) {
    $list_area = $area_cache;
} else {
    foreach ($area_list as $key => $value) {
        $list_area[$value] = $area_cache[$value];
    }
}

$this->view->list_area   = $list_area;
$params['month']         = $month;
$params['year']          = $year;
//$params['current_month'] = date('m');
$params['current_month'] = $max_month['month']+ 1;
$params['min_month']     = reset($months);
$string_cate             = '';
for ($i = $max_month['month'] - 2; $i <= $max_month['month']; $i++) {
    $i_t = $i;
    if ($i < 1) {
        $i_t = $i + 12;
    }
    $dateObj   = DateTime::createFromFormat('!m', $i_t);
    $monthName = $dateObj->format('F'); // March
    $string_cate .= "'" . $monthName . "',";
}
$this->view->params = $params;
if (!empty($export_detail)) {
    $range = array(
        'SALES' => array(
            1 => "Sell out <200",
            2 => "200 < Sell out < 300",
            3 => "300 < Sell out < 500",
            4 => "500 < Sell out < 700",
            5 => "Sell out > 700"
        ),
        'PGPB'  => array(
            1 => "Sell out < 15",
            2 => "15 < Sell out < 25",
            3 => "25 < Sell out < 50",
            4 => "50 < Sell out < 100",
            5 => "Sell out > 100"
        )
    );

    $sql_params = array(
        $params['month'], $params['year'], $params['area_id']
    );
    $sql        = 'CALL p_salary_report_by_area(?,?,?)';
    $stmt       = $db->query($sql, $sql_params);
    $list       = $stmt->fetchAll();
    $stmt->closeCursor();

    include 'PHPExcel/IOFactory.php';
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'area_cost_detail_template.xlsx';

    $inputFileType = PHPExcel_IOFactory::identify($path);
    $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel   = $objReader->load($path);
    $sheet         = $objPHPExcel->getActiveSheet();
    $index         = 4;
    $stt           = 1;


    $sheet->getCell("A1")->setValueExplicit("BẢNG LƯƠNG CHI TIẾT THÁNG " . $params['month'] . " NĂM " . $params['year'], PHPExcel_Cell_DataType::TYPE_STRING);

    foreach ($list as $key => $value) {
        $alpha   = 'A';
        $alpha_1 = 'A';
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['khu_vuc']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['tinh']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($stt++), PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['ma_so_nv']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['ho_ten']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['bo_phan']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['bo_phan_team']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['chuc_vu']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['total_store']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['sellout'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(empty($value['nhom']) ? 0 : ($value['chuc_vu'] == "SALES" ? $range['SALES'][$value['nhom']] : $range['PGPB'][$value['nhom']]), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong_ngay_cong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong_bo_sung'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cac_khoan_cong_khac'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tong_cong_chi_phi_tien_luong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['trich_nop'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['thue_tncn'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cac_khoan_tru_khac'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['thuc_nhan'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['chi_phi_luong_thuong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['bhbb_cty_tra'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['chi_phi_cong_them'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_phu_kien_digital'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['rr_cost'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $index++;
    }
    $filename  = 'area_cost_report_detai_' . $params['month'] . "_" . $params['year'];
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    header('Set-Cookie: fileLoading=true');
    $objWriter->save('php://output');
    exit;
}



if (!empty($params['area_id'])) {
    $QHrManagement        = new Application_Model_HrManagement();
    // head cound
    $sql_params_headcount = array(
        $from_date_bi,
        $to_date_bi,
        $params['area_id']
    );

    $sql_headcount_total  = "call p_head_count_report_total(?,?,?)";
    $stmt                 = $db->query($sql_headcount_total, $sql_params_headcount);
    $list_headcount_total = $stmt->fetchAll();

    $list_headcount_total = My_Util::changeKey($list_headcount_total, "MonthNo");

    $stmt->closeCursor();

    $sql_headcount = "call p_head_count_report(?,?,?)";
    $stmt          = $db->query($sql_headcount, $sql_params_headcount);

    $list_headcount = $stmt->fetchAll();

    $stmt->closeCursor();
    $PG_count    = [];
    $PG_off      = [];
    $PG_new      = [];
    $sale_count  = [];
    $SALE_off    = [];
    $SALE_new    = [];
    $other_count = [];
    $PG_partime  = [];
    foreach ($list_headcount as $key => $value) {
        if (in_array($value['title'], array(182, 293, 419, 420))) {
            $PG_count[$value['MonthNo']] += $value['staff_work'];
            $PG_off[$value['MonthNo']] += round($value['staff_off'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
            $PG_new[$value['MonthNo']] += round($value['staff_new'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
        }
        if (in_array($value['title'], array(183))) {
            $sale_count[$value['MonthNo']] += $value['staff_work'];
            $SALE_off[$value['MonthNo']] += round($value['staff_off'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
            $SALE_new[$value['MonthNo']] += round($value['staff_new'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
        }
        if (!in_array($value['title'], array(183, 182, 293, 419, 420, 375))) {
            $other_count[$value['MonthNo']] += $value['staff_work'];
        }
        if (in_array($value['title'], array(375))) {
            $PG_partime[$value['MonthNo']] += $value['staff_work'];
        }
    }







    // search theo kv
    // get sellout, doanh thu
    $fist_date = date($params['year'] . "-01-01 00:00:00");
    if (strtotime($from_date_bi) >= strtotime("2020-01-01")) {
        $sql_sellout = "SELECT sum(`qty`) as sellout, SUM(`value`*qty)*.8 as total,month(`timing_date`) as month FROM `kpi_by_model` WHERE date(`timing_date`) >=:fist_date and date(`timing_date`) <=:to_date ";
    } else {
        $sql_sellout = "SELECT count(`imei_sn`) as sellout, SUM(`value`)*.8 as total,month(`timing_date`) as month FROM `imei_kpi` WHERE date(`timing_date`) >=:fist_date and date(`timing_date`) <=:to_date ";
    }
   
    if ($params['area_id'] > 0) {
        $sql_sellout .= "and area_id= :area_id ";
    }
    $sql_sellout .= "group by month";
    $stmt = $db->prepare($sql_sellout);
    $stmt->bindParam('fist_date', $from_date_bi, PDO::PARAM_STR);
    $stmt->bindParam('to_date', $to_date_bi, PDO::PARAM_STR);
    if ($params['area_id'] > 0) {
        $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
    }
    $stmt->execute();
    $data_sellout_pre = $stmt->fetchAll();
    $data_sellout     = My_Util::changeKey($data_sellout_pre, "month");

    $stmt->closeCursor();

    // end get sellout

    $sql_salary = "SELECT l.month, COUNT(l.`ma_so_nv`) AS total_staff,SUM((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) AS hr_cost
,SUM(CASE WHEN l.`chuc_vu` in('ASM','RSM','ASM STANDBY','PARTNER','RGM','RM') or l.`chuc_vu` like '%SALES MANAGER%' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as asm_cost
,SUM(CASE WHEN l.`chuc_vu` in ('TRAINER','SALES ADMIN','SENIOR TRAINER','TRADE MARKETING EXECUTIVE','TRADE MARKETING LEADER') then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as admin_trainer_cost
,SUM(CASE WHEN l.`chuc_vu` = 'SALES LEADER' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as sale_leader_cost
,SUM(CASE WHEN l.`chuc_vu` = 'STORE LEADER' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as store_leader_cost
,SUM(CASE WHEN l.`chuc_vu` = 'SALES' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as sales_cost
,SUM(CASE WHEN l.`chuc_vu` in ('PGPB','SENIOR PROMOTER') then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as pg_cost
,SUM(CASE WHEN l.`chuc_vu` in ('PGS LEADER','PGs SUPERVISOR') then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as pgs_leader_cost
FROM mass_upload_salary_full l
JOIN `area` a ON l.`khu_vuc`=a.name
WHERE `year`=:year ";
    if ($params['area_id'] > 0) {
        $sql_salary .= " AND a.id =:area_id";
    }
    $sql_salary .= " GROUP BY l.month";
    $stmt = $db->prepare($sql_salary);
    $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
    if ($params['area_id'] > 0) {
        $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
    }
    $stmt->execute();
    $data_salary_pre = $stmt->fetchAll();

    $data_salary = My_Util::changeKey($data_salary_pre, "month");
    $stmt->closeCursor();

    // get averg
    $sql_averg = "
SELECT SUM(CASE WHEN temp.title = 'SALES' THEN 1 ELSE 0 END) AS count_sale
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') THEN 1 ELSE 0 END) AS count_pg
,SUM(CASE WHEN temp.title = 'SALES' THEN temp.sellout ELSE 0 END) AS sellout_sale
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') THEN temp.sellout ELSE 0 END) AS sellout_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =1 then 1 else 0 END) as 1_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =2 then 1 else 0 END) as 2_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =3 then 1 else 0 END) as 3_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =4 then 1 else 0 END) as 4_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =5 then 1 else 0 END) as 5_pg
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =1 THEN 1 ELSE 0 END) AS 1_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =2 THEN 1 ELSE 0 END) AS 2_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =3 THEN 1 ELSE 0 END) AS 3_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =4 THEN 1 ELSE 0 END) AS 4_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =5 THEN 1 ELSE 0 END) AS 5_sale
, `month`,`year` FROM (
SELECT m.chuc_vu AS title, f.*,i.sellout,r.range_sellout FROM hr.mass_upload_salary_full m 
JOIN hr.`area` a ON a.name= m.khu_vuc
JOIN hr.staff s ON s.code=m.ma_so_nv
JOIN (
SELECT l.staff_code,
SUM(l.congthuong+l.congle+l.congtraining+l.phep+l.lehuongluong - IFNULL(l.holidayX1,0)) AS cong,l.month,l.year  FROM hr.log_total_staff_time_by_month l
WHERE l.title IN ('SALES','PGPB','SENIOR PROMOTER')
and `year`= :year
GROUP BY l.staff_code,l.month,l.year
HAVING cong >=24
    ) f   
ON f.staff_code= m.ma_so_nv AND f.month=m.month AND f.year=m.year
JOIN (SELECT te.*
FROM (
SELECT 'SALES' title,`sale_id` AS staff_id,COUNT(`imei_sn`) AS sellout, MONTH(`timing_date`) AS `month`,YEAR(`timing_date`) AS `year` FROM salary.`imei_kpi_final`  i
WHERE i.`sale_id`  > 0 and i.kpi_sale > 0
AND YEAR(`timing_date`) =:year ";
    if ($params['area_id'] > 0) {
        $sql_averg .= "AND area_id=:area_id";
    }
    $sql_averg .= " GROUP BY i.sale_id ,`month`,`year`) AS te
UNION
SELECT te2.*
FROM (
SELECT 'PGPB' AS title,`pg_id` AS staff_id,COUNT(`imei_sn`) AS sellout, MONTH(`timing_date`) AS `month`,YEAR(`timing_date`) AS `year` FROM salary.`imei_kpi_final`  i
WHERE i.`pg_id`  > 0 and i.`kpi_pg`  > 0 AND i.kpi_pg<>0 AND YEAR(`timing_date`) =:year ";
    if ($params['area_id'] > 0) {
        $sql_averg .= "AND area_id=:area_id";
    }
    $sql_averg .= " GROUP BY i.`pg_id` ,`month`,`year`
    ) AS te2  
) AS i ON i.staff_id = s.id AND i.month=m.month AND i.year=m.year AND i.title= CASE WHEN  m.`chuc_vu` IN ('PGPB','SENIOR PROMOTER') THEN 'PGPB' ELSE  m.`chuc_vu` END
JOIN hr.report_cost_area_range AS r ON i.sellout >= r.from_sellout AND i.sellout <= r.to_sellout AND r.title= i.title
WHERE ";
    if ($params['area_id'] > 0) {
        $sql_averg .= "a.id=:area_id";
    } else {
        $sql_averg .= "1=1";
    }
    $sql_averg .= ") temp GROUP BY temp.month,temp.year";
    $stmt = $db->prepare($sql_averg);
    $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
    if ($params['area_id'] > 0) {
        $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
    }
    $stmt->execute();
    $data_averg_pre = $stmt->fetchAll();

    $data_averg = My_Util::changeKey($data_averg_pre, "month");
    $stmt->closeCursor();
}

$this->view->userStorage  = $userStorage;
$this->view->data_sellout = $data_sellout;
$this->view->data_salary  = $data_salary;
$this->view->data_averg   = $data_averg;

$this->view->list_headcount_total    = $list_headcount_total;
$this->view->list_headcount_pg       = $PG_count;
$this->view->list_headcount_sale     = $sale_count;
$this->view->list_headcount_parttime = $PG_partime;
$this->view->list_other              = $other_count;
$this->view->pg_off                  = "[" . implode($PG_off, ",") . "]";
$this->view->pg_new                  = "[" . implode($PG_new, ",") . "]";
$this->view->sale_off                = "[" . implode($SALE_off, ",") . "]";
$this->view->sale_new                = "[" . implode($SALE_new, ",") . "]";
$this->view->string_cate             = "[" . rtrim($string_cate, ",") . "]";


