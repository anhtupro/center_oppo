<?php

$db    = Zend_Registry::get('db');
$month = $this->getRequest()->getParam('month', date('m', strtotime("-1 month")));
$year  = $this->getRequest()->getParam('year', date('Y'));

$is_bi = $this->getRequest()->getParam('is_bi', null);

//$sql_max_month = "SELECT  max((`year`*1000000000)*10000 + `month`)%10000 as month ,MAX(`year`) `year`from mass_upload_salary_full ";
$sql_max_month = "select max(month) as `month`,m_year.year fROM mass_upload_salary_full m
join( select max(year) `year` from  mass_upload_salary_full) m_year
on m_year.year= m.year";
$stmt          = $db->query($sql_max_month);
$max_month     = $stmt->fetch(PDO::FETCH_ASSOC);
$stmt->closeCursor();
$current_year  = date('Y');
$f_year        = $f_month       = false;

if ($year >= $max_month['year']) {
    $f_year = $max_month['year'];
    if ($month >= $max_month['month']) {
        $f_month = $max_month['month'];
    } else {
        $f_month = $month;
    }
} elseif ($year < $max_month['year']) {
    $f_year  = $year;
    $f_month = $month;
}

$to_d               = DateTime::createFromFormat('Y-n-d', $f_year . '-' . $f_month . '-01');
$to_date_bi         = $to_d->format('Y-m-t');
$from_d             = $to_d->add(date_interval_create_from_date_string('-2 months'));
$from_date_bi       = $from_d->format('Y-m-01');
$first_day          = DateTime::createFromFormat('Y-m-d', $f_year . '-01-01');
$first_day_off_year = $first_day->format('Y-m-01');
if ($from_date_bi < $first_day_off_year) {
    $from_date_bi = $first_day_off_year;
}
$months     = My_Date::get_months($from_date_bi, $to_date_bi);
$list_month = implode($months, ",");

$area          = $this->getRequest()->getParam('area', null);
$export_detail = $this->getRequest()->getParam('export_detail', null);


$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$QArea          = new Application_Model_Area();
$area_cache     = $QArea->get_cache();
$QAsm           = new Application_Model_Asm();
$asm_cache      = $QAsm->get_cache();
$area_list      = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();
$params         = array('area_id' => 0);
$data_sellout   = $data_salary    = $data_averg     = $data_seniority = [];
if (!empty($area_list)) {
    if (in_array($area, $area_list)) {
        $params['area_id'] = $area;
    }
} else {
    $params['area_id'] = $area;
}
if (empty($area_list)) {
    $list_area = $area_cache;
} else {
    foreach ($area_list as $key => $value) {
        $list_area[$value] = $area_cache[$value];
    }
}

$this->view->list_area   = $list_area;
$params['month']         = $month;
$params['year']          = $year;
$params['current_month'] = end($months) + 1;
$params['min_month']     = reset($months);

$this->view->params = $params;



if (!empty($export_detail)) {
    $range = array(
        'SALES' => array(
            1 => "Sell out <200",
            2 => "200 < Sell out < 300",
            3 => "300 < Sell out < 500",
            4 => "500 < Sell out < 700",
            5 => "Sell out > 700"
        ),
        'PGPB'  => array(
            1 => "Sell out < 15",
            2 => "15 < Sell out < 25",
            3 => "25 < Sell out < 50",
            4 => "50 < Sell out < 100",
            5 => "Sell out > 100"
        )
    );

    $sql_params = array(
        $params['month'], $params['year'], $params['area_id']
    );
      $sql        = 'CALL p_salary_report_by_area(?,?,?)';
//    if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
//         $sql        = 'CALL p_salary_report_by_area_linked(?,?,?)';
//    }
  
    $stmt       = $db->query($sql, $sql_params);
    $list       = $stmt->fetchAll();
    $stmt->closeCursor();

    include 'PHPExcel/IOFactory.php';
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'area_cost_detail_template.xlsx';

    $inputFileType = PHPExcel_IOFactory::identify($path);
    $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel   = $objReader->load($path);
    $sheet         = $objPHPExcel->getActiveSheet();
    $index         = 4;
    $stt           = 1;


    $sheet->getCell("A1")->setValueExplicit("BẢNG LƯƠNG CHI TIẾT THÁNG " . $params['month'] . " NĂM " . $params['year'], PHPExcel_Cell_DataType::TYPE_STRING);

    foreach ($list as $key => $value) {
        $alpha   = 'A';
        $alpha_1 = 'A';
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['khu_vuc']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['tinh']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($stt++), PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['ma_so_nv']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['ho_ten']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['bo_phan']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['bo_phan_team']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['chuc_vu']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['total_store']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['sellout'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(empty($value['nhom']) ? 0 : ($value['chuc_vu'] == "SALES" ? $range['SALES'][$value['nhom']] : $range['PGPB'][$value['nhom']]), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong_ngay_cong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_sellout'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['luong_bo_sung'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cac_khoan_cong_khac'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['tong_cong_chi_phi_tien_luong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['trich_nop'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['thue_tncn'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['cac_khoan_tru_khac'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['thuc_nhan'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['chi_phi_luong_thuong'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['bhbb_cty_tra'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['chi_phi_cong_them'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['kpi_phu_kien_digital'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['rr_cost'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $index++;
    }

//    if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171', '171.244.18.76', '171.244.18.86'))) {


        // luong thang 13 
        if ($params['month'] == 1 and $params['year'] == 2019 and ! empty($params['area_id'])) {
            $sql      = 'select s.* from salary.s13_2019 s
join hr.`area` a on a.`name`= s.khu_vuc
where a.id=:area_id';
            $stmt     = $db->prepare($sql);
            $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
            $stmt->execute();
            $list_13  = $stmt->fetchAll();
            $stmt->closeCursor();
            $objPHPExcel->createSheet(1);
            $objPHPExcel->setActiveSheetIndex(1); // This is the first required line
            $sheet1   = $objPHPExcel->getActiveSheet();
            $sheet1->setTitle("Thưởng T13 SALES
(Sales, PGs)");
            $heads    = array(
                'Công ty',
                'Khu Vực',
                'Tỉnh',
                'STT',
                'Mã số NV',
                'Tên',
                'Bộ Phận',
                'Team',
                'Chức Vụ',
                'Tổng thưởng T13'
            );
            $alpha_13 = 'A';
            $index_13 = 1;

            foreach ($heads as $key) {
                $sheet1->setCellValue($alpha_13 . $index_13, $key);
                $alpha_13++;
            }
            $index_13++;
            $stt_13 = 1;
            foreach ($list_13 as $key => $value) {
                $alpha_13 = 'A';
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['cong_ty']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['khu_vuc']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['tinh']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($stt_13++), PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['ma_so_nv']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['ho_ten']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['bo_phan']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['bo_phan_team']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['chuc_vu']), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet1->getCell($alpha_13++ . $index_13)->setValueExplicit(trim($value['tong_luong']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $index_13++;
            }
        }
//    }
    $filename  = 'area_cost_report_detai_' . $params['month'] . "_" . $params['year'];
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    header('Set-Cookie: fileLoading=true');
    $objWriter->save('php://output');
    exit;
}



if (!empty($params['area_id'])) {

    $fist_date = date($params['year'] . "-01-01 00:00:00");
    // head cound

    $sql_params_headcount = array(
        $from_date_bi,
        $to_date_bi
    );

    if ($params['area_id'] == -1) {
        $sql_params_headcount[] = NULL;
    } else {
        $sql_params_headcount[] = $params['area_id'];
    }


    $sql_headcount = "call p_head_count_report(?,?,?)";
    $stmt          = $db->query($sql_headcount, $sql_params_headcount);

    $list_headcount = $stmt->fetchAll();


    $stmt->closeCursor();
    $PG_count    = [];
    $PG_off      = [];
    $PG_new      = [];
    $sale_count  = [];
    $SALE_off    = [];
    $SALE_new    = [];
    $other_count = [];
    $PG_partime  = [];

//    foreach ($list_headcount as $key => $value) {
//        if (in_array($value['title'], array(182, 293, 419, 420))) {
//            $PG_count[$value['MonthNo']] += $value['staff_work'];
//            $PG_off[$value['MonthNo']]   += $value['staff_off'];
//            $PG_new[$value['MonthNo']]   += $value['staff_new'];
//        }
//        if (in_array($value['title'], array(183))) {
//            $sale_count[$value['MonthNo']] += $value['staff_work'];
//            $SALE_off[$value['MonthNo']]   += $value['staff_off'];
//            $SALE_new[$value['MonthNo']]   += $value['staff_new'];
//        }
//        if (!in_array($value['title'], array(183, 182, 293, 419, 420, 375))) {
//            $other_count[$value['MonthNo']] += $value['staff_work'];
//        }
//        if (in_array($value['title'], array(375))) {
//            $PG_partime[$value['MonthNo']] += $value['staff_work'];
//        }
//    }




    $sql_headcount_total = "call p_head_count_report_total(?,?,?)";
    $stmt                = $db->query($sql_headcount_total, $sql_params_headcount);

    $list_headcount_total = $stmt->fetchAll();

    $list_headcount_total = My_Util::changeKey($list_headcount_total, "MonthNo");

    $stmt->closeCursor();
    foreach ($list_headcount as $key => $value) {
        if (in_array($value['title'], array(182, 293, 419, 420))) {
            $PG_count[$value['MonthNo']] += $value['staff_work'];
            $PG_off[$value['MonthNo']]   += round($value['staff_off'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
            $PG_new[$value['MonthNo']]   += round($value['staff_new'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
        }
        if (in_array($value['title'], array(183))) {
            $sale_count[$value['MonthNo']] += $value['staff_work'];
            $SALE_off[$value['MonthNo']]   += round($value['staff_off'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
            $SALE_new[$value['MonthNo']]   += round($value['staff_new'] / ($list_headcount_total[$value['MonthNo']]['staff_work'] + $list_headcount_total[$value['MonthNo']]['staff_off'] - $list_headcount_total[$value['MonthNo']]['staff_new']) * 100, 2);
        }
        if (!in_array($value['title'], array(183, 182, 293, 419, 420, 375))) {
            $other_count[$value['MonthNo']] += $value['staff_work'];
        }
        if (in_array($value['title'], array(375))) {
            $PG_partime[$value['MonthNo']] += $value['staff_work'];
        }
    }
    // search theo kv
    // get sellout, doanh thu

    
//  $sql_sellout = "SELECT count(`imei_sn`) as sellout, SUM(`value`)*.8 as total,month(`timing_date`) as month FROM `imei_kpi` WHERE date(`timing_date`) >=:fist_date and date(`timing_date`) <=:to_date ";
//    $fist_date = date($params['year'] . "-01-01 00:00:00");
    if (strtotime($from_date_bi) >= strtotime("2020-01-01")) {
    $sql_sellout = "SELECT sum(`qty`) as sellout, SUM(`value`*qty)*.8 as total,month(`timing_date`) as month FROM `kpi_by_model` WHERE date(`timing_date`) >=:fist_date and date(`timing_date`) <=:to_date ";
    }else{
        $sql_sellout = "SELECT count(`imei_sn`) as sellout, SUM(`value`)*.8 as total,month(`timing_date`) as month FROM `imei_kpi` WHERE date(`timing_date`) >=:fist_date and date(`timing_date`) <=:to_date ";
    }
    if ($params['area_id'] > 0) {
        $sql_sellout .= "and area_id= :area_id ";
    }
    $sql_sellout .= "group by month";
    $stmt        = $db->prepare($sql_sellout);
    $stmt->bindParam('fist_date', $from_date_bi, PDO::PARAM_STR);
    $stmt->bindParam('to_date', $to_date_bi, PDO::PARAM_STR);
    if ($params['area_id'] > 0) {
        $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
    }
    $stmt->execute();
    $data_sellout_pre = $stmt->fetchAll();

    $data_sellout = My_Util::changeKey($data_sellout_pre, "month");

    $stmt->closeCursor();

    // end get sellout
 
    $sql_salary = "SELECT l.month, COUNT(l.`ma_so_nv`) AS total_staff,SUM((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) AS hr_cost
,SUM(CASE WHEN l.`chuc_vu` in('ASM','RSM','ASM STANDBY','PARTNER','RGM','RM') or l.`chuc_vu` like '%SALES MANAGER%' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as asm_cost
,SUM(CASE WHEN l.`chuc_vu` in ('TRAINER','SALES ADMIN','SENIOR TRAINER','TRADE MARKETING EXECUTIVE','TRADE MARKETING LEADER') then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as admin_trainer_cost
,SUM(CASE WHEN l.`chuc_vu` = 'SALES LEADER' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as sale_leader_cost
,SUM(CASE WHEN l.`chuc_vu` = 'STORE LEADER' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as store_leader_cost
,SUM(CASE WHEN l.`chuc_vu` = 'SALES' then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as sales_cost
,SUM(CASE WHEN l.`chuc_vu` in ('PGPB','SENIOR PROMOTER') then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as pg_cost
,SUM(CASE WHEN l.`chuc_vu` in('PGS LEADER','PGs SUPERVISOR') then ((l.`tong_cong_chi_phi_tien_luong` + l.`21_5pt_bh_nsdld`+l.`2pt_kp_cd`+ l.`cp_cong_them` - l.kpi_phu_kien_digital)) else 0 end) as pgs_leader_cost
FROM mass_upload_salary_full l
LEFT JOIN `area` a ON l.`khu_vuc`=a.name
WHERE `year`=:year and `month` in($list_month)";
    if ($params['area_id'] > 0) {
        $sql_salary .= " AND a.id =:area_id";
    }
    $sql_salary .= " GROUP BY l.month, l.year";
    $stmt       = $db->prepare($sql_salary);
    $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
    if ($params['area_id'] > 0) {
        $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
    }
    $stmt->execute();
    $data_salary_pre = $stmt->fetchAll();

    $data_salary = My_Util::changeKey($data_salary_pre, "month");
    $stmt->closeCursor();
    // get averg
    $sql_averg   = "
SELECT SUM(CASE WHEN temp.title = 'SALES' THEN 1 ELSE 0 END) AS count_sale
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') THEN 1 ELSE 0 END) AS count_pg
,SUM(CASE WHEN temp.title = 'SALES' THEN temp.sellout ELSE 0 END) AS sellout_sale
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') THEN temp.sellout ELSE 0 END) AS sellout_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =1 then 1 else 0 END) as 1_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =2 then 1 else 0 END) as 2_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =3 then 1 else 0 END) as 3_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =4 then 1 else 0 END) as 4_pg
,SUM(CASE WHEN temp.title IN ('PGPB','SENIOR PROMOTER') AND temp.range_sellout =5 then 1 else 0 END) as 5_pg
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =1 THEN 1 ELSE 0 END) AS 1_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =2 THEN 1 ELSE 0 END) AS 2_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =3 THEN 1 ELSE 0 END) AS 3_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =4 THEN 1 ELSE 0 END) AS 4_sale
,SUM(CASE WHEN temp.title = 'SALES' AND temp.range_sellout =5 THEN 1 ELSE 0 END) AS 5_sale
, `month`,`year` FROM (
SELECT m.chuc_vu AS title, f.*,CASE WHEN m.chuc_vu IN('PGPB','SENIOR PROMOTER') THEN i.`sellout_pg` ELSE i.`sellout_sale` END sellout,r.range_sellout FROM hr.mass_upload_salary_full m 
JOIN hr.`area` a ON a.name= m.khu_vuc
JOIN hr.staff s ON s.code=m.ma_so_nv
JOIN (
SELECT l.staff_code,
SUM(l.congthuong+l.congle+l.congtraining+l.phep+l.lehuongluong - IFNULL(l.holidayX1,0)) AS cong,l.month,l.year  FROM hr.log_total_staff_time_by_month l
WHERE l.title IN ('SALES','PGPB','SENIOR PROMOTER')
and `year`= :year and `month` in ($list_month)
GROUP BY l.staff_code,l.month,l.year
HAVING cong >=24
    ) f   
ON f.staff_code= m.ma_so_nv AND f.month=m.month AND f.year=m.year
LEFT JOIN salary.kpi_summary_by_month i ON i.staff = s.id AND i.year=$f_year AND m.month IN ($list_month) AND i.month=m.month AND i.year=m.year 
LEFT JOIN hr.report_cost_area_range AS r ON CASE WHEN m.chuc_vu IN('PGPB','SENIOR PROMOTER') THEN IFNULL(i.`sellout_pg`,0) ELSE IFNULL(i.`sellout_sale`,0) END >= r.from_sellout AND CASE WHEN m.chuc_vu IN('PGPB','SENIOR PROMOTER') THEN IFNULL(i.`sellout_pg`,0) ELSE IFNULL(i.`sellout_sale`,0) END <= r.to_sellout AND r.title= CASE WHEN  m.`chuc_vu` IN ('PGPB','SENIOR PROMOTER') THEN 'PGPB' ELSE  m.`chuc_vu` END
WHERE ";
    if ($params['area_id'] > 0) {
        $sql_averg .= "a.id=:area_id";
    } else {
        $sql_averg .= "1=1";
    }
    $sql_averg .= " AND m.year=$f_year AND m.month IN ($list_month) ) temp GROUP BY temp.month,temp.year";
//    if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
//        echo "<pre>";
//        print_r($sql_averg);
//        die;
//    }
    $stmt      = $db->prepare($sql_averg);
    $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
    if ($params['area_id'] > 0) {
        $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
    }
    $stmt->execute();
    $data_averg_pre = $stmt->fetchAll();

    $data_averg = My_Util::changeKey($data_averg_pre, "month");


    // get seniority pie
    $sql_seniority_pie = "select temp.*,sum_total.total as total_all, round(temp.total/sum_total.total*100,2) as percent from (
select count(s.`code`) as total
,case 
when TIMESTAMPDIFF(MONTH, s.joined_at, date(NOW())) <3 then 1
when TIMESTAMPDIFF(MONTH, s.joined_at, date(NOW())) < 12 then 2
WHEN TIMESTAMPDIFF(MONTH, s.joined_at, DATE(NOW())) < 24 THEN 3
WHEN TIMESTAMPDIFF(MONTH, s.joined_at, DATE(NOW())) < 36 THEN 4
else 5 End as seniority
from staff s 
join regional_market rm on s.regional_market= rm.id
join `area` a on a.id= rm.area_id 
where s.`off_date` is null and s.title in (182,293,419,420) ";
    if ($params['area_id'] > 0) {
        $sql_seniority_pie .= "AND a.id=:area_id";
    }
    $sql_seniority_pie .= " group by seniority
) temp
left join  ( select count(*) as total from staff s 
join regional_market rm on s.regional_market= rm.id
join `area` a on a.id= rm.area_id 
where s.`off_date` is null and s.title in (182,293,419,420) ";
    if ($params['area_id'] > 0) {
        $sql_seniority_pie .= "AND a.id=:area_id";
    }
    $sql_seniority_pie .= " )as sum_total on 1=1";

    $stmt = $db->prepare($sql_seniority_pie);
    if ($params['area_id'] > 0) {
        $stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
    }
    $stmt->execute();
    $data_seniority_pie = $stmt->fetchAll();
    $data_seniority     = My_Util::changeKey($data_seniority_pie, "seniority");
}
$this->view->userStorage  = $userStorage;
$this->view->data_sellout = $data_sellout;
$this->view->data_salary  = $data_salary;
$this->view->data_averg   = $data_averg;

$this->view->list_headcount_total    = $list_headcount_total;
$this->view->list_headcount_pg       = $PG_count;
$this->view->list_headcount_sale     = $sale_count;
$this->view->list_headcount_parttime = $PG_partime;
$this->view->list_other              = $other_count;
$this->view->data_seniority          = $data_seniority;
$this->view->seniority_title         = array(
    1 => "Dưới 3 tháng",
    2 => "3 tháng đến 1 năm",
    3 => "1 năm đến 2 năm",
    4 => "2 năm đến 3 năm",
    5 => "3 năm trở lên"
);
$string_cate                         = '';

for ($i = reset($months); $i <= end($months); $i++) {
    $dateObj     = DateTime::createFromFormat('!m', $i);
    $monthName   = $dateObj->format('F'); // March
    $string_cate .= "'" . $monthName . "',";
}
$this->view->pg_off      = "[" . implode($PG_off, ",") . "]";
$this->view->pg_new      = "[" . implode($PG_new, ",") . "]";
$this->view->sale_off    = "[" . implode($SALE_off, ",") . "]";
$this->view->sale_new    = "[" . implode($SALE_new, ",") . "]";
$this->view->string_cate = "[" . rtrim($string_cate, ",") . "]";


