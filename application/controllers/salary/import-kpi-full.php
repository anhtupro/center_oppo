<?php


$month = $this->getRequest()->getParam('month', null);
$year = $this->getRequest()->getParam('year', null);

if(empty($month)||empty($year)){
    $month_last = strtotime ( '-1 month' , strtotime (date('Y-m-d')) ) ;
    
    $month = date ( 'm' , $month_last );  
    $year = date ( 'Y' , $month_last ); 
}

$params['month'] = $month;
$params['year'] = $year;
$this->view->params            = $params;