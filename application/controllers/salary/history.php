<?php

$flashMessenger = $this->_helper->flashMessenger;

$db     = Zend_Registry::get('db');
$export = $this->getRequest()->getParam('export');
$name   = $this->getRequest()->getParam('name', '');
$code   = $this->getRequest()->getParam('code', '');
$month  = $this->getRequest()->getParam('month', 0);
$year   = $this->getRequest()->getParam('year', 0);

if (empty($month) || empty($year)) {
    $month_last = strtotime('-1 month', strtotime(date('Y-m-d')));

    $month = date('m', $month_last);
    $year  = date('Y', $month_last);
} else {
    if (!is_numeric($month) || !is_numeric($year)) {
        echo "<pre>";
        print_r("Tháng năm không đúng định dạng");
        die;
    }
}
$params = array(
    'name'  => trim($name),
    'code'  => trim($code),
    'month' => intval($month),
    'year'  => intval($year),
);



$limit = 20;
$total = 0;
$desc  = $this->getRequest()->getParam('desc', 1);
$sort  = $this->getRequest()->getParam('sort');
$page  = $this->getRequest()->getParam('page', 1);

if ($export) {
    $limit = $page  = 0;
}

$upload_model = new Application_Model_MassUploadSalary();
$list         = $upload_model->fetchPagination($page, $limit, $total, $params);

if ($export) {
    $this->_exportInfo($list);
}
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

//$total            = $db->fetchOne('select @total');
$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$messages_error   = $flashMessenger->setNamespace('error')->getMessages();

$this->view->list             = $list;
$this->view->desc             = $desc;
$this->view->sort             = $sort;
$this->view->limit            = $limit;
$this->view->total            = $total;
$this->view->page             = $page;
$this->view->url              = HOST . 'salary/history' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset           = $limit * ($page - 1);
$this->view->params           = $params;
$this->view->messages_success = $messages_success;
$this->view->messages_error   = $messages_error;



$this->view->userStorage = $userStorage;



