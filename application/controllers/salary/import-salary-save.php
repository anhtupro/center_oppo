<?php

// config for template
define('START_ROW', 6);

define('STAFF_CODE', 0);
define('CO_BAN_LUONG_THAM_GIA_BH', 1); // cột B;
define('CHI_PHI_PHUC_VU_CONG_VIEC', 2); // cột C;
define('THAM_NIEN', 3); // cột D;
define('HO_TRO_CONG_VIEC', 4); // cột E;
define('NGAY_CONG_BINH_THUONG', 5); // cột F;
define('NGAY_CONG_CHU_NHAT', 6); // cột G;
define('NGAY_CONG_THU_7_TINH_LUONG_NHAN_DOI', 7); // cột H;
define('NGAY_NGHI_DUOC_HUONG_LUONG', 8); // cột I;
define('NGAY_CONG_DI_LAM_NGAY_LE', 9); // cột J;
define('NGAY_CONG_TRAINING', 10); // cột K;
define('PHEP_NAM', 11); // cột L;
define('UNG_PHEP_HOAN_TRA_PHEP_DO_NGHI_VIEC', 12); // cột M;
define('GIO_CONG_TANG_CA_BT', 13); // cột N;
define('GIO_CONG_CHU_NHAT', 14); // cột O;
define('THUONG_KHAC', 15); // cột P;
define('KPI_CHUYEN_PHAT_VTRI_KPI_SERVICE', 16); // cột Q;
define('BU_LUONG_CAC_KHOAN_CONG_KHAC', 17); // cột R;
define('CAC_KHOAN_TRU_KHAC', 18); // cột S;
define('TRUY_THU_BAO_HIEM_Y_TE_TRUY_DONG_NGUOC_BH', 19); // cột T;
define('QUYET_TOAN_THUE_TNCN', 20); // cột U;
define('TAM_UNG_LUONG', 21); // cột V;
define('SO_NGUOI_PHU_THUOC_TRONG_THANG', 22); // cột W;
define('LUONG_BHXH', 23); // cột X;
define('CONG_DOAN', 24); // cột Y;
define('LUONG_THANG_13', 25); // cột Z;
define('PHU_CAP_COM_SAU_DIEU_CHINH', 26); // cột AA;
define('PHU_CAP_DIEN_THOAI_SAU_DIEU_CHINH', 27); // cột AB;
define('PHU_CAP_XANG_XE_SAU_DIEU_CHINH', 28); // cột AC;
define('CHUC_DANH_TRUOC_DIEU_CHINH', 29); // cột AD;
define('PHU_CAP_COM_TRUOC_DIEU_CHINH', 30); // cột AE;
define('PHU_CAP_DIEN_THOAI_TRUOC_DIEU_CHINH', 31); // cột AF;
define('PHU_CAP_XANG_XE_TRUOC_DIEU_CHINH', 32); // cột AG;
define('NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC', 33); // cột AH;
define('NGAY_CONG_CA_GAY', 34); // cột AI;
define('KPI_PHONE_PGS_1', 35); // cột AJ;
define('KPI_PHONE_PGS_2', 36); // cột AK;
define('KPI_PHONE_PG_LEADER', 37); // cột AL;
define('KPI_PHONE_SALES', 38); // cột AM;
define('KPI_PHONE_LEADER', 39); // cột AN;
define('KPI_PHONE_MANAGEMENT', 40); // cột AO;
define('KPI_ACCE_SALE_PB_SALE', 41); // cột AP;
define('KPI_ACCE_PGPB', 42); // cột AQ;
define('KPI_ACCE_KA', 43); // cột AR;
define('KPI_ACCE_LEADER', 44); // cột AS;
define('KPI_DIGITAL', 45); // cột AT;
define('CP_CONG_THEM', 46); // cột AU;
define('KPI_BONUS', 47); // cột AV;
define('KPI_1_MIL', 48); // cột AW;
define('PASSBY_HR_COST', 49); // cột AX;
define('TGDD', 50); // cột AY;
define('KPI_AFTER', 51); // cột AZ;
define('NOTE', 52); // cột BA;
define('LUONG_TRUOC', 53); // cột BB;
define('NGAY_CONG_SAU_DIEU_CHINH_THU_VIEC', 54); // cột BC;
define('TOTAL_COM', 55); // cột BC;
define('TOTAL_XANG', 56); // cột BC;
define('TOTAL_PHONE', 57); // cột BC;



$this->_helper->layout->disableLayout();
$userStorage       = Zend_Auth::getInstance()->getStorage()->read();
$QStaff            = new Application_Model_Staff();
$QMassUploadSalary = new Application_Model_MassUploadSalary();

$created_at = date('Y-m-d H:i:s');
$created_by = $userStorage->id;
$session_id = time();

if ($this->getRequest()->getMethod() == 'POST') { // Big IF
    $month = $this->getRequest()->getParam('month');
    $year  = $this->getRequest()->getParam('year');

    $month_last  = strtotime('-1 month', strtotime(date('Y-m-d')));
    $valid_month = date('m', $month_last);
    $valid_year  = date('Y', $month_last);
    if ($month <= $month_last || $valid_year != $valid_year) {
//        $this->view->errors = "Chỉ được điều chỉnh lương của tháng " . $valid_month . "/" . $valid_year . " trở về trước trong năm hiện tại.";
//        return;
    }

    set_time_limit(0);
    ini_set('memory_limit', -1);

    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );

    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                . DIRECTORY_SEPARATOR . $file['folder'];

        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {

        $this->view->errors = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet         = $objPHPExcel->getSheet(0);
    $highestRow    = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $data_staff_code = [];

    /**
     * @author: hero
     * valid data
     * description
     * $date
     */
    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $rowData = isset($rowData[0]) ? $rowData[0] : array();

            if (empty($rowData[STAFF_CODE])) {
                throw new Exception("Empty STAFF_CODE, row: " . $row . ".Data:" . implode(",", $rowData));
            }
            if (empty($rowData[NOTE])) {

                throw new Exception("Empty NOTE, row: " . $row . ".Data:" . implode(",", $rowData));
            }

            $data_staff_code[] = trim($rowData[STAFF_CODE]);
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }

        // nothing here
    } // END loop through order rows


    $progress->flush(30);
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        if (!empty($data_staff_code)) {

            if (empty($month) || empty($year)) {
                throw new Exception("Vui lòng chọn Tháng và Năm.");
            }

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.code'))
                    ->where('p.code in (?)', $data_staff_code);
            $result_code = $db->fetchCol($select);

            if (count($data_staff_code) <> count($result_code)) {
                $diff = array_diff($data_staff_code, $result_code);
                throw new Exception("Nhân viên không tồn tại Code: " . implode($diff, ','));
            }


            for ($row = START_ROW; $row <= $highestRow; $row++) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = isset($rowData[0]) ? $rowData[0] : array();

                if (!empty($rowData[STAFF_CODE])) {


                    $staff_code               = trim($rowData[STAFF_CODE]);
                    $co_ban_luong_tham_gia_bh = $rowData[CO_BAN_LUONG_THAM_GIA_BH] ? intval($rowData[CO_BAN_LUONG_THAM_GIA_BH]) : 0;

                    $data_insert = array(
                        'staff_code'                                => trim($rowData[STAFF_CODE]),
                        'co_ban_luong_tham_gia_bh'                  => (trim($rowData[CO_BAN_LUONG_THAM_GIA_BH]) != NULL) ? intval($rowData[CO_BAN_LUONG_THAM_GIA_BH]) : NULL, // Cột B
                        'chi_phi_phuc_vu_cong_viec'                 => (trim($rowData[CHI_PHI_PHUC_VU_CONG_VIEC]) != NULL) ? intval($rowData[CHI_PHI_PHUC_VU_CONG_VIEC]) : NULL, // Cột C
                        'tham_nien'                                 => (trim($rowData[THAM_NIEN]) != NULL) ? intval($rowData[THAM_NIEN]) : NULL, // Cột D
                        'ho_tro_cong_viec'                          => (trim($rowData[HO_TRO_CONG_VIEC]) != NULL) ? intval($rowData[HO_TRO_CONG_VIEC]) : NULL, // Cột E
                        'ngay_cong_binh_thuong'                     => (trim($rowData[NGAY_CONG_BINH_THUONG]) != NULL) ? intval($rowData[NGAY_CONG_BINH_THUONG]) : NULL, // Cột F
                        'ngay_cong_chu_nhat'                        => (trim($rowData[NGAY_CONG_CHU_NHAT]) != NULL) ? intval($rowData[NGAY_CONG_CHU_NHAT]) : NULL, // Cột G
                        'ngay_cong_thu_7_tinh_luong_nhan_doi'       => (trim($rowData[NGAY_CONG_THU_7_TINH_LUONG_NHAN_DOI]) != NULL) ? intval($rowData[NGAY_CONG_THU_7_TINH_LUONG_NHAN_DOI]) : NULL, // Cột H
                        'ngay_nghi_duoc_huong_luong'                => (trim($rowData[NGAY_NGHI_DUOC_HUONG_LUONG]) != NULL) ? intval($rowData[NGAY_NGHI_DUOC_HUONG_LUONG]) : NULL, // Cột I
                        'ngay_cong_di_lam_ngay_le'                  => (trim($rowData[NGAY_CONG_DI_LAM_NGAY_LE]) != NULL) ? intval($rowData[NGAY_CONG_DI_LAM_NGAY_LE]) : NULL, // Cột J
                        'ngay_cong_training'                        => (trim($rowData[NGAY_CONG_TRAINING]) != NULL) ? intval($rowData[NGAY_CONG_TRAINING]) : NULL, // Cột K
                        'phep_nam'                                  => (trim($rowData[PHEP_NAM]) != NULL) ? intval($rowData[PHEP_NAM]) : NULL, // Cột L
                        'ung_phep_hoan_tra_phep_do_nghi_viec'       => (trim($rowData[UNG_PHEP_HOAN_TRA_PHEP_DO_NGHI_VIEC]) != NULL) ? intval($rowData[UNG_PHEP_HOAN_TRA_PHEP_DO_NGHI_VIEC]) : NULL, // Cột M
                        'gio_cong_tang_ca_bt'                       => (trim($rowData[GIO_CONG_TANG_CA_BT]) != NULL) ? intval($rowData[GIO_CONG_TANG_CA_BT]) : NULL, // Cột N
                        'gio_cong_chu_nhat'                         => (trim($rowData[GIO_CONG_CHU_NHAT]) != NULL) ? intval($rowData[GIO_CONG_CHU_NHAT]) : NULL, // Cột O
                        'thuong_khac'                               => (trim($rowData[THUONG_KHAC]) != NULL) ? intval($rowData[THUONG_KHAC]) : NULL, // Cột P
                        'kpi_chuyen_phat_vtri_kpi_service'          => (trim($rowData[KPI_CHUYEN_PHAT_VTRI_KPI_SERVICE]) != NULL) ? intval($rowData[KPI_CHUYEN_PHAT_VTRI_KPI_SERVICE]) : NULL, // Cột Q
                        'bu_luong_cac_khoan_cong_khac'              => (trim($rowData[BU_LUONG_CAC_KHOAN_CONG_KHAC]) != NULL) ? intval($rowData[BU_LUONG_CAC_KHOAN_CONG_KHAC]) : NULL, // Cột R
                        'cac_khoan_tru_khac'                        => (trim($rowData[CAC_KHOAN_TRU_KHAC]) != NULL) ? intval($rowData[CAC_KHOAN_TRU_KHAC]) : NULL, // Cột S
                        'truy_thu_bao_hiem_y_te_truy_dong_nguoc_bh' => (trim($rowData[TRUY_THU_BAO_HIEM_Y_TE_TRUY_DONG_NGUOC_BH]) != NULL) ? intval($rowData[TRUY_THU_BAO_HIEM_Y_TE_TRUY_DONG_NGUOC_BH]) : NULL, // Cột T
                        'quyet_toan_thue_tncn'                      => (trim($rowData[QUYET_TOAN_THUE_TNCN]) != NULL) ? intval($rowData[QUYET_TOAN_THUE_TNCN]) : NULL, // Cột U
                        'tam_ung_luong'                             => (trim($rowData[TAM_UNG_LUONG]) != NULL) ? intval($rowData[TAM_UNG_LUONG]) : NULL, // Cột V
                        'so_nguoi_phu_thuoc_trong_thang'            => (trim($rowData[SO_NGUOI_PHU_THUOC_TRONG_THANG]) != NULL) ? intval($rowData[SO_NGUOI_PHU_THUOC_TRONG_THANG]) : NULL, // Cột W
                        'luong_bhxh'                                => (trim($rowData[LUONG_BHXH]) != NULL) ? intval($rowData[LUONG_BHXH]) : NULL, // Cột X
                        'cong_doan'                                 => (trim($rowData[CONG_DOAN]) != NULL) ? intval($rowData[CONG_DOAN]) : NULL, // Cột Y
                        'luong_thang_13'                            => (trim($rowData[LUONG_THANG_13]) != NULL) ? intval($rowData[LUONG_THANG_13]) : NULL, // Cột Z
                        'phu_cap_com_sau_dieu_chinh'                => (trim($rowData[PHU_CAP_COM_SAU_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_COM_SAU_DIEU_CHINH]) : NULL, // Cột AA
                        'phu_cap_dien_thoai_sau_dieu_chinh'         => (trim($rowData[PHU_CAP_DIEN_THOAI_SAU_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_DIEN_THOAI_SAU_DIEU_CHINH]) : NULL, // Cột AB
                        'phu_cap_xang_xe_sau_dieu_chinh'            => (trim($rowData[PHU_CAP_XANG_XE_SAU_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_XANG_XE_SAU_DIEU_CHINH]) : NULL, // Cột AC
                        'chuc_danh_truoc_dieu_chinh'                => (trim($rowData[CHUC_DANH_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[CHUC_DANH_TRUOC_DIEU_CHINH]) : NULL, // Cột AD
                        'phu_cap_com_truoc_dieu_chinh'              => (trim($rowData[PHU_CAP_COM_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_COM_TRUOC_DIEU_CHINH]) : NULL, // Cột AE
                        'phu_cap_dien_thoai_truoc_dieu_chinh'       => (trim($rowData[PHU_CAP_DIEN_THOAI_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_DIEN_THOAI_TRUOC_DIEU_CHINH]) : NULL, // Cột À
                        'phu_cap_xang_xe_truoc_dieu_chinh'          => (trim($rowData[PHU_CAP_XANG_XE_TRUOC_DIEU_CHINH]) != NULL) ? intval($rowData[PHU_CAP_XANG_XE_TRUOC_DIEU_CHINH]) : NULL, // Cột AG
                        'ngay_cong_truoc_dieu_chinh_thu_viec'       => (trim($rowData[NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC]) != NULL) ? $rowData[NGAY_CONG_TRUOC_DIEU_CHINH_THU_VIEC] : NULL, // Cột AH
                        'ngay_cong_ca_gay'                          => (trim($rowData[NGAY_CONG_CA_GAY]) != NULL) ? intval($rowData[NGAY_CONG_CA_GAY]) : NULL, // Cột AI
                        'kpi_phone_pgs_1'                           => (trim($rowData[KPI_PHONE_PGS_1]) != NULL) ? intval($rowData[KPI_PHONE_PGS_1]) : NULL, // Cột AJ
                        'kpi_phone_pgs_2'                           => (trim($rowData[KPI_PHONE_PGS_2]) != NULL) ? intval($rowData[KPI_PHONE_PGS_2]) : NULL, // Cột AK
                        'kpi_phone_pg_leader'                       => (trim($rowData[KPI_PHONE_PG_LEADER]) != NULL) ? intval($rowData[KPI_PHONE_PG_LEADER]) : NULL, // Cột AL
                        'kpi_phone_sales'                           => (trim($rowData[KPI_PHONE_SALES]) != NULL) ? intval($rowData[KPI_PHONE_SALES]) : NULL, // Cột AM
                        'kpi_phone_leader'                          => (trim($rowData[KPI_PHONE_LEADER]) != NULL) ? intval($rowData[KPI_PHONE_LEADER]) : NULL, // Cột AN
                        'kpi_phone_management'                      => (trim($rowData[KPI_PHONE_MANAGEMENT]) != NULL) ? intval($rowData[KPI_PHONE_MANAGEMENT]) : NULL, // Cột AO
                        'kpi_acce_sale_pb_sale'                     => (trim($rowData[KPI_ACCE_SALE_PB_SALE]) != NULL) ? intval($rowData[KPI_ACCE_SALE_PB_SALE]) : NULL, // Cột AP
                        'kpi_acce_pgpb'                             => (trim($rowData[KPI_ACCE_PGPB]) != NULL) ? intval($rowData[KPI_ACCE_PGPB]) : NULL, // Cột AQ
                        'kpi_acce_ka'                               => (trim($rowData[KPI_ACCE_KA]) != NULL) ? intval($rowData[KPI_ACCE_KA]) : NULL, // Cột AR
                        'kpi_acce_leader'                           => (trim($rowData[KPI_ACCE_LEADER]) != NULL) ? intval($rowData[KPI_ACCE_LEADER]) : NULL, // Cột AS
                        'kpi_digital'                               => (trim($rowData[KPI_DIGITAL]) != NULL) ? intval($rowData[KPI_DIGITAL]) : NULL, // Cột AT
                        'cp_cong_them'                              => (trim($rowData[CP_CONG_THEM]) != NULL) ? intval($rowData[CP_CONG_THEM]) : NULL, // Cột AU
                        'kpi_bonus'                                 => (trim($rowData[KPI_BONUS]) != NULL) ? intval($rowData[KPI_BONUS]) : NULL, // Cột AV
                        'kpi_1_mil'                                 => (trim($rowData[KPI_1_MIL]) != NULL) ? $rowData[KPI_1_MIL] : NULL, // Cột AX
                        'passby_hr_cost'                            => (trim($rowData[PASSBY_HR_COST]) != NULL) ? intval($rowData[PASSBY_HR_COST]) : NULL, // Cột AW
                        'kpi_is_tgdd'                               => (trim($rowData[TGDD]) != NULL) ? intval($rowData[TGDD] == "TGDĐ" ? 1 : 0) : NULL, // Cột AY
                        'kpi_after'                                 => (trim($rowData[KPI_AFTER]) != NULL) ? $rowData[KPI_AFTER] : NULL, // Cột AY
                        'note'                                      => (trim($rowData[NOTE]) != NULL) ? $rowData[NOTE] : NULL, // Cột AY
                        'luong_truoc'                               => (trim($rowData[LUONG_TRUOC]) != NULL) ? $rowData[LUONG_TRUOC] : NULL, // Cột AY
                        'ngay_cong_sau_dieu_chinh_thu_viec'         => (trim($rowData[NGAY_CONG_SAU_DIEU_CHINH_THU_VIEC]) != NULL) ? $rowData[NGAY_CONG_SAU_DIEU_CHINH_THU_VIEC] : NULL, // Cột AY
                        'total_com'                                 => (trim($rowData[TOTAL_COM]) != NULL) ? $rowData[TOTAL_COM] : NULL, // Cột AY
                        'total_xang'                                => (trim($rowData[TOTAL_XANG]) != NULL) ? $rowData[TOTAL_XANG] : NULL, // Cột AY
                        'total_phone'                               => (trim($rowData[TOTAL_PHONE]) != NULL) ? $rowData[TOTAL_PHONE] : NULL, // Cột AY
                        'month'                                     => $month,
                        'year'                                      => $year,
                        'created_by'                                => $userStorage->id,
                        'created_at'                                => date('Y-m-d H:i:s'),
                        'session_id'                                => $session_id
                    );

                    $QMassUploadSalary->insert($data_insert);

                    $percent = round($row * 70 / count($data_staff_code), 1);
                    $progress->flush($percent);
                }
            }// end loop
        } else {
            throw new Exception("Danh sách nhân viên không hợp lệ");
        }
        $db->commit();
        $progress->flush(100);
        $this->view->success = "Bảng điều chỉnh lương tháng " . $month . "/" . $year . " đã Import thành công.";

        $QLog = new Application_Model_Log();
        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = "Salary mass upload data";
        //todo log
        $QLog->insert(array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        ));
    } catch (Exception $e) {
        $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}