<?php
    class KpiSaleController extends My_Controller_Action{
        public function init(){

        }
        public function indexAction(){
            require_once 'kpi-sale' .DIRECTORY_SEPARATOR . 'index.php';
        }
        public function createProductSaleAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'create-product-sale.php';
        }
        public function saveProductSaleAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'save-product-sale.php';
        }
        public function deleteProductSaleAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'delete-product-sale.php';
        }
        public function analyticsAreaSaleAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'analytics-area-sale.php';
        }
        public function analyticsDetailAreaAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'analytics-detail-area.php';
        }
        public function analyticsDetailStaffAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'analytics-detail-staff.php';
        }
        public function analyticsDetailProductAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'analytics-detail-product.php';
        }
        public function importFileExcelAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'import-file-excel.php';
        }
        public function createTemplateExcelAction(){
            require_once 'kpi-sale' . DIRECTORY_SEPARATOR . 'create-template-excel.php';
        }
        
        
    }
?>