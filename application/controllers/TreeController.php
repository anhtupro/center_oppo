<?php

class TreeController extends My_Controller_Action {
    protected $_flashMessenger = null;

    public function init()
    {

        $this->_redirector = $this->_helper->getHelper('Redirector');

        $this->_flashMessenger = $this->_helper->flashMessenger;
        $messages = $this->_flashMessenger->setNamespace('success')->getMessages();
        $this->view->message_success = $messages;
        $messages_error = $this->_flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages_error;
    }

    public function indexAction() {
        $QTeamNew = new Application_Model_TeamNew();
        $QOrgTree = new Application_Model_OrgTree();
        $result = $QOrgTree->getListTree(null,0);
        $result_department = $QTeamNew->getListWithType(3);
        $this->view->list_tree = $result;
        $this->view->list_department = $result_department;
//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        die();
        //$this->_helper->layout->disableLayout();
    }

    public function childTreeAction() {
        if ($this->getRequest()->isGet()) {
            $flag_id = $this->getRequest()->getParam('flag');
            if (!empty($flag_id) && isset($flag_id)) {
                $status = $this->getRequest()->getPost('status');
                $QOrgTree = new Application_Model_OrgTree();
                $QTeamNew = new Application_Model_TeamNew();
                $QTree = new Application_Model_Tree();
                $QOrgTreeLog = new Application_Model_TreeLog();
                $QStaff=new Application_Model_Staff();
                $QDetailOrgTree = new Application_Model_DetailOrgTree();
                $result = $QOrgTree->getListTree($flag_id,1);
                
                $id_label = $QDetailOrgTree->getIdTitleByLevelChartAndFlag($flag_id, 0);
                $result_team_new = $QTeamNew->getListTeam($id_label);
                $result_label = $QTree->fetchOneData($flag_id);

                $listOrgTreeLog = $QOrgTreeLog->fetchAll(["flag_org = ? " => $flag_id]);
                $staff=$QStaff->get_cache();
                $this->view->data_org = $result;
                $this->view->main_title = $result_label['name'];
                $this->view->content = $result_label['content'];
                $this->view->team_new = $result_team_new;
                $this->view->flag_id = $flag_id;
                $this->view->listOrgTreeLog = $listOrgTreeLog;
                $this->view->staff = $staff;
            }
        }
        //$this->_helper->layout->disableLayout();
    }

    public function ajaxSearchStaffAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $input = $this->getRequest()->getParam("input");
            $QStaffPermission = new Application_Model_OrgTree();
            $data = $QStaffPermission->getStaff($input);
            echo json_encode($data);
            die();
        }
    }

    function addNodeAction() {
        $QOrgTree = new Application_Model_OrgTree();
        $QDetailOrgTree = new Application_Model_DetailOrgTree();
        if ($this->getRequest()->isPost()) {
            $flag_id = $this->getRequest()->getParam("flag");
            $type_node = $this->getRequest()->getParam("type_node");
            $id_parent_node = $this->getRequest()->getParam("parent_node");
            if (empty($id_parent_node)) {
                $id_parent_node = 0;
            }
            $label_node = $this->getRequest()->getParam("label_node");
            $color_node_post = $this->getRequest()->getParam("color_node");
            $list_staff_node = $this->getRequest()->getParam("staff_add_node");
            $input_title_node = $this->getRequest()->getParam("input-title-group");
            $color_node = "";
            if ($type_node != 2) {
                $color_node = $color_node_post;
//                $QTeamNew = new Application_Model_TeamNew();
//                $result = $QTeamNew->getTypeNameLabel($label_node);
//                if (strpos(strtolower($result[0]["name"]), "assistant") !== false || strpos(strtolower($result[0]["name"]), "admin") !== false) {
//                    $color_node = "color_ad_assist";
//                } else {
//                    switch ($result[0]["type"]) {
//                        case 0: {
//                                $color_node = "group";
//                                break;
//                            }
//                        case 1: {
//                                $color_node = "company";
//                                break;
//                            }
//                        case 2: {
//                                $color_node = "devision";
//                                break;
//                            }
//                        case 3: {
//                                $color_node = "department";
//                                break;
//                            }
//                        case 4: {
//                                $color_node = "team";
//                                break;
//                            }
//                        case 5: {
//                                $color_node = "title";
//                                break;
//                            }
//                    }
//                }
            } else {
                $color_node = "color_group";
            }
            //Zend_Db_Adapter_Abstract::beginTransaction();
            //1 : level_chart
            $id_last = $QOrgTree->addNodeTree($color_node, $id_parent_node, $flag_id,1);
            if ($type_node != 2) {
                if (!empty($list_staff_node)) {
                    foreach ($list_staff_node as $value) {
                        $data_param = array(
                            'id_org_tree' => $id_last,
                            'id_label' => $label_node,
                            'staff_id' => $value,
                        );
                        $QDetailOrgTree->insert($data_param);
                    }
                } else {
                    $data_param = array(
                        'id_org_tree' => $id_last,
                        'id_label' => $label_node,
                        'staff_id' => 0,
                    );
                    $QDetailOrgTree->insert($data_param);
                }
            } else {
                $data_param = array(
                    'id_org_tree' => $id_last,
                    'group' => $input_title_node,
                );
                $QDetailOrgTree->insert($data_param);
            }
            //Zend_Db_Adapter_Abstract::commit();
            //add db

            $this->savingLog($flag_id);

            $this->_redirect('/tree/child-tree?flag=' . $flag_id);
        }
        //$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    function deleteNodeAction() {
        $QOrgTree = new Application_Model_OrgTree();
        if ($this->getRequest()->isPost()) {
            $flag_id = $this->getRequest()->getParam("flag_id");
            $id_node = $this->getRequest()->getParam("idnode");
            $QOrgTree->deleteNode($flag_id, $id_node,1);
            $data = array(
                "status" => true
            );
            $this->savingLog($flag_id);
            echo(json_encode($data));
        }
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    function checkSameLabelAction() {
        if ($this->getRequest()->isPost()) {
            $id_label = $this->getRequest()->getParam("id_label");
            $QDetailOrgTree = new Application_Model_DetailOrgTree();
            $data = array(
                "status" => false
            );
            if (count($QDetailOrgTree->getListDataWithLabel(1,$id_label)) > 0) {
                $data = array(
                    "status" => true
                );
            }
            echo (json_encode($data));
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
        }
    }

    function loadDataNodeAction() {
        if ($this->getRequest()->isPost()) {
            
        }
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    function getDataEditAction() {
        if ($this->getRequest()->isPost()) {
            $id_node = $this->getRequest()->getParam("id_node");
            $QDetailOrgTree = new Application_Model_DetailOrgTree();
            $node_child = $QDetailOrgTree->getListDataWithIdOrg($id_node);
            echo json_encode($node_child);
        }
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    function editNodeAction() {
        $QDetailOrgTree = new Application_Model_DetailOrgTree();
        if ($this->getRequest()->isPost()) {
            $flag_id = $this->getRequest()->getParam("flag");
            $id_node_edit = $this->getRequest()->getParam("node-edit");
            $id_title = $this->getRequest()->getParam("input-title-edit-temp");
            //list staff new
            $staff_node = array();
            $staff_node = $this->getRequest()->getParam("staff_edit_node");
            $input_group_edit = $this->getRequest()->getParam("input-group-edit");
            if ($input_group_edit == "") {
                //list staff old
                $list_staff_id_temp = $QDetailOrgTree->getListIdStaff($id_node_edit);
                $list_staff_id = array();
                foreach ($list_staff_id_temp as $value) {
                    array_push($list_staff_id, $value['staff_id']);
                }
                $list_staff_delete = array_diff($list_staff_id, $staff_node);
                $flag_del = false;
                if (count($staff_node) < 1){
                    $list_staff_delete = $list_staff_id;
                    $flag_del = true;
                }
                else
                    $list_staff_delete = array_diff($list_staff_id, $staff_node);

                if (count($list_staff_id) < 1)
                    $list_staff_add = $staff_node;
                else
                    $list_staff_add = array_diff($staff_node, $list_staff_id);

                //print_r($list_staff_delete);die();
                //Zend_Db_Adapter_Abstract::beginTransaction();
                foreach ($list_staff_delete as $staff_id) {
                    $QDetailOrgTree->deleteStaffByOrgTree($id_node_edit, $staff_id);
                }
                if($flag_del){
                    $QDetailOrgTree->addDetailOrgTree($id_node_edit, $id_title, 0);
                }
               
                foreach ($list_staff_add as $staff_id) {
                    $QDetailOrgTree->addDetailOrgTree($id_node_edit, $id_title, $staff_id);
                }
                //Zend_Db_Adapter_Abstract::commit();
            } else {
                $QDetailOrgTree->updateGroup($id_node_edit, $input_group_edit);
            }
            $this->_redirect('/tree/child-tree?flag=' . $flag_id);
        }
        $this->savingLog($flag_id);
        $this->_helper->viewRenderer->setNoRender();
    }
    
    function editContentAction(){
        $QTree = new Application_Model_Tree();
        if ($this->getRequest()->isPost()) {
            $flag_id = $this->getRequest()->getParam("flag");
            $content = $this->getRequest()->getParam("content-tree");
            $auth = Zend_Auth::getInstance()->getStorage()->read();
            $id = $auth->id;
            $QTree->updateContent($flag_id,$content,date("Y-m-d H:i:s"),$id);
            $this->_redirect('/tree/child-tree?flag=' . $flag_id);
        }
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }
    
    function getListTeamTitleAction(){
        if ($this->getRequest()->isPost()) {
            $QTeamNew = new Application_Model_TeamNew();
            $department_id = $this->getRequest()->getParam("department_id");
            echo json_encode($QTeamNew->getTeamAndTitleByDepartment($department_id));
        }
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }
//    TOAN
    function savingLog($flag_id)
    {
        $QOrgTree = new Application_Model_OrgTree();
        $QOrgTreeLog = new Application_Model_TreeLog();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

//        Saving Log
        $whereTree = $QOrgTree->getAdapter()->quoteInto("flag_org_tree = ?", $flag_id);
        $listTreeLog = $QOrgTree->fetchAll($whereTree);
        $listDetailOrgTreeLog = $QOrgTree->getDetailTreeByFlag($flag_id);
        $dataTreeLog = array(
            "org_tree_old" => json_encode($listTreeLog->toArray()),
            "detail_org_tree_old" =>  json_encode($listDetailOrgTreeLog),
            "flag_org" => $flag_id,
            "created_by" => $userStorage->id,
            "created_at" => date("Y-m-d H:i:s"),
        );
        $QOrgTreeLog->insert($dataTreeLog);
        //
    }
    function rollbackAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();


        $id = $this->getRequest()->getParam("id_rollback");
        $flag_id = $this->getRequest()->getParam("id_flag");


        $QOrgTree = new Application_Model_OrgTree();
        $QDetailTree = new Application_Model_DetailOrgTree();

        $QOrgTreeLog = new Application_Model_TreeLog();
        try {
//        Deleting all in Tree with Flag
            $where = null;
            $where = $QOrgTree->getAdapter()->quoteInto("flag_org_tree = ?", $flag_id);
            $listTree = $QOrgTree->fetchAll($where);
            $QOrgTree->delete($where);
            foreach ($listTree as $key => $value) {
                $where = null;
                $where = $QDetailTree->getAdapter()->quoteInto("id_org_tree =? ", $value["id"]);
                $QDetailTree->delete($where);
            }
//        Rollback-Insert after deleting
            $rollbackLogTree = $QOrgTreeLog->fetchRow(["id=?" => $id]);
            $listRollbackTree = json_decode($rollbackLogTree->org_tree_old, true);
            foreach ($listRollbackTree as $key => $object) {
                $QOrgTree->insert($object);
            }
            $listRollbackDetailTree = json_decode($rollbackLogTree->detail_org_tree_old, true);

            foreach ($listRollbackDetailTree as $key => $object) {
                $QDetailTree->insert($object);
            }
            echo true;
        } catch (Exception $e) {
            echo $e;
        }
    }
    
    //cong danh
    protected $_redirector = null;


    public function manageTeamNewAction()
    {
        $QTeamNew=new Application_Model_TeamNew();
        $listGroup=$QTeamNew->fetchGroup();
        $listCompany=$QTeamNew->fetchCompany();
        $listDivision=$QTeamNew->fetchDivision();

        $this->view->listGroup=$listGroup;
        $this->view->listCompany=$listCompany;
        $this->view->listDivision=$listDivision;
    }

    public function getGroupAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $QTeam=new Application_Model_Team();
        $group=$QTeam->getGroup();
        if (!empty($group)) {
            echo json_encode([
                'status' => 1,
                'data' => $group
            ]);
        }
        else echo json_encode(['status' => 0]);
    }

    public function getOldDepartmentAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $QTeam=new Application_Model_Team();
        $listOldDepartment=$QTeam->getAllDepartment();
        if (!empty($listOldDepartment)) {
            echo json_encode([
                'status' => 1,
                'data' => $listOldDepartment
            ]);
        }
        else echo json_encode(['status' => 0]);
    }

    public function getCompanyOfGroupAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->_request->getParams();
        $groupId=$params['groupId'];

        $QTeamNew=new Application_Model_TeamNew();

        if ($groupId && !empty($groupId)) {
            $listCompany=$QTeamNew->fetchCompanyOfGroup($groupId);
        }
        if (!empty($listCompany)) {
            echo json_encode([
                'status' => 1,
                'data' => $listCompany
            ]);
        } else
            echo json_encode([
                    'status' => 0
                ]
            );
    }

    public function getDivisionOfCompanyAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->_request->getParams();
        $companyId=$params['companyId'];

        $QTeamNew=new Application_Model_TeamNew();

        if ($companyId && !empty($companyId)) {
            $listDivision=$QTeamNew->fetchDivisionOfCompany($companyId);
        }
        if (!empty($listDivision)) {
            echo json_encode([
                'status' => 1,
                'data' => $listDivision
            ]);
        } else
            echo json_encode([
                'status' => 0
            ]);
    }


    public function getDepartmentOfDivisionAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->_request->getParams();
        $divisionId=$params['divisionId'];

        $QTeamNew=new Application_Model_TeamNew();

        if ($divisionId && !empty($divisionId)) {
            $listDepartment=$QTeamNew->fetchDepartmentOfDivision($divisionId);
        }
        if (!empty($listDepartment)) {
            echo json_encode([
                'status' => 1,
                'data' => $listDepartment
            ]);
        } else
            echo json_encode([
                'status' => 0
            ]);
    }

    public function getTeamOfDepartmentAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->_request->getParams();
        $departmentId=$params['departmentId'];

        $QTeamNew=new Application_Model_TeamNew();

        if ($departmentId && !empty($departmentId)) {
            $listTeam=$QTeamNew->fetchTeamOfDepartment($departmentId);
        }
        if (!empty($listTeam)) {
            echo json_encode([
                'status' => 1,
                'data' => $listTeam
            ]);
        } else
            echo json_encode([
                'status' => 0
            ]);
    }

    public function getTitleOfTeamAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->_request->getParams();
        $teamId=$params['teamId'];

        $QTeamNew=new Application_Model_TeamNew();

        if ($teamId && !empty($teamId)) {
            $listTitle=$QTeamNew->fetchTitleOfTeam($teamId);
        }
        if (!empty($listTitle)) {
            echo json_encode([
                'status' => 1,
                'data' => $listTitle
            ]);
        } else
            echo json_encode([
                'status' => 0
            ]);
    }

    public function getOldTeamByDepartmentAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->_request->getParams();
        $oldDepartmentId=$params['oldDepartmentId'];

        $QTeam=new Application_Model_Team();

        if ($oldDepartmentId && !empty($oldDepartmentId)) {
            $listTeam=$QTeam->getTeamInDepartment($oldDepartmentId);
        }
        if (!empty($listTeam)) {
            echo json_encode([
                'status' => 1,
                'data' => $listTeam
            ]);
        } else
            echo json_encode([
                'status' => 0
            ]);
    }

    public function getOldTitleByTeamAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->_request->getParams();
        $oldTeamId=$params['oldTeamId'];

        $QTeam=new Application_Model_Team();

        if ($oldTeamId && !empty($oldTeamId)) {
            $listTitle=$QTeam->getTitleByTeam($oldTeamId);
        }
        if (!empty($listTitle)) {
            echo json_encode([
                'status' => 1,
                'data' => $listTitle
            ]);
        } else
            echo json_encode([
                'status' => 0
            ]);
    }

    public function addGroupAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('group_name');
        $data = array(
            'name' => $name,
            'parent_id' => 0,
            'team_id' => 0,
            'type' => 0,
        );

        $QTeamNew=new Application_Model_TeamNew();
        $QTeamNew->insert($data);

        $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

    public function editGroupAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('groupName');
        $id    = $this->getRequest()->getParam('groupId');

        $data = array(
            'name' => $name,
        );
        $where = array('id = ?' => $id);

        $QTeamNew=new Application_Model_TeamNew();

        $QTeamNew->update($data, $where);

        $this->_flashMessenger->setNamespace('success')->addMessage('Edited Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

    public function deleteGroupAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //delete for this group
        $QTeamNew = new Application_Model_TeamNew();
        $groupId=$this->getRequest()->getParam('groupId');
        $where = array('id = ?' => $groupId);
        $data = array(
            'del' =>1 ,
        );

        $QTeamNew->update($data, $where);

        //get id of chid this group
        $this->getChildIdAction($groupId,$childIds);
        if(count($childIds)){
            $where = array('id IN (?)'=>$childIds);
            //delete for child of this team
            $QTeamNew->update($data, $where);
        }


        $this->_flashMessenger->setNamespace('success')->addMessage('Deleted Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

//    company section

    public function addCompanyAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('companyName');
        $groupId    = $this->getRequest()->getParam('groupId');

        $data = array(
            'name' => $name,
            'parent_id' => $groupId,
            'team_id' => 0,
            'type' => 1,
        );

        $QTeamNew=new Application_Model_TeamNew();
        $QTeamNew->insert($data);

        $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

    public function editCompanyAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('companyName');
        $id    = $this->getRequest()->getParam('companyId');

        $data = array(
            'name' => $name,
        );
        $where = array('id = ?' => $id);

        $QTeamNew=new Application_Model_TeamNew();

        $QTeamNew->update($data, $where);

        $this->_flashMessenger->setNamespace('success')->addMessage('Edited Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

    public function deleteCompanyAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //delete for this company
        $QTeamNew = new Application_Model_TeamNew();
        $companyId=$this->getRequest()->getParam('companyId');
        $where = array('id = ?' => $companyId);
        $data = array(
            'del' =>1 ,
        );

        $QTeamNew->update($data, $where);

        //get id of chid this company
        $this->getChildIdAction($companyId,$childIds);
        if(count($childIds)){
            $where = array('id IN (?)'=>$childIds);
            //delete for child of this team
            $QTeamNew->update($data, $where);
        }

        $this->_flashMessenger->setNamespace('success')->addMessage('Deleted Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    //end company section

    //division section
    public function addDivisionAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('divisionName');
        $companyId    = $this->getRequest()->getParam('companyId');

        $data = array(
            'name' => $name,
            'parent_id' => $companyId,
            'team_id' => 0,
            'type' => 2,
        );

        $QTeamNew=new Application_Model_TeamNew();
        $QTeamNew->insert($data);

        $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

    public function editDivisionAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('divisionName');
        $id    = $this->getRequest()->getParam('divisionId');

        $data = array(
            'name' => $name,
        );
        $where = array('id = ?' => $id);

        $QTeamNew=new Application_Model_TeamNew();

        $QTeamNew->update($data, $where);
        $this->_flashMessenger->setNamespace('success')->addMessage('Edited Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    public function deleteDivisionAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //delete for this division
        $QTeamNew = new Application_Model_TeamNew();
        $divisionId=$this->getRequest()->getParam('divisionId');
        $where = array('id = ?' => $divisionId);
        $data = array(
            'del' =>1 ,
        );

        $QTeamNew->update($data, $where);

        //get id of chid this division
        $this->getChildIdAction($divisionId,$childIds);
        if(count($childIds)){
            $where = array('id IN (?)'=>$childIds);
            //delete for child of this team
            $QTeamNew->update($data, $where);
        }
        $this->_flashMessenger->setNamespace('success')->addMessage('Deleted Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

    //    end division section

    //department section
    public function addDepartmentAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('departmentName');
        $divisionId    = $this->getRequest()->getParam('divisionId');
        $oldDepartmentId=$this->getRequest()->getParam('oldDepartmentId');

        $data = array(
            'name' => $name,
            'parent_id' => $divisionId,
            'team_id' => $oldDepartmentId,
            'type' => 3,
        );

        $QTeamNew=new Application_Model_TeamNew();
        $QTeamNew->insert($data);

        $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    public function editDepartmentAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('departmentName');
        $id    = $this->getRequest()->getParam('departmentId');
        $oldDepartmentId=$this->getRequest()->getParam('oldDepartmentId');
        $data = array(
            'name' => $name,
            'team_id'=>$oldDepartmentId,
        );
        $where = array('id = ?' => $id);

        $QTeamNew=new Application_Model_TeamNew();

        $QTeamNew->update($data, $where);

        $this->_flashMessenger->setNamespace('success')->addMessage('Edited Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    public function deleteDepartmentAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //delete for this department
        $QTeamNew = new Application_Model_TeamNew();
        $departmentId=$this->getRequest()->getParam('departmentId');
        $where = array('id = ?' => $departmentId);
        $data = array(
            'del' =>1 ,
        );
        $QTeamNew->update($data, $where);

        //get id of chid this department
        $this->getChildIdAction($departmentId,$childIds);
        if(count($childIds)){
            $where = array('id IN (?)'=>$childIds);
            //delete for child of this team
            $QTeamNew->update($data, $where);
        }

        $this->_flashMessenger->setNamespace('success')->addMessage('Deleted Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    //end department section

    //team section
    public function addTeamAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('teamName');
        $departmentId    = $this->getRequest()->getParam('departmentId');
        $teamId    = $this->getRequest()->getParam('oldTeamId');

        $data = array(
            'name' => $name,
            'parent_id' => $departmentId,
            'team_id' => $teamId,
            'type' => 4,
        );

        $QTeamNew=new Application_Model_TeamNew();
        $QTeamNew->insert($data);

        $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    public function editTeamAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('teamName');
        $id    = $this->getRequest()->getParam('teamId');
        $oldTeamId=$this->getRequest()->getParam('oldTeamId');
        $data = array(
            'name' => $name,
            'team_id'=>$oldTeamId,
        );
        $where = array('id = ?' => $id);

        $QTeamNew=new Application_Model_TeamNew();

        $QTeamNew->update($data, $where);

        $this->_flashMessenger->setNamespace('success')->addMessage('Edited Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    public function deleteTeamAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //delete for this team
        $QTeamNew = new Application_Model_TeamNew();
        $teamId=$this->getRequest()->getParam('teamId');
        $where = array('id = ?' => $teamId);
        $data = array(
            'del' =>1 ,
        );

        $QTeamNew->update($data, $where);

        //get id of chid this team
        $this->getChildIdAction($teamId,$childIds);
        if(count($childIds)){
            $where = array('id IN (?)'=>$childIds);
            //delete for child of this team
            $QTeamNew->update($data, $where);
        }

        $this->_flashMessenger->setNamespace('success')->addMessage('Deleted Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    //end team section

    //title section
    public function addTitleAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('titleName');
        $teamId    = $this->getRequest()->getParam('teamId');
        $oldTitleId=$this->getRequest()->getParam('oldTitleId');

        $data = array(
            'name' => $name,
            'parent_id' => $teamId,
            'team_id' => $oldTitleId,
            'type' => 5,
        );

        $QTeamNew=new Application_Model_TeamNew();
        $QTeamNew->insert($data);

        $this->_flashMessenger->setNamespace('success')->addMessage('Added Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    public function editTitleAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name    = $this->getRequest()->getParam('titleName');
        $id    = $this->getRequest()->getParam('titleId');
        $oldTitleId=$this->getRequest()->getParam('oldTitleId');
        $data = array(
            'name' => $name,
            'team_id'=>$oldTitleId,
        );
        $where = array('id = ?' => $id);

        $QTeamNew=new Application_Model_TeamNew();

        $QTeamNew->update($data, $where);

        $this->_flashMessenger->setNamespace('success')->addMessage('Edited Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }
    public function deleteTitleAction($teamId){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $titleId=$this->getRequest()->getParam('titleId');

        $QTeamNew = new Application_Model_TeamNew();

        $data = array(
            'del' =>1 ,
        );

        $where = array('id=?',$titleId);

        $QTeamNew->update($data, $where);

        $this->_flashMessenger->setNamespace('success')->addMessage('Deleted Success!');

        return $this->_redirector->gotoUrl('tree/manage-team-new');
    }

    public function getChildIdAction($parentId,&$arr = NULL){

        if(!$arr) $arr = array();
        $QTeamNew=new Application_Model_TeamNew();

        foreach ($QTeamNew->fetchChildId($parentId) as $row){
            $arr[] = $row['id'];
            $arr = $this->getChildIdAction($row['id'],$arr);
        }
        return $arr;
    }


    public function exportAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $QTeamNew=new Application_Model_TeamNew;
        // no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'Id',
            'Name',
            'Parent ID',
            'Type',
            'Team ID',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index    = 2;
        $intCount = 1;

        $data = $QTeamNew->fetchAllData();

        try {
            if ($data)
                foreach ($data as $_key => $_order) {

                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['parent_id'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['type'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['team_id'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

                    $index++;
                }
        } catch (exception $e) {
            exit;
        }

        $filename  = 'Team_New' . date('Y_m_d');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
    //end cong danh

}

?>