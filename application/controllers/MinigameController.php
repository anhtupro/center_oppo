<?php
class MinigameController extends My_Controller_Action
{

	public function indexAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'index.php';
	}
	public function playAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'play.php';
	}
	public function submitQuestionAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'submit-question.php';
	}
	public function getQuestionAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'get-question.php';
	}
	public function resultAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'result.php';
	}
	public function rewardAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'reward.php';
	}
    public function listRewardAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'list-reward.php';
    }
    public function listAnswerAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'list-answer.php';
    }
    public function rotationAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'rotation.php';
    }
    public function rotationRewardAction(){
        include 'minigame'.DIRECTORY_SEPARATOR.'2019'.DIRECTORY_SEPARATOR.'rotation-reward.php';
    }

    public function checkRewardAction(){

        $this->generateReward();
    }

    protected function getNewRotationReward() {
//        $this->generateReward();
        $QMiniGameRewardLog = new Application_Model_MiniGameRewardLog();
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id= $userStorage->id;
        $reward = array(
            "id"      => "9",
            "name"    => "Chúc may mắn lần sau",
            "percent" => 100,
            "limit"   => -1
        );

        $win_percent = 40;
        if ($userStorage->is_officer == '1')
            $win_percent = 20;

        if (in_array($userStorage->team, array(146))) {
            $win_percent = 30;
        }

        $is_win = (rand(0, 100) <= $win_percent);
        if($is_win)
        {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            try {
                $mini_game_reward = new Application_Model_MiniGameReward();

                $params = [
                    'round' => MINIGAME_ROUND
                ];

                $lst_remaining_rewards = $mini_game_reward->getRemainingItems($params);
                $lst_can_receive_reward = array();
                foreach ($lst_remaining_rewards as $key => $val)
                {
                    if($val['remain'] > 0)
                    {
                        for($i = 0; $i< $val['remain']; $i++){
                            if (in_array($userStorage->team, array(146))) {
                                if($val['id'] == '12')
                                    $this->addReward($lst_can_receive_reward, $val);
                            }
                            elseif($userStorage->is_officer == '1') {
                                if ($val['id'] != '12') {
                                    $this->addReward($lst_can_receive_reward, $val);
                                }
                            }
                            else{
                                $this->addReward($lst_can_receive_reward, $val);
                            }
                        }
                    }
                }

                $total_remaining_reward = count($lst_can_receive_reward);
                if($total_remaining_reward > 0)
                    $reward = $lst_can_receive_reward[rand(0, $total_remaining_reward - 1)];

                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $reward = array(
                    "id"      => "9",
                    "name"    => "Chúc may mắn lần sau",
                    "percent" => 100,
                    "limit"   => -1
                );
            }
        }
        $data_insert = array(
            'staff_id' => $staff_id,
            'reward' => $reward['id'],
            'round' => MINIGAME_ROUND
        );

        $QMiniGameRewardLog->insert($data_insert);

        return $reward;
    }

    protected  function addReward(&$arr_reward, $reward) {
        array_push($arr_reward, array(
            "id" => $reward['id'],
            "name" => $reward['reward'],
            "percent" => $reward['percent'],
            "limit" => $reward['limit']
        ));
    }

    protected function getRotationReward() {
//        $this->generateReward();
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
        $cache        = Zend_Registry::get('cache');
        $cache_reward = $cache->load('minigame_reward');

        $reward       = $cache_reward[rand(0, count($cache_reward) - 1)];
//        print "<pre>".print_r($cache_reward);die;
        $log_mode = new Application_Model_MiniGameRewardLog();
        $params = [
            'round' => MINIGAME_ROUND
        ];
        $log      = $log_mode->getLogRewardOfItem($params);
        if (empty($log)) {
            $log2 = [];
        } else {
            $log2 = $this->changeKey($log, "item_id");
        }
        $cache_limit = $log2;
        if (isset($cache_limit[$reward["id"]])) {
            $cache_limit[$reward["id"]]['count'] += 1;
        } else {
            $cache_limit[$reward["id"]] = array('count' => 1, 'item_id' => $reward["id"]);
        }
//Set lai reward neu da dat limit
//        if (!in_array($userStorage->title, array(182, 293, 419, 420)) && $reward['limit'] != -1) {
        if (in_array($userStorage->team, array(146))) {
//            if ($cache_limit[$reward["id"]]['count'] >= (intval($reward['limit']) / 2)) {
            $lst_reward = array(
                array(
                "id"      => "1",
                "name"    => "Chúc may mắn lần sau",
                "percent" => 100,
                "limit"   => -1
                ),
                array(
                    "id"      => "4",
                    "name"    => "Gấu bông Ollie",
                    "percent" => 0,
                    "limit"   => -1
                )
            );
            $reward = $lst_reward[0];
        }

        if ($reward["limit"] != -1 && $cache_limit[$reward["id"]]['count'] >= $reward["limit"]) {
            $reward['refresh'] = 1;
//            $this->generateReward();
        }
//        print "<pre>".print_r($reward);
        return $reward;
    }

    private function changeKey($models, $key) {
        $result = [];
        if (count($models) > 0)
            foreach ($models as $model) {
                $result[$model[$key]] = $model;
            }
        return $result;
    }

    public function generateReward() {
        $week    = 6;
//Reward
        $_reward = array(
            array(
                "id"      => "2",
                "name"    => "Ô / Dù gấp",
                "percent" => 0,
                "limit"   => -1
            ),
            array(
                "id"      => "3",
                "name"    => "Gối ngủ chữ U",
                "percent" => 0,
                "limit"   => -1
            ),
            array(
                "id"      => "4",
                "name"    => "Gấu bông Ollie",
                "percent" => 0,
                "limit"   => -1
            ),
            array(
                "id"      => "5",
                "name"    => "Sạc dự phòng Recci",
                "percent" => 0,
                "limit"   => -1
            ),
            array(
                "id"      => "6",
                "name"    => "Đèn để bàn",
                "percent" => 0,
                "limit"   => -1
            ),
            array(
                "id"      => "7",
                "name"    => "Tai nghe MH151",
                "percent" => 0,
                "limit"   => -1
            ),
            array(
                "id"      => "8",
                "name"    => "Tai nghe Bluetooth Tekin TWS-i12",
                "percent" => 0,
                "limit"   => -1
            ),
            array(
                "id"      => "1",
                "name"    => "Chúc may mắn lần sau",
                "percent" => 100,
                "limit"   => -1
            ),
        );

        $cache_reward = array();
        $cache        = Zend_Registry::get('cache');
        $reward_array = $_reward;
// $limit_array = $cache->load('oppoer_limit');
        $log_mode     = new Application_Model_MiniGameRewardLog();
        $params = array(
            'round'=>MINIGAME_ROUND
        );
        $log          = $log_mode->getLogRewardOfItem($params);
        if (empty($log)) {
            $log2 = [];
        } else {
            $log2 = $this->changeKey($log, "item_id");
        }
        $limit_array = $log2;
        if (!empty($reward_array)) {
            foreach ($reward_array as $reward) {
                if (!isset($limit_array[$reward["id"]]) || $reward["limit"] == -1 || $limit_array[$reward["id"]]['count'] < $reward["limit"]) {
                    for ($i = 0; $i < $reward["percent"]; $i++) {
                        $cache_reward[] = $reward;
                    }
                }
            }
        }
        if (!empty($cache_reward)) {
            shuffle($cache_reward);
            $cache->save($cache_reward, "minigame_reward", array(), null);
//          echo "success";
        }
        return true;
    }
}