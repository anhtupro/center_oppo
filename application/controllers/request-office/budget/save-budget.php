<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';
$QBudget = new Application_Model_BudgetFile();
$del                = $this->getRequest()->getParam('is_del', '');
$amount_t           = $this->getRequest()->getParam('amount', '');
$contract_id        = $this->getRequest()->getParam('contract', '');
$to_date            = $this->getRequest()->getParam('end_date');
$supplier_id        = $this->getRequest()->getParam('supplier', '');
$from_date          = $this->getRequest()->getParam('from_date');
$budget_type        = $this->getRequest()->getParam('radiobtns');
$name               = $this->getRequest()->getParam('budget_name', '');
$category           = $this->getRequest()->getParam('request_type', '');
$category_group     = $this->getRequest()->getParam('request_type_group', '');
$id                 = $this->getRequest()->getParam('id');
$amount             = str_replace( ',', '', $amount_t);  
if($budget_type == 1){
    $project_id         = 0;
} else {
    $project_id         = $this->getRequest()->getParam('budget_project', '');
}

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    $data = array(
        'budget_type'       => $budget_type,
        'amount'            => $amount,
        'contract_id'       => $contract_id,
        'to_date'           => date("Y/m/d", strtotime($to_date)),
        'supplier_id'       => $supplier_id,
        'from_date'         => date("Y/m/d", strtotime($from_date)),
        'name'              => trim($name),
        'category'          => $category,
        'project_id'        => $project_id,
        'category_group'    => $category_group,
        'del'               => $del,
    );

    if ($id) {        
        $where = $QBudget->getAdapter()->quoteInto('id = ?', $id);
        $QBudget->update($data, $where);   
    }
    else{        
        $QBudget->insert($data);   
    }
    $db->commit();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'100px\';</script>';
    echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
    echo '<div class="alert alert-success">Đã thực hiện thành công, đang chuyển sang trang Budget List</div>';

    /// load lại trang
    $back_url = "list-budget";
    echo '<script>setTimeout(function(){location.href="' . $back_url . '"}, 200)</script>';
} catch (Exception $e) {
    $db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">ERROR : ' . $e->getMessage() . '</div>';
    $back_url = "list-budget";
    echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 200)</script>';
}



    

