<?php

$QBudgetHistory = new Application_Model_BudgetHistory();

$page   = $this->getRequest()->getParam('page', 1);
$limit  = 10;

$result = $QBudgetHistory->fetchPagination($page, $limit, $total, $params);  
//echo '<pre>';var_dump($result); 
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url = HOST . 'request-office/list-budget' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);

