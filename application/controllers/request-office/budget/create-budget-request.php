<?php
$QRequestType       = new Application_Model_RequestType();
$QRequestTypeGroup  = new Application_Model_RequestTypeGroup();
$QProject           = new Application_Model_Project();
$QBudgetContract    = new Application_Model_BudgetContract();
$QSupplier          = new Application_Model_Supplier();
$id             = $this->getRequest()->getParam('id');
$amount_before  = $this->getRequest()->getParam('amount_before');
$amount_after   = $this->getRequest()->getParam('amount_after');
$status         = '1';  

if ($id) { 
    $SModel   = new Application_Model_BudgetFile();
    $rowset   = $SModel->find($id);
    $budget = $rowset->current();
    $this->view->get_budget = $budget;
}

$group      = $QRequestTypeGroup->get_cache();
$supplier   = $QSupplier->get_cache();
$type       = $QRequestType->get_rtype_cache();
$this->view->group = $group;
$this->view->supplier = $supplier;
$this->view->project = $QProject->fetchAll();
$this->view->contract = $QBudgetContract->fetchAll();
$this->view->type = $type;

if ($this->getRequest()->isPost()){        
        $QBudgetHistory = new Application_Model_BudgetHistory();
        $data= array(               
                'budget_id'     => $id,
                'amount_before' => $amount_before,
                'amount_after'  => $amount_after,
                'status'        => $status,
            );

        $QBudgetHistory->insert($data);
        $this->redirect(HOST . 'request-office/budget-change');
    }