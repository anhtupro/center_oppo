<?php

$QBudgetContract    = new Application_Model_BudgetContract;
$QTeam              = new Application_Model_Team();
$page   = $this->getRequest()->getParam('page', 1);
$limit  = 10;
$result = $QBudgetContract->fetchPagination($page, $limit, $total, $params);    
//echo '<pre>';var_dump($result);exit;
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url = HOST . 'request-office/list-contract' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);
$this->view->department   = $QTeam->get_list_department();

$name           = $this->getRequest()->getPost('name');
$department_id  = $this->getRequest()->getPost('department_id');
$link           = $this->getRequest()->getPost('link');
if ($this->getRequest()->isPost()){
        $data= array(               
            'name'          => $name,
            'department_id' => $department_id,
            );
        $QBudgetContract->insert($data);
        $this->redirect($link);
    }