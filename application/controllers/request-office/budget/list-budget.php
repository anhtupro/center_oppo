<?php
$QBudget            = new Application_Model_BudgetFile();
$QRequestType       = new Application_Model_RequestType();
$QRequestTypeGroup  = new Application_Model_RequestTypeGroup();
$QProject           = new Application_Model_Project();
$QBudgetContract    = new Application_Model_BudgetContract();
$QSupplier          = new Application_Model_Supplier();

$name               = $this->getRequest()->getParam('name');
$budget_type        = $this->getRequest()->getParam('budget_type');
$category_group     = $this->getRequest()->getParam('category_group');
$category           = $this->getRequest()->getParam('category');
$supplier_id        = $this->getRequest()->getParam('supplier_id');
$project_id         = $this->getRequest()->getParam('project_id');
$amount_down        = $this->getRequest()->getParam('amount_down');
$amount_up          = $this->getRequest()->getParam('amount_up');
$from_date          = $this->getRequest()->getParam('from_date');
$to_date            = $this->getRequest()->getParam('to_date');
$params = array(
    'name'              => $name,
    'budget_type'       => $budget_type,
    'category_group'    => $category_group,
    'category'          => $category,
    'supplier_id'       => $supplier_id,
    'amount_down'       => $amount_down,
    'amount_up'         => $amount_up,
    'from_date'         => $from_date,
    'to_date'           => $to_date,
    'project_id'        => $project_id,
); 
$page   = $this->getRequest()->getParam('page', 1);
$limit  = 10;

$result = $QBudget->fetchPagination($page, $limit, $total, $params);  
//echo '<pre>';var_dump($result); 
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url = HOST . 'request-office/list-budget' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);

$group      = $QRequestTypeGroup->get_cache();
$supplier   = $QSupplier->get_cache();
$type       = $QRequestType->get_rtype_cache();
$this->view->group = $group;
$this->view->supplier = $supplier;
$this->view->project = $QProject->fetchAll();
$this->view->contract = $QBudgetContract->fetchAll();
$this->view->type = $type;

