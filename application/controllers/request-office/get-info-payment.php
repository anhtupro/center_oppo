<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$payment_id = $this->getRequest()->getParam('payment_id');

$QRequestPaymentOffice         = new Application_Model_RequestPaymentOffice();
$request_payment = $QRequestPaymentOffice->getInfoPayment($payment_id);

$request_payment['list_confirm'] = $request_payment['list_confirm'].$request_payment['list_purcharsing_confirm'];
$request_payment['payment_cost_number'] = number_format($request_payment['payment_cost'])." ".$request_payment["currency_name"];
$request_payment['payment_cost_words'] = My_Number::priceText(intval($request_payment["payment_cost"]))." ".$request_payment["currency"];

echo json_encode([
    'status' => 0,
    'request_payment' => $request_payment
]);


