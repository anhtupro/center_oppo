<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$id                     = $this->getRequest()->getParam('id');
$company                = $this->getRequest()->getParam('company');
$request_type_group     = $this->getRequest()->getParam('request_type_group');
$request_type           = $this->getRequest()->getParam('request_type');
$project                = $this->getRequest()->getParam('project');
$area_id                = $this->getRequest()->getParam('area_id');
$department_id          = $this->getRequest()->getParam('department_id');
$team_id                = $this->getRequest()->getParam('team_id');
$supplier_id            = $this->getRequest()->getParam('supplier_id');
$details                = $this->getRequest()->getParam('details');
$note                   = $this->getRequest()->getParam('note');
$payment_date           = $this->getRequest()->getParam('payment_date');
$invoice_date           = $this->getRequest()->getParam('invoice_date');
$invoice_number         = $this->getRequest()->getParam('invoice_number');
$month                  = $this->getRequest()->getParam('month');
$year                   = $this->getRequest()->getParam('year');
$from_date              = $this->getRequest()->getParam('from_date');
$to_date                = $this->getRequest()->getParam('to_date');
$price                  = $this->getRequest()->getParam('price');
$currency               = $this->getRequest()->getParam('currency');
$file_delete            = $this->getRequest()->getParam('file_delete');
$submit_delete          = $this->getRequest()->getParam('submit_delete');

$QRequestOfficePlan     = new Application_Model_RequestOfficePlan();
$QRequestFilePlan       = new Application_Model_RequestFilePlan();
$QRequestOfficePlanInfo = new Application_Model_RequestOfficePlanInfo();
$flashMessenger         = $this->_helper->flashMessenger;


$db = Zend_Registry::get('db');
try {
    $db->beginTransaction();
    $data = [
        'company_id'            => $company,
        'request_type_group'    => $request_type_group,
        'request_type'          => $request_type,
        'area_id'               => $area_id,
        'department_id'         => $department_id,
        'team_id'               => $team_id,
        'supplier_id'           => $supplier_id,
        'details'               => $details,
        'note'                  => $note,
        'payment_date'          => $payment_date ? $payment_date : NULL,
        'invoice_date'          => $invoice_date ? $invoice_date : NULL,
        'invoice_number'        => $invoice_number,
        'created_by'            => $userStorage->id,
        'created_at'            => date('Y-m-d H:i:s'),
    ];

    if(!empty($id)) {
        $where_rq_plan      = $QRequestOfficePlan->getAdapter()->quoteInto('id = ?', $id);
        $where_rq_plan_info = $QRequestOfficePlanInfo->getAdapter()->quoteInto('rq_plan_id = ?', $id);
        $where_project_plan = $QRequestOfficePlanInfo->getAdapter()->quoteInto('rq_plan_id = ?', $id);

        $QRequestOfficePlan->update($data, $where_rq_plan);
        $QRequestOfficePlanInfo->update(array('del' => 1), $where_rq_plan_info);

        for ($i = 0; $i < count($price); $i++) {
            $QRequestOfficePlanInfo->insert(
                array(
                    'rq_plan_id'    => $id,
                    'project_id'    => !empty($project[$i]) ? $project[$i] : NULL,
                    'from_date'     => $from_date[$i],
                    'to_date'       => $to_date[$i],
                    'price'         => str_replace(",", "", $price[$i]),
                    'month'         => $month[$i],
                    'year'          => $year[$i],
                    'currency'      => $currency[$i],
                    'del'           => 0
                )
            );
        }
        if(!empty($file_delete)){
            foreach($file_delete as $key=>$value){
                if($value == 1){
                    $where = NULL;
                    $where = $QRequestFilePlan->getAdapter()->quoteInto("id = ?", $key);
                    $check = $QRequestFilePlan->update(['del' => 1], $where);
                }
            }
        }
    }else{
        $id = $QRequestOfficePlan->insert($data);
        
        
        
        for ($i = 0; $i < count($price); $i++) {
            
            $plan_info = array(
                    'rq_plan_id'    => $id,
                    'project_id'    => !empty($project[$i]) ? $project[$i] : NULL,
                    'from_date'     => $from_date[$i],
                    'to_date'       => $to_date[$i],
                    'price'         => str_replace(",", "", $price[$i]),
                    'month'         => $month[$i],
                    'year'          => $year[$i],
                    'currency'      => $currency[$i],
                    'del'           => 0
                );
            
            $id_info = $QRequestOfficePlanInfo->insert($plan_info);
        }
        
        
    }
    
    
    
    //Upload files
    $total = count($_FILES['file']['name']);
    for ($i = 0; $i < $total; $i++) {
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'request-file-plan' . DIRECTORY_SEPARATOR . $id;

        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);
        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
        if ($tmpFilePath != "") {
            $old_name = $_FILES['file']['name'][$i];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode);
            if($_FILES['file']['size'][$i] > 42000000)
            {
                echo json_encode([
                    'status' => 0,
                    'message' => "File Upload dung lượng quá lớn (Vui lòng upload file dưới 4M)",
                ]);
                return;
            }

            if(!in_array($extension, ['docx', 'doc', 'png', 'jpg', 'pdf', 'ppt', 'pptx', 'xlsx', 'xls', 'jpeg', 'PNG', 'JPEG', 'JPG'])){
                echo json_encode([
                    'status' => 0,
                    'message' => "File Upload sai định dạng: ".$extension,
                ]);
                return;
            }

            $new_name       = md5(uniqid('', true)) . $id . '-staff_id' . $userStorage->id."." .$extension;
            $newFilePath    = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                $url = 'files' .
                    DIRECTORY_SEPARATOR . 'request-file-plan' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
            } else {
                $url = NULL;
            }
        } else {
            $url = NULL;
        }

        $detail_file = [
            'request_id' => $id,
            'url' => $url,
            'name' => $_FILES['file']['name'][$i],
            'created_at' => date('Y-m-d H:i:s'),
            'staff_id' => $userStorage->id,
            'del' => 0
        ];
        if (!empty($url)) {
            $QRequestFilePlan->insert($detail_file);
        }
    }

    if($submit_delete){
        $where = NULL;
        $where = $QRequestOfficePlan->getAdapter()->quoteInto('id = ?', $id);
        $QRequestOfficePlan->update(['del' => 1],$where);
    }
    
    
    $db->commit();
    $flashMessenger->setNamespace('success')->addMessage('Hoàn thành!');
    $this->redirect(HOST . 'request-office/budget-details');
} catch (Exception $e) {
    $db->rollback();

    if($id){
        $flashMessenger->setNamespace('error')->addMessage('Error::'.$e->getMessage());
        $this->redirect(HOST . 'request-office/edit-budget-details?id='.$id);
    }
    else{
        $flashMessenger->setNamespace('error')->addMessage('Error::'.$e->getMessage());
        $this->redirect(HOST . 'request-office/create-budget-details');
    }
    
}



