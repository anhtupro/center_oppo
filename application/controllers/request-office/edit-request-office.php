<?php

$id = My_Util::escape_string($this->getRequest()->getParam('id'));
$submit = $this->getRequest()->getParam('submit');
$submit_delete = $this->getRequest()->getParam('submit_delete');
$reject = $this->getRequest()->getParam('reject');
$note = $this->getRequest()->getParam('note');
$submit_warning = $this->getRequest()->getParam('submit_warning');
$request_warning_checkbox = $this->getRequest()->getParam('request_warning_checkbox');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$action_link = $this->getRequest()->getActionName();

$flashMessenger = $this->_helper->flashMessenger;
//get request info
$QRequest = new Application_Model_RequestOffice();
$QRequestApprove = new Application_Model_RequestApproveOffice();
$QRequestConfirm = new Application_Model_RequestConfirm();
$QRequestCompany = new Application_Model_Company();
$QRequestCategory = new Application_Model_RequestCategoryOffice();
$QRequestFile = new Application_Model_RequestFileOffice();
$Qstaff = new Application_Model_Staff();
$QRequestPRPaymentOffice=new Application_Model_RequestPRPaymentOffice();
$QAppNoti=new Application_Model_AppNoti();

//Get Info department,team,area
$QStaff                  = new Application_Model_Staff();
$QTeam                  = new Application_Model_Team();
$QDepartment            = new Application_Model_Department();
$QArea                  = new Application_Model_Area();
$QRequestTypeGroup      = new Application_Model_RequestTypeGroup();
$QRequestType           = new Application_Model_RequestType();
$QRequestProject           = new Application_Model_RequestProject();
$QRequestOfficeNote           = new Application_Model_RequestOfficeNote();
$QRequestOfficeWarning = new Application_Model_RequestOfficeWarning();
$QPurchasingPlan = new Application_Model_PurchasingPlan();

//Get PR Info:
$QRequestPR=new Application_Model_RequestOfficePR();
$QPurchasingType        = new Application_Model_PurchasingType();
$QPurchasingProject     = new Application_Model_PurchasingProject();

// PR
$QPurchasingRequest  = new Application_Model_PurchasingRequest();
$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
$QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
$QPmodelCode            = new Application_Model_PmodelCode();
//------------------------------------------------------------------

$IS_VIEW = 0;
$requestDetail = $QRequest->findRequestByID($id);
$IS_VIEW = $QRequest->checkIsView($id, $userStorage->id);

//Kiểm tra phân quyền chỉ người tạo, người duyệt, kế toán phòng ban mới được xem
if (!$IS_VIEW) {
    $flashMessenger->setNamespace('error')->addMessage('Permission Denied !');
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->redirect(HOST . 'request-office/list-request');
}


if(!empty($requestDetail['budget_id'])){
    $budget = $QRequest->getBudgetById($requestDetail['budget_id']);
    $this->view->budget = $budget;
}


$staff_created = $QStaff->fetchRow(['id = ?' => $requestDetail['created_by']]);

//Kiểm tra xem finance confirm chưa
$finance_has_confirm = $QRequestConfirm->getFinanceHasConfirm($requestDetail['id']);
$this->view->finance_has_confirm = $finance_has_confirm;
//


//Kiểm tra các điều kiện điều chỉnh request office
$update_info_field = [
    'company'               => true,
    'category'              => true,
    'cost_temp'             => false,
    'currency'              => true,
    'payment_type'          => true,
    'finance_staff'         => true,
    'file_delete'           => true
];

$is_update_info = 0;
//Khi mới tạo thì cho điều chỉnh số tiền
if($requestDetail['step'] == 1){
    $is_update_info = 1;
    $update_info_field['cost_temp'] = true;
}

//Khi kế toán confirm rồi thì chặn ko cho điều chỉnh nữa
if($finance_has_confirm){
    $update_info_field['company'] = false;
    $update_info_field['category'] = false;
    
    $update_info_field['currency'] = false;
    $update_info_field['payment_type'] = false;
    $update_info_field['finance_staff'] = false;
    $update_info_field['file_delete'] = false;
    
}
//END kiểm tra


$department_id = $staff_created->department;

$request_type_group = $QRequestTypeGroup->fetchAll(['department_id = ?' => $department_id]);
$request_type = $QRequestType->getRequetsType($department_id);
$request_project = $QRequestProject->fetchAll(['department_id = ?' => $department_id]);

$listManyPayment=$QRequest->getManyPaymentEdit($userStorage->id, $id);

//PR Details
if(!empty($requestDetail["pr_sn"])){
    
    $pr_sn = $requestDetail["pr_sn"];
    
    $purchasing = $QPurchasingRequest->getInfoPurchasingRequest($pr_sn);
    $purchasing_details = $QPurchasingRequest->getInfoPurchasingRequestDetails($pr_sn);
    
    $details = $QPurchasingRequestDetails->getDetails($pr_sn);
    $details_request = $QPurchasingRequestDetails->getDetailsRequestBySn($pr_sn, $requestDetail['id']);
    

    $total_price_sn = 0;
    if(!empty($details)){
        foreach($details as $k=>$v){
            $total_num_sn = $total_num_sn + $v['quantity'];

            $money_ck = $price_after_ck = $total_price = 0;
            $money_ck = $v['price']*$v['ck']/100;
            $price_after_ck = $v['price'] - $money_ck;
            $total_price = $v['quantity']*$price_after_ck+$v['fee'];
            $total_price_sn = $total_price_sn + $total_price;
        }
    }
    
    $total_use = 0;
    if(!empty($details_request)){
        foreach($details_request as $k=>$v){
            $total_use = $total_use + $v['cost_temp'];
        }
    }
    
    $this->view->purchasing_pr = $purchasing;
    $this->view->purchasing_details_pr = $purchasing_details;
    $this->view->total_price_pr = $total_price_sn;
    $this->view->total_use_pr = $total_use;
    $this->view->details_request_pr = $details_request;
}
    


$staff = $Qstaff->get_cache();

$where= $QRequestPR->getAdapter()->quoteInto("request_id=?",$id);
$purchasing_request=$QRequestPR->fetchRow($where);

//get file in request
$where = [];
$where[] = $QRequestFile->getAdapter()->quoteInto("request_id =?", $id);
$where[] = $QRequestFile->getAdapter()->quoteInto("is_del = 0");
$requestFile = $QRequestFile->fetchAll($where);

$list_note = $QRequestOfficeNote->getListNote($id);

//Get request_warning
$where = [];
$where[] = $QRequestOfficeWarning->getAdapter()->quoteInto("request_id = ?", $id);
$where[] = $QRequestOfficeWarning->getAdapter()->quoteInto("del = 0");
$where[] = $QRequestOfficeWarning->getAdapter()->quoteInto("status IN (1,2)");
$request_warning = $QRequestOfficeWarning->fetchAll($where);

$this->view->request_warning = $request_warning;

if ($submit) {
    
    $company = $this->getRequest()->getParam('company');
    
    $cost_before = $this->getRequest()->getParam('cost_before', null);
    $cost_temp = $this->getRequest()->getParam('cost_temp', null);
    $currency = $this->getRequest()->getParam('currency');
    
    $contract_number = $this->getRequest()->getParam('contract_number');
    $contract_from = $this->getRequest()->getParam('contract_from');
    $contract_to = $this->getRequest()->getParam('contract_to');
    $category = $this->getRequest()->getParam('category');
    
    $request_type_group = $this->getRequest()->getParam('request_type_group', null);
    $request_type = $this->getRequest()->getParam('request_type', null);
    $project   = $this->getRequest()->getParam('project');
    
    $area_id = $this->getRequest()->getParam('area_id');
    $department_id = $this->getRequest()->getParam('department_id', null);
    $team_id = $this->getRequest()->getParam('team_id', null);
    
    $child_purchasing = $this->getRequest()->getParam('child_purchasing');
    
    
    $payment_type = $this->getRequest()->getParam('payment_type');
    $finance_staff = $this->getRequest()->getParam('finance_staff');
    
    $payee = $this->getRequest()->getParam('payee');
    $content = $this->getRequest()->getParam('content');
    $note = $this->getRequest()->getParam('note');
    $request_note = $this->getRequest()->getParam('request_note');
    $date_success = $this->getRequest()->getParam('date_success');
    
    $invoice_number = $this->getRequest()->getParam('invoice_number');
    $invoice_date = $this->getRequest()->getParam('invoice_date');
    
    $month_year_fee = $this->getRequest()->getParam('month_year_fee');
    $month_year_fee = explode('-', $month_year_fee);
    
    $cost_temp=str_replace(",","",$cost_temp);
    $cost_before=str_replace(",","",$cost_before);
    
    $many_request_id = $this->getRequest()->getParam('many_payment');
    
    $cost_many_total = $this->getRequest()->getParam('cost_many_total');
    $cost_many = $this->getRequest()->getParam('cost_many');
    $cost_many_left = $this->getRequest()->getParam('cost_many_left');

    $cost_many_total=str_replace(",","",$cost_many_total);
    $cost_many=str_replace(",","",$cost_many);
    $cost_many_left=str_replace(",","",$cost_many_left);
    
    $pr_payment_department_id = $this->getRequest()->getParam('pr_payment_department', null);
    $pr_payment_cost = $this->getRequest()->getParam('pr_payment_cost', null);
    $pr_payment_cost = str_replace(",", "", $pr_payment_cost);
    $pr_payment_amount = $this->getRequest()->getParam('pr_payment_amount', null);
    $estimasted_liquidation_date = $this->getRequest()->getParam('estimasted_liquidation_date');

    $child_purchasing       = $this->getRequest()->getParam('child_purchasing');
    $budget_purchasing      = $this->getRequest()->getParam('budget_purchasing');
    $budget_use             = $this->getRequest()->getParam('budget_use');
    
    $file_delete = $this->getRequest()->getParam('file_delete');
    
    foreach ($pr_payment_cost as $key => $value) {

        $data = array(
            "request_id" => $id,
            "cost" =>$value,
            "department_id" => $pr_payment_department_id[$key],
            "amount" => $pr_payment_amount[$key],
        );

        $QRequestPRPaymentOffice->insert($data);
    }    
      
    
    $data = array(
        
        'contract_number' => $contract_number,
        'contract_from' => !empty($contract_from) ? $contract_from : NULL,
        'contract_to' => !empty($contract_to) ? $contract_to : NULL,
        
        'area_id'          => $area_id ? $area_id : NULL,
        'department_id'    => $department_id ? $department_id : NULL,
        'team_id'          => $team_id ? $team_id : NULL,
        
        'many_request_id' => $many_request_id ? $many_request_id : NULL,
        'cost_many_total' => $cost_many_total ? $cost_many_total : NULL,
        'cost_many' => $cost_many ? $cost_many : NULL,
        'cost_many_left' => $cost_many_left ? $cost_many_left : NULL,
        'currency' => !empty($currency) ? $currency : 1,
        
        'month_fee' => !empty($month_year_fee[1]) ? $month_year_fee[1] : NULL,
        'year_fee' => !empty($month_year_fee[0]) ? $month_year_fee[0] : NULL,
        
        'request_type_group' => !empty($request_type_group) ? $request_type_group : NULL,
        'request_type' => !empty($request_type) ? $request_type : NULL,
        'project' => $project ? $project : NULL,
        'category_id' => $category,
        'payment_type' => !empty($payment_type) ? $payment_type : NULL,
        'finance_staff' => !empty($finance_staff) ? $finance_staff : NULL,
        'payee' => !empty($payee) ? $payee : NULL,
        'content' => !empty($content) ? $content : NULL,
        'note' => !empty($note) ? $note : NULL,
        'company_id' => $company,
        'date_success' => !empty($date_success) ? $date_success : NULL,
        
        'invoice_number' => !empty($invoice_number) ? $invoice_number : NULL,
        'invoice_date' => !empty($invoice_date) ? $invoice_date : NULL,
        
        'has_payment' => 1,
        
        'cost_before' => is_numeric($cost_before) ? $cost_before : 0,
        'cost_temp' => is_numeric($cost_temp) ? $cost_temp : 0,
        'updated_at' => date('Y-m-d H:i:s'),
        'estimasted_liquidation_date' => !empty($estimasted_liquidation_date) ? $estimasted_liquidation_date : NULL,
    );
    
    
    
    if(!$update_info_field['cost_temp']){
        unset($data['cost_temp']);
    }
    
    if(!$update_info_field['currency']){
        unset($data['currency']);
    }
    
    if(!$update_info_field['payment_type']){
        unset($data['payment_type']);
    }
    
    if(!$update_info_field['finance_staff']){
        unset($data['finance_staff']);
    }
    
    if(!$update_info_field['company']){
        unset($data['company_id']);
    }
    
    if(!$update_info_field['category']){
        unset($data['category_id']);
    }
    
    if(!$update_info_field['file_delete']){
        unset($file_delete);
    }
    
    //Update lại danh sách finance staff approve
    if(!$finance_has_confirm){
        
        
        $option = [];
        $option['company_id'] = $data['company_id'];
        $option['payment_type'] = $data['payment_type'];
        $option['request_id'] = $requestDetail['id'];
        $option['step'] = $requestDetail['step'];
        $option['finance_staff'] = $data['finance_staff'];
        $option['created_by'] = $requestDetail['created_by'];
        
        //Đổi lại list finance duyệt
        $where = NULL;
        $where[] = $QRequestConfirm->getAdapter()->quoteInto("request_id = ?", $requestDetail['id']);
        $where[] = $QRequestConfirm->getAdapter()->quoteInto("type = 2");
        $QRequestConfirm->delete($where);

        $list_confirm_finance = $QRequestConfirm->listFinanceConfirm($option);
        //END
    }

    $where = $QRequest->getAdapter()->quoteInto('id = ?', $id);

    $QRequest->update($data, $where);
    
    //Update lại list confirm, step = 1 mới cho update budget
    if($requestDetail['step'] == 1){
        $where = NULL;
        $where = $QRequestConfirm->getAdapter()->quoteInto('request_id = ?', $requestDetail['id']);
        $QRequestConfirm->delete($where);
        
        $data['pr_sn'] = $requestDetail['pr_sn'];
        $data['request_id'] = $requestDetail['id'];
        
        $list_confirm = $QRequestConfirm->listConfirmFlow($data);

        if(empty($list_confirm)){
            $flashMessenger->setNamespace('error')->addMessage('File Upload sai định dạng');
            $this->redirect(HOST . 'request-office/edit-request?id='.$id);
        }

        //Budget

        $QRequestOfficePlanMap = new Application_Model_RequestOfficePlanMap();

        $where = null;
        $where = $QRequestOfficePlanMap->getAdapter()->quoteInto('request_office_id = ?', $id);
        $QRequestOfficePlanMap->update(['is_del' => 1], $where);

        $total_use = 0;
        foreach($budget_purchasing as $key => $value){
            
            $use = str_replace(",","",$budget_use[$key]);
            
            $total_use = $total_use + $use;
            
            $plan_map = [
                'request_office_id' => $id,
                'plan_info_id'      => $value,
                'use'               => $use
            ];
            $QRequestOfficePlanMap->insert($plan_map);
        }
        
        if(!empty($budget_purchasing) && (float)$total_use !== (float)$cost_temp){

            $flashMessenger->setNamespace('error')->addMessage("Tổng tiền dự trù phải bằng số tiền thanh toán! (".(float)$total_use.':'.(float)$cost_temp.")");
            $this->redirect(HOST . 'request-office/edit-request?id='.$id);
        }
        

    }
    //END - 
    
    if(!empty($child_purchasing) AND $is_update_info == 1){
        foreach($child_purchasing as $k=>$v){
            $QPurchasingRequestDetails->update(['quantity_real' => $v], ['id = ?' => $k]);
        }
    }
    
    //Update note
    if(!empty($request_note)){
        $data_note = [
            'request_id' => $id,
            'created_by' => $userStorage->id,
            'created_at' => date('Y-m-d H:i:s'),
            'note' => $request_note
        ];
        $QRequestOfficeNote->insert($data_note);
    }
    //END update note
    
    //Update delete file
    if(!empty($file_delete)){
        foreach($file_delete as $key=>$value){
            
            if($value == 1){
                $where = NULL;
                $where = $QRequestFile->getAdapter()->quoteInto("id = ?",$key);
                
                $check = $QRequestFile->update(['is_del' => 1], $where);
            }
        }
    }
    //END - Update delete file

    $total = count($_FILES['file']['name']);

    for ($i = 0; $i < $total; $i++) {
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'request-office' . DIRECTORY_SEPARATOR . $id;
        
        
        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);
        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
        
        
        if ($tmpFilePath != "") {
            $old_name = $_FILES['file']['name'][$i];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode);
            
            if($_FILES['file']['size'][$i] > 5000000)
            {
                $flashMessenger->setNamespace('error')->addMessage('File Upload dung lượng quá lớn (Vui lòng upload file dưới 3MB)');
                $this->redirect(HOST . 'request-office/edit-request?id='.$id);
            }
            
            if(!in_array($extension, ['docx', 'doc', 'png', 'PNG', 'jpg', 'JPG', 'pdf', 'PDF', 'ppt', 'pptx', 'xlsx', 'xls', 'jpeg', 'JPEG', 'zip', 'ZIP', 'xml', 'XML'])){
                
                $flashMessenger->setNamespace('error')->addMessage('File Upload sai định dạng');
                $this->redirect(HOST . 'request-office/edit-request?id='.$id);
            }
            
            $new_name = preg_replace('/[^A-Za-z0-9\-]/', '', $tExplode[0]).'_'.md5(uniqid('', true))."." .$extension;
            $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                $url = 'files' .
                    DIRECTORY_SEPARATOR . 'request-office' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
            } else {
                $url = NULL;
            }
        } else {
            $url = NULL;
        }

        $detail_file = [
            'request_id' => $id,
            'url' => $url,
            'name' => $_FILES['file']['name'][$i],
            'hash_name' => $new_name,
            'created_at' => date("Y-m-d h:i:s"),
            'staff_id' => $userStorage->id,
        ];
        if (!empty($url)) {
            
            //Move file to S3
            require_once 'Aws_s3.php';
            $s3_lib = new Aws_s3();
            
            $file_location = $url;
            $detination_path = 'files/request-office/'.$id.'/';
            $destination_name = $new_name;
            
            $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
            
            if($upload_s3['message'] != 'ok'){
                
                $flashMessenger->setNamespace('error')->addMessage('Upload S3 không thành công');
                $this->redirect(HOST . 'request-office/edit-request?id='.$id);
            }
            //END - Move file to S3
            
            $QRequestFile->insert($detail_file);
        }
    }
    
    //Update request warning
    foreach($request_warning_checkbox as $key=>$value){
        if($value == 1){
            $where = NULL;
            $where = $QRequestOfficeWarning->getAdapter()->quoteInto("id = ?", $key);
            
            $warning_update = [
                'status' => 2,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => $userStorage->id
            ];
            
            $QRequestOfficeWarning->update($warning_update, $where);
        }
    }

    $flashMessenger->setNamespace('success')->addMessage('Cập nhật đề xuất thành công !');
    $this->redirect(HOST . 'request-office/list-request');
}

if($submit_delete){
    
    if($finance_has_confirm){
        $flashMessenger->setNamespace('error')->addMessage('Đề xuất đã duyệt không thể hủy!');
        $this->redirect(HOST . 'request-office/list-request');
    }
    
    $request_note = $this->getRequest()->getParam('request_note');
    
    //Update note
    if(!empty($request_note)){  
        $data_note = [
            'request_id' => $id,
            'created_by' => $userStorage->id,
            'created_at' => date('Y-m-d H:i:s'),
            'note' => $request_note
        ];
        $QRequestOfficeNote->insert($data_note);
    }
    //END update note
    
    $where = NULL;
    $where = $QRequest->getAdapter()->quoteInto('id = ?', $id);
    $data = [
        'del' => 1
    ];
    $QRequest->update($data, $where);
    
    $flashMessenger->setNamespace('success')->addMessage('Hủy đề xuất thành công !');
    $this->redirect(HOST . 'request-office/list-request');
}

if($submit_warning){
    
    $warning_note = $this->getRequest()->getParam('warning_note');
    
    $data = [
        'request_id' => $id,
        'note' => $warning_note,
        'status' => 1,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id,
    ];
    
    $QRequestOfficeWarning->insert($data);
    
    $flashMessenger->setNamespace('success')->addMessage('Hủy đề xuất thành công !');
    $this->redirect(HOST . 'request-office/list-request');
    
}

$params = [];
$params = [
    'request_id' => $id
];

$list_budget = $QPurchasingPlan->getRequestOfficePlan($params);
$this->view->list_budget = json_encode($list_budget);


$company = $QRequestCompany->getCompanyRequestOffice();
$category = $QRequestCategory->getCategory();

$where=null;
$where=$QRequestPRPaymentOffice->getAdapter()->quoteInto("request_id = ?",$id);
$listRequestPRPaymentOffice=$QRequestPRPaymentOffice->fetchAll($where);
$this->view->listRequestPRPaymentOffice = $listRequestPRPaymentOffice;


$flashMessenger = $this->_helper->flashMessenger;
$this->view->company = $company;
$this->view->channel = $channel;
$this->view->category = $category;
$this->view->requestFile = $requestFile;
$this->view->requestDetail = $requestDetail;
$this->view->staff = $staff;
$this->view->is_update_info = $is_update_info;
$this->view->list_note = $list_note;
$this->view->listManyPayment = $listManyPayment;
$this->view->department_id = $department_id;


$recursiveDepartmentTeamTitle             = $QTeam->get_recursive_cache();
$this->view->recursiveDepartmentTeamTitle = $recursiveDepartmentTeamTitle;

$department             = $QTeam->get_list_department();
$this->view->department = $department;

$this->view->channel = $channel;
$this->view->title = $title;
$this->view->requestPayment = $requestPayment;
$this->view->messages = $flashMessenger;
$this->view->requestFile = $requestFile;
$this->view->userStorage = $userStorage;
$this->view->requestApprove = $requestApprove;
$this->view->requestDetail = $requestDetail;
$this->view->staff = $staff;
$this->view->purchasing_request = $purchasing_request;

$this->view->request_type = $request_type;
$this->view->request_type_group = $request_type_group;
$this->view->request_project = $request_project;

$this->view->areas = $QArea->get_cache();
$this->view->update_info_field = $update_info_field;

$this->view->action_link = $action_link;


if (!empty($flashMessenger->setNamespace('error')->getMessages())) {
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}

?>