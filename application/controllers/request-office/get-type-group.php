<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    
    $QRequestTypeGroup = new Application_Model_RequestTypeGroup();
    
    $params = [
        'department_id' => 156
    ];
    
    $request_type_group = $QRequestTypeGroup->getList($params);
    
    $data = [
        'status' => 0,
        'message' => 'Success',
        'request_type_group' => $request_type_group
    ];
    
    echo json_encode($data);
    return;
    
    