<?php

$id = My_Util::escape_string($this->getRequest()->getParam('id'));
$submit = $this->getRequest()->getParam('submit');
$submit_note = $this->getRequest()->getParam('submit_note');
$reject = $this->getRequest()->getParam('reject');
$submit_update_payment = $this->getRequest()->getParam('submit_update_payment');
$submit_success = $this->getRequest()->getParam('submit_success');
$note = $this->getRequest()->getParam('note');
$finance_payment_date = $this->getRequest()->getParam('finance_payment_date');
$previous_link = $this->getRequest()->getParam('previous_link');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$flashMessenger = $this->_helper->flashMessenger;

$Qstaff = new Application_Model_Staff();

$QRequestApprove = new Application_Model_RequestApproveOffice();
$QRequest = new Application_Model_RequestOffice();
$QRequestFile = new Application_Model_RequestFileOffice();
$QRequestPayment = new Application_Model_RequestPaymentOffice();

$QRequestPR = new Application_Model_RequestOfficePR();
$QPurchasingType = new Application_Model_PurchasingType();
$QPurchasingProject = new Application_Model_PurchasingProject();
$QRequestPRPaymentOffice=new Application_Model_RequestPRPaymentOffice();
$QRequestOfficePR = new Application_Model_RequestOfficePR();
$QRequestConfirm = new Application_Model_RequestConfirm();
$QRequestProject = new Application_Model_RequestProject();
$QRequestOfficeNote = new Application_Model_RequestOfficeNote();
$QRequestOfficeDetails = new Application_Model_RequestOfficeDetails();
$QRequestOfficeDetailsMonth = new Application_Model_RequestOfficeDetailsMonth();

$QAppNoti=new Application_Model_AppNoti();
$QStaff=new Application_Model_Staff();

$QPurchasingRequest  = new Application_Model_PurchasingRequest();
$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
$QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
$QPmodelCode            = new Application_Model_PmodelCode();

$QRequestTypeGroup            = new Application_Model_RequestTypeGroup();
$QRequestType            = new Application_Model_RequestType();
$QRequestOfficeWarning = new Application_Model_RequestOfficeWarning();
$QPurchasingPlan            = new Application_Model_PurchasingPlan();



$QTeam = new Application_Model_Team();
$QDepartment = new Application_Model_Department();
$QArea = new Application_Model_Area();

$where = $QRequestPR->getAdapter()->quoteInto("request_id=?", $id);
$purchasing_request = $QRequestPR->fetchRow($where);

$requestDetail = $QRequest->findRequestByID($id);



$listManyPayment=$QRequest->getManyPaymentEdit($userStorage->id, $id);

if(!empty($requestDetail['budget_id'])){
    $budget = $QRequest->getBudgetById($requestDetail['budget_id']);
    $this->view->budget = $budget;
}

$staff_created = $QStaff->fetchRow(['id = ?' => $requestDetail['created_by']]);

$department_id = $staff_created->department;

$request_type_group = $QRequestTypeGroup->fetchAll(['department_id = ?' => $department_id]);
$request_type = $QRequestType->getRequetsType($department_id);
$request_project = $QRequestProject->fetchAll(['department_id = ?' => $department_id]);

$IS_APPROVE = $IS_UPDATE_PAYMENT = $IS_ACCOUNTANT = $IS_VIEW = 0;

$title = $Qstaff->getStaffInfo($userStorage->id);
$check_approve = $QRequest->checkStaffApproving($id, $userStorage->id);
$IS_VIEW = $QRequest->checkIsView($id, $userStorage->id);

$list_note = $QRequestOfficeNote->getListNote($id);

if (!empty($check_approve) ) {
    $IS_APPROVE = 1;
} else {
    $IS_APPROVE = 0;
}
if (!empty($check_approve["update_payment"])) {
    $IS_UPDATE_PAYMENT = 1;
} else {
    $IS_UPDATE_PAYMENT = 0;
}
if ($check_approve["finish_request"]) {
    $IS_ACCOUNTANT = 1;
} else {
    $IS_ACCOUNTANT = 0;
}

if (!$IS_VIEW) {
    $flashMessenger->setNamespace('error')->addMessage('Permission Denied !');
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->redirect(HOST . 'request-office/list-request');
}

$staff = $Qstaff->get_cache();
$cost_real_sum = $QRequestPayment->getRealCost($id);
$cost_real_sum = $cost_real_sum["cost_real_sum"];
$this->view->cost_real_sum = $cost_real_sum;

$team = $QTeam->get_cache();

$where = [];
$where[] = $QRequestFile->getAdapter()->quoteInto("request_id =?", $id);
$where[] = $QRequestFile->getAdapter()->quoteInto("is_del = 0");
$requestFile = $QRequestFile->fetchAll($where);

//List details
$list_details = $QRequestOfficeDetails->getListDetails($id);
$this->view->list_details = $list_details;
$request_type_details = [];
foreach($request_type as $key=>$value){
    $request_type_details[$value['group_id']][] = $value;
}
$this->view->request_type_details = $request_type_details;
$this->view->list_details_month = $list_details_month;
//END List details

//PR Details
if(!empty($requestDetail["pr_sn"])){
    
    $pr_sn = $requestDetail["pr_sn"];
    
    $purchasing = $QPurchasingRequest->getInfoPurchasingRequest($pr_sn);
    $purchasing_details = $QPurchasingRequest->getInfoPurchasingRequestDetails($pr_sn);
    
    $details = $QPurchasingRequestDetails->getDetails($pr_sn);
    $details_request = $QPurchasingRequestDetails->getDetailsRequestBySn($pr_sn, NULL);
    

    $total_price_sn = 0;
    if(!empty($details)){
        foreach($details as $k=>$v){
            $total_num_sn = $total_num_sn + $v['quantity'];

            $money_ck = $price_after_ck = $total_price = 0;
            $money_ck = $v['price']*$v['ck']/100;
            $price_after_ck = $v['price'] - $money_ck;
            $total_price = $v['quantity']*$price_after_ck+$v['fee'];
            $total_price_sn = $total_price_sn + $total_price;
        }
    }
    
    $total_use = 0;
    if(!empty($details_request)){
        foreach($details_request as $k=>$v){
            $total_use = $total_use + $v['cost_temp'];
        }
    }
    
    $this->view->purchasing_pr = $purchasing;
    $this->view->purchasing_details_pr = $purchasing_details;
    $this->view->total_price_pr = $total_price_sn;
    $this->view->total_use_pr = $total_use;
    $this->view->details_request_pr = $details_request;
    
}

//Budget details
$params_request = [
    'request_id' => $id
];
$list_budget = $QPurchasingPlan->getRequestOfficePlan($params_request);
$this->view->list_budget = $list_budget;

try {
    if ($submit) {
        
        $id_request = $requestDetail["id"];
        
        $step = $requestDetail['step'];
        $pr_id = NULL;
        $data = array(
            'step'  => $step+1,
        );
        
        
        
        $is_last_confirm = $QRequest->checkIsLastConfirm($id_request, $step);
        $is_success_confirm_internal = $QRequest->checkSuccessInternalConfirm($requestDetail["id"], $step);
        
        if($is_success_confirm_internal){
            if(!empty($requestDetail["pr_sn"])){
                $where_pr          = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?', $requestDetail["pr_sn"]);
                $row_pr = $QPurchasingRequest->fetchRow($where_pr);

                if($row_pr['status'] < 3){
                    $flashMessenger->setNamespace('error')->addMessage("Waiting Purchasing's Confirmed !");
                    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
                    $this->view->messages_error = $messages_error;
                    $this->redirect(HOST . 'request-office/confirm-request?id=' . $id_request);
                }
            }
        }
        
        if($is_last_confirm && $requestDetail['has_payment'] == 1 && $requestDetail['category_id'] != 4 && $requestDetail['category_id'] != 5){
            $option['company_id'] = $requestDetail['company_id'];
            $option['payment_type'] = $requestDetail['payment_type'];
            $option['request_id'] = $id_request;
            $option['step'] = $step+1;
            $option['finance_staff'] = $requestDetail['finance_staff'];
            $option['created_by'] = $requestDetail['created_by'];

            //Bỏ list finance confirm cũ
            $where = NULL;
            $where[] = $QRequestConfirm->getAdapter()->quoteInto("request_id = ?", $requestDetail['id']);
            $where[] = $QRequestConfirm->getAdapter()->quoteInto("type = 2");
            $QRequestConfirm->delete($where);

            $list_confirm_finance = $QRequestConfirm->listFinanceConfirm($option);
        }
        
        
        
        if($is_last_confirm && $requestDetail['has_purchasing_request'] == 1 && empty($requestDetail['pr_sn'])){
            //Lấy dữ liệu từ request để tạo pr
            $where=null;
            $where=$QRequestOfficePR->getAdapter()->quoteInto("request_id = ?",$id_request);
            $dataPurchasing=$QRequestOfficePR->fetchAll($where)->toArray();
            
             if(!empty($dataPurchasing)){

                $sn = date('YmdHis') . substr(microtime(), 2, 4);
                $where          = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?', $sn);
                $row_Purchasing = $QPurchasingRequest->fetchRow($where);
                $dataPurchasing=$dataPurchasing[0];

                $dataPurchasingReal=array(
                "sn" => $sn,
                "status" => $dataPurchasing["status"],
                "name" => $dataPurchasing["name"],
                "type" => $dataPurchasing["type"],
                "urgent_date" => $dataPurchasing["urgent_date"],
                "project_id" =>$dataPurchasing["project_id"],
                "team_id" => $dataPurchasing["team_id"],
                "department_id" => $dataPurchasing["department_id"],
                "area_id"=> $dataPurchasing["area_id"],
                "shipping_address" => $dataPurchasing["shipping_address"],
                "have_contract" => $dataPurchasing["have_contract"],
                "created_by"=> $dataPurchasing["created_by"],
                "created_at" => date("Y-m-d H:i:s"),
                );
                
                
                
                
                if (!empty($row_Purchasing)) {//TH Edit

                }
                else {//TH create
                    $pr_id = $QPurchasingRequest->insert($dataPurchasingReal);
                    if(!empty($pr_id)){
                        $data["pr_sn"] = $sn;
                    }
                }
            }

        }
        
        
        
        if($is_last_confirm){
            $staffIdPushNotification = $QRequest->getNextStaffApproving($id_request, $data['step'], 2);
        }
        else{
            $staffIdPushNotification = $QRequest->getNextStaffApproving($id_request, $data['step'], 1);
        }
        
        if(!empty($staffIdPushNotification['staff_id'])){
        $data_noti = [
                    'title' => "OPPO VIETNAM",
                    'message' => "Có một đề xuất đang chờ bạn duyệt",
                    'link' => "/request-office/list-request",
                    'staff_id' => $staffIdPushNotification['staff_id']
                ];
                
                $push_noti = $QAppNoti->sendNotificationNoTransactionERP($data_noti);
                $push_noti = $QAppNoti->sendNotificationNoTransaction($data_noti);
          
        }
        $data['status'] = !empty($staffIdPushNotification['title_confirm']) ? $staffIdPushNotification['title_confirm'] : 'Hoàn thành';
        
        $where = NULL;
        $where = $QRequest->getAdapter()->quoteInto('id = ?', $id_request);
        $QRequest->update($data, $where);

        //insert into request_approve
        $data_confirm = array(
            'is_confirm' => 1,
            'confirm_date' => date("Y-m-d H:i:s"),
        );
        
        $staff_id_confirm = $userStorage->id;
        
        $where_confirm = [];
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('staff_id = ?', $staff_id_confirm);
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('request_id = ?', $id_request);
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('step = ?', $step);
        $QRequestConfirm->update($data_confirm, $where_confirm);
        
        $data_note = [
            'request_id' => $requestDetail["id"],
            'created_by' => $userStorage->id,
            'created_at' => date('Y-m-d H:i:s'),
            'note' => $note
        ];
        $QRequestOfficeNote->insert($data_note);
        

        $total = count($_FILES['file']['name']);

        for ($i = 0; $i < $total; $i++) {
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id;
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);
            $tmpFilePath = $_FILES['file']['tmp_name'][$i];
            if ($tmpFilePath != "") {
                $old_name = $_FILES['file']['name'][$i];
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                
                if(!in_array($extension, ['docx', 'doc', 'png', 'jpg', 'pdf', 'ppt', 'pptx', 'xlsx', 'xls', 'jpeg'])){
                    echo json_encode([
                        'status' => 0,
                        'message' => "File Upload sai định dạng",
                    ]);
                    return;
                }
                
                $new_name = md5(uniqid('', true)) . $id . '-staff_id' . $userStorage->id . "." . $extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url = 'files' .
                        DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
                } else {
                    $url = NULL;
                }
            } else {
                $url = NULL;
            }

            $detail_file = [
                'request_id' => $id,
                'url' => $url,
                'name' => $_FILES['file']['name'][$i],
                'hash_name' => $new_name,
                'created_at' => date("Y-m-d h:i:s"),
                'staff_id' => $userStorage->id,
            ];
            if (!empty($url)) {
                
                //Move file to S3
                require_once 'Aws_s3.php';
                $s3_lib = new Aws_s3();

                $file_location = $url;
                $detination_path = 'files/request/'.$id.'/';
                $destination_name = $new_name;

                $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
                if($upload_s3['message'] != 'ok'){
                    echo json_encode([
                        'status' => 0,
                        'message' => "Upload S3 không thành công",
                    ]);
                    return;
                }
                // END - Move file to S3
                
                $QRequestFile->insert($detail_file);
            }
        }
        
        //Update request_warning
        $where = [];
        $where[] = $QRequestOfficeWarning->getAdapter()->quoteInto("request_id = ?", $id);
        $where[] = $QRequestOfficeWarning->getAdapter()->quoteInto("del = 0");
        $QRequestOfficeWarning->update(['status' => 3], $where);
        
        $flashMessenger->setNamespace('success')->addMessage('Xét duyệt đề xuất thành công !');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        
        $back_link = !empty($previous_link) ? $previous_link : HOST . 'request-office/list-request';
        
        $this->redirect($back_link);
        
    }
    elseif ($submit_note) {
        $data_note = [
            'request_id' => $requestDetail["id"],
            'created_by' => $userStorage->id,
            'created_at' => date('Y-m-d H:i:s'),
            'note' => $note
        ];
        $QRequestOfficeNote->insert($data_note);
        
        $flashMessenger->setNamespace('success')->addMessage('Cập nhật ghi chú thành công !');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->redirect(HOST . 'request-office/confirm-request?id=' . $requestDetail["id"]);
    }
    elseif($reject){

        //Update note
        if(!empty($note)){
            $data_note = [
                'request_id' => $id,
                'created_by' => $userStorage->id,
                'created_at' => date('Y-m-d H:i:s'),
                'note' => $note
            ];
            $QRequestOfficeNote->insert($data_note);
        }
        //END update note

        $where = NULL;
        $where = $QRequest->getAdapter()->quoteInto('id = ?', $id);
        $data = [
            'reject' => 1,
            'reject_by' => $userStorage->id,
            'reject_at' => date('Y-m-d H:i:s')
        ];
        $QRequest->update($data, $where);

        $flashMessenger->setNamespace('success')->addMessage('Reject đề xuất thành công !');
        $this->redirect(HOST . 'request-office/list-request');
    }
    elseif($submit_update_payment) {
        
        $invoice_number = $this->getRequest()->getParam('invoice_number', null);
        $invoice_date = $this->getRequest()->getParam('invoice_date', null);
        $payment_id = $this->getRequest()->getParam('request_payment_id');
        $payment_date = $this->getRequest()->getParam('request_payment_date');
        
        foreach ($payment_id as $key => $value) {
           
            $data = array(
                "invoice_number" => $invoice_number,
                "invoice_date" => $invoice_date,
                "payment_date" => !empty($payment_date[$key]) ? $payment_date[$key] : NULL,
            );

            $QRequestPayment->update($data, ['id = ?' => $value]);
        }
        $flashMessenger->setNamespace('success')->addMessage('Cập nhật thanh toán thành công !');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->redirect(HOST . 'request-office/confirm-request?id=' . $id);
        
    }
    elseif($submit_success) {
        
        $id_request = $requestDetail["id"];
        
        if(!empty($requestDetail["pr_sn"])){
            $where_pr          = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?', $requestDetail["pr_sn"]);
            $row_pr = $QPurchasingRequest->fetchRow($where_pr);
            
            if($row_pr['status'] < 3){
                $flashMessenger->setNamespace('error')->addMessage("Waiting Purchasing's Confirmed !");
                $messages_error = $flashMessenger->setNamespace('error')->getMessages();
                $this->view->messages_error = $messages_error;
                $this->redirect(HOST . 'request-office/confirm-request?id=' . $id_request);
            }
        }
        
        //insert into request_approve
        $data_confirm = array(
            'is_confirm' => 1,
            'confirm_date' => date("Y-m-d h:i:s"),
            'note' => null,
        );
        
        $staff_id_confirm = $userStorage->id;
        
        $where_confirm = [];
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('staff_id = ?', $staff_id_confirm);
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('request_id = ?', $id_request);
        $QRequestConfirm->update($data_confirm, $where_confirm);

        $data = array(
            "is_approve" => 1,
            'step' => $requestDetail['step']+1,
            'status' => "Hoàn Thành",
            "finance_payment_date" => $finance_payment_date
            
        );
        $where = $QRequest->getAdapter()->quoteInto("id=?", $id);
        $id_request=$QRequest->update($data, $where);

        $flashMessenger->setNamespace('success')->addMessage('Xác nhận hoàn tất đề xuất thành công !');
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->redirect(HOST . 'request-office/list-request');
    }

} catch (Exception $e) {

    $flashMessenger->setNamespace('error')->addMessage('Cập nhật đề xuất thất bại !'.$e->getMessage());
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;
    $this->redirect(HOST . 'request-office/confirm-request?id=' . $id);
}
$where = $QRequestApprove->getAdapter()->quoteInto("request_id=?", $id);
$requestApprove = $QRequestApprove->fetchAll($where);
$where = null;
$where = $QRequestPayment->getAdapter()->quoteInto("request_id=?", $id);
$requestPayment = $QRequestPayment->fetchAll($where);



$recursiveDepartmentTeamTitle = $QTeam->get_recursive_cache();
// $purchasing_request = $QRequestPR->getData();
$department = $QTeam->get_list_department();
$purchasingTypeByDepartment = $QPurchasingType->getCacheTypeByDepartment();


$where=null;
$where=$QRequestPRPaymentOffice->getAdapter()->quoteInto("request_id = ?",$id);
$listRequestPRPaymentOffice=$QRequestPRPaymentOffice->fetchAll($where);
$this->view->listRequestPRPaymentOffice = $listRequestPRPaymentOffice;

$this->view->areas = $QArea->get_cache();
$this->view->team = $team;

$this->view->purchasing_project = $QPurchasingProject->fetchAll();
$this->view->recursiveDepartmentTeamTitle = $recursiveDepartmentTeamTitle;
$this->view->department = $department;
$this->view->channel = $channel;
$this->view->title = $title;
$this->view->requestPayment = $requestPayment;

$this->view->messages = $flashMessenger;
$this->view->IS_ACCOUNTANT = $IS_ACCOUNTANT;
$this->view->IS_APPROVE = $IS_APPROVE;
$this->view->IS_UPDATE_PAYMENT = $IS_UPDATE_PAYMENT;

$this->view->requestFile = $requestFile;
$this->view->userStorage = $userStorage;
$this->view->requestApprove = $requestApprove;
$this->view->requestDetail = $requestDetail;
$this->view->data_total_sn = $data_total_sn;

$this->view->staff = $staff;
$this->view->purchasing_request = $purchasing_request;

$this->view->request_type_group = $request_type_group;
$this->view->request_type = $request_type;
$this->view->request_project = $request_project;
$this->view->list_note = $list_note;
$this->view->listManyPayment = $listManyPayment;

if(isset($_SERVER['HTTP_REFERER'])) {
    $previous_link = $_SERVER['HTTP_REFERER'];
}
$this->view->previous_link = $previous_link;

$this->view->purchasing_type = $purchasingTypeByDepartment[$requestDetail["department_id"]];
if (!empty($flashMessenger->setNamespace('error')->getMessages())) {
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}


?>