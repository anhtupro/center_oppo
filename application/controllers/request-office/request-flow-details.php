<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $flow_id = $this->getRequest()->getParam('flow_id');
    $staff_id = $this->getRequest()->getParam('staff_id');
    $from_price = $this->getRequest()->getParam('from_price');
    $to_price = $this->getRequest()->getParam('to_price');
    $company_id = $this->getRequest()->getParam('company_id');
    $department_flow = $this->getRequest()->getParam('department_flow');
    $team_flow = $this->getRequest()->getParam('team_flow');
    $category_group = $this->getRequest()->getParam('category_group');
    $category_fee = $this->getRequest()->getParam('category_fee');
    $has_leader = $this->getRequest()->getParam('has_leader');
    
    $from_price=str_replace(",","",$from_price);
    $to_price=str_replace(",","",$to_price);
    
    $submit = $this->getRequest()->getParam('submit');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $flashMessenger = $this->_helper->flashMessenger;



    $params = array(
        'sn' => $sn,
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $QRequestFlow = new Application_Model_RequestFlow();
    $QCompany = new Application_Model_Company();
    $QRequestFlowDetails = new Application_Model_RequestFlowDetails();
    $QTeam = new Application_Model_Team();
    
    $company = $QCompany->fetchAll();
    
    $department = $QTeam->get_list_department();
    
    if($flow_id){
        $info = $QRequestFlowDetails->getInfo($flow_id);
        $list_confirm = $QRequestFlowDetails->getListConfirm($flow_id);
    }
    
    
    if($submit){
        $db = Zend_Registry::get('db');
        try {
            $db->beginTransaction();
            
            
            $flow = [
                'company_id' => $company_id,
                'type'       => 1,
                'request_type_group' => !empty($category_group) ? $category_group : NULL,
                'request_type' => !empty($category_fee) ? $category_fee : NULL,
                'team_id'   => !empty($team_flow) ? $team_flow : NULL,
                'department_id' => $department_flow,
                'from_price' => $from_price,
                'to_price'   => $to_price,
                'has_leader' => $has_leader,
                'updated_at' => date("Y-m-d H-i-s"),
                'updated_by' => $userStorage->id
            ];

            if($flow_id){

                $where = NULL;
                $where = $QRequestFlow->getAdapter()->quoteInto('id = ?', $flow_id);
                $QRequestFlow->update($flow, $where);

            }
            else{

                $flow['created_at'] = date("Y-m-d H:i:s");
                $flow['created_by'] = $userStorage->id;
                $flow_id = $QRequestFlow->insert($flow);
            }
            
            
            $where = NULL;
            $where = $QRequestFlowDetails->getAdapter()->quoteInto('flow_id = ?', $flow_id);
            $QRequestFlowDetails->update(['is_del' => 1], $where);
            
            foreach($staff_id as $k=>$v){
                $data = [
                    'flow_id' => $flow_id,
                    'staff_id' => $v,
                    'step'  => $k+1
                ];
                $QRequestFlowDetails->insert($data);
            }

            $db->commit();
            
            $flashMessenger->setNamespace('success')->addMessage('Hoàn thành !');
            $this->redirect('/request-office/request-flow-details?flow_id=' . $flow_id);
            
        } catch (Exception $e) {

            $db->rollback();
            
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->redirect('/request-office/request-flow-details?flow_id=' . $flow_id);
            
        }
    }

    
    
    $this->view->list_confirm = $list_confirm;
    
    $this->view->department = $department;
    $this->view->list = $result;
    $this->view->info = $info;
    $this->view->data_flow = $data_flow;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'request-office/request-flow' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    $this->view->company = $company;
    
    $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
    $messages          = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;
    
    