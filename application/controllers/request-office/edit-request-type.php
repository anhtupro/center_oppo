<?php

    $department_id = $this->getRequest()->getParam('department_id');
    
    $QTeam = new Application_Model_Team();
    $QRequestTypeGroup = new Application_Model_RequestTypeGroup();
    
    $department = $QRequestTypeGroup->getDepartment();
    
    
    $params = [
        'department_id' => $department_id
    ];
    
    $request_type_group = $QRequestTypeGroup->fetchAll(['is_del = 0', 'department_id = ?' => $department_id])->toArray();
    $get_request_type = $QRequestTypeGroup->getListRequestType($params);
    
    $request_type = [];
    foreach($get_request_type as $key=>$value){
        $request_type[$value['group_id']][] = [
            'id' => $value['id'],
            'title' => $value['title'],
            'is_del' => $value['is_del'],
        ];
    }
    
    $department = $QTeam->fetchRow(['id = ?' => $department_id]);

    $this->view->department = $department;
    $this->view->request_type_group = $request_type_group;
    $this->view->request_type = $request_type;
    
    $flashMessenger     = $this->_helper->flashMessenger;
    $messages           = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;

    $messages_success   = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
    
    
    
    
    
        
    