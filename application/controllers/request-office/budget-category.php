<?php
    
    $department_id = $this->getRequest()->getParam('department_id');
    $year = $this->getRequest()->getParam('year');
    
    $QRequestType = new Application_Model_RequestType();
    $QTeam = new Application_Model_Team();
    $QRequestTypeGroup = new Application_Model_RequestTypeGroup();
    
    $params = [
        'department_id' => $department_id,
        'year'          => $year
    ];
    
    
    $deparment = $QTeam->fetchRow(['id = ?' => $department_id]);
    
    $get_request_type = $QRequestType->get_request_type($params);
    $request_type_group = [];
    foreach($get_request_type as $key=>$value){
        
        if(empty($request_type_group[$value['group_id']])){
            
            $request_type_group[$value['group_id']]['id'] = $value['group_id'];
            $request_type_group[$value['group_id']]['title'] = $value['group_title'];
            $request_type_group[$value['group_id']]['request_type'][] = [
                'id' => $value['type_id'],
                'title' => $value['type_title']
            ];
        }
        else{
            $request_type_group[$value['group_id']]['request_type'][] = [
                'id' => $value['type_id'],
                'title' => $value['type_title']
            ];
        }
    }
    
    
    $quaters = [
        [
          'quater' => 1,
          'quater_name' => 'Quater 1',
          'list_months' => [1,2,3],
          'year' => 2020
        ],
        [
          'quater' => 2,
          'quater_name' => 'Quater 2',
          'list_months' => [4,5,6],
          'year' => 2020
        ],
        [
          'quater' => 3,
          'quater_name' => 'Quater 3',
          'list_months' => [7,8,9],
          'year' => 2020
        ],
        [
          'quater' => 4,
          'quater_name' => 'Quater 4',
          'list_months' => [10,11,12],
          'year' => 2020
        ],
    ];
    
    
    //Lấy budget theo request_type
    $get_request_type_group = $QRequestTypeGroup->getList($params);
    
    $get_request_type_budget = $QRequestTypeGroup->getListTypeBudget($params);
    
    $get_request_type = $QRequestTypeGroup->getListType($params);
    
    
    $request_type_budget = [];
    foreach($get_request_type_budget as $budget){
        $request_type_budget[$budget['request_type_id']][$budget['quater']] = $budget['cost'];
    }
    
    $request_type = [];
    foreach($get_request_type as $key=>$value){
        $request_type[$value['group_id']][$value['id']] = $value;
        $request_type[$value['group_id']][$value['id']]['budget'] = $request_type_budget[$value['id']];
    }
    
    $request_type_group = [];
    foreach($get_request_type_group as $key=>$value){
        $request_type_group[$value['id']] = $value;
        $request_type_group[$value['id']]['request_type'] = $request_type[$value['id']];
    }
    
    //END Lấy budget theo request_type
    
    $this->view->request_type_group = json_encode($request_type_group);
    $this->view->quaters = json_encode($quaters);
    $this->view->department = $deparment;
    
    $this->view->year = $year;
    
    
    

