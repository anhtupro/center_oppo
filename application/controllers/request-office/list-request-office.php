<?php


require_once 'Mobile_Detect.php';
$detect_device = new Mobile_Detect;


$submit = $this->getRequest()->getParam('submit', null);
$category = $this->getRequest()->getParam('category', null);
$company_id = $this->getRequest()->getParam('company_id', null);
$channel_search_id = $this->getRequest()->getParam('channel_id', null);
$content = $this->getRequest()->getParam('content', null);
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$status_filter = $this->getRequest()->getParam('status_filter', null);
$from_date = $this->getRequest()->getParam('from_date', null);
$to_date = $this->getRequest()->getParam('to_date', null);
$request_own = $this->getRequest()->getParam('request_own', null);
$view_all = $this->getRequest()->getParam('view_all', null);
$cost_temp = $this->getRequest()->getParam('cost_temp');
$payee = $this->getRequest()->getParam('payee');
$request_id = $this->getRequest()->getParam('request_id');
$is_year_contract = $this->getRequest()->getParam('is_year_contract');

$month_year_fee = $this->getRequest()->getParam('month_year_fee');
$month_year_fee_ = explode('-', $month_year_fee);

$department_id = $this->getRequest()->getParam('department_id');
$team_id = $this->getRequest()->getParam('team_id');
$step = $this->getRequest()->getParam('step');

$project = $this->getRequest()->getParam('project');
$request_type_group = $this->getRequest()->getParam('request_type_group');
$request_type = $this->getRequest()->getParam('request_type');
$category_id = $this->getRequest()->getParam('category_id');
$export = $this->getRequest()->getParam('export');

$updateStatusPR = $this->getRequest()->getParam('update_status_pr');
$flashMessenger = $this->_helper->flashMessenger;
$Qstaff = new Application_Model_Staff();
$QstaffChannel = new Application_Model_StaffChannel();
$QRequestCategory = new Application_Model_RequestCategoryOffice();
$QRequest = new Application_Model_RequestOffice();
$QRequestApprove = new Application_Model_RequestApproveOffice();
$QRequestFile=new Application_Model_RequestFileOffice();
$QCompany = new Application_Model_Company();
$QTeam                  = new Application_Model_Team();
$QRequestPrPayment=new Application_Model_RequestPRPaymentOffice();
$QRequestOfficePR= new Application_Model_RequestOfficePR();
$QPurchasingRequest         = new Application_Model_PurchasingRequest();
$QRequestConfirm         = new Application_Model_RequestConfirm();
$QRequestTypeGroup         = new Application_Model_RequestTypeGroup();
$QRequestType         = new Application_Model_RequestType();
$QRequestProject         = new Application_Model_RequestProject();

$listPaymentDetail=$QRequestPrPayment->getPayment();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$department_id_check = $userStorage->department;

$has_request_view = $QRequestConfirm->checkRequestView($userStorage->id);


$request_type_group_list = $QRequestTypeGroup->fetchAll(['department_id = ?' => $department_id_check]);
$request_type_list = $QRequestType->getRequetsType($department_id_check);
$request_project_list = $QRequestProject->fetchAll(['department_id = ?' => $department_id_check]);

$list_staff_confirm = $QRequestConfirm->listStaffConfirm();

if(!in_array($userStorage->id, $list_staff_confirm)){
    $request_own = 1;
}

$total = 0;
$limit = 10;
$offset = ($page - 1) * $limit;
$title = $Qstaff->getStaffInfo($userStorage->id);
$channel = $QstaffChannel->get_staff_channel($userStorage->id);
$channel_id = array();
foreach ($channel as $key => $value) {
    $channel_id[$key] = $key;
}

$staff_storage_id = $userStorage->id;

//Nếu là thanhhuyen.pham1 thì cho xem danh sách như a Vủ nhưng không đc confirm
if($staff_storage_id == 26995){
    //$staff_storage_id = 17;
    //$request_own = 0;
}


        
$params = array(
    'team_name' => $title["team_name"],
    'team_id' => $userStorage->team,

    'title_name' => $title["title_name"],
    'channel' => $channel_id,
    'channel_id' => $channel_search_id,
    'company_id' => $company_id,
    'category' => $category,
    'content' => $content,
    "staff_id" => $staff_storage_id,
    "status_filter" => $status_filter,
    "from_date" => $from_date,
    "to_date" => $to_date,
    "request_own" => $request_own,
    'month_fee' => !empty($month_year_fee_[1]) ? $month_year_fee_[1] : NULL,
    'year_fee' => !empty($month_year_fee_[0]) ? $month_year_fee_[0] : NULL,
    'department_id' => $department_id,
    'team_id' => $team_id,
    'project' => $project,
    'request_type_group' => $request_type_group,
    'request_type' => $request_type,
    'category_id' => $category_id,
    'month_year_fee' => $month_year_fee,
    'step' => $step,
    'export' => $export,
    'cost_temp' => $cost_temp,
    'payee' => $payee,
    'request_id' => $request_id,
    'is_year_contract' => $is_year_contract,
    'view_all' => $view_all,
    'has_request_view' => $has_request_view
);
$params['sort']                 = $sort;
$params['desc']                 = $desc;


$title = $Qstaff->getStaffInfo($userStorage->id);

$listRequest = $QRequest->fetchPaginationNew($page, $limit, $total, $params);

if($export and $export == 1) {
    $this->reportList($listRequest);
}

$list_request_id = [];

foreach($listRequest as $key => $value){
    $listStaffApprove=$QRequest->getStaffApproved($value["id"]);

    $listRequest[$key]["staff_approved"]=$listStaffApprove[0];
    foreach ($listStaffApprove[0] as $k => $v) {
        $listRequest[$key]["staff_approved"]=str_replace(",","<br>",$v);
    }
    

    $where=null;
    $where=$QRequestFile->getAdapter()->quoteInto("request_id=?",$value["id"]);
    $listFile=$QRequestFile->fetchAll($where);
    foreach ($listFile as $k => $v) {
        # code....
        $listRequest[$key]["file_name"][$k]=$v["name"];
        $listRequest[$key]["file_url"][$k]=$v["url"];
    }
    
    $list_request_id[] = $value["id"];
    
}

$check_approve = $QRequestConfirm->checkRequestConfirm($userStorage->id, $list_request_id);

$check_has_approve = 0;
$check_tbp_has_approve = 0;
foreach($check_approve as $key=>$value){
    if($value['finish_request'] == 1){
        $check_has_approve = 1;
    }
    
    $check_tbp_has_approve = 1;
}
$this->view->check_has_approve = $check_has_approve;
$this->view->check_tbp_has_approve = $check_tbp_has_approve;        


$category = $QRequestCategory->getCategory();
$staff = $Qstaff->get_cache();
$company = $QCompany->get_cache();
$channel = $QstaffChannel->get_channel();
$department = $QTeam->get_list_department();

$this->view->listPaymentDetail = $listPaymentDetail;
$this->view->category = $category;
$this->view->company = $company;
$this->view->department = $department;
$this->view->channel = $channel;
$this->view->offset = $offset;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'request-office/list-request' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->check_approve = $check_approve;
$this->view->request_type_group_list = $request_type_group_list;
$this->view->request_type_list = $request_type_list;
$this->view->request_project_list = $request_project_list;
$this->view->department_id_check = $department_id_check;

$this->view->IS_MOBILE=$detect_device->isMobile();
$this->view->listRequest = $listRequest;
$this->view->userStorage = $userStorage;
$this->view->staff_id = $userStorage->id;
$this->view->staff = $staff;

$this->view->has_request_view = $has_request_view;

$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
$teams            = $QTeam->get_cache();

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$this->view->teams = $teams;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
$messages          = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$multi_confirm = $this->getRequest()->getParam('multi_confirm', null);
$finance_payment_date = $this->getRequest()->getParam('finance_payment_date');

//Cập nhật lại trạng thái multiple
if(!empty($multi_confirm)){
    try{

   $QAppNoti=new Application_Model_AppNoti();

    foreach($multi_confirm as $k => $id_request){
        
        $child_finance_payment_date = NULL;
        $child_finance_payment_date = !empty($finance_payment_date[$k]) ? $finance_payment_date[$k] : NULL;
        
        $requestDetail = $QRequest->findRequestByID($id_request);
        
        $step = $requestDetail['step'];
        
        $pr_id = null;
        $data = array(
            'step' => $step+1,
            'finance_payment_date' => !empty($child_finance_payment_date) ? $child_finance_payment_date : NULL,
        );
        
        $is_last_confirm = $QRequest->checkIsLastConfirm($requestDetail["id"], $step);
        $is_success_confirm_internal = $QRequest->checkSuccessInternalConfirm($requestDetail["id"], $step);
        
        if($is_success_confirm_internal){
            if(!empty($requestDetail["pr_sn"])){
                $where_pr          = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?', $requestDetail["pr_sn"]);
                $row_pr = $QPurchasingRequest->fetchRow($where_pr);

                if($row_pr['status'] < 3){
                    $flashMessenger->setNamespace('error')->addMessage("Waiting Purchasing's Confirmed !");
                    $messages_error = $flashMessenger->setNamespace('error')->getMessages();
                    $this->view->messages_error = $messages_error;
                    $this->redirect(HOST . 'request-office/confirm-request?id=' . $id_request);
                }
            }
        }
        
        if($is_last_confirm && $requestDetail['has_payment'] == 1 && $requestDetail['category_id'] != 4 && $requestDetail['category_id'] != 5){
            $option['company_id'] = $requestDetail['company_id'];
            $option['payment_type'] = $requestDetail['payment_type'];
            $option['request_id'] = $requestDetail['id'];
            $option['step'] = $step+1;
            $option['finance_staff'] = $requestDetail['finance_staff'];
            $option['created_by'] = $requestDetail['created_by'];

            //Bỏ list finance confirm cũ
            $where = NULL;
            $where[] = $QRequestConfirm->getAdapter()->quoteInto("request_id = ?", $requestDetail['id']);
            $where[] = $QRequestConfirm->getAdapter()->quoteInto("type = 2");
            $QRequestConfirm->delete($where);

            $list_confirm_finance = $QRequestConfirm->listFinanceConfirm($option);
        }
        
        //$isCreatePr = $QRequest->checkCreatePurchasing($requestDetail["id"], $data['step']);
        
        if($is_last_confirm && $requestDetail['has_purchasing_request'] == 1 && empty($requestDetail['pr_sn'])){
            //Lấy dữ liệu từ request để tạo pr

            $where = null;
            $where=$QRequestOfficePR->getAdapter()->quoteInto("request_id = ?",$requestDetail["id"]);
            $dataPurchasing=$QRequestOfficePR->fetchAll($where)->toArray();
            if(!empty($dataPurchasing)){
                $sn = date('YmdHis') . substr(microtime(), 2, 4);
                $where          = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?', $sn);
                $row_Purchasing = $QPurchasingRequest->fetchRow($where);
                $dataPurchasing=$dataPurchasing[0];

                $dataPurchasingReal=array(
                "sn" => $sn,
                "status" => $dataPurchasing["status"],
                "name" => $dataPurchasing["name"],
                "type" => $dataPurchasing["type"],
                "urgent_date" => $dataPurchasing["urgent_date"],
                "project_id" =>$dataPurchasing["project_id"],
                "team_id" => $dataPurchasing["team_id"],
                "department_id" => $dataPurchasing["department_id"],
                "area_id"=> $dataPurchasing["area_id"],
                "shipping_address" => $dataPurchasing["shipping_address"],
                "have_contract" => $dataPurchasing["have_contract"],
                "created_by"=> $dataPurchasing["created_by"],
                "created_at" => date("Y-m-d H:i:s"),
                );
                if (!empty($row_Purchasing)) {//TH Edit

                }
                else {//TH create
                    $pr_id = $QPurchasingRequest->insert($dataPurchasingReal);
                    
                    if(!empty($pr_id)){
                        $data["pr_sn"] = $sn;
                    }
                    
                }
            }
        }
        
        
        //Send Notification
        if($is_last_confirm){
            $staffIdPushNotification = $QRequest->getNextStaffApproving($id_request, $data['step'], 2);
        }
        else{
            $staffIdPushNotification = $QRequest->getNextStaffApproving($id_request, $data['step'], 1);
        }
        
        if(!empty($staffIdPushNotification['staff_id'])){
        $data_noti = [
                    'title' => "OPPO VIETNAM",
                    'message' => "Có một đề xuất đang chờ bạn duyệt",
                    'link' => "/request-office/list-request",
                    'staff_id' => $staffIdPushNotification['staff_id']
                ];
        
                $push_noti = $QAppNoti->sendNotificationNoTransactionERP($data_noti);
                $push_noti = $QAppNoti->sendNotificationNoTransaction($data_noti);
            
        }
        $data['status'] = !empty($staffIdPushNotification['title_confirm']) ? $staffIdPushNotification['title_confirm'] : 'Hoàn thành';
        
        //insert into request_approve
        $data_confirm = array(
            'is_confirm' => 1,
            'confirm_date' => date("Y-m-d h:i:s"),
            'note' => null,
        );
        
        $staff_id_confirm = $userStorage->id;
        
        $where_confirm = [];
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('staff_id = ?', $staff_id_confirm);
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('request_id = ?', $id_request);
        $where_confirm[] = $QRequestConfirm->getAdapter()->quoteInto('step = ?', $step);
        $QRequestConfirm->update($data_confirm, $where_confirm);
        
        $where = null;
        $where = $QRequest->getAdapter()->quoteInto('id = ?', $id_request);
        $QRequest->update($data, $where);

    }
    $flashMessenger->setNamespace('success')->addMessage('Xét duyệt đề xuất thành công !');
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
    $this->redirect(HOST . 'request-office/list-request');
}catch(Exception $e){
    echo $e;exit;

}

}


