<?php


    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');
    $quarter = $this->getRequest()->getParam('quarter', [1]);

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $months = [];
    foreach($quarter as $key=>$value){
        if($value == 1){
            $months[] = 1;$months[] = 2;$months[] = 3;
        }
        if($value == 2){
            $months[] = 4;$months[] = 5;$months[] = 6;
        }
        if($value == 3){
            $months[] = 7;$months[] = 8;$months[] = 9;
        }
        if($value == 4){
            $months[] = 10;$months[] = 11;$months[] = 12;
        }
    }
    
    $QTeam = new Application_Model_Team();
    $QRequestOffice = new Application_Model_RequestOffice();
    $QRequestTypeGroup = new Application_Model_RequestTypeGroup();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    
    $level = $this->storage['level'];
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
        //'department_id' => $userStorage->department
        'department_id' => 156,
        'quarter' => $quarter,
        'months' => $months,
    );
    
    
    
    $request_type_group = $QRequestTypeGroup->getListTypeGroup($params);
    $get_request_type = $QRequestTypeGroup->getListType($params);
    
    $get_budget = $QRequestTypeGroup->getBudget($params);
    $get_use = $QRequestTypeGroup->getUse($params);
    $get_list_use = $QRequestTypeGroup->getListPaymentUse($params);
    
    
    $budget = [];
    foreach($get_budget as $key=>$value){
        $budget[$value['request_type']] = $value['cost'];
    }
    
    $use = [];
    foreach($get_use as $key=>$value){
        $use[$value['request_type']] = $value['total_use'];
    }
    
    $request_use = [];
    foreach($get_list_use as $key=>$value){
        $request_use[$value['request_type']][] = $value;
    }
    
    $request_type = [];
    foreach($get_request_type as $key=>$value){
        $request_type[$value['group_id']][] = $value;
    }
    

    $this->view->plan = $plan;
    
    $this->view->request_type_group = $request_type_group;
    $this->view->request_type = $request_type;
    $this->view->budget = $budget;
    $this->view->use = $use;
    $this->view->request_use = $request_use;
    
    $this->view->is_admin = $this->is_admin;
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'capacity/list-capacity' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    
    