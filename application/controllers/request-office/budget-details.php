<?php
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$request_type_id    = $this->getRequest()->getParam('request_type');
$sort               = $this->getRequest()->getParam('sort', '');
$desc               = $this->getRequest()->getParam('desc', 1);
$sn                 = $this->getRequest()->getParam('sn');
$details            = $this->getRequest()->getParam('details');
$note               = $this->getRequest()->getParam('note');
$department_id      = $this->getRequest()->getParam('department_id');
$team_id            = $this->getRequest()->getParam('team_id');


$QRequestType           = new Application_Model_RequestType();
$QRequestOfficePlan     = new Application_Model_RequestOfficePlan();
$QRequestOfficePlanInfo = new Application_Model_RequestOfficePlanInfo();

$params = array(
    'request_type'       => $request_type_id,
    'team_id_list'       => $userStorage->team,
    'department_id_list' => $userStorage->department,
    'details'            => $details,
    'note'               => $note,
    'team_id'            => $team_id,
    'department_id'      => $department_id
);

$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;
$total = 0;
$result       = $QRequestOfficePlan->fetchPagination($page, $limit, $total, $params);

if($request_type_id){
    $request_type = $QRequestType->getInfoById($request_type_id);
}
   

if(count($result) == 0){
    //$this->_helper->layout->disableLayout();
    //$this->_helper->viewRenderer->setNoRender(true);
    //echo 'Không tìm thấy dữ liệu cần, vui lòng tạo mới!';exit;
}

$this->view->list   = $result;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->params = $params;
$this->view->desc   = $desc;
$this->view->sort   = $sort;
$this->view->url = HOST . 'request-office/budget-details' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);

$flashMessenger = $this->_helper->flashMessenger;
$this->view->request_type = $request_type;

if (!empty($flashMessenger->setNamespace('error')->getMessages())) {
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}




