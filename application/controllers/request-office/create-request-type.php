<?php
    
    $flashMessenger     = $this->_helper->flashMessenger;
        
    if($this->getRequest()->isPost()) {
        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {

            $request_group_id  = $this->getRequest()->getParam('request_group_id');
            $name_category  = $this->getRequest()->getParam('name_category');
            $department_id = $this->getRequest()->getParam('department_id');
            $type = $this->getRequest()->getParam('type');

            $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
            $QRequestType = new Application_Model_RequestType();
            $QRequestTypeGroup = new Application_Model_RequestTypeGroup();

            if($type == 'add_category'){
                $data = array(
                    'group_id'  => $request_group_id,
                    'title'     => $name_category,
                    'desc'      => $name_category,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'created_by'    => $userStorage->id
                );

                $QRequestType->insert($data);
            }
            elseif($type == 'add_group_category'){
                $data = array(
                    'title'         => $name_category,
                    'desc'          => $name_category,
                    'department_id' => $department_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'created_by'    => $userStorage->id
                );

                $QRequestTypeGroup->insert($data);
            }



            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Hoàn thành');                

        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->redirect('request-office/edit-request-type?department_id='.$department_id);

        }
        $this->redirect('request-office/edit-request-type?department_id='.$department_id);
    }
    
    
    
    
    
        
    