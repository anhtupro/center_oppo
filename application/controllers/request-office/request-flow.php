<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $sn = $this->getRequest()->getParam('sn');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();


    $params = array(
        'sn' => $sn,
    );

    $params['sort'] = $sort;
    $params['desc'] = $desc;


    $QRequestFlow = new Application_Model_RequestFlow();
    $page = $this->getRequest()->getParam('page', 1);
    $limit = 100;
    $total = 0;
    $result = $QRequestFlow->fetchPagination($page, $limit, $total, $params);

    
    $list_flow = $QRequestFlow->fetchFlow($params);
    $data_flow = [];
    foreach($list_flow as $key=>$value){
        $data_flow[$value['department_team_id']][] = $value;
    }
    

    $this->view->list = $result;
    $this->view->data_flow = $data_flow;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'request-office/request-flow' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);