<?php


$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$QAppNoti = new Application_Model_AppNoti();
$QRequestConfirm = new Application_Model_RequestConfirm();


$company = $this->getRequest()->getParam('company');
$id = My_Util::escape_string($this->getRequest()->getParam('id'));
$has_payment = $this->getRequest()->getParam('has_payment', 0);
$has_purchasing_request = $this->getRequest()->getParam('has_purchasing_request', 0);

$payee   = $this->getRequest()->getParam('payee');
$category   = $this->getRequest()->getParam('category');
$project   = $this->getRequest()->getParam('project');
$type       = $this->getRequest()->getParam('type');
$content    = $this->getRequest()->getParam('content');

$cost_temp = $this->getRequest()->getParam('cost_temp');
$cost_before = $this->getRequest()->getParam('cost_before');
$cost_advanced_payment    = $this->getRequest()->getParam('cost_advanced_payment');
$advance_request_id    = $this->getRequest()->getParam('advanced_payment');

$payment_type = $this->getRequest()->getParam('payment_type');
$finance_staff = $this->getRequest()->getParam('finance_staff');
$is_coo = $this->getRequest()->getParam('is_coo');
$many_request_id = $this->getRequest()->getParam('many_payment');

$cost_temp=str_replace(",","",$cost_temp);
$cost_before=str_replace(",","",$cost_before);
$cost_advanced_payment=str_replace(",","",$cost_advanced_payment);

$currency = $this->getRequest()->getParam('currency');

$cost_many_total = $this->getRequest()->getParam('cost_many_total');
$cost_many = $this->getRequest()->getParam('cost_many');
$cost_many_left = $this->getRequest()->getParam('cost_many_left');

$cost_many_total=str_replace(",","",$cost_many_total);
$cost_many=str_replace(",","",$cost_many);
$cost_many_left=str_replace(",","",$cost_many_left);

$request_type_group = $this->getRequest()->getParam('request_type_group', null);
$request_type = $this->getRequest()->getParam('request_type', null);


$cost_change = $this->getRequest()->getParam('cost_change');
$cost_execute = $this->getRequest()->getParam('cost_execute');
$note = $this->getRequest()->getParam('note');
$date_offer = $this->getRequest()->getParam('date_offer');
$date_success = $this->getRequest()->getParam('date_success');
$area_id = $this->getRequest()->getParam('area_id');
$department_id = $this->getRequest()->getParam('department_id', null);
$team_id = $this->getRequest()->getParam('team_id', null);
$month_year_fee = $this->getRequest()->getParam('month_year_fee');
$month_year_fee = explode('-', $month_year_fee);

$pr_area_id = $this->getRequest()->getParam('pr_area_id');
$pr_department_id = $this->getRequest()->getParam('pr_department_id', null);
$pr_team_id = $this->getRequest()->getParam('pr_team_id', null);

$contract_number = $this->getRequest()->getParam('contract_number');
$contract_from = $this->getRequest()->getParam('contract_from');
$contract_to = $this->getRequest()->getParam('contract_to');

$invoice_number = $this->getRequest()->getParam('invoice_number');
$invoice_date = $this->getRequest()->getParam('invoice_date');

$budget_id = $this->getRequest()->getParam('budget_id');
$pr_sn = $this->getRequest()->getParam('pr_sn');
$estimasted_liquidation_date = $this->getRequest()->getParam('estimasted_liquidation_date');
$is_year_contract = $this->getRequest()->getParam('is_year_contract');

$ro_project             = $this->getRequest()->getParam('ro_project');
$ro_request_type_group  = $this->getRequest()->getParam('ro_request_type_group');
$ro_request_type        = $this->getRequest()->getParam('ro_request_type');
$ro_months              = $this->getRequest()->getParam('ro_months');
$ro_cost                = $this->getRequest()->getParam('ro_cost');

$child_purchasing       = $this->getRequest()->getParam('child_purchasing');
$budget_purchasing      = $this->getRequest()->getParam('budget_purchasing');
$budget_use             = $this->getRequest()->getParam('budget_use');

//Purchasing Order
if(!empty($has_purchasing_request)) {
//    $sn_pr = $this->getRequest()->getParam('id_pr');
    $name_pr = $this->getRequest()->getParam('name_pr');
    $type_pr = $this->getRequest()->getParam('type_pr', null);
    $urgent_date = $this->getRequest()->getParam('urgent_date');
    $project_id = $this->getRequest()->getParam('project_id');

    $shipping_address = $this->getRequest()->getParam('shipping_address');
}

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QRequestOfficePR= new Application_Model_RequestOfficePR();
$QRequest = new Application_Model_RequestOffice();
$QRequestFile = new Application_Model_RequestFileOffice();
$QRequestPRPaymentOffice=new Application_Model_RequestPRPaymentOffice();
$QRequestPayment = new Application_Model_RequestPaymentOffice();
$QRequestOfficeDetails = new Application_Model_RequestOfficeDetails();
$QRequestOfficeDetailsMonth = new Application_Model_RequestOfficeDetailsMonth();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();

$flashMessenger = $this->_helper->flashMessenger;
$created_at = date('Y-m-d H:i:s');

$data = array(
    'category_id' => $category,
    'project' => $project ? $project : NULL,
    'area_id'          => $area_id ? $area_id : NULL,
    'department_id'    => $department_id ? $department_id : NULL,
    'team_id'          => $team_id ? $team_id : NULL,
    'cost_temp' => is_numeric($cost_temp) ? $cost_temp : 0,
    'cost_before' => is_numeric($cost_before) ? $cost_before : 0,
    'cost_advanced' => $cost_advanced_payment ? $cost_advanced_payment : NULL,
    
    'currency' => !empty($currency) ? $currency : 1,
    
    'many_request_id' => $many_request_id ? $many_request_id : NULL,
    'cost_many_total' => $cost_many_total ? $cost_many_total : NULL,
    'cost_many' => $cost_many ? $cost_many : NULL,
    'cost_many_left' => $cost_many_left ? $cost_many_left : NULL,
    
    'advance_request_id' => $advance_request_id ? $advance_request_id : NULL,
    'number_change' => $cost_change,
    'cost_real' => $cost_execute,
    'has_payment' => 1,
    'payment_type' => $payment_type ? $payment_type : NULL,
    'finance_staff' => $finance_staff ? $finance_staff : NULL,
    'has_purchasing_request' => $has_purchasing_request,
    'content' => $content,
    'note' => $note,
    'company_id' => $company,
    'created_by' => $userStorage->id,
    'created_at' => $created_at,
    'is_approve' => 0,
    'date_offer' => $date_offer,
    'date_success' => $date_success,
    'del' => 0,
    'status' => $status,
    'payee' => $payee,
    'step' => 1,
    'request_type_group' => !empty($request_type_group) ? $request_type_group : NULL,
    'request_type' => !empty($request_type) ? $request_type : NULL,
    'month_fee' => !empty($month_year_fee[1]) ? $month_year_fee[1] : NULL,
    'year_fee' => !empty($month_year_fee[0]) ? $month_year_fee[0] : NULL,
    'contract_number' => !empty($contract_number) ? $contract_number : NULL,
    'contract_from' => !empty($contract_from) ? $contract_from : NULL,
    'contract_to' => !empty($contract_to) ? $contract_to : NULL,
    'is_coo' => !empty($is_coo) ? $is_coo : NULL,
    'budget_id' => !empty($budget_id) ? $budget_id : NULL,
    'pr_sn' => !empty($pr_sn) ? $pr_sn : NULL,
    'invoice_number' => !empty($invoice_number) ? $invoice_number : NULL,
    'invoice_date' => !empty($invoice_date) ? $invoice_date : NULL,
    'estimasted_liquidation_date' => !empty($estimasted_liquidation_date) ? $estimasted_liquidation_date : NULL,
    'is_year_contract' => !empty($is_year_contract) ? $is_year_contract : 0,
);

$type=$QRequest->checkTypeRequest($data);
$data["type"]=$type;
try {
    $db = Zend_Registry::get('db');

    $db->beginTransaction();

    $id = $QRequest->insert($data);
    
    $data['request_id'] = $id;
    $list_confirm = $QRequestConfirm->listConfirmFlow($data);

    if(empty($list_confirm)){
        echo json_encode([
            'status' => 0,
            'message' => "Cannot setup list confirm!",
        ]);
        return;
    }
    
    $where = null;
    $where = $QRequest->getAdapter()->quoteInto('id = ?', $id);
    $QRequest->update(['status' => $list_confirm[0]['title_confirm']], $where);
    
    $id_request = $id;

    $pr_payment_department_id = $this->getRequest()->getParam('pr_payment_department', null);
    $pr_payment_cost = $this->getRequest()->getParam('pr_payment_cost', null);
    $pr_payment_amount = $this->getRequest()->getParam('pr_payment_amount', null);
    $pr_payment_cost = str_replace(",", "", $pr_payment_cost);
    foreach ($pr_payment_cost as $key => $value) {
        $data = array();
        if($key > 0){
            $invoice_number=null;
            $invoice_date=null;
        }
        $data = array(
            "request_id" => $id,
            "cost" =>$value,
            "department_id" => $pr_payment_department_id[$key],
            "amount" => !empty($pr_payment_amount[$key]) ? $pr_payment_amount[$key]:null,
        );

        $QRequestPRPaymentOffice->insert($data);
    }
    
    
    
    if(!empty($has_purchasing_request)) {
        $data_pr = array(
            'name'             => $name_pr,
            'sn'           	   => !empty($pr_sn) ? $pr_sn : NULL,
            'request_id'       => $id,
            'type'             => $type_pr ? $type_pr : NULL,
            'urgent_date'      => $urgent_date,
            'project_id'       => $project_id ? $project_id : NULL,

            'area_id' => $pr_area_id,
            'department_id' => $pr_department_id,
            'team_id' => $pr_team_id ? $pr_team_id:NULL ,

            'shipping_address' => $shipping_address,

            'status'           => 1, //new
            'created_by'       => $userStorage->id,
            'created_at'       => date('Y-m-d H:i:s')
        );
        $id_pr = $QRequestOfficePR->insert($data_pr);

        foreach($child_purchasing as $k=>$v){
            $QPurchasingRequestDetails->update(['quantity_real' => $v], ['id = ?' => $k]);
        }
        
    }
    
    //Update request payment
    $invoice_number = $this->getRequest()->getParam('invoice_number', null);
    $invoice_date = $this->getRequest()->getParam('invoice_date', null);

    $payment_date = $this->getRequest()->getParam('payment_date');
    $payment_cost = $this->getRequest()->getParam('payment_cost');
    $bill_number = $this->getRequest()->getParam('bill_number');
    $bill_date = $this->getRequest()->getParam('bill_date');
    $recommended_date = $this->getRequest()->getParam('recommended_date');

    if(!empty($payment_cost)){
        foreach ($payment_cost as $key => $value) {
            if(!empty($value)){
                $value = str_replace(",", "", $value);

            $data = array(
                "request_id" => $id,
                "invoice_number" => $invoice_number,
                "invoice_date" => $invoice_date,
                //"payment_date" => $payment_date[$key],
                "payment_cost" => $value,
                "bill_number" => !empty($bill_number[$key]) ? $bill_number[$key] : NULL,
                "bill_date" => !empty($bill_date[$key]) ? $bill_date[$key] : NULL,
                "recommended_date" => !empty($recommended_date[$key]) ? $recommended_date[$key] : NULL,
                "created_at" => date('Y-m-d H:i:s'),
                "created_by" => $userStorage->id,
            );

            $QRequestPayment->insert($data);
            }
        }

    }
    //END Update request payment
    
    
    //Insert Request details
    if(!empty($ro_months)){
        foreach($ro_months as $key => $value){
            
            $ro_cost_child = str_replace(",","",$ro_cost[$key]);
            
            $ro_detail = [
               'request_id'             => $id,
                'project'               => !empty($ro_project[$key]) ? $ro_project[$key] : NULL,
                'request_type_group'    => !empty($ro_request_type_group[$key]) ? $ro_request_type_group[$key] : NULL,
                'request_type'          => !empty($ro_request_type[$key]) ? $ro_request_type[$key] : NULL,
                'cost'                  => !empty($ro_cost_child) ? $ro_cost_child : NULL,
                'currency'              => !empty($currency) ? $currency : 1,
            ];
            $ro_id = $QRequestOfficeDetails->insert($ro_detail);
            
            
            foreach($ro_months[$key] as $k=>$v){
                $ro_month_year = explode('-', $v);
                $ro_month = [
                    'details_id' => $ro_id,
                    'month'      => $ro_month_year[1],
                    'year'       => $ro_month_year[0],
                ];
                $QRequestOfficeDetailsMonth->insert($ro_month);
            }
            
        }
    }
    //END Insert Request details
    
    $total = count($_FILES['file']['name']);

    for ($i = 0; $i < $total; $i++) {
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'request-office' . DIRECTORY_SEPARATOR . $id;

        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);
        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
        if ($tmpFilePath != "") {
            $old_name = $_FILES['file']['name'][$i];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode);
            
            if($_FILES['file']['size'][$i] > 50000000)
            {
                echo json_encode([
                    'status' => 0,
                    'message' => "File Upload dung lượng quá lớn (Vui lòng upload file dưới 3M)",
                ]);
                return;
            }
            
            if(!in_array($extension, ['docx', 'doc', 'png', 'jpg', 'pdf', 'PDF', 'ppt', 'pptx', 'xlsx', 'xls', 'jpeg', 'PNG', 'JPEG', 'JPG', 'zip', 'ZIP', 'xml', 'XML'])){
                echo json_encode([
                    'status' => 0,
                    'message' => "File Upload sai định dạng: ".$extension,
                ]);
                return;
            }
            
            $new_name = preg_replace('/[^A-Za-z0-9\-]/', '', $tExplode[0]).'_'.md5(uniqid('', true))."." .$extension;
            $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                $url = 'files' .
                    DIRECTORY_SEPARATOR . 'request-office' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
            } else {
                $url = NULL;
            }
        } else {
            $url = NULL;
        }

        $detail_file = [
            'request_id' => $id,
            'url' => $url,
            'name' => $_FILES['file']['name'][$i],
            'hash_name' => $new_name,
            'created_at' => $created_at,
            'staff_id' => $userStorage->id,
        ];
        if (!empty($url)) {
            
            //Move file to S3
            require_once 'Aws_s3.php';
            $s3_lib = new Aws_s3();
            
            $file_location = $url;
            $detination_path = 'files/request-office/'.$id.'/';
            $destination_name = $new_name;
            
            $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
            if($upload_s3['message'] != 'ok'){
                echo json_encode([
                    'status' => 0,
                    'message' => "Upload S3 không thành công",
                ]);
                return;
            }
            // END - Move file to S3
            
            $QRequestFile->insert($detail_file);
        }
    }

   if(!empty($id_request)){
       
       $staffIdPushNotification = $QRequest->getNextStaffApproving($id_request, 1, 1);
       if(!empty($staffIdPushNotification['staff_id'])){
            $data_noti = [
                'title' => "OPPO VIETNAM",
                'message' => "Có một đề xuất đang chờ bạn duyệt.",
                'link' => "/request-office/list-request",
                'staff_id' => $staffIdPushNotification['staff_id'],
                'created_at' => date('Y-m-d H:i:s')
            ];
            
            $push_noti = $QAppNoti->sendNotificationNoTransactionERP($data_noti);
            $push_noti = $QAppNoti->sendNotificationNoTransaction($data_noti);
            
        }
   }

    //Budget
    $QRequestOfficePlanMap = new Application_Model_RequestOfficePlanMap();
    $total_use = 0;
    foreach($budget_purchasing as $key => $value){
        
        $use = str_replace(",","",$budget_use[$key]);
        
        $total_use = $total_use + $use;
        
        $plan_map = [
            'request_office_id' => $id_request,
            'plan_info_id'      => $value,
            'use'               => $use
        ];
        $QRequestOfficePlanMap->insert($plan_map);
    }
    
    if(!empty($budget_purchasing) && (float)$total_use !== (float)$cost_temp){
        echo json_encode([
            'status' => 0,
            'message' => "Tổng tiền dự trù phải bằng số tiền thanh toán! (".(float)$total_use.':'.(float)$cost_temp.")",
        ]);
        return;
    }
    //END - Budget


    $flashMessenger->setNamespace('success')->addMessage('Tạo đề xuất thành công !');
    $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
    $db->commit();

    echo json_encode([
        'status' => 1,
    ]);
    return;


} catch (exception $e) {
    $db->rollBack();

    echo json_encode([
        'status' => 0,
        'message' => "Tạo đề xuất không thành công.".$e->getMessage(),
    ]);
    return;
}
?>