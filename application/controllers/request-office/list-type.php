<?php
    
    $id = $this->getRequest()->getParam('id');
    
    $QRequestTypeGroup = new Application_Model_RequestTypeGroup();
    
    $department = $QRequestTypeGroup->getDepartment();
    
    $get_request_type_group = $QRequestTypeGroup->fetchAll(['is_del = 0'])->toArray();
    
    $request_type_group = [];
    foreach($get_request_type_group as $key => $value){
        $request_type_group[$value['department_id']][] = $value['title'];
    }
    
    

    $this->view->department = $department;
    $this->view->request_type_group = $request_type_group;
    
    
        
    