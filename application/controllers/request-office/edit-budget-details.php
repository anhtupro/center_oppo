<?php

$QRequestOffice         = new Application_Model_RequestOffice();
$QPurchasingRequestRoot = new Application_Model_PurchasingRequest();
$QPurchasingProject     = new Application_Model_PurchasingProject();
$QPurchasingType        = new Application_Model_PurchasingType();
$QPurchasingRequest     = new Application_Model_RequestOfficePR();
$QArea                  = new Application_Model_Area();
$QTeam                  = new Application_Model_Team();
$QStaff                 = new Application_Model_Staff();
$QRequestCategory       = new Application_Model_RequestCategoryOffice();
$QCompany               = new Application_Model_Company();
$QRequestType           = new Application_Model_RequestType();
$QRequestTypeGroup      = new Application_Model_RequestTypeGroup();
$QRequestProject        = new Application_Model_RequestProject();
$QSupplier              = new Application_Model_Supplier();
$QRequestOfficePlan     = new Application_Model_RequestOfficePlan();
$QRequestFilePlan       = new Application_Model_RequestFilePlan();
$QRequestOfficePlanInfo = new Application_Model_RequestOfficePlanInfo();
$id                     = $this->getRequest()->getParam('id');



if(!empty($id)){
    $where                      = $QRequestOfficePlan->getAdapter()->quoteInto('id = ?', $id );
    $where_file                 = [];
    $where_file[]               = $QRequestFilePlan->getAdapter()->quoteInto("request_id = ?", $id);
    $where_file[]               = $QRequestFilePlan->getAdapter()->quoteInto("del =?", 0);
    $rq_plan                    = $QRequestOfficePlan->fetchRow($where);
    $rq_plan_info               = $QRequestOfficePlanInfo->getInfo($id);
    $requestFile                = $QRequestFilePlan->fetchAll($where_file);
    
    $supplier       = $QSupplier->get_cache();
    $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
    $department_id  = $rq_plan['department_id'];
    $staff_id       = $userStorage->id;


    if($userStorage->id == 26928 || $userStorage->id == 28102){
        $department_id = 156;
    }
    

    $team_id                = $userStorage->team;
    $department             = $QTeam->get_list_department();
    $team                   = $QTeam->get_cache();
    $teamDefault            = $QTeam->getTeamByDepartment($department_id);
    $company                = $QCompany->getCompanyRequestOffice();
    $category               = $QRequestCategory->getCategory();
    $listAdvancedPayment    = $QRequestOffice->getAdvancedPayment($userStorage->id);
    $listManyPayment        = $QRequestOffice->getManyPayment($userStorage->id);
    $request_type_group     = $QRequestTypeGroup->fetchAll(['department_id = ?' => $department_id]);
    $request_type           = $QRequestType->getRequetsType($department_id);
    $request_project        = $QRequestProject->fetchAll(['department_id = ?' => $department_id]);

    // Get PO based on User current
    $params = array(
        "id"            => $id,
        "department_id" => $department_id,
        "status"	    =>"3",
    );

    $QPurchasingOrder         = new Application_Model_PurchasingOrder();
    $total       = 0;
    $listPO      = $QPurchasingRequestRoot->getListPrForRo($params);

    $staff_area=$QStaff->getArea($userStorage->id);

    $purchasingTypeByDepartment = $QPurchasingType->getCacheTypeByDepartment();
    $this->view->areas = $QArea->get_cache();
    $this->view->staff_area = $staff_area[0];

    $flashMessenger = $this->_helper->flashMessenger;
    $this->view->listPO         = $listPO;
    $this->view->currency_list  = $QRequestOffice->getListCurrency();
    $this->view->category       = $category;
    $this->view->company        = $company;
    $this->view->userStorage    = $userStorage;
    $this->view->department     = $department;
    $this->view->team           = $team;
    $this->view->teamDefault    = $teamDefault;
    $this->view->department_id  = $department_id;
    $this->view->team_id        = $team_id;
    $this->view->staff_id       = $staff_id;
    $this->view->request_type   = $request_type;
    $this->view->supplier       = $supplier;
    $this->view->params         = $params;
    $this->view->rq_plan        = $rq_plan;
    $this->view->rq_plan_info   = $rq_plan_info;
    $this->view->requestFile    = $requestFile;
    $this->view->request_type_group     = $request_type_group;
    $this->view->request_project        = $request_project;
    $this->view->purchasing_project     = $QPurchasingProject->fetchAll();
    $this->view->purchasing_type        = $purchasingTypeByDepartment[$userStorage->department];
    $this->view->listAdvancedPayment    = $listAdvancedPayment;
    $this->view->listManyPayment        = $listManyPayment;


    if (!empty($flashMessenger->setNamespace('error')->getMessages())) {
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages;
    }
    if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
    }


}else{
    $this->_helper->layout->disableLayout();
}
?>