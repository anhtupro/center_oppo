<?php
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    $name = $this->getRequest()->getParam('name');
    $code = $this->getRequest()->getParam('code');
    $department = $this->getRequest()->getParam('department');
    $team = $this->getRequest()->getParam('team');
    $title = $this->getRequest()->getParam('title');
    $plan_id = $this->getRequest()->getParam('plan_id');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    if($userStorage->id != 5899){
        exit;
    }
    
    $QTeam = new Application_Model_Team();
    $QCapacity = new Application_Model_Capacity();
    $QRequestTypeGroup = new Application_Model_RequestTypeGroup();

    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $teams            = $QTeam->get_cache();
    
    $level = $this->storage['level'];
    
    $params = array(
        'name' => $name,
        'code' => $code,
        'department' => $department,
        'team' => $team,
        'title' => $title,
        'plan_id'   => $plan_id,
        'level'  => $level,
        'staff_id'  => $this->storage['staff_id'],
        //'department_id' => $userStorage->department,
        'department_id' => 156,
    );
    
    $list_project = $QRequestTypeGroup->getListProject($params);
    
    $get_request_type_group = $QRequestTypeGroup->getList($params);
    
    $get_request_type_budget = $QRequestTypeGroup->getListTypeBudget($params);
    $get_request_type = $QRequestTypeGroup->getListType($params);
    
    $request_type_budget = [];
    foreach($get_request_type_budget as $budget){
        $request_type_budget[$budget['request_type_id']][$budget['project']][$budget['year']][$budget['quater']] = $budget['cost'];
    }
    
    $request_type = [];
    foreach($get_request_type as $key=>$value){
        $request_type[$value['group_id']][$value['id']] = $value;
        $request_type[$value['group_id']][$value['id']]['budget'] = $request_type_budget[$value['id']];
    }
    
    $request_type_group = [];
    foreach($get_request_type_group as $key=>$value){
        $request_type_group[$value['id']] = $value;
        $request_type_group[$value['id']]['request_type'] = $request_type[$value['id']];
    }
    
    $list_month = $this->month_range('2020-01-01','2020-12-31');
    $months = [];
    foreach($list_month as $month){
        $months[] = [
            'month' => (int)(date('m', strtotime($month))),
            'month_name' => date('M', strtotime($month)),
            'year' => (int)(date('Y', strtotime($month))),
        ];
    }
    
    $quaters = [
        [
          'quater' => 1,
          'quater_name' => 'Quater 1',
          'list_months' => [1,2,3],
          'year' => 2020
        ],
        [
          'quater' => 2,
          'quater_name' => 'Quater 2',
          'list_months' => [4,5,6],
          'year' => 2020
        ],
        [
          'quater' => 3,
          'quater_name' => 'Quater 3',
          'list_months' => [7,8,9],
          'year' => 2020
        ],
        [
          'quater' => 4,
          'quater_name' => 'Quater 4',
          'list_months' => [10,11,12],
          'year' => 2020
        ],
    ];
    
    $halfYears = [
        [
          'half' => 1,
          'half_name' => 'H1',
          'list_quater' => [1,2],
          'year' => 2020
        ],
        [
          'half' => 2,
          'half_name' => 'H2',
          'list_quater' => [3,4],
          'year' => 2020
        ],
    ];
    
    $params['sort'] = $sort;
    $params['desc'] = $desc;

    $page = $this->getRequest()->getParam('page', 1);
    $limit = 20;
    $total = 0;
    $result = $QCapacity->fetchPagination($page, $limit, $total, $params);
   

    $this->view->plan = $plan;
    $this->view->request_type_group = json_encode($request_type_group);
    
    
    $this->view->is_admin = $this->is_admin;
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->teams = $teams;
    $this->view->staffs = $result;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->params = $params;
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->url = HOST . 'capacity/list-capacity' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset = $limit * ($page - 1);
    
    $this->view->months = json_encode($months);
    $this->view->quaters = json_encode($quaters);
    $this->view->halfYears = json_encode($halfYears);
    $this->view->listProjects = json_encode($list_project);
    
    