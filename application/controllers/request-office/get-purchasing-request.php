<?php
    
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $pr_sn = $this->getRequest()->getParam('pr_sn');
    $request_id = $this->getRequest()->getParam('request_id');

    $QPurchasingRequest = new Application_Model_PurchasingRequest();
    $QRequestOffice = new Application_Model_RequestOffice();
    $QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
    $QPurchasingPlan = new Application_Model_PurchasingPlan();

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $purchasing = $QPurchasingRequest->getInfoPurchasingRequest($pr_sn);
    $purchasing_details = $QPurchasingRequest->getInfoPurchasingRequestDetails($pr_sn);
    
    $details = $QPurchasingRequestDetails->getDetails($pr_sn);
    $details_request = $QPurchasingRequestDetails->getDetailsRequestBySn($pr_sn, $request_id);
    
    $params = array(
        'sn' => $pr_sn,
    );
    
    $list_budget = $QPurchasingPlan->getPurchasingPlan($params);

    $total_price_sn = 0;
    if(!empty($details)){
        foreach($details as $k=>$v){
            $total_num_sn = $total_num_sn + $v['quantity'];

            $money_ck = $price_after_ck = $total_price = 0;
            $money_ck = $v['price']*$v['ck']/100;
            $price_after_ck = $v['price'] - $money_ck;
            $total_price = $v['quantity']*$price_after_ck+$v['fee'];
            $total_price_sn = $total_price_sn + $total_price;
        }
    }
    
    $total_use = 0;
    if(!empty($details_request)){
        foreach($details_request as $k=>$v){
            $total_use = $total_use + $v['cost_temp'];
        }
    }
    
    $data = [
        'status' => 0,
        'purchasing' => $purchasing,
        'purchasing_details'    => $purchasing_details,
        'total_price'  => $total_price_sn,
        'total_use'  => !empty($total_use) ? $total_use : 0,
        'details_request_pr'   => $details_request,
        'list_budget' => $list_budget
    ];

    echo json_encode($data);
    
    
    