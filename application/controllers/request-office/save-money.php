<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    
    $p_request_type_group = $this->getRequest()->getParams();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $data_budget = $p_request_type_group['data_budget'];
    $department_id = $p_request_type_group['department_id'];
    $year = $p_request_type_group['year'];

    $department_id = $this->getRequest()->getParam('department_id');
    $year = $this->getRequest()->getParam('year');
    
    $params = array(
        'department_id' => $department_id,
        'year'          => $year
    );
    
    $QRequestOfficeBudget = new Application_Model_RequestOfficeBudget();
    $QRequestOfficeBudgetLog = new Application_Model_RequestOfficeBudgetLog();
    $QRequestTypeGroup = new Application_Model_RequestTypeGroup();
    
    $get_request_type_budget = $QRequestTypeGroup->getListTypeBudget($params);
    
    
    $check_budget = [];
    foreach($get_request_type_budget as $key=>$value){
    $check_budget[$value['request_type_id']][$value['quater']] = [
            'budget_id' => $value['budget_id'],
            'budget' => $value['cost']
        ];
    }
    
    
    $list_change = [];
    $list_new = [];
    
    foreach($data_budget as $key=>$request_group){
        foreach($request_group['request_type'] as $k=>$request_type){
            //Lấy ra từng budget theo hạng mục
            foreach($request_type['budget'] as $i=>$budget){
                //Kiểm tra khác với lúc chưa điều chỉnh không
                $k_request_type_id = $request_type['id'];
                $k_quater = $i;
                $k_check = $check_budget[$k_request_type_id][$k_quater]['budget'];
                $k_budget_id = $check_budget[$k_request_type_id][$k_quater]['budget_id'];
                
                //Nếu khác với chưa điều chỉnh
                if($budget != $k_check){
                    //Nếu đã có id thì update ko thì insert
                    if($k_budget_id){
                        $list_change[$k_budget_id] = $budget;
                    }
                    else{
                        $list_new[] = [
                            'department_id' => $department_id,
                            'year'          => $year,
                            'request_type'  => $k_request_type_id,
                            'quater'        => $k_quater,
                            'cost'          => $budget
                        ];
                    }
                    
                }
                
            }
        }
    }
    
    
    try {
        
        foreach($list_change as $key=>$value){
            $where = $QRequestOfficeBudget->getAdapter()->quoteInto('id = ?', $key);
            $update = [
                'cost' => $value
            ];
            $QRequestOfficeBudget->update($update, $where);
            
            $log = [
                'budget_id'  => $key,
                'cost'       => $value,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id
            ];
            $QRequestOfficeBudgetLog->insert($log);
        }
        
        
        foreach($list_new as $key=>$value){
            $insert = [
                'department_id' => $value['department_id'],
                'year'          => $value['year'],
                'request_type'  => $value['request_type'],
                'quater'        => $value['quater'],
                'cost'          => $value['cost']
            ];
            $id = $QRequestOfficeBudget->insert($insert);
            
            $log = [
                'budget_id'  => $id,
                'cost'       => $value['cost'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id
            ];
            $QRequestOfficeBudgetLog->insert($log);
            
        }
        
        echo json_encode([
            'status' => 0,
            'data' => $request_type_group
        ]);

    } catch (\Exception $ex) {
        echo json_encode([
            'status' => 1,
            'message' => $ex->getMessage()
        ]);
    }
    
    
    