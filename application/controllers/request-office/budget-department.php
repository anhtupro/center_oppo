<?php

    //$year = $this->getRequest()->getParam('year');

    $QTeam = new Application_Model_Team();
    $QRequestOfficeBudget = new Application_Model_RequestOfficeBudget();
    $QRequestOfficePlan = new Application_Model_RequestOfficePlan();
    
    $params = [
        'year' => 2021
    ];
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $where = [];
    $where[] = $QTeam->getAdapter()->quoteInto('id IN (?)', [434, 160, 149, 557, 154, 150, 156, 400, 610, 152, 159, 153, 151]);
    
    $admin = unserialize(BUDGET_ADMIN);
    
    if(!in_array($userStorage->id, $admin)){
        $where[] = $QTeam->getAdapter()->quoteInto('id = ?', $userStorage->department);
    }
    
    //Get Budget
    $get_deparment_budget = $QRequestOfficeBudget->listDepartmentBudget($params);
    
    $deparment_budget = [];
    $total_budget = 0;
    foreach($get_deparment_budget as $key=>$value){
        $deparment_budget[$value['department_id']] = $value['total_cost'];
        
        $total_budget = $total_budget + $value['total_cost'];
        
    }
    
    //Get Plan
    $get_deparment_plan = $QRequestOfficePlan->listDepartmentPlan($params);
    
    $deparment_plan = [];
    $total_plan = 0;
    foreach($get_deparment_plan as $key=>$value){
        $deparment_plan[$value['department_id']] = $value['total_plan'];
        
        $total_plan = $total_plan + $value['total_plan'];
    }
    
    //Get request office use
    $get_request_office_use = $QRequestOfficePlan->listRequestOfficeUse($params);
    
    $request_office_use = [];
    $total_use = 0;
    foreach($get_request_office_use as $key=>$value){
        $request_office_use[$value['department_id']] = $value['total_use'];
        
        $total_use = $total_use + $value['total_use'];
    }
    
    //Get request office use by quater
    $get_request_office_use_quater = $QRequestOfficePlan->listRequestOfficeUseQuater($params);
    
    $request_office_use_quater = [];
    foreach($get_request_office_use_quater as $key=>$value){
        $request_office_use_quater[$value['department_id']][$value['quarter']] = $value['total_use'];
    }
    
    $list_department = $QTeam->fetchAll($where);
    
    $this->view->list_department = $list_department;
    $this->view->deparment_budget = $deparment_budget;
    $this->view->deparment_plan = $deparment_plan;
    $this->view->request_office_use = $request_office_use;
    $this->view->total_budget = $total_budget;
    $this->view->total_plan = $total_plan;
    $this->view->total_use = $total_use;
    $this->view->request_office_use_quater = $request_office_use_quater;
    
    
    