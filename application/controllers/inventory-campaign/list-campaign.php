<?php


 $QInventoryCampaign = new Application_Model_InventoryCampaign();

$page = $this->getRequest()->getParam('page', 1);
$limit = LIMITATION;

$list = $QInventoryCampaign->fetchPagination($page, $limit,$total, $params);

$this->view->list = $list;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'inventory-campaign/list-campaign' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);