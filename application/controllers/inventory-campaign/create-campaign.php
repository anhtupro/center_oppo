<?php
$QInventoryCampaign= new Application_Model_InventoryCampaign();

$id = $this->getRequest()->getParam('id');

if ($id) {
    $campaign = $QInventoryCampaign->fetchRow(['id =?' => $id]);
}

$this->view->campaign = $campaign;
