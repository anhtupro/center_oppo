<?php
$QStoreStaffLog = new Application_Model_StoreStaffLog();
$QInventoryCampaignBrand = new Application_Model_InventoryCampaignBrand();
$QInventoryCampaign = new Application_Model_InventoryCampaign();
$QInventoryCampaignData = new Application_Model_InventoryCampaignData();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$id = $this->getRequest()->getParam('id');

$list_store = $QStoreStaffLog->getListStore($userStorage->id, 1, date('Y-m-d'));
if (! $list_store) {
    echo 'Sale chưa được gán shop';
    die;
}
$list_brand = $QInventoryCampaignBrand->getAll();
$campaign = $QInventoryCampaign->fetchRow(['id =?' => $id]);
$list_data = $QInventoryCampaignData->getData($id, $userStorage->id);
$list_store_not_finish = $QInventoryCampaign->getStoreNotFinish($id, $userStorage->id, $campaign['type']);

if(date('Y-m-d') > $campaign['to_date'] || date('Y-m-d') < $campaign['from_date']) {
    echo 'Không đúng thời gian điền khảo sát';
    die;
}

$this->view->list_store = $list_store;
$this->view->list_brand = $list_brand;
$this->view->userStorage = $userStorage;
$this->view->campaign = $campaign;
$this->view->list_data = $list_data;
$this->view->list_store_not_finish = $list_store_not_finish;