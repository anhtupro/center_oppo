<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$QInventoryCampaign = new Application_Model_InventoryCampaign();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$id = $this->getRequest()->getParam('id');
$name = $this->getRequest()->getParam('name');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$inventory = $this->getRequest()->getParam('inventory');
$sellout = $this->getRequest()->getParam('sellout');

$from_date = str_replace('/', '-', $from_date);
$to_date = str_replace('/', '-', $to_date);

if ($inventory & !$sellout) {
    $type = 1;
} elseif (!$inventory & $sellout) {
    $type = 2;
} elseif ($inventory & $sellout) {
    $type = 3;
}
if (!$name || !$from_date || !$to_date || !$type) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa điền đủ thông tin'
    ]);
    return;
}

if (!$id) { // insert
    $QInventoryCampaign->insert([
        'name' => $name,
        'from_date' => date('Y-m-d', strtotime($from_date)),
        'to_date' => date('Y-m-d', strtotime($to_date)),
        'status' => 1,
        'created_by' => $userStorage->id,
        'created_at' => date('Y-m-d H:i:s'),
        'type' => $type
    ]);


} else { // edit
    $QInventoryCampaign->update([
        'name' => $name,
        'from_date' => date('Y-m-d', strtotime($from_date)),
        'to_date' => date('Y-m-d', strtotime($to_date)),
        'status' => 1,
        'created_by' => $userStorage->id,
        'created_at' => date('Y-m-d H:i:s'),
        'type' => $type
    ],['id =?' => $id]);
}

echo json_encode([
    'status' => 0
]);
return;