<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$QInventoryCampaign = new Application_Model_InventoryCampaign();

$campaign_id = $this->getRequest()->getParam('campaign_id');

$QInventoryCampaign->update([
   'status' => 0
], ['id = ?' => $campaign_id]);

echo json_encode([
    'status' => 0
]);
