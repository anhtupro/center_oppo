<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$QInventoryCampaignData = new Application_Model_InventoryCampaignData();
$QInventoryCampaignBrand = new Application_Model_InventoryCampaignBrand();
$QStore = new Application_Model_Store();

$campaign_id = $this->getRequest()->getParam('campaign_id');
$store_id = $this->getRequest()->getParam('store_id');
$inventory = $this->getRequest()->getParam('inventory');
$sellout = $this->getRequest()->getParam('sellout');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$db = Zend_Registry::get('db');

$list_brand = $QInventoryCampaignBrand->getAll();

if (!$campaign_id || !$store_id) {
    echo json_encode([
        'status' => 1,
        'message' => 'Thiếu data!'
    ]);
    return;
}

$store_info = $QStore->getStoreInfo($store_id);

$db->beginTransaction();
try {
    $QInventoryCampaignData->delete([
       'store_id = ?' => $store_id,
       'campaign_id =?' => $campaign_id
    ]);

    foreach ($list_brand as $brand_id => $brand_name) {
        $sellout_insert = $sellout[$brand_id];
        $inventory_insert = $inventory[$brand_id];

        if (!is_null($sellout_insert) || !is_null($inventory_insert)) {
            $QInventoryCampaignData->insert([
                'campaign_id' => $campaign_id,
                'store_id' => $store_id,
                'area_id' => $store_info['area_id'],
                'province_id' => $store_info['province_id'],
                'district_id' => $store_info['district_id'],
                'brand_id' => $brand_id,
                'sellout' => $sellout_insert,
                'inventory' => $inventory_insert,
                'created_by' => $userStorage->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

    }

    $db->commit();

} catch (Exception $e){
//    ($e->getMessage());
    echo json_encode([
        'status' => 1,
        'message' => 'Phát hiện Lỗi, xin liên hệ bộ phận kỹ thuật để được hỗ trợ'
    ]);
    return;
}



echo json_encode([
    'status' => 0
]);
return;


