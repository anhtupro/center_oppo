<?php
class CollectionController extends My_Controller_Action
{   
    
    public function getProvinceDefaultByAreaAction() {
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        $QRegionalMarket = new Application_Model_RegionalMarket();
        
        $area_id    = 1;
        $where      = array();
        $where[]    = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
        $where[]    = $QRegionalMarket->getAdapter()->quoteInto('default_province = ?', 1);
        $province   = $QRegionalMarket->fetchRow($where);
        if(!empty($province)){
            $province  = $province->toArray();
            print_r($province['id']);
        }
        
        
    }
    public function bonusAdminAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'bonus-admin.php';
    }
    public function resultBonusAdminAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'result-bonus-admin.php';
    }
	
	  public function bonusPgsAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'bonus-pgs.php';
    }
    
    public function uploadBonusAdminAction(){
    }
     public function uploadBonusPgsAction(){
    }
    public function saveUploadBonusAdminAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'save-upload-bonus-admin.php';
    }
     public function saveUploadBonusPgsAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'save-upload-bonus-pgs.php';
    }
    public function adminTransferAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'admin-transfer.php';
    }
    
    public function infoCustomerAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'info-customer.php';
    }
    
    public function getTransferTmpInfoAction() {
        /**
         * @hac.tran
         * @description: get transfer and staff log detail
         */
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        $transfer_id = $this->getRequest()->getParam('transfer_id');
        $db          = Zend_Registry::get('db');
        $arrCols     = array(
            'from_date'     => 'p.from_date',
            'staff_id'     => 'p.staff_id',
            'info_type'     => 'sl.info_type',
            'current_value' => 'sl.current_value',
        );
        $select      = $db->select()
                ->from(array('p' => 'staff_transfer_tmp'), $arrCols)
                ->join(array('sl' => 'staff_log_detail_tmp'), 'p.id = sl.transfer_id', array())
                ->where('p.id = ?', $transfer_id)
        ;

        $result = $db->fetchAll($select);
        if (!$result) {
            exit(json_encode(array(
                'code'    => 1,
                'message' => 'load data fail'
            )));
        }

        $data = array();
        foreach ($result as $item) {
            if ($item['info_type'] == My_Staff_Info_Type::Company) {
                $data['company'] = $item['current_value'];
            } elseif ($item['info_type'] == My_Staff_Info_Type::Area) {
                $data['area'] = $item['current_value'];
            } elseif ($item['info_type'] == My_Staff_Info_Type::Region) {
                $data['region'] = $item['current_value'];
            } elseif ($item['info_type'] == My_Staff_Info_Type::Department) {
                $data['department'] = $item['current_value'];
            } elseif ($item['info_type'] == My_Staff_Info_Type::Team) {
                $data['team'] = $item['current_value'];
            } elseif ($item['info_type'] == My_Staff_Info_Type::Group) {
                $data['group'] = $item['current_value'];
            } elseif ($item['info_type'] == My_Staff_Info_Type::Title) {
                $data['title'] = $item['current_value'];
            }
            $data['from_date'] = date('Y-m-d', strtotime($item['from_date']));
            $data['staff_id'] = $item['staff_id'];
        }

        exit(json_encode(array(
            'code'   => 2,
            'result' => $data
        )));
    }
    
     public function approveTransferAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'approve-transfer.php';
    }
    
      public function adminSaveTransferAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'admin-save-transfer.php';
    }
    
    public function asmExportStaffAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'asm-export-staff.php';
    }
    
    
    public function listGiftAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'list-gift.php';
    }
    private function export($list_gift,$area){
        require_once 'collection'.DIRECTORY_SEPARATOR.'export-gift.php';
    }
    public function reportAreaTrainingAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'report-area-training.php';
    }
    private function areaExport($from_date, $to_date){
        require_once 'collection'.DIRECTORY_SEPARATOR.'area-export.php';
    }
    public function reportPgCountByChanelAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'report-pg-count-by-chanel.php';
    }
    private function pgCountByChanelExport(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'pg-count-by-chanel-export.php';
    }
    public function selloutByChanelAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'sellout-by-chanel.php';
    }
    private function selloutByChanelExport($from_imei_kpi,$to_imei_kpi,$from_diamond_club,$to_diamond_club){
        require_once 'collection'.DIRECTORY_SEPARATOR.'sellout-by-chanel-export.php';
    }
    public function selloutWorkingDayAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'sellout-working-day.php';
    }
    private function selloutWokingDayExport($from_date, $to_date){
        require_once 'collection'.DIRECTORY_SEPARATOR.'sellout-working-day-export.php';
    }
    
   
    public function testNotiAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
            $db = Zend_Registry::get('db');
            
            $sql = "CALL `PR_Get_Manager_By_Code`(:pCode)";
            $code = '16060001';
            $stmt = $db->prepare($sql);
            $stmt->bindParam('pCode', $code, PDO::PARAM_STR);
            $stmt->execute();

            $data = $stmt->fetch();
            $stmt->closeCursor();
            $stmt=null;
            $id = $data['id'];
        
            
            $QAppNoti = new Application_Model_AppNoti();
            $data = [
                    'title' => "Bạn có 1 thông báo từ OPPO Test Permission",
                    'message' => "Test Permission",
                    'link' => "/staff-time/staff-view",
                    'staff_id' => $id
            ];
            $data_res = $QAppNoti->sendNotification($data);
            
            echo "<pre>";
            print_r($data_res);
            echo "</pre>";
    } 

    public function createSmAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'controller-responsive'.DIRECTORY_SEPARATOR.'create-sm.php';
    }

    public function listStaffCheckInSmAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'controller-responsive'.DIRECTORY_SEPARATOR.'list-staff-check-in-sm.php';
    }
    public function staffViewSmAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'controller-responsive'.DIRECTORY_SEPARATOR.'staff-view-sm.php';
    }
    public function listStaffApproveSmAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'controller-responsive'.DIRECTORY_SEPARATOR.'list-staff-approve-sm.php';
    }
    public function timingCreateSmAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'controller-responsive'.DIRECTORY_SEPARATOR.'timing-create-sm.php';
    }
    public function viewStaffTimeAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'controller-responsive'.DIRECTORY_SEPARATOR.'view-staff-time.php';
    }
    public function listGiftLunarNewYearAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'list-gift-lunar-new-year.php';
    }
    private function exportListLunarNewYear($list_gift,$area){
        require_once 'collection'.DIRECTORY_SEPARATOR.'export-gift-lunar-new-year.php';
    }

    public function lunarNewYear2020Action() {
        require_once 'collection'.DIRECTORY_SEPARATOR.'lunar-new-year2020.php';
    }
    public function lunarNewYear2021Action() {
        require_once 'collection'.DIRECTORY_SEPARATOR.'lunar-new-year2021.php';
    }
    public function lunarConfirmSuccess2021Action() {
        require_once 'collection'.DIRECTORY_SEPARATOR.'lunar-confirm-success2021.php';
    }
    public function listSalaryIncreaseTotalAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'list-salary-increase-total.php';
    }
    private function exportSalaryIncrease($list_salary_increase,$area){
        require_once 'collection'.DIRECTORY_SEPARATOR.'export-salary-increase.php';
    }

    public function listSalaryIncreaseTotalTwentyAction(){//dot 1 - 2020
        require_once 'collection'.DIRECTORY_SEPARATOR.'list-salary-increase-total-twenty.php';
    }
    private function exportSalaryIncrease20($list_salary_increase,$area){//dot 1 - 2020
        require_once 'collection'.DIRECTORY_SEPARATOR.'export-salary-increase-twenty.php';
    }
    
    //Toan Nguyen 5/11/2019
    public function statusGiftAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $feedBack     = $this->getRequest()->getParam('feedback',"none");
       
        $QLunar = new Application_Model_Lunar2021();
        $where=$QLunar->getAdapter()->quoteInto("code = ? ", $userStorage->code);
        $data=array(
            "status_delivery" => 1,
            "feedback" => $feedBack ,
            "received_time" => date('Y-m-d H:i:s'),
        );
        $result=$QLunar->update($data,$where);
    }
	
	public function returnLeaveAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'return-leave.php';
    }
    
    public function massUploadDataAction(){ 
        require_once 'collection'.DIRECTORY_SEPARATOR.'mass-upload-data.php';
    } 
    
    public function massDownloadDataAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'mass-download-data.php';
    }    
    private function massExportData($area){
        require_once 'collection'.DIRECTORY_SEPARATOR.'mass-export-data.php';
    } 
    private function massExportDataReview(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'mass-export-data-review.php';
    }
    public function massCloseDataAction(){
        $id = $this->getRequest()->getParam('id');
        if ($id)
        {          
            $QMassUpload = new Application_Model_MassUpload();  
            $info           = $QMassUpload->fetchRow(['id = ?' => $id]); 
            $where          = $QMassUpload->getAdapter()->quoteInto('id = ?', $id);
            if($info['del'] == 0){                
                $QMassUpload->update(array('del' => 1), $where);
            }else{                
                $QMassUpload->update(array('del' => 0), $where);
            }
        }
        My_Controller::redirect(HOST . 'collection/mass-download-data');    
    }
    public function changeStatusSuccessAction(){
        $id = $this->getRequest()->getParam('id');
        if ($id)
        {   
            $QMassUpload = new Application_Model_MassUpload();  
            $where          = $QMassUpload->getAdapter()->quoteInto('id = ?', $id);
            $QMassUpload->update(['status_final' => 1], $where);
        }
        My_Controller::redirect(HOST . 'collection/mass-download-data');    
    }
    public function changeStatusUnsuccessAction(){
        $id = $this->getRequest()->getParam('id');
        if ($id)
        {   
            $QMassUpload = new Application_Model_MassUpload();  
            $where          = $QMassUpload->getAdapter()->quoteInto('id = ?', $id);
            $QMassUpload->update(['status_final' => 0], $where);
        }
        My_Controller::redirect(HOST . 'collection/mass-download-data');    
    }
    public function massUploadAreaAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'mass-upload-area.php';
    }
        public function confirmReceivedGiftAction(){
        require_once 'collection'.DIRECTORY_SEPARATOR.'confirm-received-gift.php';
    }
}
