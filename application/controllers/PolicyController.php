<?php
	class PolicyController extends My_Controller_Action
	{
		/*
	    * @author - thaisang.nguyen@oppomobile.vn
	    * Show dữ liệu từ bảng policy
	    * Input: số trang hiện tại
	    * Output: danh sách row, status và message
	    */

		public function indexAction()
		{
			$page = (!empty($this->_request->getParam('page')))?intval($this->_request->getParam('page')):1;
			$params_ = array(
					'page' => intval($page),
					'limit'=> 10,
					'offset' => intval(($page - 1) * 10),
				);
			$PolicyModel = new Application_Model_Policy();

			$data = $PolicyModel->selectPolicy($params_);

			$this->view->data = $data;
			$this->view->offset = $params_['offset'];
			$this->view->limit = $params_['limit'];
			
			$this->view->total = $data['total'];
			unset($params_['page']);
			$this->view->url = HOST.'policy/index'.( $params_ ? '?'.http_build_query($params_).'&' : '?' );

		}

		/*
	    * @author - thaisang.nguyen@oppomobile.vn
	    * Thêm dữ liệu vào bảng policy
	    * Input: title, notes, danh sách khu vực dạng string (a, b, c)
	    * Output: Trạng thái thêm thành công hoặc không
	    * Note: create_at lấy current time, update_at để null.
	    */

		public function createAction()
		{
			if($this->getRequest()->isPost())
			{
				$title = $this->getRequest()->getParam('title');
				$note = $this->getRequest()->getParam('note');
				$area_id = $this->getRequest()->getParam('area_id');

				if(empty($title))
				{
					$this->view->message = array("status" => false, "message" => "Xin nhập đầy đủ thông tin");
				}
				else
				{
					
					if(empty($area_id))
					{
						// $QArea = new Application_Model_Area();
						// $all_area =  $QArea->fetchAll(null, 'name');

						// $array_area_id = array();
						// foreach ($all_area as $value) {
						// 	$array_area_id[] = $value->id;
						// }
						// $area_id_input = implode(",", $array_area_id);
						$area_id_input = '';
					}
					else
					{
						$area_id_input = implode(",", $area_id);
					}
					$params = array
					(
						'title' => $title,
						'note' => (empty($note))?"":$note,
						'area' => $area_id_input,
						'create_at' => new Zend_Db_Expr('NOW()'),
					);
					$PolicyModel = new Application_Model_Policy();

					$res = $PolicyModel->insertPolicy($params);

					$this->view->message = $res;
				}
			}
			if(!isset($all_area) || empty($all_area))
			{
				$QArea = new Application_Model_Area();
				$all_area =  $QArea->fetchAll(null, 'name');
			}
			
			$this->view->areas = $all_area;
		}

		/*
	    * @author - thaisang.nguyen@oppomobile.vn
	    * Update bảng policy
	    * Input: title, notes (khi submit), id khi get từ url, danh sách khu vực dạng string (a, b, c)
	    * Output: Trạng thái cập nhật thành công hoặc không
	    * Note: param update_at lấy giá trị current time.
	    */

		public function updateAction()
		{
			$id = intval($this->_request->getParam('id'));

			if($this->getRequest()->isPost())
			{
				$title = $this->getRequest()->getParam('title');
				$note = $this->getRequest()->getParam('note');
				$area_id = $this->getRequest()->getParam('area_id');
				
				if(empty($title))
				{
					$this->view->message = array("status" => false, "message" => "Xin nhập đầy đủ thông tin");
				}
				else
				{
					if(empty($area_id))
					{
						// $QArea = new Application_Model_Area();
						// $all_area =  $QArea->fetchAll(null, 'name');

						// $array_area_id = array();
						// foreach ($all_area as $value) {
						// 	$array_area_id[] = $value->id;
						// }
						// $area_id_input = implode(",", $array_area_id);
						$area_id_input = '';
					}
					else
					{
						$area_id_input = implode(",", $area_id);
					}

					$params_update = array
					(
						'id' => $id,
						'title' => $title,
						'note' => (empty($note))?"":$note,
						'area' => $area_id_input,
						'update_at' => new Zend_Db_Expr('NOW()'),
					);

					$PolicyModel = new Application_Model_Policy();

					$res = $PolicyModel->updatePolicy($params_update);
					$this->view->message = $res;
				}
			}

			$PolicyModel = new Application_Model_Policy();
			$params_select = array("id" => $id);
			$data = $PolicyModel->selectById($params_select);
			$this->view->data = $data['data'];

			$this->view->area_id = explode(",", $data['data']['area']);

			if(!isset($all_area) || empty($all_area))
			{
				$QArea = new Application_Model_Area();
				$all_area =  $QArea->fetchAll(null, 'name');
			}

			// if(count($all_area) == count($this->view->area_id))
			// {
			// 	$this->view->area_id = array();
			// }

			$this->view->areas = $all_area;
		}

		/*
	    * @author - thaisang.nguyen@oppomobile.vn
	    * Delete một dòng trong bảng policy
	    * Input: id dòng cần delete
	    * Output: Trạng thái xóa thành công hoặc không
	    */

		public function deleteAction()
		{
			$id = intval($this->_request->getParam('id'));
			$PolicyModel = new Application_Model_Policy();
			$params = array("id" => $id);
			$data = $PolicyModel->deletePolicy($params);
			
			$back_url = $this->getRequest()->getServer('HTTP_REFERER');

        	$this->_redirect(($back_url ? $back_url : HOST . 'policy'));
		}
	}