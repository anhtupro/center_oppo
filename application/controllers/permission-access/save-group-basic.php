<?php 
if ($this->getRequest()->getMethod() == 'POST'){
    $QGroup = new Application_Model_GroupBasic();
    $QGroupBasicMapping = new Application_Model_GroupBasicMapping();

    $id = $this->getRequest()->getParam('id');
    $id_new999 = $this->getRequest()->getParam('id_new999');
    

    $name = $this->getRequest()->getParam('name');
    $default_page = $this->getRequest()->getParam('default_page');
    $raw_access = $this->getRequest()->getParam('access');
    $raw_access_wh = $this->getRequest()->getParam('access_wh');
    $raw_access_bi = $this->getRequest()->getParam('access_bi');
    $raw_access_payment = $this->getRequest()->getParam('access_payment');
    $raw_access_retail = $this->getRequest()->getParam('access_retail');
    $menus = $this->getRequest()->getParam('menus');

    $access = array();
    if (is_array($raw_access)){
        foreach ($raw_access as $module=>$item)
            foreach ($item as $controller=>$item_2)
                foreach ($item_2 as $action=>$item_3){
                    if ($item_3)
                        $access[] = $module.'::'.$controller.'::'.$action;
                }
    }

    $access_wh = array();
    if (is_array($raw_access_wh)){
        foreach ($raw_access_wh as $module=>$item)
            foreach ($item as $controller=>$item_2)
                foreach ($item_2 as $action=>$item_3){
                    if ($item_3)
                        $access_wh[] = $module.'::'.$controller.'::'.$action;
                }
    }

    $access_bi = array();
    if (is_array($raw_access_bi)){
        foreach ($raw_access_bi as $module=>$item)
            foreach ($item as $controller=>$item_2)
                foreach ($item_2 as $action=>$item_3){
                    if ($item_3)
                        $access_bi[] = $module.'::'.$controller.'::'.$action;
                }
    }
    
    $access_payment = array();
    if (is_array($raw_access_payment)){
        foreach ($raw_access_payment as $module=>$item)
            foreach ($item as $controller=>$item_2)
                foreach ($item_2 as $action=>$item_3){
                    if ($item_3)
                        $access_payment[] = $module.'::'.$controller.'::'.$action;
                }
    }

    $access_retail = array();
    if (is_array($raw_access_retail)){
        foreach ($raw_access_retail as $module=>$item)
            foreach ($item as $controller=>$item_2)
                foreach ($item_2 as $action=>$item_3){
                    if ($item_3)
                        $access_retail[] = $module.'::'.$controller.'::'.$action;
                }
    }

    $data = array(
        'name' => $name,
        'default_page' => $default_page,
        'menu' => ( is_array($menus) ? implode(',', $menus) : null ),
    );

    if($access) {
        $data_access_center = array(
            'access' => json_encode($access),
            'default_page' => $default_page,
            'system' => 1
        );
    }

    if($access_wh) {
        $data_access_warehouse = array(
            'access' => json_encode($access_wh),
            'default_page' => $default_page,
            'system' => 2
        );
    }

    if($access_bi) {
        $data_access_bi = array(
            'access' => json_encode($access_bi),
            'default_page' => $default_page,
            'system' => 3
        );
    }
    
    if($access_payment) {
        $data_access_payment = array(
            'access' => json_encode($access_payment),
            'default_page' => $default_page,
            'system' => 4
        );
    }

    if($access_retail) {
        $data_access_retail = array(
            'access' => json_encode($access_retail),
            'default_page' => $default_page,
            'system' => 5
        );
    }
    
    if (!empty($id)){
        $where = $QGroup->getAdapter()->quoteInto('id = ?', $id);
        $QGroup->update($data, $where);

        if($data_access_center) {
            $where = $QGroupBasicMapping->getAdapter()->quoteInto('id_group_basic = ? AND `system` = 1', $id);
            if($QGroupBasicMapping->fetchRow($where)) 
                $QGroupBasicMapping->update($data_access_center, $where);
            else {
                $data_access_center['id_group_basic'] = $id;
                $QGroupBasicMapping->insert($data_access_center);
            }
        }

        if($data_access_warehouse) {
            $where = $QGroupBasicMapping->getAdapter()->quoteInto('id_group_basic = ? AND `system` = 2', $id);

            if($QGroupBasicMapping->fetchRow($where)) 
                $QGroupBasicMapping->update($data_access_warehouse, $where);
            else {
                $data_access_warehouse['id_group_basic'] = $id;
                $QGroupBasicMapping->insert($data_access_warehouse);
            }
        }

        if($data_access_bi) {
            $where = $QGroupBasicMapping->getAdapter()->quoteInto('id_group_basic = ? AND `system` = 3', $id);

            if($QGroupBasicMapping->fetchRow($where)) 
                $QGroupBasicMapping->update($data_access_bi, $where);
            else {
                $data_access_bi['id_group_basic'] = $id;
                $QGroupBasicMapping->insert($data_access_bi);
            }
        }
        
        if($data_access_payment) {
            $where = $QGroupBasicMapping->getAdapter()->quoteInto('id_group_basic = ? AND `system` = 4', $id);

            if($QGroupBasicMapping->fetchRow($where)) 
                $QGroupBasicMapping->update($data_access_payment, $where);
            else {
                $data_access_payment['id_group_basic'] = $id;
                $QGroupBasicMapping->insert($data_access_payment);
            }
        }

        if($data_access_retail) {
            $where = $QGroupBasicMapping->getAdapter()->quoteInto('id_group_basic = ? AND `system` = 5', $id);

            if($QGroupBasicMapping->fetchRow($where)) 
                $QGroupBasicMapping->update($data_access_retail, $where);
            else {
                $data_access_retail['id_group_basic'] = $id;
                $QGroupBasicMapping->insert($data_access_retail);
            }
        }
    } else {
        $id_group_new = $QGroup->insert($data);

        if($data_access_center) {
            $data_access_center['id_group_basic'] = $id_group_new;
            $QGroupBasicMapping->insert($data_access_center); 
        }     

        if($data_access_warehouse) {
            $data_access_warehouse['id_group_basic'] = $id_group_new;
            $QGroupBasicMapping->insert($data_access_warehouse);
        }

        if($data_access_bi) {
            $data_access_bi['id_group_basic'] = $id_group_new;
            $QGroupBasicMapping->insert($data_access_bi);
        }
        
        if($data_access_payment) {
            $data_access_payment['id_group_basic'] = $id_group_new;
            $QGroupBasicMapping->insert($data_access_payment);
        }

        if($data_access_retail) {
            $data_access_retail['id_group_basic'] = $id_group_new;
            $QGroupBasicMapping->insert($data_access_retail);
        }
    }

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}
$this->_redirect(HOST.'permission-access/list-group-basic');