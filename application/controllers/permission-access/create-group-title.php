<?php

$id = $this->getRequest()->getParam('id');

if ($id) {
    $QTeam = new Application_Model_Team();
    $titleRowset = $QTeam->find($id);
    $title = $titleRowset->current();

    $QGroupLevel2 = new Application_Model_GroupLevel2();
    $group_contain = $QGroupLevel2->getGroupByTitleId($id);

    $id_str = "";

    foreach ($group_contain as $key => $value) {
        $id_str .= $value['gl2_id'] . ',';
    }

    $id_str = substr($id_str, 0, -1);

    $list_group = $QGroupLevel2->getGroupByName('', $id_str);


    $this->view->id_title = $id;
    $this->view->group_contain = $group_contain;
    $this->view->title = $title;
} else{
    $QGroupLevel2 = new Application_Model_GroupLevel2();
    $list_group = $QGroupLevel2->getAllGroup();
}

$this->view->list_group = $list_group;

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;