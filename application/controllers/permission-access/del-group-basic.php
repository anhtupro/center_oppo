<?php 

$id = $this->getRequest()->getParam('id');

$group = new Application_Model_GroupBasic();

$data = array(
    'del' => 1,
);

$where = $group->getAdapter()->quoteInto('id = ?', $id);

$group->update($data, $where);

$flashMessenger = $this->_helper->flashMessenger;
$flashMessenger->setNamespace('success')->addMessage('Delete Group Basic Success!');
$this->_redirect('/permission-access/list-group-basic');