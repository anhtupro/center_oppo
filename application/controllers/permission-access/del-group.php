<?php 

$id = $this->getRequest()->getParam('id');

$group = new Application_Model_GroupLevel2();

$data = array(
    'del' => 1,
);

$where = $group->getAdapter()->quoteInto('id = ?', $id);

$group->update($data, $where);

$flashMessenger = $this->_helper->flashMessenger;
$flashMessenger->setNamespace('success')->addMessage('Delete Group Success!');
$this->_redirect('/permission-access/list-group');