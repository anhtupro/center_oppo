<?php
$back_url = $this->getRequest()->getServer('HTTP_REFERER');

if ($this->getRequest()->getMethod() == 'POST'){
    $id = $this->getRequest()->getParam('id');
    $id_str = $this->getRequest()->getParam('pic');

    // if(!$id_str){

    //     $flashMessenger = $this->_helper->flashMessenger;
    //     $flashMessenger->setNamespace('error')->addMessage('Please select group basic!');

    //     $this->_redirect($back_url);
    // }
    
    $group_level2 = new Application_Model_GroupLevel2();
    $group_mapping_title_staff = new Application_Model_GroupMappingTitleStaff();

    $arr_id = explode(',',$id_str);

    if (!empty($id)){

        $where = $group_mapping_title_staff->getAdapter()->quoteInto('title = ?', $id);
        $group_mapping_title_staff->delete($where);

        foreach ($arr_id as $key => $value) {
            $data_mapping = array(
                'title' => $id,
                'group_level2' => $value,
                'type' => '2',
            );

            $group_mapping_title_staff->insert($data_mapping);
        } 
    }

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}


$this->_redirect(HOST.'staff/job-title');