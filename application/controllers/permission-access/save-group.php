<?php

if ($this->getRequest()->getMethod() == 'POST'){
    $id = $this->getRequest()->getParam('id');
    $id_str = $this->getRequest()->getParam('pic');
    $group_name = $this->getRequest()->getParam('group_name');

    if(!$id_str){
        $back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $flashMessenger = $this->_helper->flashMessenger;
        $flashMessenger->setNamespace('error')->addMessage('Please select group basic!');

        $this->_redirect($back_url);
    }
    
    $group_level2 = new Application_Model_GroupLevel2();
    $group_mapping = new Application_Model_GroupMapping();

    $arr_id = explode(',',$id_str);

    $data_grouplv2 = array(
        'name' => $group_name,
    );

    if (!empty($id)){

        if($group_name) {
            $where = $group_level2->getAdapter()->quoteInto('id = ?', $id);
            $group_level2->update($data_grouplv2, $where);
        }

        $where = $group_mapping->getAdapter()->quoteInto('group_level2 = ?', $id);
        $group_mapping->delete($where);

        foreach ($arr_id as $key => $value) {
            $data_mapping = array(
                'group_level2' => $id,
                'group_basic' => $value,
            );

            $group_mapping->insert($data_mapping);
        } 
    } else {
        $id_group_level2_new = $group_level2->insert($data_grouplv2);

        foreach ($arr_id as $key => $value) {
            $data_mapping = array(
                'group_level2' => $id_group_level2_new,
                'group_basic' => $value,
            );
            $group_mapping->insert($data_mapping);
        }
    }

    $flashMessenger = $this->_helper->flashMessenger;
    $flashMessenger->setNamespace('success')->addMessage('Done!');
}

$this->_redirect(HOST.'permission-access/list-group');