<?php
    $page               = $this->getRequest()->getParam('page', 1);
    $sname              = $this->getRequest()->getParam('sname', 0);
    $name               = $this->getRequest()->getParam('name_group', '');
    $id_str             = $this->getRequest()->getParam('id_str');
    $sname_width        = $this->getRequest()->getParam('span', 'col-12');
    $name_group         = $this->getRequest()->getParam('name');
    $export             = $this->getRequest()->getParam('export');
    $limit = LIMITATION;
    $total = 0;

    $params = array();
    $params['name'] = $name_group;

    $QGroupLevel2 = new Application_Model_GroupLevel2();
    $groups = $QGroupLevel2->fetchPagination($page, $limit, $total, $params);

    $this->view->groups = $groups;

    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->url = HOST.'permission-access/list-group'.( $params ? '?'.http_build_query($params).'&' : '?' );
    $this->view->offset = $limit*($page-1);

    $flashMessenger = $this->_helper->flashMessenger;
    $messages = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;

    if (!$sname)
        $limit = LIMITATION;
    else
        $limit = null;

    if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
    
        if ($sname) {
            $list_group = $QGroupLevel2->getGroupByName($name, $id_str);

            $this->view->list_group = $list_group;

            $this->view->col    = $sname_width;
            $this->_helper->viewRenderer->setRender('partials/search-group');
        }
    }

    if($export==1){
        $data = $QGroupLevel2->getAllGroupAndStaffTitle();
		// no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
		
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'Group ID',
            'Group Name',
            'Staff ID',
            'Staff Code',
            'Staff Full Name',
            'Title ID',
            'Title Name',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $intCount = 1;       

      
        try
        {
            if ($data)
                foreach ($data as $_key => $group)
                {
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($group['group_id'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($group['group_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($group['staff_id'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($group['staff_code'],
                        PHPExcel_Cell_DataType::TYPE_STRING);   
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($group['staff_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($group['title_id'],
                        PHPExcel_Cell_DataType::TYPE_STRING);   
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($group['title_name'],
                        PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
                }
        }
        catch (exception $e)
        {
            exit;
        }

        $filename = 'Group_list_' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
?>