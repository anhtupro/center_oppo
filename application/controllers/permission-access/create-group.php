<?php

$id = $this->getRequest()->getParam('id');

if ($id) {
    $QGroupLevel2 = new Application_Model_GroupLevel2();
    $groupRowset = $QGroupLevel2->find($id);
    $group = $groupRowset->current();

    $group_basic_contain = $QGroupLevel2->getGroupAndGroupBasic($id);

    $id_str = "";

    foreach ($group_basic_contain as $key => $value) {
        $id_str .= $value['id'] . ',';
    }

    $id_str = substr($id_str, 0, -1);

    $QGroup = new Application_Model_GroupBasic();
    $list_group_basic = $QGroup->getGroupBasicByName('', $id_str);


    $this->view->id_group_lv2 = $id;
    $this->view->group_basic_contain = $group_basic_contain;
    $this->view->group = $group;
} else{
    $QGroup = new Application_Model_GroupBasic();
    $list_group_basic = $QGroup->getAllGroupBasic();
}

$this->view->list_group_basic = $list_group_basic;

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;