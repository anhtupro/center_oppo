<?php 

$id = $this->getRequest()->getParam('id');

$group_menus = null;

if ($id) {
    $QGroup = new Application_Model_GroupBasic();
    $groupRowset = $QGroup->find($id);
    $group = $groupRowset->current();

    $group_menus = $group->menu ? explode(',', $group->menu) : null;

    $QGroupBasicMapping = new Application_Model_GroupBasicMapping();
    $where = $QGroupBasicMapping->getAdapter()->quoteInto('id_group_basic = ?', $id);
    $group_basic_mapping = $QGroupBasicMapping->fetchAll($where);
    
    foreach ($group_basic_mapping->toArray() as $value) {
        if($value['system'] == 1)
            $this->view->group_accesses = json_decode($value['access']);

        if($value['system'] == 2)
            $this->view->group_accesses_wh = json_decode($value['access']);

        if($value['system'] == 3)
            $this->view->group_accesses_bi = json_decode($value['access']);

        if($value['system'] == 4)
            $this->view->group_accesses_payment = json_decode($value['access']);

        if($value['system'] == 5)
            $this->view->group_accesses_retail = json_decode($value['access']);
    }

    $this->view->group = $group;
}

//get all controller and action
$front = $this->getFrontController();
$acl = array();

foreach ($front->getControllerDirectory() as $module => $path) {

    foreach (scandir($path) as $file) {

        if (strstr($file, "Controller.php") !== false) {

            include_once $path . DIRECTORY_SEPARATOR . $file;

            foreach (get_declared_classes() as $class) {

                if (is_subclass_of($class, 'Zend_Controller_Action')) {

                    $controller = lcfirst(substr($class, 0, strpos($class, "Controller")));
                    $tem = '';

                    for ($i=0; $i<strlen($controller); $i++){
                        $char = $controller[$i];
                        if (ord($char)<97)
                            $tem .= '-'.chr(ord($char)+32);
                        else
                            $tem .= $char;
                    }
                    $controller = $tem;

                    $actions = array();

                    foreach (get_class_methods($class) as $action) {

                        if (strstr($action, "Action") !== false) {
                            $action = substr($action, 0, strpos($action, "Action"));
                            $tem = '';

                            for ($i=0; $i<strlen($action); $i++){
                                $char = $action[$i];
                                if (ord($char)<97)
                                    $tem .= '-'.chr(ord($char)+32);
                                else
                                    $tem .= $char;
                            }
                            $actions[] = $tem;
                        }
                    }
                }
            }

            $acl[$module][$controller] = $actions;
        }
    }
}
//set current method
$actions = array();
foreach (get_class_methods($this) as $action){
    if (strstr($action, "Action") !== false) {
        $action = substr($action, 0, strpos($action, "Action"));
        $tem = '';

        for ($i=0; $i<strlen($action); $i++){
            $char = $action[$i];
            if (ord($char)<97)
                $tem .= '-'.chr(ord($char)+32);
            else
                $tem .= $char;
        }
        $actions[] = $tem;
    }
}

$acl[$this->getRequest()->getModuleName()][$this->getRequest()->getControllerName()] = $actions;

$this->view->acl = $acl;

$WarehouseService = new My_Einvoice_Warehouse_API();
$acl_warehouse = $WarehouseService->getControllerActionWarehouse();

$this->view->acl_wh = $acl_warehouse['data'];

$BiService = new My_Einvoice_Bi_API();
$acl_bi = $BiService->getControllerActionBi();
$this->view->acl_bi = $acl_bi['data'];

//payment
$PaymentService = new My_Einvoice_Payment_API();
$acl_payment = $PaymentService->getControllerActionPayment();
$this->view->acl_payment = $acl_payment['data'];
//payment

//Retail
$RetailService = new My_Einvoice_Retail_API();
$acl_retail = $RetailService->getControllerActionRetail();
$this->view->acl_retail = $acl_retail['data'];

$QMenu = new Application_Model_Menu();
//$where = $QMenu->getAdapter()->quoteInto('group_id = 1 OR group_id = 2');
$menus = $QMenu->fetchAll(null, array('position'));
foreach ($menus as $menu)
    $this->add_row($menu->id, $menu->parent_id, $menu->title);

$this->view->menus = $this->generate_list($group_menus);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;