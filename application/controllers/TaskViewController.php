<?php
/**
 * @author buu.pham
 * xem task progress
 */
class TaskViewController extends My_Controller_Action
{
    public function indexAction()
    {
        $this->_helper->layout->disableLayout();

        $QTask = new Application_Model_TaskProgress();
        $this->view->tasks = $QTask->fetchAll();
    }
}
