<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger       = $this->_helper->flashMessenger;
try {
        
    $QLockUnlockUploadBankArea = new Application_Model_LockUnlockUploadBankArea();
    $where = $QLockUnlockUploadBankArea->getAdapter()->quoteInto('id = ?', 1);
    $result = $QLockUnlockUploadBankArea->fetchRow($where);
$flag = 3;
    if($result['status'] == 0){
        $data = [
            'status' => 1
        ];
        $QLockUnlockUploadBankArea->update($data,$where);
        $flag = 1;
    }

    if($result['status'] == 1){
       $data = [
            'status' => 0
        ];
        $QLockUnlockUploadBankArea->update($data,$where);
        $flag = 0;
    }
    if($flag == 0){
         $flashMessenger->setNamespace('success')->addMessage('Unlock Success!');
    }elseif($flag == 1){
        $flashMessenger->setNamespace('success')->addMessage('Lock Success!');
    }else{
        $flashMessenger->setNamespace('error')->addMessage('Error !');
    }
   
    $this->_redirect(HOST . 'staff/view-bank-area');

} catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
        $this->_redirect(HOST . 'staff/view-bank-area');
}
