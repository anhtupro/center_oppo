<?php
echo '<pre>';
print_r('Chức năng đã bị xoá !');
echo '</pre>';
exit();
ini_set('allow_url_fopen', true);
ini_set('allow_url_include', true);
ini_set('safe_mode', false);
ini_set('max_execution_time', 0);

My_ExportCsv::_exportExel($params);

        $sort            = $this->getRequest()->getParam('sort', '');
        $desc            = $this->getRequest()->getParam('desc', 1);

        $page            = $this->getRequest()->getParam('page', 1);
        $name            = $this->getRequest()->getParam('name');
        $department      = $this->getRequest()->getParam('department');
        $off             = $this->getRequest()->getParam('off',1);
        $team            = $this->getRequest()->getParam('team');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        
        $note            = $this->getRequest()->getParam('note');
        $sname           = $this->getRequest()->getParam('sname', 0);
        $sname_width     = $this->getRequest()->getParam('span','span6');
        $code            = $this->getRequest()->getParam('code');
		$list_staff_code = $this->getRequest()->getParam('list_staff_code');
		$empty_photo = $this->getRequest()->getParam('empty-photo');
		$empty_id_photo = $this->getRequest()->getParam('empty_id_photo');
		$empty_id_photo_back = $this->getRequest()->getParam('empty_id_photo_back');
		
        if(!empty($list_staff_code)){
			$list_staff_code = str_replace(" ","", $list_staff_code);
            $list_staff_code = explode(",", $list_staff_code);
        }
        
        $s_assign        = $this->getRequest()->getParam('s_assign');
        $ood             = $this->getRequest()->getParam('ood');
        $email           = $this->getRequest()->getParam('email');
        $is_officer      = $this->getRequest()->getParam('is_officer');
        $date            = $this->getRequest()->getParam('date');
        $month           = $this->getRequest()->getParam('month');
        $year            = $this->getRequest()->getParam('year');
        $tags            = $this->getRequest()->getParam('tags');

   
        $company_id      = $this->getRequest()->getParam('company_id');
        $need_approve    = $this->getRequest()->getParam('need_approve');
        $off_date        = $this->getRequest()->getParam('off_date');
        $indentity       = $this->getRequest()->getParam('indentity');
        $joined_at       = $this->getRequest()->getParam('joined_at');
        $off_type        = $this->getRequest()->getParam('off_type');
        $is_print        = $this->getRequest()->getParam('is_print');
        $is_not_print    = $this->getRequest()->getParam('is_not_print');
        $is_official     = $this->getRequest()->getParam('is_official');
        $status          = $this->getRequest()->getParam('status', 0);
        $area_id         = $this->getRequest()->getParam('area_id');
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->area_visible = true;
        $title           = $this->getRequest()->getParam('title');

        //check if export
        $export = $this->getRequest()->getParam('export', 0);
        $download = $this->getRequest()->getParam('download', 0);
        $download_pvi = $this->getRequest()->getParam('download_pvi', 0);
        $download_scores = $this->getRequest()->getParam('download_scores', 0);
        $checkphoto = $this->getRequest()->getParam('checkphoto', 0);
       
        if (!$sname)
            $limit = LIMITATION;
        else
            $limit = null;

        $total = 0;

        $params = array_filter(array(
            'name' => $name,
            'department' => $department,
            'off' => $off,
            'team' => $team,
            'regional_market' => $regional_market,
            'district' => $district,
            'area_id' => $area_id,
            'note' => $note,
            'code' => $code,
			'list_staff_code' => $list_staff_code,
            's_assign' => $s_assign,
            'ood' => $ood,
            'email' => $email,
            's_assign' => $s_assign,
            'ood' => $ood,
            'email' => $email,
            'date' => $date,
            'month' => $month,
            'year' => $year,
            'is_officer' => $is_officer,
            'sname' => $sname,
            'tags' => $tags,
            'title' => $title,
            'company_id' => $company_id,
            'export' => $export,
            'need_approve' => $need_approve,
            'indentity' => $indentity,
            'joined_at' => $joined_at,
            'off_date'  => $off_date,
            'off_type'  => $off_type,
            'is_print'  => $is_print,
            'is_not_print' => $is_not_print,
            'is_official' => $is_official,
            'status' => $status,
            'empty_id_photo' => $empty_id_photo,
            'empty_id_photo_back' => $empty_id_photo_back
            
        ));
        $QRegionalMarket = new Application_Model_RegionalMarket();
        // GROUP TRAINING +	GROUP TRAINER LEADER
        if( in_array($userStorage->group_id, array(17,26))){
            $regional_market = $userStorage->regional_market;
            
            $where_regional = $QRegionalMarket->getAdapter()->quoteInto('id IN (?)', $regional_market);
            
            $row_regional = $QRegionalMarket->fetchRow($where_regional,'area_id');
            $params['area_id'] = $row_regional->toArray()['area_id'];
        }
        
        if($status != '')
        {
            $params['status'] = $status;
        }
        
        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;

       

        if ($area_id)
        {
            if (is_array($area_id) && count($area_id))
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

            $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
        }

        if ($regional_market)
        {
            if (is_array($regional_market) && count($regional_market))
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

            $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        /**
         * @author buu.pham
         * lọc staff thuộc khu vực nếu các title sau xem
         */
        if (in_array(
            $userStorage->title,
            array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM , SALE_SALE_ASM_STANDBY, 308, SALES_LEADER_TITLE)
        ) OR $userStorage->group_id == SALES_ADMIN_ID) {
            $QAsm = new Application_Model_Asm();
            $cachedASM = $QAsm->get_cache($userStorage->id);
            $tem = $cachedASM['province'];
            if ($tem)
                $list_province_ids = $tem;
            else
                $list_province_ids = -1;
            // doi mau report cho asm
            $export = $export ?  2 : '';

            $params['list_province_ids'] = $list_province_ids;
        }
        // end
        
        if( !in_array($userStorage->group_id, array(1,7))){
            if(!empty($department) && in_array(321, $department)){
                $params['department'] = array($userStorage->department,321);
        
            }else{
                $params['department'] = array($userStorage->department);
                //tien.vo
                if(in_array($userStorage->id, array(910,150))){
                    $params['team'] = array($userStorage->team);
                }
            }
             
        }
        // end

        $QStaff = new Application_Model_Staff();

        if(isset($export) and $export)
        {
            switch($export)
            {
                case 1 :
                    {
                        $staffs = $QStaff->fetchPaginationPhoto($page, null, $total, $params);
                        $this->_exportXlsxASM($staffs);
                    }
            }
        }
        if(isset($download) && $download)
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if($userStorage->title == ADMINISTRATOR_ID || $userStorage->title == SUPERADMIN_ID || $userStorage->title == 2087)
            {
                $zipname = "all_image-asm";
            }
            else
            {
                $this->view->area_visible = false;
                $QAsm = new Application_Model_Asm();
                $asm_cache = $QAsm->get_cache();
                $asm_cache[ $userStorage->id ]['area'];

                $area_id = $asm_cache[ $userStorage->id ]['area'];

                $QArea = new Application_Model_Area(); // Lấy danh sách khu vực để xử lý tên khu vực cho file zip
                $area_name = $QArea->get_cache()[$area_id];
                if(empty($area_name))
                {
                    $zipname = "all_image";
                }
                else
                {
                    $zipname = "all_image-" . $area_name;
                }
                
            }

            $params['only_have_photo'] = 1;
            $staffs_photo = $QStaff->fetchPaginationPhoto(0, 0, $total, $params);
//            echo "<pre>";
//            print_r($staffs_photo);
//            echo "</pre>";
//            exit();
            $zipfullname = APPLICATION_PATH . "/.." .'/public/photozip/' . $zipname .'.zip';
            $link_download = '/photozip/' . $zipname .'.zip';
            $zip = new ZipArchive();
            if(file_exists($zipfullname))
            {
                unlink($zipfullname);
            }
            $zip->open($zipfullname, ZipArchive::CREATE);
            foreach ($staffs_photo as $key => $staff_photo) {
                $file_ext = explode('.', $staff_photo['id_photo'])[1];
               
                $staff_team = $recursiveDeparmentTeamTitle[$staff_photo['department']]['children'][$staff_photo['team']]['name'];
                $file_name = $staff_photo['area_name']. '_' . $staff_team .'_' . $staff_photo['code'] . '_' . $staff_photo['firstname'] . ' ' . $staff_photo['lastname']  . '.' . $file_ext;
//             echo APPLICATION_PATH . '/..' . '/public/photo/staff/' . $staff_photo['id'] . '/ID_Front/' . $staff_photo['id_photo'];
//             exit();
                if(file_exists(APPLICATION_PATH . '/..' . '/public/photo/staff/' . $staff_photo['id'] . '/ID_Front/' . $staff_photo['id_photo']) && !empty($staff_photo['id_photo']))
                {
                    $zip->addFile(APPLICATION_PATH . '/..' . '/public/photo/staff/' . $staff_photo['id'] . '/ID_Front/' . $staff_photo['id_photo'], My_StringFormat::convert_vi_to_en($file_name ));
                }else{
                    
                }
            }
            $zip->close();
             $this->_redirect("/photozip/" . $zipname . ".zip");
        }
        
        $staffs = $QStaff->fetchPaginationPhoto($page, $limit, $total, $params);
     
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $QCompany = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $this->view->off_type = unserialize(OFF_TYPE);
        $this->view->userStorage = $userStorage;
        $this->view->desc = $desc;
        $this->view->sort = $sort;
        
        
        $this->view->staffs = $staffs;
        
        $this->view->params = $params;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'staff/list-cmnd' . ($params ? '?' . http_build_query($params) .
            '&' : '?');

        $this->view->offset = $limit * ($page - 1);

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages;

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

        if ($this->getRequest()->isXmlHttpRequest())
        {
            $this->_helper->layout->disableLayout();

            if ($sname)
            {
                $QRegion = new Application_Model_RegionalMarket();
                $regional_markets = $QRegion->fetchAll();

                $rm = array();
                foreach ($regional_markets as $key => $value)
                {
                    $rm[$value['id']] = $value['name'];
                }
                $this->view->rm_list = $rm;
                $this->view->span = $sname_width;
                $this->_helper->viewRenderer->setRender('partials/searchname');
            } elseif ($s_assign)
            {
                $this->_helper->viewRenderer->setRender('partials/staff');
            } else
                $this->_helper->viewRenderer->setRender('partials/list');
        }

?>