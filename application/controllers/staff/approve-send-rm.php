<?php

$id = intval($this->getRequest()->getParam('id'));
$approve = intval($this->getRequest()->getParam('approve'));
$QSendMailRm = new Application_Model_SendMailRm();
$QStaff = new Application_Model_Staff();
$db = Zend_Registry::get('db');
if (empty($id)) {
    $this->_redirect(HOST);
}

$where_rm = [];
$where_rm[] = $QSendMailRm->getAdapter()->quoteInto('id = ?', $id);
$data_rm = $QSendMailRm->fetchRow($where_rm);
if (empty($data_rm) || $data_rm['send_rm'] != 0) {
    $this->_redirect(HOST);
}
$where_staff = [];
$where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $data_rm->staff_id);
$data_staff = $QStaff->fetchRow($where_staff);
$check = 0;
if ($this->getRequest()->getMethod() == 'POST') {
    try {
        $db->beginTransaction();
        if ($approve == -1) {
            $QSendMailRm->update(['send_rm' => -1], $where_rm);
            $check = 1;
        } else {
            $QSendMailRm->update(['send_rm' => 1], $where_rm);
            $fullname = $data_staff->firstname . ' ' . $data_staff->lastname;
            $this->sendMailDocRm($fullname, $data_staff->email);
            $check = 1;
        }
        $db->commit();
    } catch (Exception $e) {
        $db->rollBack();
    }
}
$this->view->check = $check;
$this->view->full_name = $data_staff->firstname . ' ' . $data_staff->lastname;
$this->view->code = $data_staff->code;
