<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

// if($userStorage->id!=2287){
// 	echo 'Chức năng đang bảo trì! Quay lại sau nhé.'; exit;
// }
$QUploadBankAreaTemp = new Application_Model_UploadBankAreaTemp();
$QUploadPtiAreaTemp = new Application_Model_UploadPtiAreaTemp();
$QAsm       = new Application_Model_Asm();
$QStaff       = new Application_Model_Staff();
$staff      = $QStaff->find($userStorage->id);
$staff_current = $staff->current();
$QLockUnlockUploadBankArea = new Application_Model_LockUnlockUploadBankArea();
$where = $QLockUnlockUploadBankArea->getAdapter()->quoteInto('id = ?', 1);
$lock = $QLockUnlockUploadBankArea->fetchRow($where);
$this->view->lock = $lock;

$QArea       = new Application_Model_Area();
$asm     = $QAsm->get_cache($userStorage->id);
$area_id = $asm['area'];
$storage['area_id']  = $area_id;
$current_date = date('Y-m-d');
if($current_date > date('Y-m-10')){
	$from = date('Y-m-01');
	$to = date('Y-m-t');
}else{
	$from = date("Y-m-01", strtotime("-1 month"));
	$to = date('Y-m-t');
}
$params = [
	'from'		=> $from,
	'to'		=> $to,
	'list_area' => $storage['area_id'],
	'team'		=> $staff_current['team']
];

$area_list = $QArea->getAreaList($params);
$this->view->list_area = $area_list;

$resultList = $QUploadBankAreaTemp->getInfo($params);
$this->view->list= $resultList;

$resultListPti = $QUploadPtiAreaTemp->getInfo($params);
$this->view->list_pti= $resultListPti;

	

