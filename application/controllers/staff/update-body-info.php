<?php

$id                       = $this->getRequest()->getParam('id');
$height                   = $this->getRequest()->getParam('height');
$weight                   = $this->getRequest()->getParam('weight');

$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger     = $this->_helper->flashMessenger;


$QStaff                 = new Application_Model_Staff();


$staff = $QStaff->getStaffInfo($id);

if(!$staff){
    echo "Staff IS NOT exit";exit;
}

if($this->getRequest()->getMethod()=='POST')
{
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        
        $where = [];
        $where[] = $QStaff->getAdapter()->quoteInto('id = ?',$id);
        $where[] = $QStaff->getAdapter()->quoteInto('off_date IS NULL', NULL);

        $data = [
            'height' => $height,
            'weight' => $weight,
        ];
        
        $QStaff->update($data, $where);
        
        
        //Upload hình ảnh
        
        if(!empty($_FILES['image_body']['name'])){
            $ext_arr = array('jpg','png','JPG','PNG','jpeg','JPEG');
            
            $old_name = $_FILES['image_body']['name'];
            $tExplode = explode('.', $old_name);
            $extension = end($tExplode);
            $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
            
            
            if(($_FILES['image_body']['size'] > 3000000)){
                $flashMessenger->setNamespace('error')->addMessage("Kích thước vượt quá 3Mb");
                $this->_redirect('/staff/update-body-info?id='.$id);
                exit;
            }
            if(!in_array($extension, $ext_arr)){
                $flashMessenger->setNamespace('error')->addMessage("Hình sai định dạng png, jpg, jpeg");
                $this->_redirect('/staff/update-body-info?id='.$id);
                exit;
            }
            
            
            $file = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                    DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR . 'IMAGE_body';
            
            if (!is_dir($file))
                    @mkdir($file, 0777, true);
            
            $newFilePath = $file.DIRECTORY_SEPARATOR.$new_name;
            
            if (move_uploaded_file($_FILES['image_body']['tmp_name'], $newFilePath)) {
                $QStaff->update(['image_body' => $new_name], $where);
            }
            else{
                $flashMessenger->setNamespace('error')->addMessage("Upload hình ảnh ko thành công");
                $this->_redirect('/staff/update-body-info?id='.$id);
                exit;
            }

        }
        //END
        
        
        $db->commit();
        $flashMessenger ->setNamespace('success')->addMessage('Hoàn thành!');
    } catch (Exception $e) {
        $db->rollback();
        $error = $e->getMessage();
        
        $flashMessenger ->setNamespace('error')->addMessage($error);
    }
    $this->_redirect('/staff/update-body-info?id='.$id);
}


$this->view->staff = $staff;

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;








