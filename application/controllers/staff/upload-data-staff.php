<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$submit = $this->getRequest()->getParam('submit');
$upload = $this->getRequest()->getParam('upload', 0);
$flashMessenger = $this->_helper->flashMessenger;
$QStaff = new Application_Model_Staff();
$QSPti = new Application_Model_Pti();
$QStaffMyBank = new Application_Model_StaffMyBank();
$QDependentPersonStaff = new Application_Model_DependentPersonStaff();

if ($upload == 1) {

    // config for template
    define('START_ROW', 2);
    define('staff_code', 1);
    define('fullname', 2);
    define('bank_number', 4);
    define('at_bank', 5);
    define('branch_pgd', 6);
    define('province_city', 7);

    $datetime = date('Y-m-d H:i:s');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    if ($this->getRequest()->getMethod() == 'POST') { // Big IF
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $flashMessenger = $this->_helper->flashMessenger;
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $progress = new My_File_Progress('parent.set_progress');
        $progress->flush(0);

        $save_folder = 'tou';
        $new_file_path = '';
        $requirement = array(
            'Size' => array('min' => 5, 'max' => 5000000),
            'Count' => array('min' => 1, 'max' => 1),
            'Extension' => array('xls', 'xlsx'),
        );

        try {



            $file = My_File::get($save_folder, $requirement, true);

            if (!$file || !count($file))
                throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                    . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        } catch (Exception $e) {
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }

        //read file
        include 'PHPExcel/IOFactory.php';
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $_failed = false;
        $total_row = array();
        $dataMassUpload = array();
        $arr_code = array();
        try {
            $rowData = $sheet->rangeToArray('A' . (START_ROW - 1) . ':' . $highestColumn . (START_ROW - 1), NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            if ((isset($rowData[0]) && $rowData[0] != 'STT') || (isset($rowData[1]) && $rowData[1] != 'MNV') || (isset($rowData[2]) && $rowData[2] != 'NAME') || (isset($rowData[3]) && $rowData[3] != 'CMND') || (isset($rowData[4]) && $rowData[4] != 'NUMBER_ACCOUNT') || (isset($rowData[5]) && $rowData[5] != 'AT_BANK') || (isset($rowData[6]) && $rowData[6] != 'BRANCH') || (isset($rowData[7]) && $rowData[7] != 'PROVINCE') || (isset($rowData[8]) && $rowData[8] != 'Note') || $highestColumn != 'I') {
                throw new Exception("File invalid format");
            }
        } catch (Exception $e) {
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }
        for ($row = START_ROW; $row <= $highestRow; $row++) {

            try {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = isset($rowData[0]) ? $rowData[0] : array();
                $bank_ = '';

                if (!empty($rowData[staff_code])) {
                    $staff_id = $QStaff->getId_($rowData[staff_code]);
                    $data = array(
                        'staff_code' => trim($rowData[staff_code]),
                        'fullname' => trim($rowData[fullname]),
                        'bank_number' => trim($rowData[bank_number]),
                        'at_bank' => trim($rowData[at_bank]),
                        'branch_pgd' => trim($rowData[branch_pgd]),
                        'province_city' => trim($rowData[province_city]),
                        'staff_id' => $staff_id['id'],
                        'Note' => 'Upload'
                    );

                    $dataMassUpload[] = $data;
                    $arr_code[] = $rowData[staff_code];
                }
            } catch (Exception $e) {
                $progress->flush(100);
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
                exit;
            }
            // nothing here
        } // END loop through order rows

        $progress->flush(30);

        try {


            if (!empty($dataMassUpload)) {

                //xóa tất cả những staff code có trong file cũ 

                $where = $QStaffMyBank->getAdapter()->quoteInto('staff_code IN (?)', $arr_code);
                $resultDel = $QStaffMyBank->delete($where);
                $check_temp = My_Controller_Action::insertAllrow($dataMassUpload, 'staff_my_bank');
                if ($check_temp == false) {
                    throw new Exception('Upload dữ liệu bị lỗi.');
                }
            } else {
                throw new Exception('Danh sách thông tin rỗng.');
            }
            //$db->commit();
            $progress->flush(99);

            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-success">Success!</div>';
            exit;
        } catch (Exception $e) {
            //$db->rollback();
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }
    }
}

////////////////////////////////////DEPENDENT PERSON///////////////////////////////////////
if ($upload == 2) {

    // config for template
    define('START_ROW', 2);
    define('code', 1);
    define('name', 3);
    define('dob', 4);
    define('mst', 5);
    define('relative_id', 6);
    define('relative', 7);
    define('from_month', 8);
    define('to_month', 9);

    $datetime = date('Y-m-d H:i:s');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    if ($this->getRequest()->getMethod() == 'POST') { // Big IF
        $flashMessenger = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $progress = new My_File_Progress('parent.set_progress');
        $progress->flush(0);

        $save_folder = 'tou';
        $new_file_path = '';
        $requirement = array(
            'Size' => array('min' => 5, 'max' => 5000000),
            'Count' => array('min' => 1, 'max' => 1),
            'Extension' => array('xls', 'xlsx'),
        );

        try {



            $file = My_File::get($save_folder, $requirement, true);

            if (!$file || !count($file))
                throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                    . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        } catch (Exception $e) {
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }




        //read file
        include 'PHPExcel/IOFactory.php';
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $_failed = false;
        $total_row = array();
        $dataMassUpload = array();
        $arr_code = '';

        for ($row = START_ROW; $row <= $highestRow; $row++) {

            try {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = isset($rowData[0]) ? $rowData[0] : array();
                $bank_ = '';
                if (!empty($rowData[code])) {
                    if (!empty($rowData[dob])) {
                        $dob_temp = date_create_from_format("d/m/Y", $rowData[dob])->format("Y-m-d");
                    }
                    if (!empty($rowData[from_month])) {
                        $from_temp = date_create_from_format("d/m/Y", $rowData[from_month])->format("Y-m-d");
                    }
                    if (!empty($rowData[to_month])) {
                        $to_temp = date_create_from_format("d/m/Y", $rowData[to_month])->format("Y-m-d");
                    }



                    $staff_id = $QStaff->getId_($rowData[code]);
                    $data = array(
                        'code' => trim($rowData[code]),
                        'name' => trim($rowData[name]),
                        'dob' => !empty($dob_temp) ? $dob_temp : null,
                        'mst' => trim($rowData[mst]),
                        'relative_id' => trim($rowData[relative_id]),
                        'relative' => trim($rowData[relative]),
                        'from_month' => !empty($from_temp) ? $from_temp : null,
                        //'to_month'       => !empty($to_temp)?$to_temp:null,
                        'staff_id' => intval($staff_id['id'])
                    );

                    $dataMassUpload[] = $data;
                    $arr_code .= $rowData[code] . ',';
                }
            } catch (Exception $e) {
                $progress->flush(100);
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
                exit;
            }
            // nothing here
        } // END loop through order rows

        $progress->flush(30);

        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        try {


            if (!empty($dataMassUpload)) {
                My_Controller_Action::insertAllrow($dataMassUpload, 'dependent_person_staff');
            } else {
                throw new Exception('Danh sách thông tin rỗng.');
            }
            $db->commit();
            $progress->flush(99);

            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-success">Success!</div>';
            exit;
        } catch (Exception $e) {
            $progress->flush(100);
            $db->rollback();
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }
    }
}

//////////////////////////////// TAX ///////////////////////////////////
if ($upload == 3) {
    $QPti = new Application_Model_Pti();
    // config for template
    define('START_ROW', 2);
    define('code', 1);
    define('fullname', 2);
    define('Tax', 3);


    $datetime = date('Y-m-d H:i:s');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    if ($this->getRequest()->getMethod() == 'POST') { // Big IF
        $flashMessenger = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $progress = new My_File_Progress('parent.set_progress');
        $progress->flush(0);

        $save_folder = 'tou';
        $new_file_path = '';
        $requirement = array(
            'Size' => array('min' => 5, 'max' => 5000000),
            'Count' => array('min' => 1, 'max' => 1),
            'Extension' => array('xls', 'xlsx'),
        );

        try {



            $file = My_File::get($save_folder, $requirement, true);

            if (!$file || !count($file))
                throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                    . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        } catch (Exception $e) {
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }




        //read file
        include 'PHPExcel/IOFactory.php';
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $_failed = false;
        $total_row = array();
        $dataMassUpload = array();
        $arr_code = [];

        for ($row = START_ROW; $row <= $highestRow; $row++) {

            try {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = isset($rowData[0]) ? $rowData[0] : array();
                $bank_ = '';

                if (!empty($rowData[code])) {
                    $code_staff = trim($rowData[code]);
                    $staff_id = $QStaff->getId_($code_staff);
                    $data = array(
                        'code' => trim($rowData[code]),
                        'fullname' => trim($rowData[fullname]),
                        'Tax' => trim($rowData[Tax]),
                        'staff_id' => $staff_id['id']
                    );

                    $dataMassUpload[] = $data;
                    $arr_code[] = $code_staff;
                }
            } catch (Exception $e) {
                $progress->flush(100);
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
                exit;
            }
            // nothing here
        } // END loop through order rows

        $progress->flush(30);

//        $db = Zend_Registry::get('db');
//        $db->beginTransaction();


        try {


            if (!empty($dataMassUpload)) {
                
                //xóa tất cả những staff code có trong file cũ 
                $where = $QPti->getAdapter()->quoteInto('code IN (?)', $arr_code);
                $resultDel = $QPti->delete($where);
                
                $check_temp = My_Controller_Action::insertAllrow($dataMassUpload, 'pti');
                if ($check_temp == false) {
                    throw new Exception('Upload dữ liệu bị lỗi.');
                }
            } else {
                throw new Exception('Danh sách thông tin rỗng.');
            }
//            $db->commit();
            $progress->flush(99);

            $progress->flush(100);
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-success">Success!</div>';
            exit;
        } catch (Exception $e) {
            $progress->flush(100);
//            $db->rollback();
            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
            echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
            echo '<div class="alert alert-error">Failed - ' . $e->getMessage() . ' </div>';
            exit;
        }
    }
}