<?php
$staff_id = $this->getRequest()->getParam('staff_id');
$back_url = $this->getRequest()->getParam('back_url');

$QStaffUploadFile  = new Application_Model_StaffUploadFile();

if(!empty($staff_id)){
    $where    = $QStaffUploadFile->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    $list_file = $QStaffUploadFile->fetchAll($where);

    if (!empty($list_file)) {
        $this->view->list_file = $list_file->toArray();
    }
    
     $this->view->staff_id = $staff_id;
}