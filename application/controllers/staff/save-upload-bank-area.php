<?php 

$upload = $this->getRequest()->getParam('upload',0);
$area_id = $this->getRequest()->getParam('area_id',0);

$QUploadBankAreaTemp = new Application_Model_UploadBankAreaTemp();
$QUploadPtiAreaTemp = new Application_Model_UploadPtiAreaTemp();
echo '<link href="/css/bootstrap.min.css" rel="stylesheet">';
////////////////////////////////////// BANK /////////////////////////////////////////////////////////////////////////
if($upload == 1){

        $QLockUnlockUploadBankArea = new Application_Model_LockUnlockUploadBankArea();
        $where = $QLockUnlockUploadBankArea->getAdapter()->quoteInto('id = ?', 1);
        $lock = $QLockUnlockUploadBankArea->fetchRow($where);
        if($lock['status'] == 1){
            //throw new Exception("Hệ thống đang tạm khóa upload ! (Thử lại sau hoặc liên hệ phòng HR).");
                echo '<div class="alert alert-danger">Hệ thống tạm khóa upload, vui lòng liên hệ nhân viên phụ trách.</div>';
                exit;
        }


        // config for template
        define('START_ROW', 4);
        define('text_khuvuc', 1);
        define('staff_code', 2);
        define('full_name', 3);
        define('cmnd', 4);
        define('bank_number', 5);
        define('bank_name', 6);
        define('bank_brand', 7);
        define('bank_province', 8);
        define('note', 9);

        $datetime = date('Y-m-d H:i:s');
        $userStorage   = Zend_Auth::getInstance()->getStorage()->read();

        if ($this->getRequest()->getMethod() == 'POST') { // Big IF
            $flashMessenger       = $this->_helper->flashMessenger;
            set_time_limit(0);
            ini_set('memory_limit', -1);
            $progress = new My_File_Progress('parent.set_progress');
            $progress->flush(0);

            $save_folder   = 'tou';
            $new_file_path = '';
            $requirement   = array(
                'Size'      => array('min' => 5, 'max' => 5000000),
                'Count'     => array('min' => 1, 'max' => 1),
                'Extension' => array('xls', 'xlsx'),
            );
            
            try {

                

                $file = My_File::get($save_folder, $requirement, true);

                if (!$file || !count($file))
                    throw new Exception("Upload failed");

                    $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                    . DIRECTORY_SEPARATOR . $file['folder'];

                    $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

            } catch (Exception $e) {
                $this->view->errors = $e->getMessage();
                return;
            }




            //read file
            include 'PHPExcel/IOFactory.php';
            //  Read your Excel workbook
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            } catch (Exception $e) {    

                $this->view->errors = $e->getMessage();
                return;
            }

            //  Get worksheet dimensions
            $sheet           = $objPHPExcel->getSheet(0);
            $highestRow      = $sheet->getHighestRow();
            $highestColumn   = $sheet->getHighestColumn();
            $_failed         = false;
            $total_row       = array();
            $dataMassUpload  = array();
            for ($row = START_ROW; $row <= $highestRow; $row++) {

                try {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $rowData = isset($rowData[0]) ? $rowData[0] : array();
                    $bank_ = '';
                   	



                    if(!empty($rowData[staff_code])){
                        $data = array(
                            'area_id'               => intval($area_id),
                            'area_name'           => trim($rowData[text_khuvuc]),
                            'full_name'             => trim(mb_strtoupper($rowData[full_name])),
                            'cmnd'                  => trim($rowData[cmnd]),
                            'staff_code'            => trim($rowData[staff_code]),
                            'bank_number'           => str_replace(" ","",trim($rowData[bank_number])),
                            'bank_name'             => trim($rowData[bank_name]),
                            'bank_brand'            => (trim($rowData[bank_name]) == 'TECHCOMBANK')?'HỒ CHÍ MINH':trim($rowData[bank_brand]),
                            'bank_province'         =>  (trim($rowData[bank_name]) == 'TECHCOMBANK')?'HỒ CHÍ MINH':trim($rowData[bank_province]),
                            'brand_province'        =>   (trim($rowData[bank_name]) == 'TECHCOMBANK')?'HỒ CHÍ MINH':trim($rowData[bank_brand]).'_'.trim($rowData[bank_province]),
                            'note'                  => trim($rowData[note]),
                            'created_at'            => $datetime,
                            'created_by'            => $userStorage->id
                        );
                       
                        $dataMassUpload[] = $data;
                    }
                 
                } catch (Exception $e) {
                    $this->view->errors = $e->getMessage();
                    return;
                }
                // nothing here
            } // END loop through order rows
            $progress->flush(30);
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
             $this->_helper->layout->disableLayout();
             $this->_helper->viewRenderer->setNoRender();
            $province = array(
                'HỒ CHÍ MINH',
                'BẾN TRE',
                'ĐỒNG NAI',
                'BÌNH THUẬN',
                'CẦN THƠ',
                'TIỀN GIANG',
                'TÂY NINH',
                'BÌNH ĐỊNH',
                'QUẢNG BÌNH',
                'ĐÀ NẴNG',
                'ĐẮK LẮK',
                'KHÁNH HÒA',
                'HÀ NỘI',
                'BÀ RỊA - VŨNG TÀU',
                'NGHỆ AN',
                'KIÊN GIANG',
                'BÌNH PHƯỚC',
                'THANH HÓA',
                'HẢI PHÒNG',
                'HẬU GIANG',
                'QUẢNG NGÃI',
                'NAM ĐỊNH',
                'GIA LAI',
                'TRÀ VINH',
                'CÀ MAU',
                'AN GIANG',
                'PHÚ YÊN',
                'THỪA THIÊN - HUẾ',
                'THÁI BÌNH',
                'QUẢNG NINH',
                'HÀ NAM',
                'BẮC GIANG',
                'PHÚ THỌ',
                'HẢI DƯƠNG',
                'HƯNG YÊN',
                'YÊN BÁI',
                'VĨNH PHÚC',
                'HÀ TĨNH',
                'TUYÊN QUANG',
                'SÓC TRĂNG',
                'ĐỒNG THÁP',
                'KON TUM',
                'THÁI NGUYÊN',
                'BÌNH DƯƠNG',
                'QUẢNG NAM',
                'BẮC NINH',
                'NINH BÌNH',
                'QUẢNG TRỊ',
                'VĨNH LONG',
                'BẠC LIÊU',
                'NINH THUẬN',
                'LONG AN',
                'LÂM ĐỒNG',
                'ĐĂK NÔNG',
                'LÀO CAI',
                'HÒA BÌNH',
                'CAO BẰNG',
                'ĐIỆN BIÊN',
                'LẠNG SƠN',
                'SƠN LA',
                'HÀ GIANG',
                'BẮC KẠN',
                'LAI CHÂU'
            );
            $bank_name_array = array(
                'VDBANK',
                'CBANK',
                'OCEANBANK',
                'GPBANK',
                'AGRIBANK',
                'ACB',
                'TPBANK',
                'DONGA BANK',
                'SEABANK',
                'ABBANK',
                'BACABANK',
                'VIETCAPITALBANK',
                'MARITIME BANK',
                'MSB',
                'TECHCOMBANK',
                'KIENLONGBANK',
                'NAM A BANK',
                'NCBANK',
                'VPBANK',
                'HDBANK',
                'OCBBANK',
                'MILITARY BANK', 
                'MBB',
                'PVCOM BANK',
                'VIB BANK',
                'SCB',
                'SGB',
                'SHB',
                'SACOMBANK',
                'VIETABANK',
                'BAOVIETBANK',
                'VIETBANK',
                'PG BANK',
                'EXIMBANK',
                'LIENVIETPOSTBANK',
                'VIETCOMBANK',
                'VIETINBANK',
                'BIDV',
                'CITI BANK',
                'HSBC BANK',
                'STANDARD CHARTERED BANK',
                'SHINHAN BANK',
                'HONG LEONG BANK',
                'MILITARY BANK, MBB',
                'MBBANK'
                );
            try {
                if(empty($area_id) && !in_array($userStorage->id, [5983,4617,166])){
                    throw new Exception("Vui lòng chọn khu vực!");
                }
                
                if (!empty($dataMassUpload)) {
                        $array_err = [];
                            foreach ($dataMassUpload as $k => $upload){
                                $select = $db->select()
                                        ->from(array('p' => 'staff'), array('p.code','p.id'))
                                        ->where('p.code LIKE ?', $upload['staff_code']);
                                $result_code = $db->fetchCol($select);

                                if(empty($result_code)){
                                    $array_err[] = array(
                                        'staff_code' => $upload['staff_code'],
                                        'error' => 'Staff code không tồn tại !'
                                    );
                                   // continue;
                                      // throw new Exception('Staff code không tồn tại: '.$upload['staff_code'].' - '.$upload['full_name']); 
                                }
                                if($upload['bank_name'] == ""){
                                     $array_err[] = array(
                                        'staff_code' => $upload['staff_code'],
                                        'error' => 'Phải chọn tên ngân hàng !'
                                    );
                                    //continue;
                                     //throw new Exception('Phải chọn tên ngân hàng: '.$upload['staff_code'].' - '.$upload['full_name']); 
                                }

                                if(!in_array(strlen($upload['cmnd']),[9,12])){
                                    $array_err[] = array(
                                        'staff_code' => $upload['staff_code'],
                                        'error' => $upload['cmnd'].' Số CMND phải là 9 hoặc 12 số !'
                                    );
                                    //continue;
                                    //throw new Exception('Số CMND phải là 9 hoặc 12 số: '.$upload['staff_code'].' - '.$upload['full_name'].' - '.$upload['cmnd']);
                                }

                                $where = array();
                                $where[]    = $QUploadBankAreaTemp->getAdapter()->quoteInto('staff_code LIKE ?', $upload['staff_code']);  
                                $where[]    = $QUploadBankAreaTemp->getAdapter()->quoteInto('bank_number = ?', $upload['bank_number']); 
                                $checkRow = $QUploadBankAreaTemp->fetchRow($where); 

                                if(!empty($checkRow)){
                                     $array_err[] = array(
                                        'staff_code' => $upload['staff_code'],
                                        'error' => 'Thông tin số tài khoản trùng !'.$upload['full_name'].' -'.' BANK_NUMBER: '.$upload['bank_number']
                                    );
                                    //continue;
                                  // throw new Exception('Thông tin số tài khoản trùng. '.'STAFF: '.$upload['staff_code'].' - '.$upload['full_name'].' -'.' BANK_NUMBER: '.$upload['bank_number']);
                                }   
                                if(!in_array($upload['bank_name'], $bank_name_array)){
                                    $array_err[] = array(
                                        'staff_code' => $upload['staff_code'],
                                        'error' => 'Sai tên hoặc sai định dạng ngân hàng !'.$upload['bank_name']
                                    );
                                   //continue;
                                    //throw new Exception('Sai tên hoặc sai định dạng ngân hàng: '.$upload['bank_name'].' - của STAFF: '.$upload['staff_code'].' - '.$upload['full_name']); 
                                }   

                                if(empty($upload['bank_brand'])){
                                    $array_err[] = array(
                                        'staff_code' => $upload['staff_code'],
                                        'error' => 'Thông tin chi nhánh ngân hàng trống !'
                                    );
                                }

                                if(!in_array($upload['bank_province'], $province)){

                                    $array_err[] = array(
                                        'staff_code' => $upload['staff_code'],
                                        'error' => 'Sai tên hoặc sai định dạng tỉnh thành! '.$upload['bank_province']
                                    );
                                   // continue;
                                    //throw new Exception('Sai tên hoặc sai định dạng tỉnh thành: '.$upload['bank_province'].' - của STAFF: '.$upload['staff_code'].' - '.$upload['full_name']); 
                                }
                                
                            }
                           
                          if(!empty($array_err)){
                            foreach ($array_err as $key => $value) {
                               echo '<div class="alert alert-danger">'.$value['staff_code'].' - '.$value['error'].'</div>'; 
                            }
                            echo  '<script>window.parent.scroll(0,0);</script>';
                            exit;
                          }

                    My_Controller_Action::insertAllrow($dataMassUpload, 'upload_bank_area_temp');
                    
                } else {
                   throw new Exception('Danh sách thông tin rỗng.');
                }
                $db->commit();
                $progress->flush(99);
                
                $progress->flush(100);
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                    echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                    echo '<div class="alert alert-success">Success!</div>';
                    exit;    
            } catch (Exception $e) {
                $db->rollback();
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                    echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                    echo '<div class="alert alert-error">Failed - '.$e->getMessage().' </div>';
                    exit;
                
            }
        }
}

////////////////////////////////////// PTI //////////////////////////////////////////////////////////////////////

if($upload == 2){

        // config for template
        define('START_ROW', 2);
        define('staff_code', 1);
        define('tax', 2);
        define('full_name', 3);
        define('place', 4);
        define('cmnd', 5);

        $datetime = date('Y-m-d H:i:s');
        $userStorage   = Zend_Auth::getInstance()->getStorage()->read();

        if ($this->getRequest()->getMethod() == 'POST') { // Big IF
            $flashMessenger       = $this->_helper->flashMessenger;
            set_time_limit(0);
            ini_set('memory_limit', -1);
            $progress = new My_File_Progress('parent.set_progress');
            $progress->flush(0);

            $save_folder   = 'tou';
            $new_file_path = '';
            $requirement   = array(
                'Size'      => array('min' => 5, 'max' => 5000000),
                'Count'     => array('min' => 1, 'max' => 1),
                'Extension' => array('xls', 'xlsx'),
            );
            
            try {

                

                $file = My_File::get($save_folder, $requirement, true);

                if (!$file || !count($file))
                    throw new Exception("Upload failed");

                    $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                    . DIRECTORY_SEPARATOR . $file['folder'];

                    $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

            } catch (Exception $e) {
                $this->view->errors = $e->getMessage();
                return;
            }




            //read file
            include 'PHPExcel/IOFactory.php';
            //  Read your Excel workbook
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            } catch (Exception $e) {    

                $this->view->errors = $e->getMessage();
                return;
            }

            //  Get worksheet dimensions
            $sheet           = $objPHPExcel->getSheet(0);
            $highestRow      = $sheet->getHighestRow();
            $highestColumn   = $sheet->getHighestColumn();
            $_failed         = false;
            $total_row       = array();
            $dataMassUpload  = array();
            for ($row = START_ROW; $row <= $highestRow; $row++) {

                try {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $rowData = isset($rowData[0]) ? $rowData[0] : array();
                    $bank_ = '';
                   
                    if(!empty($rowData[staff_code])){
                        $data = array(
                            'area_id'               => intval($area_id),
                            'code'            => trim($rowData[staff_code]),
                            'tax'                  => trim($rowData[tax]),
                             'fullname'             => trim(mb_strtoupper($rowData[full_name])),
                            'place'           =>    trim($rowData[place]),
                            'cmnd'                  => trim($rowData[cmnd]),
                            'created_at'            => $datetime,
                            'created_by'            => $userStorage->id
                        );
                       
                        $dataMassUpload[] = $data;

                    }
                 
                } catch (Exception $e) {
                    $this->view->errors = $e->getMessage();
                    return;
                }
                // nothing here
            } // END loop through order rows
            $progress->flush(30);
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
             $this->_helper->layout->disableLayout();
             $this->_helper->viewRenderer->setNoRender();
           
           // var_dump($check_num_bank); exit;
            try {
                if(empty($area_id) && !in_array($userStorage->id, [5983,4617,166])){
                    throw new Exception("Vui lòng chọn khu vực!");
                }
                
                if (!empty($dataMassUpload)) {
                        $array_err = [];
                            foreach ($dataMassUpload as $k => $upload){

                                $select = $db->select()
                                        ->from(array('p' => 'staff'), array('p.code','p.id'))
                                        ->where('p.code LIKE ?', $upload['code']);
                                $result_code = $db->fetchCol($select);

                                if(empty($result_code)){
                                    $array_err[] = array(
                                        'code' => $upload['code'],
                                        'error' => 'Staff code không tồn tại !'
                                    );
                                  
                                }
                              

                                // if(!in_array(strlen($upload['cmnd']),[9,12])){
                                //     $array_err[] = array(
                                //         'staff_code' => $upload['staff_code'],
                                //         'error' => $upload['cmnd'].' Số CMND phải là 9 hoặc 12 số !'
                                //     );
                                    
                                // }

                                $where = array();
                                $where[]    = $QUploadPtiAreaTemp->getAdapter()->quoteInto('code LIKE ?', $upload['code']);  
                                $where[]    = $QUploadPtiAreaTemp->getAdapter()->quoteInto('tax = ?', $upload['tax']); 
                                $checkRow = $QUploadPtiAreaTemp->fetchRow($where); 

                                if(!empty($checkRow)){
                                     $array_err[] = array(
                                        'code' => $upload['code'],
                                        'error' => 'Thông tin mã số thuế trùng !'.$upload['fullname'].' -'.' TAX: '.$upload['tax']
                                    );
                                  
                                }   
                               
                                
                            }
                           
                          if(!empty($array_err)){
                            foreach ($array_err as $key => $value) {
                               echo '<div class="alert alert-danger">'.$value['code'].' - '.$value['error'].'</div>'; 
                            }
                            exit;
                          }
                    My_Controller_Action::insertAllrow($dataMassUpload, 'upload_pti_area_temp');
                    
                } else {
                   throw new Exception('Danh sách thông tin rỗng.');
                }
                $db->commit();
                $progress->flush(99);
                
                $progress->flush(100);
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                    echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                    echo '<div class="alert alert-success">Success!</div>';
                    exit;    
            } catch (Exception $e) {
                $db->rollback();
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                    echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                    echo '<div class="alert alert-error">Failed - '.$e->getMessage().' </div>';
                    exit;
                
            }
        }
}
