<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$QStaff = new Application_Model_Staff();
$db = Zend_Registry::get('db');

$logTime = $this->getRequest()->getParam('log_time');
$arrayTime = explode('-', $logTime);
$month = $arrayTime[0];
$year = $arrayTime[1];
//$minOffDate = $year . '-' . $month . '-01';
$minOffDate = date('Y-m-d',strtotime($year . '-' . $month  ));
$minOffDate = date_create($minOffDate);
$minOffDate = date_sub($minOffDate, date_interval_create_from_date_string('2 months'));
$minOffDate = date_format($minOffDate, 'Y-m-d');

$currentMonth = date('m');
$currentYear = date('Y');

if (! $logTime) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng nhập tháng!'
    ]);
    return;
}

$isAllowed = $QStaff->checkDistanceBetweenTwoMonth($month, $year, 2);

if ($isAllowed) {
    try {
        $db->beginTransaction();

        $tableName = 'staff_' . $month . '_' . $year;

        // create table
        $stmt = $db->prepare(
            "DROP TABLE IF EXISTS salary.$tableName; 
         CREATE TABLE salary.$tableName LIKE hr.staff;"
        );
        $stmt->execute();

        //insert into table
        $stmt = $db->prepare(
            "INSERT INTO salary.$tableName
         SELECT * FROM hr.staff as st
          WHERE (st.off_date IS NULL) OR (st.off_date >= '$minOffDate')"
        );
        $stmt->execute();

        $db->commit();

        echo json_encode([
            'status' => 0
        ]);
    } catch (\Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
    }
} else {
    echo json_encode([
        'status' => 1,
        'message' => 'Không được log quá 2 tháng so với tháng hiện tại'
    ]);
}


