<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    if($this->getRequest()->isXmlHttpRequest())
    {
        $array_href = $this->getRequest()->getPost('array_href');

        $files = $array_href;
        
        // xử lý tên file zip dựa vào chức danh
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if($userStorage->title == ADMINISTRATOR_ID || $userStorage->title == SUPERADMIN_ID || $userStorage->title == 2087)
        {
            $zipname = "image-asm";
        }
        else
        {
            $this->view->area_visible = false;
            $QAsm = new Application_Model_Asm();
            $asm_cache = $QAsm->get_cache();
            $asm_cache[ $userStorage->id ]['area'];

            $area_id = $asm_cache[ $userStorage->id ]['area'];

            $QArea = new Application_Model_Area(); // Lấy danh sách khu vực để xử lý tên khu vực cho file zip
            $area_name = $QArea->get_cache()[$area_id];
            if(empty($area_name))
            {
                $zipname = "image";
            }
            else
            {
                $zipname = "image-" . $area_name;
            }
            
        }
        
        $zipfullname = APPLICATION_PATH . "/.." .'/public/photozip/' . $zipname .'.zip';
        $link_download = '/photozip/' . $zipname .'.zip';
        $zip = new ZipArchive();
        if(file_exists($zipfullname))
        {
            unlink($zipfullname);
        }
        $zip->open($zipfullname, ZipArchive::CREATE);
        foreach ($files as $key => $file) {
            $array_file_name = explode("/", $file['href']);
            $file_ext = explode('.', $array_file_name[count($array_file_name)-1])[1];
            $file_name = "photo/" . $file['download'] . '.' . $file_ext;
            if($zip->addFile(APPLICATION_PATH . '/..' . '/public' . $file['href'], My_StringFormat::convert_vi_to_en($file_name )))
            {

            }
            else
            {
                // echo $file.'\n';
            }
        }
        $zip->close();
        // echo json_encode(array("status" => true, "link_down" => $zipfullname));
        echo json_encode(array("status" => true, "link_down" => $link_download));
    }   
    
    