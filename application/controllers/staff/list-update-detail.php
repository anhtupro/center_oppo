<?php
$staff_id = $this->getRequest()->getParam('staff_id');
$id = $this->getRequest()->getParam('id');

if(!is_numeric($staff_id)){
    $staff_id = '\\'.$staff_id;
}
if(!is_numeric($id)){
    $id = '\\'.$id;
}
$staff_id = My_Util::escape_string($staff_id);
$id = My_Util::escape_string($id);

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QStaff = new Application_Model_Staff();
$QStaffTempNew = new Application_Model_StaffTempNew();
$QProvince = new Application_Model_Province();
$QStaffAddress = new Application_Model_StaffAddress();
$QRegionalMarket = new Application_Model_RegionalMarket();
$QWard = new Application_Model_Ward();
$QStaffTempNewStatus = new Application_Model_StaffTempNewStatus();
$QReligion = new Application_Model_Religion();
$QNationality = new Application_Model_Nationality();


$StaffRowSet = $QStaff->find($staff_id);
$original_staff = $StaffRowSet->current();

$provinces = $QProvince->get_all2();
$district_cache = $QRegionalMarket->get_district_cache();

// get  user's ID Card Address (original information)
$where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $staff_id);
$where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::ID_Card);
$id_card_address = $QStaffAddress->fetchRow($where);

// get list original district and ward
if (isset($id_card_address['district']) && isset($district_cache[$id_card_address['district']])) {
    $this->view->id_card_address_districts = $QRegionalMarket->
    get_district_by_province_cache($district_cache[$id_card_address['district']]['parent']);

    //list ward due to district
    $list_id_card_ward = $QWard->getWardByDistrict($id_card_address['district']);
    $this->view->list_id_card_ward = $list_id_card_ward;

}

// get staff that need to be approved
$edited_staff = $QStaffTempNew->getStaff($id);

// get list edited district and ward (that need to be approved)
if (isset($edited_staff['id_card_district']) && isset($district_cache[$edited_staff['id_card_district']])) {
    $this->view->id_card_address_districts_edited = $QRegionalMarket->
    get_district_by_province_cache($district_cache[$edited_staff['id_card_district']]['parent']);

    //list edited ward due to district
    $list_id_card_ward_edited = $QWard->getWardByDistrict($edited_staff['id_card_district']);
    $this->view->list_id_card_ward_edited = $list_id_card_ward_edited;
}

// HR can reject
//if (in_array($userStorage = Zend_Auth::getInstance()->getStorage()->read()->title, [HR_SPECIALIST])) { //HR
//    $this->view->is_HR = true;
//}
$approve_type = $QStaffTempNewStatus->getApproveType($userStorage->title);

$ID_card_infor = $QStaff->getIDCardInfor($staff_id); // original infor

$this->view->ID_card_infor = $ID_card_infor;

$this->view->district_cache = $district_cache;
$this->view->id_card_address = $id_card_address;
$this->view->provinces = $provinces;
$this->view->original_staff = $original_staff;
$this->view->edited_staff = $edited_staff;
$this->view->all_province_cache = $QRegionalMarket->get_cache();
$this->view->all_district_cache = $QRegionalMarket->get_district_cache();
$this->view->staff_id = $staff_id;
$this->view->id = $id;
$this->view->userStorage = $userStorage;
//$this->view->can_reject = $can_reject;
$this->view->approve_type = $approve_type;
$this->view->religions = $QReligion->fetchAll();
$this->view->nationalities = $QNationality->fetchAll();

$flashMessenger = $this->_helper->flashMessenger;
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->view->messages_error = $messages_error;
