<?php
        $sort            = $this->getRequest()->getParam('sort', '');
        $desc            = $this->getRequest()->getParam('desc', 1);

        $page            = $this->getRequest()->getParam('page', 1);
        $name            = $this->getRequest()->getParam('name');
        $code            = $this->getRequest()->getParam('code');
        $area_id         = $this->getRequest()->getParam('area_id');
        $result          = $this->getRequest()->getParam('result');
        $is_off          = $this->getRequest()->getParam('is_off');
        $from_date       = $this->getRequest()->getParam('from_date');
        $to_date         = $this->getRequest()->getParam('to_date');


        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QArea = new Application_Model_Area();
        $QAsm = new Application_Model_Asm();
        $areas = $QArea->get_cache();
        $this->view->area = $areas;
        
        //check if export
        $export = $this->getRequest()->getParam('export', 0);

        $limit = LIMITATION;
        $total = 0;

        $params = array_filter(array(
            'name'      => $name,
            'code'      => trim($code),
            'area_id'   => $area_id,
            'result'    => $result,
            'is_off'    => $is_off,
            'from_date' => $from_date,
            'to_date'   => $to_date,
            'export'    => $export

         ));

        $cb = CB_SPECIALIST;
        if($userStorage->title != $cb){
            $area = $QAsm->get_cache($userStorage->id);
            $params['area_list'] = !empty($area['area']) ? $area['area'] : [0];
        }
         
        $QStaff = new Application_Model_Staff();

        $staff_scores = $QStaff->fetchPaginationScoresNew($page, $limit, $total, $params);

        if ( isset($export) && $export ) {
            //$StaffScoresExport = $QStaff->fetchPaginationScores($page, null, $total, $params);
            $this->_exportExcelScores($staff_scores);
        } else {
            //$StaffScores = $QStaff->fetchPaginationScores($page, $limit, $total, $params);
            //$this->view->StaffScores = $StaffScores;
        }

        $this->view->staff_scores = $staff_scores;

        $this->view->off_type = unserialize(OFF_TYPE);
        $this->view->userStorage = $userStorage;

        $this->view->desc = $desc;
        $this->view->sort = $sort;
        
        $this->view->params = $params;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'staff/staff-scores/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');

        $this->view->offset = $limit * ($page - 1);
