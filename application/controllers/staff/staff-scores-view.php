<?php

$id            = $this->getRequest()->getParam('id');

if($id){
    
        $QStaff = new Application_Model_Staff();
        $StaffScoresView = $QStaff->fetchPaginationScoresById($id);
        
        $this->view->StaffScoresView = $StaffScoresView;
        $this->view->id = $id;
     
}  