<?php
$back_url = $this->getRequest()->getParam('back_url');

if ($this->getRequest()->getMethod() == 'POST') {
    $QStaff = new Application_Model_Staff();
    $flashMessenger = $this->_helper->flashMessenger;

    $id                 = $this->getRequest()->getParam('id');
    $del_photo          = $this->getRequest()->getParam('del_photo', 0);
    $del_id_photo       = $this->getRequest()->getParam('del_id_photo', 0);
    $del_id_photo_back  = $this->getRequest()->getParam('del_id_photo_back', 0);

    $tags                = $this->getRequest()->getParam('tags');
    $code                = $this->getRequest()->getParam('code');
    $contract_type       = $this->getRequest()->getParam('contract_type');
    $contract_signed_at  = $this->getRequest()->getParam('contract_signed_at');
    $contract_term       = $this->getRequest()->getParam('contract_term');
    $contract_expired_at = $this->getRequest()->getParam('contract_expired_at');
    $print_time          = $this->getRequest()->getParam('print_time');
    $department          = $this->getRequest()->getParam('department');
    $team                = $this->getRequest()->getParam('team');
//    $firstname           = $this->getRequest()->getParam('firstname');
    $full_name          = $this->getRequest()->getParam('full_name');
//    $lastname            = $this->getRequest()->getParam('lastname');
    
   
   
   
    $title               = $this->getRequest()->getParam('title');
    $phone_number        = $this->getRequest()->getParam('phone_number');
    $joined_at           = $this->getRequest()->getParam('joined_at');
    $off_date            = $this->getRequest()->getParam('off_date');
    $off_type            = $this->getRequest()->getParam('off_type', null);
    $gender              = $this->getRequest()->getParam('gender');
    $office_id           = $this->getRequest()->getParam('office_id');
    $array_name=
    //$certificate         = $this->getRequest()->getParam('certificate');


    //education
    $levels          = $this->getRequest()->getParam('levels');
    $schools         = $this->getRequest()->getParam('school');
    $field_of_studys = $this->getRequest()->getParam('field_of_study');
    $graduated_years = $this->getRequest()->getParam('graduated_year');
    $grades          = $this->getRequest()->getParam('grade');
    $mode_of_studys  = $this->getRequest()->getParam('mode_of_study');
    $education_ids   = $this->getRequest()->getParam('education_id');
    $default_level   = $this->getRequest()->getParam('default_level');

    //experience
    $ex_experience_id      = $this->getRequest()->getParam('ex_experience_id');
    $ex_company_name       = $this->getRequest()->getParam('ex_company_name');
    $ex_job_position       = $this->getRequest()->getParam('ex_job_position');
    $ex_from_date          = $this->getRequest()->getParam('ex_from_date');
    $ex_to_date            = $this->getRequest()->getParam('ex_to_date');
    $ex_reason_for_leaving = $this->getRequest()->getParam('ex_reason_for_leaving');

    //relative
    $relative_id    = $this->getRequest()->getParam('relative_id');
    $relative_type  = $this->getRequest()->getParam('relative_type');
    $rlt_full_name  = $this->getRequest()->getParam('rlt_full_name');
    $rlt_gender     = $this->getRequest()->getParam('rlt_gender');
    $rlt_birth_year = $this->getRequest()->getParam('rlt_birth_year');
    $rlt_job        = $this->getRequest()->getParam('rlt_job');
    $rlt_work_place = $this->getRequest()->getParam('rlt_work_place');

    $dob = $this->getRequest()->getParam('dob');
    $company_id                   = $this->getRequest()->getParam('company_id');
    $social_insurance_time        = $this->getRequest()->getParam('social_insurance_time');
    $social_insurance_number      = $this->getRequest()->getParam('social_insurance_number');
    $personal_tax                 = $this->getRequest()->getParam('personal_tax');
    $family_allowances_registered = $this->getRequest()->getParam('family_allowances_registered');
    $is_officer                   = $this->getRequest()->getParam('is_officer', 0);
    $is_print                     = $this->getRequest()->getParam('is_print', 0);
    
    $status              = $this->getRequest()->getParam('status', My_Staff_Status::Off);
    $marital_status      = $this->getRequest()->getParam('marital_status');
    $tags                = $this->getRequest()->getParam('tags');
    $code                = $this->getRequest()->getParam('code');
    $contract_type       = $this->getRequest()->getParam('contract_type');
    $contract_signed_at  = $this->getRequest()->getParam('contract_signed_at');
    $contract_term       = $this->getRequest()->getParam('contract_term');
    $contract_expired_at = $this->getRequest()->getParam('contract_expired_at');
    $print_time          = $this->getRequest()->getParam('print_time');
    $department          = $this->getRequest()->getParam('department');
    $team                = $this->getRequest()->getParam('team');
    $firstname           = $this->getRequest()->getParam('firstname');
    $lastname            = $this->getRequest()->getParam('lastname');
    $title               = $this->getRequest()->getParam('title');
    $phone_number        = $this->getRequest()->getParam('phone_number');
    $joined_at           = $this->getRequest()->getParam('joined_at');
    $off_date            = $this->getRequest()->getParam('off_date');
    $off_type            = $this->getRequest()->getParam('off_type', null);
    $gender              = $this->getRequest()->getParam('gender');
    $level               = $this->getRequest()->getParam('level');
    $certificate         = $this->getRequest()->getParam('certificate');

    $ID_number       = $this->getRequest()->getParam('ID_number');
    $ID_place        = $this->getRequest()->getParam('ID_place');
    $ID_date         = $this->getRequest()->getParam('ID_date');
    $nationality     = $this->getRequest()->getParam('nationality');
    $religion        = $this->getRequest()->getParam('religion');
    $note            = $this->getRequest()->getParam('note');
    $email           = $this->getRequest()->getParam('email');
    $regional_market = $this->getRequest()->getParam('regional_market');
    $change_password = $this->getRequest()->getParam('change-pass');
    $password        = $this->getRequest()->getParam('password');
    $group_id        = $this->getRequest()->getParam('group_id');
    // $native_place      = $this->getRequest()->getParam('native_place');

    $dob                          = $this->getRequest()->getParam('dob');
    $company_id                   = $this->getRequest()->getParam('company_id');
    $social_insurance_time        = $this->getRequest()->getParam('social_insurance_time');
    $social_insurance_number      = $this->getRequest()->getParam('social_insurance_number');
    $personal_tax                 = $this->getRequest()->getParam('personal_tax');
    $family_allowances_registered = $this->getRequest()->getParam('family_allowances_registered');
    $is_officer                   = $this->getRequest()->getParam('is_officer', 0);
    $is_print                     = $this->getRequest()->getParam('is_print', 0);
    $pvi                          = $this->getRequest()->getParam('pvi', 0);
    $lock                         = $this->getRequest()->getParam('lock' , 0);
    
    //bank
    $bank_account                 = $this->getRequest()->getParam('bank_account');
    $bank                         = $this->getRequest()->getParam('bank');
    $bank_name                    = $this->getRequest()->getParam('bank_name');
    $cash                         = $this->getRequest()->getParam('cash',0);
    
    //dependent
    $pit_code                = $this->getRequest()->getParam('pit_code');
    $dependent_id            = $this->getRequest()->getParam('dependent_id');
    $dependent_type          = $this->getRequest()->getParam('dependent_type');
    $dependent_full_name     = $this->getRequest()->getParam('dependent_full_name');
    $dependent_id_number     = $this->getRequest()->getParam('dependent_id_number');

    $id_place_province          = $this->getRequest()->getParam('id_place_province' , 0);

    $status = $this->getRequest()->getParam('status', My_Staff_Status::Off);
    $marital_status = $this->getRequest()->getParam('marital_status',My_Staff_MaritalStatus::Single);

    $date_off_purpose_reason = $this->getRequest()->getParam('date_off_purpose_reason');
    $date_off_purpose_detail = $this->getRequest()->getParam('date_off_purpose_detail');

    $id_insert = '';

    $off_all_machine              = $this->getRequest()->getParam('off_all_machine');
    $post_map_machine             = $this->getRequest()->getParam('post_map_machine');
    if(!empty($off_all_machine))
    {
        $QCheck_in = new Application_Model_CheckIn();
        $QCheck_in->offAllMachineConfig($code);
    }

    

    $tmp = $this->_formatDate($off_date);
    $pos=strripos(trim($full_name)," ");
    $firstname= substr(trim($full_name),0,$pos);
    $lastname= substr(trim($full_name),$pos+1,strlen(trim($full_name)) - $pos);

    //dÃ¡nh dáº¥u ngÃ y cáº­p nháº­t offdate cho Báº£o hiá»ƒm
    $off_date_created_at = ($tmp) ? date('Y-m-d H:i:s') : NULL;
    $data = array(
        'contract_type'                => intval($contract_type),
        'contract_signed_at'           => $this->_formatDate($contract_signed_at),
        'contract_term'                => intval($contract_term),
        'contract_expired_at'          => $this->_formatDate($contract_expired_at),
        'print_time'                   => $print_time,
        'department'                   => intval($department),
        'team'                         => intval($team),
        'firstname'                    => My_String::trim($firstname),
        'lastname'                     => My_String::trim($lastname),
        'title'                        => $title,
        'phone_number'                 => $phone_number,
        'joined_at'                    => $this->_formatDate($joined_at),
        'off_date'                     => $tmp,
        'gender'                       => $gender,
        'off_type'                     => intval($off_type),
        'level'                        => $level,
        'certificate'                  => $certificate,
        'ID_number'                    => $ID_number,
        'ID_place'                     => $ID_place,
        'ID_date'                      => $this->_formatDate($ID_date),
        'nationality'                  => intval($nationality),
        'religion'                     => intval($religion),
        'note'                         => $note,
        'regional_market'              => intval($regional_market),
        'group_id'                     => intval($group_id),
        'social_insurance_time'        => $social_insurance_time,
        'social_insurance_number'      => $social_insurance_number,
        'personal_tax'                 => $personal_tax,
        'family_allowances_registered' => $family_allowances_registered,
        'dob'                          => $dob,
        'company_id'                   => $company_id,
        'is_officer'                   => $is_officer,
        'is_print'                     => $is_print,
        'pvi'                          => $pvi,
        'status'                       => intval($status),
        'marital_status'               => intval($marital_status),
        'id_place_province'            => intval($id_place_province),
        'office_id'                  => intval($office_id),
        'off_date_created_at'          => $off_date_created_at,
        'date_off_purpose_reason'      => ($date_off_purpose_reason != '') ? $date_off_purpose_reason : null,
        'date_off_purpose_detail'      => $date_off_purpose_detail,
    );
    
    // trÆ°á»ng há»£p lÃ  PG thÃ¬ Ä‘Ã¡nh dáº¥u ko cÃ³ email. cÃ²n láº¡i giá»¯ nguyÃªn
    if (My_Staff::isPgTitle($title))
        $data['has_email'] = 0;

    $remove_store = false;

    //set disabled
    if ($tmp) {
        $data['status'] = My_Staff_Status::Off;
        $data['group_id'] = 0;

        try {
            My_Staff::clear_all_roles($id);
            My_Staff::removeStoreForTransfer($id, $title, $tmp);
            $remove_store = true;
        } catch (exception $e) {
        }
    }

    /**
     * Náº¿u chuyá»ƒn status khÃ¡c on thÃ¬ cÅ©ng xÃ³a quyá»n, xÃ³a store
     */
    if (My_Staff_Status::On != $data['status'] && !$remove_store) {
        try {
            My_Staff::clear_all_roles($id);
            My_Staff::removeStoreForTransfer($id, $title, date('Y-m-d'));
        } catch (exception $e) {
        }
    } else {
        $data['date_off_purpose']       = null;
    }

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $db = Zend_Registry::get('db');

    try {
        $db->beginTransaction();

        if ($email) {
            $email = trim($email);
            $where = array();
            $where[] = $QStaff->getAdapter()->quoteInto('email IS NOT NULL AND email LIKE ?', str_replace(EMAIL_SUFFIX, '', $email).EMAIL_SUFFIX);

            if ($id)
                $where[] = $QStaff->getAdapter()->quoteInto('id <> ?', intval($id));

            $staff_check = $QStaff->fetchRow($where);

            if ($staff_check)
                throw new Exception("Email exists", 1);
        }

        if ($id) {
            if ($code)
                $data['code'] = $code;

            $where = $QStaff->getAdapter()->quoteInto('id = ?', $id);

            if ($change_password)
                $data['password'] = md5($password);

            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = $userStorage->id;

            $staffRowset = $QStaff->find($id);
            $s = $staffRowset->current();

            if (empty($s['old_email'])) {
                $old_email = (!empty($tmp) ? $email : '');
                $data['old_email'] = $old_email;
            }

            $email = (!empty($tmp) ? null : $email);
            $data['email'] = $email;
            
            //check staff edit bank 
            if($userStorage->id == STAFF_EDIT_BANK_PIT || $userStorage->group_id == ADMINISTRATOR_ID){
                $data['pit_code'] = $pit_code;
            }
            //end check

            $QStaff->update($data, $where);

            $staffRowset = $QStaff->find($id);
            $s_after = $staffRowset->current();

            if(isset($lock) and $lock == 0)
            {
                $QTimeStaffExpired = new Application_Model_TimeStaffExpired();
                $whereExpired = array();
                $whereExpired[] = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ?' , $id);
                $whereExpired[] = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null' , null);
                $data = array(
                    'approved_at' => date('Y-m-d h:i:s'),
                    'approved_by' => $userStorage->id
                );
                $QTimeStaffExpired->update($data, $whereExpired);
            }

            Log::w($s->toArray(), $s_after->toArray(), $id, LogGroup::Staff, LogType::
            Update);

            $QInsuranceStaffChange = new Application_Model_InsuranceStaffChange();
            $QInsuranceStaffChange->save($s->toArray(),$s_after->toArray(),$id);

        } else {
            // generate staff code
            $data['code'] = $QStaff->genStaffCode($this->_formatDate($joined_at));

            $password = empty($password) ? '123456' : $password;
            $data['password'] = md5($password);

            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $userStorage->id;

            //Default unit_code_id For insurance
            $data['unit_code_id'] = intval($company_id);
            
            //check staff edit bank 
            if($userStorage->id == STAFF_EDIT_BANK_PIT || $userStorage->group_id == ADMINISTRATOR_ID){
                $data['pit_code'] = $pit_code;
            }
            //end check
            
            $id        = $QStaff->insert($data);
            $joined_at = $this->_formatDate($joined_at);
            $result    = $this->contractAdd($id, $joined_at);

            $id_insert = $id;

            if ($result['code'] == -1) {
                throw new Exception('Error when update contract term');
            }

            $staffRowset = $QStaff->find($id);
            $s_after = $staffRowset->current();

            Log::w(array(), $s_after->toArray(), $id, LogGroup::Staff, LogType::Insert);

        }

        // get address
        $temp_address     = $this->getRequest()->getParam('temp_address');
        $temp_ward        = $this->getRequest()->getParam('temp_ward');
        $temp_district    = $this->getRequest()->getParam('temp_district');
        $temp_province    = $this->getRequest()->getParam('temp_province');
        $temp_ward_id     = $this->getRequest()->getParam('temp_ward_id');

        $perm_address     = $this->getRequest()->getParam('perm_address');
        $perm_ward        = $this->getRequest()->getParam('perm_ward');
        $perm_district    = $this->getRequest()->getParam('perm_district');
        $perm_province    = $this->getRequest()->getParam('perm_province');
        $perm_ward_id     = $this->getRequest()->getParam('perm_ward_id');

        $birth_ward       = $this->getRequest()->getParam('birth_ward');
        $birth_district   = $this->getRequest()->getParam('birth_district');
        $birth_province   = $this->getRequest()->getParam('birth_province');
        $birth_ward_id    = $this->getRequest()->getParam('birth_ward_id');		

        $id_card_address  = $this->getRequest()->getParam('id_card_address');
        $id_card_ward     = $this->getRequest()->getParam('id_card_ward');
        $id_card_district = $this->getRequest()->getParam('id_card_district');
        $id_card_province = $this->getRequest()->getParam('id_card_province');
        $id_card_ward_id  = $this->getRequest()->getParam('id_card_ward_id');

        if ($id) {

            $staffRowset = $QStaff->find($id);
            $s = $staffRowset->current();

            // ------------------ upload
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $id;

            $file_uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $id;

            $arrPhoto = array(
                'photo' => $uploaded_dir,
                'offdate_file' => $file_uploaded_dir.DIRECTORY_SEPARATOR.'off_date',
                'id_photo' => $uploaded_dir.DIRECTORY_SEPARATOR.'ID_Front',
                'id_photo_back' => $uploaded_dir.DIRECTORY_SEPARATOR.'ID_Back',
            );

            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile'=>true));

            //check function
            if (function_exists('finfo_file'))
                $upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/pdf','application/zip'));

            $upload->addValidator('Extension', false, 'jpg,jpeg,png,gif,doc,docx');
            $upload->addValidator('Size', false, array('max' => '2MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');
            $files = $upload->getFileInfo();

            $hasPhoto = false;

            $data = array();

            foreach ($arrPhoto as $key=>$val){
                $del = 'del_'.$key;
                if (isset($$del) and $$del){
                    $data[$key] = null;

                    @unlink($val . $s[$key]);
                }

                if (isset($files[$key]['name'])){
                    $hasPhoto = true;
                }
            }

            if ($hasPhoto) {

                if (!$upload->isValid()){
                    $errors = $upload->getErrors();

                    $sError = null;

                    if ($errors and isset($errors[0]))
                        switch ($errors[0]){
                            case 'fileUploadErrorIniSize':
                                $sError = 'File size is too large';
                                break;
                            case 'fileMimeTypeFalse':
                            case 'fileExtensionFalse':
                                $sError = 'The file(s) you selected weren\'t the type we were expecting';
                                break;
                            default:
                                $sError = 'The file(s) you selected weren\'t the type we were expecting';
                                break;
                        }

                    throw new Exception($sError);
                }


                foreach ($arrPhoto as $key => $val){
                    $fileInfo = (isset($files[$key]) and $files[$key]) ? $files[$key] : null;
                    if (isset($fileInfo['name']) and $fileInfo['name']) {

                        if (!is_dir($val))
                            @mkdir($val, 0777, true);

                        $upload->setDestination($val);

                        $old_name = $fileInfo['name'];

                        $tExplode = explode('.', $old_name);
                        $extension = strtolower(end($tExplode));

                        $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

                        $upload->addFilter('Rename', array('target' => $val .
                            DIRECTORY_SEPARATOR . $new_name));

                        $r = $upload->receive(array($key));

                        if ($r)
                            $data[$key] = $new_name;
                        else{
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg)
                                throw new Exception($msg);
                        }
                    }
                }
            }

            if ($data) {
                $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?', $id);
                $QStaff->update($data, $whereStaff);
            }
            // ------------------ /upload

            $QStaffAddress = new Application_Model_StaffAddress();

            //dia chi tam tru
            if ($temp_province) {
                $address_data = array(
                    'staff_id' => $id,
                    'address_type' => My_Staff_Address::Temporary,
                    'address' => $temp_address,
                    'ward' => $temp_ward,
                    'district' => $temp_district,
                    'ward_id' => $temp_ward_id != '' ? $temp_ward_id : null
                );

                try {
                    $QStaffAddress->insert($address_data);
                }
                catch (exception $e) {
                    $where = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?',
                        My_Staff_Address::Temporary);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    }
                    catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            //dia chi thuong tru
            if ($perm_province) {
                $address_data = array(
                    'staff_id' => $id,
                    'address_type' => My_Staff_Address::Permanent,
                    'address' => $perm_address,
                    'ward' => $perm_ward,
                    'district' => $perm_district,
                    'ward_id' => $perm_ward_id != '' ? $perm_ward_id : null
                );

                try {
                    $QStaffAddress->insert($address_data);
                }

                catch (exception $e) {
                    $where = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?',
                        My_Staff_Address::Permanent);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    }
                    catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            // dia chi chung minh
            if ($id_card_province) {
                $address_data = array(
                    'staff_id' => $id,
                    'address_type' => My_Staff_Address::ID_Card,
                    'address' => $id_card_address,
                    'ward' => $id_card_ward,
                    'district' => $id_card_district,
                    'ward_id' => $id_card_ward_id != '' ? $id_card_ward_id : null 
                );

                try {
                    $QStaffAddress->insert($address_data);
                }
                catch (exception $e) {
                    $where = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?',
                        My_Staff_Address::ID_Card);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    }
                    catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            //dia chi noi sinh
            if ($birth_province) {
                $address_data = array(
                    'staff_id' => $id,
                    'address_type' => My_Staff_Address::Birth_Certificate,
                    'ward' => $birth_ward,
                    'district' => $birth_district,
                    'ward_id' => $birth_ward_id != '' ? $birth_ward_id : null 
                );

                try {
                    $QStaffAddress->insert($address_data);
                }
                catch (exception $e) {
                    $where = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?',
                        My_Staff_Address::Birth_Certificate);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    }
                    catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            //check education
            if(count($levels) > 0 AND is_array($levels) AND is_array($schools) AND is_array($field_of_studys)
                AND is_array($graduated_years) AND is_array($grades) AND is_array($mode_of_studys)){
                $QStaffEducation = new Application_Model_StaffEducation();

                foreach($levels as $key => $value){

                    if($value == '' OR $value == NULL){
                        continue;
                    }

                    $dataEdu = array(
                        'staff_id'       => $id,
                        'level'          => $value,
                        'school'         => $schools[$key],
                        'field_of_study' => $field_of_studys[$key],
                        'graduated_year' => $graduated_years[$key],
                        'grade'          => $grades[$key],
                        'mode_of_study'  => $mode_of_studys[$key],
                        'default_level'  => (isset($default_level[$key]) AND $default_level[$key] == 1) ? 1:0,
                    );

                    if(isset($education_ids[$key]) AND $education_ids[$key]){
                        $where = $QStaffEducation->getAdapter()->quoteInto('id = ?',$education_ids[$key]);
                        $QStaffEducation->update($dataEdu,$where);

                    }else{
                        $QStaffEducation->insert($dataEdu);
                    }

                }
                $selectDefaultLevel = $QStaffEducation->select()
                    ->where('staff_id = ?',$id)
                    ->where('default_level = ?',1);
                $result = $QStaffEducation->fetchAll($selectDefaultLevel);
                if($result->count() > 1){
                    $flashMessenger->setNamespace('error')->addMessage('Education default is only single');
                    $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
                }
            }

            //experience
            $QStaffExperience = new Application_Model_StaffExperience();
            if(is_array($ex_company_name) AND is_array($ex_job_position) AND is_array($ex_from_date) AND is_array($ex_from_date)
                AND is_array($ex_to_date) AND is_array($ex_reason_for_leaving) AND count($ex_company_name) > 0){
                foreach($ex_company_name as $key => $value){
                    if(trim($value) == ''){
                        continue;
                    }

                    $tmp_from_date = explode('/',$ex_from_date[$key]);
                    $tmp_from_date = $tmp_from_date[2]. '-'.$tmp_from_date[1].'-'.$tmp_from_date[0];
                    $tmp_to_date = explode('/',$ex_to_date[$key]);
                    $tmp_to_date = $tmp_to_date[2].'-'.$tmp_to_date[1].'-'.$tmp_to_date[0];
                    $dataEx = array(
                        'staff_id'           => $id,
                        'company_name'       => $value,
                        'job_position'       => $ex_job_position[$key],
                        'from_date'          => date('Y-m-d',strtotime($tmp_from_date)),
                        'to_date'            => date('Y-m-d',strtotime($tmp_to_date)),
                        'reason_for_leaving' => $ex_reason_for_leaving[$key],
                    );

                    if(isset($ex_experience_id[$key]) AND $ex_experience_id[$key]){
                        $where = $QStaffExperience->getAdapter()->quoteInto('id = ?',$ex_experience_id[$key]);
                        $QStaffExperience->update($dataEx,$where);
                    }else{
                        $QStaffExperience->insert($dataEx);
                    }
                }
            }//End experience

            //relative
            if(is_array($relative_type) AND is_array($rlt_full_name) AND is_array($rlt_gender)
                AND is_array($rlt_birth_year) AND is_array($rlt_job) AND is_array($rlt_work_place) AND count($relative_type) > 0){
                $QStaffRelative = new Application_Model_StaffRelative();
                foreach($relative_type as $key => $value){
                    if(trim($value) == ''){
                        continue;
                    }
                    $dataRlt = array(
                        'staff_id'      => $id,
                        'relative_type' => intval($value),
                        'full_name'     => trim($rlt_full_name[$key]),
                        'gender'        => intval($rlt_gender[$key]),
                        'birth_year'    => ($rlt_birth_year[$key]) ? $rlt_birth_year[$key] : NULL,
                        'job'           => trim($rlt_job[$key]),
                        'work_place'    => trim($rlt_work_place[$key])
                    );
                    if(isset($relative_id[$key]) AND $relative_id[$key]){
                        $where = $QStaffRelative->getAdapter()->quoteInto('id = ?',$relative_id[$key]);
                        $QStaffRelative->update($dataRlt,$where);
                    }else{
                        $QStaffRelative->insert($dataRlt);
                    }
                }
            }//End relative
            

            //check staff edit bank, edit PIT
            if($userStorage->id == STAFF_EDIT_BANK_PIT || $userStorage->group_id == ADMINISTRATOR_ID){
                //bank
                $QStaffBank = new Application_Model_StaffBank();
                if(is_null($bank_account) || $bank_account == '' || intval($bank) == 0){
                    $cash = 1;
                }
                
                if ($bank) {
                    $bank_data = array(
                        'staff_id'     => $id,
                        'bank_account' => $bank_account,
                        'bank'         => intval($bank),
                        'bank_name'    => $bank_name,
                        'cash'         => intval($cash),
                    );
                    try {
                        $QStaffBank->insert($bank_data);
                    }
                    catch (exception $e) {
                        $where = array();
                        $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                        try {
                            $QStaffBank->update($bank_data, $where);
                        }
                        catch (exception $e) {
                            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                            $this->_redirect(HOST . 'staff');
                        }
                    }
                }
                //END bank
                
                //dependent
                if(is_array($dependent_type) AND is_array($dependent_full_name)
                    AND is_array($dependent_id_number) AND count($dependent_id) > 0){
                    $QStaffDependent = new Application_Model_StaffDependent();
                    foreach($dependent_type as $key => $value){
                        if(trim($value) == ''){
                            continue;
                        }
                        $dataDependent = array(
                            'staff_id'      => $id,
                            'relative_type' => intval($value),
                            'full_name'     => trim($dependent_full_name[$key]),
                            'ID_number'     => trim($dependent_id_number[$key])
                        );
                        if(isset($dependent_id[$key]) AND $dependent_id[$key]){
                            $where = $QStaffDependent->getAdapter()->quoteInto('id = ?',$dependent_id[$key]);
                            $QStaffDependent->update($dataDependent,$where);
                        }else{
                            $QStaffDependent->insert($dataDependent);
                        }
                    }
                }//End dependent
            }
            //end check

        }

        // insert tags
        $QTag = new Application_Model_Tag();
        $QTag->add($tags, $id, TAG_STAFF);

        $cache = Zend_Registry::get('cache');
        $cache->remove('staff_cache');

        $db->commit();

        if ($id_insert)
            My_Request::sync_table(array('table' => 'staff', 'id' => $id_insert, 'type' => 'insert'));
        else
            My_Request::sync_table(array('table' => 'staff', 'id' => $id, 'type' => 'update'));

        $flashMessenger->setNamespace('success')->addMessage('Done!');

    } catch (Exception $e){
        PC::debug($e->getMessage(), 'Exception');
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
}

$this->_redirect(($back_url ? $back_url : HOST . 'staff'));