<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$flashMessenger = $this->_helper->flashMessenger;
$id = $this->getRequest()->getParam('id');
$staff_id = $this->getRequest()->getParam('staff_id');
$ID_number = $this->getRequest()->getParam('ID_number');
$id_place_province = $this->getRequest()->getParam('id_place_province');
$ID_date = $this->getRequest()->getParam('ID_date');
$ID_date = $ID_date ? str_replace('/', '-', $ID_date) : Null;

$phone_number = $this->getRequest()->getParam('phone_number');
$id_card_district = $this->getRequest()->getParam('id_card_district');
$id_card_province = $this->getRequest()->getParam('id_card_province');
$id_card_ward = $this->getRequest()->getParam('id_card_ward');
$id_card_ward_id = $this->getRequest()->getParam('id_card_ward_id');
$id_card_address = $this->getRequest()->getParam('id_card_address');
$card_nationality = $this->getRequest()->getParam('card_nationality');
$card_religion = $this->getRequest()->getParam('card_religion');
$marital_status = $this->getRequest()->getParam('marital_status');
$note = $this->getRequest()->getParam('note');
$approve = $this->getRequest()->getParam('approve');
$reject = $this->getRequest()->getParam('reject');
$delete_id_photo = $this->getRequest()->getParam('delete_id_photo');
$delete_id_photo_back = $this->getRequest()->getParam('delete_id_photo_back');
$delete = $this->getRequest()->getParam('delete');

$QStaffTempNew = new Application_Model_StaffTempNew();
$QStaff = new Application_Model_Staff();
$QStaffAddress = new Application_Model_StaffAddress();
$QStaffTempNewLog = new Application_Model_StaffTempNewLog();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if ($delete) {
    $QStaffTempNew->update([
        'is_deleted' => 1
    ], ['id = ?' => $id]);

    $flashMessenger->setNamespace('success')->addMessage('Success');
    $this->_redirect('/staff/list-update-infor');
}


// get original staff in 'staff' table
$StaffRowSet = $QStaff->find($staff_id);
$original_staff = $StaffRowSet->current();
$ID_card_infor = $QStaff->getIDCardInfor($staff_id);
$edited_staff = $QStaffTempNew->getStaff($id);

$oldInformation = [
    'phone_number' => $original_staff ['phone_number'],
    'ID_number' => $original_staff ['ID_number'],
    'id_place_province' => $original_staff ['id_place_province'],
    'ID_date' => date('Y-m-d', strtotime($original_staff ['ID_date'])),
    'id_photo' => $original_staff ['id_photo'],
    'id_photo_back' => $original_staff ['id_photo_back'],
    'nationality' => $original_staff ['nationality'],
    'religion' => $original_staff ['religion'],
    'marital_status' => $original_staff ['marital_status'],
    'id_card_province' => $ID_card_infor ['id_card_province'],
    'id_card_district' => $ID_card_infor ['id_card_district'],
    'id_card_ward_id' => $ID_card_infor ['id_card_ward_id'],
    'street' => $ID_card_infor ['street']
];

$newInformation = [
    'phone_number' => $edited_staff['phone_number'],
    'ID_number' => $edited_staff['ID_number'],
    'id_place_province' => $edited_staff['id_place_province'],
    'ID_date' => $edited_staff['ID_date'] ? date('Y-m-d', strtotime($edited_staff['ID_date'])) : Null,
    'id_photo' => $edited_staff['id_photo'],
    'id_photo_back' => $edited_staff['id_photo_back'],
    'nationality' => $edited_staff ['nationality'],
    'religion' => $edited_staff ['religion'],
    'marital_status' => $edited_staff ['marital_status'],
    'id_card_province' => $edited_staff['id_card_province'],
    'id_card_district' => $edited_staff['id_card_district'],
    'id_card_ward_id' => $edited_staff['id_card_ward_id'],
    'street' => $edited_staff['id_card_address']
];

$onlyPhoneNummberHasChanged = $QStaffTempNew->onlyPhoneNummberHasChanged($oldInformation, $newInformation);

//save photo upload
$upload = new Zend_File_Transfer();
$images = $upload->getFileInfo();

$upload->addValidator('Extension', false, 'jpg,jpeg,png');
$upload->addValidator('Size', false, array('max' => '5MB'));
$upload->addValidator('ExcludeExtension', false, 'php,sh');

require_once "Aws_s3.php";
$s3_lib = new Aws_s3();
foreach ($images as $image_type => $image_infor) {
    if (!$image_infor['name']) {
        $arr_photo_name [$image_type] = $edited_staff[$image_type];  // ex: $edited_staff['id_photo']  $edited_staff['id_photo_back']
        continue;
    }
    //set folder to save image due to image_type
    if ($image_type == 'id_photo') {
        $folder_image_type = 'ID_Front';
    } elseif ($image_type == 'id_photo_back') {
        $folder_image_type = 'ID_Back';
    }

    $upload_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $staff_id . DIRECTORY_SEPARATOR . $folder_image_type;

    $targets3 = 'photo/staff/'.$staff_id.'/'.$folder_image_type;

    if (!is_dir($upload_dir)) {  //Check if the directory already exists, if not, create it
        mkdir($upload_dir, 0755);
    }

    $extension = pathinfo($image_infor['name'], PATHINFO_EXTENSION);
    $new_name = $upload_dir . DIRECTORY_SEPARATOR . md5(uniqid('', true)) . '.' . $extension;

    $upload->addFilter('Rename', array('target' => $new_name));  // rename the image
    // $upload->setDestination($upload_dir);
    // check file upload
    if (!$upload->isValid($image_type)) {
        $array_error = $upload->getMessages();

        if (isset($array_error['fileSizeTooBig'])) {
            $flashMessenger->setNamespace('error')->addMessage('File ảnh dung lượng quá 5MB');
        }
        if (isset($array_error['fileExtensionFalse'])) {
            $flashMessenger->setNamespace('error')->addMessage('File ảnh không đúng định dạng');
        }

        $this->_redirect('/staff/list-update-detail?staff_id=' . $staff_id . '&id=' . $id);
    }

    $result = $upload->receive($image_type); // save image
    
    $file_upload = $new_name;
    $destination =  $targets3. '/';// s3 su dung dir /
    $s3_lib->uploadS3($file_upload, $destination, basename($new_name));
    
    $arr_photo_name [$image_type] = basename($new_name); // to insert to database

} // end save photo upload

$where_staff_temp_new = $QStaffTempNew->getAdapter()->quoteInto('id = ?', $id);
$where_staff = $QStaffTempNew->getAdapter()->quoteInto('id = ?', $staff_id);

//$where_staff_address[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $staff_id);
//$where_staff_address[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::ID_Card);
$arrStaffAddressType = [My_Staff_Address::ID_Card, My_Staff_Address::Permanent, My_Staff_Address::Birth_Certificate];


$db = Zend_Registry::get('db');
$db->beginTransaction();

if ($approve) {
    if ($edited_staff['is_approved'] == 1) {  // HR Approve => save to 'staff' and 'staff_address' table
        $before = $QStaffTempNew->fetchRow($where_staff_temp_new)->toArray();

        //Update id_citizen_province
        if($id_place_province >= 66){
            $QStaff->update([
                'ID_number' => $ID_number ? $ID_number : Null,
                'ID_date' => $ID_date ? date('Y-m-d', strtotime($ID_date)) : Null,
                'id_place_province' => 0,
                'id_citizen_province' => $id_place_province ? $id_place_province : Null,
                'phone_number' => $phone_number ? $phone_number : Null,
                'id_photo' => $arr_photo_name['id_photo'] ? $arr_photo_name['id_photo'] : Null,
                'id_photo_back' => $arr_photo_name['id_photo_back'] ? $arr_photo_name['id_photo_back'] : Null,
                'nationality' => $card_nationality ? $card_nationality : null,
                'religion' => $card_religion ? $card_religion : null,
                'marital_status' => $marital_status ? $marital_status : null
            ], $where_staff);
        //Update id_place_province
        }else {
            $QStaff->update([
                'ID_number' => $ID_number ? $ID_number : Null,
                'ID_date' => $ID_date ? date('Y-m-d', strtotime($ID_date)) : Null,
                'id_place_province' => $id_place_province ? $id_place_province : Null,
                'id_citizen_province' => 0,
                'phone_number' => $phone_number ? $phone_number : Null,
                'id_photo' => $arr_photo_name['id_photo'] ? $arr_photo_name['id_photo'] : Null,
                'id_photo_back' => $arr_photo_name['id_photo_back'] ? $arr_photo_name['id_photo_back'] : Null,
                'nationality' => $card_nationality ? $card_nationality : null,
                'religion' => $card_religion ? $card_religion : null,
                'marital_status' => $marital_status ? $marital_status : null
            ], $where_staff);
        }

        //foreach ($arrStaffAddressType as $type) {
            //chỉ update id card address
            $where_staff_address = [
                'staff_id = ?' => $staff_id,
                //'address_type = ?' => $type
                'address_type = 4'
            ];
            $QStaffAddress->update([
                'address' => $id_card_address ? $id_card_address : null,
                'ward' => $id_card_ward ? $id_card_ward : null,
                'ward_id' => $id_card_ward_id ? $id_card_ward_id : null,
                'district' => $id_card_district ? $id_card_district : null
                // 'province_code' => $id_card_province,
            ], $where_staff_address);
        //}


        $QStaffTempNew->update([
            'is_approved' => 2,
            'note' => null,
            'is_rejected' => 0,
            'is_deleted' => 1
        ], $where_staff_temp_new);

        //save log
        $QStaffTempNewLog->insert([
            'staff_temp_new_id' => $id,
            'before' => json_encode($before),
            'after' => null,
            'type' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id
        ]);


    } elseif ($edited_staff['is_approved'] == 0) {  // salesadmin, assisstant approve

        $before = $QStaffTempNew->fetchRow($where_staff_temp_new)->toArray();
        $is_head_office = $QStaff->isHeadOffice($userStorage->id);
        if ($onlyPhoneNummberHasChanged) {
            $QStaff->update([
               'phone_number' => $phone_number
            ], $where_staff);

            $QStaffTempNew->update([
                'phone_number' => $phone_number,
               'is_deleted' => 1
            ], $where_staff_temp_new);
        } else {
            $data = [
                'staff_id' => $staff_id,
                'ID_number' => $ID_number ? $ID_number : Null,
                'id_place_province' => $id_place_province ? $id_place_province : Null,
                'ID_date' => $ID_date ? date('Y-m-d', strtotime($ID_date)) : Null,
                'phone_number' => $phone_number ? $phone_number : Null,
                'id_card_district' => $id_card_district ? $id_card_district : null,
                'id_card_province' => $id_card_province ? $id_card_province : null,
                'id_card_ward' => $id_card_ward ? $id_card_ward : null,
                'id_card_ward_id' => $id_card_ward_id ? $id_card_ward_id : null,
                'id_card_address' => $id_card_address ? $id_card_address : Null,
		        'nationality' => $card_nationality ? $card_nationality : null,
                'religion' => $card_religion ? $card_religion : null,
                'marital_status' => $marital_status ? $marital_status : null,
                'id_photo' => $arr_photo_name['id_photo'],
                'id_photo_back' => $arr_photo_name['id_photo_back'],
                'is_approved' => 1,
                'approve_by' => $userStorage->id,
                'approve_at' => date("Y-m-d H:i:s"),
                'is_rejected' => 0
            ];

            $QStaffTempNew->update($data, $where_staff_temp_new);
        }

        $after = $QStaffTempNew->fetchRow($where_staff_temp_new)->toArray();

        $QStaffTempNewLog->insert([
            'staff_temp_new_id' => $id,
            'before' => json_encode($before),
            'after' => json_encode($after),
            'type' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id
        ]);

    }

} elseif ($reject) { // only HR can reject
    if ($edited_staff['is_approved'] == 1) { // hr reject

        $is_head_office = $QStaff->isHeadOffice($edited_staff['staff_id']);
        $before = $QStaffTempNew->fetchRow($where_staff_temp_new)->toArray();

        $QStaffTempNew->update([
            'staff_id' => $staff_id,
            'ID_number' => $ID_number ? $ID_number : Null,
            'id_place_province' => $id_place_province ? $id_place_province : Null,
            'ID_date' => $ID_date ? date('Y-m-d', strtotime($ID_date)) : Null,
            'phone_number' => $phone_number ? $phone_number : Null,
            'id_card_district' => $id_card_district ? $id_card_district : Null,
            'id_card_province' => $id_card_province ? $id_card_province : Null,
            'id_card_ward' => $id_card_ward ? $id_card_ward : null,
            'id_card_ward_id' => $id_card_ward_id ? $id_card_ward_id : null,
            'id_card_address' => $id_card_address ? $id_card_address : null,
     	    'nationality' => $card_nationality ? $card_nationality : null,
            'religion' => $card_religion ? $card_religion : null,
            'marital_status' => $marital_status ? $marital_status : null,
            'id_photo' => $arr_photo_name['id_photo'],
            'id_photo_back' => $arr_photo_name['id_photo_back'],
            'is_approved' => 0,
            'note' => $note,
            'is_rejected' => 1,
            'reject_by' => $userStorage->id,
            'reject_at' => date("Y-m-d H:i:s"),
            'is_deleted' => $is_head_office ? 1 : 0
        ], $where_staff_temp_new);

        $after = $QStaffTempNew->fetchRow($where_staff_temp_new)->toArray();
        //save log
        $QStaffTempNewLog->insert([
            'staff_temp_new_id' => $id,
            'before' => json_encode($before),
            'after' => json_encode($after),
            'type' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id
        ]);

    }
//            elseif ($edited_staff['is_approved'] == 0){ // sales admin or asiistant reject
//
//                $before = $QStaffTempNew->fetchRow($where_staff_temp_new)->toArray();
//
//                $QStaffTempNew->update(['is_deleted' => 1], $where_staff_temp_new);
//
//                $QStaffTempNewLog->insert([
//                    'staff_temp_new_id' => $id,
//                    'before' => json_encode($before),
//                    'after' => null,
//                    'type' => 4,
//                    'created_at' => date('Y-m-d H:i:s'),
//                    'created_by' => $userStorage->id
//                ]);
//            }
}
$db->commit();

$flashMessenger->setNamespace('success')->addMessage('Success');
$this->_redirect('/staff/list-update-infor');
