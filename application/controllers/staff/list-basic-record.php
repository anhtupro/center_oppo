<?php

$sort = My_Util::escape_string($this->getRequest()->getParam('sort', ''));
$desc = My_Util::escape_string($this->getRequest()->getParam('desc', 1));
$page = My_Util::escape_string($this->getRequest()->getParam('page', 1));
$name = My_Util::escape_string($this->getRequest()->getParam('name'));
$department = My_Util::escape_string($this->getRequest()->getParam('department'));
$off = My_Util::escape_string($this->getRequest()->getParam('off', 1));
$export = My_Util::escape_string($this->getRequest()->getParam('export'));
$team = My_Util::escape_string($this->getRequest()->getParam('team'));
$regional_market = My_Util::escape_string($this->getRequest()->getParam('regional_market'));
$district = My_Util::escape_string($this->getRequest()->getParam('district'));
$area_id = My_Util::escape_string($this->getRequest()->getParam('area_id'));
$note = My_Util::escape_string($this->getRequest()->getParam('note'));
$sname = My_Util::escape_string($this->getRequest()->getParam('sname', 0));
$code = My_Util::escape_string($this->getRequest()->getParam('code'));
$s_assign = My_Util::escape_string($this->getRequest()->getParam('s_assign'));
$ood = My_Util::escape_string($this->getRequest()->getParam('ood'));
$email = My_Util::escape_string($this->getRequest()->getParam('email'));
$is_officer = My_Util::escape_string($this->getRequest()->getParam('is_officer'));
$date = My_Util::escape_string($this->getRequest()->getParam('date'));
$month = My_Util::escape_string($this->getRequest()->getParam('month'));
$year = My_Util::escape_string($this->getRequest()->getParam('year'));
$tags = My_Util::escape_string($this->getRequest()->getParam('tags'));
$title = My_Util::escape_string($this->getRequest()->getParam('title'));
$company_id = My_Util::escape_string($this->getRequest()->getParam('company_id'));
$is_approved = My_Util::escape_string($this->getRequest()->getParam('is_approved'));

if (!($tags and is_array($tags)))
    $tags = null;

if (!$sname)
    $limit = LIMITATION;
else
    $limit = null;

// check quyen view: neu la team sales admin moi gioi han
$listAreaIds = array();
if (CHECK_USER_EDIT_AREA) {
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $group_id = $userStorage->group_id;
    $QAsm = new Application_Model_Asm();

    // asm thi quan ly nhieu khuu vuc
    if (in_array($userStorage->title, array(WESTERN_SALE_ANAGER, 308, 471, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM)) OR ($group_id == SALES_ADMIN_ID || $group_id == ASM_ID)) {
        $cachedASM = $QAsm->get_cache($userStorage->id);
        $tem = $cachedASM['area'];
        if ($tem)
            $listAreaIds = $tem;
        else
            $listAreaIds = -1;
    }
}

$total = 0;

$params = array(
    'name' => $name,
    'department' => $department,
    'off' => $off,
    'team' => $team,
    'regional_market' => $regional_market,
    'district' => $district,
    'area_id' => $area_id,
    'note' => $note,
    'code' => $code,
    's_assign' => $s_assign,
    'ood' => $ood,
    'email' => $email,
    'date' => $date,
    'month' => $month,
    'year' => $year,
    'is_officer' => $is_officer,
    'sname' => $sname,
    'tags' => $tags,
    'title' => $title,
    'company_id' => $company_id,
    'is_approved' => $is_approved,
    'listAreaIds' => $listAreaIds
);
$QStaff = new Application_Model_Staff();
$QTeam = new Application_Model_Team();
$staff = $QStaff->find($userStorage->id);
$staff = $staff->current();
$title = $staff['title'];
$team = $QTeam->find($title);
$team = $team->current();
$group_id = $team['access_group'];

if (($group_id == SALES_EXT_ID ) || in_array($userStorage->id, array(240, 12719, 2123)) || in_array($userStorage->title, [621, TRADE_MARKETING_ASSISTANT])) { // 2123:dieu.le
    $params['department'] = array(DEPARTMENT_SALE, DEPARTMENT_RETAIL);
    $params['team'] = array(TEAM_SALE_BRAND_SHOP, 75);

    if ($userStorage->id == 24) {
        $params['department'] = array(DEPARTMENT_SALE, DEPARTMENT_RETAIL, 557);
    }
    if (in_array($userStorage->id, array(12719))) { //thuydung.lethi
        $params['department'] = array(BRAND_SHOP_DEPT, DEPARTMENT_SALE, DEPARTMENT_RETAIL);
        $params['team'] = array(BRANDSHOP_TEAM, TEAM_SALE_BRAND_SHOP, TEAM_RETAIL_BRAND_SHOP);
    }

    if (in_array($userStorage->id, array(2123))) { //dieu.le
        $params['department'] = array(BRAND_SHOP_DEPT, DEPARTMENT_SALE, DEPARTMENT_RETAIL);
        $params['team'] = array(BRANDSHOP_TEAM, TEAM_SALE_BRAND_SHOP, TEAM_RETAIL_BRAND_SHOP);
    }

    if ($userStorage->title == 621) {
        $params['department'] = array(DEPARTMENT_RETAIL, TRANING_DEPT, DEPARTMENT_SALE);
        $params['team'] = array(TEAM_SALE_TRAINING, TRAINING_TEAM, TEAM_TRAINING_TRAINING);
    }
    if ($userStorage->title == TRADE_MARKETING_ASSISTANT) {
        $params['department'] = array(DEPARTMENT_RETAIL, TRADE_MARKETING_DEPT, DEPARTMENT_SALE);
        $params['team'] = array(TEAM_SALE_TRADE_MARKETING, TEAM_TRADE_MARKETING, TEAM_TRADEMARKETING);
    }
} else if (!in_array($userStorage->group_id, array(1, 7, 21))) {
    if (!empty($department) && in_array(321, $department)) {
        $params['department'] = array($userStorage->department, 321);
    } else {
        $params['department'] = array($userStorage->department);
        if ($userStorage->title == SALES_ADMIN_TITLE) {
            $params['department'][] = BRAND_SHOP_DEPT;
            $params['department'][] = TRADE_MARKETING_DEPT;
            $params['department'][] = TRANING_DEPT;
        }
    }
    //minhtam.pham
    if (in_array($userStorage->id, array(4617, 24644))) {

        $params['department'] = array(159, DEPARTMENT_SALE);
    }
    //phuoctuan.nguyen
    if (in_array($userStorage->id, array(14682))) {

        $params['department'] = array(DEPARTMENT_RETAIL, DEPARTMENT_SALE);
    }

    if (in_array($userStorage->id, array(512))) {
        $params['department'] = array(DEPARTMENT_SALE);
    }
}
// end

$params['sort'] = $sort;
$params['desc'] = $desc;
$params['date_off_purpose'] = 'null';

if ($export == 2) {
    $this->_exportPictureAdd($params);
}

$QArea = new Application_Model_Area();
$this->view->areas = $QArea->fetchAll(null, 'name');

if ($area_id) {
    if (is_array($area_id) && count($area_id))
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

if ($regional_market) {
    if (is_array($regional_market) && count($regional_market))
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}

$QStaffTemp = new Application_Model_StaffTemp();

$staffs = $QStaffTemp->fetchPagination($page, $limit, $total, $params);

$this->view->staff = $staff;
$this->view->listAreaIds = $listAreaIds;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->staffs = $staffs;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'staff/list-basic-record/' . ($params ? '?' . http_build_query($params) .
        '&' : '?');

$this->view->offset = $limit * ($page - 1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages;

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

$QTeam = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

$QCompany = new Application_Model_Company();
$this->view->companies = $QCompany->get_cache();

$this->view->is_list_basic_record = 1;

$this->_helper->viewRenderer->setRender('list-basic');


if ($_GET['dev']) {
    echo "<pre>";
    print_r($QStaffTemp->fetchPagination($page, $limit, $total, $params));
}