<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$staff_code    = $this->getRequest()->getParam('staff_code');
$staff_id    = $this->getRequest()->getParam('staff_id');
$QLock_time = new Application_Model_LockTime();


if(!empty($staff_id)){
    $month 			= date ( 'm' );
    $year 			= date ( 'Y' );
    // check lock time-> khong keo lai
    $where_lock_time = array();
    $where_lock_time[] = $QLock_time->getAdapter()->quoteInto('month = ?', $month);
    $where_lock_time[] = $QLock_time->getAdapter()->quoteInto('year = ?', $year);
    $lock_time = $QLock_time->fetchAll($where_lock_time)->toArray();
    if (!empty($lock_time)) {
        echo json_encode(array(
            'message' => 'Thang '.$month.' đã bị lock công. không kéo lại được'
        ));
        exit();
    }
    
    // check rule chua lock cong duoc quyen keo lai
    $last_month = date('m', strtotime("-1 month"));
    $year_last_month = date('Y', strtotime("-1 month"));
    $where_lock_time_last = array();
    $where_lock_time_last[] = $QLock_time->getAdapter()->quoteInto('month = ?', $last_month);
    $where_lock_time_last[] = $QLock_time->getAdapter()->quoteInto('year = ?', $year_last_month);
    $lock_time_last = $QLock_time->fetchAll($where_lock_time_last)->toArray();
    if (empty($lock_time_last)) {
        $from_date = $year_last_month . '-' . $last_month . '-01';
    }else {
        $from_date = $year . '-' . $month . '-01';
    }
    
    $to_date        = $year . '-' . $month .'-'. date ( 'd' );
    
    $db   = Zend_Registry::get('db');
    $stmt   = $db->prepare("CALL `PR_calc_staff_time_by_id`(:p_from_date, :p_to_date, :p_staff_code)");
    $stmt->bindParam("p_from_date", $from_date , PDO::PARAM_STR);
    $stmt->bindParam("p_to_date", $to_date , PDO::PARAM_STR);
    $stmt->bindParam("p_staff_code", $staff_code, PDO::PARAM_STR);
    
    $stmt->execute();
    $stmt->closeCursor();
    
    echo json_encode(array(
        'from_date' =>  $from_date,
        'to_date' =>  $to_date
    ));
}

   
exit();