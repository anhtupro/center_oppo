<?php
$session_rejected = new Zend_Session_Namespace('session_rejected');

if (!isset($session_rejected->popup))
    $session_rejected->popup = true;
else
    $session_rejected->popup = false;

$this->view->rejected_popup = $session_rejected->popup;



$sort            = $this->getRequest()->getParam('sort', 'date_off_purpose');
$desc            = $this->getRequest()->getParam('desc', 1);

$page            = $this->getRequest()->getParam('page', 1);
$name            = $this->getRequest()->getParam('name');
$department      = $this->getRequest()->getParam('department');
$off             = $this->getRequest()->getParam('off', 1);
$team            = $this->getRequest()->getParam('team');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$area_id         = $this->getRequest()->getParam('area_id');
$note            = $this->getRequest()->getParam('note');
$sname           = $this->getRequest()->getParam('sname', 0);
$code            = $this->getRequest()->getParam('code');
$s_assign        = $this->getRequest()->getParam('s_assign');
$ood             = $this->getRequest()->getParam('ood');
$email           = $this->getRequest()->getParam('email');
$is_officer      = $this->getRequest()->getParam('is_officer');
$date            = $this->getRequest()->getParam('date');
$month           = $this->getRequest()->getParam('month');
$year            = $this->getRequest()->getParam('year');
$tags            = $this->getRequest()->getParam('tags');
$title           = $this->getRequest()->getParam('title');
$company_id      = $this->getRequest()->getParam('company_id');
$need_approve    = $this->getRequest()->getParam('need_approve');
$off_date        = $this->getRequest()->getParam('off_date');
$indentity       = $this->getRequest()->getParam('indentity');
$joined_at       = $this->getRequest()->getParam('joined_at');
$off_type        = $this->getRequest()->getParam('off_type');
$off_date_from   = $this->getRequest()->getParam('off_date_from');
$off_date_to     = $this->getRequest()->getParam('off_date_to');
$is_approved     = $this->getRequest()->getParam('is_approved');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if ($tags and is_array($tags))
    $tags = $tags;
else
    $tags = null;

//check if export
$export = $this->getRequest()->getParam('export', 0);

if (!$sname)
    $limit = LIMITATION;
else
    $limit = null;

$total = 0;

$params = array_filter(array(
    'name' => $name,
    'department' => $department,
    'off' => $off,
    'team' => $team,
    'regional_market' => $regional_market,
    'district' => $district,
    'area_id' => $area_id,
    'note' => $note,
    'code' => $code,
    's_assign' => $s_assign,
    'ood' => $ood,
    'email' => $email,
    's_assign' => $s_assign,
    'ood' => $ood,
    'email' => $email,
    'date' => $date,
    'month' => $month,
    'year' => $year,
    'is_officer' => $is_officer,
    'sname' => $sname,
    'tags' => $tags,
    'title' => $title,
    'company_id' => $company_id,
    'export' => $export,
    'need_approve' => $need_approve,
    'indentity' => $indentity,
    'joined_at' => $joined_at,
    'off_date'  => $off_date,
    'off_type'  => $off_type,
    'off_date_from' => $off_date_from,
    'off_date_to' => $off_date_to,
    'is_approved' => $is_approved,
));

$params['sort'] = $sort;
$params['desc'] = $desc;

if (!in_array('default::staff::approve-basic', $userStorage->accesses))
    $params['date_off_purpose_update_by'] = $params_rejected['date_off_purpose_update_by'] = $userStorage->id;

$QArea = new Application_Model_Area();
$all_area =  $QArea->fetchAll(null, 'name');
$this->view->areas = $all_area;
$this->view->areas_all = $QArea->get_cache();

$QRegionalMarket = new Application_Model_RegionalMarket();
$this->view->regional_markets_all = $QRegionalMarket->get_cache_all();

$QStaffOffdateReason = new Application_Model_StaffDateoffReason();
$this->view->staff_dateoff_reason = $QStaffOffdateReason->get_cache();

if ($area_id)
{
    if (is_array($area_id) && count($area_id))
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

if ($regional_market)
{
    if (is_array($regional_market) && count($regional_market))
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}


$QStaff = new Application_Model_Staff();

if(isset($export) and $export)
{
    switch($export)
    {
        //report cho nhan su
        case 1 :
        {
            $staffs = $QStaff->offDatePurposeFetchPagination($page, null, $total, $params);
            $this->_exportDateOffCsv($staffs);
        }
    }
}


$staffs = $QStaff->offDatePurposeFetchPagination($page, $limit, $total, $params);

foreach($staffs as $k => $v)
{
    $v['date_off_purpose_update_by'] = ($v['date_off_purpose_update_by'] != '') ? $v['date_off_purpose_update_by'] : $v['staff_temp_created_by'];
    $where_staff_update_offdate_purpose = $QStaff->getAdapter()->quoteInto('id = ?', $v['date_off_purpose_update_by']);
    $staff_update_offdate_purpose = $staff = $QStaff->fetchRow($where_staff_update_offdate_purpose);

    $staffs[$k] = $v;
    $staffs[$k]['date_off_purpose_update_by_name'] = $staff_update_offdate_purpose['email'];
}

$QTeam = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

$QCompany = new Application_Model_Company();
$this->view->companies = $QCompany->get_cache();

$this->view->off_type = unserialize(OFF_TYPE);
$this->view->userStorage = $userStorage;

//Danh sach rejected
$params_rejected = array();

if (!in_array('default::staff::approve-basic', $userStorage->accesses))
    $params['date_off_purpose_update_by'] = $params_rejected['date_off_purpose_update_by'] = $userStorage->id;

$params_rejected['is_approved'] = '-1';
$staffs_rejected = $QStaff->offDatePurposeFetchPagination($page, null, $total, $params_rejected);
$this->view->staffs_rejected = $staffs_rejected;

$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->staffs = $staffs;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'staff/list-offdate-purpose/' . ($params ? '?' . http_build_query($params) .
        '&' : '?');

$this->view->offset = $limit * ($page - 1);

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages;

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

if ($this->getRequest()->isXmlHttpRequest())
{
    $this->_helper->layout->disableLayout();

    if ($sname)
    {
        $QRegion = new Application_Model_RegionalMarket();
        $regional_markets = $QRegion->fetchAll();

        $rm = array();
        foreach ($regional_markets as $key => $value)
        {
            $rm[$value['id']] = $value['name'];
        }
        $this->view->rm_list = $rm;

        $this->_helper->viewRenderer->setRender('partials/searchname');
    } elseif ($s_assign)
    {
        $this->_helper->viewRenderer->setRender('partials/staff');
    } else
        $this->_helper->viewRenderer->setRender('partials/list');
}