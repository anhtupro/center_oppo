<?php
$flashMessenger = $this->_helper->flashMessenger;
$messages_success = $flashMessenger->setNamespace('success')->getMessages();

// search bar
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$name = $this->getRequest()->getParam('name');
$department = $this->getRequest()->getParam('department');
$off = $this->getRequest()->getParam('off', 1);
$export = $this->getRequest()->getParam('export');
$team = $this->getRequest()->getParam('team');
$regional_market = $this->getRequest()->getParam('regional_market');
$district = $this->getRequest()->getParam('district');
$area_id = $this->getRequest()->getParam('area_id');
$note = $this->getRequest()->getParam('note');
$sname = $this->getRequest()->getParam('sname', 0);
$code = $this->getRequest()->getParam('code');
$s_assign = $this->getRequest()->getParam('s_assign');
$ood = $this->getRequest()->getParam('ood');
$email = $this->getRequest()->getParam('email');
$is_officer = $this->getRequest()->getParam('is_officer');
$date = $this->getRequest()->getParam('date');
$month = $this->getRequest()->getParam('month');
$year = $this->getRequest()->getParam('year');
$tags = $this->getRequest()->getParam('tags');
$title = $this->getRequest()->getParam('title');
$company_id = $this->getRequest()->getParam('company_id');
$limit = LIMITATION;
$page = $this->getRequest()->getParam('page', 1);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_title = $userStorage->title;

$QArea = new Application_Model_Area();
$QRegionalMarket = new Application_Model_RegionalMarket();
$QCompany = new Application_Model_Company();
$QTeam = new Application_Model_Team();
$QStaffTempNew = new Application_Model_StaffTempNew();
$QStaffTempNewStatus = new Application_Model_StaffTempNewStatus();
$QAsm = new Application_Model_Asm();
$QStaff = new Application_Model_Staff();

$params = array(
    'name' => $name,
    'department' => $department,
    'team' => $team,
    'regional_market' => $regional_market,
    'district' => $district,
    'area_id' => $area_id,
    'code' => $code,
    'email' => $email,
    'date' => $date,
    'title' => $title,
    'company_id' => $company_id,
);



if ($area_id) {
    if (is_array($area_id) && count($area_id))
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

if ($regional_market) {
    if (is_array($regional_market) && count($regional_market))
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

//end search

$listArea = $QAsm->getArea($userStorage->id);

//$listArea = $QStaff->getArea($userStorage->id);
$approve_type = $QStaffTempNewStatus->getApproveType($userStorage->title);

if (! in_array(1, $approve_type)) { // HR thấy tất cả danh sách nhân viên, sales admin chỉ thấy của khu vực
    if ($userStorage->department == DEPARTMENT_SALE) {
        $params['area_list'] = $listArea;
    } else {
        $params['exception'] = 1; // cho Logistic, CALL CENTER, SERVICE
    }
}

$params ['approve_type'] = $approve_type;

if ($params ['approve_type']) {
    $listUpdateInfor = $QStaffTempNew->fetchPagination($page, $limit, $total, $params);
}

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$this->view->companies = $QCompany->get_cache();
$this->view->listUpdateInfor = $listUpdateInfor;
$this->view->messages_success = $messages_success;
$this->view->areas = $QArea->getListArea($params);
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'staff/list-update-infor' . ($params ? '?' . http_build_query($params) . '&' : '?');

$flashMessenger = $this->_helper->flashMessenger;
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$this->view->messages_error = $messages_error;
