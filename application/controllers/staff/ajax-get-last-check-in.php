<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    
    $staff_id = $this->getRequest()->getParam('staff_id',null);
    $date = $this->getRequest()->getParam('date',null);
    $date = date("Y-m-d", strtotime($date));
    $db = Zend_Registry::get("db");
    $stmt = $db->prepare("CALL `PR_get_last_check_in`(:p_staff_id,:p_date)");
    $stmt->bindParam('p_staff_id', $staff_id, PDO::PARAM_STR);
    $stmt->bindParam('p_date', $date, PDO::PARAM_STR);
    $stmt->execute();
    $result = $stmt->fetch();
    $stmt->closeCursor();
    print_r(json_encode($result));

