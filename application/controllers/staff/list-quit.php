<?php

  // LIST QUIT
  $QTeam    = new Application_Model_Team();
  $QStaff   = new Application_Model_Staff();
  $QArea    = new Application_Model_Area();

  $export           = $this->getRequest()->getParam('export', 0);
  $name             = $this->getRequest()->getParam('name');
  $code             = $this->getRequest()->getParam('code');
  $offdate_file     = $this->getRequest()->getParam('offdate_file');
  $off_type         = $this->getRequest()->getParam('off_type');
  $off              = $this->getRequest()->getParam('off', 1);
  $area_id          = $this->getRequest()->getParam('area_id');
  $department       = $this->getRequest()->getParam('department');
  $team             = $this->getRequest()->getParam('team');
  $regional_market  = $this->getRequest()->getParam('regional_market');
  $title            = $this->getRequest()->getParam('title');
  $off_date_from    = $this->getRequest()->getParam('off_date_from');
  $off_date_to      = $this->getRequest()->getParam('off_date_to');

  $params = array_filter(array(
    'name'            => $name,
    'code'            => $code,
    'export'          => $export,
    'offdate_file'    => $offdate_file,
    'off_type'        => $off_type,
    'area_id'         => $area_id,
    'department'      => $department,
    'team'            => $team,
    'regional_market' => $regional_market,
    'title'           => $title,
    'off_date_from'   => $off_date_from,
    'off_date_to'     => $off_date_to
  ));

  $userStorage = Zend_Auth::getInstance()->getStorage()->read();
  $this->view->area_visible = true;
  $title           = $this->getRequest()->getParam('title');

  if (in_array(
    $userStorage->title,
    array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM , SALE_SALE_ASM_STANDBY, 308, SALES_LEADER_TITLE)
    ) OR $userStorage->group_id == SALES_ADMIN_ID) {
      $QAsm = new Application_Model_Asm();
      $cachedASM = $QAsm->get_cache($userStorage->id);
      $tem = $cachedASM['province'];

      if ($tem)
        $list_province_ids = $tem;
      else
        $list_province_ids = -1;

      // doi mau report cho asm
//      $export = $export ?  2 : '';

      $params['list_province_ids'] = $list_province_ids;
    }
    // end

  $page     = $this->getRequest()->getParam('page',1);
  $limit    = LIMITATION;
  $total    = 0;

  if( $export == 1){
    $staffs = $QStaff->fetchPaginationQuit($page, null, $total, $params);
    $this->_exportXlsxASM($staffs);
  }

  $staffs = $QStaff->fetchPaginationQuit($page, $limit, $total, $params);
  $all_area =  $QArea->fetchAll(null, 'name');
  $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

  $this->view->areas    = $all_area;
  $this->view->params   = $params;
  $this->view->staffs   = $staffs;
  $this->view->limit    = $limit;
  $this->view->total    = $total;
  $this->view->offset   = $limit * ($page - 1);
  $this->view->url      = HOST . 'staff/list-quit' . ($params ? '?' . http_build_query($params) .'&' : '?');
  $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
