<?php

$staff_id   = $this->getRequest()->getParam('staff_id');

if($staff_id){
    
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
        $Staff = $QStaff->fetchRow($where);
        $this->view->Staff = $Staff;
        
        $this->view->ScoresCourse = $QStaff->getScoresCourse($staff_id);
        $this->view->ScoresWarning = $QStaff->getScoresWarning($staff_id);
        $this->view->attendanceDetail = $QStaff->getAttendanceDetail($staff_id);
        $this->view->KPIdetail = $QStaff->getKPIdetail($staff_id);
        
//        $tom = $QStaff->getScoresWarning($staff_id);
//     	echo "<pre>";
//	print_r($tom);
//	echo "</pre>";  \

        // get List store
	    $db = Zend_Registry::get('db');
	    $arrCols = array(
	        's.name',
	        's.id AS store_id',
	        'p.joined_at',
	        'p.released_at',
	        'level' => 'y.name',
	        'd.is_ka'
	    );
	    $select = $db->select()
	        ->from(array('p'=>'store_staff_log'), $arrCols)
	        ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
	        ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 's.d_id = d.id',array())
	        ->joinLeft(array('l' => 'dealer_loyalty'),'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)',array())
	        ->joinLeft(array('y'=> 'loyalty_plan'), 'l.loyalty_plan_id = y.id',array())
	        ->where("p.staff_id = ?", $staff_id)
	        ->order('p.id DESC')
	    ;

	    $transfer = $db->fetchAll($select);
	    $this->view->resultStaffTransfer = $transfer;
}  	