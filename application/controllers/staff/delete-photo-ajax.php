<?php

/* @thaisang.nguyen
* 
*
*/


$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
if($this->getRequest()->isXmlHttpRequest())
{
    $staff_id = $this->getRequest()->getPost('id_delete_photo');
    
    if(isset($staff_id) && !empty($staff_id) && is_numeric($staff_id))
    {
        $QStaff = new Application_Model_Staff();
        $staff_update = $QStaff->find($staff_id);
        
        $params = array();

        $params['old_name'] = $staff_update[0]->photo;
        $params['staff_id'] = $staff_update[0]->id;
        $params['new_name'] = '';

        $res = $QStaff->updatePhoto($params);
        if($res)
        {
            echo json_encode(array("status" => true, "message" => "Xóa ảnh thành công"));
        }
        else
        {
            echo json_encode(array("status" => false, "message" => "Xóa ảnh thành công"));
        }
        
    }
}