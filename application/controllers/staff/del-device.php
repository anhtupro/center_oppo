<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$id = $this->getRequest()->getParam('id');

$QCheckInMachineMap = new Application_Model_CheckInMachineMap();

$response = 0;
if(!empty($id) && is_numeric($id)){
    $where = array();
    $where[] = $QCheckInMachineMap->getAdapter()->quoteInto('id = ?', $id);
    $result = $QCheckInMachineMap->delete($where);
    $response = (int)$result >= 1 ? 1 : 0;
}
$response = [
    'status' => $response,
    'message' => $response == 1 ? 'Thành công' : 'Thất bại'
];
echo json_encode($response);
exit();