<?php
  $sort            = $this->getRequest()->getParam('sort', '');
        $desc            = $this->getRequest()->getParam('desc', 1);

        $page            = $this->getRequest()->getParam('page', 1);
        $name            = $this->getRequest()->getParam('name');
        $department      = $this->getRequest()->getParam('department');
        $off             = $this->getRequest()->getParam('off', 1);
        $team            = $this->getRequest()->getParam('team');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $area_id         = $this->getRequest()->getParam('area_id');
        $note            = $this->getRequest()->getParam('note');
        $sname           = $this->getRequest()->getParam('sname', 0);
        $sname_width     = $this->getRequest()->getParam('span','span6');
        $code            = $this->getRequest()->getParam('code');
        $s_assign        = $this->getRequest()->getParam('s_assign');
        $ood             = $this->getRequest()->getParam('ood');
        $email           = $this->getRequest()->getParam('email');
        $is_officer      = $this->getRequest()->getParam('is_officer');
        $date            = $this->getRequest()->getParam('date');
        $month           = $this->getRequest()->getParam('month');
        $year            = $this->getRequest()->getParam('year');
        $tags            = $this->getRequest()->getParam('tags');
        $title           = $this->getRequest()->getParam('title');
        $company_id      = $this->getRequest()->getParam('company_id');
        $need_approve    = $this->getRequest()->getParam('need_approve');
        $off_date        = $this->getRequest()->getParam('off_date');
        $indentity       = $this->getRequest()->getParam('indentity');
        $joined_at       = $this->getRequest()->getParam('joined_at');
        $off_type        = $this->getRequest()->getParam('off_type');
        $is_print        = $this->getRequest()->getParam('is_print');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if ($tags and is_array($tags))
            $tags = $tags;
        else
            $tags = null;

        //check if export
        $export = $this->getRequest()->getParam('export', 0);

        if (!$sname)
            $limit = LIMITATION;
        else
            $limit = null;

        $total = 0;

        $params = array_filter(array(
            'name' => $name,
            'department' => $department,
            'off' => $off,
            'team' => $team,
            'regional_market' => $regional_market,
            'district' => $district,
            'area_id' => $area_id,
            'note' => $note,
            'code' => $code,
            's_assign' => $s_assign,
            'ood' => $ood,
            'email' => $email,
            's_assign' => $s_assign,
            'ood' => $ood,
            'email' => $email,
            'date' => $date,
            'month' => $month,
            'year' => $year,
            'is_officer' => $is_officer,
            'sname' => $sname,
            'tags' => $tags,
            'title' => $title,
            'company_id' => $company_id,
            'export' => $export,
            'need_approve' => $need_approve,
            'indentity' => $indentity,
            'joined_at' => $joined_at,
            'off_date'  => $off_date,
            'off_type'  => $off_type,
            'is_print'  => $is_print,
         ));

         // danh sach cac title dc nhin thay boi training
         $array_title_by_traning = array(CHUYEN_VIEN_BAN_HANG_TITLE, PGPB_TITLE, TRAINING_TEAM, TRADE_TEAM);
        if($userStorage->group_id == TRAINING_TEAM_ID && (empty($title) || !in_array($title, $array_title_by_traning)))
        {
            $params['title'] = $array_title_by_traning;
        }

        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;

        $QRegionalMarket = new Application_Model_RegionalMarket();

        if ($area_id)
        {
            if (is_array($area_id) && count($area_id))
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

            $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
        }

        if ($regional_market)
        {
            if (is_array($regional_market) && count($regional_market))
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

            $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
        }

        /**
         * @author buu.pham
         * lọc staff thuộc khu vực nếu các title sau xem
         */
        if (in_array(
            $userStorage->title,
            array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM , SALE_SALE_ASM_STANDBY)
        )) {
            $QAsm = new Application_Model_Asm();
            $cachedASM = $QAsm->get_cache($userStorage->id);
            $tem = $cachedASM['province'];
            if ($tem)
                $list_province_ids = $tem;
            else
                $list_province_ids = -1;
            // doi mau report cho asm
            $export = $export ?  2 : '';

            $params['list_province_ids'] = $list_province_ids;
        }
        // end

        $QStaff = new Application_Model_Staff();

        if(isset($export) and $export)
        {
            switch($export)
            {
                //report cho nhan su
                case 1 :
                {
                    $staffs = $QStaff->fetchPagination($page, null, $total, $params);
                    $this->_exportCsv($staffs);
                }
                //report cho asm
                case 2:
                {
                    $staffs = $QStaff->fetchPagination($page, null, $total, $params);
                    $this->_exportCsvASM($staffs);
                }
            }
        }




        $staffs = $QStaff->fetchPagination($page, $limit, $total, $params);

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $QCompany = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $this->view->off_type = unserialize(OFF_TYPE);
        $this->view->userStorage = $userStorage;

        $this->view->desc = $desc;
        $this->view->sort = $sort;
        $this->view->staffs = $staffs;
        $this->view->params = $params;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'staff/' . ($params ? '?' . http_build_query($params) .
            '&' : '?');

        $this->view->offset = $limit * ($page - 1);

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages;

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

        if ($this->getRequest()->isXmlHttpRequest())
        {
            $this->_helper->layout->disableLayout();

            if ($sname)
            {
                $QRegion = new Application_Model_RegionalMarket();
                $regional_markets = $QRegion->fetchAll();

                $rm = array();
                foreach ($regional_markets as $key => $value)
                {
                    $rm[$value['id']] = $value['name'];
                }
                $this->view->rm_list = $rm;
                $this->view->span = $sname_width;
                $this->_helper->viewRenderer->setRender('partials/searchname');
            } elseif ($s_assign)
            {
                $this->_helper->viewRenderer->setRender('partials/staff');
            } else
                $this->_helper->viewRenderer->setRender('partials/list');
        }