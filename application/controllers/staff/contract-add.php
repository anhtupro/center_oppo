<?php 
$QStaff       = new Application_Model_Staff();
$QSalaryPG    = new Application_Model_SalaryPg();
$QSalarySales = new Application_Model_SalarySales();
$QSalaryLog   = new Application_Model_SalaryLog();
$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$staff_rowset = $QStaff->find($staff_id);
$staff        = $staff_rowset->current();
$title        = $staff['title'];
$data         = array();
$db           = Zend_Registry::get('db');
try
{
    
    if (My_Staff::isPgTitle($title))
    {
        $data['contract_term']       = CONTRACT_TERM_SEASONAL;
        $data['contract_signed_at']  = date("Y-m-d", strtotime($joined_at . "+ 0 days"));
        $data['contract_expired_at'] = date("Y-m-d", strtotime($joined_at . "+ 84 days"));
    }else{
        $data['contract_term'] = CONTRACT_TERM_LABOUR;
        $data['contract_signed_at'] = date("Y-m-d", strtotime($joined_at . "+ 0 days"));

        if($title == NHAN_VIEN_KIEM_HANG_TITLE){
            $data['contract_expired_at'] = date("Y-m-d", strtotime($joined_at . "+ 29 days"));
        }elseif($title == PB_SALES_TITLE){
            $data['contract_expired_at'] = date("Y-m-d", strtotime($joined_at . "+ 59 days"));
        }else{
            $data['contract_expired_at'] = date("Y-m-d", strtotime($joined_at . "+ 59 days"));
        }
    
    }
    //luu log contract

    $where = array();
    $where[] = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
    $QStaff->update($data, $where);

    if ($staff['title'] == SALES_TITLE || $staff['title'] == SALES_ACCESSORIES_TITLE)
    {
        $where = array();
        $where[] = $QSalarySales->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
        $luong = $QSalarySales->fetchRow($where);
    }

    //pg team
    if (My_Staff::isPgTitle($staff['title']))
    {
        $where = array();
        $where[] = $QSalaryPG->getAdapter()->quoteInto('province_id = ?', $staff['regional_market']);
        $luong = $QSalaryPG->fetchRow($where);
    }

    if($staff['title'] == PB_SALES_TITLE){
       $luong = $QSalaryLog->getSalaryLogTitle($staff['title'],$data['contract_signed_at'],$staff['regional_market']);
    }

    //Thêm vào bảng hợp đồng
    $QAsmContract = new Application_Model_AsmContract();
    $data_contract = array(
        'company_id'      => $staff['company_id'],
        'staff_id'        => $staff['id'],
        'from_date'       => $data['contract_signed_at'],
        'to_date'         => $data['contract_expired_at'],
        'title'           => $staff['title'],
        'regional_market' => $staff['regional_market'],
        'new_contract'    => $data['contract_term'],
        'salary'          => (isset($luong['base_salary']) and $luong['base_salary']) ? $luong['base_salary'] : 0,
        'print_type'      => 1,
        'created_at'      => date('Y-m-d H:i:s'),
        'created_by'      => $userStorage->id
    );
    $QAsmContract->save($data_contract);

    //successful
    return array('code' => 1, 'message' => 'success');
}catch (exception $e){
    return array('code' => -1, 'message' => $e->getMessage());
}
?>