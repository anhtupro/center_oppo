<?php
	$id              			= $this->getRequest()->getParam('id');
	$contract_id              	= $this->getRequest()->getParam('contract_id');
	$QContracFileLog 			= new Application_Model_ContractFileLog();
	$QStafContract 				= new Application_Model_StaffContract();

	$created_at = date('Y-m-d H:i:s');
	$total_frond 		= count($_FILES['file_contract_frond']['name']);
	$total_back		= count($_FILES['file_contract_back']['name']);

	$url_frond = [];
	$url_back  = [];
		for($i=0; $i<$total_frond; $i++){
			if($_FILES['file_contract_frond']['name'][$i]!=""){
				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
								DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
								DIRECTORY_SEPARATOR . 'staff' .DIRECTORY_SEPARATOR . 'contract-file-log' . DIRECTORY_SEPARATOR . $id;
				if (!is_dir($uploaded_dir))
				@mkdir($uploaded_dir, 0777, true);
				$tmpFilePath = $_FILES['file_contract_frond']['tmp_name'][$i];
				if ($tmpFilePath != ""){
					$old_name 	= $_FILES['file_contract_frond']['name'][$i];
					$tExplode 	= explode('.', $old_name);
					$extension  = end($tExplode);
					$new_name = 'LOG-FILE-CONTRACT-' . md5(uniqid('', true)) . '.' . $extension;
					$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
						if(move_uploaded_file($tmpFilePath, $newFilePath)) {
							$url    = 'files' .
									DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR. 'contract-file-log' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
						}else{
							$url = NULL;
						}
				}else{
					$url = NULL;
				}
		}		
		$url_frond[] = $url;
	}


	/////BACK IMAGE
for($i=0; $i<$total_back; $i++){
			if($_FILES['file_contract_back']['name'][$i]!=""){
				$uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
								DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
								DIRECTORY_SEPARATOR . 'staff' .DIRECTORY_SEPARATOR . 'contract-file-log' . DIRECTORY_SEPARATOR . $id;
				if (!is_dir($uploaded_dir))
				@mkdir($uploaded_dir, 0777, true);
				$tmpFilePath = $_FILES['file_contract_back']['tmp_name'][$i];
				if ($tmpFilePath != ""){
					$old_name 	= $_FILES['file_contract_back']['name'][$i];
					$tExplode 	= explode('.', $old_name);
					$extension  = end($tExplode);
					$new_name = 'LOG-FILE-CONTRACT-' . md5(uniqid('', true)) . '.' . $extension;
					$newFilePath = $uploaded_dir. DIRECTORY_SEPARATOR .$new_name;
						if(move_uploaded_file($tmpFilePath, $newFilePath)) {
							$url    = 'files' .
									DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR. 'contract-file-log' . DIRECTORY_SEPARATOR . $id. DIRECTORY_SEPARATOR .$new_name;
						}else{
							$url = NULL;
						}
				}else{
					$url = NULL;
				}

				// $details = [
				// 	'staff_id' => $id,
				// 	'url'  => $url,
				// 	'type'	   => 1,
				// 	'file_name'=>$new_name,
				// 	'created_at' => $created_at,
				// 	'contract_id'	=>$contract_id[$i],
				// 	'stt'		=> 0
				// ];
				// $QContracFileLog->insert($details);
			
		}		
		$url_back[] = $url;
	}

	// foreach ($contract_id as $key => $value) {
	// 	$where_update = [];
	// 			$where_update[] = $QStafContract->getAdapter()->quoteInto('id = ?', $value);
	// 			$where_update[] = $QStafContract->getAdapter()->quoteInto('staff_id = ?', $id);

	// 			$data_update=array(
	// 				'img_contract_frond'      =>$url_frond[$key],
	// 				'img_contract_back'		  =>$url_back[$key],
	// 				'is_approved'			  =>0
	// 			);

	// 			$QStafContract->update($data_update,$where_update);
	// }
		$total_contract = count($contract_id); 
			for($i=0 ;$i<$total_contract;$i++){
				$where_update = [];
				$where_update[] = $QStafContract->getAdapter()->quoteInto('id = ?', $contract_id[$i]);
				$where_update[] = $QStafContract->getAdapter()->quoteInto('staff_id = ?', $id);

				$data_update=array(
					'img_contract_frond'      =>$url_frond[$i],
					'img_contract_back'		  =>$url_back[$i],
					'upload_letter'			  => $created_at,
					'is_approved'			  =>0
				);

				$QStafContract->update($data_update,$where_update);
			}
			
		$this->redirect(HOST.'staff/view-t?id='.$id);