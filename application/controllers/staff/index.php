<?php

$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);

$page            = $this->getRequest()->getParam('page', 1);
$name            = $this->getRequest()->getParam('name');
$department      = $this->getRequest()->getParam('department');
$off             = $this->getRequest()->getParam('off', 1);
$team            = $this->getRequest()->getParam('team');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$area_id         = $this->getRequest()->getParam('area_id');
$note            = $this->getRequest()->getParam('note');
$sname           = $this->getRequest()->getParam('sname', 0);
$sname_width     = $this->getRequest()->getParam('span', 'span6');
$code            = $this->getRequest()->getParam('code');
$s_assign        = $this->getRequest()->getParam('s_assign');
$ood             = $this->getRequest()->getParam('ood');
$email           = $this->getRequest()->getParam('email');
$is_officer      = $this->getRequest()->getParam('is_officer');
$date            = $this->getRequest()->getParam('date');
$month           = $this->getRequest()->getParam('month');
$year            = $this->getRequest()->getParam('year');
$tags            = $this->getRequest()->getParam('tags');
$title           = $this->getRequest()->getParam('title');
$company_id      = $this->getRequest()->getParam('company_id');
$need_approve    = $this->getRequest()->getParam('need_approve');
$off_date        = $this->getRequest()->getParam('off_date');
$indentity       = $this->getRequest()->getParam('indentity');
$joined_at       = $this->getRequest()->getParam('joined_at');
$off_type        = $this->getRequest()->getParam('off_type');
$is_print        = $this->getRequest()->getParam('is_print');
$off_date_from   = $this->getRequest()->getParam('off_date_from');
$off_date_to     = $this->getRequest()->getParam('off_date_to');
$joined_at_from  = $this->getRequest()->getParam('joined_at_from');
$joined_at_to    = $this->getRequest()->getParam('joined_at_to');
$is_no_body      = $this->getRequest()->getParam('is_no_body');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();



if ($tags and is_array($tags))
    $tags = $tags;
else
    $tags = null;

//check if export
$export     = $this->getRequest()->getParam('export', 0);
//check if export relative person lcdanh
$export_rlt = $this->getRequest()->getParam('export_rlt', 0);


if (!$sname)
    $limit = LIMITATION;
else
    $limit = null;

$total = 0;

$params = array_filter(array(
    'name'            => $name,
    'department'      => $department,
    'off'             => $off,
    'team'            => $team,
    'regional_market' => $regional_market,
    'district'        => $district,
    'area_id'         => $area_id,
    'note'            => $note,
    'code'            => $code,
    's_assign'        => $s_assign,
    'ood'             => $ood,
    'email'           => $email,
    's_assign'        => $s_assign,
    'ood'             => $ood,
    'email'           => $email,
    'date'            => $date,
    'month'           => $month,
    'year'            => $year,
    'is_officer'      => $is_officer,
    'sname'           => $sname,
    'tags'            => $tags,
    'title'           => $title,
    'company_id'      => $company_id,
    'export'          => $export,
    'need_approve'    => $need_approve,
    'indentity'       => $indentity,
    'joined_at'       => $joined_at,
    'off_date'        => $off_date,
    'off_type'        => $off_type,
    'is_print'        => $is_print,
    'off_date_from'   => $off_date_from,
    'off_date_to'     => $off_date_to,
    'joined_at_from'  => $joined_at_from,
    'joined_at_to'    => $joined_at_to,
    'is_no_body'      => $is_no_body
        ));

$params['sort'] = $sort;
$params['desc'] = $desc;

$QArea             = new Application_Model_Area();
$where             = $QArea->getAdapter()->quoteInto('del = ?', 0);
$all_area          = $QArea->fetchAll($where, 'name');
$this->view->areas = $all_area;

$QRegionalMarket = new Application_Model_RegionalMarket();
$QStaff          = new Application_Model_Staff();
$QTeam           = new Application_Model_Team();
$staff           = $QStaff->find($userStorage->id);
$staff           = $staff->current();

$title    = $staff['title'];
$team     = $QTeam->find($title);
$team     = $team->current();
$group_id = $team['access_group'];
if ($area_id) {
    if (is_array($area_id) && count($area_id))
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

if ($regional_market) {
    // echo $regional_market;die;
    if (is_array($regional_market) && count($regional_market))
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
    else
        $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

    // $QOffice = new Application_Model_Office();
    // $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
    // echo "<pre>";print_r($QRegionalMarket->fetchAll($where, 'name'));die;
}
/**
 * @author buu.pham
 * lọc staff thuộc khu vực nếu các title sau xem
 */
if (($group_id == SALES_EXT_ID ) || in_array($userStorage->id, array(240, 12719, 2123)) || in_array($userStorage->title, [621,TRADE_MARKETING_ASSISTANT])) { // 2123: dieu.le
    if ($group_id == SALES_EXT_ID) {
        $params['department'] = array();
        $params['team']       = array(TEAM_SALE_BRAND_SHOP, SALES_TEAM, TEAM_TRADE_MARKETING, TEAM_SALE_TRADE_MARKETING, TRAINING_TEAM, TEAM_SALE_TRAINING, TEAM_RETAIL_BRAND_SHOP);
    }
    if ($userStorage->id == 341) { // chau.phan
        $list_team_brand = $QTeam->getTeamByDepartment(BRAND_SHOP_DEPT);
        $list_team_trade = $QTeam->getTeamByDepartment(TRADE_MARKETING_DEPT);
        $list_team_traning = $QTeam->getTeamByDepartment(TRANING_DEPT);
        $list_merge_main = [];
        foreach ($list_team_brand as $key=>$value){
            $list_merge_main[] = $key;
        }
        foreach ($list_team_trade as $key=>$value){
            $list_merge_main[] = $key;
        }
        foreach ($list_team_traning as $key=>$value){
            $list_merge_main[] = $key;
        }
        $params['team'] = array_merge($params['team'],$list_merge_main);
    }
    if ($userStorage->id == 24) { // lien.pham
        $params['department'] = array(DEPARTMENT_SALE, DEPARTMENT_RETAIL, 557);
         $params['team']    =[];
    }
    if (in_array($userStorage->id, array(12719))) { //thuydung.lethi
        $params['department'] = array(BRAND_SHOP_DEPT,DEPARTMENT_SALE, DEPARTMENT_RETAIL);
        $params['team'] = array(BRANDSHOP_TEAM,TEAM_SALE_BRAND_SHOP,TEAM_RETAIL_BRAND_SHOP);
    }
    if (in_array($userStorage->id, array(2123))) { //dieu.le
        $params['department'] = array(BRAND_SHOP_DEPT,DEPARTMENT_SALE, DEPARTMENT_RETAIL);
        $params['team'] = array(BRANDSHOP_TEAM,TEAM_SALE_BRAND_SHOP,TEAM_RETAIL_BRAND_SHOP);
    }
    if ($userStorage->id == 240) {
        $params['team']       = array();
        $params['department'] = array($userStorage->department, DEPARTMENT_SALE, TRANING_DEPT , TRADE_MARKETING_DEPT); //152 sale
    } 
   
    if($userStorage->title == 621){
        $params['department'] = array(DEPARTMENT_RETAIL,TRANING_DEPT, DEPARTMENT_SALE);
        $params['team'] = array(TEAM_SALE_TRAINING,TRAINING_TEAM,TEAM_TRAINING_TRAINING);
    }
    
    if($userStorage->title == TRADE_MARKETING_ASSISTANT){
        $params['department'] = array(DEPARTMENT_RETAIL,TRADE_MARKETING_DEPT, DEPARTMENT_SALE);
        $params['team'] = array(TEAM_SALE_TRADE_MARKETING,TEAM_TRADE_MARKETING,TEAM_TRADEMARKETING);
    }
//    else {
//        $params['team']       = array();
//        $params['department'] = array($userStorage->department);
//    }
} else if (in_array(
                $userStorage->title, array(WESTERN_SALE_ANAGER, 308, 471, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM, SALE_SALE_ASM_STANDBY, 535, 465, 463, 392, 391, 467, 464, 471, 463, 466, 536)
        ) OR ( $group_id == SALES_ADMIN_ID || $group_id == ASM_ID)) {
    $QAsm              = new Application_Model_Asm();
    $cachedASM         = $QAsm->get_cache($userStorage->id);
    $tem               = $cachedASM['province'];
    if ($tem)
        $list_province_ids = $tem;
    else
        $list_province_ids = -1;
    // doi mau report cho asm
    $export            = $export ? 2 : '';

    $params['list_province_ids'] = $list_province_ids;
}else if (!in_array($group_id, array(1, 7, 8, 644))) {
    if (!empty($department) && in_array(321, $department)) {
        $params['department'] = array($userStorage->department, DEPARTMENT_OTHER);
    } else {
        
        $params['department'] = array($userStorage->department);
        if($userStorage->department == TRADE_MARKETING_DEPT){
            $params['department'] = array($userStorage->department,DEPARTMENT_SALE,DEPARTMENT_RETAIL);
            $params['team'] = array($userStorage->team,TEAM_SALE_TRADE_MARKETING,TEAM_TRADE_MARKETING);
        }
        if($userStorage->department == BRAND_SHOP_DEPT){
            $params['department'] = array($userStorage->department,DEPARTMENT_SALE,DEPARTMENT_RETAIL);
            $params['team'] = array($userStorage->team,TEAM_SALE_BRAND_SHOP,TEAM_RETAIL_BRAND_SHOP);
        }
        if($userStorage->department == TRANING_DEPT){
            $params['department'] = array($userStorage->department,DEPARTMENT_SALE,DEPARTMENT_RETAIL);
            $params['team'] = array($userStorage->team,TEAM_SALE_TRAINING,TRAINING_TEAM);
        }

        //minhtam.pham
        if (in_array($userStorage->id, array(4617, 24644))) {

            $params['department'] = array(SERVICE_CENTER);
        }
        
        if (in_array($userStorage->id, array(23003))) {
            $params['department'][] = 525;
        }
        
        if (in_array($userStorage->id, array(23154))) {//dieuhoa.truong
            $params['department'][] = DEPARTMENT_SALE;
            $params['team'][] = TEAM_SALE_SALE;
        }
    }
}
//        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//$user_id     = $userStorage->id;
//if ($user_id == 5899) {
//    echo "<pre>";
//    print_r($params);
//    die;
//}
// end
if (isset($export) and $export) {


    if (in_array($group_id, array(ADMINISTRATOR_ID, HR_ID))) {
        $staffs = $QStaff->fetchPagination($page, null, $total, $params);
//                $this->_exportCsv($staffs);
        $this->_exportxlsxNew($staffs);
    } else {
        $staffs = $QStaff->fetchPagination($page, null, $total, $params);
        $this->_exportxlsxASMNew($staffs);
    }
    /*
      switch($export)
      {
      //report cho nhan su
      case 1 :
      {
      $staffs = $QStaff->fetchPagination($page, null, $total, $params);
      $this->_exportCsv($staffs);
      }
      //report cho asm
      case 2:
      {
      $staffs = $QStaff->fetchPagination($page, null, $total, $params);
      $this->_exportCsvASM($staffs);
      }
      }
     */
}

//export thong tin nguoi than (Danh)
if (isset($export_rlt) and $export_rlt) {

    //get list staff id

    $listStaffId = array();
    $staffs      = $QStaff->fetchPagination($page, null, $total, $params);

    foreach ($staffs as $staff) {
        $listStaffId[] = $staff['id'];
    }

    $QStaffRelative = new Application_Model_StaffRelative();

    $data = $QStaffRelative->exportByStaffId($listStaffId);


    // no limit time
    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'STT',
        'Staff Code',
        'Staff Name',
        'Department',
        'Team',
        'Title',
        'Area',
//                'Province',
//                'District',
        'Join At',
        'Off Date',
        'Relative Type',
        'Name',
        'Gender',
        'Householder',
        'Birth Year',
        'Day Of Birth',
        'Job',
        'Word Place',
        'Id Card Number'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;


    try {
        if ($data)
            foreach ($data as $_key => $_order) {

                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['department'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['team'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['title'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['joined_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['off_date'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['relative_type'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['rlt_full_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['gender'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(($_order['owner_house'] == 1?'X':''), PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['birth_year'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['job'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['work_place'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['id_card_number'], PHPExcel_Cell_DataType::TYPE_STRING);


                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename  = 'Report_Relative_Staff_Person_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}

//end export thong tin nguoi than
//get regional markets
$QRegionalMarket              = new Application_Model_RegionalMarket();
$regional_markets             = $QRegionalMarket->get_cache_all();
$this->view->regional_markets = $regional_markets;

$QArea                  = new Application_Model_Area();
$area_cache             = $QArea->get_cache();
$this->view->area_cache = $area_cache;

if ($_GET['dev']) {
    echo "<pre>";
    print_r($params);
}
$staffs = $QStaff->fetchPagination($page, $limit, $total, $params);

$QTeam                       = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();


//phan quyen exporrt relative person cho HR va Admin
if (in_array($group_id, array(ADMINISTRATOR_ID, HR_ID))) {
    $showExportRelative = true;
} else {
    $showExportRelative = false;
}

$this->view->showExportRelative = $showExportRelative;

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

$QCompany                = new Application_Model_Company();
$this->view->companies   = $QCompany->get_cache();
// $this->view->test = 123123;
$this->view->off_type    = unserialize(OFF_TYPE);
$this->view->userStorage = $userStorage;

$this->view->desc   = $desc;
$this->view->sort   = $sort;
$this->view->staffs = $staffs;
$this->view->params = $params;
$this->view->limit  = $limit;
$this->view->total  = $total;
$teams              = $QTeam->get_cache();
$this->view->teams  = $teams;
$this->view->url    = HOST . 'staff/' . ($params ? '?' . http_build_query($params) .
                '&' : '?');

$this->view->offset = $limit * ($page - 1);

$flashMessenger               = $this->_helper->flashMessenger;
$messages                     = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages;

$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

if ($this->getRequest()->isXmlHttpRequest()) {
    $this->_helper->layout->disableLayout();

    if ($sname) {
        $QRegion          = new Application_Model_RegionalMarket();
        $regional_markets = $QRegion->fetchAll();

        $rm = array();
        foreach ($regional_markets as $key => $value) {
            $rm[$value['id']] = $value['name'];
        }
        $this->view->rm_list = $rm;
        $this->view->span    = $sname_width;
        $this->_helper->viewRenderer->setRender('partials/searchname');
    } elseif ($s_assign) {
        $this->_helper->viewRenderer->setRender('partials/staff');
    } else
        $this->_helper->viewRenderer->setRender('partials/list');
}