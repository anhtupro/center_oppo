<?php

$back_url = $this->getRequest()->getParam('back_url');

if ($this->getRequest()->getMethod() == 'POST') {
    $QStaff               = new Application_Model_Staff();
    $QNotificationTrainer = new Application_Model_NotificationTrainer();
    $flashMessenger       = $this->_helper->flashMessenger;

    $id                = $this->getRequest()->getParam('id');
    $del_photo         = $this->getRequest()->getParam('del_photo', 0);
    $del_id_photo      = $this->getRequest()->getParam('del_id_photo', 0);
    $del_id_photo_back = $this->getRequest()->getParam('del_id_photo_back', 0);

    $tags                = $this->getRequest()->getParam('tags');
    $code                = $this->getRequest()->getParam('code');
    $contract_type       = $this->getRequest()->getParam('contract_type');
    $contract_signed_at  = $this->getRequest()->getParam('contract_signed_at');
    $contract_term       = $this->getRequest()->getParam('contract_term');
    $contract_expired_at = $this->getRequest()->getParam('contract_expired_at');
    $print_time          = $this->getRequest()->getParam('print_time');
    $department          = $this->getRequest()->getParam('department');
    $team                = $this->getRequest()->getParam('team');
//    $firstname           = $this->getRequest()->getParam('firstname');
    $full_name           = $this->getRequest()->getParam('full_name');
//    $lastname            = $this->getRequest()->getParam('lastname');




    $title                 = $this->getRequest()->getParam('title');
    $phone_number          = $this->getRequest()->getParam('phone_number');
    $joined_at             = $this->getRequest()->getParam('joined_at');
    $off_date              = $this->getRequest()->getParam('off_date');
    $off_type              = $this->getRequest()->getParam('off_type', null);
    $gender                = $this->getRequest()->getParam('gender');
    $office_id             = $this->getRequest()->getParam('office_id');
    $array_name            = //$certificate         = $this->getRequest()->getParam('certificate');
            //education
            $levels                = $this->getRequest()->getParam('levels');
    // $levels_id          = $this->getRequest()->getParam('levels_id');
    $schools               = $this->getRequest()->getParam('school');
    $field_of_studys       = $this->getRequest()->getParam('field_of_study');
    $graduated_years       = $this->getRequest()->getParam('graduated_year');
    $grades                = $this->getRequest()->getParam('grade');
    $mode_of_studys        = $this->getRequest()->getParam('mode_of_study');
    $education_ids         = $this->getRequest()->getParam('education_id');
    $default_level         = $this->getRequest()->getParam('default_level');
    //experience
    $ex_experience_id      = $this->getRequest()->getParam('ex_experience_id');
    $ex_company_name       = $this->getRequest()->getParam('ex_company_name');
    $ex_job_position       = $this->getRequest()->getParam('ex_job_position');
    $ex_from_date          = $this->getRequest()->getParam('ex_from_date');
    $ex_to_date            = $this->getRequest()->getParam('ex_to_date');
    $ex_reason_for_leaving = $this->getRequest()->getParam('ex_reason_for_leaving');

    //relative
    $relative_id        = $this->getRequest()->getParam('relative_id');
    $relative_type      = $this->getRequest()->getParam('relative_type');
    $rlt_full_name      = $this->getRequest()->getParam('rlt_full_name');
    $rlt_gender         = $this->getRequest()->getParam('rlt_gender');
    $rlt_birth_year     = $this->getRequest()->getParam('rlt_birth_year');
    $rlt_job            = $this->getRequest()->getParam('rlt_job');
    $rlt_work_place     = $this->getRequest()->getParam('rlt_work_place');
    $rlt_ID_card_number = $this->getRequest()->getParam('rlt_ID_card_number');
    $owner_house        = $this->getRequest()->getParam('owner_house2', -100);


    $dob                          = $this->getRequest()->getParam('dob');
    $company_id                   = $this->getRequest()->getParam('company_id');
    $social_insurance_time        = $this->getRequest()->getParam('social_insurance_time');
    $social_insurance_number      = $this->getRequest()->getParam('social_insurance_number');
    $personal_tax                 = $this->getRequest()->getParam('personal_tax');
    $family_allowances_registered = $this->getRequest()->getParam('family_allowances_registered');
    $is_officer                   = $this->getRequest()->getParam('is_officer', 0);
    $is_print                     = $this->getRequest()->getParam('is_print', 0);

    $status              = $this->getRequest()->getParam('status', My_Staff_Status::Off);
    $marital_status      = $this->getRequest()->getParam('marital_status');
    $tags                = $this->getRequest()->getParam('tags');
    $code                = $this->getRequest()->getParam('code');
    $contract_type       = $this->getRequest()->getParam('contract_type');
    $contract_signed_at  = $this->getRequest()->getParam('contract_signed_at');
    $contract_term       = $this->getRequest()->getParam('contract_term');
    $contract_expired_at = $this->getRequest()->getParam('contract_expired_at');
    $print_time          = $this->getRequest()->getParam('print_time');
    $department          = $this->getRequest()->getParam('department');
    $team                = $this->getRequest()->getParam('team');
    $firstname           = $this->getRequest()->getParam('firstname');
    $lastname            = $this->getRequest()->getParam('lastname');
    $title               = $this->getRequest()->getParam('title');
    $phone_number        = $this->getRequest()->getParam('phone_number');
    $joined_at           = $this->getRequest()->getParam('joined_at');
    $off_date            = $this->getRequest()->getParam('off_date');
    $off_type            = $this->getRequest()->getParam('off_type', null);
    $gender              = $this->getRequest()->getParam('gender');
    $level               = $this->getRequest()->getParam('level');
    $certificate         = $this->getRequest()->getParam('certificate');

    $ID_number       = $this->getRequest()->getParam('ID_number');
    $ID_place        = $this->getRequest()->getParam('ID_place');
    $ID_date         = $this->getRequest()->getParam('ID_date');
    $nationality     = $this->getRequest()->getParam('nationality');
    $religion        = $this->getRequest()->getParam('religion');
    $note            = $this->getRequest()->getParam('note');
    $email           = $this->getRequest()->getParam('email');
    $regional_market = $this->getRequest()->getParam('regional_market');
    $change_password = $this->getRequest()->getParam('change-pass');
    $password        = $this->getRequest()->getParam('password');
    $group_id        = $this->getRequest()->getParam('group_id', 0);

    // $native_place      = $this->getRequest()->getParam('native_place');

    $dob                          = $this->getRequest()->getParam('dob');
    $company_id                   = $this->getRequest()->getParam('company_id');
    $social_insurance_time        = $this->getRequest()->getParam('social_insurance_time');
    $social_insurance_number      = $this->getRequest()->getParam('social_insurance_number');
    $personal_tax                 = $this->getRequest()->getParam('personal_tax');
    $family_allowances_registered = $this->getRequest()->getParam('family_allowances_registered');
    $is_officer                   = $this->getRequest()->getParam('is_officer', 0);
    $is_print                     = $this->getRequest()->getParam('is_print', 0);
    $pvi                          = $this->getRequest()->getParam('pvi', 0);
    $lock                         = $this->getRequest()->getParam('lock', 0);

    //bank
    $bank_account  = $this->getRequest()->getParam('bank_account');
    $bank          = $this->getRequest()->getParam('bank');
    $bank_name     = $this->getRequest()->getParam('bank_name');
    $cash          = $this->getRequest()->getParam('cash', 0);
    /// TUONG 1/11/2018
    $bank_num      = $this->getRequest()->getParam('bank_number');
    $bank_name_    = $this->getRequest()->getParam('bank_name_');
    $bank_brand    = $this->getRequest()->getParam('bank_brand');
    $bank_province = $this->getRequest()->getParam('bank_province');

    //TUONG 15/11 UPDATE DEPENDENT_PERSON
    $pti_dependent           = $this->getRequest()->getParam('mst_dependent');
    $fullname_dependent      = $this->getRequest()->getParam('fullname_dependent');
    $dob_dependent           = $this->getRequest()->getParam('dob_dependent');
    $name_relative_dependent = $this->getRequest()->getParam('name_relative_dependent');
    $month_start             = $this->getRequest()->getParam('month_start');
    $month_end               = $this->getRequest()->getParam('month_end');
    //TUONG 
    //dependent
    $pit_code                = $this->getRequest()->getParam('pit_code');
    $dependent_id            = $this->getRequest()->getParam('dependent_id');
    $dependent_type          = $this->getRequest()->getParam('dependent_type');
    $dependent_full_name     = $this->getRequest()->getParam('dependent_full_name');
    $dependent_id_number     = $this->getRequest()->getParam('dependent_id_number');

    $id_place_province   = $this->getRequest()->getParam('id_place_province', 0);
    $id_citizen_province = $this->getRequest()->getParam('id_citizen_province', 0);

    $status         = $this->getRequest()->getParam('status', My_Staff_Status::Off);
    $marital_status = $this->getRequest()->getParam('marital_status', My_Staff_MaritalStatus::Single);

    $date_off_purpose_reason = $this->getRequest()->getParam('date_off_purpose_reason');
    $date_off_purpose_detail = $this->getRequest()->getParam('date_off_purpose_detail');

    $id_insert = '';

    $off_all_machine  = $this->getRequest()->getParam('off_all_machine');
    $post_map_machine = $this->getRequest()->getParam('post_map_machine');
    if (!empty($off_all_machine)) {
        $QCheck_in = new Application_Model_CheckIn();
        $QCheck_in->offAllMachineConfig($code);
    }



    $tmp = $this->_formatDate($off_date);
    $pos = strripos(trim($full_name), " ");
    if (trim($pos)) {
        $firstname = substr(trim($full_name), 0, $pos);
        $lastname  = substr(trim($full_name), $pos + 1, strlen(trim($full_name)) - $pos);
    } else {
        $lastname = trim($full_name);
    }

    //dánh dấu ngày cập nhật offdate cho Bảo hiểm
    $off_date_created_at = ($tmp) ? date('Y-m-d H:i:s') : NULL;
    $data                = array(
        'contract_type'                => intval($contract_type),
        'contract_signed_at'           => $this->_formatDate($contract_signed_at),
        'contract_term'                => intval($contract_term),
        'contract_expired_at'          => $this->_formatDate($contract_expired_at),
        'print_time'                   => $print_time,
        'department'                   => intval($department),
        'team'                         => intval($team),
        'firstname'                    => My_String::trim($firstname),
        'lastname'                     => My_String::trim($lastname),
        'title'                        => $title,
        'phone_number'                 => $phone_number,
        'joined_at'                    => $this->_formatDate($joined_at),
        'off_date'                     => $tmp,
        'gender'                       => $gender,
        'off_type'                     => intval($off_type),
        'level'                        => $level,
        'certificate'                  => $certificate,
        'ID_number'                    => $ID_number,
        'ID_place'                     => $ID_place,
        'ID_date'                      => $this->_formatDate($ID_date),
        'nationality'                  => intval($nationality),
        'religion'                     => intval($religion),
        'note'                         => $note,
        'regional_market'              => intval($regional_market),
        // 'group_id'                     => intval($group_id),
        'social_insurance_time'        => $social_insurance_time,
        'social_insurance_number'      => $social_insurance_number,
        'personal_tax'                 => $personal_tax,
        'family_allowances_registered' => $family_allowances_registered,
        'dob'                          => $dob,
        'company_id'                   => $company_id,
        'is_officer'                   => $is_officer,
        'is_print'                     => $is_print,
        'pvi'                          => $pvi,
        'status'                       => intval($status),
        'marital_status'               => intval($marital_status),
        'id_place_province'            => intval($id_place_province),
        'id_citizen_province'          => intval($id_citizen_province),
        'office_id'                    => intval($office_id),
        'off_date_created_at'          => $off_date_created_at,
        'date_off_purpose_reason'      => ($date_off_purpose_reason != '') ? $date_off_purpose_reason : null,
        'date_off_purpose_detail'      => $date_off_purpose_detail,
    );
    /**
     * @author: hero
     * title
     * Thanh chinh quan huyen default 27-10-2020 
     * date
     */
    if (!in_array($title, TITLE_NON_OFFICE)) {
        $check_history = 0;
        if ($id) {
            $where = [];
            $where[] = $QStaff->getAdapter()->quoteInto('id = ?', intval($id));
            $staff = $QStaff->fetchRow($where);
            if($staff['regional_market'] == $data['regional_market']){
                $check_history = 1;
            }
        }
        if($check_history == 0){
            //$arr_none_office          = array(182, 183, 162, 164, 293, 312, 295, 419, 403, 420, 417, 545, 577);
            $QRegional_market         = new Application_Model_RegionalMarket();
            $where_regional_market    = array();
            $where_regional_market[]  = $QRegional_market->getAdapter()->quoteInto('parent = ?', $regional_market);
            $where_regional_market[]  = $QRegional_market->getAdapter()->quoteInto('default_contract_district = ?', 1);
            $where_regional_market[]  = $QRegional_market->getAdapter()->quoteInto('del = ?', 0);
            $regional_market_district = $QRegional_market->fetchRow($where_regional_market);
            $default_district         = $regional_market_district['id'];
            if (empty($default_district) || $QRegional_market->checkDistrictExits($default_district) == 0) {
                echo "<pre>";
                var_dump("Tỉnh không tồn tại quận huyện default");
                die;
            }
            $data['district'] = $default_district;
        }
    }


    // trường hợp là PG thì đánh dấu ko có email. còn lại giữ nguyên
    if (My_Staff::isPgTitle($title))
        $data['has_email'] = 0;

    $remove_store = false;

    //set disabled
    if ($tmp) {
        $data['status']   = My_Staff_Status::Off;
        $data['group_id'] = 0;

        try {
            My_Staff::clear_all_roles($id);
            // My_Staff::removeStoreForTransfer($id, $title, $tmp);
            $remove_store = true;
        } catch (exception $e) {
            
        }
    }

    /**
     * Nếu chuyển status khác on thì cũng xóa quyền, xóa store
     */
    if (My_Staff_Status::On != $data['status'] && !$remove_store) {
        try {
            My_Staff::clear_all_roles($id);
            // My_Staff::removeStoreForTransfer($id, $title, date('Y-m-d'));
        } catch (exception $e) {
            
        }
    } else {
        $data['date_off_purpose'] = null;
    }

    $string = "Bạn phải gỡ tất cả các shop của " . $this->getRequest()->getParam('full_name') . " - " . $this->getRequest()->getParam('code') . " trước khi bật OFF : <a href=" . HOST . 'manage/sales-pg-view?id=' . $this->getRequest()->getParam('id') . ">Tại đây</a> ";


    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $db = Zend_Registry::get('db');

    try {
        $db->beginTransaction();


        if ($this->getRequest()->getParam('off_date') != null && in_array($this->getRequest()->getParam('status'), array(0, 3)) && in_array($this->getRequest()->getParam('title'), array(PGPB_TITLE, SALES_TITLE, PG_BRANDSHOP, SENIOR_PROMOTER_BRANDSHOP, CHUYEN_VIEN_BAN_HANG_TITLE, PG_LEADER_TITLE, STORE_LEADER, LEADER_TITLE))) {

            $string          = "Bạn phải gỡ tất cả các shop của " . $this->getRequest()->getParam('full_name') . " - " . $this->getRequest()->getParam('code') . " trước khi bật OFF : <a href=" . HOST . 'manage/sales-pg-view?id=' . $this->getRequest()->getParam('id') . ">Tại đây</a> ";
            $QStoreLeaderLog = new Application_Model_StoreStaffLog();
            $where           = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null and staff_id = ?', $id);
            $stores          = $QStoreLeaderLog->fetchAll($where);

            if (count($stores) > 0) {
                throw new Exception($string);
            }
        }



        if ($email) {
            $email   = trim($email);
            $where   = array();
            $where[] = $QStaff->getAdapter()->quoteInto('email IS NOT NULL AND email LIKE ?', str_replace(EMAIL_SUFFIX, '', $email) . EMAIL_SUFFIX);

            if ($id)
                $where[] = $QStaff->getAdapter()->quoteInto('id <> ?', intval($id));

            $staff_check = $QStaff->fetchRow($where);

            if ($staff_check)
                throw new Exception("Email exists", 1);
        }
        
        $update_contract = 0;
        //check gán shop thì không cho edit title + province + company
        if ($id) {
            if(in_array($data['title'], TITLE_ASSIGN_SHOP)){
                $where = [];
                $where[] = $QStaff->getAdapter()->quoteInto('id = ?', intval($id));
                $staff = $QStaff->fetchRow($where);
                if($staff['title'] != $data['title'] || $staff['regional_market'] != $data['regional_market'] || $staff['company_id'] != $data['company_id']){
                    $QStoreLeaderLog = new Application_Model_StoreStaffLog();
                    $where = [];
                    $where           = $QStoreLeaderLog->getAdapter()->quoteInto('staff_id = ?', $id);
                    $stores          = $QStoreLeaderLog->fetchAll($where);
                    if (count($stores) > 0) {
                        throw new Exception('Bạn sử dụng transfer để thay đổi những thông tin này !');
                    }else {
                        $update_contract = 1;
                    }
                }   
            }
        }
        
        //TUONG update bank
        if ($id) {
            $QStaffMyBank = new Application_Model_StaffMyBank();
            $where_bank   = array();
            $where_bank[] = $QStaffMyBank->getAdapter()->quoteInto('staff_id = ?', intval($id));
            $bank_exist   = $QStaffMyBank->fetchRow($where_bank);
            if (!empty($bank_exist)) {
                $data_bank = [
                    'bank_number'   => $bank_num,
                    'at_bank'       => $bank_name_,
                    'branch_pgd'    => $bank_brand,
                    'province_city' => $bank_province
                ];
                $QStaffMyBank->update($data_bank, $where_bank);
            } else {

                $data_bank = [
                    'staff_code'    => $code,
                    'fullname'      => $firstname . ' ' . $lastname,
                    'bank_number'   => $bank_num,
                    'at_bank'       => $bank_name_,
                    'branch_pgd'    => $bank_brand,
                    'province_city' => $bank_province,
                    'Note'          => 'Update Staff Edit',
                    'staff_id'      => $id
                ];
                $QStaffMyBank->insert($data_bank);
            }


            //TUONG 15//11 UPDATA dependent person
            $QDependentPersonStaff = new Application_Model_DependentPersonStaff();

            //xóa data cũ
            $where_pit = $QDependentPersonStaff->getAdapter()->quoteInto('staff_id = ?', $id);
            $QDependentPersonStaff->delete($where_pit);

            foreach ($pti_dependent as $key => $value) {

                $dob_dependent_ = $dob_dependent[$key] ? date_create_from_format("d/m/Y", $dob_dependent[$key])->format("Y-m-d") : null;
                $month_start_   = $month_start[$key] ? date_create_from_format("d/m/Y", $month_start[$key])->format("Y-m-d") : null;
                $month_end_     = $month_end[$key] ? date_create_from_format("d/m/Y", $month_end[$key])->format("Y-m-d") : null;

                if ($name_relative_dependent[$key] == 1):
                    $relative_ = 'Con';
                elseif ($name_relative_dependent[$key] == 2):
                    $relative_ = 'Vợ/Chồng';
                elseif ($name_relative_dependent[$key] == 3):
                    $relative_ = 'Mẹ/Cha';
                elseif ($name_relative_dependent[$key] == 4):
                    $relative_ = 'Khác';
                endif;


                $data_pit = [
                    'code'        => $code,
                    'name'        => $fullname_dependent[$key],
                    'dob'         => $dob_dependent_,
                    'mst'         => $value,
                    'relative_id' => $name_relative_dependent[$key],
                    'from_month'  => $month_start_,
                    'to_month'    => $month_end_,
                    'relative'    => $relative_,
                    'staff_id'    => $id
                ];
                $QDependentPersonStaff->insert($data_pit);
            }

            //15/11
        }
        //TUONG

        if ($id) {
            if ($code)
                $data['code'] = $code;

            $where = $QStaff->getAdapter()->quoteInto('id = ?', $id);

            if ($change_password)
                $data['password'] = md5($password);

            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = $userStorage->id;

            $staffRowset = $QStaff->find($id);
            $s           = $staffRowset->current();

            if (empty($s['old_email'])) {
                $old_email         = (!empty($tmp) ? $email : '');
                $data['old_email'] = $old_email;
            }

            $email         = (!empty($tmp) ? null : $email);
            $data['email'] = $email;

            //check staff edit bank 
            if ($userStorage->id == STAFF_EDIT_BANK_PIT || $userStorage->group_id == ADMINISTRATOR_ID) {
                $data['pit_code'] = $pit_code;
            }
            //end check

            $QStaff->update($data, $where);
            
            if($update_contract == 1){
                $this->updateContractStaffEdit($id);
            }
            
            $staffRowset = $QStaff->find($id);
            $s_after     = $staffRowset->current();

            if (isset($lock) and $lock == 0) {
                $QTimeStaffExpired = new Application_Model_TimeStaffExpired();
                $whereExpired      = array();
                $whereExpired[]    = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ?', $id);
                $whereExpired[]    = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null', null);
                $data              = array(
                    'approved_at' => date('Y-m-d h:i:s'),
                    'approved_by' => $userStorage->id
                );
                $QTimeStaffExpired->update($data, $whereExpired);
            }

            Log::w($s->toArray(), $s_after->toArray(), $id, LogGroup::Staff, LogType::
                    Update);

            $QInsuranceStaffChange = new Application_Model_InsuranceStaffChange();
            $QInsuranceStaffChange->save($s->toArray(), $s_after->toArray(), $id);

            //UPDATE TRAINING ONLINE
            if ($s['ID_number'] != $s_after['ID_number']) {
                $QLessonScores = new Application_Model_LessonScores();
                $where_lesson  = $QLessonScores->getAdapter()->quoteInto('staff_cmnd = ?', $s['ID_number']);
                $data_lesson   = ['staff_cmnd' => $s_after['ID_number']];
                $QLessonScores->update($data_lesson, $where_lesson);
            }
        } else {
            if (in_array($title, TITLE_NOT_MAIL)) {
                if (empty($email)) {
                    $data['email'] = $this->getEmailStaffNew($data['firstname'], $data['lastname']);
                }
            }
            // generate staff code
            $data['code'] = $QStaff->genStaffCode($this->_formatDate($joined_at));

            $password         = empty($password) ? '123456' : $password;
            $data['password'] = md5($password);

            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $userStorage->id;

            //Default unit_code_id For insurance
            $data['unit_code_id'] = intval($company_id);

            //check staff edit bank 
            if ($userStorage->id == STAFF_EDIT_BANK_PIT || $userStorage->group_id == ADMINISTRATOR_ID) {
                $data['pit_code'] = $pit_code;
            }
            //end check

            $id        = $QStaff->insert($data);
            $joined_at = $this->_formatDate($joined_at);
//            $result    = $this->contractAdd($id, $joined_at);

            $id_insert = $id;

            if (in_array($title, [PGPB_TITLE, PG_BRANDSHOP, PGPB_PART_TIME_TITLE])) {
                $data_notification                    = [];
                $data_notification['regional_market'] = $regional_market;
                $data_notification['staff_id']        = $id_insert;
                $data_notification['title']           = $title;
                $data_notification['firstname']       = $data['firstname'];
                $data_notification['lastname']        = $data['lastname'];

                //  $result_noti = $this->notiTrainerInsert($data_notification);

                /* if ($result_noti['code'] != 1) {
                  throw new Exception($result_noti['message']);
                  } */
            }
            // insert hop dong thu viec
            $resultnew = $this->contractInsert($id);
            if ($resultnew['code'] != 1) {
                throw new Exception($resultnew['message']);
            }
            $staffRowset = $QStaff->find($id);
            $s_after     = $staffRowset->current();

            Log::w(array(), $s_after->toArray(), $id, LogGroup::Staff, LogType::Insert);
        }

        // get address
        $temp_address  = $this->getRequest()->getParam('temp_address');
        $temp_ward     = $this->getRequest()->getParam('temp_ward');
        $temp_district = $this->getRequest()->getParam('temp_district');
        $temp_province = $this->getRequest()->getParam('temp_province');
        $temp_ward_id  = $this->getRequest()->getParam('temp_ward_id');

        $perm_address  = $this->getRequest()->getParam('perm_address');
        $perm_ward     = $this->getRequest()->getParam('perm_ward');
        $perm_district = $this->getRequest()->getParam('perm_district');
        $perm_province = $this->getRequest()->getParam('perm_province');
        $perm_ward_id  = $this->getRequest()->getParam('perm_ward_id');

        $birth_ward     = $this->getRequest()->getParam('birth_ward');
        $birth_district = $this->getRequest()->getParam('birth_district');
        $birth_province = $this->getRequest()->getParam('birth_province');
        $birth_ward_id  = $this->getRequest()->getParam('birth_ward_id');

        $id_card_address  = $this->getRequest()->getParam('id_card_address');
        $id_card_ward     = $this->getRequest()->getParam('id_card_ward');
        $id_card_district = $this->getRequest()->getParam('id_card_district');
        $id_card_province = $this->getRequest()->getParam('id_card_province');
        $id_card_ward_id  = $this->getRequest()->getParam('id_card_ward_id');

        if ($id) {

            $staffRowset = $QStaff->find($id);
            $s           = $staffRowset->current();

            // ------------------ upload
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $id;

            $file_uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                    DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $id;

            $arrPhoto = array(
                'photo'         => $uploaded_dir,
                'offdate_file'  => $file_uploaded_dir . DIRECTORY_SEPARATOR . 'off_date',
                'id_photo'      => $uploaded_dir . DIRECTORY_SEPARATOR . 'ID_Front',
                'id_photo_back' => $uploaded_dir . DIRECTORY_SEPARATOR . 'ID_Back',
            );

            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile' => true));

            //check function
            if (function_exists('finfo_file'))
                $upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf', 'application/zip'));

            $upload->addValidator('Extension', false, 'jpg,jpeg,png,gif,doc,docx');
            $upload->addValidator('Size', false, array('max' => '2MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');
            $files = $upload->getFileInfo();

            $hasPhoto = false;

            $data = array();

            foreach ($arrPhoto as $key => $val) {
                $del = 'del_' . $key;
                if (isset($$del) and $ $del) {
                    $data[$key] = null;

                    @unlink($val . $s[$key]);
                }

                if (isset($files[$key]['name'])) {
                    $hasPhoto = true;
                }
            }

            if ($hasPhoto) {

                if (!$upload->isValid()) {
                    $errors = $upload->getErrors();

                    $sError = null;

                    if ($errors and isset($errors[0]))
                        switch ($errors[0]) {
                            case 'fileUploadErrorIniSize':
                                $sError = 'File size is too large';
                                break;
                            case 'fileMimeTypeFalse':
                            case 'fileExtensionFalse':
                                $sError = 'The file(s) you selected weren\'t the type we were expecting';
                                break;
                            default:
                                $sError = 'The file(s) you selected weren\'t the type we were expecting';
                                break;
                        }

                    throw new Exception($sError);
                }

            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
                foreach ($arrPhoto as $key => $val) {
                    $fileInfo = (isset($files[$key]) and $files[$key]) ? $files[$key] : null;
                    if (isset($fileInfo['name']) and $fileInfo['name']) {

                        if (!is_dir($val))
                            @mkdir($val, 0777, true);

                        $upload->setDestination($val);

                        $old_name = $fileInfo['name'];

                        $tExplode  = explode('.', $old_name);
                        $extension = strtolower(end($tExplode));

                        $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

                        $upload->addFilter('Rename', array('target' => $val .
                            DIRECTORY_SEPARATOR . $new_name));

                        $r = $upload->receive(array($key));
                        if($r){
                            $file1 = $val.DIRECTORY_SEPARATOR . $new_name;
                            $destination = '';
                            if($key == 'photo') {
                                $destination = "photo/staff/".$id."/";
                            }else if($key == 'offdate_file') {
                                $destination = "files/staff/".$id."/off_date/";
                            }else if($key == 'id_photo') {
                                $destination = "photo/staff/".$id."/ID_Front/";
                            }else if($key == 'id_photo_back') {
                                $destination = "photo/staff/".$id."/ID_Back/";
                            }
                            $s3_lib->uploadS3($file1, $destination, $new_name);
                        }

                        if ($r)
                            $data[$key] = $new_name;
                        else {
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg)
                                throw new Exception($msg);
                        }
                    }
                }
            }

            if ($data) {
                $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?', $id);
                $QStaff->update($data, $whereStaff);
            }
            // ------------------ /upload

            $QStaffAddress = new Application_Model_StaffAddress();

            //dia chi tam tru
            if ($temp_province) {
                $province_code = $this->getProviceCodeById($temp_province);
                $address_data  = array(
                    'staff_id'      => $id,
                    'address_type'  => My_Staff_Address::Temporary,
                    'address'       => $temp_address,
                    'ward'          => $temp_ward,
                    'district'      => $temp_district,
                    //'ward_id'       => $temp_ward_id != '' ? $temp_ward_id : null,
                    'province_code' => $province_code
                );
                if($temp_ward_id == 'null'){
                    $address_data['ward_id'] = null;
                }else if($temp_ward_id == ''){
                    
                }else {
                    $address_data['ward_id'] = $temp_ward_id;
                }
                try {
                    $QStaffAddress->insert($address_data);
                } catch (exception $e) {
                    $where   = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Temporary);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    } catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            //dia chi thuong tru
            if ($perm_province) {
                $province_code = $this->getProviceCodeById($perm_province);
                $address_data  = array(
                    'staff_id'      => $id,
                    'address_type'  => My_Staff_Address::Permanent,
                    'address'       => $perm_address,
                    'ward'          => $perm_ward,
                    'district'      => $perm_district,
                    //'ward_id'       => $perm_ward_id != '' ? $perm_ward_id : null,
                    'province_code' => $province_code
                );
                if($perm_ward_id == 'null'){
                    $address_data['ward_id'] = null;
                }else if($perm_ward_id == ''){
                    
                }else {
                    $address_data['ward_id'] = $perm_ward_id;
                }
                try {
                    $QStaffAddress->insert($address_data);
                } catch (exception $e) {
                    $where   = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Permanent);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    } catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            // dia chi chung minh
            if ($id_card_province) {
                $province_code = $this->getProviceCodeById($id_card_province);
                $address_data  = array(
                    'staff_id'      => $id,
                    'address_type'  => My_Staff_Address::ID_Card,
                    'address'       => $id_card_address,
                    'ward'          => $id_card_ward,
                    'district'      => $id_card_district,
                    //'ward_id'       => $id_card_ward_id != '' ? $id_card_ward_id : null,
                    'province_code' => $province_code
                );
                if($id_card_ward_id == 'null'){
                    $address_data['ward_id'] = null;
                }else if($id_card_ward_id == ''){
                    
                }else {
                    $address_data['ward_id'] = $id_card_ward_id;
                }
                try {
                    $QStaffAddress->insert($address_data);
                } catch (exception $e) {
                    $where   = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::ID_Card);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    } catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            //dia chi noi sinh
            if ($birth_province) {
                $province_code = $this->getProviceCodeById($birth_province);
                $address_data  = array(
                    'staff_id'      => $id,
                    'address_type'  => My_Staff_Address::Birth_Certificate,
                    'ward'          => $birth_ward,
                    'district'      => $birth_district,
                    //'ward_id'       => $birth_ward_id != '' ? $birth_ward_id : null,
                    'province_code' => $province_code
                );
                if($birth_ward_id == 'null'){
                    $address_data['ward_id'] = null;
                }else if($birth_ward_id == ''){
                    
                }else {
                    $address_data['ward_id'] = $birth_ward_id;
                }
                try {
                    $QStaffAddress->insert($address_data);
                } catch (exception $e) {
                    $where   = array();
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                    $where[] = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::Birth_Certificate);

                    try {
                        $QStaffAddress->update($address_data, $where);
                    } catch (exception $e) {
                        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                        $this->_redirect(HOST . 'staff');
                    }
                }
            }

            //check education
            if (count($levels) > 0 AND is_array($levels) AND is_array($schools) AND is_array($field_of_studys)
                    AND is_array($graduated_years) AND is_array($grades) AND is_array($mode_of_studys)) {
                $QStaffEducation = new Application_Model_StaffEducation();

                foreach ($levels as $key => $value) {

                    if ($value == '' OR $value == NULL) {
                        continue;
                    }

                    $dataEdu = array(
                        'staff_id'       => $id,
                        'level_id'       => $value,
                        'school'         => $schools[$key],
                        'field_of_study' => $field_of_studys[$key],
//                        'graduated_year' => isset($graduated_years[$key]) ? $graduated_years[$key] : null,
                        'grade'          => $grades[$key],
                        'mode_of_study'  => $mode_of_studys[$key],
                        'default_level'  => (isset($default_level[$key]) AND $default_level[$key] == 1) ? 1 : 0,
                    );

                    if (isset($graduated_years[$key]) and ! empty($graduated_years[$key])) {
                        $dataEdu['graduated_year'] = isset($graduated_years[$key]) ? $graduated_years[$key] : null;
                    }

                    if (isset($education_ids[$key]) AND $education_ids[$key]) {
                        $where = $QStaffEducation->getAdapter()->quoteInto('id = ?', $education_ids[$key]);
                        $QStaffEducation->update($dataEdu, $where);
                    } else {
                        $QStaffEducation->insert($dataEdu);
                    }
                }
                $selectDefaultLevel = $QStaffEducation->select()
                        ->where('staff_id = ?', $id)
                        ->where('default_level = ?', 1);
                $result             = $QStaffEducation->fetchAll($selectDefaultLevel);
                if ($result->count() > 1) {
                    $flashMessenger->setNamespace('error')->addMessage('Education default is only single');
                    $this->_redirect(($back_url ? $back_url : HOST . 'staff'));
                }
            }

            //experience
            $QStaffExperience = new Application_Model_StaffExperience();
            if (is_array($ex_company_name) AND is_array($ex_job_position) AND is_array($ex_from_date) AND is_array($ex_from_date)
                    AND is_array($ex_to_date) AND is_array($ex_reason_for_leaving) AND count($ex_company_name) > 0) {
                foreach ($ex_company_name as $key => $value) {
                    if (trim($value) == '') {
                        continue;
                    }

                    $tmp_from_date = explode('/', $ex_from_date[$key]);
                    $tmp_from_date = $tmp_from_date[2] . '-' . $tmp_from_date[1] . '-' . $tmp_from_date[0];
                    $tmp_to_date   = explode('/', $ex_to_date[$key]);
                    $tmp_to_date   = $tmp_to_date[2] . '-' . $tmp_to_date[1] . '-' . $tmp_to_date[0];
                    $dataEx        = array(
                        'staff_id'           => $id,
                        'company_name'       => $value,
                        'job_position'       => $ex_job_position[$key],
                        'from_date'          => date('Y-m-d', strtotime($tmp_from_date)),
                        'to_date'            => date('Y-m-d', strtotime($tmp_to_date)),
                        'reason_for_leaving' => $ex_reason_for_leaving[$key],
                    );

                    if (isset($ex_experience_id[$key]) AND $ex_experience_id[$key]) {
                        $where = $QStaffExperience->getAdapter()->quoteInto('id = ?', $ex_experience_id[$key]);
                        $QStaffExperience->update($dataEx, $where);
                    } else {
                        $QStaffExperience->insert($dataEx);
                    }
                }
            }//End experience
            //relative
            if (is_array($relative_type) AND is_array($rlt_full_name) AND is_array($rlt_gender)
                    AND is_array($rlt_birth_year) AND is_array($rlt_job) AND is_array($rlt_work_place) AND count($relative_type) > 0) {
                $QStaffRelative = new Application_Model_StaffRelative();
                $var_temp       = 0;
                foreach ($relative_type as $key => $value) {
                    if (trim($value) == '') {
                        continue;
                    }
                    $dataRlt = array(
                        'staff_id'       => $id,
                        'relative_type'  => intval($value),
                        'full_name'      => trim($rlt_full_name[$key]),
                        'gender'         => intval($rlt_gender[$key]),
                        'birth_year'     => ($rlt_birth_year[$key]) ? $rlt_birth_year[$key] : NULL,
                        'job'            => trim($rlt_job[$key]),
                        'work_place'     => trim($rlt_work_place[$key]),
                        'ID_card_number' => trim($rlt_ID_card_number[$key])
                    );
                    if (isset($relative_id[$key]) AND $relative_id[$key]) {
                        if ($relative_id[$key] == $owner_house) {
                            $dataRlt['owner_house'] = 1;
                        } else {
                            $dataRlt['owner_house'] = 0;
                        }
                        $where = $QStaffRelative->getAdapter()->quoteInto('id = ?', $relative_id[$key]);
                        $QStaffRelative->update($dataRlt, $where);
                    } else {
                        if ($var_temp == $owner_house) {
                            $dataRlt['owner_house'] = 1;
                        } else {
                            $dataRlt['owner_house'] = 0;
                        }
                        $QStaffRelative->insert($dataRlt);
                        $var_temp--;
                    }
                }
            }//End relative
            //check staff edit bank, edit PIT
            if ($userStorage->id == STAFF_EDIT_BANK_PIT || $userStorage->group_id == ADMINISTRATOR_ID) {
                //bank
                $QStaffBank = new Application_Model_StaffBank();
                if (is_null($bank_account) || $bank_account == '' || intval($bank) == 0) {
                    $cash = 1;
                }

                if ($bank) {
                    $bank_data = array(
                        'staff_id'     => $id,
                        'bank_account' => $bank_account,
                        'bank'         => intval($bank),
                        'bank_name'    => $bank_name,
                        'cash'         => intval($cash),
                    );
                    try {
                        $QStaffBank->insert($bank_data);
                    } catch (exception $e) {
                        $where   = array();
                        $where[] = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $id);
                        try {
                            $QStaffBank->update($bank_data, $where);
                        } catch (exception $e) {
                            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                            $this->_redirect(HOST . 'staff');
                        }
                    }
                }
                //END bank
                //dependent
                if (is_array($dependent_type) AND is_array($dependent_full_name)
                        AND is_array($dependent_id_number) AND count($dependent_id) > 0) {
                    $QStaffDependent = new Application_Model_StaffDependent();
                    foreach ($dependent_type as $key => $value) {
                        if (trim($value) == '') {
                            continue;
                        }
                        $dataDependent = array(
                            'staff_id'      => $id,
                            'relative_type' => intval($value),
                            'full_name'     => trim($dependent_full_name[$key]),
                            'ID_number'     => trim($dependent_id_number[$key])
                        );
                        if (isset($dependent_id[$key]) AND $dependent_id[$key]) {
                            $where = $QStaffDependent->getAdapter()->quoteInto('id = ?', $dependent_id[$key]);
                            $QStaffDependent->update($dataDependent, $where);
                        } else {
                            $QStaffDependent->insert($dataDependent);
                        }
                    }
                }//End dependent
            }
            //end check
        }

        // insert tags
        $QTag = new Application_Model_Tag();
        if ($id_insert) {
            $tags[] = 'BHS';
        }
        $QTag->add($tags, $id, TAG_STAFF);
        
        //send mail approve doc rm
        if($id_insert && in_array($title, [RM_TITLE,RM_STANDBY_TITLE])){
            $this->sendMailApproveRM($id,1);
        }

        $cache = Zend_Registry::get('cache');
        $cache->remove('staff_cache');
        
        $db->commit();

        if ($id_insert)
            My_Request::sync_table(array('table' => 'staff', 'id' => $id_insert, 'type' => 'insert'));
        else
            My_Request::sync_table(array('table' => 'staff', 'id' => $id, 'type' => 'update'));
    } catch (Exception $e) {
        PC::debug($e->getMessage(), 'Exception');
        $db->rollback();
        echo "<pre>";
        var_dump($e->getMessage());
        die;
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
}
$this->_redirect(($back_url ? $back_url : HOST . 'staff'));
