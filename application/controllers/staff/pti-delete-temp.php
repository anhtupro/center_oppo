<?php
$QUploadPtiAreaTemp = new Application_Model_UploadPtiAreaTemp();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger       = $this->_helper->flashMessenger;
$id            = $this->getRequest()->getParam('id');
try{

    $where = $QUploadPtiAreaTemp->getAdapter()->quoteInto('id = ?', $id);
    $QUploadPtiAreaTemp->delete($where);

    $flashMessenger->setNamespace('success')->addMessage('Xóa thành công!');
    $this->redirect(HOST.'staff/view-pti-area');

}catch (Exception $e)
{
    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
    $this->redirect(HOST.'staff/view-pti-area');
}


if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
    $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
    $messages          = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
}


