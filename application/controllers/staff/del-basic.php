<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$id    = $this->getRequest()->getParam('id');

if(!empty($id)){
    
    $QStaffTemp    = new Application_Model_StaffTemp();
    $where =     $QStaffTemp->getAdapter()->quoteInto('id = ?' , $id );
    $data = array(
        'del'   => 1
    );
    $QStaffTemp->update($data, $where);
    
}

$this->_redirect("/staff/list-basic-record");