<?php
$QUploadBankAreaTemp = new Application_Model_UploadBankAreaTemp();
$QArea = new Application_Model_Area();
$QAsm       = new Application_Model_Asm();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);

$area_id        = $this->getRequest()->getParam('area_id');
$staff_code        = $this->getRequest()->getParam('code');
$fullname        = $this->getRequest()->getParam('full_name');

$export        = $this->getRequest()->getParam('export');
$flashMessenger       = $this->_helper->flashMessenger;
$asm     = $QAsm->get_cache($userStorage->id);
$storage = $asm['area'];
$limit = 10;
$total = 0;
$params = [
     'list_area' => $storage,
	'area_id' => $area_id,
	'code'	  => $staff_code,
	'fullname' =>$fullname,
	'export'	=> $export
];
$params['sort'] = $sort;
$params['desc'] = $desc;
$resultList = $QUploadBankAreaTemp->fetchPagination($page, $limit, $total, $params);
$this->view->list= $resultList;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->url = HOST.'staff/view-bank-area'.($params ? '?'.http_build_query($params).'&' : '?');

$this->view->offset = $limit * ($page - 1);

$area_list = $QArea->getAreaList($params);
$this->view->list_area = $area_list;
$QLockUnlockUploadBankArea = new Application_Model_LockUnlockUploadBankArea();
$where = $QLockUnlockUploadBankArea->getAdapter()->quoteInto('id = ?', 1);
$lock = $QLockUnlockUploadBankArea->fetchRow($where);
$this->view->lock = $lock;

if(!empty($export) && $export== 1)
{

    ini_set("memory_limit", -1);
    ini_set("display_error", 1);
    error_reporting(~E_ALL);
    


    require_once 'PHPExcel.php';

    $PHPExcel = new PHPExcel();
    $heads = array(
        'STT',
        'AREA',
        'STAFF CODE',
        'FULL NAME',
        'CMND',
        'BANK NUMBER',
        'BANK NAME',
        'BRAND_PROVINCE',
        'BRAND',
        'PROVINCE',
        'NOTE'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet    = $PHPExcel->getActiveSheet();

    $alpha    = 'A';
    $index    = 1;
    foreach($heads as $key)
    {
        $sheet->setCellValue($alpha.$index, $key);
        $alpha++;
    }
    $index    = 2;

    $i = 1;

    foreach($resultList as $item){
        
       
        $alpha    = 'A';
        $sheet->setCellValue($alpha++.$index, $i++);
        $sheet->setCellValue($alpha++.$index, $item['area_name']);
        $sheet->setCellValue($alpha++.$index, "=\"" .$item['staff_code']. "\"");
        $sheet->setCellValue($alpha++.$index, $item['full_name']);
        $sheet->setCellValue($alpha++.$index, "=\"" .$item['cmnd']. "\"");
        $sheet->setCellValue($alpha++.$index, "=\"" .$item['bank_number']. "\"");
        $sheet->setCellValue($alpha++.$index, $item['bank_name']);
        $sheet->setCellValue($alpha++.$index, $item['brand_province']);
        $sheet->setCellValue($alpha++.$index, $item['bank_brand']);
        $sheet->setCellValue($alpha++.$index, $item['bank_province']);
        $sheet->setCellValue($alpha++.$index, $item['note']);
        $index++;

    }
    
    $filename = 'Report_Bank_Info' . date('d-m-Y H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
    $objWriter->save('php://output');
    exit;

}
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
