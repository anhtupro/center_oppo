<?php

ini_set('allow_url_fopen', true);
ini_set('allow_url_include', true);
ini_set('safe_mode', false);
ini_set('max_execution_time', 0);

My_ExportCsv::_exportExel($params);

        $sort            = $this->getRequest()->getParam('sort', '');
        $desc            = $this->getRequest()->getParam('desc', 1);

        $page            = $this->getRequest()->getParam('page', 1);
        $name            = $this->getRequest()->getParam('name');
        $department      = $this->getRequest()->getParam('department');
        $off             = $this->getRequest()->getParam('off', 1);
        $team            = $this->getRequest()->getParam('team');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        
        $note            = $this->getRequest()->getParam('note');
        $sname           = $this->getRequest()->getParam('sname', 0);
        $sname_width     = $this->getRequest()->getParam('span','span6');
        $code            = $this->getRequest()->getParam('code');
        $s_assign        = $this->getRequest()->getParam('s_assign');
        $ood             = $this->getRequest()->getParam('ood');
        $email           = $this->getRequest()->getParam('email');
        $is_officer      = $this->getRequest()->getParam('is_officer');
        $date            = $this->getRequest()->getParam('date');
        $month           = $this->getRequest()->getParam('month');
        $year            = $this->getRequest()->getParam('year');
        $tags            = $this->getRequest()->getParam('tags');

        
        
        $company_id      = $this->getRequest()->getParam('company_id');
        $need_approve    = $this->getRequest()->getParam('need_approve');
        $off_date        = $this->getRequest()->getParam('off_date');
        $indentity       = $this->getRequest()->getParam('indentity');
        $joined_at       = $this->getRequest()->getParam('joined_at');
        $off_type        = $this->getRequest()->getParam('off_type');
        $is_print        = $this->getRequest()->getParam('is_print');
        $is_not_print    = $this->getRequest()->getParam('is_not_print');
        $is_official     = $this->getRequest()->getParam('is_official');
        $status          = $this->getRequest()->getParam('status', 0);

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        // if($userStorage->title == ADMINISTRATOR_ID || $userStorage->title == SUPERADMIN_ID || $userStorage->id == 2807)
        // {
            
        //     $area_id    = $this->getRequest()->getParam('area_id', null);
        //     $this->view->area_visible = true;
        // }
        // else
        // {
        //     $this->view->area_visible = false;
            
        //     if(!empty($userStorage->regional_market_id) && isset($userStorage->regional_market_id))
        //     {
        //         $area_id = $userStorage->regional_market_id;
        //     }
        //     else
        //     {
        //         $area_id = -1;
        //     }
            
        // }
        
        $title           = $this->getRequest()->getParam('title');

        if ($tags and is_array($tags))
            $tags = $tags;
        else
            $tags = null;

        //check if export
        $export = $this->getRequest()->getParam('export', 0);
        $download = $this->getRequest()->getParam('download', 0);
        $checkphoto = $this->getRequest()->getParam('checkphoto', 0);

        if (!$sname)
            $limit = LIMITATION;
        else
            $limit = null;

        $total = 0;

        $params = array_filter(array(
            'name' => $name,
            'department' => $department,
            'off' => $off,
            'team' => $team,
            'regional_market' => $regional_market,
            'district' => $district,
            'area_id' => $area_id,
            'note' => $note,
            'code' => $code,
            's_assign' => $s_assign,
            'ood' => $ood,
            'email' => $email,
            's_assign' => $s_assign,
            'ood' => $ood,
            'email' => $email,
            'date' => $date,
            'month' => $month,
            'year' => $year,
            'is_officer' => $is_officer,
            'sname' => $sname,
            'tags' => $tags,
            'title' => $title,
            'company_id' => $company_id,
            'export' => $export,
            'need_approve' => $need_approve,
            'indentity' => $indentity,
            'joined_at' => $joined_at,
            'off_date'  => $off_date,
            'off_type'  => $off_type,
            'is_print'  => $is_print,
            'is_not_print' => $is_not_print,
            'is_official' => $is_official,
            'status' => $status,
        ));
         
        
        if($status != '')
        {
            $params['status'] = $status;
        }
        
        $params['sort'] = $sort;
        $params['desc'] = $desc;

        $QArea = new Application_Model_Area();
        $all_area =  $QArea->fetchAll(null, 'name');
        $this->view->areas = $all_area;

        $QRegionalMarket = new Application_Model_RegionalMarket();

        if ($area_id)
        {
            if (is_array($area_id) && count($area_id))
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $area_id);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);

            $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
        }

        if ($regional_market)
        {
            if (is_array($regional_market) && count($regional_market))
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent IN (?)', $regional_market);
            else
                $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);

            $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
        }

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

        /**
         * @author buu.pham
         * lọc staff thuộc khu vực nếu các title sau xem
         */
        if (in_array(
            $userStorage->title,
            array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM , SALE_SALE_ASM_STANDBY, 308, SALES_LEADER_TITLE)
        )) {
            $QAsm = new Application_Model_Asm();
            $cachedASM = $QAsm->get_cache($userStorage->id);
            $tem = $cachedASM['province'];
            if ($tem)
                $list_province_ids = $tem;
            else
                $list_province_ids = -1;
            // doi mau report cho asm
            $export = $export ?  2 : '';

            $params['list_province_ids'] = $list_province_ids;
        }
        // end

        $QStaff = new Application_Model_Staff();

        if(isset($export) and $export)
        {
            switch($export)
            {
                case 1 :
                {
                    $staffs = $QStaff->fetchPaginationPhoto($page, null, $total, $params);
                    $this->_exportXlsxASM($staffs);
                }
            }
        }

        if(isset($checkphoto) && $checkphoto)
        {
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'A' => 'STT',
                'B' => 'Staff Name',
                'C' => 'Code',
                'D' => 'Email',
                'E' => 'Ảnh thẻ',
                'F' => 'Ảnh CMND mặt trước',
                'G' => 'Ảnh CMND mặt sau',
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();

            foreach($heads as $key => $value)
                $sheet->setCellValue($key.'1', $value);

            $sheet->getStyle('A1:G1')->applyFromArray(array('font' => array('bold' => true)));

            $sheet->getColumnDimension('A')->setWidth(10);
            $sheet->getColumnDimension('B')->setWidth(30);
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('D')->setWidth(40);
            $sheet->getColumnDimension('E')->setWidth(45);
            $sheet->getColumnDimension('F')->setWidth(45);
            $sheet->getColumnDimension('G')->setWidth(45);

            $staffs_photo_export = $QStaff->fetchPaginationPhoto($page, null, $total, $params);

            $main_key = 0;

            $empty_photo = $this->getRequest()->getParam('empty-photo');
            $empty_id_photo = $this->getRequest()->getParam('empty-id-photo');
            $empty_id_photo_back = $this->getRequest()->getParam('empty-id-photo-back');
            foreach ($staffs_photo_export as $key => $value) {
                if(!empty($value['photo']) && file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/' . $value['photo']) && $empty_photo == 1)
                {
                    continue;
                }
                if((empty($value['photo']) || !file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/' . $value['photo'])) && $empty_photo == 2)
                {
                    continue;
                }

                if(!empty($value['id_photo']) && file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/ID_Front/' . $value['id_photo']) && $empty_id_photo == 1)
                {
                    continue;
                }
                if((empty($value['id_photo']) || !file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/ID_Front/' . $value['id_photo'])) && $empty_id_photo == 2)
                {
                    continue;
                }

                if(!empty($value['id_photo_back']) && file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/ID_Back/' . $value['id_photo_back']) && $empty_id_photo_back == 1)
                {
                    continue;
                }
                if((empty($value['id_photo_back']) || !file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/ID_Back/' . $value['id_photo_back'])) && $empty_id_photo_back == 2)
                {
                    continue;
                }
                $sheet->setCellValue('A' . ($main_key+2), $main_key+1);
                $sheet->setCellValue('B' . ($main_key+2), $value['firstname'] . ' ' . $value['lastname']);
                $sheet->setCellValue('C' . ($main_key+2), $value['code']);
                $sheet->setCellValue('D' . ($main_key+2), $value['email']);

                if(!empty($value['photo']) && file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/' . $value['photo']))
                {
                    $sheet->setCellValue('E' . ($main_key+2), $value['photo']);
                }
                else
                {
                    $sheet->setCellValue('E' . ($main_key+2), '');
                }
                
                if(!empty($value['id_photo']) && file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/ID_Front/' . $value['id_photo']))
                {
                    $sheet->setCellValue('F' . ($main_key+2), $value['id_photo']);
                }
                else
                {
                    $sheet->setCellValue('F' . ($main_key+2), '');
                }

                if(!empty($value['id_photo_back']) && file_exists(APPLICATION_PATH . '/../public/photo/staff/' . $value['id'] . '/ID_Back/' . $value['id_photo_back']))
                {
                    $sheet->setCellValue('G' . ($main_key+2), $value['id_photo_back']);
                }
                else
                {
                    $sheet->setCellValue('G' . ($main_key+2), '');
                }
                $main_key++;
            }
            $filename = 'Staff Photo - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            
            exit;
        }

        if(isset($download) && $download)
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if($userStorage->title == ADMINISTRATOR_ID || $userStorage->title == SUPERADMIN_ID || $userStorage->title == 2087)
            {
                $zipname = "all_image-asm";
            }
            else
            {
                $this->view->area_visible = false;
                $QAsm = new Application_Model_Asm();
                $asm_cache = $QAsm->get_cache();
                $asm_cache[ $userStorage->id ]['area'];

                $area_id = $asm_cache[ $userStorage->id ]['area'];

                $QArea = new Application_Model_Area(); // Lấy danh sách khu vực để xử lý tên khu vực cho file zip
                $area_name = $QArea->get_cache()[$area_id];
                if(empty($area_name))
                {
                    $zipname = "all_image";
                }
                else
                {
                    $zipname = "all_image-" . $area_name;
                }
                
            }

            $params['only_have_photo'] = 1;
            $staffs_photo = $QStaff->fetchPaginationPhoto(0, 0, $total, $params);
            $zipfullname = APPLICATION_PATH . "/.." .'/public/photozip/' . $zipname .'.zip';
            $link_download = '/photozip/' . $zipname .'.zip';
            $zip = new ZipArchive();
            if(file_exists($zipfullname))
            {
                unlink($zipfullname);
            }
            $zip->open($zipfullname, ZipArchive::CREATE);
            foreach ($staffs_photo as $key => $staff_photo) {
                $file_ext = explode('.', $staff_photo['photo'])[1];
                $staff_team = $recursiveDeparmentTeamTitle[$staff_photo['department']]['children'][$staff_photo['team']]['name'];
                $file_name = $staff_photo['area_name']. '_' . $staff_team .'_' . $staff_photo['code'] . '_' . $staff_photo['firstname'] . ' ' . $staff_photo['lastname']  . '.' . $file_ext;
                if(file_exists(APPLICATION_PATH . '/..' . '/public/photo/staff/' . $staff_photo['id'] . '/' . $staff_photo['photo']) && !empty($staff_photo['photo']))
                {
                    $zip->addFile(APPLICATION_PATH . '/..' . '/public/photo/staff/' . $staff_photo['id'] . '/' . $staff_photo['photo'], My_StringFormat::convert_vi_to_en($file_name ));
                }
            }
            $zip->close();
            // header('Content-Type: application/zip');
            // header('Content-disposition: attachment; filename=' . $zipname .'.zip');
            
            // header('Content-Length: ' . filesize($zipfullname));
            // header("Pragma: no-cache");
            // header("Cache-Control: private",false);
	        // header("Expires: 0"); 
            // readfile($zipfullname);
            // ini_set('max_execution_time', 0);
            // exit;
            $this->_redirect("/photozip/" . $zipname . ".zip");
        }


        $staffs = $QStaff->fetchPaginationPhoto($page, $limit, $total, $params);
        
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $QCompany = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $this->view->off_type = unserialize(OFF_TYPE);
        $this->view->userStorage = $userStorage;
        $this->view->desc = $desc;
        $this->view->sort = $sort;
        $this->view->staffs = $staffs;
        $this->view->params = $params;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'staff/list-photo' . ($params ? '?' . http_build_query($params) .
            '&' : '?');

        $this->view->offset = $limit * ($page - 1);

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages;

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $this->view->current_url = trim($this->getRequest()->getRequestUri(), '/');

        if ($this->getRequest()->isXmlHttpRequest())
        {
            $this->_helper->layout->disableLayout();

            if ($sname)
            {
                $QRegion = new Application_Model_RegionalMarket();
                $regional_markets = $QRegion->fetchAll();

                $rm = array();
                foreach ($regional_markets as $key => $value)
                {
                    $rm[$value['id']] = $value['name'];
                }
                $this->view->rm_list = $rm;
                $this->view->span = $sname_width;
                $this->_helper->viewRenderer->setRender('partials/searchname');
            } elseif ($s_assign)
            {
                $this->_helper->viewRenderer->setRender('partials/staff');
            } else
                $this->_helper->viewRenderer->setRender('partials/list');
        }

?>