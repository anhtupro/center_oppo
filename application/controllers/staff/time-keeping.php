<?php

    $id = $this->getRequest()->getParam("get_data");
    $sort = $this->getRequest()->getParam('sort', '');
    $desc = $this->getRequest()->getParam('desc', 1);
    
    $flashMessenger = $this->_helper->flashMessenger;
    $QTimekeeping = new Application_Model_Timekeeping(); 
    $db = Zend_Registry::get('db');
    
    //Lấy data
    if($id == 1){
        $result       = $QTimekeeping->getQueryTimeKeeping();
        
        try{
            if($result['ResponseCode'] == 00){
                $sql1 = 'DELETE FROM timekeeping_temp;';
                $sql2 = 'INSERT INTO timekeeping_temp(EnrollNumber, VerifyMode, InOutMode, Date, WorkCode, IdMachine)VALUES
    				 '.$result['Data'];
                $sql3 = "INSERT INTO timekeeping (SELECT a.* FROM timekeeping_temp  AS a LEFT JOIN timekeeping AS b ON a.EnrollNumber = b.EnrollNumber AND a.VerifyMode = b.VerifyMode AND a.InOutMode = b.InOutMode AND a.Date = b.Date AND a.WorkCode = b.WorkCode AND a.IdMachine = b.IdMachine WHERE b.EnrollNumber IS NULL AND b.VerifyMode IS NULL AND b.VerifyMode IS NULL AND b.InOutMode IS NULL and b.Date IS NULL);";
                
                $stmt = $db->query($sql1);
                $stmt = $db->query($sql2);
                $stmt = $db->query($sql3);
                $stmt->execute();
            }
            
        }
        catch (Exception $e) {
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
                
        $flashMessenger->setNamespace('success')->addMessage("Lấy dữ liệu thành công!");
    }
    //end
    

    $page            = $this->getRequest()->getParam('page', 1);
    $name            = $this->getRequest()->getParam('name');
    $total           = 0;
    $limit           = LIMITATION;
    $params = array(
        'name'  => $name,
    );
    $params['sort'] = $sort;
    $params['desc'] = $desc;
    
    $timekeeping = $QTimekeeping->fetchPagination($page, $limit, $total, $params);
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->params = $params;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->url = HOST . 'staff/time-keeping' . ($params ? '?' . http_build_query
        ($params) . '&' : '?');
    $this->view->offset = $limit * ($page - 1);
    
    $this->view->timekeeping = $timekeeping;
    
    $messages = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages;
   
    $messages_loi = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages_loi;
    



