<?php
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    if($this->getRequest()->isXmlHttpRequest())
    {
        $staff_id = $this->getRequest()->getPost('id_update_photo');
  	$uptype   = $this->getRequest()->getPost('uptype');

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR  . DIRECTORY_SEPARATOR . $staff_id ;

  	$off_path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
              DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
              DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR  . $staff_id .
              DIRECTORY_SEPARATOR . 'off_date';

  $arrPhoto = array(
    'file_photo_' . $staff_id => $uploaded_dir,
    'offdate_file_' . $staff_id => $off_path,
    'id_photo' => $uploaded_dir.DIRECTORY_SEPARATOR.'ID_Front',
    'id_photo_back' => $uploaded_dir.DIRECTORY_SEPARATOR.'ID_Back',
  );

            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();
	    if($uptype == "offdate_file"){
	    	$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif,pdf,doc,docx');
	    }else{
	    	$upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
	    }
            $upload->addValidator('Size', false, array('max' => '2MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');

  $update_mode = '';
  $hasPhoto = false;

  foreach ($files as $file => $info) {

    if($file == 'file_photo_' . $staff_id || $file == 'offdate_file_' . $staff_id)
    {

      if (!$upload->isUploaded($file)) {
        $messages = 'Không có ảnh được upload';
        continue;
      }
      if (!$upload->isValid($file)) {
        $array_error = $upload->getMessages();
        if(isset($array_error['fileSizeTooBig']))
        {
          $messages = 'File có dung lượng vượt quá 2MB';
        }
        if(isset($array_error['fileExtensionFalse']))
        {
          $messages = 'File không đúng định dạng';
        }
        continue;
      }
      if( $file == 'file_photo_'.$staff_id ){
        $update_mode = 'file_photo';
      }else{
        $update_mode = 'offdate_file';
      }
                    $hasPhoto = true;
                }
                
            }
            
            if($hasPhoto == true)
            {
                require_once "Aws_s3.php";
                $s3_lib = new Aws_s3();
                foreach ($arrPhoto as $key=>$val)
                {
                    
                        $fileInfo = (isset($files[$key]) and $files[$key]) ? $files[$key] : null;
                        
                        if (isset($fileInfo['name']) and $fileInfo['name']) {
                            
                            if (!is_dir($val))
                                @mkdir($val, 0777, true);

                            $upload->setDestination($val);

                            $old_name = $fileInfo['name'];

                            $tExplode = explode('.', $old_name);
                            $extension = strtolower(end($tExplode));

                            $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                            
                            $upload->addFilter('Rename', array('target' => $val .
                                DIRECTORY_SEPARATOR . $new_name));
                            $r = $upload->receive(array($key));

                            if($fileInfo['size'] > 100000)
                            {
                                $image = new My_SimpleImage();
                                $image->load($val .DIRECTORY_SEPARATOR . $new_name);
                                $image->save($val .DIRECTORY_SEPARATOR . $new_name,'',50);
                            }
                                
                            if ($r){

                                $file1 = $val.DIRECTORY_SEPARATOR.$new_name;
                                $destination = '';
                                if($key == 'file_photo_' . $staff_id) {
                                    $destination = "photo/staff/".$staff_id."/";
                                }else if($key == 'offdate_file_' . $staff_id) {
                                    $destination = "files/staff/".$staff_id."/off_date/";
                                }else if($key == 'id_photo') {
                                    $destination = "photo/staff/".$staff_id."/ID_Front/";
                                }else if($key == 'id_photo_back') {
                                    $destination = "photo/staff/".$staff_id."/ID_Back/";
                                }
                                $s3_lib->uploadS3($file1, $destination, $new_name);
                                $data[$key] = $new_name;
                                $data['is_update_'.$key] = 1;
                                // upload db

                                $QStaff = new Application_Model_Staff();
                                $staff_update = $QStaff->find($staff_id);
                                
                                $params = array();

                                $params['old_name'] = $staff_update[0]->photo;
                                $params['staff_id'] = $staff_update[0]->id;
                                $params['new_name'] = $new_name;
			        if($update_mode == 'file_photo'){
			            $res = $QStaff->updatePhoto($params);
			        }else{
			            $res = $QStaff->updateOffFile($params);
			        }
                                if($res)
                                {
                                    echo json_encode(array('status' => true, 'message' => 'Cập nhật ảnh thành công', 'new_name' => $new_name));
                                }
                                else
                                {
                                    echo json_encode(array('status' => false, 'message' => 'Cập nhật ảnh không thành công'));
                                }
                            }else{
                                $messages = $upload->getMessages();
                                foreach ($messages as $msg)
                                    throw new Exception($msg);
                            }
                        }
                    }
                }
                else
                {
                    echo json_encode(array('status' => false, 'message' => $messages));
                }
    }