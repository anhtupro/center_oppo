<?php
$QStaffCSPriviledge = new Application_Model_StaffCSPriviledge();
$QStaffCSGroup      = new Application_Model_StaffCSGroup();
$flashMessenger     = $this->_helper->flashMessenger;
if ($this->getRequest()->getMethod() == 'POST')
{
    $back_url           = $this->getRequest()->getParam('back_url');
    $db                 = Zend_Registry::get('db');
    $db->beginTransaction();
    try {

        $staff_id       = $this->getRequest()->getParam('staff_id');
        $group_id       = $this->getRequest()->getParam('group_id');
        $default_page   = $this->getRequest()->getParam('default_page');
        $showroomsAction= $this->getRequest()->getParam('showroomsAction');
        $showroomsWHKeeper = $this->getRequest()->getParam('showroomsWHKeeper');
        $updatePrivilege= $this->getRequest()->getParam('updatePrivilege');
        $menus          = $this->getRequest()->getParam('menus');
        $raw_access     = $this->getRequest()->getParam('access');
        $conf           = $this->getRequest()->getParam('conf');
        $access         = array();

        if (is_array($raw_access)) {
            foreach ($raw_access as $module => $item)
                foreach ($item as $controller => $item_2)
                    foreach ($item_2 as $action => $item_3)
                        if ($item_3)
                            $access[] = $module . '::' . $controller . '::' . $action;
        }

        // fetch all old record
        $where              = $QStaffCSPriviledge->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $allPrivilege       = $QStaffCSPriviledge->fetchAll($where);

        foreach($allPrivilege as $v)
            My_Request::sync_table(array('table' => 'staff_cs_priviledge', 'id' => $v->id, 'type' => 'delete'));

        // delete old record
        $where = $QStaffCSPriviledge->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $QStaffCSPriviledge->delete($where);

        $data = array();
        $dataInsertId = array();

        // insert private privilege
        if ($updatePrivilege){
            // default_page
            $data[] = array(
                'staff_id'  => $staff_id,
                'value'     => ($default_page ? $default_page : null),
                'type'      => STAFF_CS_PRIVILEGE_TYPE_DEFAULT_PAGE,
            );
            // menu
            $data[] = array(
                'staff_id'  => $staff_id,
                'value'     => (is_array($menus) ? implode(',', $menus) : null),
                'type'      => STAFF_CS_PRIVILEGE_TYPE_MENU,
            );
            // access
            $data[] = array(
                'staff_id'  => $staff_id,
                'value'     => json_encode($access),
                'type'      => STAFF_CS_PRIVILEGE_TYPE_ACCESS,
            );
        }

        if ($group_id and is_array($group_id))
            foreach ($group_id as $gr)
                $data[] = array(
                    'staff_id'  => $staff_id,
                    'value'     => $gr,
                    'type'      => STAFF_CS_PRIVILEGE_TYPE_GROUP,
                );
        if ($showroomsAction and is_array($showroomsAction))
            foreach ($showroomsAction as $gr)
                $data[] = array(
                    'staff_id'  => $staff_id,
                    'value'     => $gr,
                    'type'      => STAFF_CS_PRIVILEGE_TYPE_SHOWROOM,
                );
        if ($showroomsWHKeeper and is_array($showroomsWHKeeper))
            foreach ($showroomsWHKeeper as $gr)
                $data[] = array(
                    'staff_id'  => $staff_id,
                    'value'     => $gr,
                    'type'      => STAFF_CS_PRIVILEGE_TYPE_WH_KEEPER,
                );

        $data[] = array(
            'staff_id'  => $staff_id,
            'value'     => $conf,
            'type'      => STAFF_CS_PRIVILEGE_TYPE_CONF,
        );

        foreach ($data as $d)
            $dataInsertId[] = $QStaffCSPriviledge->insert($d);

        $db->commit();

        foreach($dataInsertId as $v)
            My_Request::sync_table(array('table' => 'staff_cs_priviledge', 'id' => $v, 'type' => 'insert'));

        $flashMessenger->setNamespace('success')->addMessage('Done!');
    } catch (Exception $e){
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    }
    $this->redirect($back_url ? $back_url : '/staff');
}

$staff_id           = $this->getRequest()->getParam('id');
$this->view->staff_id= $staff_id;

$where              = $QStaffCSPriviledge->getAdapter()->quoteInto('staff_id = ?', $staff_id);
$allPrivilege       = $QStaffCSPriviledge->fetchAll($where);

$pDefaultPage = $pMenu = $pAccess = $pGroup = $pShowroom = $pShowroomWHKeeper = $pConf = array();
if ($allPrivilege->count()){
    $allPrivilege   = $allPrivilege->toArray();

    foreach ($allPrivilege as $pr){
        switch ($pr['type']){
            case STAFF_CS_PRIVILEGE_TYPE_DEFAULT_PAGE:
                $pDefaultPage = $pr['value'];
                break;
            case STAFF_CS_PRIVILEGE_TYPE_MENU:
                $pMenu = $pr['value'];
                break;
            case STAFF_CS_PRIVILEGE_TYPE_ACCESS:
                $pAccess = $pr['value'];
                break;
            case STAFF_CS_PRIVILEGE_TYPE_GROUP:
                $pGroup[] = $pr['value'];
                break;
            case STAFF_CS_PRIVILEGE_TYPE_SHOWROOM:
                $pShowroom[] = $pr['value'];
                break;
            case STAFF_CS_PRIVILEGE_TYPE_WH_KEEPER:
                $pShowroomWHKeeper[] = $pr['value'];
                break;
            case STAFF_CS_PRIVILEGE_TYPE_CONF:
                $pConf = $pr['value'];
                break;
        }
    }
}

$QWS                = new Application_Model_WS();

$groups             = $QStaffCSGroup->fetchAll(null,'name');
$this->view->groups = $groups;

//get group access
if (!$pAccess)
{
    if (isset($pGroup) and is_array($pGroup) and $pGroup){
        $where              = $QStaffCSGroup->getAdapter()->quoteInto('id IN (?)', $pGroup);
        $listStaffGroups = $QStaffCSGroup->fetchAll($where);
        if ($listStaffGroups->count()){
            $groupAccesses = $groupMenus = array();
            foreach ($listStaffGroups as $gr){
                $grAc = isset($gr['access']) ? json_decode($gr['access']) : null;
                if ($grAc)
                    foreach ($grAc as $ga)
                        $groupAccesses[] = $ga;

                $grM = (isset($gr['menu']) and $gr['menu']) ? explode(',', $gr['menu']) : null;
                if ($grM)
                    foreach ($grM as $ga)
                        $groupMenus[] = $ga;
            }
            array_filter($groupAccesses);
            array_filter($groupMenus);
        }
    }

    $assignedMenus = $groupMenus;
    $this->view->accesses = $groupAccesses;
} else
{
    $perAccesses = $perMenus = array();
    if (isset($pAccess) and $pAccess){
        $grAc = @json_decode($pAccess);
        if ($grAc)
            foreach ($grAc as $ga)
                $perAccesses[] = $ga;

        array_filter($perAccesses);
    }

    if (isset($pMenu) and $pMenu){
        $grAc = @explode(',', $pMenu);
        if ($grAc)
            foreach ($grAc as $ga)
                $perMenus[] = $ga;

        array_filter($perMenus);
    }

    $assignedMenus = $perMenus;
    $this->view->accesses = $perAccesses;
}

$this->view->pAccess            = $pAccess;
$this->view->pMenu              = $pMenu;
$this->view->pGroup             = $pGroup;
$this->view->pShowroom          = $pShowroom;
$this->view->pShowroomWHKeeper  = $pShowroomWHKeeper;
$this->view->pConf              = $pConf;

if (isset($pShowroom) and is_array($pShowroom)){
    foreach ($pAccess as $gr){
        $grAc = isset($gr['value']) ? json_decode($gr['value']) : null;
        if ($grAc)
            foreach ($grAc as $ga)
                $perAccesses[] = $ga;

    }
    array_filter($perAccesses);
}

$acl = $QWS->getListActions();
$this->view->acl = $acl;

$showrooms = $QWS->getAllShowroom();
$this->view->showrooms = $showrooms;

//menu
$menus = $QWS->getListMenus();
foreach ($menus as $menu)
    $this->add_row($menu['id'], $menu['parent_id'], $menu['title']);

$this->view->menus = $this->generate_list($assignedMenus);

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');