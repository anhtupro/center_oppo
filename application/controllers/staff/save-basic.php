<?php

$this->_helper->layout()->disableLayout(true);

$back_url = $this->getRequest()->getParam('back_url');
$QStaffAddress = new Application_Model_StaffAddress();
$QRegionalMarket = new Application_Model_RegionalMarket();
if ($this->getRequest()->getMethod() == 'POST') {

    $flashMessenger = $this->_helper->flashMessenger;
    // echo 123;die;
    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    try {
        $QStaffTemp = new Application_Model_StaffTemp();
        $QStaff = new Application_Model_Staff();
        $id = $this->getRequest()->getParam('id');
        $staff_id = $this->getRequest()->getParam('staff_id');
        $photo = $this->getRequest()->getParam('photo');
        $id_photo = $this->getRequest()->getParam('id_photo');
//         $firstname         = $this->getRequest()->getParam('firstname');
//         $lastname          = $this->getRequest()->getParam('lastname');
        $full_name_basic = $this->getRequest()->getParam('full_name_basic');
        $pos = strripos(trim($full_name_basic), " ");
        $firstname = substr(trim($full_name_basic), 0, $pos);
        $lastname = substr(trim($full_name_basic), $pos + 1, strlen(trim($full_name_basic)) - $pos);

        $gender = $this->getRequest()->getParam('gender');
        $department = $this->getRequest()->getParam('department');
        $team = $this->getRequest()->getParam('team');
        $title = $this->getRequest()->getParam('title');
        $joined_at = $this->getRequest()->getParam('joined_at');
        $regional_market = $this->getRequest()->getParam('regional_market');
        // Chuc danh sale set get&set default_province
        $area_id = $this->getRequest()->getParam('area_id');
        if (empty($staff_id) && empty($id) && $title == 183 && !empty($area_id)) {

            $QRegionalMarket = new Application_Model_RegionalMarket();
            $QLogTracking = new Application_Model_LogTracking();
            $where_province = array();
            $where_province[] = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
            $where_province[] = $QRegionalMarket->getAdapter()->quoteInto('default_province = ?', 1);
            $province = $QRegionalMarket->fetchRow($where_province);
            if (!empty($province)) {
                $regional_market = $province->toArray()['id'];
                $data_tracking = array(
                    'name' => 'save_basic_sale',
                    'note' => 'area_id: ' . $area_id . ' regional_market: ' . $regional_market,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id
                );
                $QLogTracking->insert($data_tracking);
            }
        }

        $company_id = $this->getRequest()->getParam('company_id');
        $phone_number = $this->getRequest()->getParam('phone_number');
        $temp_note = $this->getRequest()->getParam('temp_note');
        $tags = $this->getRequest()->getParam('tags');
        $dob = $this->getRequest()->getParam('dob');
        $ID_number = $this->getRequest()->getParam('ID_number');
        $ID_place = $this->getRequest()->getParam('ID_place');
        $ID_date = $this->getRequest()->getParam('ID_date');
        $nationality = $this->getRequest()->getParam('nationality');
        $religion = $this->getRequest()->getParam('religion');
        $marital_status = $this->getRequest()->getParam('marital_status', My_Staff_MaritalStatus::Single);
        $id_place_province = $this->getRequest()->getParam('id_place_province', 0);
        $id_citizen_province = $this->getRequest()->getParam('id_citizen_province', 0);
        $is_print = $this->getRequest()->getParam('is_print', 0);

        // get address
        $temp_address = $this->getRequest()->getParam('temp_address');
        $temp_ward = $this->getRequest()->getParam('temp_ward');
        $temp_district = $this->getRequest()->getParam('temp_district');
        $temp_province = $this->getRequest()->getParam('temp_province');
        $temp_ward_id = $this->getRequest()->getParam('temp_ward_id');

        $perm_address = $this->getRequest()->getParam('perm_address');
        $perm_ward = $this->getRequest()->getParam('perm_ward');
        $perm_district = $this->getRequest()->getParam('perm_district');
        $perm_province = $this->getRequest()->getParam('perm_province');
        $perm_ward_id = $this->getRequest()->getParam('perm_ward_id');

        $birth_ward = $this->getRequest()->getParam('birth_ward');
        $birth_district = $this->getRequest()->getParam('birth_district');
        $birth_province = $this->getRequest()->getParam('birth_province');
        $birth_ward_id = $this->getRequest()->getParam('birth_ward_id');

        $id_card_address = $this->getRequest()->getParam('id_card_address');
        $id_card_ward = $this->getRequest()->getParam('id_card_ward');
        $id_card_district = $this->getRequest()->getParam('id_card_district');
        $id_card_province = $this->getRequest()->getParam('id_card_province');
        $id_card_ward_id = $this->getRequest()->getParam('id_card_ward_id');

        $level = $this->getRequest()->getParam('level');
        $school = $this->getRequest()->getParam('school');
        $field_of_study = $this->getRequest()->getParam('field_of_study');
        $graduated_year = $this->getRequest()->getParam('graduated_year');
        $grade = $this->getRequest()->getParam('grade');
        $mode_of_study = $this->getRequest()->getParam('mode_of_study');
        $default_level = $this->getRequest()->getParam('default_level');

        $company_name = $this->getRequest()->getParam('company_name');
        $job_position = $this->getRequest()->getParam('job_position');
        $from_date = $this->getRequest()->getParam('from_date');
        $to_date = $this->getRequest()->getParam('to_date');
        $reason_for_leaving = $this->getRequest()->getParam('reason_for_leaving');

        $relative_type = $this->getRequest()->getParam('relative_type');
        $full_name = $this->getRequest()->getParam('full_name');
        $rlt_gender = $this->getRequest()->getParam('rlt_gender');
        $birth_year = $this->getRequest()->getParam('birth_year');
        $job = $this->getRequest()->getParam('rlt_job');
        $work_place = $this->getRequest()->getParam('rlt_work_place');
        $rlt_ID_card_number = $this->getRequest()->getParam('rlt_ID_card_number');
        $owner_house = $this->getRequest()->getParam('owner_house2', -100); //trang edit    


        $del_photo = $this->getRequest()->getParam('del_photo', 0);
        $del_id_photo = $this->getRequest()->getParam('del_id_photo', 0);
        $del_id_photo_back = $this->getRequest()->getParam('del_id_photo_back', 0);

        $date_off_purpose = $this->getRequest()->getParam('date_off_purpose', '');
        $date_off_purpose_reason = $this->getRequest()->getParam('date_off_purpose_reason', '');
        $date_off_purpose_detail = $this->getRequest()->getParam('date_off_purpose_detail', '');

        $office_id = $this->getRequest()->getParam('office_id');

        $off_type = $this->getRequest()->getParam('off_type', 0);
        $height = $this->getRequest()->getParam('height');
        $weight = $this->getRequest()->getParam('weight');

        $currentTime = date('Y-m-d H:i:s');
       
        if(empty($full_name_basic) || !in_array($gender, [0,1]) 
           || empty($department) || empty($team) || empty($title) || empty($joined_at)
           || empty($area_id) || empty($regional_market) || empty($office_id)
           || $company_id == '' || empty($dob) || empty($ID_number)
           || (empty($id_place_province) && empty($id_citizen_province))
           || empty($ID_date) || empty($nationality) || empty($religion) || empty($marital_status)
           || empty($id_card_province) || empty($id_card_district) || empty($id_card_ward_id)
           || empty($perm_province) || empty($perm_district) || empty($perm_ward_id)
           || empty($temp_province) || empty($temp_district) || empty($temp_ward_id)
           || empty($birth_province) || empty($birth_district) || empty($birth_ward_id)
        ){
            throw new Exception('You have missing data');
        }
        $postedData = $data = array(
            'firstname' => My_String::trim($firstname),
            'lastname' => My_String::trim($lastname),
            'gender' => intval($gender),
            'department' => intval($department),
            'team' => intval($team),
            'title' => intval($title),
            'joined_at' => $this->_formatDate($joined_at),
            'regional_market' => intval($regional_market),
            'company_id' => intval($company_id),
            'phone_number' => $phone_number,
            'note' => $temp_note,
            'dob' => $dob,
            'ID_number' => $ID_number,
            'ID_place' => $ID_place,
            'ID_date' => $this->_formatDate($ID_date),
            'nationality' => intval($nationality),
            'religion' => intval($religion),
            'marital_status' => intval($marital_status),
            'is_approved' => 0,
            'id_place_province' => intval($id_place_province),
            'id_citizen_province' => intval($id_citizen_province),
            'date_off_purpose' => $this->_formatDate($date_off_purpose),
            'date_off_purpose_reason' => ($date_off_purpose_reason != '') ? $date_off_purpose_reason : null,
            'date_off_purpose_detail' => $date_off_purpose_detail,
            'off_type' => ($off_type != '') ? $off_type : null,
            'is_print' => $is_print,
            'office_id' => intval($office_id),
        );

        $data_body = [
            'height' => !empty($height) ? $height : NULL,
            'weight' => !empty($weight) ? $weight : NULL,
        ];

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        // check quyen view: neu la team sales admin moi gioi han
        if (CHECK_USER_EDIT_AREA) {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QRegionalMarket = new Application_Model_RegionalMarket();
            if (in_array($userStorage->title, array(WESTERN_SALE_ANAGER, SALE_DIRECTOR, NATIONAL_SALE_MANAGER, SALES_ADMIN_TITLE, SALE_SALE_ASM))) {

                $QAsm = new Application_Model_Asm();
                $cachedASM = $QAsm->get_cache($userStorage->id);
                $listAreaIds = $cachedASM['area'];

                $rowset = $QRegionalMarket->find($regional_market);
                $provinceUser = $rowset->current();
                $area_user = $provinceUser['area_id'];

                if (!$listAreaIds)
                    throw new Exception('You don\'t have permission on this Area');

                if (
                        (is_array($listAreaIds) and!in_array($area_user, $listAreaIds))

                        or (!is_array($listAreaIds) and $listAreaIds != $area_user)
                )
                    throw new Exception('You don\'t have permission on this Area');
            }
        }

        $string = "Bạn phải gỡ tất cả các shop của " . $this->getRequest()->getParam('firstname') . " " . $this->getRequest()->getParam('lastname') . " trước khi bật OFF : <a href=" . HOST . 'manage/sales-pg-view?id=' . $this->getRequest()->getParam('staff_id') . ">Tại đây</a> ";

        if ($this->getRequest()->getParam('date_off_purpose') != null && in_array($this->getRequest()->getParam('title'), array(PGPB_TITLE, SALES_TITLE, PG_BRANDSHOP, SENIOR_PROMOTER_BRANDSHOP, CHUYEN_VIEN_BAN_HANG_TITLE, PG_LEADER_TITLE, STORE_LEADER, LEADER_TITLE, 545, 577))) {

            $string = "Bạn phải gỡ tất cả các shop của " . $this->getRequest()->getParam('firstname') . " " . $this->getRequest()->getParam('lastname') . " trước khi bật OFF : <a href=" . HOST . 'manage/sales-pg-view?id=' . $this->getRequest()->getParam('staff_id') . " target='_blank'>Tại đây</a> ";
            $QStoreLeaderLog = new Application_Model_StoreStaffLog();
            $where = $QStoreLeaderLog->getAdapter()->quoteInto('released_at is null and staff_id = ?', $staff_id);
            $stores = $QStoreLeaderLog->fetchAll($where);

            if (count($stores) > 0) {
                throw new Exception($string);
            }
            $QStoreLeaderLog2 = new Application_Model_StoreLeaderLog();
            $wherel = $QStoreLeaderLog2->getAdapter()->quoteInto('released_at is null and staff_id = ?', $staff_id);
            $stores2 = $QStoreLeaderLog2->fetchAll($wherel);
            if (count($stores2) > 0) {
                throw new Exception($string);
            }
        }

        //  Neu co date off va ly do nghi = 1, cap nhat luon vao staff khong doi HR duyet
        /* $noNeedApprove = false;
          if ($data['date_off_purpose_reason'] == 1 && $data['date_off_purpose'] != '') {
          // nếu quá 15 thì phải pendinh ( HR lại thay đổi )
          if (My_Date::date_diff($data['date_off_purpose'], date('Y-m-d')) <= 15) {
          $noNeedApprove = true;
          }
          } */


        $currentStaff = $staffTemp = null;

        if ($id) {
            $where = $QStaffTemp->getAdapter()->quoteInto('id = ?', $id);
            $staffTemp = $QStaffTemp->fetchRow($where);

            $where = $QStaff->getAdapter()->quoteInto('id = ?', $staffTemp['staff_id']);
            $currentStaff = $QStaff->fetchRow($where);

            if ($currentStaff)
                $_tmp_staff = $currentStaff;
            else
                $_tmp_staff = $staffTemp;
        }

        if ($staff_id) {

            $where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
            $currentStaff = $QStaff->fetchRow($where);

            $where = $QStaffTemp->getAdapter()->quoteInto('staff_id = ?', $staff_id);
            $staffTemp = $QStaffTemp->fetchRow($where);

            if ($currentStaff)
                $_tmp_staff = $currentStaff;
            else
                $_tmp_staff = $staffTemp;

            if ($staffTemp) {
                $id = $staffTemp['id'];
            }
        }

        //  Neu co date off va ly do nghi = 1, cap nhat luon vao staff khong doi HR duyet
        $noNeedApprove = false;
        //rule new
        if ($data['date_off_purpose'] != '') {
            $check = false; //pedding
            // 1: Choosen reason 6 and 7
            if (($data['date_off_purpose_reason'] == 6 || $data['date_off_purpose_reason'] == 7)) {
                $check = true;
            }
            //2 
            if (in_array($_tmp_staff['title'], array(181, 190, 191, 462, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674))) {// group pedding
                $check = true;
            }
            //3:date_now - off_date > 03 d
            $date_off_temp = date_create($data['date_off_purpose']);
            $date_now = date_create(date('Y-m-d'));
            if (date_diff($date_off_temp, $date_now)->format('%a') > 3) {
                $check = true;
            }
            //3: month off_date != month now
            if ($date_off_temp->format('m') != $date_now->format('m')) {
                $check = true;
            }
            //4; date_now < off_date
            if ($date_now < $date_off_temp) {
                $check = true;
            }
            //5:Update miss info : Off_date_reason  , off_type , off_date
            if ($data['date_off_purpose_reason'] == null || $data['date_off_purpose_reason'] == null) {
                $check = true;
            }
            // additional inf off date : Off_date , off_date_reason , off_type
            $date_off_purpose_current = isset($staffTemp['date_off_purpose']) ? ($staffTemp['date_off_purpose'] ? date('d/m/Y', strtotime($staffTemp['date_off_purpose'])) : '') : (isset($currentStaff['date_off_purpose']) ? ( $currentStaff['date_off_purpose'] ? date('d/m/Y', strtotime($currentStaff['date_off_purpose'])) : '') : '');
            if (!empty($date_off_purpose_current)) {
                $check = true;
            }
            if ($check == false) {
                $noNeedApprove = true;
            }
        }

// send email
        if ($data['date_off_purpose'] != '' && in_array($userStorage->title, array(191)) && in_array($_tmp_staff['title'], array(179, 181, 308, 190, 391, 392, 462, 463, 464, 465, 466, 467, 471, 533, 534, 535, 536))) {
            //$noNeedApprove = false;
            $mailto = 'chau.phan@oppomobile.vn';
            $subject = 'THÔNG TIN NHÂN SỰ OFF THÁNG ' . date("m", strtotime($data['date_off_purpose'])) . '/' . date("Y", strtotime($data['date_off_purpose']));
            $content_new = '<body lang="EN-US" class="MsgBody MsgBody-html" style="margin: 0px; height: auto; overflow: visible; width: 878px;"><div style=""><div><div class="WordSection1"><p class="MsoNormal" style="line-height: 17pt;"><span style="font-family: &quot;times new roman&quot;, serif;">Dear Chị Châu,</span></p><p class="MsoNormal" style="line-height: 17pt;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">Em gửi Anh/Chị danh sách nhân viên cập nhật off date trong tháng ' . date("m", strtotime($data['date_off_purpose'])) . '/' . date("Y", strtotime($data['date_off_purpose'])) . ':</span></p>'
                    . '<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="1079" style="width: 808.95pt; margin-left: 5.15pt; border-collapse: collapse;">'
                    . '<tbody><tr style="height: 51pt;">'
                    . '<td width="71" nowrap="" style="width: 53pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 9pt; font-family: &quot;times new roman&quot;, serif;">Mã NV</span></b></p></td>'
                    . '<td width="200" nowrap="" style=" border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" style="line-height: 17pt; text-align: center; "><b><span style="font-size: 9pt; font-family: &quot;times new roman&quot;, serif;">Họ tên NV</span></b></p></td>'
                    . '<td width="192" style="width: 2in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(194, 214, 155); padding: 0in 5.4pt; height: 51pt;"><p class="MsoListParagraphCxSpLast" align="center" style="margin-left: 0in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b><span style="font-size: 9pt; line-height: 150%; color: black;">Ngày thay đổi off date </span></b></p></td></tr>';

            $xhtml .= '<tr style="height: 51pt;">'
                    . '<td width="71" nowrap="" style="width: 53pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 9pt; font-family: &quot;times new roman&quot;, serif;">' . $_tmp_staff['code'] . '</span></b></p></td>'
                    . '<td width="89" nowrap="" style="width: 67.1pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" style="line-height: 17pt;"><b><span style="font-size: 9pt; font-family: &quot;times new roman&quot;, serif;">' . $_tmp_staff['firstname'] . ' ' . $_tmp_staff['lastname'] . '</span></b></p></td>'
                    . '<td width="78" style="width: 58.5pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 51pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 17pt;"><b><span style="font-size: 9pt; font-family: &quot;times new roman&quot;, serif;">' . date("d-m-Y", strtotime($data['date_off_purpose'])) . '</span></b></p></td></tr>';

            $end = '</tbody></table><p class="MsoNormal"><span style="color: rgb(31, 73, 125);">&nbsp;</span></p><div><p class="MsoNormal"><i><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">Thanks &amp; Best Regards</span></i><span style="color: black;"></span></p><p class="MsoNormal"><i><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">&nbsp;</span></i><span style="color: black;"></span></p><table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;"><tbody><tr style="height: 29.4pt;">'
                    . '</tbody></table><p class="MsoNormal"><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div><p class="MsoNormal"><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div></div></div></body>';
            $content_full = $content_new . $xhtml . $end;
            $ccStaff = 'phuongnga.nguyen@oppomobile.vn';
            $res = $this->sendmail($mailto, $subject, $content_full, NULL, $ccStaff);
        }
        if ($staffTemp) {
            $where = $QStaffTemp->getAdapter()->quoteInto('id = ?', $staffTemp['id']);

            $data['updated_at'] = $currentTime;
            $data['updated_by'] = $userStorage->id;

            if ($staffTemp['approve_by']) {
                $postedData['is_approved'] = 2;
                $data['is_approved'] = 2;
            }

            //$data['height'] = $data_body['height'];
            //$data['weight'] = $data_body['weight'];

            $staffTempRowset = $QStaffTemp->find($id);
            $s = $staffTempRowset->current();

            $QStaffTemp->update($data, $where);
        } else {

            if (isset($currentStaff['id']) and $currentStaff['id'])
                $data['staff_id'] = $currentStaff['id'];

            $data['created_at'] = $currentTime;
            $data['created_by'] = $userStorage->id;

            //$data['height'] = $data_body['height'];
            //$data['weight'] = $data_body['weight'];

            $id = $QStaffTemp->insert($data);
        }

        $address_data = array();

        if ($id) {

            // ----------------- upload
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $id;

            $arrPhoto = array(
                'photo' => $uploaded_dir,
                'offdate_file' => $uploaded_dir,
                'id_photo' => $uploaded_dir . DIRECTORY_SEPARATOR . 'ID_Front',
                'id_photo_back' => $uploaded_dir . DIRECTORY_SEPARATOR . 'ID_Back',
                'image_body' => $uploaded_dir . DIRECTORY_SEPARATOR . 'IMAGE_body',
            );

            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile' => true));

            //check function
            if (function_exists('finfo_file'))
                $upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf', 'application/zip'));

            $upload->addValidator('Extension', false, 'jpg,jpeg,png,gif,doc,docx');
            $upload->addValidator('Size', false, array('max' => '2MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');
            $files = $upload->getFileInfo();

            $hasPhoto = false;

            $data = array();

            foreach ($arrPhoto as $key => $val) {
                $del = 'del_' . $key;
                if (isset($$del) and $ $del) {
                    $data[$key] = null;
                    $data['is_update_' . $key] = 1;
                } else { // check required photos
                    switch ($key) {
                        case 'id_photo':
                            $_tmp_name = 'ID Card Photo - Front';
                            break;
                        case 'id_photo_back':
                            $_tmp_name = 'ID Card Photo - Back';
                            break;
                        case 'image_body':
                            $_tmp_name = 'Image - Body';
                            break;
                        default:
                            $_tmp_name = 'Photo';
                            break;
                    }

                    if (($key != 'photo' && $key != 'offdate_file') && (!isset($files[$key]['name']) || empty($files[$key]['name'])) && $_tmp_staff && (!isset($_tmp_staff[$key]) || empty($_tmp_staff[$key]))) {
                        //throw new Exception($_tmp_name . ' is required');
                    }
                }

                if (isset($files[$key]['name']))
                    $hasPhoto = true;
            }

            if ($hasPhoto) {

                if (!$upload->isValid()) {
                    $errors = $upload->getErrors();

                    $sError = null;

                    if ($errors and isset($errors[0]))
                        switch ($errors[0]) {
                            case 'fileUploadErrorIniSize':
                            case 'fileFilesSizeTooBig':
                                $sError = 'File size is too large';
                                break;
                            case 'fileMimeTypeFalse':
                            case 'fileExtensionFalse':
                                $sError = 'The file(s) you selected weren\'t the type we were expecting';
                                break;
                            default:
                                $sError = $errors[0];
                                break;
                        }

                    throw new Exception($sError);
                }

                require_once "Aws_s3.php";
                $s3_lib = new Aws_s3();
                foreach ($arrPhoto as $key => $val) {
                    $fileInfo = (isset($files[$key]) and $files[$key]) ? $files[$key] : null;
                    if (isset($fileInfo['name']) and $fileInfo['name']) {

                        if (!is_dir($val))
                            @mkdir($val, 0777, true);

                        $upload->setDestination($val);

                        $old_name = $fileInfo['name'];

                        $tExplode = explode('.', $old_name);
                        $extension = strtolower(end($tExplode));

                        $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

                        $upload->addFilter('Rename', array('target' => $val .
                            DIRECTORY_SEPARATOR . $new_name));

                        $r = $upload->receive(array($key));

                        if ($r) {
                            $file1 = $val . DIRECTORY_SEPARATOR . $new_name;
                            $destination = '';
                            if ($key == 'photo') {
                                $destination = "photo/staff/temp/" . $id . "/";
                            } else if ($key == 'offdate_file') {
                                $destination = "photo/staff/temp/" . $id . "/";
                            } else if ($key == 'id_photo') {
                                $destination = "photo/staff/temp/" . $id . "/ID_Front/";
                            } else if ($key == 'id_photo_back') {
                                $destination = "photo/staff/temp/" . $id . "/ID_Back/";
                            } else if ($key == 'image_body') {
                                $destination = "photo/staff/temp/" . $id . "/IMAGE_body/";
                            }
                            $s3_lib->uploadS3($file1, $destination, $new_name);
                        }
                        if ($r) {
                            $data[$key] = $new_name;
                            $data_body[$key] = $new_name;
                            $data['is_update_' . $key] = 1;
                        } else {
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg)
                                throw new Exception($msg);
                        }
                    }
                }
            }

            if ($data) {
                $whereStaffTemp = $QStaffTemp->getAdapter()->quoteInto('id = ?', $id);
                $QStaffTemp->update($data, $whereStaffTemp);
            }
            // -------------- /upload
            $currentAddresses = [];
            if ($currentStaff) {
                $whereStaffAddress = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $currentStaff['id']);
                $dataAddresses = $QStaffAddress->fetchAll($whereStaffAddress);
                if ($dataAddresses->count()) {
                    foreach ($dataAddresses as $item) {
                        $currentAddresses[$item['address_type']] = array(
                            'staff_id' => $item['staff_id'],
                            'address_type' => $item['address_type'],
                            'address' => $item['address'],
                            'ward' => $item['ward'],
                            'ward_id' => $item['ward_id'],
                            'district' => $item['district']
                        );
                    }
                }
            }
            if ($temp_province) {
                $address_data[My_Staff_Address::Temporary] = array(
                    'address_type' => My_Staff_Address::Temporary,
                    'address' => $temp_address,
                    'ward' => $temp_ward,
                    'district' => $temp_district
                        //'ward_id'      => $temp_ward_id,
                );
                if ($temp_ward_id != 'value_null') {
                    $address_data[My_Staff_Address::Temporary]['ward_id'] = $temp_ward_id;
                } else if ($temp_ward_id == 'value_null' && $currentAddresses[My_Staff_Address::Temporary]) {
                    $address_data[My_Staff_Address::Temporary]['ward_id'] = $currentAddresses[My_Staff_Address::Temporary]['ward_id'];
                }
                if (isset($currentStaff['id']) and $currentStaff['id'])
                    $address_data[My_Staff_Address::Temporary]['staff_id'] = $currentStaff['id'];
            }
            if ($perm_province) {
                $address_data[My_Staff_Address::Permanent] = array(
                    'address_type' => My_Staff_Address::Permanent,
                    'address' => $perm_address,
                    'ward' => $perm_ward,
                    'district' => $perm_district
                        //'ward_id'      => $perm_ward_id
                );
                if ($perm_ward_id != 'value_null') {
                    $address_data[My_Staff_Address::Permanent]['ward_id'] = $perm_ward_id;
                } else if ($perm_ward_id == 'value_null' && $currentAddresses[My_Staff_Address::Permanent]) {
                    $address_data[My_Staff_Address::Permanent]['ward_id'] = $currentAddresses[My_Staff_Address::Permanent]['ward_id'];
                }

                if (isset($currentStaff['id']) and $currentStaff['id'])
                    $address_data[My_Staff_Address::Permanent]['staff_id'] = $currentStaff['id'];
            }

            if ($id_card_province) {
                $address_data[My_Staff_Address::ID_Card] = array(
                    'address_type' => My_Staff_Address::ID_Card,
                    'address' => $id_card_address,
                    'ward' => $id_card_ward,
                    'district' => $id_card_district
                        //'ward_id'      => $id_card_ward_id
                );
                if ($id_card_ward_id != 'value_null') {
                    $address_data[My_Staff_Address::ID_Card]['ward_id'] = $id_card_ward_id;
                } else if ($id_card_ward_id == 'value_null' && $currentAddresses[My_Staff_Address::ID_Card]) {
                    $address_data[My_Staff_Address::ID_Card]['ward_id'] = $currentAddresses[My_Staff_Address::ID_Card]['ward_id'];
                }
                if (isset($currentStaff['id']) and $currentStaff['id'])
                    $address_data[My_Staff_Address::ID_Card]['staff_id'] = $currentStaff['id'];
            }

            if ($birth_province) {
                $address_data[My_Staff_Address::Birth_Certificate] = array(
                    'address_type' => My_Staff_Address::Birth_Certificate,
                    'ward' => $birth_ward,
                    'district' => $birth_district
                        //'ward_id'      => $birth_ward_id
                );
                if ($birth_ward_id != 'value_null') {
                    $address_data[My_Staff_Address::Birth_Certificate]['ward_id'] = $birth_ward_id;
                } else if ($birth_ward_id == 'value_null' && $currentAddresses[My_Staff_Address::Birth_Certificate]) {
                    $address_data[My_Staff_Address::Birth_Certificate]['ward_id'] = $currentAddresses[My_Staff_Address::Birth_Certificate]['ward_id'];
                }
                if (isset($currentStaff['id']) and $currentStaff['id'])
                    $address_data[My_Staff_Address::Birth_Certificate]['staff_id'] = $currentStaff['id'];
            }

            // education
            $education_data = array();
            if ($level) {
                for ($i = 0; $i < count($level); $i++) {
                    $education_data[] = array(
                        'level_id' => $level[$i],
                        'school' => $school[$i],
                        'field_of_study' => $field_of_study[$i],
                        'graduated_year' => intval($graduated_year[$i]),
                        'grade' => $grade[$i],
                        'mode_of_study' => $mode_of_study[$i],
                        'default_level' => (isset($default_level[$i]) AND $default_level[$i] == 1) ? 1 : 0
                    );
                }
            }

            // experience
            $experience_data = array();
            if ($company_name) {
                for ($i = 0; $i < count($company_name); $i++) {
                    if(empty($company_name[$i]) || empty($job_position[$i]) || empty($from_date[$i]) || empty($to_date[$i])){
                        throw new Exception('You check data Experience');
                    }
                    $from_date_tmp = NULL;
                    $to_date_tmp = NULL;
                    if ($from_date[$i]) {
                        $from_date_tmp = My_Date::normal_to_mysql($from_date[$i]);
                    }
                    if ($to_date[$i]) {
                        $to_date_tmp = My_Date::normal_to_mysql($to_date[$i]);
                    }
                    $experience_data[] = array(
                        'company_name' => $company_name[$i],
                        'job_position' => $job_position[$i],
                        'from_date' => $from_date_tmp,
                        'to_date' => $to_date_tmp,
                        'reason_for_leaving' => $reason_for_leaving[$i],
                    );
                }
            }

            // relative
            $relative_data = array();
            $temp_count = 0;
            for ($i = 0; $i < count($relative_type); $i++) {
                $owner_house_main = 0;
                if ($owner_house == $temp_count) {
                    $owner_house_main = 1;
                }
                $temp_count--;
                $relative_data[] = array(
                    'relative_type' => $relative_type[$i],
                    'full_name' => $full_name[$i],
                    'gender' => intval($rlt_gender[$i]),
                    'birth_year' => intval($birth_year[$i]),
                    'job' => $job[$i],
                    'work_place' => $work_place[$i],
                    'id_card_number' => trim($rlt_ID_card_number[$i]),
                    'owner_house' => $owner_house_main,
                );
            }
            $dt = array(
                'address_data' => $address_data,
                'education_data' => $education_data,
                'experience_data' => $experience_data,
                'relative_data' => $relative_data,
            );
            $dataTemp = array(
                'data' => json_encode($dt));

            if ($staff_id) {
                $QStaff = new Application_Model_Staff();
                $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $staff = $QStaff->fetchRow($whereStaff);

                //BODY UPDATE, $id = tempId
                $body_update = [
                    'height' => !empty($data_body['height']) ? $data_body['height'] : $staff['height'],
                    'weight' => !empty($data_body['weight']) ? $data_body['weight'] : $staff['weight'],
                    'image_body' => !empty($data_body['image_body']) ? $data_body['image_body'] : $staff['image_body'],
                ];
                //$QStaff->update( $body_update , $whereStaff );

                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $id;

                $uploadedPrimarydir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $staff_id;

                $body_link_temp = $uploaded_dir . DIRECTORY_SEPARATOR . 'IMAGE_body';
                $body_link_primary = $uploadedPrimarydir . DIRECTORY_SEPARATOR . 'IMAGE_body';

                if (!is_dir($body_link_primary))
                    @mkdir($body_link_primary, 0777, true);

                //copy($body_link_temp . DIRECTORY_SEPARATOR . $data_body['image_body'], $body_link_primary . DIRECTORY_SEPARATOR . $data_body['image_body']);
                //END BODY UPDATE

                $dataTemp['code'] = $staff['code'];
            }

            $postedData['data'] = $dt;

            $whereStaffTemp = $QStaffTemp->getAdapter()->quoteInto('id = ?', $id);
            $QStaffTemp->update($dataTemp, $whereStaffTemp);
        }

        // log thong tin thay doi
        $beforeData = null;
        if ($staffTemp) {
            $beforeData = $staffTemp->toArray();
        } elseif ($currentStaff) {

            $beforeData = $currentStaff->toArray();

            $currentStaffId = $currentStaff['id'];

            // get permanent address

            $all_province_cache = $QRegionalMarket->get_cache();
            $district_cache = $QRegionalMarket->get_district_cache();

            $whereStaffAddress = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $currentStaffId);
            $dataAddresses = $QStaffAddress->fetchAll($whereStaffAddress);
            $currentAddresses = array();
            if ($dataAddresses->count()) {
                foreach ($dataAddresses as $item) {
                    $provinceName = isset($all_province_cache[$district_cache[$item['district']]['parent']]) ? $all_province_cache[$district_cache[$item['district']]['parent']] : '';
                    $districtName = isset($district_cache[$item['district']]['name']) ? $district_cache[$item['district']]['name'] : '';
                    $currentAddresses[$item['address_type']] = array(
                        'staff_id' => $item['staff_id'],
                        'address_type' => $item['address_type'],
                        'address' => $item['address'],
                        'ward' => $item['ward'],
                        'ward_id' => $item['ward_id'],
                        'district' => $item['district'],
                        'province_name' => $provinceName,
                        'district_name' => $districtName,
                    );
                }
            }

            $QStaffEducation = new Application_Model_StaffEducation();
            $whereStaffEducation = $QStaffEducation->getAdapter()->quoteInto('staff_id = ?', $currentStaffId);
            $currentEducationData = $QStaffEducation->fetchAll($whereStaffEducation);
            $staffEducation = array();
            if ($currentEducationData->count())
                $staffEducation = $currentEducationData->toArray();


            $QStaffExperience = new Application_Model_StaffExperience();
            $whereStaffExperience = $QStaffExperience->getAdapter()->quoteInto('staff_id = ?', $currentStaffId);
            $currentExperience = $QStaffExperience->fetchAll($whereStaffExperience);
            $staffExperience = array();
            if ($currentExperience->count())
                $staffExperience = $currentExperience->toArray();

            $QStaffRelative = new Application_Model_StaffRelative();
            $where = $QStaffRelative->getAdapter()->quoteInto('staff_id = ?', $currentStaffId);
            $currentRelative = $QStaffRelative->fetchAll($where);
            $staffRelative = array();
            if ($currentRelative->count())
                $staffRelative = $currentRelative->toArray();

            $beforeData['data'] = array(
                'address_data' => $currentAddresses,
                'education_data' => $staffEducation,
                'experience_data' => $staffExperience,
                'relative_data' => $staffRelative,
            );
        }

        $QStaffTempLog = new Application_Model_StaffTempLog();
        $QStaffTempLog->insert(array(
            'staff_temp_id' => $id,
            'before_data' => json_encode($beforeData),
            'after_data' => json_encode($postedData),
            'created_at' => $currentTime,
            'created_by' => $userStorage->id,
        ));
        // End of log thong tin thay doi

        if ($noNeedApprove == true) {

            $staff_temp_rowset = $QStaffTemp->find($id);
            $staff_temp = $staff_temp_rowset->current();

            // insert photo
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $staff_temp['id'];

            $uploadedPrimarydir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                    DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR . $staff_temp['staff_id'];

            if (!is_dir($uploadedPrimarydir . DIRECTORY_SEPARATOR . 'off_date'))
                @mkdir($uploadedPrimarydir . DIRECTORY_SEPARATOR . 'off_date', 0777, true);

            copy($uploaded_dir . DIRECTORY_SEPARATOR . $staff_temp['offdate_file'], $uploadedPrimarydir . DIRECTORY_SEPARATOR . 'off_date' . DIRECTORY_SEPARATOR . $staff_temp['offdate_file']);

            @unlink($uploaded_dir . DIRECTORY_SEPARATOR . $staff_temp['offdate_file']);


            $data_staff = array();
            $data_staff['off_date'] = $staff_temp['date_off_purpose'];
            $data_staff['off_type'] = $staff_temp['off_type'];
            $data_staff['date_off_purpose'] = $staff_temp['date_off_purpose'];
            $data_staff['date_off_purpose_detail'] = $staff_temp['date_off_purpose_detail'];
            $data_staff['date_off_purpose_reason'] = $staff_temp['date_off_purpose_reason'];
            $data_staff['date_off_purpose_update_by'] = $userStorage->id;

            $edit_staff = $QStaff->find($staff_id);
            if ($data_staff['off_date'] != $edit_staff[0]->off_date || empty($edit_staff[0]->off_date)) {
                $data_staff['date_off_purpose_update_at'] = date('Y-m-d H:i:s');
            }

            $data_staff['offdate_file'] = $staff_temp['offdate_file'];

            //set disabled
            if ($staff_temp['date_off_purpose'] != '') {
                $data_staff['status'] = My_Staff_Status::Off;
                $data_staff['group_id'] = 0;

                try {
                    My_Staff::clear_all_roles($staff_temp['staff_id']);
                    My_Staff::removeStoreForTransfer($staff_temp['staff_id'], $staff_temp['title'], $staff_temp['date_off_purpose']);
                } catch (exception $e) {
                    
                }
            }



            if (empty($s['old_email']))
                $data_staff['old_email'] = $s['email'];

            $data_staff['email'] = null;

            $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_temp['staff_id']);

            $QStaff->update($data_staff, $where_staff);

            $whereStaffTemp = $QStaffTemp->getAdapter()->quoteInto('id = ?', $staff_temp['id']);
            $QStaffTemp->delete($whereStaffTemp);
        }

        $db->commit();
        $this->view->result = "success";
        $this->view->back_url = ($back_url ? $back_url : HOST . 'staff/list-basic');
    } catch (Exception $e) {
        $db->rollback();
        $this->view->error = $e->getMessage();
    }
}
