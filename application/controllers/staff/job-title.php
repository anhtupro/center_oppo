<?php

$export                     = $this->getRequest()->getParam('export');
$QTeam                      = new Application_Model_Team();
$whereDepartment[]          = $QTeam->getAdapter()->quoteInto('parent_id = ?', 0);
$whereDepartment[]          = $QTeam->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
$listDepartment             = $QTeam->fetchAll($whereDepartment);
$this->view->listDepartment = $listDepartment;
$contrac_types              = new Application_Model_ContractTypes();
$listcontract               = $contrac_types->get_cache();
$this->view->listcontract   = $listcontract;

$policy_group                  = new Application_Model_CompanyGroup();
$access_group                  = new Application_Model_Group();
$list_policy_group             = $policy_group->getAll();
$this->view->list_policy_group = $list_policy_group;

$list_company_group             = $policy_group->getAllCompany();
$this->view->list_company_group = $list_company_group;

$QOfficeType             = new Application_Model_OfficeType();
$this->view->officeTypes = $QOfficeType->get_cache();

$list_access_group             = $access_group->get_cache();
$this->view->list_access_group = $list_access_group;

$userStorage                 = Zend_Auth::getInstance()->getStorage()->read();
$QLog                        = new Application_Model_Log();
$ip                          = $this->getRequest()->getServer('REMOTE_ADDR');
$flashMessenger              = $this->_helper->flashMessenger;
$messages                    = $flashMessenger->setNamespace('success')->getMessages();
$this->view->message_success = $messages;
$messages_error              = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages        = $messages_error;

// SEARCH AND EXPORT
if ($this->getRequest()->getMethod() == 'POST' && !empty($export)) {
    $title      = $this->getRequest()->getParam('title');
    $team       = $this->getRequest()->getParam('team');
    $department = $this->getRequest()->getParam('department');
    $total      = 0;
    $params     = array('title' => $title, 'team' => $team, 'department' => $department);


    $result   = $QTeam->getTitleTeamDepartmentList(0, 0, $total, $params);
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'STT',
        'DEPARTMENT',
        'TEAM',
        'TITLE',
        'TITLE VN',
        '1st contract',
        'title id',
        'Policy Group',
        'Campany Group',
        'Access Group',
        'Phép năm',
        'SET OFFICE LOCATION',
        'Phạt chuyên cần',
        'Ẩn'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }
    $index = 2;
    $stt   = 0;
    foreach ($result as $key => $value):
        $alpha = 'A';
        $sheet->getCell($alpha++ . $index)->setValueExplicit(++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['department_name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['team_name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['title_name']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['title_name_vn']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['contract_term']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['policy_group']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['company_group']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(trim($value['access_group']), PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['is_yearly_leave']) ? 'X' : '', PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['office_type']) ? $value['office_type'] : '', PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['diligent']) ? 'X' : '', PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit(!empty($value['is_hidden']) ? 'X' : '', PHPExcel_Cell_DataType::TYPE_STRING);

        $index++;
    endforeach;

    $filename  = 'title_' . date('d-m-Y');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');

    exit;
}

// PROCESS ADD EDIT DELETE
if ($this->getRequest()->getMethod() == 'POST') {
    // Department
    $db = Zend_Registry::get('db');
    $db->beginTransaction();

    try {
        $idDepartment     = $this->getRequest()->getParam('idDepartment');
        $idTeam           = $this->getRequest()->getParam('idTeam');
        $idJobTitle       = $this->getRequest()->getParam('idJobTitle');
        $addDepartment    = $this->getRequest()->getParam('addDepartment');
        $editDepartment   = $this->getRequest()->getParam('editDepartment');
        $delDepartment    = $this->getRequest()->getParam('delDepartment');
        $addTeam          = $this->getRequest()->getParam('addTeam');
        $editTeam         = $this->getRequest()->getParam('editTeam');
        $deleteTeam       = $this->getRequest()->getParam('deleteTeam');
        $addJobTitle      = $this->getRequest()->getParam('addJobTitle');
        $addJobTitleVn    = $this->getRequest()->getParam('addJobTitleVn');
        $addcontract      = $this->getRequest()->getParam('addContract');
        $addPolicyGroup   = $this->getRequest()->getParam('addPolicyGroup');
        $addCompanyGroup  = $this->getRequest()->getParam('addCompanyGroup');
        $addAccessGroup   = $this->getRequest()->getParam('addAccessGroup');
        $editJobTitle     = $this->getRequest()->getParam('editJobTitle');
        $editJobTitleVn   = $this->getRequest()->getParam('editJobTitleVn');
        $editcontract     = $this->getRequest()->getParam('editConrtact');
        $editPolicyGroup  = $this->getRequest()->getParam('editPolicyGroup');
        $editCompanyGroup = $this->getRequest()->getParam('editCompanyGroup');
        $editAccessGroup  = $this->getRequest()->getParam('editAccessGroup');
        $delJobTitle      = $this->getRequest()->getParam('delJobTitle');
        $editleave        = $this->getRequest()->getParam('editleave');
        $addleave         = $this->getRequest()->getParam('addleave');
        $setOffice        = $this->getRequest()->getParam('setOffice');
        $addDiligent      = $this->getRequest()->getParam('addDiligent');
        $editDiligent     = $this->getRequest()->getParam('editDiligent');
        $ishidden         = $this->getRequest()->getParam('ishidden');

        if ($addDepartment) {

            // add
            $dataAdd = array('name' => $addDepartment, 'parent_id' => 0);
            $QTeam->insert($dataAdd);
            //to do log
            $info    = array('Add Department', 'new' => $dataAdd);
            $QLog->insert(array(
                'info'       => json_encode($info),
                'user_id'    => $userStorage->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));
        }
        if ($editDepartment) {

            // edit
            $whereUpdateDepartment = $QTeam->getAdapter()->quoteInto('id = ?', $idDepartment);
            $rowDepartment         = $QTeam->fetchRow($whereUpdateDepartment);
            if ($rowDepartment) {
                $dataUpdate = array('name' => $editDepartment);
                $QTeam->update($dataUpdate, $whereUpdateDepartment);
                //to do log
                $info       = array(
                    'Update Department' => $idDepartment,
                    'new'               => $dataUpdate,
                    'old'               => $rowDepartment);
                $QLog->insert(array(
                    'info'       => json_encode($info),
                    'user_id'    => $userStorage->id,
                    'ip_address' => $ip,
                    'time'       => date('Y-m-d H:i:s'),
                ));
            }
        }
        if ($delDepartment) {

            // del
            $whereDelDepartment = $QTeam->getAdapter()->quoteInto('id = ?', $delDepartment);
            $rowDepartment      = $QTeam->fetchRow($whereDelDepartment);
            if ($rowDepartment) {
                $dataDel = array('del' => 1);
                $QTeam->update($dataDel, $whereDelDepartment);
                //to do log
                $info    = array(
                    'Del Department' => $idDepartment,
                    'new'            => $dataDel,
                    'old'            => $rowDepartment);
                $QLog->insert(array(
                    'info'       => json_encode($info),
                    'user_id'    => $userStorage->id,
                    'ip_address' => $ip,
                    'time'       => date('Y-m-d H:i:s'),
                ));
            }
        }
        if ($addTeam) {

            if (!$idDepartment) {
                throw new Exception('NOT CHOOSE DEPARTMENT');
            }

            $dataAddTeam = array(
                'name'      => $addTeam,
                'parent_id' => $idDepartment,
                'del'       => 0);

            $QTeam->insert($dataAddTeam);

            // to do log
            $info = array('Add Team', 'new' => $dataAddTeam);
            $QLog->insert(array(
                'info'       => json_encode($info),
                'user_id'    => $userStorage->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));
        }
        if ($editTeam) {

            if (!$idTeam) {
                throw new Exception('Team is Not True');
            }
            $whereTeam   = array();
            $whereTeam[] = $QTeam->getAdapter()->quoteInto('id = ?', $idTeam);
            $whereTeam[] = $QTeam->getAdapter()->quoteInto('del <> ?', 1);
            $rowTeam     = $QTeam->fetchRow($whereTeam);

            if ($rowTeam) {
                $dataEditTeam = array('name' => $editTeam);
                $QTeam->update($dataEditTeam, $whereTeam);
                // to do log
                $info         = array(
                    'Edit Team',
                    'new' => $dataEditTeam,
                    'old' => $rowTeam);
                $QLog->insert(array(
                    'info'       => json_encode($info),
                    'user_id'    => $userStorage->id,
                    'ip_address' => $ip,
                    'time'       => date('Y-m-d H:i:s'),
                ));
            } else {
                throw new Exception('Team Is Not True');
            }
        }
        if ($deleteTeam) {
            // delete
            $whereDeleteTeam = $QTeam->getAdapter()->quoteInto('id = ?', $deleteTeam);
            $rowDeleteTeam   = $QTeam->fetchRow($whereDeleteTeam);
            if ($rowDeleteTeam) {
                $dataDel = array('del' => 1);
                $QTeam->update($dataDel, $whereDeleteTeam);

                // to do log
                $info = array(
                    'Del Team',
                    'new' => $dataDel,
                    'old' => $rowDeleteTeam);
                $QLog->insert(array(
                    'info'       => json_encode($info),
                    'user_id'    => $userStorage->id,
                    'ip_address' => $ip,
                    'time'       => date('Y-m-d H:i:s'),
                ));
            } else {
                throw new Exception('Not Team');
            }
        }
        if ($addJobTitle) {
            if (!$idTeam) {
                throw new Exception('NOT CHOOSE TEAM');
            }

            $dataAddJobTitle = array(
                'name'            => $addJobTitle,
                'name_vn'         => $addJobTitleVn,
                'parent_id'       => $idTeam,
                'contract_term'   => $addcontract,
                'policy_group'    => $addPolicyGroup,
                'company_group'   => $addCompanyGroup,
                'access_group'    => $addAccessGroup,
                'del'             => 0,
                'is_yearly_leave' => $addleave,
                'office_type'     => !empty($setOffice) ? $setOffice:null,
                'diligent'        => $addDiligent,
              
            );

            $QTeam->insert($dataAddJobTitle);

            // to do log
            $info = array('Add JobTile', 'new' => $dataAddJobTitle);
            $QLog->insert(array(
                'info'       => json_encode($info),
                'user_id'    => $userStorage->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));
        }

        if ($editJobTitle) {
            echo 'aaaaabb';
            echo $idJobTitle;
            if (!$idJobTitle) {
                throw new Exception('JobTitle is Not True');
            }
            $whereJobTitle   = array();
            $whereJobTitle[] = $QTeam->getAdapter()->quoteInto('id = ?', $idJobTitle);
            $whereJobTitle[] = $QTeam->getAdapter()->quoteInto('del <> ?', 1);
            $rowJobTitle     = $QTeam->fetchRow($whereJobTitle);

            if ($rowJobTitle) {
                $dataEditJobTitle = array(
                    'name'            => $editJobTitle,
                    'name_vn'         => $editJobTitleVn,
                    'contract_term'   => $editcontract,
                    'policy_group'    => $editPolicyGroup,
                    'company_group'   => $editCompanyGroup,
                    'access_group'    => $editAccessGroup,
                    'is_yearly_leave' => $editleave,
                    'office_type'     => $setOffice,
                    'diligent'        => $editDiligent,
                      'is_hidden'=>$ishidden
                );

                $QTeam->update($dataEditJobTitle, $whereJobTitle);
                // to do log
                $info = array(
                    'Edit Team',
                    'new' => $dataEditJobTitle,
                    'old' => $rowJobTitle);
                $QLog->insert(array(
                    'info'       => json_encode($info),
                    'user_id'    => $userStorage->id,
                    'ip_address' => $ip,
                    'time'       => date('Y-m-d H:i:s'),
                ));
            } else {
                throw new Exception('Team Is Not True');
            }
        }
        if ($delJobTitle) {
            // delete
            $whereDeleteJobTitle = $QTeam->getAdapter()->quoteInto('id = ?', $delJobTitle);
            $rowDeleteJobTitle   = $QTeam->fetchRow($whereDeleteJobTitle);
            if ($rowDeleteJobTitle) {
                $dataDel = array('del' => 1);
                $QTeam->update($dataDel, $whereDeleteJobTitle);

                // to do log
                $info = array(
                    'Del Job Title',
                    'new' => $dataDel,
                    'old' => $rowDeleteJobTitle);
                $QLog->insert(array(
                    'info'       => json_encode($info),
                    'user_id'    => $userStorage->id,
                    'ip_address' => $ip,
                    'time'       => date('Y-m-d H:i:s'),
                ));
            } else {
                throw new Exception('Not JobTitle');
            }
        }

        My_Cache::clear_cache();
        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Done!');
        $this->_redirect(HOST . 'staff/job-title');
    } catch (exception $e) {
        $db->rollback();
        $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->_redirect(HOST . 'staff/job-title');
    }
}