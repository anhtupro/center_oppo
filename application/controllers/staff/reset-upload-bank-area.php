<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$flashMessenger       = $this->_helper->flashMessenger;
try {
        
    $QUploadBankAreaTemp = new Application_Model_UploadBankAreaTemp();
    $where = '1';
    $QUploadBankAreaTemp->delete($where);

    $flashMessenger->setNamespace('success')->addMessage('Deleted Success!');
    $this->_redirect(HOST . 'staff/view-bank-area');

} catch (Exception $e) {
        $flashMessenger->setNamespace('error')->addMessage('Error Sytems: '.$e->getMessage());
        $this->_redirect(HOST . 'staff/view-bank-area');
}
