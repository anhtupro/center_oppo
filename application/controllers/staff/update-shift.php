<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$staff_code    = $this->getRequest()->getParam('staff_code');
$shift_type    = $this->getRequest()->getParam('shift_type');

if(!empty($shift_type) AND !empty($staff_code)){
    $QcheckInShift = new Application_Model_CheckInShift();
    $where_shift   = array();
    $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('staff_code = ?' , $staff_code );
    $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('date IS NULL');
    $res_shift = $QcheckInShift->fetchRow($where_shift);
    
    $QShiftType         = new Application_Model_ShiftType();
    $where_shift_type   = $QShiftType->getAdapter()->quoteInto('id = ?' , $shift_type );
    $res_shift_type     = $QShiftType->fetchRow($where_shift_type);
    
    if(!empty($res_shift_type)){
        if(!empty($res_shift)){
            //Update
            $data = array(
                'begin'             => $res_shift_type['begin'],
                'begin_break_time'  => $res_shift_type['begin_break_time'],
                'end'               => $res_shift_type['end'],
                'end_break_time'    => $res_shift_type['end_break_time'],
                'status'            => 1
            );
            
            $QcheckInShift->update($data, $where_shift);
            
        }else{
            //Insert
            
            $data = array(
                'staff_code'        => $staff_code,
                'begin'             => $res_shift_type['begin'],
                'begin_break_time'  => $res_shift_type['begin_break_time'],
                'end'               => $res_shift_type['end'],
                'end_break_time'    => $res_shift_type['end_break_time'],
                'status'            => 1
            );
            
            $QcheckInShift->insert($data);
        }
       
    }

}

echo json_encode(array(
    'status' =>  1,
    'message' =>  'Done'
));
exit();