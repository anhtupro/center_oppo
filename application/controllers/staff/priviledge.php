<?php
$staff_id           = $this->getRequest()->getParam('id');
$QStaffPriveledge   = new Application_Model_StaffPriviledge();
$where              = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $staff_id);
$result             = $QStaffPriveledge->fetchRow($where);
$personal_accesses  = isset($result['access']) ? json_decode($result['access']) : null;

$this->view->id     = isset($result['id']) ? $result['id'] : null;
$this->view->staff_id= $staff_id;

//get group access
if (!$personal_accesses)
{
    $db = Zend_Registry::get('db');
    $select = $db->select()->from(array('p' => 'staff'), array());
    $select->join(array('g' => 'group'), 'p.group_id = g.id', array('g.access'));
    $select->where('p.id = ?', $staff_id);

    $result = $db->fetchRow($select);
    $group_accesses = isset($result['access']) ? json_decode($result['access']) : null;
    $this->view->default_page = isset($result['default_page']) ? $result['default_page'] : null;
    $this->view->accesses = $group_accesses;
    $personal_menus = (isset($result['menu']) and $result['menu']) ? explode(',', $result['menu']) : null;

} else
{

    $personal_menus = (isset($result['menu']) and $result['menu']) ? explode(',', $result['menu']) : null;
    $this->view->accesses = $personal_accesses;
    $this->view->default_page = isset($result['default_page']) ? $result['default_page'] : null;
}

$this->view->staff_id = $staff_id;

//get all controller and action
$cache      = Zend_Registry::get('cache');
$acl     = $cache->load('actions_cache');

if ($acl === false) {

    $front = $this->getFrontController();
    $acl = array();

    foreach ($front->getControllerDirectory() as $module => $path)
    {
        foreach (scandir($path) as $file)
        {
            if (strstr($file, "Controller.php") !== false)
            {

                include_once $path . DIRECTORY_SEPARATOR . $file;

                foreach (get_declared_classes() as $class)
                {

                    if (is_subclass_of($class, 'Zend_Controller_Action'))
                    {

                        $controller = lcfirst(substr($class, 0, strpos($class, "Controller")));
                        $tem = '';

                        for ($i = 0; $i < strlen($controller); $i++)
                        {
                            $char = $controller[$i];
                            if (ord($char) < 97)
                                $tem .= '-' . chr(ord($char) + 32);
                            else
                                $tem .= $char;
                        }
                        $controller = $tem;

                        $actions = array();

                        foreach (get_class_methods($class) as $action)
                        {

                            if (strstr($action, "Action") !== false)
                            {
                                $action = substr($action, 0, strpos($action, "Action"));
                                $tem = '';

                                for ($i = 0; $i < strlen($action); $i++)
                                {
                                    $char = $action[$i];
                                    if (ord($char) < 97)
                                        $tem .= '-' . chr(ord($char) + 32);
                                    else
                                        $tem .= $char;
                                }
                                $actions[] = $tem;
                            }
                        }
                    }
                }

                $acl[$module][$controller] = $actions;
            }
        }
    }

    //set current method
    $actions = array();
    foreach (get_class_methods($this) as $action)
    {
        if (strstr($action, "Action") !== false)
        {
            $action = substr($action, 0, strpos($action, "Action"));
            $tem = '';

            for ($i = 0; $i < strlen($action); $i++)
            {
                $char = $action[$i];
                if (ord($char) < 97)
                    $tem .= '-' . chr(ord($char) + 32);
                else
                    $tem .= $char;
            }
            $actions[] = $tem;
        }
    }

    $acl[$this->getRequest()->getModuleName()][$this->getRequest()->
    getControllerName()] = $actions;
    $cache->save($acl, 'actions_cache', array(), null);
}

$this->view->acl = $acl;

//menu
$QMenu = new Application_Model_Menu();
$where = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);
$menus = $QMenu->fetchAll($where, array('parent_id', 'position'));
foreach ($menus as $menu)
    $this->add_row($menu->id, $menu->parent_id, $menu->title);

$this->view->menus = $this->generate_list($personal_menus);

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');