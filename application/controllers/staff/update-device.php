<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$checkInMachineId = $this->getRequest()->getParam('check_in_machine_id', null);
$enroll_number = $this->getRequest()->getParam('enroll_number');
$ip_machine    = $this->getRequest()->getParam('ip_machine');
$staff_id      = $this->getRequest()->getParam('staff_id');
$status_machine    = $this->getRequest()->getParam('status_machine');
$staff_code    = $this->getRequest()->getParam('staff_code');

$response = [
    'status' => 0,
    'check_in_machine_id' => 0,
    'message' => 'Thất bại'
];

if(!empty($enroll_number) && !empty($ip_machine) && !empty($staff_code)){

    $QCheckInMachineMap = new Application_Model_CheckInMachineMap();
    $where = array();
    $where[] = $QCheckInMachineMap->getAdapter()->quoteInto('enroll_number = ?', $enroll_number);
    $where[] = $QCheckInMachineMap->getAdapter()->quoteInto('ip_machine = ?', $ip_machine);
    
    $result = $QCheckInMachineMap->fetchRow($where);
    if(!empty($result)) {
        if($result['id'] !== $checkInMachineId) {
            $response['status'] = -1;
        } else {
            $data = [
                'status' => $status_machine == 1 ? 1 : 0,
                'ip_machine' => $ip_machine,
                'staff_code' => $staff_code,
                'enroll_number' =>  $enroll_number
            ];
            $QCheckInMachineMap->update($data, [$QCheckInMachineMap->getAdapter()->quoteInto('id = ?', $checkInMachineId)]);
            $response['check_in_machine_id'] = $checkInMachineId;
            $response['status'] = 1;
            $response['message'] = 'Thành công';
        }
    } else {
        if($checkInMachineId == null) {
            $data = [
                'status' => $status_machine == 1 ? 1 : 0,
                'ip_machine' => $ip_machine,
                'staff_code' => $staff_code,
                'enroll_number' =>  $enroll_number
            ];
            $id = $QCheckInMachineMap->insert($data);
            $response['check_in_machine_id'] = $id;
        } else {
            $data = [
                'status' => $status_machine == 1 ? 1 : 0,
                'ip_machine' => $ip_machine,
                'staff_code' => $staff_code,
                'enroll_number' =>  $enroll_number
            ];
            $QCheckInMachineMap->update($data, [$QCheckInMachineMap->getAdapter()->quoteInto('id = ?', $checkInMachineId)]);
            $response['check_in_machine_id'] = $checkInMachineId;
        }
        $response['status'] = 1;
        $response['message'] = 'Thành công';
    }
}
echo json_encode($response);
exit();