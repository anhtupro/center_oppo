<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$staff_id    = $this->getRequest()->getParam('staff_id');
$locked    = $this->getRequest()->getParam('locked');

if(!empty($staff_id)){
    $QStaff = new Application_Model_Staff();
    $where  = $QStaff->getAdapter()->quoteInto('id = ?' , $staff_id );
    
    $data = array(
                'is_locked_photo' => $locked
            );
    $QStaff->update($data, $where);
}

    echo json_encode(array(
        'status' =>  1,
        'message' =>  'Done'
    ));
exit();