<?php
 $id = $this->getRequest()->getParam('id');
    if ($id) {

        $QStaff            = new Application_Model_Staff();
        $staffRowset       = $QStaff->find($id);
        $staff             = $staffRowset->current();
        $this->view->staff = $staff;
        
        $QcheckInShift = new Application_Model_CheckInShift();
        $QCheckIn      = new Application_Model_CheckIn();
        $where_shift   = array();
        $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('staff_code = ?', $staff->code);
        // $where_shift[] = $QcheckInShift->getAdapter()->quoteInto('date IS NULL');
        $res_shift     = $QcheckInShift->fetchRow($where_shift);
        if (!empty($res_shift)) {
            $this->view->staff_shift = $res_shift;
        }
        
         if (!empty($postmachine)) {
                $ip_machine     = $this->getRequest()->getParam('ip_machine');
                $enroll_number  = $this->getRequest()->getParam('enroll_number');
                $status_machine = $this->getRequest()->getParam('status_machine');

                $array_data_machine = array();
                foreach ($enroll_number as $key => $value) {
                    if (!empty($value)) {
                        $data                 = array();
                        $data                 = array(
                            'enroll_number' => $value,
                            'ip_machine'    => $ip_machine[$key]
                        );
                        $data['status']       = ($status_machine[$key] == -1) ? 0 : $status_machine[$key];
                        $array_data_machine[] = $data;
                    }
                }
                $params_machine = array('data' => $array_data_machine, 'code' => $staff->code);
                $QCheckIn->insertData($params_machine);
            }
            
            $data_ip                   = $QCheckIn->getIpMachine();
            $data_staff_ip             = $QCheckIn->getStaffIpMachine($staff->code);
           
            $this->view->data_ip       = $data_ip;
            $this->view->data_staff_ip = $data_staff_ip;

    }