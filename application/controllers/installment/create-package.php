<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$package_id = $this->getRequest()->getParam('package_id');
$channel_id = $this->getRequest()->getParam('channel_id');
$financier_id = $this->getRequest()->getParam('financier_id');
$good_price_log_id = $this->getRequest()->getParam('good_price_log_id');
$prepay_rate_id = $this->getRequest()->getParam('prepay_rate_id');
$pay_term_id = $this->getRequest()->getParam('pay_term_id');
$interest_id = $this->getRequest()->getParam('interest_id');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date', NULL);

$QInstallmentPackage = new Application_Model_InstallmentPackage();

if (!$channel_id || !$financier_id || !$good_price_log_id || !$prepay_rate_id || !$pay_term_id || !$interest_id || !$from_date) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chọn đủ các thông tin !'
    ]);
    return;
}

if ($to_date && date('Y-m-d', strtotime($to_date)) < date('Y-m-d', strtotime($from_date))) {
    echo json_encode([
        'status' => 1,
        'message' => 'Ngày kết thúc không được nhỏ hơn ngày bắt đầu !'
    ]);
    return;
}


$db = Zend_Registry::get('db');
$db->beginTransaction();
if ($package_id) { // update
    $where_update = ['id = ?' => $package_id];
    $QInstallmentPackage->update([
        'channel_id' => $channel_id,
        'financier_id' => $financier_id,
        'good_price_log_id' => $good_price_log_id,
        'prepay_rate_id' => $prepay_rate_id,
        'pay_term_id' => $pay_term_id,
        'interest_id' => $interest_id,
        'from_date' => $from_date,
        'to_date' => $to_date ? $to_date : NULL,
        'updated_at' => date('Y-m-d H:i:s'),
        'updated_by' => $userStorage->id
    ], $where_update);
} else { // insert
    $package_id = $QInstallmentPackage->insert([
        'channel_id' => $channel_id,
        'financier_id' => $financier_id,
        'good_price_log_id' => $good_price_log_id,
        'prepay_rate_id' => $prepay_rate_id,
        'pay_term_id' => $pay_term_id,
        'interest_id' => $interest_id,
        'from_date' => $from_date,
        'to_date' => $to_date ? $to_date : NULL,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $userStorage->id
    ]);
}

// check đã tồn tại gói
$duplicate_package = $QInstallmentPackage->checkDuplicatePackage($package_id, $channel_id, $financier_id, $good_price_log_id, $prepay_rate_id, $pay_term_id, $interest_id, $from_date, $to_date);
if ($duplicate_package) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã có gói trả góp này rồi. Vui lòng kiểm tra lại'
    ]);
    return;
}
// check đã tồn tại gói

$db->commit();

echo json_encode([
    'status' => 0,
    'package_id' => $package_id
]);