<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QInstallmentFinancier = new Application_Model_InstallmentFinancier();
$QStoreStaffLog = new Application_Model_StoreStaffLog();
$QGood = new Application_Model_Good();
$QChannel = new Application_Model_Channel();
$QInstallmentPackage = new Application_Model_InstallmentPackage();

$list_good_price_log = $QGood->getListGoodPriceLog();
$list_financier = $QInstallmentFinancier->getAll();
$list_channel = $QInstallmentPackage->getChannelByTitle();

$this->view->list_good_price_log = $list_good_price_log;
$this->view->list_financier = $list_financier;
$this->view->userStorage = $userStorage;
$this->view->list_channel = $list_channel;
