<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

require_once 'PHPExcel.php';
$QChannel = new Application_Model_Channel();
$PHPExcel = new PHPExcel();

$list_channel = $QChannel->getAll();

$heads = array(
    'Nhà tài chính',
    'Tên sản phẩm',
    'Mã sản phẩm',
    'Màu sản phẩm',
    'Giá',
    'Trả trước(%)',
    'Kỳ hạn(tháng)',
    'Lãi suất(%)',
    'Ngày hiệu lực',
    'Ngày kết thúc'
);

foreach ($list_channel as $channel) {
    $heads [] = $channel['name'];
}

$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}


// data mẫu

    $index    = 2;
    $alpha    = 'A';

    $sheet->setCellValue($alpha++.$index, 'FE CREDIT');
    $sheet->setCellValue($alpha++.$index, 'Reno5');
    $sheet->setCellValue($alpha++.$index, 'CPH2159');
    $sheet->setCellValue($alpha++.$index, '');
    $sheet->setCellValue($alpha++.$index, '8690000');
    $sheet->setCellValue($alpha++.$index, '0%');
    $sheet->setCellValue($alpha++.$index, '3 tháng');
    $sheet->setCellValue($alpha++.$index, '0%');
    $sheet->setCellValue($alpha++.$index, '2021-08-01');
    $sheet->setCellValue($alpha++.$index, '');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');

    $index    = 3;
    $alpha    = 'A';
    $sheet->setCellValue($alpha++.$index, 'HOME CREDIT');
    $sheet->setCellValue($alpha++.$index, 'Reno5');
    $sheet->setCellValue($alpha++.$index, 'CPH2159');
    $sheet->setCellValue($alpha++.$index, 'Đen Marvel');
    $sheet->setCellValue($alpha++.$index, '9690000');
    $sheet->setCellValue($alpha++.$index, '30%');
    $sheet->setCellValue($alpha++.$index, '6 tháng');
    $sheet->setCellValue($alpha++.$index, '0.5%');
    $sheet->setCellValue($alpha++.$index, '2021-08-01');
    $sheet->setCellValue($alpha++.$index, '2021-08-31');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');
    $sheet->setCellValue($alpha++.$index, 'x');


$filename = 'Template installment' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;