<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$package_id = $this->getRequest()->getParam('package_id');

$QInstallmentPackage = new Application_Model_InstallmentPackage();
$QInstallmentLog = new Application_Model_InstallmentLog();

$db = Zend_Registry::get('db');
$db->beginTransaction();
//$row_package = $QInstallmentPackage->fetchRow(['id = ?' => $package_id]);

//if ($row_package) {
//    $row_package = $row_package->toArray();
//
//    $data_log ['good_price_log_id'] = $row_package['good_price_log_id'];
//    $data_log ['pay_term_id'] = $row_package['pay_term_id'];
//    $data_log ['prepay_rate_id'] = $row_package['prepay_rate_id'];
//    $data_log ['interest_id'] = $row_package['interest_id'];
//    $data_log ['channel_id'] = $row_package['channel_id'];
//    $data_log ['from_date'] = $row_package['from_date'];
//    $data_log ['to_date'] = $row_package['to_date'];
//    $data_log ['financier_id'] = $row_package['financier_id'];
//    $data_log ['created_at'] = date('Y-m-d H:i:s');
//    $data_log ['created_by'] = $userStorage->id;
//
//    $QInstallmentLog->insert($data_log);
//} else {
//    echo json_encode([
//        'status' => 1,
//        'message' => 'Không tìm thấy dữ liệu để xóa'
//    ]);
//    return;
//}

$where = ['id IN (?)' => $package_id];
$QInstallmentPackage->delete($where);

$db->commit();

echo json_encode([
    'status' => 0
]);
