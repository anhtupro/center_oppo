<?php
$QInstallmentFinancier = new Application_Model_InstallmentFinancier();
$QInstallmentPrepayRate = new Application_Model_InstallmentPrepayRate();
$QInstallmentPackage = new Application_Model_InstallmentPackage();
$QInstallmentPayTerm = new Application_Model_InstallmentPayTerm();
$QChannel = new Application_Model_Channel();

$good_price_log_id = $this->getRequest()->getParam('good_price_log_id');
$channel_id = $this->getRequest()->getParam('channel_id');
$pay_term_id = $this->getRequest()->getParam('pay_term_id');
$prepay_rate_id = $this->getRequest()->getParam('prepay_rate_id');

$channel = $QChannel->fetchRow(['id = ?' => $channel_id]);
//set default params
$default_package = $QInstallmentPackage->getDefaultPackage($channel_id, $good_price_log_id, $pay_term_id);

if (!$pay_term_id ) {
    $pay_term_id = $default_package['pay_term_id'] ? $default_package['pay_term_id'] : 0;
}

if (!$prepay_rate_id) {
    $prepay_rate_id = $default_package['prepay_rate_id'] ? $default_package['prepay_rate_id'] : 0;
}


//end set default params

$params = [
    'good_price_log_id' => $good_price_log_id,
    'channel_id' => $channel_id,
    'pay_term_id' => $pay_term_id,
    'prepay_rate_id' => $prepay_rate_id,
    'channel_name' => $channel['name']
];


$list_financier = $QInstallmentFinancier->getAll();
//$list_prepay_rate = $QInstallmentPrepayRate->getAll();
//$list_data = $QInstallmentPackage->getData($channel_id, $good_price_log_id, $pay_term_id, $prepay_rate_id);
$list_data = $QInstallmentPackage->getPackage($channel_id, $good_price_log_id, $pay_term_id, $prepay_rate_id);
$list_pay_term = $QInstallmentPayTerm->getTermByModel($channel_id, $good_price_log_id);
$list_prepay_rate = $QInstallmentPrepayRate->getRateByTerm($channel_id, $good_price_log_id, $pay_term_id);


$this->view->list_financier = $list_financier;
$this->view->list_prepay_rate = $list_prepay_rate;
$this->view->list_data = $list_data;
$this->view->params = $params;
$this->view->list_pay_term = $list_pay_term;

$this->_helper->layout()->disablelayout(true);
