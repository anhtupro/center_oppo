<?php

$list_channel_id = $this->getRequest()->getParam('channel_id');
$list_good_price_log_id = $this->getRequest()->getParam('good_price_log_id');
$export = $this->getRequest()->getParam('export');
$from_date = $this->getRequest()->getParam('from_date', date('Y-m-01'));
$to_date = $this->getRequest()->getParam('to_date', date('Y-m-t'));

$QChannel = new Application_Model_Channel();
$QInstallmentFinancier = new Application_Model_InstallmentFinancier();
$QInstallmentPrepayRate = new Application_Model_InstallmentPrepayRate();
$QInstallmentPayTerm = new Application_Model_InstallmentPayTerm();
$QInstallmentInterest = new Application_Model_InstallmentInterest();
$QInstallmentPackage = new Application_Model_InstallmentPackage();
$QGood = new Application_Model_Good();

$params = [
    'list_channel_id' => $list_channel_id,
    'list_good_price_log_id' => $list_good_price_log_id,
    'export' => $export,
    'from_date' => $from_date,
    'to_date' => $to_date
];

$list_channel = $QChannel->getAll($params);
$list_channel_all = $QChannel->getAll();
$list_financier = $QInstallmentFinancier->getAll();
$list_prepay_rate = $QInstallmentPrepayRate->getAll();
$list_pay_term = $QInstallmentPayTerm->getAll();
$list_interest = $QInstallmentInterest->getAll();
$list_good_price_log = $QGood->getListGoodPriceLog();
$list_package = $QInstallmentPackage->getAllByChannel($params);

if ($export) {
    $QInstallmentPackage->export($list_package);
}

$this->view->list_channel = $list_channel;
$this->view->list_channel_all = $list_channel_all;
$this->view->list_financier = $list_financier;
$this->view->list_prepay_rate = $list_prepay_rate;
$this->view->list_pay_term = $list_pay_term;
$this->view->list_interest = $list_interest;
$this->view->list_good_price_log = $list_good_price_log;
$this->view->list_package = $list_package;
$this->view->params = $params;
