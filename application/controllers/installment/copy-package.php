<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$channel_id_from = $this->getRequest()->getParam('channel_id_from');
$channel_id_to = $this->getRequest()->getParam('channel_id_to');

$QInstallmentPackage = new Application_Model_InstallmentPackage();

if ($channel_id_from == $channel_id_to) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không thể copy cùng kênh. Vui lòng chọn kênh khác!'
    ]);
    return;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();
// xóa tất cả các gói của 'copy đến kênh'
$QInstallmentPackage->delete(['channel_id = ?' => $channel_id_to]);

// copy gói mới vào 'copy đến kênh'
$QInstallmentPackage->copyPackage($channel_id_from, $channel_id_to);

$db->commit();

echo json_encode([
    'status' => 0
]);