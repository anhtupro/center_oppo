<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

include 'PHPExcel/IOFactory.php';
// config for excel template excel

require_once "Aws_s3.php";
$s3_lib = new Aws_s3();

$QInstallmentTmpUpload = new Application_Model_InstallmentTmpUpload();

// define row
define('TITLE_ROW', 1);
define('START_ROW', 2);

// define column
define('FINANCIER_NAME', 0);
define('MODEL_NAME', 1);
define('MODEL_CODE', 2);
define('MODEL_COLOR', 3);
define('PRICE', 4);
define('PREPAY_RATE', 5);
define('PAY_TERM', 6);
define('INTEREST', 7);
define('FROM_DATE', 8);
define('TO_DATE', 9);
define('START_CHANNEL_COLUMN', 10);

// get file
$save_folder = 'installment_template';
$requirement = array(
    'Size' => array('min' => 5, 'max' => 15000000),
    'Count' => array('min' => 1, 'max' => 1),
    'Extension' => array('xls', 'xlsx')
);

try {
    $file = My_File::get($save_folder, $requirement);

    if (!$file) {
        echo json_encode([
            'status' => 1,
            'message' => 'Upload failed'
        ]);
        return;
    }
    $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
        . DIRECTORY_SEPARATOR . $file['folder'];
    $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

    // upload image to server s3
//    $destination_s3 = 'files/' . $save_folder . '/' . $file['folder'] . '/';
//    $upload_s3 = $s3_lib->uploadS3($inputFileName, $destination_s3, $file['filename']);
    // upload image to server s3

} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}



//read file
//  Choose file to read
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

// read sheet
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

// read title row(first row)
$row_title = $sheet->rangeToArray('A' . 1 . ':' . $highestColumn . 1, NULL, TRUE, FALSE);
$row_title = $row_title[0];
$end_channel_column = array_key_last($row_title);

try{
    $insert_header_query = "INSERT INTO hr.installment_tmp_upload(excel_row,channel_name, financier_name, good_name, good_code, good_color, price, prepay_rate_name, pay_term_name, interest_name, from_date, to_date) VALUES ";
    $insert_body_query = '';
    for ($row = START_ROW; $row <= $highestRow; ++$row) {
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        $rowData = $rowData[0];

        $financier_name = $rowData[FINANCIER_NAME] ? "'" . TRIM($rowData[FINANCIER_NAME]) . "'" : 'NULL';
        $model_name = $rowData[MODEL_NAME] ? "'" . TRIM($rowData[MODEL_NAME]) . "'" : 'NULL';
        $model_code = $rowData[MODEL_CODE] ? "'" . TRIM($rowData[MODEL_CODE]) . "'" : 'NULL';
        $model_color = $rowData[MODEL_COLOR] ? "'" . TRIM($rowData[MODEL_COLOR]) . "'" : 'NULL';
        $price = $rowData[PRICE] ? TRIM($rowData[PRICE]) : 'NULL';
        $prepay_rate = $rowData[PREPAY_RATE] ? "'" . TRIM($rowData[PREPAY_RATE]) . "'" : 'NULL';
        $pay_term = $rowData[PAY_TERM] ? "'" . TRIM($rowData[PAY_TERM]) . "'" : 'NULL';
        $interest = $rowData[INTEREST] ? "'" . TRIM($rowData[INTEREST]) . "'" : 'NULL';
        $from_date = $rowData[FROM_DATE] ? "'" . TRIM($rowData[FROM_DATE]) . "'" : 'NULL';
        $to_date = $rowData[TO_DATE] ? "'" . TRIM($rowData[TO_DATE]) . "'" : 'NULL';

        // các channel đã tick
        $arr_channel = [];

        for ($col_channel = START_CHANNEL_COLUMN; $col_channel <= $end_channel_column; ++$col_channel) {
            $channel_name = "'" . $row_title[$col_channel] . "'";
            $tick_channel = $rowData[$col_channel];
            if($tick_channel) {
                $value_insert = '('
                    . $row . ','
                    . $channel_name . ','
                    . $financier_name . ','
                    . $model_name . ','
                    . $model_code  .  ','
                    . $model_color  .  ','
                    . $price   .  ','
                    . $prepay_rate  .  ','
                    . $pay_term .  ','
                    . $interest .  ','
                    . $from_date .  ','
                    . $to_date
                    . '),';
                $insert_body_query .= $value_insert;
            }
        }
    }
    
    // insert vào bảng tạm
    if ($insert_body_query) {
        $insert_query = $insert_header_query . $insert_body_query;
        $insert_query = TRIM($insert_query, ',');

        if($insert_query){
            $db = Zend_Registry::get("db");
            $db->query('TRUNCATE TABLE hr.installment_tmp_upload');
            $stmt1 = $db->prepare($insert_query);
            $stmt1->execute();
            $stmt1->closeCursor();
        }
    }

    // update thông tin
    $QInstallmentTmpUpload->updateInfo();

    // kiểm tra lỗi
    //1.Dòng ko đủ thông tin
    $not_enough_info = $QInstallmentTmpUpload->checkNotEnoughInfo();
    if (count($not_enough_info) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Vui lòng điền đầy đủ thông tin tại các dòng trong file: ' . implode(',', $not_enough_info)
        ]);
        return;
    }

    //2.Gói trả trước ko tồn tại
    $prepay_rate_not_exist = $QInstallmentTmpUpload->checkPrepayRate();
    if (count($prepay_rate_not_exist) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Mức trả trước không hợp lệ tại các dòng trong file: ' . implode(',', $prepay_rate_not_exist)
        ]);
        return;
    }

    //3.Kỳ hạn ko tồn tại
    $pay_term_not_exist = $QInstallmentTmpUpload->checkPayTerm();
    if (count($pay_term_not_exist) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Kỳ hạn không hợp lệ tại các dòng trong file: ' . implode(',', $pay_term_not_exist)
        ]);
        return;
    }

    //4. lãi suất ko tồn tại
    $interest_not_exist = $QInstallmentTmpUpload->checkInterest();
    if (count($interest_not_exist) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Lãi suất không hợp lệ tại các dòng trong file: ' . implode(',', $interest_not_exist)
        ]);
        return;
    }

    // 5. check sai mã sp
    $model_code_not_exist = $QInstallmentTmpUpload->checkModelCode();
    if (count($model_code_not_exist) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Mã sản phẩm không hợp lệ tại các dòng trong file: ' . implode(',', $model_code_not_exist)
        ]);
        return;
    }

    // 6. check nhà tài chính ko hợp lệ
    $financier_not_exist = $QInstallmentTmpUpload->checkFinancier();
    if (count($financier_not_exist) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Nhà tài chính không hợp lệ tại các dòng trong file: ' . implode(',', $financier_not_exist)
        ]);
        return;
    }

    // 7. check channel ko tồn tại
    $channel_not_exist = $QInstallmentTmpUpload->checkChannel();
    if (count($channel_not_exist) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Kênh không hợp lệ tại các dòng trong file: ' . implode(',', $channel_not_exist)
        ]);
        return;
    }



    // 8. check good_price_log ko tồn tại
    $price_log_not_exist = $QInstallmentTmpUpload->checkPriceLog();
    if (count($price_log_not_exist) > 0) {
        echo json_encode([
            'status' => 1,
            'message' => 'Giá của sản phẩm chưa được set trên hệ thống SALES, tại các dòng trong file: ' . implode(',', $price_log_not_exist)
        ]);
        return;
    }

    //9. check trùng gói trả góp trong file
    $duplicate_package_in_file = $QInstallmentTmpUpload->checkDuplicatePackageInFile();
    if (count($duplicate_package_in_file)) {
        echo json_encode([
            'status' => 1,
            'message' => 'Trùng gói trả góp trong file tại các dòng trong file: '  . implode(', ', $duplicate_package_in_file)
        ]);
        return;
    }


    //10. check trùng gói trả góp so với bảng gốc
    $duplicate_package = $QInstallmentTmpUpload->checkDuplicatePackage();
    if (count($duplicate_package)) {
        foreach ($duplicate_package as $dup) {
            $message .= 'Trùng gói trả góp với gói có sẵn tại kênh ' . $dup['error_channel'] .  ' tại dòng trong file: ' . $dup['excel_row'] .' ----- ';
        }
        echo json_encode([
            'status' => 1,
            'message' => $message
        ]);
        return;
    }

    //11. check total
    $error_total = $QInstallmentTmpUpload->checkTotal();
    if (count($error_total)) {

        echo json_encode([
            'status' => 1,
            'message' => 'Thông tin chưa đúng tại các dòng trong file: '. implode(', ', $error_total)
        ]);
        return;
    }

    // insert table chính
    $QInstallmentTmpUpload->insertIntoMainTable();


}catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}


$db = Zend_Registry::get("db");
$db->beginTransaction();

$db->commit();

echo json_encode([
    'status' => 0
]);