<?php


/**
 * Các report của phòng Sales
 */
class SalesReportController extends My_Controller_Action {

    public function timingDetailAction() {
        $sort            = $this->getRequest()->getParam('sort', 'product_count');
        $desc            = $this->getRequest()->getParam('desc', 1);
        $from            = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to              = $this->getRequest()->getParam('to', date('d/m/Y'));
        $phone_number    = $this->getRequest()->getParam('phone_number');
        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $limit = LIMITATION;

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'phone_number'    => $phone_number,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'export2'         => 1,
            'distributor_id'  => $distributor_id,
        );

        $total = 0;

        $QTiming = new Application_Model_Timing();
        $data    = $QTiming->report(1, null, $total, $params);

        My_Report_Sales::timingDetail($data);
    }

    public function storeAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $page               = $this->getRequest()->getParam('page', 1);
        $sort               = $this->getRequest()->getParam('sort', 'product_count');
        $desc               = $this->getRequest()->getParam('desc', 1);
        $from               = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to                 = $this->getRequest()->getParam('to', date('d/m/Y'));
        $name               = $this->getRequest()->getParam('name');
        $area               = $this->getRequest()->getParam('area');
        $province           = $this->getRequest()->getParam('province');
        $district           = $this->getRequest()->getParam('district');
        $store              = $this->getRequest()->getParam('store');
        $store_level        = $this->getRequest()->getParam('store_level');
        $channel            = $this->getRequest()->getParam('channel');
        $has_pg             = $this->getRequest()->getParam('has_pg');
        $sales_from         = $this->getRequest()->getParam('sales_from');
        $sales_to           = $this->getRequest()->getParam('sales_to');
        $distributor_id     = $this->getRequest()->getParam('distributor_id');
        $include_0_sell_out = $this->getRequest()->getParam('include_0_sell_out');

        $params = array(
            'page'               => $page,
            'sort'               => $sort,
            'desc'               => $desc,
            'from'               => $from,
            'to'                 => $to,
            'name'               => $name,
            'area'               => $area,
            'province'           => $province,
            'district'           => $district,
            'store'              => $store,
            'has_pg'             => $has_pg,
            'sales_from'         => $sales_from,
            'sales_to'           => $sales_to,
            'distributor_id'     => $distributor_id,
            'export'             => 1,
            'store_level'        => $store_level,
            'channel'            => $channel,
            'include_0_sell_out' => $include_0_sell_out
        );

         $dateFrom =  date_create_from_format("d/m/Y", $from)->format("Y-m-d");
         $dateTo   =  date_create_from_format("d/m/Y", $to)->format("Y-m-d");
         $dateDiff = My_Date::date_diff($dateFrom,$dateTo);
         if($dateDiff > 92 ){
             echo '<script>
                         alert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 90 ngày.");
                         palert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 90 ngày.");
                     </script>';
             exit;
         }

        $limit       = LIMITATION;
        $total       = 0;
        $show_value  = 1;
        $QImeiKpi    = new Application_Model_ImeiKpi();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userStorage || !isset($userStorage->id))
            $this->_redirect(HOST);
        $group_id    = $userStorage->group_id;

// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($userStorage);
//     exit;
// }
// if(empty($_GET['dev']) && $userStorage->id != 341 && $userStorage->id != 5899)
//     exit('Chức năng này tạm khóa');
        // if(in_array($userStorage->title, array(SALE_SALE_ASM, SALE_SALE_ASM_STANDBY, 308)) || in_array($userStorage->group_id, array(ADMINISTRATOR_ID, 11)))
        // {
        //     $show_value = 0;
        // }
        // elseif (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
        // lấy khu vực của asm
        if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
            $QAsm                = new Application_Model_Asm();
            $asm_cache           = $QAsm->get_cache();
            $params['area_list'] = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();
            $show_value          = 1;
        } elseif ($group_id == My_Staff_Group::SALES) {
            // lấy cửa hàng của sale
            $QStoreStaffLog       = new Application_Model_StoreStaffLog();
            $store_cache          = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
            $params['store_list'] = $store_cache;
            $params['sale_id']    = $userStorage->id;
            $show_value           = 0;
        } elseif ($group_id == My_Staff_Group::LEADER) {
            // lấy cửa hàng của sale
            $QStoreLeaderLog      = new Application_Model_StoreLeaderLog();
            $store_cache          = $QStoreLeaderLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
            $params['store_list'] = $store_cache;
            $params['leader_id']  = $userStorage->id;
            $show_value           = 0;
        }
        if (in_array($userStorage->title, array(418, 412))) {
            // BRAND SHOP MANAGER
            $params['is_brand_shop'] = 1;
        }
        // echo $show_value; die;
        $params['kpi'] = 1;
        $result        = $QImeiKpi->fetchStore(null, null, $total, $params);
// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($show_value);die;
// }
        PC::debug($result);

        My_Report_Sales::store($params['from'], $params['to'], $result, $show_value);
    }

    public function dealerAction() {
        $from       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $name       = $this->getRequest()->getParam('name');
        $area       = $this->getRequest()->getParam('area');
        $province   = $this->getRequest()->getParam('province');
        $district   = $this->getRequest()->getParam('district');
        $sales_from = $this->getRequest()->getParam('sales_from');
        $sales_to   = $this->getRequest()->getParam('sales_to');

        $params = array(
            'from'       => $from,
            'to'         => $to,
            'name'       => $name,
            'area'       => $area,
            'province'   => $province,
            'district'   => $district,
            'sales_from' => $sales_from,
            'sales_to'   => $sales_to,
            'export'     => 1,
        );

        $limit = LIMITATION;
        $total = 0;

        $params['kpi'] = 1;
        $QImeiKpi      = new Application_Model_ImeiKpi();
        $sales         = $QImeiKpi->fetchDistributor(null, null, $total, $params);
        
        My_Report_Sales::dealer($params['from'], $params['to'], $sales);
    }

    public function dealerAllAction() {
        $from       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $name       = $this->getRequest()->getParam('name');
        $area       = $this->getRequest()->getParam('area');
        $province   = $this->getRequest()->getParam('province');
        $district   = $this->getRequest()->getParam('district');
        $sales_from = $this->getRequest()->getParam('sales_from');
        $sales_to   = $this->getRequest()->getParam('sales_to');

        $params = array(
            'from'       => $from,
            'to'         => $to,
            'name'       => $name,
            'area'       => $area,
            'province'   => $province,
            'district'   => $district,
            'sales_from' => $sales_from,
            'sales_to'   => $sales_to,
            'export'     => 1,
            'kpi'        => 1,
        );

        $limit = LIMITATION;
        $total = 0;

        $params['kpi'] = 1;
        $QImeiKpi      = new Application_Model_ImeiKpi();
        $sales         = $QImeiKpi->fetchDistributor(null, null, $total, $params);

        My_Report_Sales::dealer($params['from'], $params['to'], $sales);

    }

    public function dealerKaAction() {
        $this->_helper->viewRenderer->setNoRender();
        $from       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $name       = $this->getRequest()->getParam('name');
        $area       = $this->getRequest()->getParam('area');
        $province   = $this->getRequest()->getParam('province');
        $district   = $this->getRequest()->getParam('district');
        $sales_from = $this->getRequest()->getParam('sales_from');
        $sales_to   = $this->getRequest()->getParam('sales_to');
        
        $QStaffChannel    = new Application_Model_StaffChannel();
        
        $params = array(
            'from'       => $from,
            'to'         => $to,
            'name'       => $name,
            'area'       => $area,
            'province'   => $province,
            'district'   => $district,
            'sales_from' => $sales_from,
            'sales_to'   => $sales_to,
            'export'     => 1,
            'kpi'        => 1,
        );
      
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $name_chart = $QStaffChannel->get_staff_channel($userStorage->id);
        $channel = [];
        foreach ($name_chart as $key => $value) {
                $channel[] = $key;
        }
//        $params['channel'] = $channel;
		
        $limit = LIMITATION;
        $total = 0;


        $params['kpi'] = 1;
        $QImeiKpi      = new Application_Model_ImeiKpi();
//        $sales         = $QImeiKpi->fetchDistributorKa(null, null, $total, $params);
        $sales         = $QImeiKpi->fetchDistributorKaNew(null, null, $total, $params);

        My_Report_Sales::dealer_ka_New($params['from'], $params['to'], $sales);
        
        

    }

    public function areaAction() {
      
        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to   = $this->getRequest()->getParam('to', date('d/m/Y'));
        $area = $this->getRequest()->getParam('area');

        $params = array(
            'from' => $from,
            'to'   => $to,
            'area' => $area,
        );

        $QImeiKpi = new Application_Model_ImeiKpi();
        $QArea    = new Application_Model_Area();

        $params['kpi'] = true;
//         $params['dev'] = 1;
        
        $sell_out = $QImeiKpi->fetchArea($params);

        unset($params['kpi']);
        $data_for_point            = $QImeiKpi->fetchArea($params);
        $params['get_total_sales'] = true;

        $total_sales = $QImeiKpi->fetchArea($params);
        $total_money = $total_sales['total_value'];
        $point_list  = array();

        $all_area     = $QArea->fetchAll();
        $region_share = array();

        foreach ($all_area as $_key => $_value)
            $region_share[$_value['id']] = $_value['region_share'];

        //tính point
        foreach ($data_for_point as $item)
            $point_list[$item['area_id']] = ( $total_money > 0 and ( $region_share[$item['area_id']] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($region_share[$item['area_id']] / 100), 2) : 0;

			
        My_Report_Sales::area($sell_out, $total_sales, $point_list, $params);
    }

    public function areaProvinceAction() {
        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to   = $this->getRequest()->getParam('to', date('d/m/Y'));
        $area = $this->getRequest()->getParam('area');

        $params = array(
            'from' => $from,
            'to'   => $to,
            'area' => $area,
        );

        $QImeiKpi = new Application_Model_ImeiKpi();
        $QArea    = new Application_Model_Area();

        $params['kpi'] = true;

        $sell_out = $QImeiKpi->fetchAreaProvince($params);

        unset($params['kpi']);
        $data_for_point            = $QImeiKpi->fetchAreaProvince($params);
        $params['get_total_sales'] = true;
        $total_sales               = $QImeiKpi->fetchAreaProvince($params);
        $total_money               = $total_sales['total_value'];
        $point_list                = array();

        $all_area     = $QArea->fetchAll();
        $region_share = array();

        foreach ($all_area as $_key => $_value)
            $region_share[$_value['id']] = $_value['region_share'];

        //tính point
        foreach ($data_for_point as $item)
            $point_list[$item['area_id']] = ( $total_money > 0 and ( $region_share[$item['area_id']] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($region_share[$item['area_id']] / 100), 2) : 0;

        My_Report_Sales::area($sell_out, $total_sales, $point_list, $params);
    }

    public function leaderAction() {
        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to   = $this->getRequest()->getParam('to', date('d/m/Y'));
        $area = $this->getRequest()->getParam('area');

        $params = array(
            'from' => $from,
            'to'   => $to,
            'area' => $area,
        );

        My_Report_Sales::leader($params);
    }

    public function storeListAction() {
        $name            = $this->getRequest()->getParam('name');
        $address         = $this->getRequest()->getParam('address');
        $area_id         = $this->getRequest()->getParam('area_id');
        $staff_email     = $this->getRequest()->getParam('staff_email');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');

        $have_pg    = $this->getRequest()->getParam('have_pg', 0);
        $have_sales = $this->getRequest()->getParam('have_sales', 0);
        $no_staff   = $this->getRequest()->getParam('no_staff', 0);

        $total = 0;

        $params = array_filter(array(
            'name'            => $name,
            'address'         => $address,
            'area_id'         => $area_id,
            'regional_market' => $regional_market,
            'district'        => $district,
            'staff_email'     => $staff_email,
            'have_pg'         => $have_pg,
            'no_staff'        => $no_staff,
            'have_sales'      => $have_sales,
        ));

        $params['export'] = 1;

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $group_id    = $userStorage->group_id;

        if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
            $params['asm'] = $userStorage->id;
        } elseif ($group_id == SALES_ID) {
            $params['sales_store'] = $userStorage->id;
        } elseif ($group_id == LEADER_ID) {
            $params['leader_province'] = $userStorage->id;
        }

        $QModel = new Application_Model_Store();
        $stores = $QModel->fetchPagination(1, null, $total, $params);

        My_Report_Sales::storeList($stores);
    }

    public function historicalDeductionAction() {
        $name            = $this->getRequest()->getParam('name');
        $loyalty_plan_id = $this->getRequest()->getParam('loyalty_plan_id');
        $month           = $this->getRequest()->getParam('month');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $export          = $this->getRequest()->getParam('export', 0);
        $page            = $this->getRequest()->getParam('page', 1);
        $limit           = LIMITATION;
        $total           = 0;
        $params          = array(
            'name'            => $name,
            'loyalty_plan_id' => $loyalty_plan_id,
            'month'           => $month,
            'area'            => $area,
            'regional_market' => $regional_market,
            'export'          => $export,
        );

        $QLoyaltyPlan = new Application_Model_LoyaltyPlan();
        $level_cache  = $QLoyaltyPlan->get_all_cache();

        $QHistoricalSellinDeduction = new Application_Model_HistoricalSellinDeduction();
        $sellin                     = $QHistoricalSellinDeduction->fetchPagination($page, $limit, $total, $params);

        My_Report_Sales::historicalDeduction($sellin);
    }

    public function historicalIncentiveAction() {
        $name            = $this->getRequest()->getParam('name');
        $loyalty_plan_id = $this->getRequest()->getParam('loyalty_plan_id');
        $month           = $this->getRequest()->getParam('month');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $export          = $this->getRequest()->getParam('export', 0);
        $page            = $this->getRequest()->getParam('page', 1);
        $limit           = LIMITATION;
        $total           = 0;
        $params          = array(
            'name'            => $name,
            'loyalty_plan_id' => $loyalty_plan_id,
            'month'           => $month,
            'area'            => $area,
            'regional_market' => $regional_market,
            'export'          => $export,
        );

        $QLoyaltyPlan = new Application_Model_LoyaltyPlan();
        $level_cache  = $QLoyaltyPlan->get_all_cache();

        $QHistoricalIncentive = new Application_Model_HistoricalIncentive();
        $sellin               = $QHistoricalIncentive->fetchPagination($page, $limit, $total, $params);

        My_Report_Sales::historicalIncentive($sellin);
    }

    public function reportTargetAction() {
        
    }
    
    public function leaderByQtyAction() {
        require_once 'sales-report'.DIRECTORY_SEPARATOR.'leader-by-qty.php';
    }
	 public function areaByQtyAction() {
        require_once 'sales-report'.DIRECTORY_SEPARATOR.'area-by-qty.php';
    }
	public function kpiByPolicyAction() {
        require_once 'sales-report'.DIRECTORY_SEPARATOR.'kpi-by-policy.php';
    }
	
	public function storeByQtyAction() {
        require_once 'sales-report'.DIRECTORY_SEPARATOR.'store-by-qty.php';
    }


}
