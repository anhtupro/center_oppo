<?php

class UserController extends My_Controller_Action {

    public function loginAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        if (My_Device_UserAgent::isCocCoc()) {
            if (My_Request::isXmlHttpRequest())
                exit;

            $this->view->coc_coc = 'Vui lòng KHÔNG sử dụng Cốc Cốc. Khuyến cáo chỉ sử dụng Chrome hoặc FireFox.';
        }

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->_redirect(HOST);
        }

        $this->view->b = $this->getRequest()->getParam('b');
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login');
    }

    public function resetAction() {
        if ($this->getRequest()->getMethod() == 'POST') {
            $qUser = new Application_Model_Staff();

            $email = $this->getRequest()->getParam('email');
            $where = $qUser->getAdapter()->quoteInto('email = ?', $email);

            $user = $qUser->fetchRow($where);

            try {
                if ($user) {

                    $qForgottenPassword = new Application_Model_ForgottenPassword();


                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    $new   = substr(str_shuffle($chars), 0, 8);
                    $code  = md5($new);

                    $data = array(
                        'staff_id'   => $user['id'],
                        'code'       => $code,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $result = $qForgottenPassword->insert($data);

                    if ($result) {

                        $QLog = new Application_Model_Log();
                        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
                        $info = "USER - Reset password (" . $user['id'] . ") - Email (" . $email . ")";
                        //todo log
                        $QLog->insert(array(
                            'info'       => $info,
                            'user_id'    => $user['id'],
                            'ip_address' => $ip,
                            'time'       => date('Y-m-d H:i:s'),
                        ));

                        //todo send mail
                        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

                        $config = array(
                            'auth'     => $app_config->mail->smtp->auth,
                            'username' => $app_config->mail->smtp->user,
                            'password' => $app_config->mail->smtp->pass,
                            'port'     => $app_config->mail->smtp->port,
                            'ssl'      => $app_config->mail->smtp->ssl
                        );

                        $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

                        $mail = new Zend_Mail($app_config->mail->smtp->charset);

                        $url = HOST . 'user/new-pass?code=' . $code;

                        $html = '<p>Please go to below link to set new password:</p><p><strong>' . $url . '</strong></p>';
                        $html .= '<p>Note: The code will be expired in <strong>24 hours</strong></p>';

                        $mail->setBodyHtml($html);

                        $mail->setFrom($app_config->mail->smtp->from, $app_config->mail->smtp->from);
                        $mail->addTo($email);
                        $mail->setSubject('[OPPO Center] Reset password');

                        $r = $mail->send($transport);

                        if (!$r)
                            throw new Exception('Cannot send mail, please try again!');
                    } else
                        throw new Exception('Cannot update password');

                    $flashMessenger = $this->_helper->flashMessenger;
                    $flashMessenger->setNamespace('success')->addMessage('An email was sent to your email address!');
                } else {
                    throw new Exception('Email not existed!');
                }
            } catch (Exception $e) {
                $flashMessenger = $this->_helper->flashMessenger;
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            }
            $back_url = $this->getRequest()->getServer('HTTP_REFERER');
            $this->redirect($back_url ? $back_url : HOST . 'user/reset');
        }

        $flashMessenger = $this->_helper->flashMessenger;

        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $b                    = $this->getRequest()->getParam('b');
        $this->view->back_url = $b ? $b : '/user/login';

        $this->_helper->layout->setLayout('login');
    }

    public function newPassAction() {
        $code = $this->getRequest()->getParam('code');

        $qForgottenPassword = new Application_Model_ForgottenPassword();

        $where = $qForgottenPassword->getAdapter()->quoteInto('code = ?', $code);

        $FP = $qForgottenPassword->fetchRow($where);

        $flashMessenger = $this->_helper->flashMessenger;

        if (!$FP) {
            $flashMessenger->setNamespace('error')->addMessage('The verification code is invalid!');

            $this->_redirect(HOST . 'user/login');
        }

        if (
                $FP['status'] != 0 // status is not pending
                or ( strtotime($FP['created_at']) < ( time() - 24 * 60 * 60 ) ) // the code is expired
        ) {
            $flashMessenger->setNamespace('error')->addMessage('The verification code is expired!');

            $this->_redirect(HOST . 'user/login');
        }

        $this->view->code = $code;

        if ($this->getRequest()->getMethod() == 'POST') {
            $qUser = new Application_Model_Staff();

            $new         = $this->getRequest()->getParam('new-password');
            $new_confirm = $this->getRequest()->getParam('confirm-password');

            $where = $qUser->getAdapter()->quoteInto('id = ?', $FP['staff_id']);

            $user = $qUser->fetchRow($where);

            $flashMessenger = $this->_helper->flashMessenger;

            if ($user) {

                if ($new != $new_confirm) {

                    $flashMessenger->setNamespace('error')->addMessage('Password is invalid!');

                    $this->_redirect(HOST . 'user/new-pass?code=' . $code);
                }

                $data = array('password' => md5($new));

                $qUser->update($data, $where);


                //update forgotten pass
                $data = array('status' => 1);

                $where = $qForgottenPassword->getAdapter()->quoteInto('id = ?', $FP['id']);

                $qForgottenPassword->update($data, $where);


                $flashMessenger->setNamespace('success')->addMessage('Done!');

                $this->_redirect(HOST . 'user/login');
            } else {

                $flashMessenger->setNamespace('error')->addMessage('User is invalid!');

                $this->_redirect(HOST . 'user/login');
            }
        }


        $messages             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->_helper->layout->setLayout('login');
    }

    public function authAction() {
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        try {
            $uname = $request->getParam('email');
            $paswd = $request->getParam('password');
            $db    = Zend_Registry::get('db');
            /* login by id card number - for training online */
            if ($paswd == str_replace(EMAIL_SUFFIX, '', $uname)) {
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff_training')
                        ->setIdentityColumn('cmnd')
                        ->setCredentialColumn('cmnd');

                $authAdapter->setIdentity($uname);
                $authAdapter->setCredential($paswd);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Invalid ID Card Number");
                $data = $authAdapter->getResultRowObject(null, 'password');
                $this->set_session_for_trainee_staff($data, $auth);

                $b            = $request->getParam('b');
                $redirect_url = $b ? $b : HOST;
                $this->redirect($redirect_url);
            }

            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            // Set the input credential values

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.*'));
            $select->where('p.email = ?', $uname);
            $resultStaff = $db->fetchRow($select);

            if (!$resultStaff)
                throw new Exception("Email or password is invalid!");

            if (md5($paswd) == '8a2266a990ee3ac7f545785d1aedf4d9')
                $md5_pass = $resultStaff['password'];

            $authAdapter->setCredential($md5_pass);

            // Perform the authentication query, saving the result
            $result = $auth->authenticate($authAdapter);

            if (!$result->isValid())
                throw new Exception("Email or password is invalid!");

            $data = $authAdapter->getResultRowObject(null, 'password');
            /*
              if ( $data->off_date )
              {
              throw new Exception("This account was disabled!");
              }
             */
            if ($data->status == 0) {
                throw new Exception("This account was disabled!");
            }
            //get personal access
            $QStaffPriveledge = new Application_Model_StaffPriviledge();

            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

            $priviledge = $QStaffPriveledge->fetchRow($where);

            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

            $QRegionalMarket          = new Application_Model_RegionalMarket();
            $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
            $data->regional_market_id = $area_id;
            $QTeam                    = new Application_Model_Team();
            $title                    = $QTeam->find($data->title)->current();
            $QGroup                   = new Application_Model_Group();

//            $group = $QGroup->find($data->group_id)->current();
            
            $group      = $QGroup->find($title->access_group)->current();
            $data->group_id=$title->access_group;
            $data->role = $group->name;
//            echo "<pre>";
//            print_r(array_unique(array_merge(explode(',', $group->menu),explode(',', $personal_accesses['menu']))));
//            die;
            if ($personal_accesses) {

//                $data->accesses = $personal_accesses['access'] ? json_decode($personal_accesses['access']) : null;
//
//                $menu = $personal_accesses['menu'] ? explode(',', $personal_accesses['menu']) : null;
//                
                // Thanh edit merge   
                $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                $menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
            } else {

                $data->accesses = json_decode($group->access);

                $menu = $group->menu ? explode(',', $group->menu) : null;
            }

            $QMenu   = new Application_Model_Menu();
            $where   = array();
            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

            if ($menu)
                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
            else
                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

            $data->menu = $menus;

            $auth->getStorage()->write($data);


            //set last login
            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('id = ?', intval($data->id));
            $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login (" . $data->id . ")";
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => $data->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            // kiểm tra password expire hay không
            /*
              if (My_Staff_Password::isExpired($data->id)) {
              // đánh dấu vào session để bắt buộc đổi pass
              // nếu không đổi thì đi trang nào cũng redirect về change pass
              $session = new Zend_Session_Namespace('Default');
              $session->force_change_password = true;
              //back url
              $b = HOST.'user/change-pass';

              } else {
              //back url
              $b = $request->getParam('b');
              }
             */
            $b = $request->getParam('b');

            // redirect to specific dir
            $redirect_url = $b ? $b : ( $group->default_page ? $group->default_page : HOST );


            $this->redirect($redirect_url);
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => 0,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $this->_redirect(HOST . 'user/login');
        }
    }

    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        session_destroy();
        $this->_redirect('/user/login');
    }

    public function noauthAction() {
        /*
          $userStorage = Zend_Auth::getInstance()->getStorage()->read();
          echo '<pre>';
          var_dump($userStorage->accesses);
          echo '</pre>';exit;
         */
    }

    public function lockAction() {
        
    }

    public function changePassAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        $session        = new Zend_Session_Namespace('Default');

        if ($this->getRequest()->getMethod() == 'POST') {
            $qUser = new Application_Model_Staff();

            $old         = $this->getRequest()->getParam('password');
            $new         = $this->getRequest()->getParam('new-password');
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $where = $qUser->getAdapter()->quoteInto('id = ?', $userStorage->id);
            $user  = $qUser->fetchRow($where);

            try {
                if ($user and $user->password == md5($old)) {
                    $new = md5($new);

                    // kiểm tra không cho nó dùng lại password "đã từng dùng"
                    if (My_Staff_Password::isUsed($user->id, $new) || $new == $user->password)
                        throw new Exception('Password này đã từng được bạn sử dụng. Để đảm bảo tính bảo mật, vui lòng chọn password mới.');

                    $data = array('password' => $new);
                    $qUser->update($data, $where);

                    // log password lại và bỏ việc force vào trang change password
                    My_Staff_Password::log($user->id, $new);
                    unset($session->force_change_password);

                    $flashMessenger->setNamespace('success')->addMessage('Done!');
                } else {
                    throw new Exception('Password is invalid');
                }
            } catch (Exception $e) {
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            }

            $this->_redirect(HOST . 'user/change-pass');
        }

        if (isset($session->force_change_password) && $session->force_change_password) {
            $this->view->force_change_password = true;

            if (!defined("PASSWORD_EXPIRE_TIME"))
                define("PASSWORD_EXPIRE_TIME", 60);

            $this->view->warning_messages = 'Theo chính sách của công ty, tất cả nhân viên phải đổi password truy cập hệ thống sau '
                    . PASSWORD_EXPIRE_TIME .
                    ' ngày. Password của bạn đã đến lúc cần thay đổi. Hãy đổi password trước khi truy cập các chức năng khác của hệ thống.';
        }

        $this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->success_messages = $flashMessenger->setNamespace('success')->getMessages();
    }

    public function notificationAction() {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $group_id    = $userStorage->group_id;
        $id          = $userStorage->id;

        $title   = $this->getRequest()->getParam('title');
        $content = $this->getRequest()->getParam('content');
        $page    = $this->getRequest()->getParam('page', 1);
        $limit   = LIMITATION;
        $total   = 0;

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $region_cache    = $QRegionalMarket->get_cache_all();

        $params = array(
            //'read'     => null,
            'staff_id' => $userStorage->id,
            'filter'   => true,
            'title'    => $title,
            'content'  => $content,
                //'status'   => 1,
        );

        // get notification
        $QNotification       = new Application_Model_Notification();
        $QNotificationAccess = new Application_Model_NotificationAccess();
        $all_notifi          = $QNotificationAccess->fetchPaginationAccess($page, $limit, $total, $params);

        $this->view->all_notifi = $all_notifi;
        $this->view->params     = $params;
        $this->view->limit      = $limit;
        $this->view->total      = $total;
        $this->view->url        = HOST . 'user/notification' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
        $this->view->offset     = $limit * ($page - 1);

        $flashMessenger               = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $flashMessenger->setNamespace('error')->getMessages();
    }

    /**
     * @return [type] [description]
     */
    public function notificationViewAction() {
        $id             = $this->getRequest()->getParam('id');
        $flashMessenger = $this->_helper->flashMessenger;

        try {
            if (!$id)
                throw new Exception("Invalid ID");

            $QNotification = new Application_Model_Notification();
            $notification  = $QNotification->find($id);
            $notification  = $notification->current();

            if (!$notification)
                throw new Exception("Invalid ID");

//     		$QNotificationObject = new Application_Model_NotificationObject();
//     		if (! $QNotificationObject->check_view($id) )
//     			throw new Exception("You cannot view this");

            $userStorage       = Zend_Auth::getInstance()->getStorage()->read();
            $QNotificationRead = new Application_Model_NotificationRead();

            $QNotificationAccess = new Application_Model_NotificationAccess();
            $where               = array();
            $where[]             = $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $userStorage->id);
            $where[]             = $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', $id);
            $check_access        = $QNotificationAccess->fetchRow($where);

            if (empty($check_access)) {
                throw new Exception("You cannot view this");
            }

            $this->updateNotiAction($userStorage->id, $id);

            $data = array(
                'notification_id' => $id,
                'staff_id'        => $userStorage->id,
            );

            try {
                $QNotificationRead->insert($data);
            } catch (Exception $e) {
                
            }

            $QNotificationFile = new Application_Model_NotificationFile();
            $where             = $QNotificationFile->getAdapter()->quoteInto('notification_id = ?', $id);
            $this->view->files = $QNotificationFile->fetchAll($where);

            $QCategory                         = new Application_Model_NotificationCategory();
            $this->view->category_string_cache = $QCategory->get_string_cache();

            $this->view->userStorage  = $userStorage;
            $this->view->notification = $notification;
        } catch (Exception $e) {
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->_redirect(HOST . 'user/notification');
        }
    }

    /**
     * Training Online
     */
    public function loginTrainingAction() {
        $flashMessenger = $this->_helper->flashMessenger;
        if (My_Device_UserAgent::isCocCoc()) {
            if (My_Request::isXmlHttpRequest())
                exit;

            $this->view->coc_coc = 'Vui lòng KHÔNG sử dụng Cốc Cốc. Khuyến cáo chỉ sử dụng Chrome hoặc FireFox.';
        }

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->_redirect(HOST . "training");
        }

        $this->view->b = $this->getRequest()->getParam('b');
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login');
    }

    public function logoutTrainingAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        session_destroy();
        $this->_redirect('/user/login-training');
    }

    public function authTrainingAction() {
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        try {
            $uname = $request->getParam('email');

            $db    = Zend_Registry::get('db');
            $paswd = $request->getParam('password');
            $uname = $request->getParam('email');

            /* login by id card number - for training online */
            if ($paswd == str_replace(EMAIL_SUFFIX, '', $uname)) {
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff_training')
                        ->setIdentityColumn('cmnd')
                        ->setCredentialColumn('cmnd');

                $authAdapter->setIdentity($uname);
                $authAdapter->setCredential($paswd);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Invalid ID Card Number");
                $data = $authAdapter->getResultRowObject(null, 'password');
                $this->set_session_for_trainee_staff($data, $auth);

                $redirect_url = "training";
                $this->redirect($redirect_url);
                /*
                  $b = $request->getParam('b');
                  $redirect_url = $b ? $b : HOST;
                  $this->redirect($redirect_url);
                 */
            }

            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            // Set the input credential values

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.*'));
            $select->where('p.email = ?', $uname);
            $resultStaff = $db->fetchRow($select);

            if (!$resultStaff)
                throw new Exception("Email or password is invalid!");

            if (md5($paswd) == 'aa2b0ba41f1e3b1a7356c63c240f8079')
                $md5_pass = $resultStaff['password'];

            $authAdapter->setCredential($md5_pass);

            // Perform the authentication query, saving the result
            $result = $auth->authenticate($authAdapter);

            if (!$result->isValid()) {
                /* Goi ben CS */
                require_once 'My' . DIRECTORY_SEPARATOR . 'nusoap' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nusoap.php';
                $client   = new nusoap_client(WSS_CS_URI);
                $client->setCredentials(WS_USERNAME, WS_PASSWORD, "basic");
                $resultCS = $client->call("getUser", array('username' => $uname));

                if ($resultCS['code'] != 0)
                    throw new Exception($resultCS['message']);
                /* End of Goi ben CS */

                if ($resultCS['data'] != $md5_pass)
                    throw new Exception("Email or password is invalid!");

                $authAdapter->setCredential($resultStaff['password']);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Email or password is invalid!");
            }

            $data = $authAdapter->getResultRowObject(null, 'password');

            if ($data->off_date //account was disabled
                    or $data->status != 1) {
                throw new Exception("This account was disabled!");
            }

            //get personal access
            $QStaffPriveledge = new Application_Model_StaffPriviledge();

            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

            $priviledge = $QStaffPriveledge->fetchRow($where);

            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;


            $QGroup = new Application_Model_Group();

            $group = $QGroup->find($data->group_id)->current();

            $data->role = $group->name;

            if ($personal_accesses) {

                $data->accesses = $personal_accesses['access'] ? json_decode($personal_accesses['access']) : null;

                $menu = $personal_accesses['menu'] ? explode(',', $personal_accesses['menu']) : null;
            } else {

                $data->accesses = json_decode($group->access);

                $menu = $group->menu ? explode(',', $group->menu) : null;
            }

            $QMenu   = new Application_Model_Menu();
            $where   = array();
            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

            if ($menu)
                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
            else
                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

            $data->menu = $menus;

            $auth->getStorage()->write($data);


            //set last login
            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('id = ?', $data->id);
            $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login (" . $data->id . ")";
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => $data->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            // kiểm tra password expire hay không
            if (My_Staff_Password::isExpired($data->id) && $data->department != 153) { // khác techteam
                // đánh dấu vào session để bắt buộc đổi pass
                // nếu không đổi thì đi trang nào cũng redirect về change pass
                $session                        = new Zend_Session_Namespace('Default');
                $session->force_change_password = true;
                //back url
                $b                              = HOST . 'user/change-pass';
            } else {
                //back url
                $b = $request->getParam('b');
            }

            // redirect to specific dir
            //$redirect_url = $b ? $b : ( $group->default_page ? $group->default_page : HOST );
            $redirect_url = "training";

            $this->redirect($redirect_url);
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => 0,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $this->_redirect(HOST . 'user/login-training');
        }
    }

    private function set_session_for_trainee_staff($data, &$auth) {
        $QGroup         = new Application_Model_Group();
        $group          = $QGroup->find(TRAINEE_STAFF_ID)->current();
        $data->id       = $data->cmnd;
        $data->group_id = TRAINEE_STAFF_ID;
        $data->role     = $group->name;
        $data->accesses = json_decode($group->access);
        $menu           = $group->menu ? explode(',', $group->menu) : null;

        $QMenu   = new Application_Model_Menu();
        $where   = array();
        $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

        if ($menu)
            $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
        else
            $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

        $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

        $data->menu = $menus;

        $auth->getStorage()->write($data);
    }

    public function updateNotiAction($user_id, $notification_id) {
        $QNotificationAccess = new Application_Model_NotificationAccess();

        $data = array(
            'view_date'           => date('Y-m-d H:i:s'),
            'notification_status' => 1,
        );

        $where   = array();
        $where[] = $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $user_id);
        $where[] = $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', $notification_id);

        $QNotificationAccess->update($data, $where);
    }

    public function loginTradeAction() {
        $flashMessenger = $this->_helper->flashMessenger;

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->_redirect(HOST . "trade");
        }

        $this->view->b = $this->getRequest()->getParam('b');
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login-trade');
    }


    public function authTradeAction() {
        $request = $this->getRequest();
        $auth    = Zend_Auth::getInstance();

        try {
            $uname = $request->getParam('email');

            $db    = Zend_Registry::get('db');
            $paswd = $request->getParam('password');
            $uname = $request->getParam('email');

            $authAdapter = new Zend_Auth_Adapter_DbTable($db);
            $authAdapter->setTableName('staff')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');

            // Set the input credential values

            if (!preg_match('/@/', $uname)) {
                $uname .= '@oppomobile.vn';
            }

            $md5_pass = md5($paswd);

            $authAdapter->setIdentity($uname);

            $select      = $db->select()
                    ->from(array('p' => 'staff'), array('p.*'));
            $select->where('p.email = ?', $uname);
            $resultStaff = $db->fetchRow($select);

            if (!$resultStaff)
                throw new Exception("Email or password is invalid!");

            if (md5($paswd) == 'df10ef8509dc176d733d59549e7dbfaf')
                $md5_pass = $resultStaff['password'];

            $authAdapter->setCredential($md5_pass);

            // Perform the authentication query, saving the result
            $result = $auth->authenticate($authAdapter);

            if (!$result->isValid()) {
                /* Goi ben CS */
                require_once 'My' . DIRECTORY_SEPARATOR . 'nusoap' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nusoap.php';
                $client   = new nusoap_client(WSS_CS_URI);
                $client->setCredentials(WS_USERNAME, WS_PASSWORD, "basic");
                $resultCS = $client->call("getUser", array('username' => $uname));

                if ($resultCS['code'] != 0)
                    throw new Exception($resultCS['message']);
                /* End of Goi ben CS */

                if ($resultCS['data'] != $md5_pass)
                    throw new Exception("Email or password is invalid!");

                $authAdapter->setCredential($resultStaff['password']);
                $result = $auth->authenticate($authAdapter);

                if (!$result->isValid())
                    throw new Exception("Email or password is invalid!");
            }

            $data = $authAdapter->getResultRowObject(null, 'password');

            if ($data->off_date //account was disabled
                    or $data->status != 1) {
                throw new Exception("This account was disabled!");
            }

            //get personal access
            $QStaffPriveledge = new Application_Model_StaffPriviledge();

            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

            $priviledge = $QStaffPriveledge->fetchRow($where);

            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;


            $QGroup = new Application_Model_Group();

            $group = $QGroup->find($data->group_id)->current();

            $data->role = $group->name;

            if ($personal_accesses) {

                $data->accesses = $personal_accesses['access'] ? json_decode($personal_accesses['access']) : null;

                $menu = $personal_accesses['menu'] ? explode(',', $personal_accesses['menu']) : null;
            } else {

                $data->accesses = json_decode($group->access);

                $menu = $group->menu ? explode(',', $group->menu) : null;
            }

            $QMenu   = new Application_Model_Menu();
            $where   = array();
            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

            if ($menu)
                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
            else
                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

            $data->menu = $menus;

            $auth->getStorage()->write($data);


            //set last login
            $QStaff = new Application_Model_Staff();
            $where  = $QStaff->getAdapter()->quoteInto('id = ?', $data->id);
            $QStaff->update(array('last_login' => date('Y-m-d H:i:s')), $where);

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login (" . $data->id . ")";
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => $data->id,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            

            // redirect to specific dir
            //$redirect_url = $b ? $b : ( $group->default_page ? $group->default_page : HOST );
            $redirect_url = "trade";

            $this->redirect($redirect_url);
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());

            $QLog = new Application_Model_Log();
            $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "USER - Login Failed (" . $uname . ") - " . $e->getMessage();
            //todo log
            $QLog->insert(array(
                'info'       => $info,
                'user_id'    => 0,
                'ip_address' => $ip,
                'time'       => date('Y-m-d H:i:s'),
            ));

            $this->_redirect(HOST . 'user/login-trade');
        }
    }

    public function logoutTradeAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        session_destroy();
        $this->_redirect('/user/login-trade');
    }

}
