<?php

    class TradeDeleteController extends My_Controller_Action
    {
        public function reportAction(){
            
            $QTradeDelete = new Application_Model_TradeDelete();
            $QArea        = new Application_Model_Area();

            $QAsm         = new Application_Model_Asm();

            $userStorage  = Zend_Auth::getInstance()->getStorage()->read();

            

            $category = $QTradeDelete->getCategory();
            $area = $QArea->get_cache();

            $params = array();

            if($userStorage->title == SALES_TITLE || $userStorage->title == LEADER_TITLE){
                $params['staff_id'] = $userStorage->id;
            }
            elseif($userStorage->group_id == ASM_ID){
                $asm_cache              = $QAsm->get_cache();
                $params['area_list']    = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

            }
            elseif($userStorage->group_id == TRADE_MARKETING_ID){
                $area_list              = $QAsm->get_area_trade($userStorage->email);
                $params['area_list']    = $area_list;
            }
            elseif($userStorage->title == 173){

            }
            elseif(!in_array($userStorage->id, array(SUPERADMIN_ID,150,241))){
                echo "You have no permission to access this.";exit;
            }

            $result = $QTradeDelete->getDataArea($params);


            $data_area = array();
            foreach($result as $key=>$value){
                $data_area[$value['id']][$value['good_id']] = $value['total'];
            }

            $this->view->data_area = $data_area;
            $this->view->category = $category;
            $this->view->area = $area;
            

        }

        public function reportChannelAction(){
            $QTradeDelete = new Application_Model_TradeDelete();
            $QArea        = new Application_Model_Area();

            $QAsm         = new Application_Model_Asm();

            $userStorage  = Zend_Auth::getInstance()->getStorage()->read();

            

            $category = $QTradeDelete->getCategory();
            $area = $QArea->get_cache();

            $params = array();

            if($userStorage->title == SALES_TITLE || $userStorage->title == LEADER_TITLE){
                $params['staff_id'] = $userStorage->id;
            }
            elseif($userStorage->group_id == ASM_ID){
                $asm_cache              = $QAsm->get_cache();
                $params['area_list']    = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

            }
            elseif($userStorage->group_id == TRADE_MARKETING_ID){
                $area_list              = $QAsm->get_area_trade($userStorage->email);
                $params['area_list']    = $area_list;
            }
            elseif($userStorage->title == 173){

            }
            elseif(!in_array($userStorage->id, array(SUPERADMIN_ID,150,241))){
                echo "You have no permission to access this.";exit;
            }

            $result = $QTradeDelete->getDataAreaChannel($params);


            $data_area = array();
            foreach($result as $key=>$value){
                $data_area[$value['id']][$value['is_ka']][$value['good_id']] = $value['total'];
            }

            $this->view->data_area = $data_area;
            $this->view->category = $category;
            $this->view->area = $area;

        }

        public function reportSaleAction(){
            
            $QStaff       = new Application_Model_Staff();
            $QTradeDelete = new Application_Model_TradeDelete();
            $QArea        = new Application_Model_Area();
            $QAsm         = new Application_Model_Asm();
            $QKpiMonth    = new Application_Model_KpiMonth();
            $QConfirmQrcode = new Application_Model_ConfirmQrcode();

            $userStorage  = Zend_Auth::getInstance()->getStorage()->read();

            $data_qrcode_asm = $QConfirmQrcode->get_confirm_asm();
            $data_qrcode_leader = $QConfirmQrcode->get_confirm_leader();
            $data_qrcode_trade = $QConfirmQrcode->get_confirm_trade();

            $confirm_qrcode = [];
            foreach($data_qrcode_asm as $key=>$value){
                $confirm_qrcode[$value['sale_id']]['asm'] = $value['full_name'];
            }
            foreach($data_qrcode_leader as $key=>$value){
                $confirm_qrcode[$value['sale_id']]['leader'] = $value['full_name'];
            }
            foreach($data_qrcode_trade as $key=>$value){
                $confirm_qrcode[$value['sale_id']]['trade'] = $value['full_name'];
            }

            $this->view->confirm_qrcode = $confirm_qrcode;

            $category = $QTradeDelete->getCategory();
            $area = $QArea->get_cache();

            $params = array();

            if($userStorage->title == SALES_TITLE){
                $params['staff_id'] = $userStorage->id;
            }
            elseif($userStorage->title == LEADER_TITLE){
                $params['staff_id'][]  = $userStorage->id;
                $params['leader_id'] = $userStorage->id;
                $params['title'] = SALES_TITLE;
                //Lấy danh sách Sale quản lý
                $list_sale = $QKpiMonth->getListSale($params);
                foreach($list_sale as $key=>$value){
                    $params['staff_id'][] = $value['staff_id'];
                }
            }
            elseif($userStorage->group_id == ASM_ID){
                $asm_cache              = $QAsm->get_cache();
                $params['area_list']    = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

            }
            elseif($userStorage->group_id == TRADE_MARKETING_ID){
                $area_list              = $QAsm->get_area_trade($userStorage->email);
                $params['area_list']    = $area_list;
            }
            elseif($userStorage->title == 173){

            }
            elseif(!in_array($userStorage->id, array(SUPERADMIN_ID,150,241))){
                echo "You have no permission to access this.";exit;
            }

            $result = $QTradeDelete->getDataSale($params);
            $list_sale = [];
            $data = [];

            foreach($result as $key=>$value){
                if(empty($list_sale[$value['staff_id']])){
                    $list_sale[$value['staff_id']] = [ 'email' => $value['email'] , 'fullname' => $value['fullname'] , 'area_name' => $value['area_name'] ];
                }

                $data[$value['staff_id']][$value['good_id']] = $value['total'];

            }



            $this->view->list_sale = $list_sale;
            $this->view->data = $data;
            $this->view->category = $category;
            $this->view->area = $area;

            $flashMessenger       = $this->_helper->flashMessenger;
            $messages             = $flashMessenger->setNamespace('success')->getMessages();
            $this->view->messages = $messages;
            $messages_error       = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages_error = $messages_error;
            

        }

        public function saveConfirmAction(){
            
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $flashMessenger = $this->_helper->flashMessenger;
            $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
            $QLog           = new Application_Model_Log();
            $ip             = $this->getRequest()->getServer('REMOTE_ADDR');
            $now            = date('Y-m-d H:i:s');

            $back_url = HOST.'trade-delete/report-sale';

            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            try
            {
                $QConfirmQrcode = new Application_Model_ConfirmQrcode();

                $staff_id = $this->getRequest()->getParam('staff_id');

                foreach($staff_id as $key => $value){

                    $where = $QConfirmQrcode->getAdapter()->quoteInto("sale_id = ?", $value);
                    $confirm_qrcode = $QConfirmQrcode->fetchRow($where);

                    $data = array(
                        'status'    => 1,
                        'sale_id'   => $value
                    );

                    if($userStorage->title == LEADER_TITLE){

                        if( empty($confirm_qrcode['leader_id']) ){
                            $data['leader_id']          = $userStorage->id;
                            $data['leader_confirm_at']  = $now;
                            $QConfirmQrcode->insert($data);
                        }


                    }elseif($userStorage->group_id == ASM_ID){
                        
                        if( empty($confirm_qrcode['asm_id']) ){
                            $data['asm_id']         = $userStorage->id;
                            $data['asm_confirm_at'] = $now;
                            $QConfirmQrcode->insert($data);
                        }

                    }
                    elseif($userStorage->group_id == TRADE_MARKETING_ID){

                        if( empty($confirm_qrcode['trade_id']) ){
                            $data['trade_id']         = $userStorage->id;
                            $data['trade_confirm_at'] = $now;
                            $QConfirmQrcode->insert($data);
                        }

                    }
                    else{
                        $db->rollback();
                        $flashMessenger->setNamespace('error')->addMessage('Permission Denied Confirm!');
                        $this->redirect($back_url);
                    }

                }

                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Done!');
                $this->redirect($back_url);
                
            }
            catch (Exception $e)
            {
                $db->rollback();
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                $this->redirect($back_url);
            }
            exit;
            
        }

        public function indexAction()
        {
            
            $page                   = $this->getRequest()->getParam('page', 1);
            $imei                   = $this->getRequest()->getParam('imei', null);
            $store_code             = $this->getRequest()->getParam('store_code', null);
            $store_name             = $this->getRequest()->getParam('store_name', null);
            $del                    = $this->getRequest()->getParam('del');
            $lock                   = $this->getRequest()->getParam('lock');
            $area_id                = $this->getRequest()->getParam('area_id');
            $export                 = $this->getRequest()->getParam('export');
            $is_scan                = $this->getRequest()->getParam('is_scan');
            $email                  = $this->getRequest()->getParam('email');

            $QStore                 = new Application_Model_Store();
            $userStorage            = Zend_Auth::getInstance()->getStorage()->read();

            $QArea                  = new Application_Model_Area();
            $QAsm                   = new Application_Model_Asm();
            $QKpiMonth                   = new Application_Model_KpiMonth();

            $this->view->areas      = $QArea->fetchAll(null, 'name');

            $params = array(
                'page'       => $page,
                'imei'       => $imei,
                'limit'      => $limit,
                'store_code' => $store_code,
                'store_name' => $store_name,
                'offset'     => $offset,
                'get_store'  => 1,
                'area_id'    => $area_id,
                'is_scan'    => $is_scan,
                'email'      => $email
            );

            if($userStorage->title == SALES_TITLE){
                $params['staff_id'] = $userStorage->id;
            }
            elseif($userStorage->title == LEADER_TITLE){
                $params['staff_id'][]  = $userStorage->id;
                $params['leader_id'] = $userStorage->id;
                $params['title'] = SALES_TITLE;
                //Lấy danh sách Sale quản lý
                $list_sale = $QKpiMonth->getListSale($params);
                foreach($list_sale as $key=>$value){
                    $params['staff_id'][] = $value['staff_id'];
                }
            }
            elseif($userStorage->group_id == ASM_ID){
                $asm_cache              = $QAsm->get_cache();
                $params['area_list']    = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

            }
            elseif($userStorage->group_id == TRADE_MARKETING_ID){
                $area_list              = $QAsm->get_area_trade($userStorage->email);
                $params['area_list']    = $area_list;
            }
            elseif($userStorage->title == 173){

            }
            elseif(!in_array($userStorage->id, array(SUPERADMIN_ID,150,241))){
                echo "You have no permission to access this.";exit;
            }

            //GET LIST STORE SCAN
            if(isset($is_scan) and ($is_scan == 1 OR $is_scan == 2)){
                $QTradeDelete = new Application_Model_TradeDelete();
                $list_scan  = $QTradeDelete->getListStoreScan();
                if($is_scan == 1){
                    $params['list_scan'] = $list_scan;
                }
                elseif($is_scan == 2){
                    $params['list_not_scan'] = $list_scan;
                }
                
            }
            //END GET LIST STORE SCAN

            $page               = $this->getRequest()->getParam('page', 1);
            $limit              = LIMITATION;
            $total              = 0;
            $data = $QStore->fetchPaginationStoreStaff($page, $limit, $total, $params);

            if($export and $export == 1){
                $data = $QStore->fetchPaginationStoreStaffExport($page, $limit, $total, $params);

                $this->reportStoreStaffExport($data);
            }

            unset($params['list_scan']);
            unset($params['list_not_scan']);

            $this->view->params = $params;
            $this->view->data   = $data;
            $this->view->limit  = $limit;
            $this->view->total  = $total;
            unset($params['area_list']);
            $this->view->url    = HOST.'trade-delete'.( $params ? '?'.http_build_query($params).'&' : '?' );
            $this->view->offset = $limit*($page-1);
            
        }

        public function detailStoreAction()
        {
            
            $QStore = new Application_Model_Store();

            $object_id = $this->getRequest()->getParam('object_id');
            $lock = $this->getRequest()->getParam('lock');
            $add = $this->getRequest()->getParam('add');
            $del_id = $this->getRequest()->getParam('del_id');
            $del = $this->getRequest()->getParam('del');

            if(empty($object_id))
            {
                $this->_redirect('/trade-delete');
                exit;
            }

            if(!empty($lock))
            {
                $this->lockImeiStore($object_id);
            }

            if(!empty($del_id))
            {
                $this->deleteGood($del_id);
            }

            if(!empty($del))
            {
                $this->deleteImei($del);
            }

            if(!empty($add))
            {
                $object_id = $this->getRequest()->getParam('object_id');
                $good_id = $this->getRequest()->getParam('good_id');
                $number = $this->getRequest()->getParam('number');
                $params = array(
                    'object_id' => $object_id,
                    'good_id' => $good_id,
                    'number' => $number
                );
                //$this->addGood($params);

                //UPLOAD HÌNH ẢNH
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'trade-marketing' .
                DIRECTORY_SEPARATOR . $object_id . DIRECTORY_SEPARATOR . $good_id;
                

                $upload = new Zend_File_Transfer();
                $upload->setOptions(array('ignoreNoFile'=>true));
                
                //check function
                if (function_exists('finfo_file'))
                    $upload->addValidator('MimeType', false, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif' ,'application/vnd.ms-powerpoint'));
            


                $upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
                $upload->addValidator('Size', false, array('max' => '7MB'));
                $upload->addValidator('ExcludeExtension', false, 'php,sh');
                $files = $upload->getFileInfo();
                $hasPhoto = false;
                $data_file = array();
                


                $fileInfo = (isset($files['image']) and $files['image']) ? $files['image'] : null;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);
                $upload->setDestination($uploaded_dir);
                    
                //Rename
                $old_name = $fileInfo['name'];
                $tExplode = explode('.', $old_name);
                $extension = end($tExplode);
                $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;

                $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                $r = $upload->receive(array('image'));
                if($r){
                    $data_file['file_name'] = $new_name;

                    $uploaded_dir_image = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
                    //Resize Anh            
                    $image = new My_Image_Resize();
                    $image->load($uploaded_dir_image);
                    $image->resizeToWidth(750);
                    $image->save($uploaded_dir_image);                                                            
                    //END Resize
                }
                else{
                    $messages = $upload->getMessages();
                    foreach ($messages as $msg){
                        $back_url           = HOST.'trade-delete/detail-store?object_id='.$object_id;
                        $this->redirect($back_url);
                    }
                }
                //END UPLOAD HÌNH ẢNH

                $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                $db = Zend_Registry::get('db');
                $sql = "INSERT  INTO `".DATABASE_TRADE."`.`store_good` (`object_id`, `good_id`, `number`, `created_by`, `file`)
                            VALUES (:object_id, :good_id, :number, :created_by, :file)";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('object_id', $params['object_id'], PDO::PARAM_INT);
                $stmt->bindParam('good_id', $params['good_id'], PDO::PARAM_INT);
                $stmt->bindParam('number', $params['number'], PDO::PARAM_INT);
                $stmt->bindParam('created_by', $userStorage->id, PDO::PARAM_INT);
                $stmt->bindParam('file', $data_file['file_name'], PDO::PARAM_STR);
                $stmt->execute();


                $stmt->closeCursor();
                $stmt = $db = null;
                
                $back_url           = HOST.'trade-delete/detail-store?object_id='.$object_id;
                $this->redirect($back_url);  
            }

            $data_imei = $this->getImeiByStore($object_id);
            $data_good = $this->getGoodByStore($object_id);
            $this->view->category = $this->getCategory();
            $this->view->object_id = $object_id;
            $this->view->data_imei = $data_imei;
            $this->view->data_good = $data_good;

            $where = $QStore->getAdapter()->quoteInto("id = ?", $object_id);
            $this->view->store = $QStore->fetchRow($where);
           
        }

        public function getCategory()
        {

            $db = Zend_Registry::get('db');
            $sql = "SELECT *
                    FROM `".DATABASE_TRADE."`.`category`  
                    WHERE `type` = 2 AND id IN (43,421,113,44,363,261,153,154,152)
                    ORDER BY name ASC
                    ";
            
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;

            return $data;
        }

        public function getImeiByStore($store_id)
        {
            $db = Zend_Registry::get('db');
            $sql = "SELECT i.*, c.name as `category`, photo.photo
                    FROM `".DATABASE_TRADE."`.`imei` i 
                    LEFT JOIN `".DATABASE_TRADE."`.imei_photo photo ON photo.imei_sn = i.imei_sn
                    INNER JOIN `".DATABASE_TRADE."`.`category` c ON c.id = i.good_id 
                    WHERE (i.del <> 1 OR i.del IS NULL) AND i.object_id = :store_id
                    AND ((i.object_active >= '2012-08-08 00:00' AND (i.area_id <> 1 OR i.area_id IS NULL)) OR (i.object_active >= '2012-08-08 00:00' AND i.area_id = 1))
                    ";
            
            $stmt = $db->prepare($sql);
            $stmt->bindParam('store_id', $store_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;

            return $data;
        }

        public function getGoodByStore($store_id)
        {
            $db = Zend_Registry::get('db');
            $sql = "SELECT i.*, c.name as `category`
                    FROM `".DATABASE_TRADE."`.`store_good` i 
                    INNER JOIN `".DATABASE_TRADE."`.`category` c ON c.id = i.good_id 
                    WHERE i.status <> 0 AND i.object_id = :store_id
                    ";
            
            $stmt = $db->prepare($sql);
            $stmt->bindParam('store_id', $store_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;

            return $data;
        }

        public function lockImeiStore($object_id)
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db = Zend_Registry::get('db');
            $sql = "CALL PR_trade_lock_imei_store(:object_id)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('object_id', $object_id, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = $db = null;

            $QLog = new Application_Model_Log();
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "Lock Imei Trade Marketing Store" . $object_id;
            $QLog->insert(array(
                'info' => $info,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
            ));
        }

        public function deleteImei($imei)
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db = Zend_Registry::get('db');
            $sql = "CALL PR_trade_delete_imei(:imei)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = $db = null;

            $QLog = new Application_Model_Log();
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "Delete Imei Trade Marketing " . $imei;
            $QLog->insert(array(
                'info' => $info,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
            ));
        }

        public function addGood($params = array())
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db = Zend_Registry::get('db');
            $sql = "INSERT  INTO `".DATABASE_TRADE."`.`store_good` (`object_id`, `good_id`, `number`, `created_by`)
                        VALUES (:object_id, :good_id, :number, :created_by)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('object_id', $params['object_id'], PDO::PARAM_INT);
            $stmt->bindParam('good_id', $params['good_id'], PDO::PARAM_INT);
            $stmt->bindParam('number', $params['number'], PDO::PARAM_INT);
            $stmt->bindParam('created_by', $userStorage->id, PDO::PARAM_INT);
            $stmt->execute();

            $stmt->closeCursor();
            $stmt = $db = null;
        }

        public function deleteGood($id)
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db = Zend_Registry::get('db');
            $sql = "UPDATE `".DATABASE_TRADE."`.`store_good` SET `status` = 0 WHERE id = :id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = $db = null;

            $QLog = new Application_Model_Log();
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "Delete Store Good Trade Marketing " . $id;
            $QLog->insert(array(
                'info' => $info,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
            ));
        }

        public function lockImei($staff_id, $imei)
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $db = Zend_Registry::get('db');
            $sql = "CALL PR_trade_lock_imei(:staff_id, :imei)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
            $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = $db = null;

            $QLog = new Application_Model_Log();
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
            $info = "Lock Imei Trade Marketing " . $imei;
            $QLog->insert(array(
                'info' => $info,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
            ));
        }



        public function reportStoreStaffExport($data){

            set_time_limit(0);
            error_reporting(0);
            ini_set('memory_limit', -1);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();

            $PHPExcel->setActiveSheetIndex(0);
            $sheet    = $PHPExcel->getActiveSheet();
            $index    = 1;
            
            $heads = array(
                '#',
                'Store ID',
                'Store Name',
                'Store Code',
                'Sellout',
                'Email Sale',
                'Area',
                'Category Name',
                'Number',
                'Imei Sn',
                'Date Scan In Store',
                'Imei Delete',

            );
            
            $alpha    = 'A';
        
            foreach($heads as $key)
            {
                $sheet->setCellValue($alpha.$index, $key);
                $alpha++;
            }

            $index    = 2;
        $i = 1;

            foreach($data as $item){
                $alpha = 'A';
               
                $sheet->setCellValue($alpha++.$index, $i++);
                $sheet->setCellValue($alpha++.$index, $item['store_id']);
                $sheet->setCellValue($alpha++.$index, $item['name']);
                $sheet->setCellValue($alpha++.$index, $item['store_code']);
                $sheet->setCellValue($alpha++.$index, $item['sellout']);
                $sheet->setCellValue($alpha++.$index, $item['email']);
                $sheet->setCellValue($alpha++.$index, $item['area_name']);
                $sheet->setCellValue($alpha++.$index, $item['category_name']);
                $sheet->setCellValue($alpha++.$index, $item['number']);
                $sheet->setCellValueExplicit($alpha++.$index, $item['imei_sn'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alpha++.$index, $item['object_active'] ? date('H:i d/m/Y', strtotime($item['object_active'])) : null );
                $sheet->setCellValue($alpha++.$index, ($item['imei_delete'] == 1) ? 'Imei đã bị delete' : null);

                $index++;

            }
            
            $filename = 'Store Trade - '.date('d/m/Y');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }
    }