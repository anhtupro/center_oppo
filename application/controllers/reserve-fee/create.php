<?php
$QDepartment=new Application_Model_Team();
$QReserveFee=new Application_Model_ReserveFee();
$department= $QDepartment->get_department();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
//debug($department);
//debug($userStorage->department);
$day = $this->getRequest()->getParam('day',0);


$params=array();
$listReserve=$QReserveFee->getReserveFeeInDepartment($day);
$totalFee=$QReserveFee->getTotalFee($day);

//        get form today to 6 days after
$arrayDate=array();
if(!empty($day)){
    $i=$day;
    $today=date("Y-m-d",strtotime("+".$i." days"));
}else{
    $today=date("Y-m-d");
}
$day_name = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
for($i=0;$i<=6;$i++){
    $arrayDate[$today]=$day_name[date('w', strtotime($today))];
    $today=date("Y-m-d",strtotime($today."+1 days"));

}
//debug($listReserve);
//


$this->view->day=$day;
$this->view->arrayDate=$arrayDate;
$this->view->totalFee=$totalFee;
$this->view->listReserve=$listReserve;
$this->view->userStorage=$userStorage;
$this->view->department=$department;