<?php
class RequestOfficeController extends My_Controller_Action
{
    public function createRequestAction()
    {
        require_once 'request-office' . DIRECTORY_SEPARATOR . 'create-request-office.php';
    }
    public function confirmRequestAction()
    {
        require_once 'request-office' . DIRECTORY_SEPARATOR . 'confirm-request-office.php';
    }
    public function saveCreateRequestAction()
    {        
        require_once 'request-office' . DIRECTORY_SEPARATOR . 'save-create-request-office.php';
    }
    public function listRequestAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'list-request-office.php';
    }
    public function editRequestAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'edit-request-office.php';
    }
    
    public function getInfoPaymentAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'get-info-payment.php';
    }
    
    public function getListBudgetAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $project = $this->getRequest()->getParam('project_id');
        $request_type_group = $this->getRequest()->getParam('request_type_group');
        $request_type = $this->getRequest()->getParam('request_type');
        $company = $this->getRequest()->getParam('company');
        
        $QRequestOffice = new Application_Model_RequestOffice();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $params = [
            'department_id' => $userStorage->department,
            'project' => $project,
            'request_type_group' => $request_type_group,
            'request_type' => $request_type,
            'company' => $company
        ];
        
        $list_budget = $QRequestOffice->getBudgetPayment($params);
        
        if($userStorage->id == 7564){
            echo "<pre>";
            var_dump($list_budget);
            exit;
        }
        
        $data = [
            'status' => 0,
            'list_budget' => $list_budget
        ];
        
        echo json_encode($data);
        
    }
    
    public function getPurchasingRequestAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $pr_sn = $this->getRequest()->getParam('pr_sn');
        
        $QPurchasingRequest = new Application_Model_PurchasingRequest();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $purchasing = $QPurchasingRequest->getInfoPurchasingRequest($pr_sn);
        
        $data = [
            'status' => 0,
            'purchasing' => $purchasing
        ];
        
        echo json_encode($data);
    }
    
    public function reportList($data){

        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $index    = 1;
        
        $QCompany = new Application_Model_Company();
        $QStaff = new Application_Model_Staff();
        $QTeam = new Application_Model_Team();
        
        $company = $QCompany->get_cache();
        $staff = $QStaff->get_cache();
        $department = $QTeam->get_list_department();
        
        $payment_type_arr = [
            1 => 'Cash',
            2 => 'Money Bank Transfer'
        ];
        
        $status_pr_arr = [
            1 => "Waiting Purchasing's Confirmed",
            2 => "Waiting Purchasing's Approved",
            3 => "Approved",
        ];
        
        $heads = array(
            '#',
            'MÃ ĐỀ XUẤT',
            'CÔNG TY',
            'THÁNG',
            'NHÓM HẠNG MỤC',
            'HẠNG MỤC',
            'NỘI DUNG',
            
            'SỐ TẠM TÍNH',
            'TĂNG/GIẢM',
            'SỐ TIỀN THỰC TẾ',
            
            'ĐỀ NGHỊ TRẢ CHO',
            
            'SỐ HÓA ĐƠN',
            'NGÀY HÓA ĐƠN',
            
            
            'NGƯỜI YÊU CẦU',
            'PHÒNG BAN',
            'LOẠI ĐỀ XUẤT',
            'LOẠI THANH TOÁN',
            
            
            'TRẠNG THÁI',
            'TRẠNG THÁI PR',
            'NGÀY ĐỀ NGHỊ THANH TOÁN',
            'NGÀY THANH TOÁN',
        );

        $alpha    = 'A';

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }

        $index    = 2;
        $i = 1;

        foreach($data as $value){
            
            $company_name = !empty($company[$value["company_id"]]) ? $company[$value["company_id"]] : NULL;
            $created_by = !empty($staff[$value["created_by"]]) ? $staff[$value["created_by"]] : NULL;
            $department_name = !empty($department[$value["department_id"]]) ? $department[$value["department_id"]] : NULL;
            $payment_type = $payment_type_arr[$value["payment_type"]];
            $status_pr = $status_pr_arr[$value["status_pr"]];
            
            $left = $value["cost_before"]-$value["cost_temp"];
            if($left > 0){
                $left = "(".$left.")";
            }
            
            $alpha = 'A';
            
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $value['id']);
            $sheet->setCellValue($alpha++.$index, $company_name);
            $sheet->setCellValue($alpha++.$index, "Tháng ".$value["month_fee"]."-".$value["year_fee"]);
            $sheet->setCellValue($alpha++.$index, $value["type_group_title"]);
            $sheet->setCellValue($alpha++.$index, $value["request_type_title"]);
            $sheet->setCellValue($alpha++.$index, $value["content"]);
            
            $sheet->setCellValue($alpha++.$index, $value["cost_before"]);
            $sheet->setCellValue($alpha++.$index, $left);
            $sheet->setCellValue($alpha++.$index, $value["cost_temp"]);
            
            $sheet->setCellValue($alpha++.$index, $value["payee"]);
            
            $sheet->setCellValueExplicit($alpha++.$index, $value["invoice_number"], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++.$index, $value["invoice_date"]);
            
            
            $sheet->setCellValue($alpha++.$index, $created_by);
            $sheet->setCellValue($alpha++.$index, $department_name);
            $sheet->setCellValue($alpha++.$index, $value["name_category"]);
            $sheet->setCellValue($alpha++.$index, $payment_type);
            
            
            
            
            
            $sheet->setCellValue($alpha++.$index, $value["status"]);
            $sheet->setCellValue($alpha++.$index, $status_pr);
            $sheet->setCellValue($alpha++.$index, $value["date_success"]);
            $sheet->setCellValue($alpha++.$index, $value["finance_payment_date"]);
            

            $index++;

        }

        $filename = 'List request - '.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
    
    public function budgetAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'budget.php';
    }
    
    public function budgetTestAction(){
       
    }
    
    public function budgetProjectAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'budget-project.php';
    }
    
    public function getTypeGroupAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'get-type-group.php';
    }
    
    public function budgetListAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'budget-list.php';
    }
    
    public function saveMoneyAction(){
       require_once 'request-office' . DIRECTORY_SEPARATOR . 'save-money.php';
    }
    
    private function month_range($first, $last, $step = '+1 month', $output_format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
    
        while( $current <= $last ) {
    
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
    
        return $dates;
    }
   
}