<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$page                    = $this->getRequest()->getParam('page', 1);
$name                    = $this->getRequest()->getParam('name');
$code                    = $this->getRequest()->getParam('code');
$department              = $this->getRequest()->getParam('department');
$month                   = $this->getRequest()->getParam('month', date('n'));
$team                    = $this->getRequest()->getParam('team');
$area_id                 = $this->getRequest()->getParam('area_id');
$regional_market         = $this->getRequest()->getParam('regional_market');
$district                = $this->getRequest()->getParam('district');
$sales_team              = $this->getRequest()->getParam('sales_team');
$from                    = $this->getRequest()->getParam('from', date('01/m/Y'));
$to                      = $this->getRequest()->getParam('to', date('d/m/Y'));
$export_time             = $this->getRequest()->getParam('export_time', 0);
$store                   = $this->getRequest()->getParam('store');
$imei                    = $this->getRequest()->getParam('imei');
$email                   = $this->getRequest()->getParam('email');
$sort                    = $this->getRequest()->getParam('sort', 'p.id');
$desc                    = $this->getRequest()->getParam('desc', 1);
$department              = $this->getRequest()->getParam('department');
$title                   = $this->getRequest()->getParam('title');
$asm                     = $this->getRequest()->getParam('asm');
$QStore                  = new Application_Model_Store();
$this->view->desc        = $desc;
$this->view->current_col = $sort;
$limit                   = LIMITATION;
$total                   = 0;
$lock_timing_hr          = $this->getRequest()->getParam('lock_timing_hr');
$flashMessenger          = $this->_helper->flashMessenger;

if(isset($lock_timing_hr) and $lock_timing_hr)
{
    $QLocing = new Application_Model_HrTimingLock();
    $currentTime = date('Y-m-d h:i:s');

    // kiểm tra xem da lock thang nay chua

    $whereLocking   = array();
    $whereLocking[] = $QLocing->getAdapter()->quoteInto('month = ?' , $_month);
    $whereLocking[] = $QLocing->getAdapter()->quoteInto('year  = ?' , $_year);
    $locking        = $QLocing->fetchRow($whereLocking);

    $temp   = explode('/' , $from);
    $_year  = $temp[2];
    $_month = $temp[1];

    if($locking)
    {
        $messages_error = $flashMessenger->setNamespace('error')->getMessages('This month is lock');
        $this->view->messages_error = $messages_error;
        $this->redirect('time/month');
    }

    $dataLocking = array(
        'month'      => $_month,
        'year'       => $_year,
        'created_at' => $currentTime,
        'created_by' => $userStorage->id,
        'locked_at'  => $currentTime,
        'locked_by'  => $userStorage->id
    );

    $QLocing->insert($dataLocking);
    $messages = $flashMessenger->setNamespace('success')->getMessages('Done');
    $this->view->messages = $messages;
    $this->redirect('time/month');
}



$params = array_filter(array(
    'name'            => $name,
    'department'      => $department,
    'team'            => $team,
    'area_id'         => $area_id,
    'regional_market' => $regional_market,
    'district'        => $district,
    'sales_team'      => $sales_team,
    'store'           => $store,
    'imei'            => $imei,
    'email'           => $email,
    'from'            => $from,
    'to'              => $to,
    'code'            => $code,
    'title'           => $title,
    'department'      => $department,
    'asm'             => $asm,
    ));

$params['sort'] = $sort;
$params['desc'] = $desc;
$params['month'] = $month;

if (isset($export_time) && $export_time == 1)
{
    $this->_exportExcel_time_by_month($params);
    exit;
}


$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id = $userStorage->id;
$group_id = $userStorage->group_id;

if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
    ADMINISTRATOR_ID,
    HR_ID,
    HR_EXT_ID,
    BOARD_ID)))
{
} elseif ($group_id == PGPB_ID)
    $params['staff_id'] = $user_id;

elseif ($group_id == SALES_ID)
    $params['sale'] = $user_id;


elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID)
    $params['asm'] = $user_id;

elseif ($group_id == ACCESSORIES_ID)
    $params['other'] = $user_id;

elseif (in_array($group_id,array(TRAINING_TEAM_ID)))
    $params['other'] = $user_id;

elseif (in_array($group_id,array(TRAINING_LEADER_ID)))
    $params['trainer_leader'] = $user_id;

elseif ($group_id == DIGITAL_ID)
    $params['other'] = $user_id;

elseif ($group_id == SERVICE_ID)
    $params['other'] = $user_id;

elseif ($group_id == TRADE_MARKETING_GROUP_ID)
    $params['other'] = $user_id;


//set quyen cho hong nhung trainer
elseif ($user_id == 7278 || $user_id == 240)
    $params['other'] = $user_id;
//chi van ha noi
elseif ($user_id == 95)
    $params['other'] = $user_id;

elseif ($group_id == 22)
    $params['other'] = $user_id;
elseif ($user_id == 3601 || $user_id == 2768 || $user_id == 5915)
    $params['asm'] = $user_id;

else
    $params['sale'] = $user_id;

$QTiming = new Application_Model_Time();
$QTimeNorm = new Application_Model_DateNorm();
$date_norm = $QTimeNorm->get_cache();

$QTimes = $QTiming->fetchPagination($page, $limit, $total, $params);

unset($params['sale']);
unset($params['asm']);
unset($params['staff_id']);

$data = array();
$data = $QTimes;
foreach ($QTimes as $k => $v)
{
    $data[$k]['date_approved'] = $QTiming->getDay($v['staff_id'], $month);
}
$this->view->date_norm = $date_norm;
$this->view->month = $month;
$this->view->timings = $data;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->url = HOST . 'time/month' . ($params ? '?' . http_build_query($params) .
    '&' : '?');
$this->view->offset = $limit * ($page - 1);

$QArea = new Application_Model_Area();
$this->view->areas = $QArea->fetchAll(null, 'name');

$QRegionalMarket = new Application_Model_RegionalMarket();
$this->view->regional_market_all = $QRegionalMarket->get_cache();

$QTeam = new Application_Model_Team();
$this->view->team = $QTeam->get_cache_team();

$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;


if ($area_id)
{
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id in (?)', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}


if ($regional_market)
{
    //get district
    $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}


if ($district)
{
    //get store
    $where = array();
    $where[] = $QStore->getAdapter()->quoteInto('district = ?', $district);
    $where[] = $QStore->getAdapter()->quoteInto('(del IS NULL OR del = 0)', 1);

    $this->view->stores = $QStore->fetchAll($where, 'name');
}

$QTeam = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;



$messages = $flashMessenger->setNamespace('success')->getMessages();
$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
$this->view->messages_error = $messages_error;

//get product count
$staff_ids = array();

if ($this->getRequest()->isXmlHttpRequest())
{
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setRender('partials/index');
}