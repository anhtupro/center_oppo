<?php
     $this->_redirect('/leave/create');           
		   $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $flashMessenger = $this->_helper->flashMessenger;

            //get staff info
            $QStaff = new Application_Model_Staff();
            $where = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
            $this->view->staff = $staff = $QStaff->fetchRow($where);
            $this->view->user_id = $userStorage->id;
            //back url
            $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

            $messages = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $messages;

            $messages_success = $flashMessenger->setNamespace('success')->getMessages();
            $this->view->messages_success = $messages_success;