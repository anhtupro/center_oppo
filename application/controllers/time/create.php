<?php
     $this->_redirect('/time/pg-time-create');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

//get staff info
$QStaff            = new Application_Model_Staff();
$where             = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
$this->view->staff = $staff = $QStaff->fetchRow($where);

$QTeam            = new Application_Model_Team();
$team             = $QTeam->get_cache();
$this->view->team = $team;

$QShift            = new Application_Model_Shift();
$shift             = $QShift->getShiftByTitle($userStorage->title);
$this->view->shift = $shift;

$QDate            = new Application_Model_DateNorm();
$date             = $QDate->get_cache();
$this->view->date = $date;

$QTime   = new Application_Model_Time();
$where   = array();
$where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
$where[] = $QTime->getAdapter()->quoteInto('DATE(created_at) = CURDATE()', '');
$time    = $QTime->fetchRow($where);

if (isset($time) and $time)
{
    $this->view->time = 1;
}

$this->view->user_id = $userStorage->id;
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

if(isset($staff['title']) && My_Staff::isPgTitle($staff['title']))
{
    $this->view->is_pg = 1;
}