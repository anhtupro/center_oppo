<?php 
	
	$flashMessenger = $this->_helper->flashMessenger;
	$from_date      = $this->getRequest()->getParam('from_date');
	$to_date        = $this->getRequest()->getParam('to_date');
	$staff_id       = $this->getRequest()->getParam('staff_id');
	$reason         = $this->getRequest()->getParam('reason');
	$shift          = $this->getRequest()->getParam('shift');
	$type           = $this->getRequest()->getParam('type');
	$id             = $this->getRequest()->getParam('id');
    $ignore_status  = $this->getRequest()->getParam('ignore_status',0);
	$QStaff         = new Application_Model_Staff();
	$QPreTimeOff    = new Application_Model_PreTimeOff();
	$db             = Zend_Registry::get('db');
	$back_url       = '/time/create-off-long-time';

    if($id){
        $back_url .='?id='.$id;
    }
    
    if ($this->getRequest()->isPost())
    {
        if (!$type)
        {
            $flashMessenger->setNamespace('error')->addMessage('Please select type off');
            $this->_redirect($back_url);
        }

        if (!$from_date)
        {
            $flashMessenger->setNamespace('error')->addMessage('please select "from date"');
            $this->_redirect($back_url);
        }

        if (!$to_date)
        {
            $flashMessenger->setNamespace('error')->addMessage('Please select "to date"');
            $this->_redirect($back_url);
        }

        if (!$staff_id)
        {
            $flashMessenger->setNamespace('error')->addMessage('Please select a staff');
            $this->_redirect($back_url);
        }
        
        $from_date = My_Date::normal_to_mysql($from_date);

        if (trim($to_date)){
            $to_date = My_Date::normal_to_mysql($to_date);
        }else{
            $to_date = NULL;
        }

        $startTime   = strtotime($from_date);
        $endTime     = strtotime($to_date);

        if($endTime < $startTime):
            $flashMessenger->setNamespace('error')->addMessage('Thời gian bắt đầu phải nhỏ hơn thời gian đến"');
            $this->_redirect($back_url);
        endif;

        $QTime       = new Application_Model_Time();
        $QOffHistory = new Application_Model_OffHistory();
        $staff       = $QStaff->find($staff_id)->current();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $created_at  = date('Y-m-d H:i:s');
        $db->beginTransaction();
        try
        {
            $data = array(
                'staff_id'   => $staff_id,
                'from_date'  => $from_date,
                'to_date'    => $to_date,
                'off_type'   => $type,
                'reason'     => $reason,
                'created_at' => date('Y-m-d h:i:s'),
                'created_by' => $userStorage->id
            );

            if(!$id){
                //check date
                $selectDate = $db->select()
                    ->from(array('p'=>'pre_time_off'))
                    ->where('to_date >= ?',$from_date)
                    ->where('staff_id = ?',$staff_id);
                $resultCheck = $db->fetchRow($selectDate);
                if($resultCheck){
                    $flashMessenger->setNamespace('error')->addMessage('Nhân viên này đã đăng kí nghỉ vào thời gian này!');
                    $this->_redirect($back_url);
                }

                //INSERT VÀO BẢNG PRE TIME OFF
                $id = $QPreTimeOff->insert($data);
            }else{
                $where = $QPreTimeOff->getAdapter()->quoteInto('id = ?',$id);
                $QPreTimeOff->update($data,$where);
                $stmt  = $db->query('DELETE `off_history`, `time`
                    FROM `off_history`
                    INNER JOIN `time` ON `off_history`.time_id = `time`.id
                    WHERE off_history.pre_time_off_id = ?',array($id));
                $stmt->execute();
            }

            //INSERT VÀO OFF HISTORY AND TIME
            for ($i = $startTime; $i <= $endTime; $i = $i + 86400)
            {
                $date     = date('Y-m-d', $i);
                $d        = date('d', strtotime($date));
                $m        = intval(date('m', strtotime($date)));
                $y        = date('Y', strtotime($date));
                $day_name = date('N', strtotime($date));//chủ nhật

                if ($day_name == 7){
                    continue;
                }

                //check duplicate date
                $selectDuplicate = $db->select()
                    ->from(array('p' => 'off_history'), array('p.*'))
                    ->where('p.staff_id = ?', $staff_id)
                    ->where('p.`date` = ?', $date);
                ;

                $checkDuplicate = $db->fetchRow($selectDuplicate);
                if ($checkDuplicate)
                {
                    $flashMessenger->setNamespace('error')->addMessage($staff->firstname .' '. $staff->lastname. ' has already registered on :' . $date);
                    $this->_redirect($back_url);
                }

                $data_time = array(
                    'staff_id'        => $staff_id,
                    'off'             => 1,
                    'reason'          => $reason,
                    'off_type'        => $type,
                    'shift'           => 0,
                    'regional_market' => $staff->regional_market,
                    'created_by'      => $userStorage->id,
                    'created_at'      => $date,
                    'approved_at'     => $created_at,
                    'approved_by'     => $userStorage->id,
                    'status'          => 1
                );
                $time_id = $QTime->insert($data_time);

                //INSERT HISTORY
                $data_history = array(
                    'staff_id'        => $staff_id,
                    'month_id'        => $m,
                    'date'            => $date,
                    'type'            => $type,
                    'reason'          => $reason,
                    'shift'           => 0,
                    'off_date'        => 0,
                    'time_id'         => $time_id,
                    'pre_time_off_id' => $id
                );
                $QOffHistory->insert($data_history);
            }//END INSERT TIME AND OFF HISTORY

            $status = in_array($type, array(PERMIT, OFF_TEMP)) ? My_Staff_Status::Temporary_Off : ($type == CHILDBEARING ? My_Staff_Status::Childbearing : null);
            
            if ($status AND $ignore_status == 0) {
                $data = array('status' => $status);
                $where = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
                $QStaff->update($data, $where);
            }

            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
            $this->_redirect('/time/list-pre-time-off');
        }catch (exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->_redirect($back_url);

        }
    } // End if post

?>