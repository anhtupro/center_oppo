<?php
    
        $id = $this->getRequest()->getParam('id');
        $month = $this->getRequest()->getParam('month', date('n'));
        try
        {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $flashMessenger = $this->_helper->flashMessenger;
            $QTime  = new Application_Model_Time();
            $QLockTiming = new Application_Model_HrTimingLock();
            $QStaff = new Application_Model_Staff();
            $staff  = $QStaff->get_cache();

            if (!isset($id))
            {
                $flashMessenger->setNamespace('error')->addMessage('Staff is not exist');

                $this->_redirect('/time/month');
            }

           
            $year  = date('Y');
            $where = array();

            $whereLocktiming   = array();
            $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('month = ?' , intval($month));
            $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('year = ?' , intval($year));
            $lockTiming        = $QLockTiming->fetchRow($whereLocktiming);

            if($lockTiming)
            {
                throw new Exception("This month can\'t not approve!", 1);                
            }

            //if (!in_array($userStorage->group_id, array(
            //                SALES_ID,
            //                LEADER_ID,
            //                ASM_ID)) && $userStorage->group_id != ADMINISTRATOR_ID && $userStorage->
            //                group_id != SUPERADMIN_ID) {
            //
            //                $flashMessenger->setNamespace('error')->addMessage('Permission Deny, please try again!');
            //
            //                $this->_redirect('/time/month');
            //            }

            $where = array();
            $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $id);
            $where[] = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $month);
            $data = array(
                'status' => 1,
                'approved_at' => date('Y-m-d H:i:s'),
                'approved_by' => $userStorage->id,
                );

            $QTime->update($data, $where);


            //todo log
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');

            $info = 'TIME SYSTEM - Approve Timing Month for : ' . $staff[$id] . ' - by - ' .
                $staff[$userStorage->id];

            $QLog = new Application_Model_Log();

            $QLog->insert(array(
                'info' => $info,
                'user_id' => $userStorage->id,
                'ip_address' => $ip,
                'time' => date('Y-m-d H:i:s'),
                ));

            $flashMessenger->setNamespace('success')->addMessage('Done!');

            $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));

        }
        catch (exception $e)
        {

            $flashMessenger->setNamespace('error')->addMessage('Cannot update, please try again!');

            $this->_redirect('/time/month');
        }