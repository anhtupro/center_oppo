<?php

$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$QTimeGps    = new Application_Model_TimeGps();
$shift       = $this->getRequest()->getParam('shift');
$img1        = $this->getRequest()->getParam('imagedata1');
$img2        = $this->getRequest()->getParam('imagedata2');
$is_img1     = $this->getRequest()->getParam('isimg1');
$is_img2     = $this->getRequest()->getParam('isimg2');
$latitude    = $this->getRequest()->getParam('latitude');
$longitude   = $this->getRequest()->getParam('longitude');
$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();
if ($this->getRequest()->getMethod() == 'POST') {
    
    if(empty($latitude) || empty($longitude)){
        $reponse = array('status' => -2, 'message' => 'Vui lòng bật GPS');
        echo json_encode($reponse);
        exit();
    }
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QTime       = new Application_Model_Time();
    $QStaff      = new Application_Model_Staff();
    $staffRowSet = $QStaff->find($userStorage->id);
    $staff       = $staffRowSet->current();

    $db = Zend_Registry::get('db');
    
    try {
        $db->beginTransaction();
        if (!empty($staff['regional_market'])) {
            if($staff['status']!= 1){
                $data_staff = array(
                    'status' => 1,
					'off_date' => NULL
                );
                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
                $QStaff->update($data_staff,$where_staff);
            
            }
            $result_date = array();
            $date           = date('Y-m-d');
            $datetime       = date('Y-m-d H:i:s');
            $where          = array();
            $where[]        = $QTime->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $where[]        = $QTime->getAdapter()->quoteInto('off = 0');
            $where[]        = $QTime->getAdapter()->quoteInto('DATE(created_at) = ?', $date);
            $result_date    = $QTime->fetchRow($where);
            
            if (empty($result_date)) { //cham lan dau
                 
                $data_time = array(
                    'staff_id'        => $userStorage->id,
                    'created_at'      => $datetime,
                    'regional_market' => $staff['regional_market'],
                    'note'            => 'check in by mobile tool',
                );
                $data_time['shift'] = $shift;
                 
                $id_time = $QTime->insert($data_time);
                 
                //update log
                $QLog = new Application_Model_Log();
                $ip = $this->getRequest()->getServer('REMOTE_ADDR');
                $info = "TIME - Check in";
                $QLog->insert(array(
                    'info' => $info,
                    'user_id' => $userStorage->id,
                    'ip_address' => $ip,
                    'time' => date('Y-m-d H:i:s'),
                ));
                if(($is_img1 == 1) && ($is_img2 == 1)){
        
                    // insert to time_image_localtion
                    $img2 = str_replace('data:image/png;base64,', '', $img2);
                    $img2 = str_replace(' ', '+', $img2);
                    $data2 = base64_decode($img2);
                    $name2= "image_2_".$userStorage->id."_". time().".png";
                    $file2 = APPLICATION_PATH . "/../public/photo/pg-check-in/" .$name2;
                    $success2 = file_put_contents($file2, $data2);
                    
                    $img1 = str_replace('data:image/png;base64,', '', $img1);
                    $img1 = str_replace(' ', '+', $img1);
                    $data = base64_decode($img1);
                    $name1= "image_1_".$userStorage->id."_". time().".png";
                    $file1 = APPLICATION_PATH . "/../public/photo/pg-check-in/" . $name1;
                    $success1 = file_put_contents($file1, $data);
                    // image 2
                    
                    if($success1 && $success2){
                        // upload success
                        $QTimeImageLocation= new Application_Model_TimeImageLocation();
                        $data_insert_image=array(
                            'time_id'=>$id_time,
                            'staff_id'=>$userStorage->id,
                            'created_at'=>$datetime,
                            'latitude'=>$latitude,
                            'longitude'=>$longitude,
                            'image_1'=>$name1,
                            'image_2'=>$name2,
                        );
                        $QTimeImageLocation->insert($data_insert_image);
        
                        //Insert tbl time_gps
                        $data_gps = array(
                            'staff_id'        => $userStorage->id,
                            'check_in_day'    => date('Y-m-d'),
                            'check_in_at'     => date('Y-m-d H:i:s'),
                            'note'            => 'check in by mobile tool',
                            'created_at'      => $datetime,
                            'regional_market' => $staff['regional_market'],
                            'shift'           => $shift,
                            'image_1'         => $name1,
                            'image_2'         => $name2,
                            'latitude'        => $latitude,
                            'longitude'       => $longitude,
                            'is_half_day'     => ($shift == 9) ? 1 : 0
                        );
                        $QTimeGps->insert($data_gps);
        
//                         $reponse['status']=1;
//                         $reponse['message']="Thành công";
//                         echo json_encode($reponse);
                    }
                }else{
                    $QTimeImageLocation= new Application_Model_TimeImageLocation();
                    $data_insert_image=array(
                        'time_id'=>$id_time,
                        'staff_id'=>$userStorage->id,
                        'created_at'=>$datetime,
                        'latitude'=>$latitude,
                        'longitude'=>$longitude,
                    );
                    $QTimeImageLocation->insert($data_insert_image);
        
                    //Insert tbl time_gps
                    $data_gps = array(
                        'staff_id'        => $userStorage->id,
                        'check_in_day'    => date('Y-m-d'),
                        'check_in_at'     => date('Y-m-d H:i:s'),
                        'note'            => 'check in by mobile tool',
                        'created_at'      => $datetime,
                        'regional_market' => $staff['regional_market'],
                        'shift'           => $shift,
                        'latitude'        => $latitude,
                        'longitude'       => $longitude,
                        'is_half_day'     => ($shift == 9) ? 1 : 0
        
                    );
                    $QTimeGps->insert($data_gps);
         
//                     $reponse['status']=1;
//                     $reponse['message']="Thành công";
//                      echo json_encode($reponse);
                }
        
            }else{
                $id_time=$result_date['id'];
                 
                if(($is_img1 == 1) && ($is_img2 == 1)){
                    // insert to time_image_localtion
                    $img1 = str_replace('data:image/png;base64,', '', $img1);
                    $img1 = str_replace(' ', '+', $img1);
                    $data = base64_decode($img1);
                    $name1= "image_1_".$userStorage->id."_". time().".png";
                    $file1 = APPLICATION_PATH . "/../public/photo/pg-check-in/" . $name1;
                    $success1 = file_put_contents($file1, $data);
                    
                    
                    // image 2
                    $img2 = str_replace('data:image/png;base64,', '', $img2);
                    $img2 = str_replace(' ', '+', $img2);
                    $data2 = base64_decode($img2);
                    $name2= "image_2_".$userStorage->id."_". time().".png";
                    $file2 = APPLICATION_PATH . "/../public/photo/pg-check-in/" .$name2;
                    $success2 = file_put_contents($file2, $data2);
        
                    if($success1 && $success2){
                        // upload success
                        $QTimeImageLocation= new Application_Model_TimeImageLocation();
                         
                        $data_insert_image=array(
                            'image_1'=>$name1,
                            'image_2'=>$name2,
                        );
                         
                        $where_img = $QTimeImageLocation->getAdapter()->quoteInto('time_id = ?', $id_time);
                        $img_location = $QTimeImageLocation->fetchAll($where_img);
                        $id_img_location = $img_location[0]['id'];
                         
                        if(!empty($img_location) and !empty($img_location[0]['image_1']) and !empty($img_location[0]['image_2'])){
                            @unlink(APPLICATION_PATH . "/../public/photo/pg-check-in/" .$img_location[0]['image_1']);
                            @unlink(APPLICATION_PATH . "/../public/photo/pg-check-in/" .$img_location[0]['image_2']);
                        }
                        
                        $where_gps    = array();
                        $where_gps[]  = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
                        $where_gps[]  = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date);
                        
                        $QTimeGps->update($data_insert_image, $where_gps);
                        
                        $where_update = $QTimeImageLocation->getAdapter()->quoteInto('id = ?', $id_img_location);
                        $QTimeImageLocation->update($data_insert_image,$where_update);
                        
//                         $reponse['status']=1;
//                         $reponse['message']="Thành công";
//                         echo json_encode($reponse);
//                         exit();
                    }
                }else{
//                     $reponse['status']=1;
//                     $reponse['message']="Thành công";
//                     echo json_encode($reponse);
//                     exit();
                }
            }
        }
        
        $reponse['status']=1;
        $reponse['message']="Thành công";
        echo json_encode($reponse);
        //commit
        $db->commit();
        exit();
    } catch (Exception $e) {
       
        $reponse['status']=-1;
        $reponse['message']="thất bại";
        echo json_encode($reponse);
        $db->rollback();
        exit();
    }
    
    
}