<?php
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$QTime              = new Application_Model_Time();
$QTimeGps           = new Application_Model_TimeGps();
$QLogCheckIn        = new Application_Model_LogCheckIn();
$QStoreStaffLog     = new Application_Model_StoreStaffLog();
$QOffice            = new Application_Model_Office();
$shift              = $this->getRequest()->getParam('shift');
$latitude           = $this->getRequest()->getParam('latitude');
$longitude          = $this->getRequest()->getParam('longitude');

if ($this->getRequest()->getMethod() == 'POST') {
    $reponse = array('status' => 0, 'message' => 'Thông tin chưa đúng, vui lòng kiểm tra lại');
    if(empty($latitude) || empty($longitude)){
    	$reponse = array('status' => 0, 'message' => 'Vui lòng bật GPS');
    }else {
    	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
    	$QTime = new Application_Model_Time();
    	$QStaff = new Application_Model_Staff();
    	$staffRowSet = $QStaff->find($userStorage->id);
    	$staff = $staffRowSet->current();
    	$title = $userStorage->title;
    	$date = date('Y-m-d');
    	$datetime = date('Y-m-d H:i:s');
    	$staff_id = $userStorage->id;
        $office_id  = $userStorage->office_id;
	$distance_check_in = 0;
        $db = Zend_Registry::get('db');
    	if (!empty($staff['regional_market'])) {
    		$QTimeStaffExpired = new Application_Model_TimeStaffExpired();
    		$where = array();
    		$where[] = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
    		$where[] = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null', null);
    		$result = $QTimeStaffExpired->fetchRow($where);
    		if (empty($result)) {
    			$where = array();
    			$where[]        = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
    			$where[]        = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date);
    			$result_date = $QTimeGps->fetchRow($where);
    			
    			if (empty($result_date)) { //cham lan dau
    				$reponse['message'] = "Bạn chưa Check in";
    			}else {
                            //Check GPS
                            //if PGPB, SENIOR PROMOTER, STORE LEADER, PGPB, SENIOR PROMOTER
                            if(in_array($title, array(419,420,403,182,293) ) ){
                                //Check distanceGPS
                                $where_shop   = array();
                                $where_shop[] = $QStoreStaffLog->getAdapter()->quoteInto('staff_id = ?', $staff_id);
                                $where_shop[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at IS NULL AND is_leader IN (0,3) ');
                                $shop_location = $QStoreStaffLog->fetchAll($where_shop);

                                if(empty($shop_location->toArray())){
                                    $reponse = array('status' => -2, 'message' => 'Bạn chưa được gán shop');
                                    echo json_encode($reponse);
                                    exit();
                                }else{

                                    $sql_get_location = "SELECT st.`id`,str.id as 'store_id',st.`code`, str.`latitude`,str.`longitude`, str.`shipping_address`
                                                                FROM staff st
                                                                INNER JOIN store_staff_log sst ON st.id = sst.`staff_id` AND sst.released_at is null AND sst.is_leader IN (0,3) 
                                                                INNER JOIN store str ON sst.`store_id` = str.`id` 
                                                                WHERE
                                                                 st.id = $staff_id
                                                                AND str.`latitude` IS NOT NULL AND str.`latitude` != '' ";

                                    $stmt_location = $db->prepare($sql_get_location);
                                    $stmt_location->execute();
                                    $result_location = $stmt_location->fetchAll();

                                    if(empty($result_location)){
                                        $reponse = array('status' => -2, 'message' => 'Shop chưa được gán GPS');
                                        echo json_encode($reponse);
                                        exit();
                                    }else{
                                        //Check kc shop
                                        $check_distance = 0;
                                        foreach ($result_location as $store_gps){
                                            $distance_gps = My_DistanceGps::getDistance($store_gps['latitude'], $store_gps['longitude'], $latitude, $longitude, "K");

                                            if($distance_gps <= 2){
                                                $check_distance = 1;
                                                $distance_check_in = $distance_gps;
                                            }
                                        }

                                        if($check_distance == 0){
                                                                //data insert log_check_in
                                                                $date_log = array(
                                                                        'staff_id'        =>   $staff_id,
                                                                        'check_in_day'    =>   $date,
                                                                        'latitude'        =>   $latitude,
                                                                        'longitude'       =>   $longitude,
                                                                        'created_at'      =>   $datetime,
                                                                        'is_check_out'    =>   1
                                                                    
                                                                );
                                                                $QLogCheckIn->insert($date_log);
                                            $reponse = array('status' => -2, 'message' => 'Bạn check out cách shop vượt quá quy định' );
                                            echo json_encode($reponse);
                                            exit();
                                        }
                                    }
                                }
                            }
                            
                            	//ASSISTANT, SALES ADMIN, SERVICE CENTER Team
                                if(in_array($title, array(351,191) )  || in_array($team, array(146))){

                                    if(empty($office_id)){
                                        $reponse = array('status' => -2, 'message' => 'Bạn chưa được gán địa chỉ văn phòng');
                                        echo json_encode($reponse);
                                        exit();
                                    }

                                    $row_office = $QOffice->getLocationOffice($office_id);

                                    if(empty($row_office)){
                                        $reponse = array('status' => -2, 'message' => 'Văn phòng phòng bạn gán ko đúng');
                                        echo json_encode($reponse);
                                        exit();
                                    }

                                    $distance_office = My_DistanceGps::getDistance($row_office['latitude'], $row_office['longitude'], $latitude, $longitude, "K");

                                    if($distance_office > 2){
                                                    //data insert log_check_in
                                                                    $date_log = array(
                                                                            'staff_id'        =>   $staff_id,
                                                                            'check_in_day'    =>   $date,
                                                                            'latitude'        =>   $latitude,
                                                                            'longitude'       =>   $longitude,
                                                                            'created_at'      =>   $datetime,
                                                                             'is_check_out'   =>   1
                                                                    );
                                                                    $QLogCheckIn->insert($date_log);

                                        $reponse = array('status' => -2, 'message' => 'Bạn check out cách văn phòng vượt quá quy định');
                                        echo json_encode($reponse);
                                        exit();
                                    }

                                }
    
                            //End Check GPS
                            
    				$QTimeImageLocation= new Application_Model_TimeImageLocation();
    				//Check id_time exits in ImageLocation
    				$where_update = array();
    				$where_update[] = $QTimeImageLocation->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
    				$where_update[] = $QTimeImageLocation->getAdapter()->quoteInto('check_in_day = ?', $date);
    				$result_update = $QTimeImageLocation->fetchAll($where_update);
    				
    				if(!empty($result_update) and count($result_update) == 1){
    					$data_insert_image=array(
    							 'check_in_day'=>date('Y-m-d'),
    							'staff_id'=>$userStorage->id,
    							'created_at'=>$datetime,
    							'latitude'=>$latitude,
    							'longitude'=>$longitude,
    								
    					);
    					$QTimeImageLocation->insert($data_insert_image);
    					
    					$where_gps    = array();
    					$where_gps[]  = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
    					$where_gps[]  = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date);
    					
                        $gpsInfo = $QTimeGps->fetchAll($where_gps);
                        $dateDiff = intval((strtotime(date ( 'Y-m-d H:i:s' )) - strtotime($gpsInfo[0]['check_in_at']))/60);
                        $hoursWork = intval($dateDiff/60);
                        
                        if($hoursWork >= 6){
                            $time_number = 1;
                        }elseif ($hoursWork >= 2){
                            $time_number = 0.5;
                        }else{
                            $time_number = 0;
                        }
                        
                        $data_gps = array(
                            'check_out_at' => date('Y-m-d H:i:s'),
                            'time_number'  => $time_number,
                            'real_time'    => $hoursWork,
                            'status'       => 1
                        );
                        $QTimeGps->update($data_gps, $where_gps);
    				
    					$reponse['status']=1;
    					$reponse['message']="Thành công";
    				}else{
    					$reponse['message'] = "Bạn đã Check out rồi";
    				}
    			}
    				
    		} else {
    			$reponse['message'] = "You have banned ! Please contact HR Group";
    		}
    	} else {
    		$reponse['message'] = "Not set Regional ! Please contact HR Group";
    	}
    }
    
    $this->view->mess = $reponse;
    echo json_encode($reponse);
    
    exit();
}