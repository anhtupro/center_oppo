<?php

        $this->_helper->layout->setLayout('layout_bi_new');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $flashMessenger = $this->_helper->flashMessenger;
        
        $title = $userStorage->title;
        
        /*SALES,TRADE MARKETING EXECUTIVE,TRADE MARKETING LEADER,
         TRADE MARKETING SUPERVISOR,SENIOR TRAINER,TRAINER,TRAINER LEADER,
         TRAINER LEADER STANDBY,TRAINER TRAINEE,TRAINING SUPERVISOR*/
        if(in_array($title, array(164,162,399,424,295,319,316,183,173,349,168,317,175,174,279,281,383))){
            $this->_redirect('/staff-time/staff-view');
        }
        
        //get staff info
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $this->view->staff = $staff = $QStaff->fetchRow($where);
		$QShift            = new Application_Model_Shift();
		$shift             = $QShift->getShiftByTitle($userStorage->title);
		
		$this->view->shift = $shift;
		
        $this->view->user_id = $userStorage->id;
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;