<?php
    
    $id          = $this->getRequest()->getParam('id');
    $shift       = $this->getRequest()->getParam('shift');
    $off         = $this->getRequest()->getParam('off');
    
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    set_time_limit(0);

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QLockTiming = new Application_Model_HrTimingLock();
    $QTime       = new Application_Model_Time();
    $QStaff      = new Application_Model_Staff();
    $QOffHistory = new Application_Model_OffHistory();
    $staffRowSet = $QStaff->find($userStorage->id);
    $staff       = $staffRowSet->current();


    $shift = intval($shift);



    if (!$id){
        My_Controller::jsAlert("id is not valid",'error');
        exit;
    }

    $time = $QTime->find($id)->current();

    if (!$time){
        My_Controller::jsAlert("invalid time checking",'error');            
        exit;
    }

    $staff_checkin = $QStaff->find($time['staff_id'])->current();

    if($off){
        if(My_Staff_Title::isPg($staff_checkin['title'])){
            $shift = 4;    
        }      
    }

    $datetime = new DateTime($time['created_at']);
    $day      = $datetime->format('d');
    $month    = $datetime->format('m');
    $year     = $datetime->format('Y');

    $whereLocktiming   = array();
    $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('month = ?' , intval($month));
    $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('year = ?' , intval($year));
    $lockTiming        = $QLockTiming->fetchRow($whereLocktiming);

    if($lockTiming){ 
        My_Message::jsAlert("This month is lock",'error');
        exit;                
    }

    if (!$shift AND My_Staff_Title::isPg($staff_checkin['title'])){
        My_Message::jsAlert("Please input shift for change",'error');
        exit;
    }

    $db = Zend_Registry::get('db');
    $db->beginTransaction();
    try {
        $where   = array();
        $where[] = $QTime->getAdapter()->quoteInto('id = ?' , $id);
        $data    = array('shift' => $shift,'off' => $off );
        $QTime->update($data , $where);

        //nếu có thay đổi trạng thái chấm công
        if(intval($off) != intval($time['off'])){
            if($time['approved_by']){
                if($off){
                    $data = array(
                        'staff_id' => $time['staff_id'],
                        'month_id' => $m,
                        'date'     => $dateTime,
                        'type'     => $time['off_type'],
                        'reason'   => $time['reason'],
                        'shift'    => $shift,
                        'off_date' => 0,
                        'time_id'  => $id,
                    );
                    $QOffHistory->insert($data);
                }else{
                    $where_history = $QOffHistory->getAdapter()->quoteInto('time_id = ?',$time['id']);
                    $QOffHistory->delete($where_history);
                }
            }
        }

        //todo log
        $ip   = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = "TIME SYSTEM - UPDATE (" . $id . ")";
        $log  = array(
            'info'       => $info,
            'user_id'    => $userStorage->id,
            'ip_address' => $ip,
            'time'       => date('Y-m-d H:i:s'),
        );

        My_Log::log($log);
        $db->commit();
        My_Message::jsAlert("success",'success');
    } catch (Exception $e) {
        $db->rollBack();
        My_Message::jsAlert($e->getMessage(),'error');
        exit;
    }
    
    $back_url = $this->getRequest()->getParam('back_url');
    echo '<script>parent.location.href="/time"</script>';
    exit;

        

