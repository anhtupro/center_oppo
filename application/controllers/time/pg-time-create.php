<?php
                
		// $this->_redirect('/staff-time/staff-view');
		$this->_helper->layout->setLayout('layout_bi_new');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $flashMessenger = $this->_helper->flashMessenger;

        //get staff info
        $QStaff = new Application_Model_Staff();
        $QPgcheckDate = new Application_Model_PgcheckDate();
        $QPgcheckCategory = new Application_Model_PgcheckCategory();
        $QPgcheckChannelFile = new Application_Model_PgcheckChannelFile();
        
        $pg_id = $userStorage->id;
        
        $channel_id = $QPgcheckDate->getChannelIdFromStaff($pg_id);
        
        $channel_id_list = NULL;
        foreach($channel_id as $key=>$value){
            $channel_id_list[] = $value;
        }
        
        if($channel_id_list){
            $pgcheck_category = $QPgcheckCategory->getCategory($channel_id_list);
        }
        
        $list_store = $QPgcheckDate->getListStore($pg_id);
        
        if($channel_id_list){
            $image_flash_sale = $QPgcheckChannelFile->getList($channel_id_list);
        }
        
        $this->view->pgcheck_category = $pgcheck_category;
        $this->view->list_store = $list_store;
        $this->view->image_flash_sale = $image_flash_sale;
        
        $where = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $this->view->staff = $staff = $QStaff->fetchRow($where);
		$QShift            = new Application_Model_Shift();
		$shift             = $QShift->getShiftByTitle($userStorage->title);
		$this->view->title = $userStorage->title;
		$this->view->team = $userStorage->team;
		
		$this->view->shift = $shift;
		
		$db = Zend_Registry::get('db');
		$sql = "SELECT fn_check_group_local(:_title_id) as `is_local`";
		
		$_title_id = $userStorage->title;
		$stmt = $db->prepare($sql);
		$stmt->bindParam('_title_id', $_title_id, PDO::PARAM_INT);
		
		$stmt->execute();
		$data = $stmt->fetch();
		
		if($data['is_local'] == 3){
		    $this->view->is_local = 1;
		}
		
		
// 		$stmt->closeCursor();
// 		$stmt = $db = null;
		
//        $QDate = new Application_Model_DateNorm();
//        $date = $QDate->get_cache();
//        $this->view->date = $date;

//        $QTime = new Application_Model_Time();
//        $where = array();
//        $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
//        $where[] = $QTime->getAdapter()->quoteInto('DATE(created_at) = CURDATE()', '');
//        $time = $QTime->fetchRow($where);
//
//        if (isset($time) and $time)
//        {
//            $this->view->time = 1;
//        }

        $this->view->user_id = $userStorage->id;
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;