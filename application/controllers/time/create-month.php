<?php
           
  $userStorage = Zend_Auth::getInstance()->getStorage()->read();
  $user_id     = $userStorage->id;
  $user_group  = $userStorage->group_id;
  $flashMessenger = $this->_helper->flashMessenger;
  $id = $this->getRequest()->getParam('id');
  $month = $this->getRequest()->getParam('month', date('n'));
  $year  = $this->getRequest()->getParam('year' , date('Y'));

  $QTeam = new Application_Model_Team();
  $team = $QTeam->fetchAll();
  $team_data = array();
  foreach($team as $k=>$v)
  {
      $team_data[$v['id']] = $v['name'];
  }
  $this->view->team = $team_data;
  //get staff info
  $QStaff = new Application_Model_Staff();
  $where = $QStaff->getAdapter()->quoteInto('id = ?', $id);
  $this->view->staff = $staff = $QStaff->fetchRow($where);

  /*if(!in_array($user_group , array(ASMSTANDBY_ID ,ASM_ID ,HR_ID ,HR_EXT_ID , ADMINISTRATOR_ID)) and $user_id != $id)
  {
        throw new exception('you dont\'t have permission!');
  }
  */

  $QDate = new Application_Model_DateNorm();
  $date = $QDate->get_cache();
  $this->view->date = $date;

  $QTime = new Application_Model_Time();
  $where = array();
  $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
  $where[] = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $month);
  $where[] = $QTime->getAdapter()->quoteInto('YEAR(created_at) = ?', $year);

  $time = count($QTime->fetchAll($where));

  if (isset($time) and $time)
  {
      $this->view->time = $time;
  }

  $this->view->month = $month;
  //back url
  $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

  $messages = $flashMessenger->setNamespace('error')->getMessages();
  $this->view->messages = $messages;

  $messages_success = $flashMessenger->setNamespace('success')->getMessages();
  $this->view->messages_success = $messages_success;

  //office
 /* if(isset($staff['is_officer']) and $staff['is_officer'])
  {
      $this->_helper->viewRenderer('time/create-month-office', null, true);
  }
  */
