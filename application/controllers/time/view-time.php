<?php
    
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

//get staff info
try {
    $id = $this->getRequest()->getParam('id');

    if (empty($id))
        throw new exception('Invalid Staff id');

    $QTime  = new Application_Model_Time();
    $QStaff = new Application_Model_Staff();
    $QTeam  = new Application_Model_Team();
    $QShift = new Application_Model_Shift();

    $team = $QTeam->get_cache();

    $timeRowset        = $QTime->find($id);
    $time              = $timeRowset->current();
    $this->view->shift = $QShift->get_cache();
    $this->view->team  = $team;

    $staff = $QStaff->find($time['staff_id']);
    $staff = $staff->current();

    $staff_approved = $QStaff->find($time['approved_by']);
    $staff_approved = $staff_approved->current();

    $QDate = new Application_Model_DateNorm();
    $date = $QDate->get_cache();

    $this->view->isPg = My_Staff_Title::isPg($staff['title']);
    $this->view->staff_approved = $staff_approved;
    $this->view->date     = $date;
    $this->view->staff    = $staff;
    $this->view->time     = $time;
    $this->view->user_id  = $userStorage->id;
    $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;

    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}
catch (Exception $e){
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect(($back_url ? $back_url : HOST . 'time'));
}