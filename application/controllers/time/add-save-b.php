<?php
	$from_date = $this->getRequest()->getParam('from_date');
	$staffs    = $this->getRequest()->getParam('a_staffresult');
	$areas     = $this->getRequest()->getParam('a_area');
	$teams     = $this->getRequest()->getParam('team');
	$shilf     = $this->getRequest()->getParam('shilf', 0);
	$asm       = $this->getRequest()->getParam('asm');
	$off       = $this->getRequest()->getParam('off', '');
	$yesterday = $this->getRequest()->getParam('yesterday');
	$type      = $this->getRequest()->getParam('type');
	$to_date   = $this->getRequest()->getParam('to');
	$reason    = $this->getRequest()->getParam('reason', '');
	$shift     = $this->getRequest()->getParam('shift');

	$flashMessenger = $this->_helper->flashMessenger;
	$QTime = new Application_Model_Time();
	$QStaff = new Application_Model_Staff();
    $QLockTiming = new Application_Model_HrTimingLock();
	$QTimePermission = new Application_Model_TimePermission();
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$user_id = $userStorage->id;
	$group_id = $userStorage->group_id;

	try
	{
	    $db = Zend_Registry::get('db');
	    $db->beginTransaction();


	    if (!isset($asm))
	    {
	        if (!$areas && $teams != ACCESSORIES_TEAM)
	        {
	            throw new Exception("Please choose Areas");
	        }

	        if (!$teams)
	        {
	            throw new Exception("Please choose your team");
	        }

	       

	        if (!$staffs)
	        {
	            throw new Exception("Please choose staff for checkin");
	        }
	        if (HAPPY_TIME_CHECKIN == 1)
	        {
	            if (!$from_date)
	            {
	                throw new Exception("Please Choose Date for Checkin");
	            }
	        }
	    }


	    $temp = explode('/', $from_date);

	    if (!(isset($temp[2]) and intval($temp[2]) >= 2013 and intval($temp[2]) <= 2020 and
	        isset($temp[1]) and intval($temp[1]) <= 12 and intval($temp[1]) >= 1 and isset($temp[0]) and
	        intval($temp[0]) <= 31 and intval($temp[0]) >= 1))
	    {
	        throw new Exception("Date format is error");
	    }

	    if (!in_array($group_id, array(
	            ADMINISTRATOR_ID,
	            HR_ID,
	            HR_EXT_ID)))
	    {
	        if (HAPPY_TIME_CHECKIN != 1)
	        {
	            if ($temp[0] != date('d'))
	            {
	                throw new Exception("Hacking is attemp! Information will report");
	            }
	        }
	    }


	    // xin nghi off
	    if (isset($off) and $off)
	    {
	        if (count($staffs) != 1)
	        {
	            throw new Exception("Please choose only one staff for request off");
	        }
	        $from_date = $this->getRequest()->getParam('from');

	        $from_date = explode('/', $from_date);
	        $from_date = $from_date[2] . '-' . $from_date[1] . '-' . $from_date[0];

	        $to_date = explode('/', $to_date);
	        $to_date = $to_date[2] . '-' . $to_date[1] . '-' . $to_date[0];

	        $startTime = strtotime($from_date);
	        $endTime = strtotime($to_date);

	        // Loop between date
	        for ($i = $startTime; $i <= $endTime; $i = $i + 86400)
	        {

	            $date = date('Y-m-d', $i);
	            $d = date('d', strtotime($date));
	            $m = intval(date('m', strtotime($date)));
	            $y = date('Y', strtotime($date));


	            $result = $QTime->fetchRow($where);
	            if (isset($result) and $result)
	            {
	                throw new Exception("Checking List is exist");
	            }

	            $data = array(
	                'staff_id' => $staffs[0],
	                'created_at' => $date,
	                'off' => 1,
	                'reason' => $reason,
	                'off_type' => $type,
	                'shift' => $shilf
	            );
	            $QTime->insert($data);

	            $QLog = new Application_Model_Log();
	            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
	            $info = "TIME ADD - OFF for (" . $staffs[0] . ")";
	            //todo log
	            $QLog->insert(array(
	                'info' => $info,
	                'user_id' => $userStorage->id,
	                'ip_address' => $ip,
	                'time' => date('Y-m-d H:i:s'),
	                ));
	        }
	    } else
	    {
	        $from_date = explode('/', $from_date);
	        $from_date = $from_date[2] . '-' . $from_date[1] . '-' . $from_date[0];

	        if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
	            ADMINISTRATOR_ID,
	            HR_ID,
	            HR_EXT_ID,
	            BOARD_ID)))
	        {
	        } else
	        {
	            $where = $QTimePermission->getAdapter()->quoteInto('staff_id = ? ', $user_id);
	            $result = $QTimePermission->fetchAll($where);
	            if (!$result)
	            {
	                throw new Exception("Permission denied");
	            }
	        }




	        if (isset($yesterday) and $yesterday)
	            $from_date = date("Y-m-d", strtotime($from_date . "- 1 days"));


	        foreach ($staffs as $k => $staff_id)
	        {
	            $staff_rowset = $QStaff->find($staff_id);
	            $staff = $staff_rowset->current();

	            if (!$staff)
	            {
	                throw new Exception("Invalid staff, Please try again");
	            }

                $joined_at = strtotime($staff['joined_at']);

                if(strtotime($from_date) < $joined_at)
                    throw new Exception("You can\'t add time before staff's joined date");

	            $datetime = new DateTime($from_date);
	            $day = $datetime->format('d');
	            $month = $datetime->format('m');
	            $year = $datetime->format('Y');

	            $where = array();

                $whereLocktiming   = array();
                $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('month = ?' , intval($month));
                $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('year = ?' , intval($year));
                $lockTiming        = $QLockTiming->fetchRow($whereLocktiming);

                if($lockTiming)
                {
                    return array(
                        'code' => -1,
                        'message' => 'This month can\'t not approve!',
                    );
                }

	            $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $staff->id);
	            $where[] = $QTime->getAdapter()->quoteInto('DAY(created_at) = ?', $day);
	            $where[] = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $month);
	            $where[] = $QTime->getAdapter()->quoteInto('YEAR(created_at) = ?', $year);

	            $result = $QTime->fetchRow($where);
	         
	            if (isset($result) and $result)
	            {
	                throw new Exception("Checking List is exist in: " . $datetime->format('d-m-Y'));
	            }

	            $data = array(
	                    'staff_id'        => $staff_id,
	                    'created_at'      => $from_date,
	                    'reason'          => $reason,
	                    'approved_at'     => date('Y-m-d H:i:s'),
	                    'approved_by'     => $user_id,
	                    'status'          => 1,
	                    'add_at'          => date('Y-m-d H:i:s'),
	                    'add_by'          => $user_id,
	                    'regional_market' => $staff['regional_market'],
	                    'shift'           => $shift ? $shift : 0
	            );

	            if (isset($shilf) and $shilf)
	                $data['shift'] = 1;

	            $QTime->insert($data);

	            $QLog = new Application_Model_Log();
	            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
	            $info = "TIME ADD - CHECK for (" . $staff_id . ")";
	            //todo log
	            $QLog->insert(array(
	                'info' => $info,
	                'user_id' => $userStorage->id,
	                'ip_address' => $ip,
	                'time' => date('Y-m-d H:i:s'),
	                ));

	              echo '<script>
						    parent.palertDone("Done");
						    parent.alert("Done");
				        </script>';

	        }
	    }
		
		$db->commit();
        exit;

	}
	catch (exception $ex)
	{
	    $db->rollback();
	    echo '<script>
	    parent.palert("' . $ex->getMessage() . '");
	    parent.alert("' . $ex->getMessage() . '");
	         </script>';
	    exit;
	}

exit;