<?php

define('MAX_SIZE_UPLOAD', 3145728);
$this->_helper->layout->disableLayout();
$reponse = array();
if ($this->getRequest()->getMethod() == 'POST') {
    $reponse = array('status' => 1, 'message' => 'Thành công');
    $image1 = $this->getRequest()->getParam('upload_iphone1_hide');
    $image2 = $this->getRequest()->getParam('upload_iphone2_hide');
    $image3 = $this->getRequest()->getParam('upload_iphone3_hide');
    
    $latitude = $this->getRequest()->getParam('latitude1');
    $longitude = $this->getRequest()->getParam('longitude1');
    $check_out_pg = $this->getRequest()->getParam('check_out_pg');

    $city_input4 = $this->getRequest()->getParam('city_input4');
    $city_input5 = $this->getRequest()->getParam('city_input5');
    $city_input6 = $this->getRequest()->getParam('city_input6');
    $city_input8 = $this->getRequest()->getParam('city_input8');
    
    $store_id_ios   = $this->getRequest()->getParam('store_id_ios');
    $pgcheck_note   = $this->getRequest()->getParam('pgcheck_note');
    $pgcheck_category_ios   = $this->getRequest()->getParam('pgcheck_category_ios');


    $address_input1 = $this->getRequest()->getParam('address_input1');

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QTime = new Application_Model_Time();
    $QTimeGps = new Application_Model_TimeGps();
    $QLogStaffTime = new Application_Model_LogStaffTime();
    $QOffice = new Application_Model_Office();
    $QLogCheckIn = new Application_Model_LogCheckIn();
    $QStoreStaffLog = new Application_Model_StoreStaffLog();
    $QPgcheckDate      = new Application_Model_PgcheckDate();
    $QPgcheckDateDetails      = new Application_Model_PgcheckDateDetails();
    
    $shift = $this->getRequest()->getParam('shift1');
    $name1 = "image_1_" . $userStorage->id . "_" . time() . ".jpg";
    $name2 = "image_2_" . $userStorage->id . "_" . time() . ".jpg";
    $name3 = "image_3_" . $userStorage->id . "_" . time() . ".jpg";
    $file1 = APPLICATION_PATH . "/../public/photo/pg-check-in/" . $name1;
    $file2 = APPLICATION_PATH . "/../public/photo/pg-check-in/" . $name2;
    $file3 = APPLICATION_PATH . "/../public/photo/pg-check-in/" . $name3;
    $QStaffStatusLog = new Application_Model_StaffStatusLog();
    $db = Zend_Registry::get('db');
    $date = date('Y-m-d');
    $datetime = date('Y-m-d H:i:s');
    $staff_id = $userStorage->id;
    $department = $userStorage->department;
    $team = $userStorage->team;
    $title = $userStorage->title;
    $email = $userStorage->email;
    $office_id = $userStorage->office_id;
    $distance_check_in = 0;
    if (empty($latitude) || empty($longitude)) {
        $reponse = array('status' => -2, 'message' => 'Vui lòng bật GPS');
        $this->view->mess = $reponse;
        return;
    }
    //if PGPB, SENIOR PROMOTER, STORE LEADER, PGPB, SENIOR PROMOTER
    if (in_array($title, array(419, 420, 403, 182, 293))) {
        //Check distanceGPS
        $where_shop = array();
        $where_shop[] = $QStoreStaffLog->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where_shop[] = $QStoreStaffLog->getAdapter()->quoteInto('released_at IS NULL AND is_leader IN (0,3) ', null);
        $shop_location = $QStoreStaffLog->fetchAll($where_shop);

        if (empty($shop_location->toArray())) {
            $reponse = array('status' => -2, 'message' => 'Bạn chưa được gán shop');
            $this->view->mess = $reponse;
            return;
        } else {

            $sql_get_location = "SELECT st.`id`,str.id as 'store_id',st.`code`, str.`latitude`,str.`longitude`, str.`shipping_address`
		        FROM staff st
		        INNER JOIN store_staff_log sst ON st.id = sst.`staff_id` AND sst.released_at is null AND sst.is_leader IN (0,3)
		        INNER JOIN store str ON sst.`store_id` = str.`id`
		        WHERE
		        st.id = $staff_id
		        AND str.`latitude` IS NOT NULL AND str.`latitude` != '' ";

            $stmt_location = $db->prepare($sql_get_location);
            $stmt_location->execute();
            $result_location = $stmt_location->fetchAll();

            if (empty($result_location)) {
                $reponse = array('status' => -2, 'message' => 'Shop chưa được gán GPS');
                $this->view->mess = $reponse;
                return;
            } else {
                //Check kc shop
                $check_distance = 0;
                foreach ($result_location as $store_gps) {
                    $distance_gps = My_DistanceGps::getDistance($store_gps['latitude'], $store_gps['longitude'], $latitude, $longitude, "K");

                    if ($distance_gps <= 2) {
                        $check_distance = 1;
                        $distance_check_in = $distance_gps;
                    }
                }

                if ($check_distance == 0) {

                    //data insert log_check_in
                    $date_log = array(
                        'staff_id' => $staff_id,
                        'check_in_day' => $date,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'city1' => $city_input4,
                        'city2' => $city_input5,
                        'city3' => $city_input6,
                        'address' => $address_input1,
                        'created_at' => $datetime
                    );
                    $QLogCheckIn->insert($date_log);
                    $reponse = array('status' => -2, 'message' => 'Bạn check in cách shop vượt quá quy định');
                    $this->view->mess = $reponse;
                    return;
                }
            }
        }
    }

    //ASSISTANT, SALES ADMIN, SERVICE CENTER Team
    if (in_array($title, array(351, 191, 395)) || in_array($team, array(146))) {
        if (empty($office_id)) {
            $reponse = array('status' => -2, 'message' => 'Bạn chưa được gán địa chỉ văn phòng');
            $this->view->mess = $reponse;
            return;
        }

        $row_office = $QOffice->getLocationOffice($office_id);

        if (empty($row_office)) {
            $reponse = array('status' => -2, 'message' => 'Văn phòng phòng bạn gán ko đúng');
            $this->view->mess = $reponse;
            return;
        }

        $distance_office = My_DistanceGps::getDistance($row_office['latitude'], $row_office['longitude'], $latitude, $longitude, "K");

        if ($distance_office > 2) {
            //data insert log_check_in
            $date_log = array(
                'staff_id' => $staff_id,
                'check_in_day' => $date,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'city1' => $city_input4,
                'city2' => $city_input5,
                'city3' => $city_input6,
                'address' => $address_input1,
                'created_at' => $datetime
            );
            $QLogCheckIn->insert($date_log);
            $reponse = array('status' => -2, 'message' => 'Bạn check in cách văn phòng vượt quá quy định');
            $this->view->mess = $reponse;
            return;
        }
    }
    
    if (in_array($title, array(419, 420, 403, 182, 293))) {
        if ($shift == 1 AND strtotime(date('H:i:s')) > strtotime('10:00:00')) {
            $reponse = array('status' => -1, 'message' => 'Ca sáng không check in sau 10h');
            $this->view->mess = $reponse;
            return;
        }
    }else {
        if ($shift == 1 AND strtotime(date('H:i:s')) > strtotime('09:00:00')) {
            $reponse = array('status' => -1, 'message' => 'Ca sáng không check in sau 9h');
            $this->view->mess = $reponse;
            return;
        }
    }

    //if team trade
    if (in_array($team, array(131, 611)) || ($staff_id == 5899)) {
        $select_area = "SELECT st.id, p.`name`  AS name_gps
                        FROM trade_marketing.staff st
			LEFT JOIN trade_marketing.area_staff ast ON st.id = ast.`staff_id` 
			LEFT JOIN trade_marketing.province p ON (
                            ast.area_id = p.area_id
                            OR ast.area_id
                                IN (
				p.area_id
				)
                            )
			WHERE st.`email` =  '$email'
			AND p.`name` IS NOT NULL
                        UNION ALL
			SELECT st.id, CASE
                            WHEN IFNULL(ast.area_id,0) IN (1,24,25,26,34) THEN 'Hồ Chí Minh'
                            WHEN IFNULL(ast.area_id,0) IN (10,51,32,33) THEN 'Hà Nội'
                            WHEN IFNULL(ast.area_id,0) IN (61) THEN 'Nghệ An'
                            WHEN IFNULL(ast.area_id,0) IN (55) THEN 'Bình Dương'
                            WHEN IFNULL(ast.area_id,0) IN (53) THEN 'Đồng Nai'
                            ELSE NULL END AS name_gps
			FROM trade_marketing.staff st
			LEFT JOIN trade_marketing.area_staff ast ON st.id = ast.`staff_id` 
			WHERE st.`email` =  '$email'
			AND ast.area_id IN (1,24,25,26,34,10,51,32,33,61,55,53) ";
        $stmt_area = $db->prepare($select_area);
        $stmt_area->execute();
        $result_area = $stmt_area->fetchAll();

        //data insert log_check_in
        $date_log = array(
            'staff_id' => $staff_id,
            'check_in_day' => $date,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'city1' => $city_input4,
            'city2' => $city_input5,
            'city3' => $city_input6,
            'address' => $address_input1,
            'created_at' => $datetime
        );
    }

   

    $QTeam = new Application_Model_Team();
    $QStaff = new Application_Model_Staff();
    $staffRowSet = $QStaff->find($userStorage->id);
    $staff = $staffRowSet->current();

    $title_id = $staff['title'];

    $titleRowSet = $QTeam->find($title_id);
    $check_in_group = $titleRowSet->current()['check_in_group'];

    $db = Zend_Registry::get('db');
    try {
        $db->beginTransaction();
        if (!empty($staff['regional_market'])) {
            if ($staff['status'] != 1) {
                $data_staff = array(
                    'status' => 1
                );
                $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
                $QStaff->update($data_staff, $where_staff);
                $array_log = array(
                    'staff_id' => $userStorage->id,
                    'status' => 1,
                    'created_by' => $userStorage->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'note' => 'Staff Check in',
                );
                $QStaffStatusLog->insert($array_log);
            }
            $date = date('Y-m-d');
            $datetime = date('Y-m-d H:i:s');

            $where = array();
            $where[] = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $where[] = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date);
            $result_date = $QTimeGps->fetchRow($where);
            
            //Kiểm tra hình ảnh
            if(in_array($title, array(419,420,403,182,293) ) ){
                $pg_id = $userStorage->id;
                
                $channel_id = $QPgcheckDate->getChannelIdFromStaffStore($pg_id, $store_id_ios);
                
                $data = array(
                    'pg_id'   => $pg_id,
                    'store_id' => $store_id_ios,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id,
                    'shift' => $shift,
                    'channel_id' => $channel_id,
                    'note' => $pgcheck_note
                );

                $pgcheck_date_id = $QPgcheckDate->insert($data);

                $category = $QPgcheckDate->getListCategoryChannel($channel_id);
                
                $pgcheck_category_ios = explode(",",$pgcheck_category_ios);
                
                foreach($category as $key=>$value){
                    $details = [
                        'pgcheck_date_id' => $pgcheck_date_id,
                        'pgcheck_category_id' => $value['id'],
                        'status' => (in_array($value['id'], $pgcheck_category_ios)) ? 1 : 0,
                    ];
                    $QPgcheckDateDetails->insert($details);
                }
            }
            //END - Kiểm tra hình ảnh
            
            if (empty($result_date)) { // cham lan dau
                $data_time = array(
                    'staff_id' => $userStorage->id,
                    'created_at' => $datetime,
                    'regional_market' => $staff['regional_market'],
                    'note' => 'check in by ios'
                );
                $data_time['shift'] = $shift;

                $id_time = $QTime->insert($data_time);

                // update log
                $QLog = new Application_Model_Log();
                $ip = $this->getRequest()->getServer('REMOTE_ADDR');
                $info = "TIME - Check in";
                $QLog->insert(array(
                    'info' => $info,
                    'user_id' => $userStorage->id,
                    'ip_address' => $ip,
                    'time' => $datetime
                ));

                //Insert Log_Staff_Time
                $log_staff_time = array(
                    'staff_id' => $userStorage->id,
                    'check_in_day' => $date,
                    'info' => 'check in by mobile tool',
                    'created_at' => $datetime,
                    'created_by' => $userStorage->id,
                    'ip_address' => $ip
                );

                $QLogStaffTime->insert($log_staff_time);

                if (!empty($_FILES['file_1']['name']) and!empty($_FILES['file_2']['name'])) {
                    // Upload images
                    //upload image 1
                    $source = fopen($image1, 'r');
                    $destination = fopen($file1, 'w');
                    $success1 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);

                    //upload image 2
                    $source = fopen($image2, 'r');
                    $destination = fopen($file2, 'w');
                    $success2 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);
                    
                    //upload image 3
                    $source = fopen($image3, 'r');
                    $destination = fopen($file3, 'w');
                    $success3 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);
                    
                    if ($success1 && $success2) {
                        // upload success
                        $QTimeImageLocation = new Application_Model_TimeImageLocation();
                        $data_insert_image = array(
                            'check_in_day' => date('Y-m-d'),
                            'staff_id' => $userStorage->id,
                            'created_at' => $datetime,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                            'image_1' => $name1,
                            'image_2' => $name2,
                            'image_3' => $name3 ? $name3 : NULL,
                        );
                        $QTimeImageLocation->insert($data_insert_image);

                        // Insert tbl time_gps
                        $data_gps = array(
                            'staff_id' => $userStorage->id,
                            'check_in_day' => date('Y-m-d'),
                            'check_in_at' => $datetime,
                            'note' => 'check in by ios',
                            'created_at' => $datetime,
                            'regional_market' => $staff['regional_market'],
                            'shift' => $shift,
                            'image_1' => $name1,
                            'image_2' => $name2,
                            'image_3' => $name3,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                            'is_half_day' => ($shift == 9) ? 1 : 0,
                            'address' => $address_input1
                        );

                        /* SALES,TRADE MARKETING EXECUTIVE,TRADE MARKETING LEADER,
                          TRADE MARKETING SUPERVISOR,SENIOR TRAINER,TRAINER,TRAINER LEADER,
                          TRAINER LEADER STANDBY,TRAINER TRAINEE,TRAINING SUPERVISOR */
                        if (in_array($title, array(164, 162, 399, 424, 295, 319, 316, 183, 417, 173, 537, 574, 521, 349, 168, 317, 175, 174, 279, 281, 383, 395)) || in_array($department, array(TRADE_MARKETING_DEPT ,TRANING_DEPT )) ||  in_array($team, [TEAM_TRADE_MARKETING,TRAINING_TEAM]) ||  in_array($team, [TEAM_TRADE_MARKETING,TRAINING_TEAM])) {
                            $data_gps['status'] = 1;
                            $data_gps['time_number'] = ($shift == 9) ? 0.5 : 1;
                        }

                        $QTimeGps->insert($data_gps);
                    }
                } else {
                    $QTimeImageLocation = new Application_Model_TimeImageLocation();
                    $data_insert_image = array(
                        'check_in_day' => date('Y-m-d'),
                        'staff_id' => $userStorage->id,
                        'created_at' => $datetime,
                        'latitude' => $latitude,
                        'longitude' => $longitude
                    );
                    $QTimeImageLocation->insert($data_insert_image);

                    // Insert tbl time_gps
                    $data_gps = array(
                        'staff_id' => $userStorage->id,
                        'check_in_day' => date('Y-m-d'),
                        'check_in_at' => $datetime,
                        'note' => 'check in by ios',
                        'created_at' => $datetime,
                        'regional_market' => $staff['regional_market'],
                        'shift' => $shift,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'is_half_day' => ($shift == 9) ? 1 : 0,
                        'address' => $address_input1
                    );
                    /* SALES,TRADE MARKETING EXECUTIVE,TRADE MARKETING LEADER,
                      TRADE MARKETING SUPERVISOR,SENIOR TRAINER,TRAINER,TRAINER LEADER,
                      TRAINER LEADER STANDBY,TRAINER TRAINEE,TRAINING SUPERVISOR */
                    if (in_array($title, array(164, 162, 399, 424, 295, 319, 316, 183, 537, 574, 521, 173, 349, 168, 317, 175, 174, 279, 281, 383, 395)) || in_array($department, array(TRADE_MARKETING_DEPT ,TRANING_DEPT )) ||  in_array($team, [TEAM_TRADE_MARKETING,TRAINING_TEAM])) {
                        $data_gps['status'] = 1;
                        $data_gps['time_number'] = ($shift == 9) ? 0.5 : 1;
                    }
                    $QTimeGps->insert($data_gps);
                }
            } else {
                if (!empty($_FILES['file_1']['name']) and!empty($_FILES['file_2']['name'])) {
                    //Upload images
                    //upload image 1
                    $source = fopen($image1, 'r');
                    $destination = fopen($file1, 'w');
                    $success1 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);

                    //upload image 2
                    $source = fopen($image2, 'r');
                    $destination = fopen($file2, 'w');
                    $success2 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);
                    
                    $source = fopen($image3, 'r');
                    $destination = fopen($file3, 'w');
                    $success3 = stream_copy_to_stream($source, $destination);
                    fclose($source);
                    fclose($destination);
                    
                    if ($success1 && $success2) {
                        // upload success
                        $QTimeImageLocation = new Application_Model_TimeImageLocation();
                        $data_insert_image = array(
                            'image_1' => $name1,
                            'image_2' => $name2,
                            'image_2' => $name3,
                        );

                        $where_img = array();
                        $where_img[] = $QTimeImageLocation->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
                        $where_img[] = $QTimeImageLocation->getAdapter()->quoteInto('check_in_day = ?', $date);
                        $img_location = $QTimeImageLocation->fetchAll($where_img);
                        $id_img_location = $img_location[0]['id'];

                        if (!empty($img_location)) {
                            @unlink(APPLICATION_PATH . "/../public/photo/pg-check-in/" . $img_location[0]['image_1']);
                            @unlink(APPLICATION_PATH . "/../public/photo/pg-check-in/" . $img_location[0]['image_2']);
                        }

                        $where_update = $QTimeImageLocation->getAdapter()->quoteInto('id = ?', $id_img_location);
                        $QTimeImageLocation->update($data_insert_image, $where_update);

                        $where_gps = array();
                        $where_gps[] = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
                        $where_gps[] = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date);

                        $QTimeGps->update($data_insert_image, $where_gps);
                    }
                } else {
                }
            }
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            if ($success1) {
                $s3_lib->uploadS3($file1, "photo/pg-check-in/", $name1);
            }
            if ($success2) {
                $s3_lib->uploadS3($file2, "photo/pg-check-in/", $name2);
            }
            if ($success3) {
                $s3_lib->uploadS3($file3, "photo/pg-check-in/", $name3);
            }
        }

        $reponse = array(
            'status' => 1,
            'message' => 'Thành công'
        );

        $this->view->mess = $reponse;
        //commit
        $db->commit();
    } catch (Exception $e) {
        $reponse['status'] = -1;
        $reponse['message'] = "thất bại".$e->getMessage();
        $this->view->mess = $reponse;
        $db->rollback();
    }
}

function png2jpg($originalFile, $outputFile, $quality) {
    $image = imagecreatefrompng($originalFile);
    imagejpeg($image, $outputFile, $quality);
    imagedestroy($image);
    return $outputFile;
}

// created compressed JPEG file from source file
function compressImage($source_image, $compress_image) {
    $image_info = getimagesize($source_image);
    if ($image_info['mime'] == 'image/jpeg') {
        $source_image = imagecreatefromjpeg($source_image);
        imagejpeg($source_image, $compress_image, 10);
    } elseif ($image_info['mime'] == 'image/gif') {
        $source_image = imagecreatefromgif($source_image);
        imagegif($source_image, $compress_image, 10);
    } elseif ($image_info['mime'] == 'image/png') {
        $source_image = imagecreatefrompng($source_image);
        imagepng($source_image, $compress_image, 10);
    }
    return $compress_image;
}

?>