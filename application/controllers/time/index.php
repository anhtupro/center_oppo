<?php
$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$page            = $this->getRequest()->getParam('page', 1);
$name            = $this->getRequest()->getParam('name');
$code            = $this->getRequest()->getParam('code');
$department      = $this->getRequest()->getParam('department');
$from_date       = $this->getRequest()->getParam('from_date', date('01/m/Y'));
$to_date         = $this->getRequest()->getParam('to_date', date('t/m/Y'));
$team            = $this->getRequest()->getParam('team');
$title           = $this->getRequest()->getParam('title');
$area_id         = $this->getRequest()->getParam('area_id');
$regional_market = $this->getRequest()->getParam('regional_market');
$district        = $this->getRequest()->getParam('district');
$sales_team      = $this->getRequest()->getParam('sales_team');
$team            = $this->getRequest()->getParam('team');
$store           = $this->getRequest()->getParam('store');
$imei            = $this->getRequest()->getParam('imei');
$email           = $this->getRequest()->getParam('email');
$off             = $this->getRequest()->getParam('off');
$sort            = $this->getRequest()->getParam('sort', 'p.id');
$desc            = $this->getRequest()->getParam('desc', 1);

$this->view->desc        = $desc;
$this->view->current_col = $sort;
$limit                   = LIMITATION;
$total                   = 0;

$params = array_filter(array(
    'name'            => $name,
    'department'      => $department,
    'from_date'       => $from_date,
    'team'            => $team,
    'title'           => $title,
    'to_date'         => $to_date,
    'team'            => $team,
    'area_id'         => $area_id,
    'regional_market' => $regional_market,
    'district'        => $district,
    'sales_team'      => $sales_team,
    'store'           => $store,
    'imei'            => $imei,
    'email'           => $email,
    'off'             => $off,
    'code'            => $code,
	// 'time_image_location' => 1,
    ));

$params['sort'] = $sort;
$params['desc'] = $desc;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id     = $userStorage->id;
$group_id    = $userStorage->group_id;

$QShift = new Application_Model_Shift();

if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
    ADMINISTRATOR_ID,
    HR_ID,
    HR_EXT_ID,
    BOARD_ID)))
{
}

elseif ($group_id == PGPB_ID){
    $params['staff_id'] = $user_id;
}elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID){
    $params['asm'] = $user_id;
}elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID, MODULE_SALE))){
    $params['other'] = $user_id;
}elseif (in_array($group_id,array(TRAINING_LEADER_ID))){
    $params['trainer_leader'] = $user_id;
}elseif($group_id == LEADER_ID){
    $params['leader'] = $user_id;
}
//set quyen cho hong nhung trainer
elseif ($user_id == 7278 || $user_id == 240){
    $params['other'] = $user_id;
}
//chi van ha noi
elseif ($user_id == 95){
    $params['other'] = $user_id;
}
elseif ($group_id == 22)
    $params['other'] = $user_id;
elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915)
    $params['asm'] = $user_id;
else{
    $params['sale'] = $user_id;
}


$QTiming = new Application_Model_Time();
$QStore  = new Application_Model_Store();

// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($params);
//     exit;
// }

$QTimes = $QTiming->fetchPagination($page, $limit, $total, $params);

$month = date('m');

$params_array = array(
    'user_id' => $user_id,
    'month' => $month,
);

$day = $QTiming->getDayDashboard($params_array);

unset($params['sale']);
unset($params['asm']);
unset($params['staff_id']);

$this->view->timings = $QTimes;
$this->view->day     = $day;
$this->view->params  = $params;
$this->view->limit   = $limit;
$this->view->total   = $total;
$this->view->url     = HOST . 'time' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset  = $limit * ($page - 1);

$QArea = new Application_Model_Area();
$this->view->areas = $QArea->fetchAll(null, 'name');

$QRegionalMarket = new Application_Model_RegionalMarket();
$this->view->regional_market_all = $QRegionalMarket->get_cache();

$QTeam = new Application_Model_Team();
$this->view->team = $QTeam->get_cache_team();
if ($area_id) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

if ($regional_market)
{
    //get district
    $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $regional_market);
    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}

if ($district)
{
    //get store
    $where = array();
    $where[] = $QStore->getAdapter()->quoteInto('district = ?', $district);
    $where[] = $QStore->getAdapter()->quoteInto('(del IS NULL OR del = 0)', 1);

    $this->view->stores = $QStore->fetchAll($where, 'name');
}

$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages       = $messages;
$this->view->messages_error = $messages_error;
$this->view->shift          = $QShift->get_cache();

//get product count
$staff_ids = array();
if ($this->getRequest()->isXmlHttpRequest()){
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setRender('partials/index');
}

$QTeam = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$this->view->userStorage = $userStorage;

$not_approve_self = array(LEADER_ID,SALES_ID, PB_SALES_ID, PGPB_ID);
$this->view->not_approve_self = $not_approve_self;