<?php
set_time_limit(0);
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QTime       = new Application_Model_Time();
$QStaff      = new Application_Model_Staff();
$staffRowSet = $QStaff->find($userStorage->id);
$staff       = $staffRowSet->current();
$yesterday   = $this->getRequest()->getParam('yesterday');
$off         = $this->getRequest()->getParam('off', 0);
$shift       = $this->getRequest()->getParam('shift');
$id          = $this->getRequest()->getParam('id');
$work        = $this->getRequest()->getParam('work');
$locale      = $this->getRequest()->getParam('locale');
$number      = $this->getRequest()->getParam('number');
$note        = $this->getRequest()->getParam('note');

if ($this->getRequest()->getMethod() != 'POST') exit;



if (!$id)
{
    echo '<script> alert("id is not valid"); </script>';
    exit;
}

$staffRowSet = $QStaff->find($id);
$staff       = $staffRowSet->current();
if (!$staff['regional_market']){
    echo '<script> alert("Not set Regional ! Please contact HR Group"); </script>';
    exit;
}

$QTimeStaffExpired = new Application_Model_TimeStaffExpired();
$where             = array();
$where[]           = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ?' , $userStorage->id );
$where[]           = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null' , null);
$result            = $QTimeStaffExpired->fetchRow($where);

if($result) {
    echo '<script> alert("You have banned ! Please contact HR Group"); </script>';
    exit;
}

$date = date('Y-m-d H:i:s');
if (isset($yesterday) and $yesterday){
    $date = date("Y-m-d H:i:s", strtotime($date . "- 1 days"));
}

// kiem tra ngay cham cong
if ($date)
{
    $where    = array();
    $datetime = new DateTime($date);
    $day      = $datetime->format('d');
    $month    = $datetime->format('m');
    $year     = $datetime->format('Y');
    $where[]  = $QTime->getAdapter()->quoteInto('staff_id = ?', $id);
    $where[]  = $QTime->getAdapter()->quoteInto('DAY(created_at) = ?', $day);
    $where[]  = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $month);
    $where[]  = $QTime->getAdapter()->quoteInto('YEAR(created_at) = ?' , $year);
    $result   = $QTime->fetchRow($where);
    if (isset($result) and $result){
        echo '<script> alert("You checked in this days! See you tomorrow."); </script>';
        echo '<script>parent.location.href="/time"</script>';
        exit;
    }
}

$data = array(
    'staff_id'        => $id,
    'created_at'      => $date,
    'regional_market' => $staff['regional_market'],
);

if (isset($yesterday) and $yesterday) {
    $data['yesterday'] = 1;
} elseif (isset($off) && $off) {
    $data['off'] = 1;
}

if (isset($shift) && $shift && isset($staff['title']) && My_Staff::isPgTitle($staff['title']))
{
    $data['shift'] = $shift;
}

if (isset($staff) and $staff['team'] == TRAINING_TEAM){
    $data['work']   = $work;
    $data['locale'] = $locale;
    $data['number'] = $number;
    $data['note']   = trim($note);
}

$QTime->insert($data);

if ($id)
{
    //update log
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $userStorage->id;
    $QLog               = new Application_Model_Log();
    $ip                 = $this->getRequest()->getServer('REMOTE_ADDR');
    $info               = "TIME - Check in";
    $QLog->insert(array(
        'info'       => $info,
        'user_id'    => $userStorage->id,
        'ip_address' => $ip,
        'time'       => date('Y-m-d H:i:s'),
        ));
}

$back_url = $this->getRequest()->getParam('back_url');
echo '<script>parent.location.href="/time"</script>';
exit;
