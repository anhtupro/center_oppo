<?php 
set_time_limit(0);
error_reporting(1);
define('SIGN_SUNDAY_FULLDAY_CHECKIN' , 'C');
define('MEKONG_SALES_MANAGER', 186);
define('CENTRAL_HIGHLAND_SALE_MANAGER', 299);
define('RSM', 308);

ini_set('display_error', -1);
$filename = 'Time By Month - ' . date('d/m/Y H:i:s');
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=' . $filename . '.csv');
echo "\xEF\xBB\xBF"; // UTF-8 BOM
$output = fopen('php://output', 'w');
$QDateSpecial = new Application_Model_DateSpecial();
$QDateNorm = new Application_Model_DateNorm();
$QTeam = new Application_Model_Team();
$teams_data = $QTeam->get_cache();

$title_khong_dinh_muc = array(MEKONG_SALES_MANAGER, 
                                            CENTRAL_HIGHLAND_SALE_MANAGER,
                                            RSM,
                                            SALE_SALE_ASM,
                                            SALE_SALE_ASM_STANDBY,
                                            LEADER_TITLE,
                                            SALES_ACCESSORIES_LEADER_TITLE);
// danh sach title ko dinh muc khong chua asm
$title_khong_dinh_muc_ngoai_asm = array(MEKONG_SALES_MANAGER, 
                                            CENTRAL_HIGHLAND_SALE_MANAGER,
                                            RSM,
                                            LEADER_TITLE,
                                            SALES_ACCESSORIES_LEADER_TITLE);

if (isset($params['from']) && $params['from'] && isset($params['to']) && $params['to'])
{
    $from = explode('/', $params['from']);
    $from = $from[2] . '-' . $from[1] . '-' . $from[0] . ' 00:00:00';
    $to = explode('/', $params['to']);
    $to = $to[2] . '-' . $to[1] . '-' . $to[0] . ' 23:59:59';
} elseif (isset($params['from']) && $params['from'])
{
    $from = explode('/', $params['from']);
    $from = $from[2] . '-' . $from[1] . '-' . $from[0] . ' 00:00:00';
    $to = date('Y-m-d 23:59:59');
} elseif (isset($params['to']) && $params['to'])
{
    $to = explode('/', $params['to']);
    $to = $to[2] . '-' . $to[1] . '-' . $to[0] . ' 23:59:59';
    $from = date_sub(date_create(), new DateInterval('P30D'))->format('Y-m-d 00:00:00');
} else
{
    $from = date_sub(date_create(), new DateInterval('P30D'))->format('Y-m-d 00:00:00');
    $to = date('Y-m-d 23:59:59');
}



$heads = array(
    'CMND',
    'Area',
    'Company',
    'Team',
    'Title',
    'Code',
    'Name',
    'Join Date',
    'Off Date',
    );
for ($i = 1; $i <= 31; $i++)
{
    $heads[] = $i;
}


$heads[] = 'Tổng ngày công';
$heads[] = 'Tổng ngày phép';
$heads[] = 'Thứ 7';
$heads[] = 'Chủ Nhật';
$heads[] = "Training";
$heads[] = "Ca gãy";
$heads[] = 'Ngày công đặc biệt x2';
$heads[] = 'Ngày công đặc biệt x3';
$heads[] = 'Ngày công trước transfer';
$heads[] = 'Ngày công sau transfer';
$heads[] = 'Ngày transfer';
$heads[] = 'Noted';


fputcsv($output, $heads);
$db = Zend_Registry::get('db');
$sql = "SELECT s.id, s.code, t.`created_at`, t.`off` , t.`off_type` ,  t.`shift` FROM time t
            INNER JOIN staff s
            ON t.staff_id=s.id

            AND t.approved_at IS NOT NULL
            AND t.approved_at <> ''
            AND t.approved_at <> 0

             WHERE  t.`created_at` >= '$from'
                    AND t.`created_at` <= '$to'
                    AND t.`shift` <> 4
                    AND s.title NOT IN (" . implode(",", $title_khong_dinh_muc) .  ")
        ORDER BY s.id";

$timings = $db->query($sql);
$timing_staffs = array();
$date_staffs = array();
$date_off = array();

if (isset($params['team']) and $params['team'])
{
    $teams = join(',', $params['team']);
    if (substr($teams, 0, 1) == ',')
        $teams = substr($teams, 1);
    foreach ($teams as $k => $v)
    {
        if ($v == SALES_TEAM)
            $asm_id = 1;
    }
}
if (isset($params['area_id']) and $params['area_id'])
{
    $ads = join(',', $params['area_id']);
    if (substr($ads, 0, 1) == ',')
        $ads = substr($ads, 1);
}

$db = Zend_Registry::get('db');
$select = $db->select();
$select->from(array('t' => 'time'), array('staff_id'));
$select->join(array('s' => 'staff'), 't.staff_id = s.id', array(
    't.created_at',
    's.id',
    's.code',
    's.title',
    's.firstname',
    's.lastname',
    's.joined_at',
    's.off_date',
    's.ID_number',
    's.company_id',
    'title_id' => 'IFNULL(v.title,s.title)'
));
$select->join(array('tm' => 'team'), 's.team = tm.id', array('team' => 'tm.id',
        'team_name' => 'tm.name'));
$select->join(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
$select->joinLeft(array('v' => 'v_staff_transfer') , "s.id = v.staff_id and v.`from_date` >= '". $from . "' and v.`from_date` <= '". $to ."'" , array('transfer' => 'v.staff_id'));
$select->join(array('a' => 'area'), 'a.id = r.area_id', array('name' => 'a.name'));
$select->where('t.created_at >= ?', $from);
$select->where('t.created_at <= ?', $to);
$select->where("s.title NOT IN (" . implode(",", $title_khong_dinh_muc) .  ")");
$select->group('id');
$select->order('s.id');


if (isset($teams) and $teams)
{
    $select->where('s.team in (?)', $teams);
}

if (isset($ads) and $ads)
{
    $select->where('a.id in (?)', $ads);
}



$staffs = $db->fetchAll($select);

$staffs_all = array();
foreach ($staffs as $key => $staff)
{
    $staffs_all[$staff['id']] = array(
        "company"   => $staff['company_id'] == 1 ? 'OPPO' : 'SMART_MOBILE',
        "indentity" => $staff['ID_number'],
        "code"      => $staff['code'],
        "name"      => $staff['firstname'] . " " . $staff['lastname'],
        "area"      => $staff['name'],
        "team"      => $staff['team'],
        "title"     => $teams_data[$staff['title']],
        "team_name" => $staff['team_name'],
        "joined_at" => $staff['joined_at'],
        "off_date"  => $staff['off_date'],
        "id"        => $staff['id'],
        'transfer'  => $staff['transfer'],
        'title_id'  => $staff['title_id']
        );
}

// l?y ng? ngh?

$saturdays = array();
$sundays = array();
$date_specials_double = $date_specials_triple = array();
$month = '';


// chưa biết gọi nó là cai nồi j :D
$arr_title_none_office = array(STORE_LEADER, PGPB_TITLE, SALES_TITLE, LEADER_TITLE, SALES_ACCESSORIES_TITLE, SALES_ACCESSORIES_LEADER_TITLE, PGPB_2_TITLE, SALES_TRAINEE_TITLE, DELIVERY_MAN_TITLE,PB_SALES_TITLE);
$arr_team = array(DELIVERY_TEAM, TRAINING_TEAM);


// lấy danh sách ngày training
$sql_training = "select tct.date as `date`, 
                        tcd.staff_id as `staff_id`
                    from `trainer_course_timing` tct
                    join `trainer_course_detail` tcd
                        on tcd.id = tct.course_detail_id
                    where (tct.date between '" . $from . "' and '" . $to . "')
                        and tcd.staff_id is not null";
    

$data_training = $db->fetchAll($sql_training);

$date_training_array = array();

foreach ($data_training as $k => $v) 
{
    $date_training_array[$v['staff_id']][intval(date('d', strtotime($v['date'])))] = 1;
}


if ($timings)
{
    foreach ($timings as $key => $timing)
    {
        if (!isset($timing_staffs[$timing['id']]))
        {
            $timing_staffs[$timing['id']] = array();
        }

        $d     = date('d', strtotime($timing['created_at']));
        $month = date('m', strtotime($timing['created_at']));
        $id    = $timing['id'];

        $date_staffs[$id][$d] = $timing['created_at'];
        if (isset($timing['off']) and $timing['off']){
            $date_off[$id][$d] = $timing['off_type'] ? $timing['off_type'] : 1;
        }

        $timing_staffs[$id][$d] = $timing['shift'];
    }


    //load ngay cong dac biet trong thang

    $date_special = $QDateSpecial->get_date($month);
    $date_special_array = array();

    foreach ($date_special as $k => $v)
    {
        $date_special_array[$k]['date'] = $v['date'];
        $date_special_array[$k]['team'] = $v['team'];
        $date_special_array[$k]['type'] = $v['type'];
    }

    //ngay cong dinh muc trong thang
    $date_norm = $QDateNorm->get_cache();
    $date_norm_month = $date_norm[intval(preg_replace("/([0-9]+)\./", "\\1", $month))];
    foreach ($staffs_all as $id => $staff)
    {
        $off_month = false; // ki? tra th?g hi?n ngh? c?tr?ng th?g l?y d? li?u
        if ($staff['off_date'] == null or $staff['off_date'] == 0)
        {
            $off_day = 0;
        } else
        {
            $off_day = date('d', strtotime($staff['off_date']));
            $tmp = explode('/', $params['from']);
             if (isset($staff['off_date'])
                            and ( strtotime($staff['off_date']) >= strtotime($from) and  strtotime($staff['off_date']) <= strtotime($to) ))
            {
                $off_month = true;
            }
     }
        $off_day = intval($off_day);
        $row = array();
        $row[0] = $staff['indentity'];
        $row[1] = $staff['area'];
        $row[2] = $staff['company'];
        $row[3] = $staff['team_name'];
        $row[4] = $staff['title'];
        $row[5] = $staff['code'];
        $row[6] = $staff['name'];
        $row[7] = $staff['joined_at'];
        $row[8] = $staff['off_date'];

        $d = count($row);
        $ca_gay = $num = $off = $training = $date_special_time_double = $date_special_time_triple = $saturday = $sunday = 0;
        for ($i = 1; $i <= 31; $i++)
        {
            if ($i < 10)
            {
                $a = "0" . $i;
            } else
            {
                $a = $i . "";
            }

            $date = date('Y-m-d', strtotime($date_staffs[$id][$a]));
            $week = date('l', strtotime($date));
            $sign = My_Timing::SIGN_TIME_CHECKIN_WEEKDAY_FULLDAY;

            // có lẽ xét trường hợp khối văn phòng :D
            // chức vụ ko đứng đường và team ko phải training và delivery
            if(!in_array($staff['title_id'] , $arr_title_none_office) and !in_array($staff['team'], $arr_team)){

                if ($week == 'Saturday')
                {
                    if ($timing_staffs[$id][$a] == 1)
                    {
                        $saturday = $saturday + 0.5;

                    } else
                    {
                        $saturday++;
                    }

                    $saturdays[$id] = $saturday;
                } elseif ($week == 'Sunday')
                {
                    if ($timing_staffs[$id][$a] == 1)
                    {
                        $sunday = $sunday + 0.5;
                        $sign = My_Timing::SIGN_TIME_CHECKIN_SUNDAY_HALFDAY;
                    } else
                    {
                        $sunday++;
                        $sign = My_Timing::SIGN_TIME_CHECKIN_SUNDAY_FULLDAY;
                    }
                    $sundays[$id] = $sunday;
                }

                foreach ($date_special_array as $k => $v)
                {
                    if ($staff['team'] == $v['team'] and $v['type'] and date('d', strtotime($date_staffs[$id][$a])) ==
                        $v['date'])
                    {
                        if ($v['type'] == 2)
                        {
                            if ($timing_staffs[$id][$a] == 1)
                            {
                                $date_special_time_double = $date_special_time_double + 0.5;
                            } else
                            {
                                $date_special_time_double++;
                            }

                            $date_specials_double[$id] = $date_special_time_double;
                        } elseif ($v['type'] == 3)
                        {
                            if ($timing_staffs[$id][$a] == 1)
                            {
                                $date_special_time_triple = $date_special_time_triple + 0.5;
                            } else
                            {
                                $date_special_time_triple++;
                            }
                            $date_specials_triple[$id] = $date_special_time_triple;
                        }

                        $sign = My_Timing::SIGN_TIME_CHECKIN_SPECIAL_DAY;
                    }
                }
            }

            if (isset($timing_staffs[$id][$a]) and $off_day > 0 and $off_month == true)
            {
                if (intval($i) < $off_day)
                {
                    if ($date_off[$id][$a])
                    {
                        if(in_array($date_off[$id][$a] , array(CHILDBEARING , CHILDBEARING_KL)))
                            $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_CHILDBEARING;
                        else
                            $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_OFF_DAY;
                        // $off++;
                    }
                    else{
                    if(isset($date_training_array[$id][$i]))
                    {
                        $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_TRAINING;
                        $training++;
                    }
                    elseif (isset($timing_staffs[$id][$a]) and $timing_staffs[$id][$a] == 0)
                    {
                        $row[$d + $i - 1] = $sign;
                        $num++;
                    }
                    // else if (isset($timing_staffs[$id][$a]) and $timing_staffs[$id][$a] == 7)
                    // {
                    //     $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_TRAINING;
                    //     $training++;
                    // }
                    else if (isset($timing_staffs[$id][$a]) and $timing_staffs[$id][$a] == 2 and in_array($staff['title_id'],array(PGPB_2_TITLE,PGPB_TITLE)))
                    {
                        //chir có pg chấm
                        $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_GAY;
                        $num++;
                        $ca_gay++;
                    }
                     // cham cong nua ngay
                     // array('PGPB' , 'SALE' ,'SALE LEADER' , 'SALE TRAINEE' ,'SENIOR PROMOTER' , 'DELIVERY_MAN_TITLE');
                    else if ($timing_staffs[$id][$a] == 1 and !in_array($staff['title_id'] , $arr_title_none_office ))
                    {
                        $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_WEEKDAY_HALFDAY;
                        $num += 0.5;
                    }

                     else
                    {
                        if ($date_off[$id][$a])
                        {
                            if(in_array($date_off[$id][$a] , array(CHILDBEARING , CHILDBEARING_KL)))
                                $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_CHILDBEARING;
                            else
                                $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_OFF_DAY;
                           // $off++;
                        }
                        else{
                            $row[$d + $i - 1] = $sign;
                        }
                        $num++;
                    }}
                }
            } else
            {
                if (isset($timing_staffs[$id][$a]) and !$date_off[$id][$a])
                {
                    if(isset($date_training_array[$id][$i]))
                    {
                        $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_TRAINING;
                        $training++;
                    }
                    elseif (isset($timing_staffs[$id][$a]) and $timing_staffs[$id][$a] == 0)
                    {
                        $row[$d + $i - 1] = $sign;
                        $num++;
                    }
                    // else if (isset($timing_staffs[$id][$a]) and $timing_staffs[$id][$a] == 7)
                    // {
                    //     $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_TRAINING;
                    //     $training++;
                    // }
                    else if (isset($timing_staffs[$id][$a]) and $timing_staffs[$id][$a] == 2 and in_array($staff['title_id'] ,array(PGPB_2_TITLE,PGPB_TITLE)))
                    {
                        $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_GAY;
                        $num++;
                        $ca_gay++;
                    }
                    else if ($timing_staffs[$id][$a] == 1 and $sign == My_Timing::SIGN_TIME_CHECKIN_WEEKDAY_FULLDAY and !in_array($staff['title_id'] ,$arr_title_none_office))
                    {
                        $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_WEEKDAY_HALFDAY;
                        $num += 0.5;
                    }
                   
                     else
                    {
                        $row[$d + $i - 1] = $sign;
                        $num++;
                    }
                  
                }
                if ($date_off[$id][$a] )
                {
                    if ($timing_staffs[$id][$a] == 1 and !in_array($staff['title_id'] , $arr_title_none_office))
                    {
                       $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_WEEKDAY_HALFDAY; $num += 0.5;$off += 0.5;
                    }
                    else
                    {
                        if(in_array($date_off[$id][$a] , array(CHILDBEARING , CHILDBEARING_KL)))
                            $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_CHILDBEARING;
                        else
                            $row[$d + $i - 1] = My_Timing::SIGN_TIME_CHECKIN_OFF_DAY;
                        $off++;
                    }
                }

            }

        }

        for ($i = 1; $i <= 31; $i++)
        {
            if (!isset($row[$d + $i - 1]))
            {
                $row[$d + $i - 1] = '-';
            }
        }

        
        if(isset($staff['transfer']) and $staff['transfer'])
            $transfer = My_Timing::transfer($staff['id'] , $from , $to);
        else
            $transfer = null;

        $total_timing_check_in = $num - $sundays[$id] ? $sundays[$id] : 0 ;

        $row[] = $num ? $num -  ($sundays[$id] ? $sundays[$id] : 0) : 0;
        $row[] = $off ? $off : '';
        $row[] = $saturdays[$id] ? $saturdays[$id] : '';
        $row[] = $sundays[$id] ? $sundays[$id] : '';
        $row[] = $training ? $training : '';
        $row[] = $ca_gay ? $ca_gay : '';
        $row[] = $date_specials_double[$id] ? $date_specials_double[$id] : '';
        $row[] = $date_specials_triple[$id] ?  $date_specials_triple[$id] : '';
        $row[] = isset($transfer['before']) ? $transfer['before'] : '';
        $row[] = isset($transfer['after'])  ? $transfer['after'] : '';
        $row[] = isset($transfer['transfer_date']) ? $transfer['transfer_date'] : '';
        $row[] = isset($transfer['noted'])  ? $transfer['noted'] : '';

        ksort($row);
        fputcsv($output, $row);
    }

    $select = $db->select()
        ->from('v_training_course_lock_data')
        ->where('`v_training_course_lock_data`.from >= ?' , $from)
        ->where('`v_training_course_lock_data`.from <= ?' , $to);

    $results = $select->query()->fetchAll();
    $timing_staffs = array();
    $num = 0;

    if (isset($results) and $results)
    {

        $training_staff = array();
        foreach ($results as $key => $training)
        {
            if(!$training_staff[$training['cmnd']])
                $training_staff[$training['cmnd']] = array(
                    "indentity" => $training['cmnd'],
                    "name"      => $training['name'],
                    "area"      => $training['area_name'],
                    "area_id"   => $training['area_id'],
                    "title"     => 'PG-PB',
                    "cmnd"      => $training['cmnd'],
                    "code"      => $training['code']
                );
        }


        foreach ($results as $key => $timing)
        {
            if (!isset($timing_staffs[$timing['cmnd']]))
            {
                $timing_staffs[$timing['cmnd']] = array();
            }

            $d  = date('d', strtotime($timing['from']));
            $id = $timing['cmnd'];
            $timing_staffs[$id][$d] = 1;
        }

        $array_ddtm = array(
            24, 25, 26, 1, 34, 10, 51, 32, 33, 7, 43, 13, 40, 41, 4
        );

        foreach ($training_staff as $id => $training)
        {
            $row = array();
            $row[] = $training['indentity'];
            $row[] = $training['area'];
            if(in_array($training['area_id'], $array_ddtm))
            {
                $row[] = 'SMART_MOBILE';
            }
            else
            {
                $row[] = 'OPPO';
            }
            $row[] = $training['title'];
            $row[] = '';
            $row[] = $training['code'];
            $row[] = $training['name'];
            $row[] = '';
            $row[] = '';
            // $row[] = '';

            $d = count($row);

            $num =  0;

            for ($i = 1; $i <= 31; $i++) {
                if ($i < 10) {
                    $a = "0" . $i;
                } else {
                    $a = $i;
                }

                if (isset($timing_staffs[$training['cmnd']][$a])) {
                    $row[$d + $i - 1] = 'T';
                    $num++;

                }
                else{
                    $row[$d + $i - 1] = '-';
                }
            }

            $row[] = $num;
            ksort($row);
            fputcsv($output, $row);
        }
    }


    if(isset($params['asm']) and $params['asm'])
    {
        $where = " WHERE s.group_id IN (5, 16) OR s.title IN (" . implode(",", $title_khong_dinh_muc_ngoai_asm) . ") ";
    }
    else
    {
        $where = " WHERE s.title IN (" . implode(",", $title_khong_dinh_muc_ngoai_asm) . ") ";
    }
        //export ASM timing
        $sql = "SELECT
            s.id,
            s.`code`,
            s.firstname,
            s.lastname,
            a.`name`,
            tm.`name` as team,
            s.`joined_at`,
            s.`off_date`,
            s.`title`,
            s.`ID_number`,
            s.`company_id`
            FROM
                staff s
            INNER JOIN team tm ON s.team = tm.id
            INNER JOIN regional_market r ON r.id=s.regional_market
            INNER JOIN area a ON a.id=r.area_id
            " . $where . "
              AND  (s.off_date IS NULL OR  s.off_date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m-01') )
            GROUP BY s.`id`
        ORDER BY s.id";

        $staffs = $db->query($sql, array(ASM_ID, ASMSTANDBY_ID));
        $asm_all = array();

        foreach ($staffs as $key => $asm)
        {
            $asm_all[$asm['id']] = array(
                "code"      => $asm['code'],
                "name"      => $asm['firstname'] . " " . $asm['lastname'],
                "area"      => $asm['name'],
                "team"      => $teams_data[$asm['team']],
                "title"     => $teams_data[$asm['title']],
                "joined_at" => $asm['joined_at'],
                "off_date"  => $asm['off_date'],
                "company"   => $asm['company_id'] == 1 ? 'OPPO' : 'SMART_MOBILE',
                "indentity" => $asm['ID_number'],

                );
        }


        foreach ($asm_all as $id => $asm)
        {
            $row = array();
            $row[0] = $asm['indentity'];
            $row[1] = $asm['area'];
            $row[2] = $asm['team_name'];
            $row[3] = $asm['company'];
            $row[4] = $asm['title'];
            $row[5] = $asm['code'];
            $row[6] = $asm['name'];
            $row[7] = $asm['joined_at'];
            $row[8] = $asm['off_date'];
            $datetocheck = "2012-03-01";
            $lastday = date('t',strtotime($datetocheck));
            for ($i = 9; $i <= 39; $i++)
            {
                $date = date('Y') . '/' . $month . '/' . intval($i - 8);
                $week = date('l', strtotime($date));
                if(empty($asm['off_date']) || (intval($i - 8) < intval(date('d', strtotime($asm['off_date']))) ))
                {
                    if ($week == 'Sunday')
                        $row[$i] = '-';
                    else
                        $row[$i] = 'X';
                }
                else
                {
                    $row[$i] = '-';
                }
            }

            $row[] = $date_norm_month;
            ksort($row);
            fputcsv($output, $row);
        }
    // } // end if asm
    exit;
}
?>