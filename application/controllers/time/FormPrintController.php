<?php
/**
 *
 */
class FormPrintController extends My_Controller_Action
{

  function init()
  {
    # code...
  }
  function createAction(){
      $flashMessenger   = $this->_helper->flashMessenger;
      $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
      $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
      $this->view->messages_error   = $flashMessenger->setNamespace('error')->getMessages();

      $QStaff           = new Application_Model_Staff();
      $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id         = $userStorage->id;
      $current_user     = $QStaff->findStaffid($staff_id);

      $QOffice          = new Application_Model_Office();
      $offices          = $QOffice->get_cache();
      $this->view->offices           = $offices;

      $QTeam    = new Application_Model_Team();
      $this->view->depart = $QTeam->fetchRow($QTeam->getAdapter()->quoteInto('id = ?', $current_user['team']))->toArray()['name'];
      $this->view->user = $current_user;
  }

  function saveAction(){
      $QFormPrint = new Application_Model_FormPrint();
      $flashMessenger = $this->_helper->flashMessenger;

      $userStorage = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id    = $userStorage->id;

      if($this->getRequest()->getMethod() == "POST"){

          $save_mode          = $this->getRequest()->getParam('save_mode');

          $mail_code          = $this->getRequest()->getParam('mail_code');
          $guarantee_delivery = $this->getRequest()->getParam('guarantee_delivery',0);
          $fast_delivery      = $this->getRequest()->getParam('fast_delivery',0);
          $time_delivery      = $this->getRequest()->getParam('time_delivery',0);
          $sender_name        = $this->getRequest()->getParam('sender_name');
          $sender_phone       = $this->getRequest()->getParam('sender_phone');
          $sender_department  = $this->getRequest()->getParam('sender_department');
          $receiver_name      = $this->getRequest()->getParam('receiver_name');
          $receiver_address   = $this->getRequest()->getParam('receiver_address');
          $receiver_phone     = $this->getRequest()->getParam('receiver_phone');
          $content            = $this->getRequest()->getParam('content');


          $data_arr = array(
            'mail_code'         => $mail_code,
            'content'           => $content,
            'sender_name'       => $sender_name,
            'sender_phone'      => $sender_phone,
            'sender_department' => $sender_department,
            'receiver_name'     => $receiver_name,
            'receiver_address'  => $receiver_address,
            'receiver_phone'    => $receiver_phone,
            'guarantee_delivery'=> $guarantee_delivery,
            'fast_delivery'     => $fast_delivery,
            'time_delivery'     => $time_delivery,
            'created_by'        => $staff_id
          );
          $data_arr['created_at'] = date('d-m-Y');

          $save_result = true;
          $direct_print = "";
          if($save_mode == "edit"){

            $id = $this->getRequest()->getParam("id");
            $current_row = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
            $diff_count = 0;
            $diff_str = '';
            $code_change = false;

            foreach ($data_arr as $key => $value) {
              if( $current_row[$key] != $value ){
                if($key == 'mail_code'){
                  $code_change = true;
                }
                $diff_count++;
                $diff_str.=$key;
              }
            }
            // 2: min change (mail_code created_at)
            if( !($code_change && $diff_count == 2) ){
              $direct_print = "&print=true";
              $data_arr['mail_code'] = "";
            }

            $id_redirect = $id;
            $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $id);
            $save_result = $QFormPrint->update($data_arr, $where);
          }else{
            $save_result = $QFormPrint->insert($data_arr);
            $id_redirect = $save_result;
          }

          if($save_result == false){
              $flashMessenger->setNamespace('error')->addMessage("Có lỗi xảy ra, Vui lòng nhập lại");
              $this->_redirect(HOST.'form-print');
          }
          $this->_redirect(HOST.'form-print/preview?id='.$id_redirect.$direct_print);

      }else{

        $flashMessenger->setNamespace('error')->addMessage("Thông tin không hợp lệ, Vui lòng nhập lại");
        $this->_redirect(HOST.'form-print');

      }

  }

  function previewAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $data_arr = $QFormPrint->fetchRow($where);

    $address_arr = $this->explodeAddress($data_arr['receiver_address']);

    $this->view->show_print = $this->getRequest()->getParam('print');
    $this->view->id_infor = $id;
    $this->view->infor = $data_arr;
    $this->view->address = $address_arr;
  }

  function startAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);
    $this->view->infor = $form_print_data;
    $this->view->infor['created_at'] = $this->format_day($form_print_data['created_at']);


    $this->view->address = $this->explodeAddress($form_print_data['receiver_address']);

    $this->_helper->layout->disableLayout();

  }

  function format_day($datetime){
    $date = array();
    $date = explode('-', explode(' ', $datetime)[0]);
    return $date[2].'/'.$date[1].'/'.$date[0];
  }

  function indexAction(){

      $flashMessenger = $this->_helper->flashMessenger;
      $db = Zend_Registry::get('db');
      $QFormPrint   = new Application_Model_FormPrint();
      $QDepartments = new Application_Model_Team();

      $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id     = $userStorage->id;

      $user_right   = ($staff_id == 2128) ? true : false;
      $this->view->user_right = $user_right;

      $departments  = $QDepartments->get_department();
      $this->view->departments = $departments;

      $export             = $this->getRequest()->getParam('export',0);
      $created_at         = $this->getRequest()->getParam('created_at');
      $created_to         = $this->getRequest()->getParam('created_to');
      $sender_name        = $this->getRequest()->getParam('sender_name');
      $receiver_name      = $this->getRequest()->getParam('receiver_name');
      $sender_department  = $this->getRequest()->getParam('sender_department');
      $content            = $this->getRequest()->getParam('content');
      $receiver_address   = $this->getRequest()->getParam('receiver_address');
      $receiver_phone     = $this->getRequest()->getParam('receiver_phone');
      $sender_phone       = $this->getRequest()->getParam('sender_phone');
      $mail_code          = $this->getRequest()->getParam('mail_code');
      $page               = $this->getRequest()->getParam('page', 1);

      $params = array(
        "sender_name"       => $sender_name,
        "receiver_name"     => $receiver_name,
        "sender_department" => $sender_department,
        "content"           => $content,
        "receiver_address"  => $receiver_address,
        "receiver_phone"    => $receiver_phone,
        "sender_phone"      => $sender_phone,
        "mail_code"         => $mail_code,
        'created_at'        => $created_at,
        'created_to'        => $created_to,
        'created_by'        => $staff_id
      );

      $page     = $this->getRequest()->getParam('page',1);
      $limit    = LIMITATION;
      $total    = 0;

      $mail_list = $QFormPrint->fetchData($page, $limit, $total, $params);

      if($export){
        $this->_exportXlsx($mail_list);
      }

      $this->view->params     = $params;
      $this->view->mail_list  = $mail_list;
      $this->view->limit      = $limit;
      $this->view->total      = $total;
      $this->view->offset     = $limit * ($page - 1);
      $this->view->url        = HOST . 'form-print' . ($params ? '?' . http_build_query($params) .'&' : '?');
      $this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
      $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();

  }

  function editAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_cache();
    $this->view->offices           = $offices;

    $QTeam    = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->departments = $recursiveDeparmentTeamTitle;

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_cache();
    $this->view->offices           = $offices;

    $this->view->infor_arr = $form_print_data;
    $this->_helper->viewRenderer->setRender('create');
  }

  function savecodeAction(){

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    if ( ! $this->getRequest()->isXmlHttpRequest() ) {
      echo '-100';
      exit;
    }

    $data_arr = $this->getRequest()->getParam('data_arr');
    $count_arr = count($data_arr);

    $QFormPrint = new Application_Model_FormPrint();

    foreach ($data_arr as $k => $item) {

      $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $item['id']);
      $result = $QFormPrint->update( array('mail_code' => $item['mail_code']), $where);
      if(!$result){
        echo json_encode(array(
          'status' => false,
          'id'     => $item['id']
        ));
      }

    }

    echo json_encode(array(
      'status' => true,
      'count'  => $count_arr
    ));
    exit();

  }

  function explodeAddress($address_str){
    $return_arr = array();
    $address_arr = explode(',' , $address_str);

    $return_arr['street'] = $address_arr[0];
    $return_arr['ward'] = $address_arr[1];
    $return_arr['district'] = $address_arr[2];
    $return_arr['city'] = $address_arr[3];
    $return_arr['nation'] = $address_arr[4];

    return $return_arr;
  }

  private function _exportXlsx($data){
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
      'ID',
      'Mã phiếu gửi',
      'Người gửi',
      'Số điện thoại gửi',
      'Phòng ban gửi',
      'Người nhận',
      'Địa chỉ nhận',
      'Số điện thoại nhận',
      'Nội dung',
      'Gửi đảm bảo',
      'Hỏa tốc',
      'Phát hẹn giờ',
      'Ngày lập đơn'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    foreach ($heads as $key) {
      $sheet->setCellValue($alpha . $index, $key);
      $alpha++;
    }
    $index = 2;

    foreach($data as $key => $value):
      $alpha = 'A';
      $guarantee_delivery = ( intval($item['guarantee_delivery']) == 1 ) ? "Có" : "Không";
      $fast_delivery = ( intval($item['fast_delivery']) == 1 ) ? "Có" : "Không";
      $time_delivery = ( intval($item['time_delivery']) == 1 ) ? "Có" : "Không";

      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['id']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['mail_code']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sender_name']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sender_phone']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sender_department']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['receiver_name']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['receiver_address']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['receiver_phone']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['content']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($guarantee_delivery), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($fast_delivery), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($time_delivery), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['created_at']), PHPExcel_Cell_DataType::TYPE_STRING);

      $index++;
    endforeach;

    $filename = 'Phieu-Gui_' . date('d-m-Y');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
  }

  function copyAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);

    $QTeam    = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->departments = $recursiveDeparmentTeamTitle;

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_cache();
    $this->view->offices           = $offices;

    $this->view->copy = true;
    $this->view->infor_arr = $form_print_data;
    $this->_helper->viewRenderer->setRender('create');
  }

  function importAction(){

    $QFormPrint = new Application_Model_FormPrint();
    $flashMessenger = $this->_helper->flashMessenger;

    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id     = $userStorage->id;

    // import IOFactory Lib
    include 'PHPExcel/IOFactory.php';

    $inputFileName = $_FILES['file']['tmp_name'];

    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch(Exception $e) {
      $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Import File thất bại !");
      $this->redirect(HOST.'form-print');
    }

    $sheet          = $objPHPExcel->getSheet(0);
    $highestRow     = $sheet->getHighestRow();
    $highestColumn  = $sheet->getHighestColumn();
    $rowData        = array();

    for ($row = 2; $row <= $highestRow; $row++){
        $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE,FALSE);
    }

    $success_rows = 0;

    foreach ($rowData as $key => $item) {

      $value = $item[0];

      $guarantee_delivery   = ( trim($value[7]) == 'x' ) ? 1 : 0;
      $fast_delivery        = ( trim($value[8]) == 'x' ) ? 1 : 0;
      $time_delivery        = ( trim($value[9]) == 'x' ) ? 1 : 0;

      $data_arr = array(
        'sender_name'       => $value[0],
        'sender_phone'      => $value[1],
        'sender_department' => $value[2],
        'receiver_name'     => $value[3],
        'receiver_address'  => $value[4],
        'receiver_phone'    => $value[5],
        'content'           => $value[6],
        'guarantee_delivery'=> $guarantee_delivery,
        'fast_delivery'     => $fast_delivery,
        'time_delivery'     => $time_delivery,
        'created_by'        => $staff_id
      );

      // echo "<pre>";
      // print_r($data_arr);
      $data_arr['created_at']   = date('Y-m-d H:i:s');
      $save_result              = $QFormPrint->insert($data_arr);

      if($save_result != null){
        $success_rows ++;
      }

    }

    // exit();

    $flashMessenger->setNamespace('success')->addMessage( "Đã Import thành công ".$success_rows." dòng dữ liệu.");
    $this->redirect(HOST.'form-print');

  }

  function deleteAction(){
    $id = $this->getRequest()->getParam('id');
    if( $this->getRequest()->getParam('action') == 'delete' ){
      $QFormPrint = new Application_Model_FormPrint();
      $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $id);
      $result = $QFormPrint->update(array('del'=>'1'),$where);
      echo $result;
      die;
    }
    echo false;
    die;
  }

  function multipleAction(){
    $QFormPrint = new Application_Model_FormPrint();
    $flashMessenger = $this->_helper->flashMessenger;

    $action = $this->getRequest()->getParam('function');
    $multi_arr = $this->getRequest()->getParam('selected_item');

    if($action == "delete")
    {
      $success_rows = 0;

      foreach ($multi_arr as $key => $id) {
        $save_result = $QFormPrint->update(array('del'=>'1'),$QFormPrint->getAdapter()->quoteInto('id = ?', $id));
        if($save_result != 0){
          $success_rows ++;
        }
      }

      $flashMessenger->setNamespace('success')->addMessage( "Đã Xóa thành công ".$success_rows." dòng dữ liệu.");
      $this->redirect(HOST.'form-print');

    } else {

        $item_arr = Array();
        foreach ($multi_arr as $key => $id) {
          $form_item = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
          $this_add = $this->explodeAddress( $form_item['receiver_address'] );
          $form_item['address'] = $this_add;
          $item_arr[] = $form_item;
        }

        $this->view->item_arr = $item_arr;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setRender('start');

    }

  }



}
