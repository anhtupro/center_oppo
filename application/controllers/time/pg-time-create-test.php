<?php
                
		// $this->_redirect('/staff-time/staff-view');
		$this->_helper->layout->setLayout('layout_bi_new');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $flashMessenger = $this->_helper->flashMessenger;

        //get staff info
        $QStaff = new Application_Model_Staff();
        $QTimeGps = new Application_Model_TimeGps();
        $QArea= new Application_Model_Area();

        $where = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $this->view->staff = $staff = $QStaff->fetchRow($where);
		$QShift            = new Application_Model_Shift();
		$shift             = $QShift->getShiftByTitle($userStorage->title);
		$this->view->title = $userStorage->title;
		$this->view->team = $userStorage->team;
		
		$this->view->shift = $shift;
		
		$db = Zend_Registry::get('db');
		$sql = "SELECT fn_check_group_local(:_title_id) as `is_local`";
		
		$_title_id = $userStorage->title;
		$stmt = $db->prepare($sql);
		$stmt->bindParam('_title_id', $_title_id, PDO::PARAM_INT);
		
		$stmt->execute();
		$data = $stmt->fetch();
		
		if($data['is_local'] == 3){
		    $this->view->is_local = 1;
		}
		
		$dataTest=$QTimeGps->getStaffForTestByTeam(611);
        $this->view->dataTest = $dataTest;

        //get Area cache
        $listArea=$QArea->fetchAll();
        $arrTemp=array();
        foreach ($listArea as $key => $value) {
        	# code...
        	if(!empty($value['name_gps'])){
        		$arrTemp[$value['id']]=$value['name_gps'];
        	}
        }
        $listArea=$arrTemp;

        $this->view->listArea = $listArea;


// 		$stmt->closeCursor();
// 		$stmt = $db = null;
		
//        $QDate = new Application_Model_DateNorm();
//        $date = $QDate->get_cache();
//        $this->view->date = $date;

//        $QTime = new Application_Model_Time();
//        $where = array();
//        $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $staff['id']);
//        $where[] = $QTime->getAdapter()->quoteInto('DATE(created_at) = CURDATE()', '');
//        $time = $QTime->fetchRow($where);
//
//        if (isset($time) and $time)
//        {
//            $this->view->time = 1;
//        }

        $this->view->user_id = $userStorage->id;
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

                    $this->_helper->viewRenderer->setRender('/pg-time-create-test');

