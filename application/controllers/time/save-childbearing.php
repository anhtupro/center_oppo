<?php 

$flashMessenger = $this->_helper->flashMessenger;
$from_date      = $this->getRequest()->getParam('from_date');
$to_date        = $this->getRequest()->getParam('to_date');
$staff_id       = $this->getRequest()->getParam('staff_id');
$reason         = $this->getRequest()->getParam('reason');
$type           = $this->getRequest()->getParam('type');
$id             = $this->getRequest()->getParam('id');
$back_url       = $this->getRequest()->getParam('back_url');
if(!$back_url){
    $back_url = '/time/list-childbearing';
}

if ($this->getRequest()->isPost())
{
    if (!$type)
    {
        $flashMessenger->setNamespace('error')->addMessage('Please select type off');
        $this->_redirect($back_url);
    }

    if (!$from_date)
    {
        $flashMessenger->setNamespace('error')->addMessage('please select "from date"');
        $this->_redirect($back_url);
    }

    if (!$to_date)
    { 
        $flashMessenger->setNamespace('error')->addMessage('Please select "to date"');
        $this->_redirect($back_url);
    }

    if (!$staff_id)
    {
        $flashMessenger->setNamespace('error')->addMessage('Please select a staff');
        $this->_redirect($back_url);
    }

    $from_date   = My_Date::normal_to_mysql($from_date);
    $to_date     = My_Date::normal_to_mysql($to_date);
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $QOffChildbearing = new Application_Model_OffChildbearing();
    $data = array(
        'from_date' => $from_date,
        'to_date'   => $to_date,
        'off_type'  => $type,
        'staff_id'  => $staff_id,
        'reason'    => trim($reason)
    );

    if($id) {
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $userStorage->id;
    }else{
        $data['created_at'] =date('Y-m-d H:i:s');
        $data['created_by'] = $userStorage->id;
    }

    $result = $QOffChildbearing->save($data,$id);
    
    if( isset($result['code']) AND $result['code'] == 0){
        $flashMessenger->setNamespace('success')->addMessage('Done');
    }else{
        $flashMessenger->setNamespace('error')->addMessage($result['message']);
    }

    $this->_redirect($back_url);
} // End if post

?>