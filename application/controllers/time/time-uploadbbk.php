<?php 
define('MAX_SIZE_UPLOAD', 3145728);
$this->_helper->layout->disableLayout();
$reponse = array();
if ($this->getRequest()->getMethod() == 'POST') {
	$reponse = array('status' => 1, 'message' => 'Thành công');
	
	if(!empty($_FILES['file_1']['name']) || !empty($_FILES['file_2']['name'])){
	    $ext_arr = array('jpg','png','JPG','PNG','jpeg','JPEG');
	    
	    $ext1 = pathinfo($_FILES['file_1']['name'], PATHINFO_EXTENSION);
	    
	    $ext2 = pathinfo($_FILES['file_2']['name'], PATHINFO_EXTENSION);
	    
	    if(($_FILES['file_1']['size'] > MAX_SIZE_UPLOAD) || ($_FILES['file_2']['size'] > MAX_SIZE_UPLOAD)){
	        $reponse['status']= -5;
	        $reponse['message'] = "Kích thước vượt quá 3Mb";
	        $this->view->mess = $reponse;
	        return ;
	    }
	    if(!in_array($ext1, $ext_arr) || !in_array($ext2, $ext_arr)){
	        $reponse = array('status' => -3, 'message' => 'Hình sai định dạng png, jpg, jpeg');
	        $this->view->mess = $reponse;
	        return;
	    }
	}
	
    	$latitude = $this->getRequest()->getParam('latitude1');
    	$longitude = $this->getRequest()->getParam('longitude1');
    	$check_out_pg = $this->getRequest()->getParam('check_out_pg');
	
		$userStorage = Zend_Auth::getInstance()->getStorage()->read();
		$QTime = new Application_Model_Time();
		$QTimeGps = new Application_Model_TimeGps();
		
		$shift = $this->getRequest()->getParam('shift1');
		$name1= "image_1_".$userStorage->id."_". time().".png";
		$name2= "image_2_".$userStorage->id."_". time().".png";
		$file1 = APPLICATION_PATH . "/../public/photo/pg-check-in/" . $name1;
		$file2 = APPLICATION_PATH . "/../public/photo/pg-check-in/" . $name2;
		
		if(empty($latitude) || empty($longitude)){
			$reponse = array('status' => -2, 'message' => 'Vui lòng bật GPS');
			$this->view->mess = $reponse;
			return;
		}
		
		$QStaff = new Application_Model_Staff();
		$staffRowSet = $QStaff->find($userStorage->id);
		$staff = $staffRowSet->current();
		$db = Zend_Registry::get('db');
		try {
		    $db->beginTransaction();
		if (!empty($staff['regional_market'])) {
        		if($staff['status']!= 1){
                        $data_staff = array(
                            'status' => 1
                        );
                        $where_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
                        $QStaff->update($data_staff,$where_staff);
                    
                 }
				$date = date('Y-m-d');
				$datetime = date('Y-m-d H:i:s');
				$where = array();
				$where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
				$where[] = $QTime->getAdapter()->quoteInto('off = 0');
				$where[] = $QTime->getAdapter()->quoteInto('DATE(created_at) = ?', $date);
				$result_date = $QTime->fetchRow($where);
				
		        if (empty($result_date)) { // cham lan dau
                
                $data_time = array(
                    'staff_id' => $userStorage->id,
                    'created_at' => $datetime,
                    'regional_market' => $staff['regional_market'],
                    'note' => 'check in by ios'
                );
                $data_time['shift'] = $shift;
                
                $id_time = $QTime->insert($data_time);
                
                // update log
                $QLog = new Application_Model_Log();
                $ip = $this->getRequest()->getServer('REMOTE_ADDR');
                $info = "TIME - Check in";
                $QLog->insert(array(
                    'info' => $info,
                    'user_id' => $userStorage->id,
                    'ip_address' => $ip,
                    'time' => $datetime
                ));
                
                if (!empty($_FILES['file_1']['name']) and !empty($_FILES['file_2']['name'])) {
                    // Upload images
                    $success1 = @move_uploaded_file($_FILES['file_1']['tmp_name'], $file1);
                    $success2 = @move_uploaded_file($_FILES['file_2']['tmp_name'], $file2);
                    
                    if ($success1 && $success2) {
                        // upload success
                        $QTimeImageLocation = new Application_Model_TimeImageLocation();
                        $data_insert_image = array(
                            'time_id' => $id_time,
                            'staff_id' => $userStorage->id,
                            'created_at' => $datetime,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                            'image_1' => $name1,
                            'image_2' => $name2
                        );
                        $QTimeImageLocation->insert($data_insert_image);
                        
                        // Insert tbl time_gps
                        $data_gps = array(
                            'staff_id' => $userStorage->id,
                            'check_in_day' => date('Y-m-d'),
                            'check_in_at' => $datetime,
                            'note' => 'check in by ios',
                            'created_at' => $datetime,
                            'regional_market' => $staff['regional_market'],
                            'shift' => $shift,
                            'image_1' => $name1,
                            'image_2' => $name2,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                            'is_half_day'     => ($shift == 9) ? 1 : 0
                        );
                        $QTimeGps->insert($data_gps);
                        
//                         $reponse = array(
//                             'status' => 1,
//                             'message' => 'Thành công'
//                         );
//                         $this->view->mess = $reponse;
                    }
                } else {
                    $QTimeImageLocation = new Application_Model_TimeImageLocation();
                    $data_insert_image = array(
                        'time_id' => $id_time,
                        'staff_id' => $userStorage->id,
                        'created_at' => $datetime,
                        'latitude' => $latitude,
                        'longitude' => $longitude
                    );
                    $QTimeImageLocation->insert($data_insert_image);
                    
                    // Insert tbl time_gps
                    $data_gps = array(
                        'staff_id' => $userStorage->id,
                        'check_in_day' => date('Y-m-d'),
                        'check_in_at' => $datetime,
                        'note' => 'check in by ios',
                        'created_at' => $datetime,
                        'regional_market' => $staff['regional_market'],
                        'shift' => $shift,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'is_half_day'     => ($shift == 9) ? 1 : 0
                    )
                    ;
                    $QTimeGps->insert($data_gps);
                    
//                     $reponse = array(
//                         'status' => 1,
//                         'message' => 'Thành công'
//                     );
//                     $this->view->mess = $reponse;
                }
            }else{
				$id_time=$result_date['id'];
				if(!empty($_FILES['file_1']['name']) and !empty($_FILES['file_2']['name'])){
				    //Upload images
				    $success1 = @move_uploaded_file($_FILES['file_1']['tmp_name'], $file1);
				    $success2 = @move_uploaded_file($_FILES['file_2']['tmp_name'], $file2);
				    
				    if($success1 && $success2){
				        // upload success
				        $QTimeImageLocation= new Application_Model_TimeImageLocation();
				        $data_insert_image=array(
				            'image_1'=>$name1,
				            'image_2'=>$name2,
				        );
				    
				        $where_img = $QTimeImageLocation->getAdapter()->quoteInto('time_id = ?', $id_time);
				        $img_location = $QTimeImageLocation->fetchAll($where_img);
				        $id_img_location = $img_location[0]['id'];
				    
				        if(!empty($img_location)){
				            @unlink(APPLICATION_PATH . "/../public/photo/pg-check-in/" .$img_location[0]['image_1']);
				            @unlink(APPLICATION_PATH . "/../public/photo/pg-check-in/" .$img_location[0]['image_2']);
				        }
				    
				        $where_update = $QTimeImageLocation->getAdapter()->quoteInto('id = ?', $id_img_location);
				        $QTimeImageLocation->update($data_insert_image,$where_update);
				        
				        $where_gps    = array();
				        $where_gps[]  = $QTimeGps->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
				        $where_gps[]  = $QTimeGps->getAdapter()->quoteInto('check_in_day = ?', $date);
				        
				        $QTimeGps->update($data_insert_image, $where_gps);
				        
// 				        $reponse = array('status' => 1, 'message' => 'Thành công');
// 				        $this->view->mess = $reponse;
				    }
				}else{
// 				    $reponse = array('status' => 1, 'message' => 'Thành công');
// 				    $this->view->mess = $reponse;
				}
			}
		}    
		
		$reponse = array(
		    'status' => 1,
		    'message' => 'Thành công'
		);
		
		$this->view->mess = $reponse;
		//commit
        $db->commit();
    } catch (Exception $e) {
        $reponse['status']=-1;
        $reponse['message']="thất bại";
        $this->view->mess = $reponse;
        $db->rollback();
    }
}


?>