<?php
$id = $this->getRequest()->getParam('id');
try
{
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QPermission = new Application_Model_TimePermission();
    $QLockTiming = new Application_Model_HrTimingLock();
    $QStore      = new Application_Model_Store();
    $QTime       = new Application_Model_Time();
    $QOff        = new Application_Model_OffDate();
    $QOffHistory = new Application_Model_OffHistory();
    $QStaff      = new Application_Model_Staff();

    if (!in_array($userStorage->group_id, array(
        ASMSTANDBY_ID,
        ASM_ID,
        SALES_ADMIN_ID,
        ACCESSORIES_ID,
        SALES_ID,
        LEADER_ID,
        SERVICE_ID)) && $userStorage->group_id != ADMINISTRATOR_ID 
        && $userStorage->group_id != HR_ID &&
         $userStorage->group_id != SUPERADMIN_ID && !$QPermission->getPermission($userStorage->id))
    {
        echo '-1';
        exit;
    }

    $where       = $QTime->getAdapter()->quoteInto('id = ?', $id);
    $item        = $QTime->fetchRow($where);

    $date = date('Y-m-d', strtotime($item['created_at']));
    $d    = intval(date('d', strtotime($date)));
    $m    = intval(date('m', strtotime($date)));
    $y    = intval(date('Y', strtotime($date)));

    $whereLocktiming   = array();
    $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('month = ?' , $m);
    $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('year = ?' , $y);
    $lockTiming        = $QLockTiming->fetchRow($whereLocktiming);

    if($lockTiming)
    {
        echo '-1';
        exit;             
    }

    $staffRowSet = $QStaff->find($item['staff_id']);
    $staff       = $staffRowSet->current();

    if(isset($staff) and $staff){
        $staff = $staff->toArray();
    }


    if(in_array($userStorage->group_id, array(
            SALES_ID,
            LEADER_ID)) and $staff['id'] == $userStorage->id)
    {
        echo '-1';
        exit;
    }

    if(in_array($userStorage->group_id, array(
            SALES_ID,
            LEADER_ID)) and !My_Staff_Title::getPg($staff['title']))
    {
        echo '-1';
        exit;
    }


    $shift = $item['shift'];

    //////////////////////////
    ////giai quyet don off////
    /////////////////////////
    if ($item['off'] == 1)
    {

        $off = $QOff->checkOff($staff['code'], $m, $y);
        // $m = date('m', strtotime($date));
        if (!isset($shift) and $shift != 0)
        {
            $off = $off - 1;
        } else {
            $off = $off - 0.5;
        }

        if ($off < -1)
        {
            //neu da tru qua ngay phep cua thang do thi k tru nua
            $off = -1;
        }

        $data = array('date' => $off, );

        $where = array();

        $where[] = $QOff->getAdapter()->quoteInto('staff_id = ? ', $staff['code']);

        $QOff->update($data, $where);
        
        ///luu log va ngay phep con lai cua nhan vien///
        $data = array(
            'staff_id' => $item['staff_id'],
            'month_id' => $m,
            'date'     => $date,
            'type'     => $item['off_type'],
            'reason'   => $item['reason'],
            'shift'    => $shift,
            'off_date' => $off,
            'time_id'  => $id,
        );

        $QOffHistory->insert($data);
    }


    $data = array(
        'status' => 1,
        'approved_at' => date('Y-m-d H:i:s'),
        'approved_by' => $userStorage->id,
        );
    $where = $QTime->getAdapter()->quoteInto('id = ?', $id);
    $QTime->update($data, $where);

    $QLog = new Application_Model_Log();
    $ip = $this->getRequest()->getServer('REMOTE_ADDR');
    $info = "TIME SYSTEM - Approve (" . $id . ")";
    //todo log
    $QLog->insert(array(
        'info' => $info,
        'user_id' => $userStorage->id,
        'ip_address' => $ip,
        'time' => date('Y-m-d H:i:s'),
        ));


    echo 99;
    exit;
}
catch (exception $e)
{
    echo - 1;
    exit;
}