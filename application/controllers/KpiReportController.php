<?php

class KpiReportController extends My_Controller_Action{

	public function init(){
		$this->view->act = $this->getRequest()->getActionName();
	}

	public function kpiSettingViewAction(){
		$pars = array_merge(
			array(
				'product_name' => null,
				'policy' => null,
				'from_date' => null,
				'to_date' => null,
				'title' => null,
				'page' => 1
			),
			$this->_request->getParams()
		);

		$pars['product_name'] = trim($pars['product_name']);
		$pars['limit'] = 25;
		$pars['offset'] = ($pars['page'] - 1) * $pars['limit'];
		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		$pars['policy'] = empty($pars['policy'])?null:$pars['policy'];
		$pars['title'] = empty($pars['title'])?null:$pars['title'];


		$gkl = new Application_Model_GoodKpiLog2;
		// Start export
		if(!empty($pars['export'])){
			unset($pars['limit'], $pars['offset']);

			$res = $gkl->GetAll($pars);

			require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();

			$heads = array(
				'A' => 'Policy',
				'B' => 'Title',
				'C' => 'Product',
				'D' => 'Color',
				'E' => 'KPI',
				'F' => 'Effect',
				'G' => 'Expire',
				'H' => 'POLICY EFFECT FROM',
			);
			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			foreach($heads as $key => $value)
				$sheet->setCellValue($key.'1', $value);
			$sheet->getStyle('A1:H1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(30);
			$sheet->getColumnDimension('B')->setWidth(10);
			$sheet->getColumnDimension('C')->setWidth(15);
			$sheet->getColumnDimension('D')->setWidth(10);
			$sheet->getColumnDimension('E')->setWidth(10);
			$sheet->getColumnDimension('F')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(15);


			foreach($res['data'] as $key => $value){
				$sheet->setCellValue('A'.($key + 2), $value['policy']);
				$sheet->setCellValue('B'.($key + 2), $value['title']);
				$sheet->setCellValue('C'.($key + 2), $value['product']);
				$sheet->setCellValue('D'.($key + 2), $value['color']);
				$sheet->setCellValue('E'.($key + 2), $value['kpi']);
				$sheet->setCellValue('F'.($key + 2), date('d/m/Y', $value['from_date']));
				$sheet->setCellValue('G'.($key + 2), ($value['to_date'] ? date('d/m/Y', $value['to_date']) : null));
				$sheet->setCellValue('H'.($key + 2), ($value['policy_from_date'] ? date('d/m/Y', $value['policy_from_date']) : null));
			}

			$filename = 'Report KPI_'.date('d-m-Y');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
			$objWriter->save('php://output');

			exit;
		}
		// End export

		$this->view->kpi = $gkl->GetAll($pars);
		$QPolicy = new Application_Model_Policy();
		$policy = $QPolicy->selectPolicy();
		$this->view->policy = $policy['data'];

		unset($pars['module'], $pars['controller'], $pars['action'], $pars['page']);
		$pars['url'] = HOST.'kpi-report/kpi-setting-view?'.http_build_query($pars).'&';
		$this->view->pars = $pars;
		// echo '<pre>';print_r($pars);die;
	}

	public function kpiByPolicyAction(){    
		$pars = array_merge(
			array(
				'from_date' => date('1/m/Y', strtotime("-1 months")),
				'to_date' => date('t/m/Y' , strtotime("-1 months")),
				'policy' => null,
				'title' => null,
				'name' => null,
				'code' => null,
				'area' => null,
				'page' => 1,
			),
			$this->_request->getParams()
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		
		$this->view->from_date = $pars['from_date'];
		$this->view->to_date = $pars['to_date'];

		// echo "<pre>";print_r($pars);die


		if($this->_request->getParam('export'))
		{

			// $QPolicy = new Application_Model_Policy();
			// $all_policy = $QPolicy->selectPolicy()['data'];
			// 
			$ik = new Application_Model_ImeiKpi2();
			$data_export =  $ik->GetByTitle(null, null, $pars);

			// echo "<pre>";print_r($data_export['data']);die;

			set_time_limit(0);
            error_reporting(0);
            ini_set('memory_limit', -1);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();


            $heads = array(
                /*'No.',*/
               	'Name',
				'Code',
				'Area',
				'Title',
				'Chính sách công ty - PG',
				// 'Chính sách Thanh Hóa cũ - PG & PB-Sales',
				// 'Chính sách Thanh Hóa (IND & KA) - PG & PB-Sales',
				// 'Chính sách TGDĐ - PG & PB-Sales',
				// 'Chính sách PG Partner (IND & KA)',
				// 'Chính sách PG Partner TGDĐ',
				'Chính sách IND - Sales',
				'Chính sách KA - Sales',
				// 'Chính sách Leader - Brandshop',
				'Chính Sách Store Leader - Brandshop',
				'Chính Sách Store CONSULTANT - Brandshop',
				'Total',
                
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            $alpha = 'A';
            $index = 1;
            foreach ($heads as $key) {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }

			$sheet->getStyle('A1:K1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(25);
			$sheet->getColumnDimension('B')->setWidth(20);
			$sheet->getColumnDimension('C')->setWidth(20);
			$sheet->getColumnDimension('D')->setWidth(30);
			$sheet->getColumnDimension('E')->setWidth(35);
			$sheet->getColumnDimension('F')->setWidth(40);
			$sheet->getColumnDimension('G')->setWidth(50);
			// $sheet->getColumnDimension('H')->setWidth(30);
			// $sheet->getColumnDimension('I')->setWidth(30);
			// $sheet->getColumnDimension('J')->setWidth(30);
			// $sheet->getColumnDimension('K')->setWidth(20);
			// $sheet->getColumnDimension('L')->setWidth(20);
			// $sheet->getColumnDimension('M')->setWidth(30);
			$sheet->getColumnDimension('N')->setWidth(30);
			$sheet->getColumnDimension('O')->setWidth(20);

			
			$index = 2;
            $stt = 0;

			foreach($data_export['data'] as $key => $value){
				$alpha = 'A';
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['staff_name']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_4'] ? $value['policy_4'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_5'] ? $value['policy_5'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_6'] ? $value['policy_6'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_7'] ? $value['policy_7'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_10'] ? $value['policy_10'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_11'] ? $value['policy_11'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_8'] ? $value['policy_8'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_9'] ? $value['policy_9'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_12'] ? $value['policy_12'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_13'] ? $value['policy_13'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_16'] ? $value['policy_16'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_4'] + $value['policy_5'] + $value['policy_6'] + $value['policy_7'] + $value['policy_8'] + $value['policy_9'] + $value['policy_10'] + $value['policy_11'] + $value['policy_12'] + $value['policy_13'] + $value['policy_16']), PHPExcel_Cell_DataType::TYPE_STRING);
             	$index++;
	        } 
			

			$filename = 'KPI By Policy - ' . date('Y-m-d H-i-s');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;

			// $array_ez = range('E', 'Z');
			// $temp = 0;
			// foreach($all_policy as $key => $value)
			// {
			// 	$heads[$array_ez[$key]] = $value['title'];
			// 	$temp = $key+1;
			// }
			// $heads[$array_ez[$temp]] = 'Total';
			// $PHPExcel->setActiveSheetIndex(0);
			// $sheet = $PHPExcel->getActiveSheet();

			// foreach($heads as $key => $value)
			// 	$sheet->setCellValue($key.'1', $value);
			
			// $sheet->getStyle('A1:'.$array_ez[$temp].'1')->applyFromArray(array('font' => array('bold' => true)));

			// $sheet->getColumnDimension('A')->setWidth(30);
			// $sheet->getColumnDimension('B')->setWidth(15);
			// $sheet->getColumnDimension('C')->setWidth(20);
			// $sheet->getColumnDimension('D')->setWidth(10);
			// foreach($all_policy as $key => $value)
			// {
			// 	$heads[$array_ez[$key]] = $value['title'];
			// 	$sheet->getColumnDimension($array_ez[$key])->setWidth(30);
			// }
			// $sheet->getColumnDimension($array_ez[$temp])->setWidth(20);

			// $ik = new Application_Model_ImeiKpi2();
			// $data_export =  $ik->GetByTitle(0, 10, $pars);;

			// foreach($data_export['data'] as $key => $value)
			// {
			// 	$sheet->setCellValue('A' . ($key+2), $value['staff_name']);
			// 	$sheet->setCellValue('B' . ($key+2), $value['code']);
			// 	$sheet->setCellValue('C' . ($key+2), $value['area_name']);
			// 	$sheet->setCellValue('D' . ($key+2), $value['title']);
			// 	$total_kpi = 0;
			// 	foreach($all_policy as $key_policy => $value_policy)
			// 	{
			// 		if($value_policy['id'] == $value['policy_id'])
			// 		{
			// 			$data_kpi = $value['kpi'];
			// 		}
			// 		else
			// 		{
			// 			$data_kpi = null;
			// 		}
			// 		$total_kpi += $data_kpi;
			// 		$sheet->setCellValue($array_ez[$key_policy] . ($key+2), $data_kpi);
			// 	}
			// 	$sheet->setCellValue($array_ez[$temp] . ($key+2), $total_kpi);
			// }

			// $filename = 'KPI By Policy - ' . date('Y-m-d H-i-s');
			// $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			// header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			// $objWriter->save('php://output');
			// exit;
		}
		
		$this->view->pars = $pars;
		$total = 0;
		$limit = 50;
		$offset = ($pars['page']-1)*$limit;
		$ik = new Application_Model_ImeiKpi2();
		// $ik->ReportByPolicy(10, 0, $pars); die;
		$data = $ik->GetByTitle($limit, $offset, $pars);
               
		// print_r($data['data']); die;
		$this->view->data = $data['data'];

		$QPolicy = new Application_Model_Policy();
		$policy = $QPolicy->selectPolicy();
		$this->view->policy = $policy['data'];

		$this->view->limit = $limit;
		$this->view->offset = $pars['page'];
		$this->view->total = $data['total'];

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));
		unset($pars['page']);
	
		$this->view->url = HOST . 'kpi-report/kpi-by-policy' . ($pars ? '?' . http_build_query($pars) . '&' : '?');

		$QArea = new Application_Model_Area();
		$all_area =  $QArea->fetchAll(null, 'name');
		$this->view->areas = $all_area;
	}

	public function groupByPolicyAction()
	{
		$pars = array_merge (
			array(
				'from_date' => date('1/m/Y', strtotime("-1 months")),
				'to_date' => date('t/m/Y' , strtotime("-1 months")),
				'policy' => null,
				'title' => null,
				'page' => 1
			),
			$this->_request->getParams()
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;

		if($this->_request->getParam('export'))
		{
			require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();
			$heads = array(
				'A' => 'Title',
				'B' => 'Policy',
				'C' => 'Sell out',
				'D' => 'Total',
			);

			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			foreach($heads as $key => $value)
				$sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:D1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(20);
			$sheet->getColumnDimension('B')->setWidth(30);
			$sheet->getColumnDimension('C')->setWidth(30);
			$sheet->getColumnDimension('D')->setWidth(30);

			$ik = new Application_Model_ImeiKpi2();
			$data_export =  $ik->GroupByPolicy(null, null, $pars);
			
			foreach($data_export['data'] as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['title']);
				$sheet->setCellValue('B' . ($key+2), $value['policy']);
				$sheet->setCellValue('C' . ($key+2), $value['sell_out']);
				$sheet->setCellValue('D' . ($key+2), $value['kpi']);
			}

			$filename = 'KPI Group By Policy - ' . date('Y-m-d H-i-s');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;
		}

		$QPolicy = new Application_Model_Policy();
		$policy = $QPolicy->selectPolicy();
		$this->view->policy = $policy['data'];

		$total = 0;
		$limit = 50;
		$offset = ($pars['page']-1)*$limit; 
		$ik = new Application_Model_ImeiKpi2();
		$data = $ik->GroupByPolicy($limit, $offset, $pars);
		$this->view->pars = $pars;
		$this->view->data = $data['data'];

		$this->view->total = $data['total'];

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));
		unset($pars['page']);

		$this->view->url = HOST . 'kpi-report//group-by-policy' . ($pars ? '?' . http_build_query($pars) . '&' : '?');
	}

	public function byStaffAction()
	{
		$pars = array_merge(
			array(
				'from_date' => date('1/m/Y', strtotime("-1 months")),
				'to_date' => date('t/m/Y' , strtotime("-1 months")),
				// 'name' => null,
				// 'code' => null,
				// 'title' => null,
				// 'area' => null,
				'page' => 1,
			),
			$this->_request->getParams()
		);
		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		
		$this->view->from_date_ajax = $pars['from_date'] . " 00:00:00";
		$this->view->to_date_ajax = $pars['to_date'] . " 23:59:59";
		
		if($this->_request->getParam('export'))
		{

			require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();
			$heads = array(
				'A' => 'Name',
				'B' => 'Code',
				'C' => 'Area',
				'D' => 'KPI PG',
				'E' => 'Sell out PG',
				'F' => 'KPI Sale',
				'G' => 'Sale out sale',
				'H' => 'KPI Store Leader',
				'I' => 'Sale out Store Leader',
				'J' => 'KPI Sale Leader Brandshop',
				'K' => 'Sale out Sale Leader Brandshop',
				'L' => 'KPI out CONSULTANT Brandshop',
				'M' => 'Sale out CONSULTANT Brandshop',
				'N' => 'Total',
			);

			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			foreach($heads as $key => $value)
				$sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:L1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(35);
			$sheet->getColumnDimension('B')->setWidth(15);
			$sheet->getColumnDimension('C')->setWidth(20);
			$sheet->getColumnDimension('D')->setWidth(15);
			$sheet->getColumnDimension('E')->setWidth(15);
			$sheet->getColumnDimension('F')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(15);
			$sheet->getColumnDimension('H')->setWidth(15);
			$sheet->getColumnDimension('I')->setWidth(15);
			$sheet->getColumnDimension('J')->setWidth(15);
			$sheet->getColumnDimension('K')->setWidth(15);
			$sheet->getColumnDimension('L')->setWidth(15);

			

			$ik = new Application_Model_ImeiKpi2();
			
			$data_export =  $ik->GetAllByStaff(null,null,$pars);
			foreach($data_export['data'] as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['staff_name']);
				$sheet->setCellValue('B' . ($key+2), $value['code']);
				$sheet->setCellValue('C' . ($key+2), $value['area']);
				$sheet->setCellValue('D' . ($key+2), $value['kpi_pg']);
				$sheet->setCellValue('E' . ($key+2), $value['sell_out_pg']);
				$sheet->setCellValue('F' . ($key+2), $value['kpi_sale']);
				$sheet->setCellValue('G' . ($key+2), $value['sell_out_sale']);
				$sheet->setCellValue('H' . ($key+2), $value['kpi_store_leader']);
				$sheet->setCellValue('I' . ($key+2), $value['sell_out_store_leader']);
				$sheet->setCellValue('J' . ($key+2), $value['kpi_sales_brs']);
				$sheet->setCellValue('K' . ($key+2), $value['sell_out_sales_brs']);
				$sheet->setCellValue('L' . ($key+2), $value['kpi_consultant']);
				$sheet->setCellValue('M' . ($key+2), $value['sell_out_consultant']);
				$sheet->setCellValue('N' . ($key+2), $value['kpi_pg']+$value['kpi_sale']+$value['kpi_store_leader']+$value['kpi_sales_brs']+$value['kpi_consultant']);
				
			}

			$filename = 'KPI Group By Staff - ' . date('Y-m-d H-i-s');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;
		}
		
		$QArea = new Application_Model_Area();
		$all_area =  $QArea->fetchAll(null, 'name');
		$this->view->areas = $all_area;

		$total = 0;
		$limit = 50;
		$offset = ($pars['page']-1)*$limit; 
		$ik = new Application_Model_ImeiKpi2();
		$data = $ik->GetAllByStaff($limit,$offset,$pars);
		$this->view->pars = $pars;
		$this->view->data = $data;
		$this->view->limit = $limit;
		$this->view->offset = $pars['page'];
		$this->view->total = $data['total'];

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));
		unset($pars['page']);

		$this->view->url = HOST . 'kpi-report/by-staff' . ($pars ? '?' . http_build_query($pars) . '&' : '?');


		$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
	}

	public function ajaxDetailKpiByPolicyAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->getRequest()->isXmlHttpRequest())
		{
			$pars = $this->_request->getParams();
			$QImeiKpi2 = new Application_Model_ImeiKpi2();

			$data = $QImeiKpi2->getDetailKpiByPolicy($pars);
			echo json_encode($data); die;
		}
	}

	public function ajaxDetailKpiStaffByPolicyAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$table_name = $this->getRequest()->getParam('table_name');
		$staff_id = $this->getRequest()->getParam('staff_id');
		$from_date = $this->getRequest()->getParam('from_date');
		$to_date = $this->getRequest()->getParam('to_date');
		
		$pars = array(
			'table_name' => $table_name,
			'staff_id' => $staff_id,
			'from_date' => $from_date,
			'to_date' => $to_date,
		);
		
		$QImeiKpi2 = new Application_Model_ImeiKpi2();

		$data = $QImeiKpi2->getDetailKpiStaffByPolicy($pars);
		echo json_encode($data); die;
	}

	public function exportStaffAction()
	{
		$export = $this->getRequest()->getParam('export');
		$from_date = $this->getRequest()->getParam('from_date', date('1/m/Y', strtotime("-1 months")));
		$to_date = $this->getRequest()->getParam('to_date', date('t/m/Y' , strtotime("-1 months")));
		$code = $this->getRequest()->getParam('code');

		$pars = array(
			'from_date' => $from_date,
			'to_date' => $to_date,
			'code' => $code,
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : date('Y-m-1');
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : date('Y-m-d');
		$pars['from_date'] = $pars['from_date'].' 00:00:00';
		$pars['to_date'] = $pars['to_date'].' 23:59:59';

		if($export)
		{
			
			set_time_limit(0);
			$QImeiKpi2 = new Application_Model_ImeiKpi2();
			$data = $QImeiKpi2->DetailStaff($pars);

			require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();
			$heads = array(
				'A' => 'Name',
				'B' => 'Code',
				'C' => 'Area',
				'D' => 'Title',
				'E' => 'Good name',
				'F' => 'Color',
				'G' => 'Policy',
				'H' => 'Sell out',
				'I' => 'KPI',
				'J' => 'Total',
				'L' => 'Team'
			);

			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			foreach($heads as $key => $value)
				$sheet->setCellValue($key.'1', $value);
			
			$sheet->getStyle('A1:J1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(35);
			$sheet->getColumnDimension('B')->setWidth(25);
			$sheet->getColumnDimension('C')->setWidth(25);
			$sheet->getColumnDimension('D')->setWidth(15);
			$sheet->getColumnDimension('E')->setWidth(15);
			$sheet->getColumnDimension('F')->setWidth(15);
			$sheet->getColumnDimension('G')->setWidth(15);
			$sheet->getColumnDimension('H')->setWidth(20);
			$sheet->getColumnDimension('I')->setWidth(20);
			$sheet->getColumnDimension('J')->setWidth(20);

			foreach($data as $key => $value)
			{
				$sheet->setCellValue('A' . ($key+2), $value['staff_name']);
				$sheet->setCellValue('B' . ($key+2), $value['code']);
				$sheet->setCellValue('C' . ($key+2), $value['area_name']);
				$sheet->setCellValue('D' . ($key+2), $value['title']);
				$sheet->setCellValue('E' . ($key+2), $value['good_name']);
				$sheet->setCellValue('F' . ($key+2), $value['color_name']);
				$sheet->setCellValue('G' . ($key+2), $value['policy_title']);
				$sheet->setCellValue('H' . ($key+2), $value['sell_out']);
				$sheet->setCellValue('I' . ($key+2), $value['kpi']);
				$sheet->setCellValue('J' . ($key+2), $value['total']);
				$sheet->setCellValue('K' . ($key+2), $value['team']);
			}

			$filename = 'KPI Detail Staff - ' . date('Y-m-d H-i-s');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;
		}

		$pars['from_date'] = $from_date;
		$pars['to_date'] = $to_date;
		$this->view->pars = $pars;
	}

	public function storeByStaffAction()
	{
		$staff_id = $this->getRequest()->getParam('staff_id');
		$from_date = $this->getRequest()->getParam('from_date', date('1/m/Y', strtotime("-1 months")));
		$to_date = $this->getRequest()->getParam('to_date', date('t/m/Y' , strtotime("-1 months")));

		$pars = array(
			'staff_id' => $staff_id,
			'from_date' => $from_date,
			'to_date' => $to_date,
		);

		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		
		if(!empty($staff_id))
		{
			$ik = new Application_Model_ImeiKpi2();
			$data = $ik->storeByStaff($pars);
			$this->view->data = $data;
		}

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));

		$this->view->pars = $pars;
	}

	public function ajaxSearchStaffAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->getRequest()->isXmlHttpRequest())
		{
			$input = $this->getRequest()->getParam('input');

			$ik = new Application_Model_ImeiKpi2();
			$data = $ik->SearchStaff($input);
			echo json_encode($data); die;
		}
	}
	
	public function leaderAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$ImeiKpi	= new Application_Model_ImeiKpi2();
		// echo 123;
		// $from_date	= $this->getRequest()->getParam('from');
		// $to_date	= $this->getRequest()->getParam('to');

		// $from_date	= !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
		// $to_date	= !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;

		$month	= $this->getRequest()->getParam('month');
		$year	= $this->getRequest()->getParam('year');

		$date = $month.'-'.$year;

		$good		= $ImeiKpi->report_by_good($date);
		$sell_out	= $ImeiKpi->report_by_sellout($date);
		$summary	= $ImeiKpi->report_kpi_leader($date);
		// var_dump($summary);die;

		if($summary){

			set_time_limit(0);
	        error_reporting(0);
	        ini_set('memory_limit', -1);

			require_once 'PHPExcel.php';
			$PHPExcel = new PHPExcel();

			$PHPExcel->setActiveSheetIndex(0);
			$sheet = $PHPExcel->getActiveSheet();

			$headRow = 1;
			$priceRow = 2;
			$dataRow = 3;
			$listProductCol = 'F';
			$arrayProductCol = array();
			$sell_out_by_leader = array();
			

			$arrayProductCol = array(
				'A' => 'STT',
				'B' => 'MÃ NV',
				'C' => 'MÃ KD PHỤ TRÁCH',
				'D' => 'CHỨC VỤ',
				'E' => 'TỔNG CỘNG'
			);

			foreach($good as $key => $value){
				$listProductCol;
				$arrayProductCol[$listProductCol] = $value['good_name'].' / '.$value['from_day'].' -> '.$value['to_day'];
			
				$sheet->setCellValue($listProductCol.$priceRow, $value['value']);

				foreach($sell_out as $k => $v){
					
					if($value['good_id'] == $v['good_id'])
						$sell_out_by_leader[$listProductCol][$v['leader_id']] = $v['sell_out'];
					
				}
				$listProductCol++;
			}
		
			$dt		= $listProductCol++;
			$point	= $listProductCol++;
			$tlmca	= $listProductCol++;
			$dtca	= $listProductCol++;
			$dttb	= $listProductCol++;
			$kpil	= $listProductCol++;
			$kpistt	= $listProductCol++;
			$kpitn	= $listProductCol++;
			$kv		= $listProductCol++;



			$arrayProductCol[$dt]		= 'DOANH THU';
			$arrayProductCol[$point]	= 'POINT';
			$arrayProductCol[$tlmca]	= 'TỸ LỆ MÁY CHƯA ACTIVE';
			$arrayProductCol[$dtca]		= 'DOANH THU CHƯA ACTIVE';
			$arrayProductCol[$dttb]		= 'DOANH THU TÍNH BONUS';
			$arrayProductCol[$kpil]		= 'KPI LEADER';
			$arrayProductCol[$kpistt]	= 'KPI SALE TRỰC TIẾP';
			$arrayProductCol[$kpitn]	= 'KPI THỰC NHẬN';
			$arrayProductCol[$kv]		= 'KHU VỰC';

		

			foreach($summary as $key => $value){
				$r = $key + $dataRow;
				foreach($sell_out_by_leader as $k => $v){
					$so = !empty($v[$value['staff_id']]) ? $v[$value['staff_id']] : NULL;
					$sheet->setCellValue($k.$r,$so);
				}

				$sheet->setCellValue('A'.$r, $key + 1);
				$sheet->setCellValue('B'.$r, $value['staff_code']);
				$sheet->setCellValue('C'.$r, $value['staff_name']);
				$sheet->setCellValue('D'.$r, 'Leader');
				$sheet->setCellValue('E'.$r, $value['total_sellout']);
				

				// Doanh thu 80
				$sheet->setCellValue($dt.$r, $value['value_80']);
				
				// Điểm
				$sheet->setCellValue($point.$r, $value['_point']);
				
				// Phần trăm máy không active
				$sheet->setCellValue($tlmca.$r, $value['per_imei_not_active'].'%');
				
				// Doanh thu chưa active
				$sheet->setCellValue($dtca.$r,$value['value_not_active_80']);
				
				// Bonus
				$sheet->setCellValue($dttb.$r,$value['value_bonus_80']);
				
				// KPI leader
				$sheet->setCellValue($kpil.$r, $value['kpi_leader']);
				
				// KPI sale trực tiếp
				$sheet->setCellValue($kpistt.$r, $value['kpi_sale_direct']);
				
				// KPI thực
				$sheet->setCellValue($kpitn.$r,$value['kpi_real']);
				
				// Khu vực
				$sheet->setCellValue($kv.$r, $value['area_name']);
				
			}
	
			
			foreach($arrayProductCol as $key => $value)
				$sheet->setCellValue($key.$headRow, $value);

			$fn = 'Report KPI Leader - '.date('d_m_Y');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename='.$fn.'.xlsx');
			$objWriter->save('php://output');

		}
	}
	public function exportAsmAction(){
		
		


		$export		= $this->getRequest()->getParam('export');
		$from_date	= $this->getRequest()->getParam('from_date', date('1/m/Y', strtotime("-1 months")));
		$to_date	= $this->getRequest()->getParam('to_date', date('t/m/Y' , strtotime("-1 months")));
		

		$pars = array(
			'from_date'	=> $from_date,
			'to_date'	=> $to_date
		);

		$pars['from_date']	= !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : date('Y-m-1');
		$pars['to_date']	= !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : date('Y-m-d');



		if($export)
		{
			
			$QImeiKpi2 = new Application_Model_ImeiKpi2();
			$data = $QImeiKpi2->report_kpi_asm($pars['from_date']);

			// set_time_limit(0);
	  //       error_reporting(0);
	  //       ini_set('memory_limit', -1);

			require_once 'PHPExcel.php';
	        $PHPExcel = new PHPExcel();
	        $heads = array(
	            'STT',
	            'MÃ NV',
	            'NV KD PHỤ TRÁCH
				(Name Sale)',
	            'KHU VỰC LÀM VIỆC',
	            'CHỨC VỤ',
	            'KHU VỰC',
	            'TỔNG CỘNG',
	            'POINT',
	            'MỨC KPI',
	            'VALUE (80%)',
	            'LEADER',
	            'KPI',
	            'KPI SAU KHI CẤN TRỪ LEADER',
	            'TỶ LỆ CHÊNH LỆCH CHƯA ACTIVE',
	            'PHẠT',
	            'THỰC NHẬN',

	        );

	        
	        $PHPExcel->setActiveSheetIndex(0);
	        $sheet = $PHPExcel->getActiveSheet();
	        $alpha = 'A';
	        $index = 1;
	        foreach ($heads as $key) {
	            $sheet->setCellValue($alpha . $index, $key);
	            $alpha++;
	        }

	        $index = 2;
        	$stt = 0;

        	foreach($data as $key => $value):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['fullname']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( ($value['is_partner'] == 0 || $value['is_partner'] == NULL ) ? 'ASM':'PARTNER', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['total_sellout']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['_point']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['kpi_limit'].'%'), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['value_80']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['kpi_leader']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['kpi']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['exclude_kpi_leader']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['per_imei_not_active'].'%'), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['penalize']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['kpi_real']), PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;

	        $filename = 'Report KPI ASM - ' . date('Y-m-d');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;
		}
		
        

        $pars['from_date'] = $from_date;
		$pars['to_date'] = $to_date;
		$this->view->pars = $pars;

	}

}