<?php

    class CheckInGpsController extends My_Controller_Action
    {
        public function createCheckInGpsAction() {
            require_once 'check-in-gps' . DIRECTORY_SEPARATOR . 'create-check-in-gps.php';
        }

        public function saveCheckInGpsAction() {
            require_once 'check-in-gps' . DIRECTORY_SEPARATOR . 'save-check-in-gps.php';
        }
    }