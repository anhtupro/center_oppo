<?php

  class TrackingLocationController extends My_Controller_Action{

      function init(){

      }

      function indexAction(){
        $QTrackingPos     = new Application_Model_TrackingPosition();
        $pos_arr = array();

        $params   = array();
        $page     = $this->getRequest()->getParam('page',1);
        $limit    = LIMITATION;
        $total    = 0;

        $pos_arr = $QTrackingPos->fetchPagination($page, $limit, $total, $params);
        $this->view->tracking_list = $pos_arr;
      }

      function userAction(){

        $this->_helper->layout->disableLayout();

        $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
        $QStaff           = new Application_Model_Staff();
        $staff_id         = $userStorage->id;
        $current_user     = $QStaff->findStaffid($staff_id)->toArray();
        // echo "<pre>";
        // var_dump($current_user->toArray());
        // exit;

        $QDepartments     = new Application_Model_Team();
        $current_team     =  $QDepartments->get_all_cache()[$current_user['department']]['name'];
        $titles           = $QDepartments->get_cache_title();

        $QRegionalMarket  = new Application_Model_RegionalMarket();
        $rms              = $QRegionalMarket->get_cache_all();
        $current_rm       = $rms[$current_user['regional_market']];

        $QArea            = new Application_Model_Area();
        $areas            = $QArea->get_cache();
        $current_area     = $areas[$current_rm['area_id']];

        $current_user['name'] = mb_strtolower($current_user['firstname'].' '.$current_user['lastname']);
        $current_user["area"] = $current_area;
        $current_user["title"] = $titles[$current_user['title']];
        $current_user["team"] = $current_team;

        // $user = array(
        //   "name"        =>  mb_strtolower($current_user['firstname'].' '.$current_user['lastname']),
        //   "area"        =>  $current_area,
        //   "title"       =>  $titles[$current_user['title']],
        //   "team"        =>  $current_team
        // );

        $this->view->user = $current_user;

      }

      function checkinAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
       $content          = $this->getRequest()->getParam("content");
       $checkInMode      =  $this->getRequest()->getParam('mode');
       
//        if($this->getRequest()->getMethod() == "POST"){
          if($checkInMode == "login"){
          }else if($checkInMode == "push"){
            // get content from ajax post
            $staff_id = "5899";
            $data_arr = array(
              'staff_id'      =>    $staff_id,
              'location_arr'  =>    $content,
              'created_at'    =>    date('Y-m-d H:i:s')
            );
           
            $QTrackingPos     = new Application_Model_TrackingPosition();
            $save_result      = $QTrackingPos->insert($data_arr);
             echo json_encode(array('status' => 1,
                                    'message' => 'success',
                                    'id'    =>$save_result 
                 ));
          }
//        }

      }

      function viewAction(){

        $staff_id = $this->getRequest()->getParam('staff_id');
        if( isset($staff_id) ){

            $params   = array();
            $page     = $this->getRequest()->getParam('page',1);
            $limit    = 100;
            $total    = 0;

            $params['staff_id'] = $staff_id;

            $QTrackingPos               = new Application_Model_TrackingPosition();
            $pos_arr                    = $QTrackingPos->fetchPagination($page, $limit, $total, $params);
            // echo "<pre>";
            // var_dump($pos_arr);
            // die;

            $this->view->user_infor     = $pos_arr[0];
            $this->view->tracking_list  = $pos_arr;

        }else{
          $this->_redirect(HOST.'tracking-location');
        }

      }

      function mapAction(){
        $track_id   = $this->getRequest()->getParam('track_id');
        $track_date = $this->getRequest()->getParam('created_at');

        if( isset($track_id) || isset($track_date)){

            $params   = array();
            $page     = $this->getRequest()->getParam('page',1);
            $limit    = 100;
            $total    = 0;

            $params['id']   = $track_id;
            $params['created_at'] = $track_date;

            $QTrackingPos               = new Application_Model_TrackingPosition();
            $pos_arr                    = $QTrackingPos->fetchPagination($page, $limit, $total, $params);

            $count_pos = count($pos_arr);
            $locations = array();

            if( $count_pos == 0 ){
              $this->_redirect(HOST.'tracking-location');
            } else{
              foreach ($pos_arr as $key => $value) {
                $locations = array_merge($locations, json_decode($value['location']));
              }
            }

            $this->view->locations       = json_encode($locations);
            $this->view->track_infor     = $pos_arr[0];

        }else{
          $this->_redirect(HOST.'tracking-location');
        }

      }
  }
