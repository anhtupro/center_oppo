<?php
class LeaveController extends My_Controller_Action{
    
    public function init(){
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if(empty($userStorage)){
            $this->_redirect("/user/login?b=" . urlencode($_SERVER['REQUEST_URI']));
        }
    }

    public function ajaxLoadImageAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        if($this->getRequest()->isXmlHttpRequest()){
            $id = $this->getRequest()->getParam('id');
            $QLeaveDetail = new Application_Model_LeaveDetail();
            $data = $QLeaveDetail->getImageById($id);

            $list_image = json_decode($data['image']);

            $path = '/public/files/leave/' . $data['staff_id'] . '/';
            $array_link_img = array();

            foreach($list_image as $key => $val){
                $array_link_img[] = $path . $val;
            }
            echo json_encode($array_link_img); die;
        }
    }

    public function createAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
       
        if($this->getRequest()->isPost()){
            $is_leave_half = My_Util::escape_string($this->getRequest()->getParam("is-leave-half"));
            $from = My_Util::escape_string($this->converDate($this->getRequest()->getParam("from")));
            $to = My_Util::escape_string($this->converDate($this->getRequest()->getParam("to")));
            $date = My_Util::escape_string($this->converDate($this->getRequest()->getParam("date-leave-half")));
            $leave_type = My_Util::escape_string($this->getRequest()->getParam("leave-type"));
            $reason = My_Util::escape_string($this->getRequest()->getParam("reason"));
            $due_date 	    = My_Util::escape_string($this->converDate($this->getRequest ()->getParam ( 'due_date' )));
           
            $auth = Zend_Auth::getInstance()->getStorage()->read();
            $is_office = $auth->is_officer;
            $staff_id = $auth->id;
            $db = Zend_Registry::get('db');
            
            $QStaff = new Application_Model_Staff();

            if($from<$userStorage->joined_at){
                $flashMessenger->setNamespace('error')->addMessage("Ngày phép trước ngày bắt đầu làm việc.");
                $this->_redirect("/leave/create");
            }
            
            if($leave_type == 17){
                if(empty($due_date)){
                    $flashMessenger->setNamespace('error')->addMessage("Vui lòng nhập ngày dự sinh ");
                    $this->_redirect("/leave/create");
                }
                
            }
            
            if($leave_type == 2){
                //Check locked time
                $QLockTime      = new Application_Model_LockTime();
                $where_lock     = array ();
                if(!empty($from)){
                
                    $date_check = date_create($from);
                    $month_locked = date_format($date_check,"m");
                    $year_locked  = date_format($date_check,"Y");
                
                }elseif (!empty($date)){
                
                    $date_check = date_create($date);
                    $month_locked = date_format($date_check,"m");
                    $year_locked  = date_format($date_check,"Y");
                }
                
                $where_lock[]   = $QLockTime->getAdapter ()->quoteInto ( 'month = ?', $month_locked );
                $where_lock[]   = $QLockTime->getAdapter()->quoteInto('year = ? ', $year_locked);
                $result_lock    = $QLockTime->fetchRow ( $where_lock );
                
                if(!empty($result_lock)){
                    $flashMessenger->setNamespace('error')->addMessage("Tháng này đã lock bảng công.");
                    $this->_redirect("/leave/create");
                }
                //End Check locked time
                
                if(empty($date_check)){
                    echo json_encode(array('status' => 0, 'message' => "Check ngày đăng kí phép."));
                    exit();
                }
                $date_check_contract = date_format($date_check, "Y-m-d");
                //Check Firt contract type in (2, 3, 17)
                $sql_contract = "SELECT sc.* FROM staff_contract sc
                    INNER JOIN (
                                SELECT MIN( sc.id ) id, sc.staff_id, COUNT( sc.id ) total
                                FROM staff_contract sc
                                WHERE sc.contract_term
                                IN ( 2, 3, 17 ) 
                                AND sc.`is_expired` = 0
                                AND sc.`is_disable` = 0
                                AND sc.staff_id = $staff_id
                                GROUP BY sc.staff_id
                    )c ON c.id = sc.id and sc.from_date <= '$date_check_contract' AND sc.to_date >= '$date_check_contract' AND sc.`is_expired` = 0 AND sc.`is_disable` = 0";
                $stmt_contract = $db->prepare($sql_contract);
                
                $stmt_contract->execute();
                $check_data = $stmt_contract->fetchAll();
                
                if(!empty($check_data)){
                    $flashMessenger->setNamespace('error')->addMessage("Thời gian đăng ký phép năm là thời gian thử việc, bạn vui lòng chọn lại thời gian nghỉ phép năm từ thời điểm ký hợp đồng chính thức trở đi");
                    $this->_redirect("/leave/create");
                }
        
//                $stmt = $db->prepare('CALL PR_check_max_day_per_time (:staff_id, :leave_type, :from_date, :to_date) ');
            }
//            if($leave_type == 2 && ($date >= '2019-03-01' || $from >= '2019-03-01')){
//                $flashMessenger->setNamespace('error')->addMessage("Hệ thống chặn đăng ký phép tháng 3 để tính hoàn phép. Thời gian mở lại 03/03/2019");
//                $this->_redirect("/leave/create");
//            }
            // check phep hop le hay khong
            // check 1 lan nghi
            $stmt = $db->prepare('CALL PR_check_max_day_per_time (:staff_id, :leave_type, :from_date, :to_date) ');
            $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
            $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
            $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
            $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
            $stmt->execute();
            $pass_per_time = $stmt->fetchAll();
            $stmt->closeCursor();
            if ($pass_per_time[0]['pass_per_time'] == 1) {
                $flashMessenger->setNamespace('error')->addMessage("Số ngày nghỉ vượt quá giới hạn của 1 lần nghỉ.");
                $this->_redirect("/leave/create");
            }
            // check ca nam
            if($date){
                $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
                $stmt->bindParam('to_date', My_Util::escape_string($date), PDO::PARAM_STR);
                $stmt->bindParam('from_date', My_Util::escape_string($date), PDO::PARAM_STR);
            }else{
                $stmt = $db->prepare('CALL PR_check_max_day_per_year_fix (:staff_id, :leave_type, :from_date, :to_date, :is_leave_half) ');
                $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
                $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
            }
            
            $leave_half_check = ($is_leave_half == 0.5) ? 1 : 0 ;
            $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
            $stmt->bindParam('leave_type', $leave_type, PDO::PARAM_INT);
            $stmt->bindParam('is_leave_half', $leave_half_check , PDO::PARAM_INT);

            $stmt->execute();
            $pass_per_year = $stmt->fetchAll();
            $stmt->closeCursor();
            if($leave_type == 2){
                if ($pass_per_year[0]['pass_per_year'] == 1) {
                $flashMessenger->setNamespace('error')->addMessage("Số ngày nghỉ vượt quá giới hạn của 1 năm. (Tối đa ".$pass_per_year[0]['annual']." ngày)");
                $this->_redirect("/leave/create");
                }
            }

            
            define('MAX_SIZE_UPLOAD', 3145728);
            
            $name1 = "";
            if(!empty($_FILES['image']['name'])){
                $ext_arr = array('jpg','png','JPG','PNG','jpeg','JPEG');
                $ext1 = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                if(($_FILES['image']['size'] > MAX_SIZE_UPLOAD)){
                    $flashMessenger->setNamespace('error')->addMessage("Kích thước vượt quá 3Mb");
                    $this->_redirect("/leave/create");
                    return ;
                }
                if(!in_array($ext1, $ext_arr)){
                    $flashMessenger->setNamespace('error')->addMessage("Hình sai định dạng png, jpg, jpeg");
                    $this->_redirect("/leave/create");
                    return;
                }
                $name1= "leave_".$userStorage->id."_". time().".png";
                $file1 = APPLICATION_PATH . "/../public/photo/leave/" . $name1;
                if (!empty($_FILES['image']['name']) ) {
                    $success1 = @move_uploaded_file($_FILES['image']['tmp_name'], $file1);
                    require_once "Aws_s3.php";
                    $s3_lib = new Aws_s3();
                    if ($success1) {
                        $s3_lib->uploadS3($file1, "photo/leave/", $name1);
                    }
                }
            
            }
            
            if($is_leave_half == 1){
                if(strtotime($from) > strtotime($to)){
                    $flashMessenger->setNamespace('error')->addMessage("Ngày bắt đầu nghỉ phép không được lơn hơn ngày kết thúc");
                    $this->_redirect("/leave/create");
                }else{
                    $params = array(
                        'from' => $from,
                        'to' => $to,
                        'leave_type' => $leave_type,
                        'is_officer' => $is_office,
                        'staff_id' => $staff_id,
                        'reason' => $reason,
                    );
                    if(!empty($name1)){
                        $params['image'] = $name1;
                    }
                    if(!empty($due_date)){
                        $params['due_date'] = $due_date;
                    }
                    $QLeaveDetail = new Application_Model_LeaveDetail();
                    $data = $QLeaveDetail->insertLeave2($params);
                }
            }else{
                $params = array(
                    'date' => $date,
                    'leave_type' => $leave_type,
                    'is_officer' => $is_office,
                    'staff_id' => $staff_id,
                    'reason' => $reason,
                );
                if(!empty($name1)){
                    $params['image'] = $name1;
                }
                if(!empty($due_date)){
                    $params['due_date'] = $due_date;
                }
                $QLeaveDetail = new Application_Model_LeaveDetail();
                $data = $QLeaveDetail->insertLeaveHalf2($params);
            }
            if($data['status'] == 1){
                $flashMessenger->setNamespace('success')->addMessage('Thêm phép thành công.');
                $this->_redirect("/leave/list-my-leave");
            }else{
                $flashMessenger->setNamespace('error')->addMessage($data['message']);
                $this->_redirect("/leave/create");
            }
        }
        
        $QLeavInfo = new Application_Model_LeaveInfo();
        $this->view->checkHalfLeave = $QLeaveDetail->checkHalfLeave();
        $this->view->stock = $QLeavInfo->getStockById(Zend_Auth::getInstance()->getStorage()->read()->id);
        $QLeaveType = new Application_Model_LeaveType();
        $this->view->parent_type = $QLeaveType->getParent(); 
        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
    }

    public function createTimeOffAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $QLeaveDetail = new Application_Model_LeaveDetail();
        if($this->getRequest()->isPost()){
            $is_leave_half = $this->getRequest()->getParam("is-leave-half");
            $from = $this->converDate($this->getRequest()->getParam("from"));
            $to = $this->converDate($this->getRequest()->getParam("to"));
            $date = $this->converDate($this->getRequest()->getParam("date-leave-half"));
            $leave_type = $this->getRequest()->getParam("leave-type");
            $reason = $this->getRequest()->getParam("reason");
            $staff_id = $this->getRequest()->getParam('staff_id');
            $QStaff = new Application_Model_Staff();
            $auth = $QStaff->find($staff_id);
            $auth = $auth->current();
            $is_office = $auth->is_officer;
            
            $upload = new Zend_File_Transfer_Adapter_Http();
            $uniqid = uniqid('', true);
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                    . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'
                    . DIRECTORY_SEPARATOR . 'leave'
                    . DIRECTORY_SEPARATOR . $userStorage->id;
            
            if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

            $upload->setDestination($uploaded_dir);

            $array_img = array();
            $files = $upload->getFileInfo();
            foreach($files as $file => $fileInfo) 
            {
                if(!empty($fileInfo['name']))
                {
                    $old_name = $fileInfo['name'];

                    $tExplode = explode('.', $old_name);
                    $extension = strtolower(end($tExplode));
                    $new_name = md5(uniqid('', true)) . '.' . $extension;
                    $upload->receive($file);
                    if (is_file($uploaded_dir . DIRECTORY_SEPARATOR . $old_name))
                    {
                        rename($uploaded_dir . DIRECTORY_SEPARATOR . $old_name, $uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
                    }
                    $array_img[] = $new_name;
                }
            }

            if($is_leave_half == null){
                if(strtotime($from) > strtotime($to)){
                    $flashMessenger->setNamespace('error')->addMessage("Ngày bắt đầu nghỉ phép không được lơn hơn ngày kết thúc");
                    $this->_redirect("/leave/create");
                }
                else{
                    $params = array(
                        'from' => $from,
                        'to' => $to,
                        'leave_type' => $leave_type,
                        'is_officer' => $is_office,
                        'staff_id' => $staff_id,
                        'reason' => $reason,
                        'image' => json_encode($array_img),
                    );
                    $QLeaveDetail = new Application_Model_LeaveDetail();
                    $data = $QLeaveDetail->insertLeave($params);
                }
            }else{
                $params = array(
                    'date' => $date,
                    'leave_type' => $leave_type,
                    'is_officer' => $is_office,
                    'staff_id' => $staff_id,
                    'reason' => $reason,
                    'image' => json_encode($array_img),
                );
                $QLeaveDetail = new Application_Model_LeaveDetail();
                $data = $QLeaveDetail->insertLeaveHalf($params);
            }
            if($data['status'] == 1){
                $flashMessenger->setNamespace('success')->addMessage('Thêm phép thành công.');
                $this->_redirect("/leave/list-my-leave");
            }else{
                $flashMessenger->setNamespace('error')->addMessage($data['message']);
                $this->_redirect("/leave/create");
            }
        }
        
        $QLeavInfo = new Application_Model_LeaveInfo();

        $this->view->checkHalfLeave = $QLeaveDetail->checkHalfLeave();
        $this->view->stock = $QLeavInfo->getStockById($auth->id);
        $QLeaveType = new Application_Model_LeaveType();
        $this->view->parent_type = $QLeaveType->getParent(); 

        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
    }


    public function setAllDate($now = '', $end = ''){
        $now = "2016-01-01";
        $end = "2016-12-31";

        $now_val = strtotime($now);
        $end_val = strtotime($end);

        $Sat_if_off = 1;
        $array_data = array();
        while ($now_val <= $end_val) {
            $is_off = 0;
            if(date('D', $now_val) == 'Sun'){
                $is_off = 1;
            }elseif(date('D', $now_val) == 'Sat'){
                if ($Sat_if_off == 1){
                    $is_off = 2;
                    $Sat_if_off = 0;
                }else{
                    $Sat_if_off = 1;
                }
            }
            $array_data[] = "('" . date("Y-m-d", $now_val) . "', " . $is_off . ",'')";
            $now_val += 86400;
        }
        

        $values = "INSERT INTO `all_date` (`date`, is_off, note) values " . implode(",", $array_data);
        echo $values;
        die;
    }

    public function allDateSettingAction(){  
    }

    public function listMyLeaveAction(){
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
       
        $time       = My_Util::escape_string($this->getRequest()->getParam('time', date('m').'/'.date('Y')));
        $staff_id   = $userStorage->id;
        $delete     = $this->getRequest()->getParam('delete');
        $page       = $this->getRequest()->getParam('page');
        
        $limit = 10;
        if(!empty($delete)){
            $QLeaveDetail->deleteLeave($delete);
        }

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];
        $year1 = $array_time[1];
        if($month != 2){
           //  $this->_redirect('/leave/list-my-leave?time=02%2F2019');
        }
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
//        if(in_array($month, array(1,2)) AND $year == date('Y')){
//            $year = $year - 1;
//        }
        $cycle      = $QLeaveDetail->getCycleByYear((in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year);
        $params = array(
            'staff_id' => Zend_Auth::getInstance()->getStorage()->read()->id,
            'month' => $month,
            'year' =>  $year,
            'from' => in_array($month, array(1,2)) ?  $year  . '-' . $month . '-01' : $from,
            'to' =>  in_array($month, array(1,2)) ?  $year   . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year + 1) : $to,
        );
        
        $db  = Zend_Registry::get('db');
        //Lấy chu kỳ phép
        $stmt = $db->prepare("SELECT * FROM v_staff_contract_begin WHERE staff_id = :p_staff_id");
        $stmt->bindParam('p_staff_id',$staff_id , PDO::PARAM_INT);
        $stmt->execute();
        $result_staff_contract_begin = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = null;
        $this->view->staff_contract_begin = $result_staff_contract_begin[0];
        $this->view->off_date = $this->off_date;
		
        $data = $QLeaveDetail->_select($limit, $page, $params);
        $this->view->data = $data;
        $this->view->limit = $limit;
        $this->view->offset = $page;
        $this->view->total = $data['total'];

        $this->view->params = $params;
        $this->view->cycle = $cycle;
                    
//        $stmt_seniority = $db->prepare("SELECT * FROM v_staff_seniority_five_year WHERE id = $staff_id");
        $stmt_seniority = $db->prepare("SELECT * FROM v_staff_seniority_five_year WHERE id = :p_staff_id");
        $stmt_seniority->bindParam('p_staff_id',$staff_id , PDO::PARAM_INT);
        $stmt_seniority->execute();
        $staff_seniority = $stmt_seniority->fetchAll();
        $stmt_seniority->closeCursor();
        $this->view->staff_seniority = $staff_seniority[0];
        $this->view->url = HOST . 'leave/list-my-leave' . ('' ? '?' . http_build_query('') . '&' : '?');

       //Quy phep    
        
        $p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;
        $stmt_leave_fund = $db->prepare('CALL PR_annual_leave_fund_by_staff (:staff_id, :year, :month) ');
        $stmt_leave_fund->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt_leave_fund->bindParam('year', $p_year , PDO::PARAM_INT);
        $stmt_leave_fund->bindParam('month', $month, PDO::PARAM_INT);
        $stmt_leave_fund->execute();
        $annual_leave_fund = $stmt_leave_fund->fetchAll();
        $stmt_leave_fund->closeCursor();
        $stmt_leave_fund = null;
        $this->view->annual_leave_fund =   $annual_leave_fund;
        
        $p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;  
        
        //Transfer    
        $stmt_leave_history = $db->prepare('CALL PR_history_leave_fund_by_staff (:staff_id, :year, :month) ');
        $stmt_leave_history->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt_leave_history->bindParam('year', $p_year , PDO::PARAM_INT);
        $stmt_leave_history->bindParam('month', $month, PDO::PARAM_INT);
        $stmt_leave_history->execute();
        $annual_leave_history = $stmt_leave_history->fetchAll();
        $stmt_leave_history->closeCursor();
        $stmt_leave_history = null;
        $this->view->annual_leave_history =   $annual_leave_history;

        $time_cycle_leave_staff = [];
            foreach($annual_leave_history as $tran_time){
                $lag_time = $tran_time['from_date'];
                while ($lag_time < $tran_time['to_date']){
                    $time_cycle_leave_staff[] = $lag_time;
                    $time = strtotime($lag_time);
                    $lag_time = date("Y-m-d", strtotime("+1 month", $time));
                }
            }
        $this->view->time_cycle_leave = $time_cycle_leave_staff;

        //Lich su
        $p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;
        
        $stmt_historyLeave = $db->prepare('CALL PR_year_leave_history (:staff_id, :year, :month) ');
        $stmt_historyLeave->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt_historyLeave->bindParam('year',$p_year , PDO::PARAM_INT);
        $stmt_historyLeave->bindParam('month', $month, PDO::PARAM_INT);
        $stmt_historyLeave->execute();
        $historyLeave = $stmt_historyLeave->fetchAll();
        $stmt_historyLeave->closeCursor();
        $stmt_historyLeave = null;
        $this->view->historyLeave =   $historyLeave;
        if($p_year == 2019 ){
            $stmt_staff = $db->prepare("SELECT * FROM chotphep_2019 WHERE staff_id = :p_staff_id");
            $stmt_staff->bindParam('p_staff_id',$staff_id , PDO::PARAM_INT);
        
            $stmt_staff->execute();
            $data_staff_2019 = $stmt_staff->fetch();
            $stmt_staff->closeCursor();
            $stmt_staff = null;
            $this->view->data_staff_2019 =   $data_staff_2019;
            
        }
        $flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();

    }
    
    public function eventsAction(){
        $QAll_date = new Application_Model_AllDate();
        $data_off_date = $QAll_date->getOffDay(date("Y"));

        $data = array();
        foreach ($data_off_date as $key => $value) {
            $array_event = array(
                'title' => $value['note'],
                'off_type' => $value['is_off'],
                'start' => $value['date'],
                'end' => $value['date'],
            );
            switch ($value['is_off']) {
                case 1:
                    $array_event['className'] = 'off';
                    break;
                case 2:
                    $array_event['className'] = 'sat-off';
                    break;
                case 3:
                    $array_event['className'] = 'holiday';
                    break;
                default:
                    break;
            }
            $data[] = $array_event;
            unset($array_event);
        }

        echo json_encode($data);
        exit;
    }

    public function ajaxLoadChildLeaveTypeAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->isXmlHttpRequest()){
            $id = $this->getRequest()->getParam('id');
            if(!empty($id)){
                $QLeaveType = new Application_Model_LeaveType();
                echo json_encode($QLeaveType->getChild($id));
            }else{
                echo json_encode(array());
            }
        }
        exit;
    }

    public function ajaxLoadDetailLeaveAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        // if($this->getRequest()->isXmlHttpRequest())
        {
            $id = $this->getRequest()->getParam('id');
            if(!empty($id))
            {
                $QLeaveType = new Application_Model_LeaveType();
                $info = $QLeaveType->findById($id);
                echo json_encode($info);
            }
            else
            {
                echo json_encode(array());
            }
        }
        exit;
    }

    public function setupDateAjaxAction()
    {   
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->isXmlHttpRequest())
        {
            $date = date("Y-m-d", ($this->getRequest()->getParam("date")/1000));
            $is_off = $this->getRequest()->getParam("is_off");
            $note = $this->getRequest()->getParam("note");
            $off_type = $this->getRequest()->getParam("off_type");
            $params = array(
                'date' => $date,
                'is_off' => $is_off,
                'off_type' => $off_type,
                'note' => $note,
            );
            $QAllDate = new Application_Model_AllDate();
            if($QAllDate->updateAllDate($params))
            {
                $QLeaveDetail = new Application_Model_LeaveDetail();
                // $QLeaveDetail->updateTotal($date);
                echo json_encode(array("status" => 1));
            }
            else
            {
                echo json_encode(array("status" => 0));
            }
            die;
        }
    }

    public function listLeaveDetailAction()
    {
   
	$flashMessenger = $this->_helper->flashMessenger;
        $QLeaveDetail   = new Application_Model_LeaveDetail();
        $page           = $this->getRequest()->getParam('page');
        $page           = empty($page)?1:$page;
        $limit          = 10;
        $offset         = ($page-1)*$limit;

        $code           = $this->getRequest()->getParam('code');
        $name           = $this->getRequest()->getParam('name');
	$hr_status      = $this->getRequest()->getParam('hr_approved');
        $export         = $this->getRequest()->getParam('export');
        $export_year    = $this->getRequest()->getParam('export_year');
	$export_leave   = $this->getRequest()->getParam('export_leave');
	$export_leave_detail = $this->getRequest()->getParam('export_leave_detail');
        $export_leave_detail1 = $this->getRequest()->getParam('export_leave_detail1');
        $email          = $this->getRequest()->getParam('email');
        $month          = $this->getRequest()->getParam('month', date('m'));
        $year           = $this->getRequest()->getParam('year', date('Y'));        
        $status         = $this->getRequest()->getParam('status');
        $approved       = $this->getRequest()->getParam('approved', 0);
        $notapproved    = $this->getRequest()->getParam('notapproved', 0);
        $hr_approved    = $this->getRequest()->getParam('hr_approved1',0);
        $nothr_approved = $this->getRequest()->getParam('nothr_approved');
        $from_date      = $this->getRequest()->getParam('from_date', date('Y-m-1'));
	$number_day_of_month = cal_days_in_month ( CAL_GREGORIAN, intval ( date('m') ), date('Y') );
        $to_date        = $this->getRequest()->getParam('to_date', date('Y') . "-" . date('m') . "-" . $number_day_of_month);
        $note           = $this->getRequest()->getParam('note');

        $delete         = $this->getRequest()->getParam('delete');
        $change_date_approve = $this->getRequest()->getParam('choose-date-leave');// id leave
        $date_approve_leave = $this->getRequest()->getParam('date-approve-leave');
        if(!empty($delete)){
            $QLeaveDetail->deleteLeave($delete);
        }
        
        $QStaffPermisson = new Application_Model_StaffPermission();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QTeam  = new Application_Model_Team();
        $staff_title_info = $userStorage->title;
        $team_info = $QTeam->find($staff_title_info);
        $team_info = $team_info->current();
        $group_id = $team_info['access_group'];
        $this->view->group_id = $group_id;
        $this->view->show_detail =  (in_array($group_id, array(HR_ID)) || in_array($userStorage->id, array(5899, 341, 7278)) ) ? 1 : 0;
        
        if($QStaffPermisson->checkPermission($userStorage->code) 
                || in_array( $group_id, array(ASM_ID, 16)) 
                || in_array($userStorage->title, array(SALE_SALE_ASM, SALES_ADMIN_TITLE, SALES_TITLE, SALES_LEADER_TITLE, PB_SALES_TITLE)) 
                ||  $group_id == HR_ID 
                || in_array($userStorage->id , array(341 , 7278)) ){
          
            if(!empty($approved)){
                $params_approved = array(
                    'status' => 1,
                    'id' => $approved,
                    'note' => $note,
                );
                $QLeaveDetail->updateStatus($params_approved);
                $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
            }

            if(!empty($notapproved)){
                $params_notapproved = array(
                    'status' => 2,
                    'id' => $notapproved,
                    'note' => $note,
                );
                $QLeaveDetail->updateStatus($params_notapproved);
            }

            if(!empty($hr_approved)){
                $params_hr_approved = array(
                    'status' => 1,
                    'id' => $hr_approved,
                    'note' => $note,
                    'hr_approved_by' => $userStorage->id,
                );
                $QLeaveDetail->updateHrStatus($params_hr_approved);
                $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
            }
            
            if(!empty($change_date_approve) && !empty($date_approve_leave)){
                if(date('Y-m-d H:i:s') >= date('Y-m-01 00:00:00') && date('Y-m-d H:i:s') < date('Y-m-05 19:00:00')){//rule set v
                    $params_hr_approved = array(
                        'status' => 1,
                        'id' => $change_date_approve,
                        'note' => $note,
                        'hr_approved_by' => $userStorage->id,
                        'hr_approved_at' => $date_approve_leave
                    );
                    $QLeaveDetail->updateHrChooseDateStatus($params_hr_approved);
                    $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
                }
            }
            
            if(!empty($nothr_approved)){
                $params_hr_approved = array(
                    'status' => 2,
                    'id' => $nothr_approved,
                    'note' => $note,
                );
                $QLeaveDetail->updateHrStatus($params_hr_approved);
                $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
            }

            $params = array(
                'code'      => $code,
                'name'      => $name,
                'status'    => $status,
                'hr_approved' => $hr_status,
		'from_date' => $from_date,
                'to_date'   => $to_date 
            );
            $params_insurance = $params_viec_rieng = $params_nam = $params;
            $QTime            = new Application_Model_Time2();
            $params = array(
                'off' => 1,
                'name' => $name,
                'code' => $code,
                'email' => $email,
                'month' => $month,
                'year' => $year,
                'area' => $area,
                'department' => $department,
                'team' => $team,
                'title' => $title,
                'off_long' => $off_long
            );
                
                        if(isset($export_year) and $export_year == 1){
           
		   $param_export  = array(
                    'code'      => $code,
                    'name'      => $name,
                    'status'    => $status,
                    'hr_approved' => $hr_status,
                    'from_date'  => $from_date,
                    'to_date'   => $to_date,
                    );
				
                require_once 'PHPExcel.php';
                $PHPExcel = new PHPExcel();
                $alphaExcel = new My_AlphaExcel();
                $heads = array(
                    $alphaExcel->ShowAndUp() => 'STT',
                    $alphaExcel->ShowAndUp() => 'Code',
                    $alphaExcel->ShowAndUp() => 'Staff Name',
                    $alphaExcel->ShowAndUp() => 'Team',
                    $alphaExcel->ShowAndUp() => 'Title',
                    $alphaExcel->ShowAndUp() => 'Off date',
                    $alphaExcel->ShowAndUp() => 'From',
                    $alphaExcel->ShowAndUp() => 'To',
                    // $alphaExcel->ShowAndUp() => 'To Điều chỉnh',
                    // $alphaExcel->ShowAndUp() => 'Total điều chỉnh',
                    $alphaExcel->ShowAndUp() => 'Total theo đơn',
                    $alphaExcel->ShowAndUp() => 'Total theo đơn theo thời gian search',
                    $alphaExcel->ShowAndUp() => 'Nghỉ thực tế theo thời gian search',
                    $alphaExcel->ShowAndUp() => 'Reason',
                    $alphaExcel->ShowAndUp() => 'Group Type',
                    $alphaExcel->ShowAndUp() => 'Type',
                    $alphaExcel->ShowAndUp() => 'Status',
                    $alphaExcel->ShowAndUp() => 'Hr Approve',
                    $alphaExcel->ShowAndUp() => 'Ngày dự sinh',
                    $alphaExcel->ShowAndUp() => 'Ngày đăng ký',
                    
                );

                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();

                foreach($heads as $key => $value)
                    $sheet->setCellValue($key.'1', $value);

                $sheet->getStyle('A1:Q1')->applyFromArray(array('font' => array('bold' => true)));

                $sheet->getColumnDimension('A')->setWidth(10);
                $sheet->getColumnDimension('B')->setWidth(30);
                $sheet->getColumnDimension('C')->setWidth(20);
                $sheet->getColumnDimension('D')->setWidth(20);
                $sheet->getColumnDimension('E')->setWidth(15);
                $sheet->getColumnDimension('F')->setWidth(30);
                $sheet->getColumnDimension('G')->setWidth(20);
                $sheet->getColumnDimension('H')->setWidth(15);

                $db             = Zend_Registry::get('db');

                $stmt = $db->prepare('CALL PR_Export_Leave_Detail_Year (:code, :name, :status, :hr_approved, :from_date, :to_date) ');
                $stmt->bindParam('code', $param_export['code'], PDO::PARAM_STR);
                $stmt->bindParam('name', $param_export['name'], PDO::PARAM_STR);
                $stmt->bindParam('status', $param_export['status'], PDO::PARAM_STR);
                $stmt->bindParam('hr_approved', $param_export['hr_approved'], PDO::PARAM_STR);
                $stmt->bindParam('from_date', $param_export['from_date'], PDO::PARAM_STR);
                $stmt->bindParam('to_date', $param_export['to_date'], PDO::PARAM_STR);
//                $list_export_staff_id_admin = (in_array($group_id, array(HR_ID)) || in_array($userStorage->id, array(5899, 341, 7278)) ) ? 0 : $list_export_staff_id;
//                $stmt->bindParam('p_staff_id', $list_export_staff_id_admin , PDO::PARAM_STR);

                $stmt->execute();
                $data_export = $stmt->fetchAll();
                $stmt->closeCursor();
				
                foreach ($data_export as $key => $value) {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staff_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['team_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['title_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($value['off_date']) ? date("d/m/Y", strtotime($value['off_date'])) : '');
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['from_date'])));
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['to_date'])));
                    // $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($value['to_date_after']) ?   date("d/m/Y", strtotime($value['to_date_after'])) : '');
                    // $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_after']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_theo_don']);
//                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_thucte']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_thucte_search']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['reason']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['leave_group']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['leave_type_note']);
                    $status = $value['status'];
                    if($status == 1){
                        $note = 'Approved';
                    }elseif($status == 2){
                        $note = 'Not Approved';
                    }else{
                        $note = 'Pending';
                    }
                    
                    $hr_approved  = $value['hr_approved'];
                    if($hr_approved == 1){
                        $note_hr = 'Approved';
                    }elseif($hr_approved == 2){
                        $note_hr = 'Not Approved';
                    }else{
                        $note_hr = 'Pending';
                    }
                    
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $note);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $note_hr);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['due_date'])?date('d/m/Y', strtotime($value['due_date'])):'');
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['created_at'])?date('d/m/Y', strtotime($value['created_at'])):'');
                    
                }

                $filename = 'Leave - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;
            }
            $list_export_staff_id = $QTime->getListStaffById($params);
            $params_insurance['parent_leave_type'] = 6;
            $params_viec_rieng['parent_leave_type'] = 3;
            $params_nam['parent_leave_type'] = 1;
            $db   = Zend_Registry::get('db');
            $stmt_due   = $db->prepare("CALL `PR_over_due_leave`(:p_from_date, :p_to_date, :p_staff_id)");
            $stmt_due->bindParam('p_from_date', $from_date , PDO::PARAM_STR);
            $stmt_due->bindParam('p_to_date', $to_date, PDO::PARAM_STR);
            $stmt_due->bindParam('p_staff_id', $list_export_staff_id, PDO::PARAM_STR);
            $stmt_due->execute();
            $data_due = $stmt_due->fetchAll();
            $stmt_due->closeCursor(); 
            $stmt_due = $db = null;

            
             $data_nam = $QLeaveDetail->_selectAdmin(NULL, $page, $params_nam, $list_export_staff_id);
            $data_viec_rieng = $QLeaveDetail->_selectAdmin(NULL, $page, $params_viec_rieng, $list_export_staff_id);
            $data_insurance = $QLeaveDetail->_selectAdmin(NULL, $page, $params_insurance, $list_export_staff_id);
            $this->view->params = $params;
             $this->view->data_limit = $data_due;
            $this->view->data_nam = $data_nam;
            $this->view->data_viec_rieng = $data_viec_rieng;
            $this->view->data_insurance = $data_insurance;

        if(isset($export_leave_detail) and $export_leave_detail){
                $array_time = explode("-", $from_date);
                $month      = $array_time[1];
                $year      = $array_time[0];

                $alphaExcel = new My_AlphaExcel();
                $heads = array(
                    $alphaExcel->ShowAndUp() => 'STAFF ID',
                    $alphaExcel->ShowAndUp() => 'CODE',
                    $alphaExcel->ShowAndUp() => 'NAME',
                    $alphaExcel->ShowAndUp() => 'DEPARTMENT',
                    $alphaExcel->ShowAndUp() => 'TEAM',
                    $alphaExcel->ShowAndUp() => 'TITLE',
                    $alphaExcel->ShowAndUp() => 'BEGIN CONTRACT',
                    $alphaExcel->ShowAndUp() => 'JOINED AT',
                    $alphaExcel->ShowAndUp() => 'OFF DATE',
                    $alphaExcel->ShowAndUp() => 'TỔNG QUỸ PHÉP ĐÃ CÔNG THÂM NIÊN',
                    $alphaExcel->ShowAndUp() => 'PHÉP THÂM NIÊN TRÊN 5 NĂM',
                    $alphaExcel->ShowAndUp() => 'TỔNG QUỸ PHÉP THEO CHU KỲ',
                );
                 $month_head = in_array($month, array(1,2)) ? 13 : $month;
//                if($month > 2 && $month <=12 ){
                    $headsMonth = 3;
                    while ($headsMonth < $month_head){
                        $heads[$alphaExcel->ShowAndUp()] = 'THÁNG ' . $headsMonth;
                         $headsMonth++;
                    }
//                }
                 if($month == 2){
                     $heads[$alphaExcel->ShowAndUp()] = 'THÁNG 1';
                 }   
                 $heads[$alphaExcel->ShowAndUp()] = 'THÁNG SEARCH';
                 $heads[$alphaExcel->ShowAndUp()] = 'TỔNG PHÉP ĐÃ SỬ DỤNG';
                 $heads[$alphaExcel->ShowAndUp()] = 'TỒN PHÉP';
                 $heads[$alphaExcel->ShowAndUp()] = 'TITLE_ID';
                 $heads[$alphaExcel->ShowAndUp()] = 'COMPANY GROUP';
                set_time_limit(0);
                ini_set('memory_limit', -1);
                error_reporting(~E_ALL);
                ini_set('display_error', 0);

                require_once 'PHPExcel.php';
                $PHPExcel = new PHPExcel();

                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();
                $alpha = 'A';
                $index = 1;
                foreach ($heads as $key)
                {
                    $sheet->setCellValue($alpha . $index, $key);
                    $alpha++;
                }
                $index = 2;
                $intCount = 1;   
                $db   = Zend_Registry::get('db');
                $stmt_approve   = $db->prepare("CALL `PR_Get_Info_Leave_Detail_Fix`(:p_from_date, :p_to_date, :p_staff_id)");
                $stmt_approve->bindParam('p_from_date', $from_date , PDO::PARAM_STR);
                $stmt_approve->bindParam('p_to_date', $to_date, PDO::PARAM_STR);
                $list_export_staff_id_admin = (in_array($group_id, array(HR_ID)) || in_array($userStorage->id, array(5899, 341, 7278)) ) ? 0 : $list_export_staff_id;
                $stmt_approve->bindParam('p_staff_id', $list_export_staff_id_admin , PDO::PARAM_STR);
                $stmt_approve->execute();
                $data_leave_detail = $stmt_approve->fetchAll();
                $stmt_approve->closeCursor(); 
                $stmt_approve = $db = null;
//                echo "<pre>";
//                print_r($data_leave_detail);
//                echo "</pre>";
//                exit();
                try{
                    if ($data_leave_detail)
                        foreach ($data_leave_detail as $_ke1 => $val_de){
                           
                            $alpha = 'A';
                            $sheet->setCellValue($alpha++ . $index, $val_de['id']);
                            $sheet->getCell($alpha++ . $index)->setValueExplicit($val_de['staff_code'],PHPExcel_Cell_DataType::TYPE_STRING);
                            $sheet->getCell($alpha++ . $index)->setValueExplicit($val_de['staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);  
                            $sheet->setCellValue($alpha++ . $index, isset($val_de['dep']) ? $val_de['dep'] : "");
                            $sheet->setCellValue($alpha++ . $index, isset($val_de['team']) ? $val_de['team'] : "");
                            $sheet->setCellValue($alpha++ . $index, isset($val_de['title']) ? $val_de['title'] : "");
                            $sheet->setCellValue($alpha++ . $index, isset($val_de['staff_joined']) ? $val_de['staff_joined'] : "");
                            $sheet->setCellValue($alpha++ . $index, isset($val_de['joined_at']) ? $val_de['joined_at'] : "");
                            $sheet->setCellValue($alpha++ . $index, isset($val_de['off_date']) ? $val_de['off_date'] : "");
//                            $seniority_year = ($val_de['quyphep'] > 0 && $val_de['seniority_year'] > 0 && (empty($val_de['off_date']) || $val_de['off_date'] > $val_de['seniority_begin']) && $val_de['seniority_begin'] <= '2019-02-28') ? 1 : 0;
                            $seniority_year = !empty($val_de['seniority_year']) ?  $val_de['seniority_year'] : 0;
                            $sheet->setCellValue($alpha++ . $index, (isset($val_de['quyphep']) || $seniority_year > 0) ? $val_de['quyphep'] + $seniority_year  : "");
                            $sheet->setCellValue($alpha++ . $index, $seniority_year );
                            $sheet->setCellValue($alpha++ . $index, isset($val_de['quyphep']) ? $val_de['quyphep'] : "");
                            $hoanphep = 0;
                            $month_search = 0;
                            if(in_array($month, array(1,2))){
                                $month_search = 13;
                            }else{
                                $month_search = $month;
                            }
//                            $month = in_array($month, array(1,2)) ? 12 : $month;
//                            if($month > 2 && $month <=12 ){
                                $headsMonth = 3;
                                while ($headsMonth < $month_search){
                                    $text_month = 'Thang'.$headsMonth; 
                                    $array_time = explode("-", $val_de['off_date']);
                                    $month_off  = $array_time[1];
                                    $year_off  = $array_time[0];
                                    
                                    if(!empty($val_de['off_date']) &&($headsMonth == $month_off)  &&($val_de['off_date'] <= $to_date) ){
                                            $hoanphep = (($val_de['quyphep'] + $seniority_year) - ($val_de['phepnam'] + $val_de['tamungphep'] + $val_de['Tong'] ));
                                          
                                            if($hoanphep != 0){
                                                $sheet->setCellValue($alpha++ . $index, isset($text_month) ? $hoanphep+$val_de[$text_month] : "");
                                            }else{
                                                $sheet->setCellValue($alpha++ . $index, isset($text_month) ? $val_de[$text_month] : "");
                                            }
                                    }else{
                                        $sheet->setCellValue($alpha++ . $index, isset($text_month) ? $val_de[$text_month] : "");
                                    }
                                     $headsMonth++;
                                }
//                            }
                                if(in_array($month, array(1,2))){
                                    $headsMonth = 1;
                                    while ($headsMonth < $month){
                                        $text_month = 'Thang'.$headsMonth; 
                                        $array_time = explode("-", $val_de['off_date']);
                                        $month_off  = $array_time[1];
                                        if(!empty($val_de['off_date']) &&($headsMonth == $month_off) ){
                                                $hoanphep = (($val_de['quyphep'] + $seniority_year)  - ($val_de['phepnam'] + $val_de['tamungphep'] + $val_de['Tong'] ));
                                             
                                                if($hoanphep != 0){
                                                    $sheet->setCellValue($alpha++ . $index, isset($text_month) ? $hoanphep+$val_de[$text_month] : "");
                                                }else{
                                                    $sheet->setCellValue($alpha++ . $index, isset($text_month) ? $val_de[$text_month] : "");
                                                }
                                        }else{
                                            $sheet->setCellValue($alpha++ . $index, isset($text_month) ? $val_de[$text_month] : "");
                                        }
                                         $headsMonth++;
                                    }
                                }
                                
                            if(!empty($val_de['off_date']) && ($val_de['off_date'] <= $to_date) && ($val_de['off_date'] >= $from_date)) {
                                $thangSearch = $val_de['phepnam'] + $val_de['tamungphep'] ;
                                         //+ $seniority_year;
                                $tongphep = $val_de['phepnam'] + $val_de['tamungphep'] + $val_de['Tong'] + $hoanphep ;
//                                        + $seniority_year;
                            } else {
                                $thangSearch = $val_de['phepnam'] + $val_de['tamungphep'];
                                $tongphep = $val_de['phepnam'] + $val_de['tamungphep'] + $val_de['Tong'] + $hoanphep;
                            }
                            $sheet->setCellValue($alpha++ . $index, $thangSearch);
                            $sheet->setCellValue($alpha++ . $index, $tongphep);
                            $tonphep =  (!empty($val_de['off_date']) && ($val_de['off_date'] <= $to_date)) ? 0 :  ($seniority_year + $val_de['quyphep']) - ($val_de['phepnam'] + $val_de['tamungphep'] + $val_de['Tong']);
                            $sheet->setCellValue($alpha++ . $index, $tonphep);
                             
                            $sheet->setCellValue($alpha++ . $index, $val_de['title_id']);
                            $sheet->setCellValue($alpha++ . $index, $val_de['company_group']);
                            $index++;

                        }
                }
                catch (exception $e)
                {
                    exit;
                }
                $filename = 'Leave_detail_' . date('d-m-Y H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;
                
            }//END IF $export_leave_detail1

            
 
          
           
          
            $this->view->url = HOST . 'leave/list-leave-detail' . ($params ? '?' . http_build_query($params) . '&' : '?');

            $messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $messages = $flashMessenger->setNamespace('success')->getMessages();
            $this->view->messages = $messages;
            $this->view->messages_error = $messages_error;
            $this->view->params = $params;
            

            
            if(isset($export) and ($export  == 2)){
		   $param_export  = array(
                    'code'      => $code,
                    'name'      => $name,
                    'status'    => $status,
                    'hr_approved' => $hr_status,
                    'from_date'  => $from_date,
                    'to_date'   => $to_date,
                    );
				
                require_once 'PHPExcel.php';
                $PHPExcel = new PHPExcel();
                $alphaExcel = new My_AlphaExcel();
                $heads = array(
                    $alphaExcel->ShowAndUp() => 'STT',
                    $alphaExcel->ShowAndUp() => 'Code',
                    $alphaExcel->ShowAndUp() => 'Staff Name',
                    $alphaExcel->ShowAndUp() => 'Team',
                    $alphaExcel->ShowAndUp() => 'Title',
                    $alphaExcel->ShowAndUp() => 'Off date',
                    $alphaExcel->ShowAndUp() => 'From',
                    $alphaExcel->ShowAndUp() => 'To',
                    // $alphaExcel->ShowAndUp() => 'To Điều chỉnh',
                    // $alphaExcel->ShowAndUp() => 'Total điều chỉnh',
                    $alphaExcel->ShowAndUp() => 'Total theo đơn',
                    $alphaExcel->ShowAndUp() => 'Total theo đơn theo thời gian search',
                    $alphaExcel->ShowAndUp() => 'Nghỉ thực tế theo thời gian search',
                    $alphaExcel->ShowAndUp() => 'Reason',
                    $alphaExcel->ShowAndUp() => 'Group Type',
                    $alphaExcel->ShowAndUp() => 'Type',
                    $alphaExcel->ShowAndUp() => 'Status',
                    $alphaExcel->ShowAndUp() => 'Hr Approve',
                    $alphaExcel->ShowAndUp() => 'Ngày dự sinh',
                    $alphaExcel->ShowAndUp() => 'Ngày đăng ký',
                    
                );

                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();

                foreach($heads as $key => $value)
                    $sheet->setCellValue($key.'1', $value);

                $sheet->getStyle('A1:Q1')->applyFromArray(array('font' => array('bold' => true)));

                $sheet->getColumnDimension('A')->setWidth(10);
                $sheet->getColumnDimension('B')->setWidth(30);
                $sheet->getColumnDimension('C')->setWidth(20);
                $sheet->getColumnDimension('D')->setWidth(20);
                $sheet->getColumnDimension('E')->setWidth(15);
                $sheet->getColumnDimension('F')->setWidth(30);
                $sheet->getColumnDimension('G')->setWidth(20);
                $sheet->getColumnDimension('H')->setWidth(15);

                $db             = Zend_Registry::get('db');

                $stmt = $db->prepare('CALL PR_Export_Leave_Detail (:code, :name, :status, :hr_approved, :from_date, :to_date, :p_staff_id) ');
                $stmt->bindParam('code', $param_export['code'], PDO::PARAM_STR);
                $stmt->bindParam('name', $param_export['name'], PDO::PARAM_STR);
                $stmt->bindParam('status', $param_export['status'], PDO::PARAM_STR);
                $stmt->bindParam('hr_approved', $param_export['hr_approved'], PDO::PARAM_STR);
                $stmt->bindParam('from_date', $param_export['from_date'], PDO::PARAM_STR);
                $stmt->bindParam('to_date', $param_export['to_date'], PDO::PARAM_STR);
                $list_export_staff_id_admin = (in_array($group_id, array(HR_ID)) || in_array($userStorage->id, array(5899, 341, 7278)) ) ? 0 : $list_export_staff_id;
                $stmt->bindParam('p_staff_id', $list_export_staff_id_admin , PDO::PARAM_STR);

                $stmt->execute();
                $data_export = $stmt->fetchAll();
                $stmt->closeCursor();
				
                foreach ($data_export as $key => $value) {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staff_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['team_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['title_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($value['off_date']) ? date("d/m/Y", strtotime($value['off_date'])) : '');
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['from_date'])));
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['to_date'])));
                    // $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($value['to_date_after']) ?   date("d/m/Y", strtotime($value['to_date_after'])) : '');
                    // $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_after']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_theo_don']);
//                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_thucte']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['total_thucte_search']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['reason']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['leave_group']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['leave_type_note']);
                    $status = $value['status'];
                    if($status == 1){
                        $note = 'Approved';
                    }elseif($status == 2){
                        $note = 'Not Approved';
                    }else{
                        $note = 'Pending';
                    }
                    
                    $hr_approved  = $value['hr_approved'];
                    if($hr_approved == 1){
                        $note_hr = 'Approved';
                    }elseif($hr_approved == 2){
                        $note_hr = 'Not Approved';
                    }else{
                        $note_hr = 'Pending';
                    }
                    
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $note);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $note_hr);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['due_date'])?date('d/m/Y', strtotime($value['due_date'])):'');
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['created_at'])?date('d/m/Y', strtotime($value['created_at'])):'');
                    
                }

                $filename = 'Leave - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;
            }
			

            
            if(isset($export_leave) and $export_leave){
            
               $params['from_date'] = $from_date;
               $params['to_date'] = $to_date;
                
                require_once 'PHPExcel.php';
                $PHPExcel_le = new PHPExcel();
                $alphaExcel = new My_AlphaExcel();
                $heads = array(
                    $alphaExcel->ShowAndUp() => 'STT',
                    $alphaExcel->ShowAndUp() => 'Từ ngày',
                    $alphaExcel->ShowAndUp() => 'Đến ngày',
                    $alphaExcel->ShowAndUp() => 'Loại phép',
                    $alphaExcel->ShowAndUp() => 'Mã Nhân viên',
                    $alphaExcel->ShowAndUp() => 'Tên Nhân viên',
                    $alphaExcel->ShowAndUp() => 'Phép',
                    $alphaExcel->ShowAndUp() => 'Công'
                );

                $PHPExcel_le->setActiveSheetIndex(0);
                $sheet = $PHPExcel_le->getActiveSheet();

                foreach($heads as $key => $value)
                    $sheet->setCellValue($key.'1', $value);

                $sheet->getStyle('A1:H1')->applyFromArray(array('font' => array('bold' => true)));                

                $sheet->getColumnDimension('A')->setWidth(10);
                $sheet->getColumnDimension('B')->setWidth(30);
                $sheet->getColumnDimension('C')->setWidth(20);
                $sheet->getColumnDimension('D')->setWidth(20);
                $sheet->getColumnDimension('E')->setWidth(15);
                $sheet->getColumnDimension('F')->setWidth(30);
                $sheet->getColumnDimension('G')->setWidth(20);
                $sheet->getColumnDimension('H')->setWidth(15);
               
                $data_export_le = $QLeaveDetail->GetReportLeave($params);

                foreach ($data_export_le as $key => $value) {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['from_date'])));
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  date("d/m/Y", strtotime($value['to_date'])));
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['leavetype']);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($key+2), $value['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staffname']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['phep']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['cong']);
                }

                $filename_li = 'Report Leave - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel_le);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename_li . '.xlsx"');
                $objWriter->save('php://output');
                exit;
            }
        }
        else
        {
            echo 'jobjob';
//            $this->_redirect('/user/noauth');   
        }
        $this->view->auth = Zend_Auth::getInstance()->getStorage()->read();
    }

    public function approveLeaveDetailAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $page = $this->getRequest()->getParam('page');
        $page = empty($page)?1:$page;
        $limit = 10;
        $offset  = ($page-1)*$limit;

        $staff_id = $this->getRequest()->getParam('staff_id');
        $id = $this->getRequest()->getParam('id');
        $code = $this->getRequest()->getParam('code');
        $name = $this->getRequest()->getParam('name');
        $year = $this->getRequest()->getParam('year', date('Y'));
        $month = $this->getRequest()->getParam('month', date('m'));
        $export = $this->getRequest()->getParam('export');
        $status = $this->getRequest()->getParam('status', 0);
        $approved = $this->getRequest()->getParam('approved', 0);
        $notapproved = $this->getRequest()->getParam('notapproved', 0);
        $hr_approved = $this->getRequest()->getParam('hr_approved', 0);

        $time = $this->getRequest()->getParam('time', date('m').'/'.date('Y'));

        $delete = $this->getRequest()->getParam('delete');
        if(!empty($delete))
        {
            $QLeaveDetail->deleteLeave($delete);
            $this->_redirect(HOST . 'staff-time/list-staff-approve');
        }

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];

        $QStaffPermisson = new Application_Model_StaffPermission();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

        if($QStaffPermisson->checkPermission($userStorage->code) || in_array($userStorage->group_id, array(ASM_ID, 16)) || in_array($userStorage->title, array(SALE_SALE_ASM, SALES_ADMIN_TITLE, SALES_TITLE, SALES_LEADER_TITLE)) || $userStorage->group_id == HR_ID)
        {
            if(!empty($approved))
            {
                $params_approved = array(
                    'status' => 1,
                    'id' => $approved,
                );
                $QLeaveDetail->updateStatus($params_approved);
                $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
                $this->_redirect(HOST . 'staff-time/list-staff-approve');
            }

            if(!empty($notapproved))
            {
                $params_notapproved = array(
                    'status' => 2,
                    'id' => $notapproved,
                );
                $QLeaveDetail->updateStatus($params_notapproved);
                $this->_redirect(HOST . 'staff-time/list-staff-approve');
                // $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
            }

            if(!empty($hr_approved))
            {
                $params_hr_approved = array(
                    'status' => 1,
                    'id' => $hr_approved,
                );
                $QLeaveDetail->updateHrStatus($params_hr_approved);
                $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
            }

            $params = array(
                'id' => $id,
                'staff_id' => $staff_id,
                'code' => $code,
                'name' => $name,
                'from' => $from,
                'to' => $to,
                'status' => $status,
                'month' => $month,
                'year' => $year,
            );
            
            $data = $QLeaveDetail->_selectAdminById($limit, $page, $params);
            $this->view->data = $data;
            $this->view->limit = $limit;
            $this->view->offset = $page;
            $this->view->total = $data['total'];
            $this->view->url = HOST . 'leave/list-leave-detail' . ($params ? '?' . http_build_query($params) . '&' : '?');

            $messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $messages = $flashMessenger->setNamespace('success')->getMessages();
            $this->view->messages = $messages;
            $this->view->messages_error = $messages_error;
            $this->view->params = $params;
        }
        else
        {
            $this->_redirect('/user/noauth');   
        }
        $this->view->auth = Zend_Auth::getInstance()->getStorage()->read();
    }

    public function approvalAction()
    {
        $id = $this->getRequest()->getParam("id");
        $status = $this->getRequest()->getParam("status");

        if(isset($id))
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $QLeaveDetail = new Application_Model_LeaveDetail();
            $params = array(
                'id' => $id,
                'status' => $status,
            );
            $QLeaveDetail->updateStatus($params);

            $flashMessenger->setNamespace('success')->addMessage('Xác nhận phép thành công.');
        }
        $this->_redirect("/leave/list-leave-detail");
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam("id");
        $auth = Zend_Auth::getInstance()->getStorage()->read();
        if(isset($id) && ($auth->group_id == HR_ID || $auth->group_id == BOARD_ID))
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $QLeaveDetail = new Application_Model_LeaveDetail();
            $QLeaveDetailDay = new Application_Model_LeaveDetailDay();
//             $QLeaveDetail->deleteLeave($id);
            
            $where_detail = $QLeaveDetail->getAdapter()->quoteInto('id = ?', $id);
            $QLeaveDetail->delete($where_detail);
            
            $where_detail_day = $QLeaveDetail->getAdapter()->quoteInto('leave_detail_id  = ?', $id);
            $QLeaveDetailDay->delete($where_detail_day);
            
            $flashMessenger->setNamespace('success')->addMessage('Xóa phép thành công.');
        }
        $this->_redirect("/leave/list-leave-detail");
    }

    public function editImageAction()
    {
        $id = $this->getRequest()->getParam('id');
        
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id = $auth->id;

        if($this->getRequest()->isPost())
        {
            $listOldImage = $this->getRequest()->getParam('list_old_images', array());

            $upload = new Zend_File_Transfer_Adapter_Http();
            $uniqid = uniqid('', true);
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                    . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'
                    . DIRECTORY_SEPARATOR . 'leave'
                    . DIRECTORY_SEPARATOR . $userStorage->id;
            
            if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);

            $upload->setDestination($uploaded_dir);

            $array_img = array();
            $files = $upload->getFileInfo();
            foreach($files as $file => $fileInfo) 
            {
                if(!empty($fileInfo['name']))
                {
                    $old_name = $fileInfo['name'];

                    $tExplode = explode('.', $old_name);
                    $extension = strtolower(end($tExplode));
                    $new_name = md5(uniqid('', true)) . '.' . $extension;
                    $upload->receive($file);
                    if (is_file($uploaded_dir . DIRECTORY_SEPARATOR . $old_name))
                    {
                        rename($uploaded_dir . DIRECTORY_SEPARATOR . $old_name, $uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
                    }
                    $array_img[] = $new_name;
                }
            }
            $array_img_edit = array_merge($listOldImage, $array_img);
            $params = array(
                'id' => $id,
                'image' => json_encode($array_img_edit)
            );

            $QLeaveDetail->editImage($params);
        }
        $data = $QLeaveDetail->getLeaveById($id, $staff_id);
        $this->view->data = $data;
    }

    public function detailLeaveAction()
    {
        $id = $this->getRequest()->getParam('id');
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $data = $QLeaveDetail->getDetailLeaveById($id);
        $this->view->data = $data;
    }

    function converDate($date = '')
    {
        $array_date = explode("/", $date);

        if(count($array_date) == 3)
        {
            return $array_date[2] . '-' . $array_date[1] . '-' . $array_date[0];
        }
        return;
    }

    public function listLeaveTypeAction()
    {
        $QLeaveType = new Application_Model_LeaveType();
        $LeaveType = $QLeaveType->selectAll();
        $this->view->LeaveType = $LeaveType;
        $flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages_error= $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages      = $flashMessenger->setNamespace('success')->getMessages();
    }

    public function createLeaveTypeAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        $id = $this->getRequest()->getParam("id");
        if($this->getRequest()->isPost())
        {
            $note = $this->getRequest()->getParam('note');
            $status = ($this->getRequest()->getParam('status')==null)?0:1;
            $parent = $this->getRequest()->getParam('parent', null);
            $sub_stock = $this->getRequest()->getParam('sub_stock', 0);
            $sub_time = $this->getRequest()->getParam('sub_time', 0);
            $hr_approved = $this->getRequest()->getParam('hr_approved', 0);
            $chedo = $this->getRequest()->getParam('chedo', 0);
            $need_images = $this->getRequest()->getParam('need_images', 0);
            $need_insurance = $this->getRequest()->getParam('need_insurance', 0);
            $max_day_per_time = $this->getRequest()->getParam('max_day_per_time', 0);
            $max_day_per_year = $this->getRequest()->getParam('max_day_per_year', 0);
            $summary = $this->getRequest()->getParam('summary');

            $QLeaveType = new Application_Model_LeaveType();
            $params = array("note" => $note,
                            "status" => 1, 
                            'parent' => $parent, 
                            'sub_stock' => $sub_stock,
                            'sub_time' => $sub_time,
                            'hr_approved' => $hr_approved,
                            'chedo' => $chedo,
                            'need_images' => $need_images,
                            'need_insurance' => $need_insurance,
                            'max_day_per_time' => $max_day_per_time,
                            'max_day_per_year' => $max_day_per_year,
                            'summary' => $summary,
                        );

            $params['max_day_per_time'] = (empty($params['max_day_per_time'])?0:$params['max_day_per_time']);
            $params['max_day_per_year'] = (empty($params['max_day_per_year'])?0:$params['max_day_per_year']);

            if($id)
            {
                $params['id'] = $id;
                $res = $QLeaveType->updateLeaveType($params);
                $flashMessenger->setNamespace('success')->addMessage("Đã cập nhật thành công.");
                $this->redirect('/leave/create-leave-type?id=' . $id);
            }
            else
            {
                $res = $QLeaveType->insertLeaveType($params);
                $flashMessenger->setNamespace('success')->addMessage("Tạo mới thành công.");
                $this->redirect('/leave/list-leave-type');
            }
        }
        if($id)
        {
            $QLeaveType = new Application_Model_LeaveType();
            $this->view->data = $QLeaveType->selectById($id);
            $this->view->parent_type = $QLeaveType->getParent(array("id" => $id));
            $this->view->h1 = "Edit";
        }
        else
        {
            $QLeaveType = new Application_Model_LeaveType();
            $this->view->parent_type = $QLeaveType->getParent(array());
            $this->view->h1 = "Create";
        }
        $this->view->messages_error= $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages      = $flashMessenger->setNamespace('success')->getMessages();
    }

    public function listLeaveAdminAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        $QLeaveDetail = new Application_Model_LeaveDetail();

        $name = $this->getRequest()->getParam('name');
        $code = $this->getRequest()->getParam('code');
        $time = $this->getRequest()->getParam('time', date('m') . '/' . date('Y'));
        $id_edit = $this->getRequest()->getParam('id_edit');
        $type_edit = $this->getRequest()->getParam('type_edit');
        $edit = $this->getRequest()->getParam('edit', 0);
        $delete = $this->getRequest()->getParam('delete', 0);

        if(!empty($edit))
        {
            $QLeaveDetail->updateStatus(array('id' => $id_edit, 'status' => $type_edit));
        }

        if(!empty($delete))
        {
            
        }

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        

        $params = array(
            'name' => $name,
            'code' => $code,
            'month' => $month,
            'year' => $year,
            'from' => $from,
            'to' => $to,
        );

        $data = $QLeaveDetail->_selectAdmin(0, 1, $params);

        $this->view->data = $data['data'];
        $this->view->params = $params;
    }

    public function listLeaveHrAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        $QLeaveDetail = new Application_Model_LeaveDetail();

        $name = $this->getRequest()->getParam('name');
        $code = $this->getRequest()->getParam('code');
        $time = $this->getRequest()->getParam('time', date('m') . '/' . date('Y'));
        $deparment = $this->getRequest()->getParam('department');
        $team = $this->getRequest()->getParam('team');
        $title = $this->getRequest()->getParam('title');

        $id_edit = $this->getRequest()->getParam('id_edit');
        $type_edit = $this->getRequest()->getParam('type_edit');
        $edit = $this->getRequest()->getParam('edit, 0');

        if(!empty($edit))
        {
            $QLeaveDetail->updateStatus(array('id' => $id_edit, 'type_edit' => $type_edit));
        }

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $params = array(
            'name' => $name,
            'code' => $code,
            'month' => $month,
            'year' => $year,
            'from' => $from,
            'to' => $to,
            'department' => $deparment,
            'team' => $team,
            'title' => $title,
        );

        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $data = $QLeaveDetail->_selectHR(0, 1, $params);

        $this->view->data = $data['data'];
        $this->view->params = $params;
        $this->view->is_hr = 1;
    }

    public function createStaffLeaveAction()
    {
        $staff_id = $this->getRequest()->getParam('staff_id');
        $is_leave_half = $this->getRequest()->getParam("is-leave-half");
        $from = $this->converDate($this->getRequest()->getParam("from"));
        $to = $this->converDate($this->getRequest()->getParam("to"));
        $date = $this->converDate($this->getRequest()->getParam("date-leave-half"));
        $leave_type = $this->getRequest()->getParam("leave-type");
        $reason = $this->getRequest()->getParam("reason");
        $submit = $this->getRequest()->getParam('submit');

        $StaffRowSet = $QStaff->find($user_id);
        $staff = $StaffRowSet->current();

        if(!empty($submit))
        {

        }
    }
    
    public function viewStaffLeaveAction() {
        $QLeaveDetail = new Application_Model_LeaveDetail();
        $time         = $this->getRequest()->getParam('time', date('m').'/'.date('Y'));
        $code         = trim($this->getRequest()->getParam('code'));
        $QStaff       = new Application_Model_Staff();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if(!empty($code)){
            $where      = $QStaff->getAdapter()->quoteInto('code = ?' , $code);
            $result     = $QStaff->fetchRow($where);
            $this->view->info_staff = $result;
            $staff_id   = $result['id'];
            $off_date   = $result['off_date'];
            $this->view->off_date = $off_date;
            
        }
        $db  = Zend_Registry::get('db');
        
        $QLeavInfo  = new Application_Model_LeaveInfo();
        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];
        
//        if(in_array($month, array(1,2)) AND $year > date('Y')){
//                $year = $year - 1;
//        }
  
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
//        $p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;
//        $cycle      = $QLeaveDetail->getCycleByYear($year);
        $cycle      = $QLeaveDetail->getCycleByYear((in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year);
        $params = array(
            'staff_id' => $staff_id,
            'month' => $month,
            'year' =>  (in_array($month, array(1,2)) AND $year > date('Y')) ? $year + 1 : $year,
            'from' => in_array($month, array(1,2)) ?  $year  . '-' . $month . '-01' : $from,
            'to' =>  in_array($month, array(1,2)) ?  $year   . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year + 1) : $to,
        );
        if(!empty($staff_id)){
            $stmt_seniority = $db->prepare("SELECT * FROM v_staff_seniority_five_year WHERE id = $staff_id");
            $stmt_seniority->execute();
            $staff_seniority = $stmt_seniority->fetchAll();
            $stmt_seniority->closeCursor();
            $this->view->staff_seniority = $staff_seniority[0];
            
            $data = $QLeaveDetail->_select(1, null, $params);
            $this->view->data = $data;
            $this->view->total = $data['total'];
           
            $stmt = $db->prepare("SELECT * FROM v_staff_contract_begin WHERE staff_id = $staff_id");
            $stmt->execute();
            $result_staff_contract_begin = $stmt->fetchAll();
            $stmt->closeCursor();
            $this->view->staff_contract_begin = $result_staff_contract_begin[0];
            
            $p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;
            //Quy phep    
            $stmt_leave_fund = $db->prepare('CALL PR_annual_leave_fund_by_staff (:staff_id, :year, :month) ');
            $stmt_leave_fund->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
            $stmt_leave_fund->bindParam('year',$p_year , PDO::PARAM_INT);
            $stmt_leave_fund->bindParam('month', $month, PDO::PARAM_INT);
            $stmt_leave_fund->execute();
            $annual_leave_fund = $stmt_leave_fund->fetchAll();
            $stmt_leave_fund->closeCursor();
            $this->view->annual_leave_fund =   $annual_leave_fund;
            
            $p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;
            //Transfer    
            $stmt_leave_history = $db->prepare('CALL hr.PR_history_leave_fund_by_staff(:staff_id, :year, :month) ');
            $stmt_leave_history->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
            $stmt_leave_history->bindParam('year', $p_year, PDO::PARAM_INT);
            $stmt_leave_history->bindParam('month', $month, PDO::PARAM_INT);
            $stmt_leave_history->execute();
            $annual_leave_history = $stmt_leave_history->fetchAll();
            $stmt_leave_history->closeCursor();
            $this->view->annual_leave_history =   $annual_leave_history;
            $time_cycle_leave_staff = [];
            foreach($annual_leave_history as $tran_time){
                $lag_time = $tran_time['from_date'];
                while ($lag_time < $tran_time['to_date']){
                    $time_cycle_leave_staff[] = $lag_time;
                    $time = strtotime($lag_time);
                    $lag_time = date("Y-m-d", strtotime("+1 month", $time));
                }
            }
            $this->view->time_cycle_leave = $time_cycle_leave_staff;
            
           $p_year = (in_array($month, array(1,2)) AND $year == date('Y')) ? $year - 1 : $year;
            //Lich su
            $stmt_historyLeave = $db->prepare('CALL PR_year_leave_history (:staff_id, :year, :month) ');
            $stmt_historyLeave->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
            $stmt_historyLeave->bindParam('year',$p_year  , PDO::PARAM_INT);
               $stmt_historyLeave->bindParam('month', $month, PDO::PARAM_INT);
            $stmt_historyLeave->execute();
            $historyLeave = $stmt_historyLeave->fetchAll();
            $stmt_historyLeave->closeCursor();
            $this->view->historyLeave =   $historyLeave;
           
           
        
        }
        
        $this->view->params = $params;
        
        $this->view->stock = $QLeavInfo->getStockById($staff_id);

        $this->view->year_stock = $QLeavInfo->getYearStockById($staff_id);
        //  $this->view->historyLeave = $QLeavInfo->getHistoryLeave($staff_id);
       
        $this->view->code = $code;
        
        $this->view->cycle = $cycle;
        
    }
    public function changeSauchedoToDateAction(){
        require_once 'leave'.DIRECTORY_SEPARATOR.'change-sauchedo-to-date.php';
    }
    public function changeDiligentPassbyFromAction(){
        require_once 'leave'.DIRECTORY_SEPARATOR.'change-diligent-passby-from.php';
    }
    
    public function changeToDateAfterAction(){
        require_once 'leave'.DIRECTORY_SEPARATOR.'change-to-date-after.php';
    }

}