<?php
class HrCourseController extends My_Controller_Action
{

    public function indexAction()
    {
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'index.php';
    }

    public function listCourseAction(){
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'list-course.php';
    }
    
    public function createCourseAction(){
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'create-course.php';
    }
    
    public function saveCourseAction(){
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'save-course.php';
    }
    
    public function staffCourseAction(){
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'staff-course.php';
    }
    
    public function loadStaffAction(){
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'load-staff.php';
    }

    public function surveyCreatorAction(){
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'survey-creator.php';
    }

    public function saveSurveyCreatorAction(){
        require_once 'hr-course'.DIRECTORY_SEPARATOR.'save-survey-creator.php';
    }

    public function deleteCourseAction(){
        $id             = $this->getRequest()->getParam("id");
        $QHrCourse      = new Application_Model_HrCourse();
        $QHrCourseStaff = new Application_Model_HrCourseStaff();
        $db             = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
                $where   = $QHrCourse->getAdapter()->quoteInto('id = ?', $id);
                $where_  = $QHrCourseStaff->getAdapter()->quoteInto('course_id = ?', $id);
                $QHrCourse->update(array('del' => '1'), $where);
                $QHrCourseStaff->update(array('del' => '1'), $where_);
                $db->commit();
                return;
        } catch (exception $e) {
                $db->rollBack();
                return;
        }
    }

    public function ratingCourseAction(){
        $userStorage            = Zend_Auth::getInstance()->getStorage()->read();
        $course_id              = $this->getRequest()->getParam("course_id");
        $rating_value           = $this->getRequest()->getParam("rating_value");

        $QHrCourseRatingStars   = new Application_Model_HrCourseRatingStars();
        $db                     = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $where      = [];
            $where[]    = $QHrCourseRatingStars->getAdapter()->quoteInto('course_id = ?', $course_id);
            $where[]    = $QHrCourseRatingStars->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $data       = $QHrCourseRatingStars->fetchAll($where);

            if(!empty($data[0])):
                $QHrCourseRatingStars->update(array(
                    'star' => $rating_value
                ), $where);
            else:
                $QHrCourseRatingStars->insert(array(
                    'staff_id' => $userStorage->id,
                    'course_id' => $course_id,
                    'star'      => $rating_value
                ));
            endif;
            $db->commit();
            return;
        } catch (exception $e) {
            $db->rollBack();
            return;
        }
    }

    public function staffReportAction(){
        require_once 'hr-course'. DIRECTORY_SEPARATOR . 'staff-report.php';
    }

    public function staffReportModelAction(){
        $staff_id           = $this->getRequest()->getParam("staff_id", 0);
        $type               = $this->getRequest()->getParam("type", 0);
        $staff_name         = $this->getRequest()->getParam("staff_name", NULL);
        
        $QHrCourse          = new Application_Model_HrCourse();
        $hr_course          = $QHrCourse->getListCourse($staff_id, $type);
        $this->view->full_name = $staff_name;
        $this->view->hr_course = $hr_course;
        $this->_helper->layout()->disablelayout(true);
    }

    public function oppoClassroomAction(){
        require_once 'hr-course'. DIRECTORY_SEPARATOR . 'oppo-classroom.php';
    }

    public function oppoClassroomModelAction(){
        require_once 'hr-course'. DIRECTORY_SEPARATOR . 'oppo-classroom-model.php';
    }

    public function saveOppoClassroomAction(){
        require_once 'hr-course'. DIRECTORY_SEPARATOR . 'save-oppo-classroom.php';
    }

    public function oppoClassroomInfoAction(){
        require_once 'hr-course'. DIRECTORY_SEPARATOR . 'oppo-classroom-info.php';
    }
}
