<?php 

	$from		= $this->getRequest()->getParam('from_date');
	$to			= $this->getRequest()->getParam('to_date');
	$area_id	= $this->getRequest()->getParam('area_id');
	$list_title	= $this->getRequest()->getParam('title');
	$department	= $this->getRequest()->getParam('department');
	$team		= $this->getRequest()->getParam('team');
	$export		= $this->getRequest()->getParam('export');
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();

	if(!empty($from)){
		$from = str_replace('/', '-', $from);
		$from = date('Y-m-d', strtotime($from));
	}
	else{
		$from = date("Y-01-01",strtotime("-1 year"));
	}

	if(!empty($to)){
		$to = str_replace('/', '-', $to);
		$to = date('Y-m-d', strtotime($to));
	}
	else{
		$to = date("Y-m-d");
	}



	$list_year = $this->get_list_year($from, $to);

	
	
	$QHrManagement 		= new Application_Model_HrManagement();
	$QTeam 				= new Application_Model_Team();
	$QArea 				= new Application_Model_Area();
	$QRegionalMarket  	= new Application_Model_RegionalMarket();
	$QTeam 				= new Application_Model_Team();
        
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $regional_markets = $QRegionalMarket->get_cache_all();

    $all_area =  $QArea->fetchAll(null, 'name');
	$team_cache = $QTeam->get_cache();
	$title_cache = $QTeam->get_cache_title();


	if ($export) {
		$d = new DateTime($from);
		$to_date = $d->format('Y').'-'.$d->format('m').'-'.cal_days_in_month(CAL_GREGORIAN,$d->format('m'), $d->format('Y'));
		$data = $QHrManagement->exportReport($from,$to_date);
		My_Report_Hr::reportHr($data);
	}

	$params = [
		'from' 			=> $from,
		'to'			=> $to,
		'title'			=> $list_title,
		'area_id'		=> $area_id,
		'department'	=> $department,
		'team'			=> $team
	];

	$seniority_type = [
		1 => [
			'color'=> '#379B65',
			'name' => 'Dưới 3 Tháng'
		],
		2 => [
			'color'=> '#3FBF74',
			'name' => 'Dưới 6 Tháng',
		],
		3 => [
			'color'=> '#26A65B',
			'name' => 'Dưới 12 Tháng',
		],
		4 => [
			'color'=> '#EB6670',
			'name' => 'Dưới 2 Năm',
		],
		5 => [
			'color'=> '#EB6B98',
			'name' => 'Dưới 3 Năm',
		],
		6 => [
			'color'=> '#FFD771',
			'name' => 'Dưới 4 Năm',
		],
		7 => [
			'color'=> '#1BBC9B',
			'name' => 'Trên 4 Năm',
		],
	];

	$list_month_year = $this->list_month_by_year(date('Y', strtotime($from)));

	$all_month_year = [];
	foreach($list_year as $key=>$value){
		$all_month_year[$value] = $this->list_month_by_year($value);
	}

	$list_month_from = $this->list_month_by_year($params['from_year']);
	$list_month_to   = $this->list_month_by_year($params['to_year']);

	$params['list_month_from'] 	= $list_month_from;
	$params['list_month_to'] 	= $list_month_to;

	$get_headcount 		 = $QHrManagement->getHeadcount($params);
	$get_seniority 		 = $QHrManagement->getSeniority($params);
	$get_headcount_total = $QHrManagement->getHeadcountTotal($params);

	$headcount = [];

	foreach($get_headcount as $key=>$value){

		$headcount[$value['MonthNo']][$value['YearNo']] = [
			'staff_work'	=> $value['staff_work'],
			'staff_off'		=> $value['staff_off']
		];
	}

	$seniority = [];

	foreach($get_seniority as $key=>$value){
		$seniority[$value['MonthNo']][$value['YearNo']][$value['seniority']] = 
			[
				'work' => $value['count_seniority'],
				'off' => $value['count_seniority_off']
			];
	}

	$headcount_total = [];

	foreach($get_headcount_total as $key=>$value){

		$headcount_total[$value['MonthNo']][$value['YearNo']][$value['title']] = [
			'staff_work'	=> $value['staff_work'],
			'staff_off'		=> $value['staff_off'],
			'staff_new'		=> $value['staff_new']
		];
	}


	$list_month_from = $this->list_month_by_year($params['from_year']);
	$list_month_to   = $this->list_month_by_year($params['to_year']);


	//NEW STAFF
	$get_cache_title_team = $QTeam->get_cache_title_team();

	$title_new = [];
	$month = (int)date('m', strtotime($params['from']));
	$year  = (int)date('Y', strtotime($params['from']));
	$total_new  = 0;

	$title_new_2 = [];
	$month_2 = (int)date('m', strtotime($params['to']));
	$year_2  = (int)date('Y', strtotime($params['to']));
	$total_new_2  = 0;
	
	foreach($title_cache as $key=>$value){
		$sum = !empty($headcount_total[$month][$year][$key]['staff_new']) ? $headcount_total[$month][$year][$key]['staff_new'] : 0;
		$total_new += $sum;

		$sum_2 = !empty($headcount_total[$month][$year][$key]['staff_new']) ? $headcount_total[$month][$year][$key]['staff_new'] : 0;
		$total_new_2 += $sum_2;
	}


	foreach($title_cache as $key=>$value){

		//1
		$sum = !empty($headcount_total[$month][$year][$key]['staff_new']) ? $headcount_total[$month][$year][$key]['staff_new'] : 0;

		if(($sum/$total_new*100) > 1){
			$title_new[$key] = [
				'name'	=> $value.' - '.$get_cache_title_team[$key],
				'sum'	=> $sum
			];
		}
		else{
			if(empty($title_new[999])){
				$title_new[999] = ['name'	=> 'ORTHER','sum' => $sum];
			}
			else{
				$title_new[999]['sum'] = $title_new[999]['sum'] + $sum;
			}
			
		}

		//2
		$sum_2 = !empty($headcount_total[$month_2][$year_2][$key]['staff_new']) ? $headcount_total[$month_2][$year_2][$key]['staff_new'] : 0;

		if(($sum_2/$total_new_2*100) > 1){
			$title_new_2[$key] = [
				'name'	=> $value.' - '.$get_cache_title_team[$key],
				'sum'	=> $sum_2
			];
		}
		else{
			if(empty($title_new_2[999])){
				$title_new_2[999] = ['name'	=> 'ORTHER','sum' => $sum_2];
			}
			else{
				$title_new_2[999]['sum'] = $title_new_2[999]['sum'] + $sum_2;
			}
			
		}
	}
	//END NEW STAFF


	//Tranfer 
	$get_tranfer = $QHrManagement->getTranfer($params);
	$tranfer = [];
	$tranfer_total = [];
	$tranfer_title = [];
	foreach($get_tranfer as $key=>$value){

		if(empty($tranfer_total[$value['month']][$value['year']])){
			$tranfer[$value['month']][$value['year']] =  $value['sum'];
		}
		else{
			$tranfer[$value['month']][$value['year']] +=  $value['sum'];
		}

		$tranfer_total[$value['month']][$value['year']][$value['title']] =  $value['sum'];
		$tranfer_title[$value['title']] = $value['title'];
	}
	//END Tranfer

	$this->view->list_month    		= $this->month_range($params['from'], $params['to']);
	$this->view->list_year          = $list_year;
	$this->view->list_month_year    = $list_month_year;
	$this->view->list_month_from 	= $list_month_from;
	$this->view->all_month_year     = $all_month_year;
	$this->view->list_month_to 		= $list_month_to;
	$this->view->headcount 			= $headcount;
	$this->view->seniority          = $seniority;
	$this->view->seniority_type     = $seniority_type;
	$this->view->headcount_total 	= $headcount_total;
	$this->view->list_title 		= $list_title;
	$this->view->team_cache 		= $team_cache;
	$this->view->areas 				= $all_area;
	$this->view->regional_markets 	= $regional_markets;
	$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
	$this->view->params = $params;
	$this->view->title_cache        = $title_cache;
	$this->view->title_new          = $title_new;
	$this->view->title_new_2        = $title_new_2;
	$this->view->tranfer_total      = $tranfer_total;
	$this->view->tranfer_title		= $tranfer_title;
	$this->view->tranfer			= $tranfer;

    $this->_helper->layout->setLayout('layout_bi_new');