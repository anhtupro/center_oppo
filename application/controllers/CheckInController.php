<?php

    class CheckInController extends My_Controller_Action
    {

        public function init()
        {
            parent::init();
            // $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            // if(empty($userStorage))
            // {
            //     $this->_redirect("/user/login");
            // }

            // if(empty($userStorage->staff_permission) && $userStorage->group_id != HR_ID)
            // {
            //     $this->_redirect("/user/noauth");
            // }
            
        }

        public function checkInApprAction()
        {
            $from_date = $this->getRequest()->getParam('from_date', date('1/m/Y'));
            $to_date = $this->getRequest()->getParam('to_date', date('d/m/Y'));
            $code = $this->getRequest()->getParam('code');
            $name = $this->getRequest()->getParam('name');
            $email = $this->getRequest()->getParam('email');
            $page = $this->getRequest()->getParam('page', 1);
            $export = $this->getRequest()->getParam('export');
            $approved = $this->getRequest()->getParam('approved', 0);

            $from_date_format = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') . " 00:00:00" : null;
            $to_date_format = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') . " 23:59:59" : null;

            $params = array(
                'from_date' => $from_date_format,
                'to_date' => $to_date_format,
                'code' => $code,
                'name' => $name,
                'limit' => null,
            );
            $QCheckIn = new Application_Model_CheckIn();

            if(!empty($approved))
            {
                $date_approved = $this->getRequest()->getParam('date_approved');
                $code_approved = $this->getRequest()->getParam('code_approved');
                $check_in_approved = $this->getRequest()->getParam('check_in_approved');
                $check_out_approved = $this->getRequest()->getParam('check_out_approved');

                $params = array(
                    'date' => $date_approved,
                    'staff_code' => $code_approved,
                    'check_in_at' => $check_in_approved,
                    'check_out_at' => $check_out_approved,
                );

            }

            if(!empty($export))
            {
                require_once 'PHPExcel.php';
                $PHPExcel = new PHPExcel();
                $heads = array(
                    'A' => 'Name',
                    'B' => 'Code',
                    'C' => 'Date',
                    'D' => 'Check in',
                    'E' => 'Check out',
                    'F' => 'Overtime',
                    'G' => 'Status',
                    'H' => 'Time',
                );

                $data_export = $QCheckIn->get_check_in_list($params);
                
                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();

                foreach($heads as $key => $value)
                    $sheet->setCellValue($key.'1', $value);
                
                $sheet->getStyle('A1:H1')->applyFromArray(array('font' => array('bold' => true)));

                $sheet->getColumnDimension('A')->setWidth(30);
                $sheet->getColumnDimension('B')->setWidth(10);
                $sheet->getColumnDimension('C')->setWidth(15);
                $sheet->getColumnDimension('D')->setWidth(15);
                $sheet->getColumnDimension('E')->setWidth(20);
                $sheet->getColumnDimension('F')->setWidth(20);
                $sheet->getColumnDimension('G')->setWidth(30);
                $sheet->getColumnDimension('H')->setWidth(7);

                foreach($data_export['data'] as $key => $value)
                {
                    $sheet->setCellValue('A' . ($key+2), $value['firstname'] . ' ' . $value['lastname']);
                    $sheet->setCellValue('B' . ($key+2), $value['staff_code']);
                    $sheet->setCellValue('C' . ($key+2), date('d/m/Y', strtotime($value['check_in_day'])));
                    $sheet->setCellValue('D' . ($key+2),!empty($value['check_in_at'])?date('H:i', strtotime($value['check_in_at'])):'');
                    
                    $value_check_in = strtotime($value['check_in_at']);
                    $value_check_out = strtotime($value['check_out_at']);
                    if(date('d/m/Y',$value_check_in) != date('d/m/Y', $value_check_out))
                    {
                        $format = "H:i - d/m/Y";
                    }
                    else
                    {
                        $format = "H:i";
                    }

                    $sheet->setCellValue('E' . ($key+2),!empty($value['check_out_at'])?date($format, strtotime($value['check_out_at'])):'');
                    $sheet->setCellValue('F' . ($key+2), $value['overtime']);
                    $status = "";
                    if(!empty($value['realtime']))
                    {
                        if($value['check_in_late'] > 0 && !empty($value['check_in_late_time']))
                        {
                            $status .= "Đi trễ: " . $value['check_in_late_time'] . PHP_EOL;
                        }
                        if($value['check_out_soon'] > 0 && !empty($value['check_out_soon_time']))
                        {
                            $status .= "Về sớm: " . $value['check_out_soon_time'] . PHP_EOL;
                        }
                    }
                    else
                    {
                        $status .= "Vắng";
                    }
                    $sheet->setCellValue('G' . ($key+2), $status);
                    $sheet->getStyle('G' . ($key+2))->getAlignment()->setWrapText(true);
                    

                    $check_in = floatval(date('H', strtotime($value['check_in_at']))) + floatval(date('i', strtotime($value['check_in_at'])))/60;
                    $check_out = !empty($value['check_out_at'])?(floatval(date('H', strtotime($value['check_in_at']))) + floatval(date('i', strtotime($value['check_in_at'])))/60):'';
                    $begin = floatval(date('H', strtotime($value['begin']))) + floatval(date('i', strtotime($value['begin'])))/60;
                    $end = floatval(date('H', strtotime($value['end']))) + floatval(date('i', strtotime($value['end'])))/60;
                    $begin_break_time = floatval(date('H', strtotime($value['begin_break_time']))) + floatval(date('i', strtotime($value['begin_break_time'])))/60;
                    $end_break_time = floatval(date('H', strtotime($value['end_break_time']))) + floatval(date('i', strtotime($value['end_break_time'])))/60;

                    if( ($check_in >= $begin_break_time && $check_in <= $end_break_time) ||
                        ($check_out >= $begin_break_time && $check_out <= $end_break_time) )
                    {
                        $sheet->setCellValue('H' . ($key+2), "0.5");
                    }
                    elseif(!empty($check_out))
                    {
                        $check_in_calc = ($check_in<$begin)?$begin:$check_in;
                        $check_out_calc = ($check_out<$end)?$end:$check_out;
                        $time = (($check_out_calc-$check_in_calc)/($end-$begin))>0.5?1:0.5;
                        $sheet->setCellValue('H' . ($key+2),  $time);
                    }
                    else
                    {
                        $sheet->setCellValue('H' . ($key+2), 0);
                    }
                }

                $filename = 'List check in - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;

            }

            $params['limit'] = 10;
            $params['offset'] = ($page-1)*$params['limit'];

            $data = $QCheckIn->get_check_in_list($params);
            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

            $params['from_date'] = $from_date;
            $params['to_date'] = $to_date;

            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
            $this->view->data = $data['data'];
            $this->view->params = $params;
            $this->view->total = $data['total'];
            $this->view->limit = $params['limit'];
            $this->view->offset = $params['offset'];
            $this->view->url = HOST . 'check-in/check-in-appr' . ($params ? '?' . http_build_query($params) . '&' : '?');
        }

        public function indexAction()
        {
            $from_date = $this->getRequest()->getParam('from_date', date('1/m/Y'));
            $to_date = $this->getRequest()->getParam('to_date', date('d/m/Y'));
            $code = $this->getRequest()->getParam('code');
            $name = $this->getRequest()->getParam('name');
            $email = $this->getRequest()->getParam('email');
            $page = $this->getRequest()->getParam('page', 1);
            $export = $this->getRequest()->getParam('export');

            $from_date_format = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') . " 00:00:00" : null;
            $to_date_format = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') . " 23:59:59" : null;

            $params = array(
                'from_date' => $from_date_format,
                'to_date' => $to_date_format,
                'code' => $code,
                'name' => $name,
                'limit' => null,
            );
            $QCheckIn = new Application_Model_CheckIn();
            if(!empty($export))
            {
                require_once 'PHPExcel.php';
                $PHPExcel = new PHPExcel();
                $heads = array(
                    'A' => 'Name',
                    'B' => 'Code',
                    'C' => 'Date',
                    'D' => 'Check in',
                    'E' => 'Check out',
                    'F' => 'Overtime',
                    'G' => 'Status',
                    'H' => 'Time',
                    'I' => 'Real Time'
                );

                $data_export = $QCheckIn->get_check_in_list($params);
                
                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();

                foreach($heads as $key => $value)
                    $sheet->setCellValue($key.'1', $value);
                
                $sheet->getStyle('A1:I1')->applyFromArray(array('font' => array('bold' => true)));

                $sheet->getColumnDimension('A')->setWidth(30);
                $sheet->getColumnDimension('B')->setWidth(10);
                $sheet->getColumnDimension('C')->setWidth(15);
                $sheet->getColumnDimension('D')->setWidth(15);
                $sheet->getColumnDimension('E')->setWidth(20);
                $sheet->getColumnDimension('F')->setWidth(20);
                $sheet->getColumnDimension('G')->setWidth(30);
                $sheet->getColumnDimension('H')->setWidth(7);
                $sheet->getColumnDimension('I')->setWidth(12);

                foreach($data_export['data'] as $key => $value)
                {
                    $sheet->setCellValue('A' . ($key+2), $value['firstname'] . ' ' . $value['lastname']);
                    $sheet->setCellValue('B' . ($key+2), $value['staff_code']);
                    $sheet->setCellValue('C' . ($key+2), date('d/m/Y', strtotime($value['check_in_day'])));
                    $sheet->setCellValue('D' . ($key+2),!empty($value['check_in_at'])?date('H:i', strtotime($value['check_in_at'])):'');
                    
                    $value_check_in = strtotime($value['check_in_at']);
                    $value_check_out = strtotime($value['check_out_at']);
                    if(date('d/m/Y',$value_check_in) != date('d/m/Y', $value_check_out))
                    {
                        $format = "H:i - d/m/Y";
                    }
                    else
                    {
                        $format = "H:i";
                    }

                    $sheet->setCellValue('E' . ($key+2),!empty($value['check_out_at'])?date($format, strtotime($value['check_out_at'])):'');
                    $sheet->setCellValue('F' . ($key+2), $value['overtime']);
                    $status = "";
                    if(!empty($value['realtime']))
                    {
                        if($value['check_in_late'] > 0 && !empty($value['check_in_late_time']))
                        {
                            $status .= "Đi trễ: " . $value['check_in_late_time'] . PHP_EOL;
                        }
                        if($value['check_out_soon'] > 0 && !empty($value['check_out_soon_time']))
                        {
                            $status .= "Về sớm: " . $value['check_out_soon_time'] . PHP_EOL;
                        }
                    }
                    else
                    {
                        $status .= "Vắng";
                    }
                    $sheet->setCellValue('G' . ($key+2), $status);
                    $sheet->getStyle('G' . ($key+2))->getAlignment()->setWrapText(true);
                    

                    $check_in = floatval(date('H', strtotime($value['check_in_at']))) + floatval(date('i', strtotime($value['check_in_at'])))/60;
                    $check_out = !empty($value['check_out_at'])?(floatval(date('H', strtotime($value['check_in_at']))) + floatval(date('i', strtotime($value['check_in_at'])))/60):'';
                    $begin = floatval(date('H', strtotime($value['begin']))) + floatval(date('i', strtotime($value['begin'])))/60;
                    $end = floatval(date('H', strtotime($value['end']))) + floatval(date('i', strtotime($value['end'])))/60;
                    $begin_break_time = floatval(date('H', strtotime($value['begin_break_time']))) + floatval(date('i', strtotime($value['begin_break_time'])))/60;
                    $end_break_time = floatval(date('H', strtotime($value['end_break_time']))) + floatval(date('i', strtotime($value['end_break_time'])))/60;

                    if( ($check_in >= $begin_break_time && $check_in <= $end_break_time) ||
                        ($check_out >= $begin_break_time && $check_out <= $end_break_time) )
                    {
                        $sheet->setCellValue('H' . ($key+2), "0.5");
                    }
                    elseif(!empty($check_out))
                    {
                        $check_in_calc = ($check_in<$begin)?$begin:$check_in;
                        $check_out_calc = ($check_out<$end)?$end:$check_out;
                        $time = (($check_out_calc-$check_in_calc)/($end-$begin))>0.5?1:0.5;
                        $sheet->setCellValue('H' . ($key+2), $time);
                        $sheet->setCellValue('I' . ($key+2), (($check_out_calc-$check_in_calc)/($end-$begin)));
                    }
                    else
                    {
                        $sheet->setCellValue('H' . ($key+2), 0);
                        $sheet->setCellValue('I' . ($key+2), 0);
                    }
                }

                $filename = 'List check in - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;

            }

            $params['limit'] = 10;
            $params['offset'] = ($page-1)*$params['limit'];

            $data = $QCheckIn->get_check_in_list($params);
            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();

            $params['from_date'] = $from_date;
            $params['to_date'] = $to_date;

            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
            $this->view->data = $data['data'];
            $this->view->params = $params;
            $this->view->total = $data['total'];
            $this->view->limit = $params['limit'];
            $this->view->offset = $params['offset'];
            $this->view->url = HOST . 'check-in/index' . ($params ? '?' . http_build_query($params) . '&' : '?');
        }

        public function totalByStaffAction()
        {
            $month = $this->getRequest()->getParam('month', intval(date('m')));
            $year = $this->getRequest()->getParam('year', intval(date('Y')));
            $name = $this->getRequest()->getParam('name');
            $code = $this->getRequest()->getParam('code');
            $area = $this->getRequest()->getParam('area');
            $page = $this->getRequest()->getParam('page',1);

            $params = array(
                'month' => $month,
                'year' => $year,
                'name' => $name,
                'code' => $code,
                'area' => $area,
            );

            $this->view->params = $params;

            $QArea = new Application_Model_Area();
            $all_area =  $QArea->fetchAll(null, 'name');
            $this->view->areas = $all_area;
        }

    }