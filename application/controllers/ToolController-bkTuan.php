<?php

class ToolController extends Zend_Controller_Action
{
    private $table_name;
    private $uploaded_dir;
    private $month;

    public function get_dealer_name(){
        switch($this->fordealer){
            case 2:
                return 'vta';
            break;
            case 3:
                return 'viettel';
            break;
            default: return 'tgdđ';
        }
    }

    function exportAction(){

        $QGood = new Application_Model_Good();

        $where = $QGood->getAdapter()->quoteInto('cat_id = ?', PHONE_CAT_ID);
        $goods = $QGood->fetchAll($where, 'desc');
        $this->view->goods = $goods;

        $date = array();

        $QTiming = new Application_Model_Timing();
        foreach ($goods as $good){
            for ($i=1; $i<31; $i++){
                $tm = ($i<10 ? '0' : '').$i;
                $sel = $QTiming->getSellOut('2014-06-'.$tm, '2014-06-'.$tm, $good->id);
                $date[$i][$good->id] = $sel;
            }
        }
        //var_dump($date);exit;
        $this->view->date = $date;

        set_time_limit(0);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            '',
        );

        for ($i=1; $i<31; $i++){
            $tm = ($i<10 ? '0' : '').$i;
            $heads[] = '2014-06-'.$tm;
        }

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;


        foreach($goods as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['desc']);


            for ($i=1; $i<31; $i++){

                if (isset($date[$i][$item['id']]) and $date[$i][$item['id']])
                    $sheet->setCellValue($alpha++.$index, $date[$i][$item['id']]);
                else
                    $sheet->setCellValue($alpha++.$index, 0);

            }

            $index++;
        }

        $filename = 'TIMING_REPORT_BY_STORE_'.date('d/m/Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;

    }

    public function campaignAction()
    {
        require_once 'tool' . DIRECTORY_SEPARATOR . 'index.php';
    }

    public function campaignCreateAction()
    {
        require_once 'tool' . DIRECTORY_SEPARATOR . 'campaign-create.php';
    }

    function export2Action(){

        $QGood = new Application_Model_Good();

        $where = $QGood->getAdapter()->quoteInto('cat_id = ?', PHONE_CAT_ID);
        $goods = $QGood->fetchAll($where, 'desc');
        $this->view->goods = $goods;

        $date = $data = array();

        $start = '2014-04-01';

        while (strtotime($start)<strtotime('2014-06-30')){
            $date[] = array(
                $start, date('Y-m-d', strtotime('+6 days', strtotime($start)))
            );
            $start = date('Y-m-d', strtotime('+1 week', strtotime($start)));
        }

        $QTiming = new Application_Model_Timing();
        foreach ($goods as $good){

            foreach ($date as $d){
                $sel = $QTiming->getSellOut($d[0], $d[1], $good->id);
                $data[$d[0]][$good->id] = $sel;
            }
        }

        set_time_limit(0);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            '',
        );

        foreach ($date as $d){
            $heads[] = $d[0] . ' - '. $d[1];
        }

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;


        foreach($goods as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['desc']);


            foreach ($date as $d){

                if (isset($data[$d[0]][$item['id']]) and $data[$d[0]][$item['id']])
                    $sheet->setCellValue($alpha++.$index, $data[$d[0]][$item['id']]);
                else
                    $sheet->setCellValue($alpha++.$index, 0);

            }

            $index++;
        }

        $filename = 'SELL_OUT_WEEK_'.date('d/m/Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;

    }

    public function sendSmsAction()
    {

        if ( $this->getRequest()->getMethod() == 'POST' ) {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            echo '<link href="/css/bootstrap.min.css" rel="stylesheet">';

            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $user_id = $userStorage->id;
            set_time_limit(0);
            try{

            $content = trim($this->getRequest()->getParam('content'));
            $phone   = $this->getRequest()->getParam('phone');

            if(empty($content))
                throw new exception('Invalid content');

            $phone = trim($phone);
            $phone = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $phone);
                $phone = explode("\n", $phone);

            if (count($phone) == 1 && $phone[0] == '') {
                exit;
            }

            $phone = array_unique($phone);

            $content = My_String::khongdau($content);

            error_reporting(1);
            require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
            $uri = 'http://61.28.225.75/dong-hanh-cung-son-tung/sms?';
            $auth  = 'cuongdethuong';
            $token = md5($auth . SECRET_KEY);

            $this->wssURI = $uri .'&wsdl';
            $this->namespace = 'OPPOVN';



            $client = new nusoap_client($this->wssURI);
            $client->soap_defencoding = 'UTF-8';
            $client->decode_utf8 = false;

            $wsParams = array(
                'phone' => $phone,
                'content' => $content,
                'system' => 'CENTER',
                'token' => $token,
                'auth'  => $auth
            );
			
			foreach($phone as $key=>$phone)
            {
				$rs = $this->postSMS($phone, $content);
				if($rs['BulkSendSmsResult']['error_code'] == 0){
					echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
					echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
					echo '<div class="alert alert-success">'.$phone.'</div>';
				}
				else{
					echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
					echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
					echo '<div class="alert alert-error">Failed - '.$phone.'</div>';
				}
				sleep(2);
			}

            //$result = $client->call("postSMS", $wsParams);
            }
            catch(exception $e)
            {

                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'40px\';</script>';
                echo '<div class="alert alert-error">Failed - '.$e->getMessage().'</div>';
                exit;
            }
        }
        else
        {

        }
    }

    public function tgddAction()
    {
        $QSelloutDistributorMap = new Application_Model_SelloutDistributorMap();
        $listSelloutDistributorMap = $QSelloutDistributorMap->getAll();
        $this->view->listSelloutDistributorMap = $listSelloutDistributorMap;
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->uid = $userStorage->id;
    }

    public function tgddSaveAction()
    {
        @ignore_user_abort(1);
        @set_time_limit(0);
        @ini_set('memory_limit', -1);
        @ini_set('max_execution_time', 0);
        ini_set('upload_max_filesize', "1000M");
        ini_set('post_max_size', "1000M");

        $this->_helper->layout->disableLayout();
        $month = $this->getRequest()->getParam('month');

        $this->fordealer = $this->getRequest()->getParam('fordealer');
        $this->fordealer = $this->get_dealer_name();

// if(!empty($_GET['dev'])){
//     var_dump($fordealer);exit;
// }
        if ( $this->getRequest()->getMethod() != 'POST' )
            exit;

        try {
            // check month
            if (!$month || !date_create_from_format('m/Y', $month))
            throw new Exception("Invalid month");

            $month = date_create_from_format('m/Y', $month);
            $d = date_diff(new DateTime(), $month);
            if ($d->y*12 + $d->m != 1)
                throw new Exception("Only upload for last month");
            $this->month = $month;
            $month_name = $month->format('Ym');

            $upload = new Zend_File_Transfer();
            $uniqid = uniqid('', true);
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'
                . DIRECTORY_SEPARATOR . 'timing'
                . DIRECTORY_SEPARATOR . $userStorage->id
                . DIRECTORY_SEPARATOR . $uniqid;

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $upload->setDestination($uploaded_dir);

            $upload->setValidators(array(
                'Size'  => array('min' => 50, 'max' => 25000000),
                'Count' => array('min' => 1, 'max' => 1),
                'Extension' => array('xlsx'),
            ));

            $errors = $upload->getErrors();
            $sError = null;

            if ($errors and isset($errors[0])) {
                switch ($errors[0]) {
                    case 'fileUploadErrorIniSize':
                        $sError = 'File size is too large';
                        break;
                    case 'fileMimeTypeFalse':
                        $sError = 'The file you selected weren\'t the type we were expecting';
                        break;
                    case 'fileExtensionFalse':
                        $sError = 'Please choose a PO file in XLSX format';
                        break;
                    case 'fileCountTooFew':
                        $sError = 'Please choose a PO file in XLSX format';
                        break;
                    case 'fileUploadErrorNoFile':
                        $sError = 'Please choose a PO file in XLSX format';
                        break;
                    case 'fileSizeTooBig':
                        $sError = 'File size is too big';
                        break;
                }
                throw new Exception($sError);
            }

            $upload->receive();

            $path_info = pathinfo($upload->getFileName());
            $filename = $path_info['filename'];
            $extension = $path_info['extension'];

            $old_name = $filename . '.'.$extension;
            $new_name = 'UPLOAD-'.md5($filename . uniqid('', true)) . '.'.$extension;

            if (is_file($uploaded_dir . DIRECTORY_SEPARATOR . $old_name)){
                rename($uploaded_dir . DIRECTORY_SEPARATOR . $old_name, $uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
            } else {
                $new_name = $old_name;
            }

            $this->uploaded_dir = $uploaded_dir;
            $this->view->uploaded_dir =  HOST . 'files'
                . '/' . 'timing'
                . '/' . $userStorage->id
                . '/' . $uniqid;

            $QFileLog = new Application_Model_FileUploadLog();

            $data = array(
                'staff_id'       => $userStorage->id,
                'folder'         => $uniqid,
                'filename'       => $new_name,
                'type'           => 'mass timing tgdd',
                'real_file_name' => $filename . '.'.$extension,
                'uploaded_at'    => time(),
            );

            $log_id = $QFileLog->insert($data);

            define("TIMING_ROW_START", 2);
            define("MODEL_COL", 0);
            define("IMEI_COL", 1);
            define("INVOICE_COL", 2);
            define("DATE_COL", 3);
            define("STORE_NAME_COL", 4);
            define("STORE_COL", 5); // giờ sẽ dùng ID của TGDĐ
            define("CUSTOMER_NAME_COL", 6);
            define("CUSTOMER_PHONE_COL", 8);
            define("CUSTOMER_ADDRESS_COL", 7);
            define("TITLE_ROW", 1);
            define("STAFF_ID", 3025);

            $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
            $_db = $app_config->resources->db->params;
            define("TOOL_DB_HOST", $_db->host);
            define("TOOL_DB_USERNAME", $_db->username);
            define("TOOL_DB_PASSWORD", $_db->password);
            define("TOOL_DB_DBNAME", $_db->dbname);

            // $table_name = "check_imei_".$month_name.'_'.$userStorage->id.'_'.date('Y_m_d_H_i_s');
            $table_name = "check_imei_".$month_name.'_'.$userStorage->id.'_'.date('Y_m_d_H_i_s').'_'.$this->fordealer;
            $this->table_name = $table_name;

            require_once 'PHPExcel.php';
            $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
            $cacheSettings = array( 'memoryCacheSize' => '128MB');
            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

            switch ($extension) {
                case 'xlsx':
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    break;
                default:
                    throw new Exception("Invalid file extension");
                    break;
            }

            $objReader->setReadDataOnly(true);
            $objPHPExcel        = $objReader->load($uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
            $objWorksheet       = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow         = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn      = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
            $total_order_row    = $highestRow - TIMING_ROW_START + 1;
            $sheet              = $objPHPExcel->getSheet(0);

            // get title
            $title = $sheet->rangeToArray('A' . TITLE_ROW . ':' . $highestColumn . TITLE_ROW, NULL, TRUE, FALSE);
            $title = isset($title[0]) ? $title[0] : array();
            $db = Zend_Registry::get('db');

            $this->_create_table();

            $sql_insert = " INSERT INTO `".$table_name."`
                (product_name, imei_sn, invoice, tgdd_store, tgdd_store_id, sellout_date, customer_name, address, phone_number)
                VALUES ";
            $sql_values = array();

            // $i là dòng
            // đọc file và insert data

            for ($i = TIMING_ROW_START; $i <= $highestRow; $i++) {
                $rowData = $sheet->rangeToArray('A' . $i . ':' . $highestColumn . $i, NULL, TRUE, FALSE);

                if (!isset($rowData[0])){
                    break;
                }

                $rowData = $rowData[0];
                if (My_String::trim($rowData[IMEI_COL]) == ''){
                    break;
                }

                $__date = DateTime::createFromFormat('Y-m-d', My_String::trim($rowData[DATE_COL]));
                if(!$__date){
                    throw new Exception('Wrong date format, correct format: <b>yyyy-mm-dd</b>');
                }

                // if(!empty($_POST['is_dev'])){
                //     echo '<pre>';
                //     var_dump($__date);
                //     exit;
                // }

                $MODEL_COL            = $db->quote( My_String::trim($rowData[MODEL_COL]) );
                $IMEI_COL             = My_String::trim($rowData[IMEI_COL]);
                $INVOICE_COL          = $db->quote( My_String::trim($rowData[INVOICE_COL]) );
                $STORE_NAME_COL       = $db->quote( My_String::trim($rowData[STORE_NAME_COL]) );
                $STORE_COL            = $db->quote( My_String::trim($rowData[STORE_COL]) );
                $DATE_COL             = $db->quote( My_String::trim($rowData[DATE_COL]) );
                $CUSTOMER_NAME_COL    = $db->quote( My_String::trim($rowData[CUSTOMER_NAME_COL]) );
                $CUSTOMER_ADDRESS_COL = $db->quote( My_String::trim($rowData[CUSTOMER_ADDRESS_COL]) );
                $CUSTOMER_PHONE_COL   = $db->quote( My_String::trim($rowData[CUSTOMER_PHONE_COL]) );

                $sql_values[] = sprintf(
                    "(%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    $MODEL_COL,
                    $IMEI_COL,
                    $INVOICE_COL,
                    $STORE_NAME_COL,
                    $STORE_COL,
                    $DATE_COL,
                    $CUSTOMER_NAME_COL,
                    $CUSTOMER_ADDRESS_COL,
                    $CUSTOMER_PHONE_COL
                );
                // echo "<pre>";
                // print_r($sql_values); die;

                if (is_array($sql_values) && count($sql_values)
                    && (($i > 0 && $i % 1000 == 0) || ($i == $highestRow))) {
                    $sql_insert_query = $sql_insert . implode(', ', $sql_values);
                    $this->_sqlquery($sql_insert_query);
                    $sql_values = array();
                }

                unset($sql_insert_query);
                unset($rowData);
                unset($MODEL_COL);
                unset($IMEI_COL);
                unset($INVOICE_COL);
                unset($STORE_NAME_COL);
                unset($STORE_COL);
                unset($DATE_COL);
                unset($CUSTOMER_NAME_COL);
                unset($CUSTOMER_ADDRESS_COL);
                unset($CUSTOMER_PHONE_COL);
            } // END big FOR

            $new_stores = $this->_check_new_store();

            if ($new_stores) {
                $this->view->filename_new_stores = $new_stores;
                throw new Exception("Create new store / Fill TGDD Store ID before importing");
            }

            $this->_update_oppo_store_id();
            $this->_set_uid();
            $this->_add_key();

            $this->view->filename_duplicate  = $this->_duplicate();
            $this->view->filename_check_imei = $this->_check_imei();
            $this->_import_timing();
        } catch (Zend_File_Transfer_Exception $e) {
            $this->view->error =  $e->getMessage();
        } catch (Exception $e) {
            $this->view->error =  $e->getMessage();
        }
    }

    public function removeImeiAction()
    {
        $this->_helper->layout->disableLayout();
        $db = Zend_Registry::get('db');
        try {

            $select_closing_date = $db->select()
                ->from(array('a'=>'closing_date'),array('closing_date'))
                ->where('type = 1');
            $closing_date = $db->fetchOne($select_closing_date);
            if(!$closing_date){
                throw new Exception("closing_date is false", 1);   
            }

            $imeis = $this->getRequest()->getParam('imeis');
            if (!$imeis) throw new Exception("No IMEI");

            $imei_list = trim($imeis);
            $imei_list = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $imei_list);
            $imei_list = explode("\n", $imei_list);
            $imei_list = array_filter($imei_list);

            foreach ($imei_list as $key => $value) {
                if (!preg_match('/^[0-9]{15}$/', $value))
                    throw new Exception("IMEI " . $value .": wrong format");
            }

            if (!count($imei_list)) throw new Exception("No IMEI");

            $db->beginTransaction();
            $sql = "DELETE timing_sale FROM timing_sale 
                    INNER JOIN timing ON timing.id = timing_sale.timing_id 
                    WHERE DATE(timing.`from`) > '".$closing_date."'  AND imei IN (".implode(',', $imei_list).")";
            $db->query($sql);

            $sql = "DELETE FROM imei_kpi WHERE DATE(`timing_date`) > '".$closing_date."'  AND imei_sn IN (".implode(',', $imei_list).")";
            $db->query($sql);

            $sql = "SELECT GROUP_CONCAT(imei_sn) imeis 
                    FROM imei_kpi 
                    WHERE DATE(`timing_date`) <= '".$closing_date."' AND imei_sn IN (".implode(',', $imei_list).")";
            $imei_before_month = $db->fetchRow($sql);

            if($imei_before_month and $imei_before_month != '' and isset($imei_before_month['imeis'])){
                $imei_before_month = explode(',',$imei_before_month['imeis']);
                if(is_array($imei_before_month) AND count($imei_before_month)){
                    $this->view->imei_before_month = $imei_before_month;
                }
            }

            $this->view->message = "Done";

            $db->commit();
        } catch (Exception $e) {
            $this->view->error = $e->getMessage();
            try {
                $db->rollback();
            } catch (Exception $e) {

            }
        }
    }

    public function runAction()
    {
        @ignore_user_abort(1);
        @set_time_limit(0);
        @ini_set('memory_limit', -1);
        @ini_set('max_execution_time', 0);

        $this->_helper->layout->disableLayout();
        $month = $this->getRequest()->getParam('month');

        if ( $this->getRequest()->getMethod() != 'POST' )
            exit;

        $db = Zend_Registry::get('db');
        try {
            // check month
            if (!$month || !date_create_from_format('m/Y', $month))
            throw new Exception("Invalid month");

            $month = date_create_from_format('m/Y', $month);
            $d = date_diff(new DateTime(), $month);
            if ($d->y*12 + $d->m != 1)
                throw new Exception("Only sync for last month");

            $sql = "call sp_imei_kpi(?, ?)";
            $db->query($sql, array($month->format('Y-m-01'), $month->format('Y-m-t')));

            $sql = "UPDATE closing_date SET closing_date = ? WHERE `type` = 1";
            $db->query($sql, array($month->format('Y-m-t')));

            My_Cache::clear_cache();

            $this->view->message = "Done";
        } catch(Exception $e) {
            $this->view->error = $e->getMessage();
        }
    }

    private function _sqlquery($sqlquery){
        date_default_timezone_set('Asia/Saigon');
        $con = mysqli_connect(TOOL_DB_HOST, TOOL_DB_USERNAME, TOOL_DB_PASSWORD, TOOL_DB_DBNAME);

        // Check connection
        if (mysqli_connect_errno())
        {
            die( "Failed to connect to MySQL: " . mysqli_connect_error() );
        }

        mysqli_set_charset($con, "utf8");
        $response = mysqli_query($con, $sqlquery);
        mysqli_close($con);

        if (!$response) throw new Exception("Error Processing Query: " . $sqlquery);

        return $response;
    }

    private function _create_table()
    {
        $sql_create_table = "DROP TABLE IF EXISTS `".$this->table_name."`;";
        $this->_sqlquery($sql_create_table);

        $sql_create_table = "CREATE TABLE `".$this->table_name."` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `product_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `imei_sn` bigint(20) unsigned NOT NULL,
                `invoice` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
                `tgdd_store` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
                `tgdd_store_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `oppo_store_id` int(11) unsigned DEFAULT NULL,
                `uid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
                `sellout_date` datetime DEFAULT NULL,
                `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
                `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
                `result` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `staff_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `store_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `sellout_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `result_code` tinyint(2) unsigned DEFAULT NULL,
                `duplicated` tinyint(2) unsigned DEFAULT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ;
        ";

        $this->_sqlquery($sql_create_table);
    }

    private function _add_key()
    {
        $sqlquery = "ALTER TABLE `".$this->table_name."`
            ADD INDEX (`imei_sn`) ,
            ADD INDEX (`invoice`) ,
            ADD INDEX (`tgdd_store_id`) ,
            ADD INDEX (`uid`) ,
            ADD INDEX (`sellout_date`) ,
            ADD INDEX (`result_code`) ,
            ADD INDEX (`duplicated`) ;
            ";
        $this->_sqlquery($sqlquery);
    }

    private function _duplicate()
    {
        $sql_mark_duplicate = "UPDATE ".$this->table_name." A
            JOIN (
                SELECT
                    MIN(t.id) AS new_id,
                    t.imei_sn,
                    MIN(t.sellout_date) AS new_date
                FROM
                    ".$this->table_name." t
                GROUP BY
                    t.imei_sn
                HAVING
                    COUNT(t.imei_sn) > 1
                ORDER BY
                    COUNT(t.imei_sn)
            ) X ON A.imei_sn = X.imei_sn
            SET A.duplicated = 1
            WHERE
                A.sellout_date > X.new_date
            OR A.id > X.new_id;
        ";

        $this->_sqlquery($sql_mark_duplicate);

        $db = Zend_Registry::get('db');
        $sql_duplicate = "SELECT
                t.product_name,
                t.imei_sn,
                t.invoice,
                tgdd_store,
                tgdd_store_id,
                uid,
                sellout_date,
                customer_name,
                address,
                phone_number
            FROM
                ".$this->table_name." t
            WHERE
                duplicated IS NOT NULL
            AND duplicated = 1";
        $result_duplicate = $this->_sqlquery($sql_duplicate);

        if (!$result_duplicate) return false;
        // xuất kết quả duplicate

        $PHPExcel = new PHPExcel();
        $heads = array(
            "Tên sản phẩm",
            "IMEI",
            "Số hóa đơn",
            "Ngày hóa đơn",
            "Kho xuất",
            "ID Kho xuất",
            "Tên khách hàng",
            "Địa chỉ",
            "Số điện thoại",
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
            $sheet->setCellValue($alpha++ . $index, $key);

        $index++;

        foreach ($result_duplicate as $key => $value) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $value['product_name']);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['imei_sn']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['invoice']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $value['sellout_date']);
            $sheet->setCellValue($alpha++ . $index, $value['tgdd_store']);
            $sheet->setCellValue($alpha++ . $index, $value['tgdd_store_id']);
            $sheet->setCellValue($alpha++ . $index, $value['customer_name']);
            $sheet->setCellValue($alpha++ . $index, $value['address']);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['phone_number']), PHPExcel_Cell_DataType::TYPE_STRING);

            $index++;
        }

        $filename = 'TGDD - IMEI Duplicated in list - ' . date('YmdHis').'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        $objWriter->save($this->uploaded_dir.DIRECTORY_SEPARATOR.$filename);

        return $filename;
    }

    private function _check_imei()
    {
        $sql_check_imei = "SELECT
            tc.product_name
            , tc.imei_sn
            , tc.invoice
            , tc.tgdd_store
            , tc.tgdd_store_id
            , tc.uid
            , tc.sellout_date
            , tc.customer_name
            , tc.address
            , tc.phone_number
            , i.out_date
            , ia.activated_at
            , l.imei_sn as lock_imei
            , f.imei_sn as fpt_imei
            , ie.type as ex_type
            , m.type as market_type
            , s.firstname
            , s.lastname
            , s.email
            , st.`name` as store_name
            , t.`from` as timing_date
            , ts.id
            , t.staff_id as timing_staff_id
            , i.good_id
            , i.good_color
            ,
            -- imei không tồn tại
            CASE
                WHEN i.imei_sn IS NULL
                    THEN 2
                        -- imei chưa xuất kho
                WHEN i.out_date IS NULL AND i.return_sn IS NULL
                    THEN 10
                        -- nó chấm công rồi
                WHEN t.`from` IS NOT NULL OR ts.imei = tc.imei_sn
                    -- AND 3025 = t.staff_id
                    THEN 1
                        -- thằng khác chấm công rồi
                /*
                WHEN t.`from` IS NOT NULL
                    AND 3025 <> t.staff_id
                    AND i.activated_date IS NOT NULL
                    AND i.activated_date <> 0
                    AND DATEDIFF(tc.sellout_date, i.activated_date) > 7
                    THEN 5
                        -- thằng khác chấm công rồi
                WHEN t.`from` IS NOT NULL
                    AND 3025 <> t.staff_id
                    AND (
                        i.activated_date IS NULL
                        OR i.activated_date = 0
                        OR DATEDIFF(tc.sellout_date, i.activated_date) <= 7
                        )
                    THEN 3
                */
                        -- bị khóa
                WHEN l.imei_sn IS NOT NULL
                    THEN 6
                        -- hàng demo fpt
                WHEN f.imei_sn IS NOT NULL
                    THEN 7
                        -- hàng đặc biệt
                WHEN ie.type IS NOT NULL
                    AND ie.type = 2
                    THEN 7
                WHEN ie.type IS NOT NULL
                    AND ie.type = 3
                    THEN 8
                WHEN ie.type IS NOT NULL
                    AND ie.type = 4
                    THEN 9
                        -- hàng demo
                /*
                WHEN m.type IS NOT NULL
                    AND m.type = 2
                    THEN 7
                        -- hàng nhân viên
                WHEN m.type IS NOT NULL
                    AND m.type = 3
                    THEN 8
                        -- hàng mượn event
                WHEN m.type IS NOT NULL
                    AND m.type = 4
                    THEN 9
                */
                        -- imei hợp lệ để chấm công
                ELSE 0
                END AS _result
            ,
            -- imei không tồn tại
            CASE
                WHEN i.imei_sn IS NULL
                    THEN 'IMEI không tồn tại'
                        -- imei chưa xuất kho
                WHEN i.out_date IS NULL AND i.return_sn IS NULL
                    THEN 'IMEI chưa xuất kho'
                        -- nó chấm công rồi
                WHEN t.`from` IS NOT NULL OR ts.imei = tc.imei_sn
                    -- AND 3025 = t.staff_id
                    THEN 'IMEI đã được chấm công'
                        -- thằng khác chấm công rồi
                /*
                WHEN t.`from` IS NOT NULL
                    AND 3025 <> t.staff_id
                    AND i.activated_date IS NOT NULL
                    AND i.activated_date <> 0
                    AND DATEDIFF(tc.sellout_date, i.activated_date) > 7
                    THEN 'IMEI đã được nhân viên khác chấm công'
                        -- thằng khác chấm công rồi
                WHEN t.`from` IS NOT NULL
                    AND 3025 <> t.staff_id
                    AND (
                        i.activated_date IS NULL
                        OR i.activated_date = 0
                        OR DATEDIFF(tc.sellout_date, i.activated_date) <= 7
                        )
                    THEN 'IMEI đã được nhân viên khác chấm công'
                */
                        -- bị khóa
                WHEN l.imei_sn IS NOT NULL
                    THEN 'IMEI khóa chấm công'
                        -- hàng demo fpt
                WHEN f.imei_sn IS NOT NULL
                    THEN 'IMEI demo FPT'
                        -- hàng đặc biệt
                WHEN ie.type IS NOT NULL
                    AND ie.type = 2
                    THEN 'IMEI demo'
                WHEN ie.type IS NOT NULL
                    AND ie.type = 3
                    THEN 'IMEI nhân viên'
                WHEN ie.type IS NOT NULL
                    AND ie.type = 4
                    THEN 'IMEI event'
                        -- hàng demo
                /*
                WHEN m.type IS NOT NULL
                    AND m.type = 2
                    THEN 'IMEI demo'
                        -- hàng nhân viên
                WHEN m.type IS NOT NULL
                    AND m.type = 3
                    THEN 'IMEI nhân viên'
                        -- hàng mượn event
                WHEN m.type IS NOT NULL
                    AND m.type = 4
                    THEN 'IMEI event'
                */
                        -- imei hợp lệ để chấm công
                ELSE 'OK'
                END AS _result_text
            FROM ".$this->table_name." tc
            LEFT JOIN warehouse.imei i ON tc.imei_sn = i.imei_sn
                AND (
                    tc.duplicated IS NULL
                    OR tc.duplicated = 0
                    )
            LEFT JOIN warehouse.market m ON m.sn = i.sales_sn
                AND m.outmysql_time <> 0
                AND m.outmysql_time IS NOT NULL --  AND i.imei_sn = _imei
            LEFT JOIN warehouse.imei_activation ia ON i.imei_sn = ia.imei_sn
            LEFT JOIN timing_sale ts ON i.imei_sn = ts.imei
                AND ts.id <> 1
            LEFT JOIN timing t ON t.id = ts.timing_id
            LEFT JOIN store st ON st.id = t.store
            LEFT JOIN staff s ON s.id = t.staff_id
            LEFT JOIN locked_imei l ON l.imei_sn = i.imei_sn
            LEFT JOIN imei_demo_fpt f ON f.imei_sn = i.imei_sn
            LEFT JOIN imei_exception ie ON ie.imei_sn = i.imei_sn
            WHERE tc.imei_sn NOT IN ('867717029729614','867717029729937','867717029731198','867717029731552','867717029731891','867717029740595','867717029741072','867717029744415','868572022137334','868572022138639','868572022138811','868572022143738','869700026913736','869700026915152','869700026916499','869700026937438','869700026937511','869700026941919','869700026946199','869700026958475','869700026958715','869700026960570','869700026960737','869700026961396','869700026964812','869700026974811','869700027117873','869700027118251','869700027118533')
            GROUP BY tc.imei_sn --  LIMIT 1;
            HAVING _result <> 0
            ";

        $result_failed = $this->_sqlquery($sql_check_imei);

        $sql_update_check_imei = "UPDATE ".$this->table_name."
            LEFT JOIN warehouse.imei ON ".$this->table_name.".imei_sn = warehouse.imei.imei_sn
                AND (
                    ".$this->table_name.".duplicated IS NULL
                    OR ".$this->table_name.".duplicated = 0
                    )
            LEFT JOIN warehouse.market ON warehouse.market.sn = warehouse.imei.sales_sn
                AND warehouse.market.outmysql_time <> 0
                AND warehouse.market.outmysql_time IS NOT NULL
            LEFT JOIN warehouse.imei_activation ia ON warehouse.imei.imei_sn = ia.imei_sn
            LEFT JOIN timing_sale ON warehouse.imei.imei_sn = timing_sale.imei
                AND timing_sale.id <> 1
            LEFT JOIN timing ON timing.id = timing_sale.timing_id
            LEFT JOIN store ON store.id = timing.store
            LEFT JOIN staff ON staff.id = timing.staff_id
            LEFT JOIN locked_imei ON locked_imei.imei_sn = warehouse.imei.imei_sn
            LEFT JOIN imei_demo_fpt ON imei_demo_fpt.imei_sn = warehouse.imei.imei_sn
            LEFT JOIN imei_exception ON imei_exception.imei_sn = warehouse.imei.imei_sn

            SET result_code = CASE
                    WHEN warehouse.imei.imei_sn IS NULL
                        THEN 2
                            -- imei chưa xuất kho
                    WHEN warehouse.imei.out_date IS NULL AND warehouse.imei.return_sn IS NULL
                        THEN 10
                            -- nó chấm công rồi
                    WHEN timing.`from` IS NOT NULL OR timing_sale.imei = ".$this->table_name.".imei_sn
                        -- AND 3025 = timing.staff_id
                        THEN 1
                            -- thằng khác chấm công rồi
                    /*
                    WHEN timing.`from` IS NOT NULL
                        AND 3025 <> timing.staff_id
                        AND warehouse.imei.activated_date IS NOT NULL
                        AND warehouse.imei.activated_date <> 0
                        AND DATEDIFF(".$this->table_name.".sellout_date, warehouse.imei.activated_date) > 7
                        THEN 5
                            -- thằng khác chấm công rồi
                    WHEN timing.`from` IS NOT NULL
                        AND 3025 <> timing.staff_id
                        AND (
                            warehouse.imei.activated_date IS NULL
                            OR warehouse.imei.activated_date = 0
                            OR DATEDIFF(".$this->table_name.".sellout_date, warehouse.imei.activated_date) <= 7
                            )
                        THEN 3
                    */
                            -- bị khóa
                    WHEN locked_imei.imei_sn IS NOT NULL
                        THEN 6
                            -- hàng demo fpt
                    WHEN imei_demo_fpt.imei_sn IS NOT NULL
                        THEN 7
                            -- hàng đặc biệt
                    WHEN imei_exception.type IS NOT NULL
                        AND imei_exception.type = 2
                        THEN 7
                    WHEN imei_exception.type IS NOT NULL
                        AND imei_exception.type = 3
                        THEN 8
                    WHEN imei_exception.type IS NOT NULL
                        AND imei_exception.type = 4
                        THEN 9
                            -- hàng demo
                    /*
                    WHEN warehouse.market.type IS NOT NULL
                        AND warehouse.market.type = 2
                        THEN 7
                            -- hàng nhân viên
                    WHEN warehouse.market.type IS NOT NULL
                        AND warehouse.market.type = 3
                        THEN 8
                            -- hàng mượn event
                    WHEN warehouse.market.type IS NOT NULL
                        AND warehouse.market.type = 4
                        THEN 9
                            -- imei hợp lệ để chấm công
                    */
                    ELSE 0
                    END
            , result =
            -- imei không tồn tại
            CASE
                WHEN warehouse.imei.imei_sn IS NULL
                    THEN 'IMEI không tồn tại'
                        -- imei chưa xuất kho
                WHEN warehouse.imei.out_date IS NULL AND warehouse.imei.return_sn IS NULL
                    THEN 'IMEI chưa xuất kho'
                        -- nó chấm công rồi
                WHEN timing.`from` IS NOT NULL
                    AND 3025 = timing.staff_id
                    THEN 'IMEI đã được chấm công'
                        -- thằng khác chấm công rồi
                /*
                WHEN timing.`from` IS NOT NULL
                    AND 3025 <> timing.staff_id
                    AND warehouse.imei.activated_date IS NOT NULL
                    AND warehouse.imei.activated_date <> 0
                    AND DATEDIFF(".$this->table_name.".sellout_date, warehouse.imei.activated_date) > 7
                    THEN 'IMEI đã được nhân viên khác chấm công'
                        -- thằng khác chấm công rồi
                WHEN timing.`from` IS NOT NULL
                    AND 3025 <> timing.staff_id
                    AND (
                        warehouse.imei.activated_date IS NULL
                        OR warehouse.imei.activated_date = 0
                        OR DATEDIFF(".$this->table_name.".sellout_date, warehouse.imei.activated_date) <= 7
                        )
                    THEN 'IMEI đã được nhân viên khác chấm công'
                */
                        -- bị khóa
                WHEN locked_imei.imei_sn IS NOT NULL
                    THEN 'IMEI khóa chấm công'
                        -- hàng demo fpt
                WHEN imei_demo_fpt.imei_sn IS NOT NULL
                    THEN 'IMEI demo FPT'
                        -- hàng đặc biệt
                WHEN imei_exception.type IS NOT NULL
                    AND imei_exception.type = 2
                    THEN 'IMEI demo'
                WHEN imei_exception.type IS NOT NULL
                    AND imei_exception.type = 3
                    THEN 'IMEI nhân viên'
                WHEN imei_exception.type IS NOT NULL
                    AND imei_exception.type = 4
                    THEN 'IMEI event'
                        -- hàng demo
                /*
                WHEN warehouse.market.type IS NOT NULL
                    AND warehouse.market.type = 2
                    THEN 'IMEI demo'
                        -- hàng nhân viên
                WHEN warehouse.market.type IS NOT NULL
                    AND warehouse.market.type = 3
                    THEN 'IMEI nhân viên'
                        -- hàng mượn event
                WHEN warehouse.market.type IS NOT NULL
                    AND warehouse.market.type = 4
                    THEN 'IMEI event'
                        -- imei hợp lệ để chấm công
                */
                ELSE 'OK'
                END";
        $this->_sqlquery($sql_update_check_imei);
        // xuất kết quả failed

        $PHPExcel = new PHPExcel();
        $heads = array(
            "Tên sản phẩm",
            "IMEI",
            "Số hóa đơn",
            "Ngày hóa đơn",
            "Kho xuất",
            "ID Kho xuất",
            "Tên khách hàng",
            "Địa chỉ",
            "Số điện thoại",
            "Loại",
            "Staff",
            "Email",
            "Time",
            "Store",
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
            $sheet->setCellValue($alpha++ . $index, $key);

        $index++;

        foreach ($result_failed as $key => $value) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $value['product_name']);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['imei_sn']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['invoice']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $value['sellout_date']);
            $sheet->setCellValue($alpha++ . $index, $value['tgdd_store']);
            $sheet->setCellValue($alpha++ . $index, $value['tgdd_store_id']);
            $sheet->setCellValue($alpha++ . $index, $value['customer_name']);
            $sheet->setCellValue($alpha++ . $index, $value['address']);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['phone_number']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $value['_result_text']);
            $sheet->setCellValue($alpha++ . $index, $value['firstname'] . ' ' . $value['lastname']);
            $sheet->setCellValue($alpha++ . $index, $value['email']);
            $sheet->setCellValue($alpha++ . $index, $value['store_name']);
            $sheet->setCellValue($alpha++ . $index, $value['timing_date']);

            $index++;
        }

        $filename = 'TGDD - IMEI import failed - ' . date('YmdHis').'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        $objWriter->save($this->uploaded_dir.DIRECTORY_SEPARATOR.$filename);

        return $filename;
    }

    private function _set_uid()
    {
        $sql = "UPDATE ".$this->table_name."
            SET uid = CONCAT(
                DATE_FORMAT(sellout_date, '%Y%m%d'),
                '_',
                oppo_store_id
            );";
        $this->_sqlquery($sql);
    }

    private function _import_timing()
    {
        // nhập chấm công
        $sql = "INSERT INTO timing (
                store,
                `from`,
                `to`,
                uid,
                created_at,
                approved_at,
                `status`,
                `note`,
                `shift`
            ) SELECT
                oppo_store_id,
                sellout_date,
                sellout_date,
                uid,
                sellout_date,
                sellout_date,
                1,
                -- 'auto insert',
                'auto insert ".$this->fordealer."',
                4
            FROM
                ".$this->table_name."
            WHERE
                (duplicated IS NULL OR duplicated = 0)
                AND result_code = 0
            GROUP BY
                uid;
        ";
        $this->_sqlquery($sql);

        // nhập chấm công
        $sql = "INSERT INTO timing_sale (
                timing_sale.imei,
                timing_sale.product_id,
                timing_sale.model_id,
                timing_sale.timing_id,
                timing_sale.customer_name,
                timing_sale.address,
                timing_sale.phone_number
            ) SELECT DISTINCT
                i.imei_sn,
                i.good_id,
                i.good_color,
                MAX( t.id ) ,
                SUBSTR(tc.customer_name FROM 1 FOR 64),
                SUBSTR(tc.address FROM 1 FOR 64),
                tc.phone_number
            FROM
                ".$this->table_name." tc
            JOIN timing t ON tc.uid = t.uid
                AND (tc.duplicated IS NULL OR tc.duplicated=0)
                AND tc.result_code = 0
            JOIN warehouse.imei i ON i.imei_sn = tc.imei_sn
            GROUP BY i.imei_sn;

        ";
        $this->_sqlquery($sql);

        // update staff
        $sql = "UPDATE timing, store
            SET timing.staff_id = fn_get_pg_id(timing.store, DATE(timing.`from`))
            WHERE
            timing.store=store.id
            AND (store.d_id=2316 OR store.`name` LIKE 'TGDĐ-%' OR store.`name` LIKE 'TGDĐ-%' OR store.`name` LIKE 'TGDĐ -%')
            AND timing.`from` >= '".$this->month->format('Y-m-01 00:00:00')."'
            AND timing.`from` <= '".$this->month->format('Y-m-t 23:59:59')."'
        ;
        ";
        $this->_sqlquery($sql);

        // update staff
        $sql = "UPDATE timing
            SET staff_id=3025
            -- WHERE note LIKE 'auto insert' AND staff_id = 0
            WHERE note LIKE 'auto insert ".$this->fordealer."' AND staff_id = 0
                AND timing.`from` >= '".$this->month->format('Y-m-01 00:00:00')."'
                AND timing.`from` <= '".$this->month->format('Y-m-t 23:59:59')."'
        ;
        ";
        $this->_sqlquery($sql);
    }

    private function _check_new_store()
    {
        $sql = "SELECT DISTINCT f.tgdd_store, f.tgdd_store_id FROM ".$this->table_name." f
            LEFT JOIN store s ON f.tgdd_store_id=s.partner_id
            WHERE s.id IS NULL
            ;";
        $new = $this->_sqlquery($sql);

        $PHPExcel = new PHPExcel();
        $heads = array(
            "Kho xuất",
            "ID Kho Xuất",
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
            $sheet->setCellValue($alpha++ . $index, $key);

        $index++;
        $flag = false;

        foreach ($new as $key => $value) {
            $flag = true;
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $value['tgdd_store']);
            $sheet->setCellValue($alpha++ . $index, $value['tgdd_store_id']);

            $index++;
        }

        $filename = 'TGDD - New Stores - ' . date('YmdHis').'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        $objWriter->save($this->uploaded_dir.DIRECTORY_SEPARATOR.$filename);

        if ($flag)
            return $filename;
        return false;
    }

    private function _update_oppo_store_id()
    {
        $sql = "UPDATE ".$this->table_name.", store
            SET oppo_store_id = store.id
            WHERE ".$this->table_name.".tgdd_store_id=store.partner_id
            ;";
        $this->_sqlquery($sql);
    }

    public function reimportAction()
    {

    }

    public function reimportSaveAction()
    {
        $this->_helper->layout->disableLayout();

        if ( $this->getRequest()->getMethod() != 'POST' ) { // Big IF

        } else {
            define("TITLE_ROW", 1);
            define("ROW_START", 2);
            define("IMEI_COL", 0);
            define("DATE_COL", 1);
            define("TYPE_COL", 2);
            define("REASON_COL", 3);

            set_time_limit(0);
            ini_set('memory_limit', -1);

            $upload = new Zend_File_Transfer();

            $uniqid = uniqid('', true);
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..'
                    . DIRECTORY_SEPARATOR .'public' . DIRECTORY_SEPARATOR . 'files'
                    . DIRECTORY_SEPARATOR . 'timing'
                    . DIRECTORY_SEPARATOR . 'kpi'
                    . DIRECTORY_SEPARATOR  . $userStorage->id
                    . DIRECTORY_SEPARATOR . $uniqid;

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);

            $upload->setDestination($uploaded_dir);

            $upload->setValidators(array(
                'Size'  => array('min' => 50, 'max' => 5000000),
                'Count' => array('min' => 1, 'max' => 1),
                'Extension' => array('xlsx', 'xls'),
            ));

            if (!$upload->isValid()){ // validate IF
                $errors = $upload->getErrors();
                $sError = null;

                if ($errors and isset($errors[0]))
                    switch ($errors[0]) {
                        case 'fileUploadErrorIniSize':
                            $sError = 'File size is too large';
                            break;
                        case 'fileMimeTypeFalse':
                            $sError = 'The file you selected weren\'t the type we were expecting';
                            break;
                        case 'fileExtensionFalse':
                            $sError = 'Please choose a file in XLS or XLSX format.';
                            break;
                        case 'fileCountTooFew':
                            $sError = 'Please choose a PO file (in XLS or XLSX format)';
                            break;
                        case 'fileUploadErrorNoFile':
                            $sError = 'Please choose a PO file (in XLS or XLSX format)';
                            break;
                        case 'fileSizeTooBig':
                            $sError = 'File size is too big';
                            break;
                    }

                $this->view->error = $sError;

            } else {
                try {
                    $upload->receive();

                    $path_info = pathinfo($upload->getFileName());
                    $filename = $path_info['filename'];
                    $extension = $path_info['extension'];

                    $old_name = $filename . '.'.$extension;
                    $new_name = 'UPLOAD-'.md5($filename . uniqid('', true)) . '.'.$extension;

                    if (is_file($uploaded_dir . DIRECTORY_SEPARATOR . $old_name)){
                        rename($uploaded_dir . DIRECTORY_SEPARATOR . $old_name, $uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
                    } else {
                        $new_name = $old_name;
                    }

                    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                    $QFileLog = new Application_Model_FileUploadLog();

                    $data = array(
                        'staff_id'       => $userStorage->id,
                        'folder'         => $uniqid,
                        'filename'       => $new_name,
                        'type'           => 'reimport kpi',
                        'real_file_name' => $filename . '.'.$extension,
                        'uploaded_at'    => time(),
                        );

                    $log_id = $QFileLog->insert($data);

                    // action
                    $error_list = array();
                    $success_list = array();
                    $number_of_order = 0;
                    $total_order_row = 0;

                    require_once 'PHPExcel.php';
                    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
                    $cacheSettings = array( 'memoryCacheSize' => '32MB');
                    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

                    switch ($extension) {
                        case 'xls':
                            $objReader = PHPExcel_IOFactory::createReader('Excel5');
                            break;
                        case 'xlsx':
                            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                            break;
                        default:
                            throw new Exception("Invalid file extension");
                            break;
                    }

                    $objReader->setReadDataOnly(true);

                    $objPHPExcel = $objReader->load($uploaded_dir . DIRECTORY_SEPARATOR . $new_name);
                    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

                    $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                    $total_order_row = $highestRow - ROW_START + 1;

                    $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

                    // get title
                    $file_title = array();
                    for ($i = 1; ( $title_tmp = trim($objWorksheet
                            ->getCellByColumnAndRow($i, TITLE_ROW)
                            ->getValue()) ) != ''; $i++) {
                        $file_title[] = $title_tmp;
                    }

                    $staff_list = array();
                    $QImeiKpi = new Application_Model_ImeiKpi();

                    $QStaff = new Application_Model_Staff();
                    $staff_cache = $QStaff->get_cache();

                    $QGood = new Application_Model_Good();
                    $good_cache = $QGood->get_cache();

                    $QGoodColor = new Application_Model_GoodColor();
                    $color_cache = $QGoodColor->get_cache();

                    $QStore = new Application_Model_Store();
                    $store_cache = $QStore->get_cache();

                    $month_pr = $this->getRequest()->getParam('month');
                    $month = date_create_from_format("m/Y", $month_pr);
                    $next_month_last_day = $month->format('t');
                    $full_month = $month->format("Y-m-");

                    // $i là dòng
                    for ($i = ROW_START; $i <= $highestRow; $i++) {
                        try {
                            $flag = true;
                            $number_of_order++;
                            $percent = round($number_of_order*100/$total_order_row, 1);

                            // get IMEI
                            $imei_tmp = trim($objWorksheet
                                ->getCellByColumnAndRow(IMEI_COL, $i)
                                ->getValue()
                            );
                            // get date
                            $date_tmp = trim($objWorksheet
                                ->getCellByColumnAndRow(DATE_COL, $i)
                                ->getValue()
                            );
                            // get type
                            $type_tmp = trim($objWorksheet
                                ->getCellByColumnAndRow(TYPE_COL, $i)
                                ->getValue()
                            );
                            // get reason
                            $reason_tmp = trim($objWorksheet
                                ->getCellByColumnAndRow(REASON_COL, $i)
                                ->getValue()
                            );

                            if (!isset($imei_tmp) || empty($imei_tmp)) continue;

                            $where = $QImeiKpi->getAdapter()->quoteInto('imei_sn = ?', $imei_tmp);
                            $imei_kpi = $QImeiKpi->fetchRow($where);

                            if (!$imei_kpi) {
                                $error_list[] = array(
                                    'reason' => 'IMEI not found: '.$imei_tmp,
                                    'row'    => $i,
                                );
                                continue;
                            }

                            if (!isset($imei_kpi['status']) || $imei_kpi['status'] != 0) {
                                $error_list[] = array(
                                    'reason' => 'Status not for reimporting: '.$imei_tmp,
                                    'row'    => $i,
                                );
                                continue;
                            }

                            if (!isset($imei_kpi['timing_date']) || $imei_kpi['timing_date'] != $date_tmp) {
                                $error_list[] = array(
                                    'reason' => 'Timing date not match: '.$imei_tmp,
                                    'row'    => $i,
                                );
                                continue;
                            }

                            $kpi = false;

                            // update
                            if (isset($type_tmp) && $type_tmp == 1) {
                                $imei_old_date = $imei_kpi['timing_date'];
                                $imei_old_day = date('d', strtotime($imei_kpi['timing_date']));

                                // cập nhật lại ngày
                                // nếu ngày imei lớn hơn ngày cuối cùng của tháng tiếp theo thì lấy là ngày cuối tháng
                                if ($imei_old_day > $next_month_last_day) $imei_old_day = $next_month_last_day;

                                $next_month = date_create_from_format("Y-m-d", $full_month . sprintf("%02d", $imei_old_day));

                                $data = $imei_kpi->toArray();
                                $data['timing_date'] = $next_month->format("Y-m-d 00:00:00");
                                $data['status'] = 2;
                                $where = $QImeiKpi->getAdapter()->quoteInto('imei_sn = ?', $imei_tmp);

                                $QImeiKpi->update($data, $where);
                                $kpi = true;
                            }

                            $staff_list[ $imei_kpi['pg_id'] ][ $kpi ? "kpi" : ($type_tmp == 0 ? "no_kpi" : "will_kpi") ][] = array(
                                'imei'     => $imei_kpi['imei_sn'],
                                'good_id'  => $imei_kpi['good_id'],
                                'color_id' => $imei_kpi['color_id'],
                                'store_id' => $imei_kpi['store_id'],
                                'old_date' => date_create_from_format("Y-m-d H:i:s", $imei_kpi['timing_date'])->format('d/m/Y'),
                                'new_date' => $kpi ? $next_month->format("d/m/Y") : null,
                                'reason'   => $kpi ? null : $reason_tmp,
                            );

                            $success_list[] = array('imei' => $imei_tmp, 'date' => $imei_kpi['timing_date']);
                        } catch (Exception $e) {
                            // $db->rollback();
                            $error_list[] = array(
                                'reason' => 'Unknown Error',
                                'row'    => $i,
                            );
                            continue;
                        } // try-catch
                    } // END big FOR

                    // $db->commit();

                    // gửi thông báo:
                    $title = sprintf("Danh sách IMEI được tính KPI bổ sung cho tháng %s", $month_pr);
                    $category_id = 4;

                    if (isset($staff_list) && count($staff_list)) {
                        foreach ($staff_list as $_staff_id => $_timings) {
                            if (! count($_timings) ) continue;

                            $content = sprintf("<p>Chào bạn %s,</p>
                                <p>Sau khi Công ty thực hiện kiểm tra, xác nhận thông tin khách hàng với các IMEI chưa được tính KPI ở tháng trước do chưa active, Công ty quyết định:</p>",
                                $staff_cache[ $_staff_id ],
                                date("d/m/Y")
                            );

                            $stt = 1;

                            // danh sách IMEI bổ sung
                            if (isset($_timings['kpi']) && count($_timings['kpi'])) {
                                $content .= "<p>".$stt++." Bổ sung  KPI cho các IMEI sau trong kỳ tính KPI tháng 6/2015:</p>
                                    <table class=\"table\">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>IMEI</th>
                                                <th>Model</th>
                                                <th>Màu sắc</th>
                                                <th>Ngày bán</th>
                                                <th>Ngày được tính KPI</th>
                                                <th>Cửa hàng</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
                                $i = 1;
                                foreach ($_timings['kpi'] as $_key => $_timing) {
                                    $content .= sprintf("<tr>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                        </tr>",
                                        $i++,
                                        $_timing['imei'],
                                        isset( $good_cache[ $_timing['good_id'] ] ) ? $good_cache[ $_timing['good_id'] ] : '#',
                                        isset( $color_cache[ $_timing['color_id'] ] ) ? $color_cache[ $_timing['color_id'] ] : '#',
                                        $_timing['old_date'],
                                        $_timing['new_date'],
                                        isset( $store_cache[ $_timing['store_id'] ] ) ? $store_cache[ $_timing['store_id'] ] : '#'
                                    );
                                } // END foreach: danh sách IMEI bổ sung
                                $content .= "</tbody>
                                    </table>";
                            }

                            ///////////////////////////////

                            // danh sách IMEI không bổ sung
                            if (isset($_timings['no_kpi']) && count($_timings['no_kpi'])) {
                                $i = 1;
                                $content .= "<p></p><p>".$stt++." Không tính KPI cho các IMEI sau:</p>";
                                $content .= "<p></p><table class=\"table\">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>IMEI</th>
                                            <th>Model</th>
                                            <th>Màu sắc</th>
                                            <th>Ngày bán</th>
                                            <th>Cửa hàng</th>
                                            <th>Lý do</th>
                                        </tr>
                                    </thead>
                                    <tbody>";
                                foreach ($_timings['no_kpi'] as $_key => $_timing) {
                                    $content .= sprintf("<tr>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                        </tr>",
                                        $i++,
                                        $_timing['imei'],
                                        isset( $good_cache[ $_timing['good_id'] ] ) ? $good_cache[ $_timing['good_id'] ] : '#',
                                        isset( $color_cache[ $_timing['color_id'] ] ) ? $color_cache[ $_timing['color_id'] ] : '#',
                                        $_timing['old_date'],
                                        isset( $store_cache[ $_timing['store_id'] ] ) ? $store_cache[ $_timing['store_id'] ] : '#',
                                        $_timing['reason']
                                    );
                                } // END foreach: danh sách IMEI không bổ sung
                                $content .= "</tbody>
                                    </table><p></p>";
                            }

                            // danh sách IMEI sẽ xem xét bổ sung
                            if (isset($_timings['will_kpi']) && count($_timings['will_kpi'])) {
                                $i = 1;
                                $content .= "<p></p><p>".$stt++." IMEI xem xét thưởng KPI trong kỳ tính KPI tháng 7:</p>";
                                $content .= "<p></p><table class=\"table\">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>IMEI</th>
                                            <th>Model</th>
                                            <th>Màu sắc</th>
                                            <th>Ngày bán</th>
                                            <th>Cửa hàng</th>
                                            <th>Lý do</th>
                                        </tr>
                                    </thead>
                                    <tbody>";
                                foreach ($_timings['will_kpi'] as $_key => $_timing) {
                                    $content .= sprintf("<tr>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                            <td>%s</td>
                                        </tr>",
                                        $i++,
                                        $_timing['imei'],
                                        isset( $good_cache[ $_timing['good_id'] ] ) ? $good_cache[ $_timing['good_id'] ] : '#',
                                        isset( $color_cache[ $_timing['color_id'] ] ) ? $color_cache[ $_timing['color_id'] ] : '#',
                                        $_timing['old_date'],
                                        isset( $store_cache[ $_timing['store_id'] ] ) ? $store_cache[ $_timing['store_id'] ] : '#',
                                        $_timing['reason']
                                    );
                                } // END foreach: danh sách IMEI không bổ sung
                                $content .= "</tbody>
                                    </table><p></p>";
                            }

                            My_Notification::add(
                                $title,
                                $content,
                                $category_id,
                                array(
                                    'staff' => $_staff_id,
                                    'from'  => date('d/m/Y H:i:s'),
                                    'to'    => (new DateTime('now'))
                                        ->add(new DateInterval('P40D'))
                                        ->format('d/m/Y H:i:s'),
                                ),
                                1,
                                My_Notification_Type::SystemCreate
                            );

                            unset($content);
                        } // END foreach: tạo notification
                    }

                    unset($staff_list);
                    $data = array(
                        'total'   => $number_of_order,
                        'failed'  => count($error_list),
                        'succeed' => $number_of_order - count($error_list),
                        'value'   => 0,
                    );

                    // xuất file excel các order lỗi
                    if (is_array($error_list) && count($error_list) > 0) {
                        $data['error_file_name'] = 'FAILED-'.md5(microtime(true) . uniqid('', true)) . '.'.$extension;
                        // xuất excel @@
                        //
                        $objPHPExcel_out = new PHPExcel();
                        $objPHPExcel_out->createSheet();
                        $objWorksheet_out = $objPHPExcel_out->getActiveSheet();
                        //
                        $objWorksheet = $objPHPExcel->getActiveSheet();

                        $alpha = 'A';
                        $i = 1;
                        foreach ($file_title as $key => $value)
                            $objWorksheet_out->setCellValue($alpha++.$i, $value);

                        $i++;
                        // các dòng lỗi
                        $objWorksheet = $objPHPExcel->getActiveSheet();
                        foreach ($error_list as $key => $row) {
                            for ($j=0; $j <= $highestColumnIndex; $j++) {
                                if ($j == IMEI_COL)
                                    $objWorksheet_out->getCellByColumnAndRow($j, $i)->setValueExplicit($objWorksheet->getCellByColumnAndRow($j, $row['row'])->getValue(), PHPExcel_Cell_DataType::TYPE_STRING);

                                else
                                    $objWorksheet_out->setCellValueByColumnAndRow(
                                        $j, $i, $objWorksheet->getCellByColumnAndRow($j, $row['row'])->getValue()
                                    );
                            }

                            $objWorksheet_out->setCellValueByColumnAndRow($j, $i, $row['reason']);

                            $i++;
                        }
                        //
                        switch ($extension) {
                            case 'xls':
                                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel_out);
                                break;
                            case 'xlsx':
                                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel_out);
                                break;
                            default:
                                throw new Exception("Invalid file extension");
                                break;
                        }

                        $new_file_dir = $uploaded_dir . DIRECTORY_SEPARATOR . $data['error_file_name'];

                        $objWriter->save($new_file_dir);
                    }
                    // END IF // xuất file excel các order lỗi

                    // xuất file excel các order thành công
                    if (is_array($success_list) && count($success_list) > 0) {
                        $data['success_file_name'] = 'SUCCESS-'.md5(microtime(true) . uniqid('', true)) . '.'.$extension;
                        // xuất excel @@
                        //
                        $objPHPExcel_out = new PHPExcel();
                        $objPHPExcel_out->createSheet();
                        $objWorksheet_out = $objPHPExcel_out->getActiveSheet();
                        //
                        $headers = array(
                            'No.',
                            'IMEI',
                            'Date',
                            );

                        $sxx_list = array();
                        $stt = 1;
                        $objWorksheet_out->fromArray($headers, NULL, 'A1');
                        $index = 2;
                        foreach ($success_list as $key => $value) {
                            $col = "A";
                            $objWorksheet_out->setCellValue($col++.$index, $stt++);
                            $objWorksheet_out->getCell($col++.$index)->setValueExplicit($value['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
                            $objWorksheet_out->getCell($col++.$index)->setValueExplicit($value['date'], PHPExcel_Cell_DataType::TYPE_STRING);
                            $index++;
                        }

                        $objWorksheet_out->fromArray($sxx_list, NULL, 'A2');

                        switch ($extension) {
                            case 'xls':
                                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel_out);
                                break;
                            case 'xlsx':
                                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel_out);
                                break;
                            default:
                                throw new Exception("Invalid file extension");
                                break;
                        }

                        $new_file_dir = $uploaded_dir . DIRECTORY_SEPARATOR . $data['success_file_name'];

                        $objWriter->save($new_file_dir);
                    }
                    // END IF // xuất file excel các order thành công

                    $where = $QFileLog->getAdapter()->quoteInto('id = ?', $log_id);
                    $QFileLog->update($data, $where);

                    $this->view->error_list = $error_list;
                    $this->view->objWorksheet = $objWorksheet;
                    $this->view->number_of_order = $number_of_order;

                    $this->view->success_file = isset($data['success_file_name']) ? (HOST
                            . 'files'
                            . DIRECTORY_SEPARATOR . 'timing'
                            . DIRECTORY_SEPARATOR . 'kpi'
                            . DIRECTORY_SEPARATOR  . $userStorage->id
                            . DIRECTORY_SEPARATOR . $uniqid
                            . DIRECTORY_SEPARATOR . $data['success_file_name']):false;
                    $this->view->error_file = isset($data['error_file_name']) ? (HOST
                            . 'files'
                            . DIRECTORY_SEPARATOR . 'timing'
                            . DIRECTORY_SEPARATOR . 'kpi'
                            . DIRECTORY_SEPARATOR  . $userStorage->id
                            . DIRECTORY_SEPARATOR . $uniqid
                            . DIRECTORY_SEPARATOR . $data['error_file_name']):false;

                } catch (Zend_File_Transfer_Exception $e) {
                    $this->view->error =  $e->getMessage();
                } catch (Exception $e) {
                    $this->view->error =  $e->getMessage();
                }
            } // END validate IF

            // file_put_contents(APPLICATION_PATH.'/../public/files/timing/'.$userStorage->id.'/status.txt', '0');
        } // END Big IF
    }

    public function reportCustomerBuyProductAction(){
        $sql = "
            SELECT e.name as area_name, g.name as store_name, b.customer_name, b.phone_number, DATE(a.from) as date_buy, c.firstname, c.lastname, c.code 
            FROM timing a
            INNER JOIN timing_sale b ON a.id = b.id AND DATE(a.from) BETWEEN '2016-03-04' AND '2016-03-13'
            INNER JOIN staff c ON c.id = a.staff_id
            INNER JOIN regional_market d ON d.id = c.regional_market
            INNER JOIN area e ON e.id = d.area_id
            INNER JOIN store_staff_log f ON f.staff_id = c.id 
                    AND (   DATE(a.from) >= FROM_UNIXTIME(f.joined_at) 
                            AND (   DATE(a.from) <= FROM_UNIXTIME(f.released_at) OR f.released_at IS NULL )
                        )
            INNER JOIN store g ON g.id = f.store_id 
            ORDER BY e.name ASC;
        ";

        $QArea = new Applciation_Model_Area();
    }

    public function changeImageAction(){
        require_once 'tool'.DIRECTORY_SEPARATOR.'change-image.php';
    }

    private function imei_closed($imeis)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            "imei",
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
            $sheet->setCellValue($alpha++ . $index, $key);

        $index++;

        foreach ($imeis as $key => $imei) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $imei);
            $index++;
        }

        $filename = 'TGDD - IMEI closed in list - ' . date('YmdHis').'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        $objWriter->save($this->uploaded_dir.DIRECTORY_SEPARATOR.$filename);
        return $filename;
    }
	
	/* post SMS */
	
	function testSmsAction(){
			
		$this->postSMS('01687048216', 'absbjsbas');
		exit;
	}
	
    function postSMS($phone, $content){
   
        try {
			require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
			include_once 'Sms.php';

			$client = new nusoap_client("http://brandsms.vn:8018/VMGAPI.asmx?wsdl", 'wsdl', '', '', '', '');
			$err = $client->getError();
			
			if ($err) {
				echo "SMS Error";exit;
			}
			
			$sendTime     = date('d/m/Y h:i');
			$phone = preg_replace('/[^0-9]/','',$phone);

			if(!in_array(strlen($phone) , array(10,11)))
			{
				//echo "Invalid phone number";
			}

			$fphone = preg_replace('/^0/', '84', $phone);
			$fphone = preg_replace('/^\+84/', '84', $fphone);

			$result = $client->call('BulkSendSms',
				array(
					'msisdn' => $fphone,
					'alias'=> 'OPPO',
					'message' => sprintf('%s', $content),
					'sendTime'=>$sendTime,
					'authenticateUser'=>'oppo2',
					'authenticatePass'=>'vmg123456'
				),'','',''
			);

			if ($client->fault) {
				//echo "- " . $phone . " : FAULT : <br /><pre>" . $result."</pre><br />";
			} else {
				// Check for errors
				$err = $client->getError();
				if ($err) {
					// Display the error
					//echo "- " . $phone . " : ERROR : <br /><pre>" . $err."</pre><br />";

				} else {
					return ($result);
				}
			}
		}
		catch (Exception $e){
			$message = $e->getMessage();
			if (!isset($code)) {
				$code = 2;
				$message = 'Unknown error';
			}
			$rs['code'] = $code;
			$rs['message'] = $message;

		}
    }
    /* end of post SMS */

}
