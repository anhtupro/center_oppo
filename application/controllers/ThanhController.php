<?php

class ThanhController extends My_Controller_Action {

    public function indexAction() {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $list_lang = My_Lang::getLangList();
        $current_lang = $userStorage->defaut_language;

        $trans = Zend_Registry::get('translate');

        echo $trans->translate('good_morning', $list_lang[$current_lang]) . ', Pascal!';
        die;
    }

    public function changeLangDefaultAction() {
        $lang = $this->getRequest()->getParam('lang');
        $response = array('status' => 0);
        if (in_array($lang, array(1, 2, 3))) {
            $auth = Zend_Auth::getInstance();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QStaff = new Application_Model_Staff();
            $data_update = array('defaut_language' => $lang);
            $where = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
            $result = $QStaff->update($data_update, $where);
            if ($result) {
                $userStorage->defaut_language = $lang;
                $response = array('status' => 1, 'message' => 'Thành công');
                $auth->getStorage()->write($userStorage);
            }
        }
        echo json_encode($response);
        exit();
    }

    public function dingAction() {
        
//        $this->_helper->layout->disableLayout();
//        require_once("dingtalk/config.php");
//        require_once("dingtalk/util/Log.php");
//        require_once("dingtalk/util/Cache.php");
//        require_once("dingtalk/api/Auth.php");
//        require_once("dingtalk/api/User.php");
//        require_once("dingtalk/api/Message.php");
//        
//
//        $auth = new dingtalk\Auth();
//        $user = new dingtalk\User();
//        
//        $accessToken = $auth->getAccessToken();
//        
//        //$userInfo = $user->getUserInfo($accessToken, $code);
//        
//        $userInfo = $user->testsmg($accessToken);
//        
//        echo "<pre>";
//        var_dump($userInfo);exit;
        
        
        $flashMessenger = $this->_helper->flashMessenger;

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->_redirect(HOST . "trade");
        }

        $this->view->b = $this->getRequest()->getParam('b');
        $messages      = $flashMessenger->setNamespace('error')->getMessages();

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

        $this->view->messages = $messages;
        $this->_helper->layout->setLayout('login-trade');
    }

    public function dingcallbackAction() {
        
        require_once("dingtalk/config.php");
        require_once("dingtalk/util/Log.php");
        require_once("dingtalk/util/Cache.php");
        require_once("dingtalk/crypto/DingtalkCrypt.php");

        $signature = $_GET["signature"];
        $timeStamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $postdata = file_get_contents("php://input");
        $postList = json_decode($postdata, true);
        $encrypt = $postList['encrypt'];
        $msg = "";

        /**
         * TOKEN, ENCODING_AES_KEY, CORPID配置在config文件中
         */
        try {
            $crypt = new DingtalkCrypt(TOKEN, ENCODING_AES_KEY, CORPID);
            $errCode = $crypt->DecryptMsg($signature, $timeStamp, $nonce, $encrypt, $msg);
        } catch (Exception $e) {
            Log::e("DecryptMsg Exception" . $e->getMessage());
            print $e->getMessage();
            exit();
        }

        $eventMsg = json_decode($msg);
        $eventType = $eventMsg->EventType;

        switch ($eventType) {
            case "user_add_org":
                //通讯录用户增加 do something
                Log::i("【callback】:user_add_org_action");
                break;
            case "user_modify_org":
                //通讯录用户更改 do something
                Log::i("【callback】:user_modify_org_action");
                break;
            case "user_leave_org":
                //通讯录用户离职  do something
                Log::i("【callback】:user_leave_org_action");
                break;
            case "org_admin_add":
                //通讯录用户被设为管理员 do something
                Log::i("【callback】:org_admin_add_action");
                break;
            case "org_admin_remove":
                //通讯录用户被取消设置管理员 do something
                Log::i("【callback】:org_admin_remove_action");
                break;
            case "org_dept_create":
                //通讯录企业部门创建 do something
                Log::i("【callback】:org_dept_create_action");
                break;
            case "org_dept_modify":
                //通讯录企业部门修改 do something
                Log::i("【callback】:org_dept_modify_action");
                break;
            case "org_dept_remove":
                //通讯录企业部门删除 do something
                Log::i("【callback】:org_dept_remove_action");
                break;
            case "org_remove":
                //企业被解散 do something
                Log::i("【callback】:org_remove_action");
                break;


            case "check_url"://do something
            default : //do something
                break;
        }

        /*         * 对返回信息进行加密* */
        $res = "success";
        $encryptMsg = "";
        $errCode = $crypt->EncryptMsg($res, $timeStamp, $nonce, $encryptMsg);
        if ($errCode == 0) {
            echo $encryptMsg;
            Log::i("【callback】:RESPONSE: " . $encryptMsg);
        } else {
            Log::e("RESPONSE ERR: " . $errCode);
        }
    }

    public function getOapiByNameAction() {

        $this->_helper->layout->disableLayout();
        require_once("dingtalk//config.php");
        require_once("dingtalk/util/Log.php");
        require_once("dingtalk/util/Cache.php");
        require_once("dingtalk/api/Auth.php");
        require_once("dingtalk/api/User.php");
        require_once("dingtalk/api/Message.php");

        $auth = new dingtalk\Auth();
        $user = new dingtalk\User();
        $message = new Message();

        $event = $_REQUEST["event"];
        switch ($event) {
            case '':
                echo json_encode(array("error_code" => "4000"));
                break;
            case 'getuserid':
                $accessToken = $auth->getAccessToken();
                $code = $_POST["code"];
                $userInfo = $user->getUserInfo($accessToken, $code);
                $response = array('status' => 0, 'data' => array());
                dingtalk\Log::i("[USERINFO-getuserid]" . json_encode($userInfo));
                if (!empty($userInfo)) {
                    
                }
                if ($userInfo->errcode == 0 && !empty($userInfo->userid)) {

                    $user_id = $userInfo->userid;
                    $db = Zend_Registry::get('db');
                    $auth = Zend_Auth::getInstance();
                    $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                    $authAdapter->setTableName('staff')
                            ->setIdentityColumn('email')
                            ->setCredentialColumn('password');
                    $select = $db->select()
                            ->from(array('p' => 'staff'), array('p.*'));
                    $select->where('p.dingtalk_id = ?', $user_id);
                    $resultStaff = $db->fetchRow($select);

                    if (!empty($resultStaff)) {
                        $md5_pass = $resultStaff['password'];
                        $authAdapter->setIdentity($resultStaff['email']);
                        $authAdapter->setCredential($md5_pass);
                        $result = $auth->authenticate($authAdapter);
                        if ($result->isValid()) {
                            $data = $authAdapter->getResultRowObject(null, 'password');
//                            echo '<pre>';
//                            print_r($data);
//                            die;
                            $QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();

                            $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

                            $priviledge = $QStaffPriveledge->fetchRow($where);

                            $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

                            $QRegionalMarket = new Application_Model_RegionalMarket();
                            $area_id = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
                            $data->regional_market_id = $area_id;
                            $QTeam = new Application_Model_Team();
                            $title = $QTeam->find($data->title)->current();
                            $QGroup = new Application_Model_GroupTrade();
                            $group = $QGroup->find($title->access_group)->current();
                            $data->group_id = $title->access_group;
                            $data->role = $group->name;

                            if ($personal_accesses) {
                                // Thanh edit merge   
                                $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                                //$menu           = array_unique(array_merge(explode(',', $group->menu), explode(',', $personal_accesses['menu'])));
                                $menu = explode(',', $personal_accesses['menu']);
                            } else {

                                $data->accesses = json_decode($group->access);

                                $menu = $group->menu ? explode(',', $group->menu) : null;
                            }


                            $QMenu = new Application_Model_MenuTrade();
                            $where = array();
                            $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

                            if ($menu)
                                $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
                            else
                                $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

                            $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

                            $data->menu = $menus;
                            $auth->getStorage()->write($data);
                            if (!empty($resultStaff)) {
                                $response['status'] = 1;
                                $response['data'] = $resultStaff;
                            }
                        }
                    }
                }

                echo json_encode($response, JSON_UNESCAPED_SLASHES);
                break;

            case 'get_userinfo':
                $accessToken = $auth->getAccessToken();
                $userid = $_POST["userid"];
                $userInfo = $user->get($accessToken, $userid);
                dingtalk\Log::i("[get_userinfo]" . json_encode($userInfo));
                echo json_encode($userInfo, true);
                break;
            case 'jsapi-oauth':
                $href = $_GET["href"];
                $configs = $auth->getConfig($href);
                $configs['errcode'] = 0;
                echo json_encode($configs, JSON_UNESCAPED_SLASHES);
                break;
        }
        header('Content-Type: application/json');
        exit();
    }
    
    public function checkLoginDingAction(){
        
        $result = [
            'check' => 0,
            'message' => 'OK'
        ];
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            
            $this->_helper->viewRenderer->setNoRender(true);
            $this->_helper->layout->disableLayout();

            $code = $_POST['code'];

            require_once("dingtalk/config.php");
            require_once("dingtalk/util/Log.php");
            require_once("dingtalk/util/Cache.php");
            require_once("dingtalk/api/Auth.php");
            require_once("dingtalk/api/User.php");
            require_once("dingtalk/api/Message.php");


            $auth = new dingtalk\Auth();
            $user = new dingtalk\User();

            $accessToken = $auth->getAccessToken();
            $userInfo = $user->getUserInfo($accessToken, $code);
            
            

            $data_user = (array)$userInfo;
            $userid = $data_user['userid'];

            $QStaff = new Application_Model_Staff();


            $where = $QStaff->getAdapter()->quoteInto('dingtalk_id = ?', $userid);
            $staff = $QStaff->fetchRow($where);

            if(!empty($staff)){
                
                //LOGIN
                $db    = Zend_Registry::get('db');
                $auth_login    = Zend_Auth::getInstance();
                
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('staff')
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('password');

                $md5_pass = $staff['password'];
                
                $authAdapter->setIdentity($staff['email']);

                $select      = $db->select()
                        ->from(array('p' => 'staff'), array('p.*'));
                $select->where('p.email = ?', $staff['email']);
                $resultStaff = $db->fetchRow($select);


                $authAdapter->setCredential($md5_pass);
                $result = $auth_login->authenticate($authAdapter);
                $data = $authAdapter->getResultRowObject(null, 'password');

                if ($data->off_date or $data->status != 1) {
                    throw new Exception("This account was disabled!".$data->status);
                }

                //get personal access
                $QStaffPriveledge = new Application_Model_StaffPriviledgeTrade();

                $where = $QStaffPriveledge->getAdapter()->quoteInto('staff_id = ?', $data->id);

                $priviledge = $QStaffPriveledge->fetchRow($where);

                $personal_accesses = (isset($priviledge) and $priviledge) ? $priviledge : null;

                $QRegionalMarket          = new Application_Model_RegionalMarket();
                $area_id                  = $QRegionalMarket->find($data->regional_market)[0]['area_id'];
                $data->regional_market_id = $area_id;
                $QTeam                    = new Application_Model_Team();
                $title                    = $QTeam->find($data->title)->current();
                $QGroup                   = new Application_Model_GroupTrade();

                $group      = $QGroup->find($title->access_group)->current();
                $data->group_id=$title->access_group;
                $data->role = $group->name;
                
                if ($personal_accesses) {
                    // Thanh edit merge   
                    $data->accesses = array_unique(array_merge(json_decode($group->access), json_decode($personal_accesses['access'])));
                    $menu = explode(',', $personal_accesses['menu']);
                } else {

                    $data->accesses = json_decode($group->access);

                    $menu = $group->menu ? explode(',', $group->menu) : null;
                }


                $QMenu   = new Application_Model_MenuTrade();
                $where   = array();
                $where[] = $QMenu->getAdapter()->quoteInto('group_id = ?', 1);

                if ($menu)
                    $where[] = $QMenu->getAdapter()->quoteInto('id IN (?)', $menu);
                else
                    $where[] = $QMenu->getAdapter()->quoteInto('1 = ?', 0);

                $menus = $QMenu->fetchAll($where, array('parent_id', 'position'));

                $data->menu = $menus;

                $auth_login->getStorage()->write($data);

                $QLogSystemTrade = new Application_Model_LogSystemTrade();
                $action_link = $this->getRequest()->getActionName();

                $log_system_trade = [
                    'staff_id' => $data->id,
                    'action'   => $action_link,
                    'created_at' => date('Y-m-d H:i:s')
                ];
                $QLogSystemTrade->insert($log_system_trade);


                
                $result = [
                    'check' => 1,
                    'userid'  => $userid,
                    'message' => 'Dingtalk Exits',
                ];
            }
            else{
                $result = [
                    'check' => 2,
                    'userid'  => $userid,
                    'message' => 'Dingtalk Not Exits'
                ];
            }
            
            
            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            $message = $e->getMessage();
            
            $result = [
                'check' => 3,
                'userid'  => $userid,
                'message' => $message
            ];
            
        }
        
        
        echo json_encode($result);exit;
    }

}
