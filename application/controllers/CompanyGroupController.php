<?php

    class CompanyGroupController extends My_Controller_Action
    {
        public function indexAction()
        {

        }

        public function createAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $staff_id = $this->getRequest()->getParam('staff_id');
            $company_group = $this->getRequest()->getParam('company_group');
            $submit = $this->getRequest()->getParam('submit', 0);

            $QCompanyGroup = new Application_Model_CompanyGroup();
            if(!empty($submit))
            {
                $params = array(
                    'staff_id' => $staff_id,
                    'company_group' => $company_group,
                );
                $QCompanyGroup->insertStaff($params);
                $flashMessenger->setNamespace('success')->addMessage('Done.');
                $this->_redirect("/company-group/list-staff");
            }

            $listCompanyGroup = $QCompanyGroup->getAll();
            $this->view->listCompanyGroup = $listCompanyGroup;
        }

        public function setCompanyGroupAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $company_group = $this->getRequest()->getParam('company_group');
            $id = $this->getRequest()->getParam('id');
            $title = $this->getRequest()->getParam('title');
            $submit = $this->getRequest()->getParam('submit');

            $QCompanyGroup = new Application_Model_CompanyGroup();
            if(!empty($submit))
            {
                $params = array(
                    'company_group' => $company_group,
                    'title' => $title
                );

                $QCompanyGroup->setCompanyGroup($params);
                $flashMessenger->setNamespace('success')->addMessage('Done.');
                $this->_redirect("/company-group/list");
            }

            if(!empty($id))
            {
                $params_edit = array(
                    "id" => $id,
                );

                $data = $QCompanyGroup->getTitle($params_edit);
                $this->view->params = $data['data'][0];
            }

            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
            
            $listCompanyGroup = $QCompanyGroup->getAll();
            $this->view->listCompanyGroup = $listCompanyGroup;
        }

        public function createTeamAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $department = $this->getRequest()->getParam('department', null);
            $team = $this->getRequest()->getParam('team', null);
            $title = $this->getRequest()->getParam('title', null);
            $company_group = $this->getRequest()->getParam('company_group', null);
            $submit = $this->getRequest()->getParam('submit', 0);

            $QCompanyGroup = new Application_Model_CompanyGroup();
            if(!empty($submit) && !empty($department))
            {
                $params = array(
                    'company_group' => $company_group,
                    'department' => $department,
                    'team' => $team,
                    'title' => $title,
                );
                $QCompanyGroup->insertTeam($params);
                $flashMessenger->setNamespace('success')->addMessage('Done.');
                $this->_redirect("/company-group/list-staff");
            }

            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
            
            $listCompanyGroup = $QCompanyGroup->getAll();
            $this->view->listCompanyGroup = $listCompanyGroup;
        }

        public function removeAction()
        {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $flashMessenger = $this->_helper->flashMessenger;
            $staff_id = $this->getRequest()->getParam('id');
            if(!empty($staff_id))
            {
                $QCompanyGroup = new Application_Model_CompanyGroup();
                $QCompanyGroup->removeTitle($staff_id);
                $flashMessenger->setNamespace('success')->addMessage('Done');
            }
            $this->_redirect("/company-group/list");
        }

        
        public function listStaffAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $name = $this->getRequest()->getParam('name');
            $code = $this->getRequest()->getParam('code');
            $department = $this->getRequest()->getParam('department', null);
            $team = $this->getRequest()->getParam('team', null);
            $title = $this->getRequest()->getParam('title', null);
            $company_group = $this->getRequest()->getParam('company_group', null);
            $page = $this->getRequest()->getParam('page', 1);


            $params = array(
                'name' => $name,
                'code' => $code,
                'department' => $department,
                'team' => $team,
                'title' => $title,
                'company_group' => $company_group,
                'limit' => 30,
            );

            $params['offset'] = ($page - 1) * $params['limit'];

            
            $QCompanyGroup = new Application_Model_CompanyGroup();
            $data = $QCompanyGroup->getStaff($params) ;

            $this->view->data = $data['data'];

            $this->view->params = $params;
            $this->view->total = $data['total'];
            $this->view->offset = $params['offset'];
            $this->view->limit = $params['limit'];
            $this->view->url = $this->view->url = HOST . 'company-group/list-staff' . ($params ? '?' . http_build_query($params) . '&' : '?');

            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

            $listCompanyGroup = $QCompanyGroup->getAll();
            $this->view->listCompanyGroup = $listCompanyGroup;
            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }

        public function listAction()
        {
            $QCompanyGroup = new Application_Model_CompanyGroup();

            $flashMessenger = $this->_helper->flashMessenger;
            $name = $this->getRequest()->getParam('name');
            $department = $this->getRequest()->getParam('department', null);
            $team = $this->getRequest()->getParam('team', null);
            $title = $this->getRequest()->getParam('title', null);
            $company_group = $this->getRequest()->getParam('company_group', null);
            $export = $this->getRequest()->getParam('export', 0);

            if(!empty($export))
            {
                require_once 'PHPExcel.php';

                $alphaExcel = new My_AlphaExcel();
                $PHPExcel = new PHPExcel();

                $PHPExcel->setActiveSheetIndex(0);
                $sheet = $PHPExcel->getActiveSheet();

                $heads = array(
                    $alphaExcel->ShowAndUp() => 'Department',
                    $alphaExcel->ShowAndUp() => 'Team',
                    $alphaExcel->ShowAndUp() => 'Title',
                    $alphaExcel->ShowAndUp() => 'Group',
                    $alphaExcel->ShowAndUp() => 'Policy Group'
                );

                foreach($heads as $key => $value)
                {
                    $sheet->setCellValue($key.'1', $value);
                }

                $sql = "SELECT 
                        t3.name as `department_name`,
                        t2.name as `team_name`,
                        t.name as `title_name`,
                        cg2.name as `group`,
                        cg.name as `policy_group`
                    FROM `company_group_map` cgm
                    JOIN `company_group` cg ON cgm.company_group = cg.id
                    JOIN `company_group` cg2 ON cg.parent = cg2.id
                    JOIN `team` t ON cgm.title = t.id
                    JOIN `team` t2 ON t2.id = t.parent_id
                    JOIN `team` t3 ON t3.id = t2.parent_id";

                $db = Zend_Registry::get('db');
                $stmt = $db->prepare($sql);
                $stmt->execute();

                $data_export = $stmt->fetchAll();

                foreach($data_export as $key => $value)
                {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['department_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['team_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['title_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['group']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['policy_group']);
                }

                $filename = 'Policy group - ' . date('Y-m-d H-i-s');
                $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
                // $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
                
                
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                $objWriter->save('php://output');
                exit;

            }

            $params = array(
                'department' => $department,
                'team' => $team,
                'title' => $title,
                'company_group' => $company_group,
                'limit' => null,
            );

            $params['offset'] = ($page - 1) * $params['limit'];
            
            $QCompanyGroup = new Application_Model_CompanyGroup();
            $data = $QCompanyGroup->getAllTitle($params) ;
            $this->view->data = $data;
            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }

        public function listTitleAction()
        {
            $flashMessenger = $this->_helper->flashMessenger;
            $name = $this->getRequest()->getParam('name');
            $department = $this->getRequest()->getParam('department', null);
            $team = $this->getRequest()->getParam('team', null);
            $title = $this->getRequest()->getParam('title', null);
            $company_group = $this->getRequest()->getParam('company_group', null);
            $page = $this->getRequest()->getParam('page', 1);


            $params = array(
                'department' => $department,
                'team' => $team,
                'title' => $title,
                'company_group' => $company_group,
                'limit' => 30,
            );

            $params['offset'] = ($page - 1) * $params['limit'];
            
            $QCompanyGroup = new Application_Model_CompanyGroup();
            $data = $QCompanyGroup->getTitle($params) ;

            $this->view->data = $data['data'];

            $this->view->params = $params;
            $this->view->total = $data['total'];
            $this->view->offset = $params['offset'];
            $this->view->limit = $params['limit'];
            $this->view->url = $this->view->url = HOST . 'company-group/list-title' . ($params ? '?' . http_build_query($params) . '&' : '?');

            $QTeam = new Application_Model_Team();
            $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
            $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

            $listCompanyGroup = $QCompanyGroup->getAll();
            $this->view->listCompanyGroup = $listCompanyGroup;
            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }
    }