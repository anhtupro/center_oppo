<?php
/**
 * Xuất các report cho HR, dành cho view ở local
 */
class HrReportController extends My_Controller_Action
{
    /**
     * Giao diện chính; chọn thời gian để xuất các loại báo cáo
     */
    public function indexAction()
    {
        $from_date = $this->getRequest()->getParam('from_date', date('01/m/Y'));
        $to_date   = $this->getRequest()->getParam('to_date', date('d/m/Y'));
        $export    = $this->getRequest()->getParam('export');


        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'      => $from,
            'to'        => $to,
        );

        $this->view->params = $params;

        if ( isset( $export ) && intval( $export ) > 0 ) {
            if ( $export == 1 ) {
                My_Report_Hr::kpiPg($from, $to);
                exit;
            }

            if ( $export == 2 ) {
                My_Report_Hr::kpiSale($from, $to);
                exit;
            }

            if ( $export == 3 ) {
                My_Report_Hr::kpiLeader($from, $to);
                exit;
            }
        }
    }


    public function pgKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);
        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export )){
            My_Report_Hr::kpiPg($from, $to, $params);
        }
    }
	
	public function pgKpiAccessoriesAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);

        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export )){
            My_Report_Hr::kpiPgAccessories($from, $to, $params);
        }
    }

    public function saleKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);

        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiSale($from, $to, $params);
    }

     public function pbSaleKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 1);

        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
            'export'          => $export,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiPbSale($from, $to, $params);
    }

    public function leaderKpiAction()
    {
        $from_date       = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date         = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export          = $this->getRequest()->getParam('export', 0);
        $name            = $this->getRequest()->getParam('name');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $district        = $this->getRequest()->getParam('district');
        $store           = $this->getRequest()->getParam('store');
        $distributor_id  = $this->getRequest()->getParam('distributor_id');

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from'            => $from,
            'to'              => $to,
            'name'            => $name,
            'area'            => $area,
            'regional_market' => $regional_market,
            'district'        => $district,
            'store'           => $store,
            'distributor_id'  => $distributor_id,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiLeader($from, $to, $params);
    }

    public function accessoriesKpiAction()
    {
        $from_date = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to_date   = $this->getRequest()->getParam('to', date('d/m/Y'));
        $export    = $this->getRequest()->getParam('export', 0);

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0] . " 23:59:59";

        $params = array(
            'from' => $from,
            'to'   => $to,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiAccessories($from, $to);
    }

    public function timingAction()
    {
        $from_date = $this->getRequest()->getParam('from_date', date('01/m/Y'));
        $to_date   = $this->getRequest()->getParam('to_date', date('d/m/Y'));
        $export    = $this->getRequest()->getParam('export', 0);

        $d = explode('/', $from_date);
        $from = $d[2].'-'.$d[1].'-'.$d[0];

        $d = explode('/', $to_date);
        $to = $d[2].'-'.$d[1].'-'.$d[0];

        $params = array(
            'from_date' => $from_date,
            'to_date'   => $to_date,
            'from'      => $from,
            'to'        => $to,
        );

        $this->view->params = $params;

        if (isset( $export ) && intval( $export ))
            My_Report_Hr::kpiPg($from, $to);
    }

    /**
     * Dùng để gán các hằng số dùng tính KPI;
     *		-- bảng KPI của sales
     * 		giá trị từng biến có thể thay đổi bởi HR;
     * 		các biến mới chỉ Technology tạo thêm được
     *		-- riêng bên product thì dùng chung bảng (thêm cột pg_kpi)
     *		chỉ HR thấy các số KPI, còn bên khác chỉ thấy/sửa giá, tên sản phẩm...
     */
    public function settingsAction()
    {
        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
        if (empty($from)) $from = date('01/m/Y');

        $to = $this->getRequest()->getParam('to', date('d/m/Y'));
        if (empty($to)) $to = date('d/m/Y');

        $type   = $this->getRequest()->getParam('type');
        $cat   = $this->getRequest()->getParam('cat');
        $name   = $this->getRequest()->getParam('name');
        $page   = $this->getRequest()->getParam('page', 1);
        $limit  = LIMITATION;
        $total  = 0;
        $params = array('from' => $from, 'to' => $to, 'type' => $type, 'cat' => $cat, 'name' => $name);

    	$QGoogKpiLog = new Application_Model_GoodKpiLog();
        $this->view->logs = $QGoogKpiLog->fetchByMonth($page, $limit, $total, $params);
        $QGood = new Application_Model_Good();
        $this->view->goods = $QGood->get_cache();
        $this->view->good_all = $QGood->get_cache_all(null);
        $QGoodColor = new Application_Model_GoodColor();
        $this->view->good_colors = $QGoodColor->get_cache();
        $QGoodCat = new Application_Model_GoodCategory();
        $this->view->cat = $QGoodCat->get_cache();

        $this->view->params = $params;
        
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST . 'hr-report/settings' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset = $limit * ($page - 1);

    	$flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
   		$this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
    }

    public function settingsCreateAction()
    {
        $QGood                   = new Application_Model_Good();
        $this->view->goods       = $QGood->get_cache();
        $QGoodCat                = new Application_Model_GoodCategory();
        $this->view->cat         = $QGoodCat->get_cache();
        $QGoodColor              = new Application_Model_GoodColor();
        $this->view->good_colors = $QGoodColor->get_cache();

        $this->view->good_all = $QGood->get_cache_all(null);

        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();

        $this->view->areas = $areas;

        $this->view->refer       = My_Url::refer('hr-report/settings');

        $flashMessenger = $this->_helper->flashMessenger;
        $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
    }
    
    public function loadProductAction()
    {
        $cat_id = $this->getRequest()->getParam('cat_id');
        $QGood = new Application_Model_Good();

        if ($cat_id) {
            $where = array();
            $where[] = $QGood->getAdapter()->quoteInto('cat_id = ?',    $cat_id);
            $where[] = $QGood->getAdapter()->quoteInto('del = ?',       0);
            $goods = $QGood->fetchAll($where, 'name');

            echo json_encode(array('goods' => $goods->toArray()));
            exit;

        }
        else{
            echo json_encode(array());
            exit;
        }
    }

    public function settingsEditAction()
    {
        $id = $this->getRequest()->getParam('id');
        $refer = My_Url::refer('hr-report/settings');
        $db =  Zend_Registry::get('db');
        try {
            if (!$id) throw new Exception("Invalid ID", 1);

            $id = intval($id);
            $QGoodKpiLog = new Application_Model_GoodKpiLog();
            $where       = $QGoodKpiLog->getAdapter()->quoteInto('id = ?', $id);
            $log_check   = $QGoodKpiLog->fetchRow($where);

            if (!$log_check) throw new Exception("Wrong ID", 2);
            
            $QGoodLog = new Application_Model_Good();
            $where    = $QGoodLog->getAdapter()->quoteInto('id = ?', $log_check->good_id);
            $good     = $QGoodLog->fetchRow($where);
            
            $this->view->log_cat = $good->cat_id;
            $this->view->log     = $log_check;
            $this->view->refer   = $refer;

            $QGood = new Application_Model_Good();
            $this->view->goods = $QGood->get_cache();
            $this->view->good_all = $QGood->get_cache_all($good->cat_id);
            
            $QGoodCat = new Application_Model_GoodCategory();
            $this->view->cat = $QGoodCat->get_cache();
            
            $QGoodColor = new Application_Model_GoodColor();
            $this->view->good_colors = $QGoodColor->get_cache();

            $QArea = new Application_Model_Area();
            $areas = $QArea->get_cache();
            $this->view->areas = $areas;

            $QDistributor = new Application_Model_Distributor();
            if($log_check['distributor_id']){

                //$arr_distr         = explode(',',$log_check['distributor_id']);
                $arr_distr[]         = $log_check['distributor_id'];
                $params['list_id'] = $arr_distr;
                $total             = 0;

                $arr_distributors             = $QDistributor->fetchPagination(1,0,$total,$params);
                $this->view->arr_distributors = $arr_distributors;
            }


            $this->_helper->viewRenderer->setRender('settings-create');
        } catch (Exception $e) {
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage(sprintf("[%s] %s", $e->getCode(), $e->getMessage()));
            $this->_redirect($refer);
        }
    }


    public function settingsSaveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if($this->getRequest()->isPost()){
            $good_id             = $this->getRequest()->getParam('good_id');
            $color_id            = $this->getRequest()->getParam('color_id');
            $from                = $this->getRequest()->getParam('from');
            $to                  = $this->getRequest()->getParam('to');
            $kpi                 = $this->getRequest()->getParam('kpi', 0);
            $type                = $this->getRequest()->getParam('type');
            $id                  = $this->getRequest()->getParam('id');
            $refer               = HOST.'hr-report/settings';
            $distributor_id_text = $this->getRequest()->getParam('distributor_id_text');
            $area_id             = $this->getRequest()->getParam('area_id');
            $flashMessenger      = $this->_helper->flashMessenger;
            $QGoodKpiLog         = new Application_Model_GoodKpiLog();
            $db                  = Zend_Registry::get('db');

            $db->beginTransaction();
            try {
                if ($id) {
                    $where = $QGoodKpiLog->getAdapter()->quoteInto('id = ?', intval($id));
                    $log_check = $QGoodKpiLog->fetchRow($where);
                    if (!$log_check) throw new Exception("Invalid log", 7);
                }

                if (!$type || !isset(My_Kpi_Object::$name[$type])) throw new Exception("Invalid KPI For", 8);

                if (!$from || !date_create_from_format('d/m/Y', $from))
                    throw new Exception("Invalid From date", 1);

                $from = date_create_from_format('d/m/Y', $from)->format('Y-m-d');

                if ($to && !date_create_from_format('d/m/Y', $to)){
                    throw new Exception("Invalid To date", 2);
                }

                if ($to){
                    $to = date_create_from_format('d/m/Y', $to)->format('Y-m-d');
                }

                if (!$good_id) throw new Exception("Invalid product", 3);
            

                if ($color_id) {
                    $QGoodColor = new Application_Model_GoodColor();
                    $colors = $QGoodColor->get_cache();
                    if (!isset($colors[ $color_id ])) throw new Exception("Invalid color", 5);
                }

                $where        = array();
                $where_date   = array();
                $where[]      = $QGoodKpiLog->getAdapter()->quoteInto('good_id = ?', intval($good_id));
                $where[]      = $QGoodKpiLog->getAdapter()->quoteInto('type = ?', intval($type));
                $where_date[] = $QGoodKpiLog->getAdapter()->quoteInto('from_date <= ? AND to_date >= ?', $from);

                if ($to){
                    $where_date[] = $QGoodKpiLog->getAdapter()->quoteInto('from_date <= ? AND to_date >= ?', $to);
                }

                $where[] = implode(' OR ', $where_date);

                if ($color_id){
                    $where[] = $QGoodKpiLog->getAdapter()->quoteInto('color_id = ?', intval($color_id));
                }

                if ($id){
                    $where[] = $QGoodKpiLog->getAdapter()->quoteInto('id <> ?', intval($id));
                }

                $log_check = $QGoodKpiLog->fetchRow($where);

                if ($log_check) throw new Exception("Conflict dates", 6);

                $data = array(
                    'good_id'        => intval($good_id),
                    'color_id'       => intval($color_id),
                    'from_date'      => $from,
                    'to_date'        => empty($to) ? null : $to,
                    'kpi'            => floatval( $kpi ),
                    'type'           => intval( $type ),
                    'area_id'        => $area_id,
                    'distributor_id' => intval($distributor_id_text),
                );

                $result = $QGoodKpiLog->save($data,$id);
                if($result['code'] !== 0){
                    throw new Exception($result['message'],$result['code']);
                }

                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage($result['message']);
                My_Message::redirect($refer,0);

            } catch (Exception $e) {
                $db->rollBack();
                $message = sprintf("[%s] %s", $e->getCode(), $e->getMessage());
                My_Message::jsAlert($message,'error');
            
            }
        }
        exit; 
    }

    public function settingsDeleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $QGoodKpiLog = new Application_Model_GoodKpiLog();
        $where = $QGoodKpiLog->getAdapter()->quoteInto('id = ?', intval($id));
        $QGoodKpiLog->delete($where);
        $this->_redirect(My_Url::refer('hr-report/settings'));
    }

    public function saveSettingsAction()
    {
    	if ($this->getRequest()->getMethod() == 'POST') {
    		// update cho PG
            $flashMessenger = $this->_helper->flashMessenger;

    		$QGood = new Application_Model_Good();
            $goods = $QGood->get_cache();

            $QGoodKpi = new Application_Model_GoodKpi();

            foreach ($goods as $id => $desc) {
                $pg_kpi     = $this->getRequest()->getParam('p_'.$id, 0);
                $sales_kpi  = $this->getRequest()->getParam('s_'.$id, 0);
                $leader_kpi = $this->getRequest()->getParam('l_'.$id, 0);
                $pg_kpi     = intval($pg_kpi);
                $sales_kpi  = intval($sales_kpi);
                $leader_kpi = intval($leader_kpi);

                $data = array(
                    'pg_kpi'     => $pg_kpi,
                    'sales_kpi'  => $sales_kpi,
                    'leader_kpi' => $leader_kpi,
                );

                $where = $QGoodKpi->getAdapter()->quoteInto('good_id = ?', $id);
                $it = $QGoodKpi->fetchRow($where);

                try {
                    if ($it){
                    $QGoodKpi->update($data, $where);
                    } else {
                        $data['good_id'] = $id;
                        $QGoodKpi->insert($data);
                    }
                } catch (Exception $e) {
                    $flashMessenger->setNamespace('error')->addMessage(sprintf("[%s] %s", $e->getCode(), $e->getMessage()));
                    $this->_redirect(HOST.'hr-report/settings');
                }
    		}

       		$messages = $flashMessenger->setNamespace('success')->addMessage('Done!');
    	}

    	$this->_redirect(HOST.'hr-report/settings');
    }

    private function _error($fm){
        $fm->setNamespace('error')->addMessage('Error');
        $this->_redirect('/hr-report/new-settings-create');
    }

    public function newSettingsCreateAction(){
        $fm = $this->_helper->FlashMessenger();
        if($this->_request->isPost()){
            $params = $this->_request->getParams();
            $params = $this->validate_data($params);
            if($params){
                $data = array();
                foreach($params['group'] as $key => $value){
                    if($value['kpi']){
                        if(!empty($value['area'])){
                            foreach($value['area'] as $k => $v){
                                if(!empty($value['distributor'])){
                                    foreach($value['distributor'] as $kd => $vd){
                                        $data[] = '('.
                                            $params['product_id'].', '.
                                            (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                                            $value['kpi'].', '.
                                            $this->get_kpi_type($params['team']).', '.
                                            '"'.$params['from'].'", '.
                                            ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                                            '"'.date('Y-m-d H:i:s').'", '.
                                            $v.', '.
                                            $vd
                                        .')';
                                    }
                                }
                                else{
                                    $data[] = '('.
                                        $params['product_id'].', '.
                                        (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                                        $value['kpi'].', '.
                                        $this->get_kpi_type($params['team']).', '.
                                        '"'.$params['from'].'", '.
                                        ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                                        '"'.date('Y-m-d H:i:s').'", '.
                                        $v.', '.
                                        0
                                    .')';
                                }
                            }
                        }
                        else{
                            if(!empty($value['distributor'])){
                                foreach($value['distributor'] as $kd => $vd){
                                    $data[] = '('.
                                        $params['product_id'].', '.
                                        (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                                        $value['kpi'].', '.
                                        $this->get_kpi_type($params['team']).', '.
                                        '"'.$params['from'].'", '.
                                        ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                                        '"'.date('Y-m-d H:i:s').'", '.
                                        '0, '.
                                        $vd
                                    .')';
                                }
                            }
                            else{
                                $data[] = '('.
                                    $params['product_id'].', '.
                                    (!empty($params['color_id']) ? $params['color_id'] : 0).', '.
                                    $value['kpi'].', '.
                                    $this->get_kpi_type($params['team']).', '.
                                    '"'.$params['from'].'", '.
                                    ($params['to'] ? '"'.$params['to'].'"' : 'null').', '.
                                    '"'.date('Y-m-d H:i:s').'", 0, 0)';
                            }
                        }
                    }
                }
                $value = implode(',', $data);
                // $value = implode('<br>', $data);
                // echo '<pre>';
                // echo($value);die;
                if($value){
                    $res = (new Application_Model_GoodKpiLog())->Insert($value);
                    if($res){
                        $fm->setNamespace('success')->addMessage('Success');
                        $this->_redirect('/hr-report/settings');
                    }
                }
                else $this->_error($fm);
            }
            else $this->_error($fm);
        }

        $QProduct = new Application_Model_Good();
        $QArea = new Application_Model_Area();
        $team = new Application_Model_Team();
        
        $this->view->listProduct = $QProduct->get_cache();
        $this->view->areas = $QArea->get_cache();
        $this->view->team = $team->getSpecialist(implode(',', array(PGPB_TITLE, SALES_TITLE, LEADER_TITLE, PB_SALES_TITLE)));
        $this->view->message = $fm;
        // var_dump($fm->setNamespace('error')->hasMessages());die;
    }

    public function ajaxKpiSettingAction()
    {
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
        	$this->_helper->layout()->disableLayout();
        
        	$product_id = $this->_request->getParam('product_id');
            $QGoodColor = new Application_Model_GoodColor();
            $GoodColor = $QGoodColor->getById($product_id);
        
        	if(!empty($GoodColor)){
                $this->view->GoodColor = $GoodColor;
        	}
    
        }
    }
    
    public function getFilterDistributorAction(){
        if($this->_request->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $this->_helper->layout()->disableLayout();
            $search = trim(addslashes($this->_request->getParam('search')));
            $data = (new Application_Model_Distributor())->getFilter($search);
            if(!empty($data)){
                $this->view->data = $data;
            }
        }
    }

    private function get_kpi_type($id){
        $array = array(
            182 => 1,
            183 => 2,
            190 => 3,
            312 => 4
        );
        return $array[$id];
    }

    private function validate_data($params){
        if(
            !$params['product_id'] ||
            !$params['team'] ||
            !$params['from']
        )
            return false;
        return $params;
    }
}