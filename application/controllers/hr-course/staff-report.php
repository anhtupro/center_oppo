<?php
$sort = $this->getRequest()->getParam("sort", "");
$desc = $this->getRequest()->getParam("desc", 1);
$staff_code     = $this->getRequest()->getParam("staff_code", array());
$staff_name     = $this->getRequest()->getParam("staff_name", NULL);
$department_id  = $this->getRequest()->getParam("department_id", array());

$staff_code  = preg_replace("/(^[\r\n]*|[\r\n]+)[\r\t]*[\s\n]+/", "\n", $staff_code);
$staff_code  = explode("\n", $staff_code);
$staff_code  = array_unique(array_filter($staff_code));

$QHrCourseStaff = new Application_Model_HrCourseStaff();
$QTeam          = new Application_Model_Team();

$params["sort"] = $sort;
$params["desc"] = $desc;

$params = array(
    'staff_code'    => $staff_code,
    'staff_name'    => $staff_name,
    'department_id' => $department_id,
);

$page = $this->getRequest()->getParam("page", 1);
$limit = LIMITATION;
$total = 0;
$result = $QHrCourseStaff->fetchPagination($page, $limit, $total, $params);


if(!empty($staff_code)):
    $staffCodeList = '';
    foreach ($staff_code as $code):
        $staffCodeList .= $code . "\n";
    endforeach;
    $this->view->staffCodeList = $staffCodeList;
endif;

$department                                 = $QTeam->get_list_department();
$recursiveDeparmentTeamTitle                = $QTeam->get_recursive_cache();
$this->view->department                     = $department;
$this->view->recursiveDeparmentTeamTitle    = $recursiveDeparmentTeamTitle;
$this->view->current_course                 = $QHrCourseStaff->getCurrentCourse();
$this->view->list = $result;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->url = HOST . "/hr-course/staff-report" . ( $params ? "?" . http_build_query($params) . "&" : "?" );
$this->view->offset = $limit * ($page - 1);

