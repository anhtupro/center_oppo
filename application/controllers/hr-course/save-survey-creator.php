<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$course_id          = $this->getRequest()->getParam('course_id', 0);
$form               = $this->getRequest()->getParam('form', 0);
$data_answer        = $this->getRequest()->getParam('data_answer', array());
$data_check         = $this->getRequest()->getParam('data_check', array());
$question           = $this->getRequest()->getParam('question', array());
$question_choice    = $this->getRequest()->getParam('question_choice', array());
// lọc bỏ những giá trị NULL
$question           = array_unique(array_filter($question));
$question_choice    = array_unique(array_filter($question_choice));

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {
    $QHrCourseSurvey        = new Application_Model_HrCourseSurvey();
    $QHrCourseSurveyAnswer  = new Application_Model_HrCourseSurveyAnswer();
    if (empty($course_id) || empty($form)) {
        echo json_encode([
            'status' => 0,
            'message' => 'Bạn chưa chọn một khóa học cụ thể',
        ]);
        return;
    }


    //Lấy câu hỏi trắc nghiệm
    $whereSurveyCheck       = [];
    $whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $course_id);
    $whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
    $whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 0);
    $question_              = $QHrCourseSurvey->fetchAll($whereSurveyCheck);
    //Lấy câu hỏi tự luận
    $whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $course_id);
    $whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 1);
    $whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
    $question2_             = $QHrCourseSurvey->fetchAll($whereSurveyQues);

    if(!empty($question_)):
        $QHrCourseSurvey->update(array('del' => '1'), $whereSurveyCheck);
        $id_ans = array();
        foreach($question_ as $_nkey => $_nvalue):
            $whereAns               = [];
            $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('survey_id = ?', $_nvalue['id']);
            $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('del = ?', 0);
            $QHrCourseSurveyAnswer->update(array('del' => 1), $whereAns);
        endforeach;
    endif;

    if(!empty($question2_)):
        $QHrCourseSurvey->update(array('del' => 1), $whereSurveyQues);
    endif;

    foreach($question_choice as $_key => $_value):
        if(trim($_value) != ''):
            $data = array(
                'content'       => $_value,
                'course_id'     => $course_id,
                'form'          => $form,
                'created_by'    => $userStorage->id,
                'created_from'  => date('Y-m-d H:i:s'),
                'type'          => 0,
                'del'           => 0
            );
            $idSurvey = $QHrCourseSurvey->insert($data);

            $dataCheck = json_decode($data_check[$_key],true);
            foreach(json_decode($data_answer[$_key],true) as $key => $value):
                $true = is_null($dataCheck[$key]) ? 0 : $dataCheck[$key];
                if(trim($value) != ''):
                    $dataAnswer = array(
                        'title'     => $value,
                        'survey_id' => $idSurvey,
                        'true'      => $true,
                        'del'       => 0
                    );
                    $QHrCourseSurveyAnswer->insert($dataAnswer);
                endif;
            endforeach;
        endif;
    endforeach;

    //save câu hỏi tự luận
    foreach($question as $_key => $_value):
        if(trim($_value) != ''):
            $data = array(
                'content'       => $_value,
                'course_id'     => $course_id,
                'form'          => $form,
                'created_by'    => $userStorage->id,
                'created_from'  => date('Y-m-d H:i:s'),
                'type'          => 1,
                'del'           => 0
            );
            $QHrCourseSurvey->insert($data);
        endif;
    endforeach;

    $db->commit();
    echo json_encode([
        'status' => 1,
    ]);
    return;
}catch (Exception $e) {
    $db->rollBack();
    echo json_encode([
        'status' => 0,
        'message' => $e->getMessage(),
    ]);
    return;
}
exit;
