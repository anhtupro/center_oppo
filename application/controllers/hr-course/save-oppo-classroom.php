<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$step                   = $this->getRequest()->getParam('step', 0);
$course_id              = $this->getRequest()->getParam('course_id', 0);
$answer_check           = $this->getRequest()->getParam('answer_check', array());
$answer_scores          = $this->getRequest()->getParam('answer_scores', array());
$answer_content         = $this->getRequest()->getParam('answer_content', array());
$survey_question1_id    = $this->getRequest()->getParam('survey_question1_id', array());
$survey_question2_id    = $this->getRequest()->getParam('survey_question2_id', array());

$improvement           = $this->getRequest()->getParam('improvement', array());
$expectation           = $this->getRequest()->getParam('expectation', array());
$review_process        = $this->getRequest()->getParam('review_process', array());
$review_result         = $this->getRequest()->getParam('review_result', array());
$is_update             = $this->getRequest()->getParam('is_update', 0);

$db = Zend_Registry::get('db');
$db->beginTransaction();
try {
    $QHrCourseStaffFlow             = new Application_Model_HrCourseStaffFlow();
    $QHrCourseSurveyDetails         = new Application_Model_HrCourseSurveyDetails();
    $QHrCourseSurveyScores          = new Application_Model_HrCourseSurveyScores();
    $QHrCourseActionPlan            = new Application_Model_HrCourseActionPlan();
    $QHrCourseActionPlanDetails     = new Application_Model_HrCourseActionPlanDetails();

    if(in_array($step, [1, 2])):
        foreach ($survey_question1_id as $key_1 => $value_1):
            foreach(json_decode($answer_check[$key_1],true) as $_key1 => $_value1):
                $QHrCourseSurveyDetails->insert(array(
                    'staff_id'      => $userStorage->id,
                    'course_id'     => $course_id,
                    'survey_id'     => $value_1,
                    'form'          => $step,
                    'answer_check'  => $_value1,
                ));
            endforeach;
        endforeach;

        foreach ($survey_question2_id as $key_2 => $value_2):
            $QHrCourseSurveyDetails->insert(array(
                'staff_id'      => $userStorage->id,
                'course_id'     => $course_id,
                'survey_id'     => $value_2,
                'form'          => $step,
                'answer_content'  => $answer_content[$key_2]
            ));
        endforeach;

        if($step == '2'):
            $QHrCourseSurveyScores->insert(array(
                'staff_id'      => $userStorage->id,
                'course_id'     => $course_id,
                'score'         => array_sum($answer_scores),
                'survey_lenght' => count($answer_scores)
            ));
        endif;

        $QHrCourseStaffFlow->insert(array(
            'staff_id'      => $userStorage->id,
            'course_id'     => $course_id,
            'step'          => $step,
            'created_at'    => date('Y-m-d H:i:s')
        ));
    elseif ($step == 3):
        if($is_update):
            $whereCourseActionPlan = [];
            $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('course_id = ?', $course_id);
            $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('del = ?', 0);
            $QHrCourseActionPlan->update(array('del' => 1), $whereCourseActionPlan);
        else:
            $QHrCourseStaffFlow->insert(array(
                'staff_id'      => $userStorage->id,
                'course_id'     => $course_id,
                'step'          => $step,
                'created_at'    => date('Y-m-d H:i:s')
            ));
        endif;

        $action_plan_id = $QHrCourseActionPlan->insert(
            array(
                'staff_id'      => $userStorage->id,
                'course_id'     => $course_id,
                'created_at'    => date('Y-m-d H:i:s'),
                'del'           => '0'
            )
        );
        for($i = 0; $i < count($improvement); $i++):
            $QHrCourseActionPlanDetails->insert(
                array(
                    'action_plan_id'    => $action_plan_id,
                    'improvement'       => $improvement[$i],
                    'expectation'       => $expectation[$i],
                    'review_process'    => $review_process[$i],
                    'review_result'     => $review_result[$i]
                )
            );
        endfor;
    endif;
    $db->commit();
    echo json_encode(['status' => 1]);
    return;
}catch (Exception $e) {
    $db->rollBack();
    echo json_encode([
        'status' => 0,
        'message' => $e->getMessage(),
    ]);
    return;
}
