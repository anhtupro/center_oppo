<?php
$form               = $this->getRequest()->getParam("form");
$course_id          = $this->getRequest()->getParam("course_id");
$staff_id           = $this->getRequest()->getParam("staff_id", 0);

$sql        = 'SELECT CONCAT(firstname, " ",  lastname) AS "fullname" FROM staff s WHERE s.id =  "' . $staff_id . '"';
PC::debug($sql);
$db         = Zend_Registry::get('db');
$stmt       = $db->query($sql);
$fullname   = $stmt->fetchAll();
$staff_name = $fullname[0]['fullname'];
$this->view->staff_name = $staff_name;

$QHrCourseSurvey            = new Application_Model_HrCourseSurvey();
$QHrCourseSurveyAnswer      = new Application_Model_HrCourseSurveyAnswer();
$QHrCourseSurveyDetails     = new Application_Model_HrCourseSurveyDetails();
$QHrCourseSurveyScores      = new Application_Model_HrCourseSurveyScores();
$QHrCourseActionPlan        = new Application_Model_HrCourseActionPlan();
$QHrCourseActionPlanDetails = new Application_Model_HrCourseActionPlanDetails();

if(in_array($form, [1, 2])):
    //Lấy câu hỏi trắc nghiệm
    $whereSurveyCheck       = [];
    $whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $course_id);
    $whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
    $whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 0);
    $whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('del = ?', 0);
    $question               = $QHrCourseSurvey->fetchAll($whereSurveyCheck);

    //Lấy câu hỏi tự luận
    $whereSurveyQues        = [];
    $whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $course_id);
    $whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 1);
    $whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
    $whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('del = ?', 0);
    $question2              = $QHrCourseSurvey->fetchAll($whereSurveyQues);

    $answer = array();
    foreach($question as $_key => $_value):
        $whereAns               = [];
        $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('survey_id = ?', $_value['id']);
        $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('del = ?', 0);
        $answer["$_key"]        = $QHrCourseSurveyAnswer->fetchAll($whereAns);
    endforeach;

    $params = array();
    $params['staff_id']         = $staff_id;
    $params['course_id']        = $course_id;
    $answer_info                = $QHrCourseSurveyDetails->getAnswerData($params, 1);

    $ans = array();
    foreach ($answer_info as $row):
        $ans[$row['survey_id']][] = $row['answer_check'];
    endforeach;

    $answer_content             = $QHrCourseSurveyDetails->getAnswerData($params, 2);

    $whereCourseSurveyScores   = [];
    $whereCourseSurveyScores[] = $QHrCourseSurveyScores->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    $whereCourseSurveyScores[] = $QHrCourseSurveyScores->getAdapter()->quoteInto('course_id = ?', $course_id);
    $scores_info               = $QHrCourseSurveyScores->fetchRow($whereCourseSurveyScores);

    $this->view->scores_info        = $scores_info;
    $this->view->question           = $question;
    $this->view->question2          = $question2;
    $this->view->answer             = $answer;
    $this->view->answer_info        = $ans;
    $this->view->answer_content      = $answer_content;
else:
    $whereCourseActionPlan = [];
    $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('course_id = ?', $course_id);
    $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('del = ?', 0);
    $action_plan                = $QHrCourseActionPlan->fetchRow($whereCourseActionPlan);

    $whereCourseActionPlanDetails   = NULL;
    $whereCourseActionPlanDetails   = $QHrCourseActionPlanDetails->getAdapter()->quoteInto('action_plan_id = ?', $action_plan['id']);
    $action_plan_details            = $QHrCourseActionPlanDetails->fetchAll($whereCourseActionPlanDetails);
    $this->view->action_plan_details = $action_plan_details;
endif;

if($form == 1):
    $btn_title  = 'Khảo sát trước khóa học'; $color = "text-primary";
elseif ($form == 2):
    $btn_title = 'Khảo sát sau khóa học'; $color = "text-danger";
elseif ($form == 3):
    $btn_title = 'Xây dựng Action Plan'; $color = "text-success";
endif;

$this->view->course_id       = $course_id;
$this->view->form            = $form;
$this->view->color           = $color;
$this->view->btn_title       = $btn_title;
$this->_helper->layout()->disablelayout(true);