<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger         = $this->_helper->flashMessenger;

$id                     = $this->getRequest()->getParam('id');
$name                   = $this->getRequest()->getParam('name');
$content                = $this->getRequest()->getParam('content');
$note                   = $this->getRequest()->getParam('note');
$from_date              = $this->getRequest()->getParam('from_date');
$to_date                = $this->getRequest()->getParam('to_date');
$type                   = $this->getRequest()->getParam('type');
$lecturer               = $this->getRequest()->getParam('lecturer');
$student_type           = $this->getRequest()->getParam('student_type');
$file_delete            = $this->getRequest()->getParam('file_delete');
$rating_value           = $this->getRequest()->getParam('rating_value');
$link                   = $this->getRequest()->getParam('link', NULL);

$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$QHrCourse              = new Application_Model_HrCourse();
$db = Zend_Registry::get('db');

try {
    $db->beginTransaction();
    $data = [
        'name'          => $name,
        'content'       => $content,
        'note'          => $note,
        'from_date'     => $from_date,
        'to_date'       => $to_date,
        'type'          => $type,
        'lecturer'      => $lecturer,
        'student_type'  => $student_type,
        'created_at'    => date('Y-m-d H:i:s'),
        'created_by'    => $userStorage->id,
        'rating_value'  => $rating_value
    ];

    if($type == 2){ $data['link'] = NULL; }
    elseif ($type == 1){

        $is_youtube = 'www.youtube.com/';
        $pos        = strpos($link, $is_youtube);
        if(!empty($pos)){
            $embed_youtube = str_replace('watch?v=', 'embed/', $link);
            $data['link'] = $embed_youtube;
        }else{
            $data['link'] = $link;
        }
    }

    if(empty($id)){
        $id = $QHrCourse->insert($data);
    }
    else{
        $where = NULL;
        $where = $QHrCourse->getAdapter()->quoteInto("id = ?", $id);
        $QHrCourse->update($data, $where);
    }
    
    //Upload file
    $filename_wallpaper = $_FILES['file']['name'];
    $filename_document  = $_FILES['document']['name'];

    if(!empty($filename_wallpaper) ){

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                        DIRECTORY_SEPARATOR . 'hr-course' . DIRECTORY_SEPARATOR . $id;
        
        if(!is_dir($uploaded_dir)){
            @mkdir($uploaded_dir, 0777, true);
        }

        $ext_arr = array('jpg','png','JPG','PNG','jpeg','JPEG');
	    $ext1 = pathinfo($filename_wallpaper, PATHINFO_EXTENSION);
        if(!in_array($ext1, $ext_arr)){

            $flashMessenger->setNamespace('error')->addMessage("File không đúng định dạng");
            $this->redirect(HOST . '/hr-course/list-course');

        }
        $name= "Pic_" . $id . "_" . time() . "." . $ext1;

        $file = $uploaded_dir . DIRECTORY_SEPARATOR . $name;
        @move_uploaded_file($_FILES['file']['tmp_name'], $file);

        $where = NULL;
        $where = $QHrCourse->getAdapter()->quoteInto("id = ?", $id);
        $QHrCourse->update(['file' => $name], $where);
    }
    //END Upload file

    if(!empty($file_delete) and $file_delete == 1){
        $where = $QHrCourse->getAdapter()->quoteInto("id = ?", $id);
        $QHrCourse->update(['document' => NULL], $where);
    }

    if(!empty($filename_document) ){

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
            DIRECTORY_SEPARATOR . 'hr-course'. DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . $id;

        if(!is_dir($uploaded_dir)){
            @mkdir($uploaded_dir, 0777, true);
        }

        $ext_arr = array('docx', 'doc', 'png', 'jpg', 'pdf', 'ppt', 'pptx', 'xlsx', 'xls', 'jpeg', 'PNG', 'JPEG', 'JPG');
        $ext1 = pathinfo($filename_document, PATHINFO_EXTENSION);
        if(!in_array($ext1, $ext_arr)){
            $flashMessenger->setNamespace('error')->addMessage("File không đúng định dạng");
            $this->redirect(HOST . '/hr-course/list-course');
        }
        $doc_name = "Document_" . $id . "_" . time() . "." . $ext1;
        $file = $uploaded_dir . DIRECTORY_SEPARATOR . $doc_name;
        @move_uploaded_file($_FILES['document']['tmp_name'], $file);
        $where = NULL;
        $where = $QHrCourse->getAdapter()->quoteInto("id = ?", $id);
        $QHrCourse->update(['document' => $doc_name], $where);
    }
    
    $db->commit();
    $flashMessenger->setNamespace('success')->addMessage('Hoàn thành!');
    $this->redirect(HOST . '/hr-course/list-course');
    
} catch (Exception $e) {

    $db->rollback();
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->redirect(HOST . '/hr-course/list-course');
    
}


