<?php 
$sort = $this->getRequest()->getParam("sort", "");
$desc = $this->getRequest()->getParam("desc", 1);

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$name           = $this->getRequest()->getParam("name");
$min_number     = $this->getRequest()->getParam("min_number");
$max_number     = $this->getRequest()->getParam("max_number");
$student_type   = $this->getRequest()->getParam("student_type");
$to_date        = $this->getRequest()->getParam("to_date");
$from_date      = $this->getRequest()->getParam("from_date");
$export         = $this->getRequest()->getParam('export');
if($min_number >= $max_number):
    $min_number = $max_number = NULL;
endif;

$params = array(
    'name'          => trim($name),
    'min_number'    => $min_number,
    'max_number'    => $max_number,
    'student_type'  => $student_type,
    'to_date'       => $to_date,
    'from_date'     => $from_date
);

$params["sort"] = $sort;
$params["desc"] = $desc;


$QHrCourse = new Application_Model_HrCourse();
$QHrCourseRatingStars = new Application_Model_HrCourseRatingStars();
$page = $this->getRequest()->getParam("page", 1);
$limit = LIMITATION;
$total = 0;
$result = $QHrCourse->fetchPagination($page, $limit, $total, $params);

$db         = Zend_Registry::get('db');
$get_stars  = $db->fetchAll('SELECT AVG(star) AS "re", course_id FROM hr_course_rating_stars GROUP BY course_id');
$star_score = array();
foreach ($get_stars as $k => $v):
    $star_score[$v['course_id']] = $v['re'];
endforeach;

$this->view->star_score = $star_score;
$this->view->list = $result;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->url = HOST . "/hr-course/list-course" . ( $params ? "?" . http_build_query($params) . "&" : "?" );
$this->view->offset = $limit * ($page - 1);

if (!empty($flashMessenger->setNamespace('error')->getMessages())) {
    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;
}
if (!empty($flashMessenger->setNamespace('success')->getMessages())) {
    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;
}

if(isset($export) and $export == 1)
{
    set_time_limit( 0 );
    error_reporting( 0 );
    ini_set('display_error', 0);
    ini_set('memory_limit', -1);

    $name = "Report List Course".date('Ymd').".csv";
    $fp   = fopen('php://output', 'w');

    header('Content-Type: text/csv; charset=utf-8');
    echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
    header('Content-Disposition: attachment; filename='.$name);

    $heads = array(
        'Khóa học',
        'Đối tượng',
        'Ngày bắt đầu',
        'Ngày kết thúc',
        'Số lượng học viên',
        'Loại khóa học'
    );
    fputcsv($fp, $heads);
    foreach($result as $row)
    {
        $write  = array(
            (isset($row['name']) and $row['name']) ? $row['name'] : NULL,
            (isset($row['student_type']) and $row['student_type'] == 1) ? 'Trưởng bộ phận' : 'Nhân viên',
            (isset($row['from_date']) and $row['from_date']) ? $row['from_date'] : NULL,
            (isset($row['to_date']) and $row['to_date']) ? $row['to_date'] : NULL,
            (isset($row['number_of_staff']) and $row['number_of_staff']) ? $row['number_of_staff'] : 0,
            (isset($row['type']) and $row['type'] == 1) ? 'Online' : 'Office'
        );
        fputcsv($fp, $write);
    }
    fclose($fp);
    exit;
}