<?php
$id     = $this->getRequest()->getParam("id");
$form   = $this->getRequest()->getParam("form");

$QHrCourse              = new Application_Model_HrCourse();
$where = NULL;
$where = $QHrCourse->getAdapter()->quoteInto('id = ?', $id);
$course = $QHrCourse->fetchRow($where);

$QHrCourseSurvey        = new Application_Model_HrCourseSurvey();
//Lấy câu hỏi trắc nghiệm
$whereSurveyCheck       = [];
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $id);
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 0);
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('del = ?', 0);
$question               = $QHrCourseSurvey->fetchAll($whereSurveyCheck);
//Lấy câu hỏi tự luận
$whereSurveyQues        = [];
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $id);
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 1);
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('del = ?', 0);
$question2              = $QHrCourseSurvey->fetchAll($whereSurveyQues);

$answer = array();
foreach($question as $_key => $_value):
    $QHrCourseSurveyAnswer  = new Application_Model_HrCourseSurveyAnswer();
    $whereAns               = [];
    $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('survey_id = ?', $_value['id']);
    $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('del = ?', 0);
    $answer["$_key"]        = $QHrCourseSurveyAnswer->fetchAll($whereAns);
endforeach;

$params['id'] = $id;

if(!empty($form)):
    if($form == '1'):
        $title = "Mẫu khảo sát trước khóa học";
        $color = "text-primary";
    elseif ($form == '2'):
        $title = "Mẫu khảo sát sau khóa học";
        $color = "text-danger";
    elseif ($form == '3'):
        $title = "Mẫu Action Plan của khóa học";
        $color = "text-success";
    else:
        exit;
    endif;
    $this->view->form   = $form;
    $this->view->title  = $title;
    $this->view->color = $color;
endif;

$this->view->course     = $course;
$this->view->params     = $params;
$this->view->question   = $question;
$this->view->question2  = $question2;
$this->view->answer     = $answer;