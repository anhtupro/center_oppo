<?php
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$id             = $this->getRequest()->getParam("id");
$staff_id       = $this->getRequest()->getParam("staff_id");
$export         = $this->getRequest()->getParam('export');
$QStaff                 = new Application_Model_Staff();
$QHrCourse              = new Application_Model_HrCourse();
$QHrCourseStaff         = new Application_Model_HrCourseStaff();
$QHrCourseStaffFlow     = new Application_Model_HrCourseStaffFlow();
$QTeam                  = new Application_Model_Team();
$QHrCourseSurveyScores  = new Application_Model_HrCourseSurveyScores();
$params = [
    'course_id' => $id
];
$where = NULL;
$where = $QHrCourse->getAdapter()->quoteInto('id = ?', $id);
$course = $QHrCourse->fetchRow($where);

$whereScores = NULL;
$whereScores = $QHrCourseSurveyScores->getAdapter()->quoteInto('course_id = ?', $id);
$scores      = $QHrCourseSurveyScores->fetchAll($whereScores);

$array_scores = array();
foreach($scores as $kc => $vc):
    $array_scores[$vc['staff_id']] = ($vc['score']/$vc['survey_lenght'])*10;
endforeach;

$where_   = [];
$where_[] = $QHrCourseStaff->getAdapter()->quoteInto('course_id = ?', $id);
$where_[] = $QHrCourseStaff->getAdapter()->quoteInto('del = ?', 0);
$listStaffTemp = $QHrCourseStaff->fetchAll($where_, array('staff_id'));

if(!empty($listStaffTemp)):
    $listStaff = [];
    foreach ($listStaffTemp as $staff):
        $listStaff[] = $staff['staff_id'];
    endforeach;
    $recursiveDeparmentTeamTitle                = $QTeam->get_recursive_cache();
    $get_all_staff                              = $QStaff->getStaffByArray(array('staff_id' => $listStaff));
    $this->view->recursiveDeparmentTeamTitle    = $recursiveDeparmentTeamTitle;
    $this->view->listStaff                      = $get_all_staff;
    $staffCodeList = '';
    foreach ($get_all_staff as $code):
        $staffCodeList .= $code['staff_code'] . "\n";
    endforeach;
    $this->view->staffCodeList = $staffCodeList;
    $whereCourseStaffFlow   = NULL;
    $whereCourseStaffFlow   = $QHrCourseStaffFlow->getAdapter()->quoteInto('course_id = ?', $id);
    $data_flow              = $QHrCourseStaffFlow->fetchAll($whereCourseStaffFlow);
    $list_success           = array();
    $list_step              = array();
    foreach ($data_flow as $flow):
        $list_success[$flow['staff_id']][]  = $flow['step'];
        $list_step[]                        = $flow['step'];
    endforeach;

    $this->view->list_step    = array_unique($list_step);
    $this->view->list_success = $list_success;

    if(isset($export) and $export == 1)
    {
        set_time_limit( 0 );
        error_reporting( 0 );
        ini_set('display_error', 0);
        ini_set('memory_limit', -1);

        $name = "Report Staff Course".date('Ymd').".csv";
        $fp   = fopen('php://output', 'w');

        header('Content-Type: text/csv; charset=utf-8');
        echo chr(239) . chr(187) . chr(191); // UTF-8 BOM
        header('Content-Disposition: attachment; filename='.$name);

        $heads = array(
            'Mã nhân viên',
            'Phòng ban',
            'Tên nhân viên',
            'Chức danh',
            'Khảo sát trước khóa học',
            'Khảo sát sau khóa học',
            'Action Plan'
        );
        fputcsv($fp, $heads);
        foreach($get_all_staff as $row)
        {
            $write  = array(
                (isset($row['staff_code']) and $row['staff_code']) ? $row['staff_code'] : NULL,
                (isset($recursiveDeparmentTeamTitle) and $recursiveDeparmentTeamTitle[$row['department']]['name']) ? $recursiveDeparmentTeamTitle[$row['department']]['name'] : NULL,
                (isset($row['full_name']) and $row['full_name']) ? $row['full_name'] : NULL,
                (isset($row['position']) and $row['position']) ? $row['position'] : NULL,
                (isset($list_success) and in_array(1, $list_success[$row['staff_id']])) ? 'Đã làm' : 'Chưa làm',
                (isset($list_success) and in_array(2, $list_success[$row['staff_id']])) ? $array_scores[$row['staff_id']] : 'Chưa làm',
                (isset($list_success) and in_array(3, $list_success[$row['staff_id']])) ? 'Đã làm' : 'Chưa làm',
            );
            fputcsv($fp, $write);
        }
        fclose($fp);
        exit;
    }

endif;

$staff_course       = $QHrCourse->getListStaffCourse($params);
$this->view->course = $course;
$this->view->params = $params;

if($this->getRequest()->isPost()) {
    $db = Zend_Registry::get('db');

    try {
        $db->beginTransaction();
        $whereCourseStaff = $QHrCourseStaff->getAdapter()->quoteInto('course_id = ?', $id);
        $dataCheck        = $QHrCourseStaff->fetchAll($whereCourseStaff);
        if(count($dataCheck) > 0):
            $QHrCourseStaff->update(array(
                'del'       => 1,
            ), $whereCourseStaff);
        endif;
        for($i = 0; $i < count($staff_id); $i++):
            $QHrCourseStaff->insert(array(
                'course_id'  => $id,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'staff_id'   => $staff_id[$i],
                'del'        => 0
            ));
        endfor;
        $db->commit();
        $this->redirect(HOST . '/hr-course/staff-course?id='.$id);
    } catch (Exception $e) {
       $db->rollback();
        $this->redirect(HOST . '/hr-course/staff-course?id='.$id);
    }
}

    

    

