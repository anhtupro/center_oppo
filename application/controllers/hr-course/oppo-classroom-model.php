<?php 
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$course_id      = $this->getRequest()->getParam("course_id", 0);
$form           = $this->getRequest()->getParam("form", 0);
$step           = $this->getRequest()->getParam("step", 0);

$QHrCourse                  = new Application_Model_HrCourse();
$QHrCourseSurvey            = new Application_Model_HrCourseSurvey();
$QHrCourseSurveyAnswer      = new Application_Model_HrCourseSurveyAnswer();
$QHrCourseSurveyDetails     = new Application_Model_HrCourseSurveyDetails();
$QHrCourseSurveyScores      = new Application_Model_HrCourseSurveyScores();
$QHrCourseActionPlan        = new Application_Model_HrCourseActionPlan();
$QHrCourseActionPlanDetails = new Application_Model_HrCourseActionPlanDetails();

$params = array();
$params['course_id'] = $course_id;
$params['step']      = $form;

$whereCourse            = NULL;
$whereCourse            = $QHrCourse->getAdapter()->quoteInto('id = ?', $course_id);
$course                 = $QHrCourse->fetchRow($whereCourse)->toArray();
//Lấy câu hỏi trắc nghiệm
$whereSurveyCheck       = [];
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $course_id);
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 0);
$whereSurveyCheck[]     = $QHrCourseSurvey->getAdapter()->quoteInto('del = ?', 0);
$question               = $QHrCourseSurvey->fetchAll($whereSurveyCheck);

//Lấy câu hỏi tự luận
$whereSurveyQues        = [];
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('course_id = ?', $course_id);
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('type = ?', 1);
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('form = ?', $form);
$whereSurveyQues[]      = $QHrCourseSurvey->getAdapter()->quoteInto('del = ?', 0);
$question2              = $QHrCourseSurvey->fetchAll($whereSurveyQues);

$answer = array();
foreach($question as $_key => $_value):
    $whereAns               = [];
    $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('survey_id = ?', $_value['id']);
    $whereAns[]             = $QHrCourseSurveyAnswer->getAdapter()->quoteInto('del = ?', 0);
    $answer["$_key"]        = $QHrCourseSurveyAnswer->fetchAll($whereAns);
endforeach;

if(!empty($step) and $step == 2):

    $params['staff_id']         = $userStorage->id;
    $params['course_id']        = $course_id;
    $answer_info                = $QHrCourseSurveyDetails->getAnswerData($params, 1);

    $ans = array();
    foreach ($answer_info as $row):
        $ans[$row['survey_id']][] = $row['answer_check'];
    endforeach;

    $answer_content             = $QHrCourseSurveyDetails->getAnswerData($params, 2);

    $whereCourseActionPlan      = [];
    $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
    $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('course_id = ?', $course_id);
    $whereCourseActionPlan[]    = $QHrCourseActionPlan->getAdapter()->quoteInto('del = ?', 0);
    $action_plan                = $QHrCourseActionPlan->fetchRow($whereCourseActionPlan);

    $whereCourseActionPlanDetails   = NULL;
    $whereCourseActionPlanDetails   = $QHrCourseActionPlanDetails->getAdapter()->quoteInto('action_plan_id = ?', $action_plan['id']);
    $action_plan_details            = $QHrCourseActionPlanDetails->fetchAll($whereCourseActionPlanDetails);

    $whereCourseSurveyScores        = [];
    $whereCourseSurveyScores[]      = $QHrCourseSurveyScores->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
    $whereCourseSurveyScores[]      = $QHrCourseSurveyScores->getAdapter()->quoteInto('course_id = ?', $course_id);
    $scores_info                    = $QHrCourseSurveyScores->fetchRow($whereCourseSurveyScores);

    $this->view->scores_info         = $scores_info;
    $this->view->action_plan_details = $action_plan_details;
    $this->view->answer_info         = $ans;
    $this->view->answer_content      = $answer_content;
endif;
$this->view->course             = $course;
$this->view->question           = $question;
$this->view->question2          = $question2;
$this->view->answer             = $answer;
$this->view->form               = $form;
$this->view->params             = $params;

if($step == 1):
    if($form == 1):
        $this->view->title          = 'Khảo sát trước khóa học';
        $this->view->color          = 'text-primary';
        $this->_helper->viewRenderer->setRender('partials/oppo-classroom-survey');
    elseif ($form == '2'):
        $this->view->title          = 'Khảo sát sau khóa học';
        $this->view->color          = 'text-danger';
        $this->_helper->viewRenderer->setRender('partials/oppo-classroom-survey');
    elseif ($form == '3'):
        $this->_helper->viewRenderer->setRender('partials/oppo-classroom-action-plan');
    endif;
elseif($step == 2):
    if($form == 1):
        $this->view->title          = 'Khảo sát trước khóa học';
        $this->view->color          = 'text-primary';
    elseif ($form == '2'):
        $this->view->title          = 'Khảo sát sau khóa học';
        $this->view->color          = 'text-danger';
    endif;
    $this->_helper->viewRenderer->setRender('partials/oppo-classroom-result');
endif;

$this->_helper->layout()->disablelayout(true);