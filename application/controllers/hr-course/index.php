<?php
// @ nhhthong
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$QCapacity              = new Application_Model_Capacity();
$QCapacityPlan          = new Application_Model_CapacityPlan();
$QHrCourse              = new Application_Model_HrCourse();
$QStaffCapacity         = new Application_Model_StaffCapacity();
$QCapacityDictionary    = new Application_Model_CapacityDictionary();
$QCapacityRecommend     = new Application_Model_CapacityRecommend();

$plan_year      = $QCapacityPlan->fetchAll(['is_year = 1']);

$params = array(
    'staff_id'      => $userStorage->id,
    'department_id' => $userStorage->department
);
$total = 0;
$params['level'] = 3;
$params['export'] = 1;
$result = $QCapacity->fetchPaginationResult(NULL, NULL, $total, $params);

foreach($result as $key=>$value){
    $list_staff[] = $value['staff_id'];
    $data['is_staff_appraisal'][$value['plan_id']][$value['is_staff_appraisal']] = $value['staff_capacity_id'];
    $data['is_head_appraisal'][$value['plan_id']][$value['is_head_appraisal']] = $value['staff_capacity_id'];
}

$capacity_id = '';
$recomment_id = '';

foreach ($plan_year as $ky => $vy):
    if($data['is_head_appraisal'][$vy['id']][1]):
        $capacity_id  = $data['is_head_appraisal'][$vy['id']][1];
        $recomment_id = $data['is_staff_appraisal'][$vy['id']][1];
    endif;
endforeach;

if($capacity_id && $recomment_id):
    $capacity_info              = $QStaffCapacity->getInfoCapacity($capacity_id);
    $staff                      = $QCapacity->getInfoStaff($capacity_info['staff_id']);
    $dictionary_list            = $QStaffCapacity->getDictionaryStaff($capacity_id);
    $science_list               = $QStaffCapacity->getScienceStaff($capacity_id);
    $dictionary_science         = $QCapacity->getDictionaryScience();
    $directory_recommed         = $QCapacityRecommend->getRecommend($capacity_id);
    $dictionary_list_group      = $QStaffCapacity->getDictionaryStaffGroup($capacity_id);
    $directory_recommed_staff   = $QCapacityRecommend->getRecommendStaff($recomment_id);
endif;

$hr_course                      = $QHrCourse->getListCourse($userStorage->id, 0);
$list_hr_course = array();
foreach ($hr_course as $k => $c):
    $list_hr_course[$c['type']][] = [
        'id'        => $c['id'],
        'name'      => $c['name'],
        'file'      => $c['file'],
        'from_date' => $c['from_date']
    ];
endforeach;
$this->view->hr_course = $list_hr_course;

$science_name = [];
foreach($dictionary_science as $key=>$value){
    $science_name[$value['level']] = $value['title'];
}

$total_point = 0;
$total_share = 0;
foreach($dictionary_list as $key=>$value){
    $total_share = $total_share + $value['share'];
}

foreach($science_list as $key=>$value){
    $total_share = $total_share + $value['share'];
}

$dictionary_chart = [];
foreach($dictionary_list_group as $key=>$value){
    $dictionary_chart[$value['type']][] = [
        'dictionary_id' => $value['dictionary_id'],
        'title_name'    => trim($value['title_name']),
        'level'         => $value['level'],
        'appraisal_head_level'  => $value['appraisal_head_level'],
        'appraisal_staff_level' => $value['appraisal_staff_level']
    ];
}

foreach($dictionary_list as $key=>$value){
    $total_point = $total_point + (($value['appraisal_head_level']/$value['level'])*$value['share']);
}


$total_level = 0;
$total_appraisal_head_level = 0;
$total_appraisal_staff_level = 0;
foreach($science_list as $key=>$value){
    $total_level                    = $total_level + $value['level'];
    $total_appraisal_head_level     = $total_appraisal_head_level + $value['appraisal_head_level'];
    $total_appraisal_staff_level    = $total_appraisal_staff_level + $value['appraisal_staff_level'];
    $total_point = $total_point + ($value['appraisal_head_level']/$value['level'])*$value['share'];
}

$science_chart = [
    'title_name'            => 'Kiến thức chuyên môn',
    'level'                 => (int)($total_level/count($science_list)),
    'appraisal_head_level'  => (int)($total_appraisal_head_level/count($science_list)),
    'appraisal_staff_level' => (int)($total_appraisal_staff_level/count($science_list)),
];


$all_dictionary = $QCapacityDictionary->getDictionaryLevel();

$dictionary = [];
foreach($all_dictionary as $key=>$value){
    $dictionary[$value['id']][] = $value;
}

$where_capacity     = [];
$where_capacity[]   = $QStaffCapacity->getAdapter()->quoteInto('staff_id = ?', $capacity_info['staff_id']);
$where_capacity[]   = $QStaffCapacity->getAdapter()->quoteInto('plan_id = ?', $capacity_info['plan_id']);
$where_capacity[]   = $QStaffCapacity->getAdapter()->quoteInto('is_staff_appraisal = 1');
$where_capacity[]   = $QStaffCapacity->getAdapter()->quoteInto('is_deleted = 0');
$staff_capacity     = $QStaffCapacity->fetchRow($where_capacity);

if($staff_capacity){
    $dictionary_list_group_staff = $QStaffCapacity->getDictionaryStaffGroup($staff_capacity['id']);
    $dictionary_chart_staff = [];
    foreach($dictionary_list_group_staff as $key=>$value){
        $dictionary_chart_staff[$value['type']][$value['dictionary_id']] = [
            'title_name'            => trim($value['title_name']),
            'level'                 => $value['level'],
            'appraisal_staff_level' => $value['appraisal_staff_level']
        ];
    }

    $science_list_staff = $QStaffCapacity->getScienceStaff($staff_capacity['id']);

    $total_level = 0;
    $total_appraisal_head_level = 0;
    $total_appraisal_staff_level = 0;
    foreach($science_list_staff as $key=>$value){
        $total_level = $total_level + $value['level'];
        $total_appraisal_head_level     = $total_appraisal_head_level + $value['appraisal_head_level'];
        $total_appraisal_staff_level    = $total_appraisal_staff_level + $value['appraisal_staff_level'];

        $total_point = $total_point + ($value['appraisal_head_level']/$value['level'])*$value['share'];
    }

    $science_chart_staff = [
        'title_name'            => 'Kiến thức chuyên môn',
        'level'                 => (int)($total_level/count($science_list_staff)),
        'appraisal_head_level'  => (int)($total_appraisal_head_level/count($science_list_staff)),
        'appraisal_staff_level' => (int)($total_appraisal_staff_level/count($science_list_staff)),
    ];
    $this->view->dictionary_chart_staff = $dictionary_chart_staff;
    $this->view->science_chart_staff = $science_chart_staff;
}
//END - Kết quả nhân viên tự đánh giá
$this->view->capacity_info = $capacity_info;
$this->view->staff = $staff;
$this->view->dictionary_list = $dictionary_list;
$this->view->science_list = $science_list;
$this->view->science_chart = $science_chart;
$this->view->total_point = $total_point/$total_share*100;
$this->view->total_share = $total_share;
$this->view->science_name = $science_name;
$this->view->dictionary_chart = $dictionary_chart;
$this->view->dictionary = $dictionary;
$this->view->directory_recommed_staff = $directory_recommed_staff;
$this->view->directory_recommed = $directory_recommed;
