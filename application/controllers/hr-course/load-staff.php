<?php
$course     = $this->getRequest()->getParam('course');
$staffCode  = $this->getRequest()->getParam('staffCode');
$staffCode  = preg_replace("/(^[\r\n]*|[\r\n]+)[\r\t]*[\s\n]+/", "\n", $staffCode);
$staffCode  = explode("\n", $staffCode);
$staffCode  = array_unique(array_filter($staffCode));
if (is_array($staffCode)) {
    $staffCodeLengths = 8;
    foreach ($staffCode as $key => $value) {
        if (strlen($value) != $staffCodeLengths) {
            $error = $value . ' Không đúng định dạng mã nhân viên Oppo';
            $this->view->error = $error;
        }else{
            $QStaff = new Application_Model_Staff();
            $get_all_staff = $QStaff->getStaffByArray(array('code' => $staffCode));
            if(count($get_all_staff) == 0){
                $error = 'Không tìm thấy dữ liệu nào liên quan đến bộ mã này';
                $this->view->error = $error;
            }else{
                $QTeam                       = new Application_Model_Team();
                $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
                $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
                $this->view->course = $course;
                $this->view->list = $get_all_staff;
            }
        }
    }
}
$this->_helper->layout()->disablelayout(true);