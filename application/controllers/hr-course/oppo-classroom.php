<?php
require_once 'Mobile_Detect.php';
$detect_device  = new Mobile_Detect;
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$course_id      = $this->getRequest()->getParam("id");

if(empty($course_id)){echo 'Chưa có khóa học được chọn, vui lòng <a href="/hr-course/index">quay lại</a> trang chủ'; exit;}

$QHrCourse              = new Application_Model_HrCourse();
$QHrCourseStaff         = new Application_Model_HrCourseStaff();
$QHrCourseStaffFlow     = new Application_Model_HrCourseStaffFlow();
$QHrCourseRatingStars   = new Application_Model_HrCourseRatingStars();

$where_course           = NULL;
$where_course           = $QHrCourse->getAdapter()->quoteInto('id = ?', $course_id);
$course                 = $QHrCourse->fetchRow($where_course);

$where_courseStaff      = [];
$where_courseStaff[]    = $QHrCourseStaff->getAdapter()->quoteInto('course_id = ?', $course_id);
$where_courseStaff[]    = $QHrCourseStaff->getAdapter()->quoteInto('staff_id  = ?', $userStorage->id);
$where_courseStaff[]    = $QHrCourseStaff->getAdapter()->quoteInto('del = ?', 0);
$data_staff             = $QHrCourseStaff->fetchRow($where_courseStaff);

$whereRatingStar        = [];
$whereRatingStar[]      = $QHrCourseRatingStars->getAdapter()->quoteInto('course_id = ?', $course_id);
$whereRatingStar[]      = $QHrCourseRatingStars->getAdapter()->quoteInto('staff_id  = ?', $userStorage->id);
$rating_star            = $QHrCourseRatingStars->fetchAll($whereRatingStar);

$is_studying            = empty($data_staff);
$is_empty               = empty($course);

if($is_studying || $is_empty) {echo 'Bạn không có tên trong khóa học này, vui lòng <a href="/hr-course/index">quay lại</a> trang chủ';exit;}

$is_online              = $course['type'] == 1 ? 1 : NULL;

// Kiểm tra dữ liệu câu hỏi
$sql        = 'SELECT a.form AS survey FROM hr_course_survey a WHERE a.course_id = "' . $course_id . '" GROUP BY a.form';
PC::debug($sql);
$db         = Zend_Registry::get('db');
$stmt       = $db->query($sql);
$h_survey   = $stmt->fetchAll();

$not_empty  = array();
foreach($h_survey as $ks => $sv):
    $not_empty[] = $sv['survey'];
endforeach;

$whereStaffFlow         = array();
$whereStaffFlow[]       = $QHrCourseStaffFlow->getAdapter()->quoteInto('course_id = ?', $course_id);
$whereStaffFlow[]       = $QHrCourseStaffFlow->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
$list_success           = $QHrCourseStaffFlow->fetchAll($whereStaffFlow);

$is_success = array();
foreach ($list_success as $sc):
    $is_success[] = $sc['step'];
endforeach;

$this->view->is_success = $is_success;
$this->view->star       = !empty($rating_star[0]['star']) ? $rating_star[0]['star'] : 0;
$this->view->course     = $course;
$this->view->not_empty  = $not_empty;
$this->view->IS_ONLINE  = $is_online;
$this->view->IS_MOBILE  = $detect_device->isMobile();