<?php
/**
 * @author buu.pham
 * tập hợp các tool cho call center sử dụng
 */
class CallCenterController extends My_Controller_Action
{
    /**
     * @author buu.pham
     * Danh sách IMEI chưa active, cần gọi điện kiểm tra thông tin khách hàng
     * @return list imei+info
     */
    public function inactiveAction()
    {
        $from        = $this->getRequest()->getParam('from', date('01/m/Y'));
        $to          = $this->getRequest()->getParam('to', date('d/m/Y'));
        $area_id     = $this->getRequest()->getParam('area_id');
        $imei        = $this->getRequest()->getParam('imei');
        $status      = $this->getRequest()->getParam('status');
        $imei_status = $this->getRequest()->getParam('imei_status');
        $resurvey    = $this->getRequest()->getParam('resurvey', 0);

        $page    = $this->getRequest()->getParam('page', 1);
        $limit   = LIMITATION;
        $total   = 0;
        $params  = array(
            'from'        => $from,
            'to'          => $to,
            'area_id'     => $area_id,
            'imei'        => $imei,
            'status'      => $status,
            'imei_status' => $imei_status,
            'resurvey'    => $resurvey,
        );

        $params['inactive'] = 1;

        $QImeiKpi = new Application_Model_ImeiKpi();
        $this->view->imeis = $QImeiKpi->fetchDetailPagination($page, $limit, $total, $params);
        unset($params['inactive']);

        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->page   = $page;
        $this->view->offset = $limit*($page-1);
        $this->view->params = $params;
        $this->view->url    = HOST.'call-center/inactive'.( $params ? '?'.http_build_query($params).'&' : '?' );

        $QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();
    }

    /**
     * @author buu.pham
     * Thông tin chi tiết để kiểm tra đối chiếu
     * @return html
     */
    public function inactiveDetailAction()
    {
        $this->_helper->layout->disableLayout();
        if (!$this->getRequest()->isXmlHttpRequest()) exit(json_encode(array('error' => 'AJAX required')));

        $imei   = $this->getRequest()->getParam('imei');
        $total  = 0;
        $params = array('imei' => $imei);

        $params['inactive'] = 1;
        $params['detail'] = 1;

        $QImeiKpi = new Application_Model_ImeiKpi();
        $result = $QImeiKpi->fetchDetailPagination(1, 1, $total, $params);

        if ($result) $detail = $result->fetch();

        if ($detail) {
            $QCustomerCheckCallHistory = new Application_Model_CustomerCheckCallHistory();
            $where = $QCustomerCheckCallHistory->getAdapter()->quoteInto('customer_check_id = ?', $detail['cc_id']);
            $call_history = $QCustomerCheckCallHistory->fetchAll($where, 'time ASC');

            $QStaff = new Application_Model_Staff();

            $this->view->staffs = $QStaff->get_cache();
            $this->view->detail = $detail;
            $this->view->call_history = $call_history;
        }

        $QGood = new Application_Model_Good();
        $this->view->goods = $QGood->get_cache();
        $QGoodColor = new Application_Model_GoodColor();
        $this->view->colors = $QGoodColor->get_cache();

        $this->_helper->viewRenderer->setRender('partials/detail');
    }

    public function inactiveResurveyAction()
    {
        $imei = $this->getRequest()->getParam('imei');
        $id = $this->getRequest()->getParam('id');

        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id)) throw new Exception("Invalid user", 6);

            if (!$imei) throw new Exception("Invalid IMEI", 1);

            $imei = trim($imei);

            $QImeiKpi = new Application_Model_ImeiKpi();
            $where = $QImeiKpi->getAdapter()->quoteInto('imei_sn = ?', $imei);
            $imei_kpi = $QImeiKpi->fetchRow($where);

            if (!$imei_kpi) throw new Exception("Invalid IMEI", 2);
            $QCustomerCheck = new Application_Model_CustomerCheck();

            // đã có sẵn thông tin check - cập nhật thôi
            if ($id) {
                $id = intval($id);

                $where = $QCustomerCheck->getAdapter()->quoteInto('id = ?', $id);
                $customer_check = $QCustomerCheck->fetchRow($where);

                if (!$customer_check)
                    throw new Exception("Wrong info", 4);

                if (!isset($customer_check['imei_sn']) && $customer_check['imei_sn'] != $imei)
                        throw new Exception("Imvalid IMEI", 3);

                $resurvey = 0;
                if (isset($customer_check['resurvey']) && $customer_check['resurvey']) {
                    if ($customer_check['resurvey'] >= 2)
                        throw new Exception("Already resurvey 2 times. You must finish the survey this time.", 5);

                    $resurvey = $customer_check['resurvey'] + 1;
                }

                $data = array('resurvey' => $resurvey);
                $QCustomerCheck->update($data, $where);
            } else { // chưa có thông tin check - tạo mới
                $resurvey = 1;
                $data = array(
                    'imei_sn'  => $imei,
                    'resurvey' => $resurvey,
                );
                $id = $QCustomerCheck->insert($data);
            }

            // tạo call history
            $time = time();
            $QCustomerCheckCallHistory = new Application_Model_CustomerCheckCallHistory();
            $data = array(
                'customer_check_id' => $id,
                'time'              => date('Y-m-d H:i:s', $time),
                'staff_id'          => $userStorage->id,
                'result'            => My_Survey_Result::RESURVEY,
            );
            $QCustomerCheckCallHistory->insert($data);

            exit(json_encode(array('result' => 1, 'resurvey' => $resurvey)));
        } catch (Exception $e) {
            exit(json_encode(array('error' => sprintf("[%s] %s", $e->getCode(), $e->getMessage()))));
        }
    }

    public function inactiveSaveDetailAction()
    {
        $imei                = $this->getRequest()->getParam('imei');
        $id                  = $this->getRequest()->getParam('id');
        $customer_name_check = $this->getRequest()->getParam('customer_name_check', 0);
        $model_check         = $this->getRequest()->getParam('model_check', 0);
        $store_check         = $this->getRequest()->getParam('store_check', 0);
        $sell_out_date_check = $this->getRequest()->getParam('sell_out_date_check', 0);
        $price_check         = $this->getRequest()->getParam('price_check', 0);
        $network             = $this->getRequest()->getParam('network');
        $status_check        = $this->getRequest()->getParam('status_check');
        $imei_check          = $this->getRequest()->getParam('imei_check');

        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id)) throw new Exception("Invalid user", 6);

            if (!$imei) throw new Exception("Invalid IMEI", 1);

            $imei = trim($imei);

            $QImeiKpi = new Application_Model_ImeiKpi();
            $where = $QImeiKpi->getAdapter()->quoteInto('imei_sn = ?', $imei);
            $imei_kpi = $QImeiKpi->fetchRow($where);

            if (!$imei_kpi) throw new Exception("Invalid IMEI", 2);
            $QCustomerCheck = new Application_Model_CustomerCheck();

            if (!isset(My_Network::$name[$network]) && in_array($status_check, array(My_Survey_Result::OK, My_Survey_Result::NOT_CONFIRMED)))
                throw new Exception("Invalid network", 7);

            if (!isset(My_Survey_Result::$name[ $status_check ]))
                throw new Exception("Invalid status", 8);

            if (My_Survey_Result::OK == $status_check
                && !($customer_name_check &&  $store_check && $sell_out_date_check && ($model_check || $imei_check || $price_check)))
                throw new Exception("Status OK requires [OK] in at least 4 checkboxes", 9);

            $info_checking = 0;
            if ($customer_name_check) $info_checking += pow(2, My_Survey_Info::CUSTOMER_NAME);
            if ($store_check) $info_checking         += pow(2, My_Survey_Info::STORE);
            if ($price_check) $info_checking         += pow(2, My_Survey_Info::PRICE);
            if ($model_check) $info_checking         += pow(2, My_Survey_Info::MODEL_COLOR);
            if ($sell_out_date_check) $info_checking += pow(2, My_Survey_Info::SELL_OUT_DATE);
            if ($imei_check) $info_checking          += pow(2, My_Survey_Info::IMEI);

            // đã có sẵn thông tin check - cập nhật thôi
            if ($id) {
                $id = intval($id);

                $where = $QCustomerCheck->getAdapter()->quoteInto('id = ?', $id);
                $customer_check = $QCustomerCheck->fetchRow($where);

                if (!$customer_check)
                    throw new Exception("Wrong info", 4);

                if (!isset($customer_check['imei_sn']) && $customer_check['imei_sn'] != $imei)
                        throw new Exception("Imvalid IMEI", 3);

                $data = array(
                    'resurvey'      => 0,
                    'info_checking' => $info_checking,
                    'network'       => intval($network),
                    'status'        => intval($status_check),
                );
                $QCustomerCheck->update($data, $where);
            } else { // chưa có thông tin check - tạo mới

                $data = array(
                    'imei_sn'       => $imei,
                    'resurvey'      => 0,
                    'info_checking' => $info_checking,
                    'network'       => intval($network),
                    'status'        => intval($status_check),
                );
                $id = $QCustomerCheck->insert($data);
            }

            // tạo call history
            $time = time();
            $QCustomerCheckCallHistory = new Application_Model_CustomerCheckCallHistory();
            $data = array(
                'customer_check_id' => $id,
                'time'              => date('Y-m-d H:i:s', $time),
                'staff_id'          => $userStorage->id,
                'result'            => intval($status_check),
            );
            $QCustomerCheckCallHistory->insert($data);

            exit(json_encode(array('result' => 1, 'status' => My_Survey_Result::$name[ $status_check ])));
        } catch (Exception $e) {
            exit(json_encode(array('error' => sprintf("[%s] %s", $e->getCode(), $e->getMessage()))));
        }
    }

    public function inactiveEndAction()
    {
        $imei         = $this->getRequest()->getParam('imei');
        $id           = $this->getRequest()->getParam('id');
        $status_check = $this->getRequest()->getParam('status_check');

        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if (!$userStorage || !isset($userStorage->id)) throw new Exception("Invalid user", 6);

            if (!$imei) throw new Exception("Invalid IMEI", 1);

            $imei = trim($imei);

            $QImeiKpi = new Application_Model_ImeiKpi();
            $where = $QImeiKpi->getAdapter()->quoteInto('imei_sn = ?', $imei);
            $imei_kpi = $QImeiKpi->fetchRow($where);

            if (!$imei_kpi) throw new Exception("Invalid IMEI", 2);
            $QCustomerCheck = new Application_Model_CustomerCheck();

            if (!isset(My_Survey_Result::$name[ $status_check ]) || $status_check == My_Survey_Result::NOT_CONFIRMED)
                throw new Exception("Invalid status", 8);

            // đã có sẵn thông tin check - cập nhật thôi
            if ($id) {
                $id = intval($id);

                $where = $QCustomerCheck->getAdapter()->quoteInto('id = ?', $id);
                $customer_check = $QCustomerCheck->fetchRow($where);

                if (!$customer_check)
                    throw new Exception("Wrong info", 4);

                if (!isset($customer_check['imei_sn']) && $customer_check['imei_sn'] != $imei)
                        throw new Exception("Imvalid IMEI", 3);

                $data = array(
                    'resurvey' => 0,
                    'status'   => intval($status_check),
                );
                $QCustomerCheck->update($data, $where);
            } else { // chưa có thông tin check - tạo mới

                $data = array(
                    'imei_sn'  => $imei,
                    'resurvey' => 0,
                    'status'   => intval($status_check),
                );
                $id = $QCustomerCheck->insert($data);
            }

            exit(json_encode(array('result' => 1, 'status' => My_Survey_Result::$name[ $status_check ])));
        } catch (Exception $e) {
            exit(json_encode(array('error' => sprintf("[%s] %s", $e->getCode(), $e->getMessage()))));
        }
    }

    public function inactiveCallHistoryAction()
    {
        $this->_helper->layout->disableLayout();
        if (!$this->getRequest()->isXmlHttpRequest()) exit;
        $id = $this->getRequest()->getParam('id');

        if (!$id || !intval($id)) return false;
        $QCustomerCheckCallHistory = new Application_Model_CustomerCheckCallHistory();
        $where = $QCustomerCheckCallHistory->getAdapter()->quoteInto('customer_check_id = ?', intval($id));
        $this->view->history = $QCustomerCheckCallHistory->fetchAll($where);

        $this->_helper->viewRenderer->setRender('partials/call-history');
    }
}
