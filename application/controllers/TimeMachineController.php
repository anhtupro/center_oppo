<?php
class TimeMachineController extends My_Controller_Action{
    private $hr_signature = '<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" style="font-family: &quot;segoe ui&quot;, &quot;lucida sans&quot;, sans-serif; border-collapse: collapse;">'
            . '<tbody >'
            . '<tr style="height: 29.4pt;">'
            . '<td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 29.4pt;">'
            . '<p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;">'
            . '<span style="font-family: arial, sans-serif; color: rgb(0, 146, 93);">HR Department<br ></span>'
            . '<span style="font-size: 10pt; font-family: arial, sans-serif;">Giá trị cốt lõi – “Bổn phận cao hơn chữ tín”</span>'
            . '<span style="font-family: arial, sans-serif; color: rgb(0, 146, 93);"></span>'
            . '</p>'
            . '</td>'
            . '</tr>'
            . '<tr style="height: 37.5pt;">'
            . '<td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 37.5pt;">'
            . '<p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;">'
            . '<span style="font-size: 10pt; font-family: arial, sans-serif;">T:&nbsp;'
            . '<span class="Object" id="OBJ_PREFIX_DWT357_com_zimbra_phone" style="color: rgb(0, 90, 149); cursor: pointer;">'
            . '<a href="callto:(84-8) 3920 2555" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;" target="_blank">(84-8) 3920 2555</a></span>&nbsp;- Ext: 111</span></p><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><span style="font-size: 10pt; font-family: arial, sans-serif;">F:&nbsp;<span class="Object" id="OBJ_PREFIX_DWT358_com_zimbra_phone" style="color: rgb(0, 90, 149); cursor: pointer;"><a href="callto:(84-8) 3920 4095" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;" target="_blank">(84-8) 3920 4095</a></span><br >E:&nbsp;<span class="Object" id="OBJ_PREFIX_DWT359_ZmEmailObjectHandler" style="color: rgb(0, 90, 149); cursor: pointer;"><span class="Object" role="link" id="OBJ_PREFIX_DWT45_ZmEmailObjectHandler">hr@oppo-aed.vn</span></span></span></p></td></tr><tr style="height: 38.9pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><b ><span style="font-size: 10pt; font-family: arial, sans-serif; color: rgb(4, 106, 56);">OPPO Authorized Exclusive Distributor <br> CÔNG TY CỔ PHẦN KỸ THUẬT & KHOA HỌC VĨNH KHANG</span></b><span style="font-size: 10pt; font-family: arial, sans-serif;"><br >12<sup >th</sup><b >&nbsp;</b>Floor, Lim II Tower<br >No. 62A CMT8 St., Dist. 3, HCMC, VN</span></p></td></tr><tr style="height: 16.5pt;"><td width="374" style="width: 3.9in; padding: 0in 5.4pt; height: 16.5pt;"><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><span style="color: rgb(31, 73, 125);"><span class="Object" id="OBJ_PREFIX_DWT360_com_zimbra_url" style="color: rgb(0, 90, 149); cursor: pointer;"><span class="Object" role="link" id="OBJ_PREFIX_DWT46_com_zimbra_url"><a href="https://www.oppo.com/vn/" target="_blank" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;"><span style="font-size: 10pt; font-family: arial, sans-serif; color: rgb(0, 146, 93);">www.oppo.com/vn</span></a></span></span></span></p></td></tr></tbody></table>';
    public function indexAction(){
    }
    private function getMail($staff_email, $type = 1) {
        $staff_email = trim($staff_email);
      
        if (!empty($staff_email)) {
            $list_email       = [
                'from' => 'hr@oppo-aed.vn',
                'to'   => '',
                'cc'   => array(),
                'bcc'  => ''
            ];
            $send_mail        = 0;
            $staff_model      = new Application_Model_Staff();
            $staff_info_where = $staff_model->getAdapter()->quoteInto('email = ?', $staff_email);
            $staff_info       = $staff_model->fetchRow($staff_info_where);
         
            if (!empty($staff_info)) {
                $staff_info = $staff_info->toArray();
            } else {
                //$this->view->data = "Email chưa chính xác hoặc không tồn tại";
                return null;
            }
            if (!empty($staff_info)) {
                $list_email['to'] = $staff_info['email'];
                $db               = Zend_Registry::get('db');
                if (in_array($staff_info['department'], array(152, 157)) && !in_array($staff_info['team'], array(133, 131,406))) {
                    //  sale department && not trainer & not trade
                    $select_rule = $db->select()
                            ->from(array('a' => 'auto_mail_rule'), array('a.*'))
                            ->where('status = 1')
                            ->where('type = ?', $type)
                            ->where('department_id = ? ', $staff_info['department'])
                            ->where('(team_id is NULL ')
                            ->orwhere('team_id not in  (?)) ', array(133, 131,406));
                    $result_mail = $db->fetchRow($select_rule);

                    if (!empty($result_mail)) {
                        if (in_array($staff_info['title'], array(
                                    190, // SALE LEADER
                                    183, // SALE
                                    179, // ASM
                                    308, // RSM
                                    181, // ASM STANBY
                                    373, // STORE LEADER
                                    162, // SALE LEADER ACCESSORIES
                                    164, // SALE ACCESSORIES
                                    291, // ACCESSORIES SALES MANAGER
                                    191, // SALE ADMIN
                                ))) {
                            if ($type == 1) {
                                if (!empty($result_mail['area_mail_cc'])) {
                                    $select_area = $db->select()
                                            ->from(array('r' => 'regional_market'), array('r.*'))
                                            ->join(array('ar' => 'area'), 'r.area_id =ar.id', array('ar.email'))
                                            ->where('r.id = ?', $staff_info['regional_market']);
                                    $result_area = $db->fetchRow($select_area);
                                    $result_area = $db->fetchRow($select_area);
                                    if (!empty($result_area['email'])) {
                                        $list_email['cc'][] = $result_area['email'];
                                        $send_mail          = 1;
                                    }
                                }
                            } else {
                                if (in_array($staff_info['title'], array(
                                            191, // SALE ADMIN
                                        ))) {
                                    $select_area = $db->select()
                                            ->from(array('r' => 'regional_market'), array('r.*'))
                                            ->join(array('ar' => 'area'), 'r.area_id =ar.id', array('ar.email'))
                                            ->where('r.id = ?', $staff_info['regional_market']);
                                    $result_area = $db->fetchRow($select_area);
                                    $result_area = $db->fetchRow($select_area);
                                    if (!empty($result_area['email'])) {
                                        $list_email['cc'][] = $result_area['email'];
                                        $send_mail          = 1;
                                    }
                                }
                            }
                        } else {

                            if (!empty($result_mail['only_office'])) {
                                if ($staff_info['office_id'] == 49) {
                                    $list_email['cc'][] = "headoffice@oppo-aed.vn";
                                    $send_mail          = 1;
                                }
                            }
                        }
                    }
                } else {
                    // get mail rule by team
                    $select_rule = $db->select()
                            ->from(array('a' => 'auto_mail_rule'), array('a.*'))
                            ->where('status = 1')
                            ->where('type = ?', $type);
                    if (in_array($staff_info['department'], array(150,149,156,557))) {
                        $select_rule->where('department_id = ?', $staff_info['department']);
                    } else {
                        $select_rule->where('team_id = ? ', $staff_info['team']);
                    }

                    $result_mail = $db->fetchRow($select_rule);

                    if (!empty($result_mail)) {
                        $list_email['cc'][] = $result_mail['team_mail_cc'];
                        $send_mail          = 1;
                        if ($result_mail['officer_check'] == 1 && $staff_info['office_id'] == 49) {
                            $list_email['cc'][] = "headoffice@oppo-aed.vn";
                        } else
                        if ($result_mail['area_mail_cc'] == 1) {
                            $select_area        = $db->select()
                                    ->from(array('r' => 'regional_market'), array('r.*'))
                                    ->join(array('ar' => 'area'), 'r.area_id =ar.id', array('ar.email'))
                                    ->where('r.id = ?', $staff_info['regional_market']);
                            $result_area        = $db->fetchRow($select_area);
                            $list_email['cc'][] = $result_area['email'];
                        }
                        if ($result_mail['area_mail_cc'] == 2) {
                            $list_email['cc'][] = "headoffice@oppo-aed.vn";
                        }
                    }
                }
                $list_email['bcc'] = $result_mail['email_bcc'];
                $list_email['bcc'] .= ',tanthinh.phan@oppo-aed.vn,daithanh.nguyen@oppo-aed.vn';
                if ($send_mail == 1) {
                    //$this->view->data = $list_email;
                    return $list_email;
                } else {
                    //$this->view->data = "Không nằm trong danh sách gửi mail";
                    return null;
                }
                //return;
            }
        }
    }
    public function listManagerAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'list-manager.php'; 
    }
    
    public function hrPassbyAction(){
        require_once 'time-machine'.DIRECTORY_SEPARATOR.'hr-passby.php';
    }	
	public function sendEmailManagerDeparmentAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'send-email-manager-deparment.php'; 
    }
	
    public function uploadTempTimeAction(){
        require_once 'time-machine'.DIRECTORY_SEPARATOR.'upload-temp-time.php';
    }
    
    public function overTimeAction()
    {
        $export = $this->getRequest()->getParam('export');
        $department = $this->getRequest()->getParam('department');
        
    }

    public function listStaffMapAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'list-staff-map.php'; 
    }

    public function personalPermissionAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'personal-permission.php'; 
    }

    public function addPersonalAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'add-personal.php';
    }

    public function delPersonalAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'del-personal.php';
    }

    public function lateListAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'late-list.php';
    }
    
    public function lateListNewAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'late-list-new.php';
    }

    public function lateReportAction()
    {
        $export = $this->getRequest()->getParam('export', 0);

        $db = Zend_Registry::get('db');

        $sql = "SELECT
                    st.id as `id`,
                    st.code as `staff_code`,
                    st.ID_number as `cmnd`,
                    st.off_date as `off_date`,
                    st.joined_at as `joined_at`,
                    t1.name as `department_name`,
                    t2.name as `team_name`,
                    ar.name as `area` ,
                    CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                    cid.check_in_day as `check_in_day`,
                    cid.check_in_at as `check_in_at`,
                    cid.check_out_at as `check_out_at`,
                    IF(TIMEDIFF(TIME(cid.check_in_at), cis.begin) > '00:00:00',
                        TIMEDIFF(TIME(cid.check_in_at), cis.begin),
                        NULL
                    )
                     as `check_in_late_time`,
                    IF(TIMEDIFF(cis.end, TIME(cid.check_out_at)) > '00:00:00',
                        TIMEDIFF(cis.end, TIME(cid.check_out_at)),
                        NULL
                    )
                    as `check_out_soon_time`,
                    tt.reason,
                    tt.status,
                    IF(cid.check_in_late_time <= '00:15:00', 1, 0) as `soon_15`
                FROM `check_in_detail` cid
                JOIN `staff` st 
                    ON cid.staff_code = st.code
                LEFT JOIN `temp_time` tt 
                    ON tt.staff_id = st.id AND tt.date = cid.check_in_day AND tt.status = 1
                LEFT JOIN `time_late` tl
                    ON tl.staff_id = st.id AND tl.date = cid.check_in_day AND tl.status = 1
                LEFT JOIN `leave_detail` ld
                    ON ld.staff_id = st.id 
                        AND ld.status = 1 
                        AND (cid.check_in_day BETWEEN ld.from_date AND ld.to_date)
                LEFT JOIN `team` t1 ON st.department = t1.id
                LEFT JOIN `team` t2 ON st.team = t2.id
                LEFT JOIN `check_in_shift` cis ON st.code = cis.staff_code AND cis.date IS NULL
                INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                WHERE (cid.check_in_day BETWEEN '2017-04-01' AND '2017-04-30')
                AND (cid.check_in_at IS NOT NULL AND cid.check_out_at IS NOT NULL)
                AND (tt.id IS NULL AND tl.id IS NULL AND ld.id IS NULL )
                AND DAY(cid.check_in_day) NOT IN (1, 2, 7, 14, 21, 28)
                AND (
                    TIMEDIFF(TIME(cid.check_in_at), cis.begin) > '00:00:00'
                );";
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $data_late = array();
        $data_staff_number = array();

        foreach($data as $value)
        {
            if($value['soon_15'] == 1)
            {
                if($data_staff_number[$value['id']] == 3 )
                {
                    $data_late[] = $value;
                }
                elseif(empty($data_staff_number[$value['id']]))
                {
                    $data_staff_number[$value['id']]++;
                }
                else
                {
                    $data_staff_number[$value['id']] = 1;
                }
            }
            else
            {
                $data_late[] = $value;
            }
            
        }

        // echo '<pre>';
        // print_r($data_late); die;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $alphaExcel = new My_AlphaExcel();

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Mã nhân viên',
            $alphaExcel->ShowAndUp() => 'Tên nhân viên',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Phòng ban',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Khu vực',
            $alphaExcel->ShowAndUp() => 'Ngày vào làm',
            $alphaExcel->ShowAndUp() => 'Ngày nghỉ',
            $alphaExcel->ShowAndUp() => 'Ngày',
            $alphaExcel->ShowAndUp() => 'Thứ',
            $alphaExcel->ShowAndUp() => 'Giờ vào',
            $alphaExcel->ShowAndUp() => 'Giờ ra',
            $alphaExcel->ShowAndUp() => 'Trễ',
            $alphaExcel->ShowAndUp() => 'Sớm',
        );
        
        $array_thu = array(
            'Mon' => 'Thứ 2',
            'Tue' => 'Thứ 3',
            'Wed' => 'Thứ 4',
            'Thu' => 'Thứ 5',
            'Fri' => 'Thứ 6',
            'Sat' => 'Thứ 7',
            'Sun' => 'CN',
        );

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }
        $index = 0;
        foreach($data_late as $key => $value)
        {
            $late = (!empty($value['check_in_late_time']) )?date('H:i', strtotime($value['check_in_late_time'])):'';
            $soon = (!empty($value['check_out_soon_time']) )?date('H:i', strtotime($value['check_out_soon_time'])):'';

            if( ($late == '00:00' && $soon == '') || ($late == '' && $soon == '00:00')
                    || ($late == '00:00' && $soon == '00:00') )
            {
                
            }
            else
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['staff_code']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['staff_name']);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+2), $value['cmnd'],  PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['department_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['team_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['area']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), date('d/m/Y', strtotime($value['check_in_day'])) );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $array_thu[date('D', strtotime($value['check_in_day']))] );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['check_in_at'])?date('H:i', strtotime($value['check_in_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['check_out_at'])?date('H:i', strtotime($value['check_out_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), (!empty($value['check_in_late_time']) )?date('H:i', strtotime($value['check_in_late_time'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), (!empty($value['check_out_soon_time']) )?date('H:i', strtotime($value['check_out_soon_time'])):'' );
                $index++;
            } 
        }

        $filename = 'Xuất đi trễ - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        $time_end_write = $this->microtime_float();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    public function lateDetailAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'late-detail.php';
    }

    public function printAction()
    {
        $this->_helper->layout->disableLayout();
        $month = $this->getRequest()->getParam('month', intval(date('m')));
        $year = $this->getRequest()->getParam('year', intval(date('Y')));
        $staff_id = $this->getRequest()->getParam('staff_id');
        $export = $this->getRequest()->getParam('export', 0);
		
		$QLogStaffWarning = new Application_Model_LogStaffWarning();
        
        $where = array();
        $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "MONTH(penalty_day) = $month" );
        $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year" );
        $locked_day = $QLogStaffWarning->fetchRow ( $where );
        if($_GET['dev']){
            echo "<pre>";
            print_r($locked_day);exit;
        }


        if(!empty($export))
        {
            $this->exportFile($month, $year, implode(",", $staff_id));
            exit;
        }

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $QCheckIn = new Application_Model_CheckIn();

        $params = array(
            'from_date' => $from,
            'to_date' => $to,
            'month' => $month,
            'year' => $year,
        );
		if(!empty($locked_day)){
             
            $db = Zend_Registry::get('db');
        
            $sql = "CALL `get_late_list_log`(:month, :year)";
        
            $stmt = $db->prepare($sql);
            $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
            $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
            $stmt->execute();
        
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;
           
        }else{
            $data = $QCheckIn->listLate($params);
        }
   
        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->staff_id = $staff_id;
        // print_r($params); die;
    }

    public function printEditAction()
    {
        $this->_helper->layout->disableLayout();

        $params = array();
    
        $params['staff_name']= $this->getRequest()->getParam('staff_name');
        $params['department'] = $this->getRequest()->getParam('staff_department');
        $params['title']= $this->getRequest()->getParam('staff_title');
        $params['number_15'] = $this->getRequest()->getParam('number_15');
        $params['number_30']= $this->getRequest()->getParam('number_30');
        $params['phat'] = $this->getRequest()->getParam('phat');
        $params['ngayxuphat'] = $this->getRequest()->getParam('ngayxuphat');
        $params['forget_check_in'] = $this->getRequest()->getParam('forget_check_in');
        $params['date_late'] = $this->getRequest()->getParam('date_late');
        $params['day_temp_time'] = $this->getRequest()->getParam('day_temp_time');
         $params['solan'] = $this->getRequest()->getParam('solan');
        
        $this->view->params = $params;
    }

    public function luoiAction()
    {
        $export = $this->getRequest()->getParam('export');
        $from_date = $this->getRequest()->getParam('from_date', date('1/m/Y'));
        $to_date = $this->getRequest()->getParam('to_date', date('d/m/Y'));
        $code = $this->getRequest()->getParam('code');
        $name = $this->getRequest()->getParam('name');
        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $params = array(
        );

        $params['from_date'] = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
		$params['to_date'] = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;
        $params['staff_code'] = $auth->code;
        $params['code'] = $code;
        $params['name'] = $name;

        $QCheckIn = new Application_Model_CheckIn();
        
        if(!empty($export))
        {   
            $time_start = $this->microtime_float();
            $data = $QCheckIn->xuat_luoi($params);
            $time_start_write = $this->microtime_float();
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            
            $heads = array(
                $alphaExcel->ShowAndUp() => 'STT',
                $alphaExcel->ShowAndUp() => 'Mã nhân viên',
                $alphaExcel->ShowAndUp() => 'Tên họ',
                $alphaExcel->ShowAndUp() => 'Phòng ban',
                $alphaExcel->ShowAndUp() => 'Khu vực',
                $alphaExcel->ShowAndUp() => 'Ngày',
                $alphaExcel->ShowAndUp() => 'Thứ',
                $alphaExcel->ShowAndUp() => 'Giờ vào',
                $alphaExcel->ShowAndUp() => 'Giờ ra',
                $alphaExcel->ShowAndUp() => 'Trễ',
                $alphaExcel->ShowAndUp() => 'Sớm',
                $alphaExcel->ShowAndUp() => 'Công',
                $alphaExcel->ShowAndUp() => 'Tổng giờ',
                $alphaExcel->ShowAndUp() => 'Tăng ca',
                // $alphaExcel->ShowAndUp() => 'Tổng toàn bộ',
                // $alphaExcel->ShowAndUp() => 'Ca',
            );
            
            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }

            foreach($data as $key => $value)
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['code']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staff_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['department']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['area_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($value['check_in_day'])) );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('D', strtotime($value['check_in_day'])) );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['check_in_at'])?date('H:i', strtotime($value['check_in_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['check_out_at'])?date('H:i', strtotime($value['check_out_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (!empty($value['late']) && !empty($value['check_out_at']) )?date('H:i', strtotime($value['late'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (!empty($value['soon']) && !empty($value['check_out_at']) )?date('H:i', strtotime($value['soon'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (floatval($value['cong'])>0)?number_format($value['cong'], 2):'0.00' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (floatval($value['cong'])>0)?number_format($value['cong']*8, 2):'0.00' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($value['overtime'])?date('H:i', strtotime($value['overtime'])):'');
                // $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), number_format($value['total']-1.5, 2) );
                
            }

            $filename = 'Xuất lưới - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
            $time_end_write = $this->microtime_float();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;

        }
        $data = $QCheckIn->xuat_luoi($params);
        $params = array(
            'from_date' => $from_date,
            'to_date' => $to_date,
            'staff_id' => $auth->id,
        );
        $this->view->params = $params;
        $this->view->data = $data;
    }

    public function lateAction()
    {
        $time = $this->getRequest()->getParam('time', date('m').'/'.date('Y'));
        $export = $this->getRequest()->getParam('export');

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $params = array(
            'from' => $from,
            'to' => $to,
            'time' => $time,
        );

        $QCheckIn = new Application_Model_CheckIn();

        if(!empty($export))
        {
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            
            $heads = array(
                $alphaExcel->ShowAndUp() => 'STT',
                $alphaExcel->ShowAndUp() => 'Mã nhân viên',
                $alphaExcel->ShowAndUp() => 'Tên họ',
                $alphaExcel->ShowAndUp() => 'Phòng ban',
                $alphaExcel->ShowAndUp() => 'Quên chấm công',
                $alphaExcel->ShowAndUp() => 'Trễ từ 15 đến 30 phút',
                $alphaExcel->ShowAndUp() => 'Trễ trên 30 phút',
                $alphaExcel->ShowAndUp() => 'Tổng số ngày công phạt'
            );
            
            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }
            $data = $QCheckIn->fetchLate($params);
            $row = 0;
            foreach($data as $key => $value)
            {
                $phat_15_30 = floor ($value['15_30']/3) * 0.5;
                $phat_30 = floor ($value['30']) * 0.5;
                $phat_quen_cham = floor ($value['quen_cham_cong']/2) * 0.5;
                $total = $phat_15_30 + $phat_30 + $phat_quen_cham;

                if(!empty($total))
                {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $key+1);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['staff_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['code']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['department_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['quen_cham_cong']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['15_30']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['30']);
                    
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $total);
                    $row++;
                } 
            }

            $filename = 'Đi trễ - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
            $time_end_write = $this->microtime_float();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;

        }
        
        $data = $QCheckIn->fetchLate($params);

        $this->view->params = $params;
        $this->view->data = $data;
    }


    public function departmentAction()
    {
        $export = $this->getRequest()->getParam('export');
        $time = $this->getRequest()->getParam('time', date('m').'/'.date('Y'));

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $params = array(
            'from' => $from,
            'to' => $to,
            'time' => $time,
        );

        $QCheckIn = new Application_Model_CheckIn();
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if(!empty($export))
        {
            $data = $QCheckIn->getByDepartment($params);
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();
            $start_row = 1;

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            $sheet->getColumnDimension('B')->setWidth(25);
            $sheet->getColumnDimension('C')->setWidth(20);
            // header 1
            $heads = array(
                $alphaExcel->ShowAndUp() => 'STT',
                $alphaExcel->ShowAndUp() => 'Họ tên',
                $alphaExcel->ShowAndUp() => 'Code',
                $alphaExcel->ShowAndUp() => 'Chức vụ',
                $alphaExcel->ShowAndUp() => 'Khu vực'
            );
            
            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.$start_row, $value);
            }
            $start_row++;

            // header 2
            $alphaExcel = new My_AlphaExcel();
            $heads_2 = array(
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
            );

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                $heads_2[$alphaExcel->ShowAndUp()] = $i;
            }

            foreach($heads_2 as $key => $value)
            {
                $sheet->setCellValue($key.$start_row, $value);
            }
            $start_row++;

            // header 3
            $alphaExcel = new My_AlphaExcel();
            $heads_3 = array(
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
            );

            $array_sunday = array();

            $off_date = $QCheckIn->getDateInfo($params);

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                $day_of_week = date('D', strtotime($year . '-' . $month . '-' . $i));
                $heads_3[$alphaExcel->ShowAndUp()] = $day_of_week;
                if($day_of_week == 'Sun')
                {
                    $array_sunday[] = $i;
                }
            }

            foreach($off_date as $key => $value)
            {
                if($value['is_off'] > 0)
                {
                    $array_sunday[] = intval(date('d', strtotime($value['date'])));
                }
            }

            foreach($heads_3 as $key => $value)
            {
                $sheet->setCellValue($key.$start_row, $value);
            }
            $start_row++;
            $stt = 1;
            foreach($data as $key_department => $value_department)
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, '');
                $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_department['name']);
                $start_row++;
                foreach($value_department['list_staff'] as $key_staff => $value_staff)
                {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $stt);
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_staff['name']);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp().$start_row, $value_staff['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_staff['title']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_staff['area_name']);
                    for($i = 1; $i <= $number_day_of_month; $i++)
                    {
                        $cell = $alphaExcel->ShowAndUp();
                        if(in_array($i, $array_sunday))
                        {
                            $sheet->getStyle($cell.$start_row)->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'FF0000')
                                    )
                                )
                            );
                        }

                        $sheet->setCellValue($cell.$start_row, number_format($value_staff['list_time'][$i], 2));
                    }
                    $start_row++;
                    $stt++;
                }
                
            }

            $filename = 'Công phòng ban - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
            
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }

        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }

    public function listStaffMachineAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $email = $this->getRequest()->getParam('email');
        $name = $this->getRequest()->getParam('name');
       
        $registry = $this->getRequest()->getParam('registry');
        $code = $this->getRequest()->getParam('code');
        $ip = $this->getRequest()->getParam('ip');
        $enroll_number = $this->getRequest()->getParam('enroll_number');
        
        $QCheckin = new Application_Model_CheckIn();
        if(!empty($registry))
        {
            $params_insert = array(
                'ip' => $ip,
                'staff_code' => $code,
                'enroll_number' => $enroll_number
            );

            $QCheckin->insertMachineStaff($params_insert);
        }

        $limit = 20;
        $offset = ($page - 1) * $limit;

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $list_info = $QCheckin->getListCodePermission($auth->code);
        if(!empty($list_info) || in_array($userStorage->group_id, array(HR_ID, 1)))
        {

        }
        else
        {
            $this->_redirect('/user/noauth');
        }

        $QCheckIn = new Application_Model_CheckIn();

        $params = array(
            'page' => $page,
            'code' => $code,
            'email' => $email,
            'name' => $name,
            'offset' => $offset,
            'limit' => $limit
        );

        $data = $QCheckIn->getStaffEnrollNumber($params);

        $this->view->data = $data['data'];
        $this->view->limit = $limit;
        $this->view->offset = $offset;
        $this->view->total = $data['total'];
        $this->view->url = HOST . 'time-machine/list-staff-machine' . ($params ? '?' . http_build_query($params) . '&' : '?');

    }

    protected function _curl($url= null, $pars = array()){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $pars);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0');
        $html = curl_exec($curl);
        curl_close($curl);
        return $html;
    }

    public function exportFile($month, $year, $staff_id)
    {
        
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $params = array(
            'month' => $month,
            'year' => $year,
            'staff_id' => $staff_id,
            'from_date' => $from,
            'to_date' => $to
        );
        
        $db = Zend_Registry::get('db');
        $sql = "CALL `detail_late`(:from_date, :to_date, :staff_id)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
        $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
        $stmt->bindParam('staff_id', My_Util::escape_string($staff_id), PDO::PARAM_STR);
        $stmt->execute();
        
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
    
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'Mã nhân viên ',
            'Tên nhân viên',
            'Phòng ban',
            'Thứ',
            'Ngày',
            'Giờ vào',
            'Giờ ra'
        );
    
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
    
        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;
    
        foreach($data as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['staff_code']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['department_name']);
            $sheet->setCellValue($alpha++.$index, date('D', strtotime($item['day'])));
            $sheet->setCellValue($alpha++.$index, date('d/m/Y', strtotime($item['day'])));
            $sheet->setCellValue($alpha++.$index, date('H:i', strtotime($item['check_in_at'])));
            $sheet->setCellValue($alpha++.$index, date('H:i', strtotime($item['check_out_at'])));
            $index++;
        }
    
        $filename = 'Time_late_detail_'.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    
        $objWriter->save('php://output');
    
        exit;
    }

	
    public function exportAction(){
    
        $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('year');
        $staff_id = $this->getRequest()->getParam('staff_id');
        
        $this->exportFile($month, $year, $staff_id);
        exit;
    }
    
    public function cancelDayLateAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $staff_id = $this->getRequest()->getParam('staff_id');
        $list_date = $this->getRequest()->getParam('list_date');
        $listTempTime = $this->getRequest()->getParam('listTempTime');
        $listTimeMachine = $this->getRequest()->getParam('listTimeMachine');
        
        $QTimeMachine = new Application_Model_TimeMachine();
        $month = $this->getRequest()->getParam('month_cancel',date('m'));
        $year = $this->getRequest()->getParam('year_cancel',date('Y'));
        $db = Zend_Registry::get('db');
        
        
        $str_insert = '';
        foreach($listTimeMachine as $key => $value){
            
            if(empty($value) || ($value == ''))
            {
                unset($listTimeMachine[$key]);
                
            }else{
                $str_insert .= "'". $value ."'" . ',';
            }
            
        }
        $str_time_machine = '';
        if(strlen($str_insert) > 1){
            $str_time_machine = rtrim($str_insert,',');
        }
        
        if(strlen($str_time_machine) > 1){
            $sql_time_machine = "UPDATE `time_machine`
            SET late_passby = 1
            WHERE staff_id = $staff_id
            AND check_in_day IN ($str_time_machine) ";
            $db->query($sql_time_machine);
            
            $sql_time_gps = "UPDATE `time_gps`
            SET late_passby = 1
            WHERE staff_id = $staff_id
            AND check_in_day IN ($str_time_machine) ";
            $db->query($sql_time_gps);
            
        }
       
        if(!empty($listTempTime)){
            $str_TempTime  = implode(',',$listTempTime);
            $sql1 = "UPDATE `temp_time`
            SET `passby` = 1,
            `hr_reject` = 1
            WHERE staff_id = $staff_id
            AND `id` IN ($str_TempTime)" ;
            $db->query($sql1);
        }
           
        if(!empty($list_date)){
            
//             $str_list_date = implode(',',$list_date);
            
//             $sql = "UPDATE `time_machine`
//                     SET late_passby = 1
//                     WHERE staff_id = $staff_id 
//                     AND check_in_day IN ($str_list_date)
//                 ";
            
//             $sql1 = "UPDATE `temp_time`
//                     SET `passby` = 1
//                     WHERE staff_id = $staff_id 
//                     AND `date` IN ($str_list_date)" ;
            
//             $db = Zend_Registry::get('db');
//             $db->query($sql);
//             $db->query($sql1);
            
            
            echo json_encode (array(
                'status' => 1,
                'message' => "success",
				'month'	 => $month,
				'year'	 => $year
            ));
            exit();
        }else{
			  echo json_encode (array(
				'status' => 1,
				'message' => "Bạn chưa chọn ngày",
				'month'	 => $month,
					'year'	 => $year
				));
        exit();
		}
      
    }
    
    public function byPassLateAction() {
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'by-pass-late.php';    
    }
    
    public function hrLogAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'hr-log.php';
    }
    
    public function hrRemoveAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'hr-remove.php';
    }
    
    public function sendNotiAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $month = $this->getRequest()->getParam('month');
        $year  = $this->getRequest()->getParam('year');
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $db = Zend_Registry::get('db');

        $sql = "CALL `PR_Remind_By_Month`(:from_date, :to_date)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', My_Util::escape_string($from), PDO::PARAM_STR);
        $stmt->bindParam('to_date', My_Util::escape_string($to), PDO::PARAM_STR);
        $stmt->execute();

        $data_noti = $stmt->fetchAll();
        $stmt->closeCursor();
        
        $QLogStaffWarmingDetail = new Application_Model_LogStaffWarningDetail();
        $QLogStaffWarning  = new Application_Model_LogStaffWarning();
        $where_warning_3 = array();
        $where_warning_3[] = $QLogStaffWarning->getAdapter()->quoteInto ('YEAR(penalty_day) = ?', $year );
        $where_warning_3[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) = ?', $month);
        $where_warning_3[] = $QLogStaffWarning->getAdapter()->quoteInto('warning_type = 3');
        $result_red = $QLogStaffWarning->fetchAll($where_warning_3);
        $list_warning_red = $list_warning_last = array();
        if(!empty($result_red)){
             foreach ($result_red as $k => $val){
                 $list_warning_red[] = $val['staff_id'];
             }
        }
//        $arr_warning_noti = array();
//        if(!empty($data_noti)){
//             foreach ($data_noti as $k => $val){
//                 if(!in_array($val['staff_id'], $list_warning_red)){
//                     $arr_warning_noti[] = $val['staff_id'];
//                 }
//                 
//             }
//           
//        }

        $arr_warning_noti = array(9388, 2128);

        //bỏ qua tháng 6
//        $penalty_day_passby = '2017-6-30';
//        $where   = array ();
//        $where[] = $QLogStaffWarning->getAdapter()->quoteInto ( 'YEAR(penalty_day) = ?', $year );
//        $where[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) < ?', $month);
//        $where[] = $QLogStaffWarning->getAdapter()->quoteInto('	warning_type <> 0');
//        $result_last  = $QLogStaffWarning->fetchAll ( $where );
//        
//        $list_warning_last = array();
//        
//        if(!empty($result_last)){
//             foreach ($result_last as $k => $val){
//                 $list_warning_last[] = $val['staff_id'];
//             }
//        }
//        $where_warning_2 = array();
//        $where_warning_2[] = $QLogStaffWarning->getAdapter()->quoteInto ('YEAR(penalty_day) = ?', $year );
//        $where_warning_2[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) = ?', $month);
//        $where_warning_2[] = $QLogStaffWarning->getAdapter()->quoteInto('warning_type = 2');
//        $result_noti = $QLogStaffWarning->fetchAll($where_warning_2);
        
//        $where_warning_first = array();
//        $where_warning_first[] = $QLogStaffWarning->getAdapter()->quoteInto ('YEAR(penalty_day) = ?', $year );
//        $where_warning_first[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) = ?', $month);
//        $where_warning_first[] = $QLogStaffWarning->getAdapter()->quoteInto('warning_type = 1');
//        $result_warning_first  = $QLogStaffWarning->fetchAll($where_warning_first);
//        
//        $arr_warning_noti = array();
//        if(!empty($result_noti)){
//            foreach ($result_noti as $k => $val){
//                $arr_warning_noti[] = $val['staff_id'];
//            }
//        }
//        
//        if(!empty($result_warning_first)){
//            foreach ($result_warning_first as $k => $val){
//                $arr_warning_noti[] = $val['staff_id'];
//            }
//        }
        
        $date_time = date( 'Y-m-d H:i:s' );
        //Insert notification_new
        $data = array(
            'title'         => 'Thông báo vi phạm chuyên cần',
            'content'       => "<p> Bạn có tổng số giờ đi trễ ≤ 2h hoặc quên check in từ 01 đến 02 lần trong tháng $month </p>",
            'category_id'   => 1,
            'created_at'    => $date_time,
            'created_by'    => 1,
            'pop_up'        => 1,
            'status'        => 1,      
            'type'          => 1
        );
        
        $QModelNoti = new Application_Model_Notification();
        $QModelNotificationAccess = new Application_Model_NotificationAccess();
        
       $id_noti =  $QModelNoti->insert($data);
       
       foreach ($arr_warning_noti as $k => $val){
           $data_staff = array(
               'user_id'           => $val,
               'notification_id'   => $id_noti,
               'notification_status' => 0,
               'created_date'      => $date_time,
               'note'              => 'chuyen can '. $date_time
           );
           $QModelNotificationAccess->insert($data_staff);
       }
        
        echo json_encode(array(
                'status' => 1,
                'message' => "success"
            ));
        
    }
    
    public function sendEmailManagerAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'send-email-manager.php';
    }
    
    public function sendEmailAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $staff_id = $this->getRequest()->getParam('staff_id');
        $code = $this->getRequest()->getParam('staff_code');
        $month = $this->getRequest()->getParam('month', intval(date('m')));
        $year = $this->getRequest()->getParam('year', intval(date('Y')));
        $time_dealine = $this->getRequest()->getParam('time_dealine');
        $QTestEmail = new Application_Model_TestEmail();
        $check_email = $QTestEmail->fetchRow();

        $db = Zend_Registry::get('db');
        $sql = "SELECT st.code,s1.`email`, sp.* FROM staff st 
                INNER JOIN `staff_permission` sp ON sp.department_id = st.department AND ( sp.team_id IS NULL OR sp.team_id = st.team ) 
                 AND sp.`is_manager` = 1 
                INNER JOIN `staff` s1 ON sp.staff_code = s1.`code` 
                WHERE st.`code` = :code
                UNION ALL
                SELECT st.code,s1.`email`, sp.* FROM staff st 
                INNER JOIN `staff_permission` sp ON sp.department_id = st.department AND ( sp.team_id IS NULL OR sp.team_id = st.team ) 
                AND sp.`is_leader` = 1 AND (sp.office_id = st.office_id OR sp.is_all_office = 1)
                INNER JOIN `staff` s1 ON sp.staff_code = s1.`code`
                WHERE st.`code` = :code AND s1.`code` NOT IN ('15010021')
                ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('code', My_Util::escape_string($code), PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = null;

        $ccStaff = array();

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $QCheckIn = new Application_Model_CheckIn();
        $QLogSendMail = new Application_Model_LogSendMail();
        $params = array(
            'from_date' => $from,
            'to_date' => $to,
            'month' => $month,
            'year' => $year,
            'department' => NULL,
            'team' => NULL,
            'title' => NULL,
            'code' => $code,
            'name' => NULL,
            'area' => NULL
        );


        $data = $QCheckIn->listLate($params);


        $content = $subject = $day_15_30 = $day_30 = $forget_check_in_day = '';
        $mailto = array();

        if (!empty($data[0])) {
            $thoi_gian_1 = ($data[0]['total_late'] <= 180 && $data[0]['total_late'] > 120) ? $data[0]['thoi_gian'] : '';
            $phat_1 = ($data[0]['total_late'] <= 180 && $data[0]['total_late'] > 120) ? 0.5 . ' ngày công' : '';

            $thoi_gian_2 = ($data[0]['total_late'] > 180 ) ? $data[0]['thoi_gian'] : '';
            $thoi_gian_2_phat = ($data[0]['total_late'] > 180 ) ? My_Controller_Action::rule_penalti($data[0]['total_late']) . ' ngày công' : '';
            $forget_check_in_day = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 : '';
            $phat_forget_check_in_day = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 . ' ngày công' : '';
            $ngay_phat = ($data[0]['ngay_phat'] > 0) ? $data[0]['ngay_phat'] . ' ngày công' : '';

            $forget_check_in = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 : 0;
            $late_less_3 = (My_Controller_Action::rule_penalti($data[0]['total_late']) <= 0.5) ? My_Controller_Action::rule_penalti($data[0]['total_late']) : 0;
            $late_over_3 = (My_Controller_Action::rule_penalti($data[0]['total_late']) > 0.5) ? My_Controller_Action::rule_penalti($data[0]['total_late']) : 0;
            $tong_phat = $forget_check_in + $late_less_3 + $late_over_3 . ' ngày công';

            $content_new = '<body lang="EN-US" style="margin: 0px; height: auto; overflow: visible; width: 878px;" class="MsgBody MsgBody-html">
                <div ><div ><style></style><div class="WordSection1" >
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="984" style="width: 10.25in; border-collapse: collapse;">
                <tbody ><tr style="height: 15pt;">
                <td width="121" nowrap="" valign="bottom" style="width: 90.75pt; background: white; padding: 0in 5.4pt; height: 15pt;">
                <p class="MsoNormal" style="line-height: 115%;">
                <span style="font-family: &quot;times new roman&quot;, serif; color: black;">Chào bạn,</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="227" nowrap="" colspan="2" valign="bottom" style="width: 170.25pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="174" nowrap="" colspan="2" valign="bottom" style="width: 130.5pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="462" nowrap="" colspan="2" valign="bottom" style="width: 346.5pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td></tr><tr style="height: 15.75pt;">
                <td width="522" nowrap="" colspan="5" valign="bottom" style="width: 391.5pt; background: white; padding: 0in 5.4pt; height: 15.75pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">Phòng Nhân sự gửi bạn thông tin vi phạm chuyên cần tháng ' . $month . '/'.$year.'</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="462" nowrap="" colspan="2" valign="bottom" style="width: 346.5pt; background: white; padding: 0in 5.4pt; height: 15.75pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td></tr><tr style="height: 24.75pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border: 1pt solid windowtext; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b ><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Nội dung</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="203" nowrap="" style="width: 152.25pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpFirst" align="center" style="margin-left: 0.25in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b ><span style="font-size: 9pt; line-height: 150%; color: black;">2h &lt;&nbsp;&nbsp;Tổng số thời gian đi trễ&nbsp; <u>&lt;</u>&nbsp; 3h</span></b></p></td>'
                    . '<td width="168" nowrap="" colspan="2" style="width: 1.75in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpMiddle" align="center" style="margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b ><span style="font-size: 9pt; line-height: 150%; color: black;">Tổng số thời gian đi trễ&nbsp; &gt;&nbsp; 3h </span></b></p></td>'
                    . '<td width="240" nowrap="" colspan="2" style="width: 2.5in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpLast" align="center" style="margin-left: 0in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b ><span style="font-size: 9pt; line-height: 150%; color: black;">Quên check in từ lần thứ 3 trở đi </span></b></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 24.75pt;"><p class="MsoNormal" >&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border-top: none; border-left: 1pt solid windowtext; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b ><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Thời gian vi phạm</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="203" nowrap="" style=" text-align: center ; width: 152.25pt; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">' . $thoi_gian_1 . '</span></td>'
                    . '<td width="168" nowrap="" colspan="2" style=" text-align: center ; width: 1.75in; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">' . $thoi_gian_2 . '</span></td>'
                    . '<td width="240" nowrap="" colspan="2" style=" text-align: center ;  width: 2.5in; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">' . $forget_check_in_day . '</span></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" >&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border-top: none; border-left: 1pt solid windowtext; border-bottom: none; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b ><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Mức phạt</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="203" nowrap="" style="width: 152.25pt; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b ><i ><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: rgb(89, 89, 89);"></span></i></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">' . $phat_1 . '</span></p></td>'
                    . '<td width="168" nowrap="" colspan="2" style="text-align: center ; width: 1.75in; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">' . $thoi_gian_2_phat . '</span></td>'
                    . '<td width="240" nowrap="" colspan="2" style="width: 2.5in; border-top: none; border-left: none; border-bottom: 1pt solid rgb(166, 166, 166); border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><span style="font-size: 9pt;text-align: center; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: rgb(89, 89, 89);">' . $phat_forget_check_in_day . '</span></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" >&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b ><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="611" nowrap="" colspan="5" style="width: 458.25pt; border-top: 1pt solid rgb(166, 166, 166); border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid black; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b ><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Tổng mức phạt: ' . $tong_phat . '</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" >&nbsp;</p></td></tr><tr style="height: 32.25pt;">'
                    . '<td width="732" colspan="6" style="width: 549pt; background: white; padding: 0in 5.4pt; height: 32.25pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family:">Bạn vui lòng phản hồi nếu thông tin chưa chính xác hoặc cần điều chỉnh qua email phuongnga.nguyen@oppo-aed.vn hoặc hotline 3920 2555 - Ext: 105 gặp Ms. Nga Nhân sự.
                    <b >Deadline phản hồi trước ' . $time_dealine . '.</b> <br >
                    Phòng Nhân sự sẽ căn cứ nội dung trong mail để thực hiện xử phạt theo đúng quy định Công ty.</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 32.25pt;"><p class="MsoNormal" >&nbsp;</p></td></tr><tr >'
                    . '<td width="121" style="width: 90.75pt; padding: 0in;"></td>'
                    . '<td width="203" style="width: 152.25pt; padding: 0in;"></td>'
                    . '<td width="24" style="width: 0.25in; padding: 0in;"></td>'
                    . '<td width="144" style="width: 1.5in; padding: 0in;"></td>'
                    . '<td width="30" style="width: 22.5pt; padding: 0in;"></td>'
                    . '<td width="210" style="width: 157.5pt; padding: 0in;"></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in;"></td></tr></tbody></table><p class="MsoNormal" ><span style="color: rgb(31, 73, 125);">&nbsp;</span></p><div><table class="MsoNormalTable" cellspacing="0" cellpadding="0" style="border-collapse: collapse;"> <tbody > <tr style="height: 38.9pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"><p class="MsoNormal"><b style="color: #028553;font-family:sans-serif;font-size: 15px;">Ms.NGUYEN THI PHUONG NGA</b><br><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">C&B Specialist</span></p></td></tr><tr style="height: 37.5pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 37.5pt;"><p class="MsoNormal" ><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">T: <span class="Object" id="OBJ_PREFIX_DWT68_com_zimbra_phone"><a style="color: black;" href="callto:(84-8) 3920 2555" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()">(84-8) 3920 2555</a></span> - Ext: 105 <br>E: <span class="Object" role="link" id="OBJ_PREFIX_DWT69_ZmEmailObjectHandler"><a href="mailto:phuongnga.nguyen@oppo-aed.vn" target="_blank">phuongnga.nguyen@oppo-aed.vn</a></span><br> M: <span class="Object" id="OBJ_PREFIX_DWT68_com_zimbra_phone"><a href="callto:(84) 036 6001 601" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()" style="color: black;">(84) 036 6001 601</a></span> <br></span></span><span style="color: black;"></span> </p> </td> </tr> <tr style="height: 38.9pt;"> <td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"> <p class="MsoNormal"> <b style="color: #028553; font-size: 12px;font-family:arial, sans-serif;">OPPO Authorized Exclusive Distributor<br>CÔNG TY CỔ PHẦN KỸ THUẬT & KHOA HỌC VĨNH KHANG</b> <span style="font-size: 10pt; font-family: arial, sans-serif; color: black;"><br >12<sup >th</sup><b >&nbsp;</b>Floor, Lim II Tower<br>No. 62A CMT8 St., Dist. 3, HCMC, VN</span><span style="color: rgb(31, 73, 125);"></span> </p> </td> </tr> <tr style="height: 16.5pt;"> <td width="374" style="width: 3.9in; padding: 0in 5.4pt; height: 16.5pt;"> <p class="MsoNormal" ><span style="color: #028553;"><span class="Object" role="link" id="OBJ_PREFIX_DWT71_com_zimbra_url"><a style="text-decoration-color:#028553" href="http://www.oppomobile.vn/" target="_blank" ><span style="font-size: 10pt; font-family: arial, sans-serif; color:#028553;font-weight: 600;">www.oppomobile.vn</span></a></span></span> </p> </td> </tr> </tbody> </table><p class="MsoNormal" ><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div><p class="MsoNormal" ><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div></div></div></body>';

            $subject = 'THÔNG BÁO VI PHẠM CHUYÊN CẦN THÁNG ' . $month . '/' . $year;
            if ($check_email) {
                $mailto[] = 'congtrang.huynh@oppo-aed.vn';
//                $ccStaff[] = 'dieptruc.nguyen@oppo-aed.vn';
            } else {
                $mailto[] = $data[0]['email'];
            }

            $res = $this->sendmailnew($mailto, $subject, $content_new, NULL, $ccStaff);
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            if ($res == 1) {
                $data_send_mail = array(
                    'staff_id' => $staff_id,
                    'month' => $month,
                    'year' => $year,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id
                );
//                  $QLogSendMail->insert($data_send_mail);
                $reponse['status'] = 1;
                $reponse['message'] = "Thành công";
            } else {
                $reponse['status'] = $res;
                $reponse['message'] = "Lỗi";
            }

            echo json_encode($reponse);
        }
    }

    public function sendAllEmailAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'send-all-email.php';
    }
    
    public function testMailAction(){
       
        /////////////////////////
        /////// CONFIG
        /////////////////////////
        error_reporting(~E_ALL);
        ini_set("display_error", 0);
        set_time_limit(0);
        $db         = Zend_Registry::get('db');
        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config     = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

        // Thay mặt cho BCH Công đoàn và BGĐ Công ty
        /////////////////////////
        /////// GET STAFFs HAVING BIRTHDAY TODAY
        /////////////////////////


        $sql = "SELECT s.email, s.code,CONCAT(s.firstname,' ',s.lastname) AS fullname,s.joined_at,s.title,DATEDIFF(CURDATE(),s.joined_at) AS diff ,ROUND(DATEDIFF(CURDATE(),s.joined_at)/365) AS nam  
FROM staff s 
WHERE s.off_date IS NULL and DATE(s.joined_at)<CURDATE()
AND s.email IS NOT NULL  and s.id = 9289
HAVING DAYOFMONTH(s.joined_at)=DAYOFMONTH(CURDATE()) AND MONTH(s.joined_at)=MONTH(CURDATE()) ";
//                $sql    = "SELECT s.email, s.code,CONCAT(s.firstname,' ',s.lastname) AS fullname,s.joined_at,s.title,DATEDIFF(CURDATE(),s.joined_at) AS diff ,ROUND(DATEDIFF(CURDATE(),s.joined_at)/365) AS nam
//FROM staff s
//WHERE s.off_date IS NULL
//AND s.email IS NOT NULL
//HAVING DAYOFMONTH(s.joined_at)=DAYOFMONTH('2018-03-28') AND MONTH(s.joined_at)=MONTH('2018-03-28')
//                ";
//        $staffs = $db->query($sql);
        $stmt   = $db->prepare($sql);
        $stmt->execute();
        $staffs = $stmt->fetchAll();

//        print "<pre>".print_r($staffs)."</pre>";
//        die;
        /////////////////////////
        /////// SEND EMAIL
        /////////////////////////



            foreach ($staffs as $k => $staff) {
                try {
                       

                    $list_email = $this->getMail($staff['email'], 1);
					
//	
                    if (!empty($list_email) && count($list_email)) {
                        $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

                        $mail = new My_Mail($app_config->mail->smtp->charset);

                        $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);

                        $fname = trim($this->mb_ucwords($staff['fullname']));
                        $mail->setSubject('Chúc mừng bạn đã đồng hành ' . $staff['nam'] . ' năm cùng gia đình OPPO ');


                        /*
                          TẠO HÌNH ẢNH
                         */
                        // Set the content-type
                        //header('Content-Type: image/png');
                        // Create the image
                        $im = imagecreatefrompng(APPLICATION_PATH . "/../public/photo/mail/remind/" . $staff['nam'] . ".png");
//                    $im = imagecreatefromjpeg(APPLICATION_PATH . "/../public/photo/mail/birthday/thiep-2019-0" . rand(1, 4) . ".jpg");

                        // Create some colors
                        $white = imagecolorallocate($im, 255, 255, 255);
                        $grey = imagecolorallocate($im, 128, 128, 128);
                        $black = imagecolorallocate($im, 0, 0, 0);

                        $green = imagecolorallocate($im, 0, 139, 86);
                        $pink = imagecolorallocate($im, 232, 120, 139);

                        // The text to draw
                        $name = $fname;

                        // Replace path by your own font path

                        $font_b = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';

                        // Add the text
                        imagettftext($im, 20, 0, (460 - (mb_strlen($name, "UTF-8") * 14 / 2)), 200, $green, $font_b, $name);


                        $date_cur = date('Y-m-d');
                        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                            DIRECTORY_SEPARATOR . 'remind' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

                        if (!is_dir($uploaded_dir)) {
                            $old = umask(0);
                            @mkdir($uploaded_dir, 0777, true);
                            umask($old);
                        }
                        imagepng($im, $uploaded_dir . $staff['code'] . '.png');
                        imagedestroy($im);
                        /*
                          END TẠO HÌNH ẢNH
                         */


                        //end lưu nội dung thành hình ảnh
                        $img_birthday = "<div><table><tr><td><img src='" . HOST . "photo/remind/" . $date_cur . "/" . $staff['code'] . ".png'/></td></tr></table></div>";
                        $img_birthday .= $this->hr_signature;

                        $mail->setBodyHtml($img_birthday);

                        $mailTo = array();
                        $mailBcc = array();
                        echo $staff['email'];
                        $mail->addTo($staff['email']);
//                $mail->addTo('thinh.phan@oppo-aed.vn');

                        $list_email['bcc'] = 'oppofamily@oppo-aed.vn,daithanh.nguyen@oppo-aed.vn,khahoang.le@oppo-aed.vn,vandiem.nguyen@oppo-aed.vn,tanthinh.phan@oppo-aed.vn';
                        if (!empty($list_email['bcc'])) {
                            $mailBcc = explode(",", $list_email['bcc']);
                            $mail->addBcc($mailBcc);
                        }
//                echo "<pre>";
//                print_r($list_email['bcc']);
//                $mail->addBcc('anhthoai.vo@oppo-aed.vn');
                       $r = $mail->send($transport);
                        sleep(1);
                    }
                }
                catch( Exception $e)
                {
                    var_dump($e->getMessage());
                }
            }
            echo "<pre>";
            print_r('ok');

            exit;
        
    }
 private function mb_ucwords($str) {
        $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
        return ($str);
    }
    private function sendmailnew($to, $subject, $maildata, $image = '',$ccStaff){
         //todo send mail
        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => 'phuongnga.nguyen@oppo-aed.vn',
            'password' => 'Nga119.94',
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );
        $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);
        $mail = new Zend_Mail($app_config->mail->smtp->charset);
        $mail->setBodyHtml($maildata);
        $mail->setFrom($config['username'], $config['username']);
        $mail->addTo($to);
        $mail->setSubject($subject);
        foreach ($ccStaff as $k => $emailStaff) {
              $mail->addCc($emailStaff);
        }
        
        $r = $mail->send($transport);
        if(!$r ) {
            return -1;
        } else {
            return 1;
        }
        
    }
    private function sendmail($to, $subject, $maildata, $image = '',$ccStaff){
        
            
            
            exit();
            
        include_once APPLICATION_PATH.'/../library/phpmail/class.phpmailer.php';
        $app_config   = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config       = $app_config->mail->smtp;

        $mailfrom     = 'dieptruc.nguyen@oppomobile.vn';
    // $mailfrom     = $config->user2;
        $mailfromname = $mailfrom;
        $mail = new PHPMailer();

        $body = $maildata; // nội dung email
        // $body = eregi_replace("[\]",'',$body);
//	$body = preg_replace("[\]",'',$body);

        $mail->IsHTML();

        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = $config->ssl;                 // sets the prefix to the servier // Config this
        $mail->Host       = $config->host;      // sets GMAIL as the SMTP server // Config this
        $mail->Port       = $config->port;                   // set the SMTP port for the GMAIL server // Config this
       // $mail->Username   = $config->user2;  // GMAIL username // Config this
        $mail->Username   = 'dieptruc.nguyen@oppomobile.vn';  // GMAIL username // Config this
     //   $mail->Password   = $config->pass2;            // GMAIL password // Config this
        $mail->Password   = 'dieptruc20191';            // GMAIL password // Config this
        
        $mail->From = $mailfrom;
        $mail->FromName = $mailfromname;

        $mail->SetFrom($mailfrom, $mailfromname); //Định danh người gửi

        //$mail->AddReplyTo($mailfrom, $mailfromname); //Định danh người sẽ nhận trả lời

//        $mail->Subject    = $subject; //Tiêu đề Mail
        $mail->Subject    = "=?UTF-8?B?".base64_encode($subject)."?="; //Tiêu đề Mail


        $mail->AltBody    = "Để xem tin này, vui lòng bật chế độ hiển thị mã HTML!"; // optional, comment out and test

        // $mail->AddAttachment($image);
        if($image != '')
            $mail->AddEmbeddedImage($image, 'embed_img');

        $mail->MsgHTML($body);

        if (is_array($to)) {
            foreach ($to as $key => $value) {
                $mail->AddAddress($value, ''); //Gửi tới ai ?
            }
        } else {
            $mail->AddAddress($to, ''); //Gửi tới ai ?
        }
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QStaff = new Application_Model_Staff();
        $select_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $infoStaff = $QStaff->fetchRow($select_staff);
        $emailInfo      = $infoStaff->email;
//        $mail->addBcc($emailInfo);
        
        foreach ($ccStaff as $k => $emailStaff) {
              $mail->addCc($emailStaff);
        }
       
        if(!$mail->Send() ) {
//             echo date('Y-m-d H:i:s')." - Failed\n";
            return -1;
        } else {
            //echo date('Y-m-d H:i:s')." - Done\n"; //DEBUG
            return 1;
        }

    }
    
    public function getManagerAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'get-manager.php';
    }
    public function saveTestEmailAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'save-test-email.php';
    }

    public function listManagerDepartmentAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'list-manager-department.php';
    }
    public function sendEmailManagerDepartmentAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'send-email-manager-deparment.php';
    }
        public function exportTemplateTimeInMonthAction()
    {
        include 'PHPExcel/IOFactory.php';
        // no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        $heads = array(
            'Policy Group',
            'Company Group',
            'Company',
            'Department',
            'Team',
            'Title',
            'Code',
            'CMND',
            'Name',
            'Area',
            'Join date',
            'Off date',
            'Tổng công bình thường',
            'Ngày công bình thường tính lương',
            'Ngày công chủ nhật đi làm',
            'Ngày đi làm lễ x3',
            'Ngày lễ đi làm x4',
            'Ngày công thứ 7',
            'Phép năm sử dụng trong tháng',
            'Ứng phép/ Hoàn phép do nhân viên nghỉ việc',
            'Nghỉ việc riêng hưởng lương',
            'Ngày điều chỉnh',
            'Công trước điều chỉnh',
            'Công sau điều chỉnh',
            'Nghỉ được hưởng lương',
        );
        $PHPExcel->setActiveSheetIndex(0);

        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;

        try {
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit("Group 1", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("Local", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("OPPO", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("SALES", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("SALES", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("RSM", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("1304KD12AAA", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("089090000008", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("TRẦN TUẤN ANH", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("VINH LONG", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("3/18/2013", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("26", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("26.0", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("26.0", PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
        } catch (exception $e) {
            exit;
        }
        $filename = 'Template_upload';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function importTemplateTimeInMonthAction()
    {
        $month_year=$this->getRequest()->getParam("month_year");
        $month = date("m", strtotime($month_year));
        $year = date("Y", strtotime($month_year));

        include 'PHPExcel/IOFactory.php';
        set_time_limit(0);
        ini_set('memory_limit', -1);
//        $progress = new My_File_Progress('parent.set_progress');
//        $progress->flush(0);

        $save_folder = 'total-time-by-month';
        $new_file_path = '';
        $requirement = array(
//            'Size' => array('min' => 5, 'max' => 50000000),
            'Count' => array('min' => 1, 'max' => 1),
            'Extension' => array('xls', 'xlsx'),
        );
        $QTotalTimeByMonth=new Application_Model_TotalTimeByMonth();
        try {
            $file = My_File::get($save_folder, $requirement, true);
            if (!$file || !count($file))
                throw new Exception("Upload failed");
            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                . DIRECTORY_SEPARATOR . $file['folder'];
            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

//             My_File_Progress::set('mou', 0);
        } catch (Exception $e) {

            $this->view->errors = $e->getMessage();

            echo $e;
            exit;
        }

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);

            $path_info = pathinfo($inputFileName);
            $extension = $path_info['extension'];

            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            echo $e;
            exit;
        }
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        try {
            for ($row = 2; $row <= $highestRow; $row++) {
                // row=1 is title of sheet

                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $join_date=(!empty(date("Y-m-d",strtotime($rowData[0][11]))) AND date("Y", strtotime($rowData[0][10])) != 1970) ? date("Y-m-d",strtotime($rowData[0][10])):null;
                $off_date=(!empty(date("Y-m-d",strtotime($rowData[0][11]))) AND date("Y", strtotime($rowData[0][11])) != 1970) ? date("Y-m-d",strtotime($rowData[0][11])):null;
                $editing_date=(!empty(date("Y-m-d",strtotime($rowData[0][20]))) AND date("Y", strtotime($rowData[0][11])) != 1970) ? date("Y-m-d",strtotime($rowData[0][20])):null;
                $dataInput = array(
                    "policy_group" => (string)$rowData[0][0] ,
                    "company_group" => (string)$rowData[0][1],
                    "company" => (string)$rowData[0][2],
                    "department" => (string)$rowData[0][3],
                    "team" => (string)$rowData[0][4],
                    "title" => (string)$rowData[0][5],
                    "staff_code" => (string)$rowData[0][6],
                    "identity" => (string)$rowData[0][7],
                    "name" => (string)$rowData[0][8],
                    "area" => (string)$rowData[0][9],
                    "join_date" => $join_date,
                    "off_date" =>  $off_date,
                    "congthuong" => floatval($rowData[0][12]),
                    "congthuongtinhluong" => floatval($rowData[0][12]),
                    "sunday" => floatval($rowData[0][14]),
                    "holidayX3" => floatval($rowData[0][15]),
                    "holidayX4" => floatval($rowData[0][16]),
                    "saturday" => floatval($rowData[0][17]),
                    "phep" => floatval($rowData[0][18]),
                    "ungphep" => floatval($rowData[0][19]),
                    "nghiviecrienghuongluong" => floatval($rowData[0][20]),
                    "editing_date" => $editing_date ,
                    "total_before" =>floatval($rowData[0][21]) ,
                    "total_after" => floatval($rowData[0][23]),
                    "nghiviechuongluong" => floatval($rowData[0][24]),
                    "month"=> $month,
                    "year"=> $year,
                );

                $QTotalTimeByMonth->insert($dataInput);

            }
        } catch (Exception $e) {
            echo $e;
            exit;
        }
        $this->redirect(HOST . "time-machine/late-list-new");

    }

    public function exportDataTimeInMonthAction()
    {
        $month_year=$this->getRequest()->getParam("month_year");
        $month = date("m", strtotime($month_year));
        $year = date("Y", strtotime($month_year));

        $QTotalTime = new Application_Model_TotalTimeByMonth();
        $total = 0;
        $department=$this->getRequest()->getParam('department', null);
        $team=$this->getRequest()->getParam('team', null);
        $title=$this->getRequest()->getParam('title', null);
        $month_year= $this->getRequest()->getParam('month_year');
        if(!empty($month_year)){
            $month = date("m", strtotime($month_year));
            $year = date("Y", strtotime($month_year));
        }


        $params = array(
            'department' => $department,
            'team' => $team,
            'title' => $title,
            'month' => !empty($month) ? $month:null,
            'year' => !empty($year) ? $year:null,
        );
        $data = $QTotalTime->fetchPagination(null, null, $total, $params);
        include 'PHPExcel/IOFactory.php';
        // no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        $heads = array(
            'Policy Group',
            'Company Group',
            'Company',
            'Department',
            'Team',
            'Title',
            'Code',
            'CMND',
            'Name',
            'Area',
            'Join date',
            'Off date',
            'Tổng công bình thường',
            'Ngày công bình thường tính lương',
            'Ngày công chủ nhật đi làm',
            'Ngày đi làm lễ x3',
            'Ngày lễ đi làm x4',
            'Ngày công thứ 7',
            'Phép năm sử dụng trong tháng',
            'Ứng phép/ Hoàn phép do nhân viên nghỉ việc',
            'Nghỉ việc riêng hưởng lương',
            'Ngày điều chỉnh',
            'Công trước điều chỉnh',
            'Công sau điều chỉnh',
            'Nghỉ được hưởng lương',
        );
        $PHPExcel->setActiveSheetIndex(0);

        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;

        try {
            foreach ($data as $key => $value){
                $alpha = 'A';

                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["policy_group"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["company_group"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["company"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["department"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["team"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["title"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["staff_code"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["identity"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["name"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["area"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["join_date"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["off_date"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["congthuong"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["congthuongtinhluong"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["sunday"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["holidayX3"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["holidayX4"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["saturday"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["phep"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["ungphep"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["nghiviecrienghuongluong"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["editing_date"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["total_before"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["total_after"], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($value["nghiviechuongluong"], PHPExcel_Cell_DataType::TYPE_STRING);


                $index++;

            }

        } catch (exception $e) {
            exit;
        }
        $filename = 'Data_staff_time';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
    public function uploadTimeFinalAction(){
    }


}
