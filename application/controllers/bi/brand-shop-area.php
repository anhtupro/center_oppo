<?php
$sort            = $this->getRequest()->getParam('sort');
$desc            = $this->getRequest()->getParam('desc', 1);
$export          = $this->getRequest()->getParam('export', 0);
$dealer_id       = $this->getRequest()->getParam('dealer_id', 0);
$from            = $this->getRequest()->getParam('from', date('01/m/Y') );
$to              = $this->getRequest()->getParam('to', date('d/m/Y') );
$area            = $this->getRequest()->getParam('area');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QKpiMonth   = new Application_Model_KpiMonth();
$QStore      = new Application_Model_Store();
$QArea       = new Application_Model_Area();

if($from){
    $from = date("Y-m-d 00:00", strtotime($from) );
}

if($to){
    $to = date("Y-m-d 23:59:59", strtotime($to) );
}

$params = array(
    'sort'      => $sort,
    'desc'      => $desc,
    'from'      => $from,
    'to'        => $to,
    'area'      => $area,
    'export'    => $export,
);

$params['is_brand_shop'] = 1;
$brand_shop = $QStore->getStoreBrandshop($params);
$brand_shop_id = [];
foreach ($brand_shop as $key => $value) {
    $brand_shop_id[] = $value['id'];
}
$params['brand_shop_id'] = $brand_shop_id;

$sales = $QKpiMonth->getSelloutGoodBrandShopArea($params);
$sellout = [];
$total = 0;
$total_value = 0;
foreach($sales as $key=>$value){
    $sellout[$value['area_id']] = array(
                                    'num' => $value['num'],
                                    'total_value' => $value['total_value'],
                                    );
    $total += $value['num'];
    $total_value += $value['total_value'];
}


$this->view->total = $total;
$this->view->total_value = $total_value;

$this->view->areas = $QArea->get_cache();

$this->view->total_sales           = $total_sales['total_quantity'];
$this->view->total_sales_activated = $total_sales['total_activated'];
$this->view->sellout               = $sellout;
$this->view->url                   = HOST.'timing/analytics-area'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->desc                  = $desc;
$this->view->current_col           = $sort;
$this->view->to                    = $to;
$this->view->from                  = $from;
$this->view->params                = $params;





