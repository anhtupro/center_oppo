<?php 
    $id              = $this->getRequest()->getParam('id');
    $current_date    = date('Y-m-d H:i:s');
    $QStaff          = new Application_Model_Staff();
    $QLessonScores   = new Application_Model_LessonScores();
    $QKpiMonth       = new Application_Model_KpiMonth();
    $QGood           = new Application_Model_Good();
    $QAsm            = new Application_Model_Asm();
    $QStoreStaffLog  = new Application_Model_StoreStaffLog();
    $QNotificationPgs  = new Application_Model_NotificationPgs();
    $QNotificationTrainer   = new Application_Model_NotificationTrainer();
    $QSpAssessmentApprove   = new Application_Model_SpAssessmentApprove();
    $QCertificate           = new Application_Model_Certificate();
    $QCertificateStaff      = new Application_Model_CertificateStaff();   
    $QSpAssessmentScores    = new Application_Model_SpAssessmentScores();

    $certificate            = $QCertificate->getData(array());        
    $certificate_staff      = $QCertificateStaff->getAll($certificate['id']);
    $this->view->certificate        = $certificate;
    $this->view->certificate_staff  = $certificate_staff;
   
    
    $params = array();

    // get Staff
    $staff = $QKpiMonth->getStaffById($id);
    $area_staff_id = $staff['area_id'];

    $upstar_date = $QKpiMonth->getUpstarDate($id);

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $group_id = $userStorage->group_id;

    if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list_check'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    
    }

    // lấy khu vực của trainer
    $area = $QAsm->get_cache($id);
    $list_area = $area['area'];
    $params['area_list'] = $list_area;


    //Kiểm tra pgs này có nằm trong trainer team ko
    if($group_id == TRAINING_TEAM_ID AND $userStorage->id != 910){
        if(!in_array($area_staff_id, $params['area_list_check'])){
            $this->_redirect(HOST);
            exit;
        }
    }
    

    if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        if($group_id == ASM_ID){
            if((!in_array($staff['area_id'], $params['area_list_check']))){
                $this->_redirect(HOST);
                exit;
            }
        }
        elseif($group_id == LEADER_ID){
            $params['leader_id'] = $userStorage->id;

            //lấy danh sách PG theo SALE quản lý
            $list_sale = $QKpiMonth->getListSale($params);
            $list_sale_array = array();
            foreach ($list_sale as $key => $value) {
                $list_sale_array[] = $value['staff_id'];
            }

            $params['sale_id'] = $list_sale_array;
            $list_pg = $QKpiMonth->getListPg($params);  
            
            $list_pg_array = array();
            foreach ($list_pg as $key => $value) {
                $list_pg_array[] = $value['staff_id'];
            }

            //lấy danh sách PG theo quản lý trực tiếp
            // get List store
            $db = Zend_Registry::get('db');
            $arrCols = array(
                's.name',
                's.id AS store_id',
                'p.joined_at',
                'p.released_at',
                'num' => 'COUNT(i.imei_sn)',
                'level' => 'lp.name'
            );
            $select = $db->select()
                ->from(array('p'=>'store_staff_log'), $arrCols)
                ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
                ->joinLeft(array('i'=>'imei_kpi'), "i.store_id = s.id AND i.timing_date >= '".date('Y-m-01')."'",array())
                ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id',array())
                ->joinLeft(array('l'=>'dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)',array())
                ->joinLeft(array('lp'=>'loyalty_plan'), 'lp.id = l.loyalty_plan_id',array())
                ->where("p.staff_id = ?", $params['leader_id'])
                ->where('released_at IS NULL', NULL)
                ->group('s.id')
                ->order('num DESC')
            ;
            
            $list_store = $db->fetchAll($select);

            // get list sale, pg
            $list_store_arr = array();
            foreach ($list_store as $key => $value) {
                $list_store_arr[] = $value['store_id'];
            }
            $list_sale_pg = $QStoreStaffLog->get_list_sale($list_store_arr, $current_date);
            $list_sale_pg_array = array();
            foreach ($list_sale_pg as $key => $value) {
                $list_sale_pg_array[] = $value['staff_id'];
            }
            //END 


            if((!in_array($id, $list_pg_array)) AND (!in_array($id, $list_sale_pg_array))){
                $this->_redirect(HOST);
                exit;
            }
        }
        elseif($group_id == SALES_ID){
            $params['sale_id'] = $userStorage->id;
            $list_pg = $QKpiMonth->getListPg($params);

            $list_pg_array = array();
            foreach ($list_pg as $key => $value) {
                $list_pg_array[] = $value['staff_id'];
            }

            if((!in_array($id, $list_pg_array))){
                $this->_redirect(HOST);
                exit;
            }
        }
        elseif($group_id == PGPB_ID){
            if($id != $userStorage->id){
                $this->_redirect(HOST);
                exit;
            }
        }
    }
    //END 
	
    if($staff['title'] == PB_SALES_TITLE){
        $params['pb_sale_id'] = $id;
    }
    else{
        $params['pg_id'] = $id;
    }

    // get List store
    $db = Zend_Registry::get('db');
    $arrCols = array(
        's.name',
        's.id AS store_id',
        'p.joined_at',
        'p.released_at'
    );
    $select = $db->select()
        ->from(array('p'=>'store_staff_log'),$arrCols)
        ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
        ->where("p.staff_id = ?",$id)
        ->order('p.id DESC')
    ;

    $transfer = $db->fetchAll($select);

    //get training Online
    $arrCols = array(
        'l.title',
        'p.staff_cmnd',
        'p.lesson_id',
        'dtb' => 'AVG(p.scores)',
        'ketqua' => '(CASE WHEN (max(p.scores) = 10) THEN "PASS" ELSE "FAIL" END)'
    );
    $select = $db->select()
        ->from(array('p'=>'lesson_scores'), $arrCols)
        ->joinLeft(array('l'=>'lesson'), 'p.lesson_id = l.id',array())
        ->joinLeft(array('s'=>'staff'), 's.ID_number = p.staff_cmnd',array())
        ->where("s.id = ?",$id)
        ->group('p.lesson_id')
        ->order('l.created_at DESC')
    ;
    $list_lesson = $db->fetchAll($select);
    $this->view->list_lesson = $list_lesson;
    
    
    //List sale
    $this->view->list_date = $list_date;
    $this->view->staff = $staff;
    $this->view->resultStaffTransfer = $transfer;

    $this->_helper->viewRenderer->setRender('staff/pg-details');


    //Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );

    $name_chart = array(
        '2316'  => 'TGDĐ',
        '2363'  => 'FPT',
        '2317'  => 'VTA',
        '9187'  => 'Vinpro',
        '10007' => 'VIETTEL',
        '2325'  => 'CES',
        '3'     => 'DIAMOND',
        '2'     => 'GOLD',
        '1'     => 'SILVER',
        '4'     => 'NORMAL',
        '999'   => 'OTHER KA',
        '888'   => 'RETURN',
    );

    //$params = array();
    $params['leader_id'] = null;
    $params['sale_id'] = null;
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
    $params['to'] = date('Y-m-d');
	
    if($staff['title'] == 545){
        $stmt = $db->prepare("CALL `PR_getSelloutByDealer`(:p_staff_id,:_from,:_to)");
        $_from = $params['from'];
        $_to = $params['to'];
			   
        $stmt->bindParam('p_staff_id', $id, PDO::PARAM_INT);
        $stmt->bindParam('_from',$_from , PDO::PARAM_STR);
        $stmt->bindParam('_to', $_to, PDO::PARAM_STR);
        $stmt->execute();
		$sell_out_dealer = $stmt->fetchAll();
		$stmt->closeCursor();

    }else{
	$sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
    }
    
    $data_good = $QKpiMonth->getSelloutGood($params);

    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
            $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
        }
        else{
            $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
        }
                                            
    }
    /* END sellout, total value theo từng kênh */
	
    $this->view->data_sellout_dealer = $data_sellout_dealer;
    $this->view->data_sellout_total = $data_sellout_total;
    $this->view->list_date = $list_date;
    $this->view->name_chart = $name_chart;

    $this->view->data_sellout_good = $data_good;
    $this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
    $this->view->good = $QGood->get_cache();

    /* Tích điểm */
    $StaffScoresView = $QStaff->fetchPaginationScoresById($id);
	 
    $this->view->StaffScoresView = $StaffScoresView;
    $this->view->id = $id;
    /* END Tích điểm */

    /* sellout by date */
    $params['from'] = date('Y-m-01', strtotime(date('Y-m-d')." -1 month"));
    $data_good_by_date = $QKpiMonth->getGoodByDatePgs($params);
    $good_by_date = array();
    foreach ($data_good_by_date as $key => $value) {
        $good_by_date[$value['date']][$value['good_id']] = $value['num'];
        $good_by_date[$value['date']][9999] += $value['num'];
    }
    $list_date_range = $this->date_range($params['from'], $params['to']);
    $this->view->good_by_date = $good_by_date;
    $this->view->list_date_range = $list_date_range;

    //Số bán hàng ngày
    $good_hero_total = array(
        HERO_ID => HERO_NAME,
        '9999' => 'Total'
    );
    $this->view->good_hero_total = $good_hero_total;
    /* end sellout by date */

    /* DASHBOARD SALE */
    $ik = new Application_Model_NewImeiKpi();
    $data = array(
        'staff_id' => $id,
        'from' => date('Y-m-01 00:00:00'),
        'to' => date('Y-m-d H:i:s')
    );
    $data = $ik->GetAll($data);
    $this->view->res = $data;


    $QInform = new Application_Model_Inform();
    $params_inform = array(
                'staff_id' => $id,
                'filter'   => true,
                'status'   => 1,
                );
    $params_inform['group_cat'] = 1;
    $params_inform['filter_display'] = 1;
    $page = 1;
    $limit = 5;
    $total = 0;
    $inform = $QInform->fetchPagination($page, $limit, $total, $params_inform);
    $this->view->informs = $inform;
    /* END */


    /*Lấy điểm vi phạm*/
    $db = Zend_Registry::get('db');
    $select = $db->select();
    $select = $db->select()->from(array('p'=> 'staff_reward_warning'), array('p.*'));
    $select->joinLeft(array('r' => 'reward_warning_type'), 'r.id = p.warning_type', array('warning_title'=>'r.title', 'score'=>'r.score'));
    $select->where('p.staff_id = ?',$id);
    $select->where('p.type = ?',2); 
    $select->where('p.del <> ?',1);
    $list = $db->fetchAll($select);
    $this->view->list=$list;

    $total_phat = 0;
    foreach($list as $key=>$value){
        $total_phat = $total_phat + $value['score'];
    }
    $this->view->total_phat = $total_phat;

    /*Lấy điểm thưởng*/
    $db = Zend_Registry::get('db');
    $select = $db->select();
    $select = $db->select()->from(array('p'=> 'staff_reward_warning'), array('p.*'));
    $select->joinLeft(array('r' => 'reward_warning_type'), 'r.id = p.warning_type', array('warning_title'=>'r.title', 'score'=>'r.score'));
    $select->where('p.staff_id = ?',$id);
    $select->where('p.type = ?',1);
    $select->where('p.del <> ?',1);
    $diemthuong = $db->fetchAll($select);
    $this->view->diemthuong = $diemthuong;

    $total_thuong = 0;
    foreach($diemthuong as $key=>$value){
        $total_thuong = $total_thuong + $value['score'];
    }
    $this->view->total_thuong = $total_thuong;


    $other_scores = $QSpAssessmentScores->getOtherScores($id);
    $this->view->other_scores = $other_scores;

    /* Lấy danh sách notification */
    if($group_id == PGPB_ID){
        $params_noti = [
            'area'              => $staff['area_name'],
            'staff_id_read'     => $userStorage->id
        ];
        $notification_area = $QNotificationPgs->getNotificationPgs($params_noti);
        
        $notification_trainer = $QNotificationTrainer->getNotificationTrainer($params_noti);
    }
    $this->view->notification_area = $notification_area;
    $this->view->notification_trainer = $notification_trainer;
    /* END Lấy danh sách notification */


    /* Lấy danh sách notification theo tháng */
    if($group_id == PGPB_ID){
        $params_noti = [
            'staff_id'     => $userStorage->id
        ];
        $notification_month = $QNotificationPgs->getNotificationMonth($params_noti);
    }
    $this->view->notification_month = $notification_month;
    /* END lấy danh sách notification theo tháng */


    /* Lấy điểm chi tiết training online */
    //$scores_training = $QLessonScores->getLessonScroreStaff($params);
    $scores_training = $QLessonScores->getLessonScroreByStaff($id);
    $this->view->scores_training = $scores_training;
    /* END - Lấy điểm chi tiết training online */

    $this->view->upstar_date = $upstar_date;


?>