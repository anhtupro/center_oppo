<?php 

    $id         		= $this->getRequest()->getParam('id');
    $QTitle          		= new Application_Model_Team();
    $QStaff         		= new Application_Model_Staff();
    $QKpiMonth        		= new Application_Model_KpiMonth();
    $QGood            		= new Application_Model_Good();
    $QArea            		= new Application_Model_Area();
    $QAsm            		= new Application_Model_Asm();
    $QStaffTrainingReport 	=  new Application_Model_StaffTrainingReport();
    $QTrainerCourseType 	=  new Application_Model_TrainerCourseType();
    $QTrainerInventory  	=  new Application_Model_TrainerInventory();
    $QNotificationPgs  		= new Application_Model_NotificationPgs();
    $QStaffTraining             = new Application_Model_StaffTraining();
    $QNotificationTrainer       = new Application_Model_NotificationTrainer();
    $QLesson                    = new Application_Model_Lesson();
    $QRankStarSale= new Application_Model_RankStarSales();

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $id = !empty($id) ? $id : $userStorage->id;
    
    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );
    
    
    $list_date_off = array(
        date('Y-m-d', strtotime(date('Y-m')." -4 month")),
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );
    
    
    
    $params = array(
        'trainer_id' => $id
    );

    
    $staff = $QKpiMonth->getStaffById($id);
    $transfer = $QKpiMonth->getStaffTransfer($id);
    
    if($staff['title'] == PB_SALES_TITLE)
        $params['pb_sale_id'] = $id;
    else
        $params['sale_id'] = $id;


    $area = $QAsm->get_cache($id);
    $params['area_list'] = !empty($area['area']) ? $area['area'] : [0];
    
    

    $row_trainer = $QStaffTrainingReport->reportDataBi($params);
    $row_trainer_course = $QStaffTrainingReport->reportDataBiCourse($params);
    
    $data_trainer = array();
    foreach ($row_trainer as $key => $value) {
        $data_trainer[$value['type']][$value['month_date']][$value['year_date']] = array(
                                                                    'sum' => $value['sum'],
                                                                    'quantity_session' => $value['quantity_session']
                                                                    );
    }

    
    $data_trainer_course = array();
    foreach ($row_trainer_course as $key => $value) {
        $data_trainer_course[$value['type']][$value['month_date']][$value['year_date']] = array(
                                                                    'quantity_pg' => $value['quantity_pg'],
                                                                    'quantity_pass' => $value['quantity_pass'],
                                                                    'quantity_course' => $value['quantity_course']
                                                                    );
    }
    
    $list_inventory  = $QTrainerInventory->fetchInventoryByUser($params);
    
    $list_sale_psg = unserialize(LIST_PGS_BI);
    $params['list_title_staff']=$list_sale_psg; 
    $params['cat_id']=1; //KienThucSanPham parent_id=1
    $row_lesson = $QKpiMonth->reportDataLesson($params);
    
    $trainer_course_type = $QTrainerCourseType->get_cache();
    $total_sale_pgs = $QStaff->getTotalSalePgs($params);

    
    
    $row_lesson_sales = $QLesson->reportLessonSales($params);

    $total_sale_sale_leader = $QLesson->getTotalSaleSaleLeader($params);
    $total_pass_lesson = $QLesson->getPassLesson($params);
    
    //END SALE TRAINING
    
    if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        if($userStorage->group_id == ASM_ID){
            if((!in_array($staff['area_id'], $params['area_list_check']))){
                $this->_redirect(HOST);
                exit;
            }
        }
        elseif($userStorage->group_id == LEADER_ID){
            $params['leader_id'] = $userStorage->id;
            $list_sale = $QKpiMonth->getListSale($params);
            $list_sale_array = array();
            foreach ($list_sale as $key => $value) {
                $list_sale_array[] = $value['staff_id'];
            }

            if((!in_array($id, $list_sale_array))){
                $this->_redirect(HOST);
                exit;
            }
        }elseif($userStorage->group_id == SALES_ID){
            if($userStorage->id != $id){
                $this->_redirect(HOST);
                exit;
            }
        }
    }
    
    // Lấy total sell out
    /*
    $list_date_sellout = $QKpiMonth->getSellOut_($params);
    $sell_out=array();
    foreach ($list_date_sellout as $key => $value) {
        $sell_out[$value['year']][$value['month']][$value['sellout_type']] = $value;
    }    
     */
    
    //$params = array();
    $params['leader_id'] = null;
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -4 month"));
    $params['to'] = date('Y-m-d');
    
    
    $staff_hr = $QStaff->getStaffBi($params);
    

    if(isset($params['area_list']) and $params['area_list']){
        $headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale_area_index($params);
    }
    else{
        $headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale($params);
    }
    
    
    
    $data_pgpb_sale = array();
    foreach($headcount_pgpb_sale as $key => $value) {
        $data_pgpb_sale[$value['YearNo']][$value['MonthNo']][$value['title']] = array(
            'totalEmp'   => $value['totalEmp'],
            'Working'    => $value['Working'],
            'Off'        => $value['off'],
            'Per_Working'=> $value['Per_Working'],
            'Per_Off'    => $value['Per_Off']
            );
    }
    
    // 1
    $rank_star_result = $QKpiMonth->getRankStar($params);
    $rank_star = array();
    foreach($rank_star_result as $k => $v){
        $rank_star[$v['star']] = $v['sum'];
    }
    //Toan
    $rank_star_result_sales = $QRankStarSale->getRankStarRegion($params);

    $rank_star_sales = array();
    foreach($rank_star_result_sales as $k => $v){
        $rank_star_sales[$v['star']] = $v['quantity'];
    }

    if($userStorage->title == TITLE_TRAINER_LEADER){
        $params['view_trainer'] = array('317','174','175','281');
        $staff_team = $QKpiMonth->getStaffBiTeamTrainer($params);
    }
    else{
        $staff_team = $QKpiMonth->getStaffBiTeam($params);
    }
    
    // echo $userStorage->title." ".TITLE_TRAINER_LEADER;
    

    $params['list_title']         = unserialize(LIST_PGS_BI);
    $this->view->listStaffByTitle = $QKpiMonth->getListStaffByTitle($params);
    
    
    
    
    if(isset($params['area_list']) and $params['area_list']){
        //$avg_sale_pg = $QKpiMonth->kpi_avg_bi($params);
    }
    else {
        //$avg_sale_pg = $QKpiMonth->kpi_avg_bi_cache($params);
    }
    //END KPI avg

    $this->view->avg_sale_pg = $avg_sale_pg;
    $this->view->params = $params;


    
    /* Lấy danh sách notification */
    if(in_array($userStorage->title, [317,TRAINER_TITLE_ID, 281])){
        $params_noti = [
            'area'              => $staff['area_name'],
            'staff_id_read'     => $userStorage->id
        ];
        $notification_area = $QNotificationPgs->getNotificationPgs($params_noti);
        
        $notification_trainer = $QNotificationTrainer->getNotificationTrainer($params_noti);
    }
    $this->view->notification_area = $notification_area;
    $this->view->notification_trainer = $notification_trainer;
    /* END Lấy danh sách notification */

    $thamnien = $QKpiMonth->getThamnien_Month($params, $total_thamnien);
    $this->view->thamnien = $thamnien;
    $this->view->total_thamnien = $total_thamnien;
    
    /*total_sales_channel */
    $channel_list = $QKpiMonth->getChannel($params);
    
    $staff_channel   = $QKpiMonth->getTotalSalesChannel($params);
    
    $this->view->staff_channel = $staff_channel;
    $this->view->channel_list = $channel_list;
   // var_dump($sales_channel); exit;
    //COUNT BRAND SHOP
    $data_brandshop = $QKpiMonth->getDataBrandshop($params);
    $count_brandshop = [];
    $count_brandshop['store_id'] = [];
    $count_brandshop['staff_id'] = [];
    foreach($data_brandshop as $key => $value){
        if(empty($count_brandshop['store_id'][$value['store_id']])){
            $count_brandshop['store_id'][$value['store_id']] = $value;
        }
        if(empty($count_brandshop['staff_id'][$value['staff_id']])){
            $count_brandshop['staff_id'][$value['staff_id']] = $value;
        }

    }
    $this->view->count_brandshop = $count_brandshop;
    //END COUNT BRAND SHOP

    //Staff Main New
    $get_staff_main_new = $QStaffTraining->staffMainNew($params);
    $staff_main_new = [];
    foreach($get_staff_main_new as $key=>$value){
        $staff_main_new[$value['MonthNo']][$value['YearNo']] = $value;
    }
    $this->view->staff_main_new = $staff_main_new;
    //END Staff Main New
    
    
    $this->view->data_trainer = $data_trainer;
    $this->view->data_trainer_course = $data_trainer_course;
    $this->view->trainer_course_type = $trainer_course_type;    
    $this->view->row_lesson   = $row_lesson;

    
    $this->view->list_inventory = $list_inventory;
    $this->view->total_sale_pgs = $total_sale_pgs['sum'];
    // $this->view->total_sale_pgs = $total_sale_pgs['total_pgs'];

    $this->view->row_lesson_sales = $row_lesson_sales;
    $this->view->total_sale_sale_leader = $total_sale_sale_leader;
    $this->view->total_pass_lesson = $total_pass_lesson;
    
    //List sale
    $this->view->staff = $staff;
    $this->view->resultStaffTransfer = $transfer;
    $this->view->sell_out = $sell_out;
    
    $this->view->list_date = $list_date;
    $this->view->list_date_off = $list_date_off;
    $this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
    $this->view->good = $QGood->get_cache();
    $this->view->area = $QArea->get_cache();
    $this->view->title = $QTitle->get_cache();
    
    $this->view->staff_hr = $staff_hr;
    $this->view->staff_team = $staff_team;
    $this->view->data_pgpb_sale = $data_pgpb_sale;
    $this->view->rank_star = $rank_star;
    $this->view->rank_star_sales = $rank_star_sales;

    
    $this->_helper->viewRenderer->setRender('staff/trainer-leader-details');
    
?>