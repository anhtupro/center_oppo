<?php 

    $id         		= $this->getRequest()->getParam('id');
    $QTitle          		= new Application_Model_Team();
    $QStaff         		= new Application_Model_Staff();
    $QKpiMonth        		= new Application_Model_KpiMonth();
    $QGood            		= new Application_Model_Good();
    $QArea            		= new Application_Model_Area();
    $QAsm            		= new Application_Model_Asm();
    $QStaffTrainingReport 	=  new Application_Model_StaffTrainingReport();
    $QTrainerCourseType 	=  new Application_Model_TrainerCourseType();
    $QTrainerInventory  	=  new Application_Model_TrainerInventory();
    $QNotificationPgs  		= new Application_Model_NotificationPgs();
    $QStaffTraining             = new Application_Model_StaffTraining();
    $QNotificationTrainer       = new Application_Model_NotificationTrainer();
    $QLesson                    = new Application_Model_Lesson();
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    if($userStorage->title == PGS_SUPERVISOR){
        $id = $userStorage->id;
    }
    
    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );
    
    $params = array(
        'pgs_supervisor' => $id,
        'list_staff' => []
    );
    
    $staff = $QKpiMonth->getStaffById($id);
    $list_staff = $QKpiMonth->getStaffByPgsSupervisor($id);
    
    if($list_staff){
        foreach($list_staff as $key=>$value){
            $params['list_staff'][] = $value['staff_id'];
        }
    }
    
    
    /*total_sales_channel */
    $name_chart_sales_channel = [
        //'888'   => 'RETURN',
        '4'     => 'NORMAL',
        '1'     => 'SILVER',
        '2'     => 'GOLD',
        '3'     => 'DIAMOND',
        '999'   => 'OTHER KA',
        '2325'  => 'CES',
        '10007' => 'VIETTEL',
        '22670' => 'Vinpro',
        //'9187'  => 'Vinpro',
        //'2317'  => 'VTA',
        '2363'  => 'FPT',
        '8366'  => 'ĐMX',
        '2316'  => 'TGDĐ',
    ];
    
    $name_chart = array(
        '2316'  => 'TGDĐ',
        '2363'  => 'FPT',
        '2317'  => 'VTA',
        '9187'  => 'Vinpro',
        '10007' => 'VIETTEL',
        '2325'  => 'CES',
        '3'     => 'DIAMOND',
        '2'     => 'GOLD',
        '1'     => 'SILVER',
        '4'     => 'NORMAL',
        '999'   => 'OTHER KA',
        '888'   => 'RETURN',
    );
    
    $total_sales_channel   = $QKpiMonth->getTotalSalesChannel($params);
    $sales_channel = array();
    foreach ($total_sales_channel as $key => $value) {
        $sales_channel[$value['channel']][$value['title_sum']] = $value['sum'];
    }
    
    $thamnien = $QKpiMonth->getThamnien_Month($params, $total_thamnien);
    
    // Lấy total sell out
    //$list_date_sellout = $QKpiMonth->getSellOut_($params);
    $params['pgs_supervisor'] = $userStorage->id;
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
    $params['to'] = date('Y-m-d');
    $sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
    
    $data_good = $QKpiMonth->getSelloutGood($params);
    
    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
            $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
        }
        else{
            $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
        }
                                            
    }
    /* END sellout, total value theo từng kênh */
    
    
    $sell_out = array();
    foreach ($list_date_sellout as $key => $value) {
        $sell_out[$value['year']][$value['month']][$value['sellout_type']] = $value;
    }
    
    $rank_star_result = $QKpiMonth->getRankStar($params);
    $rank_star = array();
    foreach($rank_star_result as $k => $v){
        $rank_star[$v['star']] = $v['sum'];
    }
    
    $row_lesson = $QKpiMonth->reportDataLesson($params);
    
    $this->view->data_sellout_dealer = $data_sellout_dealer;
    $this->view->data_sellout_total = $data_sellout_total;
    $this->view->list_date = $list_date;
    $this->view->name_chart = $name_chart;
    
    $this->view->data_sellout_good = $data_good;
    $this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
    $this->view->good = $QGood->get_cache();
    
    $this->view->staff = $staff;
    $this->view->title = $QTitle->get_cache();
    $this->view->list_staff = $list_staff;
    
    $this->view->sales_channel            = $sales_channel;
    $this->view->name_chart_sales_channel = $name_chart_sales_channel;
    
    $this->view->thamnien = $thamnien;
    $this->view->total_thamnien = $total_thamnien;
    $this->view->sell_out = $sell_out;
    
    $this->view->rank_star = $rank_star;
    
    $this->view->row_lesson = $row_lesson;
    $this->view->params = $params;
    
    $this->_helper->viewRenderer->setRender('staff/pgs-supervisor');
?>