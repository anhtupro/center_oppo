<?php 

$title = $this->getRequest()->getParam('title');
$page = $this->getRequest()->getParam('page', 1);

$QTeam = new Application_Model_Team();
$QArea = new Application_Model_Area();
$QStaff = new Application_Model_Staff();
$QAsm = new Application_Model_Asm();
$QRegion = new Application_Model_RegionalMarket();

$this->view->areas = $QArea->get_cache();
$this->view->regions = $QRegion->get_cache();

$params = array();
$params['title'] = $title;

$limit = LIMITATION;
$total = 0;

/* Lấy khu vực của ASM */
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
    // lấy khu vực của asm
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
}

if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    exit;
}
/* END Lấy khu vực của ASM */

$asm = $QAsm->fetchPagination($page, $limit, $total, $params);

$list = array();
foreach ($asm as $key => $value) {
    if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        if(in_array($value['area_id'], $params['area_list'])){
            if (! isset($list[ $value['group_id'] ]) )
                $list[ $value['group_id'] ] = array();

            if (! isset($list[ $value['group_id'] ][ $value['id'] ]) )
                $list[ $value['group_id'] ]
                        [ $value['id'] ] = array(
                                            'firstname' => $value['firstname'],
                                            'lastname'  => $value['lastname'],
                                            'email'     => $value['email'],
                                            'area'      => array(),
                                        );

            $list[ $value['group_id'] ]
                    [ $value['id'] ]
                        ['area'][] = $value['area_id'];

        }
    }
    else{
        if (! isset($list[ $value['group_id'] ]) )
                $list[ $value['group_id'] ] = array();

            if (! isset($list[ $value['group_id'] ][ $value['id'] ]) )
                $list[ $value['group_id'] ]
                        [ $value['id'] ] = array(
                                            'firstname' => $value['firstname'],
                                            'lastname'  => $value['lastname'],
                                            'email'     => $value['email'],
                                            'area'      => array(),
                                        );

            $list[ $value['group_id'] ]
                    [ $value['id'] ]
                        ['area'][] = $value['area_id'];
    }
    
}

$this->view->list = $list;
$this->view->team = $QTeam->get_cache();
$this->view->params = $params;

$flashMessenger = $this->_helper->flashMessenger;
$this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
/*
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$this->view->userStorage = $userStorage;
*/
$this->_helper->viewRenderer->setRender('staff/asm');

?>