<?php
$this->_helper->layout->disableLayout();
$export = $this->getRequest()->getParam('export', 0);

if ($export == 1) {

    $title  = $this->getRequest()->getParam('title');
    $params = array(
        'title'				=> $title,
    );

    $QStaff                 = new Application_Model_Staff();
    $QKpiMonth      		= new Application_Model_KpiMonth();
    $leader_area            = $QStaff->getCacheLeaderByArea();

    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';

    $PHPExcel = new PHPExcel();
    $heads    = array(
        'NO.',
        'Area',
        'ID Code',
        'Họ & tên PGs',
        'Kênh',
        'ID shop',
        'Tên shop',
        'Họ & tên Sales Quản lý',
        'Thâm niên (Tháng)',
        'Star',
        'Số SO tháng hiện tại',
        'Share Hero',
        'Phone'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }
    $index    = 2;
    $intCount = 1;
    $params['export'] = $export;
    $data = $QKpiMonth->fetchStaffTitlePagination($page, $limit, $total, $params);
exit;
    try {
        if ($data)
            foreach ($data as $_key => $_info) {
                $m = (int)abs((strtotime("now") - strtotime($_info['joined']))/(60*60*24*30));
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['fullname'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['channel'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['shop_id'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['shop'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($leader_area[$_info['area_name']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($m , PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['star'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['sellout'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit(($value['sellout']<>0 ||  $value['hero'] <>0) ? round(($value['hero']*100)/$value['sellout'],1) : 0, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_info['phone'], PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
    } catch (exception $e) {
        echo $e->getMessage();
        exit;
    }

    $filename  = 'Report_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
}
exit;