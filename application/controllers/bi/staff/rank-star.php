<?php

$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$rank 		 	 = $this->getRequest()->getParam('rank');
$area_id 		 = $this->getRequest()->getParam('area_id');
$district 		 = $this->getRequest()->getParam('district');
$name 		 = $this->getRequest()->getParam('name');
$code 		 = $this->getRequest()->getParam('code');
$pgs_supervisor		= $this->getRequest()->getParam('pgs_supervisor');
$export		= $this->getRequest()->getParam('export');

$total              = 0;
$limit              = LIMITATION;

$params = array(
	'rank' => $rank,
	'area_id' => $area_id,
	'district' => $district,
        'name'  => $name,
        'code'  => $code,
    'export' => $export
);


$params['sort']                 = $sort;
$params['desc']                 = $desc;
    
$QKpiMonth = new Application_Model_KpiMonth();
$QLesson   = new Application_Model_Lesson();
$QAsm 	   = new Application_Model_Asm();
$QArea 	   = new Application_Model_Area();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
	// lấy khu vực của asm
	$asm_cache = $QAsm->get_cache();
	$params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
	$params['area'] = $params['area_list'];
}

if($userStorage->team == BRANDSHOP_TEAM){
	$params['title'] = array(PG_BRANDSHOP, SENIOR_PROMOTER_BRANDSHOP);
}


$where      = array();
$where[]    = $QLesson->getAdapter()->quoteInto('id = ?', $lesson_id);
$lesson 	= $QLesson->fetchRow($where);

$params['list_staff'] = [];
//$list_staff = $QKpiMonth->getStaffByPgsSupervisor($pgs_supervisor);
foreach($list_staff as $key=>$value){
    $params['list_staff'][] = $value['staff_id'];
}



$StaffScores = $QKpiMonth->getListRankStar($page, $limit, $total, $params);


if ($export) {
    $QKpiMonth->exportPgScore($StaffScores);
}

$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->StaffScores  = $StaffScores;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'bi/rank-star' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);

$this->view->rank = $rank;

$areas       = $QArea->get_cache();
$this->view->areas = $areas;

$this->_helper->viewRenderer->setRender('staff/rank-star');

