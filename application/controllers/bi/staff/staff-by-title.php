<?php 
 	$QArea           	= new Application_Model_Area();



$title          		= $this->getRequest()->getParam('title');
$name           		= $this->getRequest()->getParam('name');
$pgs_supervisor           		= $this->getRequest()->getParam('pgs_supervisor');

	$code  = $this->getRequest()->getParam('code');
	$email  = $this->getRequest()->getParam('email');
	$department  = $this->getRequest()->getParam('department');
	$team  = $this->getRequest()->getParam('team');
	$star  = $this->getRequest()->getParam('star');
	$title_search  = $this->getRequest()->getParam('title_search');
	$shop  = $this->getRequest()->getParam('shop');
	$from_date  = $this->getRequest()->getParam('from_date');
	$to_date = $this->getRequest()->getParam('to_date');
	$sellout_from = $this->getRequest()->getParam('sellout_from');
	$sellout_to = $this->getRequest()->getParam('sellout_to');
	//seniority
	$seniority = $this->getRequest()->getParam('seniority');
	$area_id 			= $this->getRequest()->getParam('area_id');
	$province_id		= $this->getRequest()->getParam('province_id');
	$big_area_id     = $this->getRequest()->getParam('big_area_id',null);

$page          			= $this->getRequest()->getParam('page', 1);
$id_trainer				= $this->getRequest()->getParam('id_trainer');
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
$group_id 				= $userStorage->group_id;
$limit          		= LIMITATION;
$total          		= 0;
$params = array(
		'title'				=> $title,
		'id_trainer' 		=>$id_trainer,

		'name'					=>$name,
		'code'					=>$code,
		'email'					=>$email,
		'department'			=>$department,
		'team'					=>$team,
		'star'					=>$star,
		//'title_search'			=>$title_search,
		'shop'					=>$shop,
		'from_date'				=>$from_date,
		'to_date'				=>$to_date,
		'sellout_from'			=>$sellout_from,
		'sellout_to'			=>$sellout_to,
		'type'					=>$seniority,
		'seniority'				=>$seniority,
		'area_id'		=>$area_id,
		'big_area_id'		=>$big_area_id,
		'province_id'	=>$province_id,
                'pgs_supervisor' => $pgs_supervisor
);
$QStaff                 =  new Application_Model_Staff();
$QKpiMonth      		= new Application_Model_KpiMonth();
$QAsm      				= new Application_Model_Asm();
$area 					= $QAsm->get_cache($id_trainer);
$QBigArea              = new Application_Model_BigArea();
$QStaffTrainer          = new Application_Model_StaffTrainer();

$list_area 				= $area['area'];
$params['area_list'] 	= $list_area;


if( $userStorage->team == TRAINING_TEAM
    || in_array($userStorage->group_id,array(ADMINISTRATOR_ID,ASM_ID,ASMSTANDBY_ID ))) {
    $area_id_login = $QStaffTrainer->getAreaTrainer($userStorage->id);

}

if(isset($area_id_login['province']) and count($area_id_login['province']))
{

    $params['regional_market_right']     = array_keys($area_id_login['province']);

    $this->view->regional_markets  = $area_id_login['province'];
}

if(isset($area_id_login['area']) and count($area_id_login['area']) )
{

    $params['area_trainer_right']   = array_keys($area_id_login['area']);


    if(!empty($big_area_id)){ //for seacrhing view
        $area_trainer = $QArea->getAreaByBigArea($big_area_id,array_keys($area_id_login['area']));
    }else{
        $area_trainer = $area_id_login['area'];
    }
}
$this->view->area_trainer = $area_trainer;


// if($group_id == TRAINING_LEADER_ID){
//     // lấy khu vực của trainer
// 	$area = $QAsm->get_cache($userStorage->id);
// 	$list_area = $area['area'];
// 	$params['area_list'] = $list_area;
// 	if(!empty($big_area_id)){ //for seacrhing view
//         $list_area = $QArea->getAreaByBigArea($big_area_id);
//     }
// }

// if($_GET['dev']){
// 	echo "<pre>";
// 	print_r($QBigArea->get_big_area_by_area(array_keys($list_area)));

// }
// if(!empty($pgs_supervisor)){
//     $params['list_staff'] = [];
//     $list_staff = $QKpiMonth->getStaffByPgsSupervisor($pgs_supervisor);
//     foreach($list_staff as $key=>$value){
//         $params['list_staff'][] = $value['staff_id'];
//     }
// }

$result 				= $QKpiMonth->fetchStaffTitlePagination($page, $limit, $total, $params);


$this->view->params 	= $params;
$this->view->list 		= $result;
$this->view->limit 		= $limit;
$this->view->total 		= $total;
// $this->view->area_cache       = $QArea->get_cache();
// $this->view->big_area_cache       = $QArea->get_cache();

$this->view->leader_area        = $QStaff->getCacheLeaderByArea();
//$this->view->list_big_area    = $list_big_area;
$this->view->big_area       = $QBigArea->get_big_area_by_area(array_keys($area_id_login['area']));
// $this->view->list_area       = $list_area;

// $this->view->big_area       = $QBigArea->get_big_area_by_area(array_keys($list_area));

$this->view->url 		= HOST . 'bi/staff-by-title' . ($params ? '?' . http_build_query($params) .
    '&' : '?');
$this->view->offset 	= $limit * ($page - 1);
$this->_helper->viewRenderer->setRender('staff/staff-by-title');

?>