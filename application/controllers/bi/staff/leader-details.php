<?php 
    $id               = $this->getRequest()->getParam('id');
    $current_date     = date('Y-m-d H:i:s');
	$QStaff           = new Application_Model_Staff();
	$QKpiMonth        = new Application_Model_KpiMonth();
    $QGood            = new Application_Model_Good();
    $QAsm             = new Application_Model_Asm();
    $QArea            = new Application_Model_Area();
    $QStoreStaffLog   = new Application_Model_StoreStaffLog();
    $QNotificationPgs = new Application_Model_NotificationPgs();
    $QRankStarSale= new Application_Model_RankStarSales();

	// get Staff
    $staff = $QKpiMonth->getStaffById($id);

    //
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $group_id = $userStorage->group_id;

    if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list_check'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    
    }

    if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        if($group_id == ASM_ID){
            if((!in_array($staff['area_id'], $params['area_list_check']))){
                $this->_redirect(HOST);
                exit;
            }
        }
        elseif($group_id == LEADER_ID){
            if($userStorage->id != $id){
                $this->_redirect(HOST);
                exit;
            }
        }
    }

    
    //END
        // Get Sales star
    $where=null;
    $where= $QRankStarSale->getAdapter()->quoteInto("staff_code = ? ",$staff['code']);
    $this->view->rank_star=$QRankStarSale->fetchRow($where);
    if($_GET["dev"]){
        echo "<pre>";
        print_r($QRankStarSale->fetchRow($where));
        echo "</pre>";
    }
    // END

    // get Transfer
    $db = Zend_Registry::get('db');
    $arrCols = array(
        'note'=>'s.note',
        'sl.transfer_id',
        'staff_id'=> 's.staff_id',
        'sl.object',
        's.from_date',
        'info_types'     => new Zend_Db_Expr('GROUP_CONCAT(sl.info_type)'),
        'current_values' => new Zend_Db_Expr('GROUP_CONCAT(sl.current_value)'),
    );
    $select = $db->select()
        ->from(array('s'=>'staff_transfer'),$arrCols)
        ->joinLeft(array('sl'=>'staff_log_detail'),'s.id = sl.transfer_id',array())
        ->where("s.staff_id = ?",$id)
        ->group('sl.transfer_id')
        ->order('s.from_date DESC')
    ;

    $transfer = $db->fetchAll($select);

    // get List store
    $db = Zend_Registry::get('db');
    $arrCols = array(
        's.name',
        's.id AS store_id',
        'p.joined_at',
        'p.released_at',
        'num' => 'COUNT(i.imei_sn)',
        'level' => 'lp.name'
    );
	
    $select = $db->select()
        ->from(array('p'=>'store_staff_log'), $arrCols)
        ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
        ->joinLeft(array('i'=>'imei_kpi'), "i.store_id = s.id AND i.timing_date >= '".date('Y-m-01')."'",array())
        ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id',array())
        ->joinLeft(array('l'=>'dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)',array())
        ->joinLeft(array('lp'=>'loyalty_plan'), 'lp.id = l.loyalty_plan_id',array())
        ->where("p.staff_id = ?",$id)
        ->where('released_at IS NULL', NULL)
        ->group('s.id')
        ->order('num DESC')
    ;

    $list_store = $db->fetchAll($select);
    $this->view->list_store = $list_store;

    // get list sale, pg
    $list_store_arr = array();
    foreach ($list_store as $key => $value) {
        $list_store_arr[] = $value['store_id'];
    }
	
	if(isset($list_store_arr) and $list_store_arr){
		$list_sale_pg = $QStoreStaffLog->get_list_sale($list_store_arr, $current_date);
    }
	
	$this->view->list_sale_pg = $list_sale_pg;
	
    $params = array(
    	'leader_id' => $id
    );
	
    //List sale
    $this->view->staff_sale = $QKpiMonth->getListSale($params);
    $this->view->staff = $staff;
    $this->view->resultStaffTransfer = $transfer;

    $this->_helper->viewRenderer->setRender('staff/leader-details');


    //Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );

    $name_chart = array(
        '2316'  => 'TGDĐ',
        '2363'  => 'FPT',
        '2317'  => 'VTA',
        '9187'  => 'Vinpro',
        '10007' => 'VIETTEL',
        '2325'  => 'CES',
        '3'     => 'DIAMOND',
        '2'     => 'GOLD',
        '1'     => 'SILVER',
        '4'     => 'NORMAL',
        '999'   => 'OTHER KA',
        '888'   => 'RETURN',
    );

    //$params = array();
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
    $params['to'] = date('Y-m-d');
    $sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);

    $data_good = $QKpiMonth->getSelloutGood($params);

    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
            $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
        }
        else{
            $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
        }
                                            
    }
    /* END sellout, total value theo từng kênh */

    $this->view->data_sellout_dealer = $data_sellout_dealer;
    $this->view->data_sellout_total = $data_sellout_total;
    $this->view->list_date = $list_date;
    $this->view->name_chart = $name_chart;

    $this->view->data_sellout_good = $data_good;
    $this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
    $this->view->good = $QGood->get_cache();
    
    $this->view->area = $QArea->get_cache();
    
    /* sellout by date */
    $params['from'] = date('Y-m-01', strtotime(date('Y-m-d')." -1 month"));
    $data_good_by_date = $QKpiMonth->getGoodByDate($params);
    $good_by_date = array();
    foreach ($data_good_by_date as $key => $value) {
        $good_by_date[$value['date']][$value['good_id']] = $value['num'];
        $good_by_date[$value['date']][9999] += $value['num'];
    }
    $list_date_range = $this->date_range($params['from'], $params['to']);
    $this->view->good_by_date = $good_by_date;
    $this->view->list_date_range = $list_date_range;

    //Số bán hàng ngày
    $good_hero_total = array(
        '426' => 'F1S',
        '9999' => 'Total'
    );
    $this->view->good_hero_total = $good_hero_total;
    /* end sellout by date */

    /* Lấy danh sách notification */
    if($group_id == LEADER_ID){
        $params_noti = [
            'area'              => $staff['area_name'],
            'staff_id_read'     => $userStorage->id
        ];
        $notification_area = $QNotificationPgs->getNotificationPgs($params_noti);
    }
    $this->view->notification_area = $notification_area;
    /* END Lấy danh sách notification */

    $QView = new Application_Model_AppraisalSaleView();
    $QPlan = new Application_Model_AppraisalSalePlan();
    $QToDo = new Application_Model_AppraisalSaleToDo();
    $plan = $QPlan->getLastPlanData();
    $userTitleId = $QToDo->getTitleById($userStorage->id, $plan['asp_id']);
    $this->view->appraisal_data = $userTitleId;

?>