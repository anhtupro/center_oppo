<?php 
 
$QArea           	= new Application_Model_Area();
$QStaffTraining     = new Application_Model_StaffTraining();
$QTeam     = new Application_Model_Team();
$QArea     = new Application_Model_Area();
$QRegionalMarket = new Application_Model_RegionalMarket();

$team = $QTeam->get_cache();
$area_cache = $QArea->get_cache();
$regional_market = $QRegionalMarket->get_cache();

$month          		= $this->getRequest()->getParam('month');
$page          			= $this->getRequest()->getParam('page', 1);
$year           		= $this->getRequest()->getParam('year');
$staff_fails  			= $this->getRequest()->getParam('staff_fails');
$name  					= $this->getRequest()->getParam('name');
$from_date  			= $this->getRequest()->getParam('from_date');
$to_date  				= $this->getRequest()->getParam('to_date');
$area_id  				= $this->getRequest()->getParam('area_id');
$province_id  			= $this->getRequest()->getParam('province_id');
$title  				= $this->getRequest()->getParam('title');
	


$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
$group_id 				= $userStorage->group_id;
$limit          		= LIMITATION;
$total          		= 0;
$params = array(
		'month'				=> $month,
		'year' 				=>$year,
		'staff_fails' 		=>$staff_fails,
		'name'				=>$name,
		'from_date'			=>$from_date,
		'to_date'			=>$to_date,
		'area_id'			=>$area_id,
		'province_id'		=>$province_id,
		'title'				=>$title,

);

$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
$params['to'] = date('Y-m-d');

$QKpiMonth      		= new Application_Model_KpiMonth();
$QAsm      				= new Application_Model_Asm();
$area 					= $QAsm->get_cache($userStorage->id);
$list_area 				= $area['area'];
$params['area_list'] 	= $list_area;

$result 				= $QStaffTraining->fetchStaffNewMain($page, $limit, $total, $params);

$this->view->team = $team;
$this->view->area_cache = $area_cache;
$this->view->regional_market = $regional_market;
$this->view->params 	= $params;
$this->view->list 		= $result;
$this->view->limit 		= $limit;
$this->view->total 		= $total;
$this->view->area       = $QArea->get_cache();
$this->view->url 		= HOST . 'bi/staff-new-main' . ($params ? '?' . http_build_query($params) .
    '&' : '?');
$this->view->offset 	= $limit * ($page - 1);
$this->_helper->viewRenderer->setRender('staff/staff-new-main');

?>