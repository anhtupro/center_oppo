<?php

$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$rank = $this->getRequest()->getParam('rank');
$area_id = $this->getRequest()->getParam('area_id');
$district = $this->getRequest()->getParam('district');
$bigarea_id = $this->getRequest()->getParam('bigarea_id');
$name = $this->getRequest()->getParam('name');
$code = $this->getRequest()->getParam('code');
$pgs_supervisor = $this->getRequest()->getParam('pgs_supervisor');
$export = $this->getRequest()->getParam('export');
$total = 0;
$limit = LIMITATION;
$params = array(
    'rank' => $rank,
    'area_id' => $area_id,
    'district' => $district,
    'name' => $name,
    'code' => $code,
    'export' => $export,
    'bigarea_id' => $bigarea_id,
);

$QRankStarSale = new Application_Model_RankStarSales();
$QRegion = new Application_Model_RegionalMarket();
$QTeam = new Application_Model_Team();
$QArea = new Application_Model_Area();
$QAsm                   = new Application_Model_Asm();
$team = $QTeam->get_cache();
$regional_mk = $QRegion->get_cache();
$params['sort'] = $sort;
$params['desc'] = $desc;


$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id = $userStorage->group_id;


if ($userStorage->team == BRANDSHOP_TEAM) {
    $params['title'] = array(PG_BRANDSHOP, SENIOR_PROMOTER_BRANDSHOP);
}


$params['list_staff'] = [];
//$list_staff = $QKpiMonth->getStaffByPgsSupervisor($pgs_supervisor);
foreach ($list_staff as $key => $value) {
    $params['list_staff'][] = $value['staff_id'];
}

//lấy khu vưc
$area                   = $QAsm->get_cache($userStorage->id);
$list_area              = $area['area'];
$params['area_list']    = $list_area;
// echo "<pre>";
// print_r($params);
// echo "</pre>";
// if($group_id == TRAINING_LEADER_ID){
// 	$params['area_list']=
// }
// TRAINER_LEADER
$StaffScores = $QRankStarSale->fetchPagination($page, $limit, $total, $params);
//debug($StaffScores);exit;

if ($export) {

}

$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->StaffScores = $StaffScores;
$this->view->team = $team;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->regional_mk = $regional_mk;
$this->view->url = HOST . 'bi/rank-star-sales' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->offset = $limit * ($page - 1);
$this->view->rank = $rank;
$this->view->areas = $QArea->get_cache();


$this->_helper->viewRenderer->setRender('staff/rank-star-sales');

