<?php 

	$id         = $this->getRequest()->getParam('id');
    $QTitle          = new Application_Model_Team();
	$QStaff         = new Application_Model_Staff();
	$QKpiMonth        = new Application_Model_KpiMonth();
    $QGood            = new Application_Model_Good();
    $QArea            = new Application_Model_Area();
    $QAsm            = new Application_Model_Asm();
    $QNotificationPgs  = new Application_Model_NotificationPgs();
    
    $params = array(
    );

    // get Staff
    $staff = $QKpiMonth->getStaffById($id);
    $area_staff_id = $staff['area_id'];

    //PHÂN QUYỀN
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $group_id = $userStorage->group_id;

    if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list_check'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    
    }
    
    

    //Kiểm tra pgs này có nằm trong trainer team ko
    if($group_id == TRAINING_TEAM_ID AND $userStorage->id != 910){
        if(!in_array($area_staff_id, $params['area_list_check'])){
            $this->_redirect(HOST);
            exit;
        }
    }

    if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        if($group_id == ASM_ID){
            if((!in_array($staff['area_id'], $params['area_list_check']))){
                $this->_redirect(HOST);
                exit;
            }
        }
        elseif($group_id == LEADER_ID){
            $params['leader_id'] = $userStorage->id;
            $list_sale = $QKpiMonth->getListSale($params);
            $list_sale_array = array();
            foreach ($list_sale as $key => $value) {
                $list_sale_array[] = $value['staff_id'];
            }

            if((!in_array($id, $list_sale_array))){
                $this->_redirect(HOST);
                exit;
            }
        }elseif($group_id == SALES_ID){
            if($userStorage->id != $id){
                $this->_redirect(HOST);
                exit;
            }
        }
    }
    
    
    
    //END 
	
	/*
    if($staff['title'] == PB_SALES_TITLE){
        $params['pb_sale_id'] = $id;
    }elseif($staff['title'] == SALES_TITLE OR $staff['title'] == 274){
        $params['sale_id'] = $id;
    }
	*/
	if($staff['title'] == PB_SALES_TITLE){
        $params['pb_sale_id'] = $id;
    }elseif($staff['title'] == PG_LEADER_TITLE){
        $params['pgs_leader'] = $id;
    }
    else{
        $params['sale_id'] = $id;
    }
    
    // get List store
    $db = Zend_Registry::get('db');
    $arrCols = array(
        's.name',
        's.id AS store_id',
        'p.joined_at',
        'p.released_at',
        'num' => 'COUNT(i.imei_sn)',
        'level' => 'lp.name'
    );
    $select = $db->select()
        ->from(array('p'=>'store_staff_log'), $arrCols)
        ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
        ->joinLeft(array('i'=>'imei_kpi'), "i.store_id = s.id AND i.timing_date >= '".date('Y-m-01')."'",array())
        ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id',array())
        ->joinLeft(array('l'=>'dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)',array())
        ->joinLeft(array('lp'=>'loyalty_plan'), 'lp.id = l.loyalty_plan_id',array())
        ->where("p.staff_id = ?",$id)
        ->where('released_at IS NULL', NULL)
        ->group('s.id')
        ->order('num DESC')
    ;

    $list_store = $db->fetchAll($select);
    $this->view->list_store = $list_store;

    //get training Online
    $arrCols = array(
        'l.title',
        'p.staff_cmnd',
        'p.lesson_id',
        'dtb' => 'AVG(p.scores)',
        'ketqua' => '(CASE WHEN (max(p.scores) = 10) THEN "PASS" ELSE "FAIL" END)'
    );
    $select = $db->select()
        ->from(array('p'=>'lesson_scores'), $arrCols)
        ->joinLeft(array('l'=>'lesson'), 'p.lesson_id = l.id',array())
        ->joinLeft(array('s'=>'staff'), 's.ID_number = p.staff_cmnd',array())
        ->where("s.id = ?",$id)
        ->group('p.lesson_id')
        ->order('l.created_at DESC')
    ;
    $list_lesson = $db->fetchAll($select);
    $this->view->list_lesson = $list_lesson;
   

    // get Transfer
    $db = Zend_Registry::get('db');
    $arrCols = array(
        'note'=>'s.note',
        'sl.transfer_id',
        'staff_id'=> 's.staff_id',
        'sl.object',
        's.from_date',
        'info_types'     => new Zend_Db_Expr('GROUP_CONCAT(sl.info_type)'),
        'current_values' => new Zend_Db_Expr('GROUP_CONCAT(sl.current_value)'),
    );
    $select = $db->select()
        ->from(array('s'=>'staff_transfer'),$arrCols)
        ->joinLeft(array('sl'=>'staff_log_detail'),'s.id = sl.transfer_id',array())
        ->where("s.staff_id = ?",$id)
        ->group('sl.transfer_id')
        ->order('s.from_date DESC')
    ;

    $transfer = $db->fetchAll($select);

    //List sale
    
    $params['from_date'] = date('Y-m-01', strtotime(date('Y-m')." -1 month"));
    $params['to_date'] = date('Y-m-d');

    // echo "<pre>";print_r($params);die;    


    $staff_pg  = $QKpiMonth->getPgPgsLeader($params);
    $ID_number = [];
    $list_pgs  = [];
    foreach($staff_pg as $key=>$value){
        $ID_number[] = $value['ID_number'];
        $list_pgs[]  = $value['staff_id'];
    }
    $params['list_pg_id'] = implode(",", $list_pgs);

    unset($params['sale_id']);

    $params['ID_number'] = $ID_number;
    $list_score = $QKpiMonth->getScoreStaff($params);
  


    $this->view->staff_pg = $staff_pg;
    $this->view->list_date = $list_date;
    $this->view->staff = $staff;
    $this->view->resultStaffTransfer = $transfer;
    $this->view->sellout = $sellout;
    $this->view->list_score = $list_score;

	$this->_helper->viewRenderer->setRender('staff/pgs-leader');

    //Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );

    $name_chart = array(
        '2316'  => 'TGDĐ',
        '2363'  => 'FPT',
        '2317'  => 'VTA',
        '9187'  => 'Vinpro',
        '10007' => 'VIETTEL',
        '2325'  => 'CES',
        '3'     => 'DIAMOND',
        '2'     => 'GOLD',
        '1'     => 'SILVER',
        '4'     => 'NORMAL',
        '999'   => 'OTHER KA',
        '888'   => 'RETURN',
    );

    //$params = array();
    $params['leader_id'] = null;
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
    $params['to'] = date('Y-m-d');
    $sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
    $data_good = $QKpiMonth->getSelloutGood($params);

    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
            $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
        }
        else{
            $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
        }
                                            
    }
    /* END sellout, total value theo từng kênh */

    // echo "<pre>";print_r($data_sellout_dealer);die;

    $this->view->data_sellout_dealer = $data_sellout_dealer;
    $this->view->data_sellout_total = $data_sellout_total;
    $this->view->list_date = $list_date;
    $this->view->name_chart = $name_chart;

    $this->view->data_sellout_good = $data_good;
    $this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
    $this->view->good = $QGood->get_cache();

    $this->view->area = $QArea->get_cache();
    $this->view->title = $QTitle->get_cache();

    /* sellout by date */
    $params['from'] = date('Y-m-01', strtotime(date('Y-m-d')." -1 month"));
    $data_good_by_date = $QKpiMonth->getGoodByDate($params);
    $good_by_date = array();
    foreach ($data_good_by_date as $key => $value) {
        $good_by_date[$value['date']][$value['good_id']] = $value['num'];
        $good_by_date[$value['date']][9999] += $value['num'];
    }
    $list_date_range = $this->date_range($params['from'], $params['to']);
    $this->view->good_by_date = $good_by_date;
    $this->view->list_date_range = $list_date_range;

    //Số bán hàng ngày
    $good_hero_total = array(
        HERO_ID => HERO_NAME,
        '9999' => 'Total'
    );
    $this->view->good_hero_total = $good_hero_total;
    /* end sellout by date */


    /* DASHBOARD SALE */
    $ik = new Application_Model_NewImeiKpi();
    $data = array(
        'staff_id' => $id,
        'from' => date('Y-m-01 00:00:00'),
        'to' => date('Y-m-d H:i:s')
    );
    $data = $ik->GetAll($data);
    $this->view->res = $data;


    $QInform = new Application_Model_Inform();
    $params_inform = array(
                'staff_id' => $id,
                'filter'   => true,
                'status'   => 1,
                );
    $params_inform['group_cat'] = 1;
    $params_inform['filter_display'] = 1;
    $page = 1;
    $limit = 5;
    $total = 0;
    $inform = $QInform->fetchPagination($page, $limit, $total, $params_inform);
    $this->view->informs = $inform;
    /* END */


    /* Lấy danh sách notification */
    if($userStorage->title == PG_LEADER_TITLE){
        
        $params_noti = [
            'area'              => $staff['area_name'],
            'staff_id_read'     => $userStorage->id
        ];
        $notification_area = $QNotificationPgs->getNotificationPgs($params_noti);
    }
    $this->view->notification_area = $notification_area;
    /* END Lấy danh sách notification */
?>