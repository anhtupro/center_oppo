<?php 

$area   = $this->getRequest()->getParam('area');
$campaign   = $this->getRequest()->getParam('campaign');

$QKpiMonth        = new Application_Model_KpiMonth();
$QArea 	          = new Application_Model_Area();
$where_area       = $QArea->getAdapter()->quoteInto('id = ?', $area);
$data_area 		  = $QArea->fetchRow($where_area);


$params = array(
	'area'       => $area,
	'campaign'	 => $campaign,
	'from'		 => date('Y-m-01', strtotime(date('Y-m')." -3 month")),
	'to'		 => date('Y-m-d')
);


$list_date = array(
    date('Y-m-d', strtotime(date('Y-m')." -3 month")),
    date('Y-m-d', strtotime(date('Y-m')." -2 month")),
    date('Y-m-d', strtotime(date('Y-m')." -1 month")),
    date('Y-m-d'),
);

$params['month'] = array();

foreach ($list_date as $key => $value) {
    $params['month'][] = intval(date('m', strtotime($value)));
}
$total_price  = 0;
$fee_campaign = $QKpiMonth->get_fee_campaign_area2($params);

$this->view->data_area  = $data_area;
$this->view->total_price  = $total_price;

$this->view->fee_campaign   = $fee_campaign;

$this->_helper->viewRenderer->setRender('trade/area-trade-details-posm2');
?>