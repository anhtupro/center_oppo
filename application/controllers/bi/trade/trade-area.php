<?php 

$QBiTrade = new Application_Model_BiTrade();
$QArea    = new Application_Model_Area();


$params 		= array();
$air_investment = $QBiTrade->getTradeAreaCategory($params);
$category 		= $QBiTrade->getCategoryBi($params);

$area = $QArea->GetAll($params);


$data = [];
foreach($air_investment as $key=>$value){
    $data[$value['area_id']][$value['cat_id']]['quantity']         =  $value['quantity'];  
    $data[$value['area_id']][$value['cat_id']]['total_value'] 	   =  $value['value'];
}

$this->view->data     		= $data;
$this->view->category 		= $category;
$this->view->air_investment = $air_investment;
$this->view->area 			= $QBiTrade->getListArea();

$this->_helper->viewRenderer->setRender('/trade/trade-area');
