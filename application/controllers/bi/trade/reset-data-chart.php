<?php 

$from_date     	   = $this->getRequest()->getParam('from_date');
$to_date     	   = $this->getRequest()->getParam('to_date');
$area_id     	   = $this->getRequest()->getParam('area_id', array() );
$province_id       = $this->getRequest()->getParam('province_id');
$district_id       = $this->getRequest()->getParam('district_id');
$channel_id        = $this->getRequest()->getParam('channel_id');

$from_date = explode('/', $from_date);
$from_date = date('Y-m-01', strtotime($from_date[1].'-'.$from_date[0].'-01'));

$to_date = explode('/', $to_date);
$to_date = date('Y-m-t', strtotime($to_date[1].'-'.$to_date[0].'-01'));

$area_id        = ($area_id AND $area_id != 'null') ? explode(',', $area_id) : NULL;
$province_id    = ($province_id AND $province_id != 'null') ? explode(',', $province_id) : NULL;
$district_id    = ($district_id AND $district_id != 'null') ? explode(',', $district_id) : NULL;
$channel_id     = ($channel_id AND $channel_id != 'null') ? explode(',', $channel_id) : NULL;

$QBiTrade = new Application_Model_BiTrade();
$KpiMonth = new Application_Model_KpiMonth();

$list_month = $this->month_range($from_date, $to_date);
$category   = $QBiTrade->getCategoryBi($params);

$params = array(
	'from_date'		=> $from_date,
	'to_date'		=> $to_date,
	'area_list'		=> $area_id,
	'province_list'	=> $province_id,
	'district_list'	=> $district_id,
    'channel_id'    => $channel_id
);

$params['from'] 	= date('Y-m-01', strtotime(date('Y-m-d')." -0 month"));
$params['to'] 		= date('Y-m-t', strtotime(date('Y-m-d')." -0 month"));

if( $province_id OR $district_id ){
    $good_by_month  = $KpiMonth->loadGoodByMonthProvince($params);
}
else{
    $good_by_month  = $QBiTrade->loadGoodByMonthAll($params);
}

$data_trade_total	= $QBiTrade->getTradeByMonth($params);
$data_quantity_cate = $QBiTrade->getQuantityTrade($params);

$data_sellout_dealer = array();
$data_sellout_total = array();
foreach ($good_by_month as $key => $value) {
    $data_sellout_dealer[$value['channel']][$value['month_date']][$value['year_date']] = array(
                                                                        'num' => $value['num'],
                                                                        'total_value' => $value['total_value']
                                                                        );


    if(isset($data_sellout_total[$value['month_date']][$value['year_date']]) and $data_sellout_total[$value['month_date']][$value['year_date']]){
        $data_sellout_total[$value['month_date']][$value['year_date']]['num']         +=  $value['num']; 
        $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] +=  $value['total_value']; 
    }
    else{
        $data_sellout_total[$value['month_date']][$value['year_date']]['num']         =  $value['num'];  
        $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] =  $value['total_value'];  
    }
                                        
}


//ADD JSON
$data_json = array();
foreach ($list_month as $key => $value) {
    $m = intval(date('m', strtotime($value)));
    $y = intval(date('Y', strtotime($value)));

    $data_json['months'][] = "Tháng ".$m;
    $data_json['total_doanhthu'][] = !empty($data_sellout_total[$m][$y]['total_value']) ? floatval($data_sellout_total[$m][$y]['total_value']) : 0;
    $data_json['total_trade'][]	= !empty($data_trade_total[$m][$y]['money']) ? floatval($data_trade_total[$m][$y]['money']) : 0;

    foreach($category as $k=>$v){
        $data_json['data_quantity_cate_quantity'][$v['id']][] = !empty($data_quantity_cate[$m][$y][$v['id']]['quantity']) ? floatval($data_quantity_cate[$m][$y][$v['id']]['quantity']) : 0;

        $data_json['data_quantity_cate_value'][$v['id']][] = !empty($data_quantity_cate[$m][$y][$v['id']]['money']) ? floatval($data_quantity_cate[$m][$y][$v['id']]['money']) : 0;
    }

}

$data_json['from_date'] = $from_date;
$data_json['to_date']   = $to_date;
$data_json['area_id']   = $area_id;
$data_json['category']  = $category;

echo json_encode($data_json);
exit;