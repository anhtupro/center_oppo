<?php 

$area_id = $this->getRequest()->getParam('area_id');

$params = array(
	'area_id' 	 => $area_id,
    'area_list'  => [$area_id]
);

$QArea  = new Application_Model_Area();
$QBiTrade  = new Application_Model_BiTrade();
$QRegionalMarket  = new Application_Model_RegionalMarket();

$where  	 = $QArea->getAdapter()->quoteInto('id = ?', $area_id);
$this_area   = $QArea->fetchRow($where);
$list_area   = $QArea->fetchAll($where);

$province = $QRegionalMarket->fetchAll( $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id) );
$district = $QBiTrade->getDistrictByArea($params);

$this->view->this_area 	   = $this_area;
$this->view->list_area = $list_area;
$this->view->province  = $province;
$this->view->district  = $district;
$this->_helper->viewRenderer->setRender('/trade/trade-area-detail');



$from_date = date('Y-m-01', strtotime(date('Y-m-d')." -13 month"));
$to_date   = date('Y-m-d');

$params = array(
	'from_date'	=> $from_date,
	'to_date'	=> $to_date,
	'area_id' 	=> $area_id,
	'area_list' => [$area_id]
);

$QArea = new Application_Model_Area();
$QBiTrade = new Application_Model_BiTrade();
$KpiMonth = new Application_Model_KpiMonth();
$QChannel = new Application_Model_Channel();

$list_month = $this->month_range($from_date, $to_date);

//Lấy số liệu S.O
$params['from'] = date('Y-m-01', strtotime(date('Y-m-d')." -0 month"));
$params['to']   = date('Y-m-d');

$params['from_channel'] = date('Y-m-01', strtotime(date('Y-m-d')." -3 month"));
$params['to_channel']   = date('Y-m-d');
$list_month_channel = $this->month_range($params['from_channel'], $params['to_channel']);

$good_by_month 		= $KpiMonth->loadGoodByMonthAll($params);

$data_sellout_dealer = array();
$data_sellout_total = array();
foreach ($good_by_month as $key => $value) {
    $data_sellout_dealer[$value['channel']][$value['month_date']][$value['year_date']] = array(
                                                                        'num' => $value['num'],
                                                                        'total_value' => $value['total_value']
                                                                        );


    if(isset($data_sellout_total[$value['month_date']][$value['year_date']]) and $data_sellout_total[$value['month_date']][$value['year_date']]){
        $data_sellout_total[$value['month_date']][$value['year_date']]['num']         +=  $value['num']; 
        $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] +=  $value['total_value']; 
    }
    else{
        $data_sellout_total[$value['month_date']][$value['year_date']]['num']         =  $value['num'];  
        $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] =  $value['total_value'];  
    }
                                        
}



//Lấy số liệu đầu tư
//I.Lấy tổng chi phí đầu tư
$data_trade_total		= $QBiTrade->getTradeByMonth($params);

//II.Lấy đầu tư theo hạng mục
$data_quantity_cate = $QBiTrade->getQuantityTrade($params);

//III.Lấy đầu tư theo kênh
$data_channel = $QBiTrade->getChannelTrade($params);

//IV. Lấy đầu tư theo tỉnh huyện
$district_so = $QBiTrade->getDistrictSO($params);
$data_district_so = array();
foreach($district_so as $key=>$value){
    $data_district_so[$value['district_id']] = $value['value'];
}

$district_trade   = $QBiTrade->getDistrictTrade($params);

$data_district_trade = array();
foreach($district_trade as $key=>$value){
    $data_district_trade[$value['district_id']] = number_format( $value['total_value']/$data_district_so[$value['district_id']]*100, 2 );
}

//V.Lấy Chi phí đầu tư bảng hiệu
$params['group_cat_id'] = 1;
$banghieu_trade = $QBiTrade->getCategoryTradeArea($params);
 
$t_quantity = 0;
$t_value = 0;
foreach($banghieu_trade as $key=>$value){
    $t_quantity += $value['quantity'];
    $t_value    += $value['value'];
}
$banghieu_avg = [];
$banghieu_avg['avg_quantity'] = $t_quantity/count($banghieu_trade);
$banghieu_avg['avg_value']    = $t_value/count($banghieu_trade);

//VI.Lấy Chi phí đầu tư bàn trải nghiệm
$params['group_cat_id'] = 2;
$bantrainghiem_trade = $QBiTrade->getCategoryTradeArea($params);
$t_quantity = 0;
$t_value = 0;
foreach($bantrainghiem_trade as $key=>$value){
    $t_quantity += $value['quantity'];
    $t_value    += $value['value'];
}
$bantrainghiem_avg = [];
$bantrainghiem_avg['avg_quantity'] = $t_quantity/count($bantrainghiem_trade);
$bantrainghiem_avg['avg_value']    = $t_value/count($bantrainghiem_trade);

//VII.Lấy Chi phí đầu tư tủ kính
$params['group_cat_id'] = 3;
$tukinh_trade = $QBiTrade->getCategoryTradeArea($params);
$t_quantity = 0;
$t_value = 0;
foreach($tukinh_trade as $key=>$value){
    $t_quantity += $value['quantity'];
    $t_value    += $value['value'];
}
$tukinh_avg = [];
$tukinh_avg['avg_quantity'] = $t_quantity/count($tukinh_trade);
$tukinh_avg['avg_value']    = $t_value/count($tukinh_trade);

//VII.Lấy Chi phí đầu tư vách hình ảnh
$params['group_cat_id'] = 4;
$vach_trade = $QBiTrade->getCategoryTradeArea($params);
$t_quantity = 0;
$t_value = 0;
foreach($vach_trade as $key=>$value){
    $t_quantity += $value['quantity'];
    $t_value    += $value['value'];
}
$vach_avg = [];
$vach_avg['avg_quantity'] = $t_quantity/count($vach_trade);
$vach_avg['avg_value']    = $t_value/count($vach_trade);

//VIII.Lấy số lượng tổng các hạng mục
$category_total = $QBiTrade->getCategoryTotal($params);
$group_bi     = $QBiTrade->getGroupBi($params);

//IX. Lấy tổng số lượng thực tế
$checkshop  = $QBiTrade->getCheckshop($params);


$this->view->category           = $QBiTrade->getCategoryBi($params);
$this->view->data_sellout_total = $data_sellout_total;
$this->view->data_trade_total   = $data_trade_total;
$this->view->data_quantity_cate = $data_quantity_cate;
$this->view->data_district_trade= $data_district_trade;
$this->view->area               = $QArea->get_cache_bi();
$this->view->list_month         = $list_month;
$this->view->list_month_channel = $list_month_channel;
$this->view->data_district_so   = $data_district_so;
$this->view->channel            = $QChannel->get_cache();
$this->view->data_channel       = $data_channel;
$this->view->category_total     = $category_total;
$this->view->group_bi           = $group_bi;
$this->view->checkshop          = $checkshop;

$this->view->banghieu_trade     = $banghieu_trade;
$this->view->banghieu_avg       = $banghieu_avg;

$this->view->bantrainghiem_trade     = $bantrainghiem_trade;
$this->view->bantrainghiem_avg       = $bantrainghiem_avg;

$this->view->tukinh_trade     = $tukinh_trade;
$this->view->tukinh_avg       = $tukinh_avg;

$this->view->vach_trade     = $vach_trade;
$this->view->vach_avg       = $vach_avg;

$this->view->params             = $params;

$this->_helper->viewRenderer->setRender('/trade/trade-area-detail');

