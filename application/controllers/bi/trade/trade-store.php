<?php 

$page     	   = $this->getRequest()->getParam('page', 1);
$dealer_id     = $this->getRequest()->getParam('dealer_id');

$QBiTrade 		= new Application_Model_BiTrade();
$QArea    		= new Application_Model_Area();

$dealer = $QBiTrade->getDealerById($dealer_id);


$params	  = array(
	'dealer_id'	=> $dealer_id
);

$total    = 0;
$limit    = LIMITATION; 

$air_investment = $QBiTrade->getAirInvestmentStore($page, $limit, $total, $params);
$category       = $QBiTrade->getCategoryBi($params);

$area = $QArea->GetAll($params);

$this->view->dealer = $dealer;
$this->view->air_investment = $air_investment;
$this->view->category   = $category;
$this->view->area 		= $area;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'bi/trade-store' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);

$this->_helper->viewRenderer->setRender('/trade/trade-store');
