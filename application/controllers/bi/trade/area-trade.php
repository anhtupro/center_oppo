<?php 
$month   = $this->getRequest()->getParam('month');
$month   = $month ? $month : date("m/Y");
$from_date = DateTime::createFromFormat('d/m/Y', '01/'.$month)->format('Y-m-d');
$to_date = date("Y-m-t", strtotime($from_date));

$params = array(
    'from_date' => $from_date,
    'to_date' => $to_date,
    'month' => $month
);

$area_fee = array();
$cost_fee = array();
$air_fee = array();
$campaign_fee = array();

$QKpiMonth = new Application_Model_KpiMonth();
$QArea = new Application_Model_Area();
$QAsm  = new Application_Model_Asm();

$cost = $QKpiMonth->get_fee_cost_month($params);
$air = $QKpiMonth->get_fee_air_month($params);
$campaign = $QKpiMonth->get_fee_campaign_month($params);

foreach($cost AS $key => $value){
    $cost_fee[$value['area_id']] = $value['sum'];
}

foreach($air AS $key => $value){
    $air_fee[$value['area_id']] = $value['sum'];
}

foreach($campaign AS $key => $value){
    $campaign_fee[$value['area_id']] = $value['sum'];
}



//Phân quyền
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;
$areas       = $QArea->get_cache();

if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
    exit;
}

if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    $params['area'] = $params['area_list'];
}


if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    exit;
}



$area = $QArea->get_cache();
foreach($area as $key=>$value){
    if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        if(in_array($key, $params['area_list'])){
            if(!in_array($key, array(2,15))){
                $area_fee[$key]['name'] = $value;
                $area_fee[$key]['cost'] = (isset($cost_fee[$key]) and $cost_fee[$key]) ? $cost_fee[$key]: 0;
                $area_fee[$key]['air'] = (isset($air_fee[$key]) and $air_fee[$key]) ? $air_fee[$key] : 0;
                $area_fee[$key]['campaign'] = (isset($campaign_fee[$key]) and $campaign_fee[$key]) ? $campaign_fee[$key] : 0;
            }
        }
    }
    else{
        if(!in_array($key, array(2,15))){
            $area_fee[$key]['name'] = $value;
            $area_fee[$key]['cost'] = (isset($cost_fee[$key]) and $cost_fee[$key]) ? $cost_fee[$key]: 0;
            $area_fee[$key]['air'] = (isset($air_fee[$key]) and $air_fee[$key]) ? $air_fee[$key] : 0;
            $area_fee[$key]['campaign'] = (isset($campaign_fee[$key]) and $campaign_fee[$key]) ? $campaign_fee[$key] : 0;
        }
    }
}

$this->view->area_fee = $area_fee;
$this->view->params   = $params;

$this->_helper->viewRenderer->setRender('trade/area-trade');
?>