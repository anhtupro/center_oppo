<?php 

$asm_id = $this->getRequest()->getParam('asm_id');
$area   = $this->getRequest()->getParam('area');
$page   = $this->getRequest()->getParam('page', 1);

$QKpiMonth        = new Application_Model_KpiMonth();
$QArea 	          = new Application_Model_Area();
$QAsm             = new Application_Model_Asm();

//Phân quyền
$params = array();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm = new Application_Model_Asm();
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

}

if((!in_array($area, $params['area_list'])) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    $this->_redirect(HOST);
    exit;
}
//ENd phân quyền

$asm_cache = $QAsm->get_cache();
if(empty($asm_id) and !empty($area)){
	$area = array($area);
}
else{
	$area = isset($asm_cache[$asm_id]['area']) ? $asm_cache[$asm_id]['area'] : array();
}

$total              = 0;
$limit              = LIMITATION;

$params = array(
	'area'       => $area,
	'asm_id'	 => $asm_id,
	'from'		 => date('Y-m-01', strtotime(date('Y-m-d')." -3 month")),
	'to'		 => date('Y-m-d')
);


$list_date = array(
    date('Y-m-d', strtotime(date('Y-m')." -3 month")),
    date('Y-m-d', strtotime(date('Y-m')." -2 month")),
    date('Y-m-d', strtotime(date('Y-m')." -1 month")),
    date('Y-m-d'),
);

$params['month'] = array();

foreach ($list_date as $key => $value) {
    $params['month'][] = intval(date('m', strtotime($value)));
}

$total_price  = 0;
$fee_campaign = $QKpiMonth->get_fee_campaign_area($page, $limit, $total, $total_price, $params);

$this->view->data_area  = $QArea->get_cache();
$this->view->total_price  = $total_price;

$this->view->fee_campaign   = $fee_campaign;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'bi/area-trade-details-posm' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);

$this->_helper->viewRenderer->setRender('trade/area-trade-details-posm');
?>