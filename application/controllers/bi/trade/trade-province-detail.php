<?php 

$province_id = $this->getRequest()->getParam('province_id');

$QRegionalMarket  = new Application_Model_RegionalMarket();
$where  		  = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $province_id);
$province   	  = $QRegionalMarket->fetchRow($where);


$this->view->province = $province;
$this->_helper->viewRenderer->setRender('/trade/trade-province-detail');
