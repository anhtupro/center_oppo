<?php 

$id     	   = $this->getRequest()->getParam('id');

$QStore    	   = new Application_Model_Store();
$QBiTrade      = new Application_Model_BiTrade();
$QKpiMonth     = new Application_Model_KpiMonth();

$params = array(
	'store_id'	=> $id
);

$category      = $QBiTrade->getCategoryBiStore($params);
$store_date    = $QBiTrade->getFromToSO($params);
$list_month    = $this->month_range($store_date['from_date'], $store_date['to_date']);

//Get data S.O
$sellout 	   = $QBiTrade->getSelloutByStore($params);
$data_sellout  = array();
foreach ($sellout as $key => $value) {
	if(isset($data_sellout[$value['month']][$value['year']]) and $data_trade_total[$value['month']][$value['year']]){
        $data_sellout[$value['month']][$value['year']]['total_value'] +=  $value['total_value']; 
    }
    else{
        $data_sellout[$value['month']][$value['year']]['total_value'] =  $value['total_value'];  
    }
}

//Get data Trade
$trade 	   = $QBiTrade->getTradeByStore($params);

$total_trade_value = 0;
$data_trade = array();
foreach ($trade as $key => $value) {

    $total_trade_value += $value['total_value'];

	if(isset($data_trade[$value['month']][$value['year']]) and $data_trade[$value['month']][$value['year']]){
        $data_trade[$value['month']][$value['year']]['total_value'] +=  $value['total_value']; 
    }
    else{
        $data_trade[$value['month']][$value['year']]['total_value'] =  $value['total_value'];  
    }
}

$where = $QStore->getAdapter()->quoteInto('id = ?', $id);
$store = $QStore->fetchRow($where);

$investments = $QKpiMonth->get_investments_by_store($params['store_id']);
$this->view->investments = $investments;

$total_trade = $QKpiMonth->get_total_trade_store($params);


$this->view->category   	= $category;
$this->view->store 			= $store;
$this->view->list_month 	= $list_month;
$this->view->sellout 		= $sellout;
$this->view->data_sellout	= $data_sellout;
$this->view->data_trade 	= $data_trade;
$this->view->total_trade    = $total_trade;
$this->view->store_date     = $store_date;
$this->view->total_trade_value = $total_trade_value;
$this->_helper->viewRenderer->setRender('/trade/trade-store-detail');



