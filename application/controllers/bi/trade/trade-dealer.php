<?php 

$page     	 = $this->getRequest()->getParam('page', 1);
$area_id     = $this->getRequest()->getParam('area_id');

$QBiTrade = new Application_Model_BiTrade();
$QArea    = new Application_Model_Area();


$params	  = array(
	'area_id'	=> $area_id
);

$total    = 0;
$limit    = LIMITATION; 

$air_investment = $QBiTrade->getAirInvestmentDealer($page, $limit, $total, $params);
$category       = $QBiTrade->getCategoryBi($params);

$area = $QArea->GetAll($params);

$this->view->air_investment = $air_investment;
$this->view->category   = $category;
$this->view->area 		= $area;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'bi/trade-dealer' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);

$this->_helper->viewRenderer->setRender('/trade/trade-dealer');
