<?php 

set_time_limit(0);
    
$QStaff           = new Application_Model_Staff();
$QKpiMonth        = new Application_Model_KpiMonth();
$QArea            = new Application_Model_Area();
$QRegionalMarket  = new Application_Model_RegionalMarket();
$QGood            = new Application_Model_Good();
$QStaffTrainingReport = new Application_Model_StaffTrainingReport();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;
$areas       = $QArea->get_cache();
$this->view->areas = $areas;


$area       = $this->getRequest()->getParam('area');
$where      = $QArea->getAdapter()->quoteInto('name = ?', $area);
$temp       = $QArea->fetchRow($where);
$area       = $temp['id'];

$params = array(
    'area'       => $area,
    'area_list'  => $area,
);


if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm = new Application_Model_Asm();
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

}

if((!in_array($area, $params['area_list'])) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    $this->_redirect(HOST);
    exit;
}

//Biểu đồ

$year3 = date('Y', strtotime(date('Y-m')." -3 month"));
$year2 = date('Y', strtotime(date('Y-m')." -2 month"));
$year1 = date('Y', strtotime(date('Y-m')." -1 month"));
$year0 = date('Y', strtotime(date('Y-m')." 0 month"));
$year = array(
    $year0,
    $year1,
    $year2,
    $year3
);

//
$year = array(
    date('Y', strtotime(date('Y-m')." 0 month")),
    date('Y', strtotime(date('Y-m')." -1 month")),
    date('Y', strtotime(date('Y-m')." -2 month")),
    date('Y', strtotime(date('Y-m')." -3 month"))
);
$this->view->year = $year;
//
$month3 = intval(date('m', strtotime(date('Y-m')." -3 month")));
$month2 = intval(date('m', strtotime(date('Y-m')." -2 month")));
$month1 = intval(date('m', strtotime(date('Y-m')." -1 month")));
$month0 = intval(date('m', strtotime(date('Y-m')." 0 month")));

$list_month = $month3.",".$month2.",".$month1.",".$month0;
$this->view->list_month = explode(',',$list_month);

//HR
$staff      = $QStaff->getStaffBi($params);
$staff_off = $QStaff->getStaffBiOff($params);
$staff_joined = $QStaff->getStaffBiJoined($params);

$this->view->staff = $staff;
$this->view->staff_off = $staff_off;
$this->view->staff_joined = $staff_joined;

$params['area_list'] = array($area);



//Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
$list_date = array(
    date('Y-m-d', strtotime(date('Y-m')." -3 month")),
    date('Y-m-d', strtotime(date('Y-m')." -2 month")),
    date('Y-m-d', strtotime(date('Y-m')." -1 month")),
    date('Y-m-d'),
);

$name_chart = array(
    '2316'  => 'TGDĐ',
    '2363'  => 'FPT',
    '2317'  => 'VTA',
    '9187'  => 'Vinpro',
    '10007' => 'VIETTEL',
    '2325'  => 'CES',
    '3'     => 'DIAMOND',
    '2'     => 'GOLD',
    '1'     => 'SILVER',
    '4'     => 'NORMAL',
    '999'   => 'OTHER KA',
    '888'   => 'RETURN',
);

//$params = array();
$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
$params['to'] = date('Y-m-d');
$sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
$data_good = $QKpiMonth->getSelloutGood($params);

/* get sellout, total value theo từng kênh */
$data_sellout_dealer = array();
$data_sellout_total = array();
foreach ($sell_out_dealer as $key => $value) {
    $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                        'num' => $value['num'],
                                                                        'total_value' => $value['total_value']
                                                                        );


    if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
        $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
        $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
    }
    else{
        $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
        $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
    }
                                        
}
/* END sellout, total value theo từng kênh */

$this->view->data_sellout_dealer = $data_sellout_dealer;
$this->view->data_sellout_total = $data_sellout_total;
$this->view->list_date = $list_date;
$this->view->name_chart = $name_chart;

$this->view->data_sellout_good = $data_good;
$this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
$this->view->good = $QGood->get_cache();


/* TRADE MARKETTING*/
$params['month'] = array();

foreach ($list_date as $key => $value) {
    $params['month'][] = intval(date('m', strtotime($value)));
}

$fee_cost = $QKpiMonth->get_fee_cost($params);
$fee_air  = $QKpiMonth->get_fee_air($params);
$fee_campaign  = $QKpiMonth->get_fee_campaign($params);

$data_trade = array();
$data_trade_total = array(
    'fee_cost'     => 0,
    'fee_air'      => 0,
    'fee_campaign' => 0,
);

foreach ($fee_cost as $key => $value) {
    $data_trade[intval($value['month_date'])] = $value['sum'];
    //
    $data_trade_total['fee_cost'] += $value['sum'];
}

foreach ($fee_air as $key => $value) {
    if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
        $data_trade[intval($value['month_date'])] += $value['sum'];
    }
    else{
        $data_trade[intval($value['month_date'])] = $value['sum'];
    }
    //
    $data_trade_total['fee_air'] += $value['sum'];
}

foreach ($fee_campaign as $key => $value) {
    if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
        $data_trade[intval($value['month_date'])] += $value['sum'];
    }
    else{
        $data_trade[intval($value['month_date'])] = $value['sum'];
    }
    //
    $data_trade_total['fee_campaign'] += $value['sum'];
}
$this->view->data_trade = $data_trade;
$this->view->data_trade_total = $data_trade_total;
/* END TRADE MARKETTING*/

$this->view->params = $params;

$this->_helper->viewRenderer->setRender('trade/area-trade-details');
?>