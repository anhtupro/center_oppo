<?php 

$QBiTrade 			= new Application_Model_BiTrade();
$QRegionalMarket    = new Application_Model_RegionalMarket();


$params 		= array();
$air_investment = $QBiTrade->getAirInvestment($params);
$category 		= $QBiTrade->getCategoryBi($params);

$province = $QBiTrade->get_province();

$data = [];
foreach($air_investment as $key=>$value){
	$data[$value['area_id']][$value['cate_id']] = [
		'quantity'		=> $value['quantity'],
		'total_price'	=> $value['total_price'],
	];
}

$this->view->air_investment = $air_investment;
$this->view->province = $province;

$this->_helper->viewRenderer->setRender('/trade/trade-district');
