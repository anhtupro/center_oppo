<?php 

$from_date         = $this->getRequest()->getParam('from_date');
$to_date           = $this->getRequest()->getParam('to_date');
$area_list         = $this->getRequest()->getParam('area_id', array() );

$from_date = explode('/', $from_date);
$from_date = date('Y-m-01', strtotime($from_date[1].'-'.$from_date[0].'-01'));

$to_date = explode('/', $to_date);
$to_date = date('Y-m-t', strtotime($to_date[1].'-'.$to_date[0].'-01'));

$params = array(
    'from_date'     => $from_date,
    'to_date'       => $to_date,
    'area_list'     => $area_list
);


$QBiTrade       = new Application_Model_BiTrade();
$list_district      = $QBiTrade->getDistrictByArea($params);
$data_doanhthu_district = $QBiTrade->getDataDoanhthuDistrict($params);
$data_dautu_district    = $QBiTrade->getDataDautuDistrict($params);

//I.Lấy Chi phí đầu tư bảng hiệu
$params['group_cat_id'] = 1;
$banghieu_trade = $QBiTrade->getCategoryTradeDistrict($params);

//II.Lấy Chi phí đầu tư bàn trải nghiệm
$params['group_cat_id'] = 2;
$bantrainghiem_trade = $QBiTrade->getCategoryTradeDistrict($params);

//III.Lấy Chi phí đầu tư bàn trải nghiệm
$params['group_cat_id'] = 3;
$tukinh_trade = $QBiTrade->getCategoryTradeDistrict($params);

//IV.Lấy Chi phí đầu tư vách
$params['group_cat_id'] = 4;
$vach_trade = $QBiTrade->getCategoryTradeDistrict($params);

$chart_doanhthu_area = array();
$chart_doanhthu_0 = array();

foreach($list_district as $key=>$value){
    $chart_doanhthu_district[] = $value['name'];
    $chart_doanhthu_0[]    = $data_dautu_district[$value['id']]/$data_doanhthu_district[$value['id']]*100;
    $chart_banghieu_0[]    = (int)$banghieu_trade[$value['id']]['quantity'];
    $chart_banghieu_2[]    = (int)$banghieu_trade[$value['id']]['value'];

    $chart_bantrainghiem_0[]    = (int)$bantrainghiem_trade[$value['id']]['quantity'];
    $chart_bantrainghiem_2[]    = (int)$bantrainghiem_trade[$value['id']]['value'];

    $chart_tukinh_0[]    = (int)$bantrainghiem_trade[$value['id']]['quantity'];
    $chart_tukinh_2[]    = (int)$bantrainghiem_trade[$value['id']]['value'];

    $chart_vach_0[]    = (int)$vach_trade[$value['id']]['quantity'];
    $chart_vach_2[]    = (int)$vach_trade[$value['id']]['value'];
}

$average = array_sum($chart_doanhthu_0)/count($chart_doanhthu_0);
$average_bh = array_sum($chart_banghieu_0)/count($chart_banghieu_0);
$average_bh_value = array_sum($chart_banghieu_2)/count($chart_banghieu_2);

$average_btn = array_sum($chart_bantrainghiem_0)/count($chart_bantrainghiem_0);
$average_btn_value = array_sum($chart_bantrainghiem_2)/count($chart_bantrainghiem_2);

$average_tk = array_sum($chart_tukinh_0)/count($chart_tukinh_0);
$average_tk_value = array_sum($chart_tukinh_2)/count($chart_tukinh_2);

$average_v = array_sum($chart_vach_0)/count($chart_vach_0);
$average_v_value = array_sum($chart_vach_2)/count($chart_vach_2);

foreach($list_district as $key=>$value){
    $chart_doanhthu_1[] = $average;
    $chart_banghieu_1[] = $average_bh;
    $chart_banghieu_3[] = $average_bh_value;

    $chart_bantrainghiem_1[] = $average_btn;
    $chart_bantrainghiem_3[] = $average_btn_value;

    $chart_tukinh_1[] = $average_tk;
    $chart_tukinh_3[] = $average_tk_value;

    $chart_vach_1[] = $average_v;
    $chart_vach_3[] = $average_v_value;
}

//ADD JSON
$data_json = array();

$data_json['district'] = $chart_doanhthu_district; 
$data_json['chart_doanhthu_0'] = $chart_doanhthu_0; 
$data_json['chart_doanhthu_1'] = $chart_doanhthu_1;

$data_json['chart_banghieu_0'] = $chart_banghieu_0;
$data_json['chart_banghieu_1'] = $chart_banghieu_1;
$data_json['chart_banghieu_2'] = $chart_banghieu_2;
$data_json['chart_banghieu_3'] = $chart_banghieu_3;

$data_json['chart_bantrainghiem_0'] = $chart_bantrainghiem_0;
$data_json['chart_bantrainghiem_1'] = $chart_bantrainghiem_1;
$data_json['chart_bantrainghiem_2'] = $chart_bantrainghiem_2;
$data_json['chart_bantrainghiem_3'] = $chart_bantrainghiem_3;

$data_json['chart_tukinh_0'] = $chart_tukinh_0;
$data_json['chart_tukinh_1'] = $chart_tukinh_1;
$data_json['chart_tukinh_2'] = $chart_tukinh_2;
$data_json['chart_tukinh_3'] = $chart_tukinh_3;

$data_json['chart_vach_0'] = $chart_vach_0;
$data_json['chart_vach_1'] = $chart_vach_1;
$data_json['chart_vach_2'] = $chart_vach_2;
$data_json['chart_vach_3'] = $chart_vach_3;

echo json_encode($data_json);
exit;
