<?php 

$sn   = $this->getRequest()->getParam('sn');

$QKpiMonth 		= new Application_Model_KpiMonth();
$QGood          = new Application_Model_Good();
$QArea          = new Application_Model_Area();
$QStore         = new Application_Model_Store();

$params = array(
	'sn' => $sn
);

$air = $QKpiMonth->getListAir($params);
$params['store_id'] = $air[0]['store_id'];
$this->view->air = $air;


$where = $QStore->getAdapter()->quoteInto('id = ?', $params['store_id']);
$store = $QStore->fetchRow($where);
$this->view->store = $store;

//HIGHTCHART STORE S.O
//Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
$list_date = array(
    date('Y-m-d', strtotime(date('Y-m')." -3 month")),
    date('Y-m-d', strtotime(date('Y-m')." -2 month")),
    date('Y-m-d', strtotime(date('Y-m')." -1 month")),
    date('Y-m-d'),
);

$name_chart = array(
    '2316'  => 'TGDĐ',
    '2363'  => 'FPT',
    '2317'  => 'VTA',
    '9187'  => 'Vinpro',
    '10007' => 'VIETTEL',
    '2325'  => 'CES',
    '3'     => 'DIAMOND',
    '2'     => 'GOLD',
    '1'     => 'SILVER',
    '4'     => 'NORMAL',
    '999'   => 'OTHER KA',
    '888'   => 'RETURN',
);

//$params = array();
$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
$params['to'] = date('Y-m-d');
$sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
$data_good = $QKpiMonth->getSelloutGood($params);

/* get sellout, total value theo từng kênh */
$data_sellout_dealer = array();
$data_sellout_total = array();
$sellout_total = array(
    'num' => 0,
    'total_value' => 0
);
foreach ($sell_out_dealer as $key => $value) {
    $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                        'num' => $value['num'],
                                                                        'total_value' => $value['total_value']
                                                                        );


    if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
        $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
        $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
    }
    else{
        $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
        $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
    }

    $sellout_total['num'] += $value['num'];
    $sellout_total['total_value'] += $value['total_value'];
                                        
}
/* END sellout, total value theo từng kênh */

$this->view->data_sellout_dealer = $data_sellout_dealer;
$this->view->data_sellout_total  = $data_sellout_total;
$this->view->sellout_total       = $sellout_total;
$this->view->list_date           = $list_date;
$this->view->name_chart          = $name_chart;

$this->view->data_sellout_good = $data_good;
$this->view->good_hero         = unserialize(LIST_PRODUCT_HERO_BI);
$this->view->good              = $QGood->get_cache();
$this->view->params            = $params;
//END HIGHTCHART STORE S.O

$this->_helper->viewRenderer->setRender('trade/area-trade-details-decor-t');
?>