<?php 

$area   = $this->getRequest()->getParam('area');
$campaign   = $this->getRequest()->getParam('campaign');
$title   = $this->getRequest()->getParam('title');

$QKpiMonth        = new Application_Model_KpiMonth();
$QArea 	          = new Application_Model_Area();
$where_area       = $QArea->getAdapter()->quoteInto('id = ?', $area);
$data_area 		  = $QArea->fetchRow($where_area);


//Phân quyền
$params = array();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm = new Application_Model_Asm();
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

}

if((!in_array($area, $params['area_list'])) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    $this->_redirect(HOST);
    exit;
}
//ENd phân quyền

$params = array(
	'area'       => $area,
	'campaign'	 => $campaign,
	'title'		 => $title,
	'from'		 => date('Y-m-01', strtotime(date('Y-m')." -3 month")),
	'to'		 => date('Y-m-d')
);


$list_date = array(
    date('Y-m-d', strtotime(date('Y-m')." -3 month")),
    date('Y-m-d', strtotime(date('Y-m')." -2 month")),
    date('Y-m-d', strtotime(date('Y-m')." -1 month")),
    date('Y-m-d'),
);

$params['month'] = array();

foreach ($list_date as $key => $value) {
    $params['month'][] = intval(date('m', strtotime($value)));
}
$total_price  = 0;

if(!empty($area)){
	$fee_campaign = $QKpiMonth->get_fee_campaign_area2($params);
}
else{
	$fee_campaign = $QKpiMonth->get_fee_campaign_area_noarea($params);
}

$this->view->params = $params;
$this->view->data_area  = $data_area;
$this->view->total_price  = $total_price;

$this->view->fee_campaign   = $fee_campaign;

$this->_helper->viewRenderer->setRender('trade/area-trade-details-posm-t');
?>