<?php

	$QKpiMonth        = new Application_Model_KpiMonth();
    $QAsm            = new Application_Model_Asm();
    $id_trainer  = $this->getRequest()->getParam('trainer_id'); 
	$id  = $this->getRequest()->getParam('id');	
    $type  = $this->getRequest()->getParam('type'); 
	$month	= $this->getRequest()->getParam('month');	
    $year  = $this->getRequest()->getParam('year'); 

    $area = $QAsm->get_cache($id_trainer);
    $list_kv = $area['area'];
    $params['area_list'] = $list_kv;
    $params['id']	= $id;
    $params['type']   = $type;
    $params['month']	=$month;
    $params['year']    =$year;
    if($type==3){
        $list_area_course = $QKpiMonth->getListAreaCourse($params);
    }elseif($type==1 || $type == 2)
    {
        $list_area_course = $QKpiMonth->getAreaTrainerReport($params);
    }
    // if($type == 2){
    //     $list_area_dealer=$QKpiMonth->getAreaDealerTrainingReport($params);
    // }
    $this->view->list_area_course = $list_area_course;
    $this->view->params=$params;
   // $this->view->list_area_dealer=$list_area_dealer;


?>