<?php
    $QArea              = new Application_Model_Area();
    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();
    $rank               = $this->getRequest()->getParam('rank');
    $regional               = $this->getRequest()->getParam('regional');
    $area               = $this->getRequest()->getParam('area');
    $sort                 = $this->getRequest()->getParam('sort', '');
    $desc                   = $this->getRequest()->getParam('desc', 1);
    $page                   = $this->getRequest()->getParam('page', 1);
    $params =array(
        'rank'                => $rank,
        'regional'             => $regional,
        'area'                  => $area
    );
    $params['sort']         = $sort;
    $params['desc']         = $desc;
    $total                      = 0;
    $limit                      = LIMITATION;
    //lấy khu vưc
    // $area                   = $QAsm->get_cache($id);
    // $list_area              = $area['area'];
    // $params['area_list']    = $list_area;
//var_dump($area); exit;



    $rows = $QKpiMonth->getRankStarStaff($page, $limit, $total,$params);


$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->result     = $rows;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->area       = $QArea->get_cache();
$this->view->url        = HOST.'bi/rank-star-staff'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset     = $limit*($page-1); 



?>