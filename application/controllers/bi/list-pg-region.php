<?php
    $QArea              = new Application_Model_Area();
    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
  
    $id                     = $this->getRequest()->getParam('id');
    $y                      = $this->getRequest()->getParam('y');
    $m                      = $this->getRequest()->getParam('m');
    $regional                   = $this->getRequest()->getParam('regional');
    $params =array(
        'y'                 => intval($y),
        'm'                 => intval($m),
        'id'                => $id,
        'regional'          => $regional
    );
    
    //lấy khu vưc
    if($userStorage->group_id == BOARD_ID){
        $area_cache = $QArea->get_cache();
        $area = [];
        foreach($area_cache as $key=>$value){
            $area[] = $key;
        }
        $list_area = $area;
    }
    else{
        $area                   = $QAsm->get_cache($id);
        $list_area              = $area['area'];
    }

    $params['area_list']    = $list_area;
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -4 month"));
    $params['to'] = date('Y-m-d');

    $rows = $QKpiMonth->getListPgRegion($params);

    $this->view->result     = $rows;
    $this->view->params     = $params;

    $this->view->area       = $QArea->get_cache();


?>