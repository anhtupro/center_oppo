<?php

$QKpiMonth = new Application_Model_KpiMonth();
$QTeam = new Application_Model_Team();
$QAsm = new Application_Model_Asm();

$params = array();

/* Lấy khu vực của ASM */
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
    // lấy khu vực của asm
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
}

if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    exit;
}
/* END Lấy khu vực của ASM */

$list_trainer = $QKpiMonth->getListTrainer($params);
$team = $QTeam->get_cache();

$this->view->list_trainer = $list_trainer;
$this->view->team = $team;
$this->_helper->viewRenderer->setRender('training/trainer-area');

