<?php

$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$lesson_id 		 = $this->getRequest()->getParam('lesson_id');
$area_id 		 = $this->getRequest()->getParam('area_id');
$type			 = $this->getRequest()->getParam('type');
$title			 = $this->getRequest()->getParam('title');
$pgs_supervisor			 = $this->getRequest()->getParam('pgs_supervisor');

//var_dump($lesson_id); exit;
$total              = 0;
$limit              = LIMITATION;


$params = array(
    'lesson_id' => $lesson_id,
    'area_id'	=> $area_id,
    'title'     => $title,
    'type'      => $type,
    'region_id' => $region_id
);

if(empty($title)){
    $params['title']         = unserialize(LIST_PGS_BI);
}
if(empty($type)){
    $type='fail';
}


$params['sort']                 = $sort;
$params['desc']                 = $desc;

$QKpiMonth = new Application_Model_KpiMonth();
$QLesson   = new Application_Model_Lesson();
$QAsm 	   = new Application_Model_Asm();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
	// lấy khu vực của asm
	$asm_cache = $QAsm->get_cache();
	$params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
	$params['area'] = $params['area_list'];
}

$where      = array();
$where[]    = $QLesson->getAdapter()->quoteInto('id = ?', $lesson_id);
$lesson 	= $QLesson->fetchRow($where);

if(!empty($pgs_supervisor)){
    $params['list_staff'] = [];
    $list_staff = $QKpiMonth->getStaffByPgsSupervisor($pgs_supervisor);
    foreach($list_staff as $key=>$value){
        $params['list_staff'][] = $value['staff_id'];
    }
}


if($type=='fail')
{
    $list_fail = $QKpiMonth->getListFail($page, $limit, $total, $params);
}elseif($type=='pass')
{
	$list_fail = $QKpiMonth->getListPass($page, $limit, $total, $params);
}

$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->list_fail  = $list_fail;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'bi/list-fail' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);

$this->view->lesson = $lesson;

$this->_helper->viewRenderer->setRender('training/list-fail');

