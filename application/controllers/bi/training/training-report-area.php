<?php
$QArea = new Application_Model_Area();
$QAsm = new Application_Model_Asm();
$params = array();
/* Phân quyền */
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;

if (in_array($group_id, My_Staff_Group::$allow_in_area_view) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
    // lấy khu vực của asm
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
}

if($group_id == TRAINING_LEADER_ID){
    // lấy khu vực của trainer
    $area = $QAsm->get_cache($userStorage->id);
    $list_area = $area['area'];
    $params['area_list'] = $list_area;

}

if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    exit;
}
/* end Phân quyền */

$list_date = array(
	date('Y-m-d', strtotime(date('Y-m')." -3 month")),
	date('Y-m-d', strtotime(date('Y-m')." -2 month")),
	date('Y-m-d', strtotime(date('Y-m')." -1 month")),
	date('Y-m-d'),
);

$QStaffTrainingReport = new Application_Model_StaffTrainingReport();
$row_trainer = $QStaffTrainingReport->reportDataBiArea($params);

$data_trainer = array();
foreach ($row_trainer as $key => $value) {
	$data_trainer[$value['area_id']][$value['type']][$value['month_date']][$value['year_date']] = array(
																'sum' => $value['sum'],
																'quantity_session' => $value['quantity_session']
																);
}


$this->view->data_trainer = $data_trainer;
$this->view->list_date  = $list_date;
$this->view->params = $params;

$this->view->area = $QArea->get_cache();
if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    $where = array();
    $where = $QArea->getAdapter()->quoteInto('id IN (?)', $params['area_list']);
    $area_array = array();
    $area = $QArea->fetchAll($where);

    foreach ($area as $key => $value) {
        $area_array[$value['id']] = $value['name'];
    }
    $this->view->area = $area_array;
}

$this->_helper->viewRenderer->setRender('training/training-report-area');

