<?php
$QArea              = new Application_Model_Area();
$QRankStarSale=new Application_Model_RankStarSales();
$QAsm                   = new Application_Model_Asm();
$QTeam= new Application_Model_Team();

$rank               = $this->getRequest()->getParam('rank');
$bigarea_id               = $this->getRequest()->getParam('bigarea_id');
$regional               = $this->getRequest()->getParam('regional');
$area_id               = $this->getRequest()->getParam('area_id');
$sort                 = $this->getRequest()->getParam('sort', '');
$desc                   = $this->getRequest()->getParam('desc', 1);
$page                   = $this->getRequest()->getParam('page', 1);
$params =array(
    'rank'                => $rank,
    'regional'             => $regional,
    'area_id'                  => $area_id,
    'bigarea_id'                  => $bigarea_id
);
$params['sort']         = $sort;
$params['desc']         = $desc;
$total                      = 0;
$limit                      = LIMITATION;
$team=$QTeam->get_cache();

//lấy khu vưc
// $area                   = $QAsm->get_cache($id);
// $list_area              = $area['area'];
// $params['area_list']    = $list_area;
//var_dump($area); exit;
// lấy khu vưc
$area                   = $QAsm->get_cache($id);
$list_area              = $area['area'];
$params['area_list']    = $list_area;
$StaffScores = $QRankStarSale->fetchPagination($page, $limit, $total, $params);


$rows = $QRankStarSale->fetchPagination($page, $limit, $total,$params);
//debug($team);

$this->view->team       = $team;
$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->result     = $rows;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->area       = $QArea->get_cache();
$this->view->url        = HOST.'bi/rank-star-staff-sales'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset     = $limit*($page-1);



?>