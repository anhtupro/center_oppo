<?php
    $QArea              = new Application_Model_Area();
    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();

  
    $id                     = $this->getRequest()->getParam('id');
    $y                      = $this->getRequest()->getParam('y');
    $m                      = $this->getRequest()->getParam('m');
    $regional                   = $this->getRequest()->getParam('regional');
    $params =array(
        'y'                 => intval($y),
        'm'                 => intval($m),
        'id'                => $id,
        'regional'          => $regional
    );
    //lấy khu vưc
    $area                   = $QAsm->get_cache($id);
    $list_area              = $area['area'];
    $params['area_list']    = $list_area;
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
    $params['to'] = date('Y-m-d');



    $data = $QKpiMonth->getExportListPgRegion($params);

    include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    $heads = array(
        'CODE',
        'FULLNAME',
        'DEPARTMENT',
        'TEAM',
        'TITLE',
        'AREA'
       
    );
    foreach ($heads as $key)
    {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

     
     $index = 2;
    foreach($data as $key=>$value){
     $alpha = 'A';

     $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
     $sheet->getCell($alpha++ . $index)->setValueExplicit($value['fullname'],PHPExcel_Cell_DataType::TYPE_STRING);
     $sheet->getCell($alpha++ . $index)->setValueExplicit($value['department'],PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++ . $index)->setValueExplicit($value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
       $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['regional'],PHPExcel_Cell_DataType::TYPE_STRING);
         $index ++;
    } 
    // //format
    

    $filename = ' Staff_Pg ';
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
    $objWriter->save('php://output');
    exit;
    $this->redirect(HOST.'bi/list-pg-sellout-area?id='.$id.'&y='.$y.'&m='.$m);




?>