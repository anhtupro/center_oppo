<?php 
     
    $page = $this->getRequest()->getParam('page', 1);
    $title = $this->getRequest()->getParam('key');
    $content = $this->getRequest()->getParam('content');
    $sort = $this->getRequest()->getParam('sort');
    $desc = $this->getRequest()->getParam('desc', 1);
    
    $limit = LIMITATION;
    $total = 0;
    
    $params = array_filter(array(
        'title' => $title,
        'content' => $content,
        'sort' => $sort,
        'desc' => $desc,
        ));
    $params['sort'] = $sort;
    $params['desc'] = $desc;
    $params['group_cat'] = 1;

            
    $this->view->params = $params;
    $this->view->sort = $sort;
    $this->view->desc = $desc;
    
    
    unset($params['group_cat']);
    $QModel = new Application_Model_Lesson();
    $inform = $QModel->fetchPagination($page, $limit, $total, $params);
    $QInformCategory = new Application_Model_LessonCat();
    $this->view->informs = $inform;
    
    
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->url = HOST . 'bi/sales-leader/' . ($params ? '?' . http_build_query($params) .
        '&' : '?');
    $this->view->offset = $limit * ($page - 1);
    
    $flashMessenger = $this->_helper->flashMessenger;
    $messages = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;        

?>