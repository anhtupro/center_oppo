<?php
    
    $QKpiMonth        	= new Application_Model_KpiMonth();
    $QAsm            	= new Application_Model_Asm();
 	$QArea           	= new Application_Model_Area();

    $sort            	= $this->getRequest()->getParam('sort', '');
	$desc            	= $this->getRequest()->getParam('desc', 1);
	$page           	= $this->getRequest()->getParam('page', 1);
	$type  				= $this->getRequest()->getParam('type');
	$id					= $this->getRequest()->getParam('id');

	//search
	$name  				= $this->getRequest()->getParam('name');
	$code  				= $this->getRequest()->getParam('code');
	$email  			= $this->getRequest()->getParam('email');
	$department  		= $this->getRequest()->getParam('department');
	$team  				= $this->getRequest()->getParam('team');
	$star  				= $this->getRequest()->getParam('star');
	$title  			= $this->getRequest()->getParam('title');
	$shop  				= $this->getRequest()->getParam('shop');
	$from_date 			= $this->getRequest()->getParam('from_date');
	$to_date 			= $this->getRequest()->getParam('to_date');
	$sellout_from  		= $this->getRequest()->getParam('sellout_from');
	$sellout_to 		= $this->getRequest()->getParam('sellout_to');
	$area_id 			= $this->getRequest()->getParam('area_id');
	$province_id		= $this->getRequest()->getParam('province_id');

	$params =array(
		'id'			=>$id,
		'type'			=>$type,
		'name'			=>$name,
		'code'			=>$code,
		'email'			=>$email,
		'department'	=>$department,
		'team'			=>$team,
		'star'			=>$star,
		'title'			=>$title,
		'shop'			=>$shop,
		'from_date'		=>$from_date,
		'to_date'		=>$to_date,
		'sellout_to'	=>$sellout_to,
		'sellout_from'	=>$sellout_from,
		'area_id'		=>$area_id,
		'province_id'	=>$province_id
	);
	//lấy khu vưc
        
        //PHÂN QUYỀN
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        //Phan quyen cho tien.vo Brandshop
        if($userStorage->id == 910){
            $params['list_title_staff'] = [419, 403];
        }
        
        
    $area = $QAsm->get_cache($id);
    $list_area = $area['area'];
    $params['area_list'] = $list_area;
    $params['sort']                 = $sort;
	$params['desc']                 = $desc;

$total              	 = 0;
$limit             		 = LIMITATION;

	$rows = $QKpiMonth->getListSeniority($page, $limit, $total, $params);

$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->result  	= $rows;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->area       = $QArea->get_cache();

$this->view->url 		= HOST.'bi/list-pg-seniority'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset 	= $limit*($page-1); 

?>
