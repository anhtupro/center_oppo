<?php 
	set_time_limit(0);
    
    $QStaff           = new Application_Model_Staff();
    $QKpiMonth        = new Application_Model_KpiMonth();
    $QArea            = new Application_Model_Area();
    $QRegionalMarket  = new Application_Model_RegionalMarket();
    $QGood            = new Application_Model_Good();
    $QStaffTrainingReport = new Application_Model_StaffTrainingReport();
    $QAsm               = new Application_Model_Asm();
    $QTrainerCourseType = new Application_Model_TrainerCourseType();
    $QRankByMonth = new Application_Model_RankByMonth();
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
        exit;
    }

	$group_id    = $userStorage->group_id;
    $areas       = $QArea->get_cache();
    $this->view->areas = $areas;
    
    if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    
    }
    
    $district = $this->getRequest()->getParam('district');
    
    $area = $QRegionalMarket->getAreaByDistrict($district);

	if((!in_array($area, $params['area_list'])) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        $this->_redirect(HOST);
        exit;
    }

	$params = array(
		'area'       => $area,
		'area_list'  => $area,
        'district'   => $district
	);
	
    
    //Biểu đồ
    
    $year3 = date('Y', strtotime(date('Y-m')." -3 month"));
    $year2 = date('Y', strtotime(date('Y-m')." -2 month"));
    $year1 = date('Y', strtotime(date('Y-m')." -1 month"));
    $year0 = date('Y', strtotime(date('Y-m')." 0 month"));
    $year = array(
        $year0,
        $year1,
        $year2,
        $year3
    );
    
    //
    $year = array(
        date('Y', strtotime(date('Y-m')." 0 month")),
        date('Y', strtotime(date('Y-m')." -1 month")),
        date('Y', strtotime(date('Y-m')." -2 month")),
        date('Y', strtotime(date('Y-m')." -3 month"))
    );
    $this->view->year = $year;
    //
    $month3 = intval(date('m', strtotime(date('Y-m')." -3 month")));
    $month2 = intval(date('m', strtotime(date('Y-m')." -2 month")));
    $month1 = intval(date('m', strtotime(date('Y-m')." -1 month")));
    $month0 = intval(date('m', strtotime(date('Y-m')." 0 month")));
    
    $list_month = $month3.",".$month2.",".$month1.",".$month0;
    $this->view->list_month = explode(',',$list_month);
    
    $params['area_list'] = array($area);
    
    //Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );

    $name_chart = array(
        '2316'  => 'TGDĐ',
        '2363'  => 'FPT',
        '2317'  => 'VTA',
        '9187'  => 'Vinpro',
        '10007' => 'VIETTEL',
        '2325'  => 'CES',
        '3'     => 'DIAMOND',
        '2'     => 'GOLD',
        '1'     => 'SILVER',
        '4'     => 'NORMAL',
        '999'   => 'OTHER KA',
        '888'   => 'RETURN',
    );

    //$params = array();
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
    $params['to'] = date('Y-m-d');

    $sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
    $data_good = $QKpiMonth->getSelloutGood($params);

    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
            $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
        }
        else{
            $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
        }
                                            
    }
    /* END sellout, total value theo từng kênh */

    $this->view->data_sellout_dealer = $data_sellout_dealer;
    $this->view->data_sellout_total = $data_sellout_total;
    $this->view->list_date = $list_date;
    $this->view->name_chart = $name_chart;

    $this->view->data_sellout_good = $data_good;
    $this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
    $this->view->good = $QGood->get_cache();

    $this->view->params = $params;

    /*total_sales_channel */
    $name_chart_sales_channel = array(
        '2316'  => 'TGDĐ',
        '2363'  => 'FPT',
        '2317'  => 'VTA',
        '9187'  => 'Vinpro',
        '10007' => 'VIETTEL',
        '2325'  => 'CES',
        '5'     => 'Diamond Club'
    );

    $total_sales_channel = $QKpiMonth->getTotalSalesChannel($params);
    $sales_channel_quarter = $QKpiMonth->getTotalSalesChannelQuarter($params);
    $sales_channel_quarter_this = $QKpiMonth->getTotalSalesChannelQuarterThis($params);

    $sales_channel = array();
    foreach ($total_sales_channel as $key => $value) {
        $sales_channel[$value['channel']][$value['title_sum']] = $value['sum'];
    }
    $this->view->sales_channel = $sales_channel;
    $this->view->name_chart_sales_channel = $name_chart_sales_channel;
    $this->view->sales_channel_quarter    = $sales_channel_quarter;

    $this->view->sales_channel_quarter_first = $sales_channel_quarter[1];
    $this->view->sales_channel_quarter_this  = $sales_channel_quarter_this;

    /*total_store_channel */
    $total_store_channel   = $QKpiMonth->getTotalStoreChannel($params);
    $store_channel = array();
    foreach ($total_store_channel as $key => $value) {
        $store_channel[$value['channel']] = $value['sum'];
    }
    $this->view->store_channel = $store_channel;

    $staff = $QKpiMonth->getListStaff($params);
    $staff_title = array();
    $total_staff = 0;
    foreach ($staff as $key => $value) {
        if(!empty($staff_title[$value['title']])){
            $staff_title[$value['title']]['num'] += 1;
            $total_staff += 1;
        }
        else{
            $staff_title[$value['title']]['num'] = 1;
            $staff_title[$value['title']]['name'] = $value['title_name'];
            $total_staff += 1;
        }
    }
    $this->view->total_staff = $total_staff;
    $this->view->staff_title = $staff_title;

    /* STAR */
    $rank_star_result = $QKpiMonth->getRankStar($params);
    $rank_star = array();
    foreach($rank_star_result as $k => $v){
        $rank_star[$v['star']] = $v['sum'];
    }
    $this->view->rank_star = $rank_star;

    /* sellout by date */
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -1 month"));
    $data_good_by_date = $QKpiMonth->getGoodByDate($params);
    $good_by_date = array();
    foreach ($data_good_by_date as $key => $value) {
        $good_by_date[$value['date']][$value['good_id']] = $value['num'];
        $good_by_date[$value['date']][9999] += $value['num'];
    }
    $list_date_range = $this->date_range($params['from'], $params['to']);
    $this->view->good_by_date = $good_by_date;
    $this->view->list_date_range = $list_date_range;

    //Số bán hàng ngày
    $good_hero_total = array(
        PRODUCT_HERO_BI => PRODUCT_HERO_BI_NAME,
        '9999' => 'Total'
    );
    $this->view->good_hero_total = $good_hero_total;
    /* end sellout by date */

    /* RANK SELLOUT BY PG */
    $rank_by_month = $QRankByMonth->getAll($params);

    $rank = array();
    foreach ($rank_by_month as $key => $value) {
        $rank[$value['rank']][$value['month']][$value['year']] = array(
            'sum_store' =>  $value['sum'],
            'sum_pg'    =>  $value['count_staff'],
            );
    }
    $this->view->rank = $rank;
    /* END RANK SELLOUT BY PG */


    $this->view->district = $QRegionalMarket->get_district_cache();
?>