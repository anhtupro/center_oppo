<?php 
	
	$area 		= $this->getRequest()->getParam('area');
	$staff_main = $this->getRequest()->getParam('staff_main');
	$content 	= $this->getRequest()->getParam('content');
	$submit 	= $this->getRequest()->getParam('submit');
	$title 		= $this->getRequest()->getParam('title');

	$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
	$QStaffRewardWarning = new Application_Model_StaffRewardWarning();

	$flashMessenger = $this->_helper->flashMessenger;

	if(!empty($submit)){

		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {

			foreach($staff_main as $key=>$staff){
				foreach($staff as $k=>$value){

					$staffID      	= $key;
					$warning_type 	= $value;
					$type 			= 1; //Khen thưởng
					$content_reward = $content[$staffID][$warning_type];

					$data = array(
			            'staff_id' => $staffID,
			            'type'     => $type,
			            'content'  => $content_reward,
			            'date'     => date('Y-m-d'),
			            'warning_type' => !empty($warning_type) ? $warning_type : null,
			            'created_at'   => date('Y-m-d H:i:s'),
	        			'created_by'   => $userStorage->id,
			        );
					
					$QStaffRewardWarning->insert($data);

				}
			}
			
			$flashMessenger->setNamespace('success')->addMessage('Cộng điểm thành công!');
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
			$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
		}
		
		$this->_redirect(HOST.'bi/reward-warning-all');
	
	}
	

	$params = [
		'area'	=> $area
	];

	$QArea  = new Application_Model_Area();
	$QAsm   = new Application_Model_Asm();
	$QKpiMonth   = new Application_Model_KpiMonth();
	$QStaff = new Application_Model_Staff();
	$areas = $QArea->get_cache();

	$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
	$id_trainer 			= $userStorage->id;

	if(!in_array($userStorage->title, [317, 175])){
		$this->_redirect(HOST.'bi');
	}

	$area 					= $QAsm->get_cache($id_trainer);

	$params['area_list'] 	= $area['area'];
	$params['title'] 		= $title;

	if(empty($params['area_list'])){
		$this->_redirect(HOST.'bi');
	}

	$staff = $QKpiMonth->fetchStaffTitlePagination($page, $limit, $total, $params);

	$this->view->staff = $staff;
	$this->view->areas = $areas;
	$this->view->params = $params;

	$this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();


?>