<?php 
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$KpiMonth = new Application_Model_KpiMonth();
$QAsm     = new Application_Model_Asm();
$params   = array();

/* Phân quyền */
if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
    exit;
}

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
    // lấy khu vực của asm
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    $params['area'] = $params['area_list'];
}

if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    exit;
}
/* end phân quyền */

$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -1 month"));
$params['to'] = date('Y-m-d');

$good_by_date = $KpiMonth->getGoodByDate($params);
$data = array();
foreach ($good_by_date as $key => $value) {
    $data[$value['date']][$value['good_id']] = $value['num'];
    $data[$value['date']][9999] += $value['num'];
}


//Data final
$list_date = $this->date_range($params['from'], $params['to']);
$list_good = array(HERO_ID, 9999);

$data_json = array();
foreach ($list_date as $key => $value) {
    $data_json['name'][] = $value;
    foreach ($list_good as $k => $v) {
        $data_json['good'.$v][] = (isset($data[$value][$v]) and $data[$value][$v]) ? intval($data[$value][$v]) : 0;
    }
}
//END Data final

echo json_encode($data_json);
exit;


?>