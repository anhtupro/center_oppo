<?php 

$month_check  = $this->getRequest()->getParam('month');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$KpiMonth = new Application_Model_KpiMonth();
$QAsm     = new Application_Model_Asm();
$params   = array();

/* Phân quyền */
if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
    exit;
}

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
    // lấy khu vực của asm
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    $params['area'] = $params['area_list'];
}

if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
    exit;
}
/* end phân quyền */

$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -1 month"));
$params['to'] = date('Y-m-t', strtotime(date('Y-m')." -1 month"));

$month = intval(date('m', strtotime($params['from'])));
$year = intval(date('Y', strtotime($params['from'])));

$where = array();
$where[] = $KpiMonth->getAdapter()->quoteInto('month = ?', $month);
$where[] = $KpiMonth->getAdapter()->quoteInto('year = ?', $year);
$kpi_month = $KpiMonth->fetchRow($where);

if(!isset($kpi_month)){
    $db         = Zend_Registry::get('db');
    $sql = 'CALL sp_bi(?,?)';
    $stmt = $db->query($sql, array($params['from'], $params['to']));
    $stmt->closeCursor();  
}


$params['from'] = date('Y-m-01', strtotime(date('Y-m-d')." -0 month"));
$params['to'] = date('Y-m-t', strtotime(date('Y-m-d')." -0 month"));


$good_by_month = $KpiMonth->loadGoodByMonthAll($params);

$data_sellout_dealer = array();
$data_sellout_total = array();
foreach ($good_by_month as $key => $value) {
    $data_sellout_dealer[$value['channel']][$value['month_date']][$value['year_date']] = array(
                                                                        'num' => $value['num'],
                                                                        'total_value' => $value['total_value']
                                                                        );


    if(isset($data_sellout_total[$value['month_date']][$value['year_date']]) and $data_sellout_total[$value['month_date']][$value['year_date']]){
        $data_sellout_total[$value['month_date']][$value['year_date']]['num']         +=  $value['num']; 
        $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] +=  $value['total_value']; 
    }
    else{
        $data_sellout_total[$value['month_date']][$value['year_date']]['num']         =  $value['num'];  
        $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] =  $value['total_value'];  
    }
                                        
}


$params['from'] = date('Y-m-01', strtotime(date('Y-m-d')." -".intval($month_check)." month"));
$params['to'] = date('Y-m-t', strtotime(date('Y-m-d')." -0 month"));
//Data final
$list_date = $this->month_range($params['from'], $params['to']);

$data_json = array();
foreach ($list_date as $key => $value) {
    $m = intval(date('m', strtotime($value)));
    $y = intval(date('Y', strtotime($value)));

    $data_json['months'][] = "Tháng ".$m;
    $data_json['tgdd'][] = !empty($data_sellout_dealer[2316][$m][$y]['num']) ? intval($data_sellout_dealer[2316][$m][$y]['num']) : 0;
    $data_json['other'][] = !empty($data_sellout_total[$m][$y]['num']-$data_sellout_dealer[2316][$m][$y]['num']) ? intval($data_sellout_total[$m][$y]['num']-$data_sellout_dealer[2316][$m][$y]['num']) : 0;
    $data_json['total'][] = !empty($data_sellout_total[$m][$y]['num']) ? intval($data_sellout_total[$m][$y]['num']) : 0;

    $data_json['tgdd_doanhthu'][] = !empty($data_sellout_dealer[2316][$m][$y]['total_value']) ? floatval($data_sellout_dealer[2316][$m][$y]['total_value']) : 0;
    $data_json['other_doanhthu'][] = !empty($data_sellout_total[$m][$y]['total_value']-$data_sellout_dealer[2316][$m][$y]['total_value']) ? floatval($data_sellout_total[$m][$y]['total_value']-$data_sellout_dealer[2316][$m][$y]['total_value']) : 0;
    $data_json['total_doanhthu'][] = !empty($data_sellout_total[$m][$y]['total_value']) ? floatval($data_sellout_total[$m][$y]['total_value']) : 0;
}
//END Data final
echo json_encode($data_json);
exit;


?>