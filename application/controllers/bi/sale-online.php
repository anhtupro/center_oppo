<?php
$QMarket        = new Application_Model_Market();

$list_date = array(        
        date('Y-m-d', strtotime(date('Y-m')." -6 month")),
	date('Y-m-d', strtotime(date('Y-m')." -5 month")),
	date('Y-m-d', strtotime(date('Y-m')." -4 month")),
	date('Y-m-d', strtotime(date('Y-m')." -3 month")),
	date('Y-m-d', strtotime(date('Y-m')." -2 month")),
	date('Y-m-d', strtotime(date('Y-m')." -1 month")),       
);
$name_chart = unserialize(ONLINE_CHANNEL);
$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -6 month"));
$params['to'] = date('Y-m-d');
$channel = array();

foreach ($name_chart as $key => $value) {
	$channel[] = $key;
}

$params['channel'] = $channel;
$this->view->list_date = $list_date;
$this->view->name_chart = $name_chart;
if(isset($params['channel']) and $params['channel']){
    $sell_in_dealer      = $QMarket->onlineSellIn($params);
    
    $data_sellin_dealer = array();
    foreach ($sell_in_dealer as $key => $value) {
        $data_sellin_dealer[$value['channel']][$value['month']] = array(
                                                                            'sum_acc' => $value['sum_acc'],
                                                                            'total_price' => $value['total_price'],
                                                                            'total_activated' => $value['total_activated']
                                                                        );
    }
    $this->view->data_sellin_dealer = $data_sellin_dealer;
}
