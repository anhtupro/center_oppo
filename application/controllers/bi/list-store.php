<?php
    
    $name = $this->getRequest()->getParam('name');
    $area = $this->getRequest()->getParam('area');
    $province = $this->getRequest()->getParam('province');
    $district = $this->getRequest()->getParam('district');
    $from       = $this->getRequest()->getParam('from');
    $to         = $this->getRequest()->getParam('to');
    $QStore     = new Application_Model_Store();
    $QAppAir    = new Application_Model_AppAir();
    $flashMessenger       = $this->_helper->flashMessenger;

    $title       = $userStorage->title;
    $id_user= $userStorage->id;
    $id   = $this->getRequest()->getParam('id');

     $params = array(
                'id'            =>$id,
                'title'         => $title,
                'id_user'       =>$id_user,
                'name'          =>$name,
                'area'          =>$area,
                'province'      =>$province,
                'district'      =>$district,
                'brand_shop'    => 1,
                'from'          => $from,
                'to'            => $to
                );

    $page           = $this->getRequest()->getParam('page', 1);
    $limit          = LIMITATION;
    $total          = 0;
    $rows = $QStore->fetchPaginationStoreBrandshop($page, $limit, $total, $params);

    $get_sellout_month = $QStore->getSelloutBranshopMonth($params);

    $sellout_month = [];
    foreach($get_sellout_month as $key=>$value){
        $sellout_month[$value['id']][$value['year'].'-'.$value['month']] = $value['sellout'];
    }

    $list_date = array(
        date('Y-n', strtotime(date('Y-m')." -3 month")),
        date('Y-n', strtotime(date('Y-m')." -2 month")),
        date('Y-n', strtotime(date('Y-m')." -1 month")),
        date('Y-n'),
    );

    $this->view->result         = $rows;
    $this->view->limit          = $limit;
    $this->view->total          = $total;
    $this->view->params         = $params;
    $this->view->sellout_month  = $sellout_month;
    $this->view->list_date      = $list_date;
    $this->view->url = HOST.'bi/list-store'.($params ? '?'.http_build_query($params).'&' : '?');
    $this->view->offset = $limit*($page-1); 

    $area = $QAppAir->getArea();
    $this->view->area=$area;
    // ---------------HIEN THI THONG BAO ----------------------
    if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){

        $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_error = $messages_error;

    }
    if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
        $messages          = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
    }
