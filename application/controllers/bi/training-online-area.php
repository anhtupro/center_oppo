<?php 
    
    $id         = $this->getRequest()->getParam('id');
    $QKpiMonth = new Application_Model_KpiMonth();
    $Area = new Application_Model_Area();
    $QAsm = new Application_Model_Asm();

    $params = array(
        'lesson_id' => $id
    );

    /* Phân quyền */
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $group_id    = $userStorage->group_id;

    if (in_array($group_id, My_Staff_Group::$allow_in_area_view) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    }

    if($group_id == TRAINING_LEADER_ID){
        // lấy khu vực của trainer
        $area = $QAsm->get_cache($userStorage->id);
        $list_area = $area['area'];
        $params['area_list'] = $list_area;
    }

    if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        exit;
    }
    /* end phân quyền */

    $staff = $QKpiMonth->getStaffBiArea($params);
    $staff_area = array();
    foreach($staff as $key=>$value){
        $staff_area[$value['area_id']] = $value['num'];
    }

    $row_lesson = $QKpiMonth->reportDataLessoArea($params);
    $total_pgs_sales = $QKpiMonth->totalPgsSales($params);

    $area_num = array();
    foreach ($total_pgs_sales as $key => $value) {
        $area_num[$value['area_id']] = $value['num'];
    }

    $this->view->row_lesson = $row_lesson;
    $this->view->area = $Area->get_cache();
    $this->view->area_num = $area_num;
    $this->view->staff_area = $staff_area;

?>