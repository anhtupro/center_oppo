<?php 
    $page      = $this->getRequest()->getParam('page', 1);
    $from      = $this->getRequest()->getParam('from');
	$to        = $this->getRequest()->getParam('to');
    $name      = $this->getRequest()->getParam('name');
    $good_id   = $this->getRequest()->getParam('good_id');
    $parent    = $this->getRequest()->getParam('parent');
    $export    = $this->getRequest()->getParam('export');
    
    
    $current_date       = date('Y-m-d');
    $from_current_date  = date('Y-m-d', strtotime(date($current_date)." -1 month"));
    $from = is_null($from) ? $from_current_date : date("Y-m-d",strtotime($from));
    $to  = is_null($to) ? $current_date : date("Y-m-d",strtotime($to));
    
    $QKpiMonth = new Application_Model_KpiMonth();
    $QGood     = new Application_Model_Good();
    
    $range = $this->createDateRangeArray($from, $to);
    
    
    $range_date = "";
    foreach($range as $key=>$value){
        $range_date .= ",MAX(CASE WHEN date = '".$value."' THEN num END) AS `".$value."`";
    }
    
    $limit     = 10;
    $total     = 0;
    $params    = array(
        'from'    => $from,
        'to'      => $to,
        'name'    => $name,
        'good_id' => $good_id,
        'parent'  => $parent,
        'export'  => $export
    );
    
    if(count($range) < 100 AND isset($params['from']) AND $params['from'] AND isset($params['from']) AND $params['to']){
        
        try {
            if(isset($export) and $export == 1){
                $data = $QKpiMonth->getSellInDealer(NULL,NULL,$total,$params,$range_date);
                $this->_exportSellIn($data,$range);
                
            }
            
            $data = $QKpiMonth->getSellInDealer($page,$limit,$total,$params,$range_date);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        
        
    }
    
    $this->view->data        = $data;
	$this->view->offset      = $limit*($page-1);
	$this->view->total       = $total;
	$this->view->limit       = $limit;
	$this->view->url         = HOST.'bi/sell-in-dealer'.( $params ? '?'.http_build_query($params).'&' : '?' );
    $this->view->range_date  = $range;        
    $this->view->params      = $params;  
    
    $this->view->good        = $QGood->get_cache();
    
    
    $this->_helper->viewRenderer->setRender('sell-in/sell-in-dealer');
?>














