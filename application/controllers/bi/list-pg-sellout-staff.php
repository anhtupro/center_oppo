<?php
    $QArea              = new Application_Model_Area();

        

    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();
    $sort                 = $this->getRequest()->getParam('sort', '');
    $desc                   = $this->getRequest()->getParam('desc', 1);
    $page                   = $this->getRequest()->getParam('page', 1);
    //totalEmp
    $titleEmp               = $this->getRequest()->getParam('title');
    //sellout
    $id                     = $this->getRequest()->getParam('id');
    $y                      = $this->getRequest()->getParam('y');
    $m                      = $this->getRequest()->getParam('m');
    $type_sell              = $this->getRequest()->getParam('type_sell');
    //search
    $name                   = $this->getRequest()->getParam('name');
    $code                   = $this->getRequest()->getParam('code');
    $email                  = $this->getRequest()->getParam('email');
    $title                  = $this->getRequest()->getParam('title');
    $shop                   = $this->getRequest()->getParam('shop');
    $star                   = $this->getRequest()->getParam('star');
    $from_date              = $this->getRequest()->getParam('from_date');
    $to_date                = $this->getRequest()->getParam('to_date');
    $sellout_from           = $this->getRequest()->getParam('sellout_from');
    $sellout_to             = $this->getRequest()->getParam('sellout_to');
    $seniority              = $this->getRequest()->getParam('seniority');
    $area_id            = $this->getRequest()->getParam('area_id');
    $area_staff            = $this->getRequest()->getParam('area_staff');
    $province_id        = $this->getRequest()->getParam('province_id');
    $regional                   = $this->getRequest()->getParam('regional');
    $params =array(
        'y'                 => intval($y),
        'm'                 => intval($m),
        'type_sell'         => intval($type_sell),
        'id'                => $id,
        'name'              => $name,
        'code'              => $code,
        'email'             => $email,
        'title'             => $title,
        'star'              => $star,
        'shop'              => $shop,
        'from_date'         => $from_date,
        'to_date'           => $to_date,
        'sellout_from'      => $sellout_from,
        'sellout_to'        => $sellout_to,
        'seniority'         => $seniority,
        'area_id'           =>$area_id,
        'province_id'       =>$province_id,
        'regional'          => $regional,
        'area_staff'        => $area_staff
    );
    //lấy khu vưc
    $area                   = $QAsm->get_cache($id);
    $list_area              = $area['area'];
    $params['area_list']    = $list_area;
     $params['sort']         = $sort;
    $params['desc']         = $desc;
    $total                      = 0;
    $limit                      = LIMITATION;



    $rows = $QKpiMonth->getListStaffSellout($page, $limit, $total, $params);


$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->result     = $rows;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->area       = $QArea->get_cache();
$this->view->url        = HOST.'bi/list-pg-sellout-staff'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset     = $limit*($page-1); 

?>