<?php 
    
    $QStaff = new Application_Model_Staff();
    $QTeam  = new Application_Model_Team();
    
    $page   = $this->getRequest()->getParam('page', 1);
    $title  = $this->getRequest()->getParam('title');
    $team   = $this->getRequest()->getParam('team');
    $off    = $this->getRequest()->getParam('off', 1);
    
    //redirect
    if($title == SALE_SALE_ASM){
        My_Controller::redirect(HOST . 'bi/asm');
    }
    //end redirect
    
    $department   = $this->getRequest()->getParam('department');
    
    $limit = 20;
    $total = 0;
    
    $params = array(
        'off'   => $off,
        'title' => array($title),
        'team'  =>  array($team),
        'department' =>  array($department),
    );
	$staffs = $QStaff->fetchPagination($page, $limit, $total, $params);
    
    $this->view->staffs = $staffs;
    
    $this->view->desc = $desc;
    $this->view->sort = $sort;
    $this->view->staffs = $staffs;
    $this->view->params = $params;
    $this->view->limit = $limit;
    $this->view->total = $total;
    $this->view->url = HOST . 'bi/staff/' . ($params ? '?' . http_build_query($params) .
        '&' : '?');

    $this->view->offset = $limit * ($page - 1);
    $this->view->title = $title;
    
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $team = $QTeam->get_cache();
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    $this->view->team = $team;
?>