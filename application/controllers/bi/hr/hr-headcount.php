<?php 
$list_date = array(
	date('Y-m-d', strtotime(date('Y-m-d')." -6 month")),
	date('Y-m-d', strtotime(date('Y-m-d')." -5 month")),
	date('Y-m-d', strtotime(date('Y-m-d')." -4 month")),
	date('Y-m-d', strtotime(date('Y-m-d')." -3 month")),
	date('Y-m-d', strtotime(date('Y-m-d')." -2 month")),
	date('Y-m-d', strtotime(date('Y-m-d')." -1 month")),
	date('Y-m-d'),
);

$mon_en = array(
	1 => 'January',
	2 => 'February',
	3 => 'March',
	4 => 'April',
	5 => 'May',
	6 => 'June',
	7 => 'July',
	8 => 'August',
	9 => 'September',
	10 => 'October',
	11 => 'November',
	12 => 'December'
);

$curMonth = date("m", time());
$curQuarter = ceil($curMonth/3);

$KpiMonth = new Application_Model_KpiMonth();

$params = array();
$params['year'] = date('Y');
$params['group'] = array(1,15,14,140,129,130);
$params['cur_quarter'] = $curQuarter;

$staff_evolution = $KpiMonth->staff_evolution($params);
$headcount = $KpiMonth->headcount($params);
$turnover_rate = $KpiMonth->turnover_rate($params);

$params['group_turnover_rate'] = array(1,15,14,140,129,130);
$turnover_pass_probation1 = $KpiMonth->turnover_pass_probation($params);

$params['group_turnover_rate'] = array(146,141,331,131,75);
$turnover_pass_probation2 = $KpiMonth->turnover_pass_probation($params);


/* turnover */
$params['date_probation'] = date('Y-m-d');
$params['group_headcount_probation'] = array(1,15,14,140,129,130);
$headcount_probation1 = $KpiMonth->headcount_probation($params);

$params['group_headcount_probation'] = array(146,141,331,131,75);
$headcount_probation2 = $KpiMonth->headcount_probation($params);
/* end turnover */

$this->view->list_date = $list_date;
$this->view->staff_evolution = $staff_evolution;
$this->view->cur_quarter = $curQuarter;
$this->view->headcount 		 = $headcount;
$this->view->turnover_rate   = $turnover_rate;

$this->view->turnover_pass_probation1  = $turnover_pass_probation1;
$this->view->turnover_pass_probation2  = $turnover_pass_probation2;

$this->view->headcount_probation1  = $headcount_probation1;
$this->view->headcount_probation2  = $headcount_probation2;

$this->view->mon_en = $mon_en;

$this->_helper->viewRenderer->setRender('hr/hr-headcount');


?>