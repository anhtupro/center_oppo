<?php 

    $QKpiMonth = new Application_Model_KpiMonth();
    $Area = new Application_Model_Area();
    $QAsm  = new Application_Model_Asm();

    $params = array();
    /* Phân quyền */
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $group_id    = $userStorage->group_id;

    if (in_array($group_id, My_Staff_Group::$allow_in_area_view) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    }

    if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        exit;
    }
    /* end Phân quyền */

    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -11 month")),
        date('Y-m-d', strtotime(date('Y-m')." -10 month")),
        date('Y-m-d', strtotime(date('Y-m')." -9 month")),
        date('Y-m-d', strtotime(date('Y-m')." -8 month")),
        date('Y-m-d', strtotime(date('Y-m')." -7 month")),
        date('Y-m-d', strtotime(date('Y-m')." -6 month")),
        date('Y-m-d', strtotime(date('Y-m')." -5 month")),
        date('Y-m-d', strtotime(date('Y-m')." -4 month")),
		date('Y-m-d', strtotime(date('Y-m')." -3 month")),
		date('Y-m-d', strtotime(date('Y-m')." -2 month")),
		date('Y-m-d', strtotime(date('Y-m')." -1 month")),
		date('Y-m-d'),
	);

    $params['from'] = date('Y-m-01', strtotime(date('Y-m-d')." -11 month"));
	$params['to'] = date('Y-m-d');

    /* NHÂN SỰ NEW */
    $headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale_area($params);
    $data_pgpb_sale = array();
    foreach ($headcount_pgpb_sale as $key => $value) {
        $data_pgpb_sale[$value['YearNo']][$value['MonthNo']][$value['area_id']][$value['title']] = array(
            'totalEmp'   => $value['totalEmp'],
            'Working'    => $value['Working'],
            'Off'        => $value['off'],
            'Per_Working'=> $value['Per_Working'],
            'Per_Off'    => $value['Per_Off']
            );
    }

    $this->view->data_pgpb_sale = $data_pgpb_sale;
    /* NHÂN SỰ */

    $this->view->area = $Area->get_cache();
    if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        $where = array();
        $where = $Area->getAdapter()->quoteInto('id IN (?)', $params['area_list']);
        $area_array = array();
        $area = $Area->fetchAll($where);

        foreach ($area as $key => $value) {
            $area_array[$value['id']] = $value['name'];
        }
        $this->view->area = $area_array;
    }

    $this->view->list_date = $list_date;

    $this->_helper->viewRenderer->setRender('hr/hr-pgpb-sale-area');

    //KPI avg
    if(isset($params['area_list']) and $params['area_list']){
        $avg_sale_pg = $QKpiMonth->kpi_avg_bi_area($params);
    }
    else {
        $avg_sale_pg = $QKpiMonth->kpi_avg_bi_area_cache($params);
    }
    $this->view->avg_sale_pg = $avg_sale_pg;
    //END KPI avg

?>