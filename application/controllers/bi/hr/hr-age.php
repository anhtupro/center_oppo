<?php 
$list_date = array(
	date('Y-m-d', strtotime(date('Y-m')." -6 month")),
	date('Y-m-d', strtotime(date('Y-m')." -5 month")),
	date('Y-m-d', strtotime(date('Y-m')." -4 month")),
	date('Y-m-d', strtotime(date('Y-m')." -3 month")),
	date('Y-m-d', strtotime(date('Y-m')." -2 month")),
	date('Y-m-d', strtotime(date('Y-m')." -1 month")),
	date('Y-m-d'),
);

$mon_en = array(
	1 => 'January',
	2 => 'February',
	3 => 'March',
	4 => 'April',
	5 => 'May',
	6 => 'June',
	7 => 'July',
	8 => 'August',
	9 => 'September',
	10 => 'October',
	11 => 'November',
	12 => 'December'
);

$curMonth = date("m", time());
$curQuarter = ceil($curMonth/3);

$KpiMonth = new Application_Model_KpiMonth();

$params = array();
$params['year'] = date('Y');
$params['group'] = array(1,15,14,140,129,130);

$staff_evolution = $KpiMonth->staff_evolution($params);
$headcount = $KpiMonth->headcount($params);

$this->view->list_date = $list_date;
$this->view->staff_evolution = $staff_evolution;
$this->view->cur_quarter = $curQuarter;
$this->view->headcount 		 = $headcount;
$this->view->mon_en = $mon_en;

$this->_helper->viewRenderer->setRender('hr/hr-age');


?>