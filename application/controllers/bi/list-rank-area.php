<?php

$params = array();
$QArea = new Application_Model_Area();
$QRankByMonth = new Application_Model_RankByMonth();

/* PHân quyền */
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
    exit;
}

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
	// lấy khu vực của asm
	exit;
}
/* end phân quyền */


$list_date = array(
	date('Y-m-d', strtotime(date('Y-m')." -3 month")),
	date('Y-m-d', strtotime(date('Y-m')." -2 month")),
	date('Y-m-d', strtotime(date('Y-m')." -1 month")),
	date('Y-m-d'),
);

/* RANK SELLOUT BY PG */
$rank_by_month = $QRankByMonth->getAllArea($params);

$rank = array();
foreach ($rank_by_month as $key => $value) {
	$rank[$value['area_id']][$value['rank']][$value['month']][$value['year']] = array(
		'sum_store' =>  $value['sum'],
		'sum_pg' 	=>  $value['count_staff'],
		);
}

$this->view->rank = $rank;

$this->view->area = $QArea->get_cache();
$this->view->list_date = $list_date;

