<?php

    $QAsm = new Application_Model_Asm();
    $QKpiMonth = new Application_Model_KpiMonth();
    $QStaff = new Application_Model_Staff();
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $group_id = $userStorage->group_id;
    
    $params = [
        'limit' => 100
    ];
    
    $area = $QAsm->get_cache($userStorage->id);
    $list_area = $area['area'];
    $params['area_list'] = $list_area;
    
    
    $row_lesson = $QKpiMonth->reportDataLesson($params);
    $this->view->row_lesson   = $row_lesson;

    $total_sale_pgs = $QStaff->getTotalSalePgs($params);
    $this->view->total_sale_pgs = $total_sale_pgs['sum'];



