<?php

$tmpTo          = date('Y-m-d',strtotime('-1 days'));
$tmpFrom        = date('Y-m-d',strtotime($tmpTo.' -30 days'));
$from           = $this->getRequest()->getParam('from',$tmpFrom);
$to             = $this->getRequest()->getParam('to',$tmpTo);
$channel        = $this->getRequest()->getParam('channel');

$QKpiMonth = new Application_Model_KpiMonth();
$QGood     = new Application_Model_Good();
$QStaffChannel     = new Application_Model_StaffChannel();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$name_chart = $QStaffChannel->get_staff_channel($userStorage->id);
$channel_ = [];
foreach ($name_chart as $key => $value) {
	$channel_[] = $key;
}

if(!in_array($channel, $channel_)){
    $this->_redirect(HOST.'bi/key-acount');
}

$params = array(
    'from'           => $from,
    'to'             => $to,
    'channel'        => $channel
);

$channel_ka = $QStaffChannel->get_channel_ka($channel);

$list_date = $this->date_range($from, $to);

$list_good = $QKpiMonth->goodByKaTop($params);

$sellout = $QKpiMonth->goodByKa($params);

$data_good = array();
$data_date = array();

foreach($sellout as $key=>$value){
    /* good sellout*/
    if(isset($data_good[$value['good_id']]['sellout']))
        $data_good[$value['good_id']]['sellout'] += intval($value['sellout']);
    else
        $data_good[$value['good_id']]['sellout'] = intval($value['sellout']);

    
    /* date sellout */
    if(isset($data_date[$value['date']][$value['good_id']]['sellout']))
        $data_date[$value['date']][$value['good_id']]['sellout'] += intval($value['sellout']);
    else
        $data_date[$value['date']][$value['good_id']]['sellout'] = intval($value['sellout']);

    
    /* date sellout active by date*/
    if(isset($data_date[$value['date']]['total_sellout']))
        $data_date[$value['date']]['total_sellout'] += intval($value['sellout']);
    else
        $data_date[$value['date']]['total_sellout'] = intval($value['sellout']);

}

$sellout_month = $QKpiMonth->goodByKaMonth($params);
$pgs_month = $QKpiMonth->countPgsKaByMonth($params);

$QDistributor = new Application_Model_Distributor();

$this->view->distributor = $QDistributor->get_cache();

$this->view->params = $params;
$this->view->list_date = $list_date;
$this->view->list_good = $list_good;
$this->view->data_date = $data_date;
$this->view->good_cache = $QGood->get_cache();
$this->view->sellout_month = $sellout_month;
$this->view->pgs_month = $pgs_month;
$this->view->channel_ka = $channel_ka;

?>