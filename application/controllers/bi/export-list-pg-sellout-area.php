<?php
    $QArea              = new Application_Model_Area();

        

    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();

    //totalEmp
    $titleEmp               = $this->getRequest()->getParam('title');
    //sellout
    $id                     = $this->getRequest()->getParam('id');
    $y                      = $this->getRequest()->getParam('y');
    $m                      = $this->getRequest()->getParam('m');
    $type_sell              = $this->getRequest()->getParam('type_sell');
    //search
    $name                   = $this->getRequest()->getParam('name');
    $code                   = $this->getRequest()->getParam('code');
    $email                  = $this->getRequest()->getParam('email');
    $title                  = $this->getRequest()->getParam('title');
    $shop                   = $this->getRequest()->getParam('shop');
    $star                   = $this->getRequest()->getParam('star');
    $from_date              = $this->getRequest()->getParam('from_date');
    $to_date                = $this->getRequest()->getParam('to_date');
    $sellout_from           = $this->getRequest()->getParam('sellout_from');
    $sellout_to             = $this->getRequest()->getParam('sellout_to');
    $seniority              = $this->getRequest()->getParam('seniority');
    $area_id            = $this->getRequest()->getParam('area_id');
    $province_id        = $this->getRequest()->getParam('province_id');
    $regional                   = $this->getRequest()->getParam('regional');
    $params =array(
        'y'                 => intval($y),
        'm'                 => intval($m),
        'type_sell'         => intval($type_sell),
        'id'                => $id,
        'name'              => $name,
        'code'              => $code,
        'email'             => $email,
        'title'             => $title,
        'star'              => $star,
        'shop'              => $shop,
        'from_date'         => $from_date,
        'to_date'           => $to_date,
        'sellout_from'      => $sellout_from,
        'sellout_to'        => $sellout_to,
        'seniority'         => $seniority,
        'area_id'           =>$area_id,
        'province_id'       =>$province_id,
        'regional'          => $regional
    );
    //lấy khu vưc
    $area                   = $QAsm->get_cache($id);
    $list_area              = $area['area'];
    $params['area_list']    = $list_area;




    $data = $QKpiMonth->getExportListAreaSellout($params);
    include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    $heads = array(
        'CODE',
        'FULLNAME',
        'DEPARTMENT',
        'TEAM',
        'TITLE',
        'AREA'
       
    );
    foreach ($heads as $key)
    {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

     
     $index = 2;
    foreach($data as $key=>$value){
     $alpha = 'A';

     $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
     $sheet->getCell($alpha++ . $index)->setValueExplicit($value['fullname'],PHPExcel_Cell_DataType::TYPE_STRING);
     $sheet->getCell($alpha++ . $index)->setValueExplicit($value['department'],PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++ . $index)->setValueExplicit($value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
       $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
        $sheet->getCell($alpha++ . $index)->setValueExplicit($value['area_name'],PHPExcel_Cell_DataType::TYPE_STRING);
         $index ++;
    } 
    // //format
    

    $filename = ' Staff_Sellout ';
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
    $objWriter->save('php://output');
    exit;
    $this->redirect(HOST.'bi/list-pg-sellout-area?id='.$id.'&y='.$y.'&m='.$m.'&type_sell='.$type_sell.'&regional='.$regional);


