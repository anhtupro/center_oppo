<?php 
    $id      = $this->getRequest()->getParam('id');
    $current_date = date('Y-m-d H:i:s');
    
    if($id){
        $QStore         = new Application_Model_Store();
        $QStoreStaffLog = new Application_Model_StoreStaffLog();
        $QKpiMonth      = new Application_Model_KpiMonth();
        $QGood          = new Application_Model_Good();
        $QArea          = new Application_Model_Area();
        $QAsm           = new Application_Model_Asm();

        $params = array();

        $store = $QKpiMonth->getStoreById($id);
        $this->view->store = $store;
        //
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $group_id = $userStorage->group_id;

        if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
            // lấy khu vực của asm
            $asm_cache = $QAsm->get_cache();
            $params['area_list_check'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
        
        }

        if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
            if($group_id == ASM_ID){
                if((!in_array($store['area_id'], $params['area_list_check']))){
                    $this->_redirect(HOST);
                    exit;
                }
            }
            elseif($group_id == LEADER_ID){

                $params['leader_id'] = $userStorage->id;
                $list_sale = $QKpiMonth->getListSale($params);
                $list_sale_array = array();
                foreach ($list_sale as $key => $value) {
                    $list_sale_array[] = $value['staff_id'];
                }
                $list_sale_array[] = $userStorage->id;

                // get List store
                $db = Zend_Registry::get('db');
                $arrCols = array(
                    's.name',
                    's.id AS store_id',
                    'p.joined_at',
                    'p.released_at',
                    'num' => 'COUNT(i.imei_sn)',
                    'level' => 'lp.name'
                );
                $select = $db->select()
                    ->from(array('p'=>'store_staff_log'), $arrCols)
                    ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
                    ->joinLeft(array('i'=>'imei_kpi'), "i.store_id = s.id AND i.timing_date >= '".date('Y-m-01')."'",array())
                    ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id',array())
                    ->joinLeft(array('l'=>'dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)',array())
                    ->joinLeft(array('lp'=>'loyalty_plan'), 'lp.id = l.loyalty_plan_id',array())
                    ->orWhere("p.staff_id IN (?)", $list_sale_array)
                    ->where('released_at IS NULL', NULL)
                    ->group('s.id')
                    ->order('num DESC')
                ;
                $list_store = $db->fetchAll($select);
                $list_store_array = array();
                foreach ($list_store as $key => $value) {
                    $list_store_array[] = $value['store_id'];
                }

                if(!in_array($id, $list_store_array)){
                    $this->_redirect(HOST);
                    exit;
                }
            }
            elseif($group_id == SALES_ID){
                // get List store
                $db = Zend_Registry::get('db');
                $arrCols = array(
                    's.name',
                    's.id AS store_id',
                    'p.joined_at',
                    'p.released_at',
                    'num' => 'COUNT(i.imei_sn)',
                    'level' => 'lp.name'
                );
                $select = $db->select()
                    ->from(array('p'=>'store_staff_log'), $arrCols)
                    ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
                    ->joinLeft(array('i'=>'imei_kpi'), "i.store_id = s.id AND i.timing_date >= '".date('Y-m-01')."'",array())
                    ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id',array())
                    ->joinLeft(array('l'=>'dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)',array())
                    ->joinLeft(array('lp'=>'loyalty_plan'), 'lp.id = l.loyalty_plan_id',array())
                    ->where("p.staff_id = ?", $userStorage->id)
                    ->where('released_at IS NULL', NULL)
                    ->group('s.id')
                    ->order('num DESC')
                ;

                $list_store = $db->fetchAll($select);
                $list_store_array = array();
                foreach ($list_store as $key => $value) {
                    $list_store_array[] = $value['store_id'];
                }

                if(!in_array($id, $list_store_array)){
                    $this->_redirect(HOST);
                    exit;
                }

            }
            elseif($group_id == PGPB_ID){
                // get List store
                $db = Zend_Registry::get('db');
                $arrCols = array(
                    's.name',
                    's.id AS store_id',
                    'p.joined_at',
                    'p.released_at',
                    'num' => 'COUNT(i.imei_sn)',
                    'level' => 'lp.name'
                );
                $select = $db->select()
                    ->from(array('p'=>'store_staff_log'), $arrCols)
                    ->joinLeft(array('s'=>'store'), 's.id = p.store_id',array())
                    ->joinLeft(array('i'=>'imei_kpi'), "i.store_id = s.id AND i.timing_date >= '".date('Y-m-01')."'",array())
                    ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id',array())
                    ->joinLeft(array('l'=>'dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)',array())
                    ->joinLeft(array('lp'=>'loyalty_plan'), 'lp.id = l.loyalty_plan_id',array())
                    ->where("p.staff_id = ?", $userStorage->id)
                    ->where('released_at IS NULL', NULL)
                    ->group('s.id')
                    ->order('num DESC')
                ;

                $list_store = $db->fetchAll($select);
                $list_store_array = array();
                foreach ($list_store as $key => $value) {
                    $list_store_array[] = $value['store_id'];
                }

                if(!in_array($id, $list_store_array)){
                    $this->_redirect(HOST);
                    exit;
                }
            }
        }



        $params['store_id'] = $id;
        
        

        $staff   = $QStoreStaffLog->get_list_sale($id, $current_date);
        $staff_leader   = $QStoreStaffLog->get_list_sale_leader($id, $current_date);
        $this->view->staff = $staff;
        $this->view->staff_leader = $staff_leader;
        
        
        //Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
        $list_date = array(
            date('Y-m-d', strtotime(date('Y-m')." -3 month")),
            date('Y-m-d', strtotime(date('Y-m')." -2 month")),
            date('Y-m-d', strtotime(date('Y-m')." -1 month")),
            date('Y-m-d'),
        );

        $name_chart = array(
            '2316'  => 'TGDĐ',
            '2363'  => 'FPT',
            '2317'  => 'VTA',
            '9187'  => 'Vinpro',
            '10007' => 'VIETTEL',
            '2325'  => 'CES',
            '3'     => 'DIAMOND',
            '2'     => 'GOLD',
            '1'     => 'SILVER',
            '4'     => 'NORMAL',
            '999'   => 'OTHER KA',
            '888'   => 'RETURN',
        );

        //$params = array();
        $params['leader_id'] = null;
        $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
        $params['to'] = date('Y-m-d');
        $sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
        $data_good = $QKpiMonth->getSelloutGood($params);

        /* get sellout, total value theo từng kênh */
        $data_sellout_dealer = array();
        $data_sellout_total = array();
        $sellout_total = array(
            'num' => 0,
            'total_value' => 0
        );
        foreach ($sell_out_dealer as $key => $value) {
            $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                                'num' => $value['num'],
                                                                                'total_value' => $value['total_value']
                                                                                );


            if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
                $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
                $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
            }
            else{
                $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
                $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
            }

            $sellout_total['num'] += $value['num'];
            $sellout_total['total_value'] += $value['total_value'];
                                                
        }
        /* END sellout, total value theo từng kênh */

        $this->view->data_sellout_dealer = $data_sellout_dealer;
        $this->view->data_sellout_total  = $data_sellout_total;
        $this->view->sellout_total       = $sellout_total;
        $this->view->list_date           = $list_date;
        $this->view->name_chart          = $name_chart;

        $this->view->data_sellout_good = $data_good;
        $this->view->good_hero         = unserialize(LIST_PRODUCT_HERO_BI);
        $this->view->good              = $QGood->get_cache();
        
        
        /* TRADE MARKETTING*/
        $params['month'] = array();

        foreach ($list_date as $key => $value) {
            $params['month'][] = intval(date('m', strtotime($value)));
        }

        //$fee_cost = $QKpiMonth->get_fee_cost($params);
        $fee_air  = $QKpiMonth->get_fee_air($params);
        $fee_campaign  = $QKpiMonth->get_fee_campaign_distributor($params);

        $data_trade = array();
        $data_trade_total = array(
            'fee_cost'     => 0,
            'fee_air'      => 0,
            'fee_campaign' => 0,
        );

        foreach ($fee_cost as $key => $value) {
            $data_trade[intval($value['month_date'])] = $value['sum'];
            //
            $data_trade_total['fee_cost'] += $value['sum'];
        }

        foreach ($fee_air as $key => $value) {
            if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
                $data_trade[intval($value['month_date'])] += $value['sum'];
            }
            else{
                $data_trade[intval($value['month_date'])] = $value['sum'];
            }
            //
            $data_trade_total['fee_air'] += $value['sum'];
        }

        foreach ($fee_campaign as $key => $value) {
            if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
                $data_trade[intval($value['month_date'])] += $value['sum'];
            }
            else{
                $data_trade[intval($value['month_date'])] = $value['sum'];
            }
            //
            $data_trade_total['fee_campaign'] += $value['sum'];
        }
        $this->view->data_trade = $data_trade;
        $this->view->data_trade_total = $data_trade_total;
        /* END TRADE MARKETTING*/

        /* sellout by date */
        $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -1 month"));
        $data_good_by_date = $QKpiMonth->getGoodByDate($params);
        $good_by_date = array();
        foreach ($data_good_by_date as $key => $value) {
            $good_by_date[$value['date']][$value['good_id']] = $value['num'];
            $good_by_date[$value['date']][9999] += $value['num'];
        }
        $list_date_range = $this->date_range($params['from'], $params['to']);
        $this->view->good_by_date = $good_by_date;
        $this->view->list_date_range = $list_date_range;

        //Số bán hàng ngày
        $good_hero_total = array(
            HERO_ID => HERO_NAME,
            '9999' => 'Total'
        );
        $this->view->good_hero_total = $good_hero_total;
        /* end sellout by date */



        //TRADE MARKETING
        $QBiTrade      = new Application_Model_BiTrade();

        $params = array(
            'store_id'  => $id
        );

        $category      = $QBiTrade->getCategoryBiStore($params);
        $store_date    = $QBiTrade->getFromToSO($params);
        $list_month    = $this->month_range($store_date['from_date'], $store_date['to_date']);

        //Get data S.O
        $sellout       = $QBiTrade->getSelloutByStore($params);
        $data_sellout  = array();
        foreach ($sellout as $key => $value) {
            if(isset($data_sellout[$value['month']][$value['year']]) and $data_trade_total[$value['month']][$value['year']]){
                $data_sellout[$value['month']][$value['year']]['total_value'] +=  $value['total_value']; 
            }
            else{
                $data_sellout[$value['month']][$value['year']]['total_value'] =  $value['total_value'];  
            }
        }

        //Get data Trade
        $trade     = $QBiTrade->getTradeByStore($params);

        $total_trade_value = 0;
        $data_trade = array();
        foreach ($trade as $key => $value) {

            $total_trade_value += $value['total_value'];

            if(isset($data_trade[$value['month']][$value['year']]) and $data_trade[$value['month']][$value['year']]){
                $data_trade[$value['month']][$value['year']]['total_value'] +=  $value['total_value']; 
            }
            else{
                $data_trade[$value['month']][$value['year']]['total_value'] =  $value['total_value'];  
            }
        }

        $where = $QStore->getAdapter()->quoteInto('id = ?', $id);
        $store = $QStore->fetchRow($where);

        $investments = $QKpiMonth->get_investments_by_store($params);
        $this->view->investments = $investments;

        $total_trade = $QKpiMonth->get_total_trade_store($params);


        $this->view->category       = $category;
        $this->view->store          = $store;
        $this->view->list_month     = $list_month;
        $this->view->sellout        = $sellout;
        $this->view->data_sellout   = $data_sellout;
        $this->view->data_trade     = $data_trade;
        $this->view->total_trade    = $total_trade;
        $this->view->store_date     = $store_date;
        $this->view->total_trade_value = $total_trade_value;
        //END TRADE MARKETING


        
    }


        
?>