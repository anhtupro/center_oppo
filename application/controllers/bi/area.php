<?php
$sort            = $this->getRequest()->getParam('sort');
$desc            = $this->getRequest()->getParam('desc', 1);
$export          = $this->getRequest()->getParam('export', 0);
$dealer_id       = $this->getRequest()->getParam('dealer_id', 0);
$from            = $this->getRequest()->getParam('from', date('01/m/Y') );
$to              = $this->getRequest()->getParam('to', date('d/m/Y') );
$area            = $this->getRequest()->getParam('area');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$params = array(
    'sort'      => $sort,
    'desc'      => $desc,
    'from'      => $from,
    'to'        => $to,
    'area'      => $area,
    'export'    => $export,
);

if ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $area_list    = array();
    $QAsm         = new Application_Model_Asm();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $this->view->viewed_area_id = $list_regions;
}

$QArea = new Application_Model_Area();
$QImeiKpi = new Application_Model_ImeiKpi();
$this->view->userStorage = $userStorage;
$data = $QImeiKpi->fetchArea($params);

if($dealer_id){
    $params['dealer_id'] = $dealer_id;
    $dealer              = $QImeiKpi->fetchArea($params);
    
    $data_dealer         = [];
    foreach ($dealer as $key => $value) {
        $data_dealer[$value['area_id']] = $value;
    }
    $this->_exportAreaKa($data, $data_dealer);
}

$params['get_total_sales'] = true;
$total_sales = $QImeiKpi->fetchArea($params);

//get total money
$total_money = $total_sales['total_value'];
$sales = array();


//tính point
foreach($data as $item){
    $point = ( $total_money > 0 and ($item ['region_share']/100) > 0 ) ?  round ( ($item ['total_value']/$total_money) * 60 / ($item ['region_share']/100), 2 ) : 0;
    $val = $item;
    $val['point'] = $point;
    $sales[] = $val;
}

unset($params['kpi']);
unset($params['get_total_sales']);
usort($sales, array($this, 'cmp'));


$this->view->areas = $QArea->get_cache();

$this->view->total_sales           = $total_sales['total_quantity'];
$this->view->total_sales_activated = $total_sales['total_activated'];
$this->view->sales                 = $sales;
$this->view->url                   = HOST.'timing/analytics-area'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->desc                  = $desc;
$this->view->current_col           = $sort;
$this->view->to                    = $to;
$this->view->from                  = $from;
$this->view->params                = $params;





