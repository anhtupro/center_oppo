<?php 
	
	$area_id = $this->getRequest()->getParam('area_id');
	$province_id = $this->getRequest()->getParam('province_id');
	$area_province_id = $this->getRequest()->getParam('area_province_id');
	$params = [
		'area_id' 		=> $area_id,
		'province_id'		=>$province_id,
		'area_province_id'	=>$area_province_id
	];
	$QStore 	= new Application_Model_Store();
	$QAppAir = new Application_Model_AppAir();
	if(!empty($area_id))
	{
		$data['province']= $QAppAir->getAjax($params);
	}
	if(!empty($province_id))
	{

		$data['district']= $QAppAir->getAjaxDistrict($params);
	}
	if(!empty($area_province_id))
	{
		$data['area_province']= $QAppAir->getPovince($params);
	}
	

	echo json_encode($data);
exit;