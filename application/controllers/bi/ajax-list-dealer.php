<?php
	$QKpiMonth        = new Application_Model_KpiMonth();
	$QAsm            = new Application_Model_Asm();
	$area = $this->getRequest()->getParam('area');
	$type = $this->getRequest()->getParam('type');
	$id = $this->getRequest()->getParam('id');
	$month = $this->getRequest()->getParam('month');
	$year = $this->getRequest()->getParam('year');

	$region = $this->getRequest()->getParam('region');
	$trainer_id = $this->getRequest()->getParam('trainer_id');
	$kv = $QAsm->get_cache($trainer_id);
    $list_kv = $kv['area'];
	$params = array(
		'area_list'     => $list_kv,
		'area'	=>$area,
		'region' =>$region,	
		'id'	=>$id,
		'type'	=>$type,
		'month'	=>$month,
		'year'	=>$year
	);
if(!empty($area))
{
	$result = $QKpiMonth->getAreaDealerTrainingReport($params);
}

 
if(!empty($region))
{
	$result = $QKpiMonth->getRegionDealerTrainingReport($params);
	
}
echo json_encode($result);
exit;