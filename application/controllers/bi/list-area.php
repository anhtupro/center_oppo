<?php
    $QKpiMonth        = new Application_Model_KpiMonth();
    $QAsm            = new Application_Model_Asm();

    $id_trainer  = $this->getRequest()->getParam('trainer_id');	
    $lesson_id	= $this->getRequest()->getParam('lesson_id');	
    $region_id  = $this->getRequest()->getParam('region_id');
    $title = $this->getRequest()->getParam('title');
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    
    $params = [];
    
    $area = $QAsm->get_cache($userStorage->id);  
    $list_kv = $area['area'];
    $params['area_list'] = $list_kv;    
    
    //Phan quyen cho tien.vo Brandshop
    if($userStorage->id == 910){
        $params['list_title_staff'] = [419, 403];
    }
    $params['lesson_id']	= $lesson_id;
    $params['region_id']    = $region_id;
    $params['title']    = $title;
    $params['trainer_id']    = $id_trainer;
    
    
    $list_area = $QKpiMonth->getLessonListArea($params);

    
    $this->view->list_area = $list_area;
    $this->view->params = $params;
?>