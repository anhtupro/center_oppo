<?php

$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$page            = $this->getRequest()->getParam('page', 1);
$name            = $this->getRequest()->getParam('name');
$area_id         = $this->getRequest()->getParam('area_id',null);
$regional_market = $this->getRequest()->getParam('regional_market',null);
$code            = $this->getRequest()->getParam('code');
$email           = $this->getRequest()->getParam('email');
$date            = $this->getRequest()->getParam('date');
$month           = $this->getRequest()->getParam('month');
$year            = $this->getRequest()->getParam('year');
$title           = $this->getRequest()->getParam('title');
$sell_out_from   = $this->getRequest()->getParam('sell_out_from');
$sell_out_to     = $this->getRequest()->getParam('sell_out_to');

$from_date       = $this->getRequest()->getParam('from_date');
$to_date         = $this->getRequest()->getParam('to_date');
$status          = $this->getRequest()->getParam('status');
$export          = $this->getRequest()->getParam('export');

$id_trainer          = $this->getRequest()->getParam('id_trainer');


$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger  = $this->_helper->flashMessenger;

$TITLE_PGPB_SALE    = array(PGPB_TITLE, PGPB_2_TITLE, SALE_SALE_SALE, STORE_LEADER);

if($title)
{
    $TITLE_PGPB_SALE    =  array($title);
    $this->view->title  = $title;
}

$total              = 0;
$limit              = LIMITATION;

$area_id_login      = array();

$QStaffTrainer   = new Application_Model_StaffTrainer();
$QStaff          = new Application_Model_Staff();
$QAsm                   = new Application_Model_Asm();

$params = array_filter(array(
    'name'              => $name,
    'title'             => $TITLE_PGPB_SALE,
    'area_id'           => $area_id,
    'regional_market'   => $regional_market,
    'code'              => $code,
    'email'             => $email,
    'date'              => $date,
    'month'             => $month,
    'year'              => $year,
    'off'               => My_Staff_Status::On,
    'from_date'         => $from_date,
    'to_date'           => $to_date,
    'status'            => $status
));
$params['sort']                 = $sort;
$params['desc']                 = $desc;

if(!empty($id_trainer)){
    $area = $QAsm->get_cache($id_trainer);
}else{
    $area = $QAsm->get_cache($userStorage->id);
}
$params['area_id'] = !empty($area['area']) ? $area['area'] : null;

if( $userStorage->team == TRAINING_TEAM
    || in_array($userStorage->group_id,array(ADMINISTRATOR_ID,ASM_ID,ASMSTANDBY_ID ))) {
    $area_id_login = $QStaffTrainer->getAreaTrainer($userStorage->id);
}

if(isset($area_id_login['province']) and count($area_id_login['province']))
{

    $params['regional_market_right']     = array_keys($area_id_login['province']);

    $this->view->regional_markets  = $area_id_login['province'];
}

if(isset($area_id_login['area']) and count($area_id_login['area']) )
{

    $params['area_trainer_right']   = array_keys($area_id_login['area']);

    $this->view->area_trainer = $area_id_login['area'];
}

$QTeam = new Application_Model_Team();
$recursiveDepartmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->recursiveDepartmentTeamTitle = $recursiveDepartmentTeamTitle;


$staffs = $QStaff->fetchPaginationStaffBrandShop($page, $limit, $total, $params);

if($export == 1){

    $data = $QStaff->fetchPaginationStaffBrandShopExport($page, $limit, $total, $params);

    ini_set('memory_limit', '-1');

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
        '#',
        'CODE',
        'NAME',
        'EMAIL',
        'DEPARTMENT',
        'TEAM',
        'TITLE',
        'JOINED',
        'PHONE NO',
        'STATUS',
        'STORE',
        'FROM DATE',
        'TO DATE'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();

    $alpha = 'A';
    $index = 1;
    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }
    $index = 2;


    $i = 1;

    foreach ($data as $key => $item) {
        $alpha = 'A';
        $sheet->setCellValue($alpha++ . $index, $i++);
        $sheet->setCellValue($alpha++ . $index, $item['code']);
        $sheet->setCellValue($alpha++ . $index, $item['full_name']);
        $sheet->setCellValue($alpha++ . $index, $item['email']);
        $sheet->setCellValue($alpha++ . $index, $recursiveDepartmentTeamTitle[$item['department']]['name']);
        $sheet->setCellValue($alpha++ . $index, $recursiveDepartmentTeamTitle[$item['department']]['children'][$item['team']]['name']);
        $sheet->setCellValue($alpha++ . $index, $recursiveDepartmentTeamTitle[$item['department']]['children'][$item['team']]['children'][$item['title']]['name']);
        $sheet->setCellValue($alpha++ . $index, $item['joined_at']);
        $sheet->setCellValue($alpha++ . $index, $item['phone_number']);
        $sheet->setCellValue($alpha++ . $index, ($item['status']== 1 ? 'Enabled' : 'Disabled'));
        $sheet->setCellValue($alpha++ . $index, $item['store_name']);
        $sheet->setCellValue($alpha++ . $index, $item['from_date']);
        $sheet->setCellValue($alpha++ . $index, $item['to_date']);

        $index++;

    }

    $filename = 'Staff_LIST_' . date('d/m/Y');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

    $objWriter->save('php://output');

    exit;
}

$this->view->desc       = $desc;
$this->view->sort       = $sort;
$this->view->staffs     = $staffs;
$this->view->params     = $params;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->url        = HOST . 'bi/list-staff' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->offset     = $limit * ($page - 1);

$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages   = $messages;
$messages_error         = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;