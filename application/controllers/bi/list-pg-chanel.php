<?php

    $QKpiMonth        = new Application_Model_KpiMonth();
    $QAsm            = new Application_Model_Asm();
    $QArea           	= new Application_Model_Area();
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();


    $sort           = $this->getRequest()->getParam('sort', '');
    $desc           = $this->getRequest()->getParam('desc', 1);
    $page           = $this->getRequest()->getParam('page', 1);
    $channel         = $this->getRequest()->getParam('channel');
    $id             = $this->getRequest()->getParam('id');
    

    //search
    $name  = $this->getRequest()->getParam('name');
    $code  = $this->getRequest()->getParam('code');
    $email  = $this->getRequest()->getParam('email');
    $department  = $this->getRequest()->getParam('department');
    $team  = $this->getRequest()->getParam('team');
    $star  = $this->getRequest()->getParam('star');
    $title  = $this->getRequest()->getParam('title');
    $shop  = $this->getRequest()->getParam('shop');
    $from_date  = $this->getRequest()->getParam('from_date');
    $to_date = $this->getRequest()->getParam('to_date');
    $sellout_from = $this->getRequest()->getParam('sellout_from');
    $sellout_to = $this->getRequest()->getParam('sellout_to');
    //seniority
    $seniority = $this->getRequest()->getParam('seniority');
    $area_id 			= $this->getRequest()->getParam('area_id');
    $province_id		= $this->getRequest()->getParam('province_id');

    //var_dump($from_date); exit;
    $params =array(
            'id'					=>$id,
            'channel'				=>$channel,
            'name'					=>$name,
            'code'					=>$code,
            'email'					=>$email,
            'department'			=>$department,
            'team'					=>$team,
            'star'					=>$star,
            'title'					=>$title,
            'shop'					=>$shop,
            'from_date'				=>$from_date,
            'to_date'				=>$to_date,
            'sellout_from'			=>$sellout_from,
            'sellout_to'			=>$sellout_to,
            'seniority'				=>$seniority,
            'area_id'				=>$area_id,
            'province_id'			=>$province_id
    );

    //lấy khu vưc
    $area = $QAsm->get_cache($userStorage->id);
    $list_area = $area['area'];
    $params['area_list'] = $list_area;
    $params['sort']      = $sort;
    $params['desc']      = $desc;

    $total              = 0;
    $limit              = LIMITATION;
    if($params['chanel']=='brandshop')
    {
        $rows = $QKpiMonth->getListBrandshop($page, $limit, $total, $params);
    }
    else{
        $rows = $QKpiMonth->getListChannel($page, $limit, $total, $params);
    }

    //var_dump($rows); exit;
    $this->view->desc       = $desc;
    $this->view->sort       = $sort;
    $this->view->result  	= $rows;
    $this->view->params     = $params;
    $this->view->limit      = $limit;
    $this->view->total      = $total;
    $this->view->area       = $QArea->get_cache();

    $this->view->url = HOST.'bi/list-pg-chanel'.($params ? '?'.http_build_query($params).'&' : '?');
    $this->view->offset = $limit*($page-1); 

?>
