<?php 
	set_time_limit(0);

	$QAsm            = new Application_Model_Asm();

	$page       = $this->getRequest()->getParam('page', 1);
	$sort       = $this->getRequest()->getParam('sort', 'total_quantity');
	$desc       = $this->getRequest()->getParam('desc', 1);
	$export     = $this->getRequest()->getParam('export', 0);
	$from       = $this->getRequest()->getParam('from', date('01/m/Y') );
	$to         = $this->getRequest()->getParam('to', date('d/m/Y'));
	$name       = $this->getRequest()->getParam('name');
	$area       = $this->getRequest()->getParam('area');
	$province   = $this->getRequest()->getParam('province');
	$district   = $this->getRequest()->getParam('district');
	$sales_from = $this->getRequest()->getParam('sales_from');
	$sales_to   = $this->getRequest()->getParam('sales_to');
	$store_level = $this->getRequest()->getParam('store_level');
	
	$params = array(
		'page'       => $page,
		'sort'       => $sort,
		'desc'       => $desc,
		'from'       => $from,
		'to'         => $to,
		'name'       => $name,
		'area'       => $area,
		'province'   => $province,
		'district'   => $district,
		'sales_from' => $sales_from,
		'sales_to'   => $sales_to,
		'export'     => $export,
		'area_list'  => $area,
		'store_level' => $store_level
	
	);
	
	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$group_id = $userStorage->group_id;

	
	//
	if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    
    }

    if(!isset($params['area']) and !$params['area']){
		if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
			$params['area'] = $params['area_list'][0];
		}
	}

    if((!in_array($params['area'], $params['area_list'])) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        $this->_redirect(HOST);
        exit;
    }
    //END 
	
	$QArea            = new Application_Model_Area();
	$areas            = $QArea->get_cache();
	
	$QRegionalMarket  = new Application_Model_RegionalMarket();
	
	if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
		// l?y khu v?c c?a asm
		$QAsm = new Application_Model_Asm();
		$asm_cache = $QAsm->get_cache();
		$params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
	
	} elseif ($group_id == My_Staff_Group::SALES) {
		// l?y c?a hàng c?a sale
		$QStoreStaffLog = new Application_Model_StoreStaffLog();
		$store_cache = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
	
		$params['store_list'] = $store_cache;
		$params['sale_id'] = $userStorage->id;
	} elseif ($group_id == My_Staff_Group::LEADER) {
		// l?y c?a hàng c?a sale
		$QStoreLeaderLog = new Application_Model_StoreLeaderLog();
		$store_cache = $QStoreLeaderLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
	
		$params['store_list'] = $store_cache;
		$params['leader_id'] = $userStorage->id;
	}
	
	if ($area) {
		$where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area);
		$this->view->provinces = $QRegionalMarket->fetchAll($where, 'name');
	}
	
	if ($province) {
		$where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $province);
		$this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
	}
	
	if ($district) {
		//get store
		$QDistributor = new Application_Model_Distributor();
		$where = $QDistributor->getAdapter()->quoteInto('district = ?', $district);
		$this->view->distributors = $QDistributor->fetchAll($where, 'title');
	}
	
	$limit = 20;
	$total = $total2 = 0;
	
	$QKpiMonth = new Application_Model_KpiMonth();
    $sales = $QKpiMonth->fetchDistributorBi($page, $limit, $total, $params);
    
	$params['get_total_sales'] = true;
	$total_sales = $QKpiMonth->fetchDistributorBi(null, null, $total2, $params);
	unset($params['get_total_sales']);
	unset($params['asm']);
	unset($params['store_list']);
	unset($params['area_list']);
	unset($params['sale_id']);
	unset($params['leader_id']);
	
	$QLoyaltyPlan = new Application_Model_LoyaltyPlan();
	$loyalty_plan_list = $QLoyaltyPlan->get_cache();
	$this->view->loyalty_plan_list = $loyalty_plan_list;
	
	$this->view->total_quantity  = $total_sales['total_quantity'];
	$this->view->total_activated = $total_sales['total_activated'];
	$this->view->sales       = $sales;
	$this->view->offset      = $limit*($page-1);
	$this->view->total       = $total;
	$this->view->limit       = $limit;
	$this->view->url         = HOST.'bi/dealer'.( $params ? '?'.http_build_query($params).'&' : '?' );
	$this->view->desc        = $desc;
	$this->view->current_col = $sort;
	$this->view->to          = $to;
	$this->view->from        = $from;
	$this->view->areas       = $areas;
	$this->view->params      = $params;
	$this->view->action      = 'dealer-all';
    
       
?>