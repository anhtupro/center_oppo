<?php 
	set_time_limit(0);
    
    $QStaff           = new Application_Model_Staff();
    $QKpiMonth        = new Application_Model_KpiMonth();
    $QArea            = new Application_Model_Area();
    $QRegionalMarket  = new Application_Model_RegionalMarket();
    $QGood            = new Application_Model_Good();
    $QStaffTrainingReport = new Application_Model_StaffTrainingReport();
    $QAsm             = new Application_Model_Asm();
    $QTrainerCourseType = new Application_Model_TrainerCourseType();
    $QRankByMonth = new Application_Model_RankByMonth();
    
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
        exit;
    }

	$group_id    = $userStorage->group_id;
    $areas       = $QArea->get_cache();
    $this->view->areas = $areas;
    
    if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
        // lấy khu vực của asm
        $asm_cache = $QAsm->get_cache();
        $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
    
    }
    
	$area = $this->getRequest()->getParam('area');
    $province = $this->getRequest()->getParam('province');
    
    $where = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $province);
    $provi = $QRegionalMarket->fetchRow($where);
    $area  = $provi['area_id'];

	if((!in_array($area, $params['area_list'])) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
        $this->_redirect(HOST);
        exit;
    }

	$params = array(
		'area'       => $area,
		'area_list'  => $area,
        'province'   => $province
	);
	
    
    //Biểu đồ
    
    $year3 = date('Y', strtotime(date('Y-m')." -3 month"));
    $year2 = date('Y', strtotime(date('Y-m')." -2 month"));
    $year1 = date('Y', strtotime(date('Y-m')." -1 month"));
    $year0 = date('Y', strtotime(date('Y-m')." 0 month"));
    $year = array(
        $year0,
        $year1,
        $year2,
        $year3
    );
    
    //
    $year = array(
        date('Y', strtotime(date('Y-m')." 0 month")),
        date('Y', strtotime(date('Y-m')." -1 month")),
        date('Y', strtotime(date('Y-m')." -2 month")),
        date('Y', strtotime(date('Y-m')." -3 month"))
    );
    $this->view->year = $year;
    //
    $month3 = intval(date('m', strtotime(date('Y-m')." -3 month")));
    $month2 = intval(date('m', strtotime(date('Y-m')." -2 month")));
    $month1 = intval(date('m', strtotime(date('Y-m')." -1 month")));
    $month0 = intval(date('m', strtotime(date('Y-m')." 0 month")));
    
    $list_month = $month3.",".$month2.",".$month1.",".$month0;
    $this->view->list_month = explode(',',$list_month);
    
    //HR
    $staff      = $QStaff->getStaffBi($params);
    $staff_off = $QStaff->getStaffBiOff($params);
    $staff_joined = $QStaff->getStaffBiJoined($params);
    
    $this->view->staff = $staff;
    $this->view->staff_off = $staff_off;
    $this->view->staff_joined = $staff_joined;
    
    $params['area_list'] = array($area);

    //Trainer 
    $row_trainer = $QStaffTrainingReport->reportDataBi($params);
    $row_trainer_course = $QStaffTrainingReport->reportDataBiCourse($params);

    $data_trainer = array();
    foreach ($row_trainer as $key => $value) {
        $data_trainer[$value['type']][$value['month_date']][$value['year_date']] = array(
                                                                    'sum' => $value['sum'],
                                                                    'quantity_session' => $value['quantity_session']
                                                                    );
    }


    $data_trainer_course = array();
    foreach ($row_trainer_course as $key => $value) {
        $data_trainer_course[$value['type']][$value['month_date']][$value['year_date']] = array(
                                                                    'quantity_pg' => $value['quantity_pg'],
                                                                    'quantity_pass' => $value['quantity_pass'],
                                                                    'quantity_course' => $value['quantity_course']
                                                                    );
    }

    $row_lesson = $QKpiMonth->reportDataLesson($params);

    $trainer_course_type = $QTrainerCourseType->get_cache();

    $this->view->data_trainer = $data_trainer;
    $this->view->data_trainer_course = $data_trainer_course;
    $this->view->trainer_course_type = $trainer_course_type;
    $this->view->row_lesson   = $row_lesson;

    $total_sale_pgs = $QStaff->getTotalSalePgs($params);
    $this->view->total_sale_pgs = $total_sale_pgs['sum'];
    //END trainer
    
    
    //Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
    $list_date = array(
        date('Y-m-d', strtotime(date('Y-m')." -3 month")),
        date('Y-m-d', strtotime(date('Y-m')." -2 month")),
        date('Y-m-d', strtotime(date('Y-m')." -1 month")),
        date('Y-m-d'),
    );

    $name_chart = array(
        '2316'  => 'TGDĐ',
        '2363'  => 'FPT',
        '2317'  => 'VTA',
        '9187'  => 'Vinpro',
        '10007' => 'VIETTEL',
        '2325'  => 'CES',
        '3'     => 'DIAMOND',
        '2'     => 'GOLD',
        '1'     => 'SILVER',
        '4'     => 'NORMAL',
        '999'   => 'OTHER KA',
        '888'   => 'RETURN',
    );

    //$params = array();
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
    $params['to'] = date('Y-m-d');

    $sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
    $data_good = $QKpiMonth->getSelloutGood($params);

    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
            $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value']; 
        }
        else{
            $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value'];  
        }
                                            
    }
    /* END sellout, total value theo từng kênh */

    $this->view->data_sellout_dealer = $data_sellout_dealer;
    $this->view->data_sellout_total = $data_sellout_total;
    $this->view->list_date = $list_date;
    $this->view->name_chart = $name_chart;

    $this->view->data_sellout_good = $data_good;
    $this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);
    $this->view->good = $QGood->get_cache();
    
    
    /* TRADE MARKETTING*/
    $params['month'] = array();

    foreach ($list_date as $key => $value) {
        $params['month'][] = intval(date('m', strtotime($value)));
    }

    $fee_cost = $QKpiMonth->get_fee_cost($params);
    $fee_air  = $QKpiMonth->get_fee_air($params);
    $fee_campaign  = $QKpiMonth->get_fee_campaign($params);

    $data_trade = array();
    $data_trade_total = array(
        'fee_cost'     => 0,
        'fee_air'      => 0,
        'fee_campaign' => 0,
    );

    foreach ($fee_cost as $key => $value) {
        $data_trade[intval($value['month_date'])] = $value['sum'];
        //
        $data_trade_total['fee_cost'] += $value['sum'];
    }

    foreach ($fee_air as $key => $value) {
        if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
            $data_trade[intval($value['month_date'])] += $value['sum'];
        }
        else{
            $data_trade[intval($value['month_date'])] = $value['sum'];
        }
        //
        $data_trade_total['fee_air'] += $value['sum'];
    }

    foreach ($fee_campaign as $key => $value) {
        if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
            $data_trade[intval($value['month_date'])] += $value['sum'];
        }
        else{
            $data_trade[intval($value['month_date'])] = $value['sum'];
        }
        //
        $data_trade_total['fee_campaign'] += $value['sum'];
    }
    $this->view->data_trade = $data_trade;
    $this->view->data_trade_total = $data_trade_total;
    /* END TRADE MARKETTING*/

    $this->view->params = $params;
    /* NHÂN SỰ NEW */
    $headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale_area($params);
    $data_pgpb_sale = array();
    foreach ($headcount_pgpb_sale as $key => $value) {
        $data_pgpb_sale[$value['YearNo']][$value['MonthNo']][$value['title']] = array(
            'totalEmp'   => $value['totalEmp'],
            'Working'    => $value['Working'],
            'Off'        => $value['off'],
            'Per_Working'=> $value['Per_Working'],
            'Per_Off'    => $value['Per_Off']
            );
    }
    $this->view->data_pgpb_sale = $data_pgpb_sale;

    $rank_star_result = $QKpiMonth->getRankStar($params);
    $rank_star = array();
    foreach($rank_star_result as $k => $v){
        $rank_star[$v['star']] = $v['sum'];
    }
    $this->view->rank_star = $rank_star;

    /*total_sales_channel */
    $channel_list = $QKpiMonth->getChannel($params);
    $get_staff_channel   = $QKpiMonth->getTotalSalesChannel($params);
    $staff_channel = [];
    foreach($get_staff_channel as $key=>$value){
        $staff_channel[$value['channel_id']]['sale'] = $value['total_sale'];
        $staff_channel[$value['channel_id']]['pgs'] = $value['total_pgs'];
    }


    $this->view->channel_list            = $channel_list;
    $this->view->staff_channel = $staff_channel;

    $total_sales_channel = $QKpiMonth->getTotalSalesChannel($params);
    $sales_channel_quarter = $QKpiMonth->getTotalSalesChannelQuarter($params);
    $sales_channel_quarter_this = $QKpiMonth->getTotalSalesChannelQuarterThis($params);

    $sales_channel = array();
    foreach ($total_sales_channel as $key => $value) {
        $sales_channel[$value['channel']][$value['title_sum']] = $value['sum'];
    }
    $this->view->sales_channel = $sales_channel;
    $this->view->name_chart_sales_channel = $name_chart_sales_channel;
    $this->view->sales_channel_quarter    = $sales_channel_quarter;

    $this->view->sales_channel_quarter_first = $sales_channel_quarter[1];
    $this->view->sales_channel_quarter_this  = $sales_channel_quarter_this;

    /*total_store_channel */
    $total_store_channel   = $QKpiMonth->getTotalStoreChannel($params);
    $store_channel = array();
    foreach ($total_store_channel as $key => $value) {
        $store_channel[$value['channel_id']] = $value['total_store'];
    }

    $this->view->store_channel = $store_channel;
    
    //KPI avg
    $avg_sale_pg = $QKpiMonth->kpi_avg_bi($params);
    $this->view->avg_sale_pg = $avg_sale_pg;
    //END KPI avg
    /* NHÂN SỰ */

    /* sellout by date */
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -1 month"));
    $data_good_by_date = $QKpiMonth->getGoodByDate($params);
    $good_by_date = array();
    foreach ($data_good_by_date as $key => $value) {
        $good_by_date[$value['date']][$value['good_id']] = $value['num'];
        $good_by_date[$value['date']][9999] += $value['num'];
    }
    $list_date_range = $this->date_range($params['from'], $params['to']);
    $this->view->good_by_date = $good_by_date;
    $this->view->list_date_range = $list_date_range;

    //Số bán hàng ngày
    $good_hero_total = array(
        HERO_ID => HERO_NAME,   
        '9999' => 'Total'
    );
    $this->view->good_hero_total = $good_hero_total;
    /* end sellout by date */

    /* RANK SELLOUT BY PG */

    $rank_by_month = $QRankByMonth->getAll($params);
    $rank = array();
    foreach ($rank_by_month as $key => $value) {
        $rank[$value['rank']][$value['month']][$value['year']] = array(
            'sum_store' =>  $value['sum'],
            'sum_pg'    =>  $value['count_staff'],
            );
    }
    $this->view->rank = $rank;
    /* END RANK SELLOUT BY PG */


    $this->view->province = $QRegionalMarket->get_cache();
?>