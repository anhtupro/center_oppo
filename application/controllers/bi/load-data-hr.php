<?php 
    
    $area_id = $this->getRequest()->getParam('area_id');

    $QKpiMonth = new Application_Model_KpiMonth();
    
    $params = [
        'area_id' => $area_id
    ];
    
    $list_staff = $QKpiMonth->getListStaffDashboard($params);
    
    echo json_encode($list_staff);
    exit;
    
	