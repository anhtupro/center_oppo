<?php 

$params = array();

$QKpiMonth = new Application_Model_KpiMonth();
$QStaff = new Application_Model_Staff();
$QGood = new Application_Model_Good();
$QStaffTrainingReport = new Application_Model_StaffTrainingReport();
$QTrainerCourseType = new Application_Model_TrainerCourseType();
$QArea = new Application_Model_Area();
$QAsm = new Application_Model_Asm();
$QRankByMonth = new Application_Model_RankByMonth();
$QStaffChannel = new Application_Model_StaffChannel();

//Phân quyền
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;

if($group_id == LEADER_ID){
    $this->_redirect(HOST.'bi/leader-details?id='.$userStorage->id);
}
elseif($group_id == SALES_ID){
	$this->_redirect(HOST.'bi/sale-details?id='.$userStorage->id);
}
elseif($group_id == PGPB_ID){
	$this->_redirect(HOST.'bi/pg-details?id='.$userStorage->id);
}
elseif($group_id == TRAINING_TEAM_ID){
	$this->_redirect(HOST.'bi/trainer-details?id='.$userStorage->id);
}
elseif($group_id == TRAINING_LEADER_ID){
	$this->_redirect(HOST.'bi/trainer-leader-details?id='.$userStorage->id);
}  


$areas       = $QArea->get_cache();


if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
    exit;
}

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
	// lấy khu vực của asm
	$asm_cache = $QAsm->get_cache();
	$params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
	$params['area'] = $params['area_list'];
}

if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
	exit;
}

$staff_team = $QKpiMonth->getStaffBiTeam($params);
$staff_title = $QKpiMonth->getStaffBiTitle($params);

$staff_off = $QStaff->getStaffBiOff($params);
$staff_joined = $QStaff->getStaffBiJoined($params);
$total_sale_pgs = $QStaff->getTotalSalePgs($params);
$this->view->total_sale_pgs = $total_sale_pgs['sum'];

//END Phân quyền

$year3 = date('Y', strtotime(date('Y-m')." -3 month"));
$year2 = date('Y', strtotime(date('Y-m')." -2 month"));
$year1 = date('Y', strtotime(date('Y-m')." -1 month"));
$year0 = date('Y', strtotime(date('Y-m')." 0 month"));
$year = array(
    $year0,
    $year1,
    $year2,
    $year3
);

$month3 = intval(date('m', strtotime(date('Y-m')." -3 month")));
$month2 = intval(date('m', strtotime(date('Y-m')." -2 month")));
$month1 = intval(date('m', strtotime(date('Y-m')." -1 month")));
$month0 = intval(date('m', strtotime(date('Y-m')." 0 month")));

$list_month = array(
	$month3,
	$month2,
	$month1,
	$month0
	);

$this->view->list_month = $list_month;
$this->view->staff_team = $staff_team;
$this->view->staff_title = $staff_title;

$this->view->staff_off = $staff_off;
$this->view->staff_joined = $staff_joined;
$this->view->year = $year;


//Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
$list_date = array(
	date('Y-m-d', strtotime(date('Y-m')." -3 month")),
	date('Y-m-d', strtotime(date('Y-m')." -2 month")),
	date('Y-m-d', strtotime(date('Y-m')." -1 month")),
	date('Y-m-d'),
);

$name_chart = $QStaffChannel->get_staff_channel($userStorage->id);

$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
$params['to'] = date('Y-m-d');
$params['staff_id'] = $userStorage->id;

$channel = array();

foreach ($name_chart as $key => $value) {
	$channel[] = $key;
}

$params['channel'] = $channel;

$this->view->channel = $channel;

if((isset($params['area_list']) and $params['area_list']) OR (isset($params['channel']) and $params['channel'])){
    
    $sell_out_dealer = $QKpiMonth->getSelloutByDealer($params);
    $data_good = $QKpiMonth->getSelloutGood($params);

    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value'],
                                                                            'total_value_100' => $value['total_value_100']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]){
            $data_sellout_total[$value['month_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']]['total_value'] +=  $value['total_value'];
            $data_sellout_total[$value['month_date']]['total_value_100'] +=  $value['total_value_100'];
        }
        else{
            $data_sellout_total[$value['month_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']]['total_value'] =  $value['total_value']; 
            $data_sellout_total[$value['month_date']]['total_value_100'] =  $value['total_value_100']; 
        }
                                            
    }
    /* END sellout, total value theo từng kênh */
    $this->view->data_sellout_dealer = $data_sellout_dealer;
	$this->view->data_sellout_total = $data_sellout_total;
    $bk_sellout_dealer = $data_sellout_dealer;
}
else{
	$sell_out_dealer = $QKpiMonth->getSelloutDealer($params);
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        
        
	$data_good = $QKpiMonth->getSelloutGoodCache($params);
	$this->view->data_sellout_dealer = $sell_out_dealer['data_sellout_dealer'];
	$this->view->data_sellout_total = $sell_out_dealer['data_sellout_total'];
    $bk_sellout_dealer = $sell_out_dealer['data_sellout_dealer'];
}

$good_hero = unserialize(LIST_PRODUCT_HERO_BI);

$this->view->list_date = $list_date;
$this->view->name_chart = $name_chart;
$this->view->data_sellout_good = $data_good;
$this->view->good_hero = $good_hero;
$this->view->good = $QGood->get_cache();

//Số bán hàng ngày
$list_good_hero_total = array(426, 9999);
$good_hero_total = array(
	'426' => 'F1S',
	'9999' => 'Total'
);

$this->view->list_good_hero_total = $list_good_hero_total;
$this->view->good_hero_total = $good_hero_total;



/* NHÂN SỰ NEW */
if(isset($params['area_list']) and $params['area_list']){
	$headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale_area_index($params);
}
else{
	$headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale($params);
}

$data_pgpb_sale = array();
foreach ($headcount_pgpb_sale as $key => $value) {
	$data_pgpb_sale[$value['YearNo']][$value['MonthNo']][$value['title']] = array(
		'totalEmp'   => $value['totalEmp'],
		'Working'	 => $value['Working'],
		'Off'	 	 => $value['off'],
		'Per_Working'=> $value['Per_Working'],
		'Per_Off'	 => $value['Per_Off']
		);
}
$this->view->data_pgpb_sale = $data_pgpb_sale;

/*total_sales_channel */
$total_sales_channel   = $QKpiMonth->getTotalSalesChannelKa($params);

$sales_channel = array();
foreach ($total_sales_channel as $key => $value) {
	$sales_channel[$value['channel']][$value['title_sum']] = $value['sum'];
}
$this->view->sales_channel            = $sales_channel;
$this->view->name_chart_sales_channel = $name_chart;

/*total_store_channel */
$total_store_channel   = $QKpiMonth->getTotalStoreChannelKa($params);

$store_channel = array();
foreach ($total_store_channel as $key => $value) {
	$store_channel[$value['channel']] = $value['sum'];
}
$this->view->store_channel = $store_channel;

//KPI avg
if(isset($params['area_list']) and $params['area_list']){
	$avg_sale_pg = $QKpiMonth->kpi_avg_bi($params);
}
else {
	$avg_sale_pg = $QKpiMonth->kpi_avg_bi_cache($params);
}
//END KPI avg

$this->view->avg_sale_pg = $avg_sale_pg;
$this->view->params = $params;


?>