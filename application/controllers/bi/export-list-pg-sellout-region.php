<?php
    $QArea              = new Application_Model_Area();
    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();
    $titleEmp               = $this->getRequest()->getParam('title');
    $id                     = $this->getRequest()->getParam('id');
    $y                      = $this->getRequest()->getParam('y');
    $m                      = $this->getRequest()->getParam('m');
    $type_sell              = $this->getRequest()->getParam('type_sell');
    $area_id            = $this->getRequest()->getParam('area_id');
    $province_id        = $this->getRequest()->getParam('province_id');
    $regional                   = $this->getRequest()->getParam('regional');
    $params =array(
        'y'                 => intval($y),
        'm'                 => intval($m),
        'type_sell'         => intval($type_sell),
        'id'                => $id,
        'area_id'           =>$area_id,
        'province_id'       =>$province_id,
        'regional'          => $regional
    );
    //lấy khu vưc
    $area                   = $QAsm->get_cache($id);
    $list_area              = $area['area'];
    $params['area_list']    = $list_area;

    $data = $QKpiMonth->getExportListRegionSellout($params);

	include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
	require_once 'PHPExcel.php';
	$PHPExcel = new PHPExcel();
	$PHPExcel->setActiveSheetIndex(0);
	$sheet = $PHPExcel->getActiveSheet();
	$alpha = 'A';
	$index = 1;
	$heads = array(
	    'CODE',
	    'FULLNAME',
	    'DEPARTMENT',
	    'TEAM',
	    'TITLE',
	    'AREA'
	   
	);
	foreach ($heads as $key)
	{
	    $sheet->setCellValue($alpha . $index, $key);
	    $alpha++;
	}

	 
	 $index = 2;
	foreach($data as $key=>$value){
	 $alpha = 'A';

	 $sheet->getCell($alpha++ . $index)->setValueExplicit($value['code'],PHPExcel_Cell_DataType::TYPE_STRING);
	 $sheet->getCell($alpha++ . $index)->setValueExplicit($value['fullname'],PHPExcel_Cell_DataType::TYPE_STRING);
	 $sheet->getCell($alpha++ . $index)->setValueExplicit($value['department'],PHPExcel_Cell_DataType::TYPE_STRING);
	  $sheet->getCell($alpha++ . $index)->setValueExplicit($value['team'],PHPExcel_Cell_DataType::TYPE_STRING);
	   $sheet->getCell($alpha++ . $index)->setValueExplicit($value['title'],PHPExcel_Cell_DataType::TYPE_STRING);
	    $sheet->getCell($alpha++ . $index)->setValueExplicit($value['regional'],PHPExcel_Cell_DataType::TYPE_STRING);
		 $index ++;
	} 
	// //format
	

	$filename = ' Staff_Sellout ';
	$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
	$objWriter->save('php://output');
	exit;
	$this->redirect(HOST.'bi/list-pg-sellout-region?id='.$id.'&y='.$y.'&m='.$m.'&type_sell='.$type_sell);


