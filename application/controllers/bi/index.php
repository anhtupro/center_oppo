<?php 

$month_number  = $this->getRequest()->getParam('month_number');

$month_number = (!empty($month_number) AND $month_number >= 3 AND $month_number <= 12) ? $month_number : 3;

$params = array(
    'month_number' => $month_number
);

$QKpiMonth = new Application_Model_KpiMonth();
$QStaff = new Application_Model_Staff();
$QGood = new Application_Model_Good();
$QStaffTrainingReport = new Application_Model_StaffTrainingReport();
$QTrainerCourseType = new Application_Model_TrainerCourseType();
$QArea = new Application_Model_Area();
$QAsm = new Application_Model_Asm();
$QRankByMonth = new Application_Model_RankByMonth();
$QNotificationPgs  = new Application_Model_NotificationPgs();
$QStaffTraining  = new Application_Model_StaffTraining();

//Phân quyền
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;
$this->view->userStorage=$userStorage;

$params['trainer_id'] = $userStorage->id;

if($userStorage->title == 374 || $userStorage->id == 910 || $userStorage->title == 748 || $userStorage->title == 751){
	$this->_redirect(HOST.'bi/trainer-leader-details?id='.$userStorage->id);
} 

if(in_array($userStorage->id, array(910,16090,4266,2123))){
	$this->_redirect(HOST.'bi/brand-shop');
}

if($group_id == LEADER_ID){
    $this->_redirect(HOST.'bi/leader-details?id='.$userStorage->id);
}
elseif($group_id == SALES_ID || $group_id == PB_SALES_ID || $userStorage->title == SALES_TITLE || $userStorage->title == STORE_LEADER_TITLE){
	$this->_redirect(HOST.'bi/sale-details?id='.$userStorage->id);
}
elseif($group_id == PG_LEADER_ID){
	$this->_redirect(HOST.'bi/pgs-leader?id='.$userStorage->id);
}
elseif($group_id == PGPB_ID || $userStorage->title == PGPB_TITLE){
	$this->_redirect(HOST.'bi/pg-details?id='.$userStorage->id);
}
elseif($group_id == TRAINING_TEAM_ID || $userStorage->title == 746 || $userStorage->title == 747 || $userStorage->title == 749 || $userStorage->title == 751){
	$this->_redirect(HOST.'bi/trainer-details?id='.$userStorage->id);
}
elseif($group_id == TRAINING_LEADER_ID){
	$this->_redirect(HOST.'bi/trainer-leader-details?id='.$userStorage->id);
}  

//KEY ACCOUNT LEADER -> bi/key-acount
if($userStorage->team == KA_TEAM_ID){
    $this->_redirect(HOST.'bi/key-acount');
}

$areas       = $QArea->get_cache();
$this->view->area_cache=$areas;

if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
    echo "<pre>";
    print_r("No permission");
    die;
    exit;
}

if (!My_Staff_Permission_Area::view_bi_all($userStorage->id)) {
	// lấy khu vực của asm
	$asm_cache = $QAsm->get_cache();
	$params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
	$params['area'] = $params['area_list'];
       
}

if((!isset($params['area_list']) or !$params['area_list']) and !My_Staff_Permission_Area::view_bi_all($userStorage->id)){
	exit;
}

$staff_team = $QKpiMonth->getStaffBiTeam($params);
$staff_title = $QKpiMonth->getStaffBiTitle($params);

$staff_off = $QStaff->getStaffBiOff($params);
$staff_joined = $QStaff->getStaffBiJoined($params);
$total_sale_pgs = $QStaff->getTotalSalePgs($params);
$this->view->total_sale_pgs = $total_sale_pgs['sum'];

//END Phân quyền

$year3 = date('Y', strtotime(date('Y-m')." -3 month"));
$year2 = date('Y', strtotime(date('Y-m')." -2 month"));
$year1 = date('Y', strtotime(date('Y-m')." -1 month"));
$year0 = date('Y', strtotime(date('Y-m')." 0 month"));
$year = array(
    $year0,
    $year1,
    $year2,
    $year3
);


$list_date_off = array(
    date('Y-m-d', strtotime(date('Y-m')." -4 month")),
    date('Y-m-d', strtotime(date('Y-m')." -3 month")),
    date('Y-m-d', strtotime(date('Y-m')." -2 month")),
    date('Y-m-d', strtotime(date('Y-m')." -1 month")),
    date('Y-m-d'),
);


$month3 = intval(date('m', strtotime(date('Y-m')." -3 month")));
$month2 = intval(date('m', strtotime(date('Y-m')." -2 month")));
$month1 = intval(date('m', strtotime(date('Y-m')." -1 month")));
$month0 = intval(date('m', strtotime(date('Y-m')." 0 month")));

$list_month = array(
	$month3,
	$month2,
	$month1,
	$month0
	);

$this->view->list_month = $list_month;
$this->view->staff_team = $staff_team;
$this->view->staff_title = $staff_title;

$this->view->staff_off = $staff_off;
$this->view->staff_joined = $staff_joined;
$this->view->year = $year;
$this->view->list_date_off = $list_date_off;

//Report mới tạo : lấy dữ liệu live (ko chốt nữa) 
$list_date = [];
for($i = $month_number; $i > 0; $i--){
    $list_date[] = date('Y-m-d', strtotime(date('Y-m')." -".$i." month"));
}
$list_date[] = date('Y-m-d');


$get_name_chart = $QKpiMonth->getChannel($params);
$name_chart_color = [];
$name_chart = [];
foreach($get_name_chart as $key=>$value){
    $name_chart[$value['id']] = $value['name'];
    $name_chart_color[$value['id']] = $value['color'];
}

$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -".$month_number." month"));
$params['to'] = date('Y-m-d');


$this->view->avg_sale_pg = $avg_sale_pg;
$this->view->params = $params;

if(isset($params['area_list']) and $params['area_list']){
    
    $sell_out_dealer = $QKpiMonth->getSelloutByDealerNewArea($params);
    $data_good = $QKpiMonth->getSelloutGoodAreaNew($params);

    /* get sellout, total value theo từng kênh */
    $data_sellout_dealer = array();
    $data_sellout_total = array();
    foreach ($sell_out_dealer as $key => $value) {
        $data_sellout_dealer[$value['channel']][$value['month_date']][$value['year_date']] = array(
                                                                            'num' => $value['num'],
                                                                            'total_value' => $value['total_value']
                                                                            );


        if(isset($data_sellout_total[$value['month_date']][$value['year_date']]) and $data_sellout_total[$value['month_date']][$value['year_date']]){
            $data_sellout_total[$value['month_date']][$value['year_date']]['num']         +=  $value['num']; 
            $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] +=  $value['total_value']; 
        }
        else{
            $data_sellout_total[$value['month_date']][$value['year_date']]['num']         =  $value['num'];  
            $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] =  $value['total_value'];  
        }
                                            
    }
    /* END sellout, total value theo từng kênh */
    $this->view->data_sellout_dealer = $data_sellout_dealer;
    $this->view->data_sellout_total = $data_sellout_total;
}
else{
	$sell_out_dealer = $QKpiMonth->getSelloutDealerNew($params);
	$data_good = $QKpiMonth->getSelloutGoodCacheNew($params);
	$this->view->data_sellout_dealer = $sell_out_dealer['data_sellout_dealer'];
	$this->view->data_sellout_total = $sell_out_dealer['data_sellout_total'];
}
//Lấy danh sách hero good
$good_group = [];
$total_imei = 0;
foreach($data_good as $key=>$value){
    foreach($value as $k=>$v){
        foreach($v as $i=>$j){
            if(isset($good_group[$key]) and $good_group[$key]){
                $good_group[$key] += (int)$j['num'];
                $total_imei += (int)$j['num'];
            }
            else{
                $good_group[$key] = (int)$j['num'];
                $total_imei += (int)$j['num'];
            }
        }
    }
}

$good_hero = [];
foreach($good_group as $key=>$value){
    if($value/$total_imei*100 >= 5){
        $good_hero[] = $key;
    }
}
//END Lấy danh sách hero good

$this->view->list_date = $list_date;
$this->view->name_chart = $name_chart;
$this->view->name_chart_color = $name_chart_color;

$this->view->data_sellout_good = $data_good;
$this->view->good_hero = $good_hero;
$this->view->good = $QGood->get_cache();

//Số bán hàng ngày
$list_good_hero_total = array(HERO_ID, 9999);
$good_hero_total = array(
	HERO_ID => HERO_NAME,
	'9999' => 'Total'
);

$this->view->list_good_hero_total = $list_good_hero_total;
$this->view->good_hero_total = $good_hero_total;
$this->view->params = $params;

//HR
$thamnien = $QKpiMonth->getThamnien($params, $total_thamnien);
$this->view->thamnien = $thamnien;
$this->view->total_thamnien = $total_thamnien;


$channel_list = $QKpiMonth->getChannel($params);
$get_staff_channel   = $QKpiMonth->getTotalSalesChannel($params);
$staff_channel = [];
foreach($get_staff_channel as $key=>$value){
    $staff_channel[$value['channel_id']]['sale'] = $value['total_sale'];
    $staff_channel[$value['channel_id']]['pgs'] = $value['total_pgs'];
}


$this->view->channel_list            = $channel_list;
$this->view->staff_channel = $staff_channel;

$get_store_channel   = $QKpiMonth->getTotalStoreChannel($params);

$store_channel = [];
foreach($get_store_channel as $key=>$value){
    $store_channel[$value['channel_id']] = $value['total_store'];
}
$this->view->store_channel = $store_channel;


$rank_star_result = $QKpiMonth->getRankStar($params);
$rank_star = array();
foreach($rank_star_result as $k => $v){
	$rank_star[$v['star']] = $v['sum'];
}
$this->view->rank_star = $rank_star;


$rank_star_result_sales = $QKpiMonth->getRankStarSales($params);
$rank_star_sales = array();
foreach($rank_star_result_sales as $k => $v){
	$rank_star_sales[$v['star']] = $v['sum'];
}
$this->view->rank_star_sales = $rank_star_sales;


//Trainer 
$row_trainer = $QStaffTrainingReport->reportDataBi($params);
$row_trainer_course = $QStaffTrainingReport->reportDataBiCourse($params);

$data_trainer = array();
foreach ($row_trainer as $key => $value) {
	$data_trainer[$value['type']][$value['month_date']][$value['year_date']] = array(
																'sum' => $value['sum'],
																'quantity_session' => $value['quantity_session']
																);
}


$data_trainer_course = array();
foreach ($row_trainer_course as $key => $value) {
	$data_trainer_course[$value['type']][$value['month_date']][$value['year_date']] = array(
																'quantity_pg' => $value['quantity_pg'],
																'quantity_pass' => $value['quantity_pass'],
																'quantity_course' => $value['quantity_course']
																);
}

$row_lesson = $QKpiMonth->reportDataLesson($params);
$trainer_course_type = $QTrainerCourseType->get_cache();

$this->view->data_trainer = $data_trainer;
$this->view->data_trainer_course = $data_trainer_course;
$this->view->trainer_course_type = $trainer_course_type;
$this->view->row_lesson   = $row_lesson;





$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -4 month"));

if(isset($params['area_list']) and $params['area_list']){
    $headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale_area_index($params);
}
else{
    $headcount_pgpb_sale = $QKpiMonth->headcount_pgpb_sale($params);
}



$data_pgpb_sale = array();
foreach($headcount_pgpb_sale as $key => $value) {
    $data_pgpb_sale[$value['YearNo']][$value['MonthNo']][$value['title']] = array(
        'totalEmp'   => $value['totalEmp'],
        'Working'    => $value['Working'],
        'Off'        => $value['off'],
        'Per_Working'=> $value['Per_Working'],
        'Per_Off'    => $value['Per_Off']
        );
}
$this->view->data_pgpb_sale = $data_pgpb_sale;

//Staff Main New
$get_staff_main_new = $QStaffTraining->staffMainNew($params);

$staff_main_new = [];
foreach($get_staff_main_new as $key=>$value){
    $staff_main_new[$value['MonthNo']][$value['YearNo']] = $value;
}
$this->view->staff_main_new = $staff_main_new;


if(isset($params['area_list']) and $params['area_list']){
    $avg_sale_pg = $QKpiMonth->kpi_avg_bi($params);
}
else {
    $avg_sale_pg = $QKpiMonth->kpi_avg_bi_cache($params);
}
//END KPI avg

$this->view->avg_sale_pg = $avg_sale_pg;

//END Staff Main New


?>