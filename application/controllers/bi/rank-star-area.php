<?php
    $QArea              = new Application_Model_Area();
    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();
    $id               = $this->getRequest()->getParam('id');
    $rank               = $this->getRequest()->getParam('rank');
    $regional               = $this->getRequest()->getParam('regional');
    $params =array(
        'id'                => $id,
        'rank'                => $rank,
        'regional'             => $regional
    );

    //lấy khu vưc
    $area                   = $QAsm->get_cache($id);
    $list_area              = $area['area'];
    $params['area_list']    = $list_area;
//var_dump($area); exit;



    $rows = $QKpiMonth->getRankStarArea($params);



$this->view->result     = $rows;
$this->view->params     = $params;

$this->view->area       = $QArea->get_cache();


?>