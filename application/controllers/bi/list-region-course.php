<?php

	$QKpiMonth        = new Application_Model_KpiMonth();
    $QAsm            = new Application_Model_Asm();

    $type_course = [
        1 => 'Training nội bộ',
        2 => 'Training đối tác',
        3 => 'PGs mới'
    ];

	$id_trainer  = $this->getRequest()->getParam('trainer_id');	
	$lesson_id	= $this->getRequest()->getParam('lesson_id');
    $month  = $this->getRequest()->getParam('month');
    $year  = $this->getRequest()->getParam('year');
    $type  = $this->getRequest()->getParam('type');

	$area = $QAsm->get_cache($id_trainer);
    $list_kv = $area['area'];
    $params = [
        'area_list'     => $list_kv,
        'lesson_id'     => $lesson_id,
        'trainer_id'    => $id_trainer,
        'month'         => $month,
        'year'          => $year,
        'type'          => $type
    ];
    if($type == 3){
        $list_region = $QKpiMonth->getBigareaCourse($params);
    }
    elseif($type == 1 || $type==2){
        $list_region = $QKpiMonth->getBigareaTrainingReport($params);
    }

// if($type==2){

//     $list_region_dealer=$QKpiMonth->getRegionDealerTrainingReport($params);
// }
    

    $this->view->list_region = $list_region;
    $this->view->params=$params;
    $this->view->type_course = $type_course;
//    $this->view->list_region_dealer=$list_region_dealer;


?>