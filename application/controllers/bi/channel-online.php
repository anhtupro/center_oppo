<?php
$tmpTo          = date('Y-m-d',strtotime('-1 days'));
$tmpFrom        = date('Y-m-d',strtotime($tmpTo.' -30 days'));
$from           = $this->getRequest()->getParam('from',$tmpFrom);
$to             = $this->getRequest()->getParam('to',$tmpTo);
$channel        = $this->getRequest()->getParam('channel');
$name_chart     = unserialize(ONLINE_CHANNEL);
$QMarket        = new Application_Model_Market();
$QDistributor   = new Application_Model_Distributor();
$QGood = new Application_Model_Good();
$channel_ = [];
foreach ($name_chart as $key => $value) {
	$channel_[] = $key;
}
$params = array('from' => $from, 'to' => $to, 'channel' => $channel);

$sellin = $QMarket->onlineGood($params);
$sellin_month = $QMarket->OnlineGoodByMonth($params);
$list_good = $QMarket->onlineGoodByTop($params);

$list_date = $this->date_range($from, $to);

$data_good = array();
$data_date = array();

foreach($sellin as $key=>$value){
    
    if(isset($data_good[$value['good_id']]['sellin']))
        $data_good[$value['good_id']]['sellin'] += intval($value['sellin']);
    else
        $data_good[$value['good_id']]['sellin'] = intval($value['sellin']);

    if(isset($data_date[$value['date']][$value['good_id']]['sellin']))
        $data_date[$value['date']][$value['good_id']]['sellin'] += intval($value['sellin']);
    else
        $data_date[$value['date']][$value['good_id']]['sellin'] = intval($value['sellin']);

    if(isset($data_date[$value['date']]['total_sellin']))
        $data_date[$value['date']]['total_sellin'] += intval($value['sellin']);
    else
        $data_date[$value['date']]['total_sellin'] = intval($value['sellin']);

}
$this->view->data_date = $data_date;
$this->view->list_good = $list_good;
$this->view->distributor = $QDistributor->get_cache();
$this->view->sellin_month = $sellin_month;
$this->view->params = $params;
$this->view->list_date = $list_date;
$this->view->good_cache = $QGood->get_good_name();
