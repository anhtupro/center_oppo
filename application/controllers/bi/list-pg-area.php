<?php
    $QArea              = new Application_Model_Area();

        

    $QKpiMonth              = new Application_Model_KpiMonth();
    $QAsm                   = new Application_Model_Asm();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();


    //totalEmp
    $titleEmp               = $this->getRequest()->getParam('title');
    //sellout
    $id                     = $this->getRequest()->getParam('id');
    $y                      = $this->getRequest()->getParam('y');
    $m                      = $this->getRequest()->getParam('m');
    $type_sell              = $this->getRequest()->getParam('type_sell');
    //search
    $name                   = $this->getRequest()->getParam('name');
    $code                   = $this->getRequest()->getParam('code');
    $email                  = $this->getRequest()->getParam('email');
    $title                  = $this->getRequest()->getParam('title');
    $shop                   = $this->getRequest()->getParam('shop');
    $star                   = $this->getRequest()->getParam('star');
    $from_date              = $this->getRequest()->getParam('from_date');
    $to_date                = $this->getRequest()->getParam('to_date');
    $sellout_from           = $this->getRequest()->getParam('sellout_from');
    $sellout_to             = $this->getRequest()->getParam('sellout_to');
    $seniority              = $this->getRequest()->getParam('seniority');
    $area_id            = $this->getRequest()->getParam('area_id');
    $province_id        = $this->getRequest()->getParam('province_id');
    $regional                   = $this->getRequest()->getParam('regional');
    $params =array(
        'y'                 => intval($y),
        'm'                 => intval($m),
        'type_sell'         => intval($type_sell),
        'id'                => $id,
        'name'              => $name,
        'code'              => $code,
        'email'             => $email,
        'title'             => $title,
        'star'              => $star,
        'shop'              => $shop,
        'from_date'         => $from_date,
        'to_date'           => $to_date,
        'sellout_from'      => $sellout_from,
        'sellout_to'        => $sellout_to,
        'seniority'         => $seniority,
        'area_id'           =>$area_id,
        'province_id'       =>$province_id,
        'regional'          => $regional
    );
    //lấy khu vưc
    
    //lấy khu vưc
    if($userStorage->group_id == BOARD_ID){
        $area_cache = $QArea->get_cache();
        $area = [];
        foreach($area_cache as $key=>$value){
            $area[] = $key;
        }
        $list_area = $area;
    }
    else{
        $area                   = $QAsm->get_cache($id);
        $list_area              = $area['area'];
    }
    
    
    $params['area_list']    = $list_area;
    $params['from'] = date('Y-m-01', strtotime(date('Y-m')." -4 month"));
    $params['to'] = date('Y-m-d');



    $rows = $QKpiMonth->getListPgArea($params);



$this->view->result     = $rows;
$this->view->params     = $params;

$this->view->area       = $QArea->get_cache();


?>