<?php 

$params = array();

$QKpiMonth = new Application_Model_KpiMonth();
$QStore    = new Application_Model_Store();
$QTeam     = new Application_Model_Team();
$QStaff    = new Application_Model_Staff();
$QGood 	   = new Application_Model_Good();
$QStaffTrainingReport = new Application_Model_StaffTrainingReport();
$QTrainerCourseType = new Application_Model_TrainerCourseType();
$QArea 	      = new Application_Model_Area();
$QAsm 	      = new Application_Model_Asm();
$QRankByMonth = new Application_Model_RankByMonth();

$good_hero = unserialize(LIST_PRODUCT_HERO_BI);
$this->view->good_hero = $good_hero;
$this->view->good = $QGood->get_cache();

//Phân quyền
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id    = $userStorage->group_id;
	
$areas       = $QArea->get_cache();


if(!My_Staff_Permission_Area::view_bi($userStorage->id)){
    exit;
}

if(!My_Staff_Permission_Area::view_bi_all($userStorage->id)){
	exit;
}



$list_date = array(
	date('Y-m-d', strtotime(date('Y-m')." -3 month")),
	date('Y-m-d', strtotime(date('Y-m')." -2 month")),
	date('Y-m-d', strtotime(date('Y-m')." -1 month")),
	date('Y-m-d'),
);
$this->view->list_date = $list_date;
$this->view->good_hero = unserialize(LIST_PRODUCT_HERO_BI);

$params = [];
$params['is_brand_shop'] = 1;
$params['from'] = date('Y-m-01', strtotime(date('Y-m')." -3 month"));
$params['to'] = date('Y-m-d');

$get_list_store = $QStore->getStoreBrandshop($params);
$list_store = [];

foreach($get_list_store as $key=>$value){
	$list_store[] = $value['id'];
}

$params['list_store'] = $list_store;

$sellout 						= $QKpiMonth->getSelloutGoodBrandShop($params);
$this->view->data_sellout 		= $sellout['data_sellout'];
$this->view->data_sellout_good  = $sellout['data_sellout_good'];
$this->view->brand_shop 		= $list_store;

$good_top = $QKpiMonth->getSelloutGoodBrandShopTop($params);
$this->view->good_top = $good_top;

 /* sellout by date */
$data_good_by_date = $QKpiMonth->getGoodByDate($params);
$good_by_date = array();
foreach ($data_good_by_date as $key => $value) {
    $good_by_date[$value['date']][$value['good_id']] = $value['num'];
    $good_by_date[$value['date']][9999] += $value['num'];
}
$list_date_range = $this->date_range($params['from'], $params['to']);
$this->view->good_by_date = $good_by_date;
$this->view->list_date_range = $list_date_range;

//Số bán hàng ngày
$good_hero_total = array(
    HERO_ID => HERO_NAME,
    '9999' => 'Total'
);
$this->view->good_hero_total = $good_hero_total;
/* end sellout by date */

$list_title_dept_brandshop_tmp = $QTeam->getTeamInDepartment(TEAM_RETAIL_BRAND_SHOP);

$list_title_dept_brandshop = array();
foreach ($list_title_dept_brandshop_tmp as $value){
    $list_title_dept_brandshop[] = $value['id'];
}

//Lấy danh sách nhân sự
$params['list_team'] = [BRAND_SHOP_TEAM];
$count_list_staff = $QKpiMonth->countListStaff($params);

/* RAND STAR */
$rank_star_result = $QKpiMonth->getRankStar($params);
$rank_star = array();
foreach($rank_star_result as $k => $v){
	$rank_star[$v['star']] = $v['sum'];
}
$this->view->rank_star = $rank_star;
/* END RAND STAR */



/* TRADE MARKETTING*/
$params['month'] = array();

foreach ($list_date as $key => $value) {
	$params['month'][] = intval(date('m', strtotime($value)));
}

//$fee_cost = $QKpiMonth->get_fee_cost($params);
$fee_air  = $QKpiMonth->get_fee_air($params);
//$fee_campaign  = $QKpiMonth->get_fee_campaign($params);

$data_trade = array();
$data_trade_total = array(
	'fee_cost' 	   => 0,
	'fee_air' 	   => 0,
	'fee_campaign' => 0,
);

if($fee_cost){
	foreach ($fee_cost as $key => $value) {
		$data_trade[intval($value['month_date'])] = $value['sum'];
		//
		$data_trade_total['fee_cost'] += $value['sum'];
	}
}

foreach ($fee_air as $key => $value) {
	if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
		$data_trade[intval($value['month_date'])] += $value['sum'];
	}
	else{
		$data_trade[intval($value['month_date'])] = $value['sum'];
	}
	//
	$data_trade_total['fee_air'] += $value['sum'];
}

foreach ($fee_campaign as $key => $value) {
	if(isset($data_trade[intval($value['month_date'])]) and $data_trade[intval($value['month_date'])]){
		$data_trade[intval($value['month_date'])] += $value['sum'];
	}
	else{
		$data_trade[intval($value['month_date'])] = $value['sum'];
	}
	//
	$data_trade_total['fee_campaign'] += $value['sum'];
}
$this->view->data_trade = $data_trade;
$this->view->data_trade_total = $data_trade_total;
/* END TRADE MARKETTING*/

$this->view->count_list_staff = $count_list_staff;

?>