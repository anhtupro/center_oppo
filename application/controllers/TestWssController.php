<?php
class TestWssController extends My_Controller_Action
{
    private $wssURI;
    private $namespace;

    public function init()
    {
        error_reporting(0);
        ini_set("display_error", -1);
        set_time_limit(0);
        ini_set('memory_limit', -1);

        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->wssURI = HOST.'test-wss';
        $this->namespace = 'OPPOVN';

        //doAuthenticate();
    }

    public function indexAction()
    {
        //Create a new soap server
        $server = new soap_server(null, array('uri' => $this->wssURI));

        /*$server->soap_defencoding = 'utf-8';
        $server->decode_utf8 = false;*/

        //Configure our WSDL
        $server->configureWSDL($this->namespace, $this->namespace, $this->wssURI);
        $server->wsdl->schemaTargetNamespace = $this->namespace;

        $server->register(
                'test_a',
                array(
                    'a'=> 'xsd:string'
                ),
                array('return' => 'xsd:Array'),
                $this->namespace,
                $this->namespace.'#test_a',
                'rpc',
                'encoded',
                'test thôi ma'
            );
        $POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
        
        //$POST_DATA = file_get_contents('php://input');
        
        $server->service($POST_DATA);
    }
}

function test_a($a){
    return array('a'=>1);
}