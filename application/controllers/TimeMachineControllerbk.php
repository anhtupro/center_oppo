z<?php
class TimeMachineController extends My_Controller_Action{
    
    public function indexAction(){
    }
	
    public function listManagerAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'list-manager.php'; 
    }

    public function overTimeAction()
    {
        $export = $this->getRequest()->getParam('export');
        $department = $this->getRequest()->getParam('department');
        
    }

    public function listStaffMapAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'list-staff-map.php'; 
    }

    public function personalPermissionAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'personal-permission.php'; 
    }

    public function addPersonalAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'add-personal.php';
    }

    public function delPersonalAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'del-personal.php';
    }

    public function lateListAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'late-list.php';
    }

    public function lateReportAction()
    {
        $export = $this->getRequest()->getParam('export', 0);

        $db = Zend_Registry::get('db');

        $sql = "SELECT
                    st.id as `id`,
                    st.code as `staff_code`,
                    st.ID_number as `cmnd`,
                    st.off_date as `off_date`,
                    st.joined_at as `joined_at`,
                    t1.name as `department_name`,
                    t2.name as `team_name`,
                    ar.name as `area` ,
                    CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                    cid.check_in_day as `check_in_day`,
                    cid.check_in_at as `check_in_at`,
                    cid.check_out_at as `check_out_at`,
                    IF(TIMEDIFF(TIME(cid.check_in_at), cis.begin) > '00:00:00',
                        TIMEDIFF(TIME(cid.check_in_at), cis.begin),
                        NULL
                    )
                     as `check_in_late_time`,
                    IF(TIMEDIFF(cis.end, TIME(cid.check_out_at)) > '00:00:00',
                        TIMEDIFF(cis.end, TIME(cid.check_out_at)),
                        NULL
                    )
                    as `check_out_soon_time`,
                    tt.reason,
                    tt.status,
                    IF(cid.check_in_late_time <= '00:15:00', 1, 0) as `soon_15`
                FROM `check_in_detail` cid
                JOIN `staff` st 
                    ON cid.staff_code = st.code
                LEFT JOIN `temp_time` tt 
                    ON tt.staff_id = st.id AND tt.date = cid.check_in_day AND tt.status = 1
                LEFT JOIN `time_late` tl
                    ON tl.staff_id = st.id AND tl.date = cid.check_in_day AND tl.status = 1
                LEFT JOIN `leave_detail` ld
                    ON ld.staff_id = st.id 
                        AND ld.status = 1 
                        AND (cid.check_in_day BETWEEN ld.from_date AND ld.to_date)
                LEFT JOIN `team` t1 ON st.department = t1.id
                LEFT JOIN `team` t2 ON st.team = t2.id
                LEFT JOIN `check_in_shift` cis ON st.code = cis.staff_code AND cis.date IS NULL
                INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
                INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
                WHERE (cid.check_in_day BETWEEN '2017-04-01' AND '2017-04-30')
                AND (cid.check_in_at IS NOT NULL AND cid.check_out_at IS NOT NULL)
                AND (tt.id IS NULL AND tl.id IS NULL AND ld.id IS NULL )
                AND DAY(cid.check_in_day) NOT IN (1, 2, 7, 14, 21, 28)
                AND (
                    TIMEDIFF(TIME(cid.check_in_at), cis.begin) > '00:00:00'
                );";
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $data_late = array();
        $data_staff_number = array();

        foreach($data as $value)
        {
            if($value['soon_15'] == 1)
            {
                if($data_staff_number[$value['id']] == 3 )
                {
                    $data_late[] = $value;
                }
                elseif(empty($data_staff_number[$value['id']]))
                {
                    $data_staff_number[$value['id']]++;
                }
                else
                {
                    $data_staff_number[$value['id']] = 1;
                }
            }
            else
            {
                $data_late[] = $value;
            }
            
        }

        // echo '<pre>';
        // print_r($data_late); die;

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $alphaExcel = new My_AlphaExcel();

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        
        $heads = array(
            $alphaExcel->ShowAndUp() => 'Mã nhân viên',
            $alphaExcel->ShowAndUp() => 'Tên nhân viên',
            $alphaExcel->ShowAndUp() => 'CMND',
            $alphaExcel->ShowAndUp() => 'Phòng ban',
            $alphaExcel->ShowAndUp() => 'Team',
            $alphaExcel->ShowAndUp() => 'Khu vực',
            $alphaExcel->ShowAndUp() => 'Ngày vào làm',
            $alphaExcel->ShowAndUp() => 'Ngày nghỉ',
            $alphaExcel->ShowAndUp() => 'Ngày',
            $alphaExcel->ShowAndUp() => 'Thứ',
            $alphaExcel->ShowAndUp() => 'Giờ vào',
            $alphaExcel->ShowAndUp() => 'Giờ ra',
            $alphaExcel->ShowAndUp() => 'Trễ',
            $alphaExcel->ShowAndUp() => 'Sớm',
        );
        
        $array_thu = array(
            'Mon' => 'Thứ 2',
            'Tue' => 'Thứ 3',
            'Wed' => 'Thứ 4',
            'Thu' => 'Thứ 5',
            'Fri' => 'Thứ 6',
            'Sat' => 'Thứ 7',
            'Sun' => 'CN',
        );

        foreach($heads as $key => $value)
        {
            $sheet->setCellValue($key.'1', $value);
        }
        $index = 0;
        foreach($data_late as $key => $value)
        {
            $late = (!empty($value['check_in_late_time']) )?date('H:i', strtotime($value['check_in_late_time'])):'';
            $soon = (!empty($value['check_out_soon_time']) )?date('H:i', strtotime($value['check_out_soon_time'])):'';

            if( ($late == '00:00' && $soon == '') || ($late == '' && $soon == '00:00')
                    || ($late == '00:00' && $soon == '00:00') )
            {
                
            }
            else
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['staff_code']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['staff_name']);
                $sheet->setCellValueExplicit($alphaExcel->ShowAndUp() . ($index+2), $value['cmnd'],  PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['department_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['team_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $value['area']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['joined_at'])?date('d/m/Y', strtotime($value['joined_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['off_date'])?date('d/m/Y', strtotime($value['off_date'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), date('d/m/Y', strtotime($value['check_in_day'])) );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), $array_thu[date('D', strtotime($value['check_in_day']))] );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['check_in_at'])?date('H:i', strtotime($value['check_in_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), !empty($value['check_out_at'])?date('H:i', strtotime($value['check_out_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), (!empty($value['check_in_late_time']) )?date('H:i', strtotime($value['check_in_late_time'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+2), (!empty($value['check_out_soon_time']) )?date('H:i', strtotime($value['check_out_soon_time'])):'' );
                $index++;
            } 
        }

        $filename = 'Xuất đi trễ - ' . date('Y-m-d H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
        $time_end_write = $this->microtime_float();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    public function lateDetailAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'late-detail.php';
    }

    public function printAction()
    {
        $this->_helper->layout->disableLayout();
        $month = $this->getRequest()->getParam('month', intval(date('m')));
        $year = $this->getRequest()->getParam('year', intval(date('Y')));
        $staff_id = $this->getRequest()->getParam('staff_id');
        $export = $this->getRequest()->getParam('export', 0);
		
		$QLogStaffWarning = new Application_Model_LogStaffWarning();
        
        $where = array();
        $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "MONTH(penalty_day) = $month" );
        $where[] = $QLogStaffWarning->getAdapter ()->quoteInto ( "YEAR(penalty_day) = $year" );
        $locked_day = $QLogStaffWarning->fetchRow ( $where );
		

        if(!empty($export))
        {
            $this->exportFile($month, $year, implode(",", $staff_id));
            exit;
        }

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $QCheckIn = new Application_Model_CheckIn();

        $params = array(
            'from_date' => $from,
            'to_date' => $to,
            'month' => $month,
            'year' => $year,
        );
		if(!empty($locked_day)){
             
            $db = Zend_Registry::get('db');
        
            $sql = "CALL `get_late_list_log`(:month, :year)";
        
            $stmt = $db->prepare($sql);
            $stmt->bindParam('month', $params['month'], PDO::PARAM_STR);
            $stmt->bindParam('year', $params['year'], PDO::PARAM_STR);
            $stmt->execute();
        
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;
           
        }else{
            $data = $QCheckIn->listLate($params);
        }
   
        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->staff_id = $staff_id;
        // print_r($params); die;
    }

    public function printEditAction()
    {
        $this->_helper->layout->disableLayout();

        $params = array();
    
        $params['staff_name']= $this->getRequest()->getParam('staff_name');
        $params['department'] = $this->getRequest()->getParam('staff_department');
        $params['title']= $this->getRequest()->getParam('staff_title');
        $params['number_15'] = $this->getRequest()->getParam('number_15');
        $params['number_30']= $this->getRequest()->getParam('number_30');
        $params['phat'] = $this->getRequest()->getParam('phat');
        $params['ngayxuphat'] = $this->getRequest()->getParam('ngayxuphat');
        $params['forget_check_in'] = $this->getRequest()->getParam('forget_check_in');
        $params['date_late'] = $this->getRequest()->getParam('date_late');
        $params['day_temp_time'] = $this->getRequest()->getParam('day_temp_time');
         $params['solan'] = $this->getRequest()->getParam('solan');
        
        $this->view->params = $params;
    }

    public function luoiAction()
    {
        $export = $this->getRequest()->getParam('export');
        $from_date = $this->getRequest()->getParam('from_date', date('1/m/Y'));
        $to_date = $this->getRequest()->getParam('to_date', date('d/m/Y'));
        $code = $this->getRequest()->getParam('code');
        $name = $this->getRequest()->getParam('name');
        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $params = array(
        );

        $params['from_date'] = !empty($from_date) ? DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') : null;
		$params['to_date'] = !empty($to_date) ? DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d') : null;
        $params['staff_code'] = $auth->code;
        $params['code'] = $code;
        $params['name'] = $name;

        $QCheckIn = new Application_Model_CheckIn();
        
        if(!empty($export))
        {   
            $time_start = $this->microtime_float();
            $data = $QCheckIn->xuat_luoi($params);
            $time_start_write = $this->microtime_float();
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            
            $heads = array(
                $alphaExcel->ShowAndUp() => 'STT',
                $alphaExcel->ShowAndUp() => 'Mã nhân viên',
                $alphaExcel->ShowAndUp() => 'Tên họ',
                $alphaExcel->ShowAndUp() => 'Phòng ban',
                $alphaExcel->ShowAndUp() => 'Khu vực',
                $alphaExcel->ShowAndUp() => 'Ngày',
                $alphaExcel->ShowAndUp() => 'Thứ',
                $alphaExcel->ShowAndUp() => 'Giờ vào',
                $alphaExcel->ShowAndUp() => 'Giờ ra',
                $alphaExcel->ShowAndUp() => 'Trễ',
                $alphaExcel->ShowAndUp() => 'Sớm',
                $alphaExcel->ShowAndUp() => 'Công',
                $alphaExcel->ShowAndUp() => 'Tổng giờ',
                $alphaExcel->ShowAndUp() => 'Tăng ca',
                // $alphaExcel->ShowAndUp() => 'Tổng toàn bộ',
                // $alphaExcel->ShowAndUp() => 'Ca',
            );
            
            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }

            foreach($data as $key => $value)
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $key+1);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['code']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staff_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['department']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['area_name']);
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('d/m/Y', strtotime($value['check_in_day'])) );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), date('D', strtotime($value['check_in_day'])) );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['check_in_at'])?date('H:i', strtotime($value['check_in_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), !empty($value['check_out_at'])?date('H:i', strtotime($value['check_out_at'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (!empty($value['late']) && !empty($value['check_out_at']) )?date('H:i', strtotime($value['late'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (!empty($value['soon']) && !empty($value['check_out_at']) )?date('H:i', strtotime($value['soon'])):'' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (floatval($value['cong'])>0)?number_format($value['cong'], 2):'0.00' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), (floatval($value['cong'])>0)?number_format($value['cong']*8, 2):'0.00' );
                $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2),  !empty($value['overtime'])?date('H:i', strtotime($value['overtime'])):'');
                // $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), number_format($value['total']-1.5, 2) );
                
            }

            $filename = 'Xuất lưới - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
            $time_end_write = $this->microtime_float();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;

        }
        $data = $QCheckIn->xuat_luoi($params);
        $params = array(
            'from_date' => $from_date,
            'to_date' => $to_date,
            'staff_id' => $auth->id,
        );
        $this->view->params = $params;
        $this->view->data = $data;
    }

    public function lateAction()
    {
        $time = $this->getRequest()->getParam('time', date('m').'/'.date('Y'));
        $export = $this->getRequest()->getParam('export');

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $params = array(
            'from' => $from,
            'to' => $to,
            'time' => $time,
        );

        $QCheckIn = new Application_Model_CheckIn();

        if(!empty($export))
        {
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            
            $heads = array(
                $alphaExcel->ShowAndUp() => 'STT',
                $alphaExcel->ShowAndUp() => 'Mã nhân viên',
                $alphaExcel->ShowAndUp() => 'Tên họ',
                $alphaExcel->ShowAndUp() => 'Phòng ban',
                $alphaExcel->ShowAndUp() => 'Quên chấm công',
                $alphaExcel->ShowAndUp() => 'Trễ từ 15 đến 30 phút',
                $alphaExcel->ShowAndUp() => 'Trễ trên 30 phút',
                $alphaExcel->ShowAndUp() => 'Tổng số ngày công phạt'
            );
            
            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.'1', $value);
            }
            $data = $QCheckIn->fetchLate($params);
            $row = 0;
            foreach($data as $key => $value)
            {
                $phat_15_30 = floor ($value['15_30']/3) * 0.5;
                $phat_30 = floor ($value['30']) * 0.5;
                $phat_quen_cham = floor ($value['quen_cham_cong']/2) * 0.5;
                $total = $phat_15_30 + $phat_30 + $phat_quen_cham;

                if(!empty($total))
                {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $key+1);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['staff_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['code']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['department_name']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['quen_cham_cong']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['15_30']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $value['30']);
                    
                    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($row+2), $total);
                    $row++;
                } 
            }

            $filename = 'Đi trễ - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
            $time_end_write = $this->microtime_float();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;

        }
        
        $data = $QCheckIn->fetchLate($params);

        $this->view->params = $params;
        $this->view->data = $data;
    }


    public function departmentAction()
    {
        $export = $this->getRequest()->getParam('export');
        $time = $this->getRequest()->getParam('time', date('m').'/'.date('Y'));

        $array_time = explode("/", $time);
        $month = $array_time[0];
        $year = $array_time[1];

        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $params = array(
            'from' => $from,
            'to' => $to,
            'time' => $time,
        );

        $QCheckIn = new Application_Model_CheckIn();
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if(!empty($export))
        {
            $data = $QCheckIn->getByDepartment($params);
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $alphaExcel = new My_AlphaExcel();
            $start_row = 1;

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            $sheet->getColumnDimension('B')->setWidth(25);
            $sheet->getColumnDimension('C')->setWidth(20);
            // header 1
            $heads = array(
                $alphaExcel->ShowAndUp() => 'STT',
                $alphaExcel->ShowAndUp() => 'Họ tên',
                $alphaExcel->ShowAndUp() => 'Code',
                $alphaExcel->ShowAndUp() => 'Chức vụ',
                $alphaExcel->ShowAndUp() => 'Khu vực'
            );
            
            foreach($heads as $key => $value)
            {
                $sheet->setCellValue($key.$start_row, $value);
            }
            $start_row++;

            // header 2
            $alphaExcel = new My_AlphaExcel();
            $heads_2 = array(
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
            );

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                $heads_2[$alphaExcel->ShowAndUp()] = $i;
            }

            foreach($heads_2 as $key => $value)
            {
                $sheet->setCellValue($key.$start_row, $value);
            }
            $start_row++;

            // header 3
            $alphaExcel = new My_AlphaExcel();
            $heads_3 = array(
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
                $alphaExcel->ShowAndUp() => '',
            );

            $array_sunday = array();

            $off_date = $QCheckIn->getDateInfo($params);

            for($i = 1; $i <= $number_day_of_month; $i++)
            {
                $day_of_week = date('D', strtotime($year . '-' . $month . '-' . $i));
                $heads_3[$alphaExcel->ShowAndUp()] = $day_of_week;
                if($day_of_week == 'Sun')
                {
                    $array_sunday[] = $i;
                }
            }

            foreach($off_date as $key => $value)
            {
                if($value['is_off'] > 0)
                {
                    $array_sunday[] = intval(date('d', strtotime($value['date'])));
                }
            }

            foreach($heads_3 as $key => $value)
            {
                $sheet->setCellValue($key.$start_row, $value);
            }
            $start_row++;
            $stt = 1;
            foreach($data as $key_department => $value_department)
            {
                $alphaExcel = new My_AlphaExcel();
                $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, '');
                $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_department['name']);
                $start_row++;
                foreach($value_department['list_staff'] as $key_staff => $value_staff)
                {
                    $alphaExcel = new My_AlphaExcel();
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $stt);
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_staff['name']);
                    $sheet->setCellValueExplicit($alphaExcel->ShowAndUp().$start_row, $value_staff['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_staff['title']);
                    $sheet->setCellValue($alphaExcel->ShowAndUp().$start_row, $value_staff['area_name']);
                    for($i = 1; $i <= $number_day_of_month; $i++)
                    {
                        $cell = $alphaExcel->ShowAndUp();
                        if(in_array($i, $array_sunday))
                        {
                            $sheet->getStyle($cell.$start_row)->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'FF0000')
                                    )
                                )
                            );
                        }

                        $sheet->setCellValue($cell.$start_row, number_format($value_staff['list_time'][$i], 2));
                    }
                    $start_row++;
                    $stt++;
                }
                
            }

            $filename = 'Công phòng ban - ' . date('Y-m-d H-i-s');
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
            
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }

        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->number_day_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }

    public function listStaffMachineAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $email = $this->getRequest()->getParam('email');
        $name = $this->getRequest()->getParam('name');
       
        $registry = $this->getRequest()->getParam('registry');
        $code = $this->getRequest()->getParam('code');
        $ip = $this->getRequest()->getParam('ip');
        $enroll_number = $this->getRequest()->getParam('enroll_number');
        
        $QCheckin = new Application_Model_CheckIn();
        if(!empty($registry))
        {
            $params_insert = array(
                'ip' => $ip,
                'staff_code' => $code,
                'enroll_number' => $enroll_number
            );

            $QCheckin->insertMachineStaff($params_insert);
        }

        $limit = 20;
        $offset = ($page - 1) * $limit;

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $list_info = $QCheckin->getListCodePermission($auth->code);
        if(!empty($list_info) || in_array($userStorage->group_id, array(HR_ID, 1)))
        {

        }
        else
        {
            $this->_redirect('/user/noauth');
        }

        $QCheckIn = new Application_Model_CheckIn();

        $params = array(
            'page' => $page,
            'code' => $code,
            'email' => $email,
            'name' => $name,
            'offset' => $offset,
            'limit' => $limit
        );

        $data = $QCheckIn->getStaffEnrollNumber($params);

        $this->view->data = $data['data'];
        $this->view->limit = $limit;
        $this->view->offset = $offset;
        $this->view->total = $data['total'];
        $this->view->url = HOST . 'time-machine/list-staff-machine' . ($params ? '?' . http_build_query($params) . '&' : '?');

    }

    protected function _curl($url= null, $pars = array()){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $pars);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0');
        $html = curl_exec($curl);
        curl_close($curl);
        return $html;
    }

    public function exportFile($month, $year, $staff_id)
    {
        
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $params = array(
            'month' => $month,
            'year' => $year,
            'staff_id' => $staff_id,
            'from_date' => $from,
            'to_date' => $to
        );
        
        $db = Zend_Registry::get('db');
        $sql = "CALL `detail_late`(:from_date, :to_date, :staff_id)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $from, PDO::PARAM_STR);
        $stmt->bindParam('to_date', $to, PDO::PARAM_STR);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_STR);
        $stmt->execute();
        
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
    
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'Mã nhân viên ',
            'Tên nhân viên',
            'Phòng ban',
            'Thứ',
            'Ngày',
            'Giờ vào',
            'Giờ ra'
        );
    
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
    
        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;
    
        foreach($data as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['staff_code']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['department_name']);
            $sheet->setCellValue($alpha++.$index, date('D', strtotime($item['day'])));
            $sheet->setCellValue($alpha++.$index, date('d/m/Y', strtotime($item['day'])));
            $sheet->setCellValue($alpha++.$index, date('H:i', strtotime($item['check_in_at'])));
            $sheet->setCellValue($alpha++.$index, date('H:i', strtotime($item['check_out_at'])));
            $index++;
        }
    
        $filename = 'Time_late_detail_'.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    
        $objWriter->save('php://output');
    
        exit;
    }

	
    public function exportAction(){
    
        $month = $this->getRequest()->getParam('month');
        $year = $this->getRequest()->getParam('year');
        $staff_id = $this->getRequest()->getParam('staff_id');
        
        $this->exportFile($month, $year, $staff_id);
        exit;
    }
    
    public function cancelDayLateAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $staff_id = $this->getRequest()->getParam('staff_id');
        $list_date = $this->getRequest()->getParam('list_date');
        $listTempTime = $this->getRequest()->getParam('listTempTime');
        $listTimeMachine = $this->getRequest()->getParam('listTimeMachine');
        
        $QTimeMachine = new Application_Model_TimeMachine();
        $month = $this->getRequest()->getParam('month_cancel',date('m'));
        $year = $this->getRequest()->getParam('year_cancel',date('Y'));
        $db = Zend_Registry::get('db');
        
        
        $str_insert = '';
        foreach($listTimeMachine as $key => $value){
            
            if(empty($value) || ($value == ''))
            {
                unset($listTimeMachine[$key]);
                
            }else{
                $str_insert .= "'". $value ."'" . ',';
            }
            
        }
        $str_time_machine = '';
        if(strlen($str_insert) > 1){
            $str_time_machine = rtrim($str_insert,',');
        }
        
        if(strlen($str_time_machine) > 1){
            $sql_time_machine = "UPDATE `time_machine`
            SET late_passby = 1
            WHERE staff_id = $staff_id
            AND check_in_day IN ($str_time_machine) ";
            $db->query($sql_time_machine);
            
            $sql_time_gps = "UPDATE `time_gps`
            SET late_passby = 1
            WHERE staff_id = $staff_id
            AND check_in_day IN ($str_time_machine) ";
            $db->query($sql_time_gps);
            
        }
       
        if(!empty($listTempTime)){
            $str_TempTime  = implode(',',$listTempTime);
            $sql1 = "UPDATE `temp_time`
            SET `passby` = 1
            WHERE staff_id = $staff_id
            AND `id` IN ($str_TempTime)" ;
            $db->query($sql1);
        }
           
        if(!empty($list_date)){
            
//             $str_list_date = implode(',',$list_date);
            
//             $sql = "UPDATE `time_machine`
//                     SET late_passby = 1
//                     WHERE staff_id = $staff_id 
//                     AND check_in_day IN ($str_list_date)
//                 ";
            
//             $sql1 = "UPDATE `temp_time`
//                     SET `passby` = 1
//                     WHERE staff_id = $staff_id 
//                     AND `date` IN ($str_list_date)" ;
            
//             $db = Zend_Registry::get('db');
//             $db->query($sql);
//             $db->query($sql1);
            
            
            echo json_encode (array(
                'status' => 1,
                'message' => "success",
				'month'	 => $month,
				'year'	 => $year
            ));
            exit();
        }else{
			  echo json_encode (array(
				'status' => 1,
				'message' => "Bạn chưa chọn ngày",
				'month'	 => $month,
					'year'	 => $year
				));
        exit();
		}
      
    }
    
    public function byPassLateAction() {
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'by-pass-late.php';    
    }
    
    public function hrLogAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'hr-log.php';
    }
    
    public function hrRemoveAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'hr-remove.php';
    }
    
    public function sendNotiAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $month = $this->getRequest()->getParam('month');
        $year  = $this->getRequest()->getParam('year');
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $db = Zend_Registry::get('db');

        $sql = "CALL `PR_Remind_By_Month`(:from_date, :to_date)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $from, PDO::PARAM_STR);
        $stmt->bindParam('to_date', $to, PDO::PARAM_STR);
        $stmt->execute();

        $data_noti = $stmt->fetchAll();
        $stmt->closeCursor();
        
        $QLogStaffWarmingDetail = new Application_Model_LogStaffWarningDetail();
        $QLogStaffWarning  = new Application_Model_LogStaffWarning();
        $where_warning_3 = array();
        $where_warning_3[] = $QLogStaffWarning->getAdapter()->quoteInto ('YEAR(penalty_day) = ?', $year );
        $where_warning_3[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) = ?', $month);
        $where_warning_3[] = $QLogStaffWarning->getAdapter()->quoteInto('warning_type = 3');
        $result_red = $QLogStaffWarning->fetchAll($where_warning_3);
        $list_warning_red = $list_warning_last = array();
        if(!empty($result_red)){
             foreach ($result_red as $k => $val){
                 $list_warning_red[] = $val['staff_id'];
             }
        }
//        $arr_warning_noti = array();
//        if(!empty($data_noti)){
//             foreach ($data_noti as $k => $val){
//                 if(!in_array($val['staff_id'], $list_warning_red)){
//                     $arr_warning_noti[] = $val['staff_id'];
//                 }
//                 
//             }
//           
//        }

        $arr_warning_noti = array(9388, 2128);

        //bỏ qua tháng 6
//        $penalty_day_passby = '2017-6-30';
//        $where   = array ();
//        $where[] = $QLogStaffWarning->getAdapter()->quoteInto ( 'YEAR(penalty_day) = ?', $year );
//        $where[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) < ?', $month);
//        $where[] = $QLogStaffWarning->getAdapter()->quoteInto('	warning_type <> 0');
//        $result_last  = $QLogStaffWarning->fetchAll ( $where );
//        
//        $list_warning_last = array();
//        
//        if(!empty($result_last)){
//             foreach ($result_last as $k => $val){
//                 $list_warning_last[] = $val['staff_id'];
//             }
//        }
//        $where_warning_2 = array();
//        $where_warning_2[] = $QLogStaffWarning->getAdapter()->quoteInto ('YEAR(penalty_day) = ?', $year );
//        $where_warning_2[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) = ?', $month);
//        $where_warning_2[] = $QLogStaffWarning->getAdapter()->quoteInto('warning_type = 2');
//        $result_noti = $QLogStaffWarning->fetchAll($where_warning_2);
        
//        $where_warning_first = array();
//        $where_warning_first[] = $QLogStaffWarning->getAdapter()->quoteInto ('YEAR(penalty_day) = ?', $year );
//        $where_warning_first[] = $QLogStaffWarning->getAdapter()->quoteInto('MONTH(penalty_day) = ?', $month);
//        $where_warning_first[] = $QLogStaffWarning->getAdapter()->quoteInto('warning_type = 1');
//        $result_warning_first  = $QLogStaffWarning->fetchAll($where_warning_first);
//        
//        $arr_warning_noti = array();
//        if(!empty($result_noti)){
//            foreach ($result_noti as $k => $val){
//                $arr_warning_noti[] = $val['staff_id'];
//            }
//        }
//        
//        if(!empty($result_warning_first)){
//            foreach ($result_warning_first as $k => $val){
//                $arr_warning_noti[] = $val['staff_id'];
//            }
//        }
        
        $date_time = date( 'Y-m-d H:i:s' );
        //Insert notification_new
        $data = array(
            'title'         => 'Thông báo vi phạm chuyên cần',
            'content'       => "<p> Bạn có tổng số giờ đi trễ ≤ 1h hoặc quên check in từ 01 đến 02 lần trong tháng $month </p>",
            'category_id'   => 1,
            'created_at'    => $date_time,
            'created_by'    => 1,
            'pop_up'        => 1,
            'status'        => 1,      
            'type'          => 1
        );
        
        $QModelNoti = new Application_Model_Notification();
        $QModelNotificationAccess = new Application_Model_NotificationAccess();
        
       $id_noti =  $QModelNoti->insert($data);
       
       foreach ($arr_warning_noti as $k => $val){
           $data_staff = array(
               'user_id'           => $val,
               'notification_id'   => $id_noti,
               'notification_status' => 0,
               'created_date'      => $date_time,
               'note'              => 'chuyen can '. $date_time
           );
           $QModelNotificationAccess->insert($data_staff);
       }
        
        echo json_encode(array(
                'status' => 1,
                'message' => "success"
            ));
        
    }
    
    public function sendEmailManagerAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'send-email-manager.php';
    }
    
    public function sendEmailAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $staff_id       = $this->getRequest()->getParam('staff_id');
        $code           = $this->getRequest()->getParam('staff_code');
        $month          = $this->getRequest()->getParam('month', intval(date('m')));
        $year           = $this->getRequest()->getParam('year', intval(date('Y')));
        $db = Zend_Registry::get('db');
        $sql = "SELECT st.code,s1.`email`, sp.* FROM staff st 
                INNER JOIN `staff_permission` sp ON sp.department_id = st.department AND ( sp.team_id IS NULL OR sp.team_id = st.team ) 
                 AND sp.`is_manager` = 1 
                INNER JOIN `staff` s1 ON sp.staff_code = s1.`code` 
                WHERE st.`code` = :code
                UNION ALL
                SELECT st.code,s1.`email`, sp.* FROM staff st 
                INNER JOIN `staff_permission` sp ON sp.department_id = st.department AND ( sp.team_id IS NULL OR sp.team_id = st.team ) 
                AND sp.`is_leader` = 1 AND (sp.office_id = st.office_id OR sp.is_all_office = 1)
                INNER JOIN `staff` s1 ON sp.staff_code = s1.`code`
                WHERE st.`code` = :code AND s1.`code` NOT IN ('15010021')
                ";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('code', $code, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt =  null;
            
             $ccStaff = array();
            foreach ($data as $staff){
//                $ccStaff[] = $staff['email'];
            }
            
//            $ccStaff[]  = 'vandiem.nguyen@oppomobile.vn';
            $ccStaff[]  = 'yennhi.pham@oppomobile.vn';
         
        $from = $year . '-' . $month . '-01';
        $to = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $QCheckIn = new Application_Model_CheckIn();
        $QLogSendMail = new Application_Model_LogSendMail();
        $params = array(
            'from_date' => $from,
            'to_date' => $to,
            'month' => $month,
            'year' => $year,
            'department' => NULL,
            'team' => NULL,
            'title' => NULL,
            'code'  => $code,
            'name'  => NULL,
            'area'  => NULL
        );
            

        $data = $QCheckIn->listLate($params);
        

         $content = $subject = $day_15_30 = $day_30 = $forget_check_in_day = '';
         $mailto = array();
           
         if(!empty($data[0])){
            $thoi_gian_1 = ($data[0]['total_late'] <= 180 && $data[0]['total_late'] > 60) ?  $data[0]['thoi_gian'] : ''  ;
            $phat_1 = ($data[0]['total_late'] <= 180 && $data[0]['total_late'] > 60) ?  0.5 . ' ngày' : ''  ;
            
            $thoi_gian_2     = ($data[0]['total_late'] > 180 ) ?  $data[0]['thoi_gian'] : ''  ;
            $thoi_gian_2_phat     = ($data[0]['total_late'] > 180 ) ? My_Controller_Action::rule_penalti($data[0]['total_late']) . ' ngày' : ''; 
            $forget_check_in_day = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 : ''; 
            $phat_forget_check_in_day = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 . ' ngày' : ''; 
            $ngay_phat = ($data[0]['ngay_phat'] > 0) ? $data[0]['ngay_phat'] . ' ngày' : ''  ;  
//                if($data[0]['ngay_phat'] > 0 && $data[0]['total_late'] > 180){
//                  $tong_phat  = $data[0]['ngay_phat'] . ' ngày + '.  $data[0]['thoi_gian'];
//                }elseif($data[0]['ngay_phat'] == 0 && $data[0]['total_late'] > 180){
//                    $tong_phat  =  $data[0]['thoi_gian'];
//                }elseif($data[0]['ngay_phat'] > 0){
//                    $tong_phat  = $data[0]['ngay_phat'] . ' ngày';
//                }else{
//                    $tong_phat  = 0;
//                }
                
                $forget_check_in = ($data[0]['forget_check_in'] >= 3) ? $data[0]['forget_check_in'] - 2 : 0;
                $late_less_3     = (My_Controller_Action::rule_penalti($data[0]['total_late']) <= 0.5) ? My_Controller_Action::rule_penalti($data[0]['total_late']) : 0; 
                $late_over_3     = (My_Controller_Action::rule_penalti($data[0]['total_late']) > 0.5) ? My_Controller_Action::rule_penalti($data[0]['total_late']) : 0; 
                $tong_phat       =  $forget_check_in + $late_less_3 + $late_over_3 . ' ngày';
              
//             $day_15_30 = $data[0]['15_30_day']; 
//             $phat_15_30 = ($data[0]['15_30_phat']); 
//             $phat_30 = ($data[0]['30_phat']); 
//             $phat_quen_check_in = ($data[0]['forget_phat']); 
//             $day_30 = $data[0]['30_day']; 

//             $songayphat = $data[0]['songayphat']; 
            $content_new = '<body lang="EN-US" style="margin: 0px; height: auto; overflow: visible; width: 878px;" class="MsgBody MsgBody-html">
                <div style=""><div style=""><style></style><div class="WordSection1" style="">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="984" style="width: 10.25in; border-collapse: collapse;">
                <tbody style=""><tr style="height: 15pt;">
                <td width="121" nowrap="" valign="bottom" style="width: 90.75pt; background: white; padding: 0in 5.4pt; height: 15pt;">
                <p class="MsoNormal" style="line-height: 115%;">
                <span style="font-family: &quot;times new roman&quot;, serif; color: black;">Chào bạn,</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="227" nowrap="" colspan="2" valign="bottom" style="width: 170.25pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="174" nowrap="" colspan="2" valign="bottom" style="width: 130.5pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>
                <td width="462" nowrap="" colspan="2" valign="bottom" style="width: 346.5pt; background: white; padding: 0in 5.4pt; height: 15pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td></tr><tr style="height: 15.75pt;">
                <td width="522" nowrap="" colspan="5" valign="bottom" style="width: 391.5pt; background: white; padding: 0in 5.4pt; height: 15.75pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">Phòng Nhân sự gửi bạn thông tin vi phạm chuyên cần tháng '.$month.'/2018</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="462" nowrap="" colspan="2" valign="bottom" style="width: 346.5pt; background: white; padding: 0in 5.4pt; height: 15.75pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td></tr><tr style="height: 24.75pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border: 1pt solid windowtext; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Nội dung</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="203" nowrap="" style="width: 152.25pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpFirst" align="center" style="margin-left: 0.25in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b style=""><span style="font-size: 9pt; line-height: 150%; color: black;">1h &lt;&nbsp;&nbsp;Tổng số thời gian đi trễ&nbsp; <u>&lt;</u>&nbsp; 3h</span></b></p></td>'
                    . '<td width="168" nowrap="" colspan="2" style="width: 1.75in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpMiddle" align="center" style="margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b style=""><span style="font-size: 9pt; line-height: 150%; color: black;">Tổng số thời gian đi trễ&nbsp; &gt;&nbsp; 3h </span></b></p></td>'
                    . '<td width="240" nowrap="" colspan="2" style="width: 2.5in; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: rgb(196, 215, 155); padding: 0in 5.4pt; height: 24.75pt;"><p class="MsoListParagraphCxSpLast" align="center" style="margin-left: 0in; margin-bottom: 0.0001pt; text-align: center; line-height: 150%;"><b style=""><span style="font-size: 9pt; line-height: 150%; color: black;">Quên check in từ lần thứ 3 trở đi </span></b></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 24.75pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border-top: none; border-left: 1pt solid windowtext; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Thời gian vi phạm</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="203" nowrap="" style=" text-align: center ; width: 152.25pt; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$thoi_gian_1.'</span></td>'
                    . '<td width="168" nowrap="" colspan="2" style=" text-align: center ; width: 1.75in; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$thoi_gian_2.'</span></td>'
                    . '<td width="240" nowrap="" colspan="2" style=" text-align: center ;  width: 2.5in; border-top: none; border-left: none; border-bottom: 1pt solid gray; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$forget_check_in_day.'</span></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border-top: none; border-left: 1pt solid windowtext; border-bottom: none; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Mức phạt</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="203" nowrap="" style="width: 152.25pt; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b style=""><i style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: rgb(89, 89, 89);"></span></i></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$phat_1.'</span></p></td>'
                    . '<td width="168" nowrap="" colspan="2" style="text-align: center ; width: 1.75in; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><span style=" text-align: center ; font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;">'.$thoi_gian_2_phat.'</span></td>'
                    . '<td width="240" nowrap="" colspan="2" style="width: 2.5in; border-top: none; border-left: none; border-bottom: 1pt solid rgb(166, 166, 166); border-right: 1pt solid windowtext; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><span style="font-size: 9pt;text-align: center; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: rgb(89, 89, 89);">'.$phat_forget_check_in_day.'</span></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 20.25pt;">'
                    . '<td width="121" nowrap="" style="width: 90.75pt; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" style="line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">&nbsp;</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="611" nowrap="" colspan="5" style="width: 458.25pt; border-top: 1pt solid rgb(166, 166, 166); border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid black; background: white; padding: 0in 5.4pt; height: 20.25pt;"><p class="MsoNormal" align="center" style="text-align: center; line-height: 115%;"><b style=""><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif; color: black;">Tổng mức phạt: '.$tong_phat.'</span></b><span style="font-size: 9pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 20.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="height: 32.25pt;">'
                    . '<td width="732" colspan="6" style="width: 549pt; background: white; padding: 0in 5.4pt; height: 32.25pt;"><p class="MsoNormal" style="line-height: 115%;"><span style="font-family: &quot;times new roman&quot;, serif; color: black;">Bạn vui lòng phản hồi nếu thông tin chưa chính xác hoặc cần điều chỉnh, <b style="">deadline phản hồi trước 11h ngày 07/01/2019</b> <br style="">Phòng Nhân sự sẽ căn cứ nội dung trong mail để thực hiện xử phạt theo đúng quy định Công ty.</span><span style="font-size: 12pt; line-height: 115%; font-family: &quot;times new roman&quot;, serif;"></span></p></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in; height: 32.25pt;"><p class="MsoNormal" style="">&nbsp;</p></td></tr><tr style="">'
                    . '<td width="121" style="width: 90.75pt; padding: 0in;"></td>'
                    . '<td width="203" style="width: 152.25pt; padding: 0in;"></td>'
                    . '<td width="24" style="width: 0.25in; padding: 0in;"></td>'
                    . '<td width="144" style="width: 1.5in; padding: 0in;"></td>'
                    . '<td width="30" style="width: 22.5pt; padding: 0in;"></td>'
                    . '<td width="210" style="width: 157.5pt; padding: 0in;"></td>'
                    . '<td width="252" style="width: 189pt; padding: 0in;"></td></tr></tbody></table><p class="MsoNormal" style=""><span style="color: rgb(31, 73, 125);">&nbsp;</span></p><div style=""><p class="MsoNormal" style=""><i style=""><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">Thanks &amp; Best Regards</span></i><span style="color: black;"></span></p><p class="MsoNormal" style=""><i style=""><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">&nbsp;</span></i><span style="color: black;"></span></p><table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;"><tbody style=""><tr style="height: 29.4pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 29.4pt;"><p class="MsoNormal" style=""><span style="font-family: arial, sans-serif; color: rgb(0, 146, 93);">Ms. PHẠM THỊ YẾN NHI<br style=""></span><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">Admin Specialist</span><span style="color: rgb(31, 73, 125);"></span></p></td></tr><tr style="height: 37.5pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 37.5pt;"><p class="MsoNormal" style=""><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">T: <span class="Object" role="link" id="OBJ_PREFIX_DWT68_com_zimbra_phone"><a href="callto:(84-8) 3920 2555" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()">(84-8) 3920 2555</a></span> - Ext: 0<br style="">E: <span class="Object" role="link" id="OBJ_PREFIX_DWT69_ZmEmailObjectHandler"><a href="mailto:yennhi.pham@oppomobile.vn" target="_blank" style="">yennhi.pham@oppomobile.vn</a></span><br style="">M: <span class="Object" role="link" id="OBJ_PREFIX_DWT70_com_zimbra_phone"><a href="callto:(84) 906 581 748" onclick="window.top.Com_Zimbra_Phone.unsetOnbeforeunload()">(84) 906 581 748</a></span></span><span style="color: rgb(31, 73, 125);"></span></p></td></tr><tr style="height: 38.9pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"><p class="MsoNormal" style=""><b style=""><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;">OPPO VIET NAM</span></b><span style="font-size: 10pt; font-family: arial, sans-serif; color: black;"><br style="">12<sup style="">th</sup><b style="">&nbsp;</b>Floor, Lim II Tower<br style="">No. 62A CMT8 St., Dist. 3, HCMC, VN</span><span style="color: rgb(31, 73, 125);"></span></p></td></tr><tr style="height: 16.5pt;"><td width="374" style="width: 3.9in; padding: 0in 5.4pt; height: 16.5pt;"><p class="MsoNormal" style=""><span style="color: rgb(31, 73, 125);"><span class="Object" role="link" id="OBJ_PREFIX_DWT71_com_zimbra_url"><a href="http://www.oppomobile.vn/" target="_blank" style=""><span style="font-size: 10pt; font-family: arial, sans-serif; color: rgb(0, 146, 93);">www.oppomobile.vn</span></a></span></span></p></td></tr></tbody></table><p class="MsoNormal" style=""><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div><p class="MsoNormal" style=""><span style="color: rgb(31, 73, 125);">&nbsp;</span></p></div></div></div></body>';
       
            $subject = 'THÔNG BÁO VI PHẠM CHUYÊN CẦN THÁNG '.$month.'/'.$year; 
        //    $mailto[] = 'nvdiemit@gmail.com';
           $mailto[] = $data[0]['email'];
 
//            echo "<pre>";
//            print_r($content_new);
//            echo "</pre>";
//            exit();
             $res = $this->sendmail($mailto, $subject, $content_new,NULL,$ccStaff);
             $userStorage = Zend_Auth::getInstance()->getStorage()->read();
              if($res == 1){
                  $data_send_mail = array(
                      'staff_id' => $staff_id,
                      'month'    => $month,
                      'year'     =>$year,
                      'created_at' => date('Y-m-d H:i:s'),
                      'created_by' => $userStorage->id
                  );
                  $QLogSendMail->insert($data_send_mail);
                $reponse['status']=1;
                $reponse['message']="Thành công";
            }else{
                $reponse['status']=$res;
                $reponse['message']="Lỗi";
            }
            
            echo json_encode($reponse);
         }
      
    }
    
    private function sendmail($to, $subject, $maildata, $image = '',$ccStaff){
        include_once APPLICATION_PATH.'/../library/phpmail/class.phpmailer.php';
        $app_config   = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config       = $app_config->mail->smtp;

        $mailfrom     = 'yennhi.pham@oppomobile.vn';
    // $mailfrom     = $config->user2;
        $mailfromname = $mailfrom;
        $mail = new PHPMailer();

        $body = $maildata; // nội dung email
        $body = eregi_replace("[\]",'',$body);

        $mail->IsHTML();

        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = $config->ssl;                 // sets the prefix to the servier // Config this
        $mail->Host       = $config->host;      // sets GMAIL as the SMTP server // Config this
        $mail->Port       = $config->port;                   // set the SMTP port for the GMAIL server // Config this
       // $mail->Username   = $config->user2;  // GMAIL username // Config this
        $mail->Username   = 'yennhi.pham@oppomobile.vn';  // GMAIL username // Config this
     //   $mail->Password   = $config->pass2;            // GMAIL password // Config this
        $mail->Password   = '0922647598';            // GMAIL password // Config this
        
        $mail->From = $mailfrom;
        $mail->FromName = $mailfromname;

        $mail->SetFrom($mailfrom, $mailfromname); //Định danh người gửi

        //$mail->AddReplyTo($mailfrom, $mailfromname); //Định danh người sẽ nhận trả lời

        $mail->Subject    = $subject; //Tiêu đề Mail

        $mail->AltBody    = "Để xem tin này, vui lòng bật chế độ hiển thị mã HTML!"; // optional, comment out and test

        // $mail->AddAttachment($image);
        if($image != '')
            $mail->AddEmbeddedImage($image, 'embed_img');

        $mail->MsgHTML($body);

        if (is_array($to)) {
            foreach ($to as $key => $value) {
                $mail->AddAddress($value, ''); //Gửi tới ai ?
            }
        } else {
            $mail->AddAddress($to, ''); //Gửi tới ai ?
        }
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QStaff = new Application_Model_Staff();
        $select_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $infoStaff = $QStaff->fetchRow($select_staff);
        $emailInfo      = $infoStaff->email;
//        $mail->addBcc($emailInfo);
        
        foreach ($ccStaff as $k => $emailStaff) {
              $mail->addCc($emailStaff);
        }
       
        if(!$mail->Send() ) {
//             echo date('Y-m-d H:i:s')." - Failed\n";
            return -1;
        } else {
            //echo date('Y-m-d H:i:s')." - Done\n"; //DEBUG
            return 1;
        }

    }
    
    public function getManagerAction(){
        require_once 'time-machine' . DIRECTORY_SEPARATOR . 'get-manager.php';
    }
    
    
}
