<?php
class ContractController extends My_Controller_Action
{

	public function indexAction(){
        include 'contract'.DIRECTORY_SEPARATOR.'index.php';
	}

	public function staffPreviewAction(){
        include 'contract'.DIRECTORY_SEPARATOR.'staff-preview.php';
	}

    public function editcontractAction()
    {
        $id = $this->getRequest()->getParam('id');

        if($this->getRequest()->isPost())
        {
            $status = $this->getRequest()->getPost('status');
            $print_status = $this->getRequest()->getPost('print_status');
            $print_type = $this->getRequest()->getPost('print_type');
            $new_contract = $this->getRequest()->getPost('new_contract');
            $from_date = $this->getRequest()->getPost('from_date');
            $to_date = $this->getRequest()->getPost('to_date');
            $send_letter = $this->getRequest()->getPost('send_letter');
            $return_letter = $this->getRequest()->getPost('return_letter');
            $salary = $this->getRequest()->getPost('salary');
            $total_salary = $this->getRequest()->getPost('total_salary');
            $allowance_1 = $this->getRequest()->getPost('allowance_1');
            $allowance_2 = $this->getRequest()->getPost('allowance_2');
            $allowance_3 = $this->getRequest()->getPost('allowance_3');
            $note = $this->getRequest()->getPost('note');

            $QAsmContract = new Application_Model_AsmContract();
            $params = array(
                'id' => $id,
                'status' => $status,
                'print_status' => $print_status,
                'print_type' => $print_type,
                'new_contract' => $new_contract,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'send_letter' => $send_letter,
                'return_letter' => $return_letter,
                'salary' => $salary,
                'total_salary' => $total_salary,
                'allowance_1' => $allowance_1,
                'allowance_2' => $allowance_2,
                'allowance_3' => $allowance_3,
                'note' => $note,
            );
            $res = $QAsmContract->updateAsmContract($params);
            $this->view->message = $res;
        }

        $db = Zend_Registry::get('db');
        $cols = array(
                'p.*',
            );

        $select = $db->select()
            ->from(array('p'=>'asm_contract'),$cols)
            ->where('p.id = ?',$id)
            ->limit('1')
            ;
        $asm_contract = $db->fetchRow($select);
            if($asm_contract){
            $this->view->asm_contract = $asm_contract;
        }

        $QContractTerm = new Application_Model_ContractTerm();
        $contractTerms = $QContractTerm->get_cache();
        $this->view->contractTerms = $contractTerms;

    }

	public function editStaffPreviewAction(){
        $userStorage             = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->userStorage = $userStorage;
        $id = $this->getRequest()->getParam('id');
        $db = Zend_Registry::get('db');
        $cols = array(
                'p.*',
                'province_id' => 'r.province_id',
                'staff_name'  => 'CONCAT(s.firstname," ",s.lastname)',
                'staff_code'  => 's.code'
            );

        $select = $db->select()
			->from(array('p'=>'asm_contract'),$cols)
            ->join(array('s'=>'staff'),'p.staff_id = s.id',array())
            ->joinLeft(array('r'=>'regional_market'),'r.id = s.regional_market',array())
            ->where('p.id = ?',$id)
            ;
        $asm_contract = $db->fetchRow($select);

        if($asm_contract){
			$this->view->asm_contract = $asm_contract;
			$QHospital                = new Application_Model_Hospital();
			$hospitals                = $QHospital->get_all(array('province_id'=>$asm_contract['province_id']));
			$this->view->hospitals    = $hospitals;
        }
        
        $from_date             = date('d/m/Y');
        $to_date               = date('d/m/Y');
        $this->view->from_date = $from_date;
        $this->view->to_date   = $to_date;

        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all();
        $this->view->provinces = $provinces;

		$QProposal             = new Application_Model_AsmProposal();
		$proposals             = $QProposal->get_all();
		$this->view->proposals = $proposals;

        $this->_helper->layout()->disableLayout(true);
	}

	public function saveProposalAction(){

        $id             = $this->getRequest()->getParam('id');
        $hospital_id    = $this->getRequest()->getParam('hospital_id',0);
        $proposal_id    = $this->getRequest()->getParam('proposal_id',0);
        $note           = $this->getRequest()->getParam('note',NULL);
        $return_letter  = $this->getRequest()->getParam('return_letter',NULL);
        $new_salary     = $this->getRequest()->getParam('new_salary',0);

        $new_salary     = intval($new_salary);
        $QStaff         = new Application_Model_Staff();
        $QAsmContract   = new Application_Model_AsmContract();
        $db             = Zend_Registry::get('db');
        $flashMessenger = $this->_helper->flashMessenger;
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        
        $return_letter  = ($return_letter) ? My_Date::normal_to_mysql($return_letter) : NULL;

        $data = array(
                'asm_proposal'  => $proposal_id,
                'note'          => $note,
            );

        if(in_array($userStorage->group_id, array(HR_ID,ADMINISTRATOR_ID))):
            $data['new_salary'] = $new_salary;        
        endif;

        if(intval($hospital_id)){
            $data['hospital_id'] = $hospital_id;
        }
        
        $db->beginTransaction();
        try {

            $select = $db->select()
                ->from(array('p'=>'asm_contract'),array('p.*'))
                ->where('p.id = ?',$id)
                ->where('p.status IN (0,1) OR p.status IS NULL');
            $r = $db->fetchRow($select);
            if($r){

                $where   = array();
                $where[] = $QAsmContract->getAdapter()->quoteInto('id = ?',$id);
                $QAsmContract->update($data,$where);   
                $select2 = $db->select()
                    ->from(array('a'=>'asm_contract'),array(
                        'hospital_name' =>'c.name',
                        'proposal'     =>'b.name',
                        'a.note',
                        'new_salary'))
                    ->join(array('b'=>'asm_proposal'),'a.asm_proposal = b.id',array())
                    ->joinLeft(array('c'=>'hospital'),'c.id = a.hospital_id',array())
                    ->where('a.id = ?',$id);
                $rData = $db->fetchRow($select2);
                $result = array('code'=>1,'messages'=>$rData);
                $db->commit();    
            }else{
                $result = array('code'=>-1,'messages'=>'Không thể cập nhật sau khi HR xác nhận!');    
            }
        } catch (Exception $e) {
            $db->rollBack();
            $result = array('code' => -1,'messages' => $e->getMessage());
        }

        $this->_helper->json->sendJson($result);
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        
    }	

    public function _confirm($params = NULL){

        $QAsmContract      = new Application_Model_AsmContract();
        $flashMessenger    = $this->_helper->flashMessenger;
        $db                = Zend_Registry::get('db');
        $userStorage       = Zend_Auth::getInstance()->getStorage()->read();
        $month             = $params['month'];
        $ids               = $params['ids'];
        $back_url          = $params['back_url'];
        $area_id_confirm   = $params['area_id_confirm'];
        $confirm           = $params['confirm'];
        $btnApproveByStaff = $params['btnApproveByStaff'];
        $btnConfirm        = $params['btnConfirm'];

        if(in_array( $userStorage->group_id, array(ASM_ID,ASMSTANDBY_ID))){
            if(count($area_id_confirm) == 0){
                $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn khu vực xác nhận');
            $this->_redirect($back_url);    
            }
            
            $ids = array();//reset nếu là asm
        }

        $month          = ($month) ? My_Date::normal_to_mysql($month) : NULL;
        if($month){
            $start   = date('Y-m-01',strtotime($month));
            $end_tmp = date('Y-m-01',strtotime($month));
            $date    = new DateTime($end_tmp);
            $date->add(new DateInterval('P1M'));
            $end = $date->format('Y-m-05');
        }

        // confirm theo tháng
        if($btnConfirm AND $confirm){
            if($month){
                try {
                    $store_params = array($userStorage->id,$start,$end,$area_id_confirm,$id=NULL);
                    // print_r($store_params); die;
                    $code = $QAsmContract->confirmContract($store_params);
                    if($code < 0){
                        $mess = '';
                        if($code == -1){
                            $mess = 'ASM da confirm roi';
                        }
                        elseif($code == -2){
                            $mess = 'ASM chua nhap benh vien';
                        }
                        elseif($code == -3){
                            $mess = 'ASM chua confirm xong(con nguoi chua duoc confirm)';
                        }
                        elseif($code == -4){
                            $mess = 'Hospital_id is null';
                        }
                        elseif ($code == -6) {
                            $mess = 'Vui lòng nhập lương';
                        }
                        elseif ($code == -7) {
                            $mess = 'Khu vực chưa xác nhận đề xuất xong';
                        }
                        else{
                            $mess = 'Lỗi';
                        }
                        $flashMessenger->setNamespace('error')->addMessage($mess);
                    }else{
                        $flashMessenger->setNamespace('success')->addMessage('Done');
                        $this->_redirect($back_url);
                    }
                } catch (Exception $e){
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $this->_redirect($back_url);
                }
            }
        }

        // confirm theo từng nhân viên
        if($btnApproveByStaff AND count($ids) == 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn nhân viên để approve');
            $this->_redirect($back_url);
        }

        if($btnApproveByStaff AND count($ids) > 0){

            $select = $db->select()
                    ->from(array('a'=>'asm_contract'),array('a.*'))
                    ->where('a.id IN (?)',$ids)
                    ->where('a.status IN (?)',array(0,1))
                    ->where('a.print_type = ?',1);
            $result = $db->fetchAll($select);

            foreach($result as $key => $value){
                $store_params = array(
                    $userStorage->id,
                    date('Y-m-01',strtotime($value['to_date'])),
                    date('Y-m-t',strtotime($value['to_date'])),
                    NULL,
                    $value['id']
                );

                $code = $QAsmContract->confirmContract($store_params);
                if($code < 0){
                    $mess = '';
                    if($code == -1){
                        $mess = 'ASM da confirm roi';
                    }
                    elseif($code == -2){
                        $mess = 'ASM chua nhap benh vien';
                    }
                    elseif($code == -3){
                        $mess = 'ASM chua confirm xong(con nguoi chua duoc confirm)';
                    }
                    elseif($code == -4){
                        $mess = 'Hospital_id is null';
                    }
                    elseif ($code == -6) {
                        $mess = 'Vui lòng nhập lương';
                    }
                    elseif ($code == -7) {
                        $mess = 'Khu vực chưa xác nhận đề xuất xong';
                    }
                    else{
                        $mess = 'Lỗi';
                    }
                    $flashMessenger->setNamespace('error')->addMessage($mess);
                    $this->_redirect($back_url);
                }
            }
            $flashMessenger->setNamespace('success')->addMessage('Done');

        }
        $this->_redirect($back_url);
               
    }

    public function _rollback($params){
        $back_url = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract/staff-preview';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();

        if(!count($params['area_id'])){
            $flashMessenger->setNamespace('error')->addMessage('Chọn khu vực để rollback');
            $this->_redirect($back_url);
        }

        if(!$params['expired_from'] OR !$params['expired_to']){
            $flashMessenger->setNamespace('error')->addMessage('Chọn thời gian rollback');
            $this->_redirect($back_url);
        }

        $expired_from = My_Date::normal_to_mysql($params['expired_from']);
        $expired_to   = My_Date::normal_to_mysql($params['expired_to']);

        //chỉ cho phép HR AND ADMIN thực hiện :D
        if(in_array($userStorage->group_id,array(HR_ID))  ){
            $db->beginTransaction();
            try {
                $str_area_id = implode(',',$params['area_id']);
                $sql = '
                    UPDATE asm_contract a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    INNER JOIN area c ON c.id = b.area_id
                    INNER JOIN contract_term d ON a.new_contract = d.id
                        SET a.status = 0
                    WHERE a.to_date BETWEEN "'.$expired_from.'" AND "'.$expired_to.'"
                        AND a.`status` != 2
                        AND CONCAT(",",c.id,",") LIKE CONCAT("%,",'.$str_area_id.',",%")
                        AND  a.title IN (182,183,190,162,164,293,312)
                ';
                $stmt = $db->query($sql);
                $stmt->execute();
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Done');
                
            } catch (Exception $e){
                $db->rollBack();
                $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                $this->_redirect($back_url);
            }
        }
        $this->_redirect($back_url);
    }

    public function _refresh($params){
        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract/';
        $ids            = $params['ids'];
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $QAsmContract   = new Application_Model_AsmContract();
        $db->beginTransaction();
        try {
            if(is_array($ids) and count($ids) > 0 ){
                foreach($ids as $id){
                    $QAsmContract->refresh($id);
                }
            }
            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function printNewAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();

        $back_url             = $this->getRequest()->getParam('back_url');
        
        $ids                  = $this->getRequest()->getParam('id');
        $print_status         = $this->getRequest()->getParam('print_status',0);

        $QArea                     = new Application_Model_Area();
        $QContract                 = new Application_Model_ContractTerm();
        $QAsmContract              = new Application_Model_AsmContract();
        $contract_name             = $QContract->get_cache();
        
        $this->view->contract_name = $contract_name;
        $this->view->areas         = $QArea->fetchAll();
        $this->view->back_url      = $back_url;
        $staffs                    = array();

        $db = Zend_Registry::get('db');

        if (is_array($ids) && $ids)
        {   
            $str        = implode(',', $ids);
            $stmt = $db->query('CALL p_contract_new_print(?)',array($str));
            $staffs     = $stmt->fetchAll();
            $stmt->closeCursor();

            $where      = array();
            $where[]    = $QAsmContract->getAdapter()->quoteInto('id IN (?)',$ids);
            $where[]    = $QAsmContract->getAdapter()->quoteInto('print_status = ?',0);
            $where[]    = $QAsmContract->getAdapter()->quoteInto('print_type = ?',1);

            $dataUpdate = array(
                'send_letter' => date('Y-m-d'),
                'print_status' => 1
            );

            if($print_status == 1){
                $db->beginTransaction();
                try {
                    $QAsmContract->update($dataUpdate,$where);
                    $db->commit();
                } catch (Exception $e) {
                    $db->rollback();
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $this->_redirect("/contract/staff-preview");
                }
            }
        }
        $this->view->staff = $staffs;
        
        $QGroup = new Application_Model_Group();
        $this->view->groups = $QGroup->fetchAll();

        $QModel = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $this->view->regional_markets = $QRegionalMarket->get_cache();

        $QModel = new Application_Model_Team();
        $this->view->teams = $QModel->fetchAll();

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

    }

    public function printAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $this->_helper->layout->disableLayout();

        $back_url             = $this->getRequest()->getParam('back_url');
        
        $ids                  = $this->getRequest()->getParam('id');
        $print_status         = $this->getRequest()->getParam('print_status',0);

        $QArea                     = new Application_Model_Area();
        $QContract                 = new Application_Model_ContractTerm();
        $QAsmContract              = new Application_Model_AsmContract();
        $contract_name             = $QContract->get_cache();
        
        $this->view->contract_name = $contract_name;
        $this->view->areas         = $QArea->fetchAll();
        $this->view->back_url      = $back_url;
        $staffs                    = array();

        $db = Zend_Registry::get('db');

        if (is_array($ids) && $ids)
        {   
            $str        = implode(',', $ids);
            $stmt = $db->query('CALL p_contract_print(?)',array($str));
            $staffs     = $stmt->fetchAll();
            $stmt->closeCursor();

            $where      = array();
            $where[]    = $QAsmContract->getAdapter()->quoteInto('id IN (?)',$ids);
            $where[]    = $QAsmContract->getAdapter()->quoteInto('print_status = ?',0);
            $where[]    = $QAsmContract->getAdapter()->quoteInto('print_type = ?',1);

            $dataUpdate = array(
                'send_letter' => date('Y-m-d'),
                'print_status' => 1
            );

            if($print_status == 1){
                $db->beginTransaction();
                try {
                    $QAsmContract->update($dataUpdate,$where);
                    $db->commit();
                } catch (Exception $e) {
                    $db->rollback();
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $this->_redirect("/contract/staff-preview");
                }
            }
        }
        $this->view->staff = $staffs;
        
        $QGroup = new Application_Model_Group();
        $this->view->groups = $QGroup->fetchAll();

        $QModel = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $this->view->regional_markets = $QRegionalMarket->get_cache();

        $QModel = new Application_Model_Team();
        $this->view->teams = $QModel->fetchAll();

        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;

    }

    

    public function _export($list){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'COMPANY',
            'DEPARTMENT',
            'TEAM',
            'CODE',
            'LASTNAME',
            'NAME',
            'DATE OF BIRTH',
            'BIRTH PLACE',
            'ID CARD ADDRESS',
            'ID NUMBER',
            'ID DATE',
            'ID PLACE',
            'CONTRACT SIGNED AT',
            'CONTRACT TERM',
            'CONTRACT EXPIRED',
            'TITLE',
            "WORK'S LOCALTION"
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        $arrId = array();
        foreach($list as $key => $value):
            $arrId[] = $value['current_id'];
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xcompany']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xdepartment']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xteam']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xcode']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xfirstname']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xlastname']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xdob']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xbirth_place']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xid_address']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xID_number']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xID_date']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xID_Place_name']) , PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xfrom_date']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xcontract_term']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xto_date']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xtitle']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xoffice_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;


        $userStorage             = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id                = $userStorage->id;
        $db = Zend_Registry::get('db');
        if(in_array($userStorage->group_id, array(HR_ID,ADMINISTRATOR_ID))){
            if(count($arrId) > 0):
                $str = implode($arrId, ',');
                $sql = '
                    UPDATE asm_contract a
                    INNER JOIN staff b ON a.staff_id = b.id AND b.title NOT IN (182,183,190,162,163,164)
                    SET a.send_letter = ?
                    WHERE a.id IN ('.$str.')
                    AND (a.send_letter IS NULL OR a.send_letter != "0000-00-00")
                ';
                $flashMessenger = $this->_helper->flashMessenger;
                $db->beginTransaction();
                try {
                    $stmt = $db->query($sql,array(date('Y-m-d')));
                    $stmt->execute();
                    $db->commit();
                } catch (Exception $e) {
                    $db->rollback();
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $this->_redirect('/contract/staff-preview');
                }
            endif;
        }

        $filename = 'staff_contract_' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function _exportInfo($list){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        $QContractTerm = new Application_Model_ContractTerm();
        $contractTerms = $QContractTerm->get_cache();

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'BỘ PHẬN',
            'MÃ NHÂN VIÊN',
            'HỌ TÊN',
            'CHỨC VỤ',
            'KHU VỰC',
            'TỈNH',
            'HĐ CŨ',
            'ĐỀ XUẤT',
            'TỪ NGÀY',
            'ĐÉN NGÀY',
            'THỜI HẠN',
            'LƯƠNG',
            'BỆNH VIỆN',
            'ĐỀ XUẤT CỦA KHU VỰC',
            'GHI CHÚ',

        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        $stt = 0;
        //echo "<pre>";print_r($list);die;
        foreach($list as $key => $value):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xdepartment']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['staff_code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['firstname'] .' '.$value['lastname']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['xtitle']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['regional_market']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['contract_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( ($value['contract_level'] == 1) ? 'KÝ HĐ CHÍNH THỨC':'TÁI KÝ HĐ', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( ($value['new_contract_from'])? date('d/m/Y',strtotime($value['new_contract_from'])):'', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( ($value['new_contract_to']) ? date('d/m/Y',strtotime($value['new_contract_to'])):'' , PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( $contractTerms[$value['new_contract']], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['salary_new'] ? $value['salary_new'] : $value['salary']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['TenBV']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['dexuat']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['ghichu']), PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;
        $filename = 'staff_contract_' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function areaApproveAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $month = $this->getRequest()->getParam('month',date('m/Y'));
        if(!$month){
            $flashMessenger->setNamespace('error')->addMessage('Please select month');
            $this->_redirect('/contract/area-approve');
        }
        $params = array(
            'month' => $month,
        );

        $month   = My_Date::normal_to_mysql($month);
        $start   = date('Y-m-01',strtotime($month));
        $end     = date('Y-m-t',strtotime($month));
        $end     = My_Date::date_add($end,1,'day');
        $end     = date('Y-m-05',strtotime($end));
    
        $sql = 'SELECT c.name, CASE WHEN  MIN(a.status) > 0 AND MIN(IFNULL(a.asm_proposal,0)) > 0 THEN 1 ELSE 0 END as "status"
                FROM asm_contract a
                INNER JOIN staff d ON d.id = a.staff_id AND d.status = 1
                INNER JOIN regional_market b ON d.regional_market = b.id AND a.title IN (182,183,190,162,164,293,295)
                INNER JOIN area c ON c.id = b.area_id
                WHERE (a.to_date BETWEEN "'.$start.'" AND "'.$end.'" OR a.to_date <= "'.$start.'") 
                        AND (a.status <= 1 AND a.locked = 0) AND a.print_type = 1
                GROUP BY b.area_id
                ORDER BY CASE WHEN  MIN(a.status) > 0 AND MIN(a.asm_proposal) > 0 THEN 1 ELSE 0 END';
        PC::debug($sql);
        $db = Zend_Registry::get('db');
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll();
        $this->view->list = $list;
        $this->view->params = $params;
        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_error = $messages_error;
        $this->view->mesages_success = $messages_success;
    }

    public function setTimeReturnLetter($params){
        $back_url = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract/staff-preview';
        $flashMessenger = $this->_helper->flashMessenger;
        $db = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if($this->getRequest()->isPost()){
            if(in_array($userStorage->group_id,array(HR_ID,ADMINISTRATOR_ID))){
                $db->beginTransaction();
                try{
                    $ids = $params['ids'];
                    $date = My_Date::normal_to_mysql($params['set_date']);
                    $data = array('return_letter'=> $date);
                    $QAsmContract = new Application_Model_AsmContract();
                    $where = $QAsmContract->getAdapter()->quoteInto('id IN (?)',$ids);
                    $QAsmContract->update($data,$where);
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                }catch(Exception $e){
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $db->rollBack();
                }
            }
            $this->_redirect($back_url);
        }
    }

    public function _setPrintStatus($params){
        $back_url = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/contract/staff-preview';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $ids = $params['ids'];
            $userStorage             = Zend_Auth::getInstance()->getStorage()->read();
            $db = Zend_Registry::get('db');
            if(in_array($userStorage->group_id, array(HR_ID,ADMINISTRATOR_ID))){
                if(count($ids) > 0){
                    $str = implode(',',$ids);
                    $sql = '
                        UPDATE asm_contract a
                        SET a.send_letter = ?,
                            a.print_status = 1
                        WHERE a.id IN ('.$str.') AND  a.title NOT IN (182,183,190,162,163,164)
                        AND (a.print_status IS NULL OR a.print_status = 0)
                    ';
                    $stmt = $db->query($sql,array(date('Y-m-d')));
                    $stmt->execute();
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                }else{
                    $flashMessenger->setNamespace('error')->addMessage('Please select item');
                }
            }else{
                $flashMessenger->setNamespace('error')->addMessage('Permission failed');
            }
        } catch (Exception $e) {
            $db->rollback();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function printAppendixAction(){
        $this->_helper->layout->disableLayout();
        $back_url             = $this->getRequest()->getParam('back_url');
        $this->view->back_url = $back_url;
        $flashMessenger       = $this->_helper->flashMessenger;
        $ids                  = $this->getRequest()->getParam('id');
        $contract_type        = $this->getRequest()->getParam('contract_type');
        $print_status         = $this->getRequest()->getParam('print_status',0);

        $QArea           = new Application_Model_Area();
        $QSalarySales    = new Application_Model_SalarySales();
        $QSalaryPG       = new Application_Model_SalaryPg();
        $QLog            = new Application_Model_StaffPrintLog();
        $QContract       = new Application_Model_ContractTerm();
        $QStaffAddress   = new Application_Model_StaffAddress();
        $QRegionalMarket = new Application_Model_RegionalMarket();
        $QAsmContract    = new Application_Model_AsmContract();

        $areas            = $QArea->get_cache();
        $regional_markets = $QRegionalMarket->get_cache_all();
        $contract_name    = $QContract->get_cache();

        $this->view->areas = $QArea->fetchAll();
        $staffs = array();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db = Zend_Registry::get('db');

        $sql = "
            SELECT
                CONCAT(b.firstname,' ',b.lastname) as 'staff_name',
                firstname,
                lastname,
                b.dob,
                b.code,
                d.birth_place_province as 'birth_place',
                CONCAT( IF(c.permanent_address IS NOT NULL, CONCAT(c.permanent_address,', '), '') ,
                        IF(c.permanent_ward IS NOT NULL, CONCAT(c.permanent_ward,', '), '') , c.permanent_province ) as permanent,
                b.ID_number,
                b.ID_date as 'ID_date',
                a.title,
                e.`name` as ID_place,
                a.from_date as 'transfer_from',
                a.from_date as 'contract_signed_at',
                a.to_date as 'contract_expired_at',
                a.new_contract as 'contract_term',
                a.company_id,
                a.regional_market,
                CASE WHEN f.base_salary IS NOT NULL THEN f.base_salary 
                    WHEN g.base_salary IS NOT NULL THEN g.base_salary 
                    WHEN l.base_salary IS NOT NULL THEN l.base_salary
                    ELSE 0 END as 'base_salary',
                CASE WHEN a.total_salary IS NOT NULL AND a.total_salary > 0 THEN IF(a.total_salary > a.salary,a.total_salary,a.salary) 
                    WHEN f.base_salary IS NOT NULL THEN f.bonus_salary 
                    WHEN g.base_salary IS NOT NULL THEN g.bonus_salary 
                    WHEN l.base_salary IS NOT NULL THEN l.bonus_salary 
                    ELSE 0 END as 'bonus_salary',
                CASE WHEN f.base_salary IS NOT NULL THEN f.allowance_1 
                    WHEN g.base_salary IS NOT NULL THEN g.allowance_1  
                    WHEN l.base_salary IS NOT NULL THEN l.allowance_1 
                    ELSE 0 END as 'allowance_1',
                CASE 
                    WHEN f.base_salary IS NOT NULL THEN f.allowance_2 
                    WHEN g.base_salary IS NOT NULL THEN g.allowance_2 
                    WHEN l.base_salary IS NOT NULL THEN l.allowance_2 
                    ELSE 0 END as 'allowance_2',
                CASE WHEN f.base_salary IS NOT NULL THEN f.allowance_3 
                    WHEN g.base_salary IS NOT NULL THEN g.allowance_3 
                    WHEN l.base_salary IS NOT NULL THEN l.allowance_3 
                    ELSE 0 END as 'allowance_3',
                CASE WHEN a.kpi_text IS NOT NULL AND a.kpi_text != '' THEN a.kpi_text 
                    WHEN f.base_salary IS NOT NULL AND a.title IN (182,293) AND a.new_contract = 3 THEN f.kpi_1     
                    WHEN g.base_salary IS NOT NULL THEN g.kpi 
                    WHEN l.base_salary IS NOT NULL THEN l.kpi 
                    ELSE 0 END as 'kpi',
                CASE WHEN a.kpi_text IS NOT NULL AND a.kpi_text != '' THEN a.kpi_text 
                    WHEN f.base_salary IS NOT NULL THEN f.kpi_1 
                    WHEN g.base_salary IS NOT NULL THEN 0 
                    ELSE 0 END as 'kpi_1',
                CASE WHEN f.base_salary IS NOT NULL THEN f.probation_salary 
                    WHEN g.base_salary IS NOT NULL THEN g.probation_salary 
                    WHEN l.base_salary IS NOT NULL THEN l.probation_salary 
                    ELSE 0 END as 'probation_salary',
                (h.level - 1) as 'print_time',
                a.print_type as 'print_type',
                k.from_date as 'original_date'
            FROM asm_contract a
            INNER JOIN staff b ON a.staff_id = b.id
            LEFT JOIN (
                SELECT
                    a.staff_id,
                    a.address as 'permanent_address',
                    a.ward as 'permanent_ward',
                    d.`name` as 'permanent_province'
                FROM staff_address a
                LEFT JOIN regional_market b ON a.district = b.id
                LEFT JOIN regional_market c ON b.parent = c.id
                LEFT JOIN province d ON c.province_id = d.id
                WHERE a.address_type = 3
                GROUP BY a.staff_id
            ) AS c ON c.staff_id = a.staff_id
            LEFT JOIN (
                SELECT
                    a.staff_id,
                    d.`name` as 'birth_place_province'
                FROM staff_address a
                LEFT JOIN regional_market b ON a.district = b.id
                LEFT JOIN regional_market c ON b.parent = c.id
                LEFT JOIN province d ON c.province_id = d.id
                WHERE a.address_type = 1
                GROUP BY a.staff_id
            ) AS d ON d.staff_id = a.staff_id
            LEFT JOIN province e ON e.id = b.id_place_province
            LEFT JOIN salary_pg f ON f.province_id = a.regional_market AND a.title IN (182,293)
            LEFT JOIN salary_sales g ON g.province_id = a.regional_market AND a.title IN (183,190,162,164)
            LEFT JOIN contract_term h ON h.id = b.contract_term
            INNER JOIN asm_contract k ON k.id = a.parent_id
            LEFT JOIN salary_log l ON l.`province_id` = a.`regional_market` AND l.`type` = 3 AND a.title = 312
            WHERE CONCAT(',',10579,',') LIKE CONCAT('%,',a.id,',%')
            AND a.print_type = 2
        ";


        if (is_array($ids) && $ids)
        {
            $str        = implode(',', $ids);
            //$stmt       = $db->query($sql,array($str));
            $stmt = $db->query('CALL p_contract_print_appendix(?)',array($str));
            $staffs = $stmt->fetchAll();
            $stmt->closeCursor();
            
            $where      = array();
            $where[]    = $QAsmContract->getAdapter()->quoteInto('id IN (?)',$ids);
            $where[]    = $QAsmContract->getAdapter()->quoteInto('print_status = ?',0);
            $where[]    = $QAsmContract->getAdapter()->quoteInto('print_type = ?',2);

            $dataUpdate = array(
                'send_letter' => date('Y-m-d'),
                'print_status' => 1
            );

            if($print_status == 1){
                $db->beginTransaction();
                try {
                    $QAsmContract->update($dataUpdate,$where);
                    $db->commit();
                } catch (Exception $e) {
                    $db->rollback();
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $this->_redirect("/contract/staff-preview");
                }
            }
        }

        $this->view->staff = $staffs;

        $QGroup = new Application_Model_Group();
        $this->view->groups = $QGroup->fetchAll();

        $QModel = new Application_Model_ContractType();
        $this->view->contract_types = $QModel->fetchAll();

        $QModel = new Application_Model_ContractTerm();
        $this->view->contract_terms = $QModel->fetchAll();

        $QModel = new Application_Model_Department();
        $this->view->departments = $QModel->fetchAll();

        $QRegionalMarket = new Application_Model_RegionalMarket();

        $this->view->regional_markets = $QRegionalMarket->get_cache();

        $QModel = new Application_Model_Team();
        $teams = $QModel->fetchAll();
        $teams_data = array();
        foreach ($teams as $k => $v)
        {
            if(in_array($v['id'], array(SALES_TITLE  , SALES_ACCESSORIES_TITLE ,PGPB_TITLE, PGPB_2_TITLE  )))
                $teams_data[$v['id']] = $v['name_vn'];
            else
                $teams_data[$v['id']] = $v['name'];
        }

        $this->view->teams = $teams_data;

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_success = $messages_success;
    }

    public function staffContractAction(){

        $confirm            = $this->getRequest()->getParam('btnConfirm');
        $rollback           = $this->getRequest()->getParam('btnRollback');
        $export             = $this->getRequest()->getParam('export');
        $export_info        = $this->getRequest()->getParam('export_info');
        $refresh            = $this->getRequest()->getParam('btnRefresh');
        $btnSetDate         = $this->getRequest()->getParam('btnSetDate');
        $btnSetPrintStatus  = $this->getRequest()->getParam('btnSetPrintStatus');
        $ids                = $this->getRequest()->getParam('id');
        $flashMessenger     = $this->_helper->flashMessenger;
        $name               = $this->getRequest()->getParam('name','');
        $code               = $this->getRequest()->getParam('code','');
        $status             = $this->getRequest()->getParam('status',-1);
        $office             = $this->getRequest()->getParam('office',-1);
        $expired_from       = $this->getRequest()->getParam('expired_from');
        $expired_to         = $this->getRequest()->getParam('expired_to');
        $signed_from        = $this->getRequest()->getParam('signed_from','');
        $signed_to          = $this->getRequest()->getParam('signed_to','');
        $print_type         = $this->getRequest()->getParam('print_type');
        $letter_status      = $this->getRequest()->getParam('letter_status');
        $area_id            = $this->getRequest()->getParam('area_id',NULL);
        $department         = $this->getRequest()->getParam('department',NULL);
        $team               = $this->getRequest()->getParam('team',NULL);
        $title              = $this->getRequest()->getParam('title',NULL);
        $different          = $this->getRequest()->getParam('different',NULL);
        $off                = $this->getRequest()->getParam('off',0);
        $contract_term      = $this->getRequest()->getParam('contract_term',NULL);
        $print_status       = $this->getRequest()->getParam('print_status',-1);
        $company_id         = $this->getRequest()->getParam('company_id',NULL);
        $set_date           = $this->getRequest()->getParam('set_date',date('d/m/Y'));
        $return_letter_from = $this->getRequest()->getParam('return_letter_from');
        $return_letter_to   = $this->getRequest()->getParam('return_letter_to');
        $re_active          = $this->getRequest()->getParam('re_active',0);
        $page               = $this->getRequest()->getParam('page',1);
        $limit              = 600;
        $total              = 0;
        $desc               = $this->getRequest()->getParam('desc',1);
        $sort               = $this->getRequest()->getParam('sort');
        $office             = intval($office);
        $print_status       = intval($print_status);
        $back_url           = $this->getRequest()->getParam('back_url');
        $send_letter_from   = $this->getRequest()->getParam('send_letter_from');
        $send_letter_to     = $this->getRequest()->getParam('send_letter_to');
        $return_letter      = $this->getRequest()->getParam('return_letter',0);

        $userStorage             = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id                = $userStorage->id;
        $db                      = Zend_Registry::get('db');
        $this->view->userStorage = $userStorage;

        $params = array(
            'name'               => trim($name),
            'code'               => trim($code),
            'status'             => intval($status),
            'expired_from'       => $expired_from,
            'expired_to'         => $expired_to,
            'signed_from'        => $signed_from,
            'signed_to'          => $signed_to,
            'print_type'         => $print_type,
            'letter_status'      => intval($letter_status),
            'confirm_date'       => date('m/Y'),
            'office'             => $office,
            'area_id'            => $area_id,
            'department'         => $department,
            'team'               => $team,
            'title'              => $title,
            'different'          => $different,
            'off'                => $off,
            'contract_term'      => $contract_term,
            'company_id'         => $company_id,
            'print_status'       => $print_status,
            'ids'                => $ids,
            'set_date'           => $set_date,
            'return_letter_from' => $return_letter_from,
            'return_letter_to'   => $return_letter_to,
            're_active'          => $re_active,
            'back_url'           => $back_url,
            'return_letter'      => $return_letter,
            'send_letter_from'   => $send_letter_from,
            'send_letter_to'     => $send_letter_to,
            're_view'            => 1
        );


        if($rollback){
            $this->_rollback($params);
        }

        if($btnSetDate){
            $this->setTimeReturnLetter($params);
        }
        if($refresh){
            $this->_refresh($params);
        }

        if($btnSetPrintStatus){
            $this->_setPrintStatus($params);
        }

        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;

        $QContractTerm = new Application_Model_ContractTerm();
        $contractTerms = $QContractTerm->get_cache();
        $this->view->contractTerms = $contractTerms;

        $QProvince               = new Application_Model_Province();
        $provinces               = $QProvince->get_all();
        $this->view->provinces   = $provinces;

        $QTeam = new Application_Model_Team();
        $recursiveDepartmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDepartmentTeamTitle;

        $area_id_confirm = array();
        $group_id = $userStorage->group_id;
        if($group_id != HR_ID){
            $sql = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = ".$userStorage->id.
                " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = ".$userStorage->id;
            ;
            $stmt1 = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();

            foreach($area_tmp as $item):
                $area_id_confirm[] = $item['area_id'];
            endforeach;
            $this->view->area_id_confirm = $area_id_confirm;
        }
        $teams = array();
        if( $department AND count($department) > 0 ){
            foreach ($department as $key => $value) {
                $teams  += $recursiveDepartmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if($team AND $department AND count($team) > 0){
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];;
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;

        $QAsmContract = new Application_Model_AsmContract();

        if($export){
            $list = $QAsmContract->fetchStaffContract(NULL,NULL,$total,$params);
            $this->_exportData($list,1);
            exit;
        }

        $list = $QAsmContract->fetchStaffContract($page,$limit,$total,$params);
        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract/staff-contract' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;
    }

    public function staffNewAction(){

        $confirm           = $this->getRequest()->getParam('btnConfirm');
        $rollback          = $this->getRequest()->getParam('btnRollback');
        $export            = $this->getRequest()->getParam('export');
        $export_info       = $this->getRequest()->getParam('export_info');
        $refresh           = $this->getRequest()->getParam('btnRefresh');
        $btnSetDate        = $this->getRequest()->getParam('btnSetDate');
        $btnSetPrintStatus = $this->getRequest()->getParam('btnSetPrintStatus');


        $ids               = $this->getRequest()->getParam('id');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db                      = Zend_Registry::get('db');
        $this->view->userStorage = $userStorage;

        $flashMessenger     = $this->_helper->flashMessenger;
        $name               = $this->getRequest()->getParam('name','');
        $code               = $this->getRequest()->getParam('code','');
        $status             = $this->getRequest()->getParam('status',-1);
        $office             = $this->getRequest()->getParam('office',-1);
        $expired_from       = $this->getRequest()->getParam('expired_from','');
        $expired_to         = $this->getRequest()->getParam('expired_to','');
        $signed_from        = $this->getRequest()->getParam('signed_from',date('01/m/Y'));
        $signed_to          = $this->getRequest()->getParam('signed_to',date('t/m/Y'));
        $print_type         = $this->getRequest()->getParam('print_type',NULL);
        $letter_status      = $this->getRequest()->getParam('letter_status');
        $area_id            = $this->getRequest()->getParam('area_id',NULL);
        $department         = $this->getRequest()->getParam('department',NULL);
        $team               = $this->getRequest()->getParam('team',NULL);
        $title              = $this->getRequest()->getParam('title',NULL);
        $different          = $this->getRequest()->getParam('different',NULL);
        $off                = $this->getRequest()->getParam('off',0);
        $contract_term      = $this->getRequest()->getParam('contract_term',NULL);
        $print_status       = $this->getRequest()->getParam('print_status',-1);
        $company_id         = $this->getRequest()->getParam('company_id',NULL);
        $set_date           = $this->getRequest()->getParam('set_date',date('d/m/Y'));
        $return_letter_from = $this->getRequest()->getParam('return_letter_from');
        $return_letter_to   = $this->getRequest()->getParam('return_letter_to');
        $re_active          = $this->getRequest()->getParam('re_active',0);
        $page               = $this->getRequest()->getParam('page',1);
        $limit              = 600;
        $total              = 0;
        $desc               = $this->getRequest()->getParam('desc',1);
        $sort               = $this->getRequest()->getParam('sort');
        $office             = intval($office);
        $print_status       = intval($print_status);
        $back_url           = $this->getRequest()->getParam('back_url');
        $send_letter_from   = $this->getRequest()->getParam('send_letter_from');
        $send_letter_to     = $this->getRequest()->getParam('send_letter_to');
        $return_letter      = $this->getRequest()->getParam('return_letter',0);

        $params = array(
            'name'               => trim($name),
            'code'               => trim($code),
            'status'             => intval($status),
            'expired_from'       => $expired_from,
            'expired_to'         => $expired_to,
            'signed_from'        => $signed_from,
            'signed_to'          => $signed_to,
            'print_type'         => $print_type,
            'letter_status'      => intval($letter_status),
            'confirm_date'       => date('m/Y'),
            'office'             => $office,
            'area_id'            => $area_id,
            'department'         => $department,
            'team'               => $team,
            'title'              => $title,
            'different'          => $different,
            'off'                => $off,
            'contract_term'      => $contract_term,
            'company_id'         => $company_id,
            'print_status'       => $print_status,
            'ids'                => $ids,
            'set_date'           => $set_date,
            'return_letter_from' => $return_letter_from,
            'return_letter_to'   => $return_letter_to,
            're_active'          => $re_active,
            'back_url'           => $back_url,
            'return_letter'      => $return_letter,
            'send_letter_from'   => $send_letter_from,
            'send_letter_to'     => $send_letter_to,
            'new_staff'          => 1,
            're_view'            => 0
        );

        if($rollback){
            $this->_rollback($params);
        }

        if($btnSetDate){
            $this->setTimeReturnLetter($params);
        }
        if($refresh){
            $this->_refresh($params);
        }

        if($btnSetPrintStatus){
            $this->_setPrintStatus($params);
        }

        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;

        $QContractTerm = new Application_Model_ContractTerm();
        $contractTerms = $QContractTerm->get_cache();
        $this->view->contractTerms = $contractTerms;

        $QProvince               = new Application_Model_Province();
        $provinces               = $QProvince->get_all();
        $this->view->provinces   = $provinces;

        $QTeam = new Application_Model_Team();
        $recursiveDepartmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDepartmentTeamTitle;

        $area_id_confirm = array();
        $group_id = $userStorage->group_id;
        if($group_id != HR_ID){
            $sql = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = ".$userStorage->id.
                " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = ".$userStorage->id;
            ;
            $stmt1 = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();

            foreach($area_tmp as $item):
                $area_id_confirm[] = $item['area_id'];
            endforeach;
            $this->view->area_id_confirm = $area_id_confirm;
        }
        $teams = array();
        if( $department AND count($department) > 0 ){
            foreach ($department as $key => $value) {
                $teams  += $recursiveDepartmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if($team AND $department AND count($team) > 0){
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];;
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;

        $QAsmContract = new Application_Model_AsmContract();
        if($export){
            $list = $QAsmContract->fetchStaffContract(NULL,NULL,$total,$params);
            $this->_exportData($list,1);
            exit;
        }
        $list = $QAsmContract->fetchStaffContract($page,$limit,$total,$params);
        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract/staff-new' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;
    }

    public function staffTransferAction(){

        $confirm           = $this->getRequest()->getParam('btnConfirm');
        $rollback          = $this->getRequest()->getParam('btnRollback');
        $export            = $this->getRequest()->getParam('export');
        $export_info       = $this->getRequest()->getParam('export_info');
        $refresh           = $this->getRequest()->getParam('btnRefresh');
        $btnSetDate        = $this->getRequest()->getParam('btnSetDate');
        $btnSetPrintStatus = $this->getRequest()->getParam('btnSetPrintStatus');

        $ids               = $this->getRequest()->getParam('id');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db                      = Zend_Registry::get('db');
        $this->view->userStorage = $userStorage;

        $flashMessenger     = $this->_helper->flashMessenger;
        $name               = $this->getRequest()->getParam('name','');
        $code               = $this->getRequest()->getParam('code','');
        $status             = $this->getRequest()->getParam('status',-1);
        $office             = $this->getRequest()->getParam('office',-1);
        $expired_from       = $this->getRequest()->getParam('expired_from','');
        $expired_to         = $this->getRequest()->getParam('expired_to','');
        $signed_from        = $this->getRequest()->getParam('signed_from',date('01/m/Y'));
        $signed_to          = $this->getRequest()->getParam('signed_to',date('t/m/Y'));
        $print_type         = $this->getRequest()->getParam('print_type',NULL);
        $letter_status      = $this->getRequest()->getParam('letter_status');
        $area_id            = $this->getRequest()->getParam('area_id',NULL);
        $department         = $this->getRequest()->getParam('department',NULL);
        $team               = $this->getRequest()->getParam('team',NULL);
        $title              = $this->getRequest()->getParam('title',NULL);
        $different          = $this->getRequest()->getParam('different',NULL);
        $off                = $this->getRequest()->getParam('off',0);
        $contract_term      = $this->getRequest()->getParam('contract_term',NULL);
        $print_status       = $this->getRequest()->getParam('print_status',-1);
        $company_id         = $this->getRequest()->getParam('company_id',NULL);
        $set_date           = $this->getRequest()->getParam('set_date',date('d/m/Y'));
        $return_letter_from = $this->getRequest()->getParam('return_letter_from');
        $return_letter_to   = $this->getRequest()->getParam('return_letter_to');
        $re_active          = $this->getRequest()->getParam('re_active',0);
        $page               = $this->getRequest()->getParam('page',1);
        $limit              = 600;
        $total              = 0;
        $desc               = $this->getRequest()->getParam('desc',1);
        $sort               = $this->getRequest()->getParam('sort');
        $office             = intval($office);
        $print_status       = intval($print_status);
        $back_url           = $this->getRequest()->getParam('back_url');
        $send_letter_from   = $this->getRequest()->getParam('send_letter_from');
        $send_letter_to     = $this->getRequest()->getParam('send_letter_to');
        $return_letter      = $this->getRequest()->getParam('return_letter',0);

        $params = array(
            'name'               => trim($name),
            'code'               => trim($code),
            'status'             => intval($status),
            'expired_from'       => $expired_from,
            'expired_to'         => $expired_to,
            'signed_from'        => $signed_from,
            'signed_to'          => $signed_to,
            'print_type'         => $print_type,
            'letter_status'      => intval($letter_status),
            'confirm_date'       => date('m/Y'),
            'office'             => $office,
            'area_id'            => $area_id,
            'department'         => $department,
            'team'               => $team,
            'title'              => $title,
            'different'          => $different,
            'off'                => $off,
            'contract_term'      => $contract_term,
            'company_id'         => $company_id,
            'print_status'       => $print_status,
            'ids'                => $ids,
            'set_date'           => $set_date,
            'return_letter_from' => $return_letter_from,
            'return_letter_to'   => $return_letter_to,
            're_active'          => $re_active,
            'back_url'           => $back_url,
            'return_letter'      => $return_letter,
            'send_letter_from'   => $send_letter_from,
            'send_letter_to'     => $send_letter_to,
            'new_staff'          => 0,
            'transfer'           => 1,
            're_view'            => 0
        );

        if($rollback){
            $this->_rollback($params);
        }

        if($btnSetDate){
            $this->setTimeReturnLetter($params);
        }
        if($refresh){
            $this->_refresh($params);
        }

        if($btnSetPrintStatus){
            $this->_setPrintStatus($params);
        }

        $QProposal             = new Application_Model_AsmProposal();
        $proposals             = $QProposal->get_all();
        $this->view->proposals = $proposals;

        $QContractTerm = new Application_Model_ContractTerm();
        $contractTerms = $QContractTerm->get_cache();
        $this->view->contractTerms = $contractTerms;

        $QProvince               = new Application_Model_Province();
        $provinces               = $QProvince->get_all();
        $this->view->provinces   = $provinces;

        $QTeam = new Application_Model_Team();
        $recursiveDepartmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDepartmentTeamTitle;

        $area_id_confirm = array();
        $group_id = $userStorage->group_id;
        if($group_id != HR_ID){
            $sql = "SELECT area_id FROM asm WHERE type = 2 AND staff_id = ".$userStorage->id.
                " UNION
                    SELECT b.area_id FROM staff a
                    INNER JOIN regional_market b ON a.regional_market = b.id
                    WHERE a.id = ".$userStorage->id;
            ;
            $stmt1 = $db->query($sql);
            $area_tmp = $stmt1->fetchAll();

            foreach($area_tmp as $item):
                $area_id_confirm[] = $item['area_id'];
            endforeach;
            $this->view->area_id_confirm = $area_id_confirm;
        }
        $teams = array();
        if( $department AND count($department) > 0 ){
            foreach ($department as $key => $value) {
                $teams  += $recursiveDepartmentTeamTitle[$value]['children'];
            }
        }
        $this->view->teams = $teams;

        $titles = array();
        if($team AND $department AND count($team) > 0){
            foreach ($team as $key => $value) {
                $titles += $teams[$value]['children'];;
            }
        }
        $this->view->titles = $titles;

        $QArea             = new Application_Model_Area();
        $areas             = $QArea->get_cache();
        $this->view->areas = $areas;

        $QAsmContract = new Application_Model_AsmContract();
        if($export){
            $list = $QAsmContract->fetchStaffContract(NULL,NULL,$total,$params);
            $this->_exportData($list,1);
            exit;
        }
        $list = $QAsmContract->fetchStaffContract($page,$limit,$total,$params);
        $this->view->list             = $list;
        $this->view->desc             = $desc;
        $this->view->sort             = $sort;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->page             = $page;
        $this->view->url              = HOST . 'contract/staff-transfer' . ($params ? '?' . http_build_query($params) . '&' : '?');
        $this->view->offset           = $limit * ($page - 1);
        $this->view->params           = $params;

        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error               = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error   = $messages_error;
    }

    public function _exportData($list,$type = 1){
        /*
         * 1: in lai
         * 2: nhan vien moi
         * 3: transfer
         * 4: de xuat
         */
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'STT',
            'Công ty',
            'Mã nhân viên',
            'Họ tên',
            'Khu vực',
            'Tỉnh',
            'Phòng ban',
            'Chức vụ',
            'Loại hợp đồng',
            'Loại in',
            'Ngày bắt đầu',
            'Ngày hết hạn',
            'Lương cơ bản',
            'Phụ cấp',
            'Điện thoại',
            'Xăng xe',
            'Địa điểm làm việc',
            'Ngày gửi HD cho nhân viên',
            'Ngày nhận lại HD',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $stt = 0;

        foreach($list as $key => $value):
            $alpha = 'A';
            $from  = '';
            $to    = '';
            $send  = '';
            $receive = '';

            if(isset($value['from_date']) AND $value['from_date']){
                $from = date('d/m/Y',strtotime($value['from_date']));
            }

            if(isset($value['to_date']) AND $value['to_date'] AND $value['contract_level'] != 4){
                $to = date('d/m/Y',strtotime($value['to_date']));
            }

            if(isset($value['send_letter']) AND $value['send_letter']){
                $send = date('d/m/Y',strtotime($value['send_letter']));
            }

            if(isset($value['receive_letter']) AND $value['receive_letter']){
                $receive = date('d/m/Y',strtotime($value['receive_letter']));
            }

            $sheet->getCell($alpha++.$index)->setValueExplicit( ++$stt, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['company_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['staff_code']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['staff_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['regional_market_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['department_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['contract_name']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( (intval($value['print_type']) == 1 ) ? 'Hợp đồng':'Phụ lục' , PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( $from, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( $to, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['salary']) , PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['to_salary']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['allowance_1']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['allowance_2']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['allowance_3']), PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( '', PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( $send, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit( $receive, PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        endforeach;

        $filename = 'staff_contract_' . date('d-m-Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function unSendLetterAction(){
        include 'contract'.DIRECTORY_SEPARATOR.'un-send-letter.php';
    }

    public function createAction(){
        include 'contract'.DIRECTORY_SEPARATOR.'create.php';
    }

    public function saveContractAction(){
        include 'contract'.DIRECTORY_SEPARATOR.'save-contract.php';
    }

    public function getBasicInfoAction(){
        include 'contract'.DIRECTORY_SEPARATOR.'get-basic-info.php';
    }


}
