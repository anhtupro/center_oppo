<?php
$this->_helper->layout->disableLayout();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
if(empty($userStorage)){
	$url = HOST . 'user/login?b='.urldecode('/culture-game/index');
	 		$this->redirect($url);
}
$from_time		= TIME_GAME_START; //thời gian bắt đầu làm
$to_time		= TIME_GAME_END; //thời gian kết thúc game
$current_time 	= date("Y-m-d H:i:s");
if(!in_array($userStorage->id, [5899,8813,22813,9289])){
	if($current_time < $from_time ){
		$url = HOST . 'culture-game/index?soon=1';
	 		$this->redirect($url);
	}
	if($current_time > $to_time ){
		$url = HOST . 'culture-game/index?late=1';
	 		$this->redirect($url);
	}
	
}
