<?php
try {
    
    $flashMessenger = $this->_helper->flashMessenger;
    $this->_helper->layout->disableLayout();
    $QCultureGame           = new Application_Model_CultureGame();
    $QMiniGameRewardLog = new Application_Model_MiniGameRewardLog();
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
//    print "<pre>".print_r($userStorage);die;
    $url = HOST . 'user/login?b=' . urlencode(HOST . 'culture-game/index');
    if (empty($userStorage)) {
        $this->_redirect($url);
    }

    $staff_id = $userStorage->id;

    $params = array(
        'staff_id' => $staff_id,
        'round' => CULTURE_GAME_CURRENT_ROUND
    );
    $get_reward_log = $QMiniGameRewardLog->getLogRewardOfPlayer($params);
    $total_rights_answer = 0;
    $err = 0;
    if (!empty($get_reward_log)){
         $err = 1;
    }else {
        //vong quay
        $data_finish = $QCultureGame->getPlayedFinishRoundThird($params);
        if(!empty($data_finish) && $data_finish['is_done'] == 1){
            $err = 0;
        }else{
            $err = 2;
        }
    }

    if ($this->getRequest()->getMethod() == 'POST' and $err == 0) {
        $response = array('status' => 0, 'message' => 'Lỗi dữ liệu không đúng');

       $reward = $this->getNewRotationCulture($staff_id);
        
        if (!empty($reward)) {
          
            $response['status'] = 1;
            $response['message'] = "Thành công";
            $response['item_id'] = $reward['id'];
            $response['name'] = $reward['name'];

        echo json_encode($response);
        exit();
        }

        
    }
$this->view->error = $err;
}
catch (exception $e)
{
    print "<pre>".print_r($e);die;
}

