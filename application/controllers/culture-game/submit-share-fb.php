<?php
try {
    $share_fb = $this->getRequest()->getParam('share_fb');
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QCultureGame = new Application_Model_CultureGame();
   
    $id_staff = $userStorage->id;
    $params = [
        'staff_id' => $id_staff,
        'round'     => CULTURE_GAME_CURRENT_ROUND
    ];


    if (!empty($share_fb)) {
       
        $where = [];
        $where[] = $QCultureGame->getAdapter()->quoteInto('staff_id = ?', $id_staff);
        $where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
        $get_data = $QCultureGame->fetchRow($where);

        if(!empty($get_data) && $get_data['share_fb'] == 0){
            $data = [
                'share_fb' => 1
            ];
            $QCultureGame->update($data, $where);
            $result = [
                'status'        => 1,
                'message'   => 'Ok!'
            ];
            echo json_encode($result);
            exit; 
        }else{
            $result = [
                'status'        => 0,
                'message'   => 'Fail!'
            ];
            echo json_encode($result);
            exit; 
        }
        
    }

}
catch (exception $e) {
    echo json_encode($e);
    exit;
}