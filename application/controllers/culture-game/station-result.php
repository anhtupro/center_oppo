<?php
$this->_helper->layout->disableLayout();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
if(empty($userStorage)){
	$url = HOST . 'user/login?b='.urldecode('/culture-game/index');
	 		$this->redirect($url);
}

$QCultureGame 			= new Application_Model_CultureGame();

if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND1){
	$list_result_current = $QCultureGame->getCurrentResultOne();
}
if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND2){
	$list_result_current = $QCultureGame->getCurrentResultTwo();
}
if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND3){
	$list_result_current = $QCultureGame->getCurrentResultThird();
}
if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND4){
	$list_result_current = $QCultureGame->getCurrentResultFour();
}
$list_result_all = $QCultureGame->getCurrentResultAll();

$this->view->list_result_current = $list_result_current;
$this->view->list_result_all = $list_result_all;
