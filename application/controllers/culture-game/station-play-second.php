<?php
$this->_helper->layout->disableLayout();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
if(empty($userStorage)){
	$url = HOST . 'user/login?b='.urldecode('/culture-game/index');
	 		$this->redirect($url);
}
$from_time		= TIME_GAME_START; //thời gian bắt đầu làm
$to_time		= TIME_GAME_END; //thời gian kết thúc game
$current_time 	= date("Y-m-d H:i:s");
if(!in_array($userStorage->id, [5899,8813,22813])){
	if($current_time < $from_time ){
		$url = HOST . 'culture-game/index?soon=1';
	 		$this->redirect($url);
	}
	if($current_time > $to_time ){
		$url = HOST . 'culture-game/index?late=1';
	 		$this->redirect($url);
	}
}
if(CULTURE_GAME_CURRENT_ROUND != CULTURE_GAME_ROUND2){
	$url = HOST . 'culture-game/station';
	 $this->redirect($url);
}
$params = [
	'staff_id'	=> $userStorage->id,
	'round'	=> CULTURE_GAME_CURRENT_ROUND
];
//check user đã hoàn thành câu hỏi chưa
$QCultureGameDetailRoundFirst = new Application_Model_CultureGameDetailRoundFirst();
$QCultureGame 			= new Application_Model_CultureGame();
$QCultureGameQuestionDetail			= new Application_Model_CultureGameQuestionDetail();
$QCultureGameQuestionRoundSecond			= new Application_Model_CultureGameQuestionRoundSecond();



$where = [];
$where[] = $QCultureGame->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
$where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
$check_play = $QCultureGame->fetchRow($where);
$num_play = 0; // đánh dấu lượt chơi

if(!empty($check_play)){

	if($check_play['num_play'] > 1 || $check_play['is_done'] == 1){
		$url = HOST . 'culture-game/station-finish';
		$this->redirect($url);
	}else{
			
			//update play 2 time
            $data = [
                'num_play' => 2,
                'started_at'	=> $current_time 
            ];
            $QCultureGame->update($data, $where);
            $num_play = 2; 
            $id_master = $check_play['id'];

            //delete detail old
   //          $where_detail = [];
			// $where_detail[] = $QCultureGameDetailRoundFirst->getAdapter()->quoteInto('master_id = ?', $check_play['id']);
			// $where_detail[] = $QCultureGameDetailRoundFirst->getAdapter()->quoteInto('round = ?', 1);
			// $QCultureGameDetailRoundFirst->delete($where_detail);
	}
	
}else{

	$data_master = [
		'staff_id'		=> $userStorage->id,
		'round'			=> CULTURE_GAME_CURRENT_ROUND,
		'started_at'	=> $current_time,
		'num_play'		=> 1
	];
	$id_master = $QCultureGame->insert($data_master);
	$num_play = 1;
}
$this->view->num_play = $num_play;
$this->view->id_master = $id_master;


$cache_question = $QCultureGameQuestionRoundSecond->get_cache();
$cache_question_detail = $QCultureGameQuestionDetail->get_cache();

//array đáp án
$answer = [
	1 => 2,
	2 => 4,
	3 => 2,
	4 => 3,
	5 => 2,
	6 => 1,
	7 => 2,
	8 => 3,
	9 => 3,
	10 => 2,
	11 => 3,
	12 => 3,
	13 => 3,
	14 => 1,
	15 => 3,
	16 => 1,
	17 => 1,
	18 => 1,
	19 => 1,
	20 => 2,
	21 => 3,
	22 => 1
];
$title = [
	1 => 'A',
	2 => 'B',
	3 => 'C',
	4 => 'D',
	
];
$character = [
	1 => 'C',
	2 => 'H',
	3 => 'U',
	4 => 'T',
	5 => 'R',
	6 => 'O',
	7 => 'N',
	8 => 'G',
	9 => 'K',
	10=> 'E',
	11=> 'T',
	12=> 'Q',
	13=> 'U',
	14=> 'A',
	15=> 'T',
	16=> 'A',
	17=> 'P',
	18=> 'T',
	19=> 'H',
	20=> 'E'
	
];


//lấy ngẫu nhiên 10 câu hỏi ko trùng nhau, trộn ngẫu nhiên với 10 câu ko có câu hỏi
$arr_zero = [0,0,0,0,0,0,0,0,0,0];
$arr_question = [];
$ques =rand(1,22);
for($num = 1; $num <= 10; $num++) {
	while (in_array($ques, $arr_question)) {
				$ques =rand(1,22);
	}
	$arr_question[] = $ques;
}
$shuffle_question = array_merge($arr_zero,$arr_question);
shuffle($shuffle_question);
// echo '<pre>';
// var_dump($shuffle_question); exit;
// echo '</pre>';

$this->view->cache_question = $cache_question;
$this->view->cache_question_detail = $cache_question_detail;
$this->view->shuffle_question = $shuffle_question;
$this->view->answer = json_encode($answer);
$this->view->title = $title;
$this->view->character = json_encode($character);



