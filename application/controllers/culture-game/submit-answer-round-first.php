<?php
try {
    $submit         = $this->getRequest()->getParam('submit');
    $question_id    = $this->getRequest()->getParam('question_id');
    $id_master      = $this->getRequest()->getParam('id_master');
    $id_random      = $this->getRequest()->getParam('id_random');
    $user_choose    = $this->getRequest()->getParam('user_choose', null);
    $answer         = $this->getRequest()->getParam('answer', null);

    $submited_at = date('Y-m-d H:i:s', strtotime(''.date('Y-m-d H:i:s').' -1 seconds')); //thời gian lúc vừa submit
    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
    $milisecond = $d->format("u"); // lấy milisecond

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    if(empty($userStorage)){
        $url = HOST . 'user/login?b='.urldecode('/culture-game/index');
                $this->redirect($url);
    }
    $QCultureGame = new Application_Model_CultureGame();
    $QCultureGameDetailRoundFirst = new Application_Model_CultureGameDetailRoundFirst();
    $id_staff = $userStorage->id;
    $params = [
        'staff_id'      => $staff_id,
        'question_id'   => $question_id,
        'id_master'     => $id_master,
        'round'         => CULTURE_GAME_CURRENT_ROUND
    ];


    if (!empty($submit)) {
        //lấy time bắt đầu câu hỏi từ database
        $time_started = $QCultureGameDetailRoundFirst->getTimeStartQuestion($params);
        $date1 = strtotime($time_started[0]['started_at']);
        $date2 = strtotime($submited_at);
        $diff = $date2 - $date1;
        $point = $diff;
        //submit quá thời gian
        if($point > CULTURE_GAME_TIME_SECOND_ROUND1 || $time_started[0]['is_done'] == 1){
             $result = [
                'status'        => 3,
                'alert'         => 'Bạn đã hết giờ chơi !!!',
                'url'           => ''.HOST.'culture-game/station-finish'
            ];
           echo json_encode($result);
            exit; 
        }
        //seconds
        $answer_ques_one = 'XINCHAOCACBAN';
        $answer_ques_two = [
            1 => 'TAPTRUNGVAOCHATLUONGSANPHAM',
            2 => 'COKETQUAMOICOGIATRI',
            3 => 'LAYMUCTIEULAMDIEMKHOIDAU'
        ];
        
//submit đầu tiên
        if($question_id == 1){
            $right = 0;
            if ($user_choose == $answer_ques_one){
                $right = 1;
                $data = [
                    'master_id'         => $id_master,
                    'question_id'       => $question_id,
                    'user_choose'       => $user_choose,
                    'started_at'        => $time_started[0]['started_at'],
                    'submit_at'         => $submited_at,
                    'point'             => $point,
                    'diff'              => $diff,
                    'submit_milisecond' => $milisecond,
                    'is_right' => $right

                ];
                $QCultureGameDetailRoundFirst->insert($data);

                $question_rand = rand(1,3);
                
                $result = [
                    'status'             => 1,
                    'question_id'        => 2,
                    'question_random'    => $question_rand,
                ];
               echo json_encode($result);
                exit; 
            }else{
                $result = [
                    'status'        => 0,
                    'message'       => 'Wrong answer !'
                ];
                echo json_encode($result);
                exit; 
            }
           
        }
        if($question_id == 2){
            $right = 0;
            if ($user_choose == $answer_ques_two[$id_random]){
                $right = 1;
                $data = [
                    'master_id'         => $id_master,
                    'question_id'       => $question_id,
                    'user_choose'       => $user_choose,
                    'started_at'        => $time_started[0]['started_at'],
                    'submit_at'         => $submited_at,
                    'point'             => $point,
                    'diff'              => $diff,
                    'submit_milisecond' => $milisecond,
                    'is_right'          => $right

                ];
                $QCultureGameDetailRoundFirst->insert($data);

                $where      = [];
                $where[]    = $QCultureGame->getAdapter()->quoteInto('id = ?', $id_master);
                $where[]    = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
                $data       = [
                    'submit_at'     => $submited_at,
                    'seconds'       => $diff,
                    'miliseconds'   => $milisecond,
                    'is_done'       => 1
                ];
                $QCultureGame->update($data, $where);
                $result = [
                    'status'        => 2,
                    'url'           => ''.HOST.'culture-game/station-finish'
                ];
                echo json_encode($result);
                exit; 
            }else{
                $result = [
                    'status'        => 0,
                    'message'       => 'Wrong answer !'
                ];
               echo json_encode($result);
                exit; 
            }
        }

        $result = [
                'status'        => 0,
                'question_id'   => 0,
                'answer'        => 'Fail',
            ];

        echo json_encode($result);
            exit;
        
    }
}
catch (exception $e) {
    echo json_encode($e);
    exit;
}