<?php
try {
    $submit = $this->getRequest()->getParam('submit');
    $id_master = $this->getRequest()->getParam('id_master');

    $submited_at = date('Y-m-d H:i:s', strtotime('' . date('Y-m-d H:i:s') . ' -1 seconds')); //thời gian lúc vừa submit
    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
    $milisecond = $d->format("u"); // lấy milisecond


    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QCultureGame = new Application_Model_CultureGame();
    $QCultureGameDetailRoundFirst = new Application_Model_CultureGameDetailRoundFirst();
    $id_staff = $userStorage->id;
    $params = [
        'staff_id' => $id_staff,
        'id_master' => $id_master,
        'round'     => CULTURE_GAME_CURRENT_ROUND
    ];


    if (!empty($submit)) {
       
        //lấy time bắt đầu câu hỏi từ database
        $time_started = $QCultureGameDetailRoundFirst->getTimeStartQuestion($params);

        $date1 = strtotime($time_started[0]['started_at']);
        $date2 = strtotime($submited_at);
        $diff = $date2 - $date1;
        $point = $diff;
 
        //submit quá thời gian / hoặc đã hoàn thành rồi
        if($point > CULTURE_GAME_TIME_SECOND_ROUND2 || $time_started[0]['is_done'] == 1){
             $result = [
                'status'        => 3,
                'alert'   => 'Bạn đã hết giờ chơi !!!',
                'url'   => ''.HOST.'culture-game/station-finish'
            ];
           echo json_encode($result);
            exit; 
        }
        //seconds



      

            $where = [];
            $where[] = $QCultureGame->getAdapter()->quoteInto('id = ?', $id_master);
            $where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
            $data = [
                'submit_at' => $submited_at,
                'seconds' => $diff,
                'miliseconds'   => $milisecond,
                'is_done'       => 1
            ];
            $QCultureGame->update($data, $where);
            $result = [
                'status'        => 2,
                'url'   => ''.HOST.'culture-game/station-finish'
            ];
           echo json_encode($result);
            exit; 
        
    }

}
catch (exception $e) {
    echo json_encode($e);
    exit;
}