<?php
$this->_helper->layout->disableLayout();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
if(empty($userStorage)){
	$url = HOST . 'user/login?b='.urldecode('/culture-game/index');
	 		$this->redirect($url);
}
$from_time		= TIME_GAME_START; //thời gian bắt đầu làm
$to_time		= TIME_GAME_END; //thời gian kết thúc game
$current_time 	= date("Y-m-d H:i:s");
if(!in_array($userStorage->id, [5899,8813])){
	if($current_time < $from_time ){
		$url = HOST . 'culture-game/index?soon=1';
	 		$this->redirect($url);
	}
	if($current_time > $to_time ){
		$url = HOST . 'culture-game/index?late=1';
	 		$this->redirect($url);
	}
}



if(CULTURE_GAME_CURRENT_ROUND != CULTURE_GAME_ROUND1){
	$url = HOST . 'culture-game/station';
	 $this->redirect($url);
}
$params = [
	'staff_id'	=> $userStorage->id,
	'round'	=> CULTURE_GAME_CURRENT_ROUND
];
//check user đã hoàn thành câu hỏi chưa
$QCultureGameDetailRoundFirst = new Application_Model_CultureGameDetailRoundFirst();
$QCultureGame 			= new Application_Model_CultureGame();
$where = [];
$where[] = $QCultureGame->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
$where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
$check_play = $QCultureGame->fetchRow($where);
$num_play = 0; // đánh dấu lượt chơi
if(!empty($check_play)){

	if($check_play['num_play'] > 1 || $check_play['is_done'] == 1){
		$url = HOST . 'culture-game/station-finish';
		$this->redirect($url);
	}else{
			
			//update play 2 time
            $data = [
                'num_play' => 2,
                'started_at'	=> $current_time 
            ];
            $QCultureGame->update($data, $where);
            $num_play = 2; 
            $id_master = $check_play['id'];

            //delete detail old
            $where_detail = [];
			$where_detail[] = $QCultureGameDetailRoundFirst->getAdapter()->quoteInto('master_id = ?', $check_play['id']);
			$where_detail[] = $QCultureGameDetailRoundFirst->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
			$QCultureGameDetailRoundFirst->delete($where_detail);
	}
	
}else{
	$data_master = [
		'staff_id'		=> $userStorage->id,
		'round'			=> CULTURE_GAME_CURRENT_ROUND,
		'started_at'	=> $current_time,
		'num_play'		=> 1
	];
	$id_master = $QCultureGame->insert($data_master);
	$num_play = 1;
}







$this->view->num_play = $num_play;
$this->view->id_master = $id_master;
$this->view->question_id = 1;
// $this->view->answer = 'XINCHAOCACBAN';