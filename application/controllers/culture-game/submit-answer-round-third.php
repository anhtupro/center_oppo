<?php
try {
    $submit = $this->getRequest()->getParam('submit');
    $id_master = $this->getRequest()->getParam('id_master');
    $option_id = $this->getRequest()->getParam('option_id');
    $array_answer = $this->getRequest()->getParam('array_answer');

    $submited_at = date('Y-m-d H:i:s', strtotime('' . date('Y-m-d H:i:s') . ' -1 seconds')); //thời gian lúc vừa submit
    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
    $milisecond = $d->format("u"); // lấy milisecond


    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $QCultureGame = new Application_Model_CultureGame();
    $QCultureGameDetailRoundFirst = new Application_Model_CultureGameDetailRoundFirst();
    $id_staff = $userStorage->id;
    $params = [
        'staff_id' => $id_staff,
        'id_master' => $id_master,
        'round'     => CULTURE_GAME_CURRENT_ROUND
    ];

//array đáp án
$answer_option1 = [
    0 => "G",
    1 => "I",
    2 => "T",
    3 => "A",
    4 => "P",
    5 => "T",
    6 => "H",
    7 => "E",
    8 => "A",
    9 => "H",
    10 => "K",
    11 => "E",
    12 => "T",
    13 => "Q",
    14 => "U",
    15 => "A",
    16 => "K",
    17 => "H",
    18 => "O",
    19 => "N",
    20 => "G",
    21 => "V",
    22 => "I",
    23 => "E",
    24 => "N",
    25 => "C",
    26 => "O",
    27 => "R",
    28 => "N",
    29 => "H",
    30 => "U",
    31 => "P",
    32 => "I",
    33 => "H",
    34 => "O",
    35 => "O",
    36 => "K",
    37 => "P",
    38 => "M",
    39 => "U",
    40 => "C",
    41 => "T",
    42 => "I",
    43 => "E",
    44 => "U",
    45 => "T",
    46 => "H",
    47 => "O",
    48 => "O",
    49 => "D",
    50 => "Q",
    51 => "A",
    52 => "N",
    53 => "A",
    54 => "U",
    55 => "C",
    56 => "G",
    57 => "U",
    58 => "H",
    59 => "A",
    60 => "N",
    61 => "C",
    62 => "H",
    63 => "E",
    64 => "Q",
    65 => "U",
    66 => "A",
    67 => "N",
];
//array đáp án
$answer_option2 = [
    0 => "O",
    1 => "P",
    2 => "P",
    3 => "K",
    4 => "H",
    5 => "O",
    6 => "N",
    7 => "G",
    8 => "V",
    9 => "I",
    10 => "E",
    11 => "N",
    12 => "C",
    13 => "O",
    14 => "M",
    15 => "A",
    16 => "U",
    17 => "N",
    18 => "V",
    19 => "T",
    20 => "H",
    21 => "A",
    22 => "N",
    23 => "H",
    24 => "C",
    25 => "O",
    26 => "N",
    27 => "G",
    28 => "K",
    29 => "H",
    30 => "A",
    31 => "C",
    32 => "H",
    33 => "Q",
    34 => "U",
    35 => "A",
    36 => "N",
    37 => "T",
    38 => "H",
    39 => "O",
    40 => "K",
    41 => "H",
    42 => "O",
    43 => "I",
    44 => "D",
    45 => "A",
    46 => "U",
    47 => "E",
    48 => "T",
    49 => "A",
    50 => "P",
    51 => "T",
    52 => "H",
    53 => "E",
    54 => "E",
    55 => "Q",
    56 => "T",
    57 => "U",
    58 => "U",
    59 => "Q",
    60 => "G",
    61 => "I",
    62 => "A",
    63 => "T",
    64 => "R",
    65 => "I",
    66 => "U",
    67 => "A",
];

    if (!empty($submit)) {
       
        


        if($option_id == 1){
            $answer_option = $answer_option1;
        }else{
             $answer_option = $answer_option2;
        }
        //lấy time bắt đầu câu hỏi từ database
        $time_started = $QCultureGameDetailRoundFirst->getTimeStartQuestion($params);

        $date1 = strtotime($time_started[0]['started_at']);
        $date2 = strtotime($submited_at);
        $diff = $date2 - $date1;
        $point = $diff;
 
        //submit quá thời gian / hoặc đã hoàn thành rồi
        if($point > CULTURE_GAME_TIME_SECOND_ROUND3 || $time_started[0]['is_done'] == 1){
             $result = [
                'status'        => 3,
                'alert'   => 'Bạn đã hết giờ chơi !!!',
                'url'   => ''.HOST.'culture-game/station-finish'
            ];
           echo json_encode($result);
            exit; 
        }
        //seconds
            $flag_wrong = 0;
            $arr_wrong = [];
            $arr_correct = [];
            $array_answer = explode(',', $array_answer);
            if(!empty($array_answer)){

                foreach ($array_answer as $key => $value) {
                    if($value == $answer_option[$key]){
                        $arr_correct[] = $key+1;
                    }else{
                        $flag_wrong = 1;
                        $arr_wrong[] = $key+1;
                    }
                }
            }

            if($flag_wrong == 0 && empty($arr_wrong)){
                $where = [];
                $where[] = $QCultureGame->getAdapter()->quoteInto('id = ?', $id_master);
                $where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
                $data = [
                    'submit_at' => $submited_at,
                    'seconds' => $diff,
                    'miliseconds'   => $milisecond,
                    'is_done'       => 1
                ];
                $QCultureGame->update($data, $where);
                $result = [
                    'status'        => 2,
                    'url'   => ''.HOST.'culture-game/station-finish',
                    'arr_wrong'     => $arr_wrong,
                    'arr_correct'     => $arr_correct
                ];
               echo json_encode($result);
                exit; 
            }else{
                $result = [
                    'status'        => 0,
                    'arr_wrong'     => $arr_wrong,
                    'arr_correct'     => $arr_correct
                ];
               echo json_encode($result);
                exit; 
            }

      

            
        
    }

}
catch (exception $e) {
    echo json_encode($e);
    exit;
}