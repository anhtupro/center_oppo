<?php
$this->_helper->layout->disableLayout();
$userStorage 			= Zend_Auth::getInstance()->getStorage()->read();
if(empty($userStorage)){
	$url = HOST . 'user/login?b='.urldecode('/culture-game/index');
	 		$this->redirect($url);
}

$QCultureGame 			= new Application_Model_CultureGame();
$QCultureGameDetailRoundFirst = new Application_Model_CultureGameDetailRoundFirst();


if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND1){
	$params = [
		'staff_id'	=> $userStorage->id,
		'round'	=> CULTURE_GAME_CURRENT_ROUND
	];

	//check còn lượt chơi hay không
	$num_play = 0;
	$where = [];
	$where[] = $QCultureGame->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
	$where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
	$check_play = $QCultureGame->fetchRow($where);


	if(empty($check_play)){
		$url = HOST . 'culture-game/station';
		$this->redirect($url);
	}

	if(!empty($check_play) && $check_play['num_play'] < 2 && $check_play['is_done'] == 0){
		$num_play = 1;
	}


	$data_finish = $QCultureGame->getPlayedFinishRoundFirst($params);
	$finish = 0;
	$time = 0;
	if(!empty($data_finish) && count($data_finish) == 2){
		$finish = 1;
		$second_finish = $QCultureGame->getSecondFinishRoundFirst($params);
		$time = $check_play['seconds'];
	}

	
	$this->view->num_play = $num_play;
	$this->view->time = $time;
	$this->view->finish = $finish;
}
if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND2){
	$params = [
		'staff_id'	=> $userStorage->id,
		'round'	=> CULTURE_GAME_CURRENT_ROUND
	];

	//check còn lượt chơi hay không
	$num_play = 0;
	$where = [];
	$where[] = $QCultureGame->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
	$where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
	$check_play = $QCultureGame->fetchRow($where);

	if(empty($check_play)){
		$url = HOST . 'culture-game/station';
		$this->redirect($url);
	}
	
	if(!empty($check_play) && $check_play['num_play'] < 2 && $check_play['is_done'] == 0){
		$num_play = 1;
	}

	
	$data_finish = $QCultureGame->getPlayedFinishRoundSecond($params);
	$finish = 0;
	$time = 0;
	if(!empty($data_finish) && $data_finish['is_done'] == 1){
		$finish = 1;
		$second_finish = $QCultureGame->getSecondFinishRoundSecond($params);
		$time = $data_finish['seconds'];
	}

	
	$this->view->num_play = $num_play;
	$this->view->time = $time;
	$this->view->finish = $finish;
}
if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND3){
	$params = [
		'staff_id'	=> $userStorage->id,
		'round'	=> CULTURE_GAME_CURRENT_ROUND
	];

	//check còn lượt chơi hay không
	$num_play = 0;
	$where = [];
	$where[] = $QCultureGame->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
	$where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
	$check_play = $QCultureGame->fetchRow($where);

	if(empty($check_play)){
		$url = HOST . 'culture-game/station';
		$this->redirect($url);
	}
	
	if(!empty($check_play) && $check_play['num_play'] < 2 && $check_play['is_done'] == 0){
		$num_play = 1;
	}

	
	$data_finish = $QCultureGame->getPlayedFinishRoundThird($params);
	$finish = 0;
	$time = 0;
	if(!empty($data_finish) && $data_finish['is_done'] == 1){
		$finish = 1;
		$second_finish = $QCultureGame->getSecondFinishRoundThird($params);
		$time = $data_finish['seconds'];
	}

	
	$this->view->num_play = $num_play;
	$this->view->time = $time;
	$this->view->finish = $finish;
}
if(CULTURE_GAME_CURRENT_ROUND == CULTURE_GAME_ROUND4){
	$params = [
		'staff_id'	=> $userStorage->id,
		'round'	=> CULTURE_GAME_CURRENT_ROUND
	];

	//check còn lượt chơi hay không
	$num_play = 0;
	$where = [];
	$where[] = $QCultureGame->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
	$where[] = $QCultureGame->getAdapter()->quoteInto('round = ?', CULTURE_GAME_CURRENT_ROUND);
	$check_play = $QCultureGame->fetchRow($where);

	if(empty($check_play)){
		$url = HOST . 'culture-game/station';
		$this->redirect($url);
	}
	
	if(!empty($check_play) && $check_play['num_play'] < 2 && $check_play['is_done'] == 0){
		$num_play = 1;
	}

	
	$data_finish = $QCultureGame->getPlayedFinishRoundFour($params);
	$finish = 0;
	$time = 0;
	if(!empty($data_finish) && $data_finish['is_done'] == 1){
		$finish = 1;
		$second_finish = $QCultureGame->getSecondFinishRoundFour($params);
		$time = $data_finish['seconds'];
	}

	
	$this->view->num_play = $num_play;
	$this->view->time = $time;
	$this->view->finish = $finish;
}