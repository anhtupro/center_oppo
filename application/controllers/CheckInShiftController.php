<?php

    class CheckInShiftController extends My_Controller_Action
    {

        public function indexAction()
        {
            $date = $this->getRequest()->getParam('date', null);
            $code = $this->getRequest()->getParam('code');
            $status = $this->getRequest()->getParam('status', null);
            $page = $this->getRequest()->getParam('page', 1);

            $params = array(
                'date' => !empty($date)?DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d'):null,
                'code' => $code,
                'status' => $status,
                'limit' => 10,
                'page' => $page,
            );
            $params['offset'] = ($params['page']-1)*$params['limit'];

            $QCheckInShift = new Application_Model_CheckInShift();
            $data = $QCheckInShift->fetchPagination($params);

            $params['date'] = $date;
            unset($params['page']);
            unset($params['offset']);
            $this->view->params = $params;

            $this->view->data = $data['data'];
            $this->view->total = $data['total'];
            $this->view->limit = $params['limit'];
            $this->view->offset = $params['offset'];
            $this->view->url = HOST . 'check-in-shift/index' . ($params ? '?' . http_build_query($params) . '&' : '?');
        }

        public function createAction()
        {
            $staff_codes = $this->getRequest()->getParam('staff_code');
            $date = $this->getRequest()->getParam('date');
            $begin = $this->getRequest()->getParam('begin') . ":00";
            $end = $this->getRequest()->getParam('end') . ":59";
            $begin_break_time = $this->getRequest()->getParam('begin_break_time') . ":00";
            $end_break_time = $this->getRequest()->getParam('end_break_time') . ":59";
            $status = $this->getRequest()->getParam('status', 0);
            $submit = $this->getRequest()->getParam('submit', 0);
            $id = $this->getRequest()->getParam('id');

            $QCheckInShift = new Application_Model_CheckInShift();

            $flashMessenger = $this->_helper->flashMessenger;
            if($submit)
            {
                if(!empty($date))
                {
                    $date = "'" . DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d') ."'";
                }
                else
                {
                    $date = "null";
                }
                if(empty($id))
                {
                    $values = "('" . $staff_codes . "', '" . $begin . "', '" . $end . "', '" . $begin_break_time . "', '" . $end_break_time . "', " . $date . ", " . $status . ")";
                    $QCheckInShift->insertCheckInShift($values);
                     $flashMessenger->setNamespace('success')->addMessage("Thêm dữ liệu thành công");
                    $this->_redirect("/check-in-shift/");
                }
                else
                {
                    $values = "(" . $id . ",'" . $staff_codes . "', '" . $begin . "', '" . $end . "', '" . $begin_break_time . "', '" . $end_break_time . "', " . $date . ", " . $status . ")";
                    $QCheckInShift->updateCheckInShift($values);
                     $flashMessenger->setNamespace('success')->addMessage("Cập nhật dữ liệu thành công");
                    $this->_redirect("/check-in-shift/create?id=" . $id);
                }
                
               
            }

            if(!empty($id))
            {
                $data = $QCheckInShift->find($id)->current();
                
                $this->view->data = $data;
                $this->view->id = $id;
            }

            $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
            $this->view->messages = $flashMessenger->setNamespace('success')->getMessages();
        }
    }