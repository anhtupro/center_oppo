<?php

set_time_limit(0);
ini_set('memory_limit', '300M');

$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);


$QCheckKpiStaff = new Application_Model_CheckKpiStaff();
$QLockedKpi = new Application_Model_LockedKpi();

$time = $this->getRequest()->getParam('time');
$done_duplicate_imei = $this->getRequest()->getParam('done_duplicate_imei');
$done_check_file = $this->getRequest()->getParam('done_check_file');
$tech_has_lock_full = $this->getRequest()->getParam('tech_has_lock_full');
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0] ? explode('-', $time) [0] : date('m');
$year = explode('-', $time) [1] ? explode('-', $time) [1] : date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$from = date('Y-m-d', strtotime('01-' . $time));
$to = date('Y-m-t', strtotime('01-' . $time));

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$current_month = date('m');
$current_year = date('Y');

$diff_month = (($current_year - $year) * 12) + ($current_month - $month);

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'month' => $month,
    'year' => $year
];
$locked_kpi = $QLockedKpi->fetchRow([
    'month = ?' => $month,
    'year = ?' => $year
]);

if (! $time) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa chọn tháng'
    ]);
    return;
}

if ($diff_month > 1) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không thể xử lý KPI quá 1 tháng trước !'
    ]);
    return;
}

if ($locked_kpi['locked_all']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã chốt KPI tháng ' . $time . ' . Không thể xử lý thêm'
    ]);
    return;
}

if ($locked_kpi['locked_commit_kpi']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã insert vào check_kpi_staff tháng ' . $time . ' này rồi. Không thể insert thêm'
    ]);
    return;
}

if (! $locked_kpi['locked_tgdd']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng khóa báo số TGDĐ tháng ' . $time
    ]);
    return;
}

if (! $locked_kpi['locked_remove_kpi']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Tech chưa quét KPI tháng ' . $time
    ]);
    return;
}

$db = Zend_Registry::get('db');

if ($done_check_file && $done_duplicate_imei && $tech_has_lock_full && $locked_kpi['locked_remove_kpi']) {
    try {
//    // chạy sp_imei_kpi
//    $stmt_imei_kpi = $db->prepare("CALL `sp_imei_kpi`(:p_from_date, :p_to_date)");
//    $stmt_imei_kpi->bindParam('p_from_date', $from);
//    $stmt_imei_kpi->bindParam('p_to_date', $to);
//    $stmt_imei_kpi->execute();
//    $stmt_imei_kpi->closeCursor();
//
//
//    // chạy kpi_by_model_staff
//    $stmt_kpi_by_model_staff = $db->prepare("CALL `kpi_by_model_staff`(:p_from_date, :p_to_date)");
//    $stmt_kpi_by_model_staff->bindParam('p_from_date', $from);
//    $stmt_kpi_by_model_staff->bindParam('p_to_date', $to);
//    $stmt_kpi_by_model_staff->execute();
//    $stmt_kpi_by_model_staff->closeCursor();

        // tạo bảng log
        $QCheckKpiStaff->createTableLog($params);


    // fetch and insert data
    $stmt = $db->prepare("CALL `sp_check_kpi_lech_timing`(:p_from_date, :p_to_date)");
    $stmt->bindParam('p_from_date', $params['from_date']);
    $stmt->bindParam('p_to_date', $params['to_date']);
    $stmt->execute();
    $data_lech_timing = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = null;

    $stmt = $db->prepare("CALL `sp_check_kpi_khong_cong`(:p_from_date, :p_to_date)");
    $stmt->bindParam('p_from_date', $params['from_date']);
    $stmt->bindParam('p_to_date', $params['to_date']);
    $stmt->execute();
    $data_khong_cong = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = null;

    $stmt = $db->prepare("CALL `sp_check_kpi_khac_chuc_danh`(:p_from_date, :p_to_date)");
    $stmt->bindParam('p_from_date', $params['from_date']);
    $stmt->bindParam('p_to_date', $params['to_date']);
    $stmt->execute();
    $data_khac_chuc_danh = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = null;

// insert
//    $db->beginTransaction();

    if ($data_lech_timing) {
        foreach ($data_lech_timing as $item) {
            $QCheckKpiStaff->insert([
                'staff_id' => $item['staff_id'],
                'sellout_timing' => $item['sellout_timing'],
                'sellout_pg' => $item['sellout_pg'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'title' => $item['title'],
                'area_id' => $item['area_id'],
                'off_date' => $item['off_date'] ? date('Y-m-d', strtotime($item['off_date'])) : Null,
                'month' => $month,
                'year' => $year,
                'type' => 1
            ]);
        }
    }

    if ($data_khong_cong) {
        foreach ($data_khong_cong as $item) {
            $QCheckKpiStaff->insert([
                'staff_id' => $item['staff_id'],
                'sellout_pg' => $item['sellout_pg'],
                'sellout_sale' => $item['sellout_sale'],
                'sellout_store_leader' => $item['sellout_store_leader'],
                'sellout_consultant' => $item['sellout_consultant'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'title' => $item['title'],
                'area_id' => $item['area_id'],
                'off_date' => $item['off_date'] ? date('Y-m-d', strtotime($item['off_date'])) : Null,
                'month' => $month,
                'year' => $year,
                'type' => 2
            ]);
        }
    }


    if ($data_khac_chuc_danh) {
        foreach ($data_khac_chuc_danh as $item) {
            $QCheckKpiStaff->insert([
                'staff_id' => $item['staff_id'],
                'sellout_pg' => $item['sellout_pg'],
                'sellout_sale' => $item['sellout_sale'],
                'sellout_store_leader' => $item['sellout_store_leader'],
                'sellout_consultant' => $item['sellout_consultant'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $userStorage->id,
                'title' => $item['title'],
                'area_id' => $item['area_id'],
                'off_date' => $item['off_date'] ? date('Y-m-d', strtotime($item['off_date'])) : Null,
//                'transfer_date' => $item['transfer_date'] ? date('Y-m-d', strtotime($item['transfer_date'])) : Null,
                'transfer_note' => $item['transfer_date'],
                'month' => $month,
                'year' => $year,
                'type' => 3
            ]);
        }
    }

    $where = [
      'month = ?' => $month,
      'year = ?' => $year
    ];
    $QLockedKpi->update([
        'locked_commit_kpi' => 1,
    ],$where);

    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }
}

//$db->commit();

echo json_encode([
   'status' => 0
]);