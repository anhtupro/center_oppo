<?php

$sort   = $this->getRequest()->getParam('sort');
$desc   = $this->getRequest()->getParam('desc', 1);
$export = $this->getRequest()->getParam('export', 0);
$from   = $this->getRequest()->getParam('from', date('01/m/Y'));
$to     = $this->getRequest()->getParam('to', date('d/m/Y'));
$QAsm       = new Application_Model_Asm();
$asm_cache  = $QAsm->get_cache();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if (!in_array($userStorage->group_id, array(My_Staff_Group::ASM, My_Staff_Group::BOARD)) && !My_Staff_Permission_Area::view_all($userStorage->id)
        && !in_array($userStorage->email,array('chau.phan@oppomobile.vn','tam.do@oppomobile.vn', 'vannam.nguyen@oppomobile.vn'))) {
    echo "<pre>";
    print_r("Permision denied");
    die;
}
$staff_id  = $userStorage->id;
$params    = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'export' => $export,
);
$area_list = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();
$this->view->area_list=$area_list;
if (!empty($area_list)) {
    $string_area    = implode(array_values($area_list), ',');
    $params['area'] = $string_area;
}

$QArea                   = new Application_Model_Area();
$QImeiKpi                = new Application_Model_ImeiKpi();
$this->view->userStorage = $userStorage;

$data = $QImeiKpi->fetchRegion($params);

$params['get_total_sales'] = true;

$total_sales = $QImeiKpi->fetchArea($params);

$total_money       = $total_sales['total_value'];
$total_area        = $total_sales['total_area'];
$total_area_active = $total_sales['total_area_active'];


$sales = array();


foreach ($data as $item) {
    $point                       = ( $total_money > 0 and ( $item ['region_share'] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($item ['region_share'] / 100), 2) : 0;
    $val                         = $item;
    $val['point_cu']             = $point;
    $val['ti_le_chua_active']    = round(( ( $item['total_quantity'] - $item['total_activated'] ) / $item['total_quantity'] ) * 100, 1);
    $val['ti_le_active_ca_nuoc'] = round((($total_area - $total_area_active) / $total_area ) * 100, 1);
    $active_vuot                 = round(( ( $item['total_quantity'] - $item['total_activated'] ) / $item['total_quantity'] ) * 100, 1) - round((($total_area - $total_area_active) / $total_area ) * 100, 1);
    $val['active_vuot']          = $active_vuot;
    if ($active_vuot <= 0)
        $val['value_kpi']            = $item ['total_value'];
    else
        $val['value_kpi']            = $item ['total_value'] * ( (100 - $active_vuot) / 100 );

    $sales[] = $val;
}
// $total_money = 0;    
// $total_sales = 0;


$sales_final = array();
$total_value = 0;
foreach ($sales as $key => $value) {
    $total_value_kpi += $value['value_kpi'];
    $total_value     += $value ['total_value'];
}




foreach ($sales as $item) {
    $point_moi     = round(($item['value_kpi'] / $total_value_kpi) * 60 / ($item['region_share'] / 100), 2);
    $val           = $item;
    $val['point']  = $point_moi;
    $sales_final[] = $val;
}



//if(!empty($area_list)){
//    $list_data_by_id[]=$sales_final[$userStorage->id];
//}else{
//    $list_data_by_id=$sales_final;
//}
unset($params['kpi']);
unset($params['get_total_sales']);
usort($sales_final, array($this, 'cmp'));
$sales_final= My_Util::changeKey($sales_final, 'region_name');

$this->view->areas           = $QArea->get_cache();
$this->view->total_value_kpi = $total_value_kpi;
$this->view->total_value     = $total_value;

$this->view->total_sales           = $total_sales['total_quantity'];
$this->view->total_sales_activated = $total_sales['total_activated'];
$this->view->sales                 = $sales_final;
$this->view->url                   = HOST . 'timing/analytics-area' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->desc                  = $desc;
$this->view->current_col           = $sort;
$this->view->to                    = $to;
$this->view->from                  = $from;
$this->view->params                = $params;





