<?php
$this->_helper->layout()->disableLayout(true);
$this->_helper->viewRenderer->setNoRender(true);

require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();


$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0] ? explode('-', $time) [0] : date('m');
$year = explode('-', $time) [1] ? explode('-', $time) [1] : date('Y');
$from_date = date('Y-m-d', strtotime('01-' . $time));
$to_date = date('Y-m-t', strtotime('01-' . $time));

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date
];

$staff_id = -1; // lấy tất cả
$string_area_id = -1; // lấy tất cả khu vực

$db = Zend_Registry::get('db');
$stmt_out = $db->prepare("CALL `sp_report_leader`(:p_from_date, :p_to_date, :p_staff_id, :p_area_id)");
$stmt_out->bindParam('p_from_date', $from_date, PDO::PARAM_STR);
$stmt_out->bindParam('p_to_date', $to_date, PDO::PARAM_STR);
$stmt_out->bindParam('p_staff_id', $staff_id, PDO::PARAM_STR);
$stmt_out->bindParam('p_area_id', $string_area_id, PDO::PARAM_STR);

$stmt_out->execute();
$data_leader = $stmt_out->fetchAll();
$stmt_out->closeCursor();


$heads = array(
    'Area',
    'Area Share(%)',
    'Staff code',
    'Staff Name',
    'Title',
    'From date',
    'To date',
    'Channel share(%)',
    'Province',

    'Unit Oppo Phone + IOT (TGDĐ)',
    'Unit Oppo Phone (TGDĐ)',
    'Unit Oppo IOT (TGDĐ)',

    'Unit Oppo Phone + IOT (Other KA+IND) ',
    'Unit Oppo Phone (Other KA+IND) ',
    'Unit Oppo IOT (Other KA+IND) ',

    'Revenue 80% OPPO Phone + IOT (TGDĐ)',
    'Revenue 80% OPPO Phone + IOT (Other KA+IND)',
    'Revenue 80% Special Model OPPO (TGDĐ)',
    'Revenue 80% Special Model (Other KA+IND)',

    'Revenue 80% OPPO Phone + IOT (TGDĐ) ( Đã trừ model đặc biệt)',
    'Revenue 80% OPPO Phone + IOT (Other KA+IND) (Đã trừ model đặc biệt)',

    'KPI OPPO Phone + IOT (Đã trừ model đặc biệt)',



);


$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

$alpha = 'A';
$index = 1;
foreach ($heads as $key) {
    $sheet->setCellValue($alpha . $index, $key);
    $alpha++;
}
$index = 3;

$i = 1;

foreach ($data_leader as  $leader) {
    $alpha = 'A';

    $sheet->setCellValue($alpha++ . $index, $leader['area_name']);
    $sheet->setCellValue($alpha++ . $index, $leader['share_area']);
    $sheet->setCellValue($alpha++ . $index, $leader['staff_code'] );
    $sheet->setCellValue($alpha++ . $index, $leader['staff_name'] );
    $sheet->setCellValue($alpha++ . $index, $leader['title']);
    $sheet->setCellValue($alpha++ . $index, date('d/m/Y', strtotime($leader['from_date'])));
    $sheet->setCellValue($alpha++ . $index, $leader['to_date'] ? date('d/m/Y', strtotime($leader['to_date'])) : '');
    $sheet->setCellValue($alpha++ . $index, $leader['share_leader']);
    $sheet->setCellValue($alpha++ . $index, $leader['region_name']);
    $sheet->setCellValue($alpha++ . $index, $leader['quantity_tgdd_oppo']);
    $sheet->setCellValue($alpha++ . $index, $leader['quantity_phone_tgdd']);
    $sheet->setCellValue($alpha++ . $index, $leader['quantity_watch_tgdd']);

    $sheet->setCellValue($alpha++ . $index, $leader['quantity_different_tgdd_oppo']);
    $sheet->setCellValue($alpha++ . $index, $leader['quantity_phone_different_tgdd']);
    $sheet->setCellValue($alpha++ . $index, $leader['quantity_watch_different_tgdd']);

    $sheet->setCellValue($alpha++ . $index, $leader['value_tgdd_total_oppo']);
    $sheet->setCellValue($alpha++ . $index, $leader['value_different_tgdd_total_oppo']);
    $sheet->setCellValue($alpha++ . $index, $leader['value_special_model_tgdd_oppo']);
    $sheet->setCellValue($alpha++ . $index, $leader['value_special_model_different_tgdd_oppo']);

    $sheet->setCellValue($alpha++ . $index, $leader['value_tgdd_oppo']);
    $sheet->setCellValue($alpha++ . $index, $leader['value_different_tgdd_oppo']);

    $sheet->setCellValue($alpha++ . $index, $leader['kpi_oppo']);





    $index++;
}

$filename = 'Report Leader ' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
