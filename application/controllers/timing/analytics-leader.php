<?php

set_time_limit(0);

$sort        = $this->getRequest()->getParam('sort');
$desc        = $this->getRequest()->getParam('desc', 1);
$export      = $this->getRequest()->getParam('export', 0);
//$from        = $this->getRequest()->getParam('from', date('01/m/Y'));
//$to          = $this->getRequest()->getParam('to', date('d/m/Y'));
$area        = $this->getRequest()->getParam('area',NULL);
$month_year = $this->getRequest()->getParam('month_year', date('m/Y'));
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$db          = Zend_Registry::get('db');
$list_area   = [];

$month_year = str_replace('/', '-', $month_year);
$month = explode('-', $month_year) [0]   ?   explode('-', $month_year) [0]   :  date('m');
$year = explode('-', $month_year) [1]   ?   explode('-', $month_year) [1]   :  date('Y');
$from = date('d/m/Y', strtotime('01-' . $month_year));
$to = date('t/m/Y', strtotime('01-' . $month_year));

$QLeaderRankByMonth = new Application_Model_LeaderRankByMonth();



if (!in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '14.161.22.196', '112.109.92.6', '115.78.166.171'))) {
//    echo "Chức năng đang được bảo trì.";
//    exit;
}


$QArea      = new Application_Model_Area();
$area_cache = $QArea->get_cache();
$QAsm       = new Application_Model_Asm();
$asm_cache  = $QAsm->get_cache();
$params     = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'area'   => $area,
    'export' => $export,
    'title'  => $title,
    'month' => $month,
    'year' => $year,
    'month_year' => $month_year
);
// echo "<pre>";print_r($params);die;
// Xuáº¥t excel

if (isset($export) && $export) {
    $this->_forward('leader', 'sales-report', null, array('params' => $params));
    return;
}
$area_list = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();

$area_array      = [$area];
if (!empty($area_list)) {
  
    if (!empty($area)) {
        $filter_area = array_intersect($area_list, $area_array);
    } else {
        $filter_area = $area_list;
    }
} else {
    if (!empty($area)) {
        $filter_area = $area_array;
    } else {
        $filter_area = $area_list;
    }
}

$from_f     = date_create_from_format("d/m/Y", $from)->format("Y-m-d 00:00:00");
$to_f       = date_create_from_format("d/m/Y", $to)->format("Y-m-d 23:59:59");
$staff_id   = $userStorage->title == LEADER_TITLE ? $userStorage->id : -1;
$sql_params = [
    $from_f,
    $to_f,
    implode($filter_area, ","),
    $staff_id
];

//$sql        = 'CALL sp_report_leader_asm(?,?,?,?,@total_value)';
$sql        = 'CALL sp_report_leader_asm_qty(?,?,?,?,@total_value)';
$stmt       = $db->query($sql, $sql_params);
$list       = $stmt->fetchAll();

$stmt->closeCursor();
$total = $db->fetchOne('select @total_value');

// danh sách chốt
//if ($_GET['dev'] == 1) {
$params ['list_area'] = $filter_area;
$list_by_month_final = $QLeaderRankByMonth->getList($params);
$total_unit = $QLeaderRankByMonth->getTotalUnit($params);
$total_by_month_final = $list_by_month_final [0] ['sum_total_value'];
if ($list_by_month_final) {
    $list = $list_by_month_final;
    $total = $total_by_month_final;
}

//}

$this->view->total_value        = $total;
$this->view->sales       = $list;
$this->view->total_value = $total;
$this->view->total_unit = $total_unit;




if (empty($area_list)) {
    if (My_Staff_Permission_Area::view_all($userStorage->id) || in_array($userStorage->id, array(341))) {
        $list_area = $area_cache;
    }
} else {
    foreach ($area_list as $key => $value) {
        $list_area[$value] = $area_cache[$value];
    }
}

$this->view->params = $params;
$this->view->areas  = $list_area;
$this->view->url    = HOST . 'timing/analytics-area' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->to     = $to;
$this->view->from   = $from;
$this->view->userStorage =$userStorage;



