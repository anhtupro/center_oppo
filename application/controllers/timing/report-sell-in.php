<?php
$sort            = $this->getRequest()->getParam('sort');
$desc            = $this->getRequest()->getParam('desc', 1);
$export          = $this->getRequest()->getParam('export', 0);
$from            = $this->getRequest()->getParam('from', date('01/m/Y') );
$to              = $this->getRequest()->getParam('to', date('d/m/Y') );
$area            = $this->getRequest()->getParam('area');
$type            = $this->getRequest()->getParam('type');
$cat             = $this->getRequest()->getParam('cat');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QArea         = new Application_Model_Area();

$params = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'area'   => $area,
    'export' => $export,
    'type'   => $type,
    'cat'    => $cat
);

if ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $area_list    = array();
    $QAsm         = new Application_Model_Asm();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $params['list_area'] = $list_regions;
    $this->view->viewed_area_id = $list_regions;
}

$QMarket         = new Application_Model_Market();
$data = $QMarket->reportSellIn($params);

$this->view->areas   = $QArea->get_cache();
$this->view->sales   = $data;
$this->view->params  = $params;















