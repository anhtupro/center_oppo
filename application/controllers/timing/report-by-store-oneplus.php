<?php
$QKpiByModelBrand = new Application_Model_KpiByModelBrand();
$QArea = new Application_Model_Area(); 

$page = $this->getRequest()->getParam('page', 1);
$from_date = $this->getRequest()->getParam('from_date', date('Y-m-01'));
$to_date = $this->getRequest()->getParam('to_date', date('Y-m-d'));
$area_id = $this->getRequest()->getParam('area_id');
$export = $this->getRequest()->getParam('export');
$limit = 20;

$params = [
  'from_date' => $from_date,
  'to_date'=> $to_date,
    'area_id' => $area_id,
    'export' => $export
];


$list = $QKpiByModelBrand->fetchPagination($page, $limit, $total, $params);
$list_area = $QArea->getAreaKpi();
$total_quantity = $QKpiByModelBrand->getToalQuantity($params);

if($export) {
    $data = $QKpiByModelBrand->fetchPagination($page, NULL, $total, $params);
    $QKpiByModelBrand->export($data);
}

$this->view->list = $list;
$this->view->list_area = $list_area;
$this->view->params = $params;
$this->view->total_quantity = $total_quantity;
$this->view->url = HOST.'timing/report-by-store-oneplus'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit * ($page - 1);
$this->view->limit = $limit;
$this->view->total = $total;