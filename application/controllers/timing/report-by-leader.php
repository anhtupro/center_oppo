<?php

$time = $this->getRequest()->getParam('time', date('m/Y'));
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d', strtotime('01-' . $time));
$to_date = date('Y-m-t', strtotime('01-' . $time));
$QAsm = new Application_Model_Asm();
$QLeaderRankByMonth = new Application_Model_LeaderRankByMonth();

$special_user = [5899, 341, 23154, 326];
if (in_array($userStorage->id, $special_user)) {
    $staff_id = -1 ;
} else {
    $staff_id = $userStorage->id;
}

// phân quyền asm
$string_area_id = -1;
if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $list_regions = $QAsm->get_cache($userStorage->id);
    $list_area = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    
    $string_area_id = implode(',', $list_area) ;

}


$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'time' => $time ,
    'month' => $month,
    'year' => $year,
    'list_area' => $list_area,
    'staff_id' => $staff_id
];

$final_data = $QLeaderRankByMonth->getFinalData($params);


if ($final_data) {
    $data_leader = $final_data;
} else {
    $db = Zend_Registry::get('db');
    $stmt_out = $db->prepare("CALL `sp_report_leader`(:p_from_date, :p_to_date, :p_staff_id, :p_area_id)");
    $stmt_out->bindParam('p_from_date', $from_date, PDO::PARAM_STR);
    $stmt_out->bindParam('p_to_date', $to_date, PDO::PARAM_STR);
    $stmt_out->bindParam('p_staff_id', $staff_id, PDO::PARAM_STR);
    $stmt_out->bindParam('p_area_id', $string_area_id, PDO::PARAM_STR);
    $stmt_out->execute();
    $data_leader = $stmt_out->fetchAll();
    $stmt_out->closeCursor();
}


$this->view->data_leader = $data_leader;
$this->view->params = $params;