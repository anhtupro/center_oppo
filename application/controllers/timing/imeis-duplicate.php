<?php
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages;
