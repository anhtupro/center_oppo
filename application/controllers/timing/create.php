<?php
echo 1;
if (defined("LOCK_TIMING") && LOCK_TIMING) {
    $this->_helper->viewRenderer->setRender('lock');
    return;
}

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger     = $this->_helper->flashMessenger;
$QShift             = new Application_Model_Shift();
$_shifts            = $QShift->get_cache();
$this->view->shifts = $_shifts;

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$user_id      = $userStorage->id;
$group_id     = $userStorage->group_id;
// $QTime        = new Application_Model_Time();
// $check_status = $QTime->checkInStatus($user_id);

if ($user_id != SUPERADMIN_ID && !in_array($group_id, array(
        ADMINISTRATOR_ID,
        HR_ID,
        HR_EXT_ID,
        SALES_EXT_ID,
        BOARD_ID,
        ASMSTANDBY_ID,
        ASM_ID)))
{

    // if(!$check_status)
    // {
    //     $this->_redirect(HOST.'time/create');
    // }


    $where    = array();
    $datetime = new DateTime('today');
    $day      = $datetime->format('d');
    $month    = $datetime->format('m');
    $year     = $datetime->format('Y');

    // $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $user_id);
    // $where[] = $QTime->getAdapter()->quoteInto('DAY(created_at) = ?', $day);
    // $where[] = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $month);
    // $where[] = $QTime->getAdapter()->quoteInto('YEAR(created_at) = ?' , $year);
    // $resultTiming  = $QTime->fetchRow($where);

    // if (isset($resultTiming) and $resultTiming)
    // {
    //     //tạm thời dùng cho sale
    //    $shift =  $resultTiming['shift'];
    //    $this->view->shiftTiming     = $_shifts[$shift];
    //    $this->view->resultTiming    = $resultTiming->toArray();
    // }
}

// không cần check thuộc store nào vào lúc này,
// mình check ajax mỗi khi chọn store và ngày



//get goods
$QGood             = new Application_Model_Good();
$where_g = array();
$where_g[] = $QGood->getAdapter()->quoteInto('cat_id IN (?)', [PHONE_CAT_ID, IOT_OPPO_CAT_ID]);
// tạm thời ko cho chấm one plus
$where_g[] = $QGood->getAdapter()->quoteInto('id  != ?', 424);
$where_g[] = $QGood->getAdapter()->quoteInto('is_timing = ?', 1);

$goods   = $QGood->fetchAll($where_g, 'desc');
$this->view->goods = $goods;


//get accessories
$QAccessories            = new Application_Model_Good();
$where                   = $QAccessories->getAdapter()->quoteInto('cat_id = ?', ACCESS_CAT_ID);
$Accessories             = $QAccessories->fetchAll($where, 'desc');
$this->view->accessories = $Accessories;

//get staff info
$QStaff              = new Application_Model_Staff();
$where               = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
$this->view->staff   = $QStaff->fetchRow($where);
$this->view->user_id = $userStorage->id;

// load all model
$QGoodColor = new Application_Model_GoodColor();
$result = $QGoodColor->fetchAll();

$data = null;
if ($result->count()) {
    foreach ($goods as $good) {
        $colors = explode(',', $good->color);
        $temp = array();
        foreach ($result as $item){
            if (in_array($item['id'], $colors)) {
                $temp[] = array(
                    'id' => $item->id,
                    'model' => $item->name,
                );
            }
        }
        $data[$good['id']] = $temp;
    }
}

$this->view->good_colors = json_encode($data);

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;