<?php
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();
$QArea      = new Application_Model_Area();
$QAreaRankByMonth      = new Application_Model_AreaRankByMonth();


$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'from_f' => $from_date,
    'to_f' => $to_date
];

$data_area = $QAreaRankByMonth->getData($params);

$list_area = $QArea->getAreaKpi();

$heads = array(
    'Area',
    'Unit',
    'Unit Phone (OPPO)',
    'Unit IOT (OPPO)',

    'Unit activated',
    'Unit Activated Phone (OPPO)',
    'Unit Activated IOT (OPPO)',

    'Value 80% Phone (OPPO)',
    'Value 80% IOT (OPPO)',
    'Value (80%)',


    'Value Special Model (80%)',
    'Value Model >= 7.990.000 (80%)',
    'Value Model còn lại (80%)',


    'Tỉ lệ chưa active cả nước (%)',
    'Tỉ lệ chưa active (%)',
    'Tỉ lệ chưa active vượt (%)',

    'Value KPI Phone (OPPO)',
    'Value KPI IOT (OPPO)',
    'Value tính KPI',



    'Value tính KPI Special Model',
    'Value tính KPI Model >= 7.990.000',
    'Value tính KPI Models còn lại',

    'Unit Brandshop Cty',
    'Value tính KPI BRS Cty',

    'Value tính KPI Special Model BRS Cty',
    'Value tính KPI Model >= 7.990.000 BRS Cty',
    'Value tính KPI Models còn lại BRS Cty',

    'Value tính KPI (trừ BRS Cty)',


    'Value tính KPI Special Model (trừ BRS Cty)',
    'Value tính KPI Model >= 7.990.000 (trừ BRS Cty)',
    'Value tính KPI Models còn lại (trừ BRS Cty)',

    'Region Share (%)',


    'Chiết khấu Model >= 7.990.000 (%)',
    'Chiết khấu Model còn lại (%)',

//    'Bonus Special model & model còn lại',
//    'Bonus Brandshop Cty',
    'Bonus (trừ BRS Cty)',
    'Bonus (trừ BRS Cty) Làm tròn số',

    'Point',
    'Rank'
);


$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 3;

$i = 1;

foreach($list_area as $area_id => $area_name){
    $alpha    = 'A';
    $data_area_excel = $data_area [$area_id];



    $sheet->setCellValue($alpha++.$index, $area_name);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['quantity']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel ['quantity_phone']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel ['quantity_watch']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['quantity_activated']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel ['quantity_activated_phone']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel ['quantity_activated_watch']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel ['value_phone']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel ['value_watch']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value']);


    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_special_model']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_gt79']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_excluded_special_model']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['ti_le_chua_active_ca_nuoc']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['ti_le_chua_active_khu_vuc']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['ti_le_chua_active_vuot']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel ['value_kpi_phone']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel ['value_kpi_watch']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi']);


    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_special_model']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_gt79']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_excluded_special_model']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['quantity_brandshop']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_brandshop']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_special_model_brandshop']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_gt79_brandshop']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_excluded_special_model_brandshop']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi'] - $data_area_excel['value_kpi_brandshop']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_special_model'] - $data_area_excel['value_kpi_special_model_brandshop']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_gt79'] - $data_area_excel['value_kpi_gt79_brandshop']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['value_kpi_excluded_special_model'] - $data_area_excel['value_kpi_excluded_special_model_brandshop']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['region_share']);

    $sheet->setCellValue($alpha++.$index, $data_area_excel['ti_le_chiet_khau_gt79']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['ti_le_chiet_khau_khu_vuc']);

//    $sheet->setCellValue($alpha++.$index, $data_area_excel['bonus']);
//    $sheet->setCellValue($alpha++.$index, $data_area_excel['bonus_brandshop']);
    $sheet->setCellValue($alpha++.$index, $data_area_excel['bonus'] - $data_area_excel['bonus_brandshop']);
    $sheet->setCellValue($alpha++.$index, ROUND($data_area_excel['bonus'] - $data_area_excel['bonus_brandshop'], -3));


    $sheet->setCellValue($alpha++.$index, $data_area_excel ['point'] ? ROUND($data_area_excel ['point'], 2) : '');
    $sheet->setCellValue($alpha++.$index, $data_area_excel ['rank']);


    $index++;
}

$filename = 'Sellout - Area ' . $month . '-' . $year ;
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
