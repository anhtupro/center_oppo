<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$QLockedKpi = new Application_Model_LockedKpi();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$time = $this->getRequest()->getParam('time');

$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');

$where_lock_kpi = [
    'month = ?' => $month,
    'year = ?' => $year
];

$locked_kpi = $QLockedKpi->fetchRow($where_lock_kpi);

$locked_tgdd = $locked_kpi['locked_tgdd'];

echo json_encode([
    'status' => 0,
    'has_locked_tgdd' => $locked_tgdd ? 1: 0
]);
