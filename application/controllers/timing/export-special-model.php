<?php
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();
$QArea      = new Application_Model_Area();
$QAreaRankByMonth      = new Application_Model_AreaRankByMonth();

$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date
];

$data = $QAreaRankByMonth->getDataSpecialModel($params);


$heads = array(
    'Area',
    'Province',

    'Tỉ lệ chưa active cả nước (%)',
    'Tỉ lệ chưa active (%)',
    'Tỉ lệ chưa active vượt (%)',

    'Model',

    'Unit',
    'Unit activated',
    'Value (80%)',
    'Value KPI',
    '------------',
    'Unit BRS Cty',
    'Value BRS Cty (80%)',
    'Value KPI BRS Cty',
    '------------',

    'Value (trừ BRS Cty) (80%)',
    'Value KPI (trừ BRS Cty)',

    'Mức chiết khấu(%)',
    'Bonus (trừ BRS Cty)',

);


$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 3;

$i = 1;

foreach($data as $item){
    $alpha    = 'A';

    $sheet->setCellValue($alpha++.$index, $item['area_name']);
    $sheet->setCellValue($alpha++.$index, $item['province_name']);
    $sheet->setCellValue($alpha++.$index, $item['ti_le_chua_active_ca_nuoc']);
    $sheet->setCellValue($alpha++.$index, $item['ti_le_chua_active_khu_vuc']);
    $sheet->setCellValue($alpha++.$index, $item['ti_le_chua_active_vuot']);


    $sheet->setCellValue($alpha++.$index, $item['model_name']);

    $sheet->setCellValue($alpha++.$index, $item['quantity']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated']);
    $sheet->setCellValue($alpha++.$index, $item['value']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi']);
    $sheet->setCellValue($alpha++.$index, '-----------');

    $sheet->setCellValue($alpha++.$index, $item['quantity_brandshop']);
    $sheet->setCellValue($alpha++.$index, $item['value_brandshop']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_brandshop']);
    $sheet->setCellValue($alpha++.$index, '-----------');


    $sheet->setCellValue($alpha++.$index, $item['value'] - $item['value_brandshop']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi'] - $item['value_kpi_brandshop']);

    $sheet->setCellValue($alpha++.$index, $item['ti_le_chiet_khau']);

    $sheet->setCellValue($alpha++.$index, $item['bonus'] - $item['bonus_brandshop']);

    $index++;
}

$filename = 'Sellout - Area - Chi tiết Special Model' . $month . '-' . $year ;
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
