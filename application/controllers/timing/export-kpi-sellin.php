<?php
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();

$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date
];

$db         = Zend_Registry::get('db');
$stmt_out = $db->prepare("CALL `sp_kpi_sellin`(:p_from_date, :p_to_date)");
$stmt_out->bindParam('p_from_date', $params['from_date']);
$stmt_out->bindParam('p_to_date', $params['to_date']);
$stmt_out->execute();
$data = $stmt_out->fetchAll();
$stmt_out->closeCursor();


$heads = array(
    'Imei',
    'Sn',
    'Invoice Time',
    'Invoice Number',
    'Ký hiệu',
    'Email',
    'Staff code',
    'Staff name',
    'Staff title',
    'Staff Area',
    'Dealer Area',
    'Dealer ID',
    'Dealer name',
    'Dealer ID parent',
    'Dealer name parent',
    'Warehouse',
    'good_id',
    'Model',
    'Color',
    'Price',
    '% doanh thu ',
    'KPI '
);


$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 2;

$i = 1;

foreach($data as $area_id => $item){
    $alpha    = 'A';
    $data_area_excel = $data_area [$area_id];



    $sheet->getCell($alpha++.$index)->setValueExplicit($item['imei_sn'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->getCell($alpha++.$index)->setValueExplicit($item['sn'], PHPExcel_Cell_DataType::TYPE_STRING);

    $sheet->setCellValue($alpha++.$index, date('Y-m-d', strtotime($item['invoice_time'])));
    $sheet->setCellValue($alpha++.$index, $item['invoice_number']);
    $sheet->setCellValue($alpha++.$index, $item['ky_hieu']);
    $sheet->setCellValue($alpha++.$index, $item['email']);
    $sheet->setCellValue($alpha++.$index, $item['staff_code']);
    $sheet->setCellValue($alpha++.$index, $item['staff_name']);
    $sheet->setCellValue($alpha++.$index, $item['staff_title']);
    $sheet->setCellValue($alpha++.$index, $item['area_staff']);
    $sheet->setCellValue($alpha++.$index, $item['area_dealer']);
    $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
    $sheet->setCellValue($alpha++.$index, $item['dealer_name']);
    $sheet->setCellValue($alpha++.$index, $item['dealer_id_parent']);
    $sheet->setCellValue($alpha++.$index, $item['dealer_name_parent']);
    $sheet->setCellValue($alpha++.$index, $item['warehouse']);
    $sheet->setCellValue($alpha++.$index, $item['good_id']);
    $sheet->setCellValue($alpha++.$index, $item['model']);
    $sheet->setCellValue($alpha++.$index, $item['color']);
    $sheet->setCellValue($alpha++.$index, $item['price']);
    $sheet->setCellValue($alpha++.$index, $item['percent_kpi']);
    $sheet->setCellValue($alpha++.$index, $item['kpi']);

    $index++;
}

$filename = 'KPI SELLIN SALES IND ' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
