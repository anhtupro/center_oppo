<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
include 'PHPExcel/IOFactory.php';
// config for excel template excel

// row
define('START_ROW', 2);

// column
define('IMEI', 0);
define('STORE_NAME', 1);
define('TIMING_DATE', 2);
define('PARTNER_ID', 3);

$QLockedKpi = new Application_Model_LockedKpi();

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$time = $this->getRequest()->getParam('time', NULL);
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$current_month = date('m');
$current_year = date('Y');

$diff_month = (($current_year - $year) * 12) + ($current_month - $month);
$locked_kpi = $QLockedKpi->fetchRow([
    'month = ?' => $month,
    'year = ?' => $year
]);

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date
];

if (! in_array($userStorage->id, [341])) {
    echo json_encode([
        'status' => 1,
        'message' => "Không có quyền thao tác!"
    ]);
    return;
}

if (!$time) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chọn tháng !'
    ]);
    return;
}
if ($diff_month > 1) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không thể xử lý KPI quá 1 tháng trước !'
    ]);
    return;
}

if ($locked_kpi['locked_all']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã chốt KPI tháng ' . $time . ' . Không thể xử lý thêm'
    ]);
    return;
}

if ($locked_kpi['locked_commit_kpi']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã chốt để xử lý 3 file không công, lệch timing, khác chức danh tháng ' . $time . ' . Không thể xử lý thêm'
    ]);
    return;
}


$QInsertDifferentTgdd = new Application_Model_InsertDifferentTgdd();
$QTiming = new Application_Model_Timing();
$QTimingSale = new Application_Model_TimingSale();
$db = Zend_Registry::get("db");

// insert vào bảng tạm
// upload and save file
if ($_FILES['file']['name']) { // if has file upload

    $save_folder = 'insert_different_tgdd';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 50000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx')
    );

    try {
        $file = My_File::get($save_folder, $requirement);

        if (!$file) {
            echo json_encode([
                'status' => 1,
                'message' => 'Upload failed'
            ]);
            return;
        }
        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];


    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }


    //read file
    //  Choose file to read
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    // read sheet
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();


    try {

        $insert_header_query = "INSERT INTO insert_different_tgdd(imei, partner_id, timing_date) VALUES ";
        $insert_body_query = '';

        $imei_wrong_format = [];
        $partner_id_wrong_format = [];
        $date_wrong_format = [];
        $month_not_equal = [];

        for ($row = START_ROW; $row <= $highestRow; ++$row) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = $rowData[0];

            $imei = "'" . (My_Util::escape_string(TRIM($rowData[IMEI]))) . "'";
            $partner_id = "'" . TRIM($rowData[PARTNER_ID]) . "'";
            $date = str_replace('/', '-', TRIM($rowData[TIMING_DATE]));
            $timing_date = $date ? "'" . date('Y-m-d 12:00:00', strtotime($date)) . "'" : 'NULL';

            if (strpos($imei, '+') || !$imei) {
                $imei_wrong_format [] = $row;
                continue;
            }

            if (!$partner_id) {
                $partner_id_wrong_format [] = $row;
                continue;
            }

            if (date('Y-m-d 12:00:00', strtotime($date)) === '1970-01-01 12:00:00') {
                $date_wrong_format [] = $row;
                continue;
            }

            if (date('m', strtotime($date)) !=  $month) { // check nếu tháng trong file khác với tháng đang tính KPI thì báo lỗi
                $month_not_equal [] = $row;
                continue;
            }


            // insert
            $value_insert = "("
                . $imei . ","
                . $partner_id  .  ","
                . $timing_date
                . "),";
            $insert_body_query .= $value_insert;

        }

        if ($imei_wrong_format) {
            echo json_encode([
                'status' => 1,
                'message' => 'Imei sai định dạng. Vui lòng sửa và upload file lại , tại các dòng: ' . implode("\n", $imei_wrong_format)
            ]);
            return;
        }

        if ($partner_id_wrong_format) {
            echo json_encode([
                'status' => 1,
                'message' => 'Partner_id đang trống , tại các dòng trong file: ' . implode(", ", $partner_id_wrong_format) . '. Vui lòng chỉnh và upload lại file'
            ]);
            return;
        }

        if ($date_wrong_format) {
            echo json_encode([
                'status' => 1,
                'message' => 'Ngày sai định dạng tại các dòng trong file: ' . implode(", ", $date_wrong_format) . '. Vui lòng chỉnh và upload lại file. Các định dạng cho phép dd-mm-yyyy hoặc yyyy-mm-dd'
            ]);
            return;
        }

        if ($month_not_equal) {
            echo json_encode([
                'status' => 1,
                'message' => 'Tháng trong file không khớp với tháng đang chọn, tại các dòng trong file: ' . implode(", ", $month_not_equal) . '. Vui lòng kiểm tra và upload lại file'
            ]);
            return;
        }



        if ($insert_body_query) {
            $insert_query = $insert_header_query . $insert_body_query;
            $insert_query = TRIM($insert_query, ',');

            if($insert_query){
                $db->query('TRUNCATE TABLE insert_different_tgdd');
                $stmt = $db->prepare($insert_query);
                $stmt->execute();
                $stmt->closeCursor();
            }
        }



    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

} else {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chọn file !'
    ]);
    return;
}


// check imei ko tồn tại
$array_invalid_imei = $QInsertDifferentTgdd->getInvalidImei();
if (count($array_invalid_imei) > 0) {
    echo json_encode([
        'status' => 1,
        'message' => 'Imei không tồn tại: ' . implode("\n", $array_invalid_imei) . '. Vui lòng kiểm tra và upload lại file'
    ]);
    return;
}


// check imei đã được báo số
$array_imei_has_timing = $QInsertDifferentTgdd->getImeiHasTiming();
if (count($array_imei_has_timing) > 0) {
    echo json_encode([
        'status' => 1,
        'message' => 'Imei đã được báo số: ' . implode("\n", $array_imei_has_timing) . '. Vui lòng kiểm tra và upload lại file'
    ]);
    return;
}


// check ko tồn tại partner_id
$array_invalid_partner = $QInsertDifferentTgdd->getInvalidPartner();
if (count($array_invalid_partner) > 0) {
    echo json_encode([
        'status' => 1,
        'message' => 'Partner_id không tồn tại: ' . implode("\n", $array_invalid_partner) . '. Vui lòng kiểm tra và upload lại file'
    ]);
    return;
}

// check store map sai distributor
$array_invalid_distributor = $QInsertDifferentTgdd->getInvalidDistributor();
if (count($array_invalid_distributor) > 0) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không tìm thấy Store hoặc store đã đóng tại Partner_id: ' . implode("\n", $array_invalid_distributor) . '. Vui lòng map lại distributor đúng cho shop và upload lại file'
    ]);
    return;
}

// update store_id theo partner_id
$QInsertDifferentTgdd->updateStoreId();

// update good
$QInsertDifferentTgdd->updateGood();

// insert timing , timing_sale

$data = $QInsertDifferentTgdd->getAll();

$db->beginTransaction();
try{
    foreach ($data as $item) {
        $timing_id = $QTiming->insert([
            'store' => $item['store_id'],
            'staff_id' => 3025,
            'from' => $item['timing_date'],
            'to' => $item['timing_date'],
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id,
            'approved_at' => date('Y-m-d H:i:s'),
            'approved_by' => $userStorage->id,
            'status' => 1,
            'note' => 'insert by tool',
            'shift' => 4
        ]);

        $QTimingSale->insert([
            'imei' => $item['imei'] ,
            'product_id' => $item['good_id'],
            'model_id' => $item['color_id'],
            'timing_id' => $timing_id
        ]);
    }
}  catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

$db->query('TRUNCATE TABLE insert_different_tgdd');
$db->commit();

echo json_encode([
    'status' => 0,
    'count_imei_success' => count($data)
]);

