<?php
$id = $this->getRequest()->getParam('id');
try {
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $user_id = $userStorage->id;

    if (!$id) $this->_redirect(HOST.'timing');
    $id = intval($id);

    // chỉ sales mới đc approve (trừ admin)
    if (! in_array( $userStorage->group_id, array(ACCESSORIES_ID) ) && $userStorage->group_id != ADMINISTRATOR_ID && $userStorage->group_id != SUPERADMIN_ID) {
        echo '-69';
        exit;
    }

    $Timing = new Application_Model_Timing();
    $where = $Timing->getAdapter()->quoteInto('id = ?', $id);
    $data = array(
        'status_accessories' => 1,
        'approved_accessories_at' => date('Y-m-d H:i:s'),
        'approved_accessories_by' => $userStorage->id,
    );
    $Timing->update($data, $where);

    echo 0;
    exit;
} catch (Exception $e){
    echo 1;
    exit;
}