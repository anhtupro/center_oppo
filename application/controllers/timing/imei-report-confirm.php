<?php
//echo 'Chức năng đã tạm khóa'; die;
/**
 * Bảng mã lỗi
 *        các mã sau được trả về khi gọi function AJAX này
 *        dùng để dịch ra ở client cho người ta dễ hiểu
 * @return  1 OK
 * @return  -100 Không phải truy vấn kiểu AJAX
 * @return  0 Thiếu thông tin
 * @return  -1 Không tồn tại thông báo trùng IMEI này
 * @return  -2 Thông báo này đã được xử lý
 * @return  -3 Bạn không có quyền sử dụng chức năng này
 * @return  -4 Không tìm thấy chấm công của người Bị báo cáo (chấm công không tồn tại hoặc đã bị xóa)
 * @return  -5 [description] chỗ này cần check lại, nhưng nếu dùng transaction thì ok, không lo bị nóng
 * @return  -6 Thiếu ID người được nhận
 * @return  -7 Không tìm thấy thông tin khách hàng của người Bị báo cáo
 * @return  -8 Không tìm thấy chấm công của người Bị báo cáo
 * @return  -9 Không tìm thấy thông tin người báo cáo
 * @return  -10 Không tìm thấy thông tin người Bị báo cáo
 * @return  -11 Không tìm thấy chấm công của người báo cáo
 * @return  -12 loi update data
 * @return  -1000 Chặn ASM báo cáo
 * @return  -9000 Éo biết vì sao lỗi (try-catch-exception)
 */
$this->_helper->viewRenderer->setNoRender(true);
$this->_helper->layout->disableLayout();

if (!$this->getRequest()->isXmlHttpRequest()) {
    exit('-100'); // not
}

$id = $this->getRequest()->getParam('id');
$staff_win = $this->getRequest()->getParam('staff_win');
$note = $this->getRequest()->getParam('note');
$now = date("Y-m-d H:i:s");


$db = Zend_Registry::get('db');
$db->beginTransaction();
if ($id) {
    // try {
    $QDuplicatedImei = new Application_Model_DuplicatedImei();
    $QTiming = new Application_Model_Timing();
    $QTimingLog = new Application_Model_TimingLog();
    $QTimingSale = new Application_Model_TimingSale();
    $QTimingSaleLog = new Application_Model_TimingSaleLog();
    $QTimingSaleTrash = new Application_Model_TimingSaleTrash();
    $QLockedKpi = new Application_Model_LockedKpi();


    // check $id
    $duplicated_imei = $QDuplicatedImei->fetchRow(['id =?' => $id]);
    $imei = $duplicated_imei['imei'];

    if (!$duplicated_imei) { // không tồn tại dòng này
        echo '-1';
        exit;
    }

    if ($duplicated_imei['solved'] == 1) { // đã xử lý rồi mà -_-
        echo '-2';
        exit;
    }

    $timing_date_duplicate = $duplicated_imei['date'];
    $from_date = date('Y-m-01 00:00:00', strtotime($timing_date_duplicate));
    $to_date = date('Y-m-t 23:59:59', strtotime($timing_date_duplicate));

    // check kpi đã chốt

    $locked_kpi = $QLockedKpi->fetchRow([
        'month = ?' => date('m', strtotime($timing_date_duplicate)),
        'year = ?' => date('Y', strtotime($timing_date_duplicate))
    ]);

    if ($locked_kpi['locked_all'] || $locked_kpi['locked_commit_kpi']) {
        echo '-15'; // đã chốt KPI tháng nên ko thể xử lý thêm
        exit;
    }

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();

    $group_id = $userStorage->group_id;
    $to_insert_timing_sales = null;
    $to_delete_timing_sales = null;
    $flag = false;

    // chia thao tác: tùy sếp hay asm mà xử lý
    if ($group_id == BOARD_ID || $group_id == SALES_EXT_ID || $userStorage->id == SUPERADMIN_ID) {
        if (!$staff_win) { // thiếu thông tin
            echo '-6';
            exit;
        }

        $timing_sales_first = $QTimingSale->fetchRow(['imei = ?' => $imei]);
        if ($timing_sales_first) {
            $timing_sales_first = $timing_sales_first->toArray();
        }

        if ($staff_win == $duplicated_imei['staff_id']) { // staff 2 win
            // nếu có người đã báo trong timing_sale thì mới insert vào timing_sale_log và xóa trong timing_sale
            if ($timing_sales_first) {
                $is_insert_timing_sale_log = FALSE;
                // backup timing_sale to timing_sale_log
                $log_note = $note ? TRIM($note) : 'Xử lý báo trùng Imei. Imei này đã được tính cho nhân viên báo trùng';
                $timing_sales_first['note'] = $log_note;
                $is_insert_timing_sale_log = $QTimingSaleLog->insert($timing_sales_first);

                // update duplicate imei with log timing_sale id
                $where_update_duplicate = $QDuplicatedImei->getAdapter()->quoteInto('id = ?', $id);
                $data_update_duplicate = [
                    'timing_sale_log_id' => $is_insert_timing_sale_log['id_main']
                ];
                $QDuplicatedImei->update($data_update_duplicate, $where_update_duplicate);

                //xóa timing sale
                $where = $QTimingSale->getAdapter()->quoteInto('imei = ?', $imei);
                $QTimingSale->delete($where);
            }

            // lưu timing
            $data_timing = [
                'staff_id' => $duplicated_imei['staff_id'],
                'shift' => 2,
                'from' => $duplicated_imei['date'],
                'to' => $duplicated_imei['date'],
                'store' => $duplicated_imei['store_id'],
                'note' => "New Tool Duplicated Inserted " . date("Y-m-d H:i:s"),
                'status' => 1,
                'created_at' => $duplicated_imei['date'],
                'created_by' => $userStorage->id,
                'approved_at' => $duplicated_imei['date'],
                'approved_by' => $userStorage->id
            ];
            $id_timing = $QTiming->insert($data_timing);

            //lưu timing sale
            $getModel = $QDuplicatedImei->getModel($imei);
            $data_timing_sale = [
                'product_id' => $getModel[0]['product_id'],
                'model_id' => $getModel[0]['color_id'],
                'customer_name' => $duplicated_imei['customer_name'],
                'phone_number' => $duplicated_imei['customer_phone'],
                'address' => $duplicated_imei['customer_address'],
                'imei' => $duplicated_imei['imei'],
                'timing_id' => $id_timing,
            ];
            $QTimingSale->insert($data_timing_sale);

            // cập nhật duplicate imei
            $data = array_filter(array(
//                            'timing_sales'  => $timing_sale_id,
                'staff_win' => $staff_win,
                'director_note' => $note ? $note : 'Imei này đã được chấm cho staff second',
                'solved' => 1,
                'solved_at' => date('Y-m-d H:i:s'),
                'solved_by' => $userStorage->id,
            ));

        } else { // staff 1 win
            $data = array_filter(array(
                'staff_win' => $staff_win,
                'director_note' => $note ? $note : 'Imei này đã được chấm cho staff first',
                'solved' => 1,
                'solved_at' => date('Y-m-d H:i:s'),
                'solved_by' => $userStorage->id,
            ));
        }

        // update 1 dòng
        $where = $QDuplicatedImei->getAdapter()->quoteInto('id = ?', $id);
        $QDuplicatedImei->update($data, $where);

        // update cột solved = 1 , đối với những imei có hơn 1 lần báo duplicate khi đã xử lý xong
        $where_many_duplicate = [
            'imei = ?' => $imei,
            'date >= ?' => $from_date,
            'date <= ?' => $to_date,
            'solved = ?' => 0,
            'asm_check = ?' => 1
        ];

        $QDuplicatedImei->update([
            'solved' => 1,
            'solved_by' => $userStorage->id,
            'solved_at' => date('Y-m-d H:i:s')
        ], $where_many_duplicate);
        // end update cột solved = 1 , đối với những imei có hơn 1 lần báo duplicate khi đã xử lý xong


    } elseif (in_array($group_id, array(ASM_ID, ASMSTANDBY_ID, SALES_ADMIN_ID))) {
        $where = $QDuplicatedImei->getAdapter()->quoteInto('id = ?', $id);

        $duplicated_imei_row = $QDuplicatedImei->fetchRow($where);

        $current_month = date('m');
        $current_day = date('d');
        $month_timing_date = date('m', strtotime($duplicated_imei_row['date']));

        if ($current_month != $month_timing_date && $current_day > 2) {
            echo -13;
            exit;
        }

        $data = array_filter(array(
            'asm_check' => 1,
            'asm_at' => date('Y-m-d H:i:s'),
            'asm_id' => $userStorage->id,
        ));
        $QDuplicatedImei->update($data, $where);
    } else {
        exit('-3');
    }


    $db->commit();
    echo '1'; // update thành công
    exit;
}

echo '0'; // không có id lấy gì mà check

exit;
