<?php
set_time_limit(0);

$page       = $this->getRequest()->getParam('page', 1);
$sort       = $this->getRequest()->getParam('sort', 'total_quantity');
$desc       = $this->getRequest()->getParam('desc', 1);
$export     = $this->getRequest()->getParam('export', 0);
$from       = $this->getRequest()->getParam('from', date('Y-m-01') );
$to         = $this->getRequest()->getParam('to', date('Y-m-d'));

$QMarket = new Application_Model_Market();
$QDistributor = new Application_Model_Distributor();
$name_chart = unserialize(ONLINE_CHANNEL);

$params = array(    
    'page'       => $page,
    'sort'       => $sort,
    'desc'       => $desc,
    'from'       => $from,
    'to'         => $to,    
    'export'     => $export,   
);
$channel = [];
foreach ($name_chart as $key => $value) {
	$channel[] = $key;
}
$params['channel'] = $channel;
$sales = $QMarket->fetchOnlineSellIn($params);

$data = array();
foreach ($sales as $key => $value) {
	$id = $value['channel'];
	if (isset($data[$id])) {
           $data[$id]['sellin'] += $value['sellin'];
           $data[$id]['active'] += $value['active'];
	   $data[$id]['total_price'] += $value['total_price'];
           $data[$id]['total_active'] += $value['total_active'];
	} else {
	   $data[$id]['sellin'] = $value['sellin'];
           $data[$id]['active'] = $value['active'];
	   $data[$id]['total_price'] = $value['total_price'];
           $data[$id]['total_active'] = $value['total_active'];
           $data[$id]['channel']  = $value['channel'];
	}
}

$this->view->sales       = $data;
$this->view->distributor = $QDistributor->get_cache();
$this->view->to          = $to;
$this->view->from        = $from;
$this->view->params      = $params;