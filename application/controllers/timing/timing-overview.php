<?php
// exit('Chức năng tạm thời bảo trì, vui lòng quay lại sau');
$params = $this->_request->getParams();

$limit = LIMITATION;
$page = $this->getRequest()->getParam('page', 1);
$total = 0;

$from = $this->getRequest()->getParam('from');
$to = $this->getRequest()->getParam('to');
$dev = $this->getRequest()->getParam('dev');

if(empty($from) && empty($to))
{
	$from = date("01/m/Y");
	$to = date("d/m/Y");
}

$params = array_filter(array(
	'from' => $from,
	'to' => $to,
	'dev' => $dev
));


$QTimingSale = new Application_Model_TimingSale();
$data = $QTimingSale->selectKpiOverview($page, $limit, $total, $params);



$this->view->data = $data;

$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST.'timing/timing-overview'.( $params ? '?'.http_build_query($params).'&' : '?' );

$this->view->params = $params;