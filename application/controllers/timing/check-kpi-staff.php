<?php
$QCheckKpiStaff = new Application_Model_CheckKpiStaff();
$QLockedKpi = new Application_Model_LockedKpi();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$export_lech_timing = $this->getRequest()->getParam('export_lech_timing');
$export_khong_cong = $this->getRequest()->getParam('export_khong_cong');
$export_khac_chuc_danh = $this->getRequest()->getParam('export_khac_chuc_danh');
$time = $this->getRequest()->getParam('time', date('m-Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0] ? explode('-', $time) [0] : date('m');
$year = explode('-', $time) [1] ? explode('-', $time) [1] : date('Y');
$from_date = date('Y-m-d', strtotime('01-' . $time));
$to_date = date('Y-m-t', strtotime('01-' . $time));

$locked_kpi = $QLockedKpi->fetchRow([
    'month = ?' => $month,
    'year = ?' => $year
]);

$locked_all = $locked_kpi['locked_all'];
$locked_remove_kpi = $locked_kpi['locked_remove_kpi'];
$locked_commit_kpi = $locked_kpi['locked_commit_kpi'];

$params = [
    'month' => $month,
    'year' => $year,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'time' => $time
];


$list_lech_timing = $QCheckKpiStaff->getLechTiming($params);
$list_khong_cong = $QCheckKpiStaff->getKhongCong($params);
$list_khac_chuc_danh = $QCheckKpiStaff->getKhacChucDanh($params);

if ($export_lech_timing) {
    $QCheckKpiStaff->exportLechTiming($list_lech_timing);
}

if ($export_khong_cong) {
    $QCheckKpiStaff->exportKhongCong($list_khong_cong);
}

if ($export_khac_chuc_danh) {
    $QCheckKpiStaff->exportKhacChucDanh($list_khac_chuc_danh);
}

if (! in_array($userStorage->id, [341])) {
    $this->view->only_see = true;
}

$this->view->list_lech_timing = $list_lech_timing;
$this->view->list_khong_cong = $list_khong_cong;
$this->view->list_khac_chuc_danh = $list_khac_chuc_danh;

$this->view->locked_all = $locked_all;
$this->view->locked_remove_kpi = $locked_remove_kpi;
$this->view->locked_commit_kpi = $locked_commit_kpi;
$this->view->params = $params;
