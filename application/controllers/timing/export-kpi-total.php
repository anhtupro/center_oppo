<?php

$QExportKpi = new Application_Model_ExportKpi();

$export_type = $this->getRequest()->getParam('export_type');
$time = $this->getRequest()->getParam('time', date('m/Y'));

$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');

$from_date = date('Y-m-d', strtotime('01-' . $time));
$to_date = date('Y-m-t', strtotime('01-' . $time));

$from_date_time = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date_time = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$params = [
    'month' => $month,
    'year' => $year,
    'time' => $time,
    'from_date' => $from_date,
    'to_date' => $to_date,
    'from_date_time' => $from_date_time,
    'to_date_time' => $to_date_time
];

if (date('Y', strtotime($from_date)) == 1970) {
    echo 'Sai định dạng ngày! Vui lòng chọn đúng theo định dạng mẫu';die;
}

if ($export_type == 1) { // sellout policy summary
   $QExportKpi->exportPolicySummary($params);
}

if ($export_type == 2) { // sellout policy
    $QExportKpi->exportPolicy($params);
}

if ($export_type == 3) { // sellout Staff
    $QExportKpi->exportStaff($params);
}

if ($export_type == 4) { // sellout staff detail
    $QExportKpi->exportStaffDetail($params);
}

if ($export_type == 5) { // kpi sale online
    $QExportKpi->exportKpiSaleOnline($params);
}

if ($export_type == 6) { // kpi PG đứng shop có sellout nhiều nhất
    $QExportKpi->exportFinalStorePg($params);
}

if ($export_type == 7) { // channel
    $QExportKpi->exportChannel($params);
}

if ($export_type == 8) { // channel system
    $QExportKpi->exportChannelSystem($params);
}