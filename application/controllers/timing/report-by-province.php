<?php

$time = $this->getRequest()->getParam('time', date('m/Y'));
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$special_user = [5899, 341, 23154, 326];
if (in_array($userStorage->id, $special_user)) {
    $staff_id = -1 ;
} else {
    $staff_id = $userStorage->id;
}

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'time' => $time,
    'month' => $month,
    'year' => $year
];

$db = Zend_Registry::get('db');
$stmt_out = $db->prepare("CALL `sp_report_province`(:p_from_date, :p_to_date)");
$stmt_out->bindParam('p_from_date', $from_date, PDO::PARAM_STR);
$stmt_out->bindParam('p_to_date', $to_date, PDO::PARAM_STR);
$stmt_out->execute();
$data = $stmt_out->fetchAll();
$stmt_out->closeCursor();


$this->view->data = $data;
$this->view->params = $params;