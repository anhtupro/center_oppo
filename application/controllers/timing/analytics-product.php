<?php
$name       = $this->getRequest()->getParam('name');
$from       = $this->getRequest()->getParam('from', date('01/m/Y'));
$to         = $this->getRequest()->getParam('to', date('d/m/Y'));
$area    = $this->getRequest()->getParam('area');
$province   = $this->getRequest()->getParam('province');
$district   = $this->getRequest()->getParam('district');
$store      = $this->getRequest()->getParam('store');
$sales_from = $this->getRequest()->getParam('sales_from');
$sales_to   = $this->getRequest()->getParam('sales_to');

$page = $this->getRequest()->getParam('page', 1);
$sort = $this->getRequest()->getParam('sort', 'total');
$desc = $this->getRequest()->getParam('desc', 1);

$limit = LIMITATION;
$total = 0;

$params = array(
    'name'            => $name,
    'from'            => $from,
    'to'              => $to,
    'area'         => $area,
    'province' => $province,
    'district'        => $district,
    'store'           => $store,
    'page'            => $page,
    'sort'            => $sort,
    'desc'            => $desc,
    'sales_from'      => $sales_from,
    'sales_to'        => $sales_to,
    );

$QGood = new Application_Model_Good();
$this->view->goods = $QGood->get_cache();

$QArea = new Application_Model_Area();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!$userStorage || !isset($userStorage->id)) $this->_redirect(HOST);

$group_id = $userStorage->group_id;

$QRegion = new Application_Model_RegionalMarket();

if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm = new Application_Model_Asm();
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

} elseif ($group_id == My_Staff_Group::SALES) {
    // lấy cửa hàng của sale
    $QStoreStaffLog = new Application_Model_StoreStaffLog();
    $store_cache = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $params['store_list'] = $store_cache;
    $params['sale_id'] = $userStorage->id;
} elseif ($group_id == My_Staff_Group::LEADER) {
    // lấy cửa hàng của sale
    $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
    $store_cache = $QStoreLeaderLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $params['store_list'] = $store_cache;
    $params['leader_id'] = $userStorage->id;
}

$this->view->areas = $QArea->get_cache();

if ($area) {
    if (is_array($area) && count($area))
        $where = $QRegion->getAdapter()->quoteInto('area_id IN (?)', $area);
    elseif (is_numeric($area) && $area)
        $where = $QRegion->getAdapter()->quoteInto('area_id = ?', intval($area));

    $regions = $QRegion->fetchAll($where, 'name');

    $regions_arr = array();

    foreach ($regions as $key => $value)
        $regions_arr[$value['id']] = $value['name'];

    $this->view->regional_markets = $regions_arr;
}

if ($province) {
    if (is_array($province) && count($province))
        $where = $QRegion->getAdapter()->quoteInto('parent IN (?)', $province);
    elseif (is_numeric($province) && $province)
        $where = $QRegion->getAdapter()->quoteInto('parent = ?', intval($province));

    $region_search = $QRegion->fetchAll($where, 'name');

    $district_arr = array();
    $district_id_list = array();

    foreach ($region_search as $_region) {
        $district_arr[$_region['id']] = $_region['name'];

        if (is_array($district) && count($district) && in_array($_region['id'], $district))
            $district_id_list[] = $_region['id'];
    }

    $this->view->districts = $district_arr;
}

if (isset($district_id_list) && is_array($district_id_list) && count($district_id_list)) {
    $QStore = new Application_Model_Store();
    $where = $QStore->getAdapter()->quoteInto('district IN (?)', $district_id_list);
    $this->view->stores = $QStore->fetchAll($where, 'name');
}

$limit = LIMITATION;
$total = $total2 = 0;

$userStorage     = Zend_Auth::getInstance()->getStorage()->read();
$QImeiKpi = new Application_Model_ImeiKpi();

//if($userStorage->id = 5899){
    $sales = $QImeiKpi->fetchProductNew($page, $limit, $total, $params);	
//}else{
//    $sales = $QImeiKpi->fetchProduct($page, $limit, $total, $params);
//}
 
 

$params['get_total_sales'] = true;
$total_sales = $QImeiKpi->fetchProductNew(null, null, $total2, $params);
unset($params['get_total_sales']);
unset($params['store_list']);
unset($params['area_list']);
unset($params['sale_id']);
unset($params['leader_id']);

$this->view->total_quantity  = $total_sales['total_quantity'];
$this->view->total_activated = $total_sales['total_activated'];
$this->view->sales           = $sales;
$this->view->total           = $total;
$this->view->limit           = $limit;
$this->view->desc            = $desc;
$this->view->offset          = $limit*($page-1);
$this->view->url             = HOST.'timing/analytics-product'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->current_col     = $sort;
$this->view->params          = $params;
