<?php
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();

$QTiming = new Application_Model_Timing();
$QDistributor = new Application_Model_Distributor();

$distributor_id = $this->getRequest()->getParam('distributor_id');
$channel_name = $this->getRequest()->getParam('channel_name');
$from_date = $this->getRequest()->getParam('from_date', date('Y-m-01'));
$to_date = $this->getRequest()->getParam('to_date', date('Y-m-d'));

$distributor_id = explode(",",$distributor_id);

$from_date = str_replace('/', '-', $from_date);
$to_date = str_replace('/', '-', $to_date);

$from_date = date('Y-m-d 00:00:00', strtotime($from_date));
$to_date = date('Y-m-d 23:59:59', strtotime($to_date));

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'distributor_id' => $distributor_id
];

//$distributor = $QDistributor->fetchRow(['id = ?' => $distributor_id]);
$data = $QTiming->getDataImeiKa($params);

$heads = array(
    'Imei',
    'Model',
    'Color',
    'Store',
    'Timing date',
    'Activated date',
    'Staff',
    'Email',
    'Code'
);


$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 2;

$i = 1;

foreach($data as $item ){
    $alpha    = 'A';

    $sheet->getCell($alpha++ . $index)->setValueExplicit($item['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alpha++.$index, $item['model']);
    $sheet->setCellValue($alpha++.$index, $item['color']);
    $sheet->setCellValue($alpha++.$index, $item['store_name']);
    $sheet->setCellValue($alpha++.$index, $item['timing_date']);
    $sheet->setCellValue($alpha++.$index, $item['activated_at']);
    $sheet->setCellValue($alpha++.$index, $item['staff_name']);
    $sheet->setCellValue($alpha++.$index, $item['staff_email']);
    $sheet->setCellValue($alpha++.$index, $item['staff_code']);

    $index++;
}

$filename = TRIM($channel_name);
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
