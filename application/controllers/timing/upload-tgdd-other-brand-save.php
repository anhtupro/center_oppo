<?php
$upload   = $this->getRequest()->getParam('upload');
$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d', strtotime('01-' . $time));
$to_date = date('Y-m-t', strtotime('01-' . $time));

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date
];

$flashMessenger = $this->_helper->flashMessenger;
$random_do_so = date("YmdHis");
$QKpiByModelBrand = new Application_Model_KpiByModelBrand();
$QKpiByModelFileBrand = new Application_Model_KpiByModelFileBrand();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$this->view->staff_id = $userStorage->id;
$brand_id = 14;//one plus

include 'PHPExcel'.DIRECTORY_SEPARATOR.'simplexlsx.class.php';
$db   = Zend_Registry::get('db');

$db->beginTransaction();

if(!empty($upload)){
    if($_FILES['file_oneplus']['name'] != NULL) {
        try {
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'oneplus-do-so';

            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777, true);
            $tmpFilePath = $_FILES['file_oneplus']['tmp_name'];

            if ($tmpFilePath != ""){
                $old_name 	= $_FILES['file_oneplus']['name'];
                $tExplode 	= explode('.', $old_name);
                $extension  = end($tExplode);
                $new_name = 'ONEPLUS-' . md5(uniqid('', true)) . '.' . $extension;
                $newFilePath = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $url= 'oneplus-do-so' . DIRECTORY_SEPARATOR . $new_name;
                }else{
                    $url = NULL;
                }
            }else{
                $url = NULL;
            }
            //đọc file excel
            $inputfile = $url;

            $xlsx = new SimpleXLSX($inputfile);
            $data = $xlsx->rows();
            foreach ($data as $key => $value) {
                $key_ = $key + 1;
                if($data[$key_][0] != "" || $data[$key_][1] != "" || $data[$key_][5] != "" || $data[$key_][7] != "" || $data[$key_][9] != "") {
                    $tgdd = [
                        'timing_date' => trim($data[$key_][0]),
                        'partner_id' => trim($data[$key_][1]),
                        'model' => trim($data[$key_][5]),
                        'color_name' => trim($data[$key_][7]),
                        'qty' => trim($data[$key_][9]),
                        'random_do_so' => $random_do_so,
                    ];
                    $QKpiByModelFileBrand->insert($tgdd);
                } else {
                    $check = $key_ - 1;
                    if($check != $key) {
                        $db->rollBack();
                        $flashMessenger->setNamespace('error')->addMessage("File Oneplus lỗi !!!");
                        $this->redirect(HOST . 'timing/upload-tgdd-other-brand');
                    }
                }
            }
            //unlink($url);


            $QKpiByModelFileBrand->updateBasic($random_do_so);

            $QKpiByModelFileBrand->updateGoodId($random_do_so);

            $wrong_product = $QKpiByModelFileBrand->getWrongProduct($random_do_so, $brand_id);
            if ($wrong_product) {
                $flashMessenger->setNamespace('error')->addMessage( "Các sản phẩm " . implode(', ', $wrong_product) . ' không phải OnePlus');
                $this->redirect(HOST . 'timing/upload-tgdd-other-brand');
            }

            $checkColor = $QKpiByModelFileBrand->checkColor($random_do_so);

            if(empty($checkColor)) {
                $QKpiByModelFileBrand->updateColorId($random_do_so);

            } else {

                $getColor = array();
                foreach($checkColor as $color) {
                    array_push($getColor, $color['desc']);
                }
                $setColor = implode(', ', $getColor);

                $QKpiByModelFileBrand->deleteModelFile($random_do_so);


                $flashMessenger->setNamespace('error')->addMessage( "Màu model: " . $setColor . " đang bị sai,  check lại file excel !!!");
                $this->redirect(HOST . 'timing/upload-tgdd-other-brand');
            }

            $QKpiByModelFileBrand->insertData($random_do_so);
            $QKpiByModelFileBrand->updateValue($params);

            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage( "Import thành công !!!");
            $this->redirect(HOST . 'timing/upload-tgdd-other-brand');
        } catch (Exception $e) {
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: ".$e->getMessage() );
            $this->redirect(HOST . 'timing/upload-tgdd-other-brand');
        }
    } else {
        $flashMessenger->setNamespace('error')->addMessage( "File Oneplus không tồn tại !!!");
        $this->redirect(HOST . 'timing/upload-tgdd-other-brand');
    }
} else {
    $flashMessenger->setNamespace('error')->addMessage( "File Oneplus không tồn tại !!!");
    $this->redirect(HOST . 'timing/upload-tgdd-other-brand');
}
if (!empty($flashMessenger->setNamespace('error')->getMessages()) ){
    $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages_error = $messages_error;

}
if (!empty($flashMessenger->setNamespace('success')->getMessages()) ){
    $messages          = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages = $messages;
}