<?php
$sort = $this->getRequest()->getParam('sort', '');
$desc = $this->getRequest()->getParam('desc', 1);
$page = $this->getRequest()->getParam('page', 1);
$from_date = $this->getRequest()->getParam('from_date', date('Y-m-d', strtotime('- 7 days')));
$to_date = $this->getRequest()->getParam('to_date', date('Y-m-d'));
$area_id = $this->getRequest()->getParam('area_id');
$channel_id = $this->getRequest()->getParam('channel_id');
$store_name = $this->getRequest()->getParam('store_name');
$good_id = $this->getRequest()->getParam('good_id');
$export = $this->getRequest()->getParam('export');
$export_by_model = $this->getRequest()->getParam('export_by_model');
$limit = 15;

$QStoreInventory = new Application_Model_StoreInventory();
$QArea = new Application_Model_Area();
$QGood = new Application_Model_Good();
$QChannel = new Application_Model_Channel();

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'area_id' => $area_id,
    'channel_id' => $channel_id,
    'store_name' => $store_name,
    'export' => $export,
    'good_id' => $good_id,
    'export_by_model' => $export_by_model
];

$list_product = $QGood->fetchAll([
   'is_timing = ?' => 1,
   'cat_id IN (?)' => [PHONE_CAT_ID, IOT_OPPO_CAT_ID]
])->toArray();

$list_area = $QArea->getAreaByTitle();
$list = $QStoreInventory->fetchPagination($page, $limit, $total, $params);
$list_channel = $QChannel->fetchAll([
//    'is_ka = ?' => 1,
    'status = ?' => 1
]);

if ($export) {
    $QStoreInventory->export($list);
}

if ($export_by_model) {
    $QStoreInventory->exportByModel($list);
}

$this->view->list_channel = $list_channel;
$this->view->list = $list;
$this->view->list_area = $list_area;
$this->view->list_product = $list_product;
$this->view->desc = $desc;
$this->view->sort = $sort;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->params = $params;
$this->view->url = HOST.'timing/store-inventory'.($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit * ($page - 1);