<?php
$sort            = $this->getRequest()->getParam('sort');
$desc            = $this->getRequest()->getParam('desc', 1);
$export          = $this->getRequest()->getParam('export', 0);
$from            = $this->getRequest()->getParam('from', date('01/m/Y') );
$to              = $this->getRequest()->getParam('to', date('d/m/Y') );
$area            = $this->getRequest()->getParam('area');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$params = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'area'   => $area,
    'export' => $export,
);

if ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $area_list    = array();
    $QAsm         = new Application_Model_Asm();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $this->view->viewed_area_id = $list_regions;
}

$QArea = new Application_Model_Area();
$QImeiKpi = new Application_Model_ImeiKpi();
$this->view->userStorage = $userStorage;

$data = $QImeiKpi->fetchProvince($params);

$params['get_total_sales'] = true;
$total_sales = $QImeiKpi->fetchProvince($params);
//get total money

$this->view->areas = $QArea->get_cache();

$this->view->total_sales           = $total_sales['total_quantity'];
$this->view->total_sales_activated = $total_sales['total_activated'];
$this->view->sales                 = $data;
$this->view->url                   = HOST.'timing/analytics-province'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->desc                  = $desc;
$this->view->current_col           = $sort;
$this->view->to                    = $to;
$this->view->from                  = $from;
$this->view->params                = $params;