<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$QLockedKpi = new Application_Model_LockedKpi();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$time = $this->getRequest()->getParam('time');

$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d', strtotime('01-' . $time));
$to_date = date('Y-m-t', strtotime('01-' . $time));

if (! $time) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa chọn tháng'
    ]);
    return;
}
if (! in_array($userStorage->id, [341])) {
    echo json_encode([
        'status' => 1,
        'message' => "Không có quyền thao tác!"
    ]);
    return;
}



$where_lock_kpi = [
    'month = ?' => $month,
    'year = ?' => $year
];

$locked_kpi = $QLockedKpi->fetchRow($where_lock_kpi);

if ($locked_kpi['locked_tgdd']) {
    echo json_encode([
       'status' => 1,
       'message' => 'Đã khóa báo số TGDĐ tháng ' . $month . ' rồi .Không thể xử lý thêm'
    ]);
    return;
}

// update status
if ($locked_kpi) {
    $QLockedKpi->update([
        'locked_tgdd' => 1
    ], $where_lock_kpi);
} else {
    $QLockedKpi->insert([
        'month' => $month,
        'year' => $year,
        'locked_tgdd' => 1
    ]);
}

// update main
$db = Zend_Registry::get('db');

$update_query = "
    UPDATE kpi_by_model km
    SET km.status_locked = 1
    WHERE km.timing_date BETWEEN '$from_date' AND '$to_date'
";

$stmt = $db->prepare($update_query);
$stmt->execute();
$stmt->closeCursor();
$db = $stmt = null;


echo json_encode([
    'status' => 0
]);

