<?php



$month = date("n");
 
//Calculate the year quarter.
$quarter = ceil($month / 3);
$year  = date('Y');
if ($this->getRequest()->getMethod() == 'POST') {
    $year   = $this->getRequest()->getParam('year');
    $export = $this->getRequest()->getParam('export', 0);
    $quarter = $this->getRequest()->getParam('quarter',2);
}

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$area_list   = array();
if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {

    $QAsm                       = new Application_Model_Asm();
    $list_regions               = $QAsm->get_cache($userStorage->id);
    $list_regions               = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $this->view->viewed_area_id = $list_regions;

    $asm_cache           = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();
    if (!empty($params['area_list'])) {
        $area_list = implode(",", $params['area_list']);
    }
}

$db                        = Zend_Registry::get('db');
$sql                       = "CALL p_hero_product_rank_by_quarter(:quarter,:year,@total_sellout)";
$stmt                      = $db->prepare($sql);
$stmt->bindParam('quarter', $quarter, PDO::PARAM_INT);
$stmt->bindParam('year', $year, PDO::PARAM_INT);

$stmt->execute();
$data                      = $stmt->fetchAll();

$stmt->closeCursor();
$total_sellout             = $db->fetchOne('select @total_sellout');
$this->view->total_sellout = $total_sellout;
if (isset($export) && $export == 1 && ((in_array($userStorage->id, array(341, 5899))) || $userStorage->group_id == BOARD_ID)) {
    $this->_exportHeroProductQuarter($data);
    exit();
}
$this->view->params      = array(
    'quarter' => $quarter,
    'year'  => $year
);
$this->view->data        = $data;
$this->view->userStorage = $userStorage;
?>