<?php
$page           = $this->getRequest()->getParam('page', 1);
$sort           = $this->getRequest()->getParam('sort', 'product_count');
$desc           = $this->getRequest()->getParam('desc', 1);
$from           = $this->getRequest()->getParam('from', date('01/m/Y'));
$to             = $this->getRequest()->getParam('to', date('d/m/Y'));
$phone_number   = $this->getRequest()->getParam('phone_number');
$name           = $this->getRequest()->getParam('name');
$area_id        = $this->getRequest()->getParam('area_id');
$region_id      = $this->getRequest()->getParam('region_id');
$district       = $this->getRequest()->getParam('district');
$store_id          = $this->getRequest()->getParam('store_id');
$export_timing  = $this->getRequest()->getParam('export_timing');
$distributor_id = $this->getRequest()->getParam('distributor_id');
$title          = $this->getRequest()->getParam('title');

//$limit = LIMITATION;
$limit = 10000;


$params = array(
    'page'           => $page,
    'sort'           => $sort,
    'desc'           => $desc,
    'from'           => $from,
    'to'             => $to,
    'phone_number'   => $phone_number,
    'name'           => $name,
    'area_id'        => $area_id,
    'region_id'      => $region_id,
    'district'       => $district,
    'store_id'          => $store_id,
    'group_id'       => 4,
    'distributor_id' => $distributor_id,
    'title'          => $title,
);

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id = $userStorage->group_id;

$this->view->group_id = $group_id;
$this->view->id = $userStorage->id;

if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id))
    $params['asm'] = $userStorage->id;
    

$total = 0;
$QImeiKpi = new Application_Model_ImeiKpi();
$sales = $QImeiKpi->fetchByMonthAllStaff($page, $limit, $total, $params);

$params['get_total_sales'] = 1;
$total_2 = 0;
$total_sales = $QImeiKpi->fetchByMonthAllStaff(1, null, $total_2, $params);


$data = array();
$title_list = array();

foreach ($sales as $_sellout) {
    $title_list[ $_sellout['year'] ][ $_sellout['month'] ] = 1;
    $data[ $_sellout['staff_id'] ][ $_sellout['year'] ][ $_sellout['month'] ] = array(
        'quantity' => $_sellout['quantity'],
    );
}

ksort($title_list);

foreach ($title_list as $key => $value){
    ksort($title_list[$key]);
}


$QArea           = new Application_Model_Area();
$QTeam           = new Application_Model_Team();
$QStaff          = new Application_Model_Staff();
$QDistributor    = new Application_Model_Distributor();
$QRegionalMarket = new Application_Model_RegionalMarket();
$QStore          = new Application_Model_Store();

$this->view->staff_cache = $QStaff->get_all_sale_cache_Tuan();
$this->view->distributor_cache = $QDistributor->get_cache();

$district_id_arr = array();
$province_id_arr = array();

if (isset($params['asm']) && $params['asm']) {
    $QAsm          = new Application_Model_Asm();
    $list_regions  = $QAsm->get_cache($params['asm']);
    $list_province = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();
    $list_area     = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
}

$area_cache = array();

if (isset($params['asm']) && $params['asm']) {
    if (isset($list_area) && is_array($list_area) && count($list_area))
        $where = $QArea->getAdapter()->quoteInto('id IN (?)', $list_area);
    else
        $where = $QArea->getAdapter()->quoteInto('1=0', 1);
} else {
    $where = $QArea->getAdapter()->quoteInto('1 = ?', 1);
}

$area_result = $QArea->fetchAll($where, 'name');
if ($area_result) {
    foreach ($area_result as $key => $value) {
        $area_cache[ $value['id'] ] = $value['name'];
    }
}

if ($area_id) {
    $province_arr = array();

    if (is_array($area_id) && count($area_id)) {
        foreach ($area_id as $key => $value) {
            if (isset($params['asm']) && $params['asm'] && !in_array($value, $list_area))
                unset($area_id[$key]);
        }

        foreach ($area_id as $key => $value) {
            $tmp = $QRegionalMarket->nget_province_by_area_cache($value);
            $province_arr = $province_arr + (is_array($tmp) ? $tmp : array());

            $tmp = $QRegionalMarket->nget_district_id_by_area_cache($value);
            $district_id_arr = array_merge($district_id_arr, is_array($tmp) ? $tmp : array());
        }

    } elseif (is_numeric($area_id) && $area_id) {
        if (!in_array($region_id, $list_province)) {
            $province_arr = array();
            $district_id_arr = array();
        } else {
            $province_arr = $QRegionalMarket->nget_province_by_area_cache($area_id);
            $district_id_arr = $QRegionalMarket->nget_district_id_by_area_cache($area_id);
        }
    }

    $this->view->regions = $province_arr;
}

if ($region_id) {
    if (is_array($region_id) && count($region_id)) {
        foreach ($region_id as $key => $value) {
            if (isset($params['asm']) && $params['asm'] && !in_array($value, $list_province))
                unset($region_id[$key]);

            $tmp = $QRegionalMarket->nget_district_id_by_province_cache($value);
            $district_id_arr = array_merge($district_id_arr, is_array($tmp) ? $tmp : array());
        }

    } elseif (is_numeric($region_id) && $region_id) {
        if (!in_array($region_id, $list_province))
            $district_id_arr = array();
        else
            $district_id_arr = $QRegionalMarket->nget_district_id_by_province_cache($region_id);
    }
}

if (isset($district_id_arr) && is_array($district_id_arr) && count($district_id_arr)) {
    $db = Zend_Registry::get('db');
    $stores = $QStore->get_by_district_id_list($district_id_arr);
} else {
    $stores = array();
}

if (isset($export_timing) && $export_timing == 1) {
    $this->_exportExcel_timing_by_month($params);
    exit;
}

unset($params['asm']);
unset($params['get_total_sales']);
unset($params['group_id']);

$this->view->provinces   = $QRegionalMarket->get_cache_all();
$this->view->sellout     = $data;
$this->view->stores      = $stores;
$this->view->title_list  = $title_list;
$this->view->teams       = $QTeam->get_cache_salesmantuan();
$this->view->desc        = $desc;
$this->view->current_col = $sort;
$this->view->to          = $to;
$this->view->from        = $from;
$this->view->areas       = $area_cache;
$this->view->params      = $params;
$this->view->total_sales = $total_sales;
$this->view->offset      = $limit*($page-1);
$this->view->total       = $total;
$this->view->limit       = $limit;
$this->view->url         = HOST.'timing/analytics'.( $params ? '?'.http_build_query($params).'&' : '?' );
