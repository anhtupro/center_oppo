<?php

if (defined("LOCK_TIMING") && LOCK_TIMING) {
    $this->_helper->viewRenderer->setRender('lock');
    return;
}

$id                = $this->getRequest()->getParam('id');
$is_rm             = $this->getRequest()->getParam('is_rm', 0);
$QTiming           = $QTime             = new Application_Model_Timing();
$QTimingSale       = new Application_Model_TimingSale();
$installment_model = new Application_Model_ImeiInstallment();
if ($is_rm == 1) {

    $QTiming     = $QTime       = new Application_Model_TimingRm();
    $QTimingSale = new Application_Model_TimingSaleRm();
}
$QStoreStaffLog = new Application_Model_StoreStaffLog();
$QLeaderLog     = new Application_Model_StoreLeaderLog();
$flashMessenger = $this->_helper->flashMessenger;
$userStorage    = Zend_Auth::getInstance()->getStorage()->read();

$timing = $QTiming->find($id)->current();
if (!$timing) {
    $flashMessenger->setNamespace('error')->addMessage('Invalid Report!');
    $this->_redirect('/timing');
}

$date             = explode(' ', $timing->from);
$this->view->from = substr($date[1], 0, -3);
$tem              = explode('-', $date[0]);
$this->view->date = $tem[2] . '/' . $tem[1] . '/' . $tem[0];
$to               = explode(' ', $timing->to);
$this->view->to   = substr($to[1], 0, -3);

//check quyền view
//check có phải là quản lý của store
if ($userStorage->id != $timing->staff_id and ! in_array($userStorage->group_id, array(ADMINISTRATOR_ID, SALES_EXT_ID))) {

    if ($userStorage->group_id == SALES_ID) {

        if (!$QStoreStaffLog->belong_to($userStorage->id, $timing['store'], $timing['from'], true)) {
            $salesTeamErr = 'Bạn không phải người quản lý của Store!';
            $flashMessenger->setNamespace('error')->addMessage($salesTeamErr);
            $back_url     = $this->getRequest()->getServer('HTTP_REFERER');
            $this->_redirect(($back_url ? $back_url : '/timing'));
        }
    } elseif ($userStorage->group_id == LEADER_ID) {

        if (!$QStoreStaffLog->belong_to($userStorage->id, $timing['store'], $timing['from'], true) && !$QLeaderLog->is_leader($userStorage->id, $timing['store'], $timing['from'])) {
            $salesTeamErr = 'Bạn không phải người quản lý/leader của Store!';
            $flashMessenger->setNamespace('error')->addMessage($salesTeamErr);
            $back_url     = $this->getRequest()->getServer('HTTP_REFERER');
            $this->_redirect(($back_url ? $back_url : '/timing'));
        }
    } elseif ($userStorage->group_id == ASM_ID || $userStorage->group_id == ASMSTANDBY_ID) {
        $QAsm = new Application_Model_Asm();

        if (!$QAsm->is_asm($userStorage->id, $timing['store'])) {
            $salesTeamErr = 'Bạn không phải ASM khu vực này';
            $flashMessenger->setNamespace('error')->addMessage($salesTeamErr);
            $back_url     = $this->getRequest()->getServer('HTTP_REFERER');
            $this->_redirect(($back_url ? $back_url : '/timing'));
        }
    }
}


$dateObj = new DateTime($timing->from);


$this->view->timing = $timing;

$QShift             = new Application_Model_Shift();
$_shifts            = $QShift->get_cache();
$this->view->shifts = $_shifts;

$where_time[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $timing['staff_id']);
$where_time[] = $QTime->getAdapter()->quoteInto('DAY(created_at) = ?', $dateObj->format('d'));
$where_time[] = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $dateObj->format('m'));
$where_time[] = $QTime->getAdapter()->quoteInto('YEAR(created_at) = ?', $dateObj->format('Y'));
$resultTiming = $QTime->fetchRow($where_time);
if (isset($resultTiming) and $resultTiming) {
    //tạm thời dùng cho sale
    $shift                    = $resultTiming['shift'];
    $this->view->shiftTiming  = $_shifts[$shift];
    $this->view->resultTiming = $resultTiming->toArray();
}


//get staff info
$QStaff            = new Application_Model_Staff();
$where             = $QStaff->getAdapter()->quoteInto('id = ?', $timing->staff_id);
$this->view->staff = $QStaff->fetchRow($where);

//timing sale
$where        = $QTimingSale->getAdapter()->quoteInto('timing_id = ?', $id);
$timing_sales = $QTimingSale->fetchAll($where);

$QGoodColor         = new Application_Model_GoodColor();
$QGoodColorCombined = new Application_Model_GoodColorCombined();
$data               = array();
if ($timing_sales->count()) {
    foreach ($timing_sales as $item) {
        $where             = $QGoodColorCombined->getAdapter()->quoteInto('good_id = ?', $item->product_id);
        $goodColorCombined = $QGoodColorCombined->fetchAll($where);

        $temp   = array();
        foreach ($goodColorCombined as $jd)
            $temp[] = $jd->good_color_id;

        if (is_array($temp) && count($temp) > 0)
            $where = $QGoodColor->getAdapter()->quoteInto('id IN (?)', $temp);
        elseif ($temp)
            $where = $QGoodColor->getAdapter()->quoteInto('id = ?', $temp);
        else
            $where = $QGoodColor->getAdapter()->quoteInto('1=0', 1);

        $goodColor         = $QGoodColor->fetchAll($where, 'name');
        $where_installment = $installment_model->getAdapter()->quoteInto('timing_sale_id = ?', $item->id);
        $installment_info  = $installment_model->fetchRow($where_installment);

        if (!empty($installment_info)) {
            $installment_info = $installment_info->toArray();
        }
        $data_temp = array(
            'id'            => $item->id,
            'photo'         => $item->photo,
            'product_id'    => $item->product_id,
            'model_id'      => $item->model_id,
            'models'        => $goodColor,
            'customer_name' => $item->customer_name,
            'phone_number'  => $item->phone_number,
            'address'       => $item->address,
            'imei'          => $item->imei,
        );
       
        if (!empty($installment_info)) {
            $data_temp['installment_id']       = $installment_info['id'];
            $data_temp['installment_company']  = $installment_info['installment_company'];
            $data_temp['installment_contract'] = $installment_info['contract_number'];
        }
        $data[]=$data_temp;
    }
}
$this->view->timing_sales = $data;

//timing accessories
$QTimingAccessories             = new Application_Model_TimingAccessories();
$where                          = $QTimingAccessories->getAdapter()->quoteInto('timing_id = ?', $id);
$timing_accessories             = $QTimingAccessories->fetchAll($where);
$this->view->timing_accessories = $timing_accessories;

//get accessories
$QAccessories            = new Application_Model_Good();
$where                   = $QAccessories->getAdapter()->quoteInto('cat_id = ?', ACCESS_CAT_ID);
$Accessories             = $QAccessories->fetchAll($where, 'desc');
$this->view->accessories = $Accessories;

//get product
$QGood             = new Application_Model_Good();
$where             = $QGood->getAdapter()->quoteInto('cat_id IN (?)', [PHONE_CAT_ID, IOT_OPPO_CAT_ID]);
$goods             = $QGood->fetchAll($where, 'desc');
$this->view->goods = $goods;

// load all model
$QGoodColor = new Application_Model_GoodColor();
$result     = $QGoodColor->fetchAll();

$data = null;
if ($result->count()) {
    foreach ($goods as $good) {
        $colors = explode(',', $good->color);

        $temp = array();
        foreach ($result as $item) {
            if (in_array($item['id'], $colors)) {
                $temp[] = array(
                    'id'    => $item->id,
                    'model' => $item->name,
                );
            }
        }
        $data[$good['id']] = $temp;
    }
}
$this->view->good_colors = json_encode($data);

//get store
$QStore         = new Application_Model_Store();
$QStoreStaffLog = new Application_Model_StoreStaffLog();
$staff_id_edit  = false;
if ($id && isset($timing['staff_id']))
    $staff_id_edit  = $timing['staff_id'];
else
    $staff_id_edit  = $userStorage->id;

$store_id_list = $QStoreStaffLog->get_stores($staff_id_edit, $timing['from']);
$where         = array();

if (is_array($store_id_list) && count($store_id_list) > 0)
    $where[] = $QStore->getAdapter()->quoteInto('id IN (?)', $store_id_list);

// Nếu mà Leader xem timing ko phải ở store của nó (làm sales) thì
// chỉ load cửa hàng ứng với chấm công
elseif ($userStorage->group_id == LEADER_ID && !empty($timing['store']))
    $where[] = $QStore->getAdapter()->quoteInto('id = ?', $timing['store']);

elseif (!in_array($userStorage->group_id, array(ADMINISTRATOR_ID)))
    $where[] = $QStore->getAdapter()->quoteInto('1=0', 1);

$this->view->stores = $QStore->fetchAll($where, 'name');
//

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

//back url
$this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');
