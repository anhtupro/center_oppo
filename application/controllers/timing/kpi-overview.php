<?php

$params = $this->_request->getParams();
$ik     = new Application_Model_NewImeiKpi();
$dev = $this->getRequest()->getParam('dev');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!$userStorage || !isset($userStorage->id))
    $this->_redirect(HOST);

$data = array(
    'staff_id' => $userStorage->id,
    'from'     => date('Y-m-01 00:00:00'),
    'to'       => date('Y-m-d H:i:s')
);

if (!empty($params['filter'])) {
    $data['from'] = $params['from'] ? DateTime::createFromFormat('d/m/Y', $params['from'])->format('Y-m-d') : date('Y-m-01 00:00:00');
    $data['to']   = $params['to'] ? DateTime::createFromFormat('d/m/Y', $params['to'])->format('Y-m-d') : date('Y-m-d H:i:s');
}


$data_new = $ik->Get_All_New_Full($data);
$this->view->res_new      = $data_new;


$this->view->params   = $params;
$this->view->userData = $userStorage;
