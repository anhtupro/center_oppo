<?php

$export    = $this->getRequest()->getParam('export', 0);
$month = $this->getRequest()->getParam('month',date("m"));
$year = $this->getRequest()->getParam('year',date("Y"));

$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$params           = array(
    'month'   => $month,
    'year'   => $year,
);
$QAsm       = new Application_Model_Asm();
$asm_cache  = $QAsm->get_cache();
$QReportArea            = new Application_Model_ReportAreaFinal();
$where=[];
$where[] =  $QReportArea->getAdapter()->quoteInto('month = ?', $month);
$where[] =  $QReportArea->getAdapter()->quoteInto('year = ?', $year);
$list= $QReportArea->fetchAll($where);
$this->view->userStorage=$userStorage;
$area_list = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();

$this->view->area_list                 = $area_list;
$this->view->list                 = $list->toArray();
$this->view->url                   = HOST . 'timing/analytics-area-final' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->params                = $params;





