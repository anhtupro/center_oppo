<?php
$sort      = $this->getRequest()->getParam('sort');
$desc      = $this->getRequest()->getParam('desc', 1);
$export    = $this->getRequest()->getParam('export', 0);
$dealer_id = $this->getRequest()->getParam('dealer_id', 0);
/*$from      = $this->getRequest()->getParam('from', date('01/m/Y'));
$to        = $this->getRequest()->getParam('to', date('d/m/Y'));*/
$time     = $this->getRequest()->getParam('time', date('m/Y'));
$area      = $this->getRequest()->getParam('area');

$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$staff_id         = $userStorage->id;
/*if($staff_id != 5899) {
	echo '<pre>'; print_r('Hệ thống đang nâng cấp!!!'); die;
}*/

$time_seg = explode('/', $time);
$month    = $time_seg[0];
$year     = $time_seg[1];
$from = '01/' . $month . '/' . $year;
$dateFormat = $year . '-' . $month . '-01';
$to = date('t/m/Y', strtotime($dateFormat));

//echo '<pre>'; print_r($to); die;

$params           = array(
    'sort'   => $sort,
    'desc'   => $desc,
    'from'   => $from,
    'to'     => $to,
    'month'   => $month,
    'year'     => $year,
    'area'   => $area,
    'export' => $export,
);
$QTeam            = new Application_Model_Team();
$staff_title_info = $userStorage->title;
$team_info        = $QTeam->find($staff_title_info);
$team_info        = $team_info->current();
$group_id         = $team_info['access_group'];

$db          = Zend_Registry::get('db');
$date_target = $this->getRequest()->getParam('to', date('d/m/Y'));
$date_target = date_create_from_format("d/m/Y", $date_target)->format("Y-m-d");
$sql_target  = "SELECT area_id, SUM(`sell_out`) AS 'target' 
							FROM `target_sales` 
							WHERE `month` = MONTH('$date_target') 
										AND `year` = YEAR('$date_target')
										AND area_id IN (SELECT area_id FROM asm WHERE staff_id = :staff_id)
							GROUP BY area_id			";
$stmt_target = $db->prepare($sql_target);
$stmt_target->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
$stmt_target->execute();
$info_target = $stmt_target->fetchAll();
$stmt_target->closeCursor();
$QTeam       = New Application_Model_Team();
$title       = $userStorage->title;
$team        = $QTeam->find($title);
$team_group  = $team->current();
$group_id    = $team['access_group'];
if (in_array($userStorage->id, array(5899, 341, 765, 156)) || in_array($group_id, array(
            ADMINISTRATOR_ID,
            HR_ID,
            HR_EXT_ID,
            BOARD_ID,
            SALES_ADMIN_ID))) {
    $sql_target  = "SELECT area_id, SUM(`sell_out`) AS 'target' 
							FROM `target_sales` 
							WHERE `month` = MONTH('$date_target') 
										AND `year` = YEAR('$date_target')
										
							GROUP BY area_id			";
    $stmt_target = $db->prepare($sql_target);
//    $stmt_target->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
    $stmt_target->execute();
    $info_target = $stmt_target->fetchAll();
    $stmt_target->closeCursor();
}
$targetArea = array();
if (!empty($info_target[0])) {

    foreach ($info_target as $target) {
        $targetArea[$target['area_id']] = $target['target'];
    }
}
$this->view->targetArea = $targetArea;
 $area_list = '';

 $area_list2 = array();
if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $area_list                  = array();
    $QAsm                       = new Application_Model_Asm();

    $list_regions               = $QAsm->get_cache($userStorage->id);
    $list_regions               = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    $this->view->viewed_area_id = $list_regions;
    $asm_cache                  = $QAsm->get_cache();
    $params['area_list']        = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();
    if(!empty($params['area_list'])){
        $area_list2 = implode(",",$params['area_list']);
    }
    
}

$QArea                   = new Application_Model_Area();
$QImeiKpi                = new Application_Model_ImeiKpi();
$this->view->userStorage = $userStorage;

//build chốt số data
$from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
$to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");
$from_f     = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d 00:00:00");
$to_f       = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d 23:59:59");
$getMonthYear = explode('-', $from);
if($getMonthYear[1] < 10) {
	$getMonth = str_replace( 0, '', $getMonthYear[1]);
} else {
	$getMonth = $getMonthYear[1];
}
$getYear = $getMonthYear[0];

$QAreaRankByMonth  = new Application_Model_AreaRankByMonth();
$getDataChot = $QAreaRankByMonth->getDataChot($getMonth, $getYear);
$data = array();
$totalUnit = $totalActive = $totalValue80 = 0;

if(!empty($getDataChot)) {
	foreach($getDataChot as $value) {
		$totalUnit += $value['total_quantity'];
		$totalActive += $value['total_activated'];
		$totalValue80 += $value['total_value'];
	}
} else {	
	$stmt_out = $db->prepare("CALL `PR_fetch_Area_Point`(:p_from_date, :p_to_date, :p_area_list)");
	$stmt_out->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
	$stmt_out->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
	$stmt_out->bindParam('p_area_list', $area_list, PDO::PARAM_STR);
	$stmt_out->execute();
	$data = $stmt_out->fetchAll();
	$stmt_out->closeCursor();
	$stmt_out = null;
}

	
		
//}else{
//	$data                    = $QImeiKpi->fetchArea($params);
//}
//$data                    = $QImeiKpi->fetchArea($params);
if ($dealer_id) {
    $params['dealer_id'] = $dealer_id;
    $dealer              = $QImeiKpi->fetchArea($params);

    $data_dealer = [];
    foreach ($dealer as $key => $value) {
        $data_dealer[$value['area_id']] = $value;
    }
    $this->_exportAreaKa($data, $data_dealer);
}

$params['get_total_sales'] = true;



//$total_sales       = $QImeiKpi->fetchArea($params);
//$total_money       = $total_sales['total_value'];
//$total_area        = $total_sales['total_quantity'];
//$total_area_active = $total_sales['total_area_active'];

$total_money = $total_area =  $total_area_active = 0;
foreach ($data as $item){
    $total_area         += $item['total_quantity'];
    $total_area_active  += $item['total_activated'];
    $total_money        += $item['total_value'];
}
$sales = array();
  
foreach ($data as $item) {

    $point                              = ( $total_money > 0 and ( $item ['region_share'] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($item ['region_share'] / 100), 2) : 0;
    $val                                = $item;
    $val['point_cu']                    = $point;
    $val['ti_le_chua_active']           = round(( ( $item['total_quantity'] - $item['total_activated'] ) / $item['total_quantity'] ) * 100, 1);
    $val['ti_le_active_ca_nuoc']        = round((($total_area - $total_area_active) / $total_area ) * 100, 1);
    $active_vuot                        = round(( ( $item['total_quantity'] - $item['total_activated'] ) / $item['total_quantity'] ) * 100, 1) - round((($total_area - $total_area_active) / $total_area ) * 100, 1);
    $val['active_vuot']                 = $active_vuot;
    $val['value_exclude_special_model'] = $val['total_value'] - $val['value_a71k'];
     $val['value_exclude_special_model'] = $val['total_value'] - $val['value_a71k'];
      $val['value_exclude_special_model'] = $val['total_value'] - $val['value_a71k'];
    if ($active_vuot <= 0) {
        $val['value_kpi']                       = $item ['total_value'];
        $val['value_kpi_exclude_special_model'] = $val['value_exclude_special_model'];
        $val['value_kpi_a71k_model']            = $item['value_a71k'];
    } else {
        $val['value_kpi']                       = $item ['total_value'] * ( (100 - $active_vuot) / 100 );
        $val['value_kpi_exclude_special_model'] = $val ['value_exclude_special_model'] * ( (100 - $active_vuot) / 100 );
        $val['value_kpi_a71k_model']            = $item ['value_a71k'] * ( (100 - $active_vuot) / 100 );
    }

    $sales[] = $val;
}

$sales_final = array();


//Lay tong value kpi cua ca nuoc
    $area_list = 0;
    $stmt_all = $db->prepare("CALL `PR_fetch_Area_Point`(:p_from_date, :p_to_date, :p_area_list)");
    $stmt_all->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
    $stmt_all->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
    $stmt_all->bindParam('p_area_list', $area_list, PDO::PARAM_STR);
    $stmt_all->execute();
    $data_all = $stmt_all->fetchAll();
    $stmt_all->closeCursor();
    $stmt_all = null;



foreach ($data_all as $item) {
    $val                                = $item;
    $active_vuot                        = round(( ( $item['total_quantity'] - $item['total_activated'] ) / $item['total_quantity'] ) * 100, 1) - round((($total_area - $total_area_active) / $total_area ) * 100, 1);
    if ($active_vuot <= 0) {
        $val['value_kpi']                       = $item ['total_value'];
    } else {
        $val['value_kpi']                       = $item ['total_value'] * ( (100 - $active_vuot) / 100 );
    }
    $sales_all[] = $val;
}
//$total_value =  
$total_value_kpi = 0;
foreach ($sales_all as $key => $value) {
   $total_value_kpi += $value['value_kpi'];
//   $total_value     += $value ['total_value'];
}
//End Lay tong value kpi cua ca nuoc
//echo count($sales);
foreach ($sales as $item) {
    $point_moi     = round(($item['value_kpi'] / $total_value_kpi) * 60 / ($item['region_share'] / 100), 2);
    $val           = $item;
    $val['point']  = $point_moi;
    $sales_final[] = $val;
}
//echo count($sales_final);
unset($params['kpi']);
unset($params['get_total_sales']);
usort($sales_final, array($this, 'cmp'));

$this->view->areas           = $QArea->get_cache();
$this->view->total_value_kpi = $total_value_kpi;

if(!empty($getDataChot)) {
	$this->view->total_value     = $totalValue80;
	$this->view->total_sales           = $totalUnit;
	$this->view->total_sales_activated = $totalActive;
	$this->view->sales = $getDataChot;
} else {
	$this->view->total_value     = $total_money;
	$this->view->total_sales           = $total_area;
	$this->view->total_sales_activated = $total_area_active;
	$this->view->sales = $sales_final;
}

$this->view->area_check_list = $area_list2;
$this->view->url                   = HOST . 'timing/analytics-area' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->desc                  = $desc;
$this->view->current_col           = $sort;
$this->view->to                    = $to;
$this->view->from                  = $from;
$this->view->params                = $params;





