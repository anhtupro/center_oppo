<?php
$this->_helper->layout->disableLayout();

$QKpiByModel = new Application_Model_KpiByModel();

$store_name = TRIM($this->getRequest()->getParam('store_name'));
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');

$from_date = str_replace('/', '-', $from_date);
$to_date = str_replace('/', '-', $to_date);

$from_date = date('Y-m-d', strtotime($from_date));
$to_date = date('Y-m-d', strtotime($to_date));



$params = [
    'store_name' => $store_name,
    'from_date' => $from_date,
    'to_date' => $to_date
];

$data = $QKpiByModel->getDataStore($params);

$this->view->data = $data;
$this->view->params = $params;
