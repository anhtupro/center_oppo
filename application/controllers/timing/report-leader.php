<?php

$this->view->year = date('Y');
$this->view->month = date('m');

$sort			= $this->getRequest()->getParam('sort');
$desc			= $this->getRequest()->getParam('desc', 1);
$area			= $this->getRequest()->getParam('area');
$month			= $this->getRequest()->getParam('month',date('m'));
$year			= $this->getRequest()->getParam('year',date('Y'));
$export			= $this->getRequest()->getParam('export', 0);
$userStorage	= Zend_Auth::getInstance()->getStorage()->read();

$params = array(
	'sort'		=> $sort,
	'desc'		=> $desc,
	'month'		=> $month,
	'year'		=> $year,
	'area'		=> $area,
	'export'	=> $export,
	
);


$QArea                   = new Application_Model_Area();
if ( isset($export) && $export ) {
    $this->_forward('leader', 'kpi-report', null, array('params' => $params));
    return;
}

if ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    
    $area_list    = array();
    $QAsm         = new Application_Model_Asm();
    $area = $QAsm->get_cache($userStorage->id);
    $area = isset($area['area']) && is_array($area['area']) ? $area['area'] : array();
    
}

$QReportLeader = new Application_Model_ReportLeader();
$data = $QReportLeader->fetch($params,$area);

$this->view->sellout = $QReportLeader->getTotalSellout($params);

$this->view->params         = $params;
$this->view->areas			= $QArea->get_cache();
$this->view->userStorage	= $userStorage;
$this->view->sales			= $data; 

// $params = array(
// 	'area' => $list_regions
// );

// echo "<pre>";print_r($params);die;