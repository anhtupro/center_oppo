<?php
$this->_helper->layout()->disableLayout(true);
$this->_helper->viewRenderer->setNoRender(true);

$QDeleteImeiTmp = new Application_Model_DeleteImeiTmp();
$QLockedKpi = new Application_Model_LockedKpi();

$db = Zend_Registry::get("db");
$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$time = $this->getRequest()->getParam('time');
$note = $this->getRequest()->getParam('note');
$list_imei = $this->getRequest()->getParam('list_imei');

$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$current_month = date('m');
$current_year = date('Y');

$diff_month = (($current_year - $year) * 12) + ($current_month - $month);
$locked_kpi = $QLockedKpi->fetchRow([
    'month = ?' => $month,
    'year = ?' => $year
]);

$list_imei = TRIM($list_imei);
$time = My_Util::escape_string($time);
$note =  TRIM($note);

$arr_imei = explode("\n", $list_imei);



$params = [
  'from_date' => $from_date,
  'to_date' => $to_date,
    'note' => $note
];

if (! in_array($userStorage->id, [341])) {
    echo json_encode([
        'status' => 1,
        'message' => "Không có quyền thao tác!"
    ]);
    return;
}

if (! $time) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa chọn tháng!'
    ]);

    return;
}

if ($diff_month > 1) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không thể xử lý KPI quá 1 tháng trước !'
    ]);
    return;
}

if ($locked_kpi['locked_all']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã chốt KPI tháng ' . $time . ' . Không thể xử lý thêm'
    ]);
    return;
}

if ($locked_kpi['locked_commit_kpi']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã chốt để xử lý 3 file không công, lệch timing, khác chức danh tháng ' . $time . ' . Không thể xử lý thêm'
    ]);
    return;
}

if (! $note) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa điền ghi chú!'
    ]);

    return;
}

if (! $list_imei) {
    echo json_encode([
        'status' => 1,
        'message' => 'Chưa điền imei'
    ]);

    return;
}

$insert_header_query = "INSERT INTO delete_imei_tmp(imei) VALUES ";
$insert_body_query = '';

foreach ($arr_imei as $imei) {
    $imei = My_Util::escape_string(TRIM($imei));
    if ($imei) {
        // insert
        $value_insert = "('" . $imei . "'),";
        $insert_body_query .= $value_insert;
    }

}


if ($insert_body_query) {
    $insert_query = $insert_header_query . $insert_body_query;
    $insert_query = TRIM($insert_query, ',');

    if($insert_query){
        $db->query('TRUNCATE TABLE delete_imei_tmp');
        $stmt = $db->prepare($insert_query);
        $stmt->execute();
        $stmt->closeCursor();
    }
}
// check trùng trong danh sách imei
$duplicate_imei = $QDeleteImeiTmp->getDuplicateImei();
if ($duplicate_imei) {
    echo json_encode([
        'status' => 1,
        'message' => 'Imei bị trùng: '  . implode("\n", $duplicate_imei) . '. Vui lòng kiểm tra và submit lại'
    ]);

    return;
}

// check ko tồn tại trong timing_sale và trong tháng đã chọn
$invalid_imei = $QDeleteImeiTmp->getInvalidImei($params);
if ($invalid_imei) {
    echo json_encode([
        'status' => 1,
        'message' => 'Imei không có báo số trong ' . $time .' : ' . implode("\n", $invalid_imei) . '. Vui lòng kiểm tra và submit lại'
    ]);

    return;
}


$db->beginTransaction();
// lưu log và xóa imei
$stmt_out = $db->prepare("CALL `sp_delete_imei_timing`(:p_from_date, :p_to_date, :p_note)");
$stmt_out->bindParam('p_from_date', $params['from_date']);
$stmt_out->bindParam('p_to_date', $params['to_date']);
$stmt_out->bindParam('p_note', $params['note'], PDO::PARAM_STR);
$stmt_out->execute();
$stmt_out->closeCursor();

$db->query('TRUNCATE TABLE delete_imei_tmp');
$db->commit();

echo json_encode([
    'status' => 0
]);



