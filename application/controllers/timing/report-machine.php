<?php

$sort      = $this->getRequest()->getParam('sort');
$time = $this->getRequest()->getParam('time', date('m/Y'));
$area = $this->getRequest()->getParam('area');

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;
//if ($staff_id != 341) {
//    echo 'Hệ thống đang bảo trì vui lòng quay lại sau';die;
//}
$time_seg = explode('/', $time);
$month = $time_seg[0];
$year = $time_seg[1];
$from = '01/' . $month . '/' . $year;
$dateFormat = $year . '-' . $month . '-01';
$to = date('t/m/Y', strtotime($dateFormat));

$from_f = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $from)));
$to_f = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $to)));
$params = array(
    'from' => $from,
    'to' => $to,
    'month' => $month,
    'year' => $year,
    'area' => $area,
    'from_f' => $from_f,
    'to_f' => $to_f
);
$QArea = new Application_Model_Area();
$QImeiKpi = new Application_Model_ImeiKpi();
$QTeam = new Application_Model_Team();
$QAsm = new Application_Model_Asm();
$QAreaRankByMonth = new Application_Model_AreaRankByMonth();
$QAreaRankMoney = new Application_Model_AreaRankMoney();

$db = Zend_Registry::get('db');
$list_area_kpi = $QArea->getAreaKpi();
$area_list = '';


if (in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $area_list = array();
    $list_regions = $QAsm->get_cache($userStorage->id);
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();

    $list_area_privilege = $list_regions;
}

$getDataFinal = $QAreaRankByMonth->getDataChot($month, $year);

if ($getDataFinal['data_area'] ) {
    $data_all =  $getDataFinal['data_area'];
    $total_quantity =  $getDataFinal['total_quantity'];
    $total_quantity_activated =  $getDataFinal['total_quantity_activated'];
    $total_value =  $getDataFinal['total_value'];
    $total_value_kpi =  $getDataFinal['total_value_kpi'];
} else {

    $data_all = $QAreaRankByMonth->getData($params);

    $total_quantity = $total_quantity_activated = $total_value = $total_value_kpi = 0;
    foreach ($data_all as $item) {
        $total_quantity += $item['quantity'];
        $total_quantity_activated += $item['quantity_activated'];
        $total_value += $item['value'];
        $total_value_kpi += $item['value_kpi'];
    }
}


$this->view->data_all = $data_all;
$this->view->total_quantity = $total_quantity;
$this->view->total_quantity_activated = $total_quantity_activated;
$this->view->total_value = $total_value;
$this->view->total_value_kpi = $total_value_kpi;
$this->view->list_area_privilege = $list_area_privilege;


$this->view->url = HOST . 'timing/report-machine' . ($params ? '?' . http_build_query($params) . '&' : '?');
$this->view->desc = $desc;
$this->view->to = $to;
$this->view->from = $from;
$this->view->params = $params;
$this->view->userStorage = $userStorage;
$this->view->is_special_user = in_array($userStorage->id, [5899, 341, 23154, 326, 156]) ? true : false;


