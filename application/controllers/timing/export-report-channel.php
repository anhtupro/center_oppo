<?php
require_once 'PHPExcel.php';

$PHPExcel = new PHPExcel();

$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'from_f' => $from_date,
    'to_f' => $to_date
];

$db         = Zend_Registry::get('db');
$stmt_out = $db->prepare("CALL `sp_report_channel`(:p_from_date, :p_to_date)");
$stmt_out->bindParam('p_from_date', $params['from_date'], PDO::PARAM_STR);
$stmt_out->bindParam('p_to_date', $params['to_date'], PDO::PARAM_STR);
$stmt_out->execute();
$data = $stmt_out->fetchAll();
$stmt_out->closeCursor();

$heads = array(
    'Channel',

    'Unit',
    'Unit activated',
    'Value (80%)',
    'Value KPI',

    'Unit Special Model',
    'Unit activated Special Model',
    'Value Special Model (80%)',
    'Value KPI Special Model',

    '(PHONE) Unit Model >= 7.990.000',
    '(PHONE) Unit activated Model >= 7.990.000',
    '(PHONE) Value Model >= 7.990.000 (80%)',
    '(PHONE) Value KPI Model >= 7.990.000',

    '(PHONE) Unit Model còn lại',
    '(PHONE) Unit Activated Model còn lại',
    '(PHONE) Value Model còn lại (80%)',
    '(PHONE) Value KPI Model còn lại',

    '(PHONE) Total Unit',
    '(PHONE) Total Unit Activated',
    '(PHONE) Total Value (80%)',
    '(PHONE) Total Value KPI',

    '-----------------------',

    '(IOT) Unit Model >= 7.990.000',
    '(IOT) Unit activated Model >= 7.990.000',
    '(IOT) Value Model >= 7.990.000 (80%)',
    '(IOT) Value KPI Model >= 7.990.000',

    '(IOT) Unit Model còn lại',
    '(IOT) Unit activated Model còn lại',
    '(IOT) Value Model còn lại (80%)',
    '(IOT) Value KPI Model còn lại',

    '(IOT) Total Unit',
    '(IOT) Total Unit Activated',
    '(IOT) Total Value (80%)',
    '(IOT) Total Value KPI',

    'Bonus Phone',
    'Bonus IOT',
    'Total Bonus'
);


$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 3;

$i = 1;

foreach($data as $item ){
    $alpha    = 'A';

    $sheet->setCellValue($alpha++.$index, $item['channel_name']);

    $sheet->setCellValue($alpha++.$index, $item['quantity']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated']);
    $sheet->setCellValue($alpha++.$index, $item['value']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi']);

    $sheet->setCellValue($alpha++.$index, $item['quantity_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['value_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_special_model']);

    $sheet->setCellValue($alpha++.$index, $item['quantity_gt79_phone']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated_gt79_phone']);
    $sheet->setCellValue($alpha++.$index, $item['value_gt79_phone']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_gt79_phone']);

    $sheet->setCellValue($alpha++.$index, $item['quantity_phone_excluded_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated_phone_excluded_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['value_phone_excluded_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_phone_excluded_special_model']);

    $sheet->setCellValue($alpha++.$index, $item['quantity_phone']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated_phone']);
    $sheet->setCellValue($alpha++.$index, $item['value_phone']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_phone']);


    $sheet->setCellValue($alpha++.$index, '-----------------------');

    $sheet->setCellValue($alpha++.$index, $item['quantity_gt79_watch']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated_gt79_watch']);
    $sheet->setCellValue($alpha++.$index, $item['value_gt79_watch']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_gt79_watch']);

    $sheet->setCellValue($alpha++.$index, $item['quantity_watch_excluded_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated_watch_excluded_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['value_watch_excluded_special_model']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_watch_excluded_special_model']);

    $sheet->setCellValue($alpha++.$index, $item['quantity_watch']);
    $sheet->setCellValue($alpha++.$index, $item['quantity_activated_watch']);
    $sheet->setCellValue($alpha++.$index, $item['value_watch']);
    $sheet->setCellValue($alpha++.$index, $item['value_kpi_watch']);

    $sheet->setCellValue($alpha++.$index, $item['channel_id'] == 17 ? 0 : $item['bonus_phone']);
    $sheet->setCellValue($alpha++.$index, $item['channel_id'] == 17 ? 0 :$item['bonus_watch']);
    $sheet->setCellValue($alpha++.$index, $item['channel_id'] == 17 ? 0 :$item['bonus']);

    $index++;
}

$filename = 'Report Channel';
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
