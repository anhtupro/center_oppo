<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

if ($this->getRequest()->getMethod() != 'POST'){
    exit;
}

$str = '<script>parent.$(\'#email_modal\').find(".modal-body .alert").remove()</script>';
echo $str;

$staffFirst      = $this->getRequest()->getParam('staffFirst');
$inputIMEI       = $this->getRequest()->getParam('inputIMEI');
$inputDate       = $this->getRequest()->getParam('inputDate');
$inputShift      = $this->getRequest()->getParam('inputShift');
$inputStore      = $this->getRequest()->getParam('inputStore');
$customerName    = $this->getRequest()->getParam('inputCustomerName');
$customerPhone   = $this->getRequest()->getParam('inputCustomerPhone');
$customerAddress = $this->getRequest()->getParam('inputCustomerAddress');
$photo           = $this->getRequest()->getParam('photo');
$inputNote       = $this->getRequest()->getParam('inputNote');
$timing_sales_id = $this->getRequest()->getParam('timing_sales_id');
$checksum        = $this->getRequest()->getParam('checksum');
$duplicate_type        = $this->getRequest()->getParam('duplicate_type');
$QDuplicatedImei = new Application_Model_DuplicatedImei();
$QTimingSale = new Application_Model_TimingSale();
$QTiming = new Application_Model_Timing();


$current_month = date('m');
$current_day = date('d');
$tmp = explode('/', $inputDate);
$formatedDate = $tmp[2].'-'.$tmp[1].'-'.$tmp[0].' 00:00:00';

$db = Zend_Registry::get('db');
$db->beginTransaction();

// check nếu sau ngày 2 thì ko cho báo trùng tháng trước
if ($current_month != $tmp[1] && $current_day > 2) {
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'Sau ngày 2 không được báo trùng tháng trước !<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');
}


$inputIMEI = My_Util::escape_string(TRIM($inputIMEI));

// Tu them imei khong ton tai
$timing_sale_row = $QTimingSale->fetchRow(['imei = ?' => $inputIMEI]);
if ($duplicate_type == 1) { // báo trùng thì mới check
    if (! $timing_sale_row) {
        // thông báo IMEI không hợp lệ
        $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'OPPO chưa có báo số IMEI này nên không thể báo trùng!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
        exit('<script>'.$str.'</script>');
    }
}


// check IMEI
if (!$inputIMEI) {
    // thông báo IMEI không hợp lệ
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'IMEI không hợp lệ!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');

}

// check ngày
if ( strtotime($formatedDate ) > strtotime( date('Y-m-d H:i:s') ) ) {
    // thông báo không thể chấm công cho trước ngày hiện tại
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'Không thể báo cáo cho tương lai!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');

}


// Tu them check bao trung thang cung shop
$params = array(
    'imei_check' => $inputIMEI,
    'month_check' => $tmp[1],
    'store_id' => $inputStore
);

// nếu IMEI đã được xử lý báo trùng rồi thì ko cho báo trùng nữa
    $imei_has_solved = $QDuplicatedImei->checkImeiHasSolved($inputIMEI);
    if ($imei_has_solved) {
        $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'IMEI này đã được công ty xử lý báo trùng/khác khu vực rồi, không thể báo thêm nữa<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
        exit('<script>'.$str.'</script>');
    }
// end nếu IMEI đã được xử lý báo trùng rồi thì ko cho báo trùng nữa



$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$user_id = $userStorage->id;
$inputNote = My_Util::escape_string(TRIM($inputNote));


// kiểm tra xem staff này đã báo cáo cho IMEI này chưa
$where = array();
$where[] = $QDuplicatedImei->getAdapter()->quoteInto('imei = ?', $inputIMEI);
$where[] = $QDuplicatedImei->getAdapter()->quoteInto('staff_id = ?', $user_id);
$dup = $QDuplicatedImei->fetchRow($where);
if ($dup) {
    // thông báo trùng
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'Bạn đã gửi báo cáo IMEI này rồi!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');
}

$checkSecondImei = $QDuplicatedImei->checkSecondImei($params);
if (!empty($checkSecondImei)) {
    // thông báo trùng
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'Đã có người báo trùng IMEI khu vực này rồi, không thể báo cho khu vực này nữa!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');
}


$checkSameStore = $QDuplicatedImei->checkSameStore($inputIMEI, $inputStore);
if ($checkSameStore) {
    // thông báo trùng
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'Không được báo giống Store với người đã báo số trước đó!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');
}

// check ca chấm công

// check store

if ($timing_sale_row['timing_id']) {
    $timing_row = $QTiming->fetchRow(['id = ?' => $timing_sale_row['timing_id']]);
}

$data = array_filter(array(
	'staff_id'           => $user_id,
	'imei'               => $inputIMEI,
	'date'               => $formatedDate,
	'shift'              => $inputShift ? $inputShift : NULL,
	'store_id'           => $inputStore,
	'customer_name'       => $customerName,
	'customer_address'    => $customerAddress,
	'customer_phone'      => $customerPhone,
	'timing_id_first'    => $timing_sale_row['timing_id'] ? $timing_sale_row['timing_id'] : NULL,
	'timing_sales_first' => $timing_sale_row['id'] ? $timing_sale_row['id'] : NULL,
	'staff_id_first'     => $timing_row['staff_id'] ? $timing_row['staff_id'] : NULL,
	'store_id_first'     => $timing_row['store'] ? $timing_row['store'] : NULL,
	'customer_name_first' => $timing_sale_row['customer_name'] ? $timing_sale_row['customer_name'] : NULL,
	'customer_phone_first' =>  $timing_sale_row['phone_number'] ? $timing_sale_row['phone_number'] : NULL,
	'customer_address_first' =>  $timing_sale_row['address'] ? $timing_sale_row['address'] : NULL,
	'note'               => $inputNote,
	'created_at'         => date('Y-m-d H:i:s'),
    'duplicate_type' => $duplicate_type
));

$last_id = $QDuplicatedImei->insert($data);

if (!$last_id) {
    // báo lỗi không insert được
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'Insert Failed!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');
}

try {
//   upload file

    $QDuplicatedImeiFile = new Application_Model_DuplicatedImeiFile();
    foreach($_FILES['file_duplicate_imei']['name'] as $k => $v) {
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'timing_imei_duplicated' . DIRECTORY_SEPARATOR . $last_id;
        if (!is_dir($uploaded_dir))
            @mkdir($uploaded_dir, 0777, true);

        $tmp_file_path = $_FILES['file_duplicate_imei']['tmp_name'][$k];
        $file_size = $_FILES['file_duplicate_imei']['size'][$k];
        $old_name = $v;
        $tExplode = explode('.', $old_name);
        $extension = end($tExplode);
        $new_name = 'duplicate_imei_' . md5(uniqid('', true)) . '.' . $extension;
        $new_file_path = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;


        //check file
        if ($file_size == 0) {
            $str = 'parent.$(\'#email_modal\').find(".modal-body")
                    .append(\'<div class="alert alert-error fade in">\' +
                      \'Vui lòng upload đầy đủ hình ảnh!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
                    \'</div>\')';
            exit('<script>'.$str.'</script>');
        }
        if ($file_size >= 5000000) {
            $str = 'parent.$(\'#email_modal\').find(".modal-body")
                    .append(\'<div class="alert alert-error fade in">\' +
                      \'Chỉ được upload file dưới 5MB!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
                    \'</div>\')';
            exit('<script>'.$str.'</script>');
        }

        if (! in_array($extension, ['png', 'jpg', 'jpeg'])) {
            $str = 'parent.$(\'#email_modal\').find(".modal-body")
                    .append(\'<div class="alert alert-error fade in">\' +
                      \'Chỉ được upload file ảnh!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
                    \'</div>\')';
            exit('<script>'.$str.'</script>');
        }
        // end check file

        if (move_uploaded_file($tmp_file_path, $new_file_path)) {
           // upload image to server s3
            require_once "Aws_s3.php";
            $s3_lib = new Aws_s3();
            $destination_s3 = 'photo/timing_imei_duplicated/' . $last_id . '/';
            $upload_s3 = $s3_lib->uploadS3($new_file_path, $destination_s3, $new_name);

          // upload image to server s3*/

            $short_url = '/photo/timing_imei_duplicated/' . $last_id . '/' . $new_name;

            $data_file = [
                'duplicated_id' => $last_id,
                'url'   => $short_url,
                'type' => 1
            ];
            $QDuplicatedImeiFile->insert($data_file);
        }
    }
} catch (Zend_File_Transfer_Exception $e) {
    $str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-error fade in">\' +
          \'Please input valid file!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\')';
    exit('<script>'.$str.'</script>');
}

$db->commit();
$str = 'parent.$(\'#email_modal\').find(".modal-body")
        .append(\'<div class="alert alert-success fade in">\' +
          \'Gửi thành công!<button type="button" class="close" data-dismiss="alert">&times;</button>\' +
        \'</div>\'); setTimeout(function(){parent.$("#email_modal").modal("hide")}, 3000)';

exit('<script>'.$str.'</script>');


exit;