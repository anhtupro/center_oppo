<?php
$this->_helper->layout->disableLayout();

$store_name = $this->getRequest()->getParam('store_name');
$distributor_id = $this->getRequest()->getParam('distributor_id');
$type = $this->getRequest()->getParam('type');

// sellin lấy theo 6 tháng gần nhất
$from_date = date('Y-m-d 00:00:00', strtotime('- 3 Months'));
$to_date = date('Y-m-d 23:59:59');

$QStoreInventory = new Application_Model_StoreInventory();

$data = $QStoreInventory->getDetailSellin($distributor_id, $from_date, $to_date, $type);

$this->view->data = $data;
$this->view->store_name = $store_name;
$this->view->type = $type;