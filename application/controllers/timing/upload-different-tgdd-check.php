<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
include 'PHPExcel/IOFactory.php';
// config for excel template excel

//type = 1 : imei chưa được báo số
//type = 2 : imei đã được báo số, ko có duplicate, giống store
//type = 3 : imei đã được báo số, ko có duplicate, khác store
//type = 4, imei đã được báo số , có duplicate , thằng báo timing giống store
//type = 5, imei đã được báo số , có duplicate , thằng báo duplicate giống store
//type = 6, imei đã được báo số , có duplicate , không thằng nào giống store

// row
define('START_ROW', 2);

// column
define('IMEI', 0);
define('STORE_NAME', 1);
define('TIMING_DATE', 2);
define('PARTNER_ID', 3);

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$distributor_id = $this->getRequest()->getParam('distributor_id');
$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

if (! $distributor_id ) {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chọn kênh ! '
    ]);
    return;
}

// xử lý kênh thành array
if ($distributor_id == 20) {
//    $array_distributor_id = [20, 79, 54882]; // 3 kênh cellphones
    $array_distributor_id = [54882, 55423]; // 3 kênh cellphones
} else {
    $array_distributor_id = [$distributor_id];
}

$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'array_distributor_id' => $array_distributor_id
];


$QUploadDifferentTgdd = new Application_Model_UploadDifferentTgdd();
$QDistributor = new Application_Model_Distributor();

$distributor = $QDistributor->fetchRow(['id = ?' => $distributor_id]);
$db = Zend_Registry::get("db");

// insert vào bảng tạm
// upload and save file
if ($_FILES['file']['name']) { // if has file upload

    $save_folder = 'upload_different_tgdd';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 50000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx')
    );

    try {
        $file = My_File::get($save_folder, $requirement);

        if (!$file) {
            echo json_encode([
                'status' => 1,
                'message' => 'Upload failed'
            ]);
            return;
        }
        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];


    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }


    //read file
    //  Choose file to read
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

    // read sheet
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();


    try {

        $insert_header_query = "INSERT INTO upload_different_tgdd(imei, partner_id, timing_date) VALUES ";
        $insert_body_query = '';
        $imei_wrong_format = [];
        $partner_id_wrong_format = [];
        $date_wrong_format = [];
        $month_not_equal = [];

        for ($row = START_ROW; $row <= $highestRow; ++$row) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = $rowData[0];

            $imei = "'" . My_Util::escape_string(TRIM($rowData[IMEI])) . "'";
            $partner_id = "'" . My_Util::escape_string(TRIM($rowData[PARTNER_ID])) . "'";
            $date = str_replace('/', '-', TRIM($rowData[TIMING_DATE]));
            $timing_date = $date ? "'" . date('Y-m-d 12:00:00', strtotime($date)) . "'" : 'NULL';

            if (strpos($imei, '+') || !$imei) {
                $imei_wrong_format [] = $imei;
                continue;
            }

            if (!$partner_id) {
                $partner_id_wrong_format [] = $row;
                continue;
            }

            if (date('Y-m-d 12:00:00', strtotime($date)) === '1970-01-01 12:00:00') {
                $date_wrong_format [] = $row;
                continue;
            }

            if (date('m', strtotime($date)) !=  $month) { // check nếu tháng trong file khác với tháng đang tính KPI thì báo lỗi
                $month_not_equal [] = $row;
                continue;
            }


            // insert
            $value_insert = '('
                . $imei . ','
                . $partner_id  .  ','
                . $timing_date
                . '),';
            $insert_body_query .= $value_insert;

        }

        if ($imei_wrong_format) {
            echo json_encode([
                'status' => 1,
                'message' => 'Imei sai định dạng. Vui lòng sửa và upload file lại , Imei: ' . implode("\n", $imei_wrong_format)
            ]);
            return;
        }

        if ($partner_id_wrong_format) {
            echo json_encode([
                'status' => 1,
                'message' => 'Partner_id đang trống , tại các dòng trong file: ' . implode(", ", $partner_id_wrong_format) . '. Vui lòng chỉnh và upload lại file'
            ]);
            return;
        }

        if ($date_wrong_format) {
            echo json_encode([
                'status' => 1,
                'message' => 'Ngày sai định dạng tại các dòng trong file: ' . implode(", ", $date_wrong_format) . '. Vui lòng chỉnh và upload lại file. Các định dạng cho phép dd-mm-yyyy hoặc yyyy-mm-dd'
            ]);
            return;
        }

        if ($month_not_equal) {
            echo json_encode([
                'status' => 1,
                'message' => 'Tháng trong file không khớp với tháng đang chọn, tại các dòng trong file: ' . implode(", ", $month_not_equal) . '. Vui lòng kiểm tra và upload lại file'
            ]);
            return;
        }



        if ($insert_body_query) {
            $insert_query = $insert_header_query . $insert_body_query;
            $insert_query = TRIM($insert_query, ',');

            if($insert_query){
                $db->query('TRUNCATE TABLE upload_different_tgdd');
                $stmt = $db->prepare($insert_query);
                $stmt->execute();
                $stmt->closeCursor();
            }
        }



    } catch (Exception $e) {
        echo json_encode([
            'status' => 1,
            'message' => $e->getMessage()
        ]);
        return;
    }

} else {
    echo json_encode([
        'status' => 1,
        'message' => 'Vui lòng chọn file !'
    ]);
    return;
}


// check imei ko tồn tại
$array_invalid_imei = $QUploadDifferentTgdd->getInvalidImei();
if (count($array_invalid_imei) > 0) {
    echo json_encode([
        'status' => 1,
        'message' => 'Imei không tồn tại: ' . implode("\n", $array_invalid_imei) . '. Vui lòng kiểm tra và upload lại file'
    ]);
    return;
}

// check ko tồn tại partner_id
$array_invalid_partner = $QUploadDifferentTgdd->getInvalidPartner($array_distributor_id);
if (count($array_invalid_partner) > 0) {
    echo json_encode([
        'status' => 1,
        'message' => 'Partner_id không tồn tại hoặc không thuộc kênh đã chọn: ' . implode("\n", $array_invalid_partner) . '. Vui lòng kiểm tra và upload lại file'
    ]);
    return;
}

// check store map sai distributor
$array_invalid_distributor = $QUploadDifferentTgdd->getInvalidDistributor();
if (count($array_invalid_distributor) > 0) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không tìm thấy store đang hoạt động tại Partner_id: ' . implode("\n", $array_invalid_distributor) . '. Vui lòng map lại distributor đúng cho shop và upload lại file'
    ]);
    return;
}

// update store_id theo partner_id, và type theo từng trường hợp
$QUploadDifferentTgdd->updateStoreIdAndType($params);


// export imei chưa báo số : type= 1
$url_not_timing = $QUploadDifferentTgdd->exportImeiNotTiming();

// export  imei đã được báo số, ko có duplicate, khác store type = 3
$url_has_timing_wrong_store = $QUploadDifferentTgdd->exportHasTimingWrongStore();

// export báo số shop FPT, NK, ...  ko có trong  FILE, NK FPT
$url_not_in_file = $QUploadDifferentTgdd->exportImeiNotInFile($params);

// export có báo số và có báo dup + staff 1 giống store
$url_has_timing_duplicate_staff_one_win = $QUploadDifferentTgdd->exportHasTimingAndDuplicate($params, 4);

// export có báo số và có báo dup + staff 2 giống store
$url_has_timing_duplicate_staff_two_win = $QUploadDifferentTgdd->exportHasTimingAndDuplicate($params, 5);

// export có báo số và có báo dup + cả 2 đều ko giống shop
$url_has_timing_duplicate_different_shop = $QUploadDifferentTgdd->exportHasTimingAndDuplicate($params, 6);

// set distributor name
if ($distributor_id == 20) {
    $distributor_name = "[" .  'Cellphones (3 kênh gộp lại)'  . "]";
} else {
    $distributor_name = "[" . $distributor['title'] . "]";
}

echo json_encode([
    'status' => 0,
    'file' => [
        [
            'url' => $url_not_timing,
            'name' => $distributor_name . ' Imei chưa được báo số.xlsx'
        ],

        [
            'url' => $url_has_timing_wrong_store,
            'name' => $distributor_name . ' Imei đã được báo số, ko có duplicate, sai store.xlsx'
        ],

        [
            'url' => $url_not_in_file,
            'name' => $distributor_name . ' Imei đã báo số nhưng không có trong file.xlsx'
        ],

        [
            'url' => $url_has_timing_duplicate_staff_one_win,
            'name' => $distributor_name . ' Imei có báo số và có duplicate và staff 1 đúng store.xlsx'
        ],

        [
            'url' => $url_has_timing_duplicate_staff_two_win,
            'name' => $distributor_name . ' Imei có báo số và có duplicate và staff 2 đúng store.xlsx'
        ],

        [
            'url' => $url_has_timing_duplicate_different_shop,
            'name' => $distributor_name . ' Imei có báo số và có duplicate và cà 2 đều sai store.xlsx'
        ]

    ]
]);

