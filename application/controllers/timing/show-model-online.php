<?php

$from           = $this->getRequest()->getParam('from');
$to             = $this->getRequest()->getParam('to');
$channel        = $this->getRequest()->getParam('distributor_id');
$QMarket        = new Application_Model_Market();
$params = array(
    'from'           => $from,
    'to'             => $to,
    'channel'        => $channel
);


$list     = $QMarket->listChannelDetails($page, $limit, $total, $params);
$sellin     = $QMarket->getSellInChannelDetails($params);
$active_all = $QMarket->getActiveAllChannelDetails($params);

$list_sellin = array();
foreach($sellin as $key=>$value){
    $list_sellin[$value['d_id']] = $value['sellin'];
}

$list_active = array();
foreach($active_all as $key=>$value){
    $list_active[$value['d_id']] = $value['active'];
}

$this->view->channel = $channel;
$this->view->list = $list;
$this->view->sellin = $list_sellin;
$this->view->active = $list_active;
$this->view->params = $params;
$this->_helper->layout()->disablelayout(true);