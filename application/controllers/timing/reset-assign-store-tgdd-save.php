<?php
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$QLockedKpi = new Application_Model_LockedKpi();
$QKpiByModel = new Application_Model_KpiByModel();
$QResetAssignStoreTgddLog = new Application_Model_ResetAssignStoreTgddLog();

$store_id = $this->getRequest()->getParam('store_id');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if (! in_array($userStorage->id, [341])) {
    echo json_encode([
        'status' => 1,
        'message' => "Không có quyền thao tác!"
    ]);
    return;
}

if (! $store_id) {
    echo json_encode([
        'status' => 1,
        'message' => "Không có store id"
    ]);
    return;
}

if (! $from_date || !$to_date) {
    echo json_encode([
        'status' => 1,
        'message' => "Ngày không hợp lệ"
    ]);
    return;
}


$from_date = str_replace('/', '-', $from_date);
$to_date = str_replace('/', '-', $to_date);

$from_date = date('Y-m-d', strtotime($from_date));
$to_date = date('Y-m-d', strtotime($to_date));

$month_from_date = date('m', strtotime($from_date));
$month_to_date = date('m', strtotime($to_date));
$year_from_date = date('Y', strtotime($from_date));
$year_to_date = date('Y', strtotime($to_date));

$locked_from_date = $QLockedKpi->fetchRow([
   'month = ?' => $month_from_date,
   'year = ?' => $year_from_date
]);

$locked_to_date = $QLockedKpi->fetchRow([
    'month = ?' => $month_to_date,
    'year = ?' => $year_to_date
]);

if ($from_date > $to_date) {
    echo json_encode([
        'status' => 1,
        'message' => "Ngày bắt đầu không được lớn hơn ngày kết thúc"
    ]);
    return;
}

if ($locked_from_date['locked_all'] || $locked_to_date['locked_all']) {
    echo json_encode([
        'status' => 1,
        'message' => "Tháng này đã chốt KPI không thể xử lý thêm"
    ]);
    return;
}


$params = [
    'store_id' => $store_id,
    'from_date' => $from_date,
    'to_date' => $to_date
];


// xử lý chính
$db = Zend_Registry::get('db');
$db->beginTransaction();
// update cột PG allow
$QKpiByModel->updatePgAllow($params);

// xóa báo số
$QKpiByModel->deleteBaoSo($params);

// lưu log
$QResetAssignStoreTgddLog->insert([
   'from_date' => $from_date,
   'to_date' => $to_date,
   'store_id' => $store_id,
   'created_at' => date('Y-m-d H:i:s'),
   'created_by' => $userStorage->id
]);

$db->commit();


echo json_encode([
    'status' => 0
]);