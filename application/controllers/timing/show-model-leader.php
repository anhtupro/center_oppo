<?php

$staff_id = $this->getRequest()->getParam('staff_id');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$staff_name = $this->getRequest()->getParam('staff_name');
$unit_all = $this->getRequest()->getParam('unit_all');

$params = array(
    'from_date' => $from_date,
    'to_date' => $to_date,
    'staff_id' => $staff_id,
    'staff_name' => $staff_name,
    'unit_all' => $unit_all
);

$QImeiKpi = new Application_Model_ImeiKpi2();
$data = $QImeiKpi->getModelLeader($params);

$this->view->data = $data;
$this->view->params = $params;

$this->_helper->layout()->disablelayout(true);
?>