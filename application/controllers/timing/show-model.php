<?php

$staff_id       = $this->getRequest()->getParam('staff_id');
$from           = $this->getRequest()->getParam('from');
$to             = $this->getRequest()->getParam('to');
$store_id       = $this->getRequest()->getParam('store_id');
$type           = $this->getRequest()->getParam('type','staff');
$distributor_id = $this->getRequest()->getParam('distributor_id');
$area_id        = $this->getRequest()->getParam('area_id');
$area           = $this->getRequest()->getParam('area');
$province       = $this->getRequest()->getParam('province');
$district       = $this->getRequest()->getParam('district');
$good_id        = $this->getRequest()->getParam('good_id');

$QDistributor = new Application_Model_Distributor();
$QStore       = new Application_Model_Store();
$QStaff       = new Application_Model_Staff();
$QImeiKpi     = new Application_Model_ImeiKpi();
$QGood        = new Application_Model_Good();
$QArea        = new Application_Model_Area();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if (!$userStorage || !isset($userStorage->id)) $this->_redirect(HOST);
$group_id = $userStorage->group_id;

$total = 0;
$list = array();
$params = array(
    'from'     => $from,
    'to'       => $to,
    'area'     => $area,
    'area_id'  => $area_id,
    'good_id'  => $good_id,
    'province' => $province,
    'district' => $district
);
$quantity_label = 'quantity';
$info_name = '';
if($type == 'staff'){
    $staff                           = $QStaff->find($staff_id)->current();
    $params['staff_id']              = $staff_id;
    $params['title']                 = array($staff->title);
    $params['group_year_month_good'] = 1;

    if(!$staff){ throw new Exception('Staff not exist'); }
    $this->view->staff               = $staff;
    $list                            = $QImeiKpi->fetchByMonthAllStaff(0,0,$total,$params);
    $quantity_label                  = 'quantity';
    $info_name                       = $staff['firstname'].' '.$staff['lastname'];
}elseif($type == 'store'){
    if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {

    } elseif ($group_id == My_Staff_Group::SALES) {
        $params['sale_id'] = $userStorage->id;
    } elseif ($group_id == My_Staff_Group::LEADER) {
        $params['leader_id'] = $userStorage->id;
    }
    $params['store']                 = $store_id;
    $params['group_year_month_good'] = 1;
//    $list                            = $QImeiKpi->fetchStore(0,0,$total,$params);
    $list                            = $QImeiKpi->fetchStoreKpiByModel(0,0,$total,$params);
    $quantity_label                  = 'total_quantity';
    $store                           = $QStore->find($store_id)->current();
    $this->view->store               = $store;
    $info_name                       = $store['short_name'];
}elseif($type == 'distributor'){
    $distributor = $QDistributor->find($distributor_id)->current();
    if(!$distributor){ throw new Exception('Distributor not exist');}
    $params['distributor']           = $distributor_id;
    $params['group_year_month_good'] = 1;
    $list                            = $QImeiKpi->fetchDistributor(0,0,$total,$params);
    $quantity_label                  = 'total_quantity';
    $this->view->distributor         = $distributor;
    $info_name                       = $distributor['title'];
}elseif($type == 'area'){
    $params['area_id']               = $area_id;
    $params['group_year_month_good'] = 1;
    $list                            = $QImeiKpi->fetchArea($params);
    $quantity_label                  = 'total_quantity';
    $current_area                    = $QArea->find($area_id)->current();
    $this->view->current_area        = $current_area;
    $info_name                       = $current_area['name'];
}elseif($type == 'product'){
    $params['group_year_month_good'] = 1;
    $list                            = $QImeiKpi->fetchProduct(0,0,$total,$params);
    $quantity_label                  = 'total_quantity';
    $current_good                    = $QGood->find($good_id)->current();
    $this->view->current_good        = $current_good;
    $info_name                       = $current_good['name'];
}

// if ($userStorage->id == 5899) {
//     echo "<pre>";print_r($list );die;
// }

$this->view->list = $list;
$this->view->type = $type;
$this->view->info_name = $info_name;

$title_list = array();
$data       = array();
$cols       = 0;
$sum = 0;

if(isset($list) and count($list)){
    foreach($list as $item){
        if(!isset($title_list[$item['year']][$item['month']])){
            $title_list[$item['year']][$item['month']] = $item['year'].'_'.$item['month'];
            $cols++;
        }
        $data[ $item['good_id'] ] [ $item['year'] ] [ $item['month'] ] = $item[$quantity_label];
        $sum +=  $item[$quantity_label];
    }
}

$this->view->data = $data;
$this->view->cols = $cols;
$this->view->sum = $sum;

ksort($title_list);
foreach ($title_list as $key => $value){
    ksort($title_list[$key]);
}


$this->view->title_list = $title_list;
$goods                  = $QGood->get_all();
$this->view->goods      = $goods;

$this->_helper->layout()->disablelayout(true);
?>