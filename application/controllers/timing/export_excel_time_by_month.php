<?php 
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $group_id = $userStorage->group_id;
        set_time_limit(0);
        error_reporting(0);
        ini_set('display_error', 0);

        $filename = 'Timing By Month - ' . date('d/m/Y H:i:s');
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        $output = fopen('php://output', 'w');

        if (isset($params['from']) && $params['from'] && isset($params['to']) && $params['to'])
        {
            $from = explode('/', $params['from']);
            $from = $from[2] . '-' . $from[1] . '-' . $from[0] . ' 00:00:00';
            $to = explode('/', $params['to']);
            $to = $to[2] . '-' . $to[1] . '-' . $to[0] . ' 23:59:59';
        } elseif (isset($params['from']) && $params['from'])
        {
            $from = explode('/', $params['from']);
            $from = $from[2] . '-' . $from[1] . '-' . $from[0] . ' 00:00:00';
            $to = date('Y-m-d 23:59:59');
        } elseif (isset($params['to']) && $params['to'])
        {
            $to = explode('/', $params['to']);
            $to = $to[2] . '-' . $to[1] . '-' . $to[0] . ' 23:59:59';
            $from = date_sub(date_create(), new DateInterval('P30D'))->format('Y-m-d 00:00:00');
        } else
        {
            $from = date_sub(date_create(), new DateInterval('P30D'))->format('Y-m-d 00:00:00');
            $to = date('Y-m-d 23:59:59');
        }

        $heads = array(
            'Area',
            'Code',
            'Name',
            'Title',
            'Indentity',
            'Joined At',
            'Off Date'
            );

        for ($i = 1; $i <= 31; $i++)
        {
            $heads[] = $i;
        }

        $heads[] = 'Tổng ngày công';
        $heads[] = 'Ca gãy';

        fputcsv($output, $heads);

        $db = Zend_Registry::get('db');



        if (!in_array($group_id, array(HR_ID, ADMINISTRATOR_ID, BOARD_ID)))
        {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();
            $ids = join(',',$list_regions);
            $where = "WHERE s.regional_market in ($ids) ORDER BY s.id";
        }

        $sql = "SELECT s.id, s.code, t.`from`, t.shift FROM timing t
                    INNER JOIN staff s
                    ON t.staff_id=s.id
                    AND t.`from` >= ?
                    AND t.`from` <= ?
                    AND t.approved_at IS NOT NULL
                    AND t.approved_at <> ''
                    AND t.approved_at <> 0
                    AND t.shift <> 4
                    -- AND s.group_id IN (?, ?)
                    ORDER BY s.id
        ";

        $timings = $db->query($sql, array(
            $from,
            $to,
            PGPB_ID,
            SALES_ID));

        $timing_staffs = array();


        $sql = "SELECT
                    s.id,
                    s.`code`,
                    s.firstname,
                    s.lastname,
                    a.`name`,
                    s.off_date,
                    s.joined_at,
                    s.ID_number,
                    ta.`name` as title
                    FROM
                        timing t
                    INNER JOIN staff s ON t.staff_id = s.id
                    AND t.`from` >= ?
                    AND t.`from` <= ?
                    INNER JOIN regional_market r ON r.id=s.regional_market
                    INNER JOIN team ta ON s.title = ta.id
                    INNER JOIN area a ON a.id=r.area_id
                    GROUP BY s.`id`
                ORDER BY s.id
                ";
        $staffs = $db->query($sql, array($from, $to));

        $staffs_all = array();

        foreach ($staffs as $key => $staff)
        {
            $staffs_all[$staff['id']] = array(
                "code"      => $staff['code'],
                "name"      => $staff['firstname'] . " " . $staff['lastname'],
                "area"      => $staff['name'],
                "indentity" => $staff['ID_number'],
                "off_date"  => $staff['off_date'],
                "joined_at" => $staff['joined_at'],
                "title"     => $staff['title']
            );
        }

        $timing_half_shift = array();

        if ($timings)
        {
            foreach ($timings as $key => $timing)
            {
                if (!isset($timing_staffs[$timing['id']]))
                {
                    $timing_staffs[$timing['id']] = array();
                }

                $d  = date('d', strtotime($timing['from']));
                $id = $timing['id'];
                $timing_staffs[$id][$d] = $timing['shift'];

                if($timing['shift'] == 2)
                {
                    $timing_half_shift[$id][$d] = 1;
                }

                if($timing['shift'] == TRAINER_SHIFT_COURSE)
                {
                    $timing_half_shift[$id][$d] = TRAINER_SHIFT_COURSE;
                }

            }

            foreach ($staffs_all as $id => $staff)
            {
                if (empty($staff['off_date'])
                    || ( isset($staff['off_date'])
                        and ( strtotime($staff['off_date']) >= strtotime($from))
                        )
                    )
                {

                    $row    = array();
                    $row[0] = $staff['area'];
                    $row[1] = $staff['code'];
                    $row[2] = $staff['name'];
                    $row[3] = $staff['title'];
                    $row[4] = $staff['indentity'];
                    $row[5] = $staff['joined_at'];
                    $row[6] = $staff['off_date'];
                    $d      = count($row);
                    $ca_gay = 0;
                    $num    = 0;

                    // lấy ngày nghỉ
                    $off_month = false; // kiêm tra tháng hiện nghỉ có trùng tháng lấy dữ liệu
                    if ($staff['off_date'] == null OR $staff['off_date'] == 0)
                    {
                        $off_day = 0;
                    } else
                    {
                        $off_day = date('d', strtotime($staff['off_date']));
                        if (isset($staff['off_date'])
                            AND ( strtotime($staff['off_date']) >= strtotime($from) AND strtotime($staff['off_date']) <= strtotime($to)))
                        {
                            $off_month = true;
                        }
                    }

                    $off_day = intval($off_day);

                    for ($i = 1; $i <= 31; $i++)
                    {
                        if ($i < 10)
                        {
                            $a = "0" . $i;
                        } else
                        {
                            $a = $i;
                        }

                        if (isset($timing_staffs[$id][$a]))
                        {

                            if ($off_day > 0 and $off_month == true)
                            {
                                // đã nghĩ và trong tháng cần lấy dữ liệu
                                if (intval($i) < $off_day)
                                {

                                    $num++;
                                    // ca gãy
                                    if ($timing_half_shift[$id][$a] == 1)
                                    {
                                         $ca_gay++;
                                         $row[$d + $i - 1] = 'G';
                                    }
                                    else
                                    {
                                         $row[$d + $i - 1] = 'X';
                                    }
                                }

                            } else
                            {
                                $num++;
                                    // ca gãy
                                    if ($timing_half_shift[$id][$a] == 1)
                                    {
                                         $ca_gay++;
                                         $row[$d + $i - 1] = 'G';
                                    }
                                    elseif(isset($timing_half_shift[$id][$a]) and $timing_half_shift[$id][$a] == TRAINER_SHIFT_COURSE)
                                    {
                                        $row[$d + $i - 1] = 'T';
                                    }
                                    else
                                    {
                                         $row[$d + $i - 1] = 'X';
                                    }
                            }

                        }
                    }

                    for ($i = 1; $i <= 31; $i++)
                    {
                        if (!isset($row[$d + $i - 1]))
                        {
                            $row[$d + $i - 1] = '-';
                        }
                    }

                    $row[] = $num;
                    $row[] = $ca_gay;

                    ksort($row);

                    fputcsv($output, $row);
                }



            }
            //export new training timing
            $select = $db->select()
                ->from('v_training_course_lock_data')
                ->where('`v_training_course_lock_data`.from >= ?' , $from)
                ->where('`v_training_course_lock_data`.from <= ?' , $to);

            $results = $select->query()->fetchAll();
            $timing_staffs = array();
            $num = 0;

            if (isset($results) and $results)
            {

                $training_staff = array();
                foreach ($results as $key => $training)
                {
                    if(!$training_staff[$training['cmnd']])
                    $training_staff[$training['cmnd']] = array(
                        "indentity" => $training['cmnd'],
                        "name"      => $training['name'],
                        "area"      => $training['area_name'],
                        "title"     => 'PG-PB',
                        "cmnd"      => $training['cmnd'],
                    );
                }


                foreach ($results as $key => $timing)
                {
                    if (!isset($timing_staffs[$timing['cmnd']]))
                    {
                        $timing_staffs[$timing['cmnd']] = array();
                    }

                    $d  = date('d', strtotime($timing['from']));
                    $id = $timing['cmnd'];
                    $timing_staffs[$id][$d] = 1;
                }


                foreach ($training_staff as $id => $training)
                {
                    $row = array();
                    $row[0] = $training['area'];
                    $row[1] = '';
                    $row[2] = $training['name'];
                    $row[3] = $training['title'];
                    $row[4] = $training['indentity'];
                    $row[5] = '';
                    $row[6] = '';
                    $d = count($row);

                    $num = 0;

                    for ($i = 1; $i <= 31; $i++) {
                        if ($i < 10) {
                            $a = "0" . $i;
                        } else {
                            $a = $i;
                        }

                        if (isset($timing_staffs[$training['cmnd']][$a])) {
                            $row[$d + $i - 1] = 'T';
                            $num++;
                        }
                        else{
                            $row[$d + $i - 1] = '-';
                        }
                    }

                    $row[] = $num;
                    ksort($row);
                    fputcsv($output, $row);
                }
            }
        }

        exit;
?>