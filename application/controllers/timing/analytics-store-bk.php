<?php
set_time_limit(0);

$page           = $this->getRequest()->getParam('page', 1);
$sort           = $this->getRequest()->getParam('sort', 'product_count');
$desc           = $this->getRequest()->getParam('desc', 1);
$from           = $this->getRequest()->getParam('from', date('01/m/Y') );
$to             = $this->getRequest()->getParam('to', date('d/m/Y'));
$name           = $this->getRequest()->getParam('name');
$search         = $this->getRequest()->getParam('search', 1);
$area           = $this->getRequest()->getParam('area');
$province       = $this->getRequest()->getParam('province');
$district       = $this->getRequest()->getParam('district');
$store          = $this->getRequest()->getParam('store');
$has_pg         = $this->getRequest()->getParam('has_pg');
$sales_from     = $this->getRequest()->getParam('sales_from');
$sales_to       = $this->getRequest()->getParam('sales_to');
$distributor_id = $this->getRequest()->getParam('distributor_id');
$store_level    = $this->getRequest()->getParam('store_level');
$channel        = $this->getRequest()->getParam('channel',-1);
$goods          = $this->getRequest()->getParam('goods');
$include_0_sell_out          = $this->getRequest()->getParam('include_0_sell_out');
$QStoreStaffLog = new Application_Model_StoreStaffLog();
$QGood          = new Application_Model_Good();

$params = array(
    'page'           => $page,
    'sort'           => $sort,
    'desc'           => $desc,
    'from'           => $from,
    'to'             => $to,
    'name'           => $name,
    'area'           => $area,
    'province'       => $province,
    'district'       => $district,
    'store'          => $store,
    'has_pg'         => $has_pg,
    'sales_from'     => $sales_from,
    'sales_to'       => $sales_to,
    'distributor_id' => $distributor_id,
    'store_level'    => $store_level,
    'channel'        => $channel,
    'goods'          => $goods,
    'search'         => $search,
    'include_0_sell_out' => $include_0_sell_out
);

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$params['title']      = $userStorage->title;

if (!$userStorage || !isset($userStorage->id)) $this->_redirect(HOST);
$group_id = $userStorage->group_id;


// if(empty($_GET['dev']) && $userStorage->id != 341 && $userStorage->id != 5899)
//     exit('Chức năng này tạm khóa');


$QArea = new Application_Model_Area();
$areas = $QArea->get_cache();

$QRegionalMarket = new Application_Model_RegionalMarket();

if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm = new Application_Model_Asm();
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();
} elseif (in_array($group_id, array(My_Staff_Group::SALES, PB_SALES_ID))) {
    // lấy cửa hàng của sale
    

    $store_cache = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
    $params['store_list'] = $store_cache;
    $params['sale_id']    = $userStorage->id;
    
} elseif ($group_id == My_Staff_Group::LEADER) {
    // lấy cửa hàng của sale
    $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
    $store_cache_leader = $QStoreLeaderLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $store_cache = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
    $store_list = array_merge($store_cache_leader, $store_cache);
    $params['store_list'] = $store_list;
    $params['leader_id'] = $userStorage->id;
}

if ($area) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area);
    $this->view->regional_markets = $QRegionalMarket->fetchAll($where, 'name');
}

if ($province) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $province);
    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}

if ($district) {
    //get store
    $QStore = new Application_Model_Store();
    $where = $QStore->getAdapter()->quoteInto('district = ?', $district);
    $this->view->stores = $QStore->fetchAll($where, 'name');
}

$limit = LIMITATION;
$total = $total2 = 0;

$QImeiKpi = new Application_Model_ImeiKpi();
if(isset($search) && $search){
    $sales = $QImeiKpi->fetchStore($page, $limit, $total, $params);
    $params['get_total_sales'] = true;
    $total_sales = $QImeiKpi->fetchStore(null, null, $total2, $params);
}
unset($params['get_total_sales']);
unset($params['asm']);
unset($params['store_list']);
unset($params['area_list']);
unset($params['sale_id']);
unset($params['leader_id']);
unset($params['title']);

$QDistributor                  = new Application_Model_Distributor();
$this->view->distributor_cache = $QDistributor->get_cache();
//echo "<pre>";print_r($this->view->distributor_cache);die;

$QLoyaltyPlan                 = new Application_Model_LoyaltyPlan();
$store_level_list             = $QLoyaltyPlan->get_cache();
$this->view->store_level_list = $store_level_list;

$channel_list = array(
    1 => 'KA',
    0 => 'IND'
);

$this->view->channel_list    = $channel_list;
if(isset($search) && $search){
    $this->view->total_quantity  = $total_sales['total_quantity'];
    $this->view->total_activated = $total_sales['total_activated'];
    $this->view->sales           = $sales;
}
$this->view->offset          = $limit*($page-1);
$this->view->total           = $total;
$this->view->limit           = $limit;
$this->view->url             = HOST.'timing/analytics-store'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->desc            = $desc;
$this->view->current_col     = $sort;
$this->view->to              = $to;
$this->view->from            = $from;
$this->view->areas           = $areas;
$this->view->params          = $params;
$this->view->goods           = $QGood->get_cache();

// ADD
// Search Distributor Using Ajax

if($this->getRequest()->isXmlHttpRequest())
{
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $SearchBox = $this->getRequest()->getParam('txt');

    //echo $SearchBox;die;
    //echo "<pre>";print_r($param);die;

    $db     = Zend_Registry::get('db');

    $sql  = 'SELECT id,title FROM warehouse.distributor WHERE title LIKE "%'.$SearchBox.'%"';
    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll();
    $stmt->closeCursor();
    $db = $stmt = null;

    $this->view->dealer_ajax = $data;
    $this->render('partials/search-dealer-ajax');
    // exit();
    // echo "<pre>";print_r($data);die;
}


