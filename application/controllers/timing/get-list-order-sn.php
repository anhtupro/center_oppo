<?php
$this->_helper->layout->disableLayout();

$store_name = $this->getRequest()->getParam('store_name');
$distributor_id = $this->getRequest()->getParam('distributor_id');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');

$from_date_time = date('Y-m-d 00:00:00', strtotime($from_date));
$to_date_time = date('Y-m-d 23:59:59', strtotime($to_date));

$QStoreInventory = new Application_Model_StoreInventory();

$data = $QStoreInventory->getTotalOrder($distributor_id, $from_date_time, $to_date_time);

$this->view->data = $data;
$this->view->store_name = $store_name;