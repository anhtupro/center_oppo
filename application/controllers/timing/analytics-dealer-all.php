<?php
// echo '<pre>';
// print_r("Chức năng tạm bảo trì.");
// die;
set_time_limit(0);

$page       = $this->getRequest()->getParam('page', 1);
$sort       = $this->getRequest()->getParam('sort', 'total_quantity');
$desc       = $this->getRequest()->getParam('desc', 1);
$export     = $this->getRequest()->getParam('export', 0);
$from       = $this->getRequest()->getParam('from', date('01/m/Y') );
$to         = $this->getRequest()->getParam('to', date('d/m/Y'));
$name       = $this->getRequest()->getParam('name');
$area       = $this->getRequest()->getParam('area');
$province   = $this->getRequest()->getParam('province');
$district   = $this->getRequest()->getParam('district');
$sales_from = $this->getRequest()->getParam('sales_from');
$sales_to   = $this->getRequest()->getParam('sales_to');
$store_level = $this->getRequest()->getParam('store_level');

$params = array(
    'page'       => $page,
    'sort'       => $sort,
    'desc'       => $desc,
    'from'       => $from,
    'to'         => $to,
    'name'       => $name,
    'area'       => $area,
    'province'   => $province,
    'district'   => $district,
    'sales_from' => $sales_from,
    'sales_to'   => $sales_to,
    'export'     => $export,
    'area_list'  => $area,
    'store_level' => $store_level

);

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$group_id = $userStorage->group_id;

$QArea            = new Application_Model_Area();
$areas            = $QArea->get_cache();

$QRegionalMarket  = new Application_Model_RegionalMarket();

if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    // lấy khu vực của asm
    $QAsm = new Application_Model_Asm();
    $asm_cache = $QAsm->get_cache();
    $params['area_list'] = isset($asm_cache[ $userStorage->id ]['area']) ? $asm_cache[ $userStorage->id ]['area'] : array();

} elseif ($group_id == My_Staff_Group::SALES) {
    // lấy cửa hàng của sale
    $QStoreStaffLog = new Application_Model_StoreStaffLog();
    $store_cache = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $params['store_list'] = $store_cache;
    $params['sale_id'] = $userStorage->id;
} elseif ($group_id == My_Staff_Group::LEADER) {
    // lấy cửa hàng của sale
    $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
    $store_cache = $QStoreLeaderLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));

    $params['store_list'] = $store_cache;
    $params['leader_id'] = $userStorage->id;
}

if ($area) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area);
    $this->view->provinces = $QRegionalMarket->fetchAll($where, 'name');
}

if ($province) {
    $where = $QRegionalMarket->getAdapter()->quoteInto('parent = ?', $province);
    $this->view->districts = $QRegionalMarket->fetchAll($where, 'name');
}

if ($district) {
    //get store
    $QDistributor = new Application_Model_Distributor();
    $where = $QDistributor->getAdapter()->quoteInto('district = ?', $district);
    $this->view->distributors = $QDistributor->fetchAll($where, 'title');
}

$limit = LIMITATION;
$total = $total2 = 0;

$QImeiKpi = new Application_Model_ImeiKpi();
$sales = $QImeiKpi->fetchDistributor($page, $limit, $total, $params);

$params['get_total_sales'] = true;
$total_sales = $QImeiKpi->fetchDistributor(null, null, $total2, $params);
unset($params['get_total_sales']);
unset($params['asm']);
unset($params['store_list']);
unset($params['area_list']);
unset($params['sale_id']);
unset($params['leader_id']);

$QLoyaltyPlan = new Application_Model_LoyaltyPlan();
$loyalty_plan_list = $QLoyaltyPlan->get_cache();
$this->view->loyalty_plan_list = $loyalty_plan_list;

$this->view->total_quantity  = $total_sales['total_quantity'];
$this->view->total_activated = $total_sales['total_activated'];
$this->view->sales       = $sales;
$this->view->offset      = $limit*($page-1);
$this->view->total       = $total;
$this->view->limit       = $limit;
$this->view->url         = HOST.'timing/analytics-dealer-all'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->desc        = $desc;
$this->view->current_col = $sort;
$this->view->to          = $to;
$this->view->from        = $from;
$this->view->areas       = $areas;
$this->view->params      = $params;
$this->view->action      = 'dealer-all';


$this->_helper->viewRenderer->setRender('analytics-dealer');