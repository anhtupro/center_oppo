<?php
//echo 'Chức năng đã tạm khóa'; die;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QDuplicatedImei = new Application_Model_DuplicatedImei();
$QTiming = new Application_Model_Timing();
$QTimingLog = new Application_Model_TimingLog();
$QTimingSale = new Application_Model_TimingSale();
$QStoreMapping = new Application_Model_StoreMapping();
$QLockedKpi = new Application_Model_LockedKpi();

$db = Zend_Registry::get('db');

$imei = $this->getRequest()->getParam('imei');
$staffOne = $this->getRequest()->getParam('staff-one');
$staffTwo = $this->getRequest()->getParam('staff-two');
$staffNo = $this->getRequest()->getParam('staff-no');
$note = $this->getRequest()->getParam('note');
$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));

$params = [
  'from_date' => $from_date,
  'to_date' => $to_date
];

$locked_kpi = $QLockedKpi->fetchRow([
    'month = ?' => $month,
    'year = ?' => $year
]);

if ($locked_kpi['locked_all']) {
    $flashMessenger->setNamespace('error')->addMessage('Đã chốt KPI tháng ' . $time . ' . Không thể xử lý thêm');
    $this->redirect(HOST . 'timing/imeis-duplicate');
}

if ($locked_kpi['locked_commit_kpi']) {
    $flashMessenger->setNamespace('error')->addMessage('Đã chốt để xử lý 3 file không công, lệch timing, khác chức danh tháng ' . $time . ' . Không thể xử lý thêm');
    $this->redirect(HOST . 'timing/imeis-duplicate');
}


$db->beginTransaction();
if (!empty($imei)) {
    $imei = $str = TRIM(preg_replace('/^[ \t]*[\r\n]+/m', '', $imei));
    $imei = explode("\n", $imei);
    $imei = implode(',', $imei);
    $imei = preg_replace("/[^0-9,\(\)]/", null, $imei);
    $imei = explode(',', $imei);

	$getDup = array();	
	$getInvalidImei = [];
	foreach($imei as $value) {
	    $value  = My_Util::escape_string($value);
		$checkDup = $QDuplicatedImei->checkDupImei(TRIM($value), $params);
		array_push($getDup, $checkDup[0]['imei']);

        // get valid imei dup
        $imei_is_valid = $QDuplicatedImei->checkValidImei($value, $params);
        if (! $imei_is_valid)  {
            array_push($getInvalidImei, $value);
        }

	}

	$getDup = array_filter($getDup);

	if(!empty($getDup)) {
		$getDup = implode(', ', $getDup);
		$flashMessenger->setNamespace('error')->addMessage("Vui lòng xử lý tay imei " . $getDup . " trước khi sử dụng tool!!!");
		$this->redirect(HOST . 'timing/imeis-duplicate');
	}

    if(!empty($getInvalidImei)) {
        $getInvalidImei = implode(', ', $getInvalidImei);
        $flashMessenger->setNamespace('error')->addMessage("Những IMEI này " . $getInvalidImei . " đã được chấm hoặc không có trong báo trùng trong tháng " . $time);
        $this->redirect(HOST . 'timing/imeis-duplicate');
    }
	
    if (isset($staffOne) && $staffOne == 1) {
        foreach ($imei as $value) {
            $value = My_Util::escape_string(TRIM($value));
            $data = array(
                'imei' => $value,
                'director_note' => $note ? $note : 'Imei này đã được chấm cho staff first',
                'solved' => 1,
                'solved_by' => $userStorage->id,
                'solved_at' => date("Y-m-d H:i:s"),
            );
           
            $QDuplicatedImei->updateStaffFirst($data, $params); // thêm dk from date và todate
        }

        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage("Bạn chọn Staff First thành công !!!");
        $this->redirect(HOST . 'timing/imeis-duplicate');
    } elseif (isset($staffTwo) && $staffTwo == 2) {
        foreach ($imei as $value) {
            $value = My_Util::escape_string(TRIM($value));
            $timing_sales_first = $QTimingSale->fetchRow(['imei = ?' => $value]);

             // nếu là PG oppo báo dup
                //insert into timing sale log

            if ($timing_sales_first) {
                $dataLog = array(
                    'imei' => $value,
                    'note' => $note ? TRIM($note) : 'Xử lý báo trùng Imei. Imei này đã được tính cho nhân viên báo trùng',
                    'create_at' => date("Y-m-d H:i:s"),
                );
                $QDuplicatedImei->insertTimingSaleLog($dataLog);

                //get id timing sale log mới insert into
                $getTimingSaleLog = $QDuplicatedImei->getTimingSaleLog($value);

                //xóa timing sale
                $where = $QTimingSale->getAdapter()->quoteInto('imei = ?', $value);
                $QTimingSale->delete($where);
            }

                // update imei cho staff second
                $data = array(
                    'imei' => $value,
                    'director_note' => $note ? $note : 'Imei này đã được chấm cho staff second',
                    'solved' => 1,
                    'solved_by' => $userStorage->id,
                    'solved_at' => date("Y-m-d H:i:s"),
                    'timing_sale_log_id' => $getTimingSaleLog[0]['id_main_max'] ? $getTimingSaleLog[0]['id_main_max'] : NULL,
                );
                $QDuplicatedImei->updateStaffSecond($data, $params); // thêm update from date và to date


                //lưu timing
                $getDupliImei = $QDuplicatedImei->getDupliImei($value, $params);

                $data_timing = [
                    'staff_id' => $getDupliImei['staff_win'],
                    'shift' => 2,
                    'from' => $getDupliImei['date'],
                    'to' => $getDupliImei['date'],
                    'store' => $getDupliImei['store_id'],
                    'note' => "New Tool Duplicated Inserted " . date("Y-m-d H:i:s"),
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id,
                    'approved_at' => date('Y-m-d H:i:s'),
                    'approved_by' => $userStorage->id
                ];
                $id_timing = $QTiming->insert($data_timing);

                //lưu timing sale
                $getModel = $QDuplicatedImei->getModel($value);
                $data_timing_sale = [
                    'product_id' => $getModel[0]['product_id'],
                    'model_id' => $getModel[0]['color_id'],
                    'customer_name' => $getDupliImei['customer_name'],
                    'phone_number' => $getDupliImei['customer_phone'],
                    'address' => $getDupliImei['customer_address'],
                    'imei' => $getDupliImei['imei'],
                    'timing_id' => $id_timing,
                ];
                 $QTimingSale->insert($data_timing_sale);

            // nếu là pg oppo báo dup
        }

        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage("Bạn chọn Staff Second thành công !!!");
        $this->redirect(HOST . 'timing/imeis-duplicate');
    } elseif (isset($staffNo) && $staffNo == 3) {
        foreach ($imei as $value) {
            $value  = My_Util::escape_string(TRIM($value));
            $data = array(
                'imei' => $value,
                'note' => 'Imei này không chấm cho staff nào',
                'solved' => 1,
                'solved_by' => $userStorage->id,
                'solved_at' => date("Y-m-d H:i:s"),
            );
            $QDuplicatedImei->updateStaffFirst($data, $params);
        }

        $db->commit();
        $flashMessenger->setNamespace('success')->addMessage("Bạn chọn không chấm cho staff nào thành công !!!");
        $this->redirect(HOST . 'timing/imeis-duplicate');
    } else {
        $flashMessenger->setNamespace('error')->addMessage("Bạn chưa click imei thuộc Staff được chọn !!!");
        $this->redirect(HOST . 'timing/imeis-duplicate');
    }
} else {
    $flashMessenger->setNamespace('error')->addMessage("Bạn chưa nhập imeis !!!");
    $this->redirect(HOST . 'timing/imeis-duplicate');
}

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages;
