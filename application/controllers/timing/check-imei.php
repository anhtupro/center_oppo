<?php

$this->_helper->layout->disableLayout();
$timing_sales_id = $this->getRequest()->getParam('timing_sales_id');
$is_check = $this->getRequest()->getParam('is_check', 0);

if ($is_check) {
    $imei = trim($this->getRequest()->getParam('value', ''));
    $imei = explode("\n", $imei);
} else {
    $imei = $this->getRequest()->getParam('value');
}

if (!defined("IMEI_ACTIVATION_EXPIRE"))
    define("IMEI_ACTIVATION_EXPIRE", 3);

if($is_check){
    $imei = trim($this->getRequest()->getParam('value', null));

    if($imei){
//        $imei = $str = preg_replace('/^[ \t]*[\r\n]+/m', '', $imei);
        $imei = explode("\n", $imei);
        $imei = "('".implode("'),('", $imei)."')";

        $db = Zend_Registry::get('db');
        $db->query('
            DROP TEMPORARY TABLE IF EXISTS _check_imei_info;

            CREATE TEMPORARY TABLE _check_imei_info(
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                imei_sn varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            ) ENGINE=INNODB DEFAULT CHARSET UTF8 COLLATE UTF8_UNICODE_CI
        ');

        try{
            $db->query('INSERT INTO _check_imei_info(imei_sn) VALUES'.$imei);
        } catch(Exception $e){
            $db = null;
            echo -1;
            exit;
        }

        $data = $db->query('
            SELECT
			t.staff_id pg_id,
			s.`code` pg_code,
			ci.imei_sn imei_sn,
			CONCAT(s.firstname," ",s.lastname) pg_name,
			s.email pg_email,
			st.`name` store_name,
			t.`from` timing_date,
			wi.activated_date activated_date,
			wi.out_date out_date,
			IFNULL(wi.imei_sn, 0) imei_status,
			s.phone_number pg_phone_number,
			ts.customer_name customer_name,
			ts.phone_number customer_phone_number,
			ts.address customer_address,
			null customer_email,
			g.name product_code,
			g.desc product_name,
			t.created_at,
			ida.note note,
						CONCAT(si1.firstname," ",si1.lastname) consutant_kpi,
                        CONCAT(si.firstname," ",si.lastname) pg_kpi,
                         CONCAT(ssi.firstname," ",ssi.lastname) sale_kpi,
                         sti.`name` store_name_kpi,
                         tsl.note log_note,
                         lk.note lock_note
		FROM _check_imei_info ci
		LEFT JOIN timing_sale ts ON ci.imei_sn = ts.imei
		LEFT JOIN timing t ON t.id = ts.timing_id
		LEFT JOIN staff s ON t.staff_id = s.id
		LEFT JOIN store st ON t.store = st.id
                LEFT JOIN imei_kpi  i ON i.imei_sn = ci.imei_sn
				LEFT JOIN imei_kpi_staff  iks ON i.imei_sn = iks.imei_sn
                LEFT JOIN staff si ON i.pg_id = si.id
				LEFT JOIN staff si1 ON iks.staff_id = si1.id
                LEFT JOIN staff ssi ON i.sale_id = ssi.id
                LEFT JOIN store sti ON i.store_id = sti.id
		LEFT JOIN warehouse.imei wi ON ci.imei_sn = wi.imei_sn
		LEFT JOIN warehouse.good g ON wi.good_id = g.id
		LEFT jOIN imei_du_an ida ON ida.imei_sn = ci.imei_sn
                LEFT JOIN (select l.`imei`,group_concat(l.`note`) as note from timing_sale_log l group by l.`imei`) tsl ON tsl.imei = ci.imei_sn
                LEFT JOIN locked_imei lk ON lk.imei_sn = ci.imei_sn
		ORDER BY ci.id ASC
        ')->fetchAll();
        $db = null;

        $this->view->data = $data;
    }
} else 
{ // Check AJAX khi chấm công

    $info = array();
    $imei = trim($imei);
    $return = $this->checkImei($imei, $timing_sales_id, $info);

    if (intval($imei) > 0 && strlen($imei) == 15) {
        $QCheckImeiLog = new Application_Model_CheckImeiLog();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        // $info .= " - IMEIs (".serialize($imeis).")";
        //
        //todo log
        $QCheckImeiLog->insert(array(
            'result' => $return,
            // 'info' => '',
            'imei' => $imei,
            'user_id' => $userStorage->id,
            'ip_address' => $ip,
            'time' => date('Y-m-d H:i:s'),
        ) );
    }

    if ($return==1) {
        // nó đã chấm ở một lúc nào đó
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 0,
                "message" => "Bạn đã báo cáo IMEI này rồi, vào lúc [" . date('d/m/Y H:i:s', strtotime($info['date'])) . "] Tại cửa hàng [".$info['store']."]",
            )
        );

    } else if ($return==2) {

        //not existed in list sales out
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 0,
                "message" => "IMEI không tồn tại. Vui lòng xem kỹ lại IMEI hoặc liên hệ bộ phận Kỹ thuật.",
            )
        );

    } else if ($return==3 || $return==5) {

        // bị thằng khác chấm trước rồi
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 0,
                "message" => 'IMEI '.$imei.' này đã được ['.@$info['staff'].'] báo cáo ở cửa hàng ['.$info['store'].'] vào lúc ['.date('d/m/Y H:i:s', strtotime($info['date'])).']
                            <a href="#" data-timing-sales-id="'.$info['timing_sales_id'].'" data-imei="'.$imei.'"
                            data-case="IMEI '.$imei.' này đã được ['.@$info['staff'].'] báo cáo ở cửa hàng ['.$info['store'].'] vào lúc ['.date('d/m/Y H:i:s', strtotime($info['date'])).']"
                            data-checksum="'.sha1(md5($imei).$info['timing_sales_id']).'"
                            data-staff-first="'.@$info['staff_id'].'"
                            class="send_notify">Bấm vào đây</a> để gửi yêu cầu xử lý.',
            )
        );

    } /*elseif ($return==5) {

        // tồn tại trong list tháng 12 về trước
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => "IMEI này đã được bán cách đây hơn 01 tháng. Bởi [" . $info['staff'].'] ở cửa hàng ['. $info['store'].'] vào lúc ['.date('d/m/Y H:i:s', strtotime($info['date'])).']. Công ty không tính doanh số đối với các máy đã bán ra thị trường hơn 01 tháng. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } */ elseif ($return == 4) {
        // tồn tại trong bảng imei acti
        $QImeiHappyTime = new Application_Model_ImeiHappyTime();
        $data = $QImeiHappyTime->checkImei($imei);
        if(empty($data))
        {
            echo json_encode(array(
                'value' => $imei,
                'valid' => 0,
                'message' => 'IMEI này đã được bán cách đây hơn '.IMEI_ACTIVATION_EXPIRE.' ngày, vào ngày ['.$info['activated_at'].']. Công ty không tính doanh số đối với các máy đã bán ra thị trường hơn '.IMEI_ACTIVATION_EXPIRE.' ngày. Vui lòng liên hệ ASM nếu có thắc mắc.',
            ));
        }
    } elseif ($return == 6) {
        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy tặng khách hàng, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } elseif ($return == 7) {

        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy demo, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    }  elseif ($return == 8) {
        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy xuất cho nhân viên, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } elseif ($return == 9) {
        // tồn tại trong bảng imei acti
        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => 'IMEI này của máy mượn, thuộc diện không tính KPI. Vui lòng liên hệ ASM nếu có thắc mắc.',
        ));

    } elseif ($return == 999) {
        $QBlock = new Application_Model_BlockCheckingImei();
        $data = array(
            'staff_id' => $userStorage->id,
            'date' => date('Y-m-d'),
        );

        try {
            @$QBlock->insert($data);
        } catch (Exception $e) {

        }

        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        session_destroy();

        echo json_encode(array(
            'value' => $imei,
            'valid' => 0,
            'message' => sprintf('Bạn đã thử sai hơn %s lần trong 1 ngày.', IMEI_CHECKING_FAILED_IN_1_DAYS),
        ));
    } else {
        //success
        echo json_encode(
            array(
                "value" => $imei,
                "valid" => 1,
                "message" => ""
            )
        );
    }

    exit;
}
