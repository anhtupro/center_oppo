<?php
//        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
//        $to   = $this->getRequest()->getParam('to', date('d/m/Y'));
//        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

$area = $this->getRequest()->getParam('area', '');

$time     = $this->getRequest()->getParam('time', date('m/Y'));

$time_seg = explode('/', $time);
$month    = $time_seg[0];
$year     = $time_seg[1];
$from = '01/' . $month . '/' . $year;
$dateFormat = $year . '-' . $month . '-01';
$to = date('t/m/Y', strtotime($dateFormat));

$params = array(
    'from' => $from,
    'to'   => $to,
    'area' => $area,
);


$QImeiKpi = new Application_Model_ImeiKpi();
$QArea    = new Application_Model_Area();
$QAreaRankByMonth    = new Application_Model_AreaRankByMonth();


$params['kpi'] = true;
//         $params['dev'] = 1;
$db         = Zend_Registry::get('db');

//        $sell_out = $QImeiKpi->fetchArea($params);
unset($params['kpi']);
$from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
$to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

$db         = Zend_Registry::get('db');
$from_f     = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d 00:00:00");
$to_f       = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d 23:59:59");

$stmt_out = $db->prepare("CALL `PR_fetch_Area`(:p_from_date, :p_to_date)");
$stmt_out->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
$stmt_out->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
//        $stmt_out->bindParam('p_area_id', $area, PDO::PARAM_STR);
$stmt_out->execute();
//        $sell_out = $stmt_out->fetchAll();
$data = $stmt_out->fetchAll();
$stmt_out->closeCursor();
$stmt_out = null;


//        $data_for_point            = $QImeiKpi->fetchArea($params);
$area_list = 0;
$stmt_point = $db->prepare("CALL `PR_fetch_Area_Point`(:p_from_date, :p_to_date, :p_area_list)");
$stmt_point->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
$stmt_point->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
$stmt_point->bindParam('p_area_list', $area_list, PDO::PARAM_STR);

$stmt_point->execute();
$data_for_point = $stmt_point->fetchAll();
$stmt_point->closeCursor();
$stmt_point = null;

$params['get_total_sales'] = true;

//        $total_sales = $QImeiKpi->fetchArea($params);
$total_money = 0;
foreach ($data_for_point as $item){
    $total_money += $item['total_value'];
}

//        $total_money = $total_sales['total_value'];
$point_list  = array();

$all_area     = $QArea->fetchAll();
$region_share = array();

foreach ($all_area as $_key => $_value)
    $region_share[$_value['id']] = $_value['region_share'];



//tính point
foreach ($data_for_point as $item)
    $point_list[$item['area_id']] = ( $total_money > 0 and ( $region_share[$item['area_id']] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($region_share[$item['area_id']] / 100), 2) : 0;


//        My_Report_Sales::area($sell_out, $total_sales, $point_list, $params);

set_time_limit(0);
ini_set('memory_limit', -1);
ini_set('display_error', ~E_ALL);

require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();
$heads    = array('Area',);

// echo "<pre>";print_r($data);die;

$from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
$to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

// các model có đổi giá
$QKpi = new Application_Model_GoodPriceLog();
$list = $QKpi->get_list($from, $to);

// echo "<pre>";print_r($list);die;
$QGood    = new Application_Model_Good();
$products = $QGood->get_cache();

$QGoodColor     = new Application_Model_GoodColor();
$product_colors = $QGoodColor->get_cache();

$product_col        = chr(ord('A') + count($heads));
$product_color_list = array();

$kpi_list = array();

foreach ($data as $_key => $_value) {
    $kpi_list[$_value['area_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
        'total_quantity'            => $_value['total_quantity'],
        'total_activated'           => $_value['total_activated'],
        'total_value'               => $_value['total_value'],
        'total_value_activated'     => $_value['total_value_activated'],
        'total_value_IND'           => $_value['total_value_IND'],
        'total_value_activated_IND' => $_value['total_value_activated_IND'],
    );
}

foreach ($products as $key => $value) {
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key])) {
        foreach ($list[$key] as $_color => $ranges) {
            if ($_color) {
                $product_color_list[$key][] = $_color;

                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                        format('d') . '->' . (new DateTime($range['to']))->format('d');
                }
            } else {
                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                        DateTime($range['to']))->format('d');
                }
            }
        }
    }
}

$heads[] = 'Unit';

foreach ($products as $key => $value) {
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key])) {
        foreach ($list[$key] as $_color => $ranges) {
            if ($_color) {
                $product_color_list[$key][] = $_color;

                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                        format('d') . '->' . (new DateTime($range['to']))->format('d') . ' activated';
                }
            } else {
                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                        DateTime($range['to']))->format('d') . ' activated';
                }
            }
        }
    }
}

$heads[] = 'Unit activated';



$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

$alpha = 'A';
$index = 1;

foreach ($heads as $key) {
    $sheet->setCellValue($alpha . $index, $key);
    $alpha++;
}

$index++;

$alpha = $product_col;
foreach ($products as $key => $value)
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key]))
        foreach ($list[$key] as $_color => $ranges)
            foreach ($ranges as $range)
                $sheet->setCellValue($alpha++ . $index, $range['price']);

$alpha++;
foreach ($products as $key => $value)
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key]))
        foreach ($list[$key] as $_color => $ranges)
            foreach ($ranges as $range)
                $sheet->setCellValue($alpha++ . $index, $range['price']);

$index++;

$QArea      = new Application_Model_Area();
//$area_cache = $QArea->get_cache_all_with_del();
$area_cache = $QArea->getAreaKpi();


// dùng mảng này để lưu thứ tự các area trong result set trên
// vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
$area_array = array();

// điền danh sách area ra file trước
//Cot Area

$alpha = 'A';
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $index, $_area_name);

    $area_array[$_area_id] = $index++; // lưu dòng ứng với area id
}

// duyệt qua dãy model để tính KPI,
// mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
$alpha = $product_col;

$QAsm        = new Application_Model_Asm();
$asm_cache   = $QAsm->get_cache();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$total_by_area                             = array();
$total_activated_by_area                   = array();
$total_value_activated_by_area             = array();
$total_value_activated_by_area_IND         = array();
$total_value_activated_by_area_KA          = array();
$total_value_by_area                       = array();
$total_value_by_area_IND                   = array();
$total_value_by_area_KA                    = array();


foreach ($products as $_product_id => $value) {
    // các model có đổi giá, tách thành 2 cột
    if (isset($list[$_product_id])) {
        foreach ($list[$_product_id] as $_color => $ranges) {
            foreach ($ranges as $range) {
                foreach ($kpi_list as $_area_id => $_data) {
                    // nếu có trong danh sách NV ở trên
                    if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                        continue;

                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                    if (isset($area_array[$_area_id]) && isset($asm_cache[$userStorage->id]['area']) && in_array($_area_id, $asm_cache[$userStorage->id]['area'])) {
                        // đưa vào đúng dòng luôn
                        if (!isset($total_by_area[$_area_id])) {
                            $total_by_area[$_area_id] = 0;
                        }
                        if (!isset($total_value_by_area[$_area_id])) {
                            $total_value_by_area[$_area_id]     = 0;
                            $total_value_by_area_IND[$_area_id] = 0;
                            $total_value_by_area_KA[$_area_id]  = 0;
                        }
                        if (!isset($total_value_activated_by_area[$_area_id])) {
                            $total_value_activated_by_area[$_area_id]     = 0;
                            $total_value_activated_by_area_IND[$_area_id] = 0;
                            $total_value_activated_by_area_KA[$_area_id]  = 0;
                        }
                        $total_by_area[$_area_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                        $total_value_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                        $total_value_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                        $total_value_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                if ($_product_id == 597) {
//                                    if (!isset($total_value_a71k_model_by_area[$_area_id])) {
//                                        $total_value_a71k_model_by_area[$_area_id]     = 0;
//                                        $total_value_a71k_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a71k_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a71k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a71k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a71k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(622, 661))) {
//                                    if (!isset($total_value_a3s16g_model_by_area[$_area_id])) {
//                                        $total_value_a3s16g_model_by_area[$_area_id]     = 0;
//                                        $total_value_a3s16g_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a3s16g_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a3s16g_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a3s16g_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a3s16g_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
                        if ($_product_id == 750) {
                            if (!isset($total_value_k3_model_by_area[$_area_id])) {
                                $total_value_k3_model_by_area[$_area_id]     = 0;
                                $total_value_k3_model_by_area_IND[$_area_id] = 0;
                                $total_value_k3_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_k3_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_k3_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_k3_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }

                        if (in_array($_product_id, array(808))) {
                            if (!isset($total_value_a1k_subsidi_model_by_area[$_area_id])) {
                                $total_value_a1k_subsidi_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_subsidi_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_subsidi_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_subsidi_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_subsidi_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_subsidi_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
                        if (in_array($_product_id, array(708))) {
                            if (!isset($total_value_a1k_model_by_area[$_area_id])) {
                                $total_value_a1k_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
//                                if (in_array($_product_id, array(747))) {
//                                    if (!isset($total_value_a1k_model_by_area[$_area_id])) {
//                                        $total_value_reno2f_model_by_area[$_area_id]     = 0;
//                                        $total_value_reno2f_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_reno2f_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_reno2f_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_reno2f_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_reno2f_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(703))) {
//                                    if (!isset($total_value_a5s_model_by_area[$_area_id])) {
//                                        $total_value_a5s_model_by_area[$_area_id]     = 0;
//                                        $total_value_a5s_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a5s_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a5s_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a5s_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a5s_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }

                        $total_value_activated_by_area[$_area_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;
                        $total_value_activated_by_area_IND[$_area_id] += isset($tmp['total_value_activated_IND']) ? $tmp['total_value_activated_IND'] : 0;
                        $total_value_activated_by_area_KA[$_area_id] += $tmp['total_value_activated'] - $tmp['total_value_activated_IND'];
                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0);
                    } elseif (!in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) ||
                        My_Staff_Permission_Area::view_all($userStorage->id)) {
                        if (!isset($total_by_area[$_area_id]))
                            $total_by_area[$_area_id]                 = 0;
                        if (!isset($total_value_by_area[$_area_id]))
                            $total_value_by_area[$_area_id]           = 0;
                        if (!isset($total_value_activated_by_area[$_area_id]))
                            $total_value_activated_by_area[$_area_id] = 0;

                        $total_by_area[$_area_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                        $total_value_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                        $total_value_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                        $total_value_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];

                        $total_value_activated_by_area[$_area_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;
                        $total_value_activated_by_area_IND[$_area_id] += isset($tmp['total_value_activated_IND']) ? $tmp['total_value_activated_IND'] : 0;
                        $total_value_activated_by_area_KA[$_area_id] += $tmp['total_value_activated'] - $tmp['total_value_activated_IND'];

//                                if ($_product_id == 597) {
//                                    if (!isset($total_value_a71k_model_by_area[$_area_id])) {
//                                        $total_value_a71k_model_by_area[$_area_id]     = 0;
//                                        $total_value_a71k_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a71k_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a71k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a71k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a71k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(622, 661))) {
//                                    if (!isset($total_value_a3s16g_model_by_area[$_area_id])) {
//                                        $total_value_a3s16g_model_by_area[$_area_id]     = 0;
//                                        $total_value_a3s16g_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a3s16g_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a3s16g_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a3s16g_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a3s16g_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
                        if ($_product_id == 750) {
                            if (!isset($total_value_k3_model_by_area[$_area_id])) {
                                $total_value_k3_model_by_area[$_area_id]     = 0;
                                $total_value_k3_model_by_area_IND[$_area_id] = 0;
                                $total_value_k3_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_k3_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_k3_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_k3_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }

                        if (in_array($_product_id, array(808))) {
                            if (!isset($total_value_a1k_subsidi_model_by_area[$_area_id])) {
                                $total_value_a1k_subsidi_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_subsidi_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_subsidi_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_subsidi_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_subsidi_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_subsidi_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
                        if (in_array($_product_id, array(708))) {
                            if (!isset($total_value_a1k_model_by_area[$_area_id])) {
                                $total_value_a1k_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
//                                if (in_array($_product_id, array(747))) {
//                                    if (!isset($total_value_reno2f_model_by_area[$_area_id])) {
//                                        $total_value_reno2f_model_by_area[$_area_id]     = 0;
//                                        $total_value_reno2f_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_reno2f_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_reno2f_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_reno2f_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_reno2f_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(703))) {
//                                    if (!isset($total_value_a5s_model_by_area[$_area_id])) {
//                                        $total_value_a5s_model_by_area[$_area_id]     = 0;
//                                        $total_value_a5s_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a5s_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a5s_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a5s_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a5s_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }

                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0);
                    } else
                        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
                }

                $alpha++;
            }
        }
    }
}

foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_by_area[$_area_id])) {
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_by_area[$_area_id]);
        $total_all_area += $total_by_area[$_area_id];
    } else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Unit activated
foreach ($products as $_product_id => $value) {
    // các model có đổi giá, tách thành 2 cột
    if (isset($list[$_product_id])) {
        foreach ($list[$_product_id] as $_color => $ranges) {
            foreach ($ranges as $range) {
                foreach ($kpi_list as $_area_id => $_data) {
                    if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                        continue;

                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                    // nếu có trong danh sách NV ở trên
                    if (isset($area_array[$_area_id]) && isset($asm_cache[$userStorage->id]['area']) &&
                        in_array($_area_id, $asm_cache[$userStorage->id]['area'])) {
                        // đưa vào đúng dòng luôn
                        if (!isset($total_activated_by_area[$_area_id]))
                            $total_activated_by_area[$_area_id] = 0;
                        $total_activated_by_area[$_area_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0);
                    }
                    elseif (!in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) ||
                        My_Staff_Permission_Area::view_all($userStorage->id)) {
                        if (!isset($total_activated_by_area[$_area_id]))
                            $total_activated_by_area[$_area_id] = 0;
                        $total_activated_by_area[$_area_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0);
                    } else
                        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
                }

                $alpha++;
            }
        }
    }
}



foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_activated_by_area[$_area_id])) {
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_activated_by_area[$_area_id]);
        $total_activated_all_area += $total_activated_by_area[$_area_id];
    } else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}


$alpha++;


$filename  = 'Report Area Detail ' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;