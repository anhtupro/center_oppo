<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$QCheckKpiStaff = new Application_Model_CheckKpiStaff();
$QLockedKpi = new Application_Model_LockedKpi();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $this->getRequest()->getParam('staff_id');
$remove_type = $this->getRequest()->getParam('remove_type'); // 1: pg, 2: sale, 3:consultant, 4:store leader, 5: lệch timing - pg và consultant
$from_date = $this->getRequest()->getParam('from_date'); // Tg xóa kpi
$to_date = $this->getRequest()->getParam('to_date'); // Tg xóa kpi
$check_kpi_staff_id = $this->getRequest()->getParam('check_kpi_staff_id');
$note = $this->getRequest()->getParam('note');
$month = $this->getRequest()->getParam('month'); // tháng xử lý KPI
$year = $this->getRequest()->getParam('year'); // tháng xử lý kpi
$time = $month . '-' . $year;

$from_date = str_replace('/', '-', $from_date);
$to_date = str_replace('/', '-', $to_date);

$from_date = date('Y-m-d', strtotime($from_date));
$to_date = date('Y-m-d', strtotime($to_date));

$from_date_time = date('Y-m-d 00:00:00', strtotime($from_date));
$to_date_time = date('Y-m-d 23:59:59', strtotime($to_date));

// month year check
$month_from_date = date('m', strtotime($from_date));
$year_from_date = date('Y', strtotime($from_date));
$month_to_date = date('m', strtotime($to_date));
$year_to_date = date('Y', strtotime($to_date));
// end month year check

// check tháng đã chốt kpi hoặc xử lý quá 1 tháng so với tháng hiện tại
$current_month = date('m');
$current_year = date('Y');

$diff_month = (($current_year - $year) * 12) + ($current_month - $month);
$locked_kpi = $QLockedKpi->fetchRow([
    'month = ?' => $month,
    'year = ?' => $year
]);
//


if ($diff_month > 1) {
    echo json_encode([
        'status' => 1,
        'message' => 'Không thể xử lý KPI quá 1 tháng trước !'
    ]);
    return;
}

if ($locked_kpi['locked_all']) {
    echo json_encode([
        'status' => 1,
        'message' => 'Đã chốt KPI tháng ' . $time . ' . Không thể xử lý thêm'
    ]);
    return;
}


if (! in_array($userStorage->id, [341])) {
    echo json_encode([
        'status' => 1,
        'message' => "Không có quyền thao tác!"
    ]);
    return;
}

if (!$staff_id || !$remove_type || !$from_date || !$to_date) {
    echo json_encode([
        'status' => 1,
        'message' => 'Thiếu thông tin'
    ]);
    return;
}

// tháng xử lý KPI và khoảng thời gian xóa KPI phải trùng nhau
if ($month_from_date != $month || $month_to_date != $month || $year_from_date != $year || $year_to_date != $year) {
    echo json_encode([
        'status' => 1,
        'message' => 'Thời gian để xóa KPI không đúng với tháng xử lý KPI'
    ]);
    return;
}

if ($from_date > $to_date) {
    echo json_encode([
        'status' => 1,
        'message' => 'From date phải nhỏ hơn hoặc bằng To date '
    ]);
    return;
}




$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'from_date_time' => $from_date_time,
    'to_date_time' => $to_date_time,
    'staff_id' => $staff_id
];

$sellout_remove = 0;

if ($remove_type == 1) {
    $sellout_remove = $QCheckKpiStaff->getSelloutPg($params);
    $QCheckKpiStaff->updateKpiPg($params);
}

if ($remove_type == 2) {
    $sellout_remove = $QCheckKpiStaff->getSelloutSale($params);
    $QCheckKpiStaff->updateKpiSale($params);
}

if ($remove_type == 3) {
    $sellout_remove = $QCheckKpiStaff->getSelloutConsultant($params);
    $QCheckKpiStaff->updateKpiConsultant($params, 7);
}

if ($remove_type == 4) {
    $sellout_remove = $QCheckKpiStaff->getSelloutStoreLeader($params);
    $QCheckKpiStaff->updateKpiStoreLeader($params);
}

if ($remove_type == 5) {
    $sellout_pg_remove = $QCheckKpiStaff->getSelloutPg($params);
    $sellout_consultant_remove = $QCheckKpiStaff->getSelloutConsultant($params);
    $sellout_remove = $sellout_pg_remove + $sellout_consultant_remove;

    $QCheckKpiStaff->updateKpiPg($params);
    $QCheckKpiStaff->updateKpiConsultant($params, 7);
}


$QCheckKpiStaff->update([
    'status' => $remove_type,
    'note' => $note,
    'remove_from_date' => $from_date,
    'remove_to_date' => $to_date,
    'sellout_remove' => $sellout_remove ? $sellout_remove : 0
], ['id = ?' => $check_kpi_staff_id]);

$remove_type_title = [
    1 => 'Đã xóa KPI PG' ,
    2 => 'Đã xóa KPI Sale',
    3 => 'Đã xóa KPI Consultant',
    4 => 'Đã xóa KPI Store Leader',
    5 => 'Đã xóa KPI'
];

echo json_encode([
    'status' => 0,
    'remove_type_title' => $remove_type_title[$remove_type],
    'sellout_remove' => $sellout_remove
]);