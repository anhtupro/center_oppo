<?php
require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();
$QKpiByModel = new Application_Model_KpiByModel();
$QArea = new Application_Model_Area();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$time = $this->getRequest()->getParam('time', date('m/Y'));
$time = str_replace('/', '-', $time);
$month = explode('-', $time) [0]   ?   explode('-', $time) [0]   :  date('m');
$year = explode('-', $time) [1]   ?   explode('-', $time) [1]   :  date('Y');
$from_date = date('Y-m-d 00:00:00', strtotime('01-' . $time));
$to_date = date('Y-m-t 23:59:59', strtotime('01-' . $time));


$params = [
    'from_date' => $from_date,
    'to_date' => $to_date,
    'group_by_type' => 1
];

$db = Zend_Registry::get('db');
$stmt = $db->prepare("CALL `sp_get_quantity_detail_by_model`(:p_from_date, :p_to_date, :p_group_by_type)");
$stmt->bindParam('p_from_date', $params['from_date']);
$stmt->bindParam('p_to_date', $params['to_date']);
$stmt->bindParam('p_group_by_type', $params['group_by_type']);
$stmt->execute();
$data = $stmt->fetchAll();
$stmt->closeCursor();
$stmt = null;

$data_convert = $QKpiByModel->convertDataQuantityDetail($data, $params['group_by_type']);
$list_model = $QKpiByModel->getListModel($params);
$list_area = $QArea->getAreaKpi();

// list quantity
$heads = [];
$heads [] =  [
    'head_name' => 'Area'
];

foreach ($list_model as $good_price_log_id => $good) {
    $heads [] =  [
        'head_name' => $good['model_name'],
        'model_price' => $good['price']
    ];
}
$heads [] =  [
    'head_name' => 'Total'
];

// list quantity activated
foreach ($list_model as $good_price_log_id => $good) {
    $heads [] =  [
        'head_name' => $good['model_name'],
        'model_price' => $good['price']
    ];
}
$heads [] =  [
    'head_name' => 'Total activated'
];


$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();
// set 2 dong đầu
$alpha    = 'A';
$row_1    = 1;
$row_2    = 2;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$row_1, $key['head_name']);
    $sheet->setCellValue($alpha.$row_2, $key['model_price']);
    $alpha++;
}
// end set 2 dòng đầu

// set từ dòng 3
$index    = 3;

$i = 1;

foreach($list_area as $area_id => $area_name ){
    $alpha    = 'A';

    $sheet->setCellValue($alpha++.$index, $area_name);

    // set số lượng mỗi model
    $total_quantity = 0;
    foreach ($list_model as $good_price_log_id => $good) {
        $quantity = $data_convert [$area_id] [$good_price_log_id] ['quantity'];
        $sheet->setCellValue($alpha++.$index, $quantity);

        $total_quantity += $quantity;
    }

    // set số lượng tổng các model
    $sheet->setCellValue($alpha++.$index, $total_quantity);



    // set số lượng mỗi model avtivated
    $total_quantity_activated = 0;
    foreach ($list_model as $good_price_log_id => $good) {
        $quantity_activated = $data_convert [$area_id] [$good_price_log_id] ['quantity_activated'];
        $sheet->setCellValue($alpha++.$index, $quantity_activated);

        $total_quantity_activated += $quantity_activated;
    }

    // set số lượng tổng  model avtivated
    $sheet->setCellValue($alpha++.$index, $total_quantity_activated);


    $index++;
}

$filename = 'Report Area - Chi tiết số lượng theo model ' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;

