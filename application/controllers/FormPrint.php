<?php

/**
 *
 */
class Application_Model_FormPrint extends Zend_Db_Table_Abstract
{
  protected $_name = 'form_print';

  private $mac_users = array("31","1357","5384","6183",'15025');
  private $msNhi_code = '2128';

  public function fetchData($page, $limit, &$total, $params){
      $db = Zend_Registry::get('db');
      $select = $db->select()
                   ->from(array('p' => $this->_name),
                       array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

      if( isset($params['id']) AND $params['id'] ){
        $select->where('p.id = ?', $params['id']);
      }
      if( isset($params['sender_name']) AND $params['sender_name'] ){
        $select->where('p.sender_name LIKE ?', '%'.$params['sender_name'].'%');
      }
      if( isset($params['receiver_name']) AND $params['receiver_name'] ){
        $select->where('p.receiver_name LIKE ?', '%'.$params['receiver_name'].'%');
      }
      if( isset($params['sender_department']) AND $params['sender_department'] ){
        $select->where('p.sender_department LIKE ?', '%'.$params['sender_department']);
      }
      if( isset($params['receiver_address']) AND $params['receiver_address'] ){
        $select->where('p.receiver_address LIKE ?', '%'.$params['receiver_address'].'%');
      }
      if( isset($params['receiver_phone']) AND $params['receiver_phone'] ){
        $select->where('p.receiver_phone LIKE ?', '%'.$params['receiver_phone']);
      }
      if( isset($params['sender_phone']) AND $params['sender_phone'] ){
        $select->where('p.sender_phone LIKE ?', '%'.$params['sender_phone'].'%');
      }
      if( isset($params['mail_code']) AND $params['mail_code'] ){
        $select->where('p.mail_code LIKE ?', '%'.$params['mail_code'].'%');
      }
      if(isset($params['created_at']) and $params['created_at'] ){
        $arrFrom = explode('/',$params['created_at']);
        $params['created_at'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
        $select->where('p.created_at >= ?' , $params['created_at']);
      }
      if(isset($params['created_to']) and $params['created_to'] ){
        $arrFrom = explode('/',$params['created_to']);
        $params['created_to'] = $arrFrom[2].'-'.$arrFrom[1].'-'.($arrFrom[0]+1);
        $select->where('p.created_at < ?' , $params['created_to']);
      }
      if( isset($params['created_by']) AND $params['created_by'] ){
        $mac_user = $this->mac_users;//macbook user
        if( $params['created_by'] == '15025' ){
          $select->where('p.created_by IN (?)' , $mac_user);
        }else if($params['created_by'] != $this->msNhi_code){
          $select->where('p.created_by = ?' , $params['created_by']);
        }
      }

      $select->where('p.del != ?',1);
      $select->order('p.created_at DESC');
      if($params['export'] != 1){
        $select->limitPage($page, $limit);
      }
      $result = $db->fetchAll($select);
      $total = $db->fetchOne("select FOUND_ROWS()");
      return $result;

  }

}
