<?php
    //    $from = $this->getRequest()->getParam('from', date('01/m/Y'));
    //    $to   = $this->getRequest()->getParam('to', date('d/m/Y'));
    $area = $this->getRequest()->getParam('area');
    $month_year = $this->getRequest()->getParam('month_year', date('m/Y'));

    $month_year = str_replace('/', '-', $month_year);
    $month = explode('-', $month_year) [0]   ?   explode('-', $month_year) [0]   :  date('m');
    $year = explode('-', $month_year) [1]   ?   explode('-', $month_year) [1]   :  date('Y');
    $from = date('d/m/Y', strtotime('01-' . $month_year));
    $to = date('t/m/Y', strtotime('01-' . $month_year));

    $params = array(
        'from' => $from,
        'to'   => $to,
        'area' => $area,
    );
//    echo "<pre>";
//    print_r($params);
//    echo "</pre>";
set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('display_error', ~E_ALL);
        require_once 'PHPExcel.php';
        $PHPExcel    = new PHPExcel();
        $heads       = array(
            'Staff',
            'Code',
            'Title',
            'Email',
            'Province',
        );
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $db         = Zend_Registry::get('db');
        $from_f     = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d 00:00:00");
        $to_f       = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d 23:59:59");
        $sql_params = [
            $from_f,
            $to_f,
            '',
            -1
        ];

        $sql           = 'CALL sp_report_leader_asm_qty(?,?,?,?,@total_value)';
        $stmt          = $db->query($sql, $sql_params);
        $list_data     = $stmt->fetchAll();
        $list_by_staff = My_Util::changeKey($list_data, "sale_region_staff_log_id");
        
       
        $stmt->closeCursor();

        // các model có đổi giá
        $QKpi     = new Application_Model_GoodPriceLog();
        $list     = $QKpi->get_list($from, $to);
        // echo "<pre>";print_r($list);die;
        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        // echo "<pre>";print_r($list);die;
        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        $QImeiKpi      = new Application_Model_ImeiKpi();
        $params['kpi'] = true;
        
        $stmt_asm = $db->prepare("CALL `PR_fetch_Region_Share_Asm`(:p_from_date, :p_to_date)");
        $stmt_asm->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
        $stmt_asm->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
        $stmt_asm->execute();
        $data_asm = $stmt_asm->fetchAll();
        $stmt_asm->closeCursor();
        $stmt_asm = null;
        
//         $data_asm = $QImeiKpi->fetchRegionShareAsm($params);

//        $data          = $QImeiKpi->fetchLeaderQty($params);
        $stmt_tt = $db->prepare("CALL `PR_fetch_Leader_Qty`(:p_from_date, :p_to_date)");
        $stmt_tt->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
        $stmt_tt->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
        $stmt_tt->execute();
        $data = $stmt_tt->fetchAll();
        $stmt_tt->closeCursor();
        $stmt_tt = null;
      

       
        // $data_asm           = $QImeiKpi->fetchRegionShareAsm($params);

        unset($params['kpi']);
        $params['get_total_sales'] = true;

        /** total sales phải lấy số của cả nước */
//                        $total_sales = $QImeiKpi->fetchArea($params);
//        echo "<pre>";print_r($total_sales);die;
        // $total_money    = $total_sales['total_value'];die;
        // echo "<pre>";print_r($total_money);die;
        // if($userStorage->id == 5899){
        //     // echo date('t',strtotime($from));die;
        //     // echo "<pre>";print_r($total_money);die;
        // }
        unset($params['get_total_sales']);

        $point_list    = array();
        $data_area_sum = array(); // dùng để cộng các giá trị của leader
        $asm_array     = array(); // danh sách các asm sẽ nắm các vùng chưa có leader
        $sales         = array();
        $kpi_list      = array();
        $info_list     = array(); // thông tin liên quan leader

        foreach ($data as $_key => $_value) {

            $kpi_list[$_value['sales_region_staff_log_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
                'total_quantity'        => intval($_value['total_quantity']),
                'total_activated'       => intval($_value['total_activated']),
                'total_value'           => $_value['total_value'],
                'total_value_bonus'     => $_value['total_value_bonus'],
                'total_value_activated' => $_value['total_value_activated'],
                'total_sellout_sale'    => intval($_value['total_sellout_sale']),
            );

            $total_day_in_month = date('t', strtotime($from));
            // $dateFrom = null;
            if (strtotime($_value['staff_from_date']) <= strtotime($from)) {
                $dateFrom = $from;
            } else {
                $dateFrom = $_value['staff_from_date'];
            }

            if (empty($_value['staff_to_date'])) {
                $dateTo = date('Y-m-t', strtotime($from));
            } else {
                $dateTo = ($_value['staff_to_date']);
            }
            // $date_1   =  date_create_from_format("d/m/Y", $dateFrom)->format("Y-m-d");
            // $date_2   =  date_create_from_format("d/m/Y", $dateTo)->format("Y-m-d");
            $day_diff                                        = My_Date::date_diff($dateFrom, $dateTo);
            $info_list[$_value['sales_region_staff_log_id']] = array(
//                'staff_name'         => $_value['staff_name'],
//                'code'               => $_value['code'],
//                'email'              => $_value['email'],
//                'region_name'        => $_value['region_name'],
//                'region_shared'      => $_value['region_shared'],
//                'area_name'          => $_value['area_name'],
//                'area_shared'        => $_value['area_shared'],
//                'staff_title'        => $_value['staff_title'],
//                // 'joined_at' => date('d/m/Y', strtotime($_value['joined_at'])),
//                // 'off_date' => isset($_value['off_date']) ? $_value['off_date'] : null
//                'from_date'          => date('d/m/Y', strtotime($_value['staff_from_date'])),
//                // 'to_date' => isset($_value['staff_to_date']) ?  date('d/m/Y', strtotime($_value['staff_to_date'])) : null,
//                // 'from_date' => strtotime(date('d/m/Y', strtotime($_value['staff_from_date']))),
//                'to_date'            => isset($_value['staff_to_date']) ? date('d/m/Y', strtotime($_value['staff_to_date'])) : null,
//                // 'date_from' => $dateFrom,
//                // 'date_to' => $dateTo,
//                'date_from'          => date('d/m/Y', strtotime($dateFrom)),
//                'date_to'            => date('d/m/Y', strtotime($dateTo)),
//                'date_diff'          => $day_diff + 1,
//                'total_day_in_month' => $total_day_in_month,
//                'value_80_off'       => empty($_value['staff_to_date']) ? 999 : $QImeiKpi->getValue80(date('d/m/Y', strtotime($dateFrom)), date('d/m/Y', strtotime($dateTo)), null),
            );
        }

        // echo "<pre>";print_r($kpi_list);die;
//        foreach ($data_asm as $_value) {
//
//            $kpi_list[$_value['sales_region_staff_log_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
//                'total_quantity'        => intval($_value['total_quantity']),
//                'total_activated'       => intval($_value['total_activated']),
//                'total_value'           => $_value['total_value'],
//                'total_value_bonus'     => $_value['total_value_bonus'],
//                'total_value_activated' => $_value['total_value_activated'],
//            );
//
//            // $info_list[ $_value['sales_region_staff_log_id'] ] = array(
//            //     'from_date' => date('d/m/Y', strtotime($_value['from_date'])),
//            // );
//        }



        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                        }
                    }
                }
            }
        }
//         echo "<pre>";print_r($list);die;
        $heads[] = 'Unit';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' activated';
                        }
                    } else {
                        foreach ($ranges as $range) {
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' activated';
                        }
                    }
                }
            }
        }
        // echo "<pre>";print_r($product_color_list);die;
        $heads[] = 'Unit activated';
        $heads[] = 'Value (80%)';
        // $heads[] = 'KPI Sale';
        // $heads[] = 'Sellout Sale';
        $heads[] = 'Value activated (80%)';
        $heads[] = 'Area';
        $heads[] = 'Area Share';
        // $heads[] = 'Province';
        $heads[] = 'Province Share (%)';

        $heads[] = 'Point';
        // $heads[] = 'Point mới';
        $heads[] = '% Doanh Thu';
        $heads[] = 'KPI';
        $heads[] = 'From date';
        $heads[] = 'To date';
        $heads[] = 'Date 1';
        $heads[] = 'Date 2';
        $heads[] = 'Số ngày làm việc';
        $heads[] = 'Tổng số ngày trong tháng';
        $heads[] = 'Tổng số value';

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        // echo "<pre>";print_r($heads);die;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;

        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        // echo $alpha;die;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        // dùng mảng này để lưu thứ tự các area trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $leader_array = array();



        // điền danh sách area ra file trước
        foreach ($info_list as $_staff_id => $_info) {
            $alpha                    = 'A';
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['staff_name']);
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['code']);
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['staff_title']);
            $sheet->setCellValue($alpha++ . $index, str_replace(EMAIL_SUFFIX, '', $list_by_staff[$_staff_id]['email']));
            $sheet->setCellValue($alpha++ . $index, $list_by_staff[$_staff_id]['region_name']);
            $leader_array[$_staff_id] = $index++; // lưu dòng ứng với area id
        }

        // echo "<pre>";print_r($leader_array);die;
        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha             = $product_col;
        $arr_total         = array();
        $arr_active        = array();
        $arr_value         = array();
        // $arr_value_bonus           = array();
        $arr_value_active  = array();
        $arr_value_sellout = array();
        // echo "<pre>";print_r(count($list));
        // echo "<pre>";print_r($list);    die;
        // echo "<pre>";print_r($kpi_list);
        // die;

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            if (!isset($arr_total[$_staff_id])) {
                                $arr_total[$_staff_id] = 0;
                            }

                            if (!isset($arr_active[$_staff_id])) {
                                $arr_active[$_staff_id] = 0;
                            }

                            // if (!isset($arr_value[ $_staff_id ])){
                            //     $arr_value[ $_staff_id ] = 0;
                            // }
                            // if (!isset($arr_value_bonus[ $_staff_id ])){
                            //     $arr_value_bonus[ $_staff_id ] = 0;
                            // }

                            if (!isset($arr_value_active[$_staff_id])) {
                                $arr_value_active[$_staff_id] = 0;
                            }

                            if (!isset($arr_value_sellout[$_staff_id])) {
                                $arr_value_sellout[$_staff_id] = 0;
                            }

                            $arr_total[$_staff_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                            $arr_active[$_staff_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;


                            $arr_value[$_staff_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_ += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            // $arr_value_bonus[$_staff_id]  += isset($tmp['total_value_bonus']) ? $tmp['total_value_bonus'] : 0;
                            // $arr_value_sellout[$_staff_id]  += isset($tmp['total_sellout_sale']) ? $tmp['total_sellout_sale'] : 0;
                            $arr_value_active[$_staff_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                            // $sheet->setCellValue($alpha . $leader_array[$_staff_id],intval($tmp['total_quantity']));
                            if (isset($leader_array[$_staff_id])) {
                                $sheet->setCellValue($alpha . $leader_array[$_staff_id], intval($tmp['total_quantity']));
                            }

                            unset($tmp);
                        }
                        // echo "<pre>";print_r($arr_value);die;
                        $alpha++;
                    }
                }
            }
        }

        //unit
        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_total[$_staff_id])) {
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_quantity']);
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
        }

        $alpha++;

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($kpi_list as $_staff_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                                continue;

                            $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                            if (!isset($arr_total[$_staff_id])) {
                                $arr_total[$_staff_id] = 0;
                            }

                            if (!isset($arr_active[$_staff_id])) {
                                $arr_active[$_staff_id] = 0;
                            }

                            if (!isset($arr_value[$_staff_id])) {
                                $arr_value[$_staff_id] = 0;
                            }

                            if (!isset($arr_value_bonus[$_staff_id])) {
                                $arr_value_bonus[$_staff_id] = 0;
                            }

                            if (!isset($arr_value_active[$_staff_id])) {
                                $arr_value_active[$_staff_id] = 0;
                            }
                            if (!isset($arr_value_sellout[$_staff_id])) {
                                $arr_value_sellout[$_staff_id] = 0;
                            }


                            if (isset($leader_array[$_staff_id])) {
                                $sheet->setCellValue($alpha . $leader_array[$_staff_id], intval($tmp['total_activated']));
                            }
                            unset($tmp);
                        }

                        $alpha++;
                    }
                }
            }
        }

        //activated
        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_active[$_staff_id])) {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], intval($arr_active[$_staff_id]));
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_activated']);
        }

        $alpha++;

        //value
        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_value[$_staff_id])) {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $arr_value[$_staff_id]);
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_value']);
        }

        $alpha++;


        foreach ($leader_array as $_staff_id => $_value) {
//            if (isset($arr_value_active[$_staff_id])) {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], $arr_value_active[$_staff_id]);
//            } else {
//                $sheet->setCellValue($alpha . $leader_array[$_staff_id], 0);
//            }
            $sheet->setCellValue($alpha . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['total_value_activated']);
        }

        $alpha++;
       
        foreach ($leader_array as $_staff_id => $_info) {

            $tmp_alpha = $alpha;
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['area_name']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['area_shared']);
            // $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $info_list[$_staff_id]['region_name']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['province_share']);


            // $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $point);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['point']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['point_doanh_thu']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['value_kpi']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['staff_from_date']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['staff_to_date']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['final_from']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['final_to']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['day_diff']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['day_diff']);
            $sheet->setCellValue($tmp_alpha++ . $leader_array[$_staff_id], $list_by_staff[$_staff_id]['sum_total_value']);
        }


        $filename  = 'Sell out - Leader - new' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;