<?php
$db = Zend_Registry::get('db');
$this->_helper->viewRenderer->setNoRender();
    $pars = array_merge(
            array(
                    'from_date' => date('1/m/Y', strtotime("-1 months")),
                    'to_date' => date('t/m/Y' , strtotime("-1 months")),
                    'policy' => null,
                    'title' => null,
                    'name' => null,
                    'code' => null,
                    'area' => null,
                    'page' => 1,
            ),
            
            $this->_request->getParams()
		);
//                print_r($pars);
		$pars['from_date'] = !empty($pars['from_date']) ? DateTime::createFromFormat('d/m/Y', $pars['from_date'])->format('Y-m-d') : null;
		$pars['to_date'] = !empty($pars['to_date']) ? DateTime::createFromFormat('d/m/Y', $pars['to_date'])->format('Y-m-d') : null;
		
		$this->view->from_date = $pars['from_date'];
		$this->view->to_date = $pars['to_date'];

		// echo "<pre>";print_r($pars);die


		if($this->_request->getParam('export_qty'))
		{
		// $stmt = $db->prepare('CALL SP_imei_kpi_GetByTitle(:from_date, :to_date, :name, :code, :area, :title, :limit, :offset, @total)');
		
		$stmt = $db->prepare('CALL SP_report_by_policy_qty(:from_date, :to_date, :limit, :offset, @total)');

               
		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
		$stmt->bindParam('limit', $limit, PDO::PARAM_INT);
		$stmt->bindParam('offset', $offset, PDO::PARAM_INT);
		$stmt->execute();
		
		$data_export = $stmt->fetchAll();
		
		$stmt->closeCursor();
		$data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
		$stmt = null;
                print_r($data_export);
                exit();
                $ik = new Application_Model_ImeiKpi2();
                $data_export =  $ik->GetByTitle(null, null, $pars);
 print_r($data_export);
 exit();
			// echo "<pre>";print_r($data_export['data']);die;

			set_time_limit(0);
            error_reporting(0);
            ini_set('memory_limit', -1);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();


            $heads = array(
                /*'No.',*/
               	'Name',
				'Code',
				'Area',
				'Title',
				'Chính sách công ty - PG',
				// 'Chính sách Thanh Hóa cũ - PG & PB-Sales',
				// 'Chính sách Thanh Hóa (IND & KA) - PG & PB-Sales',
				// 'Chính sách TGDĐ - PG & PB-Sales',
				// 'Chính sách PG Partner (IND & KA)',
				// 'Chính sách PG Partner TGDĐ',
				'Chính sách IND - Sales',
				'Chính sách KA - Sales',
				// 'Chính sách Leader - Brandshop',
				'Chính Sách Store Leader - Brandshop',
				'Chính Sách Store CONSULTANT - Brandshop',
				'Total',
                
            );

            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet();
            $alpha = 'A';
            $index = 1;
            foreach ($heads as $key) {
                $sheet->setCellValue($alpha . $index, $key);
                $alpha++;
            }

			$sheet->getStyle('A1:K1')->applyFromArray(array('font' => array('bold' => true)));

			$sheet->getColumnDimension('A')->setWidth(25);
			$sheet->getColumnDimension('B')->setWidth(20);
			$sheet->getColumnDimension('C')->setWidth(20);
			$sheet->getColumnDimension('D')->setWidth(30);
			$sheet->getColumnDimension('E')->setWidth(35);
			$sheet->getColumnDimension('F')->setWidth(40);
			$sheet->getColumnDimension('G')->setWidth(50);
			// $sheet->getColumnDimension('H')->setWidth(30);
			// $sheet->getColumnDimension('I')->setWidth(30);
			// $sheet->getColumnDimension('J')->setWidth(30);
			// $sheet->getColumnDimension('K')->setWidth(20);
			// $sheet->getColumnDimension('L')->setWidth(20);
			// $sheet->getColumnDimension('M')->setWidth(30);
			$sheet->getColumnDimension('N')->setWidth(30);
			$sheet->getColumnDimension('O')->setWidth(20);

			
			$index = 2;
            $stt = 0;

			foreach($data_export['data'] as $key => $value){
				$alpha = 'A';
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['staff_name']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['code']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['area']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['title']), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_4'] ? $value['policy_4'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_5'] ? $value['policy_5'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_6'] ? $value['policy_6'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_7'] ? $value['policy_7'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_10'] ? $value['policy_10'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_11'] ? $value['policy_11'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_8'] ? $value['policy_8'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_9'] ? $value['policy_9'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            // $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_12'] ? $value['policy_12'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_13'] ? $value['policy_13'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_16'] ? $value['policy_16'] : ""), PHPExcel_Cell_DataType::TYPE_STRING);
	            
	            $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['policy_4'] + $value['policy_5'] + $value['policy_6'] + $value['policy_7'] + $value['policy_8'] + $value['policy_9'] + $value['policy_10'] + $value['policy_11'] + $value['policy_12'] + $value['policy_13'] + $value['policy_16']), PHPExcel_Cell_DataType::TYPE_STRING);
             	$index++;
	        } 
			

			$filename = 'KPI By Policy - ' . date('Y-m-d H-i-s');
			$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			$objWriter->save('php://output');
			exit;

			// $array_ez = range('E', 'Z');
			// $temp = 0;
			// foreach($all_policy as $key => $value)
			// {
			// 	$heads[$array_ez[$key]] = $value['title'];
			// 	$temp = $key+1;
			// }
			// $heads[$array_ez[$temp]] = 'Total';
			// $PHPExcel->setActiveSheetIndex(0);
			// $sheet = $PHPExcel->getActiveSheet();

			// foreach($heads as $key => $value)
			// 	$sheet->setCellValue($key.'1', $value);
			
			// $sheet->getStyle('A1:'.$array_ez[$temp].'1')->applyFromArray(array('font' => array('bold' => true)));

			// $sheet->getColumnDimension('A')->setWidth(30);
			// $sheet->getColumnDimension('B')->setWidth(15);
			// $sheet->getColumnDimension('C')->setWidth(20);
			// $sheet->getColumnDimension('D')->setWidth(10);
			// foreach($all_policy as $key => $value)
			// {
			// 	$heads[$array_ez[$key]] = $value['title'];
			// 	$sheet->getColumnDimension($array_ez[$key])->setWidth(30);
			// }
			// $sheet->getColumnDimension($array_ez[$temp])->setWidth(20);

			// $ik = new Application_Model_ImeiKpi2();
			// $data_export =  $ik->GetByTitle(0, 10, $pars);;

			// foreach($data_export['data'] as $key => $value)
			// {
			// 	$sheet->setCellValue('A' . ($key+2), $value['staff_name']);
			// 	$sheet->setCellValue('B' . ($key+2), $value['code']);
			// 	$sheet->setCellValue('C' . ($key+2), $value['area_name']);
			// 	$sheet->setCellValue('D' . ($key+2), $value['title']);
			// 	$total_kpi = 0;
			// 	foreach($all_policy as $key_policy => $value_policy)
			// 	{
			// 		if($value_policy['id'] == $value['policy_id'])
			// 		{
			// 			$data_kpi = $value['kpi'];
			// 		}
			// 		else
			// 		{
			// 			$data_kpi = null;
			// 		}
			// 		$total_kpi += $data_kpi;
			// 		$sheet->setCellValue($array_ez[$key_policy] . ($key+2), $data_kpi);
			// 	}
			// 	$sheet->setCellValue($array_ez[$temp] . ($key+2), $total_kpi);
			// }

			// $filename = 'KPI By Policy - ' . date('Y-m-d H-i-s');
			// $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
			// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			// header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			// $objWriter->save('php://output');
			// exit;
		}
		
		$this->view->pars = $pars;
		$total = 0;
		$limit = 50;
		$offset = ($pars['page']-1)*$limit;
		$ik = new Application_Model_ImeiKpi2();
		// $ik->ReportByPolicy(10, 0, $pars); die;
		$data = $ik->GetByTitle($limit, $offset, $pars);
               
		// print_r($data['data']); die;
		$this->view->data = $data['data'];

		$QPolicy = new Application_Model_Policy();
		$policy = $QPolicy->selectPolicy();
		$this->view->policy = $policy['data'];

		$this->view->limit = $limit;
		$this->view->offset = $pars['page'];
		$this->view->total = $data['total'];

		$pars['from_date'] = date('d/m/Y', strtotime($pars['from_date']));
		$pars['to_date'] = date('d/m/Y', strtotime($pars['to_date']));
		unset($pars['page']);
	
		$this->view->url = HOST . 'kpi-report/kpi-by-policy' . ($pars ? '?' . http_build_query($pars) . '&' : '?');

		$QArea = new Application_Model_Area();
		$all_area =  $QArea->fetchAll(null, 'name');
		$this->view->areas = $all_area;