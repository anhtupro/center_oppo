<?php 
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$page               = $this->getRequest()->getParam('page', 1);
$sort               = $this->getRequest()->getParam('sort', 'product_count');
$desc               = $this->getRequest()->getParam('desc', 1);
$from               = $this->getRequest()->getParam('from', date('01/m/Y'));
$to                 = $this->getRequest()->getParam('to', date('d/m/Y'));
$name               = $this->getRequest()->getParam('name');
$area               = $this->getRequest()->getParam('area');
$province           = $this->getRequest()->getParam('province');
$district           = $this->getRequest()->getParam('district');
$store              = $this->getRequest()->getParam('store');
$store_level        = $this->getRequest()->getParam('store_level');
$channel            = $this->getRequest()->getParam('channel');
$has_pg             = $this->getRequest()->getParam('has_pg');
$sales_from         = $this->getRequest()->getParam('sales_from');
$sales_to           = $this->getRequest()->getParam('sales_to');
$distributor_id     = $this->getRequest()->getParam('distributor_id');
$include_0_sell_out = $this->getRequest()->getParam('include_0_sell_out');

    $params = array(
        'page'               => $page,
        'sort'               => $sort,
        'desc'               => $desc,
        'from'               => $from,
        'to'                 => $to,
        'name'               => $name,
        'area'               => $area,
        'province'           => $province,
        'district'           => $district,
        'store'              => $store,
        'has_pg'             => $has_pg,
        'sales_from'         => $sales_from,
        'sales_to'           => $sales_to,
        'distributor_id'     => $distributor_id,
        'export'             => 1,
        'store_level'        => $store_level,
        'channel'            => $channel,
        'include_0_sell_out' => $include_0_sell_out
    );

         $dateFrom =  date_create_from_format("d/m/Y", $from)->format("Y-m-d");
         $dateTo   =  date_create_from_format("d/m/Y", $to)->format("Y-m-d");
        $from_f     = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d 00:00:00");
        $to_f       = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d 23:59:59");
        $dateDiff = My_Date::date_diff($dateFrom,$dateTo);
         if($dateDiff > 92 ){
             echo '<script>
                         alert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 90 ngày.");
                         palert("Hệ thống chỉ hỗ trợ xuất báo cáo tối đa 90 ngày.");
                     </script>';
             exit;
         }
      
        
        
        $limit       = LIMITATION;
        $total       = 0;
        $show_value  = 1;
        $QImeiKpi    = new Application_Model_ImeiKpi();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userStorage || !isset($userStorage->id))
            $this->_redirect(HOST);
        $group_id    = $userStorage->group_id;
        $area_list = '';

        if (in_array($group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
            $QAsm                = new Application_Model_Asm();
            $asm_cache           = $QAsm->get_cache();
            $params['area_list'] = isset($asm_cache[$userStorage->id]['area']) ? $asm_cache[$userStorage->id]['area'] : array();
            if(!empty($area) && !in_array($area, $params['area_list'])){
                print_r($params['area_list']);
                echo 'Không thuộc khu vực quản lý';
                exit();
            }
            if(!empty($params['area_list'])){
                $area_list = implode(",",$params['area_list']);
            }
                
            $show_value          = 1;
        } elseif ($group_id == My_Staff_Group::SALES) {
            // lấy cửa hàng của sale
            $QStoreStaffLog       = new Application_Model_StoreStaffLog();
            $store_cache          = $QStoreStaffLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
            $params['store_list'] = $store_cache;
            $params['sale_id']    = $userStorage->id;
            $show_value           = 0;
        } elseif ($group_id == My_Staff_Group::LEADER) {
            // lấy cửa hàng của sale
            $QStoreLeaderLog      = new Application_Model_StoreLeaderLog();
            $store_cache          = $QStoreLeaderLog->get_stores_cache($userStorage->id, date_create_from_format("d/m/Y", $from)->format("Y-m-d"), date_create_from_format("d/m/Y", $to)->format("Y-m-d"));
            $params['store_list'] = $store_cache;
            $params['leader_id']  = $userStorage->id;
            $show_value           = 0;
        }
        if (in_array($userStorage->title, array(418, 412))) {
            // BRAND SHOP MANAGER
            $params['is_brand_shop'] = 1;
        }
        // echo $show_value; die;
        $params['kpi'] = 1;
//        $result        = $QImeiKpi->fetchStore(null, null, $total, $params);
// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($result);die;
// }
//        PC::debug($result);

//        My_Report_Sales::store($params['from'], $params['to'], $result, $show_value);
      
        $db         = Zend_Registry::get('db');
        $stmt_store = $db->prepare("CALL `PR_fetch_Store`(:p_from_date, :p_to_date, :p_area_id, :p_area_list)");
        $stmt_store->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
        $stmt_store->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
        $stmt_store->bindParam('p_area_id', $area, PDO::PARAM_INT);
        $stmt_store->bindParam('p_area_list', $area_list, PDO::PARAM_STR);
        $stmt_store->execute();
        $result = $stmt_store->fetchAll();
        $stmt_store->closeCursor();
        $stmt_store = null;
        
        set_time_limit(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads    = array(
            'Store ID',
            'Store Name',
            'Store Address',
            'Area',
            'Province',
            'District',
            'Sales Man',
            'Dealer ID',
            'Parent',
            'Dealer Name',
            'Partner ID',
            'KA/IND',
            'Channel',
            'Type',
            'Share',
        );

        $from = date_create_from_format("d/m/Y", $from)->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $to)->format("Y-m-d");

        // các model có đổi giá
        $QKpi = new Application_Model_GoodPriceLog();
        $list = $QKpi->get_list($from, $to);

        $QStoreStaffLog = new Application_Model_StoreStaffLog();
        $sale_list      = $QStoreStaffLog->getSaleManList($from, $to);

        $QGood    = new Application_Model_Good();
        $products = $QGood->get_cache();

        $QGoodColor     = new Application_Model_GoodColor();
        $product_colors = $QGoodColor->get_cache();

        $product_col        = chr(ord('A') + count($heads));
        $product_color_list = array();

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d');
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d');
                    }
                }
            }
        }

        $heads[] = 'Total';

        foreach ($products as $key => $value) {
            // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key])) {
                foreach ($list[$key] as $_color => $ranges) {
                    if ($_color) {
                        $product_color_list[$key][] = $_color;

                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                                            format('d') . '->' . (new DateTime($range['to']))->format('d') . ' Activated';
                    } else {
                        foreach ($ranges as $range)
                            $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                                    DateTime($range['to']))->format('d') . ' Activated';
                    }
                }
            }
        }

        $heads = array_merge($heads, array(
            'Total Activated',
            'Value',
            'Value Activated',
        ));

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index++;

        $alpha = $product_col;

        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $alpha++;
        foreach ($products as $key => $value)
        // các model có đổi giá, tách thành nhiều cột
            if (isset($list[$key]))
                foreach ($list[$key] as $_color => $ranges)
                    foreach ($ranges as $range)
                        $sheet->setCellValue($alpha++ . $index, $range['price']);

        $index++;

        $all_store = array();

        $QDistributor      = new Application_Model_Distributor();
        $distributor_cache = $QDistributor->get_cache();

//        $db     = Zend_Registry::get('db');
//        $result = $db->query($result);
        // $stmt = $db->query($result);
        // $result = $stmt->fetchAll();

        foreach ($result as $_key => $_so) {
           
            $sell_out_list[$_so['store_id']][$_so['good_id']][$_so['color_id']][$_so['from_date'] . '_' . $_so['to_date']] = array(
                'total_quantity'        => $_so['total_quantity'],
                'total_activated'       => $_so['total_activated'],
                'total_value'           => $_so['total_value'],
                'total_value_activated' => $_so['total_value_activated'],
            );
           
            if (!isset($all_store[$_so['store_id']]))
                $all_store[$_so['store_id']] = array(
                    'store_id'        => $_so['store_id'],
                    'store_name'      => $_so['store_name'],
                    'company_address' => $_so['company_address'],
                    'store_district'  => $_so['store_district'],
                    'distributor_id'  => $_so['d_id'],
                    'parent_id'       => $_so['parent_id'],
                    'store_chanel'    => $_so['store_chanel'],
                    'store_level'     => $_so['store_level'],
                    'partner_id'      => $_so['partner_id'],
                    'share'           => $_so['share'],
                    'type'            => $_so['type'],
                );
        }

        // dùng mảng này để lưu thứ tự các staff trong result set trên
        // vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
        $store_array = array();

        // điền danh sách staff ra file trước
        foreach ($all_store as $key => $store) {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $store['store_id']);
            $sheet->setCellValue($alpha++ . $index, $store['store_name']);
            $sheet->setCellValue($alpha++ . $index, $store['company_address']);
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Area) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::Province) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_district']) ? My_Region::getValue($store['store_district'], My_Region::District) : '#');
            $sheet->setCellValue($alpha++ . $index, isset($sale_list[$store['store_id']]) ? rtrim($sale_list[$store['store_id']], ", ") : '');
            $sheet->setCellValue($alpha++ . $index, isset($store['distributor_id']) ? $store['distributor_id'] : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['parent_id']) ? $store['parent_id'] : '#');
            $sheet->setCellValue($alpha++ . $index, isset($distributor_cache[$store['distributor_id']]['title']) ? $distributor_cache[$store['distributor_id']]['title'] : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['partner_id']) ? $store['partner_id'] : '#');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_chanel']) ? $store['store_chanel'] : ' ');
            $sheet->setCellValue($alpha++ . $index, isset($store['store_level']) ? $store['store_level'] : ' ');
            $sheet->setCellValue($alpha++ . $index, isset($store['type']) ? $store['type'] : ' ');
            $sheet->setCellValue($alpha++ . $index, isset($store['share']) ? $store['share'] : ' ');
            
            $alpha++;

            $store_array[$store['store_id']] = $index; // lưu dòng ứng với store id
            $index++;

            unset($store);
        }

        unset($sale_list);

        // duyệt qua dãy model để tính KPI,
        // mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
        $alpha = $product_col;

        $total_by_store           = array();
        $total_activated_by_store = array();
        $value_by_store           = array();
        $value_activated_by_store = array();

        unset($index);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {

                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {
                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id])) { // đưa vào đúng dòng luôn
                                if (isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) {

                                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                                    $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0
                                    );

                                    if (!isset($total_by_store[$_store_id]))
                                        $total_by_store[$_store_id] = 0;
                                    $total_by_store[$_store_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;

                                    if (!isset($total_activated_by_store[$_store_id]))
                                        $total_activated_by_store[$_store_id] = 0;
                                    $total_activated_by_store[$_store_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                                    if (!isset($value_by_store[$_store_id]))
                                        $value_by_store[$_store_id] = 0;
                                    $value_by_store[$_store_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;

                                    if (!isset($value_activated_by_store[$_store_id]))
                                        $value_activated_by_store[$_store_id] = 0;
                                    $value_activated_by_store[$_store_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;

                                    unset($tmp);
                                } // END inner IF
                            } // END outer IF

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach

                    unset($ranges);
                } // END outer foreach
            } // END big IF

            unset($value);
        } // END big foreach

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_by_store[$_store_id]);

        $alpha++;
        unset($total_by_store);

        foreach ($products as $_product_id => $value) {
            // các model có đổi giá, tách thành 2 cột
            if (isset($list[$_product_id])) {
                foreach ($list[$_product_id] as $_color => $ranges) {
                    foreach ($ranges as $range) {
                        foreach ($sell_out_list as $_store_id => $_data) {

                            // nếu có trong danh sách NV ở trên
                            if (isset($store_array[$_store_id]) && isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']])) { // đưa vào đúng dòng luôn
                                $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];
                                $sheet->setCellValue($alpha . $store_array[$_store_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0
                                );

                                unset($tmp);
                            } // END if

                            unset($_data);
                        } // END inner foreach

                        unset($range);
                        $alpha++;
                    } // END middle foreach
                    unset($ranges);
                } // END outer foreach
            } // end big id
        } // end big foreach
        unset($sell_out_list);
        unset($products);
        unset($list);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($total_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], $total_activated_by_store[$_store_id]);

        $alpha++;
        unset($total_activated_by_store);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($value_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], ($show_value) ? $value_by_store[$_store_id] : "-");

        $alpha++;
        unset($value_by_store);

        foreach ($all_store as $_store_id => $_data)
        // nếu có trong danh sách NV ở trên
            if (isset($value_activated_by_store[$_store_id])) // đưa vào đúng dòng luôn
                $sheet->setCellValue($alpha . $store_array[$_store_id], ($show_value) ? $value_activated_by_store[$_store_id] : "-");

        unset($all_store);
        unset($alpha);
        unset($value_activated_by_store);
        unset($store_array);

        $filename  = 'Sell Out - Store - New -' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;