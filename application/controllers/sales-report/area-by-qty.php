<?php
//        $from = $this->getRequest()->getParam('from', date('01/m/Y'));
//        $to   = $this->getRequest()->getParam('to', date('d/m/Y'));
//        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

$area = $this->getRequest()->getParam('area', '');

$time     = $this->getRequest()->getParam('time', date('m/Y'));

$time_seg = explode('/', $time);
$month    = $time_seg[0];
$year     = $time_seg[1];
$from = '01/' . $month . '/' . $year;
$dateFormat = $year . '-' . $month . '-01';
$to = date('t/m/Y', strtotime($dateFormat));

$params = array(
    'from' => $from,
    'to'   => $to,
    'area' => $area,
);


$QImeiKpi = new Application_Model_ImeiKpi();
$QArea    = new Application_Model_Area();
$QAreaRankByMonth    = new Application_Model_AreaRankByMonth();


$params['kpi'] = true;
//         $params['dev'] = 1;
$db         = Zend_Registry::get('db');

//        $sell_out = $QImeiKpi->fetchArea($params);
unset($params['kpi']);
$from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
$to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

$db         = Zend_Registry::get('db');
$from_f     = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d 00:00:00");
$to_f       = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d 23:59:59");

$stmt_out = $db->prepare("CALL `PR_fetch_Area`(:p_from_date, :p_to_date)");
$stmt_out->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
$stmt_out->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
//        $stmt_out->bindParam('p_area_id', $area, PDO::PARAM_STR);
$stmt_out->execute();
//        $sell_out = $stmt_out->fetchAll();
$data = $stmt_out->fetchAll();
$stmt_out->closeCursor();
$stmt_out = null;


//        $data_for_point            = $QImeiKpi->fetchArea($params);
$area_list = 0;
$stmt_point = $db->prepare("CALL `PR_fetch_Area_Point`(:p_from_date, :p_to_date, :p_area_list)");
$stmt_point->bindParam('p_from_date', $from_f, PDO::PARAM_STR);
$stmt_point->bindParam('p_to_date', $to_f, PDO::PARAM_STR);
$stmt_point->bindParam('p_area_list', $area_list, PDO::PARAM_STR);

$stmt_point->execute();
$data_for_point = $stmt_point->fetchAll();
$stmt_point->closeCursor();
$stmt_point = null;

$params['get_total_sales'] = true;

//        $total_sales = $QImeiKpi->fetchArea($params);
$total_money = 0;
foreach ($data_for_point as $item){
    $total_money += $item['total_value'];
}

//        $total_money = $total_sales['total_value'];
$point_list  = array();

$all_area     = $QArea->fetchAll();
$region_share = array();

foreach ($all_area as $_key => $_value)
    $region_share[$_value['id']] = $_value['region_share'];



//tính point
foreach ($data_for_point as $item)
    $point_list[$item['area_id']] = ( $total_money > 0 and ( $region_share[$item['area_id']] / 100) > 0 ) ? round(($item ['total_value'] / $total_money) * 60 / ($region_share[$item['area_id']] / 100), 2) : 0;


//        My_Report_Sales::area($sell_out, $total_sales, $point_list, $params);

set_time_limit(0);
ini_set('memory_limit', -1);
ini_set('display_error', ~E_ALL);

require_once 'PHPExcel.php';
$PHPExcel = new PHPExcel();
$heads    = array('Area',);

// echo "<pre>";print_r($data);die;

$from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
$to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

// các model có đổi giá
$QKpi = new Application_Model_GoodPriceLog();
$list = $QKpi->get_list($from, $to);

// echo "<pre>";print_r($list);die;
$QGood    = new Application_Model_Good();
$products = $QGood->get_cache();

$QGoodColor     = new Application_Model_GoodColor();
$product_colors = $QGoodColor->get_cache();

$product_col        = chr(ord('A') + count($heads));
$product_color_list = array();

$kpi_list = array();

foreach ($data as $_key => $_value) {
    $kpi_list[$_value['area_id']][$_value['good_id']][$_value['color_id']][$_value['from_date'] . '_' . $_value['to_date']] = array(
        'total_quantity'            => $_value['total_quantity'],
        'total_activated'           => $_value['total_activated'],
        'total_value'               => $_value['total_value'],
        'total_value_activated'     => $_value['total_value_activated'],
        'total_value_IND'           => $_value['total_value_IND'],
        'total_value_activated_IND' => $_value['total_value_activated_IND'],
    );
}

foreach ($products as $key => $value) {
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key])) {
        foreach ($list[$key] as $_color => $ranges) {
            if ($_color) {
                $product_color_list[$key][] = $_color;

                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                        format('d') . '->' . (new DateTime($range['to']))->format('d');
                }
            } else {
                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                        DateTime($range['to']))->format('d');
                }
            }
        }
    }
}

$heads[] = 'Unit';

foreach ($products as $key => $value) {
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key])) {
        foreach ($list[$key] as $_color => $ranges) {
            if ($_color) {
                $product_color_list[$key][] = $_color;

                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . $product_colors[$_color] . ' / ' . (new DateTime($range['from']))->
                        format('d') . '->' . (new DateTime($range['to']))->format('d') . ' activated';
                }
            } else {
                foreach ($ranges as $range) {
                    $heads[] = $value . ' / ' . (new DateTime($range['from']))->format('d') . '->' . (new
                        DateTime($range['to']))->format('d') . ' activated';
                }
            }
        }
    }
}

$heads[] = 'Unit activated';
$heads[] = 'Value (80%)';
$heads[] = 'Value IND (80%)';
$heads[] = 'Value KA (80%)';
//        $heads[] = 'Value A71k (80%)';
//        $heads[] = 'Value IND A71k (80%)';
//        $heads[] = 'Value KA A71k (80%)';
//        $heads[] = 'Value A3s16g (80%)';
//        $heads[] = 'Value IND A3s16g (80%)';
//        $heads[] = 'Value KA A3s16g (80%)';

$heads[] = 'Value K3 (80%)';
$heads[] = 'Value IND K3 (80%)';
$heads[] = 'Value KA K3 (80%)';

$heads[] = 'Value A1k SUBSIDI (80%)';
$heads[] = 'Value IND A1k SUBSIDI (80%)';
$heads[] = 'Value KA A1k SUBSIDI (80%)';

$heads[] = 'Value A1k (80%)';
$heads[] = 'Value IND A1k (80%)';
$heads[] = 'Value KA A1k (80%)';

//        $heads[] = 'Value Reno2f (80%)';
//        $heads[] = 'Value IND Reno2f (80%)';
//        $heads[] = 'Value KA Reno2f (80%)';
//
//        $heads[] = 'Value A5s (80%)';
//        $heads[] = 'Value IND A5s (80%)';
//        $heads[] = 'Value KA A5s (80%)';


$heads[] = 'Value excluded special models (80%)';
$heads[] = 'Value excluded special models IND (80%)';
$heads[] = 'Value excluded special models KA (80%)';
$heads[] = 'Value activated (80%)';
$heads[] = 'Value activated IND (80%)';
$heads[] = 'Value activated KA (80%)';
$heads[] = 'Tỉ lệ active cả nước (%)';
$heads[] = 'Tỉ lệ chưa active (%)';
$heads[] = 'Tỉ lệ chưa active vượt (%)';
$heads[] = 'Value tính KPI';
$heads[] = 'Value tính KPI IND';
$heads[] = 'Value tính KPI KA';
//        $heads[] = 'Value tính KPI A71k';
//        $heads[] = 'Value tính KPI A71k IND';
//        $heads[] = 'Value tính KPI A71k KA';
//        $heads[] = 'Value tính KPI A3s16g';
//        $heads[] = 'Value tính KPI A3s16g IND';
//        $heads[] = 'Value tính KPI A3s16g KA';
$heads[] = 'Value tính KPI K3';
$heads[] = 'Value tính KPI K3 IND';
$heads[] = 'Value tính KPI K3 KA';
$heads[] = 'Value tính KPI A1k Subsidi';
$heads[] = 'Value tính KPI A1k Subsidi IND';
$heads[] = 'Value tính KPI A1k Subsidi KA';
$heads[] = 'Value tính KPI A1k';
$heads[] = 'Value tính KPI A1k IND';
$heads[] = 'Value tính KPI A1k KA';
//        $heads[] = 'Value tính KPI Reno2F';
//        $heads[] = 'Value tính KPI Reno2F IND';
//        $heads[] = 'Value tính KPI Reno2F KA';

//        $heads[] = 'Value tính KPI A5s';
//        $heads[] = 'Value tính KPI A5s IND';
//        $heads[] = 'Value tính KPI A5s KA';
$heads[] = 'Value tính KPI Models còn lại';
$heads[] = 'Value tính KPI Models còn lại IND';
$heads[] = 'Value tính KPI Models còn lại KA';
$heads[] = 'Region Share (%)';
$heads[] = 'Point';
// $heads[] = 'Point mới';

//        $heads[] = 'Chiết khấu A71k (%)';
//        $heads[] = 'Chiết khấu A3s16g (%)';
$heads[] = 'Chiết khấu K3 (%)';
$heads[] = 'Chiết khấu A1k Subsidy (%)';
$heads[] = 'Chiết khấu A1k (%)';
//        $heads[] = 'Chiết khấu Reno2F IND (%)';
//        $heads[] = 'Chiết khấu Reno2F KA (%)';
//        $heads[] = 'Chiết khấu A5s (%)';

//
//        $heads[] = 'Chiết khấu (%)';
$heads[] = 'Chiết khấu IND (%)';
$heads[] = 'Chiết khấu KA (%)';
//        $heads[] = 'Bonus';
$heads[] = 'Bonus Special model & model còn lại';

// thêm cột cho phần máy chạy bộ

$heads[] = 'Unit Realme';
$heads[] = 'Unit Activated Realme';
$heads[] = '% chưa active cả nước Realme';
$heads[] = '% chưa active khu vực Realme';
$heads[] = '% chưa active vượt Realme';
$heads[] = 'Value 80% Realme';
$heads[] = 'Value KPI Realme';

$heads[] = 'Unit All';
$heads[] = 'Unit Activated All';
$heads[] = 'Value 80% All';
$heads[] = 'Value KPI All';

$heads[] = 'Point Máy chạy bộ';
$heads[] = 'Rank';
$heads[] = 'High-end product (>= 8 Mil)';
$heads[] = 'Low-end product (3-5 Mil)';
$heads[] = 'Loại Thưởng/phạt';
$heads[] = 'Tiền Thưởng/phạt';




$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

$alpha = 'A';
$index = 1;

foreach ($heads as $key) {
    $sheet->setCellValue($alpha . $index, $key);
    $alpha++;
}

$index++;

$alpha = $product_col;
foreach ($products as $key => $value)
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key]))
        foreach ($list[$key] as $_color => $ranges)
            foreach ($ranges as $range)
                $sheet->setCellValue($alpha++ . $index, $range['price']);

$alpha++;
foreach ($products as $key => $value)
    // các model có đổi giá, tách thành nhiều cột
    if (isset($list[$key]))
        foreach ($list[$key] as $_color => $ranges)
            foreach ($ranges as $range)
                $sheet->setCellValue($alpha++ . $index, $range['price']);

$index++;

$QArea      = new Application_Model_Area();
//$area_cache = $QArea->get_cache_all_with_del();
$area_cache = $QArea->getAreaKpi();


// dùng mảng này để lưu thứ tự các area trong result set trên
// vì duyệt xong thằng kia thì hết set rồi, con trỏ mà
$area_array = array();

// điền danh sách area ra file trước
//Cot Area

$alpha = 'A';
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $index, $_area_name);

    $area_array[$_area_id] = $index++; // lưu dòng ứng với area id
}

// duyệt qua dãy model để tính KPI,
// mỗi lần duyệt tính KPI của nguyên list NV, theo 1 model của vòng lặp hiện tại
$alpha = $product_col;

$QAsm        = new Application_Model_Asm();
$asm_cache   = $QAsm->get_cache();
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$total_by_area                             = array();
$total_activated_by_area                   = array();
$total_value_activated_by_area             = array();
$total_value_activated_by_area_IND         = array();
$total_value_activated_by_area_KA          = array();
$total_value_by_area                       = array();
$total_value_by_area_IND                   = array();
$total_value_by_area_KA                    = array();
$total_value_exclude_special_model_by_area = array();
$total_value_a71k_model_by_area            = array();
$total_value_a71k_model_by_area_IND        = array();
$total_value_a71k_model_by_area_KA         = array();
$total_value_a3s16g_model_by_area          = array();
$total_value_a3s16g_model_by_area_IND      = array();
$total_value_a3s16g_model_by_area_KA       = array();
$total_value_k3_model_by_area              = array();
$total_value_k3_model_by_area_IND          = array();
$total_value_k3_model_by_area_KA           = array();
$total_value_a1k_subsidi_model_by_area          = array();
$total_value_a1k_subsidi_model_by_area_IND      = array();
$total_value_a1k_subsidi_model_by_area_KA       = array();
$total_value_a1k_model_by_area          = array();
$total_value_a1k_model_by_area_IND      = array();
$total_value_a1k_model_by_area_KA       = array();
$total_value_reno2f_model_by_area          = array();
$total_value_reno2f_model_by_area_IND      = array();
$total_value_reno2f_model_by_area_KA       = array();

$total_value_a5s_model_by_area          = array();
$total_value_a5s_model_by_area_IND      = array();
$total_value_a5s_model_by_area_KA       = array();

foreach ($products as $_product_id => $value) {
    // các model có đổi giá, tách thành 2 cột
    if (isset($list[$_product_id])) {
        foreach ($list[$_product_id] as $_color => $ranges) {
            foreach ($ranges as $range) {
                foreach ($kpi_list as $_area_id => $_data) {
                    // nếu có trong danh sách NV ở trên
                    if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                        continue;

                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                    if (isset($area_array[$_area_id]) && isset($asm_cache[$userStorage->id]['area']) && in_array($_area_id, $asm_cache[$userStorage->id]['area'])) {
                        // đưa vào đúng dòng luôn
                        if (!isset($total_by_area[$_area_id])) {
                            $total_by_area[$_area_id] = 0;
                        }
                        if (!isset($total_value_by_area[$_area_id])) {
                            $total_value_by_area[$_area_id]     = 0;
                            $total_value_by_area_IND[$_area_id] = 0;
                            $total_value_by_area_KA[$_area_id]  = 0;
                        }
                        if (!isset($total_value_activated_by_area[$_area_id])) {
                            $total_value_activated_by_area[$_area_id]     = 0;
                            $total_value_activated_by_area_IND[$_area_id] = 0;
                            $total_value_activated_by_area_KA[$_area_id]  = 0;
                        }
                        $total_by_area[$_area_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                        $total_value_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                        $total_value_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                        $total_value_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                if ($_product_id == 597) {
//                                    if (!isset($total_value_a71k_model_by_area[$_area_id])) {
//                                        $total_value_a71k_model_by_area[$_area_id]     = 0;
//                                        $total_value_a71k_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a71k_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a71k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a71k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a71k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(622, 661))) {
//                                    if (!isset($total_value_a3s16g_model_by_area[$_area_id])) {
//                                        $total_value_a3s16g_model_by_area[$_area_id]     = 0;
//                                        $total_value_a3s16g_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a3s16g_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a3s16g_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a3s16g_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a3s16g_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
                        if ($_product_id == 750) {
                            if (!isset($total_value_k3_model_by_area[$_area_id])) {
                                $total_value_k3_model_by_area[$_area_id]     = 0;
                                $total_value_k3_model_by_area_IND[$_area_id] = 0;
                                $total_value_k3_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_k3_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_k3_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_k3_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }

                        if (in_array($_product_id, array(808))) {
                            if (!isset($total_value_a1k_subsidi_model_by_area[$_area_id])) {
                                $total_value_a1k_subsidi_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_subsidi_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_subsidi_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_subsidi_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_subsidi_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_subsidi_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
                        if (in_array($_product_id, array(708))) {
                            if (!isset($total_value_a1k_model_by_area[$_area_id])) {
                                $total_value_a1k_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
//                                if (in_array($_product_id, array(747))) {
//                                    if (!isset($total_value_a1k_model_by_area[$_area_id])) {
//                                        $total_value_reno2f_model_by_area[$_area_id]     = 0;
//                                        $total_value_reno2f_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_reno2f_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_reno2f_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_reno2f_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_reno2f_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(703))) {
//                                    if (!isset($total_value_a5s_model_by_area[$_area_id])) {
//                                        $total_value_a5s_model_by_area[$_area_id]     = 0;
//                                        $total_value_a5s_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a5s_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a5s_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a5s_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a5s_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }

                        $total_value_activated_by_area[$_area_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;
                        $total_value_activated_by_area_IND[$_area_id] += isset($tmp['total_value_activated_IND']) ? $tmp['total_value_activated_IND'] : 0;
                        $total_value_activated_by_area_KA[$_area_id] += $tmp['total_value_activated'] - $tmp['total_value_activated_IND'];
                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0);
                    } elseif (!in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) ||
                        My_Staff_Permission_Area::view_all($userStorage->id)) {
                        if (!isset($total_by_area[$_area_id]))
                            $total_by_area[$_area_id]                 = 0;
                        if (!isset($total_value_by_area[$_area_id]))
                            $total_value_by_area[$_area_id]           = 0;
                        if (!isset($total_value_activated_by_area[$_area_id]))
                            $total_value_activated_by_area[$_area_id] = 0;

                        $total_by_area[$_area_id] += isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0;
                        $total_value_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                        $total_value_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                        $total_value_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];

                        $total_value_activated_by_area[$_area_id] += isset($tmp['total_value_activated']) ? $tmp['total_value_activated'] : 0;
                        $total_value_activated_by_area_IND[$_area_id] += isset($tmp['total_value_activated_IND']) ? $tmp['total_value_activated_IND'] : 0;
                        $total_value_activated_by_area_KA[$_area_id] += $tmp['total_value_activated'] - $tmp['total_value_activated_IND'];

//                                if ($_product_id == 597) {
//                                    if (!isset($total_value_a71k_model_by_area[$_area_id])) {
//                                        $total_value_a71k_model_by_area[$_area_id]     = 0;
//                                        $total_value_a71k_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a71k_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a71k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a71k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a71k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(622, 661))) {
//                                    if (!isset($total_value_a3s16g_model_by_area[$_area_id])) {
//                                        $total_value_a3s16g_model_by_area[$_area_id]     = 0;
//                                        $total_value_a3s16g_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a3s16g_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a3s16g_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a3s16g_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a3s16g_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
                        if ($_product_id == 750) {
                            if (!isset($total_value_k3_model_by_area[$_area_id])) {
                                $total_value_k3_model_by_area[$_area_id]     = 0;
                                $total_value_k3_model_by_area_IND[$_area_id] = 0;
                                $total_value_k3_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_k3_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_k3_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_k3_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }

                        if (in_array($_product_id, array(808))) {
                            if (!isset($total_value_a1k_subsidi_model_by_area[$_area_id])) {
                                $total_value_a1k_subsidi_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_subsidi_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_subsidi_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_subsidi_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_subsidi_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_subsidi_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
                        if (in_array($_product_id, array(708))) {
                            if (!isset($total_value_a1k_model_by_area[$_area_id])) {
                                $total_value_a1k_model_by_area[$_area_id]     = 0;
                                $total_value_a1k_model_by_area_IND[$_area_id] = 0;
                                $total_value_a1k_model_by_area_KA[$_area_id]  = 0;
                            }
                            $total_value_a1k_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
                            $total_value_a1k_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
                            $total_value_a1k_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
                        }
//                                if (in_array($_product_id, array(747))) {
//                                    if (!isset($total_value_reno2f_model_by_area[$_area_id])) {
//                                        $total_value_reno2f_model_by_area[$_area_id]     = 0;
//                                        $total_value_reno2f_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_reno2f_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_reno2f_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_reno2f_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_reno2f_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }
//                                if (in_array($_product_id, array(703))) {
//                                    if (!isset($total_value_a5s_model_by_area[$_area_id])) {
//                                        $total_value_a5s_model_by_area[$_area_id]     = 0;
//                                        $total_value_a5s_model_by_area_IND[$_area_id] = 0;
//                                        $total_value_a5s_model_by_area_KA[$_area_id]  = 0;
//                                    }
//                                    $total_value_a5s_model_by_area[$_area_id] += isset($tmp['total_value']) ? $tmp['total_value'] : 0;
//                                    $total_value_a5s_model_by_area_IND[$_area_id] += isset($tmp['total_value_IND']) ? $tmp['total_value_IND'] : 0;
//                                    $total_value_a5s_model_by_area_KA[$_area_id] += $tmp['total_value'] - $tmp['total_value_IND'];
//                                }

                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_quantity']) ? $tmp['total_quantity'] : 0);
                    } else
                        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
                }

                $alpha++;
            }
        }
    }
}

foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_by_area[$_area_id])) {
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_by_area[$_area_id]);
        $total_all_area += $total_by_area[$_area_id];
    } else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Unit activated
foreach ($products as $_product_id => $value) {
    // các model có đổi giá, tách thành 2 cột
    if (isset($list[$_product_id])) {
        foreach ($list[$_product_id] as $_color => $ranges) {
            foreach ($ranges as $range) {
                foreach ($kpi_list as $_area_id => $_data) {
                    if (!isset($_data[$_product_id][$_color][$range['from'] . '_' . $range['to']]))
                        continue;

                    $tmp = $_data[$_product_id][$_color][$range['from'] . '_' . $range['to']];

                    // nếu có trong danh sách NV ở trên
                    if (isset($area_array[$_area_id]) && isset($asm_cache[$userStorage->id]['area']) &&
                        in_array($_area_id, $asm_cache[$userStorage->id]['area'])) {
                        // đưa vào đúng dòng luôn
                        if (!isset($total_activated_by_area[$_area_id]))
                            $total_activated_by_area[$_area_id] = 0;
                        $total_activated_by_area[$_area_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0);
                    }
                    elseif (!in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) ||
                        My_Staff_Permission_Area::view_all($userStorage->id)) {
                        if (!isset($total_activated_by_area[$_area_id]))
                            $total_activated_by_area[$_area_id] = 0;
                        $total_activated_by_area[$_area_id] += isset($tmp['total_activated']) ? $tmp['total_activated'] : 0;

                        $sheet->setCellValue($alpha . $area_array[$_area_id], isset($tmp['total_activated']) ? $tmp['total_activated'] : 0);
                    } else
                        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
                }

                $alpha++;
            }
        }
    }
}



foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_activated_by_area[$_area_id])) {
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_activated_by_area[$_area_id]);
        $total_activated_all_area += $total_activated_by_area[$_area_id];
    } else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}


$alpha++;

foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area[$_area_id]); //$heads[] = 'Value tính KPI' 'Value (80%)';
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_IND[$_area_id]); // $heads[] = 'Value tính KPI IND' 'Value IND (80%)';
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_KA[$_area_id]); // $heads[] = 'Value tính KPI KA' 'Value KA (80%)';
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
/**
 * @author: hero
 * mode c3
 * description
 * date
 */
$alpha++;
// Cot Value K3 (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_k3_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_model_by_area[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Value IND K3 (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_k3_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_model_by_area_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Value KA K3 (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_k3_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_model_by_area_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;

//a1k sub
// Cot Value A1k SUBSIDI (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_subsidi_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_model_by_area[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Value IND A1k SUBSIDI (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_subsidi_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_model_by_area_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Value KA A1k SUBSIDI (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_subsidi_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_model_by_area_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
//End a1k sub

//a1k
// Cot Value A1k (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_model_by_area[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Value IND A1k (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_model_by_area_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Value KA A1k (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_model_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_model_by_area_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
//End a1k

//Reno2f
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//End Reno2f

//a5s
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_model_by_area[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_model_by_area_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s_model_by_area[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_model_by_area_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//End a5s
// Cot Value excluded special models (80%) Cac sp khac model dac biet
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area[$_area_id]
            // - $total_value_a71k_model_by_area[$_area_id]
//                        - $total_value_a3s16g_model_by_area[$_area_id]
            - $total_value_k3_model_by_area[$_area_id]
            - $total_value_a1k_subsidi_model_by_area[$_area_id]
            - $total_value_a1k_model_by_area[$_area_id]
//                        - $total_value_reno2f_model_by_area[$_area_id]
//                        - $total_value_a5s_model_by_area[$_area_id]
        )
        ;
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value excluded special models IND (80%) Cac sp khac model dac biet IND
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_IND[$_area_id]
            // - $total_value_a71k_model_by_area_IND[$_area_id]
//                        - $total_value_a3s16g_model_by_area_IND[$_area_id]
            - $total_value_k3_model_by_area_IND[$_area_id]
            - $total_value_a1k_subsidi_model_by_area_IND[$_area_id]
            - $total_value_a1k_model_by_area_IND[$_area_id]
//                        - $total_value_reno2f_model_by_area_IND[$_area_id]
//                        - $total_value_a5s_model_by_area_IND[$_area_id]
        );
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value excluded special models KA (80%) Cac sp khac model dac biet KA
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_by_area_KA[$_area_id]
            // - $total_value_a71k_model_by_area_KA[$_area_id]
//                        - $total_value_a3s16g_model_by_area_KA[$_area_id]
            - $total_value_k3_model_by_area_KA[$_area_id]
            - $total_value_a1k_subsidi_model_by_area_KA[$_area_id]
            - $total_value_a1k_model_by_area_KA[$_area_id]
//                        - $total_value_reno2f_model_by_area_KA[$_area_id]
//                        - $total_value_a5s_model_by_area_KA[$_area_id]
        );
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value activated (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_activated_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_activated_by_area[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value activated IND (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_activated_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_activated_by_area_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value activated KA (80%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_activated_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_activated_by_area_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// echo 111;
// exit();
// Cot Tỉ lệ active cả nước (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_activated_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1) . '%') ;
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Tỉ lệ chưa active (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_activated_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) . '%');
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
// Cot Tỉ lệ chưa active vượt (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_activated_by_area[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) - round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1) . '%');
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;
$total_value_tinh_kpi                   = array();
$total_value_tinh_kpi_IND               = array();
$total_value_tinh_kpi_KA                = array();
//        $total_value_a71k                       = array();
//        $total_value_a71k_IND                   = array();
//        $total_value_a71k_KA                    = array();
$total_value_a3s16g                     = array();
$total_value_a3s16g_IND                 = array();
$total_value_a3s16g_KA                  = array();
$total_value_k3                         = array();
$total_value_k3_IND                     = array();
$total_value_k3_KA                      = array();


$total_value_a1k_subsidi                     = array();
$total_value_a1k_subsidi_IND                 = array();
$total_value_a1k_subsidi_KA                  = array();
$total_value_a1k                     = array();
$total_value_a1k_IND                 = array();
$total_value_a1k_KA                  = array();

$total_value_reno2f                     = array();
$total_value_reno2f_IND                 = array();
$total_value_reno2f_KA                  = array();

$total_value_a5s                     = array();
$total_value_a5s_IND                 = array();
$total_value_a5s_KA                  = array();
$total_value_excluded_special_modes     = array();
$total_value_excluded_special_modes_IND = array();
$total_value_excluded_special_modes_KA  = array();
foreach ($area_cache as $_area_id => $_area_name) {

    $active_vuot = round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) - round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1);
    if (isset($total_activated_by_area[$_area_id]) && $active_vuot <= 0) {
        $value_tinh_kpi = $total_value_by_area[$_area_id];

        $total_value_tinh_kpi[$_area_id]     = $value_tinh_kpi;
        $sheet->setCellValue($alpha . $area_array[$_area_id], $value_tinh_kpi);
        $total_value_tinh_kpi_IND[$_area_id] = $total_value_by_area_IND[$_area_id];
        $total_value_tinh_kpi_KA[$_area_id]  = $total_value_by_area_KA[$_area_id];
//                $total_value_a71k[$_area_id]         = $total_value_a71k_model_by_area[$_area_id];
//                $total_value_a71k_IND[$_area_id]     = $total_value_a71k_model_by_area_IND[$_area_id];
//                $total_value_a71k_KA[$_area_id]      = $total_value_a71k_model_by_area_KA[$_area_id];

//                $total_value_a3s16g[$_area_id]     = $total_value_a3s16g_model_by_area[$_area_id];
//                $total_value_a3s16g_IND[$_area_id] = $total_value_a3s16g_model_by_area_IND[$_area_id];
//                $total_value_a3s16g_KA[$_area_id]  = $total_value_a3s16g_model_by_area_KA[$_area_id];

        $total_value_k3[$_area_id]     = $total_value_k3_model_by_area[$_area_id];
        $total_value_k3_IND[$_area_id] = $total_value_k3_model_by_area_IND[$_area_id];
        $total_value_k3_KA[$_area_id]  = $total_value_k3_model_by_area_KA[$_area_id];

        $total_value_a1k_subsidi[$_area_id]     = $total_value_a1k_subsidi_model_by_area[$_area_id];
        $total_value_a1k_subsidi_IND[$_area_id] = $total_value_a1k_subsidi_model_by_area_IND[$_area_id];
        $total_value_a1k_subsidi_KA[$_area_id]  = $total_value_a1k_subsidi_model_by_area_KA[$_area_id];

        $total_value_a1k[$_area_id]     = $total_value_a1k_model_by_area[$_area_id];
        $total_value_a1k_IND[$_area_id] = $total_value_a1k_model_by_area_IND[$_area_id];
        $total_value_a1k_KA[$_area_id]  = $total_value_a1k_model_by_area_KA[$_area_id];

//                $total_value_reno2f[$_area_id]     = $total_value_reno2f_model_by_area[$_area_id];
//                $total_value_reno2f_IND[$_area_id] = $total_value_reno2f_model_by_area_IND[$_area_id];
//                $total_value_reno2f_KA[$_area_id]  = $total_value_reno2f_model_by_area_KA[$_area_id];

//
//                $total_value_a5s[$_area_id]     = $total_value_a5s_model_by_area[$_area_id];
//                $total_value_a5s_IND[$_area_id] = $total_value_a5s_model_by_area_IND[$_area_id];
//                $total_value_a5s_KA[$_area_id]  = $total_value_a5s_model_by_area_KA[$_area_id];

        $total_value_excluded_special_modes[$_area_id]     = $total_value_by_area[$_area_id]
            // - $total_value_a71k_model_by_area[$_area_id]
//                        - $total_value_a3s16g_model_by_area[$_area_id]
            - $total_value_k3_model_by_area[$_area_id]
            - $total_value_a1k_subsidi_model_by_area[$_area_id]
            - $total_value_a1k_model_by_area[$_area_id]
//                        - $total_value_reno2f_model_by_area[$_area_id]
//                        - $total_value_a5s_model_by_area[$_area_id]
        ;
        $total_value_excluded_special_modes_IND[$_area_id] = $total_value_by_area_IND[$_area_id]
            // - $total_value_a71k_model_by_area_IND[$_area_id]
//                        - $total_value_a3s16g_model_by_area_IND[$_area_id]
            - $total_value_k3_model_by_area_IND[$_area_id]
            - $total_value_a1k_subsidi_model_by_area_IND[$_area_id]
            - $total_value_a1k_model_by_area_IND[$_area_id]
//                        - $total_value_reno2f_model_by_area_IND[$_area_id]
//                        - $total_value_a5s_model_by_area_IND[$_area_id]
        ;
        $total_value_excluded_special_modes_KA[$_area_id]  = $total_value_by_area_KA[$_area_id]
            // - $total_value_a71k_model_by_area_KA[$_area_id]
//                        - $total_value_a3s16g_model_by_area_KA[$_area_id]
            - $total_value_k3_model_by_area_KA[$_area_id]
            - $total_value_a1k_subsidi_model_by_area_KA[$_area_id]
            - $total_value_a1k_model_by_area_KA[$_area_id]
//                        - $total_value_reno2f_model_by_area_KA[$_area_id]
//                        - $total_value_a5s_model_by_area_KA[$_area_id]
        ;
    } else if (isset($total_activated_by_area[$_area_id]) && $active_vuot > 0) {
        $value_tinh_kpi = $total_value_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );

        $total_value_tinh_kpi[$_area_id]     = $value_tinh_kpi;
        $total_value_tinh_kpi_IND[$_area_id] = $total_value_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
        $total_value_tinh_kpi_KA[$_area_id]  = $value_tinh_kpi - $total_value_tinh_kpi_IND[$_area_id];

        $sheet->setCellValue($alpha . $area_array[$_area_id], $value_tinh_kpi);

//                $total_value_a71k[$_area_id]     = $total_value_a71k_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a71k_IND[$_area_id] = $total_value_a71k_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a71k_KA[$_area_id]  = $total_value_a71k_model_by_area_KA[$_area_id] * ( (100 - $active_vuot) / 100 );

//                $total_value_a3s16g[$_area_id]     = $total_value_a3s16g_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a3s16g_IND[$_area_id] = $total_value_a3s16g_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a3s16g_KA[$_area_id]  = $total_value_a3s16g[$_area_id] - $total_value_a3s16g_IND[$_area_id];

        $total_value_k3[$_area_id]     = $total_value_k3_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
        $total_value_k3_IND[$_area_id] = $total_value_k3_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
        $total_value_k3_KA[$_area_id]  = $total_value_k3_model_by_area_KA[$_area_id] * ( (100 - $active_vuot) / 100 );

        $total_value_a1k_subsidi[$_area_id]     = $total_value_a1k_subsidi_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
        $total_value_a1k_subsidi_IND[$_area_id] = $total_value_a1k_subsidi_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
        $total_value_a1k_subsidi_KA[$_area_id]  = $total_value_a1k_subsidi[$_area_id] - $total_value_a1k_subsidi_IND[$_area_id];

        $total_value_a1k[$_area_id]     = $total_value_a1k_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
        $total_value_a1k_IND[$_area_id] = $total_value_a1k_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
        $total_value_a1k_KA[$_area_id]  = $total_value_a1k[$_area_id] - $total_value_a1k_IND[$_area_id];

//                $total_value_reno2f[$_area_id]     = $total_value_reno2f_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_reno2f_IND[$_area_id] = $total_value_reno2f_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_reno2f_KA[$_area_id]  = $total_value_reno2f[$_area_id] - $total_value_reno2f_IND[$_area_id];
//
//                $total_value_a5s[$_area_id]     = $total_value_a5s_model_by_area[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a5s_IND[$_area_id] = $total_value_a5s_model_by_area_IND[$_area_id] * ( (100 - $active_vuot) / 100 );
//                $total_value_a5s_KA[$_area_id]  = $total_value_a5s[$_area_id] - $total_value_a5s_IND[$_area_id];

        $total_value_excluded_special_modes[$_area_id]     = ($total_value_by_area[$_area_id]
//                        - $total_value_a71k_model_by_area[$_area_id]
//                        - $total_value_a3s16g_model_by_area[$_area_id]
                - $total_value_k3_model_by_area[$_area_id]
                - $total_value_a1k_subsidi_model_by_area[$_area_id]
                - $total_value_a1k_model_by_area[$_area_id]
//                        - $total_value_reno2f_model_by_area[$_area_id]
//                        - $total_value_a5s_model_by_area[$_area_id]
            ) * ( (100 - $active_vuot) / 100 );
        $total_value_excluded_special_modes_IND[$_area_id] = ($total_value_by_area_IND[$_area_id]
//                        - $total_value_a71k_model_by_area_IND[$_area_id]
//                        - $total_value_a3s16g_model_by_area_IND[$_area_id]
                - $total_value_k3_model_by_area_IND[$_area_id]
                - $total_value_a1k_subsidi_model_by_area_IND[$_area_id]
                - $total_value_a1k_model_by_area_IND[$_area_id]
//                        - $total_value_reno2f_model_by_area_IND[$_area_id]
//                        - $total_value_a5s_model_by_area_IND[$_area_id]
            ) * ( (100 - $active_vuot) / 100 );
        $total_value_excluded_special_modes_KA[$_area_id]  = ($total_value_by_area_KA[$_area_id]
//                        - $total_value_a71k_model_by_area_KA[$_area_id]
//                        - $total_value_a3s16g_model_by_area_KA[$_area_id]
                - $total_value_k3_model_by_area_KA[$_area_id]
                - $total_value_a1k_subsidi_model_by_area_KA[$_area_id]
                - $total_value_a1k_model_by_area_KA[$_area_id]
//                        - $total_value_reno2f_model_by_area_KA[$_area_id]
//                        - $total_value_a5s_model_by_area_KA[$_area_id]
            ) * ( (100 - $active_vuot) / 100 );

    } else {
        $total_value_tinh_kpi[$_area_id]                   = $total_value_tinh_kpi_IND[$_area_id]               = $total_value_tinh_kpi_KA[$_area_id]                = 0;
        $total_value_a71k[$_area_id]                       = $total_value_a71k_IND[$_area_id]                   = $total_value_a71k_KA[$_area_id]                    = 0;
        $total_value_a3s16g[$_area_id]                     = $total_value_a3s16g_KA[$_area_id]                  = $total_value_a3s16g_IND[$_area_id]                 = 0;
        $total_value_k3[$_area_id]                         = $total_value_k3_KA[$_area_id]                      = $total_value_k3_IND[$_area_id]                     = 0;
        $total_value_a1k_subsidi[$_area_id]                = $total_value_a1k_subsidi_KA[$_area_id]             = $total_value_a1k_subsidi_IND[$_area_id]            = 0;
        $total_value_a1k[$_area_id]                        = $total_value_a1k_KA[$_area_id]                     = $total_value_a1k_IND[$_area_id]                    = 0;
        $total_value_reno2f[$_area_id]                     = $total_value_reno2f_KA[$_area_id]                  = $total_value_reno2f_IND[$_area_id]                 = 0;
        $total_value_a5s[$_area_id]                        = $total_value_a5s_KA[$_area_id]                     = $total_value_a5s_IND[$_area_id]                    = 0;
        $total_value_excluded_special_modes[$_area_id]     = $total_value_excluded_special_modes_IND[$_area_id] = $total_value_excluded_special_modes_KA[$_area_id]  = 0;
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
    }
}

$alpha++;
// Cot Value tính KPI IND
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi_IND[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_tinh_kpi_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI KA
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi_KA[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_tinh_kpi_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a71k[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a71k_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a3s16g[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a3s16g_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
/**
 * @author: hero
 * k3
 * description
 * date
 */
$alpha++;
// Cot Value tính KPI K3
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_k3[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI K3 IND
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_k3[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI K3 KA
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_k3[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_k3_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}

$alpha++;

//a1k sub
// Cot Value tính KPI A1k Subsidi
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_subsidi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI A1k Subsidi IND
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_subsidi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI A1k Subsidi KA
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k_subsidi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_subsidi_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
//End a1k sub

//a1k
// Cot Value tính KPI A1k
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI A1k IND
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI A1k KA
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_a1k[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a1k_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
//End a1k


//reno2f
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_reno2f[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_reno2f_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//End reno2f

//a5s
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_IND[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_a5s[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_a5s_KA[$_area_id]);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
//End a5s
// Cot Value tính KPI Models còn lại
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_excluded_special_modes[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_excluded_special_modes[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI Models còn lại IND
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_excluded_special_modes[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_excluded_special_modes_IND[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Value tính KPI Models còn lại KA
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_excluded_special_modes[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_excluded_special_modes_KA[$_area_id]);
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// echo "<pre>";print_r($total_value_tinh_kpi);die;
foreach ($total_value_tinh_kpi as $_key => $_v) {
    $total_value_tinh_kpi_finnaly += $_v;
}


$area_list        = $QArea->fetchAll();
$area_regionshare = array();
foreach ($area_list as $_key => $_area)
    if (isset($area_array[$_area['id']])) {
        $area_regionshare[$_area['id']] = $_area['region_share'];
        $sheet->setCellValue($alpha . $area_array[$_area['id']], $_area['region_share']);
    }

$alpha++;

// foreach ($point_list as $_area_id => $_point)
//     if (isset($area_array[$_area_id]))
//         $sheet->setCellValue($alpha . $area_array[$_area_id], $_point);
// $alpha++;
$ponit_moi_ = array();
// echo "<pre>";print_r($area_cache);die;
foreach ($area_cache as $_area_id => $_area_name) {
    foreach ($point_list as $_area_id => $_point) {
        $active_vuot = round((($total_by_area[$_area_id] - $total_activated_by_area[$_area_id]) / $total_by_area[$_area_id] ) * 100, 1) - round((($total_all_area - $total_activated_all_area) / $total_all_area ) * 100, 1);
        if (isset($total_activated_by_area[$_area_id])) {

            $ponit_moi             = round(($total_value_tinh_kpi[$_area_id] / $total_value_tinh_kpi_finnaly) * 60 / ($area_regionshare[$_area_id] / 100), 2);
            $ponit_moi_[$_area_id] = round(($total_value_tinh_kpi[$_area_id] / $total_value_tinh_kpi_finnaly) * 60 / ($area_regionshare[$_area_id] / 100), 2);
            $sheet->setCellValue($alpha . $area_array[$_area_id], $ponit_moi);

            // $sheet->setCellValue($alpha . $area_array[$_area_id], $total_value_tinh_kpi[$_area_id].'----'.$total_value_tinh_kpi_finnaly.'-----'.$area_regionshare[$_area_id]) ;
        } else {
            $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
        }
    }
    // if (isset($area_array[$_area_id]))
}
// echo "<pre>";print_r($total_value_tinh_kpi);
// echo "<pre>";print_r($ponit_moi_);die;
$chiet_khau     = array();
$chiet_khau_IND = array();
$chiet_khau_KA  = array();
foreach ($ponit_moi_ as $_key => $_v) {
    $array_hcm_hn   = array(10, 51, 32, 33, 24, 25, 26, 1, 34);
    $array_dn_ct_hp = array(7, 4, 13);
    if (in_array($_key, $array_dn_ct_hp)) {
        if ($_v >= 75)
            $chiet_khau[$_key] = 6.9;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau[$_key] = 6.6;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau[$_key] = 6.3;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau[$_key] = 6;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau[$_key] = 5.85;
        elseif ($_v < 51)
            $chiet_khau[$_key] = 5.7;



        if ($_v >= 75)
            $chiet_khau_KA[$_key] = 6.1;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau_KA[$_key] = 5.9;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau_KA[$_key] = 5.7;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau_KA[$_key] = 5.5;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau_KA[$_key] = 5.3;
        elseif ($_v < 51)
            $chiet_khau_KA[$_key] = 5.1;

        if ($_v >= 75)
            $chiet_khau_IND[$_key] = 7.1;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau_IND[$_key] = 6.9;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau_IND[$_key] = 6.7;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau_IND[$_key] = 6.5;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau_IND[$_key] = 6.3;
        elseif ($_v < 51)
            $chiet_khau_IND[$_key] = 6.1;
    }
    if (in_array($_key, $array_hcm_hn)) {
        if ($_v >= 75)
            $chiet_khau[$_key] = 7.4;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau[$_key] = 7.1;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau[$_key] = 6.8;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau[$_key] = 6.5;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau[$_key] = 6.35;
        elseif ($_v < 51)
            $chiet_khau[$_key] = 6.2;

        if ($_v >= 75)
            $chiet_khau_KA[$_key] = 6.6;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau_KA[$_key] = 6.4;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau_KA[$_key] = 6.2;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau_KA[$_key] = 6;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau_KA[$_key] = 5.8;
        elseif ($_v < 51)
            $chiet_khau_KA[$_key] = 5.6;

        if ($_v >= 75)
            $chiet_khau_IND[$_key] = 7.6;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau_IND[$_key] = 7.4;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau_IND[$_key] = 7.2;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau_IND[$_key] = 7;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau_IND[$_key] = 6.8;
        elseif ($_v < 51)
            $chiet_khau_IND[$_key] = 6.6;
    }
    if (!in_array($_key, $array_hcm_hn) && !in_array($_key, $array_dn_ct_hp)) {
        if ($_v >= 75)
            $chiet_khau[$_key] = 6.4;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau[$_key] = 6.1;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau[$_key] = 5.8;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau[$_key] = 5.5;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau[$_key] = 5.35;
        elseif ($_v < 51)
            $chiet_khau[$_key] = 5.2;

        if ($_v >= 75)
            $chiet_khau_KA[$_key] = 5.6;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau_KA[$_key] = 5.4;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau_KA[$_key] = 5.2;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau_KA[$_key] = 5;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau_KA[$_key] = 4.8;
        elseif ($_v < 51)
            $chiet_khau_KA[$_key] = 4.6;

        if ($_v >= 75)
            $chiet_khau_IND[$_key] = 6.6;
        elseif ($_v >= 69 && $_v < 75)
            $chiet_khau_IND[$_key] = 6.4;
        elseif ($_v >= 63 && $_v < 69)
            $chiet_khau_IND[$_key] = 6.2;
        elseif ($_v >= 57 && $_v < 63)
            $chiet_khau_IND[$_key] = 6;
        elseif ($_v >= 51 && $_v < 57)
            $chiet_khau_IND[$_key] = 5.8;
        elseif ($_v < 51)
            $chiet_khau_IND[$_key] = 5.6;
    }
}
// echo "<pre>";print_r($total_value_tinh_kpi);
// echo "<pre>";print_r($chiet_khau);die;

//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '4.5%');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '3.5%');
//        }
$alpha++;
// Cot Chiết khấu K3 (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], '0%');
}
$alpha++;
//a1k sub
// Cot Chiết khấu A1k Subsidy (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], '4.5%');
}
$alpha++;
//a1k
// Cot Chiết khấu A1k (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], '4.5%');
}
$alpha++;
//reno2f IND %
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], ($chiet_khau_IND[$_area_id] + 1). '%' );
//        }
//        $alpha++;
//reno2f KA %
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], ($chiet_khau_KA[$_area_id] + 1). '%' );
//        }
//        $alpha++;
//a5s
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '2%');
//        }
//        $alpha++;
//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], $chiet_khau[$_area_id] . '%');
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
// Cot Chiết khấu IND (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $chiet_khau_IND[$_area_id] . '%');
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;
// Cot Chiết khấu KA (%)
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], $chiet_khau_KA[$_area_id] . '%');
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}
$alpha++;


//        foreach ($area_cache as $_area_id => $_area_name) {
//            if (isset($total_value_tinh_kpi[$_area_id]))
//                $sheet->setCellValue($alpha . $area_array[$_area_id], round(($total_value_tinh_kpi[$_area_id] * $chiet_khau[$_area_id]) / 100), 1);
//            else
//                $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
//        }
//        $alpha++;
// Cot Bonus Special model & model còn lại
foreach ($area_cache as $_area_id => $_area_name) {
    if (isset($total_value_tinh_kpi[$_area_id]))
        $sheet->setCellValue($alpha . $area_array[$_area_id], round(
            (($total_value_excluded_special_modes_IND[$_area_id] * $chiet_khau_IND[$_area_id]) / 100)
            + (($total_value_a1k_IND[$_area_id] * 4.5) / 100)
            + (($total_value_a1k_subsidi_IND[$_area_id] * 4.5) / 100)
            + (($total_value_excluded_special_modes_KA[$_area_id]
                    * $chiet_khau_KA[$_area_id]) / 100)
            + (($total_value_a1k_KA[$_area_id] * 4.5) / 100)
            + (($total_value_a1k_subsidi_KA[$_area_id] * 4.5) / 100)
            , 1));
    else
        $sheet->setCellValue($alpha . $area_array[$_area_id], '-');
}





// ----------------------------------------------------------------------------------------------------------------------
// thêm cột phần tính máy chạy bộ
$alpha++;
$params ['from_f'] = $from_f;
$params ['to_f'] = $to_f;
$sales_final_oppo = $QAreaRankByMonth->getDataOppoByArea($params);

//// reformat lại data của oppo để sử dụng hàm
//$sales_final_oppo = [];
//foreach ($area_cache as $area_id => $area_name) {
//    $sales_final_oppo [$area_id] = [
//      'total_quantity'   => $total_by_area [$area_id],
//      'total_activated'   => $total_activated_by_area [$area_id],
//      'total_value'   => $total_value_by_area [$area_id],
//      'value_kpi'   => $total_value_tinh_kpi [$area_id],
//      'region_share'   => $region_share [$area_id]
//    ];
//}

// lấy data realme + oppo
$data_all = $QAreaRankByMonth->getDataAllOrderByRank($sales_final_oppo, $sales_final_realme);
$sales_final_all = $data_all['data_area'];

// cột unit realme
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_realme [$_area_id] ['total_quantity']);
}
$alpha++;
// cột unit activated realme
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_realme [$_area_id] ['total_activated']);
}
$alpha++;

// cột tỉ lệ chưa active Realme cả nước
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_realme [$_area_id] ['percent_not_activated_total']);
}
$alpha++;

// cột tỉ lệ chưa active Realme khu vực
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_realme [$_area_id] ['percent_not_activated_area']);
}
$alpha++;

// cột tỉ lệ active vượt
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_realme [$_area_id] ['active_vuot']);
}
$alpha++;

// cột value 80% Realme
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_realme [$_area_id] ['total_value']);
}
$alpha++;

// cột value kpi Realme
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_realme [$_area_id] ['value_kpi']);
}
$alpha++;

// cột unit all
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['quantity_all']);
}
$alpha++;

// cột unit activated all
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['quantity_activated_all']);
}
$alpha++;

// cột value 80% all
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['value_all']);
}
$alpha++;

// cột value kpi all
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['value_kpi_all']);
}
$alpha++;

// cột point máy chạy bộ
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['point']);
}
$alpha++;

// cột rank máy chạy bộ
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['rank']);
}
$alpha++;

// cột highend product
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['quantity_highend_all']);
}
$alpha++;

// cột lowend product
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['quantity_lowend_all']);
}
$alpha++;

// cột loại thưởng phạt
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['rank_type'] == 1 ? 'Thưởng' : ($sales_final_all [$_area_id] ['rank_type'] == 2 ? 'Phạt' : ''));
}

$alpha++;
// cột tiền thưởng phạt
foreach ($area_cache as $_area_id => $_area_name) {
    $sheet->setCellValue($alpha . $area_array[$_area_id], $sales_final_all [$_area_id] ['money']);
}


$filename  = 'Sell out - Area - NEW' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;