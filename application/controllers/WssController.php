<?php
class WssController extends My_Controller_Action
{
    private $wssURI;
    private $namespace;

    public function init()
    {
        error_reporting(0);
        ini_set("display_error", -1);
        set_time_limit(0);
        ini_set('memory_limit', -1);

        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->wssURI = HOST.'wss';
        $this->namespace = 'OPPOVN';

        doAuthenticate();
    }

    public function indexAction()
    {
        
        $this->_helper->layout->disableLayout();
        
        //Create a new soap server
        $server = new soap_server(null, array('uri' => $this->wssURI));

        /*$server->soap_defencoding = 'utf-8';
        $server->decode_utf8 = false;*/

        //Configure our WSDL
        $server->configureWSDL($this->namespace, $this->namespace, $this->wssURI);
        $server->wsdl->schemaTargetNamespace = $this->namespace;

        // ---------------- wsAddNotification
        $server->register(
            'wsAddNotification',
            array(
                'title'    => 'xsd:string',
                'content'  => 'xsd:string',
                'category' => 'xsd:integer',
                'show_to'  => 'xsd:array',
                'pop_up'   => 'xsd:integer',
                'type'     => 'xsd:integer',
            ),
            array(),    // output parameters
            $this->namespace,
            $this->namespace.'#wsAddNotification',
            'rpc',
            'encoded',
            'Add notification'
        );

        // Complex Array Keys and Types ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('areaInfo','complexType','struct','all','',
            array(
                'id' => array('name'=>'id','type'=>'xsd:int'),
                'name' => array('name'=>'name','type'=>'xsd:string')
            )
        );
        // *************************************************************************

        // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('areaInfoArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:areaInfo[]'
                )
            ),
            'tns:areaInfo'
        );


        // Complex job ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('jobInfo','complexType','struct','all','',
            array(
                'id' => array('name'=>'id','type'=>'xsd:int'),
                'title' => array('name'=>'title','type'=>'xsd:string'),
                'status' => array('name'=>'status','type'=>'xsd:int'),
                'from' => array('name' => 'from', 'type' => 'xsd:string'),
                'to'   => array('name' => 'to', 'type' => 'xsd:string'),
                'category' => array('name' => 'category', 'type' => 'xsd:int'),
                'salary' => array('name' => 'salary', 'type' => 'xsd:int'),
                'year' => array('name' => 'year', 'type' => 'xsd:int'),
                'area_id' => array('name' => 'area_id', 'type' => 'xsd:int'),
                'regional_market_id' => array('name' => 'regional_market_id', 'type' => 'xsd:int'), 
                'content' => array('name'=>'content','type'=>'xsd:string'), 
                'created_at' => array('name' => 'created_at', 'type' => 'xsd:string'),
            )
        );

         // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('jobInfoArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:jobInfo[]'
                )
            ),
            'tns:jobInfo'
        );

        // Complex Array Keys and Types ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('provinceInfo','complexType','struct','all','',
            array(
                'id' => array('name'=>'id','type'=>'xsd:int'),
                'name' => array('name'=>'name','type'=>'xsd:string'),
                'parent' => array('name'=>'parent','type'=>'xsd:int')
            )
        );
        // *************************************************************************

        // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('provinceInfoArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:provinceInfo[]'
                )
            ),
            'tns:provinceInfo'
        );
        
        // Complex Array Keys and Types ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('provinceJob','complexType','struct','all','',
            array(
                'province_name' => array('name'=>'province_name','type'=>'xsd:string'),
                'province_code' => array('name'=>'province_code','type'=>'xsd:int'),
                'district_center' => array('name'=>'district_center','type'=>'xsd:string'),
                'district_id' => array('name'=>'district_id','type'=>'xsd:int')
            )
        );
        // *************************************************************************

        // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('provinceJobArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:provinceJob[]'
                )
            ),
            'tns:provinceJob'
        );

        // Complex Array Keys and Types ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('categoryJob','complexType','struct','all','',
            array(
                'name'      => array('name'=>'name','type'=>'xsd:string'),
                'id'        => array('id'=>'id','type'=>'xsd:int'),
                'parent'    => array('parent'=>'parent','type'=>'xsd:int'),
            )
        );

        // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('categoryJobArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:categoryJob[]'
                )
            ),
            'tns:categoryJob'
        );
        
        // Complex Array Keys and Types ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('diamondLevelInfo','complexType','struct','all','',
            array(
                'id'                  => array('name' => 'id','type' => 'xsd:int'),
                'name'                => array('name' => 'name','type' => 'xsd:string'),
                'sales_from'          => array('name' => 'sales_from', 'type' => 'xsd:integer'),
                'sales_to'            => array('name' => 'sales_to', 'type' => 'xsd:integer'),
                'sales_market_share'  => array('name' => 'sales_market_share', 'type' => 'xsd:decimal'),
                'figure_market_share' => array('name' => 'figure_market_share', 'type' => 'xsd:decimal'),
            )
        );
        // *************************************************************************

        // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('diamondLevelInfoArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:diamondLevelInfo[]'
                )
            ),
            'tns:diamondLevelInfo'
        );

        // Complex Array Keys and Types ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('storeInfo','complexType','struct','all','',
            array(
                'id'       => array('name' => 'id','type' => 'xsd:integer'),
                'name'     => array('name' => 'name','type' => 'xsd:string'),
                'company_address'  => array('name' => 'company_address','type' => 'xsd:string'),
                'loyalty_plan'  => array('name' => 'loyalty_plan','type' => 'xsd:string'),
                'district' => array('name' => 'district','type' => 'xsd:integer'),
                'd_id'     => array('name' => 'd_id','type' => 'xsd:integer'),
                'del'     => array('name' => 'del','type' => 'xsd:integer'),
            )
        );
        // *************************************************************************

        // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('storeInfoArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:storeInfo[]'
                )
            ),
            'tns:storeInfo'
        );

        // Complex Array Keys and Types ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('params','complexType','struct','all','',
            array(
                'name'  => array('name' => 'name','type' => 'xsd:string'),
                'value' => array('name' => 'value','type' => 'xsd:string'),
            )
        );

        // Complex Array ++++++++++++++++++++++++++++++++++++++++++
        $server->wsdl->addComplexType('paramsArray','complexType','array','','SOAP-ENC:Array',
            array(),
            array(
                array(
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:params[]'
                )
            ),
            'tns:params'
        );

        // ---------------- wsGetAllArea
        $server->register(
            'wsGetAllArea',
            array(),
            array('area_list' => 'tns:areaInfoArray'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetAllArea',
            'rpc',
            'encoded',
            'Get all Area from Center'
        );

        // ---------------- wsGetAllJob
        $server->register(
            'wsGetAllJob',
            array(
                'page'   => 'xsd:integer',
                'limit'  => 'xsd:integer',
                'params' => 'xsd:string',
            ),
            array('job_list' => 'xsd:Array'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetAllJob',
            'rpc',
            'encoded',
            'Get all Job from Center'
        );

         // ---------------- Insert CV
        $server->register(
            'wsInsertCV',
            array(
                'data'   => 'xsd:string',
                'token'  => 'xsd:string',
                'auth'   => 'xsd:string'
            ),
            array('result' => 'xsd:Array'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsInsertCV',
            'rpc',
            'encoded',
            'Insert CV from webservice'
        );


        // ---------------- wsGetAllJobDetail
        $server->register(
            'wsGetDetailJob',
            array(               
                'id' => 'xsd:integer',
            ),
            array('job_list' => 'xsd:Array'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetDetailJob',
            'rpc',
            'encoded',
            'Get detail Job from Center'
        );

        // ---------------- wsGetAllProvince
        $server->register(
            'wsGetAllProvince',
            array(),
            array('province_list' => 'tns:provinceInfoArray'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetAllProvince',
            'rpc',
            'encoded',
            'Get all province'
        );

        // ---------------- wsGetProvinceByAreaId
        $server->register(
            'wsGetProvinceByAreaId',
            array('id' => 'xsd:integer',),
            array('province_list' => 'tns:provinceInfoArray'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetProvinceByAreaId',
            'rpc',
            'encoded',
            'Get province by area id'
        );

        // ---------------- wsGetDistrictByProvinceId
        $server->register(
            'wsGetDistrictByProvinceId',
            array('id' => 'xsd:integer',),
            array('district_list' => 'tns:provinceInfoArray'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetDistrictByProvinceId',
            'rpc',
            'encoded',
            'Get district by province id'
        );
        
        // ---------------- wsGetProvince theo tỉnh thành
        $server->register(
            'wsGetProvince',
            array(),
            array('province_list' => 'tns:provinceJobArray'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetProvince',
            'rpc',
            'encoded',
            'Get all province'
        );

        // ---------------- wsGetJobCategory 
        $server->register(
            'wsGetJobCategory',
            array(),
            array('jobcat_list' => 'tns:categoryJobArray'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetJobCategory',
            'rpc',
            'encoded',
            'Get all categories'
        );

        // ---------------- wsGetAllDiamondLevel
        $server->register(
            'wsGetAllDiamondLevel',
            array(),
            array('list' => 'tns:diamondLevelInfoArray'),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetAllDiamondLevel',
            'rpc',
            'encoded',
            'Get all diamond level'
        );

        // ---------------- wsGetStore
        $server->register(
            'wsGetStore',
            array(
                'page'   => 'xsd:integer',
                'limit'  => 'xsd:integer',
                'params' => 'tns:paramsArray',
            ),
            array(
                'list' => 'tns:storeInfoArray',
                'total' => 'xsd:integer',
            ),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetStore',
            'rpc',
            'encoded',
            'Get store list'
        );

        // ---------------- wsLogin
        $server->register('wsLogin',                    // method name
            array(

                'username'      => 'xsd:string',
                'password'      => 'xsd:string',
                'system'        => 'xsd:string',
                'time'          => 'xsd:int',
                'authentication'=> 'xsd:string',

            ),          // input parameters
            array('return' => 'xsd:Array'),    // output parameters
            $this->namespace,                         // namespace
            $this->namespace . '#wsLogin',                   // soapaction
            'rpc',                                    // style
            'encoded',                                // use
            'wsLogin'        // documentation
        );
        
        
        // ----------------  wsAddSelloutPartner
        $server->register('wsAddSelloutPartner',                    // method name
            array(

                'PartnerAccount'      => 'xsd:string',
                'Password'            => 'xsd:string',
                'ValueDate'           => 'xsd:string',
                'Ref'                 => 'xsd:string',
                'Model'               => 'xsd:string',
                'Imei'                => 'xsd:string',
                'BillNumber'          => 'xsd:string',
                'BillDate'            => 'xsd:string',
                'WarehouseName'       => 'xsd:string',
                'WarehouseId'         => 'xsd:int',
                'CustomerName'        => 'xsd:string',
                'CustomerAddress'     => 'xsd:string',
                'CustomerPhone'       => 'xsd:string',
                'Note'                => 'xsd:string',

            ),          // input parameters
            array('return' => 'xsd:string'),    // output parameters
            $this->namespace,                           // namespace
            $this->namespace . '#wsAddSelloutPartner', // soapaction
            'rpc',                                      // style
            'encoded',                                  // use
            'wsAddSelloutPartner'                      // documentation
        );

        // ----------------  wsAddImeiChange
        $server->register('wsAddImeiChange',                    // method name
            array(

                'PartnerAccount'      => 'xsd:string',
                'Password'            => 'xsd:string',
                'ValueDate'           => 'xsd:string',
                'ModelTgdd'           => 'xsd:string',
                'WarehouseId'         => 'xsd:int',
                'Imei'                => 'xsd:string',
                'Status'              => 'xsd:int',
                'Number'              => 'xsd:string',
                'Date'                => 'xsd:string',
                'ImeiReplace'         => 'xsd:string',

            ),          // input parameters
            array('return' => 'xsd:string'),            // output parameters
            $this->namespace,                           // namespace
            $this->namespace . '#wsAddImeiChange',      // soapaction
            'rpc',                                      // style
            'encoded',                                  // use
            'wsAddImeiChange'                           // documentation
        );

        // ---------------- wsGetAllStaff
        $server->register('wsGetAllStaff',                    // method name
            array(
                'params'        => 'xsd:Array',
                'system'        => 'xsd:string',
                'time'          => 'xsd:int',
                'authentication'=> 'xsd:string',

            ),          // input parameters
            array('return' => 'xsd:Array'),    // output parameters
            $this->namespace,                         // namespace
            $this->namespace . '#wsGetAllStaff',                   // soapaction
            'rpc',                                    // style
            'encoded',                                // use
            'wsGetAllStaff'        // documentation
        );

        // ---------------- wsSetDistributorStockLog
        $server->register(
            'wsSetDistributorStockLog',
            array(
                'params' => 'tns:paramsArray',
            ),
            array(),    // output parameters
            $this->namespace,
            $this->namespace.'#wsSetDistributorStockLog',
            'rpc',
            'encoded',
            'Get wsSetDistributorStockLog'
        );

        // ---------------- wsGetStoreByStrId
        $server->register(
            'wsGetStoreByStrId',
            array(
                'str_id' => 'xsd:string',
            ),
            array(
                'result' => 'xsd:Array',
            ),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetStoreByStrId',
            'rpc',
            'encoded',
            'Get Store By String Id'
        );

        // ---------------- wsGetStoreRandom
        $server->register(
            'wsGetStoreRandom',
            array(
                'params' => 'xsd:Array',
            ),
            array(
                'return' => 'xsd:Array',
            ),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetStoreRandom',
            'rpc',
            'encoded',
            'Get Store Random'
        );
        
        // ---------------- wsGetStoreByLeader
        $server->register(
            'wsGetStoreByLeader',
            array(
                'params' => 'xsd:Array',
            ),
            array(
                'return' => 'xsd:Array',
            ),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetStoreByLeader',
            'rpc',
            'encoded',
            'Get Store Random'
        );
        
        // ---------------- wsGetStoreBySale
        $server->register(
            'wsGetStoreBySale',
            array(
                'params' => 'xsd:Array',
            ),
            array(
                'return' => 'xsd:Array',
            ),    // output parameters
            $this->namespace,
            $this->namespace.'#wsGetStoreBySale',
            'rpc',
            'encoded',
            'Get store by email'
        );

        // ----------------  wsCrossCheckIn
        $server->register('wsCrossCheckIn',             // method name
            array(
                'PartnerAccount'      => 'xsd:string',
                'Password'            => 'xsd:string',
                'ValueDate'           => 'xsd:string',

            ),                                          // input parameters
            array('return' => 'xsd:Array'),            // output parameters
            $this->namespace,                           // namespace
            $this->namespace . '#wsCrossCheckIn',       // soapaction
            'rpc',                                      // style
            'encoded',                                  // use
            'wsCrossCheckIn'                            // documentation
        );

        // ----------------  getInfoImei
        $server->register('getInfoImei',             // method name
            array(
                'Imei'           => 'xsd:string'
            ),                                          // input parameters
            array('return' => 'xsd:Array'),             // output parameters
            $this->namespace,                           // namespace
            $this->namespace . '#getInfoImei',          // soapaction
            'rpc',                                      // style
            'encoded',                                  // use
            'getInfoImei'                               // documentation
        );
        
        // ---------------- wsGetStaffCs
        $server->register('wsGetStaffCs',                    // method name
            array(

                'system'        => 'xsd:string',
                'time'          => 'xsd:int',
                'authentication'=> 'xsd:string',
                'email' => 'xsd:string',

            ),          // input parameters
            array('return' => 'xsd:Array'),    // output parameters
            $this->namespace,                         // namespace
            $this->namespace . '#wsGetStaffCs',                   // soapaction
            'rpc',                                    // style
            'encoded',                                // use
            'wsGetStaffCs'        // documentation
        );

        // $POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
        
        //$POST_DATA = file_get_contents('php://input');
        
        // $server->service($POST_DATA);
		@$server->service(file_get_contents("php://input"));
    }
}

function doAuthenticate(){
    if(isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW']) )
    {
        if($_SERVER['PHP_AUTH_USER']==WS_USERNAME and $_SERVER['PHP_AUTH_PW']=WS_PASSWORD )
            return true;
        else
            return  false;
    }
}

function wsGetAllStaff(array $params, $system, $time, $authentication){

    $rs = array('code' => 0);
    try {
        if (!$system
            or !$time
            or !$authentication){

            $code = 2;
            throw new Exception('The parameter is invalid');

        }

        // check authenticate
        $AUTHENTICATE_KEY = unserialize(AUTHENTICATE_KEY);
        if (
            !isset($AUTHENTICATE_KEY[strtoupper($system)])
            or md5($time.'0pp0'.$AUTHENTICATE_KEY[strtoupper($system)]) != $authentication
        ) {
            $code = 3;

            throw new Exception('Authentication is invalid');
        }

        // check time
        /*if (time() - $time > 30){
            $code = 4;
            throw new Exception('Time is invalid');
        }*/
        // End of check authenticate

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => 'staff'),
                array('p.id','p.firstname','p.lastname','p.email'));

        if (isset($params['group_id']) and $params['group_id']){
            if (strtoupper($system)=='CS') {
                $select->join(array('scp' => 'staff_cs_priviledge'), 'p.id=scp.staff_id AND scp.type=' . STAFF_CS_PRIVILEGE_TYPE_GROUP, array());
                $gr = explode(',', $params['group_id']);
                $select->where('scp.value IN (?)', $gr);
            }
        }

        $rs['data'] = $db->fetchAll($select);

    } catch (Exception $e){
        $rs['code'] = isset($code) ? ($code and $message = $e->getMessage()) : (1 and $message = 'Unknown Error');
        $rs['message'] = $message;
    }
    return $rs;
}

function wsAddSelloutPartner( $PartnerAccount, $Password, $ValueDate, $Ref, $Model, $Imei, $BillNumber, $BillDate, $WarehouseName, $WarehouseId, $CustomerName, $CustomerAddress, $CustomerPhone, $Note){

    $rs = array(
        'ResponseCode' => '00',
        'Description' => 'Successful',
    );

    $arg = array(
        'PartnerAccount'    => $PartnerAccount,
        'Password'          => $Password,
        'ValueDate'         => $ValueDate,
        'Ref'               => $Ref,
        'Model'             => $Model,
        'Imei'              => $Imei,
        'BillNumber'        => $BillNumber,
        'BillDate'          => $BillDate,      
        'WarehouseName'     => $WarehouseName,
        'WarehouseId'       => $WarehouseId,
        'CustomerName'      => $CustomerName,
        //'CustomerAddress'   => $CustomerAddress,
        'CustomerPhone'     => $CustomerPhone,
        'Note'              => $Note,
        
    );
    $s = serialize($arg);

    //error_log($s);

    try {
        if (!$PartnerAccount
                /* or !validateDate($ValueDate, 'Y-m-d H:i:s') */
                or ! $Password
                or ! $Ref
                or ! $Model
                or ! $Imei
                or ! $BillNumber
                or ! $BillDate
                or ! $WarehouseName
                or ! $WarehouseId
                or ! $CustomerName
                //or ! $CustomerAddress
                or ! $CustomerPhone
                or ! $Note
        ) {

            $ResponseCode = '02';
            throw new Exception('The parameter is invalid');
        }

        /* TODO check authenticate */
        $AUTHENTICATE_KEY = unserialize(AUTHENTICATE_KEY_PARTNER);

        /* TODO check account */
        $account_id = $AUTHENTICATE_KEY[$PartnerAccount]['id'];
        if (
                !$account_id
        ) {
            $ResponseCode = '04';
            throw new Exception('Partner Account is invalid');
        }
        /* End of check account */

        if (
                !isset($AUTHENTICATE_KEY[$PartnerAccount])
                or ! isset($AUTHENTICATE_KEY[$PartnerAccount]['password'])
                or md5($ValueDate . $AUTHENTICATE_KEY[$PartnerAccount]['password']) != $Password
        ) {
            $ResponseCode = '03';
            throw new Exception('Password is invalid');
        }
        /* End of check authenticate */
		
		$Ref = urldecode($Ref);
		$Ref = base64_decode($Ref);
		
		$Model = urldecode($Model);
		$Model = base64_decode($Model);
		
		$Imei = urldecode($Imei);
		$Imei = base64_decode($Imei);
		
		$BillNumber = urldecode($BillNumber);
		$BillNumber = base64_decode($BillNumber);
		
		$WarehouseName = urldecode($WarehouseName);
		$WarehouseName = base64_decode($WarehouseName);
		
		$CustomerName = urldecode($CustomerName);
		$CustomerName = base64_decode($CustomerName);
		
        if($CustomerAddress){
    		$CustomerAddress = urldecode($CustomerAddress);
    		$CustomerAddress = base64_decode($CustomerAddress);
        }
		
		$CustomerPhone = urldecode($CustomerPhone);
		$CustomerPhone = base64_decode($CustomerPhone);
		
		$Note = urldecode($Note);
		$Note = base64_decode($Note);
		
        $data = array(
            'partner_account'  => $PartnerAccount,
            'ref'              => $Ref, 
            'model'            => $Model, 
            'imei'             => $Imei, 
            'bill_number'      => $BillNumber, 
            'bill_date'        => $BillDate, 
            'warehouse_name'   => $WarehouseName, 
            'warehouse_id'     => $WarehouseId, 
            'customer_name'    => $CustomerName, 
            'customer_address' => !empty($CustomerAddress) ? $CustomerAddress : NULL, 
            'customer_phone'   => $CustomerPhone, 
            'note'             => $Note,
			'created_at'	   => date('Y-m-d H:i:s')
        );

        /* TODO insert */
        $QSelloutPartner = new Application_Model_SelloutPartner();
        $id = $QSelloutPartner->insert($data);
        /* End of insert */

    } catch (Exception $e) {
        $Description = $e->getMessage();
        if (!isset($ResponseCode)) {
            $ResponseCode = '01';
            //$Description = 'Unknown Error';
        }
        
        if($Ref == '999999'){
            $Description = $e->getMessage();
        }
        
        $rs['ResponseCode'] = $ResponseCode;

        $rs['Description'] = $Description;
    }
	
    return $rs['ResponseCode'] . '|' .$rs['Description'];
}

function wsAddImeiChange( $PartnerAccount, $Password, $ValueDate, $ModelTgdd, $WarehouseId, $Imei, $Status, $Number, $Date, $ImeiReplace){

    $rs = array(
        'ResponseCode' => '00',
        'Description' => 'Successful',
    );

    $arg = array(
        'PartnerAccount'    => $PartnerAccount,
        'Password'          => $Password,
        'ValueDate'         => $ValueDate,
        'ModelTgdd'         => $ModelTgdd,
        'WarehouseId'       => $WarehouseId,
        'Imei'              => $Imei,
        'Status'            => $Status,
        'Number'            => $Number,      
        'Date'              => $Date,
        'ImeiReplace'       => $ImeiReplace,        
    );
    $s = serialize($arg);

    error_log($s);

    try {
        if (!$PartnerAccount
                /* or !validateDate($ValueDate, 'Y-m-d H:i:s') */
                or ! $Password
                or ! $ModelTgdd
                or ! $WarehouseId
                or ! $Imei
                or ! $Status
                or ! $Number
                or ! $Date
                or ! $ImeiReplace
        ) {

            $ResponseCode = '02';
            throw new Exception('The parameter is invalid');
        }

        /* TODO check authenticate */
        $AUTHENTICATE_KEY = unserialize(AUTHENTICATE_KEY_PARTNER);

        /* TODO check account */
        $account_id = $AUTHENTICATE_KEY[$PartnerAccount]['id'];
        if (
                !$account_id
        ) {
            $ResponseCode = '04';
            throw new Exception('Partner Account is invalid');
        }
        /* End of check account */

        if (
                !isset($AUTHENTICATE_KEY[$PartnerAccount])
                or ! isset($AUTHENTICATE_KEY[$PartnerAccount]['password'])
                or md5($ValueDate . $AUTHENTICATE_KEY[$PartnerAccount]['password']) != $Password
        ) {
            $ResponseCode = '03';
            throw new Exception('Password is invalid');
        }
        /* End of check authenticate */
        
        
        $data = array(
            'partner_account'  => $PartnerAccount,
            'model_tgdd'       => $ModelTgdd, 
            'warehouse_id'     => $WarehouseId, 
            'imei'             => $Imei, 
            'status'           => $Status, 
            'number'           => $Number, 
            'date'             => $Date,
            'imei_replace'     => $ImeiReplace
        );

        /* TODO insert */
        $QImeiReplace = new Application_Model_ImeiReplace();
        $id = $QImeiReplace->insert($data);
        /* End of insert */

    } catch (Exception $e) {
        $Description = $e->getMessage();
        if (!isset($ResponseCode)) {
            $ResponseCode = '01';
            //$Description = 'Unknown Error';
        }
        
        if($Ref == '999999'){
            $Description = $e->getMessage();
        }
        
        $rs['ResponseCode'] = $ResponseCode;

        $rs['Description'] = $Description;
    }
    
    return $rs['ResponseCode'] . '|' .$rs['Description'];
}

function wsLogin($username, $password, $system, $time, $authentication){

    $rs = array('code' => 0);
    try {
        if (!$username
            or !$password
            or !$system
            or !$time
            or !$authentication){

            $code = 2;
            throw new Exception('The parameter is invalid');

        }

        // check authenticate
        $AUTHENTICATE_KEY = unserialize(AUTHENTICATE_KEY);
        if (
            !isset($AUTHENTICATE_KEY[strtoupper($system)])
            or md5($time.'0pp0'.$AUTHENTICATE_KEY[strtoupper($system)]) != $authentication
        ) {
            $code = 3;
            throw new Exception('Authentication is invalid');
        }

        // check time
        /*if (time() - $time > 30){
            $code = 4;
            throw new Exception('Time is invalid');
        }*/
        // End of check authenticate

        $login = My_Staff_Login::auth(array(
            'username' => $username,
            'password' => $password,
        ), $system);

        if ($login['code'] != 0){
            $code = $login['code'];
            throw new Exception($login['message']);
        }
        $rs['data'] = $login['data'];
    } catch (Exception $e){
        $rs['code'] = isset($code) ? ($code and $message = $e->getMessage()) : (1 and $message = 'Unknown Error');
        $rs['message'] = $message;
    }
    return $rs;
}

function wsAddNotification($title, $content, $category, $show_to, $pop_up, $type) {
    $content = serialize( $show_to );

    return My_Notification::add($title, $content, $category, $show_to, $pop_up, $type);
}

/**
 * [wsGetAllArea description]
 * @return array('id' => $id, 'name' => $name)
 */
function wsGetAllArea() {
    $QArea = new Application_Model_Area();
    return $QArea->get_index_cache();
}

/**
 * [wsGetAllProvince description]
 * @return array('id' => $id, 'name' => $name, 'area_id' => $area_id)
 */
function wsGetAllProvince() {
    $QRegion = new Application_Model_RegionalMarket();
    return $QRegion->get_index_cache();
}

/**
 * [wsGetProvinceByAreaId description]
 * @param  integer $id area_id
 * @return array('id' => $id, 'name' => $name, 'area_id' => $area_id)
 */
function wsGetProvinceByAreaId($id) {
    $QRegion = new Application_Model_RegionalMarket();
    return $QRegion->get_index_by_area_cache($id);
}

/**
 * [wsGetDistrictByProvinceId description]
 * @param  integer $id province_id
 * @return array('id' => $id, 'name' => $name, 'parent' => $parent)
 */
function wsGetDistrictByProvinceId($id) {
    $QRegion = new Application_Model_RegionalMarket();
    return $QRegion->get_district_index_by_province_cache($id);
}

/**
 * [wsGetProvince description]
 * @return array('id' => $id, 'name' => $name, 'area_id' => $area_id)
 */
function wsGetProvince() {
    $QRegion = new Application_Model_RegionalMarket();
    return $QRegion->get_province_cache();
}

/**
 * [wsGetJobCategory description]
 * @return array('id' => $id, 'name' => $name, 'area_id' => $area_id)
 */
function wsGetJobCategory() {
    $QJobCat = new Application_Model_JobCategory();
    return $QJobCat->get_cache_wss();
}

/**

/**
 * [wsGetAllJob description]
 * @param  integer $page 
 * @param  integer $limit
 * @param  string $params
 * @return array()
 */
function wsGetAllJob($page , $limit , $params) {

    $QJob = new Application_Model_Job();
    $params = unserialize(base64_decode($params));
    
    
    
    $result = $QJob->fetchAllJob($params);
    
    if($params['job_group'] == 3){
        return array(
            'job_list' => $result,
        );
    }
    
    $QJobCat = new Application_Model_JobCategory();
    $cat = $QJobCat->get_all();
    
    return array(
        'job_list' => $result,
        'job_cat' => $cat
    );
}
/**
 * [wsInsertCV description]
 * @param  string $data province_id
 * @return array()
 */
function wsInsertCV($params ,$params_data_company, $params_data_region, $token , $auth)
{
    $QCv = new Application_Model_Cv();
    $QCvCompany = new Application_Model_CvCompanyOld();
    $QCvRegion  = new Application_Model_CvRegion();
    
    $params_data = unserialize(base64_decode($params));
    $params_data_company = unserialize(base64_decode($params_data_company));
    $params_data_region = unserialize(base64_decode($params_data_region));

    $token_revert = 'cuongdethuong' . $auth;
    $token_revert = md5($token_revert);

    if(!$token || !$auth)
           return array(
            'code' => -1,
            'message' => 'empty token'
            );


    if($token_revert != $token)
      return array(
            'code' => -1,
            'message' => 'invalid token'
        );

    if(!is_array($params_data))
        return array(
                'code' => -1,
                'message' => 'invalid params'
            );


    if(is_array($params_data))
    {

         try{
             
            $where = array();
            $where[] = $QCv->getAdapter()->quoteInto('email = ?',$params_data['email']);
            $where[] = $QCv->getAdapter()->quoteInto('created_at = ?',$params_data['created_at']);
            $check_cv = $QCv->fetchAll($where);

            if(count($check_cv) == 0){

                $id = $QCv->insert($params_data);
               
                $params_data_company['cv_id'] = $id;

                foreach($params_data_company['old_name'] as $key=>$value){
                    $data_old = array(
                        'name' =>  $value,
                        'position'  =>  $params_data_company['old_position'][$key],
                        'details_job'  =>  $params_data_company['old_details_job'][$key],
                        'details_reason'  =>  $params_data_company['old_details_reason'][$key],
                        'details_partner'  =>  $params_data_company['old_details_partner'][$key],
                        'time'  =>  $params_data_company['old_time'][$key],
                        'time_end'  =>  $params_data_company['old_time_end'][$key],
                        'cv_id' => $id
                    );


                   $id_com = $QCvCompany->insert($data_old);
                }

                /* REGION */
                foreach($params_data_region['regional_market'] as $key=>$value){
                    $data_region = array(
                        'region_id'  =>  $value,
                        'district_id'  =>  $params_data_region['regional_market2'][$key],
                        'cv_id' => $id
                    );
                   $id_region = $QCvRegion->insert($data_region);
                }

                if($id){
                    $_email = $params_data['email'];
                    $_name  = $params_data['name'];
                    $QCv->replyAction($_email, $_name);
                    
                    $job_id = $params_data['job_id'];
                    if (isset($job_id) && !empty($job_id)) {
                        $QCv->informToPersonChargAction($_name, $_email, $id, $job_id);
                    }
                }
                return array(
                        'code' =>  0,
                        'message' => 'Done'
                );
                                /* END IF */

            }
         }
         catch(exception $e)
         {
            return array(
                    'code' =>  0,
                    'message' => $e->getMessage()
            );
         }
        
    }

}

function wsGetDetailJob($id)
{



     $QJob = new Application_Model_Job();
     
     //$jobCurrentset = $QJob->find($id);
     //$result = $jobCurrentset->current()->toArray();
     
     $where = array();
     $where[] = $QJob->getAdapter()->quoteInto('id = ?', $id);
     //$where[] = $QJob->getAdapter()->quoteInto('status != ?', 2);
     $where[] = $QJob->getAdapter()->quoteInto("DATE(`to`) >= DATE(NOW())");

     

     $result = $QJob->fetchRow($where)->toArray();
     
     $QJobRegional = new Application_Model_JobRegional();
     $job_regional = $QJobRegional->get_regional_by_id($id);
     

     return array(
        'job_list' => $result, 
        'job_regional' => $job_regional
     );
}


/**
 * [wsGetDistrictByAreaId description]
 * @param  integer $id province_id
 * @return array('id' => $id, 'name' => $name, 'parent' => $parent)
 */
function wsGetDistrictByAreaId($id) {
    return;
}

/**
 * [wsGetAllDiamondLevel description]
 * @return array(
 *     'id' => $id,
 *     'name' => $name,
 *     'sales_from' => $sales_from,
 *     'sales_to' => $sales_to,
 *     'sales_market_share' => $sales_market_share,
 *     'figure_market_share' => $figure_market_share
 * )
 */
function wsGetAllDiamondLevel()
{
    $QLoyaltyPlan = new Application_Model_LoyaltyPlan();
    return $QLoyaltyPlan->get_all_index_cache();
}

/**
 * $params = array(
 *     'page' => 1,
 *     'limit' => LIMITATION,
 *     'params' => array(
 *         array('name' => 'diamond_level', 'value' => 3),
 *         array('name' => 'diamond_month', 'value' => '01/05/2015'),
 *         array('name' => 'area_id', 'value' => serialize(array(1,2,3,4,5,6,7,10))),
 *         array('name' => 'diamond', 'value' => 1),
 *     ),
 * );
 */
function wsGetStore($page, $limit, $params)
{
    $total = 0;
    $new_params = array();



    foreach ($params as $key => $param)
        $new_params[ $param['name'] ] = is_array(@unserialize($param['value'])) ? unserialize($param['value']) : $param['value'];

    $QStore = new Application_Model_Store();
    $store_list = $QStore->fetchPagination($page, $limit, $total, $new_params);

    return array('list' => $store_list, 'total' => $total);
}

/**
 * @author buu.pham
 * @param  array $data = array(
 *     'distributor_id',
 *     'good_id',
 *     'color_id',
 *     'quantity',
 *     'source', // 1: Center; 2: Warehouse; 3: Warranty
 *     'type', // 1: Import; 2: Export; 3: Adjust
 *     'staff_id', // staff ID ứng với source
 *     'note',
 * )
 * @return void
 */
function wsSetDistributorStockLog($data)
{
    $params = array();

    foreach ($data as $key => $d)
        $params[ $d['name'] ] = is_array(@unserialize($d['value'])) ? unserialize($d['value']) : $d['value'];

    $QStockLog = new Application_Model_DistributorStockLog();

    switch ($data['type']) {
        case My_Sale_Source_Type::IMPORT:
            $QStockLog->import($params);
            break;
        case My_Sale_Source_Type::EXPORT:
            $QStockLog->export($params);
            break;
        case My_Sale_Source_Type::ADJUST:
            $QStockLog->adjust($params);
            break;

        default:
            break;
    }
}

function wsGetStoreByStrId($str_id)
{
    $QStore = new Application_Model_Store();
    $where[] = $QStore->getAdapter()->quoteInto('id IN (' . $str_id . ')');
    $result = $QStore->fetchAll($where)->toArray();

    return array('result' => $result);
}
function wsGetStoreRandom($params)
{
    $QStore = new Application_Model_Store();
    $result = $QStore->get_store_random($params);

    return array('return' => $result);
}
function wsGetStoreByLeader($email)
{
    $QStaff = new Application_Model_Staff();
    $QStore = new Application_Model_StoreLeaderLog();
    
    $currentTime = date('Y-m-d H:i:s');
    $where = $QStaff->getAdapter()->quoteInto("email = ?",$email);
    $staff = $QStaff->fetchRow($where);
    $result = $QStore->get_stores_cache($staff->id,$currentTime,$currentTime);

    return array('return' => $result);
}

function wsGetStoreBySale($email)
{
    $QStaff = new Application_Model_Staff();
    $QStore = new Application_Model_Store();

    $where = $QStaff->getAdapter()->quoteInto("email = ?",$email);
    $staff = $QStaff->fetchRow($where);
    $result = $QStore->get_store_by_id($staff->id);

    return array('return' => $result);
}


function wsCrossCheckIn( $PartnerAccount, $Password, $ValueDate){

    $arg = array(
        'PartnerAccount'    => $PartnerAccount,
        'Password'          => $Password,
        'ValueDate'         => $ValueDate,      
    );
    $s = serialize($arg);

    error_log($s);

    try {
        if (!$PartnerAccount
            or ! $Password
            or ! $ValueDate
        ) {
            return array('return' => 'The parameter is invalid');
        }

        /* TODO check authenticate */
        $AUTHENTICATE_KEY = unserialize(AUTHENTICATE_KEY_PARTNER);

        /* TODO check account */
        $account_id = $AUTHENTICATE_KEY[$PartnerAccount]['id'];
        if (
                !$account_id
        ) {
            return array('return' => 'Partner Account is invalid');
        }
        /* End of check account */

        if (
                !isset($AUTHENTICATE_KEY[$PartnerAccount])
                or ! isset($AUTHENTICATE_KEY[$PartnerAccount]['password'])
                or $AUTHENTICATE_KEY[$PartnerAccount]['password'] != $Password
        ) {
            return array('return' => 'Password is invalid');
        }
        /* End of check authenticate */
        
        
        $params = array(
            'purchase_date'  => $ValueDate,
        );

        $QWarrantyCard  = new Application_Model_WarrantyCard();
        $data           = $QWarrantyCard->getData($params);
        return array('return' => $data);

    } catch (Exception $e) {
        return array('return' => $e->getMessage() );
        
    }
}

function getInfoImei($Imei)
{

    try {

		
		 if (!$Imei) {
            return array('return' => 'The parameter is invalid');
        }

        $QImei = new Application_Model_Imei();

        $result = '';

        $num = preg_replace("/[^0-9]/", '', $Imei);

        if(
		 strlen($imei)!= 15){
			 $result = $QImei->getImeiInfo($Imei);

        }else{
            $result = 'Sai định dạng Imei';
			
        }	
		return array('return' =>  $result );

    } catch (Exception $e) {
        return array('return' => $e->getMessage() );
    }

 
}


function wsGetStaffCs($system,$time,$authentication,$email){

    $rs = array('code' => 0);
    try {
        if (!$system
            or !$authentication){

            $code = 2;
            throw new Exception('The parameter is invalid');

        }

        // check authenticate
        $AUTHENTICATE_KEY = unserialize(AUTHENTICATE_KEY);
        if (
            !isset($AUTHENTICATE_KEY[strtoupper($system)])
            or md5($time.'0pp0'.$AUTHENTICATE_KEY[strtoupper($system)]) != $authentication
        ) {
            $code = 3;
            throw new Exception('Authentication is invalid');
        }
        
        
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto("email = ?",$email);
        $staff = $QStaff->fetchRow($where)->toArray();
        
        return array('result' => $staff);
        
    } catch (Exception $e){
        $rs['code'] = isset($code) ? ($code and $message = $e->getMessage()) : (1 and $message = 'Unknown Error');
        $rs['message'] = $message;
    }
    return $rs;
}

