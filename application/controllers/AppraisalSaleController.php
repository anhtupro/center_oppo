<?php

class AppraisalSaleController extends My_Controller_Action
{
    const TITLE_MANAGER_ID = 462;
    const TITLE_RSM_ID = 308;
    const TITLE_ASM_ID = 179;
    const TITLE_ASM_STANDBY_ID = 181;
    const TITLE_SALE_LEADER_ID = 190;
    const TITLE_SALE_ID = 183;
    const TITLE_PG_ID = 182;

    const TITLE_MANAGER = 'MANAGER';
    const TITLE_RSM = 'RSM';
    const TITLE_ASM = 'ASM';
    const TITLE_ASM_STANDBY = 'ASM';
    const TITLE_SALE_LEADER = 'LEADER';
    const TITLE_SALE = 'SALE';
    const TITLE_PG = 'PG';

    #region AppraisalPlan
    public function planAction()
    {
        $QAppraisalOfficePlan = new Application_Model_AppraisalSalePlan();
        $plans = $QAppraisalOfficePlan->fetchAll('asp_is_deleted = 0', 'asp_id ASC')->toArray();
        for ($i = 0; $i < count($plans); $i++) {
            $plans[$i]['asp_from'] = date('d/m/Y', strtotime($plans[$i]['asp_from']));
            $plans[$i]['asp_to'] = date('d/m/Y', strtotime($plans[$i]['asp_to']));
        }
        $plans = json_encode($plans);

        $QDSurvey = new Application_Model_DynamicSurvey();
        $surveys = $QDSurvey->getActiveSurveyByRef('appraisal_sale');
        foreach ($surveys as &$survey) {
            $survey['ds_name'] = trim($survey['ds_name']);
        }
        $surveys = json_encode($surveys);
        $this->view->data = [
            'plans' => $plans,
            'surveys' => $surveys
        ];
    }

    /**
     * Save plan and clone all Skill, Level and Question to Cache
     */
    public function savePlanAction()
    {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            /** @var Zend_Controller_Request_Http $request */
            $request = $this->getRequest();
            if (!$request->isPost()) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Wrong method submitted!'
                ]);
                return;
            }

            $userId = Zend_Auth::getInstance()->getStorage()->read()->id;

            $data = $this->getRequest()->getParam('data');
            $data = json_decode($data, true);
            $data['asp_from'] = implode('-', array_reverse(explode('/', $data['asp_from'])));
            $data['asp_to'] = implode('-', array_reverse(explode('/', $data['asp_to'])));
            $data['asp_updated_at'] = date('Y/m/d h:i:s');
            $data['asp_updated_by'] = $userId;



            $QPlan = new Application_Model_AppraisalSalePlan();
            if (isset($data['asp_id']) && $data['asp_id'] != 0) {
                $plan = $QPlan->fetchRow($QPlan->getAdapter()->quoteInto('asp_id = ?', $data['asp_id']));
                if($plan) {
                    $plan = $plan->toArray();
                    if($data['asp_status'] == 1 && $plan['asp_status'] == 0 && $plan['asp_init'] == 0) {
                        $QToDo = new Application_Model_AppraisalSaleToDo();
                        $QToDo->initPlan($data['asp_id']);
                        $data['asp_init'] = 1;
                    }
                    $QPlan->update($data, $QPlan->getAdapter()->quoteInto('asp_id = ?', $data['asp_id']));
                    echo json_encode([
                        'status' => 0,
                        'data' => 0
                    ]);
                    return;
                }
                echo json_encode([
                    'status' => 1,
                    'message' => 'Kế hoạch không tồn tại, vui lòng tải lại trang!'
                ]);
                return;
            } else {
                unset($data['aop_id']);
                $data['asp_created_at'] = date('Y/m/d h:i:s');
                $data['asp_created_by'] = $userId;
                $result = $QPlan->insert($data);
                echo json_encode([
                    'status' => 0,
                    'data' => intval($result)
                ]);
                return;
            }
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage() . $ex->getTraceAsString()
            ]);
            return;
        }
    }

    public function deletePlanAction()
    {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            /** @var Zend_Controller_Request_Http $request */
            $request = $this->getRequest();
            if (!$request->isPost()) {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Wrong method submitted!'
                ]);
                return;
            }
            $asp_id = $this->getRequest()->getParam('asp_id');
            $QPlan = new Application_Model_AppraisalSalePlan();
            if (isset($asp_id)) {
                $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
                $data = [
                    'asp_is_deleted' => true,
                    'asp_updated_at' => date('Y/m/d h:i:s'),
                    'asp_updated_by' => $userId
                ];
                $QPlan->update($data, $QPlan->getAdapter()->quoteInto('asp_id = ?', $asp_id));
                echo json_encode([
                    'status' => 0,
                    'data' => 0
                ]);
                return;
            } else {
                echo json_encode([
                    'status' => 1,
                    'message' => 'Wrong plan ID submitted!'
                ]);
                return;
            }
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
            return;
        }
    }
    #endregion AppraisalPlan

    /**
     * @throws Zend_Auth_Storage_Exception
     * @throws Zend_Exception
     */
    public function toDoSurveyAction()
    {
        $user = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $user->id;

        $QView = new Application_Model_AppraisalSaleView();
        $todo = $QView->getToDoSurvey($userId);
        
        $staffIds = [];
        foreach ($todo['superior'] as $item) {
            $staffIds[] = $item['id'];
        }
        foreach ($todo['subordinate'] as $item) {
            $staffIds[] = $item['id'];
        }
        foreach ($todo['self'] as $item) {
            $staffIds[] = $item['id'];
        }
        $QStaff = new Application_Model_Staff();

        $staffs = [];
        if(count($staffIds) > 0) {
            $staffs = $QStaff->fetchAll(
                $QStaff->select()
                    ->from($QStaff, ['id', 'code', 'email', 'firstname', 'lastname', 'title'])
                    ->where(sprintf('id in (%s) and off_date is null', implode(',', $staffIds)))
            )->toArray();
        }

        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getLastPlan();
        if(is_null($plan)) {
            $this->view->data = json_encode([]);
            return;
        }

        $QMember = new Application_Model_AppraisalSaleMember();
        $member = $QMember->getByPlanAndStaff($plan['asp_id'], $userId);
        $QToDo = new Application_Model_AppraisalSaleToDo();
        $userTitleId = $QToDo->getTitleById($userId, $plan['asp_id']);
        $userTitle = null;
        if($userTitleId == self::TITLE_PG_ID) {
            $userTitle = self::TITLE_PG;
        } elseif($userTitleId == self::TITLE_SALE_ID) {
            $userTitle = self::TITLE_SALE;
        } elseif($userTitleId == self::TITLE_SALE_LEADER_ID) {
            $userTitle = self::TITLE_SALE_LEADER;
        } elseif($userTitleId == self::TITLE_ASM_ID) {
            $userTitle = self::TITLE_SALE_LEADER;
        } elseif($userTitleId == self::TITLE_RSM_ID) {
            $userTitle = self::TITLE_RSM;
        } elseif($userTitleId == self::TITLE_MANAGER_ID) {
            $userTitle = self::TITLE_MANAGER;
        }
        $data = [
            'plan' => $plan,
            'staff_id' => $userId,
            'staff_title' => $userTitle,
            'staff_title_id' => $userTitleId,
            'to_do' => $todo,
            'member' => $member,
            'staffs' => $staffs
        ];
        $this->view->data = self::escapeJsonString(json_encode($data));
    } // Done

    public function getDetailSaleAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $planId = $this->getRequest()->getParam('asp_id');
        $memberId = json_decode($this->getRequest()->getParam('sub_member_id'));
        if(!$memberId) {
            $user = Zend_Auth::getInstance()->getStorage()->read();
            $memberId = $user->id;
        }
        try {
            $QView = new Application_Model_AppraisalSaleView();
            $data = $QView->getDetailAppraisal($planId, $memberId);
            echo json_encode([
                'status' => 0,
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage().$ex->getTraceAsString()
            ]);
        }
    } // Done

    /**
     * @throws Zend_Auth_Storage_Exception
     * @throws Zend_Exception
     */
    public function viewSaleAction()
    {
        $viewReport = $this->getRequest()->getParam('view_report', 0);
        if($viewReport == 1) {
            $this->_helper->layout->setLayout('layout_bi_empty');
        }
        $user = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $user->id;
        $view = self::getView($userId);
        if($view !== 'view-sale') {
            header('Location: /appraisal-sale/'.$view);
            return;
        }

        $QView = new Application_Model_AppraisalSaleView();
        $total = $QView->getTotalAppraisalSale(0, self::TITLE_SALE_ID);
        $personal = $QView->getTotalAppraisalSale($userId, self::TITLE_SALE_ID);

        for($i = 0; $i < count($total); $i ++) {
            $total[$i]['personal'] = floatval($personal[$i]['total']);
        }

        $this->view->data = json_encode($total);
        $this->view->staff_id = $userId;
        $this->view->view_report = $viewReport;
    }

    /**
     * @throws Zend_Auth_Storage_Exception
     * @throws Zend_Exception
     */
    public function viewSaleLeaderAction()
    {
        $viewReport = $this->getRequest()->getParam('view_report', 0);
        if($viewReport == 1) {
            $this->_helper->layout->setLayout('layout_bi_empty');
        }
        $user = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $user->id;
        $view = self::getView($userId);
        if($view !== 'view-sale-leader') {
            header('Location: /appraisal-sale/'.$view);
            return;
        }

        $QView = new Application_Model_AppraisalSaleView();
        $totalSaleLeader = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_LEADER_ID);
        $personal = $QView->getTotalAppraisalSale($userId, self::TITLE_SALE_LEADER_ID);
        $totalSale = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_ID);
        $saleMembers = $QView->getTotalAppraisalSale($userId, self::TITLE_SALE_ID);

        for($i = 0; $i < count($totalSaleLeader); $i ++) {
            $totalSaleLeader[$i]['personal'] = floatval($personal[$i]['total']);
        }
        for($i = 0; $i < count($totalSale); $i ++) {
            $totalSale[$i]['sub_sale'] = floatval($saleMembers[$i]['total']);
        }

        $this->view->totalSaleLeader = json_encode($totalSaleLeader); //data_saleleader
        $this->view->totalSale = json_encode($totalSale);
        $this->view->staff_id = $userId;
        $this->view->view_report = $viewReport;
    }

    /**
     * @throws Zend_Auth_Storage_Exception
     * @throws Zend_Exception
     */
    public function viewAsmAction()
    {
        $viewReport = $this->getRequest()->getParam('view_report', 0);
        if($viewReport == 1) {
            $this->_helper->layout->setLayout('layout_bi_empty');
        }
        $user = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $user->id;
        $view = self::getView($userId);
        if($view !== 'view-asm') {
            header('Location: /appraisal-sale/'.$view);
            return;
        }

        $QView = new Application_Model_AppraisalSaleView();
        $totalAsm = $QView->getTotalAppraisalByTitle(self::TITLE_ASM_ID);
        $personalAsm = $QView->getTotalAppraisalSale($userId, self::TITLE_ASM_ID);
        $totalSaleLeader = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_LEADER_ID);
        $saleLeaderMembers = $QView->getTotalAppraisalSale($userId, self::TITLE_SALE_LEADER_ID);
        $totalSale = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_ID);
        $saleMembers = $QView->getTotalAppraisalSale($userId, self::TITLE_SALE_ID);

        for($i = 0; $i < count($totalAsm); $i ++) {
            $totalAsm[$i]['personal'] = floatval($personalAsm[$i]['total']);
        }
        for($i = 0; $i < count($totalSaleLeader); $i ++) {
            $totalSaleLeader[$i]['sub_sale_leader'] = floatval($saleLeaderMembers[$i]['total']);
        }
        for($i = 0; $i < count($totalSale); $i ++) {
            $totalSale[$i]['sub_sale'] = floatval($saleMembers[$i]['total']);
        }

        $this->view->totalAsm = json_encode($totalAsm);
        $this->view->totalSaleLeader = json_encode($totalSaleLeader);
        $this->view->totalSale = json_encode($totalSale);
        $this->view->staff_id = json_encode($userId);
        $this->view->view_report = $viewReport;
    } // Done

    /**
     * @throws Zend_Auth_Storage_Exception
     * @throws Zend_Exception
     */
    public function viewRsmAction()
    {
        $viewReport = $this->getRequest()->getParam('view_report', 0);
        if($viewReport == 1) {
            $this->_helper->layout->setLayout('layout_bi_empty');
        }
        $user = Zend_Auth::getInstance()->getStorage()->read();
        $userId = $user->id;
        $view = self::getView($userId);
        if($view !== 'view-rsm') {
            header('Location: /appraisal-sale/'.$view);
            return;
        }

        $QView = new Application_Model_AppraisalSaleView();
        $allRsm = $QView->getTotalAppraisalByTitle(self::TITLE_RSM_ID);
        $allAsm = $QView->getTotalAppraisalByTitle(self::TITLE_ASM_ID);
        $allLeader = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_LEADER_ID);
        $allSale = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_ID);

        $personal = $QView->getResultByLevel($userId, self::TITLE_RSM_ID);
        $personalAsm = $QView->getResultByLevel($userId, self::TITLE_ASM_ID);
        $personalLeader = $QView->getResultByLevel($userId, self::TITLE_SALE_LEADER_ID);
        $personalSale = $QView->getResultByLevel($userId, self::TITLE_SALE_ID);

        $data = [
            'staff_id' => $userId,
            'all_rsm' => $allRsm,
            'all_asm' => $allAsm,
            'all_leader' => $allLeader,
            'all_sale' => $allSale,
            'detail_rsm' => $personal,
            'detail_asm' => $personalAsm,
            'detail_leader' => $personalLeader,
            'detail_sale' => $personalSale,
        ];
        $this->view->data = json_encode($data);
        $this->view->view_report = $viewReport;
    } // Done

    public function viewSaleManagerAction()
    {
        $QView = new Application_Model_AppraisalSaleView();
        $QPlan = new Application_Model_AppraisalSalePlan();
        $allRsm = $QView->getTotalAppraisalByTitle(self::TITLE_RSM_ID);
        $allAsm = $QView->getTotalAppraisalByTitle(self::TITLE_ASM_ID);
        $allLeader = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_LEADER_ID);
        $allSale = $QView->getTotalAppraisalByTitle(self::TITLE_SALE_ID);
        $plans = $QPlan->getLastTopTenPlans();
        $data = [
            'all_rsm' => $allRsm,
            'all_asm' => $allAsm,
            'all_leader' => $allLeader,
            'all_sale' => $allSale,
            'plans' => $plans,
        ];
        $this->view->data = json_encode($data);
    } // Done

    /**
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function viewResultAction()
    {
        $fromName = $this->getRequest()->getParam('from_name', '');
        $toName = $this->getRequest()->getParam('to_name', '');
        $fromTitle = $this->getRequest()->getParam('from_title', 0);
        $toTitle = $this->getRequest()->getParam('to_title', 0);
        $planId = $this->getRequest()->getParam('plan_id', 0);
        $export = $this->getRequest()->getParam('export', 0);
        $page = $this->getRequest()->getParam('page', 1);

        $data = [
            'from_name' => $fromName,
            'to_name' => $toName,
            'from_title' => $fromTitle,
            'to_title' => $toTitle,
            'plan_id' => $planId,
        ];

        if($export > 0) {
            My_Report_Trainer::appraisalSaleResult($data, $export);
        }

        $QView = new Application_Model_AppraisalSaleView();
        $total = 0;
        $result = $QView->getResult($data, $total, $page, $export);
        $this->view->url = HOST . 'appraisal-sale/view-result' . ( $data ? '?' . http_build_query($data) . '&' : '?' );
        $this->view->data = json_encode($result);
        $this->view->params = $data;
        $this->view->export = $export;
        $this->view->page = $page;
        $this->view->total = $total;
    }

    /**
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultDetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $planId = $this->getRequest()->getParam('plan_id');
        $fromStaff = $this->getRequest()->getParam('from_staff');
        $toStaff = $this->getRequest()->getParam('to_staff');

        $QView = new Application_Model_AppraisalSaleView();
        $result = $QView->getResultDetail($planId, $fromStaff, $toStaff);

        echo json_encode([
            'status' => 0,
            'data' => $result
        ]);
    }

    public function getSubMemberAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $user = Zend_Auth::getInstance()->getStorage()->read();
            $userId = $user->id;
            $planId = $this->getRequest()->getParam('asp_id');

            $QAppraisalSaleMember = new Application_Model_AppraisalSaleMember();
            $subMembers = $QAppraisalSaleMember->getSubMember($planId, $userId);

            echo json_encode([
                'status' => 0,
                'data' => $subMembers
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function getListSaleLeaderByAsmAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $user = Zend_Auth::getInstance()->getStorage()->read();
            $userId = $user->id;
            $planId = $this->getRequest()->getParam('asp_id');
            $QView = new Application_Model_AppraisalSaleView();
            echo json_encode([
                'status' => 0,
                'data' => $QView->getLeaderByAsm($userId, $planId)
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                'status' => 1,
                'data' => $e->getMessage()
            ]);
        }
    } // Done

    public function getListSaleByAsmAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $user = Zend_Auth::getInstance()->getStorage()->read();
            $userId = $user->id;
            $planId = $this->getRequest()->getParam('asp_id');
            $QView = new Application_Model_AppraisalSaleView();
            echo json_encode([
                'status' => 0,
                'data' => $QView->getSaleByAsm($userId, $planId)
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                'status' => 1,
                'data' => $e->getMessage()
            ]);
        }
    } // Done

    /**
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getRegionRsmAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $planId = json_decode($this->getRequest()->getParam('asp_id'));
        $QView = new Application_Model_AppraisalSaleView();
        $data = $QView->getResultRsmRegion($planId);
        echo json_encode([
            'status' => 0,
            'data' => $data
        ]);
    }

    /**
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getAreaAsmAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $planId = json_decode($this->getRequest()->getParam('asp_id'));
        $regionId = json_decode($this->getRequest()->getParam('region_id'));
        $QView = new Application_Model_AppraisalSaleView();
        $data = $QView->getResultAsmArea($planId, $regionId);
        echo json_encode([
            'status' => 0,
            'data' => $data
        ]);
    }

    public function listCapacityAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $QDynamicSurvey = new Application_Model_DynamicSurvey();
        $page = $this->getRequest()->getParam('page', 1);
        $params = array(
//            'staff_id' => $userStorage->id,
            'ref' => 'appraisal_sale'
        );
        $limit = 10;
        $list = $QDynamicSurvey->fetchPagination($page, $limit, $total, $params);
        $this->view->list = $list;
        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->offset = $limit * ($page - 1);
        $this->view->url = HOST . 'appraisal_sale/list-capacity' . '?';
    } // Done

    public function capacityAction()
    {
        $planId = $this->getRequest()->getParam('plan_id');
        $type = $this->getRequest()->getParam('type');
        switch ($type) {
            case 'rsm':
                $defaultType = 0;
                break;
            case 'leader':
                $defaultType = 1;
                break;
            case 'pg':
                $defaultType = 2;
                break;
            default:
                $defaultType = 0;
        }
        $QSkill = new Application_Model_AppraisalSaleSkill();
        $skills = $QSkill->fetchAll([
            $QSkill->getAdapter()->quoteInto('ass_is_deleted = ?', 0),
            $QSkill->getAdapter()->quoteInto('ass_type = ?', $defaultType),
        ], 'ass_id asc')->toArray();
        $this->view->skills = json_encode($skills);
        $this->view->plan_id = $planId;
        $this->view->type = $type;
    } // Done
    
    public function saveSkillAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $skillId = $this->getRequest()->getParam('ass_id');
        $skillName = $this->getRequest()->getParam('ass_name');
        $skillInputType = $this->getRequest()->getParam('ass_input_type');
        $skillType = $this->getRequest()->getParam('ass_type');
        $skillStatus = $this->getRequest()->getParam('ass_status');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QSkill = new Application_Model_AppraisalSaleSkill();
            $skill = [
                'ass_name' => self::escapeJsonString($skillName),
                'ass_input_type' => $skillInputType,
                'ass_type' => $skillType,
                'ass_status' => $skillStatus,
                'ass_updated_at' => date('Y-m-d H:i:s'),
                'ass_updated_by' => $userStorage->id,
            ];
            if($skillId == 0) {
                $skill['ass_created_at'] = date('Y-m-d H:i:s');
                $skill['ass_created_by'] = $userStorage->id;
                $skillId = $QSkill->insert($skill);
            } else {
                $QSkill->update($skill, $QSkill->getAdapter()->quoteInto('ass_id = ?', $skillId));
            }
            echo json_encode([
                'status' => 0,
                'data' => $skillId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    } // Done

    public function deleteSkillAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $skillId = $this->getRequest()->getParam('ass_id');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QSkill = new Application_Model_AppraisalSaleSkill();
            $skill = [
                'ass_is_deleted' => 1,
                'ass_updated_at' => date('Y-m-d H:i:s'),
                'ass_updated_by' => $userStorage->id,
            ];
            $QSkill->update($skill, $QSkill->getAdapter()->quoteInto('ass_id = ?', $skillId));
            $QLevel = new Application_Model_AppraisalSaleLevel();
            $level = [
                'asl_is_deleted' => 1,
                'asl_updated_at' => date('Y-m-d H:i:s'),
                'asl_updated_by' => $userStorage->id,
            ];
            $QLevel->update($level, $QLevel->getAdapter()->quoteInto('fk_ass = ?', $skillId));

            echo json_encode([
                'status' => 0,
                'data' => $skillId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    } // Done

    public function getLevelAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $skillId = $this->getRequest()->getParam('ass_id');
        $QLevel = new Application_Model_AppraisalSaleLevel();
        $levels = $QLevel->fetchAll([
            $QLevel->getAdapter()->quoteInto('fk_ass = ?', $skillId),
            $QLevel->getAdapter()->quoteInto('asl_is_deleted = ?', 0),
        ], 'asl_point asc')->toArray();
        echo json_encode([
            'status' => 0,
            'data' => $levels
        ]);
    } // Done

    public function saveLevelAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $levelId = $this->getRequest()->getParam('asl_id');
        $skillId = $this->getRequest()->getParam('fk_ass');
        $levelName = $this->getRequest()->getParam('asl_name');
        $levelStatus = $this->getRequest()->getParam('asl_status');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QLevel = new Application_Model_AppraisalSaleLevel();
            $level = [
                'asl_name' => $levelName,
                'fk_ass' => $skillId,
                'asl_status' => $levelStatus,
                'asl_updated_at' => date('Y-m-d H:i:s'),
                'asl_updated_by' => $userStorage->id,
            ];
            if($levelId == 0) {
                $level['asl_created_at'] = date('Y-m-d H:i:s');
                $level['asl_created_by'] = $userStorage->id;
                $levelId = $QLevel->insert($level);
            } else {
                $QLevel->update($level, $QLevel->getAdapter()->quoteInto('asl_id = ?', $levelId));
            }
            $QLevel->reindexPoint($skillId);

            echo json_encode([
                'status' => 0,
                'data' => $levelId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    } // Done

    public function deleteLevelAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $levelId = $this->getRequest()->getParam('asl_id');
        try {
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QLevel = new Application_Model_AppraisalSaleLevel();
            $level = $QLevel->fetchRow($QLevel->getAdapter()->quoteInto('asl_id = ?', $levelId));
            if(!$level) {
                echo json_encode([
                    'status' => 1,
                    'data' => 'Không tìm thấy mục tiêu này! Vui lòng thử lại.'
                ]);
                return;
            }
            $skillId = $level['fk_ass'];
            $level = [
                'asl_is_deleted' => 1,
                'asl_updated_at' => date('Y-m-d H:i:s'),
                'asl_updated_by' => $userStorage->id,
            ];
            $QLevel->update($level, $QLevel->getAdapter()->quoteInto('asl_id = ?', $levelId));
            $QLevel->reindexPoint($skillId);
            echo json_encode([
                'status' => 0,
                'data' => $levelId
            ]);
        } catch (\Exception $ex) {
            echo json_encode([
                'status' => 1,
                'data' => $ex->getMessage()
            ]);
        }
    } // Done

    public function createSurveyAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $name = $this->getRequest()->getParam('survey_name');
        $title = $this->getRequest()->getParam('survey_title');
        $selectedSkill = $this->getRequest()->getParam('selected_skill');
        $planId = $this->getRequest()->getParam('plan_id');
        $type = $this->getRequest()->getParam('type');

        $db = null;
        try {
            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $QDSurvey = new Application_Model_DynamicSurvey();
            $dsId = $QDSurvey->insert([
                'ds_name' => $name,
                'ds_title' => $title,
                'ds_ref' => 'appraisal_sale',
                'ds_status' => 1,
                'ds_created_at' => date('Y-m-d H:i:s'),
                'ds_created_by' => $userStorage->id
            ]);
            $QSkill = new Application_Model_AppraisalSaleSkill();
            $QLevel = new Application_Model_AppraisalSaleLevel();
            $QDSQuestion = new Application_Model_DynamicSurveyQuestion();
            $QDSAnswer = new Application_Model_DynamicSurveyAnswer();
            $skills = $QSkill->fetchAll([
                $QSkill->getAdapter()->quoteInto('ass_status = ? and ass_is_deleted = 0', 0),
                $QSkill->getAdapter()->quoteInto('ass_id IN (?)', $selectedSkill),
            ]);
            $templateRange = json_decode('{"title":"Question","type":"checkbox","id":"range","defaultValue":1,"display_label":true,"help":"","required":1,"checked_choices":["1"],"current_field":false}', true);
            $templateRadio = json_decode('{"title":"Question","type":"radio","id":"radio","inline":0,"display_label":true,"help":"","bold":"true"}', true);
            $templateTitleBold = json_decode('{"title":"Title","type":"sub_description","display_label":true,"help":"","required":true,"bold":"true","prepend": "no","append": "no"}', true);
            $templateTitle = json_decode('{"title":"Title","type":"sub_description","display_label":true,"help":"","required":true,"bold":false,"prepend": "no","append": "no"}', true);
            $templateTextarea = json_decode('{"title":"Textarea","type":"textarea","id":"textarea","cols":40,"rows":5,"display_label":true,"help":"","bold":"true"}', true);
            $templateSelect = json_decode('{"title":"Question","type":"select","id":"select","display_label":true,"help":"1: Rất thấp - 5: Rất cao","required":"required","multiple":false,"bold":"true"}', true);
            foreach ($skills as $skill) {
                switch ($skill['ass_input_type']) {
                    case 'range':
                        $template = $templateRange;
                        break;
                    case 'radio':
                        $template = $templateRadio;
                        break;
                    case 'title-bold':
                        $template = $templateTitleBold;
                        break;
                    case 'title':
                        $template = $templateTitle;
                        break;
                    case 'textarea':
                        $template = $templateTextarea;
                        break;
                    case 'select':
                        $template = $templateSelect;
                        break;
                }
                $template['title'] = $skill['ass_name'];
                $dsqId = $QDSQuestion->insert([
                    'fk_ds' => $dsId,
                    'dsq_title' => $skill['ass_name'],
                    'dsq_ref' => $skill['ass_id'],
                    'dsq_type' => $skill['ass_input_type'],
                    'dsq_data' => json_encode($template),
                    'dsq_created_at' => date('Y-m-d H:i:s'),
                    'dsq_created_by' => $userStorage->id
                ]);
                $levels = $QLevel->fetchAll($QLevel->getAdapter()->quoteInto('fk_ass = ? and asl_status = 0 and asl_is_deleted = 0', $skill['ass_id']), 'asl_point');
                foreach ($levels as $index => $level) {
                    $dsaId = $QDSAnswer->insert([
                        'fk_dsq' => $dsqId,
                        'dsa_ref' => $level['asl_id'],
                        'dsa_title' => $level['asl_name'],
                        'dsa_value' => $level['asl_point'],
                        'dsa_sort_id' => $index + 1,
                        'dsa_created_at' => date('Y-m-d H:i:s'),
                        'dsa_created_by' => $userStorage->id,
                    ]);
                }
            }
            if($planId != 0) {
                $QPlan = new Application_Model_AppraisalSalePlan();
                $plan = $QPlan->fetchRow($QPlan->getAdapter()->quoteInto('asp_id = ?', $planId));
                if($plan) {
                    $plan = $plan->toArray();
                    switch ($type) {
                        case 'rsm':
                            $plan['asp_survey_rsm_asm'] = $dsId;
                            break;
                        case 'leader':
                            $plan['asp_survey_sale_leader'] = $dsId;
                            break;
                        case 'pg':
                            $plan['asp_survey_pg'] = $dsId;
                            break;
                    }
                    $plan['asp_updated_at'] = date('Y-m-d H:i:s');
                    $plan['asp_updated_by'] = $userStorage->id;
                    $QPlan->update($plan, $QPlan->getAdapter()->quoteInto('asp_id = ?', $planId));
                }
            }
            $db->commit();
            echo json_encode([
                'status' => 0,
                'message' => 'Success',
            ]);
        } catch (Exception $ex) {
            $db->rollback();
            echo json_encode(array('status' => 1, 'message' => $ex->getMessage()));
        }
    } // Done

    protected function getView($userId)
    {
//        return $this->view->action_name;
        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getLastPlanData();
        if(is_null($plan)) {
            return '/';
        }

        $QToDo = new Application_Model_AppraisalSaleToDo();
        $userTitleId = $QToDo->getTitleById($userId, $plan['asp_id']);
        switch ($userTitleId) {
            case self::TITLE_SALE_ID:
                return 'view-sale';
            case self::TITLE_SALE_LEADER_ID:
                return 'view-sale-leader';
            case self::TITLE_ASM_ID:
                return 'view-asm';
            case self::TITLE_RSM_ID:
                return 'view-rsm';
            default:
                return '/';
        }
    }

    protected function escapeJsonString($value)
    {
        $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
        $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
        $result = str_replace($escapers, $replacements, $value);
        return $result;
    } // Done

    public function testAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $QRegion = new Application_Model_Region();
        $QArea = new Application_Model_Area();

        var_dump($QArea->getAreaByAsm(7377));
    }
}
