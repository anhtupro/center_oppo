<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();
$QSurveyForm = new Application_Model_SurveyForm();
$QSurveyDetail = new Application_Model_SurveyDetail();
$QSurveyQuestions = new Application_Model_SurveyQuestions();
$name = json_decode($this->getRequest()->getParam('name'));
$from_date = json_decode($this->getRequest()->getParam('from_date', NULL));
$to_date = json_decode($this->getRequest()->getParam('to_date', NULL));
$arrData = json_decode($this->getRequest()->getParam('arrData'));
$formData = json_decode($this->getRequest()->getParam('formData'));
$render = json_decode($this->getRequest()->getParam('render'));
$all_staff = json_decode($this->getRequest()->getParam('all_staff', 0));
$officer = json_decode($this->getRequest()->getParam('officer', 0));
$department_objects = json_decode($this->getRequest()->getParam('department_objects', array()));
$team_objects = json_decode($this->getRequest()->getParam('team_objects', array()));
$title_objects = json_decode($this->getRequest()->getParam('title_objects', array()));
$area_objects = json_decode($this->getRequest()->getParam('area_objects', array()));
$staffs = json_decode($this->getRequest()->getParam('pic', NULL));
$from = json_decode($this->getRequest()->getParam('from', null));
$to = json_decode($this->getRequest()->getParam('to', null));
$company_objects = json_decode($this->getRequest()->getParam('company_objects'));
$type = json_decode($this->getRequest()->getParam('type'));
$survey_type = json_decode($this->getRequest()->getParam('survey_type'));
$required = json_decode($this->getRequest()->getParam('required'));
$form_name = json_decode($this->getRequest()->getParam('form_name'));
$questions = json_decode($this->getRequest()->getParam('questions'), true);
$big_questions = json_decode($this->getRequest()->getParam('big_questions'), true);
$description = json_decode($this->getRequest()->getParam('description'));
$id = $this->getRequest()->getParam('id');
$take_picture = $this->getRequest()->getParam('take_picture');
$pass_point = $this->getRequest()->getParam('pass_point');
$fail_point = $this->getRequest()->getParam('fail_point');


$QFormBuilderQuestion = new Application_Model_FormBuilderQuestion();
$QFormBuilderQuestionDetail = new Application_Model_FormBuilderQuestionDetail();

$all_staff = intval($all_staff);
$officer = intval($officer);
$pop_up = intval($pop_up);

$staff_objects = array();

if ($staffs && strlen($staffs))
    $staff_objects = explode(',', $staffs);

$staff_objects = is_array($staff_objects) ? $staff_objects : array();
$company_objects = is_array($company_objects) ? $company_objects : array();

$db = Zend_Registry::get('db');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$db->beginTransaction();

try {

    if (isset($id) && $id) {  // edit mode

        $where1 = $QSurveyForm->getAdapter()->quoteInto('id = ?', $id);
        $survey_form = $QSurveyForm->fetchRow($where1);

        $where2 = $QSurveyQuestions->getAdapter()->quoteInto('survey_id = ?', $id);

        $data = array(
            'name' => $form_name,
            'from_date' => (empty($from_date)) ? null : DateTime::createFromFormat('d/m/Y H:i', $from_date)->format('Y-m-d H:i:00'),
            'to_date' => (empty($to_date)) ? null : DateTime::createFromFormat('d/m/Y H:i', $to_date)->format('Y-m-d H:i:00'),
            'created_by' => $userStorage->id,
            'created_at' => date('Y-m-d H:i:s'),
            'render' => $render,
            'description' => $description,
            'survey_type' => $survey_type,
            'required' => $required ? 1 : 0,
            'file_name' => $_FILES['fileToUpload']['name'] ? $_FILES['fileToUpload']['name'] : $survey_form['file_name'],
            'take_picture' => ( $take_picture == 'true' || $take_picture == 1) ? 1 : 0,
            'is_trainer_survey' => ( $take_picture == 'true' || $take_picture == 1) ? 1 : 0,
            'pass_point' => $pass_point ? $pass_point : Null,
            'fail_point' => $fail_point ? $fail_point : Null
        );


        $QSurveyForm->update($data, $where1);

        $QSurveyQuestions->delete($where2);

        foreach ($questions as $index => $question) {
            $QSurveyQuestions->insert([
                'question' => $question['question'],
                'survey_id' => $id,
                'option' => $question['option'],
                'type' => $question['type'],
                'group_name' => $question['group_name']
            ]);
        }


        // new version

        $QFormBuilderQuestion->delete(['survey_id = ?' => $id]);
        $QFormBuilderQuestionDetail->delete(['survey_id = ?' => $id]);

        foreach ($big_questions as $key => $value) {
            if ($value['is_staff_code_question'] == 'true' || $value['is_staff_code_question'] == 1) {
                $question_type = 1;
            } else {
                if ($value['is_trainer_summary_point'] == 'true' || $value['is_trainer_summary_point'] == 1) {
                    $question_type = 2;
                } else {
                    $question_type = 3;
                }
            }

            $is_trainer_summary_point = ($value['is_trainer_summary_point'] == 'true' || $value['is_trainer_summary_point'] == 1) ? 1 : Null;

            $form_builder_question_id = $QFormBuilderQuestion->insert([
                'survey_id' => $id,
                'name' => TRIM($value['question']),
                'question_index' => $key + 1,
                'question_type' => $question_type,
                'type' => $value['type']
            ]);

            if ($value['option']) {
                foreach ($value['option'] as $option) {
                    $QFormBuilderQuestionDetail->insert([
                        'form_builder_question_id' => $form_builder_question_id,
                        'option_name' => $option,
                        'survey_id' => $id
                    ]);
                }
            }
        }


    } else {  // create mode
        $data = array(
            'name' => $form_name,
            'description' => $description,
            'survey_type' => $survey_type,
            'required' => $required ? 1 : 0,
            'from_date' => (empty($from_date)) ? null : DateTime::createFromFormat('d/m/Y H:i', $from_date)->format('Y-m-d H:i:00'),
            'to_date' => (empty($to_date)) ? null : DateTime::createFromFormat('d/m/Y H:i', $to_date)->format('Y-m-d H:i:00'),
            'created_by' => $userStorage->id,
            'created_at' => date('Y-m-d H:i:s'),
            'render' => $render,
            'file_name' => $_FILES['fileToUpload']['name'],
            'take_picture' =>  ( $take_picture == 'true' || $take_picture == 1) ? 1 : 0,
            'is_trainer_survey' => ( $take_picture == 'true' || $take_picture == 1) ? 1 : 0,
            'pass_point' => $pass_point ? $pass_point : Null,
            'fail_point' => $fail_point ? $fail_point : Null
        );

        $id = $QSurveyForm->insert($data);


        foreach ($questions as $index => $question) {
            $QSurveyQuestions->insert([
                'question' => $question['question'],
                'survey_id' => $id,
                'option' => $question['option'],
                'type' => $question['type'],
                'group_name' => $question['group_name']
            ]);
        }


        // new version

        foreach ($big_questions as $key => $value) {
            if ($value['is_staff_code_question'] == 'true' || $value['is_staff_code_question'] == 1) {
                $question_type = 1;
            } else {
                if ($value['is_trainer_summary_point'] == 'true' || $value['is_trainer_summary_point'] == 1) {
                    $question_type = 2;
                } else {
                    $question_type = 3;
                }
            }

            $is_trainer_summary_point = ($value['is_trainer_summary_point'] == 'true' || $value['is_trainer_summary_point'] == 1) ? 1 : Null;

            $form_builder_question_id = $QFormBuilderQuestion->insert([
                'survey_id' => $id,
                'name' => TRIM($value['question']),
                'question_index' => $key + 1,
                'question_type' => $question_type,
                'type' => $value['type']
            ]);

            if ($value['option']) {
                foreach ($value['option'] as $option) {
                    $QFormBuilderQuestionDetail->insert([
                       'form_builder_question_id' => $form_builder_question_id,
                        'option_name' => $option,
                        'survey_id' => $id
                    ]);
                }
            }
        }
    }







    $params['from'] = $from;
    $params['to'] = $to;
    $params['all_staff'] = $all_staff;
    $params['department_objects'] = $department_objects;
    $params['team_objects'] = $team_objects;
    $params['title_objects'] = $title_objects;
    $params['area_objects'] = $area_objects;
    $params['company_objects'] = $company_objects;
    $params['staff'] = $staff_objects;
    $params['id'] = $id;
    $params['type'] = $type;
    $params['survey_id'] = $id;

//    $this->addSurveyAccess($params);
    $db->commit();
    echo json_encode(array(
        'code' => 1,
        'message' => 'Success',
    ));
    exit;
} catch (Exception $ex) {
    $db->rollback();
    echo json_encode(array('code' => -1, 'message' => $ex->getMessage()));
}
            
    
    