<?php

$data = json_decode($this->getRequest()->getParam('data'), true);

$survey_id = json_decode($this->getRequest()->getParam('survey_id'));
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;

$QSurveyCollect = new Application_Model_SurveyCollect();
$QSurveyAccess = new Application_Model_SurveyAccess();
$QSurveyHistory = new Application_Model_SurveyHistory();
$QSurveyFile = new Application_Model_SurveyFile();

$survey_history_id = $QSurveyHistory->insert([
   'staff_id' => $staff_id,
   'survey_id' => $survey_id,
    'created_at' => date('Y-m-d H:i:s')
]);

$times = $QSurveyCollect->countTimesSubmit($staff_id, $survey_id); // count how many times user has submited the form

$db = Zend_Registry::get('db');
$db->beginTransaction();

foreach ($data as $question => $answers) {
    foreach ($answers as $option => $answer) {
        if (strpos($answer, '.zip') || strpos($answer, '.xlsx') || strpos($answer, '.jpg') || strpos($answer, '.png')) { // check if answer is a file
            $answer = basename($answer);
        }
        $QSurveyCollect->insert([
            'staff_id' => $userStorage->id,
            'question_index' => $question,
            'answer' => $answer,
            'checkbox_option' => is_string($option) ? $option : null,
            'survey_id' => $survey_id,
            'created_at' => date("Y-m-d H:i:s"),
            'time' => $times + 1,
            'survey_history_id' => $survey_history_id
        ]);
    }
}

// update status in survey access
$where = array(
    'survey_id = ?' => $survey_id,
    'staff_id = ?' => $staff_id
);
$QSurveyAccess->update(array('status' => 1), $where);


///////////////////
//save photo
$upload = new Zend_File_Transfer();
$images = $upload->getFileInfo();

//test

$upload->addValidator('Extension', false, 'jpg,jpeg,png,pdf');
$upload->addValidator('Size', false, array('max' => '10MB'));
$upload->addValidator('ExcludeExtension', false, 'php,sh');
$upload->addValidator('Count', false, 5);

$upload_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'survey' . DIRECTORY_SEPARATOR . 'trainer' . DIRECTORY_SEPARATOR . $survey_history_id. DIRECTORY_SEPARATOR;

if (!is_dir($upload_dir)) {
    mkdir($upload_dir, 0755, true);
}



foreach ($images as $image_type => $image_infor) {
    if (!$image_infor['name']) { // if there is no file is uploaded
        continue;
    }
    $extension = pathinfo($image_infor['name'], PATHINFO_EXTENSION);
    $new_name = $upload_dir . md5(uniqid('', true)) . '.' . $extension;
    $upload->addFilter('Rename', array('target' => $new_name)); // Set a new destination path

    // check file is valid or not
    if (!$upload->isValid($image_type)) {
        $array_error = $upload->getMessages();

        if (isset($array_error['fileSizeTooBig'])) {
            echo json_encode([
                'status' => 1,
                'message' => 'File có dung lượng vượt quá 5MB'
            ]);
            return;
        }
        if (isset($array_error['fileExtensionFalse'])) {
            echo json_encode([
                'status' => 1,
                'message' => 'File không đúng định dạng'
            ]);
            return;
        }
        if ($array_error['fileCountTooMany']) {
            echo json_encode([
                'status' => 1,
                'message' => 'Bạn chỉ được upload tối đa 5 file'
            ]);
            return;
        }
    }



    $result = $upload->receive($image_type); // save image

    //Resize Image
    $image = new My_SimpleImage();
    $image->load($new_name);
    $image->resizeToWidth(800);
    $image->save($new_name);
    //END Resize

    $list_photo [] = basename($new_name); //
    $list_url [] = 'photo/survey/trainer' . $survey_history_id . '/' . basename($new_name); // url
}

if ($list_url) {

    foreach ($list_url as $url) {
        $QSurveyFile->insert([
            'url' => $url,
            'survey_history_id' => $survey_history_id,
            'type' => 1
        ]);
    }
}
// end save photo
$db->commit();
//////////////////

// return times submited after call ajax
$times = $times + 1;
print($times);
die;