<?php 

$this->_helper->layout->disableLayout(); 

$survey_id = $this->getRequest()->getParam('survey_id'); 
$from_staff = $this->getRequest()->getParam('from_staff');
$to_staff = $this->getRequest()->getParam('to_staff');
$type_appraisal = $this->getRequest()->getParam('type_appraisal');
$survey_type = $this->getRequest()->getParam('survey_type');

$from_staff_title = $this->getRequest()->getParam('from_staff_title');
$to_staff_title = $this->getRequest()->getParam('to_staff_title');
$plan_id = $this->getRequest()->getParam('plan_id');
$QSurveyForm = new Application_Model_SurveyForm();
$QSurveyCollect = new Application_Model_SurveyCollect();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;

$survey_form = $QSurveyForm->fetchRow(['id = ?' => $survey_id]);


if ($survey_form['status'] == 2) {
    echo 'Khảo sát đã kết thúc!';
    $this->_redirect(HOST);
}

if($survey_id) {
		$QSurveyAccess = new Application_Model_SurveyAccess();      
        $where_access = array();
		$where_access[] = $QSurveyAccess->getAdapter()->quoteInto('survey_id = ?', $survey_id);   
		$where_access[] = $QSurveyAccess->getAdapter()->quoteInto('staff_id = ?', $staff_id);   
		$data_row = $QSurveyAccess->fetchRow($where_access);

		 if(empty($data_row)){
			$this->_redirect(HOST);
		 }
		 
	$where = $QSurveyForm->getAdapter()->quoteInto('id = ?', $survey_id);
	$survey_form = $QSurveyForm->fetchRow($where); 
	$type = $survey_form['survey_type']; // submit 1 or many times
	$status = $survey_form['status'];
	$from_date = $survey_form['from_date'];
	$to_date = $survey_form['to_date'];
	$now = date("Y-m-d H:i:s");

	if ($status == 1 || ($now >= $from_date && $now <= $to_date) ) { // survey is running
		
		$submitted_users = $QSurveyCollect->getSubmittedUsers($survey_id); //  lấy user đã submit 
		
		if ( $type == 0 && in_array($staff_id, $submitted_users)) { // submit 1 time
			 // $this->_helper->viewRenderer->setRender('form-builder-end');
			 $this->_redirect(HOST . 'survey/form-builder-end');
		
		}

		$form_file = $survey_form['file_name'];
		$form_html = $survey_form->render;
		$form_name = $survey_form['name'];
		$description = $survey_form['description'];
		$mode = 'fill';

		$this->view->form_html = $form_html;
		$this->view->mode = $mode;
		$this->view->survey_id = $survey_id;
		$this->view->form_file = $form_file;
		$this->view->form_name = $form_name;
		$this->view->description = $description;
		$this->view->type = $type;
        $this->view->from_staff = $from_staff;
        $this->view->to_staff = $to_staff;
        $this->view->type_appraisal = $type_appraisal;
        $this->view->survey_type = $survey_type;
        $this->view->from_staff_title = $from_staff_title;
        $this->view->to_staff_title = $to_staff_title;
        $this->view->plan_id = $plan_id;
        $this->view->survey_form = $survey_form;
	   }
	else { // survey has not run yet or has finished
		$this->_redirect(HOST);
	}
	
} 