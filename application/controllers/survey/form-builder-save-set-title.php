<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$flashMessenger = $this->_helper->flashMessenger;

$staff_titles = $this->getRequest()->getParam('staff_titles');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');
$survey_id = $this->getRequest()->getParam('survey_id');

$QSurveyNewObject = new Application_Model_SurveyNewObject();
$QSurvey = new Application_Model_SurveyForm();
$QStaff = new Application_Model_Staff();
$QSurveyAccess = new Application_Model_SurveyAccess();
$QTitle = new Application_Model_Title();
$where_survey_object = $QSurveyNewObject->getAdapter()->quoteInto('survey_id = ?', $survey_id);
$where_survey = $QSurvey->getAdapter()->quoteInto('id = ?', $survey_id);


$QSurvey->update([
    'from_date' => date("Y-m-d", strtotime($from_date)),
    'to_date' => $to_date ? date("Y-m-d", strtotime($to_date)) : NULL
], $where_survey);


$QSurveyNewObject->delete($where_survey_object);
$QSurveyAccess->delete(['survey_id = ?' => $survey_id]);

foreach ($staff_titles as $title) {
    $list_staff = $QTitle->getStaffByTtitle($title);

    foreach ($list_staff as $staff) {
        $QSurveyNewObject->insert([
            'survey_id' => $survey_id,
            'object_id' => $staff, // is staff_id
            'type' => 1
        ]);

        $QSurveyAccess->insert([
            'staff_id' => $staff,
            'survey_id' => $survey_id,
            'status' => 0
        ]);
    }
}

$flashMessenger->setNamespace('success')->addMessage('Successfully');

$this->_redirect(HOST . 'survey/form-builder-list');

