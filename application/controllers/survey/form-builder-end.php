<?php 
	$this->_helper->layout->disableLayout();

	$type = $this->getRequest()->getParam('type');
	$survey_id = $this->getRequest()->getParam('survey_id');

	$userStorage = Zend_Auth::getInstance()->getStorage()->read();
	$staff_id =  $userStorage->id;

	$QSurveyCollect = new Application_Model_SurveyCollect();

    if ($survey_id) {// submit nhiêu lần
        $times = $QSurveyCollect->countTimesSubmit($staff_id, $survey_id); // count how many times user has submited the form  
        $this->view->times = $times;
        $this->view->survey_id = $survey_id;
    }
	        

	 $this->view->type = $type;