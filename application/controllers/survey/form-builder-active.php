<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

$survey_id = $this->getRequest()->getParam('survey_id');
$status = $this->getRequest()->getParam('status');


$QSurveyForm = new Application_Model_SurveyForm();
$QNotificationAccess = new Application_Model_NotificationAccess();
$QNotificationNew = new Application_Model_Notification();
$QSurveyNewOject = new Application_Model_SurveyNewObject();

$db = Zend_Registry::get('db');
$db->beginTransaction();

$where = $QSurveyForm->getAdapter()->quoteInto('id = ?', $survey_id);
$QSurveyForm->update(array('status' => $status), $where);
$survey_form = $QSurveyForm->fetchRow($where);


if ($status == 1) {
    $title = $survey_form['name'];
    $content = "Bạn vui lòng <a style='color: red; text-decoration: underline' href='/survey/form-builder-submit?survey_id=" . $survey_id . "'" . ">click vào link</a> để thực hiện khảo sát";

    $notification_id = $QNotificationNew->insert([
       'title' => $title,
       'content' => $content,
        'pop_up' => 1,
        'show_from' => $survey_form['from_date'],
        'show_to' => $survey_form['to_date']
    ]);

    $list_staff = $QSurveyNewOject->fetchAll(['survey_id = ?' => $survey_id])->toArray();
    if (!$list_staff) {
        echo json_encode([
            'status' => 1,
            'message' => 'Vui lòng chọn nhận viên làm khảo sát trước khi chạy!'
        ]);
        return;
    }

    $insertQuery = "INSERT INTO hr.notification_access(notification_id, user_id) VALUES ";

    foreach ($list_staff as $staff) {
        $value = '(' . $notification_id . ',' . $staff['object_id'] . '),';
        $insertQuery .= $value;
    }

    $insertQuery = TRIM($insertQuery, ',');


    $stmt = $db->prepare($insertQuery);
    $stmt->execute();

    $stmt->closeCursor();
 

}

$db->commit();

echo json_encode([
   'status' => 0
]);
