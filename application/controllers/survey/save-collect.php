<?php 
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
        $data = json_decode($this->getRequest()->getParam('data'), true);    
        $survey_id = json_decode($this->getRequest()->getParam('survey_id'));
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id = $userStorage->id;

        // save upload file 
       $adapter = new Zend_File_Transfer();
       $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
       DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'. DIRECTORY_SEPARATOR . 'survey' . DIRECTORY_SEPARATOR . $survey_id ;

       if(!is_dir($uploaded_dir)){  //Check if the directory already exists, if not, create it
            mkdir($uploaded_dir, 0755);
        }
        
       $adapter->setDestination($uploaded_dir);
       $adapter->receive();
       $files = $adapter->getFileInfo();
         //end save uplaod file

        $QSurveyCollect = new Application_Model_SurveyCollect();

        $times = $QSurveyCollect->countTimesSubmit($staff_id, $survey_id); // count how many times user has submited the form  

        foreach ($data as $question => $answers) {
            foreach ($answers as $option =>  $answer) {   
                if (strpos($answer, '.zip') || strpos($answer, '.xlsx') || strpos($answer, '.jpg') || strpos($answer, '.png')) { // check if answer is a file
                    $answer = basename($answer);
                }
                $QSurveyCollect->insert([
                     'staff_id'  => $userStorage->id,
                     'question_index'  => $question,
                     'answer'    => $answer,
                     'checkbox_option' => is_string($option) ? $option : null,
                     'survey_id' => $survey_id,
                     'created_at' => date("Y-m-d H:i:s"),
                     'time'       => $times + 1
                ]);
            }
     
       }
    // return times submited after call ajax
      print($times);
      die;