<?php
//    $this->_helper->layout->disableLayout();
//    $this->_helper->viewRenderer->setNoRender();

$export = $this->getRequest()->getParam('export');
$survey_id = $this->getRequest()->getParam('survey_id');
$staff_code = $this->getRequest()->getParam('staff_code');
$page = $this->getRequest()->getParam('page', 1);
$limit = 10;

$QSurveyForm = new Application_Model_SurveyForm();
$QSurveyDetail = new Application_Model_SurveyDetail();
$QServeyCollect = new Application_Model_SurveyCollect();
$QServeyQuestions = new Application_Model_SurveyQuestions();
$QStaff = new Application_Model_Staff();

$where = $QStaff->getAdapter()->quoteInto('code = ?', $staff_code);
$staff = $QStaff->fetchRow($where);

$params = array(
    'staff_id' => $staff->id
);

$list = $QSurveyForm->fetchPagination($page, $limit, $total, $params);
$this->view->list = $list;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'survey/form-builder-list' . '?';

if ($export) {
    include_once 'PHPExcel.php';

    //get staff code and form's name to name excel files
    $whereForm = $QSurveyForm->getAdapter()->quoteInto('id = ?', $survey_id);
    $survey_form = $QSurveyForm->fetchRow($whereForm);
    $staff_id = $survey_form['created_by'];

    $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
    $staff = $QStaff->fetchRow($whereStaff);

    $staff_code = $staff['code'];
    $form_name = $survey_form['name'];
    //end get staff code and form's name to name excel files

    // $questions =   $QSurveyDetail->getAllQuestion($survey_id);
    $questions = $QServeyQuestions->getAllQuestions($survey_id);

    $records = $QServeyCollect->getAllRecord($survey_id);



    // order records to export
    foreach ($records as $value) {
        $data[$value['staff_id']] [$value['time']] ['company'] = $value['company'];
        $data[$value['staff_id']] [$value['time']] ['fullname'] = $value['fullname'];
        $data[$value['staff_id']] [$value['time']] ['code'] = $value['code'];
        $data[$value['staff_id']] [$value['time']] ['id_number'] = $value['id_number'];
        $data[$value['staff_id']] [$value['time']] ['department'] = $value['department'];
        $data[$value['staff_id']] [$value['time']] ['team'] = $value['team'];
        $data[$value['staff_id']] [$value['time']] ['title'] = $value['title'];
        $data[$value['staff_id']] [$value['time']] ['area'] = $value['area'];
        $data[$value['staff_id']] [$value['time']] ['join_date'] = $value['join_date'];
        $data[$value['staff_id']] [$value['time']] ['time'] = $value['time'];
        $data[$value['staff_id']] [$value['time']] ['survey_history_id'] = $value['survey_history_id'];
        $data[$value['staff_id']] [$value['time']] ['created_at'] = $value['created_at'];

        $data[$value['staff_id']] [$value['time']] ['answer'][] = $value['answer'];

    }

    foreach ($data as $array) {
        foreach ($array as $row) {
            $result[] = $row;
        }
    }
    // end order records to export


    $objExcel = new PHPExcel();

    $objExcel->setActiveSheetIndex(0);
    $sheet = $objExcel->getActiveSheet();
    $sheet->setTitle("Cau hoi thuong");


    $sheet->setCellValue('A1', 'COMPANY');
    $sheet->setCellValue('B1', 'CODE');
    $sheet->setCellValue('C1', '#');
    $sheet->setCellValue('D1', 'NAME');
    $sheet->setCellValue('E1', 'DEPARTMENT');
    $sheet->setCellValue('F1', 'TEAM');
    $sheet->setCellValue('G1', 'TITLE');
    $sheet->setCellValue('H1', 'AREA');
    $sheet->setCellValue('I1', 'JOIN DATE');
    $sheet->setCellValue('J1', 'CREATED AT');


    // insert questions columns
    $char = 'K';
    foreach ($questions as $question) {
        $sheet->setCellValue($char . 1, htmlspecialchars_decode($question));
        ++$char;
    }

    //style sheet
    for ($i = 'A'; $i < $char; ++$i) {
        $sheet->getStyle($i . 1)->getFont()->setBold(true);
        $sheet->getColumnDimension($i)->setAutoSize(true);
    }
    //

    $rowCount = 2;
    foreach ($result as $value) {

        // set default information
        $sheet->setCellValue('A' . $rowCount, $value['company']);
        $sheet->setCellValue('B' . $rowCount, $value['code']);
        $sheet->setCellValue('C' . $rowCount, '');
        $sheet->setCellValue('D' . $rowCount, $value['fullname']);
        $sheet->setCellValue('E' . $rowCount, $value['department']);
        $sheet->setCellValue('F' . $rowCount, $value['team']);
        $sheet->setCellValue('G' . $rowCount, $value['title']);
        $sheet->setCellValue('H' . $rowCount, $value['area']);
        $sheet->setCellValue('I' . $rowCount, $value['join_date']);
        $sheet->setCellValue('J' . $rowCount, $value['created_at']);

        //set dynamic information (answers)

        $char = 'K';
        foreach ($value['answer'] as $answer) {
            $sheet->setCellValue($char . $rowCount, $answer);
            ++$char;
        }

        // link to download file

        $linkDownload = HOST . 'survey/form-builder-download?survey_history_id=' . $value['survey_history_id'];
        $sheet->setCellValue($char . $rowCount, 'Hình ảnh');
        $sheet->getCell($char . $rowCount)
            ->getHyperlink()
            ->setUrl($linkDownload);
        $sheet->getStyle($char . $rowCount)
            ->applyFromArray(array('font' => array('color' => ['rgb' => '0000FF'], 'underline' => 'single')));

//        $sheet->setCellValue($char . $rowCount, 'https://local-center.local/survey/form-builder-download?survey_history_id=23');

        $rowCount++;
    }



    $filename = $form_name . '-' . $staff_code;
    $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;

}      