<?php 
	$this->_helper->layout->disableLayout();    
	$this->_helper->viewRenderer->setNoRender();  

	$flashMessenger  = $this->_helper->flashMessenger;

	$staff_titles = $this->getRequest()->getParam('staff_titles');
//	$from_date = $this->getRequest()->getParam('from_date');
//	$to_date = $this->getRequest()->getParam('to_date');
	$survey_id = $this->getRequest()->getParam('survey_id');
	$channel = $this->getRequest()->getParam('channel');
	$team = $this->getRequest()->getParam('teams');



	$QSurveyObject = new Application_Model_SurveyObject();
	$QSurvey = new Application_Model_Survey();
	$QSurveyObjectChannel = new Application_Model_SurveyObjectChannel();


	$where_survey_object = $QSurveyObject->getAdapter()->quoteInto('survey_id = ?', $survey_id);
	$where_survey = $QSurvey->getAdapter()->quoteInto('id = ?', $survey_id);

//	$QSurvey->update([
//		'from_date' => date("Y-m-d", strtotime($from_date)),
//		'to_date' => $to_date ? date("Y-m-d", strtotime($to_date)) : NULL
//	], $where_survey);

	$QSurveyObject->delete($where_survey_object);

	foreach ($staff_titles as $title) { // title-channel
		$QSurveyObject->insert([
			'survey_id' => $survey_id,
			'object_id' => $title,
			'type' => 0
		]);
		$where_survey_object_channel=array();
		$where_survey_object_channel[]=$QSurveyObjectChannel->getAdapter()->quoteInto('survey_id = ?', $survey_id);
		$where_survey_object_channel[]=$QSurveyObjectChannel->getAdapter()->quoteInto('object_id = ?', $title);
		$QSurveyObjectChannel->delete($where_survey_object_channel);
		foreach($channel as $k_title => $v_listChannel){
			if($k_title ==$title ){
				foreach ($v_listChannel as $key => $value) {
				# channel...
					$QSurveyObjectChannel->insert([
					'survey_id' => $survey_id,
					'object_id' => $title,
					'channel_id' => $value,
					]);
				}
			}
		}
	}


	foreach ($team as $k => $v) { // team
		$QSurveyObject->insert([
			'survey_id' => $survey_id,
			'object_id' => $v,
			'type' => 0
		]);
	}


	$flashMessenger->setNamespace('success')->addMessage('Successfully');

	$this->_redirect( HOST.'survey/list-survey');

