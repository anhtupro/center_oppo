<?php 
$save   = $this->getRequest()->getParam('save');
$submit = $this->getRequest()->getParam('submit');
$page   = $this->getRequest()->getParam('page', 1);
$limit  = null;


$QSurveyForm    = new Application_Model_SurveyForm();
$QSurveyImage   = new Application_Model_SurveyImage();
$QSurveyImageByChannel = new Application_Model_SurveyImageByChannel();

$QChannel       = new Application_Model_Channel();
$QSurvey        = new Application_Model_Survey();


$listSurvey     = $QSurvey->fetchPagination($page, $limit, $total, $params);
$list           = $QSurveyImage->fetchAll();
$listImageByChannel = $QSurveyImageByChannel->fetchAll();
$listChannel    = $QChannel->get_cache();
$listSurveyMini = [];
foreach($listSurvey as $key => $value){
		$listSurveyMini[$value['id']] = $value['title'];
}
// echo "<pre>";
// print_r($listSurveyMini);


$this->view->list=$list;
$this->view->listChannel=$listChannel;
$this->view->listSurvey=$listSurvey;
$this->view->listSurveyMini=$listSurveyMini;

$this->view->listImageByChannel=$listImageByChannel;


$total = count($_FILES['file']['name']);

if($save AND $total){
        try{

                $data=array("del" => 0);
                $id=$QSurveyImage->insert($data);
                for ($i = 0; $i < $total; $i++) {
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                    DIRECTORY_SEPARATOR . 'survey_image' . DIRECTORY_SEPARATOR . $id;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);
                $tmpFilePath    = $_FILES['file']['tmp_name'][$i];
                if ($tmpFilePath != "") {
                    $old_name   = $_FILES['file']['name'][$i];
                    $tExplode   = explode('.', $old_name);
                    $extension  = end($tExplode);
                    
                    // if(!in_array($extension, ['png', 'jpg', 'jpeg'])){
                    //     echo json_encode([
                    //         'status' => 0,
                    //         'message' => "File Upload sai định dạng",
                    //     ]);
                    //     return;
                    // }
                    
                    $new_name           = md5(uniqid('', true)) . $id . '-staff_id' . $userStorage->id."." .$extension;
                    $newFilePath        = $uploaded_dir . DIRECTORY_SEPARATOR . $new_name;

                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $url = 'files' .
                            DIRECTORY_SEPARATOR . 'survey_image' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $new_name;
                    } else {
                        $url = NULL;
                    }
                } else {
                    $url = NULL;
                }

                $detail_file = [
                    'url'       => $url,
                    'name'      => $_FILES['file']['name'][$i],
                    'hash_name' => $new_name,
                ];
                if (!empty($url)) {
                        $where=$QSurveyImage->getAdapter()->quoteInto("id =?",$id);
                        $QSurveyImage->update($detail_file,$where);


                        }
                }
        }catch(Exception $e){
                        // echo json_encode([
                        //     'status' => 0,
                        //     'message' => "Lỗi upload hình ảnh ".$e,
                        // ]);
                        // return;
                $this->redirect(HOST . 'survey/image-survey');

        }
        $this->redirect(HOST . 'survey/image-survey');


}

if($submit){
        try{
                // Data Input
                //     [listImage] => Array
                // (
                //     [36] => Array // Image id
                //         (
                //             [2] => 171 // channel id => survey id
                //             [7] => 177
                //         )
                // )
                $listDataInput = $this->getRequest()->getParam('listDataInput');

                foreach($listDataInput as $imgId => $channelSurvey){
                    // $QSurveyImageByChannel=new Application_Model_SurveyImageByChannel();
                        foreach($channelSurvey as $channelId => $surveyId){
                                $data=array(
                                                'survey_image'          => $imgId,
                                                'channel'               => $channelId,
                                                'survey_id'             => $surveyId,
                                        );
                                $t=$QSurveyImageByChannel->insert($data);
                        }
                }
        }catch(Exception $e){
                echo $e;
                // echo json_encode([
                //             'status' => 0,
                //             'message' => "Lỗi phân bố channel, survey theo hình ".$e,
                //         ]);
                // return;
        }
        $this->redirect(HOST . 'survey/image-survey');
}

