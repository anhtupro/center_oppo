<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

include 'PHPExcel/IOFactory.php';
// config for excel template
define('START_ROW', 2);
define('STAFF_CODE', 0);


$survey_id = $this->getRequest()->getParam('survey_id');


$QSurveyForm = new Application_Model_SurveyForm();
$QSurveyNewObject = new Application_Model_SurveyNewObject();
$QSurveyAccess = new Application_Model_SurveyAccess();

$save_folder = 'template_staff_survey';
$requirement = array(
    'Size' => array('min' => 5, 'max' => 15000000),
    'Count' => array('min' => 1, 'max' => 1),
    'Extension' => array('xls', 'xlsx')
);
// upload and save file

try {


    $file = My_File::get($save_folder, $requirement);

    if (!$file) {
        echo json_encode([
            'status' => 1,
            'message' => 'Upload failed'
        ]);
        return;
    }
    $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
        . DIRECTORY_SEPARATOR . $file['folder'];
    $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

//read file
//  Choose file to read
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
    return;
}

// read sheet
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
try {
    $QSurveyNewObject->delete(['survey_id = ?' => $survey_id]);
    $QSurveyAccess->delete(['survey_id = ?' => $survey_id]);

    $insertQueryObject = "INSERT INTO survey_new_object(survey_id, object_id, type) VALUES ";
    $insertQueryAccess = "INSERT INTO survey_access(survey_id, staff_id, status) VALUES ";
//    $deleteQuery = "DELETE FROM trade_marketing.store_facade_size WHERE store_id IN";
//    $updateQuery = "UPDATE trade_marketing.app_checkshop SET updated_facade_size = 1 WHERE is_lock = 1 AND store_id IN";

    for ($row = START_ROW; $row <= $highestRow; ++$row) {
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        $rowData = $rowData[0];

        $staffCode = TRIM($rowData[STAFF_CODE]);
    
        if ($staffCode) {
            $listStaffCode [] = $staffCode;

        }

    }

    if (! $listStaffCode) {
        echo json_encode([
            'status' => 1,
            'message' => 'Định dạng file không đúng. Vui lòng điền thông tin dựa trên file mẫu !'
        ]);
        return;
    } else {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['st' => 'staff'], [
                         'st.id'
                     ])
                    ->where('st.code IN (?)', $listStaffCode)
                   ->group('st.id');

        $listStaffId = $db->fetchCol($select);

        foreach ($listStaffId as $staffId) {
    
            $valueObject = '(' . $survey_id . ',' . $staffId . ',' . 1 . '),';
            $insertQueryObject .= $valueObject;

            $valueAccess = '(' . $survey_id . ',' . $staffId . ',' . 0 . '),';
            $insertQueryAccess .= $valueAccess;
        }
    }


    // check if store_id is valid

    // execute query
    $insertQueryObject = TRIM($insertQueryObject, ',');
    $insertQueryAccess = TRIM($insertQueryAccess, ',');


    if($insertQueryObject && $insertQueryAccess){
        $db = Zend_Registry::get('db');
        $db->beginTransaction();

        $stmt = $db->prepare($insertQueryObject);
        $stmt->execute();

        $stmt2 = $db->prepare($insertQueryAccess);
        $stmt2->execute();

        $stmt->closeCursor();
        $stmt2->closeCursor();

        $db->commit();

        echo json_encode([
            'status' => 0
        ]);
        return;
    }

    echo json_encode([
        'status' => 1,
        'message' => 'File trống'
    ]);
    return;


} catch (Exception $e) {
    echo json_encode([
        'status' => 1,
        'message' => $e->getMessage()
    ]);
}

