<?php
$survey_id = $this->getRequest()->getParam('survey_id');

$QSurveyForm = new Application_Model_SurveyForm();

$survey = $QSurveyForm->fetchRow(['id = ?'=> $survey_id]);

$this->view->survey_id = $survey_id;
$this->view->survey = $survey;