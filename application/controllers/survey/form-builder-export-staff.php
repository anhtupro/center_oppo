<?php
$survey_id = $this->getRequest()->getParam('survey_id');

ini_set("memory_limit", -1);
ini_set("display_error", 1);
error_reporting(~E_ALL);

//    $data_merge = $QAppCheckShop->getExcelMerge($params);
$QSurveyAccess = new Application_Model_SurveyAccess();

// get data
$db = Zend_Registry::get("db");
$select = $db->select()
             ->from(['o' => 'survey_access'], [
               'st.code',
               'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)"  
             ])
->joinLeft(['st' => 'staff'], 'o.staff_id = st.id', [])
->where('o.survey_id = ?', $survey_id);

$data = $db->fetchAll($select);


//end get data



require_once 'PHPExcel.php';

$PHPExcel = new PHPExcel();
$heads = array(
    'Staff Code',
    'Họ và tên'
);

$PHPExcel->setActiveSheetIndex(0);
$sheet    = $PHPExcel->getActiveSheet();

$alpha    = 'A';
$index    = 1;
foreach($heads as $key)
{
    $sheet->setCellValue($alpha.$index, $key);
    $alpha++;
}
$index    = 2;

$i = 1;

foreach($data as $item){

    $alpha    = 'A';
    $sheet->setCellValue($alpha++.$index, $item['code']);
    $sheet->setCellValue($alpha++.$index, $item['staff_name']);

    $index++;

}



$filename = 'Danh sách nhân viên làm khảo sát' . date('d-m-Y H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');
exit;
