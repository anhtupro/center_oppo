<?php 
        $QTeam                 = new Application_Model_Team();
        $QArea                 = new Application_Model_Area();
        $QRegionalMarket       = new Application_Model_RegionalMarket();
        $QNotificationCategory = new Application_Model_NotificationCategory();
        $QCompany              = new Application_Model_Company();
        $QSurveyObject         = new Application_Model_SurveyObject();
        $QSurvey               = new Application_Model_Survey();
        $QSurveyObjectChannel = new Application_Model_SurveyObjectChannel();
        $QChannel = new Application_Model_Channel();

        $survey_id = $this->getRequest()->getParam('survey_id');

        $where_survey = $QSurvey->getAdapter()->quoteInto('id = ?', $survey_id);
        $row_survey = $QSurvey->fetchRow($where_survey);

//        $from_date = $row_survey['from_date'] ? date("d-m-Y", strtotime($row_survey['from_date'])) : NULL;
//        $to_date = $row_survey['to_date'] ? date("d-m-Y", strtotime($row_survey['to_date'])) : NULL;
        $old_title_objects = $QSurveyObject->getTitle($survey_id);
        $listChannelOnSelectByTitle=$QSurveyObjectChannel->getChannelSelectBySurvey($survey_id);

        // $titleChannelSelected=$QSurvey->getSelectedTitleChannel($survey_id);




//        $this->view->from_date             = $from_date;
        $this->view->to_date               = $to_date;
        $this->view->old_title_objects     = $old_title_objects;
        $this->view->survey_id             = $survey_id;

        // $this->view->titleChannelSelected             = $titleChannelSelected;

        $this->view->team_cache            = $QTeam->get_recursive_cache();

        $this->view->listChannel=$QChannel->get_cache();
        $this->view->listChannelOnSelectByTitle=$listChannelOnSelectByTitle;


        $this->view->area_cache            = $QArea->get_cache();
        $this->view->region_cache          = $QRegionalMarket->get_cache();
        $this->view->category_cache        = $QNotificationCategory->get_full_cache();
        $this->view->categories            = $QNotificationCategory->get_cache();
        $this->view->category_string_cache = $QNotificationCategory->get_string_cache();
        $this->view->companies             = $QCompany->get_cache();

