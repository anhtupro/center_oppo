<?php
$this->_helper->layout->disableLayout();
$QTeam                 = new Application_Model_Team();
$QArea                 = new Application_Model_Area();
$QRegionalMarket       = new Application_Model_RegionalMarket();
$QNotificationCategory = new Application_Model_NotificationCategory();
$QCompany              = new Application_Model_Company();
$survey_id = $this->getRequest()->getParam('survey_id');
$id = $this->getRequest()->getParam('id');

$QSurveyForm = new Application_Model_SurveyForm();


$this->view->team_cache            = $QTeam->get_recursive_cache();
$this->view->area_cache            = $QArea->get_cache();
$this->view->region_cache          = $QRegionalMarket->get_cache();
$this->view->category_cache        = $QNotificationCategory->get_full_cache();
$this->view->categories            = $QNotificationCategory->get_cache();
$this->view->category_string_cache = $QNotificationCategory->get_string_cache();
$this->view->companies             = $QCompany->get_cache();

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;
if($id) {
	$where = $QSurveyForm->getAdapter()->quoteInto('id = ?', $id);
	$survey_form = $QSurveyForm->fetchRow($where);
        
	$form_file = $survey_form['file_name'];
	$filedset = $survey_form['fieldset'];
	$mode = 'fill';
	$this->view->filedset = $filedset;
	$this->view->survey_form = $survey_form->toArray();
}