<?php
    $this->_helper->layout->disableLayout();
    $form_html = $this->getRequest()->getParam('form_html');
    $description = $this->getRequest()->getParam('description_preview');
    $form_name = $this->getRequest()->getParam('title_preview');

    $this->view->form_html = $form_html;
    $this->view->description = $description;
    $this->view->form_name = $form_name;

