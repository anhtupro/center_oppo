<?php 
	$this->_helper->layout->disableLayout();    
	$this->_helper->viewRenderer->setNoRender();

	include 'PHPExcel/IOFactory.php';


     // config for excel template
	define('START_ROW', 2);
	define('STT', 0);
	define('QUESTION', 1);
	define('OPTION', 2);
	define('ORDER', 3);
	define('RIGHT', 4); // right answer
	define('RESPONE_TYPE', 'radio');

    //

	set_time_limit(0);
	ini_set('memory_limit', -1);

	$flashMessenger  = $this->_helper->flashMessenger;

	$title = $this->getRequest()->getParam('title');
	$content = $this->getRequest()->getParam('content');

	$QSurveyQuestion = new Application_Model_SurveyQuestion();
	$QSurveyQuestionOption = new Application_Model_SurveyQuestionOption();
	$QSurvey = new Application_Model_Survey();

	$auth = Zend_Auth::getInstance()->getStorage()->read();


	$save_folder = 'mass-upload-survey';
	$requirement   = array(
		'Size'      => array('min' => 5, 'max' => 5000000),
		'Count'     => array('min' => 1, 'max' => 1),
		'Extension' => array('xls', 'xlsx')
	);
     //  save file excel
	try {
		$file = My_File::get($save_folder, $requirement, true);

		if (! $file) {
			$flashMessenger->setNamespace('error')->addMessage('Upload failed.');
			$this->_redirect( HOST.'survey/mass-upload-survey' );
		} 

		$uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
		. DIRECTORY_SEPARATOR . $file['folder'];

		$inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

	} catch (Exception $e) {
		$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
		$this->_redirect( HOST.'survey/mass-upload-survey' );
	}

     //read file
    //  Choose file to read
	try {
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader     = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel   = $objReader->load($inputFileName);

	} catch (Exception $e) {    
		$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
		$this->_redirect( HOST.'survey/mass-upload-survey' );
	}

	 // read sheet 
	$sheet = $objPHPExcel->getSheet(0);
	$highestRow  = $sheet->getHighestRow();
	$highestColumn   = $sheet->getHighestColumn();

	try {
		for ($row = START_ROW; $row <= $highestRow; ++$row) {
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			$rowData = $rowData[0];

			if ($rowData[STT]) {
				$data_mass_upload [$rowData[STT]] [$rowData[QUESTION]] [$rowData[ORDER]] [] = $rowData[OPTION];		
				$data_mass_upload [$rowData[STT]] [$rowData[QUESTION]] [$rowData[ORDER]] [] = $rowData[RIGHT];								
			}
        } // end for

    } catch (Exception $e) {
    	$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    	$this->_redirect( HOST.'survey/mass-upload-survey' );
    }

    // insert to database
    if ($data_mass_upload) {
         $db = Zend_Registry::get('db');

         try {
         	 $db->beginTransaction();

	         $survey_id = $QSurvey->insert([
	         	'title' => $title,
	         	'content' => $content,
	         	'status' => 0,
	         	'created_at' => date("Y-m-d H:i:s"),
	         	'created_by' => $auth->id
	         ]);

	         foreach ($data_mass_upload as $stt => $data) {
	         	foreach ($data as $question => $options) {

	         		$survey_question_id = $QSurveyQuestion->insert([
	         			'question' => $question,
	         			'survey_id' => $survey_id,
	         			'order' => $stt,
	         			'response_type' => RESPONE_TYPE
	         		]);

	         		foreach ($options as $order => $option) {
	         			$QSurveyQuestionOption->insert([
	         				'option' => $option[0],
	         				'survey_question_id' => $survey_question_id,
	         				'order' => $order,
	         				'true' => $option[1] ? 1 : 0
	         			]);
	         		}
	         	}
	         }

	         $db->commit();

			$this->_redirect( HOST.'survey/set-title?survey_id=' . $survey_id);

         } catch (Exception $e) {
         	$db->rollback();
         	$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
			$this->_redirect( HOST.'survey/mass-upload-survey' );
         	
         }
    }
