<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$survey_id = $this->getRequest()->getParam('survey_id');
$from_date = $this->getRequest()->getParam('from_date');
$to_date = $this->getRequest()->getParam('to_date');


$QSurveyForm = new Application_Model_SurveyForm();

$QSurveyForm->update([
    'from_date' => date('Y-m-d', strtotime($from_date)),
    'to_date' => date('Y-m-d', strtotime($to_date))
], ['id = ?' => $survey_id]);

echo json_encode([
    'status' => 0
]);