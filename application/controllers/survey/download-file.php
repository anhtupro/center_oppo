<?php 
    $this->_helper->layout->disableLayout();
    $fileName = $this->getRequest()->getParam('fileName');
    $download_all = $this->getRequest()->getParam('download_all');
    $survey_id = $this->getRequest()->getParam('survey_id');

    //get staff_code to name the file
    if($survey_id) {
        $QSurveyForm = new Application_Model_SurveyForm();
        $QStaff = new  Application_Model_Staff();

        $whereForm = $QSurveyForm->getAdapter()->quoteInto('id = ?', $survey_id);
        $survey_form = $QSurveyForm->fetchRow($whereForm);

        $form_name = $survey_form['name'];
        $staff_id = $survey_form['created_by'];

        $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?', $staff_id);
        $staff = $QStaff->fetchRow($whereStaff);
        $staff_code = $staff['code'];
    }    

    
    if ($download_all) { // download file zip
        $pathDir =  APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'. DIRECTORY_SEPARATOR . 'survey'. DIRECTORY_SEPARATOR .  $survey_id;

        $zip_file_temp = tempnam("/tmp", '');
        $zip = new ZipArchive();
        $zip->open($zip_file_temp, ZipArchive::OVERWRITE);
        $dir = opendir($pathDir);
        while($file = readdir($dir)) {
            if (is_file($pathDir . DIRECTORY_SEPARATOR . $file)) {
                $zip->addFile($pathDir . DIRECTORY_SEPARATOR . $file, $file);
            }
            
        }
        $zip->close();


       //download file
        $download_filename = $form_name . '-' .  $staff_code . '.zip';
        header("Content-Type: application/zip");
        header("Content-Length: " . filesize($zip_file_temp));
        header("Content-Disposition: attachment; filename=\"" . $download_filename . "\"");

        readfile($zip_file_temp);
        unlink($excel_file_temp);
        unlink($zip_file_temp);
        exit;

    } else { // download 1 file
        $file = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files'. DIRECTORY_SEPARATOR . 'survey'. DIRECTORY_SEPARATOR . $survey_id.DIRECTORY_SEPARATOR. $fileName;

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    