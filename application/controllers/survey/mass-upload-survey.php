<?php 
	$flashMessenger       = $this->_helper->flashMessenger;
	$successes             = $flashMessenger->setNamespace('success')->getMessages();
	$errors             = $flashMessenger->setNamespace('error')->getMessages();

	$this->view->successes = $successes;
	$this->view->errors = $errors;
