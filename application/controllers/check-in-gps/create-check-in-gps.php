<?php 
    $flashMessenger = $this->_helper->flashMessenger;

    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
    $flashMessenger = $this->_helper->flashMessenger;

    $QStaff       = new Application_Model_Staff();
    $QOffice      = new Application_Model_Office();
    $QTimeGpsNew  = new Application_Model_TimeGpsNew();

    $staff_id = $userStorage->id;
    $date     = date('Y-m-d');
    $to_day   = date('d/m/Y');

    $where_check         = [];
    $where_check[]       = $QTimeGpsNew->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    $where_check[]       = $QTimeGpsNew->getAdapter()->quoteInto('check_in_day = ?', $date);
    
    $check_in = $QTimeGpsNew->fetchRow($where_check);

    $list_office = $QOffice->fetchAllOffice();

    $this->view->list_office = $list_office;
    $this->view->check_in = $check_in;
    $this->view->to_day = $to_day;

    $where = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
    $this->view->staff = $staff = $QStaff->fetchRow($where);


    $this->view->user_id = $userStorage->id;
    $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

    $messages = $flashMessenger->setNamespace('error')->getMessages();
    $this->view->messages = $messages;

    $messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_success = $messages_success;