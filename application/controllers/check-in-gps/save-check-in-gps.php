<?php
$flashMessenger = $this->_helper->flashMessenger;
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$office_id   = $this->getRequest()->getParam('office_id');
$latitude    = $this->getRequest()->getParam('latitude');
$longitude   = $this->getRequest()->getParam('longitude');
$note        = $this->getRequest()->getParam('note');

$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();

if ($this->getRequest()->getMethod() == 'POST') {
    if($userStorage->department == '152' or $userStorage->department == '610') {
        $flashMessenger->setNamespace('error')->addMessage('Chức năng này không áp dụng cho phòng ban Sale và Retail!');
        $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
        exit;
    }
    $staff_id       = $userStorage->id;
    $staff_code     = $userStorage->code;
    $date           = date('Y-m-d');
    $to_date        = date("Y-m-t");
    $datetime       = date('Y-m-d H:i:s');

    $QOffice        = new Application_Model_Office();
    $QTimeGpsNew    = new Application_Model_TimeGpsNew();

    $where_check         = [];
    $where_check[]       = $QTimeGpsNew->getAdapter()->quoteInto('staff_id = ?', $staff_id);
    $where_check[]       = $QTimeGpsNew->getAdapter()->quoteInto('check_in_day = ?', $date);

    //Check in hoac check out neu da check in
    $check_in = $QTimeGpsNew->fetchRow($where_check);

    //Kiểm tra ngày check in đã chấm máy chưa
    $db = Zend_Registry::get('db');

    $sql = "SELECT
                sta.id,
                ci.staff_code,
                cit.Date check_in_at
            FROM check_in_tmp cit
            JOIN check_in_machine_map ci ON cit.EnrollNumber = ci.enroll_number AND cit.IpMachine = ci.ip_machine
            JOIN staff sta ON ci.staff_code = sta.code
            WHERE DATE(cit.Date) = '$date'
            AND sta.id = '$staff_id'";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $check_in_tmp = $stmt->fetchAll();
    $stmt->closeCursor();
    $stmt = $db = null;

    //Validation
    if(empty($latitude) || empty($longitude)) {
        $flashMessenger->setNamespace('error')->addMessage('Vui lòng bật gps!');
        $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
        exit;
    }

    if($office_id !== '0') {
        if($office_id && $office_id == $userStorage->office_id){
            $flashMessenger->setNamespace('error')->addMessage('Không thể chấm công tại văn phòng bạn được gán!');
            $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
            exit;
        }

        //Check khoang cach office khong qua 500m
        $office_check_in = $QOffice->getLocationOffice($office_id);

        if(empty($office_check_in['latitude']) || empty($office_check_in['longitude'])) {
            $flashMessenger->setNamespace('error')->addMessage('Văn phòng này chưa có vị trí gps!');
            $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
            exit;
        }

        $distance_office = My_DistanceGps::getDistanceOffice($latitude, $longitude, $office_check_in['latitude'], $office_check_in['longitude']);

        if($distance_office > 0.5){
            $flashMessenger->setNamespace('error')->addMessage('Bạn chấm công cách văn phòng vượt quá quy định. Vui lòng sử dụng 3g/4g để tăng độ chính xác!');
            $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
            exit;
        }
    } else {
        if(empty($note)) {
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng mô tả vị trí công tác ngoài');
            $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
            exit;
        }
    }

    // if(empty($office_id) || $office_id == '0'){
    //     $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn office check in!');
    //     $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
    //     exit;
    // }

    if($check_in || $check_in_tmp) {
        $data_insert = array(
            'staff_id'         => $staff_id,
            'staff_code'       => $staff_code,
            'check_in_day'     => $date,
            'check_in_at'      => $datetime,
            'note'             => $note,
            'status'           => '1',
            'latitude'         => $latitude,
            'longitude'        => $longitude,
            'office_id'        => $office_id,
            'type'             => '2'
        );
    
        $QTimeGpsNew->insert($data_insert);

        $flashMessenger->setNamespace('success')->addMessage('Chấm công thành công!');
        $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
    } else {
        $data_insert = array(
            'staff_id'         => $staff_id,
            'staff_code'       => $staff_code,
            'check_in_day'     => $date,
            'check_in_at'      => $datetime,
            'note'             => $note,
            'status'           => '1',
            'latitude'         => $latitude,
            'longitude'        => $longitude,
            'office_id'        => $office_id,
            'type'             => '1'
        );
    
        $QTimeGpsNew->insert($data_insert);
    
        $flashMessenger->setNamespace('success')->addMessage('Chấm công thành công!');
        $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
    }

    // Sau nay neu co chup hinh thi lam
    // if($type == "2") {
    //     if($is_img1 == "1") {
    //         $img1 = str_replace('data:image/jpeg;base64,', '', $img1);
    //         $img1 = str_replace(' ', '+', $img1);
    //         $data = base64_decode($img1);
    //         $name1= "image_1_".$userStorage->id."_". time().".jpg";
    //         $file1 = APPLICATION_PATH . "/../public/photo/check-in-gps-new/" . $name1;
    //         $success1 = file_put_contents($file1, $data);
    //     } else {
    //         $flashMessenger->setNamespace('error')->addMessage('Vui lòng chụp hình check in!');
    //         $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
    //         exit;
    //     }

    //     if($check_in) {
    //         $data_update = array(
    //             'check_in_at'      => $datetime,
    //             'image'            => $name1,
    //             'office_time'      => (float) $office_time,
    //             'latitude'         => $latitude,
    //             'longitude'        => $longitude,
    //             'status'           => '0',
    //             'approved_at'      => null,
    //             'approved_by'      => null,
    //         );
        
    //         $QTimeGpsNew->update($data_update, $where_check);
        
    //         $flashMessenger->setNamespace('success')->addMessage('Check out thành công! Vui lòng xem thông tin tại Công/Phép! Chờ quản lý approve');
    //         $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
    //     } else {
    //         //Check in with image
    //         $data_insert = array(
    //             'staff_id'         => $staff_id,
    //             'staff_code'       => $staff_code,
    //             'check_in_day'     => $date,
    //             'check_in_at'      => $datetime,
    //             'note'             => "Check in by GPS tool new with image",
    //             'status'           => '0',
    //             'latitude'         => $latitude,
    //             'longitude'        => $longitude,
    //             'type'             => '3',
    //             'image'            => $name1,
    //             'office_time'      => (float) $office_time,
    //         );
        
    //         $QTimeGpsNew->insert($data_insert);
        
    //         $flashMessenger->setNamespace('success')->addMessage('Đã gửi yêu cầu tới quản lý. Vui lòng chờ quản lý approve');
    //         $this->_redirect(HOST . 'check-in-gps/create-check-in-gps');
    //     }
    // }

}

$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;