<?php

class MassuploadController extends Zend_Controller_Action{

	public function init(){}

	public function indexAction(){}

	public function importimeiAction(){
		if($this->_request->isPost()){
			$file = $_FILES['file'];
			require_once 'PHPExcel.php';
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
			foreach($objPHPExcel->getWorksheetIterator() as $worksheet){
				$data[$worksheet->getTitle()] = $worksheet->toArray();
			}

			$sql = null;
			$data = array_values($data)[0];
			foreach($data as $key => $value){
				$sql[] = '('.$value[0].')';
			}

			$db = Zend_Registry::get('db');
			$table = 'imei_sn_bk';
			$db->query('
				DROP TABLE IF EXISTS '.$table.';

				CREATE TABLE '.$table.'(
					imei_sn BIGINT NOT NULL PRIMARY KEY
				) ENGINE=INNODB CHARSET=UTF8;

				INSERT INTO '.$table.' VALUES'.implode(',', $sql).';
			');

			exit('ok');
		}
	}

	public function processAction(){
		if($this->_request->isPost()){
			$file = $_FILES['file'];
			$data = $sql = null;
			$month = $this->_request->getParam('month');
			$month = DateTime::createFromFormat('m/Y', $month);
			$month = $month ? $month->format('m_Y') : null;

			require_once 'PHPExcel.php';
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
			foreach($objPHPExcel->getWorksheetIterator() as $worksheet){
				$data[$worksheet->getTitle()] = $worksheet->toArray();
			}
			$data = array_values($data)[0];
			unset($data[0]);

			foreach($data as $key => $value){
				$imei = $value[3];
				$timing_date = DateTime::createFromFormat('d/m/Y H:i:s', $value[6]);
				$timing_date = $timing_date ? '"'.$timing_date->format('Y-m-d H:i:s').'"' : null;
				$customer_name = '"'.$value[8].'"';
				$address = '"'.$value[9].'"';
				$phone = '"'.$value[10].'"';

				if($imei) $sql[] = '('.$imei.', '.$timing_date.', '.$customer_name.', '.$address.', '.$phone.')';
			}

			if($sql){
				$ma = new Application_Model_Massupload;
				$ma->InsertTimingTemp(implode(',', $sql), $month);
			}
			// echo '<pre>'; print_r($sql);
			exit;
		}
	}

}