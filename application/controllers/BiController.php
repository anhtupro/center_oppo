<?php
class BiController extends My_Controller_Action
{   
    public function init()
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);

        $this->_helper->layout->setLayout('layout_bi_new');
    }
    
    
    public function indexAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if($userStorage->email == 'tam.do@oppomobile.vn' || $userStorage->email == 'vannam.nguyen@oppomobile.vn' || $userStorage->email == 'linh.nguyen@oppomobile.vn'){
            //require_once 'bi' . DIRECTORY_SEPARATOR . 'index.php';
            require_once 'bi' . DIRECTORY_SEPARATOR . 'index.php';
        }
        else{
            //require_once 'bi' . DIRECTORY_SEPARATOR . 'index_backup_banchuan.php';
            require_once 'bi' . DIRECTORY_SEPARATOR . 'index.php';
        }
        
    }

    public function brandShopAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'brand-shop.php';
    }

    public function brandShopAreaAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'brand-shop-area.php';
    }

    public function areaBrandAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'area-brand.php';
    }

    public function areaAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'area.php';
    }

    public function areaTradeAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'area-trade.php';
    }

    public function areaTradeDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'area-trade-details.php';
    }

    public function areaTradeDetailsCostAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'area-trade-details-cost.php';
    }

    public function areaTradeDetailsDecorAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'area-trade-details-decor.php';
    }

    public function areaTradeDetailsDecorTAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'area-trade-details-decor-t.php';
    }

    public function areaTradeDetailsPosmAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'area-trade-details-posm.php';
    }

    public function areaTradeDetailsPosmTAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'area-trade-details-posm-t.php';
    }

    public function salesLeaderAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'sales-leader.php';
    }
    
    public function dealerChartAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'dealer-chart.php';
    }

    public function provinceChartAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'province-chart.php';
    }

    public function districtChartAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'district-chart.php';
    }
    
    public function dealerAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'dealer.php';
    }
    
    public function storeAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'store.php';
    }
    
    public function storeDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'store-details.php';
    }
    
    private static function cmp($a, $b)
    {
        if ($a['point'] == $b['point'])
        {
            return 0;
        }
        return ($a['point'] < $b['point']) ? 1 : -1;
    }
    
    
    public function staffAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff.php';
    }
    
    public function asmAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'asm.php';
    }
    
    public function asmDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'asm-details.php';
    }
    
    public function leaderDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'leader-details.php';
    }

    public function saleDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'sale-details.php';
    }

    public function pgsLeaderAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'pgs-leader.php';
    }

    public function trainerDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'trainer-leader-details.php';
    }
	
    public function trainerLeaderDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'trainer-leader-details.php';
    }
    
    public function pgsSupervisorAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'pgs-supervisor.php';
    }

    public function pgDetailsAction()
    {
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'pg-details.php';
    }
    
    public function sellInDealerAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'sell-in' . DIRECTORY_SEPARATOR .'sell-in-dealer.php';
    }
    
    public function sellInDealerParentAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'sell-in' . DIRECTORY_SEPARATOR .'sell-in-dealer-parent.php';
    }

    public function hrHeadcountAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'hr' . DIRECTORY_SEPARATOR .'hr-headcount.php';
    }

    public function hrAgeAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'hr' . DIRECTORY_SEPARATOR .'hr-age.php';
    }
    
    public function hrPgpbSaleAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'hr' . DIRECTORY_SEPARATOR .'hr-pgpb-sale-area.php';
    }

    public function listStaffAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR .'list-staff.php';
    }

    public function biTradeAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'bi-trade.php';
    }

    public function biTradeAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'bi-trade-area.php';
    }

    public function tradeAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'trade-area.php';
    }

    public function tradeDistrictAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'trade-district.php';
    }

    public function tradeAreaDetailAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'trade-area-detail.php';
    }

    public function tradeProvinceDetailAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'trade-province-detail.php';
    }

    public function tradeDealerAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'trade-dealer.php';
    }

    public function tradeStoreAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'trade-store.php';
    }

    public function tradeStoreDetailAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'trade-store-detail.php';
    }

    public function resetDataChartAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'reset-data-chart.php';
    }

    public function resetDataChartAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'reset-data-chart-area.php';
    }

    public function resetDataChartDistrictAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'trade' . DIRECTORY_SEPARATOR .'reset-data-chart-district.php';
    }

    public function notificationPgAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'notification-pg.php';
    }
    
    public function notificationTrainerAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'notification-trainer.php';
    }
    
     public function listStoreAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-store.php';
    }
     public function ajaxListStoreAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'ajax-list-store.php';
    }
    public function listPgSelloutAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-sellout.php';
    }
    public function listPgChanelAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-chanel.php';
    }
    public function listPgSeniorityAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-seniority.php';
    }
     public function listPgAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg.php';
    }
     public function listAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-area.php';
    }
    public function listRegionAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-region.php';
    }
    public function listRegionCourseAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-region-course.php';
    }
    public function listAreaCourseAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-area-course.php';
    }
     public function ajaxListDealerAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'ajax-list-dealer.php';
    }
    
    public function trainingOnlineReportAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'training-online-report.php';
    }
    
    public function hrStaffEvolutionAjaxAction(){
        $year = $this->getRequest()->getParam('year');

        $KpiMonth = new Application_Model_KpiMonth();
        $params = array();
        $params['year'] = $year;
        $staff_evolution = $KpiMonth->staff_evolution($params);

        $mon_en = array(
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        );

        $data = array();
        foreach ($staff_evolution as $key => $value) {
            $data['total'][] = intval($value['total']);
            $data['month'][] = $mon_en[intval($value['month'])];
        }

        echo json_encode($data);
        exit;
    }

    public function hrHeadcountChartAjaxAction(){
        $year = $this->getRequest()->getParam('year');
        $group = $this->getRequest()->getParam('group');

        $KpiMonth = new Application_Model_KpiMonth();
        $params = array();
        $params['year'] = $year;

        $params['group'] = array();
        if($group == 1){
            $params['group'] = array(1,15,14,140,129,130);
        }
        elseif($group == 2){
            $params['group'] = array(146,141,133,131);
        }
        elseif($group == 3){
            $params['group'] = array(75);
        }

        $headcount = $KpiMonth->headcount($params);

        $data = array();
        foreach ($headcount as $key => $value) {
            $data['name'][] = $value['name'];
            $data['QuarterNo1'][] = intval($value['QuarterNo1']);
            $data['QuarterNo2'][] = intval($value['QuarterNo2']);
            $data['QuarterNo3'][] = intval($value['QuarterNo3']);
            $data['QuarterNo4'][] = intval($value['QuarterNo4']);
        }

        echo json_encode($data);
        exit;
    }

    public function hrPgpbTurnoverRateAction(){
        $year = $this->getRequest()->getParam('year');

        $KpiMonth = new Application_Model_KpiMonth();
        $params = array();
        $params['year'] = $year;
        $turnover_rate = $KpiMonth->turnover_rate($params);

        $mon_en = array(
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        );

        $data = array();
        foreach ($turnover_rate as $key => $value) {
            $data['total'][] = intval($value['TotalRate']);
            $data['month'][] = $mon_en[intval($value['MonthNo'])];
        }

        echo json_encode($data);
        exit;
    }

    public function hrQuarterTurnoverAction(){
        $quarter = $this->getRequest()->getParam('quarter');

        $KpiMonth = new Application_Model_KpiMonth();

        $params = array();
        $params['year'] = date('Y');
        $params['cur_quarter'] = $quarter;

        $params['group_turnover_rate'] = array(1,15,14,140,129,130);
        $turnover_pass_probation1 = $KpiMonth->turnover_pass_probation($params);

        $params['group_turnover_rate'] = array(146,141,331,131,75);
        $turnover_pass_probation2 = $KpiMonth->turnover_pass_probation($params);

        $data = null;

        //group 1
        $data .= "<table><tr><th></th>";
        foreach ($turnover_pass_probation1 as $key => $value) {
            $data .= "<th>".$value['name']."</th>";
        }
        $data .= "</tr><tr><td>Passed Probation Rate</td>";
        foreach ($turnover_pass_probation1 as $key => $value) {
            $data .= "<td>".($value['totalPassProbation']*100)." %</td>";
        }
        $data .= "</tr><tr><td>Total Rate</td>";
        foreach ($turnover_pass_probation1 as $key => $value) {
            $data .= "<td>".($value['totalRate']*100)." %</td>";
        }
        $data .= "</tr></table>";

        //group2
        $data .= "<table><tr><th></th>";
        foreach ($turnover_pass_probation2 as $key => $value) {
            $data .= "<th>".$value['name']."</th>";
        }
        $data .= "</tr><tr><td>Passed Probation Rate</td>";
        foreach ($turnover_pass_probation2 as $key => $value) {
            $data .= "<td>".($value['totalPassProbation']*100)." %</td>";
        }
        $data .= "</tr><tr><td>Total Rate</td>";
        foreach ($turnover_pass_probation2 as $key => $value) {
            $data .= "<td>".($value['totalRate']*100)." %</td>";
        }
        $data .= "</tr></table>";

        echo $data;
        exit;
    }

    public function hrHeadcountProbationAction(){
        $date = $this->getRequest()->getParam('date');

        $KpiMonth = new Application_Model_KpiMonth();

        $params = array();

        $params['date_probation'] = date("Y-m-d", strtotime($date) );

        $params['group_headcount_probation'] = array(1,15,14,140,129,130);
        $headcount_probation1 = $KpiMonth->headcount_probation($params);

        $params['group_headcount_probation'] = array(146,141,331,131,75);
        $headcount_probation2 = $KpiMonth->headcount_probation($params);

        $data = null;

        //group 1
        $data .= "<table><tr><th></th>";
        foreach ($headcount_probation1 as $key => $value) {
            $data .= "<th>".$value['name']."</th>";
        }
        $data .= "</tr><tr><td>On Probation</td>";
        foreach ($headcount_probation1 as $key => $value) {
            $data .= "<td>".$value['Probation']."</td>";
        }
        $data .= "</tr><tr><td>Passed Probation</td>";
        foreach ($headcount_probation1 as $key => $value) {
            $data .= "<td>".$value['Pass_Probation']."</td>";
        }
        $data .= "</tr><tr><td>Total</td>";
        foreach ($headcount_probation1 as $key => $value) {
            $data .= "<td>".$value['Total']."</td>";
        }
        $data .= "</tr></table>";

        //group2
        $data .= "<table><tr><th></th>";
        foreach ($headcount_probation2 as $key => $value) {
            $data .= "<th>".$value['name']."</th>";
        }
        $data .= "</tr><tr><td>On Probation</td>";
        foreach ($headcount_probation2 as $key => $value) {
            $data .= "<td>".$value['Probation']."</td>";
        }
        $data .= "</tr><tr><td>Passed Probation</td>";
        foreach ($headcount_probation2 as $key => $value) {
            $data .= "<td>".$value['Pass_Probation']."</td>";
        }
        $data .= "</tr><tr><td>Total</td>";
        foreach ($headcount_probation2 as $key => $value) {
            $data .= "<td>".$value['Total']."</td>";
        }
        $data .= "</tr></table>";

        echo $data;
        exit;
    }

    public function createDateRangeArray($strDateFrom,$strDateTo)
    {    
        $aryRange=array();
    
        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));
    
        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }
    
    public function _exportSellIn($select, $range){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
       
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
    
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        

        // head
        $heads = array(
            '#',
            'Name',
            'Area',
            'Province',
            'District',
        );
        
        foreach($range as $key => $value){
            $date_v = new DateTime($value);
            $heads[] = $date_v->format('d/m');
        }

        foreach ($heads as $key)
        {
            
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;
        
        $val = $select;
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();
        $con = mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);
        mysqli_set_charset($con,'utf8');

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $result = mysqli_query($con,$val);

        $stt = 0;
        while($row = @mysqli_fetch_array($result))
        {
            //$write = array(
            
            $stt = $index - 1;
            
            $area     = My_Region::getValue($row['store_district'], My_Region::Area);
            $province = My_Region::getValue($row['store_district'], My_Region::Province);
            $district = My_Region::getValue($row['store_district'], My_Region::District);
            
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit($stt,PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($row['title'],PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($area,PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($province,PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($district,PHPExcel_Cell_DataType::TYPE_STRING);
            
            foreach($range as $key => $value){
                $sheet->getCell($alpha++ . $index)->setValueExplicit($row[$value],PHPExcel_Cell_DataType::TYPE_STRING);
            }
            
            $index++;  
        }
    
        $filename = ' claim ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    /* LEADER */
    public function analyticsLeaderAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'analytics-leader.php';
    }
    public function analyticsAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'analytics.php';
    }
    /* END LEADER */


    /* TRAINING ONLINE */
    public function trainingOnlineAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'training-online-area.php';
    }
    /* END TRAINING ONLINE */


    public function loadGoodByDateAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'ajax' . DIRECTORY_SEPARATOR .'load-good-by-date.php';
    }

    public function loadGoodByMonthAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'ajax' . DIRECTORY_SEPARATOR .'load-good-by-month.php';
    }

    private function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
    
        while( $current <= $last ) {
    
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
    
        return $dates;
    }

    private function month_range($first, $last, $step = '+1 month', $output_format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
    
        while( $current <= $last ) {
    
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
    
        return $dates;
    }

    public function eventTimeAction()
    {
        $id              = $this->getRequest()->getParam('id');
        $QTime = new Application_Model_Time();
        $data = $QTime->getTimeByStaffInCurrentMonth($id);
        $array_date = array();
        foreach($data as $value)
        {
            $array_date[] = array(
                'title' => 'Check-in',
                'start' => $value['date'],
                'end' => $value['date'],
            );
        }
        echo json_encode($array_date);
        die;
    }

    public function trainingReportAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'training' . DIRECTORY_SEPARATOR .'training-report-area.php';
    }

    public function trainerCourseAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'training' . DIRECTORY_SEPARATOR .'trainer-course-area.php';
    }

    public function trainerListAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'training' . DIRECTORY_SEPARATOR .'trainer-list.php';
    }

    public function trainerAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'training' . DIRECTORY_SEPARATOR .'trainer-area.php';
    }

    public function listFailAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'training' . DIRECTORY_SEPARATOR .'list-fail.php';
    }
	
	public function listRankAreaAction(){
        require_once 'bi'. DIRECTORY_SEPARATOR .'list-rank-area.php';
    }

    public function keyAcountAction(){
        require_once 'bi'. DIRECTORY_SEPARATOR .'key-acount.php';
    }

    public function rankStarAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'rank-star.php';
    }

    public function staffByTitleAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'staff-by-title.php';
    }
	
	public function staffNewMainAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'staff-new-main.php';
    }

    // public function listStoreAction(){
    //     require_once 'bi' . DIRECTORY_SEPARATOR . 'list-store.php';
    // }

    public function _exportAreaKa($data, $data_dealer){

        ini_set("memory_limit", -1);
        ini_set("display_error", 0);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            '#',
            'Area',
            'Unit',
            'Unit Activated',
            'Value (80%)',        

            'Dealer (TGD?) Unit',
            'Dealer Unit (TGD?) Activated', 
            'Dealer Value (TGD?) (80%)',

            'Other Unit',
            'Other Unit Activated', 
            'Other Value (80%)',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;


        $i = 1;

        foreach($data as $item){

            $dealer = !empty($data_dealer[$item['area_id']]) ? $data_dealer[$item['area_id']] : null;

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['total_quantity']);
            $sheet->setCellValue($alpha++.$index, $item['total_activated']);
            $sheet->setCellValue($alpha++.$index, $item['total_value']);

            $sheet->setCellValue($alpha++.$index, $dealer['total_quantity']);
            $sheet->setCellValue($alpha++.$index, $dealer['total_activated']);
            $sheet->setCellValue($alpha++.$index, $dealer['total_value']);

            $sheet->setCellValue($alpha++.$index, $item['total_quantity'] - $dealer['total_quantity']);
            $sheet->setCellValue($alpha++.$index, $item['total_activated'] - $dealer['total_activated']);
            $sheet->setCellValue($alpha++.$index, $item['total_value'] - $dealer['total_value']);

            $index++;

        }

        $filename = 'Area_Dealer_'.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public function rewardWarningAllAction(){    
        require_once 'bi'. DIRECTORY_SEPARATOR .'reward-warning-all.php';
    }

    public function channelKaAction(){    
        require_once 'bi'. DIRECTORY_SEPARATOR .'channel-ka.php';
    }
    //T
    public function listPgSelloutRegionAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-sellout-region.php';
    }
    public function listPgSelloutAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-sellout-area.php';
    }
    public function listPgSelloutStaffAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-sellout-staff.php';
    }


    public function rankStarRegionAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'rank-star-region.php';
    }
    public function rankStarAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'rank-star-area.php';
    }
    public function rankStarStaffAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'rank-star-staff.php';
    }

   public function listPgRegionAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-region.php';
    }
    public function listPgAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-area.php';
    }
    public function listPgStaffAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'list-pg-staff.php';
    }

    public function exportListPgSelloutRegionAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'export-list-pg-sellout-region.php';
    }
    public function exportListPgSelloutAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'export-list-pg-sellout-area.php';
    }
     public function exportListPgRegionAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'export-list-pg-region.php';
    }
    public function exportListPgAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'export-list-pg-area.php';
    }

    public function exportRankStarRegionAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'export-rank-star-region.php';
    }
    public function exportRankStarAreaAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'export-rank-star-area.php';
    }
    
    public function loadDataHrAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'load-data-hr.php';
    }
    public function rankStarStaffSalesAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'rank-star-staff-sales.php';
    }
    public function rankStarAreaSalesAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'rank-star-area-sales.php';
    }
    public function rankStarRegionSalesAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'rank-star-region-sales.php';
    }
    public function rankStarSalesAction(){
        require_once 'bi' . DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR .'rank-star-sales.php';
    }    
    public function saleOnlineAction(){
        require_once 'bi'. DIRECTORY_SEPARATOR .'sale-online.php';
    }
    public function channelOnlineAction(){
        require_once 'bi'. DIRECTORY_SEPARATOR .'channel-online.php';
    }

    public function exportStaffAllAction(){
        require_once 'bi'. DIRECTORY_SEPARATOR . 'staff' .DIRECTORY_SEPARATOR .'export-staff-all.php';
    }

}




















