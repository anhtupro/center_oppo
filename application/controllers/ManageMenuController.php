<?php
class ManageMenuController extends My_Controller_Action
{

    public $ids=array();

    public function indexAction(){


        $group_id = 1;
        if (isset($_GET['group_id'])) {
            $group_id = (int)$_GET['group_id'];
        }
        $menu = $this->get_menu($group_id);
        $menu_ul = '<ul id="easymm"></ul>';
        if ($menu) {

            $tree = new TreeMenu;

            foreach ($menu as $row) {
                $tree->add_row(
                    $row['id'],
                    $row['parent_id'],
                    ' id="menu-'.$row['id'].'" class="sortable"',
                    $this->get_label($row)
                );
            }

            $menu_ul = $tree->generate_list('id="easymm"');
        }

        $group_title = $this->get_menu_group_title($group_id);
        $menu_groups = $this->get_menu_groups();

        $this->view->menu_ul = $menu_ul;
        $this->view->group_id = $group_id;
        $this->view->group_title = $group_title;
        $this->view->menu_groups = $menu_groups;

    }
    public function savePositionAction() {
        if (isset($_POST['easymm'])) {
            $easymm = $_POST['easymm'];

            $this->update_position(0, $easymm);
        }
    }

    private function update_position($parent, $children) {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $QStaff = new Application_Model_Menu();

        $i = 1;
        foreach ($children as $k => $v) {
            $id = (int)$children[$k]['id'];
            $data[MENU_PARENT] = $parent;
            $data[MENU_POSITION] = $i;


            $data = array(
                'parent_id' => $parent,
                'position' => $i
            );


            $where = $QStaff->getAdapter()->quoteInto('id = ?', $id);
            $QStaff->update($data, $where);

//            $this->db->update(MENU_TABLE, $data, MENU_ID . ' = ' . $id);
            if (isset($children[$k]['children'][0])) {
                $this->update_position($id, $children[$k]['children']);
            }
            $i++;
        }
    }

    /**
     * Add menu action
     * For use with ajax
     * Return json data
     */
    public function addAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if (isset($_POST['title'])) {

            $QMenu = new Application_Model_Menu();

            $data[MENU_TITLE] = trim($_POST['title']);
            if (!empty($data[MENU_TITLE])) {
				$data[title_vie] = $_POST['title_vie'];
                $data[MENU_URL] = $_POST['url'];
				
                $data[MENU_CLASS] = $_POST['class'];
                $data[MENU_CLASS_NEW] = $_POST['class_new'];
                $data[MENU_GROUP] = $_POST['group_id'];
                $data[MENU_POSITION] = $this->get_last_position($_POST['group_id']) + 1;
                $data['hidden'] = $_POST['is_hidden'] == 0 ? null : $_POST['is_hidden'];
                $data['group_id'] = $_POST['group_id'];

                if ($id = $QMenu->insert($data)) {

                    $response['status'] = 1;
                    $li_id = 'menu-'.$id;
                    $response['li'] = '<li id="'.$li_id.'" class="sortable">'.$this->get_label($data).'</li>';
                    $response['li_id'] = $li_id;
                } else {
                    $response['status'] = 2;
                    $response['msg'] = 'Add menu error.';
                }
            } else {
                $response['status'] = 3;
            }



            header('Content-type: application/json');
            echo json_encode($response);
        }
    }

    /**
     * Show edit menu form
     */
    public function editAction() {
        $this->_helper->layout->disableLayout();
        if (isset($_GET['id'])) {
            $id = (int)$_GET['id'];
            $data = $this->get_row($id);

            $this->view->data=$data[0];
        }
    }


    /**
     * Save menu
     * Action for edit menu
     * return json data
     */
    public function saveAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (isset($_POST['title'])) {




                try {

                    $QMenu = new Application_Model_Menu();


                    $data[MENU_TITLE] = trim($_POST['title']);
                    if (!empty($data[MENU_TITLE])) {

                        $data = array(
						'title_vie' => $_POST['title_vie'],
                            'title' => $_POST['title'],
                            'url' => $_POST['url'],
                            'group_id' => $_POST['group_id'],
                            'class' => $_POST['class'],
                            'class_new' => $_POST['class_new'],
                            'hidden' => $_POST['is_hidden'] == '0' ? null : $_POST['is_hidden'],
                        );


                        $where = $QMenu->getAdapter()->quoteInto('id = ?', $_POST['menu_id']);
                        $QMenu->update($data, $where);

                        $response['status'] = 1;
                        $d['title'] = $data[MENU_TITLE];
						$d['title_vie'] = $data['title_vie'];
                        $d['url'] = $data[MENU_URL];
                        $d['klass'] = $data[MENU_CLASS]; //klass instead of class because of an error in js
                        $d['group_id'] = $data['group_id'];
                        $d['hidden'] = $data['hidden'];
                        $response['menu'] = $d;
                    }
                }
                catch (Exception $exception){

                    $response['status'] = 2;
                    $response['msg'] = 'Edit menu error.';

                }


            } else {
                $response['status'] = 3;
            }
            header('Content-type: application/json');

            echo json_encode($response);
        }


    /**
     * Delete menu action
     * Also delete all submenus under current menu
     * return json data
     */
    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $db = Zend_Registry::get('db');

        if (isset($_POST['id'])) {
            $id = (int)$_POST['id'];

            //Update by Tam 9/2020
            if($id == null) {
                die;
            }

            $this->get_descendants($id);
            if (!empty($this->ids)) {
                $ids = implode(', ', $this->ids);
                $id = "$id, $ids";
            }

            $sql = sprintf('DELETE FROM %s WHERE %s IN (%s)', MENU_TABLE, MENU_ID, $id);



            $stmt = $db->prepare($sql);

            $delete=$stmt->execute();




            $stmt->closeCursor();
            if ($delete) {
                $response['success'] = true;
            } else {
                $response['success'] = false;
            }
            header('Content-type: application/json');
            echo json_encode($response);
        }
    }
    /**
     * Get the highest position number
     *
     * @param int $group_id
     * @return string
     */



    private function get_last_position($group_id) {

        $db = Zend_Registry::get('db');

        $sql = "SELECT max(position) as max_position from menu where group_id=1";
        $stmt = $db->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();

        return $data['max_position'];
    }





    private function get_menu($group_id){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p' => MENU_TABLE),
                array('p.*'));

        $select->order('parent_id');
        $select->order('position');


        return $result = $db->fetchAll($select);
    }

    private function get_label($row) {
        $system = 'Center';
        if($row['group_id'] == 2)
            $system = 'Warehouse';
        elseif($row['group_id'] == 3)
            $system = 'Bi';
        elseif($row['group_id'] == 4)
            $system = 'Payment';
        
        $label =
            '<div class="ns-row">' .
            '<div class="ns-title">'.$row[MENU_TITLE].'</div>' .
            '<div class="ns-url">'.$row[MENU_URL].'</div>' .
            '<div class="ns-system">'.$system.'</div>' .
            '<div class="ns-class">'.$row[MENU_CLASS].'</div>' .
            '<div class="ns-hidden">';
            
        if($row['hidden'] == null)
            $label .= '';
        else
            $label .= 'Yes';

        $label .='</div>' .
            '<div class="ns-actions">' .
            '<a href="#" class="edit-new-menu" title="Edit Menu">' .
            '<img src="'.HOST.'Menu/templates/images/edit.png" alt="Edit">' .
            '</a>' .
            '<a href="#" class="delete-new-menu" title="Delete Menu">' .
            '<img src="'.HOST.'Menu/templates/images/cross.png" alt="Delete">' .
            '</a>' .
            '<input type="hidden" name="id" value="'.$row[MENU_ID].'">' .
            '</div>' .
            '</div>';
        return $label;
    }

    private function get_menu_group_title($group_id) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p' => 'menu_group'),
                array('title'));
        $select->where('id =?', $group_id);

        return $result = $db->fetchOne($select);

    }

    private function get_menu_groups() {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p' => 'menu_group'),
                array('title','id'));

        return $result = $db->fetchAll($select);
    }

    private function get_row($id) {

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p' => 'menu'),
                array('p.*'));
        $select->where('id =?', $id);

        return $result = $db->fetchAll($select);

    }


    /**
     * Recursive method
     * Get all descendant ids from current id
     * save to $this->ids
     *
     * @param int $id
     */
    private function get_descendants($id) {


        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p' => 'menu'),
                array('p.id'));
        $select->where('parent_id =?', $id);

        $data = $db->fetchAll($select);



        if (!empty($data)) {
            foreach ($data as $v) {
                $this->ids[] = $v['id'];
                $this->get_descendants($v);
            }
        }
    }
}

class TreeMenu {

    var $data;

    function add_row($id, $parent, $li_attr, $label) {
        $this->data[$parent][] = array('id' => $id, 'li_attr' => $li_attr, 'label' => $label);
    }


    function generate_list($ul_attr = '') {
        return $this->ul(0, $ul_attr);
    }


    function ul($parent = 0, $attr = '') {
        static $i = 1;
        $indent = str_repeat("\t\t", $i);
        if (isset($this->data[$parent])) {
            if ($attr) {
                $attr = ' ' . $attr;
            }

            $class = '';
            if ($parent > 0)
                $class = ' class="dropdown"';

            $html = "\n$indent";
            $html .= "<ul$attr $class>";
            $i++;
            foreach ($this->data[$parent] as $row) {
                $child = $this->ul($row['id']);
                $html .= "\n\t$indent";
                $html .= '<li'. $row['li_attr'] . '>';
                $html .= $row['label'];
                if ($child) {
                    $i--;
                    $html .= $child;
                    $html .= "\n\t$indent";
                }
                $html .= '</li>';
            }
            $html .= "\n$indent</ul>";
            return $html;
        } else {
            return false;
        }
    }


    function clear() {
        $this->data = array();
    }
}



class TreeMenuNew
{

    /**
     * variable to store temporary data to be processed later
     *
     * @var array
     */
    var $data;

    /**
     * Add an item
     *
     * @param int $id ID of the item
     * @param int $parent parent ID of the item
     * @param string $li_attr attributes for <li>
     * @param string $label text inside <li></li>
     */
    function add_row($id, $parent, $li_attr, $label)
    {
        $this->data[$parent][] = array('id' => $id, 'li_attr' => $li_attr, 'label' => $label);
    }

    /**
     * Generates nested lists
     *
     * @param string $ul_attr
     * @return string
     */
    function generate_list($ul_attr = '')
    {
        return $this->ul(0, $ul_attr);
    }

    /**
     * Recursive method for generating nested lists
     *
     * @param int $parent
     * @param string $attr
     * @return string
     */
    function ul($parent = 0, $attr = '')
    {
        static $i = 1;
        $indent = str_repeat("\t\t", $i);
        if (isset($this->data[$parent])) {
            if ($attr) {
                $attr = ' ' . $attr;
            }

            $class = '';
            if ($parent > 0)
                $class = ' class="level2"';

            $html = "\n$indent";
            $html .= "<ul$attr $class>";
            $i++;
            foreach ($this->data[$parent] as $row) {
                $child = $this->ul($row['id']);
                $html .= "\n\t$indent";
                $html .= '<li' . $row['li_attr'] . '>';
                $html .= $row['label'];
                if ($child) {
                    $i--;
                    $html .= $child;
                    $html .= "\n\t$indent";
                }
                $html .= '</li>';
            }
            $html .= "\n$indent</ul>";
            return $html;
        } else {
            return false;
        }
    }

    /**
     * Clear the temporary data
     *
     */
    function clear()
    {
        $this->data = array();
    }
}
