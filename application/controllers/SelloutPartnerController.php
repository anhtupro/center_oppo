<?php

class SelloutPartnerController extends My_Controller_Action {

    public function init() {
//        set_time_limit(0);
//        ini_set('memory_limit', -1);

        $this->_helper->layout->setLayout('layout_bi_new');
    }

    public function indexAction() {
        require_once 'sellout-partner' . DIRECTORY_SEPARATOR . 'index.php';
    }

    public function selloutMapAction() {
        require_once 'sellout-partner' . DIRECTORY_SEPARATOR . 'sellout-map.php';
    }

    public function reportSelloutPartner($select) {

        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $index = 1;

        $heads = array(
            '#',
            'REF',
            'MODEL',
            'IMEI',
            'BILL NUMBER',
            'BILL DATE',
            'STORE NAME',
            'STORE CODE',
            'CUSTOMER NAME',
            'CUSTOMER ADDRESS',
            'CUSTOMER PHONE',
            'NOTE',
            'CREATED AT'
        );

        $alpha = 'A';

        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $i     = 1;


        //get data
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();
        $con    = mysqli_connect($config['resources']['db']['params']['host'], $config['resources']['db']['params']['username'], My_HiddenDB::decryptDb($config['resources']['db']['params']['password']), $config['resources']['db']['params']['dbname']);
        mysqli_set_charset($con, 'utf8');

        // Check connection
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $result = mysqli_query($con, $select);
        //end get data

        while ($row = @mysqli_fetch_array($result)) {
            $alpha = 'A';

            $sheet->setCellValue($alpha++ . $index, $i++);
            $sheet->setCellValue($alpha++ . $index, $row['ref']);
            $sheet->setCellValue($alpha++ . $index, $row['model']);
            $sheet->setCellValueExplicit($alpha++ . $index, $row['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $row['bill_number']);
            $sheet->setCellValue($alpha++ . $index, date('H:i d/m/Y', strtotime($row['bill_date'])));
            $sheet->setCellValue($alpha++ . $index, $row['title']);
            $sheet->setCellValue($alpha++ . $index, $row['store_code']);
            $sheet->setCellValue($alpha++ . $index, $row['customer_name']);
            $sheet->setCellValue($alpha++ . $index, $row['customer_address']);
            $sheet->setCellValueExplicit($alpha++ . $index, $row['customer_phone'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $row['note']);
            $sheet->setCellValue($alpha++ . $index, date('H:i d/m/Y', strtotime($row['created_at'])));

            $index++;
        }

        $filename  = 'Sellout Partner - ' . date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

}
