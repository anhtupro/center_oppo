<?php
 
$department_id = $this->getRequest()->getPost('department_id');
$team_id = $this->getRequest()->getPost('team_id');

$QPurchasingType  	= new Application_Model_PurchasingType();
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();

$response=array('status'=>0);
if (!empty($department_id)) {
     
    $where= [];
    $where[] = $QPurchasingType->getAdapter()->quoteInto('department_id = ?', $department_id);
    $where[] = $QPurchasingType->getAdapter()->quoteInto('team_id IS NULL');
    $data_arr = $QPurchasingType->fetchAll($where);

    if(!empty($data_arr)){
        $response['data']= $data_arr->toArray(); 
        $response['status']= 1; 
        $response['message']="Thành công"; 
    }
    
}elseif(!empty($team_id)){
    
    $where= [];
    $where[] = $QPurchasingType->getAdapter()->quoteInto('team_id = ?', $team_id);
    $data_arr = $QPurchasingType->fetchAll($where);

    if(!empty($data_arr)){
        $response['data']= $data_arr->toArray(); 
        $response['status']= 1; 
        $response['message']="Thành công"; 
    }
    
}
else{
    $response['message']="Thiếu id";
}
echo json_encode($response);
exit();