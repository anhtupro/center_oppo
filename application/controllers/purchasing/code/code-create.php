<?php

$QPmodelCode				= new Application_Model_PurchasingCode();
$id			= $this->getRequest()->getParam('id');

if ($id) { // load for editing
	$rowset				= $QPmodelCode->find($id);
	$code				= $rowset->current();
	$this->view->code	= $code;
	// echo "<pre>";print_r($code);die;
}else{
    $db = Zend_Registry::get('db');
    $select = $db->select()->from(array('p' => 'pmodel_code'), array('new_code' => 'MAX(p.code)+1','length_code' => 'LENGTH(MAX(`code`)+1)'));
    $result = $db->fetchRow($select);
    
    $i = 1; $string_new_code = '';
    while ($i <= (9-$result['length_code'])){  // Tổng length của chuỗi code là 9, length của number code là $result['length_code']
        $string_new_code .= '0';
        $i++;
    }
    $string_new_code.= $result['new_code'];

    $code['code'] = $string_new_code;
    $this->view->code = $code;
    
}

$PCModel				= new Application_Model_PurchasingCategory();
$categorys				= $PCModel->fetchAll();
$this->view->category	= $categorys;


$flashMessenger			= $this->_helper->flashMessenger;
$messages				= $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages	= $messages;

$this->_helper->viewRenderer->setRender('code/code-create');