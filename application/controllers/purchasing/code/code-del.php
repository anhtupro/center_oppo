<?php
$flashMessenger   = $this->_helper->flashMessenger;

$id = $this->getRequest()->getParam('id');
$flag = FALSE;

if ($id) {
	$CModel	= new Application_Model_PurchasingCode();
	$rowset	= $CModel->find($id);
	$code	= $rowset->current();

	if ($code) {
		$where = $CModel->getAdapter()->quoteInto('id = ?', $id);
		$CModel->delete($where);
		$messages_success = $flashMessenger->setNamespace('success')->addMessage('Success');
		$flag = TRUE;
	}
}

if (!$flag)
	$messages = $flashMessenger->setNamespace('error')->addMessage('Invalid ID');

$this->_redirect(HOST.'purchasing/code');