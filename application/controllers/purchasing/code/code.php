<?php

$page			= $this->getRequest()->getParam('page', 1);
$code			= $this->getRequest()->getParam('code', '');
$name			= $this->getRequest()->getParam('name', '');
$desc			= $this->getRequest()->getParam('desc', '');
$category_id	= $this->getRequest()->getParam('category_id', '');
$export                 = $this->getRequest()->getParam('export',0);

$limit		= LIMITATION;
$total		= 0;

$params = array(
	'code'			=> trim($code),
	'name'			=> trim($name),
	'desc'			=> trim($desc),
	'category_id'	=> $category_id
);

$CModel				= new Application_Model_PurchasingCode();

$codes				= $CModel->fetchCode($page, $limit, $total, $params);
$this->view->codes	= $codes;


$PCModel				= new Application_Model_PurchasingCategory();
$categorys				= $PCModel->fetchAll();
$this->view->category	= $categorys;

$PCModel				= new Application_Model_PurchasingCategory();
$categorys				= $PCModel->get_cache();
$this->view->categorys	= $categorys;


$this->view->params	= $params;
$this->view->limit	= $limit;
$this->view->total	= $total;
$this->view->url	= HOST.'purchasing/code/'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset	= $limit*($page-1);


$flashMessenger					= $this->_helper->flashMessenger;
$messages_success				= $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success	= $messages_success;
$messages						= $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages			= $messages;

$this->_helper->viewRenderer->setRender('code/index');


if($export==1){
		// no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
		
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'NO.',
            'CODE',
            'NAME',
            'SUB NAME',
            'DESC',
            'CATEGORY'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $intCount = 1;       
  
        $data = $CModel->fetchCode($page, null, $total, $params);
     
        try
        {
            if ($data)
                foreach ($data as $_key => $_order)
                {                
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['sub_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['desc'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($categorys[$_order['category_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
                }
        }
        catch (exception $e)
        {
            exit;
        }

        $filename = 'Report_Product_' . date('Y_m_d');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
}