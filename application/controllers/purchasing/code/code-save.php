<?php
$flashMessenger = $this->_helper->flashMessenger;

if ($this->getRequest()->getMethod() == 'POST') {
	$id				= $this->getRequest()->getParam('id');
	$code			= $this->getRequest()->getParam('code', '');
	$name			= $this->getRequest()->getParam('name', '');
	$sub_name		= $this->getRequest()->getParam('sub_name', '');
	$unit			= $this->getRequest()->getParam('unit', '');
	$desc			= $this->getRequest()->getParam('desc', '');
	$category_id	= $this->getRequest()->getParam('category_id', '');
	$disable	= $this->getRequest()->getParam('disable', 0);


	if ($code == '' || $name == '' || $desc == '' || $category_id == '' || $unit == '') {
            $flashMessenger->setNamespace('error')->addMessage('Invalid Info');
            $this->_redirect(HOST.'purchasing/code-create');
	}

	$data = array(
		'code'			=> $code,
		'name'			=> $name,
		'sub_name'		=> $sub_name,
		'unit'			=> $unit,
		'desc'			=> $desc,
		'category_id'           => $category_id,
                'status'                => $disable
	);
            $data['status'] = $disable;
	$CModel				= new Application_Model_PurchasingCode();

	if ($id) { // save
            $where = $CModel->getAdapter()->quoteInto('id = ?', $id);
            $CModel->update($data, $where);
	} else { // create new
            
            $where_check = $CModel->getAdapter()->quoteInto('code = ?', $code);
            $code_check = $CModel->fetchRow($where_check);
            if(empty($code_check)){
                $id = $CModel->insert($data);                
            }else{
                $flashMessenger->setNamespace('error')->addMessage('LỖI: Code= '.$code.' Trùng. Vui lòng nhập lại thông tin để tạo lại.');
                $this->_redirect(HOST.'purchasing/code-create');                
            }
	}
	
	$flashMessenger->setNamespace('success')->addMessage('Success');
	$this->_redirect(HOST.'purchasing/code');

} else {
	$flashMessenger->setNamespace('error')->addMessage('Wrong Action.');
	$this->_redirect(HOST.'purchasing/code-create');
}