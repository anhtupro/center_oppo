<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QPurchasingRequest        = new Application_Model_PurchasingRequest();
$QPurchasingProject        = new Application_Model_PurchasingProject();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
$QStaffPermission          = new Application_Model_StaffPermission();
$QSupplier                 = new Application_Model_Supplier();
$QTeam                     = new Application_Model_Team();
$department                = $QTeam->get_list_department();
$this->view->department    = $department;

$cache_Supplier             = $QSupplier->get_cache();
$this->view->cache_Supplier = $cache_Supplier;

$cache_Project             = $QPurchasingProject->get_cache();
$this->view->cache_Project = $cache_Project;

$sn          = $this->getRequest()->getParam('sn', null);
$name        = $this->getRequest()->getParam('name', null);
$supplier_id = $this->getRequest()->getParam('supplier_id', null);
$urgent_from = $this->getRequest()->getParam('urgent_from', null);
$urgent_to   = $this->getRequest()->getParam('urgent_to', null);
$project_id  = $this->getRequest()->getParam('project_id', null);


$params = array(
    'sn'          => trim($sn),
    'name'        => $name,
    'supplier_id' => $supplier_id,
    'urgent_from' => $urgent_from,
    'urgent_to'   => $urgent_to,
    'project_id'  => $project_id
);


$this->view->params = $params;

$listData             = $QPurchasingRequestDetails->getCreateSubmissionSupplier($params);
$this->view->listData = $listData;

$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages       = $messages;
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;
