<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QPmodelCode               = new Application_Model_PmodelCode();
$QPurchasingRequest        = new Application_Model_PurchasingRequest();
$QPurchasingProject        = new Application_Model_PurchasingProject();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
$QStaffPermission          = new Application_Model_StaffPermission();
$QPurchasingType           = new Application_Model_PurchasingType();
$QTeam                     = new Application_Model_Team();
$QSupplier                 = new Application_Model_Supplier();
$QArea                     = new Application_Model_Area();
$QRequestConfirm           = new Application_Model_RequestConfirm();

$department                  = $QTeam->get_list_department();
$this->view->department      = $department;
$list_team = $QTeam->get_cache_team();
$purchasing_type             = $QPurchasingType->get_cache();
$this->view->purchasing_type = $purchasing_type;
$this->view->cache_pmodel_code = $QPmodelCode->cache_pmodel_code();
$this->view->supplier          = $QSupplier->get_cache();


$sort           = $this->getRequest()->getParam('sort', '');
$desc           = $this->getRequest()->getParam('desc', 1);
$sn             = $this->getRequest()->getParam('sn');
$name           = $this->getRequest()->getParam('name');
$type           = $this->getRequest()->getParam('type');
$project_id     = $this->getRequest()->getParam('project_id');
$pmodel_code_id = $this->getRequest()->getParam('pmodel_code_id');
$supplier_id    = $this->getRequest()->getParam('supplier_id');
$urgent_from   = $this->getRequest()->getParam('urgent_from');
$urgent_to     = $this->getRequest()->getParam('urgent_to');
$create_from   = $this->getRequest()->getParam('create_from');
$create_to     = $this->getRequest()->getParam('create_to');
$status        = $this->getRequest()->getParam('status');
$department_id = $this->getRequest()->getParam('department_id');

$export = $this->getRequest()->getParam('export', 0);

$params = array(
    'sn'             => trim($sn),
    'name'           => trim($name),
    'type'           => $type,
    'project_id'     => $project_id,
    'pmodel_code_id' => $pmodel_code_id,
    'urgent_from'    => $urgent_from,
    'urgent_to'      => $urgent_to,
    'create_from'    => $create_from,
    'create_to'      => $create_to,
    'status'         => $status,
    'department_id'  => $department_id,
    'supplier_id'   => $supplier_id,
);

////////////PHÂN QUYỀN/////////////////////////
// Lấy phòng ban mà manager or leader quản lý
//Loại bà Dung ra
$AccessPurchasingRequest = $QStaffPermission->getPermissionPurchasingRequest($userStorage->id);
$access_department_arr   = array();
if (!empty($AccessPurchasingRequest) AND $userStorage->id != 12719) {
    foreach ($AccessPurchasingRequest as $k => $val) {
        $access_department_arr[] = $val['department_id'];
    }
}

// Phân quyền
if ( ($userStorage->id == 7)//thuy.to
        || ($userStorage->id == 14766)//quyen.le
        || ($userStorage->team == TEAM_PURCHASING)) {
    //Thấy full danh sách PR
    $this->view->edit_approve = 1;// và có quyền edit sau khi user approve
}elseif (!empty($access_department_arr)) { // quản lý phòng ban thấy PB của mình
    //$params['access_department_arr'] = $access_department_arr;
    $params['create_by'] = $userStorage->id;
}else {
    //chỉ thấy của mình
    $params['create_by'] = $userStorage->id;
}

//kiểm tra phân quyền trong request view
$request_view = $QRequestConfirm->getRequestView($userStorage->id);

if(!empty($request_view['department_id'])){
    $params['access_department_arr'] = $request_view['department_id'];
    $params['create_by'] = NULL;
}

$page   = $this->getRequest()->getParam('page', 1);
$limit  = LIMITATION;
$total  = 0;
$result = $QPurchasingRequest->fetchPagination($page, $limit, $total, $params); // DATAaaaaaaaaaaaaaaaaaaaaaaaaa

//xem chi tiết
$data_details = $data_total_sn = $data_change = array();
foreach ($result as $key => $value) {
    $details                    = $QPurchasingRequestDetails->getDetails($value['sn']);
    $data_details[$value['sn']] = $details;

    $total_num_sn = $total_price_sn = $total_price_sn_start = 0;
    if(!empty($details)){
        foreach($details as $k=>$v){
            
            $quantity = $v['quantity_real'] ? $v['quantity_real'] : $v['quantity'];
            
            $total_num_sn = $total_num_sn + $quantity;

            $money_ck = $price_after_ck = $total_price = 0;
            $money_ck = $v['price']*$v['ck']/100;
            $price_after_ck = $v['price'] - $money_ck;
            $total_price = $quantity*$price_after_ck+$v['fee'];
            $total_price_sn = $total_price_sn + $total_price;
            
            
            $total_price_start = $v['quantity_start']*$price_after_ck+$v['fee'];
            $total_price_sn_start = $total_price_sn_start + $total_price_start;

        }
        $data_total_sn[$v['sn']] = $total_price_sn;
        $data_change[$v['sn']] = $total_price_sn_start;
    }
       
}




///Tổng chi phí của $project_id
if(!empty($project_id)){

    $db = Zend_Registry::get('db');
    $select_pj = $db->select(); 
    $select_pj = " SELECT SUM(IFNULL(prs.price, 0) * IFNULL(p.quantity, 0)) AS `total` 
                FROM`purchasing_request_details` AS `p`
                LEFT JOIN `purchasing_request` AS `r` ON r.id = p.pr_id
                LEFT JOIN `pmodel_code` AS `c` ON c.id = p.pmodel_code_id
                LEFT JOIN ".DATABASE_SALARY.".`purchasing_request_supplier` AS `prs` ON p.id = prs.pr_detail_id AND prs.select_supplier = 1
                LEFT JOIN `supplier` AS `sup` ON prs.supplier_id = sup.id
                WHERE r.project_id = ".$project_id;
    $total_project = $db->fetchOne($select_pj); 

    $this->view->total_project        = $total_project;
}


//unset($params['list_staff']);


$this->view->project      = $QPurchasingProject->fetchAll();
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->desc         = $desc;
$this->view->sort         = $sort;
$this->view->url          = HOST . 'purchasing/purchasing-request' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);
$this->view->userStorage  = $userStorage;
$this->view->data_details = $data_details;
$this->view->data_total_sn = $data_total_sn;
$this->view->data_change = $data_change;

$QTrainerTypeAsset = new Application_Model_TrainerTypeAsset();
$whereTypeAsset    = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
$type_asset        = $QTrainerTypeAsset->fetchAll($whereTypeAsset);
$arrayTypeAsset    = array();
if ($type_asset->count()) {
    foreach ($type_asset as $key => $value) {
        $arrayTypeAsset[$value['id']] = $value['name'];
    }
}

$this->view->type_asset = $arrayTypeAsset;

$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages       = $messages;
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;




if ($export == 1) {

    //cache
    $project_cache_all  = $QPurchasingProject->get_cache_all();
    $type_cache_all     = $QPurchasingType->get_cache_all();
    $areas_cache_all    = $QArea->get_cache();
    $supplier_cache_all = $QSupplier->get_cache_all();
    $status_pr          = unserialize(STATUS_PR);
 

    // no limit time
    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'NO.',
        'Sn',
        'Type',
        'Urgent date',
        'Project',
        'Status',
        'Area',
        'department',
        'team',
        'Remark',
        'shipping_address',
        
        'product_name',
        'unit',
        'quantity',
        'installation_at',
        'installation_time',
         
        'supplier_id',
        'price_before_dp',
        'price',
        'ck',
        'fee',
        'article',
        'warranty',
        'discount',
        'article_other',
        
        'Created By',
        'Created At',
        'Confirm By',
        'Confirm At',
        'Approved by',
        'Approved At'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;

    $params['export'] = $export;
    $data = $QPurchasingRequest->fetchPagination($page, $limit, $total, $params);

    try {
        if ($data)
            foreach ($data as $_key => $_order) {

                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['sn'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($type_cache_all[$_order['type']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['urgent_date'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['project_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($status_pr[$_order['status']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($areas_cache_all[$_order['area_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($department[$_order['department_id']], PHPExcel_Cell_DataType::TYPE_STRING);                
                $sheet->getCell($alpha++ . $index)->setValueExplicit($list_team[$_order['team_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['remark'], PHPExcel_Cell_DataType::TYPE_STRING);                 
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['shipping_address'], PHPExcel_Cell_DataType::TYPE_STRING);     
                
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['product_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['unit'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['quantity'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['installation_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['installation_time'], PHPExcel_Cell_DataType::TYPE_STRING);
 
                $sheet->getCell($alpha++ . $index)->setValueExplicit($supplier_cache_all[$_order['supplier_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['price_before_dp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['ck'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['fee'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['article'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['warranty'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['discount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['article_other'], PHPExcel_Cell_DataType::TYPE_STRING);
       
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['created_by'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['created_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['confirm_by'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['confirm_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['approved_by'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['approved_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename  = 'Report_PR_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}