<?php
 
$id = $this->getRequest()->getPost('id');
$QPurchasingCostFile  	= new Application_Model_PurchasingCostFile();
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();

$response=array('status'=>0);
if (!empty($id)) {
     
    $where = $QPurchasingCostFile->getAdapter()->quoteInto('id = ?', $id);
    $File_row = $QPurchasingCostFile->fetchRow($where);
 
        if(!empty($File_row)){
            
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-cost' .
                DIRECTORY_SEPARATOR . $File_row['sn'] .DIRECTORY_SEPARATOR. $File_row['pr_quotation'];
           
             unlink($uploaded_dir);
             $QPurchasingCostFile->delete($where);
        }    
    $response['status']= 1; 
    $response['message']="Thành công"; 
}else{
    $response['message']="Thiếu id";
}
echo json_encode($response);
exit();