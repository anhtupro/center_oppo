<?php
$flashMessenger   = $this->_helper->flashMessenger;

$id = $this->getRequest()->getParam('id');
$flag = FALSE;

if ($id) {
	$PCModel				= new Application_Model_PurchasingCategory();
	$rowset = $PCModel->find($id);
	$category = $rowset->current();

	if ($category) {
		$where = $PCModel->getAdapter()->quoteInto('id = ?', $id);
		$PCModel->delete($where);
		$messages_success = $flashMessenger->setNamespace('success')->addMessage('Success');
		$flag = TRUE;
	}
}

if (!$flag)
	$messages = $flashMessenger->setNamespace('error')->addMessage('Invalid ID');

$this->_redirect(HOST.'purchasing/category');