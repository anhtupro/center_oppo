<?php


$PCModel				= new Application_Model_PurchasingCategory();
$categorys				= $PCModel->fetchAll();
$this->view->categorys	= $categorys;

$flashMessenger					= $this->_helper->flashMessenger;
$messages_success				= $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success	= $messages_success;
$messages						= $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages			= $messages;

$this->_helper->viewRenderer->setRender('category/index');

