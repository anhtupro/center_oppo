<?php

$id = $this->getRequest()->getParam('id');

if ($id) { // load for editing
	$PCModel				= new Application_Model_PurchasingCategory();
	$rowset					= $PCModel->find($id);
	$category				= $rowset->current();
	
	$this->view->category	= $category;
}

$flashMessenger = $this->_helper->flashMessenger;
$messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$this->_helper->viewRenderer->setRender('category/category-create');