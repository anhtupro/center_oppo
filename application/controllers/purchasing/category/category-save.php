<?php
$flashMessenger = $this->_helper->flashMessenger;

if ($this->getRequest()->getMethod() == 'POST') {
	$id       = $this->getRequest()->getParam('id');
	$name     = $this->getRequest()->getParam('name', '');
	$desc     = $this->getRequest()->getParam('desc', '');

	if ($name == '' || $desc == '') {
        $flashMessenger->setNamespace('error')->addMessage('Invalid Info');
        $this->_redirect(HOST.'purchasing/category-create');
	}

	$data = array_filter( array(
		'name'     => $name,
		'desc'	   => $desc
	));

	$PCModel				= new Application_Model_PurchasingCategory();

	if ($id) { // save
		$where = $PCModel->getAdapter()->quoteInto('id = ?', $id);
		$PCModel->update($data, $where);
	} else { // create new
		$id = $PCModel->insert($data);
	}

	

	$flashMessenger->setNamespace('success')->addMessage('Success');
	$this->_redirect(HOST.'purchasing/category');

} else {
	$flashMessenger->setNamespace('error')->addMessage('Wrong Action.');
	$this->_redirect(HOST.'product/category-create');
}