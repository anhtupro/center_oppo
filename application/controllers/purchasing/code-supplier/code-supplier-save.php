<?php
$flashMessenger = $this->_helper->flashMessenger;
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$db = Zend_Registry::get('db');
 

if ($this->getRequest()->getMethod() == 'POST') {
    
	$code_name		= $this->getRequest()->getParam('code_name');  
	$code			= $this->getRequest()->getParam('code', array());
	$id_code		= $this->getRequest()->getParam('code_id');
	$supplier		= $this->getRequest()->getParam('supplier', array());
	$supplierHidden         = $this->getRequest()->getParam('supplier_hidden', array());
	$price			= $this->getRequest()->getParam('price', array());
	$priceDefault           = $this->getRequest()->getParam('price_default', array());
	$default		= $this->getRequest()->getParam('valueDefault', array());
        $select_default		= $this->getRequest()->getParam('default', array());
    
        $dem_default = count($select_default);
   
        if($dem_default>1){
                echo '<script>
                        parent.palert("Mỗi sản phẩm chỉ chọn Default cho 1 nhà cung cấp.");
                        parent.alert("Mỗi sản phẩm chỉ chọn Default cho 1 nhà cung cấp.");
                    </script>';
                exit;
       } 
        
	$code_supplier	= $this->getRequest()->getParam('code_supplier_id', array());
	 
	$codeModel	= new Application_Model_PurchasingCode();
	$supplierModel	= new Application_Model_PurchasingSupplier();

	$CModel             = new Application_Model_CodeSupplier();
	$QLogCodeSupplier   =  new Application_Model_LogCodeSupplier();

	$getCode    = $codeModel->get_cache();
	$getSupplier = $supplierModel->get_cache();

	$arrayUniqueDefault	= array_unique($default);
	$arrayUniqueCode	= array_unique($code);
        
	$countDefault		= count(array_diff($default,array($arrayUniqueDefault[1])));
	$countNotDefault	= count(array_diff($default,array($arrayUniqueDefault[0])));

	$db->beginTransaction();

	if (count($arrayUniqueDefault) == 1 && $arrayUniqueDefault[0] == 2){
		echo '<script>
                        parent.palert("Chưa chọn nhà cung cấp mặc định cho Code. Vui lòng kiểm tra lại.");
                        parent.alert("Chưa chọn nhà cung cấp mặc định cho Code. Vui lòng kiểm tra lại.");
                    </script>';
                exit;
	}
           


	$where = $CModel->getAdapter()->quoteInto('code_id = ?', $id_code);
	$codeSupplier = $CModel->fetchAll($where);

        ////// Delete những dòng đã bỏ đi        
        
	foreach ($codeSupplier as $key => $value) {
		$where_del = $CModel->getAdapter()->quoteInto('id = ?', $value['id']);
		if (!in_array($value['id'],$code_supplier)){
			$CModel->delete($where_del);
		}
	}
        
        $where_having = array();
        $where_having[]  = $CModel->getAdapter()->quoteInto('code_id IN (?)', $code);
        $where_having[]  = $CModel->getAdapter()->quoteInto('supplier_id is not null', null);
        $CModel_having_data = $CModel->fetchAll($where_having);
 
	foreach ($code as $key => $value){
 
		$where = array();
                $where[] = $CModel->getAdapter()->quoteInto('code_id = ?', $value);
                $where[] = $CModel->getAdapter()->quoteInto('supplier_id = ?', $supplier[$key]);
		$checkCodeSupplier = $CModel->fetchAll($where);
                
                if(!empty($checkCodeSupplier)){
                    if(count($checkCodeSupplier) > 0 ){ 
                        echo '<script>
                                parent.palert("Mỗi Code chỉ được tạo cho 1 Nhà cung cấp duy nhất, không thể trùng.");
                                parent.alert("Mỗi Code chỉ được tạo cho 1 Nhà cung cấp duy nhất, không thể trùng.");
                            </script>';
                        exit;
                    }
                }

 
		if(!empty($code_supplier[$key])){
			if(My_Number::floatval($price[$key]) != My_Number::floatval($priceDefault[$key])){

                            $now = date('Y-m-d H:i:s');
			    $where_cs  = $CModel->getAdapter()->quoteInto('id = ?', $code_supplier[$key]);
			    $CModel->update(array('price' => My_Number::floatval($price[$key]),'default' => $default[$key]), $where_cs);

			    $where_log = array();
			    $where_log[] = $QLogCodeSupplier->getAdapter()->quoteInto('code_supplier_id = ?', $code_supplier[$key]);
			    $where_log[] = $QLogCodeSupplier->getAdapter()->quoteInto('to_date IS NULL', NULL);
			    $QLogCodeSupplier->update(array('to_date' => $now), $where_log);

			    $data_log = array(
			     'code_supplier_id' => $code_supplier[$key],
			     'price'      => My_Number::floatval($price[$key]),
			     'from_date'     => $now
			    );
			    $QLogCodeSupplier->insert($data_log);

			} else {
                            $where_cs  = $CModel->getAdapter()->quoteInto('id = ?', $code_supplier[$key]);
			    $CModel->update(array('default' => $default[$key]), $where_cs);
			}
                         
		} else {
                    
                    if(!empty($CModel_having_data->toArray())){
			$data_insert = array(
				'code_id'	=> $value,
				'supplier_id'	=>  $supplier[$key - count($supplierHidden) + count($supplier)],
				'price'		=> My_Number::floatval($price[$key]),
				'default'	=> $default[$key]
			);                       
                    }else{
			$data_insert = array(
				'code_id'	=> $value,
				'supplier_id'	=> $supplier[$key],
				'price'		=> My_Number::floatval($price[$key]),
				'default'	=> $default[$key]
			);                        
                    }
                    
                    
			// print_r($data_insert);
			$CModel->insert($data_insert);
		}
	}


	$db->commit();
        
        $pmodel_code_id = $arrayUniqueCode['0'];       
        
	$QPmodelCode             = new Application_Model_PmodelCode();
	$where_re = $QPmodelCode->getAdapter()->quoteInto('id = ?', $pmodel_code_id);
	$CModel_row = $QPmodelCode->fetchRow($where_re);
         
        
                echo '<script style="">
                        parent.palert("DONE."); 
                    </script>';
              // load lại trang
            $back_url =  "code-supplier-create?code_name=".$CModel_row['code']."&code_id=".$CModel_row['id'];
            echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';

} else {
	$flashMessenger->setNamespace('error')->addMessage('Wrong Action.');
	$this->_redirect(HOST.'purchasing/code-supplier');
}

