<?php

$page		= $this->getRequest()->getParam('page', 1);
$supplier	= $this->getRequest()->getParam('supplier', '');
$code		= $this->getRequest()->getParam('code', '');
$code_id	= $this->getRequest()->getParam('code_id', '');
$price		= $this->getRequest()->getParam('price', '');
$default	= $this->getRequest()->getParam('default', '');
$export         = $this->getRequest()->getParam('export',0);


$limit		= LIMITATION;
$total		= 0;

$params = array(
	'supplier'	=> trim($supplier),
	'code'		=> trim($code),
	'code_id'		=> trim($code_id),
	'price'		=> trim($price),
	'default'	=> $default
);

$CPModel					= new Application_Model_CodeSupplier();
$codesuppliers				= $CPModel->fetchCodeSupplier($page, $limit, $total, $params);
$this->view->codesuppliers	= $codesuppliers;

$this->view->params	= $params;
$this->view->limit	= $limit;
$this->view->total	= $total;
$this->view->url	= HOST.'purchasing/code-supplier/'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset	= $limit*($page-1);

// $suppliers				= $SModel->fetchAll();

$flashMessenger					= $this->_helper->flashMessenger;
$messages_success				= $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success	= $messages_success;
$messages						= $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages			= $messages;

$this->_helper->viewRenderer->setRender('code-supplier/index');


if($export==1){
		// no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
		
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'NO.',
            'CODE',
            'PRODUCT',
            'SUPPLIER',
            'PRICE',
            'DEFAULT'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $intCount = 1;       
  
        $data = $CPModel->fetchCodeSupplier($page, null, $total, $params);
     
        try
        {
            if ($data)
                foreach ($data as $_key => $_order)
                {              
                
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['code'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['supplier'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['default'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
                    
                }
        }
        catch (exception $e)
        {
            exit;
        }

        $filename = 'Purchasing_Price_' . date('Y_m_d');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
}