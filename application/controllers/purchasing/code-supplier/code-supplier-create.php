<?php

$code	= $this->getRequest()->getParam('code_name');
$code_id	= $this->getRequest()->getParam('code_id');
// echo $code;die;
$this->view->code_id = $code_id;
$this->view->code = $code;

if ($code) { // load for editing
	$CSModel				= new Application_Model_CodeSupplier();
	$rowset					= $CSModel->fetchRowCode($code);
	$this->view->codeSupplierAll	= $rowset;
	// echo "<pre>";print_r($rowset);die;
}


$SModel					= new Application_Model_PurchasingSupplier();
$suppliers				= $SModel->fetchAll();
$this->view->supplier	= $suppliers;

$CModel					= new Application_Model_PurchasingCode();
$where					= $CModel->getAdapter()->quoteInto('code = ?',$code);
$codes					= $CModel->fetchAll($where);
$this->view->code		= $codes;

// echo "<pre>";print_r($codes);die;

$flashMessenger			= $this->_helper->flashMessenger;
$messages				= $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages	= $messages;

$this->_helper->viewRenderer->setRender('code-supplier/code-supplier-create');