<?php

$code_id = $this->getRequest()->getParam('code_id');
$supplier_id = $this->getRequest()->getParam('supplier_id');

$QCodeSupplier = new Application_Model_CodeSupplier();

try {
    if ($code_id) {
        $where = array();
        $where[] = $QCodeSupplier->getAdapter()->quoteInto('code_id = ?', $code_id);
        $where[] = $QCodeSupplier->getAdapter()->quoteInto('supplier_id = ?', $supplier_id);
        $where[] = $QCodeSupplier->getAdapter()->quoteInto('status = 0', NULL);
        $CodeSupplier = $QCodeSupplier->fetchRow($where);
         
        if(!empty($CodeSupplier)){
            echo json_encode(array('supplier_price' => $CodeSupplier->toArray()));     
            exit;
        }
        
        echo json_encode(array());
        exit;
    } else {
        echo json_encode(array());
        exit;
    }
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}
