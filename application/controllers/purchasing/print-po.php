<?php 
$this->_helper->layout->disableLayout();  
$sn                     = $this->getRequest()->getParam('sn'); 
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();


$QPurchasingOrder	    = new Application_Model_PurchasingOrder();
$QPurchasingOrderDetails    = new Application_Model_PurchasingOrderDetails();
$QSupplier                  = new Application_Model_Supplier();
$supplier                   = $QSupplier->get_cache();
$QTeam = new Application_Model_Team();
$department = $QTeam->get_department();
$this->view->department = $department;
$QArea = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();

if ($sn) {

    $params = array(
        'sn'              	=> $sn,
    );


    $purchasing_order = $QPurchasingOrder->getinfoPO($params);
    $this->view->purchasing_order = $purchasing_order;//PHIẾU ĐỀ NGHỊ THANH TOÁN

    $result         = $QPurchasingOrderDetails->getPurchasingOrderDetails($params);//PO

    $total_price_PO = 0;
    foreach ($result as $value) {
        $total_price_PO = $total_price_PO + $value['quantity']*$value['price']*1.1;
    }


    $this->view->total_price_PO= $total_price_PO;
    $this->view->supplier= $supplier;
    $this->view->list    = $result;
    $this->view->params  = $params;

}
