<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';


$db = Zend_Registry::get('db');
$db->beginTransaction();
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();

try
{

	$QPurchasingOrder  		= new Application_Model_PurchasingOrder();
	$QPurchasingOrderDetails 	= new Application_Model_PurchasingOrderDetails();
	$QPurchasingRequest  		= new Application_Model_PurchasingRequest();
	$QPurchasingRequestDetails      = new Application_Model_PurchasingRequestDetails();
	$userStorage    	 	= Zend_Auth::getInstance()->getStorage()->read();

	$sn 				= $this->getRequest()->getParam('sn');

	$po_order = array(
		'confirm_by'	=> $userStorage->id,
		'confirm_at'	=> date('Y-m-d H:i:s'),
		'status'	=> 'Delivery'
	);

	$where = $QPurchasingOrder->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_order = $QPurchasingOrder->fetchRow($where);

	if(!$purchasing_order){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : SN không tồn tại</div>';
		exit;
	}

	if(!empty($purchasing_order['confirm_by'])){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : PO đã được confirm</div>';
		exit;
	}

	$QPurchasingOrder->update($po_order, $where);

	//Update order details cũ để insert lại
	$where_details = $QPurchasingOrderDetails->getAdapter()->quoteInto('po_id = ?', $purchasing_order['id']);
	$purchasing_order_details = $QPurchasingOrderDetails->fetchAll($where_details);
	foreach($purchasing_order_details as $key=>$value){
		$where_pr = $QPurchasingRequestDetails->getAdapter()->quoteInto('id = ?', $value['pr_details_id']);
		$purchasing_request_details = $QPurchasingRequestDetails->fetchRow($where_pr);

		$pr_update = array(
			'status'	=> 	'Delivery'
		);
		$QPurchasingRequestDetails->update($pr_update, $where_pr);

		//Update Purchasing request sang trạng thái delivery
		$where_request = $QPurchasingRequest->getAdapter()->quoteInto('id = ?', $purchasing_request_details['purchasing_request_id']);
		$QPurchasingRequest->update($pr_update, $where_request);
		//END
	}

	//END 

	$db->commit();
	echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
	echo '<div class="alert alert-success">Done</div>';
}
catch (Exception $e)
{
	$db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi : '.$e->getMessage().'</div>';	
}
exit;