<?php

$userStorage                = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QPurchasingRequest         = new Application_Model_PurchasingRequest();
$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
$userStorage                = Zend_Auth::getInstance()->getStorage()->read();

$sn = $this->getRequest()->getParam('sn');
$back_url = $this->getRequest()->getParam('actual_link');

$back_url = !empty($back_url) ? $back_url : HOST.'purchasing/purchasing-request';

$db = Zend_Registry::get('db');
$db->beginTransaction();


try
{

        if (($userStorage->id != 7)&&($userStorage->id != SUPERADMIN_ID)&&($userStorage->team != TEAM_PURCHASING)) {
	    $flashMessenger->setNamespace('error')->addMessage('ERROR: Chỉ có TEAM_PURCHASING và SUPERADMIN mới có quyền Approve..!!');
            $this->redirect($back_url);
        }
        
        $CheckSelectSupplier = $QPurchasingRequestDetails->getCheckSelectSupplier($sn);

        if(!empty($CheckSelectSupplier)){
            if($CheckSelectSupplier[0]['supplier_id']){
                $flashMessenger->setNamespace('error')->addMessage("ERROR: Vui lòng NHẬP GIÁ. Trước khi Approve. SN=".$sn." !");
                $this->redirect($back_url);                   
            }
	    $flashMessenger->setNamespace('error')->addMessage("ERROR: Vui lòng CHỌN NHÀ CUNG CẤP cho sản phẩm. Trước khi Approve. SN=".$sn." !");
            $this->redirect($back_url);            
        }
        
        
        //// check hien.tran appr <= 500 triệu
        $details                    = $QPurchasingRequestDetails->getDetails($sn);

        $total_price_sn = 0;
        if(!empty($details))
        foreach($details as $k=>$v){
            $money_ck = $price_after_ck = $total_price = 0;
            $money_ck = $v['price']*$v['ck']/100;
            $price_after_ck = $v['price'] - $money_ck;
            $total_price = $v['quantity']*$price_after_ck+$v['fee'];
            $total_price_sn = $total_price_sn + $total_price;
        }
        
        if(($total_price_sn > 3000000000)&&($userStorage->id!=7)){
                $flashMessenger->setNamespace('error')->addMessage("ERROR: PR này giá trị lớn hơn 3 tỷ đồng. Tài khoản thuy.to mới có quyền Approve. Vui lòng kiểm tra lại..! !");
                $this->redirect($back_url);            
        }
        
        
        
	$where = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_request = $QPurchasingRequest->fetchRow($where);

	if(empty($purchasing_request)){
	    $flashMessenger->setNamespace('error')->addMessage('SN không tồn tại!');
            $this->redirect($back_url);
        }elseif($purchasing_request['status']==3){            
	    $flashMessenger->setNamespace('error')->addMessage('Lỗi: Đơn này đã được Approve..!');
            $this->redirect($back_url);
        }

	$data = array(
            'status'      => 3,
            'approved_at'  => date('Y-m-d H:i:s'),
            'approved_by'  => $userStorage->id,
            'is_reject'    => 0
	);
        
        if($purchasing_request['status']==1){// TH từ new mà approve thẳng
            $data['confirm_at'] = date('Y-m-d H:i:s');
            $data['confirm_by'] = $userStorage->id;
        }
        
	$QPurchasingRequest->update($data, $where);

	$db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Done!');
    $this->redirect($back_url);
}
catch (Exception $e)
{
    $db->rollBack();
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->redirect($back_url);
}
exit;