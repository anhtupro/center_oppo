<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';


$db = Zend_Registry::get('db');
$db->beginTransaction();
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();

try
{

	$QPurchasingCost  		= new Application_Model_PurchasingCost();
	$QPurchasingCostDetails 	= new Application_Model_PurchasingCostDetails();
	$userStorage    	 	= Zend_Auth::getInstance()->getStorage()->read();

	$sn 				= $this->getRequest()->getParam('sn');

	$pc_cost = array(
		'confirm_by'	=> $userStorage->id,
		'confirm_at'	=> date('Y-m-d H:i:s'),
		'status'	=> 'Delivery'
	);

	$where = $QPurchasingCost->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_order = $QPurchasingCost->fetchRow($where);

	if(!$purchasing_order){
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : SN không tồn tại</div>';
		exit;
	}

	if(!empty($purchasing_order['confirm_by'])){
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : PO đã được confirm</div>';
		exit;
	}

	$QPurchasingCost->update($pc_cost, $where);


	$db->commit();
	echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
	echo '<div class="alert alert-success">Done</div>';
        

}
catch (Exception $e)
{
	$db->rollBack();
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi : '.$e->getMessage().'</div>';	
}
exit;