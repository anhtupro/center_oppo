<?php

$id = $this->getRequest()->getPost('id');
$QPurchasingOrderFile  	= new Application_Model_PurchasingOrderFile();
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();

$response=array('status'=>0);
if (!empty($id)) {
     
    $where = $QPurchasingOrderFile->getAdapter()->quoteInto('id = ?', $id);
    $File_row = $QPurchasingOrderFile->fetchRow($where);
 
        if(!empty($File_row)){
            
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-order' .
                DIRECTORY_SEPARATOR . $File_row['sn'] .DIRECTORY_SEPARATOR. $File_row['po_quotation'];
           
             unlink($uploaded_dir);
             $QPurchasingOrderFile->delete($where);
        }    
    $response['status']= 1; 
    $response['message']="Thành công"; 
}else{
    $response['message']="Thiếu id";
}
echo json_encode($response);
exit();