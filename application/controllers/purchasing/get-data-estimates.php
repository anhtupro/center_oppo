<?php

$str_balance_id   = $this->getRequest()->getPost('balance_id');
$QEstimatesDetail = new Application_Model_EstimatesDetail();

$response = array('status' => 0);

if (!empty($str_balance_id)) {

    $array_key = explode('_', $str_balance_id);

    $year           = $array_key[0];
    $month          = $array_key[1];
    $d_id           = $array_key[2];
    $pmodel_code_id = $array_key[3];

    if ($year && $month && $d_id && $pmodel_code_id) {
        $where   = array();
        $where[] = $QEstimatesDetail->getAdapter()->quoteInto('year = ?', $year);
        $where[] = $QEstimatesDetail->getAdapter()->quoteInto('month = ?', $month);
        $where[] = $QEstimatesDetail->getAdapter()->quoteInto('d_id = ?', $d_id);
        $where[] = $QEstimatesDetail->getAdapter()->quoteInto('pmodel_code_id = ?', $pmodel_code_id);

        $EstimatesDetail = $QEstimatesDetail->fetchAll($where);

        if (!empty($EstimatesDetail)) {
            $response['status']  = 1;
            $response['message'] = "Thành công";
            $response['data']    = $EstimatesDetail->toArray();
        } else {
            $response['message'] = "Load thông tin thất bại";
        }
    } else {
        $response['message'] = "ID truyền vào không đúng";
    }
} else {
    $response['message'] = "Thiếu id của giao dịch";
}
echo json_encode($response);
exit();
