<?php
 
$id = $this->getRequest()->getPost('id');
$table_name = $this->getRequest()->getPost('table_name');

$QEstimatesDetail  = new Application_Model_EstimatesDetail();
$QEstimatesReality = new Application_Model_EstimatesReality();
$userStorage        = Zend_Auth::getInstance()->getStorage()->read();

$response=array('status'=>0);
if (!empty($id) && !empty($table_name)) {
    
    $where = $QEstimatesDetail->getAdapter()->quoteInto('id = ?', $id);
    
    if($table_name=="estimates_detail"){
        $data_row = $QEstimatesDetail->fetchRow($where);
        $file_name = $data_row['file'];
        $QEstimatesDetail->update(array('file' => null),$where);
    }elseif($table_name=="estimates_reality"){
        $data_row = $QEstimatesReality->fetchRow($where);
        $file_name = $data_row['file'];
        $QEstimatesReality->update(array('file' => null),$where);
    }

    if(!empty($data_row)){

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'estimates' .
            DIRECTORY_SEPARATOR . $data_row['year'].'_'. $data_row['month'] .DIRECTORY_SEPARATOR. $file_name;

         unlink($uploaded_dir);         
    }    
    $response['status']= 1; 
    $response['message']="Thành công"; 
}else{
    $response['message']="Thiếu data post";
}
echo json_encode($response);
exit();