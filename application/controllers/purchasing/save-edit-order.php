<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';

$db = Zend_Registry::get('db');
$db->beginTransaction();

try
{

	$QPurchasingOrder  		= new Application_Model_PurchasingOrder();
	$QPurchasingOrderDetails 	= new Application_Model_PurchasingOrderDetails();
	$QCodeSupplier 			= new Application_Model_CodeSupplier();
	$QLogCodeSupplier 		= new Application_Model_LogCodeSupplier();
	$QPurchasingRequest  		= new Application_Model_PurchasingRequest();
	$QPurchasingRequestDetails      = new Application_Model_PurchasingRequestDetails();
        $QPurchasingOrderFile           = new Application_Model_PurchasingOrderFile();
	$userStorage    	 	= Zend_Auth::getInstance()->getStorage()->read();

	$sn 				= $this->getRequest()->getParam('sn');

	$po_name 			= $this->getRequest()->getParam('po_name');
	$supplier_id 		= $this->getRequest()->getParam('supplier_id');
	$delivery_date 		= $this->getRequest()->getParam('delivery_date');
	$remark  			= $this->getRequest()->getParam('remark');
	$code_supplier_id   = $this->getRequest()->getParam('code_supplier_id', array());
	$quantity   		= $this->getRequest()->getParam('quantity', array());
	$price   			= $this->getRequest()->getParam('price', array());
	$price_default   	= $this->getRequest()->getParam('price_default', array());
	$pmodel_code_id   	= $this->getRequest()->getParam('pmodel_code_id', array());
	$pr_id   			= $this->getRequest()->getParam('pr_id', array());
	$check   			= $this->getRequest()->getParam('check', array());

	if(!$supplier_id){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : Vui lòng chọn Supplier</div>';
		exit;
	}

	if(!$check){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : Không có Request nào được chọn</div>';
		exit;
	}

	$po_order = array(
		'po_name'		=> $po_name,
		'delivery_date'	=> $delivery_date,
		'remark'		=> $remark,
		'status' 		=> 'New',
		'supplier_id'	=> $supplier_id,
	);


	$where = $QPurchasingOrder->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_order = $QPurchasingOrder->fetchRow($where);

	if(!$purchasing_order){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : SN không tồn tại</div>';
		exit;
	}

	if(!$check){
		echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Không có request nào được chọn.</div>';
		exit;
	}

	//UPLOAD HÌNH ẢNH
//	if ($_FILES['quotation']['size'] != 0) {
//	    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
//	    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-order' .
//	    DIRECTORY_SEPARATOR . $sn;
//	    
//
//	    $upload = new Zend_File_Transfer();
//	    $upload->setOptions(array('ignoreNoFile'=>true));
//	    
//	    //check function
//	    if (function_exists('finfo_file')){
//	        $upload->addValidator('MimeType', false, array('application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));
//	    }
//
//
//
//	    $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif');
//	    $upload->addValidator('Size', false, array('max' => '4MB'));
//	    $upload->addValidator('ExcludeExtension', false, 'php,sh');
//	    $files = $upload->getFileInfo();
//	    $hasPhoto = false;
//	    $data_file = array();
//	    
//	    if(isset($files['quotation']) and $files['quotation']){
//	    	$fileInfo = (isset($files['quotation']) and $files['quotation']) ? $files['quotation'] : null;
//
//		    if (!is_dir($uploaded_dir))
//		        @mkdir($uploaded_dir, 0777, true);
//		    $upload->setDestination($uploaded_dir);
//		        
//		    //Rename
//		    $old_name = $fileInfo['name'];
//		    $tExplode = explode('.', $old_name);
//		    $extension = end($tExplode);
//		    $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
//
//		    $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
//		    $r = $upload->receive(array('quotation'));
//		    if($r){
//		        $data_file['file_name'] = $new_name;
//		    }
//		    else{
//		        $messages = $upload->getMessages();
//		        foreach ($messages as $msg){
//		            echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
//					echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
//					echo '<div class="alert alert-error">Lỗi : '.$msg.'</div>';	
//					exit;
//		        }
//		    }
//		    $po_order['quotation'] = $new_name;
//	    }
//	}
    //END UPLOAD HÌNH ẢNH

	//UPLOAD HÌNH ẢNH
        foreach ($_FILES['po_quotation']['size'] as $k => $val) {

            if ($_FILES['po_quotation']['size'][$k] != 0) {
                
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-order' .
                DIRECTORY_SEPARATOR . $sn;


                $upload = new Zend_File_Transfer();
                $upload->setOptions(array('ignoreNoFile'=>true));

                //check function
//                if (function_exists('finfo_file')){
//                    $upload->addValidator('MimeType', false, array('application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));
//                }

                $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx');
                $upload->addValidator('Size', false, array('max' => '4MB'));
                $upload->addValidator('ExcludeExtension', false, 'php,sh');
                
                $name_file_k = "po_quotation_".$k."_";
                $files = $upload->getFileInfo($name_file_k);
                $hasPhoto = false;
                $data_file = array();
               
                
                if(isset($files[$name_file_k]) and $files[$name_file_k]){
                    $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                        if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                        $upload->setDestination($uploaded_dir);

                        //Rename
                        $old_name = $fileInfo['name'];                     
                        
                        $tExplode = explode('.', $old_name);
                        $extension = end($tExplode);// đuôi file
                        //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k.'_'. $old_name;
                        $new_name = 'UPLOAD-' .date('Ymd').substr ( microtime (), 2, 4 ) . md5(uniqid('', true)) . '.' . $extension;
                        $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                        $r = $upload->receive(array($name_file_k));
                        if($r){
                            $data_file['file_name'] = $new_name;
                        }
                        else{
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg){
                                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                                            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                                            echo '<div class="alert alert-error">Lỗi : '.$msg.'</div>';	
                                            exit;
                            }
                        }
                        //$purchasing_request['po_quotation'] = $new_name;
                        $QPurchasingOrderFile->insert(array('sn'=>$sn, 'po_quotation'=>$new_name));
                }
            }
        }
        
    //END UPLOAD HÌNH ẢNH
        
        
	$QPurchasingOrder->update($po_order, $where);

	//Xóa hết tất cả order details cũ để insert lại
	$where_details = $QPurchasingOrderDetails->getAdapter()->quoteInto('po_id = ?', $purchasing_order['id']);
	$purchasing_order_details = $QPurchasingOrderDetails->fetchAll($where_details);
	foreach($purchasing_order_details as $key=>$value){
		$where_pr = $QPurchasingRequestDetails->getAdapter()->quoteInto('id = ?', $value['pr_details_id']);
		$purchasing_request_details = $QPurchasingRequestDetails->fetchRow($where_pr);
		$pr_update = array(
			'po_quantity'	=> 	($purchasing_request_details['po_quantity'] - $value['quantity'])
		);
		$QPurchasingRequestDetails->update($pr_update, $where_pr);
	}

	$QPurchasingOrderDetails->delete($where_details);
	//END 

	foreach($pr_id as $key=>$value){
		if(in_array($value, $check)){
			$po_order_details = array(
				'po_id'             => $purchasing_order['id'],
				'pr_details_id'     => $value,
				'code_supplier_id'  => $code_supplier_id[$key],
				'quantity'          => $quantity[$key],
				'price'             => $price[$key],
				'pmodel_code_id'    => $pmodel_code_id[$key]
			);
			$QPurchasingOrderDetails->insert($po_order_details);

			//Update số lượng details
			$where 				= $QPurchasingRequestDetails->getAdapter()->quoteInto('id = ?',$value);
			$pr_details 		= $QPurchasingRequestDetails->fetchRow($where);
                      
                        
			$quantity_stock 	= ($pr_details['quantity'] - $pr_details['po_quantity']);
			if($quantity[$key] > $quantity_stock){
				$db->rollBack();
			    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
				echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
				echo '<div class="alert alert-error">Lỗi : Số lượng điều chỉnh lớn hơn số lượng yêu cầu, Quantity_pending = '.$quantity_stock.'</div>';
				exit;
			}
			else{
				$data_pr = array(
					'po_quantity' => $pr_details['po_quantity'] + $quantity[$key]
				);
				$QPurchasingRequestDetails->update($data_pr,$where);
			}
			//END update số lượng details

			//Nếu đổi giá default đổi thì ghi log lại
			if($price[$key] != $price_default[$key]){
				$now = date('Y-m-d H:i:s');
				$where_cs 	= $QCodeSupplier->getAdapter()->quoteInto('id = ?', $code_supplier_id[$key]);
				$QCodeSupplier->update(array('price' => $price[$key]), $where_cs);

				$where_log = array();
				$where_log[] = $QLogCodeSupplier->getAdapter()->quoteInto('code_supplier_id = ?', $code_supplier_id[$key]);
				$where_log[] = $QLogCodeSupplier->getAdapter()->quoteInto('to_date IS NULL', NULL);
				$QLogCodeSupplier->update(array('to_date' => $now), $where_log);

				$data_log = array(
					'code_supplier_id' => $code_supplier_id[$key],
					'price'			   => $price[$key],
					'from_date'		   => $now
				);
				$QLogCodeSupplier->insert($data_log);
			}
			//END 

		}
	}

	$db->commit();
	echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
	echo '<div class="alert alert-success">Done</div>';
                /// load lại trang
        $back_url =  "edit-order?sn=".$sn;
        echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
}
catch (Exception $e)
{
	$db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi : '.$e->getMessage().'</div>';	
}
exit;