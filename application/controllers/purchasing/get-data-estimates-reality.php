<?php

$detail_id = $this->getRequest()->getPost('detail_id');
$QEstimatesReality  = new Application_Model_EstimatesReality();

$response=array('status'=>0);

if (!empty($detail_id)) {
    

    $where = $QEstimatesReality->getAdapter()->quoteInto('estimates_detail_id = ?', $detail_id);
    $EstimatesReality = $QEstimatesReality->fetchAll($where);
 
    if(!empty($EstimatesReality)){
      $response['status']=1;
      $response['message']="Thành công";
      $response['data']= $EstimatesReality->toArray();
    }else{
       $response['message']="Load note thất bại"; 
    }
    
}else{
    $response['message']="Thiếu id của giao dịch";
}
echo json_encode($response);
exit();
