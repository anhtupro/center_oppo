<?php

$userStorage                = Zend_Auth::getInstance()->getStorage()->read();
$flashMessenger = $this->_helper->flashMessenger;

$QPurchasingRequest         = new Application_Model_PurchasingRequest();
$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
$QPurchasingRequestFile  = new Application_Model_PurchasingRequestFile();
$QPurchasingRequestSupplier  = new Application_Model_PurchasingRequestSupplier();

$QPurchasingRequestSave         = new Application_Model_PurchasingRequestSave();
$QPurchasingRequestDetailsSave  = new Application_Model_PurchasingRequestDetailsSave();
$QPurchasingRequestSupplierSave  = new Application_Model_PurchasingRequestSupplierSave();
$QPurchasingRequestFileSave  = new Application_Model_PurchasingRequestFileSave();

$userStorage                = Zend_Auth::getInstance()->getStorage()->read();

$sn = $this->getRequest()->getParam('sn');
$back_url = $this->getRequest()->getParam('actual_link');

//$back_url = HOST.'purchasing/purchasing-request';

$db = Zend_Registry::get('db');
$db->beginTransaction();


try
{

        if ($userStorage->id != 4) {
	    //$flashMessenger->setNamespace('error')->addMessage('ERROR: Chỉ có hien.tran mới có quyền Set Edit PR..!!');
            //$this->redirect($back_url);
        }
        
        $where = NULL;
	$where = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_request = $QPurchasingRequest->fetchRow($where);

	if(empty($purchasing_request)){
	    $flashMessenger->setNamespace('error')->addMessage('SN không tồn tại!');
            $this->redirect($back_url);
        }
        
        //Purchasing Save
        $purchasing_request = $purchasing_request->toArray();
        $id = $QPurchasingRequestSave->insert($purchasing_request);
        
        $where = NULL;
	$where = $QPurchasingRequestDetails->getAdapter()->quoteInto('pr_id = ?', $purchasing_request['id']);
        $purchasing_request_details = $QPurchasingRequestDetails->fetchAll($where);
        
        foreach($purchasing_request_details as $key=>$value){
            
            $data = $value->toArray();
            $data['save_id'] = $id;
            
            $QPurchasingRequestDetailsSave->insert($data);
        }
        
        $where = NULL;
	$where = $QPurchasingRequestSupplier->getAdapter()->quoteInto('pr_id = ?', $purchasing_request['id']);
        $purchasing_supplier = $QPurchasingRequestSupplier->fetchAll($where);
        
        foreach($purchasing_supplier as $key=>$value){
            $data = $value->toArray();
            $data['save_id'] = $id;
            
            $QPurchasingRequestSupplierSave->insert($data);
        }
        
        $where = NULL;
	$where = $QPurchasingRequestFile->getAdapter()->quoteInto('pr_id = ?', $purchasing_request['id']);
        $purchasing_file = $QPurchasingRequestFile->fetchAll($where);
        
        foreach($purchasing_file as $key=>$value){
            $data = $value->toArray();
            $data['save_id'] = $id;
            
            $QPurchasingRequestFileSave->insert($data);
        }
        
        //END Purchasing Save

	$data = array(
            'status' => 1,
            'confirm_by' => NULL,
            'confirm_at' => NULL,
            'approved_by' => NULL,
            'approved_at' => NULL
	);
        
        $where = NULL;
	$where = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?',$sn);
        
	$QPurchasingRequest->update($data, $where);

	$db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Done!');
    $this->redirect($back_url);
}
catch (Exception $e)
{
    $db->rollBack();
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->redirect($back_url);
}
exit;