<?php
$QSupplier        = new Application_Model_Supplier();
$supplier 	 = $QSupplier->get_cache();

$sort                   = $this->getRequest()->getParam('sort', '');
$desc                   = $this->getRequest()->getParam('desc', 1);
$sn                     = $this->getRequest()->getParam('sn');
$name                   = $this->getRequest()->getParam('name');
$supplier_id            = $this->getRequest()->getParam('supplier_id');
$remark                 = $this->getRequest()->getParam('remark');
$code                   = $this->getRequest()->getParam('code');
$created_at_from        = $this->getRequest()->getParam('created_at_from');
$created_at_to          = $this->getRequest()->getParam('created_at_to');
$export                 = $this->getRequest()->getParam('export',0);

//////////////////////////////////////////////////// phân quyền

            $userStorage            = Zend_Auth::getInstance()->getStorage()->read();

            $user_id = $userStorage->id;
            $group_id = $userStorage->group_id;            
            $user_department = $userStorage->department;

            if ($user_id == SUPERADMIN_ID || in_array($group_id, array(
                ADMINISTRATOR_ID,
                HR_ID,
                HR_EXT_ID,
                BOARD_ID)))
            {
            }
            elseif ($group_id == PGPB_ID){
                $params['staff_id'] = $user_id;
            }elseif ($group_id == ASM_ID || $group_id == ASMSTANDBY_ID){
                $params['asm'] = $user_id;
            }elseif (in_array($group_id,array(ACCESSORIES_ID, TRAINING_TEAM_ID,TRAINING_LEADER_ID , DIGITAL_ID , SERVICE_ID ,TRADE_MARKETING_GROUP_ID))){
                $params['other'] = $user_id;
            }elseif($group_id == LEADER_ID){
                $params['leader'] = $user_id;
            }

            //set quyen cho hong nhung trainer
            elseif ($user_id == 7278 || $user_id == 240){
                $params['other'] = $user_id;
            }
            //chi van ha noi
            elseif ($user_id == 95){
                $params['other'] = $user_id;
            }
            elseif ($group_id == 22)
            $params['other'] = $user_id;
            elseif ($user_id == 3601 || $user_id == 2768 ||$user_id == 5915)
            $params['asm'] = $user_id;
            else{
                $params['sale'] = $user_id;
            }
 
            $params['year'] = date("Y");
            $params['month'] = date("m");
            $QTime2         = new Application_Model_Time2();
            $list_of_subordinates = $QTime2->getListStaffById($params);
            $list_of_subordinates = explode(",",$list_of_subordinates);
            $list_of_subordinates[] = $user_id;
 
/////////////////////////////////////////////

$params = array(
    'sn'              	=> $sn,
    'code'              => $code,
    'remark'            => $remark,
    'name'              => $name,
    'supplier_id'      => $supplier_id,
    'created_at_from'   => $created_at_from,
    'created_at_to'     => $created_at_to
);


    if(in_array($user_id, array(// user quản lý theo department
    3621// thu.nhu Chị Thu service
    ))){
        $params['permission_department'] = $user_department;
    }elseif(!in_array($user_id, array(
    340,// chị Yến yen.dao
    7, // thuy.to
    4 // hien.tran
    ))){
        $params['list_staff'] = $list_of_subordinates;
    }


$params['sort'] = $sort;
$params['desc'] = $desc;


$QPurchasingOrder         = new Application_Model_PurchasingOrder();
$page        = $this->getRequest()->getParam('page', 1);
$limit       = LIMITATION;
$total       = 0;
$result      = $QPurchasingOrder->listPO($page, $limit, $total, $params);
unset($params['list_staff']);// chuỗi dài quá truyền phân trang bị lỗi

$this->view->supplier= $supplier;
$this->view->list    = $result;
$this->view->limit   = $limit;
$this->view->total   = $total;
$this->view->params  = $params;
$this->view->desc    = $desc;
$this->view->sort    = $sort;
$this->view->url     = HOST.'purchasing/order'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset  = $limit*($page-1);


$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$whereTypeAsset             = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
$type_asset                 = $QTrainerTypeAsset->fetchAll($whereTypeAsset);
$arrayTypeAsset             = array();
if($type_asset->count())
{
    foreach($type_asset as $key => $value)
    {
        $arrayTypeAsset[$value['id']] = $value['name'];
    }
}

$this->view->type_asset     = $arrayTypeAsset;
$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;

if($export==1){// report PO
   
    if(in_array($user_id, array(// user quản lý theo department
    3621// thu.nhu Chị Thu service
    ))){
        $params['permission_department'] = $user_department;
    }elseif(!in_array($user_id, array(
    340,// chị Yến yen.dao
    7, // thuy.to
    4 // hien.tran
    ))){
        $params['list_staff'] = $list_of_subordinates;
    }

		// no limit time
        set_time_limit(0);
        ini_set('memory_limit', -1);
        error_reporting(~E_ALL);
        ini_set('display_error', 0);
		
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'NO.',
            'Sn',
            'PO name',
            'Delivery date',
            'Product',
            'Supplier',
            'Quantity',
            'price',
            'After VAT',
            'Total',
            'Total PO',
            'Status',            
            'Remark',
            'Area',
            'department',
            'Created By',
            'Created At',
            'Confirm By',
            'Confirm At'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;

        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }

        $index = 2;
        $intCount = 1;       
  
        $data = $QPurchasingOrder->reportPO($params);
     
        try
        {
            if ($data)
                foreach ($data as $_key => $_order)
                {
                
                    $alpha = 'A';
                    $sheet->setCellValue($alpha++ . $index, $intCount++);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['sn'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['po_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['delivery_date'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['product'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['supplier'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['quantity'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($_order['price']), PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($_order['after_vat']), PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($_order['total']), PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit(number_format($_order['total_po']), PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['status'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['remark'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['area'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['department'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['created_by_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['created_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['confirm_by_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['confirm_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $index++;
                    
                }
        }
        catch (exception $e)
        {
            exit;
        }

        $filename = 'Report_PO_' . date('Y_m_d');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
}




