<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QEstimatesBalance = new Application_Model_EstimatesBalance();
$QEstimatesDetail  = new Application_Model_EstimatesDetail();

$str_estimates_balance_id = $this->getRequest()->getParam('estimates_balance_id');
$name                 = $this->getRequest()->getParam('name');
$amount               = $this->getRequest()->getParam('amount', 0);
$payment_for          = $this->getRequest()->getParam('payment_for');
$note                 = $this->getRequest()->getParam('note', null);
$id_old               = $this->getRequest()->getParam('id_old'); // dùng để edit

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {


    if (empty($name) || empty($amount) || empty($payment_for)) {
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
        echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại.</div>';
        die();
    }

    $array_key = explode('_', $str_estimates_balance_id);

    $year           = $array_key[0];
    $month          = $array_key[1];
    $d_id           = $array_key[2];
    $pmodel_code_id = $array_key[3];

    $where_temp       = array();
    $where_temp[]     = $QEstimatesBalance->getAdapter()->quoteInto('year = ?', $year);
    $where_temp[]     = $QEstimatesBalance->getAdapter()->quoteInto('month = ?', $month);
    $where_temp[]     = $QEstimatesBalance->getAdapter()->quoteInto('d_id = ?', $d_id);
    $where_temp[]     = $QEstimatesBalance->getAdapter()->quoteInto('pmodel_code_id = ?', $pmodel_code_id);
    $EstimatesBalance = $QEstimatesBalance->fetchRow($where_temp);

    if (empty($EstimatesBalance)) {
        $dataInsert_eb        = array(
            'year'             => $year,
            'month'            => $month,
            'd_id'             => $d_id,
            'pmodel_code_id'   => $pmodel_code_id,
            'amount_estimates' => 0,
            'amount_reality'   => 0
        );
        $estimates_balance_id = $QEstimatesBalance->insert($dataInsert_eb);
        $where                = $QEstimatesBalance->getAdapter()->quoteInto('id = ?', $estimates_balance_id);
        $EstimatesBalance     = $QEstimatesBalance->fetchRow($where);
    } else {
        $estimates_balance_id = $EstimatesBalance['id'];
    }

    if (empty($EstimatesBalance)) {
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
        echo '<div class="alert alert-error">Lỗi : estimates_balance_id Không tồn tại trong bảng estimates_balance.</div>';
        die();
    }

    /// xóa những dòng bị bỏ
    $where_del   = array();
    $where_del[] = $QEstimatesDetail->getAdapter()->quoteInto('balance_id = ?', $estimates_balance_id);
    $where_del[] = $QEstimatesDetail->getAdapter()->quoteInto('id not in (?)', $id_old);
    $QEstimatesDetail->delete($where_del);

    $array_detail_id_full = array(); // gom tất cả ID của EstimatesDetail lại để cập nhật hình ảnh 

    foreach ($name as $k => $value) {

        if (!empty($id_old[$k])) {// có id cũ thì update
            $data_row                 = array(
                'balance_id'       => $EstimatesBalance['id'],
                'year'             => $EstimatesBalance['year'],
                'month'            => $EstimatesBalance['month'],
                'd_id'             => $EstimatesBalance['d_id'],
                'pmodel_code_id'   => $EstimatesBalance['pmodel_code_id'],
                'name'             => $name[$k],
                'amount_estimates' => My_Number::floatval($amount[$k]),
                'payment_for'      => $payment_for[$k],
                'note'             => $note[$k],
                'updated_by'       => $userStorage->id,
                'updated_at'       => date('Y-m-d H:i:s')
            );
            $where_upd                = $QEstimatesDetail->getAdapter()->quoteInto('id = ?', $id_old[$k]);
            $QEstimatesDetail->update($data_row, $where_upd);
            $array_detail_id_full[$k] = $id_old[$k];
        } else {// insert            
            $data_row                 = array(
                'balance_id'       => $EstimatesBalance['id'],
                'year'             => $EstimatesBalance['year'],
                'month'            => $EstimatesBalance['month'],
                'd_id'             => $EstimatesBalance['d_id'],
                'pmodel_code_id'   => $EstimatesBalance['pmodel_code_id'],
                'name'             => $name[$k],
                'amount_estimates' => My_Number::floatval($amount[$k]),
                'payment_for'      => $payment_for[$k],
                'note'             => $note[$k],
                'created_by'       => $userStorage->id,
                'created_at'       => date('Y-m-d H:i:s')
            );
            $id_row                   = $QEstimatesDetail->insert($data_row);
            $array_detail_id_full[$k] = $id_row;
        }
    }//End foreach
    // cập nhật lại tổng chi phí dự trù cho bảng $QEstimatesBalance
    $db                = Zend_Registry::get('db');
    $select            = $db->select()
            ->from(array('ed' => 'estimates_detail'), array('balance_estimates' => new Zend_Db_Expr('SUM(IFNULL(ed.amount_estimates,0))')))
            ->where($db->quoteInto('ed.balance_id = ?', $estimates_balance_id));
    $balance_estimates = $db->fetchOne($select);
    $where_up          = $QEstimatesBalance->getAdapter()->quoteInto('id = ?', $estimates_balance_id);
    $QEstimatesBalance->update(array('amount_estimates' => $balance_estimates), $where_up);


    //UPLOAD FILE
    $folder_name = $EstimatesBalance['year'] . "_" . $EstimatesBalance['month'];

    foreach ($_FILES['file_upload']['size'] as $k2 => $val) {

        if ($_FILES['file_upload']['size'][$k2] != 0) {

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'estimates' .
                    DIRECTORY_SEPARATOR . $folder_name;


            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile' => true));

            $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
            $upload->addValidator('Size', false, array('max' => '4MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');

            $name_file_k = "file_upload_" . $k2 . "_";
            $files       = $upload->getFileInfo($name_file_k);
            $hasPhoto    = false;
            $data_file   = array();


            if (isset($files[$name_file_k]) and $files[$name_file_k]) {
                $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);
                $upload->setDestination($uploaded_dir);

                //Rename
                $old_name  = $fileInfo['name'];
                $tExplode  = explode('.', $old_name);
                $extension = end($tExplode); // đuôi file 
                $new_name  = 'UPLOAD-' . date('Ymd') . substr(microtime(), 2, 4) . md5(uniqid('', true)) . '.' . $extension;
                $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));

                $r = $upload->receive(array($name_file_k));
                if ($r) {
                    $data_file['file_name'] = $new_name;
                } else {
                    $messages = $upload->getMessages();
                    foreach ($messages as $msg) {
                        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                        echo '<div class="alert alert-error">Lỗi : ' . $msg . '</div>';
                        exit;
                    }
                }
                $where_file = $QEstimatesDetail->getAdapter()->quoteInto('id = ?', $array_detail_id_full[$k2]);
                $QEstimatesDetail->update(array('file' => $new_name), $where_file);
            }
        }
    }

    $db->commit();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
    echo '<div class="alert alert-success">Done</div>';
    /// load lại trang
    $back_url = "estimates?month=".$EstimatesBalance['month']."&year=".$EstimatesBalance['year'];
    echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
} catch (Exception $e) {
    $db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : ' . $e->getMessage() . '</div>';
}
exit;
