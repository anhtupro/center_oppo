<?php

$id = $this->getRequest()->getParam('id');
$submit = $this->getRequest()->getParam('submit');
$category_id = $this->getRequest()->getParam('category_id');

$QPurchasingCode = new Application_Model_PurchasingCode();
$QPurchasingCodeCategory = new Application_Model_PurchasingCodeCategory();

$params = [
    'category_id' => $category_id
];

$where = NULL;
$where 	= $QPurchasingCode->getAdapter()->quoteInto('status = 0');
$purchasing_code = $QPurchasingCode->fetchAll($where);
$category_list = $QPurchasingCode->getCategoryList();

$flashMessenger = $this->_helper->flashMessenger;

$this->view->params = $params;

if($this->getRequest()->getMethod()=='POST')
{
    $db = Zend_Registry::get('db');
    try {
        $db->beginTransaction();

        $pmodel_code_id = $this->getRequest()->getParam('pmodel_code_id');
        $category_id = $this->getRequest()->getParam('category_id');
        $type = $this->getRequest()->getParam('type');
        
        $where  = [];
        $where[] = $QPurchasingCodeCategory->getAdapter()->quoteInto('code_purchasing_id = ?', $pmodel_code_id);
        $code_category = $QPurchasingCodeCategory->fetchRow($where);
        
        if($code_category){
            $flashMessenger->setNamespace('error')->addMessage('Code đã được map !');
            $this->redirect('/purchasing/map-category');
        }
        
        $where  = [];
        $where[] = $QPurchasingCodeCategory->getAdapter()->quoteInto('category_id = ?', $category_id);
        $where[] = $QPurchasingCodeCategory->getAdapter()->quoteInto('type = ?', $type);
        $category = $QPurchasingCodeCategory->fetchRow($where);
        
        if($category){
            $flashMessenger->setNamespace('error')->addMessage('Category đã được map !');
            $this->redirect('/purchasing/map-category');
        }
        
        $data = [
            'code_purchasing_id' => $pmodel_code_id,
            'category_id' => $category_id,
            'type' => $type
        ];
        
        $QPurchasingCodeCategory->insert($data);
        $db->commit();
        
        $flashMessenger->setNamespace('success')->addMessage('Thành công !');
        $this->redirect('/purchasing/map-category');
        
    } catch (Exception $e) {

        $db->rollback();
        
        $flashMessenger->setNamespace('error')->addMessage('Có lỗi !'.$e->getMessage());
        $this->redirect('/purchasing/map-category');
        
    }
}

$this->view->purchasing_code = $purchasing_code;
$this->view->category_list = $category_list;


$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
