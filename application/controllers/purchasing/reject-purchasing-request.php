<?php

    $flashMessenger = $this->_helper->flashMessenger;

    $db = Zend_Registry::get('db');
    $db->beginTransaction();

    $sn = $this->getRequest()->getParam('sn');
    $note_reject = $this->getRequest()->getParam('note_reject');
    $actual_link = $this->getRequest()->getParam('actual_link');

    $back_url = HOST.'purchasing/view-request?sn='.$sn;

    try
    {
        $QPurchasingRequest         = new Application_Model_PurchasingRequest();
        $QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
        $QAppNoti = new Application_Model_AppNoti();
        $userStorage                = Zend_Auth::getInstance()->getStorage()->read();

        if($userStorage->department != 400 AND $userStorage->id!=7){
            $flashMessenger->setNamespace('error')->addMessage('Lỗi: Team Purchasing mới Reject được..!');
            $this->redirect($back_url);
        }

        $where = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?',$sn);
        $purchasing_request = $QPurchasingRequest->fetchRow($where);

        if(!$purchasing_request){
            $flashMessenger->setNamespace('error')->addMessage('SN không tồn tại!');
            $this->redirect($back_url);
        }elseif($purchasing_request['status']!=2){            
            $flashMessenger->setNamespace('error')->addMessage('Lỗi: Đơn ở trạng thái CONFIRM mới Reject được..!');
            $this->redirect($back_url);
        }

        $data = array(
            'confirm_by'    => NULL,
            'confirm_at'    => NULL,
            'status'        => 1,
            'is_reject'     => 1,
            'reject_note'   => $note_reject,
            'reject_by'     => $userStorage->id,
        );
        $QPurchasingRequest->update($data, $where);

        $db->commit();

        $flashMessenger->setNamespace('success')->addMessage('Done!');
        $this->redirect($back_url);
    }
    catch (Exception $e)
    {
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect($back_url);
    }
    exit;
