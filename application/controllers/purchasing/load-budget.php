<?php

$request_type = $this->getRequest()->getParam('request_type');

$QRequestType           = new Application_Model_RequestType();
$QRequestOfficePlan     = new Application_Model_RequestOfficePlan();
$QRequestOfficePlanInfo = new Application_Model_RequestOfficePlanInfo();

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$reponse = [
    'status'    => 1,
    'message'   => 'Done',
    'budget'    => $request_type
];


$params = [
    'request_type'      => $request_type,
    'team_id'           => $userStorage->team,
    'department_id'     => $userStorage->department
];

$request_plan = $QRequestOfficePlan->loadBudgetList($params);

$reponse['data'] = $request_plan;

echo json_encode($reponse);
exit;
