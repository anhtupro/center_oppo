<?php

$id = $this->getRequest()->getParam('id');
$submit = $this->getRequest()->getParam('submit');
$contructors_id = $this->getRequest()->getParam('contructors_id');

$QPurchasingSupplier = new Application_Model_PurchasingSupplier();
$QSupplierContructors = new Application_Model_SupplierContructors();

$where = NULL;
$where 	= $QPurchasingSupplier->getAdapter()->quoteInto('del = 0');
$supplier_list = $QPurchasingSupplier->fetchAll($where);
$contructor_list = $QPurchasingSupplier->getContructorsList();

$flashMessenger = $this->_helper->flashMessenger;

$params = [
    'contructors_id' => $contructors_id
];
$this->view->params = $params;


if($this->getRequest()->getMethod()=='POST')
{
    $db = Zend_Registry::get('db');
    try {
        $db->beginTransaction();

        $supplier_id = $this->getRequest()->getParam('supplier_id');
        $contructor_id = $this->getRequest()->getParam('contructor_id');
        
        $where  = NULL;
        $where 	= $QSupplierContructors->getAdapter()->quoteInto('supplier_id = ?', $supplier_id);
        $supplier = $QSupplierContructors->fetchRow($where);
        
        if($supplier){
            $flashMessenger->setNamespace('error')->addMessage('Supplier đã được map !');
            $this->redirect('/purchasing/map-supplier');
        }
        
        $where  = NULL;
        $where 	= $QSupplierContructors->getAdapter()->quoteInto('contructors_id = ?', $contructor_id);
        $contructor = $QSupplierContructors->fetchRow($where);
        
        if($contructor){
            $flashMessenger->setNamespace('error')->addMessage('Contructor đã được map !');
            $this->redirect('/purchasing/map-supplier');
        }
        
        $data = [
            'supplier_id' => $supplier_id,
            'contructors_id' => $contructor_id,
        ];
        
        $QSupplierContructors->insert($data);
        $db->commit();
        
        $flashMessenger->setNamespace('success')->addMessage('Thành công !');
        $this->redirect('/purchasing/map-supplier');
        
    } catch (Exception $e) {

        $db->rollback();
        
        $flashMessenger->setNamespace('error')->addMessage('Có lỗi !'.$e->getMessage());
        $this->redirect('/purchasing/map-supplier');
        
    }
}

$this->view->supplier_list = $supplier_list;
$this->view->contructor_list = $contructor_list;


$messages_error = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;

$messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;

