<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$sn = $this->getRequest()->getParam('sn');

if (!empty($sn)) {
    $QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
    $QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
    $QPurchasingRequestFile     = new Application_Model_PurchasingRequestFile();

    $purchasing_request_supplier             = $QPurchasingRequestSupplier->getListSupplierReview($sn);
    $this->view->purchasing_request_supplier = $purchasing_request_supplier;
  
    
    $where_file   = array();
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('sn = ?', $sn);
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('prs_id is not null', null);
    $list_file    = $QPurchasingRequestFile->fetchAll($where_file);
    if (!empty($list_file)) {
        $this->view->list_file = $list_file->toArray();
    }
    
    /// danh s�ch l?ch s? ncc
    $listSupplierHistory = $arr_prs_id = array();
    if(!empty($purchasing_request_supplier)){
        foreach ($purchasing_request_supplier as $value) {
            $listHistoryArr = array();
            $listHistoryArr = $QPurchasingRequestSupplier->getListHistoryReview($value['supplier_id'], $sn);
            if(!empty($listHistoryArr)){
                foreach ($listHistoryArr as $k => $val) {
                    $listSupplierHistory[] = $val;
                    $arr_prs_id[] = $val['prs_id'];// d? l?y danh s�ch file
                }
            }
        }//End foreach
        $this->view->listSupplierHistory = $listSupplierHistory;   
        
        
        // File trong danh s�ch history        
        if(!empty($arr_prs_id)){
            $where_file_his   = array();
            $where_file_his[] = $QPurchasingRequestFile->getAdapter()->quoteInto('prs_id in (?)', $arr_prs_id );
            $where_file_his[] = $QPurchasingRequestFile->getAdapter()->quoteInto('sn <> ?', $sn );
            $list_file_his    = $QPurchasingRequestFile->fetchAll($where_file_his);
            if (!empty($list_file_his)) {
                $this->view->list_file_his = $list_file_his->toArray();
            } 
        }
    }//end if    
   
}

