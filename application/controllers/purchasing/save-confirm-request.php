<?php
echo 'die ở đầu action.'; die();
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';

$db = Zend_Registry::get('db');
$db->beginTransaction();

try
{
	$QPurchasingRequest  		= new Application_Model_PurchasingRequest();
	$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
	$QTime2  		   		 	= new Application_Model_Time2();

	$userStorage    	 		= Zend_Auth::getInstance()->getStorage()->read();

	
	
	


	$sn = $this->getRequest()->getParam('sn');
	$where = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?', $sn);
	$purchasing_request = $QPurchasingRequest->fetchRow($where);

	if(empty($purchasing_request)){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : Purchasing Request Not Exits</div>';
		exit;
	}

	if(!empty($purchasing_request['confirm_at'])){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : SN: '.$sn.' is confirm at '.date('H:i d/m/Y', strtotime($purchasing_request['confirm_at'])).'</div>';
		exit;
	}

	$data = array(
		'status'	  => 'Approved',
		'confirm_at'  => date('Y-m-d H:i:s'),
		'confirm_by'  => $userStorage->id,
	);
	$QPurchasingRequest->update($data, $where);

	//Update Status details
	$where = $QPurchasingRequestDetails->getAdapter()->quoteInto('purchasing_request_id = ?', $purchasing_request['id']);
	$data_details = array(
		'status' => 'Approved',
	);
	$QPurchasingRequestDetails->update($data_details, $where);
	//END UPDATE status details

	$db->commit();
	echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
	echo '<div class="alert alert-success">Confirm Success!</div>';
}
catch (Exception $e)
{
	$db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi : '.$e->getMessage().'</div>';	
}
exit;