<?php

$flashMessenger = $this->_helper->flashMessenger;

$db = Zend_Registry::get('db');
$db->beginTransaction();

$back_url = HOST.'purchasing/purchasing-request';

try
{
    $userStorage = Zend_Auth::getInstance()->getStorage()->read();
   

	$QPurchasingRequest         = new Application_Model_PurchasingRequest();
	$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
	$userStorage                = Zend_Auth::getInstance()->getStorage()->read();

	$sn = $this->getRequest()->getParam('sn');

	$where = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_request = $QPurchasingRequest->fetchRow($where);

	if(!$purchasing_request){
	    $flashMessenger->setNamespace('error')->addMessage('SN không tồn tại!');
            $this->redirect($back_url);
        }elseif($purchasing_request['status']!=1){
            if($userStorage->id != 4){
                $flashMessenger->setNamespace('error')->addMessage('Lỗi: Đơn ở trạng thái NEW mới Delete được..!');
                $this->redirect($back_url);
            }
	    
        }

	$data = array(
            'del'      => 1
	);
	$QPurchasingRequest->update($data, $where);

        
        // log
        $QLog = new Application_Model_Log();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = "Delete_PR_".$sn;

        $QLog->insert( array (
            'info' => $info,
            'user_id' => $userStorage->id,
            'ip_address' => $ip,
            'time' => date('Y-m-d H:i:s'),
        ) );
        
	$db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Delete success!');
    $this->redirect($back_url);
}
catch (Exception $e)
{
	$db->rollBack();
	$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->redirect($back_url);
}
exit;