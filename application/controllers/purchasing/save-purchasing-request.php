<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';

$QPurchasingRequest         = new Application_Model_PurchasingRequest();
$QPurchasingRequestFile     = new Application_Model_PurchasingRequestFile();
$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
$QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
$QPmodelCode                = new Application_Model_PmodelCode();
$QPurchasingPlan            = new Application_Model_PurchasingPlan();
$userStorage                = Zend_Auth::getInstance()->getStorage()->read();

$cache_pmodel_code = $QPmodelCode->cache_pmodel_code();


$name          = $this->getRequest()->getParam('name');
$type          = $this->getRequest()->getParam('type', null);
$urgent_date   = $this->getRequest()->getParam('urgent_date');
$project_id    = $this->getRequest()->getParam('project_id');
$area_id       = $this->getRequest()->getParam('area_id');
$department_id = $this->getRequest()->getParam('department_id', null);
$team_id       = $this->getRequest()->getParam('team_id', null);

$shipping_address = $this->getRequest()->getParam('shipping_address');
$remark           = $this->getRequest()->getParam('remark');

$pmodel_code_id    = $this->getRequest()->getParam('pmodel_code_id', array());
$quantity          = $this->getRequest()->getParam('quantity', array());
$installation_at   = $this->getRequest()->getParam('installation_at', array());
$installation_time = $this->getRequest()->getParam('installation_time', array());
$note              = $this->getRequest()->getParam('note', array());

$select_supplier = $this->getRequest()->getParam('select_supplier', array());
$supplier_id     = $this->getRequest()->getParam('supplier_id', array());
$price_before_dp = $this->getRequest()->getParam('price_before_dp', array());
$price           = $this->getRequest()->getParam('price', array());
$ck              = $this->getRequest()->getParam('ck', array());
$fee             = $this->getRequest()->getParam('fee', array());
$currency        = $this->getRequest()->getParam('currency', array());
$article         = $this->getRequest()->getParam('article', array());
$warranty        = $this->getRequest()->getParam('warranty', array());
$discount        = $this->getRequest()->getParam('discount', array());
$article_other   = $this->getRequest()->getParam('article_other', array());
$budget_info_id  = $this->getRequest()->getParam('budget_info_id', array());

$request_type_group  = $this->getRequest()->getParam('request_type_group');
$request_type  = $this->getRequest()->getParam('request_type');

$sn = $this->getRequest()->getParam('sn');
//$ids = $this->getRequest()->getParam('ids');

if (empty($name)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại..name</div>';
    die();
}

if (empty($type)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại..type</div>';
    die();
}

if (empty($urgent_date)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại..urgent_date</div>';
    die();
}

if (empty($area_id)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại..area_id</div>';
    die();
}

if (empty($shipping_address)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại..shipping_address</div>';
    die();
}


if (empty($department_id) || empty($team_id)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi :Vui lòng nhập phòng ban và team.</div>';
    die();
}


$simple_code_id = array_unique($pmodel_code_id);
if (count($simple_code_id) != count($pmodel_code_id)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : Bạn đang chọn 1 sản phẩm 2 lần.</div>';
    die();
}

if (empty($department_id)) {
    $department_id = $userStorage->department;
}

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {

    if (!$sn)
        $sn = date('YmdHis') . substr(microtime(), 2, 4);

    $where          = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?', $sn);
    $row_Purchasing = $QPurchasingRequest->fetchRow($where);

    $purchasing_request = array(
        'name'             => $name,
        'type'             => $type,
        'sn'               => $sn,
        'urgent_date'      => $urgent_date,
        'project_id'       => $project_id ? $project_id : NULL,
        'area_id'          => $area_id ? $area_id : NULL,
        'department_id'    => $department_id ? $department_id : NULL,
        'team_id'          => $team_id ? $team_id : NULL,
        'remark'           => $remark,
        'shipping_address' => $shipping_address,
        'status'           => 1, //new
        'created_by'       => $userStorage->id,
        'created_at'       => date('Y-m-d H:i:s'),
        'edit_pr'          => 0,
        'request_type_group' => !empty($request_type_group) ? $request_type_group : NULL,
        'request_type'       => !empty($request_type) ? $request_type : NULL
    );



    //UPLOAD FILE
    foreach ($_FILES['pr_quotation']['size'] as $k => $val) {

        if ($_FILES['pr_quotation']['size'][$k] != 0) {
            
            
            if($_FILES['pr_quotation']['size'][$k] > 140000000)
            {
                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                echo '<div class="alert alert-error">Lỗi : Dung lượng file uplooad vượt quá 14M</div>';
                exit;
            }

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-request' .
                    DIRECTORY_SEPARATOR . $sn;
            

            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile' => true));

            $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
            $upload->addValidator('Size', false, array('max' => '14MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');

            $name_file_k = "pr_quotation_" . $k . "_";
            $files       = $upload->getFileInfo($name_file_k);
            $hasPhoto    = false;
            $data_file   = array();

            if (isset($files[$name_file_k]) and $files[$name_file_k]) {
                $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);
                $upload->setDestination($uploaded_dir);

                //Rename
                $old_name = $fileInfo['name'];

                $tExplode  = explode('.', $old_name);
                $extension = end($tExplode); // đuôi file
                //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k.'_'. $old_name;
                $new_name  = $tExplode[0] . '.' . $extension;
                $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                $r         = $upload->receive(array($name_file_k));

                if ($r) {
                    $data_file['file_name'] = $new_name;
                }
                else {
                    $messages = $upload->getMessages();
                    foreach ($messages as $msg) {
                        throw new Exception(" ERROR: " . $msg . " !!");
                    }
                }
                //$purchasing_request['pr_quotation'] = $new_name;
                $QPurchasingRequestFile->insert(array('sn' => $sn, 'pr_quotation' => $new_name));
            }
        }
    }
    //END UPLOAD HÌNH ẢNH

    if (!empty($row_Purchasing)) {//TH Edit
        $pr_id = $row_Purchasing['id'];

        unset($purchasing_request['status']);
        unset($purchasing_request['created_by']);
        unset($purchasing_request['created_at']);
        
        $QPurchasingRequest->update($purchasing_request, $where);

        $where_details = $QPurchasingRequestDetails->getAdapter()->quoteInto('pr_id = ?', $pr_id);
        $QPurchasingRequestDetails->delete($where_details);
        $QPurchasingRequestSupplier->delete($where_details);
    }
    else {//TH create
        $pr_id = $QPurchasingRequest->insert($purchasing_request);
    }

    /////////////////////////

    //ADD Budget
    $where = [];
    $where[] = $QPurchasingPlan->getAdapter()->quoteInto('purchasing_id = ?', $pr_id);
    $where[] = $QPurchasingPlan->getAdapter()->quoteInto('del = ?', 0);
    $list_budget_old = $QPurchasingPlan->fetchAll($where);
    
    $budget_old = [];
    $budget_new = [];
    
    foreach($list_budget_old as $key=>$value){
        $budget_old[] = $value['plan_id'];
    }
    
    foreach($budget_info_id as $key=>$value){
        $budget_new[] = $value;
        
        if(!in_array($value, $budget_old)){
            $data = [
                'purchasing_id' => $pr_id,
                'plan_id'       => $value,
                'created_at'    => date('Y-m-d H:i:s'),
                'created_by'    => $userStorage->id
            ];

            $QPurchasingPlan->insert($data);
            
        }
    }
    
    foreach($budget_old as $key=>$value){
        if(!in_array($value, $budget_new)){
            $data = [
                'del' => 1,
            ];

            $where = NULL;
            $where = $QPurchasingPlan->getAdapter()->quoteInto('plan_id = ?', $value);

            $QPurchasingPlan->update($data, $where);
        }
    }
    //END ADD Budget

    foreach ($pmodel_code_id as $key => $val_code) {
        if (!empty($val_code) && !empty($pr_id)) {

            if (empty($quantity[$key]) || ($quantity[$key] < 0)) {
                throw new Exception(" ERROR !! Sản phẩm " . $cache_pmodel_code[$val_code]['name'] . ". Số lượng phải >= 0. Vui lòng kiểm tra lại !!");
            }

            ////DATA
            $purchasing_request_details = array(
                'pr_id'             => $pr_id,
                'pmodel_code_id'    => $val_code,
                'quantity'          => $quantity[$key],
                'installation_at'   => $installation_at[$key] ? $installation_at[$key] : "",
                'installation_time' => $installation_time[$key] ? $installation_time[$key] : "",
                'note'              => $note[$key] ? $note[$key] : "",
            );
            
            $pr_detail_id = 0;
            $pr_detail_id = $QPurchasingRequestDetails->insert($purchasing_request_details);


            if (!empty($supplier_id[$val_code]) && !empty($pr_detail_id)) {
            
                if(count($supplier_id[$val_code])>3){
                    throw new Exception(" ERROR !! Vui lòng chọn không quá 3 nhà cung cấp cho sản phẩm: " . $cache_pmodel_code[$val_code]['name'] . " !!");
                }
                
                foreach ($supplier_id[$val_code] as $k => $vl) {
                    if (!empty($vl)) {

                        $simple_supplier = array_unique($supplier_id[$val_code]);
                        if (count($simple_supplier) != count($supplier_id[$val_code])) {
                            throw new Exception(" ERROR !! Bạn đang chọn 1 NHÀ CUNG CẤP 2 LẦN trên sản phẩm: " . $cache_pmodel_code[$val_code]['name'] . " !!");
                        }

                        $purchasing_request_supplier = array(
                            'pr_id'           => $pr_id,
                            'pr_detail_id'    => $pr_detail_id,
                            'select_supplier' => ($select_supplier[$val_code] == $vl) ? 1 : 0,
                            'supplier_id'     => $supplier_id[$val_code][$k],
                            'price_before_dp' => My_Number::floatvalDecimal2($price_before_dp[$val_code][$k]),
                            'price'           => My_Number::floatvalDecimal2($price[$val_code][$k]),
                            'ck'              => My_Number::floatvalDecimal2($ck[$val_code][$k]),
                            'fee'             => My_Number::floatvalDecimal2($fee[$val_code][$k]),
                            'article'         => $article[$val_code][$k],
                            'currency'        => $currency[$val_code][$k],
                            'warranty'        => $warranty[$val_code][$k],
                            'discount'        => $discount[$val_code][$k],
                            'article_other'   => $article_other[$val_code][$k]
                        );

                        $QPurchasingRequestSupplier->insert($purchasing_request_supplier);


                        //UPLOAD FILE DETAIL
                        $file_name_upload = "file_supplier_detail_" . $val_code . "_" . $vl;


                        foreach ($_FILES[$file_name_upload]['size'] as $k3 => $val3) {

                            if ($_FILES[$file_name_upload]['size'][$k3] != 0) {
                                
                                if($_FILES[$file_name_upload]['size'][$k3] > 140000000)
                                {
                                    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                                    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                                    echo '<div class="alert alert-error">Lỗi : Dung lượng file uplooad vượt quá 14M</div>';
                                    exit;
                                }

                                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-request' .
                                        DIRECTORY_SEPARATOR . $sn;


                                $upload = new Zend_File_Transfer();
                                $upload->setOptions(array('ignoreNoFile' => true));

                                $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
                                $upload->addValidator('Size', false, array('max' => '14MB'));
                                $upload->addValidator('ExcludeExtension', false, 'php,sh');

                                $name_file_k = $file_name_upload . "_" . $k3 . "_";
                                $files       = $upload->getFileInfo($name_file_k);
                                $hasPhoto    = false;
                                $data_file   = array();

                                if (isset($files[$name_file_k]) and $files[$name_file_k]) {
                                    $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                                    if (!is_dir($uploaded_dir))
                                        @mkdir($uploaded_dir, 0777, true);
                                    $upload->setDestination($uploaded_dir);

                                    //Rename
                                    $old_name = $fileInfo['name'];
                                    $tExplode  = explode('.', $old_name);
                                    $extension = end($tExplode); // đuôi file
                                    
                                    //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k3.'_'. $old_name;
                                    $new_name  = $tExplode[0] . '.' . $extension;
                                    
                                    
                                    $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                                    $r         = $upload->receive(array($name_file_k));

                                    if ($r) {
                                        $data_file['file_name'] = $new_name;
                                    }
                                    else {
                                        $messages = $upload->getMessages();
                                        foreach ($messages as $msg) {
                                            throw new Exception(" ERROR: " . $msg . " !!");
                                        }
                                    }
                                    //$purchasing_request['pr_quotation'] = $new_name;
                                    $data_file_upload = array('sn'             => $sn,
                                        'pr_id'          => $pr_id,
                                        'pmodel_code_id' => $val_code,
                                        'supplier_id'    => $supplier_id[$val_code][$k],
                                        'pr_quotation'   => $new_name);

                                    $QPurchasingRequestFile->insert($data_file_upload);
                                }
                            }
                        }
                        //END UPLOAD DETAIL
                    }
                }//End foreach $supplier_id
            }
        }
    }//EndForeach
    $db->commit();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
    echo '<div class="alert alert-success">Done</div>';

    /// load lại trang
    $back_url = "create-purchasing-request?sn=" . $sn;
    echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
} catch (Exception $e) {
    $db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : ' . $e->getMessage() . '</div>';
}
exit;
