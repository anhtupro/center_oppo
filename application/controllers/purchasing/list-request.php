<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$QPurchasingRequest        = new Application_Model_PurchasingRequest();
$QPurchasingProject        = new Application_Model_PurchasingProject();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();

$sort           = $this->getRequest()->getParam('sort', '');
$desc           = $this->getRequest()->getParam('desc', 1);
$sn             = $this->getRequest()->getParam('sn');
$name           = $this->getRequest()->getParam('name');
$is_done        = $this->getRequest()->getParam('is_done');
$urgent_from   = $this->getRequest()->getParam('urgent_from');
$urgent_to     = $this->getRequest()->getParam('urgent_to');
$status        = $this->getRequest()->getParam('status');
$department_id = $this->getRequest()->getParam('department_id');

$export = $this->getRequest()->getParam('export', 0);

$params = array(
    'sn'             => trim($sn),
    'name'           => trim($name),    
    'urgent_from'    => $urgent_from,
    'urgent_to'      => $urgent_to,    
    'status'         => $status,
    'is_done'        => $is_done,
);
$page   = $this->getRequest()->getParam('page', 1);
$limit  = LIMITATION;
$total  = 0;
$result = $QPurchasingRequest->fetchPagination2($page, $limit, $total, $params);
$data_details = $data_total_sn = $data_change = array();
foreach ($result as $key => $value) {
    $details                    = $QPurchasingRequestDetails->getDetails($value['sn']);
    $data_details[$value['sn']] = $details;
    $total_num_sn = $total_price_sn = $total_price_sn_start = 0;
    if(!empty($details)){
        foreach($details as $k=>$v){            
            $quantity = $v['quantity_real'] ? $v['quantity_real'] : $v['quantity'];            
            $total_num_sn = $total_num_sn + $quantity;
            $money_ck = $price_after_ck = $total_price = 0;
            $money_ck = $v['price']*$v['ck']/100;
            $price_after_ck = $v['price'] - $money_ck;
            $total_price = $quantity*$price_after_ck+$v['fee'];
            $total_price_sn = $total_price_sn + $total_price;                       
            $total_price_start = $v['quantity_start']*$price_after_ck+$v['fee'];
            $total_price_sn_start = $total_price_sn_start + $total_price_start;
        }
        $data_total_sn[$v['sn']] = $total_price_sn;
        $data_change[$v['sn']] = $total_price_sn_start;
    }      
}
if(!empty($project_id)){

    $db = Zend_Registry::get('db');
    $select_pj = $db->select(); 
    $select_pj = " SELECT SUM(IFNULL(prs.price, 0) * IFNULL(p.quantity, 0)) AS `total` 
                FROM`purchasing_request_details` AS `p`
                LEFT JOIN `purchasing_request` AS `r` ON r.id = p.pr_id
                LEFT JOIN `pmodel_code` AS `c` ON c.id = p.pmodel_code_id
                LEFT JOIN ".DATABASE_SALARY.".`purchasing_request_supplier` AS `prs` ON p.id = prs.pr_detail_id AND prs.select_supplier = 1
                LEFT JOIN `supplier` AS `sup` ON prs.supplier_id = sup.id
                WHERE r.project_id = ".$project_id;
    $total_project = $db->fetchOne($select_pj); 
    echo $total_project;exit;
    $this->view->total_project        = $total_project;
}

$this->view->project      = $QPurchasingProject->fetchAll();
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->desc         = $desc;
$this->view->sort         = $sort;
$this->view->url          = HOST . 'purchasing/list-request' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);
$this->view->userStorage  = $userStorage;
$this->view->data_total_sn = $data_total_sn;

$QTrainerTypeAsset = new Application_Model_TrainerTypeAsset();
$whereTypeAsset    = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
$type_asset        = $QTrainerTypeAsset->fetchAll($whereTypeAsset);
$arrayTypeAsset    = array();
if ($type_asset->count()) {
    foreach ($type_asset as $key => $value) {
        $arrayTypeAsset[$value['id']] = $value['name'];
    }
}

$this->view->type_asset = $arrayTypeAsset;

$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages       = $messages;
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;

if ($this->getRequest()->isPost()) {
    $QRequestOffice = new Application_Model_RequestOffice();
    $link       = $this->getRequest()->getPost('link');
    $sn         = $this->getRequest()->getPost('sn');
    $is_done    = $this->getRequest()->getPost('is_done');
    $data = array(
        'is_done' => $is_done,
    );
    $where = $QRequestOffice->getAdapter()->quoteInto('pr_sn = ?', $sn);
    $QRequestOffice->update($data, $where);
    $this->redirect($link);
}
