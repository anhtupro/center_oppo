<?php

$code_id = $this->getRequest()->getParam('code_id');

$QCodeSupplier = new Application_Model_CodeSupplier();
$QSupplier = new Application_Model_Supplier();


try {
    if ($code_id) {
        
        $arr_supplier = $QSupplier->getSupplierDetailForProduct($code_id);
 
        if(!empty($arr_supplier)){
            echo json_encode(array('title_supplier' => $arr_supplier));
            exit;
        }

        echo json_encode(array());
        exit;
    } else {
        echo json_encode(array());
        exit;
    }
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}
