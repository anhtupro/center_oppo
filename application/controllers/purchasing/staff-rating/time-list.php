<?php
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$QPurchasingRatingTimes = new Application_Model_PurchasingRatingTimes();
$QPurchasingRatingStaff = new Application_Model_PurchasingRatingStaff();
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$name                   = $this->getRequest()->getParam('name');
$export                 = $this->getRequest()->getParam('export', 0);
$params = array(
        'name'              => trim($name), 
    );

$page   = $this->getRequest()->getParam('page', 1);
$limit  = 10;
$result = $QPurchasingRatingTimes->fetchPagination($page, $limit, $total, $params);   
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url = HOST . 'purchasing/time-list' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);
$this->view->leader       = $userStorage->id;
if ($this->getRequest()->isPost()) {           
    $name = $this->getRequest()->getPost('name', '');    
    $QPurchasingRatingTimes->insert(array('name'        => $name,     
                                          'created_by'  => $userStorage->id,
                                          'created_at'  => date('Y-m-d H:i:s'),));               
    $this->redirect(HOST . 'purchasing/time-list');
}
if ($export == 1) {
    
    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'NO.',
        'Phòng ban đánh giá',
        'Nhân viên được đánh giá',
        'Kỳ đánh giá',
        'Điểm trung bình',
        'Loại'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;

    $params['export'] = $export;
    $data = $QPurchasingRatingStaff->fetchPagination($page, $limit, $total, $params);   

    try {
        if ($data)
            foreach ($data as $_key => $_order) {
                $temp = '';
                if($_order['point'] > 1 && $_order['point'] < 2){$temp = 'Rất yếu';}
                elseif ($_order['point'] > 2 && $_order['point'] < 3) {$temp = 'Yếu';}
                elseif ($_order['point'] > 3 && $_order['point'] < 4) {$temp = 'Trung bình';}
                elseif ($_order['point'] > 4 && $_order['point'] < 5) {$temp = 'Tốt';}
                elseif ($_order['point'] = 5) {$temp = 'Rất tốt';}
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['department_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['rating_staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['rating_time_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['point'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($temp, PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename  = 'Report_PR_Rating_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}