<?php
$id                     = $this->getRequest()->getParam('id');
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$QPurchasingRatingTimes         = new Application_Model_PurchasingRatingTimes();
$QPurchasingRatingStaff         = new Application_Model_PurchasingRatingStaff();
$QPurchasingRatingSurveyDetail  = new Application_Model_PurchasingRatingSurveyDetail();
$QPurchasingRatingStaffDetail = new Application_Model_PurchasingRatingStaffDetail();
$page   = $this->getRequest()->getParam('page', 1);
$limit  = 5;

$params = array(
    'id_rating_times' => $id,
);

$result = $QPurchasingRatingStaff->fetchPagination($page, $limit, $total, $params);   
$this->view->list         = $result;
$this->view->limit        = $limit;
$this->view->total        = $total;
$this->view->params       = $params;
$this->view->url = HOST . 'purchasing/time-rating?id=' . $id . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset       = $limit * ($page - 1);

$this->view->staff              = $QPurchasingRatingStaff->get_cache();
$this->view->survey             = $QPurchasingRatingSurveyDetail->fetchAll();
$this->view->user_id            = $userStorage->id;
if (!empty($id)) {    
    $rowset                 = $QPurchasingRatingTimes->find($id);
    $this->view->time       = $rowset->current();
}else{    
    $this->redirect(HOST . 'purchasing/time-list');    
}

if ($this->getRequest()->isPost()) {           
    $point                  = $this->getRequest()->getPost('point');
    $note                   = $this->getRequest()->getPost('note');
    $staff_id               = $this->getRequest()->getPost('staff_id');  
    $survey_points          = $this->getRequest()->getPost('servey_point');
    $del                    = $this->getRequest()->getPost('del'); 
    
    if($del){
        $where_1 = $QPurchasingRatingStaff->getAdapter()->quoteInto('id = ?', $del);
        $where_2 = $QPurchasingRatingStaffDetail->getAdapter()->quoteInto('id_rating_staff = ?', $del);
        $QPurchasingRatingStaff->delete($where_1);
        $QPurchasingRatingStaffDetail->delete($where_2);
    }else{
       
        $id_ = $QPurchasingRatingStaff->insert(array(
                                                'id_rating_times'  => $id,
                                                'created_by'       => $userStorage->id,   
                                                'created_at'       => date('Y-m-d H:i:s'),
                                                'point'            => $point,
                                                'staff_id'         => $staff_id,
                                                'note'             => $note,
                                            ));  
        foreach($survey_points as $key=>$value){
        $QPurchasingRatingStaffDetail->insert(array(
                                                'id_rating_staff' => $id_,
                                                'staff_id'        => $staff_id,
                                                'survey_id'       => $key,
                                                'point'           => $value,
                ));
        }
    }
    $this->redirect(HOST . 'purchasing/time-rating?id=' . $id);
}