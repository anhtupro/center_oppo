<?php 
$this->_helper->layout->disableLayout();  
$sn                     = $this->getRequest()->getParam('sn'); 
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();


$QPurchasingCost	    = new Application_Model_PurchasingCost();
$QPurchasingCostDetails    = new Application_Model_PurchasingCostDetails();
$QSupplier                  = new Application_Model_Supplier();
$supplier                   = $QSupplier->get_cache();
$QTeam = new Application_Model_Team();
$department = $QTeam->get_department();
$this->view->department = $department;
$QArea = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();

if ($sn) {

    $params = array(
        'sn'              	=> $sn,
    );


    $purchasing_Cost = $QPurchasingCost->getinfoCOST($params);
    $this->view->purchasing_order = $purchasing_Cost;//PHIẾU ĐỀ NGHỊ THANH TOÁN
 
    $result         = $QPurchasingCostDetails->getListCostPrint($params);//PO

    $total_price_PO = 0;
    foreach ($result as $value) {
        $total_price_PO = $total_price_PO + $value['total_price'];
    }


    $this->view->total_price_PO= $total_price_PO;
    $this->view->supplier= $supplier;
    $this->view->list    = $result;
    $this->view->params  = $params;

}
