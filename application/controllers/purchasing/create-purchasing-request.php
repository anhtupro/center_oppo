<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$sn     = $this->getRequest()->getParam('sn');
$params = array(
    'sn' => $sn,
);

$QPurchasingRequest     = new Application_Model_PurchasingRequest();
$QPurchasingRequestFile = new Application_Model_PurchasingRequestFile();
$QPurchasingProject     = new Application_Model_PurchasingProject();
$QPurchasingType        = new Application_Model_PurchasingType();
$QPmodelCode            = new Application_Model_PmodelCode();
$QRegionalMarket        = new Application_Model_RegionalMarket();
$QTeam                  = new Application_Model_Team();
$QRequestOffice         = new Application_Model_RequestOffice();
$QCurrency              = new Application_Model_Currency();
$QRequestTypeGroup      = new Application_Model_RequestTypeGroup();
$QRequestType           = new Application_Model_RequestType();
$QPurchasingPlan        = new Application_Model_PurchasingPlan();

$QDepartment            = new Application_Model_Department();
$department             = $QTeam->get_list_department();
$this->view->department = $department;
$recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

$currency = $QCurrency->get_cache();

$pmodel_code = $QPmodelCode->getPmodelPrice($sn);

$request_type_group     = $QRequestTypeGroup->fetchAll(['department_id = ?' => $userStorage->department]);

$this->view->request_type_group = $request_type_group;
$request_type           = $QRequestType->getRequetsType($userStorage->department);
$this->view->request_type = $request_type;

if (!empty($sn)) {
    $QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
    $QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();

    //General Infor
    $purchasing_request             = $QPurchasingRequest->getData($params);
    $this->view->purchasing_request = $purchasing_request;

    //Details
    $purchasing_request_details             = $QPurchasingRequestDetails->getListProduct($params);
    $this->view->purchasing_request_details = $purchasing_request_details;

    $purchasing_request_supplier             = $QPurchasingRequestSupplier->getListSupplier($params);
    $this->view->purchasing_request_supplier = $purchasing_request_supplier;

    $where_file   = array();
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('sn = ?', $sn);
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pr_id is null', null);
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pmodel_code_id is null', null);
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('supplier_id is null', null);
    $list_file    = $QPurchasingRequestFile->fetchAll($where_file);
    if (!empty($list_file)) {
        $this->view->list_file = $list_file->toArray();
    }


    $where_file_detail   = array();
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('sn = ?', $sn);
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pr_id is not null', null);
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pmodel_code_id is not null', null);
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('supplier_id is not null', null);
    $list_file_detail    = $QPurchasingRequestFile->fetchAll($where_file_detail);
    if (!empty($list_file_detail)) {
        $this->view->list_file_detail = $list_file_detail->toArray();
    }
    
    //$purchasingTypeByDepartment  = $QPurchasingType->getCacheTypeByDepartment();
    //$this->view->purchasing_type = $purchasingTypeByDepartment[$purchasing_request['department_id']];
    if($purchasing_request['team_id']){
        $purchasingTypeByTeam  = $QPurchasingType->getCacheTypeByTeam($purchasing_request['team_id']);
        $this->view->purchasing_type = $purchasingTypeByTeam;
    }
    
    $file_request_office = $QRequestOffice->getListFileFromPr($sn);
    $this->view->file_request_office = $file_request_office;

    $list_budget = $QPurchasingPlan->getPurchasingPlan($params);
    $this->view->list_budget = json_encode($list_budget);

    if($purchasing_request['request_type']){
        $request_type_info = $QRequestType->getInfoById($purchasing_request['request_type']);
        $this->view->request_type_info = $request_type_info;
    }
    
    
}else {
    //trường hợp tạo mới
    $purchasing_request = array();

    $where_rm           = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $userStorage->regional_market);
    $row_RegionalMarket = $QRegionalMarket->fetchRow($where_rm);
    if ($row_RegionalMarket['area_id']) {
        $purchasing_request['area_id'] = $row_RegionalMarket['area_id'];
    }
    
    $purchasing_request['department_id'] = $userStorage->department;    
    $purchasing_request['team_id'] = $userStorage->team;
    $this->view->purchasing_request      = $purchasing_request;

    //$purchasingTypeByDepartment  = $QPurchasingType->getCacheTypeByDepartment();
    //$this->view->purchasing_type = $purchasingTypeByDepartment[$userStorage->department];
    
    if($purchasing_request['team_id']){
        $purchasingTypeByTeam  = $QPurchasingType->getCacheTypeByTeam($purchasing_request['team_id']);
        $this->view->purchasing_type = $purchasingTypeByTeam;
    }
    
}

//Kiểm tra không cho team khác tạo sản phẩm
$is_team_purchasing = false;
if($userStorage->department == 400 || $userStorage->id == 2464 || $userStorage->id == 5899){
    $is_team_purchasing = true;
}
$this->view->is_team_purchasing = $is_team_purchasing;
//END - Kiểm tra không cho team khác tạo sản phẩm

$this->view->params = $params;

$this->view->pmodel_code = $pmodel_code;

$this->view->purchasing_project = $QPurchasingProject->fetchAll();

$QArea             = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();

$this->view->currency = $currency;
