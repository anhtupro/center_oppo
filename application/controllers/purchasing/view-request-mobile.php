<?php

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$sn     = $this->getRequest()->getParam('sn');
$params = array(
    'sn' => $sn,
);

$QPurchasingRequest     = new Application_Model_PurchasingRequest();
$QPurchasingRequestFile = new Application_Model_PurchasingRequestFile();
$QPurchasingProject     = new Application_Model_PurchasingProject();
$QPurchasingType        = new Application_Model_PurchasingType();
$QPmodelCode            = new Application_Model_PmodelCode();
$QRegionalMarket        = new Application_Model_RegionalMarket();
$QTeam                  = new Application_Model_Team();
$QCurrency              = new Application_Model_Currency();

$QDepartment            = new Application_Model_Department();
$department             = $QTeam->get_list_department();
$this->view->department = $department;
$recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

$currency = $QCurrency->get_cache();

$pmodel_code = $QPmodelCode->getPmodelPrice($sn);

if (!empty($sn)) {
    $QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
    $QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();

    //General Infor
    $purchasing_request             = $QPurchasingRequest->getDataFull($params);
    $this->view->purchasing_request = $purchasing_request;

    //Details
    $purchasing_request_details             = $QPurchasingRequestDetails->getListProductInfo($params);
    $this->view->purchasing_request_details = $purchasing_request_details;

    $purchasing_request_supplier             = $QPurchasingRequestSupplier->getListSupplier($params);
    $this->view->purchasing_request_supplier = $purchasing_request_supplier;

    $where_file   = array();
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('sn = ?', $sn);
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pr_id is null', null);
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pmodel_code_id is null', null);
    $where_file[] = $QPurchasingRequestFile->getAdapter()->quoteInto('supplier_id is null', null);
    $list_file    = $QPurchasingRequestFile->fetchAll($where_file);
    if (!empty($list_file)) {
        $this->view->list_file = $list_file->toArray();
    }


    $where_file_detail   = array();
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('sn = ?', $sn);
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pr_id is not null', null);
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('pmodel_code_id is not null', null);
    $where_file_detail[] = $QPurchasingRequestFile->getAdapter()->quoteInto('supplier_id is not null', null);
    $list_file_detail    = $QPurchasingRequestFile->fetchAll($where_file_detail);
    if (!empty($list_file_detail)) {
        $this->view->list_file_detail = $list_file_detail->toArray();
    }
    
    $purchasingTypeByDepartment  = $QPurchasingType->getCacheTypeByDepartment();
    $this->view->purchasing_type = $purchasingTypeByDepartment[$purchasing_request['department_id']];
    
}else {
    //trường hợp tạo mới
    $purchasing_request = array();

    $where_rm           = $QRegionalMarket->getAdapter()->quoteInto('id = ?', $userStorage->regional_market);
    $row_RegionalMarket = $QRegionalMarket->fetchRow($where_rm);
    if ($row_RegionalMarket['area_id']) {
        $purchasing_request['area_id'] = $row_RegionalMarket['area_id'];
    }
    $purchasing_request['department_id'] = $userStorage->department;
    $this->view->purchasing_request      = $purchasing_request;

    $purchasingTypeByDepartment  = $QPurchasingType->getCacheTypeByDepartment();
    $this->view->purchasing_type = $purchasingTypeByDepartment[$userStorage->department];
}

$this->view->params = $params;

$this->view->pmodel_code = $pmodel_code;

$this->view->purchasing_project = $QPurchasingProject->fetchAll();

$QArea             = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();
$this->view->currency = $currency;