<?php

$sort            = $this->getRequest()->getParam('sort', '');
$desc            = $this->getRequest()->getParam('desc', 1);
$sn              = $this->getRequest()->getParam('sn');
$name            = $this->getRequest()->getParam('name');
$remark          = $this->getRequest()->getParam('remark');
$code            = $this->getRequest()->getParam('code');
$area_id         = $this->getRequest()->getParam('area_id');
$created_at_from = $this->getRequest()->getParam('created_at_from');
$created_at_to   = $this->getRequest()->getParam('created_at_to');
$export          = $this->getRequest()->getParam('export', 0);

$QArea                 = new Application_Model_Area();
$QPurchasingCostAccess = new Application_Model_PurchasingCostAccess();
$this->view->areas     = $QArea->get_cache();
$showrooms_list        = $QArea->get_cache_showroom();
$this->view->showrooms = $showrooms_list;


$params = array(
    'sn'              => $sn,
    'code'            => $code,
    'remark'          => $remark,
    'name'            => $name,
    'area_id'         => $area_id,
    'created_at_from' => $created_at_from,
    'created_at_to'   => $created_at_to
);

$params['sort'] = $sort;
$params['desc'] = $desc;


//////////////////////////////////////////////////// phân quyền

$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$user_id         = $userStorage->id;
$group_id        = $userStorage->group_id;
$user_department = $userStorage->department;
 
if ($user_id == SUPERADMIN_ID || in_array($group_id, array( ADMINISTRATOR_ID,HR_ID,HR_EXT_ID,BOARD_ID))) {
    //full quyền NHÓM ƯU TIÊN
}else{
    
    // cấp quyền trong bảng purchasing_cost_access    
    $where_access  = $QPurchasingCostAccess->getAdapter()->quoteInto('staff_id = ?', $user_id);
    $result_access = $QPurchasingCostAccess->fetchAll($where_access)->toArray();
    if (empty($result_access)) {
        $params['only_me'] = $user_id; // chỉ ng tạo mới thấy
    } else {
        $area_access       = $showroom_access   = $department_access = array();
        foreach ($result_access as $val) {
            if ($val['access_full'] == 1) {
                $access_full = 1; // FULL QUYỀN
            }
            if (!empty($val['area_id'])) {
                $area_access[] = $val['area_id'];
            }
            if (!empty($val['showroom_id'])) {
                $showroom_access[] = $val['showroom_id'];
            }
            if (!empty($val['department_id'])) {
                $department_access[] = $val['department_id'];
            }
        }//End foreach

        if (empty($access_full)) {// cấp theo KHU VỰC or SHOWROOM or PHÒNG BAN
            if (!empty($area_access)) {
                $params['area_access'] = $area_access;
            }
            if (!empty($showroom_access)) {
                $params['showroom_access'] = $showroom_access;
            }
            if (!empty($department_access)) {
                $params['department_access'] = $department_access;
            }
        }//End if
    }//End else
}
/////////////////////////////////////////////
 

$QPurchasingOrder = new Application_Model_PurchasingOrder();
$QPurchasingCost  = new Application_Model_PurchasingCost();
$page             = $this->getRequest()->getParam('page', 1);
$limit            = LIMITATION;
$total            = 0;
$result           = $QPurchasingCost->listCOST($page, $limit, $total, $params);


$this->view->list   = $result;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->params = $params;
$this->view->desc   = $desc;
$this->view->sort   = $sort;
$this->view->url    = HOST . 'purchasing/cost' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset = $limit * ($page - 1);


$QTrainerTypeAsset = new Application_Model_TrainerTypeAsset();
$whereTypeAsset    = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL', 0);
$type_asset        = $QTrainerTypeAsset->fetchAll($whereTypeAsset);
$arrayTypeAsset    = array();
if ($type_asset->count()) {
    foreach ($type_asset as $key => $value) {
        $arrayTypeAsset[$value['id']] = $value['name'];
    }
}

$this->view->type_asset     = $arrayTypeAsset;
$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages       = $messages;
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;


if ($export == 2) { // report COST $export==2
    // no limit time
    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'NO.',
        'Sn',
        'COST name',
        'Payment date',
        'Remark',
        'Status',
        'Product',
        'Supplier',
        'Quantity',
        'price',
        'VAT%',
        'Total',
        'Total COST',
        'Invoice Number',
        'Invoice Date',
        'note',
        'Area',
        'department',
        'showroom',
        'Created By',
        'Created At',
        'Confirm By',
        'Confirm At'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;

    $data = $QPurchasingCost->reportCOST($params);

    try {
        if ($data)
            foreach ($data as $_key => $_order) {
                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['sn'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['cost_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['delivery_date'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['remark'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['status'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['product'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['supplier'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['quantity'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['vat'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['total_price'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['total_cost'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['invoice_number'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['invoice_date'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['note'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['area'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['department'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($showrooms_list[$_order['showroom_id']], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['created_by_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['created_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['confirm_by_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['confirm_at'], PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename  = 'Report_COST_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}
