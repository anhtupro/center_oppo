<?php

$this->_helper->layout->disableLayout();
$userStorage               = Zend_Auth::getInstance()->getStorage()->read();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
$QSupplier                 = new Application_Model_Supplier();
$QPurchasingType           = new Application_Model_PurchasingType();
$QTeam                     = new Application_Model_Team();

$department                 = $QTeam->get_list_department();
$this->view->department     = $department;
$cache_Supplier             = $QSupplier->get_cache();
$this->view->cache_Supplier = $cache_Supplier;
$purchasingType             = $QPurchasingType->get_cache_all();
$this->view->purchasingType = $purchasingType;

$list_detail_id = $this->getRequest()->getParam('list_detail_id');

$params = array('list_detail_id' => $list_detail_id);

//$this->view->params = $params;
$listData                = $QPurchasingRequestDetails->getCreateSubmissionSupplier($params);
$this->view->listData    = $listData;
$this->view->userStorage = $userStorage;
 