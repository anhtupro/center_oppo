<?php
    $QPurchasingType                = new Application_Model_PurchasingType();
    $QTeam                          = new Application_Model_Team();
    $department                     = $QTeam->get_list_department();    
    $purchasing_type                = $QPurchasingType->get_cache();
    $this->view->department         = $department;
    $this->view->purchasing_type    = $purchasing_type;
    

    $name               = $this->getRequest()->getParam('name');
    $department_id      = $this->getRequest()->getParam('department_id');
    $department_name    = $this->getRequest()->getParam('department_name');
    $del                = $this->getRequest()->getParam('del');
    $team_id            = $this->getRequest()->getParam('team_id');
    $team_name          = $this->getRequest()->getParam('team_name');
    
    /*---------------------------------------------- */
    $recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    /*---------------------------------------------- */
    $params = array(
        'name'              => trim($name),        
        'department_id'     => $department_id,
        'department_name'   => trim($department_name),
        'del'               => $del,
        'team_id'           => $team_id,
        'team_name'         => $team_name,
    );

    $page   = $this->getRequest()->getParam('page', 1);
    $limit  = 10;
    
    $result = $QPurchasingType->fetchPagination($page, $limit, $total, $params);    
    $this->view->list         = $result;
    $this->view->limit        = $limit;
    $this->view->total        = $total;
    $this->view->params       = $params;
    $this->view->url = HOST . 'purchasing/manage-type' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
    $this->view->offset       = $limit * ($page - 1);
    
    $name           = $this->getRequest()->getParam('name');
    $department_id  = $this->getRequest()->getParam('department_id');
    $team_id        = $this->getRequest()->getParam('team_id');
    $link = $this->getRequest()->getPost('link');
    if ($this->getRequest()->isPost()) {
        $model = new Application_Model_PurchasingType();
        $model->addType($name, $department_id, $team_id);
        $this->redirect($link);
    }
