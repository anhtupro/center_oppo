<?php
if ($this->getRequest()->isPost()) {
        $id                 = $this->getRequest()->getPost('id');
        $name               = $this->getRequest()->getPost('name');
        $department_id      = $this->getRequest()->getPost('department_id');
        $team_id            = $this->getRequest()->getPost('team_id');
        $link               = $this->getRequest()->getPost('link');
        $types              = new Application_Model_PurchasingType();            
        $types->updateType($id ,$name, $department_id, $team_id);              
        $this ->redirect($link);
    } else{}
