<?php
if ($this->getRequest()->isPost()) {
    $del = $this->getRequest()->getPost('hide');
    $id = $this->getRequest()->getPost('id');
    $link = $this->getRequest()->getPost('link');
    $types = new Application_Model_PurchasingType();
    $types->hideType($id, $del);
    }
    $this->redirect($link);
