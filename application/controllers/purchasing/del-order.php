<?php

$flashMessenger = $this->_helper->flashMessenger;

$db = Zend_Registry::get('db');
$db->beginTransaction();

$back_url = HOST.'purchasing/order';

try
{

	$QPurchasingOrder           = new Application_Model_PurchasingOrder();
	$QPurchasingOrderDetails    = new Application_Model_PurchasingOrderDetails();
	$QPurchasingRequest         = new Application_Model_PurchasingRequest();
	$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
	$userStorage                = Zend_Auth::getInstance()->getStorage()->read();

	$sn 	= $this->getRequest()->getParam('sn');

	$where = $QPurchasingOrder->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_order = $QPurchasingOrder->fetchRow($where);

	if(!$purchasing_order){
	    $flashMessenger->setNamespace('error')->addMessage('SN không tồn tại!');
    	$this->redirect($back_url);
	}

	//Xóa hết tất cả order details cũ để insert lại
	$where_details = $QPurchasingOrderDetails->getAdapter()->quoteInto('po_id = ?', $purchasing_order['id']);
	$purchasing_order_details = $QPurchasingOrderDetails->fetchAll($where_details);
	foreach($purchasing_order_details as $key=>$value){
		$where_pr = $QPurchasingRequestDetails->getAdapter()->quoteInto('id = ?', $value['pr_details_id']);
		$purchasing_request_details = $QPurchasingRequestDetails->fetchRow($where_pr);
		$pr_update = array(
			'po_quantity'	=> 	($purchasing_request_details['po_quantity'] - $value['quantity'])
		);
		$QPurchasingRequestDetails->update($pr_update, $where_pr);
	}

	$QPurchasingOrderDetails->delete($where_details);
	//END 
        
	$QPurchasingOrder->delete($where);

	$db->commit();

	$flashMessenger->setNamespace('success')->addMessage('Done!');
        $this->redirect($back_url);
}
catch (Exception $e)
{
	$db->rollBack();
	$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect($back_url);
}
exit;