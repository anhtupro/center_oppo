<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
$url_print_submission = HOST . 'purchasing/print-submission-supplier?list_detail_id='; 

echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';
$arr_detail_id = $this->getRequest()->getParam('arr_detail_id',array());

//$db = Zend_Registry::get('db');
//$db->beginTransaction();

try {

    if (empty($arr_detail_id)) {
        throw new Exception('Vui lòng chọn sản phẩm để in tờ trình..!!');
    }else{
        
        $list_detail_id = implode(",",$arr_detail_id);
        $url_print_submission = $url_print_submission.$list_detail_id;

        //$db->commit();
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
        echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
        echo '<div class="alert alert-success">Success</div>';

        /// load lại trang
        //$back_url = "supplier-create?id=" . $id;
        echo '<script>setTimeout(function(){parent.location.href="' . $url_print_submission . '"}, 500)</script>';
        
    }
    
} catch (Exception $e) {
    //$db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">ERROR : ' . $e->getMessage() . '</div>';
}
exit;
