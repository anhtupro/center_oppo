<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';

$db = Zend_Registry::get('db');
$db->beginTransaction();

try
{
	$QPurchasingCost         = new Application_Model_PurchasingCost();
        $QPurchasingCostFile     = new Application_Model_PurchasingCostFile();
	$QPurchasingCostDetails  = new Application_Model_PurchasingCostDetails();
	$userStorage             = Zend_Auth::getInstance()->getStorage()->read();
 
	$cost_name         = $this->getRequest()->getParam('cost_name');
	$delivery_date     = $this->getRequest()->getParam('delivery_date');
	$remark    	   = $this->getRequest()->getParam('remark');
        
	$product_id    	   = $this->getRequest()->getParam('product_id');
	$supplier_id   	   = $this->getRequest()->getParam('supplier_id');
        $area_id   	   = $this->getRequest()->getParam('area_id');
        $showroom_id   	   = $this->getRequest()->getParam('showroom_id');
        $department_id 	   = $this->getRequest()->getParam('department_id');
	$quantity    	   = $this->getRequest()->getParam('quantity');
	$price    	   = $this->getRequest()->getParam('price');
	$vat    	   = $this->getRequest()->getParam('vat');
	$total_price   	   = $this->getRequest()->getParam('total_price');
	$invoice_number    = $this->getRequest()->getParam('invoice_number');
	$invoice_date  	   = $this->getRequest()->getParam('invoice_date');
	$note     	   = $this->getRequest()->getParam('note');
        
	$sn   		= $this->getRequest()->getParam('sn');
	$ids              = $this->getRequest()->getParam('ids');
       
        
    if(empty($cost_name)||empty($delivery_date)){
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại.</div>';	
        die();
    }          
    if(empty($product_id)||(in_array("", $product_id))||(in_array(0, $price))||(in_array(0, $quantity))){
        echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại.</div>';	
        die();
    }        
    
    
    if (!$sn)
	$sn = date ( 'YmdHis' ) . substr ( microtime (), 2, 4 );
    
	$where = $QPurchasingCost->getAdapter()->quoteInto('sn = ?', $sn);
	$row_Purchasing_cost   =  $QPurchasingCost->fetchRow($where);
	
	$cost_request = array(
		'sn' 		=> $sn,
                'cost_name'     => $cost_name,
                'area_id'       => $area_id,
                'department_id' => $department_id,            
		'delivery_date'	=> $delivery_date,
		'remark'	=> $remark,
		'status' 	=> 'New',
		'created_by' 	=> $userStorage->id,
		'created_at'	=> date('Y-m-d H:i:s')
	);
        if($department_id==159){
            if(empty($showroom_id)){
                    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                    echo '<div class="alert alert-error">Lỗi :Bộ phận Service. Vui lòng chọn Showroom.!</div>';	
                    die();
            }else{
                $cost_request['showroom_id'] = $showroom_id;
            }                
        }

        
	//UPLOAD FILE
        
        foreach ($_FILES['pr_quotation']['size'] as $k => $val) {

            if ($_FILES['pr_quotation']['size'][$k] != 0) {
                
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-cost' .
                DIRECTORY_SEPARATOR . $sn;


                $upload = new Zend_File_Transfer();
                $upload->setOptions(array('ignoreNoFile'=>true));

                //check function
//                if (function_exists('finfo_file')){
//                    $upload->addValidator('MimeType', false, array('application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));
//                }

                $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
                $upload->addValidator('Size', false, array('max' => '4MB'));
                $upload->addValidator('ExcludeExtension', false, 'php,sh');
                
                $name_file_k = "pr_quotation_".$k."_";
                $files = $upload->getFileInfo($name_file_k);
                $hasPhoto = false;
                $data_file = array();
               
                
                if(isset($files[$name_file_k]) and $files[$name_file_k]){
                    $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                        if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                        $upload->setDestination($uploaded_dir);

                        //Rename
                        $old_name = $fileInfo['name'];                        
                        $tExplode = explode('.', $old_name);
                        $extension = end($tExplode);// đuôi file
                        //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k.'_'. $old_name;
                        $new_name = 'UPLOAD-' .date('Ymd').substr ( microtime (), 2, 4 ) . md5(uniqid('', true)) . '.' . $extension;
                        $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                        
                        $r = $upload->receive(array($name_file_k));
                        if($r){
                            $data_file['file_name'] = $new_name;
                        }
                        else{
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg){
                                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                                            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                                            echo '<div class="alert alert-error">Lỗi : '.$msg.'</div>';	
                                            exit;
                            }
                        }
                        //$cost_request['pr_quotation'] = $new_name;
                        $QPurchasingCostFile->insert(array('sn'=>$sn, 'pr_quotation'=>$new_name));
                }
            }
        }
        
    //END UPLOAD HÌNH ẢNH
    
    if(!empty($row_Purchasing_cost)){
        
        $id = $row_Purchasing_cost['id'];
        unset($cost_request['created_by']);
        unset($cost_request['created_at']);
        $cost_request['updated_by'] = $userStorage->id;
        $cost_request['updated_at'] = date('Y-m-d H:i:s');

        $QPurchasingCost->update($cost_request, $where);

        $where_details = $QPurchasingCostDetails->getAdapter()->quoteInto('pc_id = ?', $id);
        $QPurchasingCostDetails->delete($where_details);

    }else{
        $id = $QPurchasingCost->insert($cost_request);
    }
	

	foreach($product_id as $key=>$value){
		$cost_request_details = array_filter(array(
			'pc_id' => $id,
			'product_id'	=> $product_id[$key],
			'quantity'	=> $quantity[$key],
			'price'		=> My_Number::floatval($price[$key]),
                        'vat'		=> My_Number::floatval($vat[$key]),
			'total_price'	=> My_Number::floatval($total_price[$key]),
                        'invoice_number'=> $invoice_number[$key],
                        'invoice_date'  => $invoice_date[$key],
                        'note'          => $note[$key],
		));
		if($supplier_id[$key]){
                    $cost_request_details['supplier_id']=$supplier_id[$key];
                }
		$QPurchasingCostDetails->insert($cost_request_details);
	}

	$db->commit();
	echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
	echo '<div class="alert alert-success">Done</div>';
        /// load lại trang
        $back_url =  "cost";
        echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
}
catch (Exception $e)
{
	$db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi : '.$e->getMessage().'</div>';	
}
exit;