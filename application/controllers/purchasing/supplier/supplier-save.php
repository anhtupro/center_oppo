<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';

$QSupplier     = new Application_Model_PurchasingSupplier();
$QSupplierFile = new Application_Model_SupplierFile();

$id = $this->getRequest()->getParam('id');

$name    = $this->getRequest()->getParam('name', '');
$title   = $this->getRequest()->getParam('title', '');
$address = $this->getRequest()->getParam('address', '');

$website = $this->getRequest()->getParam('website', '');
$phone   = $this->getRequest()->getParam('phone', '');
$email   = $this->getRequest()->getParam('email', '');

$surrogater_name  = $this->getRequest()->getParam('surrogater_name', '');
$surrogater_title = $this->getRequest()->getParam('surrogater_title', '');
$surrogater_phone = $this->getRequest()->getParam('surrogater_phone', '');
$surrogater_email = $this->getRequest()->getParam('surrogater_email', '');

$group_product    = $this->getRequest()->getParam('group_product', '');
$product_services = $this->getRequest()->getParam('product_services', '');
$info_history     = $this->getRequest()->getParam('info_history', '');

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {

    if (empty($name) || empty($title) || empty($address) ) {
        throw new Exception(' Vui lòng không để trống các trường bắt buộc');
    }

    $data = array(//array_filter(
        'name'    => trim($name),
        'title'   => trim($title),
        'address' => trim($address),
        'website' => trim($website),
        'phone'   => trim($phone),
        'email'   => trim($email),
        'surrogater_name'  => trim($surrogater_name),
        'surrogater_title' => trim($surrogater_title),
        'surrogater_phone' => trim($surrogater_phone),
        'surrogater_email' => trim($surrogater_email),
        'group_product'    => !empty($group_product) ? $group_product : 0,
        'product_services' => trim($product_services),
        'info_history'     => trim($info_history)
    );



    if ($id) { // save
        $where = $QSupplier->getAdapter()->quoteInto('id = ?', $id);
        $QSupplier->update($data, $where);
    }
    else { // create new
        $id = $QSupplier->insert($data);
    }

    //UPLOAD FILE
    foreach ($_FILES['supplier_file']['size'] as $k => $val) {

        if ($_FILES['supplier_file']['size'][$k] != 0) {

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-request' .
                    DIRECTORY_SEPARATOR . $id;


            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile' => true));

            $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
            $upload->addValidator('Size', false, array('max' => '4MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');

            $name_file_k = "supplier_file_" . $k . "_";
            $files       = $upload->getFileInfo($name_file_k);
            $hasPhoto    = false;
            $data_file   = array();

            if (isset($files[$name_file_k]) and $files[$name_file_k]) {
                $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);
                $upload->setDestination($uploaded_dir);

                //Rename
                $old_name = $fileInfo['name'];

                $tExplode  = explode('.', $old_name);
                $extension = end($tExplode); // đuôi file
                //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k.'_'. $old_name;
                $new_name  = 'UPLOAD-' . date('Ymd') . substr(microtime(), 2, 4) . md5(uniqid('', true)) . '.' . $extension;
                $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                $r         = $upload->receive(array($name_file_k));

                if ($r) {
                    $data_file['file_name'] = $new_name;
                }
                else {
                    $messages = $upload->getMessages();
                    foreach ($messages as $msg) {
                        throw new Exception(" ERROR: " . $msg . " !!");
                    }
                }
                
                $QSupplierFile->insert(array('supplier_id' => $id, 'name_file' => $new_name));
            }
        }
    }
    //END UPLOAD HÌNH ẢNH
    
    
    $db->commit();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
    echo '<div class="alert alert-success">Done</div>';

    /// load lại trang
    $back_url = "supplier-create?id=" . $id;
    echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
} catch (Exception $e) {
    $db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">ERROR : ' . $e->getMessage() . '</div>';
}
exit;
