<?php

$id            = $this->getRequest()->getPost('id');
$QSupplierFile = new Application_Model_SupplierFile();
$userStorage   = Zend_Auth::getInstance()->getStorage()->read();

$response = array('status' => 0);
if (!empty($id)) {

    $where    = $QSupplierFile->getAdapter()->quoteInto('id = ?', $id);
    $File_row = $QSupplierFile->fetchRow($where);

    
    if (!empty($File_row)) {

        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-request' .
                DIRECTORY_SEPARATOR . $File_row['supplier_id'] . DIRECTORY_SEPARATOR . $File_row['name_file'];

        unlink($uploaded_dir);
        $QSupplierFile->delete($where);
    }
    $response['status']  = 1;
    $response['message'] = "Thành công";
}
else {
    $response['message'] = "Thiếu id";
}
echo json_encode($response);
exit();
