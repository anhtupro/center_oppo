<?php

$page             = $this->getRequest()->getParam('page', 1);
$title            = $this->getRequest()->getParam('title', '');
$name             = $this->getRequest()->getParam('name', '');
$address          = $this->getRequest()->getParam('address', '');
$surrogater_name  = $this->getRequest()->getParam('surrogater_name', '');
$surrogater_phone = $this->getRequest()->getParam('surrogater_phone', '');
$email            = $this->getRequest()->getParam('email', '');
$group_product    = $this->getRequest()->getParam('group_product', 0);
$product_services = $this->getRequest()->getParam('product_services', '');
$export           = $this->getRequest()->getParam('export', 0);

$limit = LIMITATION;
$total = 0;

$params = array(
    'title'            => trim($title),
    'name'             => trim($name),
    'address'          => trim($address),
    'surrogater_name'  => trim($surrogater_name),
    'surrogater_phone' => trim($surrogater_phone),
    'email'            => trim($email),
    'group_product'    => $group_product,
    'product_services' => $product_services
);

$QTeam                      = new Application_Model_Team();
$QPurchasingType            = new Application_Model_PurchasingType();
$SModel                     = new Application_Model_PurchasingSupplier();
$suppliers                  = $SModel->fetchSupplier($page, $limit, $total, $params);
$recursiveDeparmentTeamTitle                = $QTeam->get_recursive_cache();
$department                                 = $QTeam->get_list_department();
$purchasing_type                            = $QPurchasingType->get_cache();
$this->view->suppliers                      = $suppliers;
$this->view->purchasingType                 = $purchasingType;
$this->view->recursiveDeparmentTeamTitle    = $recursiveDeparmentTeamTitle;
$this->view->department                     = $department;
$this->view->purchasing_type                = $purchasing_type;

$PCModel              = new Application_Model_PurchasingCategory();
$categorys            = $PCModel->fetchAll();
$this->view->category = $categorys;


$this->view->params = $params;
$this->view->limit  = $limit;
$this->view->total  = $total;
$this->view->url    = HOST . 'purchasing/supplier/' . ( $params ? '?' . http_build_query($params) . '&' : '?' );
$this->view->offset = $limit * ($page - 1);


$flashMessenger               = $this->_helper->flashMessenger;
$messages_success             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_success = $messages_success;
$messages                     = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages         = $messages;
$this->_helper->viewRenderer->setRender('supplier/index');


if ($export == 1) {

    // no limit time
    set_time_limit(0);
    ini_set('memory_limit', -1);
    error_reporting(~E_ALL);
    ini_set('display_error', 0);

    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads    = array(
        'NO.',
        'ID_supplier',
        'title',
        'name',
        'address',
        'website',
        'phone',
        'email',
        'surrogater_name',
        'surrogater_title',
        'surrogater_phone',
        'surrogater_email',
        'group_product',
        'product_services',
        'info_history'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;

    foreach ($heads as $key) {
        $sheet->setCellValue($alpha . $index, $key);
        $alpha++;
    }

    $index    = 2;
    $intCount = 1;

    $params['export'] = $export;
    $data             = $SModel->fetchSupplier($page, $limit, $total, $params);
    
    try {
        if ($data)
            foreach ($data as $_key => $_order) {

                $alpha = 'A';
                $sheet->setCellValue($alpha++ . $index, $intCount++);
                
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['id'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['title'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['address'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['website'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['phone'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['email'], PHPExcel_Cell_DataType::TYPE_STRING);
                
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['surrogater_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['surrogater_title'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['surrogater_phone'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['surrogater_email'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['group_product'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['product_services'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->getCell($alpha++ . $index)->setValueExplicit($_order['info_history'], PHPExcel_Cell_DataType::TYPE_STRING);
                $index++;
            }
    } catch (exception $e) {
        exit;
    }

    $filename  = 'Report_supplier_' . date('Y_m_d');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
}