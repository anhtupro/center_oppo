<?php
$flashMessenger   = $this->_helper->flashMessenger;

$id = $this->getRequest()->getParam('id');
$flag = FALSE;

if ($id) {
    $SModel				    = new Application_Model_PurchasingSupplier();
    $rowset = $SModel->find($id);
    $supplier = $rowset->current();

    if ($supplier) {
        $where = $SModel->getAdapter()->quoteInto('id = ?', $id);
        $SModel->update(array('del'=>1),$where);

        // log
        $QLog = new Application_Model_Log();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $info = "Delete_Supplier_id=".$id;

        $QLog->insert( array (
            'info' => $info,
            'user_id' => $userStorage->id,
            'ip_address' => $ip,
            'time' => date('Y-m-d H:i:s'),
        ) );

        $messages_success = $flashMessenger->setNamespace('success')->addMessage('Success');
        $flag = TRUE;
    }
}

if (!$flag)
	$messages = $flashMessenger->setNamespace('error')->addMessage('Invalid ID');

$this->_redirect(HOST.'purchasing/supplier');