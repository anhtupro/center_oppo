<?php

$QPurchasingType           = new Application_Model_PurchasingType();
$QSupplierFile             = new Application_Model_SupplierFile();
/**edit 22/07/2020*/
$QTeam                          = new Application_Model_Team();
$department                     = $QTeam->get_list_department();
$recursiveDeparmentTeamTitle    = $QTeam->get_recursive_cache();
$purchasing_type                = $QPurchasingType->get_cache();
$purchasing_test                = $QPurchasingType->getAll();
//echo '<pre>';
//var_dump($purchasing_test); exit();
$purchasingTypeByDepartment     = $QPurchasingType->getCacheTypeByDepartment();
$this->view->department         = $department;
$this->view->purchasing_type    = $purchasing_type;
$this->view->purchasing_test    = $purchasing_test;
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
/******************/
$group_product             = $QPurchasingType->getAll();
$this->view->group_product = $group_product;

$id = $this->getRequest()->getParam('id');

if ($id) { // load for editing
    $SModel   = new Application_Model_PurchasingSupplier();
    $rowset   = $SModel->find($id);
    $supplier = $rowset->current();
    $this->view->supplier = $supplier;
    
    $where_file   = array();
    $where_file[] = $QSupplierFile->getAdapter()->quoteInto('supplier_id = ?', $id);
    $list_file    = $QSupplierFile->fetchAll($where_file);
    if (!empty($list_file)) {
        $this->view->list_file = $list_file->toArray();
    }
}

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages = $messages;

$this->_helper->viewRenderer->setRender('supplier/supplier-create');
