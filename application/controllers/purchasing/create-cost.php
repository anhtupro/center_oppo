<?php

$userStorage            = Zend_Auth::getInstance()->getStorage()->read();
$user_id = $userStorage->id;

$QSupplier        = new Application_Model_Supplier();
$supplier 	 = $QSupplier->get_cache();
$this->view->supplier= $supplier;

$QTeam = new Application_Model_Team();
//$department = $QDepartment->get_cache();
$department = $QTeam->get_list_department();
$this->view->department = $department;
/////////////////////////
$sn                     = $this->getRequest()->getParam('sn');
$params = array(
    'sn'              	=> $sn,
);

$QPurchasingCost   = new Application_Model_PurchasingCost();
$QPurchasingCostDetails = new Application_Model_PurchasingCostDetails();
$QPurchasingCostFile   = new Application_Model_PurchasingCostFile();
$QPurchasingProject 	   = new Application_Model_PurchasingProject();
$QPmodelCode         = new Application_Model_PmodelCode();

$pmodel_code = $QPmodelCode->fetchAll(); 

if(!empty($sn)){    
    
    $where = $QPurchasingCost->getAdapter()->quoteInto('sn = ?', $sn);
    $purchasing_request = $QPurchasingCost->fetchRow($where);
 
    
    $where_detail = $QPurchasingCostDetails->getAdapter()->quoteInto('pc_id = ?', $purchasing_request['id']);
    $purchasing_request_details = $QPurchasingCostDetails->fetchAll($where_detail);//getData($params);
    
    $this->view->purchasing_request = $purchasing_request;
    $this->view->purchasing_request_details = $purchasing_request_details;
    
    
    $where_file = $QPurchasingCostFile->getAdapter()->quoteInto('sn = ?', $sn);
    $list_file = $QPurchasingCostFile->fetchAll($where_file);
    
    if(!empty($list_file)){        
        $this->view->list_file = $list_file->toArray();
    }
}

$this->view->params = $params;

$this->view->pmodel_code = $pmodel_code;

$this->view->purchasing_project = $QPurchasingProject->fetchAll();

$QArea = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();
$this->view->showrooms = $QArea->get_cache_showroom();

//Total: trên từng row chị Thu và chị Tâm có thể edit

if (in_array($user_id, array(
    ADMINISTRATOR_ID,
    5899,//tam.do
    4617,//minhtam.pham
    3621,// thu.nhu Chị Thu service
    24644 // hongngoc.nguyen1
    )))
{
    $this->view->access_edit = 1;
}else{
    $this->view->access_edit = 0;
}