<?php

$id = $this->getRequest()->getParam('id');
$params = array(
    'del' => 1
);
$QPurchasingRating = new Application_Model_PurchasingRating();
$result = $QPurchasingRating->save($params, $id);
$flashMessenger = $this->_helper->flashMessenger;

if ($result['code'] <= 0) {
    $flashMessenger->setNamespace('error')->addMessage($result['message']);
} else {
    $flashMessenger->setNamespace('success')->addMessage($result['message']);
}
$this->_redirect('/purchasing/list-purchasing-rating');