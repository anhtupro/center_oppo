<?php
function check($var)
{return $var !== null;}
$input              = $this->getRequest()->getParam('input');
$purchasingTypeId   = $this->getRequest()->getParam('purchasingTypeId');
$supplierList       = $this->getRequest()->getParam('supplierList');
$stage              = $this->getRequest()->getParam('stage');
$date               = $this->getRequest()->getParam('date');

// File insert
$files              = $_FILES;
$input              = json_decode($input);
$purchasingTypeId   = json_decode($purchasingTypeId);
$supplierList       = json_decode($supplierList);
$stage              = json_decode($stage);
$date               = json_decode($date);

$QPurchasingRatingDetail    = new Application_Model_PurchasingRatingDetailNew();
$QPurchasingRatingFile      = new Application_Model_PurchasingRatingFileNew();

$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
$db             = Zend_Registry::get('db');



try{
    $db->beginTransaction();
    foreach($input as $key => $object){//Loop on Row   
        foreach($object as $k => $detailDepartmentRate){
            $k  =   intVal($k);
            $detailDepartmentRate   =   (array)$detailDepartmentRate;
            if(!empty($supplierList[$key]) AND is_int($k) ){
                $jsonCriteria   =   is_array($detailDepartmentRate['ratingDetail'])  ? $detailDepartmentRate['ratingDetail']:["1","1","1","1","1","1","1","1"];
                $averagePoint   =   array_sum($jsonCriteria)/count(array_filter($jsonCriteria, "check"));

                $data=array(
                    'supplier_id'       => $supplierList[$key],
                    'purchasing_type_id'=> $purchasingTypeId,
                    'department_id'     => $detailDepartmentRate['departmentId'],
                    'json_criteria'     => json_encode($jsonCriteria),
                    'point'             => !empty($averagePoint) ? $averagePoint : 0,
                    'note'              => $detailDepartmentRate['note'],
                    'del'               => !empty($detailDepartmentRate['del']) ? $detailDepartmentRate['del'] : 0,
                    'stage'             => $stage,
                    'date'              => $date,
                    'active_rating'     => 1,
                    //'created_at'        => Date('Y-m-d H:i:s'),
                    //'created_by'        => $userStorage->id,
                    'updated_by'        => $userStorage->id,
                );
                $id =   $detailDepartmentRate['id'];

                if(!empty($id)){
                    if($detailDepartmentRate['departmentId'] != 400){
                        $resId  = $QPurchasingRatingDetail->update($data,['id = ?'=> $id]);
                    }
                    
                }elseif(!empty($data['department_id'])){
                    $data['created_at'] = Date('Y-m-d H:i:s');
                    $data['created_by'] = $userStorage->id;
                    $resId  = $QPurchasingRatingDetail->insert($data);
                }
            }
            
        }
            // Add file
            $dataFile   = array();
            $upload = new Zend_File_Transfer();
            foreach($files as $arrayFileByRow){
                // print_r ($arrayFileByRow['name'][$key]);
                foreach($arrayFileByRow['name'][$key] as $kInfoFile => $listFileInput){
                    // echo $arrayFileByRow['size'][$key][$kInfoFile];
                    $name       = $arrayFileByRow['name'][$key][$kInfoFile];
                    $tempName   = $arrayFileByRow['tmp_name'][$key][$kInfoFile];
                    $type       = $arrayFileByRow['type'][$key][$kInfoFile];
                    // echo $type;
                    $size=$arrayFileByRow['size'][$key][$kInfoFile];
                        if(!empty($tempName)){
                            print_r($tempName." ".$type." ".$size."--------");

                            $uploadDir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' .
                            DIRECTORY_SEPARATOR . 'purchasing-rating' . DIRECTORY_SEPARATOR . $stage . DIRECTORY_SEPARATOR . $supplierList[$key];

                        if (!is_dir($uploadDir))
                            @mkdir($uploadDir, 0777, true);
                        $tmpFilePath = $tempName;
                        if ($tmpFilePath != "") {
                            $old_name   = $name;
                            $tExplode   = explode('.', $name);
                            $extension  = end($tExplode);
                            $newName    = md5(uniqid('', true)) .'.'.$extension;
                            $newFilePath = $uploadDir . DIRECTORY_SEPARATOR . $newName;

                            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                                $url = 'files' .
                                    DIRECTORY_SEPARATOR . 'purchasing-rating' . DIRECTORY_SEPARATOR .  $stage  . DIRECTORY_SEPARATOR. $supplierList[$key]  . DIRECTORY_SEPARATOR . $new_name;
                                    echo $url;
                            } else {
                                $url = NULL;
                            }
                        } else {
                            $url = NULL;
                        }
                        $detail_file = [
                            'supplier_id'=> $supplierList[$key],
                            'purchasing_type_id'=> $purchasingTypeId,
                            'stage'=>$stage,
                            'url' => $url.$newName,
                            'name' => $name,
                        ];
                        $QPurchasingRatingFile->insert($detail_file);
                        // print_r($detail_file);

                        if (!empty($url)) {
                            
                            //Move file to S3
                            require_once 'Aws_s3.php';
                            $s3_lib = new Aws_s3();

                            $file_location = $url;
                            $detination_path = 'files/purchasing-rating/'.$stage.'/'.$supplierList[$key].'/';
                            $destination_name = $new_name;

                            $upload_s3 = $s3_lib->uploadS3($file_location, $detination_path, $destination_name);
                            if($upload_s3['message'] != 'ok'){
                                echo json_encode([
                                    'status' => 0,
                                    'message' => "Upload S3 không thành công",
                                ]);
                                return;
                            }
                            // END - Move file to S3
                            
                            // echo "ok";
                        }
                    }
                }
            }
    }

    $db->commit();
    echo json_encode(['status'=>1,'msg'=>'Success']);
}catch(Exception $e){

    $db->rollback();
    echo json_encode(['status'=>0,'msg'=> $e->getMessage() ]);
}
exit;
