<?php

        $export     = $this->getRequest()->getParam('export');
        $date       = $this->getRequest()->getParam('date');
        if(isset($date) && $date){
            $date =date('Y-m-d',strtotime($date));
        }
        $point = $this->getRequest()->getParam('point');
        $supplier_name      = $this->getRequest()->getParam('supplier_name');
        $department_name    = $this->getRequest()->getParam('department_name');
        $purchasing_type_id = $this->getRequest()->getParam('purchasing_type_id');
        $times = $this->getRequest()->getParam('times');
        
        $page               = $this->getRequest()->getParam('page',1);
        $sort 				= $this->getRequest()->getParam('sort','sn');
        $desc	 			= $this->getRequest()->getParam('desc',1);
        $params  = array(
            'supplier_name' 	=> $supplier_name,
            'department_name' 	=> $department_name,
            'purchasing_type_id'=> $purchasing_type_id,
            'date'         	=> $date,
            'point'             => $point,
            'export'            => $export,
            'sort'         	=> $sort,
            'desc'         	=> $desc,
            'times'             => $times
        );
        $purchasing =1;
        $total   = 0;
        $limit   = LIMITATION;
        $QPurchasingRating = new Application_Model_PurchasingRating();
        $QPurchasingType  	= new Application_Model_PurchasingType();
        $userStorage            = Zend_Auth::getInstance()->getStorage()->read();
            $QSupplier = new Application_Model_Supplier();
            $supplier = $QSupplier->getSupplierDetailForProduct();
            $QStaff=new Application_Model_Staff();
            $is_purchasing = $userStorage->department;
            
            $params['team_id']=$userStorage->team;

            $this->view->supplier = $supplier;
            $this->view->is_purchasing = $is_purchasing;
            $QPurchasingRatingDetail = new Application_Model_PurchasingRatingDetailNew();

            $res=$QPurchasingRatingDetail->fetchPagination($page, $limit, $total, $params);
            if(!empty($export)){
                $listAll=$QPurchasingRatingDetail->export($params);
                $this->exportListPurchasingRating($listAll,$export);
            }
            $this->view->res    = $res;
            $this->view->staff  = $QStaff->get_cache();
            $purchasing_type    = $QPurchasingType->getAll();
            $this->view->purchasing_type = $purchasing_type;
            $this->view->sort   = $sort;
            $this->view->desc   = $desc;
            $this->view->params = $params;
            $this->view->limit  = $limit;
            $this->view->total  = $total;
            $this->view->is_list= 1;
            $this->view->url    = HOST . 'purchasing'.DIRECTORY_SEPARATOR.'list-purchasing-rating'. ($params ? '?'.http_build_query($params).'&' : '?');
            $this->view->offset = $limit * ($page - 1);
            $this->_helper->viewRenderer->setRender('/list-purchasing-rating-new');