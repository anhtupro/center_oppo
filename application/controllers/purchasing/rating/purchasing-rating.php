<?php
$purchasing_type_id         = $this->getRequest()->getParam('purchasing_type_id');
$stage                      = $this->getRequest()->getParam('stage');
$date                       = $this->getRequest()->getParam('date');
$userStorage                = Zend_Auth::getInstance()->getStorage()->read();
$QTeam                      = new Application_Model_Team();
$QPurchasingType            = new Application_Model_PurchasingType();
$QPurchasingRateCriteria    = new Application_Model_PurchasingRateCriteria();
$QSupplier                  = new Application_Model_Supplier();
$QPurchasingRatingDetail    = new Application_Model_PurchasingRatingDetailNew();

$params['purchasing_type_id']=$purchasing_type_id;
$params['stage']=$stage;
$listCriteria   =$QPurchasingRateCriteria->fetchAll(null, 'id');
$supplier = $QSupplier->getSupplierDetailForProduct();


if(!empty($purchasing_type_id) AND !empty($stage) AND !empty($date)){
    $date       =   $QPurchasingRatingDetail->fetchRow(['purchasing_type_id = ?'=>$purchasing_type_id,'stage = ?'=>$stage,'date = ?'=>$date]);
    $date       =   $date['date'];
    $res        =   $QPurchasingRatingDetail->getData($params);
    $this->view->res = $res;
    $this->view->date = $date;
}
$user_department                = $userStorage->department;
if(!$purchasing_type_id && $user_department != '400'){
    $this->redirect(HOST . 'purchasing/list-purchasing-rating');
}
$department                     = $QTeam->get_list_department();
$this->view->department         = $department;  
$this->view->check_purchasing   = $user_department;
$get_team                       = $QTeam->getTitleDepartment($user_department);
foreach ($get_team as $key => $value) {
    $this->view->get_team = $value;
}
$recursiveDeparmentTeamTitle             = $QTeam->get_recursive_cache();
$this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
$test                       = $QPurchasingType->get_cache();
$this->view->test           = $test;
$purchasing_type = $QPurchasingType->getAll();
$this->view->purchasing_type = $purchasing_type;
$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages       = $messages;
$this->view->messages_error = $messages_error;
$this->view->listCriteria = $listCriteria->toArray();
$this->view->supplier = $supplier;
$this->view->params = $params;
$this->_helper->viewRenderer->setRender('/purchasing-rating-new');


