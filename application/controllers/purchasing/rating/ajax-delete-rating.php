<?php

$input = $this->getRequest()->getParam('input');
$input = json_decode($input);


$QPurchasingRatingDetail = new Application_Model_PurchasingRatingDetailNew();

$userStorage        = Zend_Auth::getInstance()->getStorage()->read();
$db                 = Zend_Registry::get('db');
try{
    $db->beginTransaction();
    foreach($input as $key => $value){
        $dataFilter =null;
        $dataFilter =$QPurchasingRatingDetail->getAdapter()->quoteInto([
                                                                        'stage = ?'=>$value->stage
                                                                        ,'purchasing_type_id = ?'=>$value->purchasingTypeId
                                                                        ]);
        if(!empty($dataFilter)){
            $QPurchasingRatingDetail->update( ['del'=> 1] , $dataFilter );
        }
    }
    $db->commit();
    echo json_encode(['status'=>1,'msg'=>'Success']);
}catch(Exception $e){
    echo $e;exit;
    $db->rollback();
    echo json_encode(['status'=>0,'msg'=>'Error occurred']);
}
exit;
