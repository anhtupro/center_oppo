<?php

$sn = $this->getRequest()->getParam('sn');
$page         = $this->getRequest()->getParam('page',1);
$sort 				= $this->getRequest()->getParam('sort','sn');
$desc	 				= $this->getRequest()->getParam('desc',1);
$params  = array(
    'sn' 	=> $sn,
    'sort'         	=> $sort,
    'desc'         	=> $desc
);

$total   = 0;
$limit   = LIMITATION;
$QPurchasingRating = new Application_Model_PurchasingRating();
$list = $QPurchasingRating->fetchPagination($page, $limit, $total, $params);
$this->view->list = $list;
$this->view->sort = $sort;
$this->view->desc = $desc;
$this->view->params = $params;
$this->view->limit = $limit;
$this->view->total = $total;
$this->view->sn = $sn;

$this->view->url = HOST . 'purchasing'.DIRECTORY_SEPARATOR.'list-rating-detail'. ($params ? '?'.http_build_query($params).'&' : '?');
$this->view->offset = $limit * ($page - 1);
$QPurchasingType  	= new Application_Model_PurchasingType();
$purchasing_type = $QPurchasingType->getAll();
$this->view->purchasing_type = $purchasing_type;
$flashMessenger             = $this->_helper->flashMessenger;
$messages                   = $flashMessenger->setNamespace('success')->getMessages();
$messages_error             = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages       = $messages;
$this->view->messages_error = $messages_error;
