<?php
$id = $this->getRequest()->getParam('id');
$sn = $this->getRequest()->getParam('sn');
$flashMessenger = $this->_helper->flashMessenger;
$QPurchasingRatingFile = new Application_Model_PurchasingRatingFile();
$where = $QPurchasingRatingFile->getAdapter()->quoteInto('id = ?', $id);
$url = $QPurchasingRatingFile->fetchRow($where);
$url_delete=APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR .$url['url'];
$result = $QPurchasingRatingFile->remove($id);
if ($result['code'] <= 0) {
    $flashMessenger->setNamespace('error')->addMessage($result['message']);
} else {
    $flashMessenger->setNamespace('success')->addMessage($result['message']);
    unlink($url_delete);
}
$this->_redirect('/purchasing/purchasing-rating?sn='.$sn);

