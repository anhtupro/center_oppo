<?php
$this->_helper->viewRenderer->setNoRender();
$this->_helper->layout->disableLayout();

set_time_limit(0);
ini_set('memory_limit', '5120M');



require_once 'PHPExcel.php';

$alphaExcel = new My_AlphaExcel();

$PHPExcel = new PHPExcel();
$heads = array(
    $alphaExcel->ShowAndUp() => 'STT',
    $alphaExcel->ShowAndUp() => 'Purchasing type name',
    $alphaExcel->ShowAndUp() => 'Supplier',
    $alphaExcel->ShowAndUp() => 'Department',
    $alphaExcel->ShowAndUp() => 'Point',
    $alphaExcel->ShowAndUp() => 'Result',
    $alphaExcel->ShowAndUp() => 'Times',
    $alphaExcel->ShowAndUp() => 'Date',
    $alphaExcel->ShowAndUp() => 'Created By',
);


$PHPExcel->setActiveSheetIndex(0);
$sheet = $PHPExcel->getActiveSheet();

foreach($heads as $key => $value)
{
    $sheet->setCellValue($key.'1', $value);
}

$index = 1;
foreach($data as $key => $value)
{
    $alphaExcel = new My_AlphaExcel();
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $key+1);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['purchasing_type_name']);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['supplier_name'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['department_name'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['point'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $res=null;
    if($value['point']==5){
        $res="Rất đạt";
    }elseif($value['point']>=3){
        $res="Đạt";
    }else{
        $res="Không đạt";
    }
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $res ,PHPExcel_Cell_DataType::TYPE_STRING);

    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['stage'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['date'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue($alphaExcel->ShowAndUp() . ($index+1), $value['created_name'] ,PHPExcel_Cell_DataType::TYPE_STRING);
    $index++;

}
$filename = 'Purchasing Rating - ' . date('Y-m-d H-i-s');
$objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
$objWriter->save('php://output');

exit;

