<?php

$month = $this->getRequest()->getParam('month', date('m'));
$year  = $this->getRequest()->getParam('year', date('Y'));

$QPmodelCode       = new Application_Model_PmodelCode();
$QEstimatesBalance = new Application_Model_EstimatesBalance();
$QDistributor      = new Application_Model_Distributor();
$userStorage       = Zend_Auth::getInstance()->getStorage()->read();

$params['month'] = $month;
$params['year']  = $year;

$EstimatesBalance = $QEstimatesBalance->get_list_table($params);

$list_channel = $list_content = array();
foreach ($EstimatesBalance as $key => $value) {
    $list_channel[$value['d_id']]           = $value['channel_name'];
    $list_content[$value['pmodel_code_id']] = $value['content_name'];
}

$this->view->estimates_balance = $EstimatesBalance;
$this->view->list_channel      = $list_channel;
$this->view->list_content      = $list_content;
$this->view->params            = $params;
$this->view->auth              = $userStorage;
