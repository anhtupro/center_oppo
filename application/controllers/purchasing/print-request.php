<?php

$sn                     = $this->getRequest()->getParam('sn');
$this->_helper->layout->disableLayout();
$params = array(
    'sn'              	=> $sn,
);

$flashMessenger = $this->_helper->flashMessenger;

$QPurchasingRequest 	   = new Application_Model_PurchasingRequest();
$QPurchasingProject 	   = new Application_Model_PurchasingProject();
$QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
$QPmodelCode         = new Application_Model_PmodelCode();

$purchasing_request = $QPurchasingRequest->getData($params);
$purchasing_request_details = $QPurchasingRequestDetails->getData($params);
if(empty($purchasing_request) || empty($purchasing_request)){
	$flashMessenger->setNamespace('error')->addMessage('Purchasing request not exits!');
	$this->redirect(HOST.'purchasing/purchasing-request');
}

$this->view->purchasing_request = $purchasing_request;
$this->view->purchasing_request_details = $purchasing_request_details;
$this->view->purchasing_project = $QPurchasingProject->fetchAll();


