<?php


$page         = $this->getRequest()->getParam('page',1);
$sort 				= $this->getRequest()->getParam('sort','sn');
$desc	 				= $this->getRequest()->getParam('desc',1);
$params  = array(
    'sort'         	=> $sort,
    'desc'         	=> $desc
);

$total   = 0;
$limit   = LIMITATION;
$QPurchasingProject=new Application_Model_PurchasingProject();





$listProject= $QPurchasingProject->fetchPagination($page, $limit, $total, $params);

$this->view->listProject=$listProject;


$this->view->total = $total;
$this->view->limit = $limit;
$this->view->offset = $limit * ($page - 1);
$this->view->url = HOST . 'purchasing'.DIRECTORY_SEPARATOR.'project-managing'. ($params ? '?'.http_build_query($params).'&' : '?');

