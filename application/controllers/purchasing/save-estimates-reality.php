<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

$QEstimatesBalance = new Application_Model_EstimatesBalance();
$QEstimatesDetail  = new Application_Model_EstimatesDetail();
$QEstimatesReality = new Application_Model_EstimatesReality();


$estimates_detail_id = $this->getRequest()->getParam('estimates_detail_id');
$amount_reality      = $this->getRequest()->getParam('amount_reality', 0);
$invoice_number      = $this->getRequest()->getParam('invoice_number');
$invoice_date        = $this->getRequest()->getParam('invoice_date');
$contract            = $this->getRequest()->getParam('contract');
$note                = $this->getRequest()->getParam('note');
$est_reality_id_old  = $this->getRequest()->getParam('est_reality_id_old'); // dùng để edit

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {


    if (empty($amount_reality)) {
        echo '<script>window.parent.document.getElementById("iframe2").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe2").height = \'60px\';</script>';
        echo '<div class="alert alert-error">Lỗi :Nhập Thông tin chưa đầy đủ. Vui lòng kiểm tra lại.</div>';
        die();
    }

    $where           = $QEstimatesDetail->getAdapter()->quoteInto('id = ?', $estimates_detail_id);
    $EstimatesDetail = $QEstimatesDetail->fetchRow($where);

    if (empty($EstimatesDetail)) {
        echo '<script>window.parent.document.getElementById("iframe2").style.display = \'block\';</script>';
        echo '<script>window.parent.document.getElementById("iframe2").height = \'60px\';</script>';
        echo '<div class="alert alert-error">Lỗi : estimates_detail_id Không tồn tại trong bảng estimates_detail.</div>';
        die();
    }

    $estimates_balance_id = $EstimatesDetail['balance_id'];


    /// xóa những dòng bị bỏ
    $where_del   = array();
    $where_del[] = $QEstimatesReality->getAdapter()->quoteInto('estimates_detail_id = ?', $estimates_detail_id);
    $where_del[] = $QEstimatesReality->getAdapter()->quoteInto('id not in (?)', $est_reality_id_old);
    $QEstimatesReality->delete($where_del);

    $array_reality_id_full = array(); // gom tất cả ID của EstimatesReality lại để cập nhật hình ảnh 

    foreach ($amount_reality as $k => $value) {

        if (!empty($est_reality_id_old[$k])) {// có id cũ thì update
            $data_row                  = array(
                'estimates_detail_id' => $estimates_detail_id,
                'balance_id'          => $estimates_balance_id,
                'year'                => $EstimatesDetail['year'],
                'month'               => $EstimatesDetail['month'],
                'd_id'                => $EstimatesDetail['d_id'],
                'pmodel_code_id'      => $EstimatesDetail['pmodel_code_id'],
                'amount_reality'      => My_Number::floatval($amount_reality[$k]),
                'invoice_number'      => $invoice_number[$k],
                'invoice_date'        => $invoice_date[$k],
                'contract'            => $contract[$k],
                'note'                => $note[$k],
                'updated_by'          => $userStorage->id,
                'updated_at'          => date('Y-m-d H:i:s')
            );
            $where_upd                 = $QEstimatesReality->getAdapter()->quoteInto('id = ?', $est_reality_id_old[$k]);
            $QEstimatesReality->update($data_row, $where_upd);
            $array_reality_id_full[$k] = $est_reality_id_old[$k];
        } else {// insert            
            $data_row                  = array(
                'estimates_detail_id' => $estimates_detail_id,
                'balance_id'          => $estimates_balance_id,
                'year'                => $EstimatesDetail['year'],
                'month'               => $EstimatesDetail['month'],
                'd_id'                => $EstimatesDetail['d_id'],
                'pmodel_code_id'      => $EstimatesDetail['pmodel_code_id'],
                'amount_reality'      => My_Number::floatval($amount_reality[$k]),
                'invoice_number'      => $invoice_number[$k],
                'invoice_date'        => $invoice_date[$k],
                'contract'            => $contract[$k],
                'note'                => $note[$k],
                'created_by'          => $userStorage->id,
                'created_at'          => date('Y-m-d H:i:s')
            );
            $id_row                    = $QEstimatesReality->insert($data_row);
            $array_reality_id_full[$k] = $id_row;
        }
    }//End foreach
    // cập nhật lại tổng chi phí dự trù cho bảng EstimatesBalance
    $db                = Zend_Registry::get('db');
    $select            = $db->select()
            ->from(array('ed' => 'estimates_reality'), array('amount_reality' => new Zend_Db_Expr('SUM(IFNULL(ed.amount_reality,0))')))
            ->where($db->quoteInto('ed.balance_id = ?', $estimates_balance_id));
    $balance_estimates = $db->fetchOne($select);
    $where_up          = $QEstimatesBalance->getAdapter()->quoteInto('id = ?', $estimates_balance_id);
    $QEstimatesBalance->update(array('amount_reality' => $balance_estimates), $where_up);


    //UPLOAD FILE
    $folder_name = $EstimatesDetail['year'] . "_" . $EstimatesDetail['month'];

    foreach ($_FILES['file']['size'] as $k2 => $val) {

        if ($_FILES['file']['size'][$k2] != 0) {

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'estimates' .
                    DIRECTORY_SEPARATOR . $folder_name;


            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile' => true));

            $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
            $upload->addValidator('Size', false, array('max' => '4MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');

            $name_file_k = "file_" . $k2 . "_";
            $files       = $upload->getFileInfo($name_file_k);
            $hasPhoto    = false;
            $data_file   = array();


            if (isset($files[$name_file_k]) and $files[$name_file_k]) {
                $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);
                $upload->setDestination($uploaded_dir);

                //Rename
                $old_name  = $fileInfo['name'];
                $tExplode  = explode('.', $old_name);
                $extension = end($tExplode); // đuôi file 
                $new_name  = 'UPLOAD-' . date('Ymd') . substr(microtime(), 2, 4) . md5(uniqid('', true)) . '.' . $extension;
                $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));

                $r = $upload->receive(array($name_file_k));
                if ($r) {
                    $data_file['file_name'] = $new_name;
                } else {
                    $messages = $upload->getMessages();
                    foreach ($messages as $msg) {
                        echo '<script>window.parent.document.getElementById("iframe2").style.display = \'block\';</script>';
                        echo '<script>window.parent.document.getElementById("iframe2").height = \'60px\';</script>';
                        echo '<div class="alert alert-error">Lỗi : ' . $msg . '</div>';
                        exit;
                    }
                }

                $where_file = $QEstimatesReality->getAdapter()->quoteInto('id = ?', $array_reality_id_full[$k2]);
                $QEstimatesReality->update(array('file' => $new_name), $where_file);
            }
        }
    }

    $db->commit();
    echo '<script>window.parent.document.getElementById("iframe2").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe2").height = \'60px\';</script>';
    echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
    echo '<div class="alert alert-success">Done</div>';
    /// load lại trang
    $back_url = "estimates?month=" . $EstimatesDetail['month'] . "&year=" . $EstimatesDetail['year'];
    echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
} catch (Exception $e) {
    $db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe2").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe2").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : ' . $e->getMessage() . '</div>';
}
exit;
