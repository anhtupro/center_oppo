<?php

$flashMessenger = $this->_helper->flashMessenger;

$db = Zend_Registry::get('db');
$db->beginTransaction();

$back_url = HOST.'purchasing/cost';

try
{

	$QPurchasingCost           = new Application_Model_PurchasingCost();
	$QPurchasingCostDetails    = new Application_Model_PurchasingCostDetails();
        
	$userStorage                = Zend_Auth::getInstance()->getStorage()->read();

	$sn 	= $this->getRequest()->getParam('sn');

	$where = $QPurchasingCost->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_Cost = $QPurchasingCost->fetchRow($where);

	if(!$purchasing_Cost){
	    $flashMessenger->setNamespace('error')->addMessage('SN không tồn tại!');
            $this->redirect($back_url);
	}

	$where_details = $QPurchasingCostDetails->getAdapter()->quoteInto('pc_id = ?', $purchasing_Cost['id']);
        
	$QPurchasingCostDetails->delete($where_details);
	$QPurchasingCost->delete($where);

	$db->commit();

	$flashMessenger->setNamespace('success')->addMessage('Done!');
        $this->redirect($back_url);
}
catch (Exception $e)
{
	$db->rollBack();
	$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        $this->redirect($back_url);
}
exit;