<?php
$sort                   = $this->getRequest()->getParam('sort', '');
$desc                   = $this->getRequest()->getParam('desc', 1);
$sn                     = $this->getRequest()->getParam('sn');
$code                   = $this->getRequest()->getParam('code');
$created_at_from        = $this->getRequest()->getParam('created_at_from');
$created_at_to          = $this->getRequest()->getParam('created_at_to');
$supplier_id            = $this->getRequest()->getParam('supplier_id');
$area_id            = $this->getRequest()->getParam('area_id');
$department_id          = $this->getRequest()->getParam('department_id');
$userStorage            = Zend_Auth::getInstance()->getStorage()->read();

$QTeam = new Application_Model_Team();
//$department = $QDepartment->get_cache();
$department = $QTeam->get_list_department();
$this->view->department = $department;
 

$params = array(
    'sn'              	=> $sn,
    'code'              => $code,
    'created_at_from'   => $created_at_from,
    'created_at_to'     => $created_at_to,
    'supplier_id'	=> $supplier_id,
    'area_id'           => $area_id,
    'department_id'     => $department_id
);

$params['sort'] = $sort;
$params['desc'] = $desc;


$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
$QSupplier        = new Application_Model_Supplier();
$supplier 	 = $QSupplier->get_cache();
$QArea = new Application_Model_Area();
$this->view->areas = $QArea->get_cache();

$page           = $this->getRequest()->getParam('page', 1);
$limit          = 10000;
$total          = 0;
$result         = $QPurchasingRequestDetails->getDataPo($params);


$this->view->supplier= $supplier;
$this->view->list    = $result;
$this->view->limit   = $limit;
$this->view->total   = $total;
$this->view->params  = $params;
$this->view->desc    = $desc;
$this->view->sort    = $sort;
$this->view->url     = HOST.'purchasing/purchasing-request'.( $params ? '?'.http_build_query($params).'&' : '?' );
$this->view->offset  = $limit*($page-1);


$QTrainerTypeAsset          = new Application_Model_TrainerTypeAsset();
$whereTypeAsset             = $QTrainerTypeAsset->getAdapter()->quoteInto('del = ? OR del IS NULL',0);
$type_asset                 = $QTrainerTypeAsset->fetchAll($whereTypeAsset);
$arrayTypeAsset             = array();
if($type_asset->count())
{
    foreach($type_asset as $key => $value)
    {
        $arrayTypeAsset[$value['id']] = $value['name'];
    }
}

$this->view->type_asset     = $arrayTypeAsset;

$flashMessenger       = $this->_helper->flashMessenger;
$messages             = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages = $messages;
$messages_error       = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_error = $messages_error;


