<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';

$QPurchasingRequest         = new Application_Model_PurchasingRequest();
$QPurchasingRequestFile     = new Application_Model_PurchasingRequestFile();
$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
$QPurchasingRequestSupplier = new Application_Model_PurchasingRequestSupplier();
$userStorage                = Zend_Auth::getInstance()->getStorage()->read();


$sn          = $this->getRequest()->getParam('sn');
$prs_id      = $this->getRequest()->getParam('prs_id', array());
$scores      = $this->getRequest()->getParam('scores', array());
$review_text = $this->getRequest()->getParam('review_text', array());


if (empty($sn) || empty($prs_id)) {
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">ERROR: DATA INPUT.!.</div>';
    die();
}

$db = Zend_Registry::get('db');
$db->beginTransaction();

try {

    foreach ($prs_id as $key => $val_id) {
        if (!empty($val_id)) {

            $where                         = $QPurchasingRequestSupplier->getAdapter()->quoteInto('id = ?', $val_id);
            $row_PurchasingRequestSupplier = $QPurchasingRequestSupplier->fetchRow($where);

            if (empty($row_PurchasingRequestSupplier)) {
                throw new Exception(" ERROR :  prs_id find not in PurchasingRequestSupplier !!");
            }
            
            $purchasing_request_supplier = array(
                'review_scores' => $scores[$val_id],
                'review_text'   => $review_text[$val_id],
                'review_by'     => $userStorage->id,
                'review_at'     => date('Y-m-d H:i:s')
            );
            $QPurchasingRequestSupplier->update($purchasing_request_supplier, $where);
 
            //UPLOAD FILE DETAIL
            $file_name_upload = "file_supplier_review_" . $val_id;
            foreach ($_FILES[$file_name_upload]['size'] as $k3 => $val3) {

                if ($_FILES[$file_name_upload]['size'][$k3] != 0) {

                    $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                            DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-request' .
                            DIRECTORY_SEPARATOR . $sn;


                    $upload = new Zend_File_Transfer();
                    $upload->setOptions(array('ignoreNoFile' => true));

                    $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
                    $upload->addValidator('Size', false, array('max' => '4MB'));
                    $upload->addValidator('ExcludeExtension', false, 'php,sh');

                    $name_file_k = $file_name_upload . "_" . $k3 . "_";
                    $files       = $upload->getFileInfo($name_file_k);
                    $hasPhoto    = false;
                    $data_file   = array();

                    if (isset($files[$name_file_k]) and $files[$name_file_k]) {
                        $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                        if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                        $upload->setDestination($uploaded_dir);

                        //Rename
                        $old_name = $fileInfo['name'];

                        $tExplode  = explode('.', $old_name);
                        $extension = end($tExplode); // đuôi file
                        //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k3.'_'. $old_name;
                        $new_name  = 'UPLOAD-' . date('Ymd') . substr(microtime(), 2, 4) . md5(uniqid('', true)) . '.' . $extension;
                        $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                        $r         = $upload->receive(array($name_file_k));

                        if ($r) {
                            $data_file['file_name'] = $new_name;
                        }
                        else {
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg) {
                                throw new Exception(" ERROR: " . $msg . " !!");
                            }
                        }
                        //$purchasing_request['pr_quotation'] = $new_name;
                        $data_file_upload = array(
                            'sn'             => $sn,
                            'prs_id'          => $val_id,
                            'pr_quotation'   => $new_name);

                        $QPurchasingRequestFile->insert($data_file_upload);
                    }
                }
            }
            //END UPLOAD DETAIL
 
        }
        else {
            throw new Exception(" ERROR : Data value prs_id !!");
        }
    }//EndForeach
    $db->commit();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
    echo '<div class="alert alert-success">Done</div>';

    /// load lại trang
    $back_url = "purchasing-review-supplier?sn=" . $sn;
    echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
} catch (Exception $e) {
    $db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : ' . $e->getMessage() . '</div>';
}
exit;
