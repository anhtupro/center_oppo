<?php

$flashMessenger = $this->_helper->flashMessenger;

$db = Zend_Registry::get('db');
$db->beginTransaction();

$back_url = HOST.'purchasing/purchasing-request';

try
{

	$QPurchasingRequest         = new Application_Model_PurchasingRequest();
	$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
        $QAppNoti = new Application_Model_AppNoti();
	$userStorage                = Zend_Auth::getInstance()->getStorage()->read();
        
        $sn = $this->getRequest()->getParam('sn');
        $actual_link = $this->getRequest()->getParam('actual_link');
        
        $back_url = $actual_link;
        
        if($userStorage->department != 400 AND $userStorage->id!=7){
            //$flashMessenger->setNamespace('error')->addMessage('Lỗi: Team Purchasing mới Confirm được..!');
            //$this->redirect($back_url);
        }

	$where = $QPurchasingRequest->getAdapter()->quoteInto('sn = ?',$sn);
	$purchasing_request = $QPurchasingRequest->fetchRow($where);

	if(!$purchasing_request){
	    $flashMessenger->setNamespace('error')->addMessage('SN không tồn tại!');
            $this->redirect($back_url);
        }elseif($purchasing_request['status']!=1){            
	    $flashMessenger->setNamespace('error')->addMessage('Lỗi: Đơn ở trạng thái NEW mới Confirm được..!');
            $this->redirect($back_url);
        }
        
        //// check hien.tran appr <= 500 triệu
        $details                    = $QPurchasingRequestDetails->getDetails($sn);

        $total_price_sn = 0;
        if(!empty($details))
        foreach($details as $k=>$v){
            $money_ck = $price_after_ck = $total_price = 0;
            $money_ck = $v['price']*$v['ck']/100;
            $price_after_ck = $v['price'] - $money_ck;
            $total_price = $v['quantity']*$price_after_ck+$v['fee'];
            $total_price_sn = $total_price_sn + $total_price;
        }
        
        if(($total_price_sn > 3000000000)&&($userStorage->id!=7)){
           $data_noti = [
                'title' => "OPPO VIETNAM",
                'message' => "Có một đề xuất Purchasing đang chờ bạn duyệt.",
                'link' => "/purchasing/view-request-mobile?sn=".$sn,
                'staff_id' => 7,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $push_noti = $QAppNoti->sendNotificationNoTransactionERP($data_noti);          
        }
        else{
            $data_noti = [
                'title' => "OPPO VIETNAM",
                'message' => "Có một đề xuất Purchasing đang chờ bạn duyệt.",
                'link' => "/purchasing/view-request-mobile?sn=".$sn,
                'staff_id' => 4,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $push_noti = $QAppNoti->sendNotificationNoTransactionERP($data_noti);
        }
        //// END check hien.tran appr <= 500 triệu

	$data = array(
            'status'      => 2,
            'confirm_at'  => date('Y-m-d H:i:s'),
            'confirm_by'  => $userStorage->id,
	);
	$QPurchasingRequest->update($data, $where);

	$db->commit();

    $flashMessenger->setNamespace('success')->addMessage('Done!');
    $this->redirect($back_url);
}
catch (Exception $e)
{
	$db->rollBack();
	$flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->redirect($back_url);
}
exit;