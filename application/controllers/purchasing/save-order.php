<?php

$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="'.HOST.'css/bootstrap.min.css">';

$db = Zend_Registry::get('db');
$db->beginTransaction();


try
{

	$QPurchasingOrder  			= new Application_Model_PurchasingOrder();
	$QPurchasingOrderDetails 	= new Application_Model_PurchasingOrderDetails();
	$QCodeSupplier 				= new Application_Model_CodeSupplier();
	$QLogCodeSupplier 			= new Application_Model_LogCodeSupplier();
	$QPurchasingRequest  		= new Application_Model_PurchasingRequest();
	$QPurchasingRequestDetails  = new Application_Model_PurchasingRequestDetails();
        $QPurchasingOrderFile    = new Application_Model_PurchasingOrderFile();
	$userStorage    	 		= Zend_Auth::getInstance()->getStorage()->read();


	$po_name 			= $this->getRequest()->getParam('po_name');
	$supplier_id 		= $this->getRequest()->getParam('supplier_id');
	$delivery_date 		= $this->getRequest()->getParam('delivery_date');
	$remark  			= $this->getRequest()->getParam('remark');
	$code_supplier_id   = $this->getRequest()->getParam('code_supplier_id', array());
	$quantity   		= $this->getRequest()->getParam('quantity', array());
	$price   			= $this->getRequest()->getParam('price', array());
	$price_default   	= $this->getRequest()->getParam('price_default', array());
	$pmodel_code_id   	= $this->getRequest()->getParam('pmodel_code_id', array());
	$pr_id   			= $this->getRequest()->getParam('pr_id', array());
	$check   			= $this->getRequest()->getParam('check', array());

	$sn = date ( 'YmdHis' ) . substr ( microtime (), 2, 4 );

	if(!$supplier_id){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : Vui lòng chọn Supplier</div>';
		exit;
	}

	if(!$check){
	    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
		echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
		echo '<div class="alert alert-error">Lỗi : Không có Request nào được chọn</div>';
		exit;
	}

	$po_order = array(
		'sn' 			=> $sn,
		'po_name'		=> $po_name,
		'delivery_date'	=> $delivery_date,
		'remark'		=> $remark,
		'status' 		=> 'New',
		'created_by' 	=> $userStorage->id,
		'supplier_id'	=> $supplier_id,
		'created_at'	=> date('Y-m-d H:i:s')
	);

	//UPLOAD HÌNH ẢNH
        foreach ($_FILES['po_quotation']['size'] as $k => $val) {

            if ($_FILES['po_quotation']['size'][$k] != 0) {
                
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'purchasing-order' .
                DIRECTORY_SEPARATOR . $sn;


                $upload = new Zend_File_Transfer();
                $upload->setOptions(array('ignoreNoFile'=>true));

                //check function
//                if (function_exists('finfo_file')){
//                    $upload->addValidator('MimeType', false, array('application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'));
//                }

                $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx');
                $upload->addValidator('Size', false, array('max' => '4MB'));
                $upload->addValidator('ExcludeExtension', false, 'php,sh');
                
                $name_file_k = "po_quotation_".$k."_";
                $files = $upload->getFileInfo($name_file_k);
                $hasPhoto = false;
                $data_file = array();
               
                
                if(isset($files[$name_file_k]) and $files[$name_file_k]){
                    $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                        if (!is_dir($uploaded_dir))
                            @mkdir($uploaded_dir, 0777, true);
                        $upload->setDestination($uploaded_dir);

                        //Rename
                        $old_name = $fileInfo['name'];                     
                        
                        $tExplode = explode('.', $old_name);
                        $extension = end($tExplode);// đuôi file
                        //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k.'_'. $old_name;
                        $new_name = 'UPLOAD-' .date('Ymd').substr ( microtime (), 2, 4 ) . md5(uniqid('', true)) . '.' . $extension;
                        $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                        $r = $upload->receive(array($name_file_k));
                        if($r){
                            $data_file['file_name'] = $new_name;
                        }
                        else{
                            $messages = $upload->getMessages();
                            foreach ($messages as $msg){
                                echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
                                            echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
                                            echo '<div class="alert alert-error">Lỗi : '.$msg.'</div>';	
                                            exit;
                            }
                        }
                        //$purchasing_request['po_quotation'] = $new_name;
                        $QPurchasingOrderFile->insert(array('sn'=>$sn, 'po_quotation'=>$new_name));
                }
            }
        }
        
    //END UPLOAD HÌNH ẢNH

	$id = $QPurchasingOrder->insert($po_order);

	foreach($pr_id as $key=>$value){
		if(in_array($value, $check)){
			$po_order_details = array(
				'po_id' 				=> $id,
				'pr_details_id'			=> $value,
				'code_supplier_id'		=> $code_supplier_id[$key],
				'quantity'				=> $quantity[$key],
				'price'					=> $price[$key],
				'pmodel_code_id'		=> $pmodel_code_id[$key]
			);
			$QPurchasingOrderDetails->insert($po_order_details);

			//Update số lượng details
			$where 				= $QPurchasingRequestDetails->getAdapter()->quoteInto('id = ?',$value);
			$pr_details 		= $QPurchasingRequestDetails->fetchRow($where);
			$quantity_stock 	= ($pr_details['quantity'] - $pr_details['po_quantity']);
			if($quantity[$key] > $quantity_stock){
				$db->rollBack();
                                
			$where_info	= $QPurchasingRequest->getAdapter()->quoteInto('id = ?',$pr_details['purchasing_request_id']);
			$QPR_info 	= $QPurchasingRequest->fetchRow($where_info);
			    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
				echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
				echo '<div class="alert alert-error">Lỗi : Số lượng điều chỉnh lớn hơn số lượng yêu cầu, PR_SN='.$QPR_info['sn'].' , Quantity_PR = '.$quantity_stock.' </div>';
				exit;
			}
			else{
				$data_pr = array(
					'po_quantity' => $pr_details['po_quantity'] + $quantity[$key]
				);
				$QPurchasingRequestDetails->update($data_pr,$where);
			}
			//END update số lượng details


			//Nếu đổi giá default đổi thì ghi log lại
			if($price[$key] != $price_default[$key]){
				$now = date('Y-m-d H:i:s');
				$where_cs 	= $QCodeSupplier->getAdapter()->quoteInto('id = ?', $code_supplier_id[$key]);
				$QCodeSupplier->update(array('price' => $price[$key]), $where_cs);

				$where_log = array();
				$where_log[] = $QLogCodeSupplier->getAdapter()->quoteInto('code_supplier_id = ?', $code_supplier_id[$key]);
				$where_log[] = $QLogCodeSupplier->getAdapter()->quoteInto('to_date IS NULL', NULL);
				$QLogCodeSupplier->update(array('to_date' => $now), $where_log);

				$data_log = array(
					'code_supplier_id' => $code_supplier_id[$key],
					'price'			   => $price[$key],
					'from_date'		   => $now
				);
				$QLogCodeSupplier->insert($data_log);
			}
			//END 


		}
	}

	$db->commit();
	echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
	echo '<div class="alert alert-success">Done</div>';        
                /// load lại trang
        $back_url =  "edit-order?sn=".$sn;
        echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
                
}
catch (Exception $e)
{
	$db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
	echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
	echo '<div class="alert alert-error">Lỗi : '.$e->getMessage().'</div>';	
}
exit;