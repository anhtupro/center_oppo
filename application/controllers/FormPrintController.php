<?php
class FormPrintController extends My_Controller_Action{

  public function massPreOrderAction(){
      $export        = $this->getRequest()->getParam('export');
      $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
      $this->view->staff_id = $userStorage->id;
      $staff_id = $userStorage->id;
      if(!empty($export)){
            $db = Zend_Registry::get('db');
           if(in_array($userStorage->id, array(5899, 24)) ){
               $sql = "SELECT m.*, ik.activation_date, ik.timing_date  FROM mass_imei_pre_order m
LEFT JOIN imei_kpi ik on ik.imei_sn = m.imei
 WHERE 1 ORDER BY m.created_by";

           }else{
                              $sql = "SELECT m.*, ik.activation_date, ik.timing_date  FROM mass_imei_pre_order m
LEFT JOIN imei_kpi ik on ik.imei_sn = m.imei
 WHERE m.created_by = $staff_id ORDER BY m.created_by";
           }
                        $stmt = $db->prepare($sql);
            $stmt->execute();
            $list = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = null;
            ini_set("memory_limit", -1);
            ini_set("display_error", 0);
            error_reporting(~E_ALL);

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $heads = array(
                'STT',
                'Store Name',
                'Store Address',
                'Area',
                'Store ID',
                'Dealer name',            
                'Họ tên KH',            
                'SĐT',            
                'Model',            
                'Màu sắc',            
                'Ngày đặt hàng',            
                'Ngày nhận máy',            
                'IMEI',           
                'Ngày bán',           
                'Ngày kích hoạt',           
            );
            $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;
        $i = 1;
        
        foreach($list as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['store_address']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_name']);
            $sheet->setCellValue($alpha++.$index, $item['hoten']);
            $sheet->setCellValue($alpha++.$index, $item['phone']);
            $sheet->setCellValue($alpha++.$index, $item['model']);
            $sheet->setCellValue($alpha++.$index, $item['color']);
            $sheet->setCellValue($alpha++.$index, $item['ngay_dat']);
            $sheet->setCellValue($alpha++.$index, $item['ngay_nhan']);
            $sheet->setCellValue($alpha++.$index, $item['imei']);
            $sheet->setCellValue($alpha++.$index, $item['timing_date']);
            $sheet->setCellValue($alpha++.$index, $item['activation_date']);
            $index++;
        }
        $filename = 'Total_'.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
      }
  }	  
  public function saveMassPreOrderAction(){

    // config for template
    define('START_ROW', 2);
    define('store_name', 0);
    define('store_address', 1);
    define('area', 2);
    define('store_id', 3);
    define('dealer_name', 4);
    define('hoten', 5);
    define('phone', 6);
    define('model', 7);
    define('color', 8);
    define('ngay_dat', 9);
    define('ngay_nhan', 10);
    define('imei', 11);
    
    $datetime = date('Y-m-d H:i:s');
   
    $userStorage   = Zend_Auth::getInstance()->getStorage()->read();
    $QInsuranceLogInfo    = new Application_Model_InsuranceLogInfo();
    $this->_helper->layout->disableLayout();

    $QStaff         = new Application_Model_Staff();
    if ($this->getRequest()->getMethod() == 'POST') { // Big IF
  
    set_time_limit(0);
    ini_set('memory_limit', -1);
    // file_put_contents(APPLICATION_PATH.'/../public/files/mou/lock', '1');
    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder   = 'mou';
    $new_file_path = '';
    $requirement   = array(
        'Size'      => array('min' => 5, 'max' => 5000000),
        'Count'     => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );
    
    try {

        $file = My_File::get($save_folder, $requirement, true);

        if (!$file || !count($file))
            throw new Exception("Upload failed");

            $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
            . DIRECTORY_SEPARATOR . $file['folder'];

            $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];

    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }
    //read file
    include 'PHPExcel/IOFactory.php';
    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel   = $objReader->load($inputFileName);
    } catch (Exception $e) {    

        $this->view->errors = $e->getMessage();
        return;
    }

    //  Get worksheet dimensions
    $sheet           = $objPHPExcel->getSheet(0);
    $highestRow      = $sheet->getHighestRow();
    $highestColumn   = $sheet->getHighestColumn();
  
    $error_list      = array();
    $success_list    = array();
    $store_code_list = array();
    $number_of_order = 0;
    $total_value     = 0;
    $total_order_row = 0;
    $order_list      = array();

    $arr_inserts   = array();
    $arr_order_ids = array();

    $_failed         = false;
    $total_row       = array();
    $data_staff_code = [];
    $dataMassUpload  = array();
    $data_staff_code = array();
    $data_imei = [];
    
    for ($row = START_ROW; $row <= $highestRow; $row++) {

        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
                $data_imei[] = trim($rowData[imei]);
                
                if(!empty(trim($rowData[imei]))){
                    
                    $date_dat = PHPExcel_Shared_Date::ExcelToPHP($rowData[ngay_dat]);
                    $date_nhan = PHPExcel_Shared_Date::ExcelToPHP($rowData[ngay_nhan]);
                    $data = array(
                    'store_name'            => trim($rowData[store_name]),
                    'store_address'         => trim($rowData[store_address]),
                    'area'                  => trim($rowData[area]),
                    'store_id'              => trim($rowData[store_id]),
                    'dealer_name'           => trim($rowData[dealer_name]),
                    'hoten'                 => trim($rowData[hoten]),
                    'phone'                 => trim($rowData[phone]),
                    'model'                 => trim($rowData[model]),
                    'color'                 => trim($rowData[color]),
                    'ngay_dat'              => date('d/m/Y', $date_dat),
                    'ngay_nhan'             => date('d/m/Y', $date_nhan),
                    'imei'                  => My_String::trim($rowData[imei]),
                    'created_at'            => $datetime,
                    'created_by'            => $userStorage->id
                    );
                    $dataMassUpload[] = $data;
                }
                
         
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }
        // nothing here
    } // END loop through order rows
    
    $progress->flush(30);
    $db = Zend_Registry::get('db');
//    $db->beginTransaction();
    
    try {
        if (!empty($dataMassUpload)) {
          
//            echo "<pre>";
//            print_r($dataMassUpload);
//            echo "</pre>";
            My_Controller_Action::insertAllrow($dataMassUpload, 'mass_imei_pre_order');
            $select = $db->select()
                        ->from(array('r' => DATABASE_CENTER.'.mass_imei_pre_order'), array('r.imei'))
                        ->joinLeft(array('p'=> WAREHOUSE_DB.'.imei'),'p.imei_sn = r.imei',array())
                        ->where('r.created_by = ?', $userStorage->id)
                        ->where('p.imei_sn  IS NULL', null)
                    ;
            $result = $db->fetchAll($select);
            $flag = 0;
            if(!empty($result)){
                $flag = 1;
                $total_err = count($result);
                echo '<h3 style="background-color:#ffc107!important; color: white">Không thành công có '.$total_err.' imei không hợp lệ</h3>';
                $listImei = '';
                foreach ($result as $value){
                    // Các dòng lệnh
                    $listImei .= $value['imei']. '&#xD;';
                }
                echo '<textarea rows="6" name="note" id="note"> '.$listImei.' </textarea>';
                $created_by  = $userStorage->id;
                $sql = "DELETE FROM mass_imei_pre_order WHERE created_by = $created_by";
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            
            
        } else {
            throw new Exception("Danh sách nhân viên không hợp lệ");
        }
        
//        $db->commit();
        $progress->flush(99);
        $progress->flush(100);
        if($flag == 0){
            echo '<h3 style="background-color:#28a745 !important; color: white">Thành công</h3>';
        }
        
    } catch (Exception $e) {
//        $db->rollback();
        $this->view->errors = $e->getMessage();
        return;
    }
}

  }	
  
  function createAction(){
      $flashMessenger   = $this->_helper->flashMessenger;
      $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
      $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
      $this->view->messages_error   = $flashMessenger->setNamespace('error')->getMessages();

      $QStaff           = new Application_Model_Staff();
      $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id         = $userStorage->id;
      $current_user     = $QStaff->findStaffid($staff_id);

      $QOffice          = new Application_Model_Office();
      $offices          = $QOffice->get_all();
      $this->view->offices           = $offices;

      $QTeam    = new Application_Model_Team();
	  $team = $current_user['team'];
	  $title = $current_user['title'];
	  $infoTeam  = $QTeam->find($title)->current();
      $team_name = $infoTeam['name'];
      $team_select = 'department';

      // check if in special team
       // check if in special team
     if($team_name == 'ASSISTANT' && in_array($team, array(75))){
		 
           $team_select = 'title';
      }elseif( in_array($team, array(131, 133 ,147, 406 )) ){
            $team_select = 'team';
      }
      $this->view->depart = $QTeam->fetchRow( $QTeam->getAdapter()->quoteInto('id = ?',  $current_user[$team_select]) )->toArray()['name'];
      $this->view->user = $current_user;
  }

  function saveAction(){
      $QFormPrint = new Application_Model_FormPrint();
      $flashMessenger = $this->_helper->flashMessenger;

      $userStorage = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id    = $userStorage->id;

      if($this->getRequest()->getMethod() == "POST"){

          $save_mode          = $this->getRequest()->getParam('save_mode');

          $mail_code          = $this->getRequest()->getParam('mail_code');
          $guarantee_delivery = $this->getRequest()->getParam('guarantee_delivery',0);
          $fast_delivery      = $this->getRequest()->getParam('fast_delivery',0);
          $time_delivery      = $this->getRequest()->getParam('time_delivery',0);
          $receiver_pay       = $this->getRequest()->getParam('receiver_pay',0);
          $sender_name        = $this->getRequest()->getParam('sender_name');
          $sender_phone       = $this->getRequest()->getParam('sender_phone');
          $sender_department  = $this->getRequest()->getParam('sender_department');
          $receiver_name      = $this->getRequest()->getParam('receiver_name');
          $receiver_address   = $this->getRequest()->getParam('receiver_address');
          $receiver_phone     = $this->getRequest()->getParam('receiver_phone');
          $content            = $this->getRequest()->getParam('content');


          $data_arr = array(
            'mail_code'         => $mail_code,
            'content'           => $content,
            'sender_name'       => $sender_name,
            'sender_phone'      => $sender_phone,
            'sender_department' => $sender_department,
            'receiver_name'     => $receiver_name,
            'receiver_address'  => $receiver_address,
            'receiver_phone'    => $receiver_phone,
            'guarantee_delivery'=> $guarantee_delivery,
            'fast_delivery'     => $fast_delivery,
            'time_delivery'     => $time_delivery,
            'receiver_pay'      => $receiver_pay,
            'created_by'        => $staff_id
          );
          $data_arr['created_at'] =  date('Y-m-d H:i:s');

          $save_result = true;
          $direct_print = "";
          if($save_mode == "edit"){

            $id = $this->getRequest()->getParam("id");
            $current_row = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
            $diff_count = 0;
            $diff_str = '';
            $code_change = false;

            foreach ($data_arr as $key => $value) {
              if( $current_row[$key] != $value ){
                if($key == 'mail_code'){
                  $code_change = true;
                }
                $diff_count++;
                $diff_str.=$key;
              }
            }
            // 2: min change (mail_code created_at)
            if( !($code_change && $diff_count == 2) ){
              $direct_print = "&print=true";
              $data_arr['mail_code'] = "";
            }

            $id_redirect = $id;
            $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $id);
            $save_result = $QFormPrint->update($data_arr, $where);
          }else{
            $save_result = $QFormPrint->insert($data_arr);
            $direct_print = "&print=true";
            $id_redirect = $save_result;
          }

          if($save_result == false){
              $flashMessenger->setNamespace('error')->addMessage("Có lỗi xảy ra, Vui lòng nhập lại");
              $this->_redirect(HOST.'form-print');
          }
          $this->_redirect(HOST.'form-print/preview?id='.$id_redirect.$direct_print);

      }else{

        $flashMessenger->setNamespace('error')->addMessage("Thông tin không hợp lệ, Vui lòng nhập lại");
        $this->_redirect(HOST.'form-print');

      }

  }

  function previewAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $data_arr = $QFormPrint->fetchRow($where);

    $address_arr = $this->explodeAddress($data_arr['receiver_address']);

    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id     = $userStorage->id;
    $test_users = USER_NHAT_TIN;
  
    $this->view->newform = (in_array($staff_id, $test_users));
    $this->view->show_print = $this->getRequest()->getParam('print');
    $this->view->id_infor = $id;
    $this->view->infor = $data_arr;
    $this->view->address = $address_arr;
  }

  function startAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);
    $this->view->infor = $form_print_data;
    $this->view->infor['created_at'] = $this->format_day($form_print_data['created_at']);


    $this->view->address = $this->explodeAddress($form_print_data['receiver_address']);

    $this->_helper->layout->disableLayout();

  }

  function startTestAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);
    $this->view->infor = $form_print_data;
    $this->view->infor['created_at'] = $this->format_day($form_print_data['created_at']);


    $this->view->address = $this->explodeAddress($form_print_data['receiver_address']);

    $this->_helper->layout->disableLayout();

  }
  
  function format_day($datetime){
    $date = array();
    $date = explode('-', explode(' ', $datetime)[0]);
    return $date[2].'/'.$date[1].'/'.$date[0];
  }

  function indexAction(){

      $flashMessenger = $this->_helper->flashMessenger;
      $db = Zend_Registry::get('db');
      $QFormPrint   = new Application_Model_FormPrint();
      $QDepartments = new Application_Model_Team();

      $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id     = $userStorage->id;

      $user_right   = in_array($staff_id, USER_NHAT_TIN) ? true : false;
      $this->view->user_right = $user_right;

      $departments  = $QDepartments->get_department();
      $this->view->departments = $departments;

      $form_id            = $this->getRequest()->getParam('form_id');
      $export             = $this->getRequest()->getParam('export',0);
      $created_at         = $this->getRequest()->getParam('created_at');
      $created_to         = $this->getRequest()->getParam('created_to');
      $sender_name        = $this->getRequest()->getParam('sender_name');
      $receiver_name      = $this->getRequest()->getParam('receiver_name');
      $sender_department  = $this->getRequest()->getParam('sender_department');
      $content            = $this->getRequest()->getParam('content');
      $receiver_address   = $this->getRequest()->getParam('receiver_address');
      $receiver_phone     = $this->getRequest()->getParam('receiver_phone');
      $sender_phone       = $this->getRequest()->getParam('sender_phone');
      $mail_code          = $this->getRequest()->getParam('mail_code');
      $page               = $this->getRequest()->getParam('page', 1);

      $params = array(
        "sender_name"       => $sender_name,
        "receiver_name"     => $receiver_name,
        "sender_department" => $sender_department,
        "content"           => $content,
        "receiver_address"  => $receiver_address,
        "receiver_phone"    => $receiver_phone,
        "sender_phone"      => $sender_phone,
        "mail_code"         => $mail_code,
        "id"                => $form_id,
        'created_at'        => $created_at,
        'created_to'        => $created_to,
        'created_by'        => $staff_id,
        'export'            => $export
      );

      $page     = $this->getRequest()->getParam('page',1);
      $limit    = LIMITATION;
      $total    = 0;

      $mail_list = $QFormPrint->fetchData($page, $limit, $total, $params);

      if($export){
        
        $this->_exportXlsx($mail_list);
      }

      $this->view->params     = $params;
      $this->view->mail_list  = $mail_list;
      $this->view->limit      = $limit;
      $this->view->total      = $total;
      $this->view->offset     = $limit * ($page - 1);
      $this->view->url        = HOST . 'form-print' . ($params ? '?' . http_build_query($params) .'&' : '?');
      $this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
      $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();

  }

  function editAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_cache();
    $this->view->offices           = $offices;

    $QTeam    = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->departments = $recursiveDeparmentTeamTitle;

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_all();
    $this->view->offices           = $offices;
    $QTeam    = new Application_Model_Team();
    $QStaff   = new Application_Model_Staff();
    $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id         = $userStorage->id;
    $current_user     = $QStaff->findStaffid($staff_id);
    $team = $current_user['team'];
    $title = $current_user['title'];
    $infoTeam  = $QTeam->find($title)->current();
      $team_name = $infoTeam['name'];
      $team_select = 'department';

      // check if in special team
       // check if in special team
      if($team_name == 'ASSISTANT' && in_array($team, array(75))){
           $team_select = 'title';
      }elseif( in_array($team, array(131, 133 ,147, 406 )) ){
            $team_select = 'team';
      }

      $this->view->depart = $QTeam->fetchRow( $QTeam->getAdapter()->quoteInto('id = ?',  $current_user[$team_select]) )->toArray()['name'];
      $this->view->user = $current_user;
    $this->view->infor_arr = $form_print_data;
    $this->_helper->viewRenderer->setRender('create');
  }

  function savecodeAction(){

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    if ( ! $this->getRequest()->isXmlHttpRequest() ) {
      echo '-100';
      exit;
    }

    $data_arr = $this->getRequest()->getParam('data_arr');
    $count_arr = count($data_arr);

    $QFormPrint = new Application_Model_FormPrint();

    foreach ($data_arr as $k => $item) {

      $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $item['id']);
      $result = $QFormPrint->update( array('mail_code' => $item['mail_code']), $where);
      if(!$result){
        echo json_encode(array(
          'status' => false,
          'id'     => $item['id']
        ));
      }

    }

    echo json_encode(array(
      'status' => true,
      'count'  => $count_arr
    ));
    exit();

  }
 function explodeAddress($address_str){
    $return_arr = array();
    $address_arr = explode(',' , $address_str);
    if(count($address_arr) <= 6){
      $return_arr['street'] = $address_arr[0] . ',' . $address_arr[1];
      $return_arr['ward'] = $address_arr[2];
      $return_arr['district'] = $address_arr[3];
      $return_arr['city'] = $address_arr[4];
      $return_arr['nation'] = $address_arr[5];  
    }else{
      $count_add = count($address_arr);
      $return_arr['street'] = "";
      for($i=0; $i<($count_add - 4); $i++){
        $return_arr['street'].= $address_arr[$i];
      }
      $return_arr['ward'] = $address_arr[$count_add - 4];
      $return_arr['district'] = $address_arr[$count_add - 3];
      $return_arr['city'] = $address_arr[$count_add - 2];
      $return_arr['nation'] = $address_arr[$count_add - 1];            
    }

    return $return_arr;
  }
  

  private function _exportXlsx($data){
    set_time_limit(0);
    error_reporting(0);
    ini_set('memory_limit', -1);
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $heads = array(
      'ID',
      'Mã phiếu gửi',
      'Người gửi',
      'Số điện thoại gửi',
      'Phòng ban gửi',
      'Người nhận',
      'Địa chỉ nhận',
      'Số điện thoại nhận',
      'Nội dung',
      'Hình thức chuyển phát',
      'Hình thức thanh toán',
//      'Gửi đảm bảo',
//      'Hỏa tốc',
//      'Phát hẹn giờ',
//      'Người nhận thanh toán',
      'Ngày lập đơn'
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
    $alpha = 'A';
    $index = 1;
    foreach ($heads as $key) {
      $sheet->setCellValue($alpha . $index, $key);
      $alpha++;
    }
    $index = 2;

    foreach($data as $key => $value):
      $alpha = 'A';
      $guarantee_delivery = ( intval($value['guarantee_delivery']) == 1 ) ? "Có" : "Không";
      $fast_delivery = ( intval($value['fast_delivery']) == 1 ) ? "Có" : "Không";
      $time_delivery = ( intval($value['time_delivery']) == 1 ) ? "Có" : "Không";
      $receiver_pay = ( intval($value['receiver_pay']) == 1 ) ? "Có" : "Không";
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['id']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['mail_code']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sender_name']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sender_phone']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['sender_department']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['receiver_name']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['receiver_address']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['receiver_phone']), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['content']), PHPExcel_Cell_DataType::TYPE_STRING);
        if($value['type']==1){
            $type= 'CPN';
        }
        else if($value['type']==2){
            $type = 'Hỏa tốc';
        }
        else if($value['type']==3){
            $type ='Quốc tế';
        }
      $sheet->getCell($alpha++.$index)->setValueExplicit( $type, PHPExcel_Cell_DataType::TYPE_STRING);
        if($value['type_pay']==1){
            $type_pay = 'Công nợ';
        }
        else if($value['type_pay']==2){
            $type_pay = 'Cá nhân';
        }
        else if($value['type_pay']==3){
            $type_pay = 'Người nhận thanh toán';
        }
        $sheet->getCell($alpha++.$index)->setValueExplicit( $type_pay, PHPExcel_Cell_DataType::TYPE_STRING);
//      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($guarantee_delivery), PHPExcel_Cell_DataType::TYPE_STRING);
//      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($fast_delivery), PHPExcel_Cell_DataType::TYPE_STRING);
//      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($time_delivery), PHPExcel_Cell_DataType::TYPE_STRING);
//      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($receiver_pay), PHPExcel_Cell_DataType::TYPE_STRING);
      $sheet->getCell($alpha++.$index)->setValueExplicit( trim($value['created_at']), PHPExcel_Cell_DataType::TYPE_STRING);

      $index++;
    endforeach;

    $filename = 'Phieu-Gui_' . date('d-m-Y');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    $objWriter->save('php://output');
    exit;
  }

  function copyAction(){
    $QFormPrint = new Application_Model_FormPrint();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);

    $QTeam    = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->departments = $recursiveDeparmentTeamTitle;
    
    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_all();
    $this->view->offices           = $offices;
    $QTeam    = new Application_Model_Team();
    $QStaff   = new Application_Model_Staff();
    $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id         = $userStorage->id;
    $current_user     = $QStaff->findStaffid($staff_id);
    $team = $current_user['team'];
    $title = $current_user['title'];
    $infoTeam  = $QTeam->find($title)->current();
      $team_name = $infoTeam['name'];
      $team_select = 'department';

      // check if in special team
       // check if in special team
      if($team_name == 'ASSISTANT' && in_array($team, array(75))){
           $team_select = 'title';
      }elseif( in_array($team, array(131, 133 ,147, 406 )) ){
            $team_select = 'team';
      }

    $this->view->depart = $QTeam->fetchRow( $QTeam->getAdapter()->quoteInto('id = ?',  $current_user[$team_select]) )->toArray()['name'];
    $this->view->user = $current_user;
    $this->view->infor_arr = $form_print_data;
    $this->view->copy = true;
    $this->view->infor_arr = $form_print_data;
    $this->_helper->viewRenderer->setRender('create');
  }

  function importAction(){

    $QFormPrint = new Application_Model_FormPrint();
    $flashMessenger = $this->_helper->flashMessenger;

    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id     = $userStorage->id;

    // import IOFactory Lib
    include 'PHPExcel/IOFactory.php';

    $inputFileName = $_FILES['file']['tmp_name'];

    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch(Exception $e) {
      $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Import File thất bại !");
      $this->redirect(HOST.'form-print');
    }

    $sheet          = $objPHPExcel->getSheet(0);
    $highestRow     = $sheet->getHighestRow();
    $highestColumn  = $sheet->getHighestColumn();
    $rowData        = array();

    for ($row = 2; $row <= $highestRow; $row++){
        $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE,FALSE);
    }

    $success_rows = 0;

    foreach ($rowData as $key => $item) {

      $value = $item[0];

      $guarantee_delivery   = ( trim($value[7]) == 'x' ) ? 1 : 0;
      $fast_delivery        = ( trim($value[8]) == 'x' ) ? 1 : 0;
      $time_delivery        = ( trim($value[9]) == 'x' ) ? 1 : 0;

      $data_arr = array(
        'sender_name'       => $value[0],
        'sender_phone'      => $value[1],
        'sender_department' => $value[2],
        'receiver_name'     => $value[3],
        'receiver_address'  => $value[4],
        'receiver_phone'    => $value[5],
        'content'           => $value[6],
        'guarantee_delivery'=> $guarantee_delivery,
        'fast_delivery'     => $fast_delivery,
        'time_delivery'     => $time_delivery,
        'created_by'        => $staff_id
      );

      // echo "<pre>";
      // print_r($data_arr);
      $data_arr['created_at']   = date('Y-m-d H:i:s');
      $save_result              = $QFormPrint->insert($data_arr);

      if($save_result != null){
        $success_rows ++;
      }

    }

    // exit();

    $flashMessenger->setNamespace('success')->addMessage( "Đã Import thành công ".$success_rows." dòng dữ liệu.");
    $this->redirect(HOST.'form-print');

  }

  function deleteAction(){
    $id = $this->getRequest()->getParam('id');
    if( $this->getRequest()->getParam('action') == 'delete' ){
      $QFormPrint = new Application_Model_FormPrint();
      $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $id);
      $result = $QFormPrint->update(array('del'=>'1'),$where);
      echo $result;
      die;
    }
    echo false;
    die;
  }

  function multipleAction(){
    $QFormPrint = new Application_Model_FormPrint();
    $flashMessenger = $this->_helper->flashMessenger;

    $action = $this->getRequest()->getParam('function');
    $multi_arr = $this->getRequest()->getParam('selected_item');

    if($action == "delete")
    {
      $success_rows = 0;

      foreach ($multi_arr as $key => $id) {
        $save_result = $QFormPrint->update(array('del'=>'1'),$QFormPrint->getAdapter()->quoteInto('id = ?', $id));
        if($save_result != 0){
          $success_rows ++;
        }
      }

      $flashMessenger->setNamespace('success')->addMessage( "Đã Xóa thành công ".$success_rows." dòng dữ liệu.");
      $this->redirect(HOST.'form-print');

    } else {

        $item_arr = Array();
        foreach ($multi_arr as $key => $id) {
          $form_item = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
          $this_add = $this->explodeAddress( $form_item['receiver_address'] );
          $form_item['address'] = $this_add;
          $form_item['created_at'] = $this->format_day($form_item['created_at']);
          $item_arr[] = $form_item;
        }

        $this->view->item_arr = $item_arr;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setRender('start');

    }

  }

// INVENTORY BY AREA
  public function inventoryAreaAction(){
    $QProductInvent = new Application_Model_ProductInventoryArea();
    $flashMessenger   = $this->_helper->flashMessenger;
    $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
    $this->view->messages_error   = $flashMessenger->setNamespace('error')->getMessages();
    $QArea = new Application_Model_Area();
    $QTeam = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;
    // PAGINATION
    $page     = $this->getRequest()->getParam('page',1);
    $area_id     = $this->getRequest()->getParam('area_id');
    $department     = $this->getRequest()->getParam('department');
    $export     = $this->getRequest()->getParam('export');
    $search     = $this->getRequest()->getParam('search');
    $limit    = LIMITATION;
    $total    = 0;
    
    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $QAsm         = new Application_Model_Asm();
    $QStaffPermissionInventory = new Application_Model_StaffPermissionInventory();   

// set permission for  assistan, superadviseor, service center leader
//    $staff_titles = array(240, 341, 238);    
//
//    if (in_array($userStorage->title, $staff_titles)) {
//      $list_regions = $this->setPermissionFor($staff_titles);
//    }   
    if (!in_array($userStorage->id, array(SUPERADMIN_ID, 340))) {  // 340 : chị Yến HR
           $list_regions = $this->setPermissionFor($userStorage->title);
           
    }else{
        $this->view->show = true;
    }
    
    if ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
    $area_list    = array();

    $list_regions = $QAsm->get_cache($userStorage->id);
    $where = $QStaffPermissionInventory->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
    $list_regions_inventory = $QStaffPermissionInventory->fetchAll($where)->toArray();
    $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
    foreach ($list_regions_inventory as $region) {
        $list_regions[] = $region['area_id'];
    } 
    $this->view->viewed_area_id = $list_regions;
    }
    
    $str_area = implode(',', $list_regions);
    // FILTER PARAMS
    $this_request = $this->getRequest();
    $params = array(
        "ma_thiet_bi"                     => $this_request->getParam('ma_thiet_bi',  null),
        "ten_thiet_bi"                    => $this_request->getParam('ten_thiet_bi',  null),
        "area_id"                         => $this_request->getParam('area_id',  null),
        "nhan_vien_su_dung_nhan_ban_giao" => $this_request->getParam('nhan_vien_su_dung_nhan_ban_giao',  null),
        "so_bien_ban_ban_giao"            => $this_request->getParam('so_bien_ban_ban_giao',  null),
        "tinh_trang_thiet_bi"             => $this_request->getParam('tinh_trang_thiet_bi',  null),
        "ngay_mua_from"                   => $this_request->getParam('ngay_mua_from',  null),
        "ngay_mua_to"                     => $this_request->getParam('ngay_mua_to',  null),
        "ngay_nhan_ban_giao_from"         => $this_request->getParam('ngay_nhan_ban_giao_from',  null),
        "ngay_nhan_ban_giao_to"           => $this_request->getParam('ngay_nhan_ban_giao_to',  null),
        "list_regions"                    => $list_regions,
        "area_id"                         => $area_id,
        "export"                          => $export,
        "department"                     => !in_array($userStorage->id, array(SUPERADMIN_ID, 340)) ? $userStorage ->department : $department,
//        "department"                      =>  $department,
        "staff_code"                      => $this_request->getParam('staff_code',  null),
        "search"                          => $search,
        
    );
   
    $data_arr = $QProductInvent->fetchPagination($page, $limit, $total, $params);
    
    if(!empty($export)){
      $this->_exportInventoryArea($data_arr);
    }
    
// RETURN VIEW
//   if (in_array($userStorage->id, array(SUPERADMIN_ID, 340))) {  // 340 : chị Yến HR
//           $this->view->show = true;
//    }

    $all_area =  $QArea->fetchAll(null, 'name');
    $this->view->areas = $all_area;
    $this->view->params     = $params;
    $this->view->data_arr   = $data_arr;
    $this->view->limit      = $limit;
    $this->view->total      = $total;
    $this->view->offset     = $limit * ($page - 1);
    $this->view->url        = HOST . 'form-print/inventory-area' . ($params ? '?' . http_build_query($params) .'&' : '?');
  }
  public function disableProductAction(){
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);
    $QProductInventoryArea = new Application_Model_ProductInventoryArea();
    $id                    = $this->getRequest()->getParam('id');

    if(empty($id)){
         $this->_redirect(HOST.'form-print/inventory-area');
    }else{
        $data_update = array(
            'del' => 1
        );
        $where_update = $QProductInventoryArea->getAdapter()->quoteInto('id = ?', $id);      
        $QProductInventoryArea->update($data_update, $where_update);
    }
  }

  public function createInventoryAreaAction(){
        $flashMessenger   = $this->_helper->flashMessenger;
        $QArea = new Application_Model_Area();
        $areas = $QArea->get_cache();
       
        $QTeam = new Application_Model_Team();
        $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
        $this->view->recursiveDeparmentTeamTitle = $recursiveDeparmentTeamTitle;

        $QProductInvent = new Application_Model_ProductInventoryArea();
        $id = $this->getRequest()->getParam('id', null);
        
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
        $QTeam  = new Application_Model_Team();
        $staff_title_info = $userStorage->title;
        $team_info = $QTeam->find($staff_title_info);
        $team_info = $team_info->current();
        $group_id = $team_info['access_group'];
		
        if ($userStorage->id == 340) {  // 340 : chị Yến HR
           $this->view->show = true;
        }
        if(in_array($group_id, array(ADMINISTRATOR_ID,HR_ID)) ){
             $all_area =  $QArea->fetchAll(null, 'name');
            foreach ($all_area->toArray() as $key => $area){
            $listArea[$area['id']] = $area['name'];
             $this->view->area = $listArea;
        }
       
        }else{
            $QAsm         = new Application_Model_Asm();
            $QStaffPermissionInventory = new Application_Model_StaffPermissionInventory();   
            // set permission for  assistan, superadviseor, service center leader
//            $staff_titles = array(240, 341, 238);    
//            if (in_array($userStorage->title, $staff_titles)) {
//              $list_regions = $this->setPermissionFor($staff_titles);
//            }
            $list_regions = $this->setPermissionFor($userStorage->title);
            if ( in_array($userStorage->group_id, My_Staff_Group::$allow_in_area_view) && !My_Staff_Permission_Area::view_all($userStorage->id)) {
            $area_list    = array();

            $list_regions = $QAsm->get_cache($userStorage->id);
            $where = $QStaffPermissionInventory->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
            $list_regions_inventory = $QStaffPermissionInventory->fetchAll($where)->toArray();
            $list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
            foreach ($list_regions_inventory as $region) {
                $list_regions[] = $region['area_id'];
            } 
            $this->view->viewed_area_id = $list_regions;
        }
        $str_area = implode(',', $list_regions);

        $where_area = $QArea->getAdapter()->quoteInto("id IN ($str_area)", '');
        $all_area   = $QArea->fetchAll($where_area);
        $listArea   = array(); 
        foreach ($all_area->toArray() as $key => $area){
            $listArea[$area['id']] = $area['name'];
        }
        $this->view->area = $listArea;
        }       
      
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id    = $userStorage->id;
        $QRegionalMarket                = new Application_Model_RegionalMarket();
        $regional_market      = $QRegionalMarket->find($userStorage->regional_market);
        $area_id = $regional_market->toArray()[0]['area_id'];
        if( !empty($_POST) ){
        $mode   = ($id == null) ? 'create' : 'update';
        $params = [
          'ma_thiet_bi'                             => $this->getRequest()->getParam('ma_thiet_bi',null),  
          'tinh_trang_tang_giam_so_luong'           => $this->getRequest()->getParam('tinh_trang_tang_giam_so_luong',null),
          'area_id'                                 => $this->getRequest()->getParam('area_id',null),
          'ngay_mua'                                => $this->getRequest()->getParam('ngay_mua',null),
          'bp_de_xuat_su_dung'                      => $this->getRequest()->getParam('bp_de_xuat_su_dung',null),
          'so_lan_sua_chua'                         => $this->getRequest()->getParam('so_lan_sua_chua',null),
          'chi_phi_sua_chua_thanh_ly'               => $this->getRequest()->getParam('chi_phi_sua_chua_thanh_ly',null),
          'ghi_chu'                                 => $this->getRequest()->getParam('ghi_chu',null),
          'ten_thiet_bi'                            => $this->getRequest()->getParam('ten_thiet_bi',null),
          'ly_do_tang_giam_so_luong'                => $this->getRequest()->getParam('ly_do_tang_giam_so_luong',null),
          'don_gia_luon_vat'                        => $this->getRequest()->getParam('don_gia_luon_vat',null),
          'ngay_nhan_ban_giao'                      => $this->getRequest()->getParam('ngay_nhan_ban_giao',null),
          'nhan_vien_su_dung_nhan_ban_giao'         => $this->getRequest()->getParam('nhan_vien_su_dung_nhan_ban_giao',null),
          'noi_dung_sua_chua_thanh_ly'              => $this->getRequest()->getParam('noi_dung_sua_chua_thanh_ly',null),
          'ly_do_sua_chua_thanh_ly'                 => $this->getRequest()->getParam('ly_do_sua_chua_thanh_ly',null),
          'dvt'                                     => $this->getRequest()->getParam('dvt',null),
          'mo_ta'                                   => $this->getRequest()->getParam('mo_ta',null),
          'sl'                                      => $this->getRequest()->getParam('sl',null),
          'sl_cu'                                   => $this->getRequest()->getParam('sl_cu',null),
          'bo_phan'                                 => $this->getRequest()->getParam('bo_phan',null),
          'bo_phan_nhan_ban_giao'                   => $this->getRequest()->getParam('bo_phan_nhan_ban_giao',null),
          'tinh_trang_thiet_bi'                     => $this->getRequest()->getParam('tinh_trang_thiet_bi',null),
          'chuc_vu_nhan_vien_su_dung_nhan_ban_giao' => $this->getRequest()->getParam('chuc_vu_nhan_vien_su_dung_nhan_ban_giao',null),
          'thoi_gian_sua_chuathanh_ly'              => $this->getRequest()->getParam('thoi_gian_sua_chuathanh_ly',null),
          'so_bien_ban_ban_giao'                    => $this->getRequest()->getParam('so_bien_ban_ban_giao',null),
          'created_by'                              => $staff_id,
          'created_at'                              => date('Y-m-d H:i:s'),
          'department'                              => ($staff_id == 340) ? $this->getRequest()->getParam('department',null) : $userStorage->department

        ];

        if( $mode == 'create' ){
          $rs   = $QProductInvent->insert($params);
        }else{
          $this_where = $QProductInvent->getAdapter()->quoteInto('id = ?', $id);
          $rs   = $QProductInvent->update($params,$this_where);
        }

        if(empty($rs)){
            $flashMessenger->setNamespace('error')->addMessage("Có lỗi xảy ra, Vui lòng thử lại");
        }else{
            $flashMessenger->setNamespace('success')->addMessage("Done");
        }

        $this->_redirect(HOST.'form-print/inventory-area');

      }else{
        if($id!=null){
          $this_where = $QProductInvent->getAdapter()->quoteInto('id = ?', $id);
          $this_product_invent = $QProductInvent->fetchRow($this_where);
          $this->view->id = $id;
          $this->view->params = $this_product_invent;
        }
      }
  
  }

  function _exportInventoryArea($data_arr){
    require_once 'PHPExcel.php';
    $PHPExcel = new PHPExcel();
    $alphaExcel = new My_AlphaExcel();

    $heads = array(
      $alphaExcel->ShowAndUp() => 'ID',
      $alphaExcel->ShowAndUp() => 'TÌNH TRẠNG TĂNG GIẢM SỐ LƯỢNG',
      $alphaExcel->ShowAndUp() => 'LÝ DO TĂNG GIẢM SỐ LƯỢNG',
      $alphaExcel->ShowAndUp() => 'KHU VỰC CỦ',
      $alphaExcel->ShowAndUp() => 'KHU VỰC',
      $alphaExcel->ShowAndUp() => 'TÊN THIẾT BỊ',
      $alphaExcel->ShowAndUp() => 'MÃ THIẾT BỊ',
      $alphaExcel->ShowAndUp() => 'MÔ TẢ',
      $alphaExcel->ShowAndUp() => 'ĐVT',
      $alphaExcel->ShowAndUp() => 'SL cũ',
      $alphaExcel->ShowAndUp() => 'SL Hiện tại',
      $alphaExcel->ShowAndUp() => 'ĐƠN GIÁ (LUÔN VAT)',
      $alphaExcel->ShowAndUp() => 'NGÀY MUA',
      $alphaExcel->ShowAndUp() => 'BP ĐỀ XUẤT/ SỬ DỤNG',
      $alphaExcel->ShowAndUp() => 'NHÂN VIÊN SỬ DỤNG/ NHẬN BÀN GIAO',
      $alphaExcel->ShowAndUp() => 'CHỨC VỤ NHÂN VIÊN SỬ DỤNG/ NHẬN BÀN GIAO',
      $alphaExcel->ShowAndUp() => 'NGÀY NHẬN BÀN GIAO',
      $alphaExcel->ShowAndUp() => 'TÌNH TRẠNG THIẾT BỊ',
      $alphaExcel->ShowAndUp() => 'SỐ LẦN SỬA CHỮA',
      $alphaExcel->ShowAndUp() => 'NỘI DUNG SỬA CHỮA/ THANH LÝ',
      $alphaExcel->ShowAndUp() => 'THỜI GIAN SỬA CHỮA/THANH LÝ',
      $alphaExcel->ShowAndUp() => 'CHI PHÍ SỬA CHỮA/ THANH LÝ',
      $alphaExcel->ShowAndUp() => 'LÝ DO SỬA CHỮA/ THANH LÝ',
      $alphaExcel->ShowAndUp() => 'SỐ BIÊN BẢN BÀN GIAO',
      $alphaExcel->ShowAndUp() => 'GHI CHÚ',
      $alphaExcel->ShowAndUp() => 'BP ĐỀ XUẤT/ SỬ DỤNG',
      $alphaExcel->ShowAndUp() => 'BỘ PHẬN NHẬN BÀN GIAO',
      $alphaExcel->ShowAndUp() => 'BỘ PHẬN',
      $alphaExcel->ShowAndUp() => 'NGƯỜI TẠO',
    );

    $PHPExcel->setActiveSheetIndex(0);
    $sheet = $PHPExcel->getActiveSheet();
   
    foreach($heads as $key => $value){
      $sheet->setCellValue($key.'1', $value);          
    }

//    $sheet->getStyle('A1:V')->applyFromArray(array('font' => array('bold' => true)));
    foreach ($data_arr as $key => $value) {            
      $alphaExcel = new My_AlphaExcel();
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['id']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['tinh_trang_tang_giam_so_luong']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['ly_do_tang_giam_so_luong']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['khu_vuc']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['area']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['ten_thiet_bi']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['ma_thiet_bi']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['mo_ta']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['dvt']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['sl_cu']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['sl']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['don_gia_luon_vat']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['ngay_mua']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['bp_de_xuat_su_dung']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['nhan_vien_su_dung_nhan_ban_giao']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['chuc_vu_nhan_vien_su_dung_nhan_ban_giao']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['ngay_nhan_ban_giao']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['tinh_trang_thiet_bi']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['so_lan_sua_chua']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['noi_dung_sua_chua_thanh_ly']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['thoi_gian_sua_chuathanh_ly']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['chi_phi_sua_chua_thanh_ly']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['ly_do_sua_chua_thanh_ly']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['so_bien_ban_ban_giao']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['ghi_chu']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['bo_phan']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['bo_phan_nhan_ban_giao']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staff_department']);
      $sheet->setCellValue($alphaExcel->ShowAndUp() . ($key+2), $value['staff_create']);
    }

    $filename_over = 'Thietbi-' . date('Y-m-d H-i-s');
    $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename_over . '.xlsx"');
    $objWriter->save('php://output');
    exit;
    die();
  }
  
  public function setPermissionFor($staff_titles )
  {
      $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
      $QStaffPermission = new Application_Model_StaffPermission();
      $QOffice = new Application_Model_Office();
      $QStaffPermissionInventory = new Application_Model_StaffPermissionInventory();   

    

      $where = $QStaffPermissionInventory->getAdapter()->quoteInto('staff_id = ?', $userStorage->id);
      $list_regions_inventory = $QStaffPermissionInventory->fetchAll($where)->toArray();

      $officeId = $QStaffPermission->getOfficeId($userStorage->code);
        
      if ($officeId) {
          $str_list_officeId = implode(',',$officeId);
          $list_regions = $QOffice->getAreaId($str_list_officeId);
      }  else {
          $list_regions[] = -1;
      }

      foreach ($list_regions_inventory as $region) {
          $list_regions[] = $region['area_id'];
      } 
           // $this->view->viewed_area_id = $list_regions;
      return $list_regions;

  }
  
  public function deletePermissionInventoryAction()
  {
      $QStaffPermissionInventory = new Application_Model_StaffPermissionInventory();

      $id = $this->getRequest()->getParam('id');
      $where = $QStaffPermissionInventory->getAdapter()->quoteInto('id = ?', $id);

      $QStaffPermissionInventory->delete($where);
  }
  
  public function savePermissionInventoryAction()
  {
        $QStaff = new Application_Model_Staff();
        $QStaffPermissionInventory = new Application_Model_StaffPermissionInventory();
       
        $staff_id = $this->getRequest()->getParam('staff_id');
        $staff_code = $this->getRequest()->getParam('staff_code');
        $areas = $this->getRequest()->getParam('area');

        $whereStaff = $QStaff->getAdapter()->quoteInto('code = ?', $staff_code);
        $staff = $QStaff->fetchRow($whereStaff);

        if ($staff_id) {
          $whereStaffId = $QStaffPermissionInventory->getAdapter()->quoteInto('staff_id = ?', $staff_id);
          $QStaffPermissionInventory->delete($whereStaffId);
        }
        
        foreach ($areas as $area) {
          $data = array(
            'staff_id' => $staff['id'],
            'area_id' => $area
          );

          $QStaffPermissionInventory->insert($data);
        }

        $this->_redirect(HOST.'form-print/list-permission-inventory');
  }
  public function createPermissionInventoryAction()
  {
     require_once 'form-print' . DIRECTORY_SEPARATOR . 'create-permission-inventory.php';  
  }
  
  public function listPermissionInventoryAction(){
     require_once 'form-print' . DIRECTORY_SEPARATOR . 'list-permission-inventory.php';  
  }
  

  public function listJtAction()
  {
         $flashMessenger = $this->_helper->flashMessenger;
      $db = Zend_Registry::get('db');
      $QFormPrint   = new Application_Model_FormPrintJt();
      $QDepartments = new Application_Model_Team();

      $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id     = $userStorage->id;
	  $office_id = $userStorage->office_id;
	  if(empty($office_id) || $office_id != 49){
		$this->_redirect(HOST);
	  }

      // $user_right   = ($staff_id == 2128) ? true : false;
	        $user_right   = in_array($staff_id, USER_NHAT_TIN) ? true : false;
      $this->view->user_right = $user_right;



      $departments  = $QDepartments->get_department();
      $this->view->departments = $departments;

      $form_id            = $this->getRequest()->getParam('form_id');
      $export             = $this->getRequest()->getParam('export',0);
      $created_at         = $this->getRequest()->getParam('created_at');
      $created_to         = $this->getRequest()->getParam('created_to');
      $sender_name        = $this->getRequest()->getParam('sender_name');
      $receiver_name      = $this->getRequest()->getParam('receiver_name');
      $sender_department  = $this->getRequest()->getParam('sender_department');
      $content            = $this->getRequest()->getParam('content');
      $receiver_address   = $this->getRequest()->getParam('receiver_address');
      $receiver_phone     = $this->getRequest()->getParam('receiver_phone');
      $sender_phone       = $this->getRequest()->getParam('sender_phone');
      $mail_code          = $this->getRequest()->getParam('mail_code');
      $page               = $this->getRequest()->getParam('page', 1);

      $params = array(
        "sender_name"       => $sender_name,
        "receiver_name"     => $receiver_name,
        "sender_department" => $sender_department,
        "content"           => $content,
        "receiver_address"  => $receiver_address,
        "receiver_phone"    => $receiver_phone,
        "sender_phone"      => $sender_phone,
        "mail_code"         => $mail_code,
        "id"                => $form_id,
        'created_at'        => $created_at,
        'created_to'        => $created_to,
        'created_by'        => $staff_id,
        'export'            => $export
      );

      $page     = $this->getRequest()->getParam('page',1);
      $limit    = LIMITATION;
      $total    = 0;

      $mail_list = $QFormPrint->fetchData($page, $limit, $total, $params);

      if($export){
        
        $this->_exportXlsx($mail_list);
      }

      $this->view->params     = $params;
      $this->view->mail_list  = $mail_list;
      $this->view->limit      = $limit;
      $this->view->total      = $total;
      $this->view->offset     = $limit * ($page - 1);
      $this->view->url        = HOST . 'form-print/list-jt' . ($params ? '?' . http_build_query($params) .'&' : '?');
      $this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
      $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
  }
  
    function editJtAction(){
    $QFormPrint = new Application_Model_FormPrintJt();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_cache();
    $this->view->offices           = $offices;

    $QTeam    = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->departments = $recursiveDeparmentTeamTitle;

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_cache();
    $this->view->offices           = $offices;

    $this->view->infor_arr = $form_print_data;
    $this->view->mode = 'edit';
    $this->_helper->viewRenderer->setRender('create-jt');
  }

    function savecodeJtAction(){

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    if ( ! $this->getRequest()->isXmlHttpRequest() ) {
      echo '-100';
      exit;
    }

    $data_arr = $this->getRequest()->getParam('data_arr');
    $count_arr = count($data_arr);

    $QFormPrint = new Application_Model_FormPrintJt();

    foreach ($data_arr as $k => $item) {

      $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $item['id']);
      $result = $QFormPrint->update( array('mail_code' => $item['mail_code']), $where);
      if(!$result){
        echo json_encode(array(
          'status' => false,
          'id'     => $item['id']
        ));
      }

    }

    echo json_encode(array(
      'status' => true,
      'count'  => $count_arr
    ));
    exit();

  }

    function copyJtAction(){
    $QFormPrint = new Application_Model_FormPrintJt();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);

    $QTeam    = new Application_Model_Team();
    $recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
    $this->view->departments = $recursiveDeparmentTeamTitle;

    $QOffice          = new Application_Model_Office();
    $offices          = $QOffice->get_cache();  
    $this->view->offices           = $offices;

    $this->view->copy = true;
    $this->view->infor_arr = $form_print_data;
    $this->_helper->viewRenderer->setRender('create-jt');
  }

    function importJtAction(){

    $QFormPrint = new Application_Model_FormPrintJt();
    $flashMessenger = $this->_helper->flashMessenger;

    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id     = $userStorage->id;

    // import IOFactory Lib
    include 'PHPExcel/IOFactory.php';

    $inputFileName = $_FILES['file']['tmp_name'];

    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch(Exception $e) {
      $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Import File thất bại !");
      $this->redirect(HOST.'form-print');
    }

    $sheet          = $objPHPExcel->getSheet(0);
    $highestRow     = $sheet->getHighestRow();
    $highestColumn  = $sheet->getHighestColumn();
    $rowData        = array();

    for ($row = 2; $row <= $highestRow; $row++){
        $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE,FALSE);
    }

    $success_rows = 0;

    foreach ($rowData as $key => $item) {

      $value = $item[0];

      $guarantee_delivery   = ( trim($value[7]) == 'x' ) ? 1 : 0;
      $fast_delivery        = ( trim($value[8]) == 'x' ) ? 1 : 0;
      $time_delivery        = ( trim($value[9]) == 'x' ) ? 1 : 0;

      $data_arr = array(
        'sender_name'       => $value[0],
        'sender_phone'      => $value[1],
        'sender_department' => $value[2],
        'receiver_name'     => $value[3],
        'receiver_address'  => $value[4],
        'receiver_phone'    => $value[5],
        'content'           => $value[6],
        'guarantee_delivery'=> $guarantee_delivery,
        'fast_delivery'     => $fast_delivery,
        'time_delivery'     => $time_delivery,
        'created_by'        => $staff_id
      );

      // echo "<pre>";
      // print_r($data_arr);
      $data_arr['created_at']   = date('Y-m-d H:i:s');
      $save_result              = $QFormPrint->insert($data_arr);

      if($save_result != null){
        $success_rows ++;
      }

    }

    // exit();

    $flashMessenger->setNamespace('success')->addMessage( "Đã Import thành công ".$success_rows." dòng dữ liệu.");
    $this->redirect(HOST.'form-print');

  }

    function deleteJtAction(){
    $id = $this->getRequest()->getParam('id');

    if( $this->getRequest()->getParam('action') == 'delete-jt' ){
      $QFormPrint = new Application_Model_FormPrintJt();
      $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $id);
      $result = $QFormPrint->update(array('del'=>'1'),$where);
      echo $result;
      die;
    }
    echo false;
    die;
  }

    function multipleJtAction(){
    $QFormPrint = new Application_Model_FormPrintJt();
    $flashMessenger = $this->_helper->flashMessenger;

    $action = $this->getRequest()->getParam('function');
    $multi_arr = $this->getRequest()->getParam('selected_item');

    if($action == "delete")
    {
      $success_rows = 0;

      foreach ($multi_arr as $key => $id) {
        $save_result = $QFormPrint->update(array('del'=>'1'),$QFormPrint->getAdapter()->quoteInto('id = ?', $id));
        if($save_result != 0){
          $success_rows ++;
        }
      }

      $flashMessenger->setNamespace('success')->addMessage( "Đã Xóa thành công ".$success_rows." dòng dữ liệu.");
      $this->redirect(HOST.'form-print/list-jt');

    } else {

        $item_arr = Array();
        foreach ($multi_arr as $key => $id) {
          $form_item = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
          $this_add = $this->explodeAddress( $form_item['receiver_address'] );
          $form_item['address'] = $this_add;
          $item_arr[] = $form_item;
        }

        $this->view->item_arr = $item_arr;

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setRender('start-jt');

    }

  }

   function startJtAction(){
    $QFormPrint = new Application_Model_FormPrintJt();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $form_print_data = $QFormPrint->fetchRow($where);
    $this->view->infor = $form_print_data;
    // echo '<pre>';
    // print_r($form_print_data->toArray()); 
    // die;
    $this->view->infor['created_at'] = $this->format_day($form_print_data['created_at']);

    $this->view->address = $this->explodeAddress($form_print_data['receiver_address']);



    $this->_helper->layout->disableLayout();

     // $this->redirect(HOST.'form-print/list-jt');
  }

     function saveJtAction(){
      $QFormPrint = new Application_Model_FormPrintJt();
      $flashMessenger = $this->_helper->flashMessenger;

      $userStorage = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id    = $userStorage->id;

      if($this->getRequest()->getMethod() == "POST"){

          $save_mode          = $this->getRequest()->getParam('save_mode');

          $mail_code          = $this->getRequest()->getParam('mail_code');
          $document           = $this->getRequest()->getParam('document',0);
          $parcel              = $this->getRequest()->getParam('parcel',0);
          $time_delivery      = $this->getRequest()->getParam('time_delivery',0);
          $receiver_pay       = $this->getRequest()->getParam('receiver_pay',0);
          $sender_name        = $this->getRequest()->getParam('sender_name');
          $sender_phone       = $this->getRequest()->getParam('sender_phone');
          $sender_department  = $this->getRequest()->getParam('sender_department');
          $receiver_name      = $this->getRequest()->getParam('receiver_name');
          $receiver_address   = $this->getRequest()->getParam('receiver_address');
          $receiver_phone     = $this->getRequest()->getParam('receiver_phone');
          $content            = $this->getRequest()->getParam('content');

          $document           = $this->getRequest()->getParam('document',0);
          $parcel              = $this->getRequest()->getParam('parcel',0);

          $data_arr = array(
            'mail_code'         => $mail_code,
            'content'           => $content,
            'sender_name'       => $sender_name,
            'sender_phone'      => $sender_phone,
            'sender_department' => $sender_department,
            'receiver_name'     => $receiver_name,
            'receiver_address'  => $receiver_address,
            'receiver_phone'    => $receiver_phone,
            'guarantee_delivery'=> $guarantee_delivery,
            'fast_delivery'     => $fast_delivery,
            'time_delivery'     => $time_delivery,
            'receiver_pay'      => $receiver_pay,
            'created_by'        => $staff_id,
            'document'          => $document,
            'parcel'            => $parcel

          );
          $data_arr['created_at'] =  date('Y-m-d H:i:s');

          $save_result = true;
          $direct_print = "";
          if($save_mode == "edit-jt"){

            $id = $this->getRequest()->getParam("id");
            $current_row = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
            $diff_count = 0;
            $diff_str = '';
            $code_change = false;

            foreach ($data_arr as $key => $value) {
              if( $current_row[$key] != $value ){
                if($key == 'mail_code'){
                  $code_change = true;
                }
                $diff_count++;
                $diff_str.=$key;
              }
            }
            // 2: min change (mail_code created_at)
            if( !($code_change && $diff_count == 2) ){
              $direct_print = "&print=true";
              $data_arr['mail_code'] = "";
            }

            $id_redirect = $id;
            $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $id);
            $save_result = $QFormPrint->update($data_arr, $where);
          }else{
            $save_result = $QFormPrint->insert($data_arr);
            $direct_print = "&print=true";
            $id_redirect = $save_result;
          }

          if($save_result == false){
              $flashMessenger->setNamespace('error')->addMessage("Có lỗi xảy ra, Vui lòng nhập lại");
              $this->_redirect(HOST.'form-print');
          }
          $this->_redirect(HOST.'form-print/preview-jt?id='.$id_redirect.$direct_print);

      }else{

        $flashMessenger->setNamespace('error')->addMessage("Thông tin không hợp lệ, Vui lòng nhập lại");
        $this->_redirect(HOST.'form-print/list-jt');

      }

  }

    function createJtAction(){
      $flashMessenger   = $this->_helper->flashMessenger;
      $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
      $this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
      $this->view->messages_error   = $flashMessenger->setNamespace('error')->getMessages();

      $QStaff           = new Application_Model_Staff();
      $userStorage      = Zend_Auth::getInstance()->getStorage()->read();
      $staff_id         = $userStorage->id;
      $current_user     = $QStaff->findStaffid($staff_id);
		
	  $office_id = $userStorage->office_id;
	  if(empty($office_id) || $office_id != 49){
		$this->_redirect(HOST);
	  }
	  
	  
      $QOffice          = new Application_Model_Office();
      $offices          = $QOffice->get_all();
      $this->view->offices           = $offices;
		
		$QTeam    = new Application_Model_Team();
	  $team = $current_user['team'];
	  $title = $current_user['title'];
	  $infoTeam  = $QTeam->find($title)->current();
      $team_name = $infoTeam['name'];
      $team_select = 'department';

      // check if in special team
       // check if in special team
     if($team_name == 'ASSISTANT' && in_array($team, array(75))){
		 
           $team_select = 'title';
      }elseif( in_array($team, array(131, 133 ,147, 406 )) ){
            $team_select = 'team';
      }

      $this->view->depart = $QTeam->fetchRow( $QTeam->getAdapter()->quoteInto('id = ?',  $current_user[$team_select]) )->toArray()['name'];
      $this->view->user = $current_user;
	  /*
      $QTeam    = new Application_Model_Team();

      $team = $current_user['team'];
      $team_select = 'department';

      // check if in special team
      if( in_array($team, array(152, 131, 133)) ){
        $team_select = 'team';
      }

      $this->view->depart = $QTeam->fetchRow( $QTeam->getAdapter()->quoteInto('id = ?',  $current_user[$team_select]) )->toArray()['name'];
      $this->view->user = $current_user;
	  */
  }

   function previewJtAction(){
    $QFormPrint = new Application_Model_FormPrintJt();

    $id = $this->getRequest()->getParam('id');

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
    $data_arr = $QFormPrint->fetchRow($where);

    $address_arr = $this->explodeAddress($data_arr['receiver_address']);

    $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
    $staff_id     = $userStorage->id;
    $test_users = USER_NHAT_TIN;
  
    $this->view->newform = (in_array($staff_id, $test_users));
    $this->view->show_print = $this->getRequest()->getParam('print');
    $this->view->id_infor = $id;
    $this->view->infor = $data_arr;
    $this->view->address = $address_arr;
  }

    public function listNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'list-nhat-tin.php';
    }
    function editNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'edit-nhat-tin.php';
    }
    function saveCodeNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'save-code-nhat-tin.php';
    }
    function copyNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'copy-nhat-tin.php';
    }
    function importNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'import-nhat-tin.php';
    }
    function deleteNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'delete-nhat-tin.php';
    }
    function multipleNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'multiple-nhat-tin.php';
    }
    function startNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'start-nhat-tin.php';
    }
    function saveNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'save-nhat-tin.php';
    }
    function createNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'create-nhat-tin.php';

    }
    function previewNhatTinAction(){
        require_once 'form-print'.DIRECTORY_SEPARATOR.'preview-nhat-tin.php';
    }
    public function uploadEmailDealerAction() {
      require_once 'form-print' . DIRECTORY_SEPARATOR . 'upload-email-dealer.php';
    }
    public function saveMassEmailDealerAction(){
        require_once 'form-print' . DIRECTORY_SEPARATOR . 'save-mass-email-dealer.php';
    }

    public function feedbackResultDcAction(){
        require_once 'form-print' . DIRECTORY_SEPARATOR . 'feedback-result-dc.php';
    }
    public function saveMassFeedbackDcAction(){
        require_once 'form-print' . DIRECTORY_SEPARATOR . 'save-mass-feedback-dc.php';
    }
}
