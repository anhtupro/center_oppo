<?php

class ManageController extends My_Controller_Action
{
    private $data;

    public function salesTeamAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-team'.DIRECTORY_SEPARATOR.'sales-team.php';
    }

    public function salesTeamCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-team'.DIRECTORY_SEPARATOR.'sales-team-create.php';
    }

    public function salesTeamSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-team'.DIRECTORY_SEPARATOR.'sales-team-save.php';
    }

    public function salesTeamDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-team'.DIRECTORY_SEPARATOR.'sales-team-del.php';

    }

    public function departmentAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'department'.DIRECTORY_SEPARATOR.'department.php';
    }

    public function departmentCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'department'.DIRECTORY_SEPARATOR.'department-create.php';

    }

    public function departmentSaveAction()
    {

        require_once 'manage'.DIRECTORY_SEPARATOR.'department'.DIRECTORY_SEPARATOR.'department-save.php';

    }

    public function departmentDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'department'.DIRECTORY_SEPARATOR.'department-del.php';

    }

    public function religionAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'religion'.DIRECTORY_SEPARATOR.'religion.php';

    }

    public function religionCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'religion'.DIRECTORY_SEPARATOR.'religion-create.php';

    }

    public function religionSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'religion'.DIRECTORY_SEPARATOR.'religion-save.php';
    }

    public function religionDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'religion'.DIRECTORY_SEPARATOR.'religion-del.php';

    }

    public function nationalityAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'nationality'.DIRECTORY_SEPARATOR.'nationality.php';

    }

    public function nationalityCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'nationality'.DIRECTORY_SEPARATOR.'nationality-create.php';

    }

    public function nationalitySaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'nationality'.DIRECTORY_SEPARATOR.'nationality-save.php';

    }

    public function nationalityDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'nationality'.DIRECTORY_SEPARATOR.'nationality-del.php';

    }

    /*********************** PROVINCE **************************************/

    public function provinceAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'province'.DIRECTORY_SEPARATOR.'list.php';
    }

    public function provinceCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'province'.DIRECTORY_SEPARATOR.'create.php';
    }

    public function provinceSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'province'.DIRECTORY_SEPARATOR.'save.php';
    }

    public function provinceDeleteAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'province'.DIRECTORY_SEPARATOR.'delete.php';
    }

    /*********************** END PROVINCE ***********************************/

    /*********************** DISTRICT **************************************/

    public function districtAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'district'.DIRECTORY_SEPARATOR.'list.php';
    }

    public function districtCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'district'.DIRECTORY_SEPARATOR.'create.php';
    }

    public function districtSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'district'.DIRECTORY_SEPARATOR.'save.php';
    }

    public function districtDeleteAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'district'.DIRECTORY_SEPARATOR.'delete.php';
    }

    /*********************** END DISTRICT ***********************************/

    public function titleAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'title'.DIRECTORY_SEPARATOR.'title.php';

    }

    public function titleCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'title'.DIRECTORY_SEPARATOR.'title-create.php';

    }

    public function titleSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'title'.DIRECTORY_SEPARATOR.'title-save.php';

    }

    public function titleDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'title'.DIRECTORY_SEPARATOR.'title-del.php';

    }

    public function teamAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'team'.DIRECTORY_SEPARATOR.'team.php';

    }

    public function teamCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'team'.DIRECTORY_SEPARATOR.'team-create.php';

    }

    public function teamSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'team'.DIRECTORY_SEPARATOR.'team-save.php';

    }

    public function teamDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'team'.DIRECTORY_SEPARATOR.'team-del.php';

    }

    public function contractTermAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-term'.DIRECTORY_SEPARATOR.'contract-term.php';

    }

    public function contractTermCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-term'.DIRECTORY_SEPARATOR.'contract-term-create.php';

    }

    public function contractTermSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-term'.DIRECTORY_SEPARATOR.'contract-term-save.php';

    }

    public function contractTermDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-term'.DIRECTORY_SEPARATOR.'contract-term-del.php';

    }

    public function contractTypeAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-type'.DIRECTORY_SEPARATOR.'contract-type.php';

    }

    public function contractTypeCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-type'.DIRECTORY_SEPARATOR.'contract-type-create.php';

    }

    public function contractTypeSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-type'.DIRECTORY_SEPARATOR.'contract-type-save.php';

    }

    public function contractTypeDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'contract-type'.DIRECTORY_SEPARATOR.'contract-type-del.php';

    }

    public function productAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.'product.php';

    }

    public function productCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.'product-create.php';

    }

    public function productSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.'product-save.php';

    }

    public function productDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.'product-del.php';

    }

    public function areaAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'area'.DIRECTORY_SEPARATOR.'area.php';

    }

    public function areaCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'area'.DIRECTORY_SEPARATOR.'area-create.php';

    }

    public function areaSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'area'.DIRECTORY_SEPARATOR.'area-save.php';

    }

    public function areaDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'area'.DIRECTORY_SEPARATOR.'area-del.php';
    }

    public function companyAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.'list.php';
    }

    public function companyDeleteAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.'delete.php';
    }

    public function companyEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.'edit.php';
    }

    public function companyCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.'create.php';
    }

    public function companySaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.'save.php';
    }

    public function assignAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id    = $userStorage->id;
        
        $QArea       = new Application_Model_Area();
        $areas  = $QArea->fetchAll();

        $this->view->areas = $areas;

        $this->_helper->viewRenderer->setRender('regional-market/assign');
    }

    public function storeAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'store.php';

    }

    public function storeLeaderAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'store-leader.php';

    }

    public function storeCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'store-create.php';

    }

    public function storeEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'store-edit.php';

    }

    public function storeSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'store-save.php';

    }

    public function storeDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'store-del.php';

    }

    public function storeUndelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'store-undel.php';

    }

    public function viewStoreStaffAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR.'view-store-staff.php';

    }

    public function notificationAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification.php';

    }

    public function notificationCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-create.php';

    }

    public function notificationSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-save.php';

    }

    public function notificationDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-del.php';

    }

    public function notificationUploadAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-upload.php';

    }

    public function notificationCategoryAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-category.php';
    }

    public function notificationCategorySaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-category-save.php';

    }

    public function notificationCategoryEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-category-edit.php';

    }

    public function notificationCategoryDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'notification-category-del.php';

    }

    public function notificationMassUploadAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'mass-upload.php';
    }

    public function notificationMassUploadSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'mass-upload-save.php';
    }

    public function dealerAction()
    {
    	require_once 'manage'.DIRECTORY_SEPARATOR.'dealer'.DIRECTORY_SEPARATOR.'dealer.php';
    }

    public function dealerSaveAction()
    {
    	require_once 'manage'.DIRECTORY_SEPARATOR.'dealer'.DIRECTORY_SEPARATOR.'dealer-save.php';
    }

    public function dealerEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'dealer'.DIRECTORY_SEPARATOR.'dealer-edit.php';
    }

    public function pgMultipleStoreAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'pg-multiple-store'.DIRECTORY_SEPARATOR.'list.php';
    }

    public function pgMultipleStoreCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'pg-multiple-store'.DIRECTORY_SEPARATOR.'create.php';
    }

    public function pgMultipleStoreSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'pg-multiple-store'.DIRECTORY_SEPARATOR.'save.php';
    }

    public function pgMultipleStoreEndAction()
    {
    	require_once 'manage'.DIRECTORY_SEPARATOR.'pg-multiple-store'.DIRECTORY_SEPARATOR.'end.php';
    }

    public function menuAction(){
        $this->_helper->viewRenderer->setRender('menu/menu');
    }

    public function cacheAction(){
        $del = $this->getRequest()->getParam('del');

        if ($del) {
            My_Cache::clear_cache();
        }

        $back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $this->_redirect( ( $back_url ? $back_url : HOST ) );
    }

    public function exceptionCaseAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $limit = LIMITATION;
        $total = 0;

        $params = array();

        $QModel = new Application_Model_ExceptionCase();
        $exceptions = $QModel->fetchPagination($page, $limit, $total, $params);

        $this->view->exceptions = $exceptions;

        $this->view->limit = $limit;
        $this->view->total = $total;
        $this->view->url = HOST.'manage/exception-case/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;

        $this->_helper->viewRenderer->setRender('exception-case/index');
    }

    public function exceptionCaseCreateAction(){

        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $QModel = new Application_Model_ExceptionCase();
            $exceptionRowset = $QModel->find($id);
            $exception_case = $exceptionRowset->current();

            $this->view->exception_case = $exception_case;

            switch ($exception_case->name) {
                case 'SALES_EXCEPTION':
                    include_once 'manage'.DIRECTORY_SEPARATOR.'exception-case'.DIRECTORY_SEPARATOR.'sales.php';
                    break;

                case 'ASM_EXCEPTION':
                    include_once 'manage'.DIRECTORY_SEPARATOR.'exception-case'.DIRECTORY_SEPARATOR.'asm.php';
                    break;

                default:
                    include_once 'manage'.DIRECTORY_SEPARATOR.'exception-case'.DIRECTORY_SEPARATOR.'default.php';
            }
        }

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages = $messages;

        //back url
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER');

        $this->_helper->viewRenderer->setRender('exception-case/create');
    }

    public function exceptionCaseSaveAction(){

        if ($this->getRequest()->getMethod() == 'POST'){
            $name = $this->getRequest()->getParam('name');

            switch ($name){
                case 'SALES_EXCEPTION':
                    include_once 'manage'.DIRECTORY_SEPARATOR.'exception-case'.DIRECTORY_SEPARATOR.'sales-save.php';
                    break;

                case 'ASM_EXCEPTION':
                    include_once 'manage'.DIRECTORY_SEPARATOR.'exception-case'.DIRECTORY_SEPARATOR.'asm-save.php';
                    break;

                default:
                    include_once 'manage'.DIRECTORY_SEPARATOR.'exception-case'.DIRECTORY_SEPARATOR.'default-save.php';
            }


        } else

            $this->_redirect( HOST.'manage/exception-case' );
    }

    public function exceptionCaseDelAction(){
        $id = $this->getRequest()->getParam('id');

        $QModel = new Application_Model_ExceptionCase();
        $where = $QModel->getAdapter()->quoteInto('id = ?', $id);
        $QModel->delete($where);

        //remove cache
        $cache = Zend_Registry::get('cache');
        $cache->remove('exception_case_cache');

        $this->_redirect('/manage/exception-case');
    }

    /* SHIFT */
    public function shiftAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'shift'.DIRECTORY_SEPARATOR.'shift.php';

    }

    public function shiftCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'shift'.DIRECTORY_SEPARATOR.'shift-create.php';

    }

    public function shiftSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'shift'.DIRECTORY_SEPARATOR.'shift-save.php';

    }

    public function shiftDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'shift'.DIRECTORY_SEPARATOR.'shift-del.php';

    }

    public function asmAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'asm'.DIRECTORY_SEPARATOR.'asm.php';
    }

    public function asmEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'asm'.DIRECTORY_SEPARATOR.'asm-edit.php';
    }

    public function asmSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'asm'.DIRECTORY_SEPARATOR.'asm-save.php';
    }

    public function salesAdminAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-admin'.DIRECTORY_SEPARATOR.'sales-admin.php';
    }

    public function salesAdminEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-admin'.DIRECTORY_SEPARATOR.'sales-admin-edit.php';
    }

    public function salesAdminSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-admin'.DIRECTORY_SEPARATOR.'sales-admin-save.php';
    }

    public function asmStandbyAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'asm'.DIRECTORY_SEPARATOR.'standby.php';
    }

    public function asmStandbyEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'asm'.DIRECTORY_SEPARATOR.'standby-edit.php';
    }

    public function asmStandbySaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'asm'.DIRECTORY_SEPARATOR.'standby-save.php';
    }

    public function leaderAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'list.php';
    }

    public function leaderRegionAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'region.php';
    }

    public function leaderRegionCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'region-create.php';
    }

    public function leaderRegionDeleteAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'region-delete.php';
    }

    public function leaderRegionEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'region-edit.php';
    }
    public function leaderRegionPlusAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'region-plus.php';
    }
    public function processleaderRegionPlusAction()
    {
        
    }
    public function leaderRegionSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'region-save.php';
    }

    public function saveSalesRegionAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'save-sales-region.php';
    }

    public function leaderViewAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'view.php';
    }

    public function leaderSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'save.php';
    }
    
    public function storeAreaSaveAction()
    {
        $flashMessenger = $this->_helper->flashMessenger;
        try {
            $area       = $this->getRequest()->getParam('area');
            $pic        = $this->getRequest()->getParam('store_ids');
            $remove     = $this->getRequest()->getParam('store_remove_ids');
        
            //ADD
            $store_ids = explode(',', $pic);
            $store_ids = is_array($store_ids) ? array_filter($store_ids) : array();
        
            $store_ids = array_unique($store_ids);
        
            // định dạng store id hiện tại
            foreach ($store_ids as $key => $s_id) {
                $store_ids[$key] = intval( trim( $s_id ) );
            }
            
            //REMOVE
            $remove_ids = explode(',', $remove);
            $remove_ids = is_array($remove_ids) ? array_filter($remove_ids) : array();
        
            $remove_ids = array_unique($remove_ids);
        
            // định dạng store id hiện tại
            foreach ($remove_ids as $key => $s_id) {
                $remove_ids[$key] = intval( trim( $s_id ) );
            }
            
            $QStore = new Application_Model_Store();
            $QArea  = new Application_Model_Area();
            $QStoreChanel = new Application_Model_StoreAreaChanel();
        
            // cập nhật d_id cho các store mới thêm
            foreach ($store_ids as $k => $s_id) {
                // kiểm tra store này có thuộc area nào chưa
                $where = array();
                $where[] = $QStoreChanel->getAdapter()->quoteInto('store_id = ?', $s_id);
                $where[] = $QStoreChanel->getAdapter()->quoteInto('area_id <> ?', $area);
                $check_store = $QStoreChanel->fetchRow($where);
                
                // kiểm tra store này có thuộc area này chưa
                $where = array();
                $where[] = $QStoreChanel->getAdapter()->quoteInto('store_id = ?', $s_id);
                $where[] = $QStoreChanel->getAdapter()->quoteInto('area_id = ?', $area);
                $check_store2 = $QStoreChanel->fetchRow($where);
                //
                
                $whereStore = $QStore->getAdapter()->quoteInto('id = ?', $s_id);
                $c_store = $QStore->fetchRow($whereStore);
                
                $whereArea = $QArea->getAdapter()->quoteInto('id = ?', $check_store['area_id']);
                $c_area = $QArea->fetchRow($whereArea);


                
                if ($check_store) {
        
                    echo '<script>
                            parent.palert("Store thứ '.($k+1).' ['.$c_store['name'] .'] đã thuộc Khu vực ['.$c_area['name'] .'].Vui lòng loại bỏ store này ở Khu vực ['.$c_area['name'] .'] rồi mới thêm.");
        
                            parent.alert("Store thứ '.($k+1).' ['.$c_store['name'] .'] đã thuộc Khu vực ['.$c_area['name'] .'].Vui lòng loại bỏ store này ở Khu vực ['.$c_area['name'] .'] rồi mới thêm.");
                        </script>';
                    exit;
                }
                
                if ($check_store2) {
        
                    echo '<script>
                            parent.palert("Store thứ '.($k+1).' ['.$c_store['name'] .'] đã tồn tại.");
        
                            parent.alert("Store thứ '.($k+1).' ['.$c_store['name'] .'] đã tồn tại.");
                        </script>';
                    exit;
                }


                $currentTime = date('Y-m-d H:i:s');

                $regional_market = NULL;
                if($area == AREA_HN1){
                    $regional_market = REGIONAL_HN1;
                }elseif($area == AREA_HN2){
                    $regional_market = REGIONAL_HN2;
                }
                $data = array(
                    'store_id'  => $s_id,
                    'area_id'   => $area,
                    'regional_market' => $regional_market,
                    'from_date' => $currentTime
                    );
        
                $parent = $QStoreChanel->insert($data);
        
            }
            
            
            // Xóa các store remove
            foreach ($remove_ids as $k => $s_id) {
                $where = array();
                $where[] = $QStoreChanel->getAdapter()->quoteInto('store_id = ?', $s_id);
                $where[] = $QStoreChanel->getAdapter()->quoteInto('area_id = ?', $area);
                $parent = $QStoreChanel->delete($where);
            }
        
        
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('success')->addMessage('Done!');
        
            echo '<script>parent.location.href="'. HOST.'manage/store-area?area_id='.$area.'"</script>';
            exit;
        
        } catch(Exception $ex) {
            echo '<script>
                    parent.palert("'.$ex->getMessage().'");
        
                    parent.alert("'.$ex->getMessage().'");
                </script>';
            exit;
        }
        
        exit;
    }
    
    public function salesPgAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-pg'.DIRECTORY_SEPARATOR.'list.php';
    }

    public function salesPgViewAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-pg'.DIRECTORY_SEPARATOR.'view.php';
    }

    public function salesPgSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'sales-pg'.DIRECTORY_SEPARATOR.'save.php';
    }
    
    public function informAction()
    {
         require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'index.php';
    }
    
    public function informUploadAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-upload.php';
    }
    
    public function informUploadSaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-upload-save.php';
    }
    
    public function informCreateAction()
    {
         require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-create.php';
    }
    
    public function informViewAction()
    {
         require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-view.php';
    }
    
    public function informSaveAction()
    {
         require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-save.php';
    }
    
    public function informDelAction()
    {
         require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-del.php';
    }
    
    public function informCategoryAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-category.php';
    }
    
    public function informCategoryCreateAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-category-create.php';
    }
    
    public function informCategoryEditAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-category-edit.php';
    }
    
    public function informCategorySaveAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-category-save.php';
    }
    
     public function informCategoryDelAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'inform-category-del.php';
    }


    private function generate_list($group_menus) {
        return $this->ul(0, '', $group_menus);
    }

    function ul($parent = 0, $attr = '', $group_menus = null) {
        static $i = 1;
        $indent = str_repeat("\t\t", $i);
        if (isset($this->data)) {
            if ($attr) {
                $attr = ' ' . $attr;
            }
            $html = "\n$indent";
            $html .= "<ul$attr>";
            $i++;
            foreach ($this->data as $row) {
                $html .= "\n\t$indent";
                $html .= '<li>';
                $html .= '<input value="'.$row['id'].'" '.( ($group_menus and in_array($row['id'] , $group_menus)) ? 'checked' : '' ).' type="checkbox" id="menus_'.$row['id'].'" name="menus[]"><label for="menus_'.$row['id'].'">'.$row['label'].'</label>';

                $html .= '</li>';
            }
            $html .= "\n$indent</ul>";
            return $html;
        } else {
            return false;
        }
    }

    private function add_row($id, $label) {
        $this->data[] = array('id' => $id, 'label' => $label);
    }
    
    public function storeAreaChanelAction() {
        // list Leaders
        // if leader logged in, view only his name
        // if asm, view leaders in his area
        // else show all
        
        // check condition
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $area_id     = $this->getRequest()->getParam('area_id');
        $province    = $this->getRequest()->getParam('province');
        $name        = $this->getRequest()->getParam('name');
        $email       = $this->getRequest()->getParam('email');
        $store       = $this->getRequest()->getParam('store');
        $page        = $this->getRequest()->getParam('page', 1);
        $sort        = $this->getRequest()->getParam('sort', 'name');
        $desc        = $this->getRequest()->getParam('desc', 0);
        $limit       = LIMITATION;
        $total       = 0;
        
        $params = array(
            'area_id'  => $area_id,
            'province' => $province,
            'name'     => $name,
            'email'    => $email,
            'store'    => $store,
            'sort'     => $sort,
            'desc'     => $desc,
            );
        
        
        $QStore = new Application_Model_StoreAreaChanel();
        $Store  = $QStore->fetchPagination($page, $limit, $total, $params);
        $this->view->store = $Store;
        
        $QArea = new Application_Model_Area();
        $QRegionalMarket = new Application_Model_RegionalMarket();
        
        if ($area_id) {
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
            $this->view->regions_search = $QRegionalMarket->fetchAll($where, 'name');
        }
        
        $this->view->areas   = $QArea->get_cache();
        $this->view->regions = $QRegionalMarket->get_cache_all();
        $this->view->params  = $params;
        $this->view->limit   = $limit;
        $this->view->total   = $total;
        $this->view->desc    = $desc;
        $this->view->sort    = $sort;
        $this->view->offset  = $limit*($page-1);
        $this->view->url     = HOST.'manage/leader'.( $params ? '?'.http_build_query($params).'&' : '?' );
        
        $flashMessenger      = $this->_helper->flashMessenger;
        
        $this->view->messages       = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        
        $this->_helper->viewRenderer->setRender('store-area-chanel/list');
        // get leader list

    
    }
    
    public function storeAreaAction() {
        // list Leaders
        // if leader logged in, view only his name
        // if asm, view leaders in his area
        // else show all
        
        // check condition
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        //Area_id : Area_chanel
        $area_id     = $this->getRequest()->getParam('area_id');
        $province    = $this->getRequest()->getParam('province');
        $name        = $this->getRequest()->getParam('name');
        $email       = $this->getRequest()->getParam('email');
        $store       = $this->getRequest()->getParam('store');
        $page        = $this->getRequest()->getParam('page', 1);
        $sort        = $this->getRequest()->getParam('sort', 'name');
        $desc        = $this->getRequest()->getParam('desc', 0);
        $limit       = LIMITATION;
        $total       = 0;
        
        $params = array(
            'area_id'  => $area_id,
            'province' => $province,
            'name'     => $name,
            'email'    => $email,
            'store'    => $store,
            'sort'     => $sort,
            'desc'     => $desc,
            );
        
        
        $QStore = new Application_Model_StoreAreaChanel();
        $Store  = $QStore->fetchPagination($page, $limit, $total, $params);
        $this->view->store = $Store;
        
        $QArea = new Application_Model_Area();
        $QRegionalMarket = new Application_Model_RegionalMarket();
        
        if ($area_id) {
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $area_id);
            $this->view->regions_search = $QRegionalMarket->fetchAll($where, 'name');
        }
        
        $this->view->areas   = $QArea->get_cache();
        $this->view->regions = $QRegionalMarket->get_cache_all();
        $this->view->params  = $params;
        $this->view->limit   = $limit;
        $this->view->total   = $total;
        $this->view->desc    = $desc;
        $this->view->sort    = $sort;
        $this->view->offset  = $limit*($page-1);
        $this->view->url     = HOST.'manage/leader'.( $params ? '?'.http_build_query($params).'&' : '?' );
        
        $flashMessenger      = $this->_helper->flashMessenger;
        
        $this->view->messages       = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages_error = $flashMessenger->setNamespace('error')->getMessages();
        
        $this->_helper->viewRenderer->setRender('store-area-chanel/store-area');
        // get leader list
        
            
    }
    
    public function saveNotificationAction($params){
    	$db = Zend_Registry::get('db');

    	//List All Staff_id
    	$user_ids		= array();
    	if($params['all_staff'] == 1){
    		   $select_Staff 	= $db->select() ->from(array('p' => 'staff'),array('p.id'))
    		   									->where('p.status = 1');
    		       
    		   $user_ids 		=	$db->fetchAll($select_Staff);
    	}
    	
//     	List Staff_id with Title
    	$user_ids_with_title = array();
    	if(!empty($params['title_objects'])){
    		$select_ids_Title 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Title->where('p.title IN (?)',$params['title_objects'])
    						->where('p.status = 1');
    		$user_ids_with_title 	=	$db->fetchAll($select_ids_Title);
    	}
    	
    	
//     	List Staff_id with area
    	$user_ids_with_area = array();
    	if(!empty($params['area_objects'])){
    		$select_ids_Area 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Area->joinInner(array('rm' => 'regional_market'), 'p.regional_market=rm.id', array());
    		$select_ids_Area->where('rm.area_id IN (?)',$params['area_objects'])
    						->where('p.status = 1');
    		$user_ids_with_area 	=	$db->fetchAll($select_ids_Area);
    		 
    	}
    	
    	//     	List Staff_id with Company
    	$user_ids_with_company = array();
        $user_company=[];
    	if(!empty($params['company_objects'])){
    		$select_ids_Company 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Company->where('p.company_id IN (?)',$params['company_objects'])
    							->where('p.status = 1');
    		$user_ids_with_company 	=	$db->fetchAll($select_ids_Company);
                if(!empty($user_ids_with_company))
                foreach ($user_ids_with_company as $k => $list_user_id){
    			$user_company[]		= $list_user_id['id'];
    		}
    	}
		
    	//     	List Staff_id width pic
    	$user_ids_with_pic = array();
    	if(!empty($params['staff'])){
    		$user_ids_with_pic 	=	$params['staff'];
    	}
    	$list_users=array();
    	if($params['type'] == 1){
    		$list_user_ids =(array_merge($user_ids,$user_ids_with_title,$user_ids_with_area)) ;
    		
    		foreach ($list_user_ids as $k => $list_user_id){
    			$list_users[]		= $list_user_id['id'];
    		}
    	}
    	//$list_user_ids =(array_merge($user_ids,$user_ids_with_title,$user_ids_with_area)) ;
    	
    	if($params['type'] == 2){
    		if($user_ids_with_title){
    			foreach ($user_ids_with_title as $k => $val){
    				$titles[]		= $val['id'];
    			}
    		}
    		
    		if($user_ids_with_area){
    			foreach ($user_ids_with_area as $k => $val){
    				$areas[]		= $val['id'];
    			}
    		}
    		
    		$list_users = array_intersect($titles,$areas);
    	}
    	
    	
    	$list_user_ids = array_unique(array_merge($list_users,$user_ids_with_pic));
    	if(!empty($params['company_objects'])){
            $list_user_ids=array_intersect($list_user_ids,$user_company);
        }
    	
    	
    	if($list_user_ids){
    		$from 	= empty($params['from'])  ? null : DateTime::createFromFormat('d/m/Y H:i', $params['from'])->format('Y-m-d H:i:00');
    		$to		= empty($params['to']) 	  ? null : DateTime::createFromFormat('d/m/Y H:i', $params['to'])->format('Y-m-d H:i:00');
    		
    		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
    		$config = $config->toArray();
    		$db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
    				'host'     => $config['resources']['db']['params']['host'],
    				'username' => $config['resources']['db']['params']['username'],
    				'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
    				'dbname'   => $config['resources']['db']['params']['dbname']
    		));
    		
    		 
    		$arr_users =     $this->get_arr_user($list_user_ids,$from,$to,$params['id']);
    		if(empty($from) || empty($to)){
    			$sql = "INSERT INTO `notification_access` (user_id,notification_id,notification_status,created_date) VALUES $arr_users ";
    		}else{
    			$sql = "INSERT INTO `notification_access` (user_id,notification_id,notification_status,notification_from,notification_to,created_date) VALUES $arr_users ";
    		
    		}
    		 
    		$db_log->query($sql);
    	}else{
    		throw new Exception("Cannot Save, Please choose Staff ");
    	}
    	
    
    }
    
    public function removeUserAction($params){
    	$notification_id= $params['notification_id'];
    	$db = Zend_Registry::get('db');
    	
    	$select_noti = $db->select()->from(array('p' => 'notification_access'),array('p.*'));
    	$select_noti->where('p.notification_id = ?',$notification_id);
    	
    	$result 	=	$db->fetchAll($select_noti);
    	 
    	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
    	$config = $config->toArray();
    	$db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
    			'host'     => $config['resources']['db']['params']['host'],
    			'username' => $config['resources']['db']['params']['username'],
    			'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
    			'dbname'   => $config['resources']['db']['params']['dbname']
    	));
    	$old_noti = array();
    	$old_user = array();

    	if($result){
    		foreach ($result as $old_notification){
    			$old_noti[]	= $old_notification['notification_id'];
    			$old_user[]	= $old_notification['user_id'];
    		}
    		 
    		$str_diff1_delete	= $this->toStringAction($old_user);
    		
    		$select_olds 	= "SELECT `p`.* FROM notification_access AS `p` WHERE(p.notification_id = $notification_id) AND ( p.user_id IN ($str_diff1_delete))";
    		 
    		$arr_deletes  			= $db_log->fetchAll($select_olds);
    		 
    		$arr_to_deletes = array();
    		foreach ($arr_deletes as $arr_delete){
    			$arr_to_deletes[]	= $arr_delete['id'];
    			 
    		}
    		 
    		$str_id_delete = $this->toStringAction($arr_to_deletes);
    		 
    		$sqlDelete = "DELETE FROM `notification_access` WHERE id IN ($str_id_delete) ";
    		
    		$db_log->query($sqlDelete);
    	}
    	
    }
    
    public function updateUserAction($params){
    	
    	$this->_helper->viewRenderer->setNoRender(true);
    	$QNotificationAccess = new Application_Model_NotificationAccess();
    	$notification_id = $params['notification_id'];
    	$db = Zend_Registry::get('db');
    	
    	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
    	$config = $config->toArray();
    	$db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
    			'host'     => $config['resources']['db']['params']['host'],
    			'username' => $config['resources']['db']['params']['username'],
    			'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
    			'dbname'   => $config['resources']['db']['params']['dbname']
    	));
    	    	 
    	$data 	= array(
    			'notification_status'	=> 0
    	);
    	
    	$from 	= !$params['from']  ? null : DateTime::createFromFormat('d/m/Y H:i', $params['from'])->format('Y-m-d H:i:00');
    	$to		= !$params['to'] 	? null : DateTime::createFromFormat('d/m/Y H:i', $params['to'])->format('Y-m-d H:i:00');
    	
    	//List All Staff_id
    	$user_ids		= array();
    	if($params['all_staff'] == 1){
    		$select_Staff 	= $db->select() ->from(array('p' => 'staff'),array('p.id'))
    										->where('p.status = 1');
    		$user_ids 		=	$db->fetchAll($select_Staff);
    	}
    
    	//     	List Staff_id with Title
    	$user_ids_with_title = array();
    	if(!empty($params['title_objects'])){
    		$select_ids_Title 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Title->where('p.title IN (?)',$params['title_objects'])
    						 ->where('p.status = 1');
    		$user_ids_with_title 	=	$db->fetchAll($select_ids_Title);
    	}
    	 
    	//     	List Staff_id with area
    	$user_ids_with_area = array();
    	if(!empty($params['area_objects'])){
    		$select_ids_Area 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Area->joinInner(array('rm' => 'regional_market'), 'p.regional_market=rm.id', array());
    		$select_ids_Area->where('rm.area_id IN (?)',$params['area_objects'])
    						->where('p.status = 1');
    		$user_ids_with_area 	=	$db->fetchAll($select_ids_Area);
    		 
    	}
    	 
    	//     	List Staff_id with Company
    	$user_ids_with_company = array();
    	if(!empty($params['company_objects'])){
    		$select_ids_Company 		= 	$db->select()->from(array('p' => 'staff'),array('p.id'));
    		$select_ids_Company->where('p.company_id IN (?)',$params['company_objects'])
    						   ->where('p.status = 1');
    		$user_ids_with_company 	=	$db->fetchAll($select_ids_Company);
    	}
    	
    	
		//     	List Staff_id with pic
    	$user_ids_with_pic = array();
    	if(!empty($params['staff'])){
    		$user_ids_with_pic 	=	$params['staff'];
    	}
		
    	$list_user_ids =(array_merge($user_ids,$user_ids_with_title,$user_ids_with_area,$user_ids_with_company)) ;
    	
    	
    	$list_users=array();
    	foreach ($list_user_ids as $k => $list_user_id){
    		$list_users[]		= $list_user_id['id'];
    	}
    	
    	
    	$list_user_ids = array_unique(array_merge($list_users,$user_ids_with_pic));
    	
    	
    	$where = $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', $notification_id
    	);
    	$old_notifications = $QNotificationAccess->fetchAll($where);
    	
    	$old_noti = array();
    	$old_user = array();
    	 
    	foreach ($old_notifications as $old_notification){
    		$old_noti[]	= $old_notification['notification_id'];
    		$old_user[]	= $old_notification['user_id'];
    	}
    	
    	
    	$arr_diff1 = $this->changeArrayAction(array_diff($old_user, $list_user_ids)); 
    	$arr_diff2 = $this->changeArrayAction(array_diff($list_user_ids, $old_user)); 
		
    	
    	//Not change Staff - Update NotificationAccess
		if(array_diff($list_user_ids,$old_user)  == array_diff($old_user,$list_user_ids)){
			$str_update_access	= $this->toStringAction($old_user);
			$select_update_access = "SELECT `p`.* FROM notification_access AS `p` WHERE(p.notification_id = $notification_id) AND ( p.user_id IN ($str_update_access))";
			$access_updates  = $db_log->fetchAll($select_update_access);
			
			$arr_access_updates=array();
			foreach ($access_updates as $access_update){
				$arr_access_updates[]	= $access_update['id'];
			}
			
			
			$str_id_update = $this->toStringAction($arr_access_updates);
			
			if(!empty($from)){
				$sqlAccessfrom = $this->sqlUpdateAccessAction($arr_access_updates,$from);
				
				$sqlUpdateAccess = "UPDATE  `notification_access` SET notification_from = CASE  $sqlAccessfrom ELSE '' END  WHERE id IN ($str_id_update) ";
				$db_log->query($sqlUpdateAccess);
			}
			if(!empty($to)){
				$sqlAccessto = $this->sqlUpdateAccessAction($arr_access_updates,$to);
				
				$sqlUpdateAccess = "UPDATE  `notification_access` SET notification_to = CASE  $sqlAccessto ELSE '' END  WHERE id IN ($str_id_update) ";
				$db_log->query($sqlUpdateAccess);
			}
			
			$sqlAccess=$this->sqlUpdateAction($arr_access_updates);
 			$sqlUpdateAccess = "UPDATE  `notification_access` SET notification_status = CASE  $sqlAccess ELSE '' END  WHERE id IN ($str_id_update) ";
 			$db_log->query($sqlUpdateAccess);
			
		}
    	
    	$arr1_arr2 = array_unique(array_merge($old_user,$list_user_ids));
    	$diff1_diff2 = array_merge($arr_diff1,$arr_diff1);
    	
    	$arr_doubles = array_diff($arr1_arr2,$diff1_diff2);
    	
    
    	
    	
    	if(!empty($arr_doubles)){
    		$str_doubles = '';
    		 
    		$new_arr_doubles = array();
    		foreach ($arr_doubles as $arr_double){
    			$new_arr_doubles[] = $arr_double;
    		}
    		$total_doubles = count($new_arr_doubles);
    		 
    		foreach ($new_arr_doubles as $k => $arr_double){
    			 
    			if($k == $total_doubles-1){
    				$str_doubles  .= $arr_double;
    			}else{
    				$str_doubles  .= $arr_double. ',';
    			}
    			 
    		}
    		
    		$select_doubles = "SELECT `p`.* FROM notification_access AS `p` WHERE(p.notification_id = $notification_id) AND ( p.user_id IN ($str_doubles))";
    		$arr_doubles_updates  = $db_log->fetchAll($select_doubles);

    		$arr_updates = array();
    		foreach ($arr_doubles_updates as $arr_doubles_update){
    			$arr_updates[]	= $arr_doubles_update['id'];
    		}
    		$str_id_update = $this->toStringAction($arr_updates);
    		$sql = $this->sqlUpdateAction($arr_updates);
    		
    		if(!empty($from)){
    			$sqlAccessfrom = $this->sqlUpdateAccessAction($arr_updates,$from);
    		
    			$sqlUpdateAccessfrom = "UPDATE  `notification_access` SET notification_from = CASE  $sqlAccessfrom ELSE '' END  WHERE id IN ($str_id_update) ";
    			echo $sqlUpdateAccessfrom;
    			$db_log->query($sqlUpdateAccessfrom);
    		}
    		if(!empty($to)){
    			$sqlAccessto = $this->sqlUpdateAccessAction($arr_updates,$to);
    		
    			$sqlUpdateAccessto = "UPDATE  `notification_access` SET notification_to = CASE  $sqlAccessto ELSE '' END  WHERE id IN ($str_id_update) ";
    			echo $sqlUpdateAccessto;
    			$db_log->query($sqlUpdateAccessto);
    		}
    		
    		$sqlUpdate = "UPDATE  `notification_access` SET notification_status = CASE  $sql ELSE '' END WHERE id IN ($str_id_update) ";
			
    		$db_log->query($sqlUpdate);
    	}
    	
    	
    	if(!empty($arr_diff1)){
    		$str_diff1_delete	= $this->toStringAction($arr_diff1);
    		$select_olds 	= "SELECT `p`.* FROM notification_access AS `p` WHERE(p.notification_id = $notification_id) AND ( p.user_id IN ($str_diff1_delete))";
    	
    		$arr_deletes  			= $db_log->fetchAll($select_olds);
    		 
    		$arr_to_deletes = array();
    		foreach ($arr_deletes as $arr_delete){
    			$arr_to_deletes[]	= $arr_delete['id'];
    				
    		}

    		$str_id_delete = $this->toStringAction($arr_to_deletes);
    		
    		$sqlDelete = "DELETE FROM `notification_access` WHERE id IN ($str_id_delete) ";
    		$db_log->query($sqlDelete);
    	}
    	
    	
    	
    	if(!empty($arr_diff2)){
    		$str_diff2_insert	= $this->toStringAction($arr_diff2);
    		
    		$arr_users =     $this->get_arr_user($arr_diff2,$from,$to,$params['id']);
    	
    		if(empty($from) || empty($to)){
    			$sql = "INSERT INTO `notification_access` (user_id,notification_id,notification_status,created_date) VALUES $arr_users ";
    		}else{
    			$sql = "INSERT INTO `notification_access` (user_id,notification_id,notification_status,notification_from,notification_to,created_date) VALUES $arr_users ";
    		}
    		
    		$db_log->query($sql);
    	}

    	exit();
    }
	
	public function get_arr_user($params, $from, $to, $notification_id) {
        $_from = !empty($from) ? $from : null;
        $_to = !empty($to) ? $to : null;
        $total = count($params);
        $str_log = '';
        foreach ($params as $k => $param) {
            $user_id = $param;
            $status = 0;

            $created_at = date('Y-m-d H:i:s');

            if (empty($_from) || empty($_to)) {
                
                    $str_log .= '(' . $user_id . ',' . $notification_id . ',' . $status . ',' . '"' . $created_at . '"' . '),';
                
            } else {
                
                    $str_log .= '(' . $user_id . ',' . $notification_id . ',' . $status . ',' . '"' . $_from . '"' . ',' . '"' . $_to . '"' . ',' . '"' . $created_at . '"' . '),';
                
            }
        }
     
        return rtrim($str_log,",");
    }

  
    public function sqlUpdateAction($params){
    	$sql_str = '';
    	foreach ($params as $param){
    		$sql_str .= ' WHEN id = ' . $param . ' THEN 0';
    	}
    	return $sql_str;
    }
    
    public function sqlUpdateAccessAction($params,$name){
    	$sql_str = '';
    	
    	if(!empty($name)){
    		foreach ($params as $param){
    			$sql_str .= ' WHEN id = ' . $param . ' THEN '. '"' .$name. '"' ;
    		}
    	}
  	
    	
    	return $sql_str;
    }
    
    public function toStringAction($params){
    	$str = '';
    	$total = count($params);
    	foreach ($params as $k => $param){
    		 
    		if($k == $total-1){
    			$str  .= $param ;
    		}else{
    			$str  .= $param. ',';
    		}
    	}
    	return $str;
    }
    
    public function changeArrayAction($params){
    	$array_change = array();
    	
    	foreach ($params as $param){
    		$array_change[]	= $param;
    	}
    	
    	return $array_change;
    }
    
    public function notificationMassUploadSaveNewAction()
    {
    	require_once 'manage'.DIRECTORY_SEPARATOR.'notification'.DIRECTORY_SEPARATOR.'mass-upload-save-new.php';
    }
    public function deleteleaderregionAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'leader'.DIRECTORY_SEPARATOR.'delete-sales-region.php';
    }
	
    public function listGuideAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'list-guide.php';
    }
    public function listPolicyAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'list-policy.php';
    }
    public function listOrganizationStructureAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'list-organization-structure.php';
    }
    public function listBrandAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'list-brand.php';
    }
    public function listInfoAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'list-info.php';
    }
	 public function listCoreValueAction()
    {
        require_once 'manage'.DIRECTORY_SEPARATOR.'inform'.DIRECTORY_SEPARATOR.'list-core-value.php';
    }
    
    
}