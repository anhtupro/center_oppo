<?php

class UserInformationController extends My_Controller_Action
{
    public function init()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if($userStorage->id == null)
        {
            $this->_redirect("/user/login");
        }
    }

    public function indexAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        // lấy thông tin user đăng nhập
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QDashboard  = new Application_Model_Dashboard();
        $group_id    = $userStorage->group_id;
        $user_id     = $userStorage->id;

        $QModel         = new Application_Model_Inform();
        $QStaff         = new Application_Model_Staff();
        $QTime          = new Application_Model_Time();
        $QTiming        = new Application_Model_Timing();
        $QTimeOffAdd    = new Application_Model_OffDateAdd();
        $QDepartment    = new Application_Model_Department();
        $QContractTerm  = new Application_Model_ContractTerm();
        $QOffHistory    = new Application_Model_OffHistory();

        $StaffRowSet = $QStaff->find($user_id);
        $staff = $StaffRowSet->current();


        $QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $month = date('m');
        $year  = date('Y');

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $QWard   = new Application_Model_Ward();
        $this->view->all_province_cache = $QRegionalMarket->get_cache();
        $this->view->all_district_cache = $QRegionalMarket->get_district_cache();
        $QProvince             = new Application_Model_Province();
        $provinces             = $QProvince->get_all2();
        $this->view->provinces = $provinces;
        $district_cache             = $QRegionalMarket->get_district_cache();
        $this->view->district_cache = $district_cache;

        // get  user's ID Card Address (original information)
        $QStaffAddress = new Application_Model_StaffAddress();
        $where[]                     = $QStaffAddress->getAdapter()->quoteInto('staff_id = ?', $user_id);
        $where[]                     = $QStaffAddress->getAdapter()->quoteInto('address_type = ?', My_Staff_Address::ID_Card);
        $id_card_address             = $QStaffAddress->fetchRow($where);
        $this->view->id_card_address = $id_card_address;


        if (isset($id_card_address['district']) && isset($district_cache[$id_card_address['district']])) {
            $this->view->id_card_address_districts = $QRegionalMarket->
            get_district_by_province_cache($district_cache[$id_card_address['district']]['parent']);

            //danh s�ch ward theo district
            $list_id_card_ward             = $QWard->getWardByDistrict($id_card_address['district']);
            $this->view->list_id_card_ward = $list_id_card_ward;


        }

        //get area & province
        $rowset = $QRegionalMarket->find($staff->regional_market);

        if ($rowset)
        {
            $this->view->regional_market = $regional_market = $rowset->current();
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $regional_market['area_id']);

            $this->view->regional_markets = $QRegionalMarket->fetchAll($where);

            $rowset = $QArea->find($regional_market['area_id']);
            $this->view->area = $rowset->current();
        }

        $day = 0;
        $params = array('user_id' => $user_id, 'month' => $month , 'year' => $year);

        if (My_Staff::isPgTitle($staff['title']))
        {
            $day = $QTime->getDayDashboard($params);
        } else
        {
            $day = $QTime->getDayDashboard($params);
        }

        $this->view->contract_term = $QContractTerm->get_cache();
        $department = $QDepartment->get_cache();
        $params = array(
            'staff_id' => $userStorage->id,
            'filter'   => true,
            'status'   => 1,
        );

        // Lấy danh sách các shop đã đứng của nhân viên
        $store_list = My_Staff::getOwnStoresUserInformation($userStorage->id);
        $store_list_array = array();

        foreach ($store_list as $k => $v)
        {
            $store_list_array[$k]['store_name'] = $v['store_name'];
            $store_list_array[$k]['from'] = $v['from'];
            $store_list_array[$k]['to'] = $v['to'];
        }


        $params['group_cat'] = 1;
        $params['filter_display'] = 1;
        $page = 1;
        $limit = 5;
        $total = 0;
        $inform = $QModel->fetchPagination($page, $limit, $total, $params);

        $params = array('name' => $user_id);

        $off = $QOffHistory->fetchPagination($page, $limit, $total, $params);


        if (isset($off) and $off)
        {
            $this->view->off = $off;
        }

        // show button 'cap nhat' if status is not pending or reject or have not update yet
        $QStaffTempNew = new Application_Model_StaffTempNew();
        $edited_staff = $QStaffTempNew->is_exist($user_id);

        if (! $edited_staff) {
            $this->view->can_update = 1;
        }

        $ID_card_infor = $QStaff->getIDCardInfor($user_id);

        $this->view->ID_card_infor = $ID_card_infor;
        $this->view->store_list    = $store_list_array;
        // $this->view->sellout       = $sell_out;
        // $this->view->sabbatical    = $sabbatical;
        // $this->view->day           = $day;
        // $this->view->department    = $department;
        $this->view->staff         = $staff;
        // $this->view->informs       = $inform;
        $QModel                    = new Application_Model_Nationality();
        $this->view->nationalities = $QModel->fetchAll();
        $QModel                    = new Application_Model_Religion();
        $this->view->religions     = $QModel->fetchAll();
        //var_dump($QModel->fetchAll());die;
        $this->_helper->viewRenderer('user-information/information', null, true);
        $this->view->back_url = $this->getRequest()->getServer('HTTP_REFERER') ? $this->
        getRequest()->getServer('HTTP_REFERER') : HOST;

        $staff_contract_model = new Application_Model_StaffContract();
        /*
		$params_contract = array(
            'office' => -1,
            'code' => '1212KI01'
            );

        $history             = $staff_contract_model->history($page, $limit, $total, $params_contract);
        */
		$staff_code =  $userStorage->code;
		 $db                 = Zend_Registry::get('db');
    $sql  = "SELECT 
	SQL_CALC_FOUND_ROWS c.*
	,concat(s.firstname,' ',s.lastname) as full_name,s.code,s.status as staff_status
	,e.name as title, f.name as team, g.name as department,cn.name as company_name,ar.name as `area`,rm.name as tinh,rm2.name as quan_huyen,h.name as hospital,ss.insurance_salary,ss.insurance_salary salary_insurance_temp,ss.work_cost_salary

	from staff_contract c
	join staff s on c.staff_id=s.id
	JOIN company cn ON c.company_id=cn.id
	INNER JOIN team e ON e.id = c.title
	INNER JOIN team f ON f.id = e.parent_id
	INNER JOIN team g ON g.id = f.parent_id
	INNER JOIN regional_market rm ON rm.id = c.regional_market
	left join regional_market rm2 ON rm2.id = c.district_id
	INNER JOIN `area` ar ON ar.id = rm.area_id
	LEFT JOIN hospital h on h.id= c.hospital_id
	LEFT JOIN staff_salary ss ON ss.id= c.salary_id
	where 

	  c.print_status = 1 and c.status=2 and (c.is_next in (0,2) or is_next is null) and c.is_expired=0
AND s.`code`  = '$staff_code' ORDER BY c.`from_date` DESC
 ";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $history   = $stmt->fetchAll();
    $stmt->closeCursor();
	
        $this->view->history = $history;  
        $this->view->id = $userStorage->id;

        $QContractType             = new Application_Model_ContractTypes();
        $QContractType             = $QContractType->get_cache();
        $this->view->contractTerms = $QContractType;

        $flashMessenger             = $this->_helper->flashMessenger;
        $messages_error             = $flashMessenger->setNamespace('error')->getMessages();
        $messages                   = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages       = $messages;
        $this->view->messages_error = $messages_error;

    }

    public function uploadphotoAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $flashMessenger             = $this->_helper->flashMessenger;

        // check file type varible

        if($this->getRequest()->isPost() && (!empty($this->getRequest()->getPost('save-photo')) || !empty($this->getRequest()->getPost('save-id-photo')) || !empty($this->getRequest()->getPost('save-id-photo-back') || !empty($this->getRequest()->getPost('save-all-photo')) ) ))
        {
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'staff' . DIRECTORY_SEPARATOR  . DIRECTORY_SEPARATOR . Zend_Auth::getInstance()->getStorage()->read()->id ;

            $arrPhoto = array(
                'photo' => $uploaded_dir,
                'offdate_file' => $uploaded_dir,
                'id_photo' => $uploaded_dir.DIRECTORY_SEPARATOR.'ID_Front',
                'id_photo_back' => $uploaded_dir.DIRECTORY_SEPARATOR.'ID_Back',
            );

            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();

            $upload->addValidator('Extension', false, 'jpg,jpeg,png,gif');
            $upload->addValidator('Size', false, array('max' => '2MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');

            $hasPhoto = false;
            if(!empty($this->getRequest()->getPost('save-photo')))
            {
                $file_type = 'photo';
            }

            if(!empty($this->getRequest()->getPost('save-id-photo')))
            {
                $file_type = 'id_photo';
            }

            if(!empty($this->getRequest()->getPost('save-id-photo-back')))
            {
                $file_type = 'id_photo_back';
            }
            $have_not_file = 0;
            foreach ($files as $file => $info) {
                // if($file == $file_type)
                // {
                    if($file == 'photo')
                    {
                        $name_file = "Ảnh nhân viên";
                    }

                    if($file == 'id_photo')
                    {
                        $name_file = "Ảnh CMND mặt trước";
                    }

                    if($file == 'id_photo_back')
                    {
                        $name_file = "Ảnh CMND mặt sau";
                    }

                    if (!$upload->isUploaded($file)) {
                        // $flashMessenger->setNamespace('error')->addMessage($name_file . ': Không có ảnh được upload');
                        continue;
                    }
                    else
                    {
                        $have_not_file = 1;
                    }
                    if (!$upload->isValid($file)) {
                        $array_error = $upload->getMessages();
                        if(isset($array_error['fileSizeTooBig']))
                        {
                            $flashMessenger->setNamespace('error')->addMessage($name_file . ': File có dung lượng vượt quá 2MB');
                        }
                        if(isset($array_error['fileExtensionFalse']))
                        {
                            $flashMessenger->setNamespace('error')->addMessage($name_file .': File không đúng định dạng');
                        }
                        $hasPhoto = false;
                        break;;
                    }
                    $hasPhoto = true;
                // }
            }
            if($have_not_file == 0)
            {
                $hasPhoto = false;
                $flashMessenger->setNamespace('error')->addMessage('Không có ảnh nào được upload');
            }
            if($hasPhoto == true)
            {
                foreach ($arrPhoto as $key=>$val)
                {
                    // if($key == $file_type)
                    // {
                        $fileInfo = (isset($files[$key]) and $files[$key]) ? $files[$key] : null;
                        
                        if (isset($fileInfo['name']) and $fileInfo['name']) {
                            
                            if (!is_dir($val))
                                @mkdir($val, 0777, true);

                            $upload->setDestination($val);
                            $old_name = $fileInfo['name'];

                            $tExplode = explode('.', $old_name);
                            $extension = strtolower(end($tExplode));

                            $new_name = 'UPLOAD-' . md5(uniqid('', true)) . '.' . $extension;
                            
                            $upload->addFilter('Rename', array('target' => $val .
                                DIRECTORY_SEPARATOR . $new_name));
                            $r = $upload->receive(array($key));

                            if($fileInfo['size'] > 100000)
                            {
                                $image = new My_SimpleImage();
                                $image->load($val .DIRECTORY_SEPARATOR . $new_name);
                                $image->save($val .DIRECTORY_SEPARATOR . $new_name,'',50);
                            }
                                
                            if ($r)
                            {
                                $data[$key] = $new_name;
                                $data['is_update_'.$key] = 1;
                                // upload db
                                
                                $user_id = Zend_Auth::getInstance()->getStorage()->read()->id;
                                $QStaff = new Application_Model_Staff();

                                $StaffRowSet = $QStaff->find($user_id);
                                $auth = $StaffRowSet->current();
                                
                                $params = array();
                                $params['staff_id'] = $auth->id;
                                $params['new_name'] = $new_name;
                                $res = 0;
                                if($key == 'photo')
                                {
                                    $params['old_name'] = $auth->photo;
                                    $res = $QStaff->updatePhoto($params);
                                    $name_type = "Ảnh nhân viên";
                                }

                                if($key == 'id_photo')
                                {
                                    $params['old_name'] = $auth->id_photo;
                                    $res = $QStaff->updateIdPhoto($params);
                                    $name_type = "Ảnh CMND mặt trước";
                                }

                                if($key == 'id_photo_back')
                                {
                                    $params['old_name'] = $auth->id_photo_back;
                                    $res = $QStaff->updateIdPhotoBack($params);
                                    $name_type = "Ảnh CMND mặt sau";
                                }
                                
                                if($res)
                                {
                                    $flashMessenger->setNamespace('success')->addMessage('Cập nhật ' . $name_type .' thành công');
                                }
                                else
                                {
                                    $flashMessenger->setNamespace('error')->addMessage('Cập nhật ' . $name_type . ' không thành công');
                                }
                            }else{
                                $messages = $upload->getMessages();
                                
                                foreach ($messages as $msg)
                                    throw new Exception($msg);
                            }
                        }
                    // }
                }
            }
                
        }

        if($this->getRequest()->isPost() && (!empty($this->getRequest()->getPost('delete-photo')) || !empty($this->getRequest()->getPost('delete-id-photo')) || !empty($this->getRequest()->getPost('delete-id-photo-back')) ) )
        {
            $user_id = Zend_Auth::getInstance()->getStorage()->read()->id;
            $QStaff = new Application_Model_Staff();

            $StaffRowSet = $QStaff->find($user_id);
            $auth = $StaffRowSet->current();
            
            $params = array();

            if(!empty($this->getRequest()->getPost('delete-photo')))
            {
                $file_type = 'photo';
                $params['old_name'] = $auth->photo;
            }

            if(!empty($this->getRequest()->getPost('delete-id-photo')))
            {
                $file_type = 'id_photo';
                $params['old_name'] = $auth->id_photo;
            }

            if(!empty($this->getRequest()->getPost('delete-id-photo-back')))
            {
                $file_type = 'id_photo_back';
                $params['old_name'] = $auth->id_photo_back;
            }

            
            $params['staff_id'] = $auth->id;
            $params['new_name'] = '';
            $res = 0;
            if($file_type == 'photo')
            {
                $name_type = "Ảnh nhân viên";
                $res = $QStaff->updatePhoto($params);
            }

            if($file_type == 'id_photo')
            {
                $name_type = "Ảnh CMND mặt trước";
                $res = $QStaff->updateIdPhoto($params);
            }

            if($file_type == 'id_photo_back')
            {
                $name_type = "Ảnh CMND mặt sau";
                $res = $QStaff->updateIdPhotoBack($params);
            }

            if($res)
            {
                $flashMessenger->setNamespace('success')->addMessage('Xóa ' . $name_type . ' thành công');
            }
            else
            {
                $flashMessenger->setNamespace('error')->addMessage('Xóa ' . $name_type .' không thành công');
            }
        }


        // pending when uploading picture in staff_temp
         $db = Zend_Registry::get('db');
         $db->beginTransaction();
         $userStorage = Zend_Auth::getInstance()->getStorage()->read();
         $staff_id = $userStorage->id;

         $select = $db->select()
                      ->from(array('st' => 'staff'), array('st.*'))
                      ->where('st.id = ?', $staff_id);
         $user = $db->fetchRow($select);

         $department = $user['department'];
         $regional_market= $user['regional_market'];
         $fistname = $user['firstname'];
         $lastname = $user['lastname'];
         $code = $user['code'];
         $title = $user['title'];
         $phone_number = $user['phone_number'];
         $team = $user['team'];
         $joined_at = $user['joined_at'];
                  
		/*
         $QStaffTemp = new Application_Model_StaffTemp();
         $data = array(
            'staff_id' => $staff_id,
            'is_approved' => 0,
            'department' => $department,
            'title' => $title,
            'regional_market' => $regional_market,
            'firstname' => $firstname,
            'lastname' =>$lastname,
            'code' => $code,
            'phone_number' => $phone_number,
            'team' =>$team,
            'joined_at' => $joined_at
         );
         $QStaffTemp->insert($data);   
		*/
         $db->commit();
        ///
        $this->_redirect('/user-information/index');
    }


    public function saveChangeInforAction()
    {
        require_once 'user-information' . DIRECTORY_SEPARATOR . 'save-change-infor.php';
    }


}
