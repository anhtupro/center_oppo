<?php
class CultureGameController extends My_Controller_Action
{

	public function indexAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'index.php';
	}
	public function stationAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station.php';
	}
	public function stationPlayAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-play.php';
	}
	public function stationFinishAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-finish.php';
	}

	public function submitAnswerRoundFirstAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'submit-answer-round-first.php';
	}
	public function stationResultAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-result.php';
	}
    public function stationRewardAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-reward.php';
    }

	public function stationPlaySecondAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-play-second.php';
	}
	public function submitAnswerRoundSecondAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'submit-answer-round-second.php';
	}
	public function submitShareFbAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'submit-share-fb.php';
	}

	public function stationPlayThirdAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-play-third.php';
	}
	public function submitAnswerRoundThirdAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'submit-answer-round-third.php';
	}

    public function stationPlayFourAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-play-four.php';
    }
    public function submitAnswerRoundFourAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'submit-answer-round-four.php';
    }
    public function iframeStationFourAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'iframe-station-four.php';
    }


	public function rotationAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'rotation.php';
	}
    public function stationLastAction(){
        include 'culture-game'.DIRECTORY_SEPARATOR.'station-last.php';
    }


	protected function getNewRotationCulture() {
        $QMiniGameRewardLog = new Application_Model_MiniGameRewardLog();
        $userStorage  = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id= $userStorage->id;
        $reward = array(
            "id"      => "21",
            "name"    => "Chúc may mắn lần sau!!!",
            "percent" => 100,
            "limit"   => -1
        );
        $current_time = date('Y-m-d H:i:s');
        //$win_percent = 50; //% trúng thưởng


        //PGPB
        if(in_array($userStorage->title, [182,375,419,417,554,577,623])){
            $win_percent = 50; //PGPB
        }elseif(in_array($userStorage->team, [141])){
            $win_percent = 40; // CALL CENTER
        }elseif(in_array($userStorage->team, [146])){
            $win_percent = 30; // SERVICE
        }else{
            $win_percent = 60; // Other
        }
        //$win_percent = 25;

        // if( $current_time >=  date('Y-m-d H:i:s', strtotime(''.MINIGAME_FROM_TIME.' + 10 minutes'))){
        //     $win_percent = 15;
        // }elseif($current_time >=  date('Y-m-d H:i:s', strtotime(''.MINIGAME_FROM_TIME.' + 20 minutes'))){
        //     $win_percent = 10;  
        // }elseif($current_time >=  date('Y-m-d H:i:s', strtotime(''.MINIGAME_FROM_TIME.' + 30 minutes'))){
        //     $win_percent = 50;  
        // }

        
        
        $is_win = (rand(0, 100) <= $win_percent); //check trúng hay ko

        if($is_win)
        {

            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            try {
                $mini_game_reward = new Application_Model_MiniGameReward();

                $params = [
                    'round' => CULTURE_GAME_CURRENT_ROUND
                ];

                $lst_remaining_rewards = $mini_game_reward->getRemainingItems($params);
               
                $lst_can_receive_reward = array();
                foreach ($lst_remaining_rewards as $key => $val)
                {
                    if($val['remain'] > 0)
                    {
                        $this->addReward($lst_can_receive_reward, $val);
                       
                    }
                }

                $total_remaining_reward = count($lst_can_receive_reward);
                
                if($total_remaining_reward > 0)
                    $reward = $lst_can_receive_reward[rand(0, $total_remaining_reward - 1)];

               

                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $reward = array(
                    "id"      => "21",
                    "name"    => "Chúc may mắn lần sau.",
                    "percent" => 100,
                    "limit"   => -1
                );
            }
        }


        // return $reward;
        $data_insert = array(
            'staff_id' => $staff_id,
            'reward' => $reward['id'],
            'round' => CULTURE_GAME_CURRENT_ROUND
        );

        $QMiniGameRewardLog->insert($data_insert);

        return $reward
;    }

    protected  function addReward(&$arr_reward, $reward) {
        array_push($arr_reward, array(
            "id" => $reward['id'],
            "name" => $reward['reward'],
            "percent" => $reward['percent'],
            "limit" => $reward['limit']
        ));
    }


    public function testLanguageAction(){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $current_lang = $userStorage->defaut_language;
        $trans = Zend_Registry::get('translate');
        $list_lang = My_Lang::getLangList();
        echo $trans->translate('searchbox_search',$list_lang[$current_lang]); exit;
    }
	
}