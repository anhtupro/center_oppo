<?php

class TimingController extends My_Controller_Action
{
    function init(){
        set_time_limit(0);
        ini_set('memory_limit', -1);
    }

    public function indexAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'index.php';
    }

    public function expiredAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'expired.php';
    }

    public function kpiAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'kpi.php';
    }

    public function expiredSubmitAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'expired-submit.php';
    }

    public function editAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'edit.php';
    }

    public function saveHappytimeAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'save-happytime.php';
    }

    public function saveAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'save.php';
    }

    public function createAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'create.php';
    }

     public function createHappytimeAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'create-happytime.php';
    }

    public function uploadAction()
    {

        require_once 'timing' . DIRECTORY_SEPARATOR . 'upload.php';

    }

    public function analyticsAreaAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-area.php';

    }

    public function analyticsAreaProvinceAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-area-province.php';

    }

    public function analyticsProvinceAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-province.php';

    }

    public function imeiAction()
    {

    }

    public function duplicatedImeiAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'duplicated-imei.php';
    }

    public function imeiReportAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'imei-report.php';
    }

    // iFrame Function
    public function imeiReportConfirmAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'imei-report-confirm.php';
    }

    public function viewAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'view.php';
    }

    public function delAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'del.php';
    }

    public function approveAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'approve.php';
    }
    
    public function approveAccessoriesAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'approve-accessories.php';
    }

    public function scheduleAction()
    {

    }

    public function eventsAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'events.php';
    }

    public function loadModelAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'load-model.php';
    }

    public function analyticsAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics.php';
    }

    public function analyticsStoreAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-store.php';
    }

    public function analyticsProductAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-product.php';
    }

    public function shortReportAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'short-report.php';
    }

    public function storeShortReportAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'store-short-report.php';
    }

    public function updateNoteAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'update-note.php';
    }

    // AJAX function
    public function checkImeiAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'check-imei.php';
    }

    // AJAX function
    public function checkImeiHappytimeAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'check-imei-happytime.php';
    }

    public function checkImeiExportAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'check-imei-export.php';
    }

    public function getImeiCustomerAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'get-imei-customer.php';
    }

    public function analyticsDealerAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-dealer.php';
    }

    public function analyticsLeaderAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-leader.php';
    }

    public function analyticsDealerAllAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-dealer-all.php';
    }

    public function analyticsDealerDetailsAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-dealer-details.php';
    }

    public function analyticsInventoryByDealerAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR .
            'analytics-inventory-by-dealer.php';
    }

    public function analyticsInventoryByDealerDetailAction(){
        require_once 'timing'.DIRECTORY_SEPARATOR.'analytics-inventory-by-dealer-detail.php';
    }

    public function manageNotActivatedAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'manage-not-activated.php';
    }

    public function kpiOverviewAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'kpi-overview.php';
    }

    public function timingOverviewAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'timing-overview.php';
    }

    public function kpiReviewAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'kpi-review.php';
    }

    public function kpiHrCheckAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'kpi-hr-check.php';
    }

    public function kpiAsmCheckAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'kpi-asm-check.php';
    }

    public function analyticsKaAction()
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'analytics-ka.php';
    }

    private function checkImei($imei, $timing_sales_id, &$info, $tool = null, $reimport = false)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userStorage || !isset($userStorage->id) || !$userStorage->id){
            return 9999;
        }

        $db = Zend_Registry::get('db');
        $sql = "CALL sp_check_imei(?, ?, ?, ?, ?, @result, @_ts_id, @_firstname, @_lastname, @_email, @_store, @_timing_from, @_timing_staff_id, @_active_date, @_good_id, @_color_id)";
        $result = $db->query($sql, array($imei, $userStorage->id, date('Y-m-d'), $timing_sales_id === null ? 0 : $timing_sales_id, IMEI_ACTIVATION_EXPIRE));

        $sql = "SELECT @result, @_ts_id, @_firstname, @_lastname, @_email, @_store, @_timing_from, @_timing_staff_id, @_active_date, @_good_id, @_color_id";
        $result = $db->query($sql);
        $result = $result->fetch();

        $info = array(
            'store'           => $result['@_store'],
            'timing_sales_id' => $result['@_ts_id'],
            'staff_id'        => $result['@_timing_staff_id'],
            'date'            => $result['@_timing_from'],
            'activated_at'    => $result['@_active_date'],
            'good_id'         => $result['@_good_id'],
            'color_id'        => $result['@_color_id'],
        );

        if (isset($result['@_firstname']) && isset($result['@_lastname'])) {
            $info['staff'] = $result['@_firstname'] . ' ' . $result['@_lastname'] . ' | '
                . (isset($result['@_email']) && !empty($result['@_email'])
                    ? str_replace(EMAIL_SUFFIX, '', $result['@_email']) : '(Đã nghỉ)');
        }

        return isset($result['@result']) ? $result['@result'] : 9999;
    }

    private function checkImei30DaysActivated($imei)
    {
        if (defined("NOT_CHECK_IMEI_ACTIVATED_7_DAYS")
            && 1 == NOT_CHECK_IMEI_ACTIVATED_7_DAYS
            && in_array($imei, array(
            '356120041053097',
            '866892026839871',
            '865804027175008',
            '866893024555915',
            '866893024553597',
            '865884024224132',
            '358916026050331',
            '865884024200165',
            '867287026245675',
            '866423020995012',
            '865884024224066',
            '867287026463559',
            '865425020567783',
        ))) return 0;

        $db = Zend_Registry::get('db');
        $db->getProfiler()->setEnabled(true);
        //Check trong bảng acti
        $QImeiActivation = new Application_Model_ImeiActivation();
        $where = array();
        $where[] = $QImeiActivation->getAdapter()->quoteInto('imei_sn = ?', $imei);
        $tmp_date = date_sub(date_create(), new DateInterval('P' .
            IMEI_ACTIVATION_EXPIRE . 'D'))->format('Y-m-d 00:00:00');
        $where[] = $QImeiActivation->getAdapter()->quoteInto('activated_at < ?', $tmp_date);
        $imei_activation = $QImeiActivation->fetchRow($where);

        if ($imei_activation)
        { // IMEI đã acti hơn IMEI_ACTIVATION_EXPIRE ngày
            return 1;
        }

        return 0;
        //
    }

    private function getCustomerInfo($imei = '', &$customer)
    {
        $QTimingSale = new Application_Model_TimingSale();
        $where = $QTimingSale->getAdapter()->quoteInto('imei=?', $imei);
        $timing_sales = $QTimingSale->fetchRow($where);

        if ($timing_sales)
        {
            $QCustomer = new Application_Model_Customer();
            $where = $QCustomer->getAdapter()->quoteInto('id = ?', $timing_sales->
                customer_id);

            $customer = $QCustomer->fetchRow($where);

            if ($customer)
                return $customer->toArray();
        }

        return false;
    }

    private function _exportExcel($data)
    {

        set_time_limit(0);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'Name',
            'Title',
            'Province',
            'Area',
            'Phone Number',
            'Sales',
            );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key)
        {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        foreach ($data as $item)
        {
            $alpha = 'A';
            $sheet->setCellValue($alpha++ . $index, $item['firstname'] . ' ' . $item['lastname']);
            $sheet->setCellValue($alpha++ . $index, $item['title']);
            $sheet->setCellValue($alpha++ . $index, $item['regional_market']);
            $sheet->setCellValue($alpha++ . $index, $item['area']);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['phone_number'],
                PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, !empty($item['total']) ? $item['total'] :
                '0');
            $index++;
        }

        $filename = 'TIMING_REPORT_' . date('d/m/Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    private function count_product_per_staff($staff_ids = array())
    {
        $db = Zend_Registry::get('db');

        $start = date('Y-m-01 00:00:00');
        $end = date('Y-m-' . date('t') . ' 23:59:59');

        $select = $db->select()->from(array('t' => 'timing'), array(
            't.staff_id',
            't.from',
            't.approved_at'))->join(array('s' => 'timing_sale'),
            's.timing_id = t.id AND t.approved_at IS NOT NULL', array('product_count' =>
                'COUNT(s.product_id)'))->group('t.staff_id');

        $select->where('t.from >= ?', $start);
        $select->where('t.from <= ?', $end);

        if (is_array($staff_ids) and $staff_ids)
            $select->where('staff_id IN (?)', $staff_ids);

        $result = $db->fetchAll($select);

        $staff = array();
        foreach ($result as $value)
        {
            $staff[$value['staff_id']] = $value['product_count'];
        }

        return $staff;
    }

    private function _exportExcel_timing_by_month($params)
    {
        require_once 'timing' . DIRECTORY_SEPARATOR . 'export_excel_time_by_month.php';
    }


    private static function cmp($a, $b)
    {
        if ($a['point'] == $b['point'])
        {
            return 0;
        }
        return ($a['point'] < $b['point']) ? 1 : -1;
    }

    private function num2alpha($n)
    {
        for ($r = ""; $n >= 0; $n = intval($n / 26) - 1)
            $r = chr($n % 26 + 0x41) . $r;
        return $r;
    }

    public function imeiActivatedAction(){

        $from = $this->getRequest()->getParam('from',date('01/m/Y'));
        $to = $this->getRequest()->getParam('to',date('d/m/Y'));
        $sort = $this->getRequest()->getParam('sort','good_desc');
        $desc  = $this->getRequest()->getParam('desc',1);
        $good_id = $this->getRequest()->getParam('good_id');
        $params = array(
            'from'    => $from,
            'to'      => $to,
            'good_id' => $good_id,
            'sort'    => $sort,
            'desc'    => $desc,

        );
        $this->view->params = $params;
        $db = Zend_Registry::get('db');

        $selectGood = $db->select()
            ->from(array('p'=>WAREHOUSE_DB.'.good'),array('id','desc'))
            ->where('p.cat_id = ?',11)
            ->order('desc')
            ;
        $goods = $db->fetchPairs($selectGood);
        $this->view->goods = $goods;

        if($from AND $to){
            $tmpFrom = explode('/', $from);
            $tmpFrom = $tmpFrom[2].'-'.$tmpFrom[1].'-'.$tmpFrom[0];
            $tmpTo   = explode('/', $to);
            $tmpTo   = $tmpTo[2].'-'.$tmpTo[1].'-'.$tmpTo[0].' 23:59:59';
            /*
            $cols = array(
                'total' => 'COUNT(DISTINCT imei_sn)'
            );
            $select = $db->select()
                ->from(array('p'=>WAREHOUSE_DB.'.imei'),$cols)
                ->where('p.activated_date >= ?',$tmpFrom)
                ->where('p.activated_date <= ?',$tmpTo)
                ;
            $total = $db->fetchOne($select);
            $this->view->total = $total;
            */
            $cols2 = array(
                    'total' => 'COUNT(DISTINCT p.imei_sn)',
                    'good_name' => 'g.name',
                    'good_desc' => 'g.desc'
                );
            $selectGoodActivated = $db->select()
                ->from(array('p'=>WAREHOUSE_DB.'.imei'),$cols2)
                ->join(array('g'=>WAREHOUSE_DB.'.good'),'g.id = p.good_id',array())
                ->where('p.activated_date >= ?',$tmpFrom)
                ->where('p.activated_date <= ?',$tmpTo)
                ->where('p.activated_date IS NOT NULL')
                ->where('p.activated_date <> 0')
                ->group('g.id')
                ;

            if($good_id) {
                $selectGoodActivated->where('g.id = ?',$good_id);
            }
            if($sort){
                    if($desc == 1){
                        $strOrder = $sort.' DESC';
                    }else{
                        $strOrder = $sort.' ASC';
                    }
                    $selectGoodActivated->order($strOrder);
            }

            $list = $db->fetchAll($selectGoodActivated);
            $total = 0;
            if($list){
                foreach($list as $key => $value ):
                    $total += $value['total'];
                endforeach;
            }

            $this->view->total = $total;
            $this->view->list = $list;
            $this->view->sort = $sort;
            $this->view->desc = $desc;
            $this->view->url = HOST . 'timing/imei-activated/' . ($params ? '?' .
            http_build_query($params) . '&' : '?');
        }

    }

    public function showModelAction(){
        require_once 'timing' . DIRECTORY_SEPARATOR . 'show-model.php';
    }

    public function showModelKaAction(){
        require_once 'timing' . DIRECTORY_SEPARATOR . 'show-model-ka.php';
    }
    
    public function reportSellInAction(){
        require_once 'timing' . DIRECTORY_SEPARATOR . 'report-sell-in.php';
    }
    
    public function showAccAction(){
        require_once 'timing' . DIRECTORY_SEPARATOR . 'show-acc.php';
    }

    public function exportImeiSellout($imeis){
        require_once 'timing' . DIRECTORY_SEPARATOR . 'export-imei-sellout.php';
    }
 
    public function reapprovalAction()
    {

    }

    public function listreapprovalAction()
    {
        $params_ = array_merge(
            array("page" => 1, 'limit' => 10),
            $this->_request->getParams()
        );
        $params_['offset'] = ($params_['page']-1)*10;
        // var_dump($params_); die;
        $limit = 10;
        $timing_reapproval_model = new Application_Model_TimingReapproval();
        $data = $timing_reapproval_model->_select($params_);
        // $total = $timing_reapproval_model->_select_total_rows($params_);
        $this->view->limit = $limit;
        $this->view->total = $data['total'];
        
        unset($params_['page']);
        $this->view->url = HOST.'timing/listreapproval'.( $params_ ? '?'.http_build_query($params_).'&' : '?' );
        $this->view->offset = $limit*($page-1);
        $this->view->data = $data['data'];
    }

    public function _exportAreaKa($data, $data_dealer){

        ini_set("memory_limit", -1);
        ini_set("display_error", 0);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            '#',
            'Area',
            'Unit',
            'Unit Activated',
            'Value (80%)',        

            'Dealer (TGDĐ) Unit',
            'Dealer Unit (TGDĐ) Activated', 
            'Dealer Value (TGDĐ) (80%)',

            'Other Unit',
            'Other Unit Activated', 
            'Other Value (80%)',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;


        $i = 1;

        foreach($data as $item){

            $dealer = !empty($data_dealer[$item['area_id']]) ? $data_dealer[$item['area_id']] : null;

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['total_quantity']);
            $sheet->setCellValue($alpha++.$index, $item['total_activated']);
            $sheet->setCellValue($alpha++.$index, $item['total_value']);

            $sheet->setCellValue($alpha++.$index, $dealer['total_quantity']);
            $sheet->setCellValue($alpha++.$index, $dealer['total_activated']);
            $sheet->setCellValue($alpha++.$index, $dealer['total_value']);

            $sheet->setCellValue($alpha++.$index, $item['total_quantity'] - $dealer['total_quantity']);
            $sheet->setCellValue($alpha++.$index, $item['total_activated'] - $dealer['total_activated']);
            $sheet->setCellValue($alpha++.$index, $item['total_value'] - $dealer['total_value']);

            $index++;

        }

        $filename = 'Area_Dealer_'.date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');

        $objWriter->save('php://output');

        exit;
    }

    public function reportLeaderAction(){
        require_once 'timing' . DIRECTORY_SEPARATOR . 'report-leader.php';
    }
}

