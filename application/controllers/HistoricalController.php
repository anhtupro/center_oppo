<?php
class HistoricalController extends My_Controller_Action
{
    public function sellinDeductionAction()
    {
        $name            = $this->getRequest()->getParam('name');
        $loyalty_plan_id = $this->getRequest()->getParam('loyalty_plan_id');
        $from            = $this->getRequest()->getParam('from', date('m/Y'));
        $to              = $this->getRequest()->getParam('to', date('m/Y'));
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $export          = $this->getRequest()->getParam('export', 0);
        $page            = $this->getRequest()->getParam('page', 1);
        $limit           = LIMITATION;
        $total           = 0;
        $params = array(
            'name'            => $name,
            'loyalty_plan_id' => $loyalty_plan_id,
            'from'            => $from,
            'to'              => $to,
            'area'            => $area,
            'regional_market' => $regional_market,
            'export'          => $export,
        );

        $QLoyaltyPlan = new Application_Model_LoyaltyPlan();
        $this->view->level_cache = $QLoyaltyPlan->get_all_cache();

        $QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $QRegion = new Application_Model_RegionalMarket();

        if ($area) {
            if (is_array($area) && count($area))
                $where = $QRegion->getAdapter()->quoteInto('area_id IN (?)', $area);
            elseif (is_numeric($area) && $area)
                $where = $QRegion->getAdapter()->quoteInto('area_id = ?', intval($area));

            $regions = $QRegion->fetchAll($where, 'name');

            $regions_arr = array();

            foreach ($regions as $key => $value)
                $regions_arr[$value['id']] = $value['name'];

            $this->view->regional_markets = $regions_arr;
        }

        $QHistoricalSellinDeduction = new Application_Model_HistoricalSellinDeduction();
        $this->view->sellin = $QHistoricalSellinDeduction->fetchPagination($page, $limit, $total, $params);
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->page   = $page;
        $this->view->offset = $limit*($page-1);
        $this->view->params = $params;
        $this->view->url    = HOST.'historical/sellin-deduction'.( $params ? '?'.http_build_query($params).'&' : '?' );
    }

    public function sellinDeductionDetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $result = array();
            $id = $this->getRequest()->getParam('id');
            if (!$id || !intval($id)) throw new Exception("Invalid ID");

            $id = intval($id);

            $QGood = new Application_Model_Good();
            $goods = $QGood->get_cache();
            $QDetail = new Application_Model_HistoricalSellinDeductionDetail();
            $where = $QDetail->getAdapter()->quoteInto('historical_sellin_deduction_id = ?', $id);
            $detail = $QDetail->fetchAll($where);

            foreach ($detail as $key => $value) {
                $result[] = array(
                    'sales_sn' => isset($value['sales_sn']) ? $value['sales_sn'] : '#',
                    'invoice_number' => isset($value['invoice_number']) ? $value['invoice_number'] : '#',
                    'invoice_sign' => isset($value['invoice_sign']) ? $value['invoice_sign'] : '#',
                    'invoice_time' => isset($value['invoice_time']) ? date('d/m/Y', strtotime($value['invoice_time'])) : '#',
                    'value'        => My_Number::f($value['value']),
                );
            }

            exit(json_encode($result));
        } catch (Exception $e) {

        }
    }

    public function incentiveAction()
    {
        $name            = $this->getRequest()->getParam('name');
        $loyalty_plan_id = $this->getRequest()->getParam('loyalty_plan_id');
        $month           = $this->getRequest()->getParam('month');
        $area            = $this->getRequest()->getParam('area');
        $regional_market = $this->getRequest()->getParam('regional_market');
        $export          = $this->getRequest()->getParam('export', 0);
        $page            = $this->getRequest()->getParam('page', 1);
        $limit           = LIMITATION;
        $total           = 0;
        $params = array(
            'name'            => $name,
            'loyalty_plan_id' => $loyalty_plan_id,
            'month'           => $month,
            'area'            => $area,
            'regional_market' => $regional_market,
            'export'          => $export,
        );

        $QLoyaltyPlan = new Application_Model_LoyaltyPlan();
        $this->view->level_cache = $QLoyaltyPlan->get_all_cache();

        $QArea = new Application_Model_Area();
        $this->view->areas = $QArea->get_cache();

        $QGood = new Application_Model_Good();
        $this->view->good_cache = $QGood->get_cache();

        $QRegion = new Application_Model_RegionalMarket();

        if ($area) {
            if (is_array($area) && count($area))
                $where = $QRegion->getAdapter()->quoteInto('area_id IN (?)', $area);
            elseif (is_numeric($area) && $area)
                $where = $QRegion->getAdapter()->quoteInto('area_id = ?', intval($area));

            $regions = $QRegion->fetchAll($where, 'name');

            $regions_arr = array();

            foreach ($regions as $key => $value)
                $regions_arr[$value['id']] = $value['name'];

            $this->view->regional_markets = $regions_arr;
        }

        $QHistoricalIncentive = new Application_Model_HistoricalIncentive();
        $this->view->sellin = $QHistoricalIncentive->fetchPagination($page, $limit, $total, $params);
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->page   = $page;
        $this->view->offset = $limit*($page-1);
        $this->view->params = $params;
        $this->view->url    = HOST.'historical/incentive'.( $params ? '?'.http_build_query($params).'&' : '?' );
    }

    public function incentiveDetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $result = array();
            $id = $this->getRequest()->getParam('id');
            if (!$id || !intval($id)) throw new Exception("Invalid ID");

            $id = intval($id);

            $QGood = new Application_Model_Good();
            $goods = $QGood->get_cache();
            $QDetail = new Application_Model_HistoricalIncentiveDetail();
            $where = $QDetail->getAdapter()->quoteInto('historical_incentive_id = ?', $id);
            $detail = $QDetail->fetchAll($where);

            foreach ($detail as $key => $value) {
                $result[] = array(
                    'product_name' => isset($goods[ $value['product_id'] ]) ? $goods[ $value['product_id'] ] : '#',
                    'quantity'     => $value['quantity'],
                    'value_unit'   => My_Number::f($value['value_unit']),
                    'total'        => My_Number::f($value['total']),
                );
            }

            exit(json_encode($result));
        } catch (Exception $e) {

        }
    }
}
