<?php
class ViewMenuController extends My_Controller_Action
{

    public function indexAction()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $group_id    = $userStorage->group_id;
        $lang = $userStorage->defaut_language;
        $id = $this->getRequest()->getParam('id');
        $actual_link = $_SERVER[REQUEST_URI];

        //lay menu trong group
        if (isset($userStorage->menu) && $userStorage->menu) {
            $groupMenu = $userStorage->menu;

        } else {
            $QMenu = new Application_Model_Menu();
            $where = $QMenu->getAdapter()->quoteInto('group_id = ? and hidden is null', $group_id);
            $menu = $QMenu->fetchAll($where, array('parent_id', 'position'));
        }


//lay menu trong staff privielege



        $QMenu2 = new Application_Model_Menu2();
        $staffMenu = $QMenu2->getMenuPermission();
//lay menu cap 2
        $db          = Zend_Registry::get('db');

        $select = $db->select()->from(array('m' => 'menu'), array(
            'id'=>'m.id'
        ));
        $select->joinLeft(array('m1' => 'menu'), 'm1.parent_id = m.id', array())
            ->where('m.parent_id =?',$id)
            ->group('m.id');

        $secondLevelMenuId = $db->fetchAll($select);
//lay ra danh sach ID menu cap 3
        $listId=array();
        foreach ($secondLevelMenuId as $Id){
            $listId[]=$Id['id'];
        }



//lay danh sach id cua menu cap 3
        $hasChildIDs=array();

        foreach ($listId as $id){
            $select = $db->select()->from('menu', array('id'))
                ->where('parent_id=?',$id);
            $result = $db->fetchAll($select);
            if(count($result)>0){
                $hasChildIDs[]=$id;
            }
        }



        $this->view->groupMenu=$groupMenu;

        $this->view->listId=$listId;
        $this->view->id=$this->getRequest()->getParam('id');
        $this->view->staffMenu=$staffMenu;
        $this->view->lang=$lang;
        $this->view->hasChildIDs=$hasChildIDs;

        if($actual_link == '/view-menu?id=467'){
       
            if(in_array($userStorage->id, array(910,16090,4266,2123))){
                $this->_redirect(HOST.'bi/brand-shop');
            }
            
            if($group_id == LEADER_ID){
                $this->_redirect(HOST.'bi/leader-details?id='.$userStorage->id);
            }
            elseif($group_id == SALES_ID || $group_id == PB_SALES_ID || $userStorage->title == SALES_TITLE || $userStorage->title == STORE_LEADER_TITLE){
                $this->_redirect(HOST.'bi/sale-details?id='.$userStorage->id);
            }
            elseif($group_id == PG_LEADER_ID){
                $this->_redirect(HOST.'bi/pgs-leader?id='.$userStorage->id);
            }
            elseif($group_id == PGPB_ID || $userStorage->title == PGPB_TITLE){
                $this->_redirect(HOST.'bi/pg-details?id='.$userStorage->id);
            }
            
            //KEY ACCOUNT LEADER -> bi/key-acount
            if($userStorage->team == KA_TEAM_ID){
                $this->_redirect(HOST.'bi/key-acount');
            }

            if($userStorage->title == 577){
                $this->_redirect(HOST . 'bi/pgs-supervisor');
            }
            
            if (in_array($userStorage->id, array(2916, 103, 765, 779, 1719, 4172, 125, 3026, 14125, 910, 3499, 4266, 6701, 2875, 8, 2123, 341)) OR in_array($userStorage->title, array(317, 175, 174, 279, 281, 178, 374, 403, SALES_TITLE, PGPB_TITLE,515,720,462))) {
                $this->_redirect(HOST . 'bi');
            }
    
            if ($userStorage->group_id == LEADER_ID) {
                $this->_redirect(HOST . 'bi/leader-details?id=' . $userStorage->id);
            }

            if($userStorage->title == 191 || $userStorage->id == 23776 ){
                $this->_redirect(HOST);               
            }
        }else if($actual_link == '/view-menu?id=91'){
            if($userStorage->title == 374 || $userStorage->id == 910 || $group_id == TRAINING_LEADER_ID || $userStorage->title == 748 || $userStorage->title == 751){
                $this->_redirect(HOST.'bi/trainer-leader-details?id='.$userStorage->id);
            } 
            elseif($group_id == TRAINING_TEAM_ID || $userStorage->title == 746 || $userStorage->title == 747 || $userStorage->title == 749 || $userStorage->title == 751){
                $this->_redirect(HOST.'bi/trainer-details?id='.$userStorage->id);
            }
        }

        if(!$_SESSION["old_layout"]){
            $this->_helper->viewRenderer->setRender('index-menu-metronic');
        }
    }

}



