<?php 
$limit = 100;
      $page = $this->getRequest()->getParam('page', 1);
      $total    = 0;

      $db = Zend_Registry::get('db');
      $select = $db->select()
                   ->from(array('spi' => 'staff_permission_inventory'), array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS spi.id'),
                      'staff_code' => 'st.code',
                      'staff_id' => 'st.id',
                        'id' => 'spi.id',
                        'name' => 'CONCAT(st.firstname, " ", st.lastname)',
                        'email' => 'st.email',
                        'area' => 'a.name'
                   ))
                   ->joinLeft(array('st' => 'staff'), 'spi.staff_id = st.id', array())
                   ->joinLeft(array('a' => 'area'), 'spi.area_id = a.id', array('area' => 'a.name'))
                   ->joinLeft(array('t' => 'team'), 'st.department = t.id', array('department' => 't.name'))
                   ->limitPage($page, $limit)
                   ->order('spi.id DESC');

      $data = $db->fetchAll($select);    
      $total = $db->fetchOne("select FOUND_ROWS()");
      
      $this->view->data = $data; 
      $this->view->total = $total;
      $this->view->limit = $limit;
      $this->view->url = HOST . 'form-print/list-permission-inventory?';