<?php
$flashMessenger   = $this->_helper->flashMessenger;
$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->view->messages_error   = $flashMessenger->setNamespace('error')->getMessages();

$QStaff           = new Application_Model_Staff();
$userStorage      = Zend_Auth::getInstance()->getStorage()->read();
$staff_id         = $userStorage->id;
$current_user     = $QStaff->findStaffid($staff_id);

$office_id = $userStorage->office_id;


$QOffice          = new Application_Model_Office();
$offices          = $QOffice->get_all();
$this->view->offices           = $offices;

$QTeam    = new Application_Model_Team();
$team = $current_user['team'];
$title = $current_user['title'];
$infoTeam  = $QTeam->find($title)->current();
$team_name = $infoTeam['name'];
$team_select = 'department';

// check if in special team
// check if in special team
if($team_name == 'ASSISTANT' && in_array($team, array(75))){

    $team_select = 'title';
}elseif( in_array($team, array(131, 133 ,147, 406 )) ){
    $team_select = 'team';
}

if(in_array($userStorage->department, array(610))){
    $team_select = 'team';
}
$this->view->depart = $QTeam->fetchRow( $QTeam->getAdapter()->quoteInto('id = ?',  $current_user[$team_select]) )->toArray()['name'];
$this->view->user = $current_user;
$this->_helper->viewRenderer->setRender('nhat-tin/create-nhat-tin');
