<?php
$export = My_Util::escape_string($this->getRequest()->getParam('export',0));
$flashMessenger = $this->_helper->flashMessenger;
$area_list = array();
$db = Zend_Registry::get('db');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;
$QAsm = new Application_Model_Asm();
$list_regions = $QAsm->get_cache($userStorage->id);
$list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
$str_area = implode("_", $list_regions);

$title = $userStorage->title;
$QTeam = new Application_Model_Team();
$team = $QTeam->find($title);
$team_info = $team->current();
$group_id = $team_info['access_group'];

$QFeedbackDcMayJune = new Application_Model_FeedbackDcMayJune();
try {
    if(!in_array($group_id, array(ADMINISTRATOR_ID)) && $userStorage->id != 4912 && !in_array($group_id, array(SALES_ADMIN_ID))){
        throw new Exception("Permission deny");
    }
    
    if (!empty($export)) {
        $list = array();
        if (in_array($group_id, array(ADMINISTRATOR_ID)) || $userStorage->id == 4912) {
            $list = $QFeedbackDcMayJune->getFeedBackDcMayJune();
        } else if (in_array($group_id, array(SALES_ADMIN_ID))) {
            $list = $QFeedbackDcMayJune->getFeedBackDcMayJune($list_regions);
        }
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        include 'PHPExcel/IOFactory.php';
        $file_name = "feedback-result-dc-5-6";
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR .$file_name. '.xlsx';
        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($path);
        $sheet = $objPHPExcel->getActiveSheet();
        $index = 3;
        
        foreach ($list as $item) {
            $alpha = 'A';
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['dealer_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['map_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['dealer_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['code_oppo'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['code_parent'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['area'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['area_map'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['target_regis'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['so_oppo_may'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['so_rm_may'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['so_oppo_june'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['so_rm_june'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($item['total'] == 0)?'-':$item['total'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit(($item['so_avg'] == 0)?'-':$item['so_avg'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['level_rate'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['result'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['discount'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['feedback'], PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        }
        if(!empty($str_area)){
            $filename = 'Upload_Feedback_dc_'.$str_area.'_d_' . date('d/m/Y');    
        }else  $filename = 'Upload_Feedback_dc_' . date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit();
    }
} catch (Exception $exc) {
    
    $this->view->messages = $exc->getMessage();
}