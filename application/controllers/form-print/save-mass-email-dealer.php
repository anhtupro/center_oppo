<?php
$this->_helper->layout->disableLayout();
$QMassUploadActiveDealer = new Application_Model_MassUploadActiveDealer();

// config for template
define('START_ROW', 2);
define('DEALER_ID', 1);
define('STORE_ID', 2);
define('NOTE', 9);

$datetime = date('Y-m-d H:i:s');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
if ($this->getRequest()->getMethod() == 'POST') {
    set_time_limit(0);
    ini_set('memory_limit', -1);
    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder = 'upload-status-dealer';
    $new_file_path = '';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 5000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );

    try {

        $file = My_File::get($save_folder, $requirement, true);
        if (!$file || !count($file))
            throw new Exception("Upload failed");

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                . DIRECTORY_SEPARATOR . $file['folder'];
                
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
        
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }
    
    include 'PHPExcel/IOFactory.php';
//  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

//  Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $progress->flush(60);
    for ($row = START_ROW; $row <= $highestRow; $row++) {
        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            if (!empty(trim($rowData[DEALER_ID])) && !empty(trim($rowData[STORE_ID]))) {
                $data = array(
                    'note' => trim($rowData[NOTE]),
                );
                
                $where[] = $QMassUploadActiveDealer->getAdapter()->quoteInto('dealer_id = ?', $rowData[DEALER_ID]);
                $where[] = $QMassUploadActiveDealer->getAdapter()->quoteInto('store_id = ?', $rowData[STORE_ID]);
                $QMassUploadActiveDealer->update($data,$where);
            }
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }
    } // END loop through order rows
    $progress->flush(100);
}