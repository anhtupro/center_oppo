<?php
$QFormPrint = new Application_Model_FormPrintNhatTin();
$flashMessenger = $this->_helper->flashMessenger;

$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id    = $userStorage->id;

if($this->getRequest()->getMethod() == "POST"){

    $save_mode          = $this->getRequest()->getParam('save_mode');

    $mail_code          = $this->getRequest()->getParam('mail_code');
    $document           = $this->getRequest()->getParam('document',0);
    $parcel              = $this->getRequest()->getParam('parcel',0);
    $time_delivery      = $this->getRequest()->getParam('time_delivery',0);
    $receiver_pay       = $this->getRequest()->getParam('receiver_pay',0);
    $sender_name        = $this->getRequest()->getParam('sender_name');
    $sender_phone       = $this->getRequest()->getParam('sender_phone');
    $sender_department  = $this->getRequest()->getParam('sender_department');
    $receiver_name      = $this->getRequest()->getParam('receiver_name');
    $receiver_address   = $this->getRequest()->getParam('receiver_address');
    $receiver_phone     = $this->getRequest()->getParam('receiver_phone');
    $content            = $this->getRequest()->getParam('content');
    $type            = $this->getRequest()->getParam('type');
    $type_pay            = $this->getRequest()->getParam('type_pay');

    $document           = $this->getRequest()->getParam('document',0);
    $parcel              = $this->getRequest()->getParam('parcel',0);

    $data_arr = array(
        'mail_code'         => $mail_code,
        'content'           => $content,
        'sender_name'       => $sender_name,
        'sender_phone'      => $sender_phone,
        'sender_department' => $sender_department,
        'receiver_name'     => $receiver_name,
        'receiver_address'  => $receiver_address,
        'receiver_phone'    => $receiver_phone,
        'guarantee_delivery'=> $guarantee_delivery,
        'fast_delivery'     => $fast_delivery,
        'time_delivery'     => $time_delivery,
        'receiver_pay'      => $receiver_pay,
        'created_by'        => $staff_id,
        'document'          => $document,
        'parcel'            => $parcel,
        'type'            => $type,
         'type_pay'            => $type_pay

    );
    $data_arr['created_at'] =  date('Y-m-d H:i:s');

    $save_result = true;
    $direct_print = "";
    if($save_mode == "edit-nhat-tin"){

        $id = $this->getRequest()->getParam("id");
        $current_row = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
        $diff_count = 0;
        $diff_str = '';
        $code_change = false;

        foreach ($data_arr as $key => $value) {
            if( $current_row[$key] != $value ){
                if($key == 'mail_code'){
                    $code_change = true;
                }
                $diff_count++;
                $diff_str.=$key;
            }
        }
        // 2: min change (mail_code created_at)
        if( !($code_change && $diff_count == 2) ){
            $direct_print = "&print=true";
            $data_arr['mail_code'] = "";
        }

        $id_redirect = $id;
        $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $id);
        $save_result = $QFormPrint->update($data_arr, $where);
    }else{
        $save_result = $QFormPrint->insert($data_arr);
        $direct_print = "&print=true";
        $id_redirect = $save_result;
    }

    if($save_result == false){
        $flashMessenger->setNamespace('error')->addMessage("Có lỗi xảy ra, Vui lòng nhập lại");
        $this->_redirect(HOST.'form-print');
    }
    $this->_redirect(HOST.'form-print/preview-nhat-tin?id='.$id_redirect.$direct_print);

}else{

    $flashMessenger->setNamespace('error')->addMessage("Thông tin không hợp lệ, Vui lòng nhập lại");
    $this->_redirect(HOST.'form-print/list-nhat-tin');

}