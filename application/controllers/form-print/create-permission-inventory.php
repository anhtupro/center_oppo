<?php 
$QArea = new Application_Model_Area();
    $QStaffPermissionInventory = new Application_Model_StaffPermissionInventory();

    $id = $this->getRequest()->getParam('id'); // id in staff_permission_inventory table
    $staff_id = $this->getRequest()->getParam('staff_id');
    $staff_code = $this->getRequest()->getParam('staff_code');  
  
    $where = $QStaffPermissionInventory->getAdapter()->quoteInto('id = ?', $id);
    $rowset = $QStaffPermissionInventory->fetchRow($where);

    $this->view->rowset = $rowset;
    $this->view->id = $id; 
    $this->view->staff_id = $staff_id;
    $this->view->staff_code = $staff_code;
//    $this->view->areas = $QArea->get_cache();
    $this->view->areas = $QArea->fetchAll(null, 'name')->toArray();
