<?php
$QFormPrint = new Application_Model_FormPrintNhatTin();
$flashMessenger = $this->_helper->flashMessenger;

$action = $this->getRequest()->getParam('function');
$multi_arr = $this->getRequest()->getParam('selected_item');

if($action == "delete")
{
    $success_rows = 0;

    foreach ($multi_arr as $key => $id) {
        $save_result = $QFormPrint->update(array('del'=>'1'),$QFormPrint->getAdapter()->quoteInto('id = ?', $id));
        if($save_result != 0){
            $success_rows ++;
        }
    }

    $flashMessenger->setNamespace('success')->addMessage( "Đã Xóa thành công ".$success_rows." dòng dữ liệu.");
    $this->redirect(HOST.'form-print/list-nhat-tin');

} else {

    $item_arr = Array();
    foreach ($multi_arr as $key => $id) {
        $form_item = $QFormPrint->fetchRow($QFormPrint->getAdapter()->quoteInto('id = ?', $id))->toArray();
        $this_add = $this->explodeAddress( $form_item['receiver_address'] );
        $form_item['address'] = $this_add;
        $item_arr[] = $form_item;
    }

    $this->view->item_arr = $item_arr;

    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setRender('nhat-tin/start-nhat-tin');

}