<?php
$this->_helper->layout->disableLayout();
$QFeedbackDcMayJune = new Application_Model_FeedbackDcMayJune();

// config for template
define('START_ROW', 3);
define('NO', 0);
define('DEALER_ID', 1);
define('MAP_ID', 2);
define('DEALER_NAME', 3);
define('CODE_OPPO', 4);
define('CODE_PARENT', 5);
define('AREA_NAME', 6);
define('AREA_MAP', 7);
define('TARGET_REGIS', 8);
define('SO_OPPO_MAY', 9);
define('SO_RM_MAY', 10);
define('SO_OPPO_JUNE', 11);
define('SO_RM_JUNE', 12);
define('TOTAL', 13);
define('SO_AVG', 14);
define('LEVEL_RATE', 15);
define('RESULT', 16);
define('DISCOUNT', 17);
define('FEEDBACK', 18);

$datetime = date('Y-m-d H:i:s');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();

if ($this->getRequest()->getMethod() == 'POST') {
    set_time_limit(0);
    ini_set('memory_limit', -1);
    $progress = new My_File_Progress('parent.set_progress');
    $progress->flush(0);

    $save_folder = 'upload-feedback-dc';
    $new_file_path = '';
    $requirement = array(
        'Size' => array('min' => 5, 'max' => 5000000),
        'Count' => array('min' => 1, 'max' => 1),
        'Extension' => array('xls', 'xlsx'),
    );

    try {

        $file = My_File::get($save_folder, $requirement, true);
        if (!$file || !count($file))
            throw new Exception("Upload failed");

        $uploaded_dir = My_File::getDefaultDir() . DIRECTORY_SEPARATOR . $save_folder
                . DIRECTORY_SEPARATOR . $file['folder'];
                
        $inputFileName = $uploaded_dir . DIRECTORY_SEPARATOR . $file['filename'];
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }
    
    include 'PHPExcel/IOFactory.php';
//  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        $this->view->errors = $e->getMessage();
        return;
    }

//  Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $progress->flush(60);
    for ($row = START_ROW; $row <= $highestRow; $row++) {
        try {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData = isset($rowData[0]) ? $rowData[0] : array();
            if (!empty(trim($rowData[DEALER_ID])) && !empty(trim($rowData[MAP_ID]))) {
                $data = array(
                    'feedback' => $rowData[FEEDBACK]
                );
                
                $where = array();
                $where[] = $QFeedbackDcMayJune->getAdapter()->quoteInto('dealer_id = ?', trim($rowData[DEALER_ID]));
                $where[] = $QFeedbackDcMayJune->getAdapter()->quoteInto('map_id = ?', trim($rowData[MAP_ID]));
                $QFeedbackDcMayJune->update($data,$where);
            }
        } catch (Exception $e) {
            $this->view->errors = $e->getMessage();
            return;
        }
    } // END loop through order rows
    $progress->flush(100);
}
