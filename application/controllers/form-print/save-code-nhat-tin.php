<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender();

if ( ! $this->getRequest()->isXmlHttpRequest() ) {
    echo '-100';
    exit;
}

$data_arr = $this->getRequest()->getParam('data_arr');
$count_arr = count($data_arr);

$QFormPrint = new Application_Model_FormPrintNhatTin();

foreach ($data_arr as $k => $item) {

    $where = $QFormPrint->getAdapter()->quoteInto('id = ?', $item['id']);
    $result = $QFormPrint->update( array('mail_code' => $item['mail_code']), $where);
    if(!$result){
        echo json_encode(array(
            'status' => false,
            'id'     => $item['id']
        ));
    }

}

echo json_encode(array(
    'status' => true,
    'count'  => $count_arr
));
exit();