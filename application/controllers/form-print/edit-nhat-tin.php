<?php
$QFormPrint = new Application_Model_FormPrintNhatTin();

$id = $this->getRequest()->getParam('id');

$where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
$form_print_data = $QFormPrint->fetchRow($where);

$QOffice          = new Application_Model_Office();
$offices          = $QOffice->get_cache();
$this->view->offices           = $offices;

$QTeam    = new Application_Model_Team();
$recursiveDeparmentTeamTitle = $QTeam->get_recursive_cache();
$this->view->departments = $recursiveDeparmentTeamTitle;

$QOffice          = new Application_Model_Office();
$offices          = $QOffice->get_cache();
$this->view->offices           = $offices;

$this->view->infor_arr = $form_print_data;
$this->view->mode = 'edit';
$this->_helper->viewRenderer->setRender('nhat-tin/create-nhat-tin');
