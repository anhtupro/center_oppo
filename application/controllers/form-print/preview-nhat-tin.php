<?php
$QFormPrint = new Application_Model_FormPrintNhatTin();

$id = $this->getRequest()->getParam('id');

$where = $QFormPrint->getAdapter()->quoteInto('id = ?',$id);
$data_arr = $QFormPrint->fetchRow($where);

$address_arr = $this->explodeAddress($data_arr['receiver_address']);

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$staff_id     = $userStorage->id;
$test_users = USER_NHAT_TIN;

$this->view->newform = (in_array($staff_id, $test_users));
$this->view->show_print = $this->getRequest()->getParam('print');
$this->view->id_infor = $id;
$this->view->infor = $data_arr;
$this->view->address = $address_arr;
$this->_helper->viewRenderer->setRender('nhat-tin/preview-nhat-tin');
