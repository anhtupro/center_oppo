<?php

//echo 'Tool đang bảo trì. xin vui lòng quay lại sau';exit();
$export = My_Util::escape_string($this->getRequest()->getParam('export', 0));
$flashMessenger = $this->_helper->flashMessenger;
$area_list = array();
$db = Zend_Registry::get('db');
$userStorage = Zend_Auth::getInstance()->getStorage()->read();
$staff_id = $userStorage->id;
$QAsm = new Application_Model_Asm();
$list_regions = $QAsm->get_cache($userStorage->id);
$list_regions = isset($list_regions['area']) && is_array($list_regions['area']) ? $list_regions['area'] : array();
$str_area = implode("_", $list_regions);

$title = $userStorage->title;
$QTeam = new Application_Model_Team();
$team = $QTeam->find($title);
$team_info = $team->current();
$group_id = $team_info['access_group'];

$QDistributor = new Application_Model_Distributor();
try {
    if (!in_array($group_id, array(ADMINISTRATOR_ID)) && $userStorage->id != 24 && !in_array($group_id, array(SALES_ADMIN_ID))) {
        throw new Exception("Permission deny");
    }

    if (!empty($export)) {
        $list = array();
        if (in_array($group_id, array(ADMINISTRATOR_ID)) || $userStorage->id == 24) {
            $list = $QDistributor->getListDistributorUpdateActive();
        } else if (in_array($group_id, array(SALES_ADMIN_ID))) {
            $list = $QDistributor->getListDistributorUpdateActive($list_regions);
        }
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'NO',
            'DEALER ID',
            'STORE ID',
            'DEALER NAME',
            'STORE ADDRESS',
            'AREA',
            'PROVINCE',
            'DISTRICT',
            'GÓI ĐĂNG KÍ THAM GIA',
            'ĐĂNG KÍ TRIỂN KHAI POSM PRE-ORDER ?',
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();
        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $sheet->getStyle('A1:J1')->applyFromArray(array('font' => array('bold' => true), 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'ffccff'))));
        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(10);
        $sheet->getColumnDimension('C')->setWidth(10);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(30);
        $sheet->getColumnDimension('F')->setWidth(15);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(15);
        $sheet->getColumnDimension('I')->setWidth(25);
        $sheet->getColumnDimension('J')->setWidth(40);
        $index = 2;
        $i = 1;
        foreach ($list as $item) {
            $alpha = 'A';

            $sheet->getCell($alpha++ . $index)->setValueExplicit($i++, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['dealer_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['store_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['store_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['store_address'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['area_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['province_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['district_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['target_regis'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['note'], PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;
        }
        if (!empty($str_area)) {
            $filename = 'Upload_recheck_preoder_shop_' . $str_area . '_d_' . date('d/m/Y');
        } else
            $filename = 'Upload_recheck_preoder_shop_' . date('d/m/Y');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit();
    }
} catch (Exception $exc) {

    $this->view->messages = $exc->getMessage();
}