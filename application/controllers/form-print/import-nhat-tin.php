<?php
$QFormPrint = new Application_Model_FormPrintNhatTin();
$flashMessenger = $this->_helper->flashMessenger;

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$staff_id     = $userStorage->id;

// import IOFactory Lib
include 'PHPExcel/IOFactory.php';

$inputFileName = $_FILES['file']['tmp_name'];

try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch(Exception $e) {
    $flashMessenger->setNamespace('error')->addMessage( "Có lỗi: Import File thất bại !");
    $this->redirect(HOST.'form-print');
}

$sheet          = $objPHPExcel->getSheet(0);
$highestRow     = $sheet->getHighestRow();
$highestColumn  = $sheet->getHighestColumn();
$rowData        = array();

for ($row = 2; $row <= $highestRow; $row++){
    $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE,FALSE);
}

$success_rows = 0;

foreach ($rowData as $key => $item) {

    $value = $item[0];

    $guarantee_delivery   = ( trim($value[7]) == 'x' ) ? 1 : 0;
    $fast_delivery        = ( trim($value[8]) == 'x' ) ? 1 : 0;
    $time_delivery        = ( trim($value[9]) == 'x' ) ? 1 : 0;

    $data_arr = array(
        'sender_name'       => $value[0],
        'sender_phone'      => $value[1],
        'sender_department' => $value[2],
        'receiver_name'     => $value[3],
        'receiver_address'  => $value[4],
        'receiver_phone'    => $value[5],
        'content'           => $value[6],
        'guarantee_delivery'=> $guarantee_delivery,
        'fast_delivery'     => $fast_delivery,
        'time_delivery'     => $time_delivery,
        'created_by'        => $staff_id
    );

    // echo "<pre>";
    // print_r($data_arr);
    $data_arr['created_at']   = date('Y-m-d H:i:s');
    $save_result              = $QFormPrint->insert($data_arr);

    if($save_result != null){
        $success_rows ++;
    }

}

// exit();

$flashMessenger->setNamespace('success')->addMessage( "Đã Import thành công ".$success_rows." dòng dữ liệu.");
$this->redirect(HOST.'form-print');