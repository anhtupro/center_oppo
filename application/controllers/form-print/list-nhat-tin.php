<?php
$flashMessenger = $this->_helper->flashMessenger;
$db = Zend_Registry::get('db');
$QFormPrint   = new Application_Model_FormPrintNhatTin();
$QDepartments = new Application_Model_Team();

$userStorage  = Zend_Auth::getInstance()->getStorage()->read();
$staff_id     = $userStorage->id;
$office_id = $userStorage->office_id;

// $user_right   = ($staff_id == 2128) ? true : false;
$user_right   = in_array($staff_id, USER_NHAT_TIN) ? true : false;
$this->view->user_right = $user_right;



$departments  = $QDepartments->get_department();
$this->view->departments = $departments;

$form_id            = $this->getRequest()->getParam('form_id');
$export             = $this->getRequest()->getParam('export',0);
$created_at         = $this->getRequest()->getParam('created_at');
$created_to         = $this->getRequest()->getParam('created_to');
$sender_name        = $this->getRequest()->getParam('sender_name');
$receiver_name      = $this->getRequest()->getParam('receiver_name');
$sender_department  = $this->getRequest()->getParam('sender_department');
$content            = $this->getRequest()->getParam('content');
$receiver_address   = $this->getRequest()->getParam('receiver_address');
$receiver_phone     = $this->getRequest()->getParam('receiver_phone');
$sender_phone       = $this->getRequest()->getParam('sender_phone');
$mail_code          = $this->getRequest()->getParam('mail_code');
$page               = $this->getRequest()->getParam('page', 1);

$params = array(
    "sender_name"       => $sender_name,
    "receiver_name"     => $receiver_name,
    "sender_department" => $sender_department,
    "content"           => $content,
    "receiver_address"  => $receiver_address,
    "receiver_phone"    => $receiver_phone,
    "sender_phone"      => $sender_phone,
    "mail_code"         => $mail_code,
    "id"                => $form_id,
    'created_at'        => $created_at,
    'created_to'        => $created_to,
    'created_by'        => $staff_id,
    'export'            => $export
);

$page     = $this->getRequest()->getParam('page',1);
$limit    = LIMITATION;
$total    = 0;

$mail_list = $QFormPrint->fetchData($page, $limit, $total, $params);
if($_GET['dev']){
    echo "<pre>";
    print_r($mail_list);exit;
}

if($export){

    $this->_exportXlsx($mail_list);
}
$this->view->params     = $params;
$this->view->mail_list  = $mail_list;
$this->view->limit      = $limit;
$this->view->total      = $total;
$this->view->offset     = $limit * ($page - 1);
$this->view->url        = HOST . 'form-print/list-nhat-tin' . ($params ? '?' . http_build_query($params) .'&' : '?');
$this->view->messages = $flashMessenger->setNamespace('error')->getMessages();
$this->view->messages_success = $flashMessenger->setNamespace('success')->getMessages();
$this->_helper->viewRenderer->setRender('nhat-tin/list-nhat-tin');
