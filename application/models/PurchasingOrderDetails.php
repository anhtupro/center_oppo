<?php
class Application_Model_PurchasingOrderDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_order_details';

    function getPurchasingOrderDetails($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'r.sn', 
                'r.area_id', 
                'r.department_id', 
                'pmodel_code' => 'c.code', 
                'pmodel_name' => 'c.name', 
                'o.supplier_id', 
                'p.quantity', 
                'c.unit',
                'p.price', 
                'r.urgent_date',
                'category' => 'cat.name'
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        $select->joinleft(array('o'=>'purchasing_order'), 'o.id = p.po_id', array());
        $select->joinleft(array('rd'=>'purchasing_request_details'), 'rd.id = p.pr_details_id', array());
        $select->joinleft(array('r'=>'purchasing_request'), 'r.id = rd.purchasing_request_id', array());
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = p.pmodel_code_id', array());
        $select->joinleft(array('s'=>'supplier'), 's.id = o.supplier_id', array());
        $select->joinleft(array('cat'=>'pcategory'), 'c.category_id = cat.id', array());

        if(!empty($params['sn'])){
            $select->where('o.sn = ?', $params['sn']);
        }
 
        $result = $db->fetchAll($select);

        return $result;
    }
}