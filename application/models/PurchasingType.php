<?php
class Application_Model_PurchasingType extends Zend_Db_Table_Abstract
{
	protected $_name = 'purchasing_type';

        /**
     *  thong July 18 2020
     */
    /**************************************** */
    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>'purchasing_type'),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.*',
                            'department_name' => 't1.name',
                            'team_name'       => 't2.name',
                        ))
            ->joinLeft(array('t1'=>'team'),'p.department_id = t1.id',array())  
            ->joinLeft(array('t2'=>'team'),'p.team_id = t2.id',array())  
            ->order('p.id DESC');
            //->where('p.del = ?',0);

     
        
        if (isset($params['name']) and $params['name']) {
            $select->where('p.name LIKE ?', '%' . $params['name'] . '%');
        }
        
        if(!empty($params['department_id'])) {
            $select->where('t1.id = ?', $params['department_id']);
        }
        
        if(!empty($params['team_id'])) {
            $select->where('t2.id = ?', $params['team_id']);
        }
        
        if(!empty($params['del'])) {
            $select->where('p.del = ?', $params['del']);
        }
        
        $select->where('p.team_id IS NOT NULL');

        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        return $result;
    }
        
    public function getType($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Could not find row $id");
        }
        return $row->toArray();
    }

    public function addType($type_name, $department_id, $team_id)
    {
        $data = array(
            'name' => $type_name,
            'department_id' => $department_id,
            'team_id' => $team_id,
        );
        $this->insert($data);
    }

    public function updateType($id, $type_name, $department_id, $team_id)
    {
        $data = array(
            'name' => $type_name,
            'department_id' => $department_id,
            'team_id' => $team_id,
        );
        $this->update($data, 'id = '. (int)$id);
    }
    
    public function hideType($id, $del)
    {
        $data = array(
            'del' => $del,
        );
        $this->update($data, 'id = '. (int)$id);
    }

    public function deleteType($id)
    {
        $this->delete('id =' . (int)$id);
    } 
    
     /**************************************** */
    
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');
        
        if ($result === false) {
            
            $db = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('p' => $this->_name), array('p.*')); 
            $select->where('p.del IS NULL OR p.del = ?', 0);

            $data = $db->fetchAll($select);
            $result = array(); 
           
            if ($data){
                foreach ($data as $k => $item){
                    $result[$item['id']] = $item['name'];                    
                }
            }
            
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
    
    function getCacheTypeByDepartment(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_by_department');
        
        if ($result === false) {
            
            $db = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('p' => $this->_name), array('p.*'));
            $select->where('p.del IS NULL OR p.del = ?', 0);

            $data = $db->fetchAll($select);
            $result = array(); 
           
            if ($data){
                foreach ($data as $k => $item){
                    $result[$item['department_id']][] = $item;                    
                }
            }
            
            $cache->save($result, $this->_name.'_cache_by_department', array(), null);
        }
        return $result;
    }
    
    function getCacheTypeByTeam($team_id){
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name), array('p.*'));
        $select->where('p.del = ?', 0);
        $select->where('p.team_id = ?', $team_id);

        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function get_cache_all(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_all');
        
        if ($result === false) {
            
            $db = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('p' => $this->_name), array('p.*'))
                    ->order('p.name'); 

            $data = $db->fetchAll($select);
            $result = array(); 
           
            if ($data){
                foreach ($data as $k => $item){
                    $result[$item['id']] = $item['name'];                    
                }
            }
            
            $cache->save($result, $this->_name.'_cache_all', array(), null);
        }
        return $result;
    }

    function getAll(){
            $db = Zend_Registry::get('db');
            $select = $db->select()
                ->from(array('p' => $this->_name), array('p.*',
                    'department_name' => 't.name',
                    ))
                ->joinLeft(array('t'=>'team'),'t.id = p.department_id',array());
            $select->where('p.del IS NULL OR p.del = ?', 0);
            $result = $db->fetchAll($select);
            return $result;
    }
}                                                      
