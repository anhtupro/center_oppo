<?php
class Application_Model_StaffEducation extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_education';
    
    public function getDataEducation($id_staff){
        $db   = Zend_Registry::get('db');
        $select       = $db->select()
            ->from(array('se'=>$this->_name),array('se.*'))
            ->joinInner(array('ls'=>'level_staff'),'ls.id = se.level_id', array('ls.name'))
            ->where('staff_id = ?',$id_staff);
        $list_education_staff = $db->fetchAll($select);
        return $list_education_staff;
    }
    public function addEducation($list_edu = array()){
        
    }

}