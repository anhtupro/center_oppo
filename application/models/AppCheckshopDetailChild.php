<?php

class Application_Model_AppCheckshopDetailChild extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_checkshop_detail_child';
    protected $_schema = DATABASE_TRADE;

    public function getDetailCheckshop($storeId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.app_checkshop'], [
                         "checkshop_id" => "a.id",
                         "a.store_id",
                         "d.category_id",
                         "category_name" => "c.name",
                         "d.quantity",
//                         "imei_sn" => "REPLACE(d.imei_sn, 'https://trade-marketing.opposhop.vn/tool/scan-imei?imei_sn=', '')",
                         'app_checkshop_detail_child_id' => 'd.id',
                         'c.has_type',
                         'c.has_width',
                         'category_type_name' => 't.name',
                         'd.height',
                         'd.width',
                         'imei_sn' => 'd.imei',
                         'd.in_processing',
                         'material_name' => 'm.name',
                         'c.has_material',
                         'category_type_child_name' => 'tc.name'
                     ])
                     ->joinLeft(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], 'a.id = d.checkshop_id AND d.quantity > 0', [])
                    ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
                    ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'd.category_type = t.id', [])
                    ->joinLeft(['tc' => DATABASE_TRADE.'.category_inventory_type'], 'd.category_type_child = tc.id', [])
                    ->joinLeft(['m' => DATABASE_TRADE.'.category_material'], 'd.material_id = m.id', [])
                    ->where('a.store_id = ?', $storeId)
                    ->where('a.is_lock = ?', 1)
//                    ->where('a.updated_info_detail = ?', 1) // shop đã được update thông tin chi tiết của hạng mục (loại, dài, cao)
                    ->where('d.id IS NOT NULL');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDetailChild($checkshopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                        'd.*',
                        'c.has_type',
                        'c.has_width',
                        'category_name' => 'c.name',
                         'category_type_name' => 't.name',
                         'checkshop_detail_child_id' => 'd.id',
                         'app_checkshop_detail_child_id' => 'd.id',
                         'category_type_child_name' => 'y.name',
                         'material_name' => 'm.name'

                     ])
                    ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
                    ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'd.category_type = t.id', [])
                    ->joinLeft(['y' => DATABASE_TRADE.'.category_inventory_type'], 'd.category_type_child = y.id', [])
                    ->joinLeft(['m' => DATABASE_TRADE.'.category_material'], 'd.material_id = m.id', [])


            ->where('d.checkshop_id = ?', $checkshopId)
                    ->where('d.quantity > 0');
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list[$element['category_id']] [] = $element;
        }
        return $list;

    }

    public function getDetailParent($checkshopId, $params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                         'd.category_id',
                         'category_name' => 'c.name',
                         'c.has_type',
                         'c.has_width',
                         'SUM(d.quantity)',
                         'c.has_material',
                         'c.has_painting',
                         'quantity' => 'SUM(d.quantity)',
                         'c.is_picture',
                         'c.company'
                     ])
                    ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
                    ->where('d.checkshop_id = ?', $checkshopId)
                    ->group('d.category_id')
                    ->having('SUM(d.quantity) > 0');

        if ($params['only_picture']) { // chỉ lấy những HM có tranh
            $select->where('c.is_picture = 1 OR c.has_painting = 1');
        }

        if ($params['company']) {
            $select->where('c.company = ?', $params['company']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getShopIsLock($page, $limit, &$total, $params )
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => 'store'], [
                         'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
                         'checkshop_id' => 'a.id',
                         'store_id' => 's.id',
                         'store_name' => 's.name',
                         'a.updated_info_detail',
                         'a.is_lock',
                         'a.updated_facade_size'
                     ])
                    ->joinLeft(['a' => DATABASE_TRADE.'.app_checkshop'], 's.id = a.store_id AND a.is_lock = 1', [])
//                    ->join(['s' => 'store'], 'a.store_id = s.id', [])
                    ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
                    ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
                    ->joinLeft(['p' => 'v_store_staff_leader_log'], 's.id = p.store_id', [])
                    ->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array())
                    ->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array())
                    ->joinLeft(['si' => DATABASE_TRADE.'.standard_image_log'], 's.id = si.store_id', [])


            ->where('s.del IS NULL OR s.del = 0');

        if ($params['has_category']) {
            $select->joinLeft(['w' => DATABASE_TRADE.'.app_checkshop_detail_child'], 'a.id = w.checkshop_id', []);
            $select->group('s.id');
            $select->having('SUM(w.quantity) > 0');
        }

        if ($params['list_area']) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }
        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }
        if ($params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }
        if ($params['store_name']) {
            $select->where('s.name LIKE ?', "%". $params['store_name'] . "%");
        }
        if ($params['regional_market']) {
            $select->where('r.id = ?', $params['regional_market']);
        }
        if ($params['district']) {
            $select->where('di.id = ?', $params['district']);
        }
        if (isset($params['is_updated']) && $params['is_updated'] == 1) {
            $select->where('updated_info_detail = ?', 1);
        }
        if (isset($params['is_updated']) && $params['is_updated'] == 2) {
            $select->where('updated_info_detail IS NULL');
        }
        if ($params['is_updated_facade'] == 3) {
            $select->where('updated_facade_size = ?', 1);
        }
        if ($params['is_updated_facade'] == 4) {
            $select->where('updated_facade_size IS NULL');
        }

        if ($params['is_updated_standard_image'] == 5) {
            $select->where('si.id IS NOT NULL');
        }
        if ($params['is_updated_standard_image'] == 6) {
            $select->where('si.id IS NULL');
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])){


            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('d.is_ka = ?', 1);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "2"){
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if(!empty($params['is_ka_details'])){

            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

        if ($params['change_picture_stage']) {
            $select->join(['g' => DATABASE_TRADE.'.change_picture_assign_store'], 's.id = g.store_id AND g.stage_id = ' . $params['change_picture_stage'], []);
        }
        
        $select->limitPage($page, $limit);
        $select->order('a.id DESC');
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function removeImeiFromString($imei, $stringImei)
    {
        $string = '';
        $string = str_replace(' ', '', $stringImei);
        $arrayImei = explode(',', $string);
        $newArrayImei = array_diff($arrayImei, [$imei]);

        $string = implode(', ', $newArrayImei);

        return $string ? $string : false;
    }

    public function getCategoryWarehouse($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'category_id' => 'o.category_id',
                'category_name' => 'c.name',
                'quantity' => 'o.quantity',
                'group_bi' => 'c.group_bi',
                'c.has_type',
                'c.has_width',
                'o.height',
                'o.width',
                'o.category_type',
                'o.category_type_child',
                'category_type_name' => 'i.name',
                'category_type_child_name' => 'y.name',
                'c.has_material',
                'c.has_painting',
                'o.material_id',
                'app_checkshop_detail_child_id' => 'o.id'

            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'o.category_id = c.id', [])
            ->joinLeft(['i' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type = i.id', [])
            ->joinLeft(['y' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type_child = y.id', [])
            ->where('c.statistic_air = ?', 1)
            ->where('o.quantity > 0');
//            ->group(['o.area_id', 'i.category_id']);

        if($params['area_id']) {
            $select->where('o.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategoryParentWarehouse($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'category_id' => 'o.category_id',
                'category_name' => 'c.name',
                'quantity' => 'SUM(o.quantity)',
                'c.has_type',
                'c.has_width',
                'c.has_material',
                'c.has_painting',
                'app_checkshop_detail_child_id' => 'o.id'

            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'o.category_id = c.id', [])
            ->joinLeft(['i' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type = i.id', [])
            ->joinLeft(['y' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type_child = y.id', [])
            ->where('o.quantity > 0')
            ->where('c.statistic_air = ?', 1)
            ->group([ 'o.category_id']);

        if($params['area_id']) {
            $select->where('o.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }
    public function getCategoryDetailWarehouse($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'category_id' => 'o.category_id',
                'category_name' => 'c.name',
                'quantity' => 'o.quantity',
                'group_bi' => 'c.group_bi',
                'c.has_type',
                'c.has_width',
                'o.height',
                'o.width',
                'o.category_type',
                'o.category_type_child',
                'category_type_name' => 'i.name',
                'category_type_child_name' => 'y.name',
                'c.has_material',
                'c.has_painting',
                'o.material_id',
                'app_checkshop_detail_child_id' => 'o.id',
                'material_name' => 'm.name'

            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'o.category_id = c.id', [])
            ->joinLeft(['i' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type = i.id', [])
            ->joinLeft(['y' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type_child = y.id', [])
            ->joinLeft(['m' => DATABASE_TRADE.'.category_material'], 'o.material_id = m.id', [])

            ->where('o.quantity > 0');

        if($params['area_id']) {
            $select->where('o.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
           $list [$element['category_id']] [] = $element;
         }

        return $list;
    }

    public function checkCategoryIsProcessing($checkshopId)
    {
        $status_transfer_finish = '(6,8)';
        $status_destruction_finish = '(7,8)';
        $status_repair_finish = '(6,7)';

        $db = Zend_Registry::get("db");
        $subSelect = $db->select()
            ->from(['c' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'c.checkshop_id',
                'is_transfering' => 'IF (transfer.id IS NULL, 0, t.quantity)',
                'is_destructing' => 'IF (destruction.id IS NULL, 0,d.quantity)',
                'is_repairing' => 'IF (repair.id IS NULL, 0, r.quantity)'
            ])
            ->joinLeft(['t' => DATABASE_TRADE.'.transfer_details_child'], 'c.id = t.app_checkshop_detail_child_id', [])
            ->joinLeft(['transfer' => DATABASE_TRADE.'.transfer'], 't.transfer_id = transfer.id AND (transfer.remove IS NULL OR transfer.remove = 0) AND transfer.status NOT IN' . $status_transfer_finish, [])

            ->joinLeft(['d' => DATABASE_TRADE.'.destruction_details_child'], 'c.id = d.app_checkshop_detail_child_id', [])
            ->joinLeft(['destruction' => DATABASE_TRADE.'.destruction'], 'd.destruction_id = destruction.id AND (destruction.remove IS NULL OR destruction.remove = 0) AND destruction.status NOT IN' . $status_destruction_finish, [])

            ->joinLeft(['r' => DATABASE_TRADE.'.repair_details'], 'c.id = r.app_checkshop_detail_child_id', [])
            ->joinLeft(['repair' => DATABASE_TRADE.'.repair'], 'r.repair_id = repair.id AND (repair.remove IS NULL OR repair.remove = 0) AND repair.status NOT IN' . $status_repair_finish, [])

            ->where('c.checkshop_id = ?', $checkshopId);


        $select =  $db->select()
            ->from(array('i' =>  new Zend_Db_Expr('(' . $subSelect . ')') ), [
                'total_transfering' => 'SUM(i.is_transfering)',
                'total_destructing' => 'SUM(i.is_destructing)',
                'total_repairing' => 'SUM(i.is_repairing)'
            ]);

        $result = $db->fetchRow($select);

        $stringAlert = '';

        if ($result['total_transfering']) {
            $stringAlert .= 'Shop có ' . $result['total_transfering'] . ' hạng mục đang được điều chuyển, ';
        }
        if ($result['total_destructing']) {
            $stringAlert .= 'Shop có ' . $result['total_destructing'] . ' hạng mục đang được tiêu hủy, ';
        }
        if ($result['total_repairing']) {
            $stringAlert .= 'Shop có ' . $result['total_repairing'] . ' hạng mục đang được sửa chữa ';
        }

        return $stringAlert ? $stringAlert : false;

    }

    public function getLastCheckshop($storeId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.app_checkshop'], [
                'a.*'
            ])
            ->where('a.store_id = ?', $storeId)
            ->order('a.id DESC')
            ->limit(1);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getDataExportFacadeShop($params)
    {
        $db = Zend_Registry::get("db");

        $arrCols = array(
            'store_name'        => 'p.name' ,
            'store_id'          => 'p.id' ,
            'dealer_id'         => 'p.d_id' ,
            'partner_id'        => 'p.partner_id' ,
            'shipping_address'  => 'p.shipping_address',
            'dealer_name'       => 'd.name',
            'is_ka'             => 'd.is_ka',
            'channel'           => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END",
            'area_name'         => "area.name",
            "parent_id"         => "IF(d.parent = 0, d.id, d.parent)",
            'province' => 'r.name',
            'z.height',
            'z.width',
            'district' => 'district.name'
        );

        $select = $db->select();
        $select->from(array('p'=> 'store'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->joinLeft(array('district' => 'regional_market'), 'p.district = district.id', array());
        $select->joinLeft(array('area' => 'area'), 'area.id = r.area_id', array());
        $select->joinLeft(array('l' => 'v_store_staff_leader_log'), 'l.store_id = p.id AND l.released_at IS NULL AND l.is_leader = 1', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_checkshop'), 'a.store_id = p.id AND a.is_lock = 1',array());
        $select->joinLeft(array('z' => DATABASE_TRADE.'.store_facade_size'), 'a.store_id = z.store_id AND z.type = ' . $params['facade_type'],array());

        $select->where('p.del IS NULL OR p.del = 0');

        if(!empty($params['staff_id'])){
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if(!empty($params['list_area'])){
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])){


            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('loyalty.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('d.is_ka = ?', 1);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "2"){
                $select->where('loyalty.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if(!empty($params['is_ka_details'])){

            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('loyalty.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportFacadeShop($data, $file_name)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'Stt',
            'Kênh',
            'Store ID',
            'Dealer ID',
            'Partner ID',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Huyện',
            'Ngang',
            'Cao'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['district']);
            $sheet->setCellValue($alpha++.$index, $item['width']);
            $sheet->setCellValue($alpha++.$index, $item['height']);

            $index++;

        }

        $filename = $file_name . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }


    public function getDataExportInfoCategory($params)
    {
        $db = Zend_Registry::get("db");

        $arrCols = array(
            'store_name'        => 's.name' ,
            'store_id'          => 's.id' ,
            'dealer_id'         => 's.d_id' ,
            'partner_id'        => 's.partner_id' ,
            'shipping_address'  => 's.shipping_address',
            'dealer_name'       => 'd.name',
            'is_ka'             => 'd.is_ka',
            'channel'           => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END",
            'area_name' => "area.name",
            "parent_id" => "IF(d.parent = 0, d.id, d.parent)",
            'province' => 'r.name',

            'child_id' => 'de.id',
            'category_name' => 'c.name',
            'de.quantity',
            'type' => 't.name',
            'type_child' => 't2.name',
            'material' => 'm.name',
            'height' => 'de.height',
            'width' => 'de.width',
            'height_painting' => 'p.height',
            'width_painting' => 'p.width',
            'material_painting' => 'm2.name',
            'district' => 'district.name',
            'sale_name' => "CONCAT(sale.firstname, ' ', sale.lastname)",
            'c.is_picture'
        );

        $select = $db->select();
        $select->from(array('s'=> 'store'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('district' => 'regional_market'), 's.district = district.id', array());
        $select->joinLeft(array('area' => 'area'), 'area.id = r.area_id', array());
        $select->joinLeft(array('l' => 'v_store_staff_leader_log'), 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', array());
        $select->joinLeft(array('sale' => 'staff'), 'l.staff_id = sale.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_checkshop'), 'a.store_id = s.id AND a.is_lock = 1',array());
        $select->joinLeft(array('de' => DATABASE_TRADE.'.app_checkshop_detail_child'), 'a.id = de.checkshop_id AND de.quantity > 0',array());
        $select->joinLeft(array('p' => DATABASE_TRADE.'.app_checkshop_detail_child_painting'), 'de.id = p.app_checkshop_detail_child_id',array());
        $select->joinLeft(array('t' => DATABASE_TRADE.'.category_inventory_type'), 'de.category_type = t.id',array());
        $select->joinLeft(array('t2' => DATABASE_TRADE.'.category_inventory_type'), 'de.category_type_child = t2.id',array());
        $select->joinLeft(array('m' => DATABASE_TRADE.'.category_material'), 'de.material_id = m.id',array());
        $select->joinLeft(array('m2' => DATABASE_TRADE.'.category_material'), 'p.material_id = m2.id',array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'de.category_id = c.id',array());
        $select->where('s.del IS NULL OR s.del = 0');

        if ($params['export_change_picture']) {
            $select->joinLeft(array('k' => DATABASE_TRADE.'.change_picture_detail'), "IF(k.app_checkshop_detail_child_id IS NULL, k.child_painting_id = p.id, k.app_checkshop_detail_child_id = de.id) AND k.stage_id = " . $params['change_picture_stage'] . ' AND k.product_id <> 0' ,array('k.has_changed_picture', 'k.logo_type', 'change_picture_note' => 'k.note'));
            $select->joinLeft(array('v' => DATABASE_TRADE.'.phone_product'), 'k.product_id = v.id',array('key_visual' => 'v.name'));
            $select->join(array('w' => DATABASE_TRADE.'.change_picture_assign_store'), 'w.store_id = s.id AND w.stage_id = ' . $params['change_picture_stage'], array('key_visual' => 'v.name'));
            $select->where('c.is_picture = 1 OR c.has_painting = 1');

        }

        if(!empty($params['staff_id'])){
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if(!empty($params['list_area'])){
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])){


            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('loyalty.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('d.is_ka = ?', 1);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "2"){
                $select->where('loyalty.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if(!empty($params['is_ka_details'])){

            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('loyalty.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportInfoCategory($data)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'Stt',
            'Kênh',
            'Store ID',
            'Dealer ID',
            'Partner ID',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Huyện',
            'Sale quản lý',
            'Hạng mục',
            'ID Hạng mục',
            'Số lượng',
            'Loại',
            'Bộ mica',
            'Chất liệu',
            'Chiều ngang vách (mm)',
            'Chiều cao vách (mm)',
            'Chiều ngang tranh (mm)',
            'Chiều cao tranh (mm)',
            'Chất liệu tranh'


        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['district']);
            $sheet->setCellValue($alpha++.$index, $item['sale_name']);
            $sheet->setCellValue($alpha++.$index, $item['category_name']);
            $sheet->setCellValue($alpha++.$index, $item['child_id']);
            $sheet->setCellValue($alpha++.$index, $item['quantity']);
            $sheet->setCellValue($alpha++.$index, $item['type']);
            $sheet->setCellValue($alpha++.$index, $item['type_child']);
            $sheet->setCellValue($alpha++.$index, $item['material']);

            if ($item['is_picture'] == 1) {
                $sheet->setCellValue($alpha++.$index, '');
                $sheet->setCellValue($alpha++.$index, '');
                $sheet->setCellValue($alpha++.$index, $item['width']);
                $sheet->setCellValue($alpha++.$index, $item['height']);
            } else {
                $sheet->setCellValue($alpha++.$index, $item['width']);
                $sheet->setCellValue($alpha++.$index, $item['height']);
                $sheet->setCellValue($alpha++.$index, $item['width_painting']);
                $sheet->setCellValue($alpha++.$index, $item['height_painting']);
            }
           
            $sheet->setCellValue($alpha++.$index, $item['material_painting']);


            $index++;

        }

        $filename = 'Thông tin kích thước hạng mục' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function exportChangePicture($data)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'Stt',
            'Kênh',
            'Store ID',
            'Dealer ID',
            'Partner ID',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Huyện',
            'Sale quản lý',
            'Hạng mục',
            'ID Hạng mục',
            'Số lượng',
            'Chiều ngang tranh (mm)',
            'Chiều cao tranh (mm)',
            'Chất liệu tranh',
            'Key visual',
            'Logo',
            'Ghi chú',
            'Trạng thái thay tranh'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['district']);
            $sheet->setCellValue($alpha++.$index, $item['sale_name']);
            $sheet->setCellValue($alpha++.$index, $item['category_name']);
            $sheet->setCellValue($alpha++.$index, $item['child_id']);
            $sheet->setCellValue($alpha++.$index, $item['quantity']);

            if ($item['is_picture'] == 1) {
                $sheet->setCellValue($alpha++.$index, $item['width']);
                $sheet->setCellValue($alpha++.$index, $item['height']);
            } else {
                $sheet->setCellValue($alpha++.$index, $item['width_painting']);
                $sheet->setCellValue($alpha++.$index, $item['height_painting']);
            }

            $sheet->setCellValue($alpha++.$index, $item['material_painting'] ? $item['material_painting'] : $item['material']);
            $sheet->setCellValue($alpha++.$index, $item['key_visual']);
            $sheet->setCellValue($alpha++.$index, $item['logo_type'] == 1 ? 'Có logo' : ($item['logo_type'] == 2 ? 'Không có logo' : ''));
            $sheet->setCellValue($alpha++.$index, $item['change_picture_note']);
            $sheet->setCellValue($alpha++.$index, $item['has_changed_picture'] == 1 ? 'Đã thay' : 'Chưa thay');


            $index++;

        }

        $filename = 'Thay tranh' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }



}