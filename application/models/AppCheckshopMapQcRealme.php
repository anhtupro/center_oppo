<?php

class Application_Model_AppCheckshopMapQcRealme extends Zend_Db_Table_Abstract {

    protected $_name = 'app_checkshop_map_qc_realme';
    protected $_schema = DATABASE_TRADE;
    
    //Check xem shop đó trong tháng đc QC chưa
    public function checkQc($store_id, $created_at,$type){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => "p.id" 
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_checkshop_map_qc_realme'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->where('c.store_id = ?', $store_id);
        $select->where('p.type = ?', $type);
        $select->where('MONTH(c.created_at) = ?', date('m', strtotime($created_at)));
        $select->where('YEAR(c.created_at) = ?', date('Y', strtotime($created_at)));

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getHistoryQc($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "id" => "p.id",
            "created_at" => "c.created_at",
            "p.note",
            "q.title",
            "p.type",
            "month_year" => "DATE_FORMAT(`c`.`created_at`, '%c/%Y')",
            "type_name" => '(CASE WHEN p.type = 1 THEN "TMK Local" WHEN p.type = 2 THEN "TMK Supervisor Realme"  END)'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_map_qc_realme'), $arrCols);
        $select->joinLeft(array('q' => DATABASE_TRADE . '.app_checkshop_qc_realme'), 'q.id = p.qc_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->where('c.store_id = ?', $params['store_id']);

        $result = $db->fetchAll($select);

        if ($_GET['dev'] == 2) {
            echo $select;
            exit;
        }

        return $result;
    }
    public function getQc($params){

        $first_date = current($params['range_date']);
        $last_date = end($params['range_date']);

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "c.store_id", 
            "month" => "MONTH(c.created_at)", 
            "q.title",
            "p.type"
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_checkshop_map_qc_realme'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->joinLeft(array('q' => DATABASE_TRADE.'.app_checkshop_qc_realme'), 'q.id = p.qc_id', array());
        $select->where('(c.created_at) >= ?', $first_date);
        $select->where('(c.created_at) <= ?', $last_date);
           $result = $db->fetchAll($select);
        
        
        $data = [];
        foreach($result as $key=>$value){
            $data[$value['store_id']][$value['month']][$value['type']] = $value['title'];
        }

        return $data;
    }

}
