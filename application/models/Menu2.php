<?php

class Application_Model_Menu2 extends Zend_Db_Table_Abstract {

    protected $_name = 'leave_detail';

    private function dbconnect() {
        return Zend_Registry::get('db');
    }

    public function getMenuPermission()
	{
		$db = Zend_Registry::get('db');
		$userStorage = Zend_Auth::getInstance()->getStorage()->read();
		$menu_time = array(
			// Hidden module Time Leave
            // array(
            //     'id' => 999,
            //     'parent_id' => 0,
            //     'title' => 'Time Leave',
            //     'title_vie' => 'Time Leave',
            //     'url' => '/staff-time/menu?id=999',
            //     'class' => 'icon-arrow-down',
            //     'class_new' => 'icon1-arrow-down'
            // ),
            array(
                'id'        => 1000,
                'parent_id' => 999,
                'postion'   => 1,
                'title'     => 'My Check In',
                'title_vie' => 'My Check In',
                'url'       => '/staff-time/staff-view',
                'class'     => ''
            ),
            array(
                'id'        => 1001,
                'parent_id' => 999,
                'postion'   => 10,
                'title'     => 'Create Leave',
                'title_vie' => 'Create Leave',
                'url'       => '/leave/create',
                'class'     => ''
            ),
            array(
                'id'        => 1002,
                'parent_id' => 999,
                'postion'   => 11,
                'title'     => 'My Leave',
                'title_vie' => 'My Leave',
                'url'       => '/leave/list-my-leave',
                'class'     => ''
            ),
                //         array(
                //             'id' => 1003,
                //             'parent_id' => 999,
                // 'postion' => 12,
                //             'title' => 'Create Realme',
                //             'title_vie' => 'Báo số Realme',
                //             'url' => '/timing-realme/create',
                //             'class' => ''
                //         ),
        );

        if (in_array($userStorage->title, array(537))) {
            $menu_time[] = array(
                'id'        => 1011,
                'parent_id' => 999,
                'postion'   => 5,
                'title'     => 'Check In GPS',
                'title_vie' => 'Check In GPS',
                'url'       => '/time/pg-time-create',
                'class'     => ''
            );

            $menu_time[] = array(
                'id'        => 1012,
                'parent_id' => 999,
                'postion'   => 6,
                'title'     => 'Check Out GPS',
                'title_vie' => 'Check Out GPS',
                'url'       => '/time/pg-check-out',
                'class'     => ''
            );
        }
        if (in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID))) {
            $menu_time[] = array(
                'id'        => 1003,
                'parent_id' => 999,
                'postion'   => 2,
                'title'     => 'List Staff Time',
                'title_vie' => 'List Staff Time',
                'url'       => '/staff-time/list-staff-check-in',
                'class'     => ''
            );
            /* $menu_time[] =  array(
              'id' => 1004,
              'parent_id' => 999,
              'postion' => 2,
              'title' => 'List Work Online',
              'title_vie' => 'Danh Sách Làm Việc Online',
              'url' => '/staff-time/show-online-time',
              'class' => ''
              ); */

            $menu_time[] = array(
                'id'        => 1005,
                'parent_id' => 999,
                'postion'   => 2,
                'title'     => 'Approve Time / Leave',
                'title_vie' => 'Approve Time / Leave',
                'url'       => '/staff-time/list-staff-approve',
                'class'     => ''
            );

            $menu_time[] = array(
                'id'        => 1006,
                'parent_id' => 999,
                'postion'   => 3,
                'title'     => 'Set Permission',
                'title_vie' => 'Set Permission',
                'url'       => '/staff-permission',
                'class'     => ''
            );



            $menu_time[] = array(
                'id'        => 1007,
                'parent_id' => 999,
                'postion'   => 3,
                'title'     => 'Chuyên cần',
                'title_vie' => 'Chuyên cần',
                'url'       => '/time-machine/late-list-new',
                'class'     => ''
            );


            $menu_time[] = array(
                'id'        => 1011,
                'parent_id' => 999,
                'postion'   => 5,
                'title'     => 'Check In GPS',
                'title_vie' => 'Check In GPS',
                'url'       => '/time/pg-time-create',
                'class'     => ''
            );

            $menu_time[] = array(
                'id'        => 1012,
                'parent_id' => 999,
                'postion'   => 6,
                'title'     => 'Check Out GPS',
                'title_vie' => 'Check Out GPS',
                'url'       => '/time/pg-check-out',
                'class'     => ''
            );

            $menu_time[] = array(
                'id'        => 1013,
                'parent_id' => 999,
                'postion'   => 5,
                'title'     => 'Date Special Setting',
                'title_vie' => 'Date Special Setting',
                'url'       => '/date/list-date-setting',
                'class'     => ''
            );
            /*
              $menu_time[] =  array(
              'id' => 1014,
              'parent_id' => 999,
              'postion' => 6,
              'title' => 'Total Time',
              'title_vie' => 'Total Time',
              'url' => '/staff-time/total',
              'class' => ''
              );
             */
            $menu_time[] = array(
                'id'        => 1015,
                'parent_id' => 999,
                'postion'   => 6,
                'title'     => 'List Staff Leave',
                'title_vie' => 'List Staff Leave',
                'url'       => '/leave/list-leave-detail',
                'class'     => ''
            );
            // $menu_time[] =  array(
            // 		'id' => 1016,
            // 		'parent_id' => 999,
            // 		'postion' => 7,
            // 		'title' => 'Upload Time Final',
            // 		'title_vie' => 'Upload Time Final',
            // 		'url' => '/time-machine/upload-time-final',
            // 		'class' => ''
            // );

            $menu_time[] = array(
                'id'        => 1017,
                'parent_id' => 999,
                'postion'   => 3,
                'title'     => 'Set Permission Final',
                'title_vie' => 'Set Permission Final',
                'url'       => '/staff-permission/list-permission-final',
                'class'     => ''
            );
        } else {
            $sql_check_permission = "SELECT id 
                                        FROM `staff_permission`
                                        WHERE staff_code = :staff_code
                                        AND (is_leader > 0 OR is_manager > 0)";
            $stmt                 = $db->prepare($sql_check_permission);
            $stmt->bindParam('staff_code', $userStorage->code, PDO::PARAM_STR);
            $stmt->execute();

            $data_check = $stmt->fetch();
            $stmt->closeCursor();
            // 2123: dieu.le
            if ((!empty($data_check) AND ! in_array($userStorage->title, array(174))) || $userStorage->id == 341 || $userStorage->id == 2123) {

                $menu_time[] = array(
                    'id'        => 1001,
                    'parent_id' => 999,
                    'postion'   => 2,
                    'title'     => 'List Staff Time',
                    'title_vie' => 'List Staff Time',
                    'url'       => '/staff-time/list-staff-check-in',
                    'class'     => ''
                );
                /* $menu_time[] =  array(
                  'id' => 1002,
                  'parent_id' => 999,
                  'postion' => 2,
                  'title' => 'List Work Online',
                  'title_vie' => 'Danh Sách Làm Việc Online',
                  'url' => '/staff-time/show-online-time',
                  'class' => ''
                  ); */

                $menu_time[] = array(
                    'id'        => 1003,
                    'parent_id' => 999,
                    'postion'   => 2,
                    'title'     => 'Approve Time / Leave',
                    'title_vie' => 'Approve Time / Leave',
                    'url'       => '/staff-time/list-staff-approve',
                    'class'     => ''
                );

                if ($userStorage->id == 10557) {
                    $menu_time[] = array(
                        'id'        => 1000,
                        'parent_id' => 999,
                        'postion'   => 1,
                        'title'     => 'Time Machine',
                        'title_vie' => 'Máy chấm công',
                        'url'       => '/time-machine/luoi',
                        'class'     => ''
                    );
                }
                /*

                 */
                $menu_time[] = array(
                    'id'        => 1015,
                    'parent_id' => 999,
                    'postion'   => 6,
                    'title'     => 'List Staff Leave',
                    'title_vie' => 'List Staff Leave',
                    'url'       => '/leave/list-leave-detail',
                    'class'     => ''
                );
                // $menu_time[]=         array(
                //             'id' => 1016,
                //             'parent_id' => 999,
                // 'postion' => 7,
                //             'title' => 'Create Realme',
                //             'title_vie' => 'Báo số Realme',
                //             'url' => '/timing-realme/create',
                //             'class' => ''
                //         	);
            } else {
                // if(in_array($userStorage->title, array(PGPB_TITLE, PB_SALES_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE)))
                // {
                // 	$menu_time[] =  array(
                // 		'id' => 1002,
                // 		'parent_id' => 999,
                // 		'postion' => 5,
                // 		'title' => 'Check In GPS',
                // 		'title_vie' => 'Check In GPS',
                // 		'url' => '/time/pg-time-create',
                // 		'class' => ''
                // 	);
                // 	$menu_time[] =  array(
                // 			'id' => 1000,
                // 			'parent_id' => 999,
                // 			'postion' => 6,
                // 			'title' => 'Check Out GPS',
                // 			'title_vie' => 'Check Out GPS',
                // 			'url' => '/time/pg-check-out',
                // 			'class' => '');
                // 	$menu_time[] =  array(
                // 			'id' => 1015,
                // 			'parent_id' => 999,
                // 			'postion' => 6,
                // 			'title' => 'List Staff Leave',
                // 			'title_vie' => 'List Staff Leave',
                // 			'url' => '/leave/list-leave-detail',
                // 			'class' => ''
                // 	);
                // }

                $sql_check_group  = "SELECT id
				 					FROM `group`
				 					WHERE CONCAT(',',menu,',') LIKE '%,73,%'
				 						AND id = :group_id
				 						AND id <> 4";
                $stmt             = $db->prepare($sql_check_group);
                $stmt->bindParam('group_id', $userStorage->group_id, PDO::PARAM_INT);
                $stmt->execute();
                $data_group_check = $stmt->fetch();
                $stmt->closeCursor();

                // echo  $userStorage->title;

                if (in_array($userStorage->group_id, array(1, 5, 8, 9, 16, 18, 19, 21, 22, 24, 28, 29, 30, 14, 33, 12)) AND ! in_array($userStorage->title, array(174))) {
                    $menu_time[] = array(
                        'id'        => 1001,
                        'parent_id' => 999,
                        'postion'   => 1,
                        'title'     => 'List Staff Time',
                        'title_vie' => 'List Staff Time',
                        'url'       => '/staff-time/list-staff-check-in',
                        'class'     => ''
                    );
                    //        $menu_time[] =  array(
                    //             'id' => 1002,
                    //             'parent_id' => 999,
                    // 'postion' => 2,
                    //             'title' => 'List Work Online',
                    //             'title_vie' => 'Danh Sách Làm Việc Online',
                    //             'url' => '/staff-time/show-online-time',
                    //             'class' => ''
                    //        );

                    $menu_time[] = array(
                        'id'        => 1003,
                        'parent_id' => 999,
                        'postion'   => 2,
                        'title'     => 'Approve Time / Leave',
                        'title_vie' => 'Approve Time / Leave',
                        'url'       => '/staff-time/list-staff-approve',
                        'class'     => ''
                    );

                    $menu_time[] = array(
                        'id'        => 1015,
                        'parent_id' => 999,
                        'postion'   => 6,
                        'title'     => 'List Staff Leave',
                        'title_vie' => 'List Staff Leave',
                        'url'       => '/leave/list-leave-detail',
                        'class'     => ''
                    );
                    // $menu_time[]=         array(
                    //             'id' => 1016,
                    //             'parent_id' => 999,
                    // 'postion' => 7,
                    //             'title' => 'Create Realme',
                    //             'title_vie' => 'Báo số Realme',
                    //             'url' => '/timing-realme/create',
                    //             'class' => ''
                    //         	);
                }
            }

            $sql_check_machine  = "SELECT id FROM `check_in_machine_map` WHERE staff_code = :staff_code AND status = 1";
            $stmt               = $db->prepare($sql_check_machine);
            $stmt->bindParam('staff_code', $userStorage->code, PDO::PARAM_STR);
            $stmt->execute();
            $data_machine_check = $stmt->fetch();
            $stmt->closeCursor();

            if ((empty($data_machine_check) OR ( in_array($userStorage->department, array(150)))) AND $userStorage->id != 24980) {
                $menu_time[] = array(
                    'id'        => 1002,
                    'parent_id' => 999,
                    'postion'   => 5,
                    'title'     => 'Check In GPS',
                    'title_vie' => 'Check In GPS',
                    'url'       => '/time/pg-time-create',
                    'class'     => ''
                );

                $menu_time[] = array(
                    'id'        => 1000,
                    'parent_id' => 999,
                    'postion'   => 6,
                    'title'     => 'Check Out GPS',
                    'title_vie' => 'Check Out GPS',
                    'url'       => '/time/pg-check-out',
                    'class'     => '');


                $sql_check_group  = "SELECT id
				 					FROM `group`
				 					WHERE CONCAT(',',menu,',') LIKE '%,73,%'
				 						AND id = :group_id
				 						AND id <> 4";
                $stmt             = $db->prepare($sql_check_group);
                $stmt->bindParam('group_id', $userStorage->group_id, PDO::PARAM_INT);
                $stmt->execute();
                $data_group_check = $stmt->fetch();
                $stmt->closeCursor();
                if (!empty($data_group_check)) {
                    $menu_time[] = array(
                        'id'        => 1015,
                        'parent_id' => 999,
                        'postion'   => 6,
                        'title'     => 'List Staff Leave',
                        'title_vie' => 'List Staff Leave',
                        'url'       => '/leave/list-leave-detail',
                        'class'     => ''
                    );
                }
            }

            $stmt = $db   = null;
        }
        return $menu_time;
    }

}
