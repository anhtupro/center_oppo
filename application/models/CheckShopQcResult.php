<?php

class Application_Model_CheckShopQcResult extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_qc_result';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params )
    {
        $str_from_date_time = "'" . $params['from_date_time'] . "'";
        $str_to_date_time = "'" . $params['to_date_time'] . "'";

        $db = Zend_Registry::get("db");

        $select_sellout_dealer = $db->select()
                    ->from(['km' => 'kpi_by_model'], [
                        'dealer_id' => "IF(dp.id IS NOT NULL, dp.id, d.id)",
                        'sellout' => "SUM(km.qty)"
                    ])
            ->join(['s' => 'store'], 'km.store_id = s.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = s.d_id', [])
            ->joinLeft(['dp' => WAREHOUSE_DB . '.distributor'], 'd.parent = dp.id', [])
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date'])
            ->where('d.is_ka = ?', 0)
            ->group('dealer_id');



        $select_result_qc = "
            SELECT
                r.store_id,
                GROUP_CONCAT(IF(r.`type` = 1, r.`month`, NULL)) as month_local_reviewed,
                GROUP_CONCAT(IF(r.`type` = 2, r.`month`, NULL)) as month_leader_reviewed,
                SUM(IF(r.`type` = 1, 1, 0)) as count_month_local_reviewed,
                SUM(IF(r.`type` = 2, 1, 0)) as count_month_leader_reviewed
            FROM
                trade_marketing.check_shop_qc_result r 
            WHERE
                r.`quarter` = " . $params['quarter']
            . " AND r.`year` = " . $params['year']
            . " GROUP BY r.store_id
        ";



        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
                'store_name' => 's.name',
                'sellout_dealer' => 'sel.sellout',
                'month_qc_local_reviewed' => 'qc.month_local_reviewed',
                'month_qc_leader_reviewed' => 'qc.month_leader_reviewed',
                'count_month_qc_local_reviewed' => 'qc.count_month_local_reviewed',
                'count_month_qc_leader_reviewed' => 'qc.count_month_leader_reviewed',
                'dealer_id_parent' => "IF(dp.id IS NOT NULL, dp.id, d.id)"
//                'ch.count_month_active',
            ])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->joinLeft(['dp' => WAREHOUSE_DB . '.distributor'], 'd.parent = dp.id', [])
            ->joinLeft(['sel' => $select_sellout_dealer], 'sel.dealer_id = IF(dp.id IS NOT NULL, dp.id, d.id)', [])
//            ->joinLeft(array('ch' => new Zend_Db_Expr('(' . $select_eligile_checkshop . ')')), 'ch.store_id = s.id', array())
            ->joinLeft(array('qc' => new Zend_Db_Expr('(' . $select_result_qc . ')')), 'qc.store_id = s.id', array())

            ->where('s.del IS NULL OR s.del = 0')
            ->where('a.id IN (?)', $params['list_area'])
            ->where('d.is_ka = ?', 0)
            ->order('sel.dealer_id');

        if ($params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if ($params['store_name']) {
            $select->where('s.name LIKE ?', "%". TRIM($params['store_name']) . "%");
        }

        if ($params['big_area_id']) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($params['regional_market']) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if($params['district']) {
            $select->where('di.id = ?', $params['district']);
        }

        if ($params['eligigle_to_review']) {
            $select->where('sel.sellout >= ?', $params['rule_sellout_dealer']); // sellout để dealer cha phải >= 75
//            $select->where('ch.count_month_active > ?', 0); // chỉ cần 1 tháng có  đủ số lần check shop là được
        }

        if ($params['status_review'] == 1){ //tmk local chưa đánh giá đủ
//            $select->where('sel.sellout >= ?', $params['rule_sellout_dealer']); // sellout để dealer cha phải >= 75
            $select->where('qc.count_month_local_reviewed < 3 OR qc.count_month_local_reviewed IS NULL'); // 3 là 3 tháng trong quí

        }

        if ($params['status_review'] == 2){ // tmk leader chưa đánh giá
//            $select->where('sel.sellout >= ?', $params['rule_sellout_dealer']); // sellout để dealer cha phải >= 75
            $select->where('qc.count_month_leader_reviewed < 3 OR qc.count_month_leader_reviewed IS NULL');
        }

        if ($params['status_review'] == 3){ // hoàn thành đánh giá
//            $select->where('sel.sellout >= ?', $params['rule_sellout_dealer']); // sellout để dealer cha phải >= 75
            $select->where('qc.count_month_local_reviewed = ?', 3);
            $select->where('qc.count_month_leader_reviewed = ?', 3);
        }

        if ($_GET['dev'] == 1) {
          echo $select; die;
        }

        $select->limitPage($page, $limit);
//        echo $select;die;
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getQc($month, $year, $store_id, $type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE . '.check_shop_qc_result'], [
                'c.*'
            ])
            ->where('month = ?', $month)
            ->where('year = ?', $year)
            ->where('store_id = ?', $store_id)
            ->where('type = ?', $type);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getResultQc($quarter, $year, $store_id, $type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['r' => DATABASE_TRADE . '.check_shop_qc_result'], [
                'r.month',
                'r.year',
                'r.quarter',
                'qc_title' => 'c.title',
                'r.type',
                'r.created_at',
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'r.note'
            ])
            ->joinLeft(['c' => DATABASE_TRADE . '.app_checkshop_qc'], 'r.qc_id = c.id', [])
            ->joinLeft(['st' => 'staff'], 'r.created_by = st.id', [])
            ->where('r.quarter = ?', $quarter)
            ->where('r.year = ?', $year)
            ->where('r.store_id = ?', $store_id)
            ->where('r.type = ?', $type);

        $result = $db->fetchAll($select);

        $list = [];
        foreach ($result as $item) {
            $list [$item['month']] = $item;
        }

        return $list;
    }

    public function getCountEligibleCheckShop($from_date, $to_date, $store_id)
    {
        $db = Zend_Registry::get("db");
        $stmt_out = $db->prepare("CALL trade_marketing.`sp_count_eligible_check_shop`(:p_from_date, :p_to_date, :p_store_id)");
        $stmt_out->bindParam('p_from_date', $from_date);
        $stmt_out->bindParam('p_to_date', $to_date);
        $stmt_out->bindParam('p_store_id', $store_id, PDO::PARAM_INT);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();

        $list = [];
        foreach ($data as $item) {
            $list [$item['month']] = $item['count'];
        }

        return $list;
    }

    public function getSelloutStoreByMonth($store_id, $from_date, $to_date)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['km' => 'kpi_by_model'], [
                'km.store_id',
                'sellout' => "SUM(km.qty)",
                'month' => 'MONTH(km.timing_date)',
                'year' => 'YEAR(km.timing_date)'
            ])
            ->where('km.store_id = ?', $store_id)
            ->where('km.timing_date >= ?', $from_date)
            ->where('km.timing_date <= ?', $to_date)
            ->group(['km.store_id', 'MONTH(km.timing_date)', 'YEAR(km.timing_date)']);
        $result = $db->fetchAll($select);

        $list = [];
        foreach ($result as $item) {
            $list [$item['month']] = $item['sellout'];
        }

        return $list;

    }

    public function getDealerParent($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'dealer_id' => "IF(dp.id IS NOT NULL, dp.id, d.id)",
                'dealer_name' => "IF(dp.id IS NOT NULL, dp.title, d.title)"
            ])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->joinLeft(['dp' => WAREHOUSE_DB . '.distributor'], 'd.parent = dp.id', [])
            ->where('s.id = ?', $store_id);

        $dealer = $db->fetchRow($select);

        return $dealer;
                             
    }


    public function getSelloutDealer($store_id, $from_date, $to_date)
    {
        $dealer = $this->getDealerParent($store_id);
        $dealer_id = $dealer['dealer_id'];

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['km' => 'kpi_by_model'], [
                'sellout' => "SUM(km.qty)"
            ])
            ->join(['s' => 'store'], 'km.store_id = s.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = s.d_id', [])
            ->where('km.timing_date >= ?', $from_date)
            ->where('km.timing_date <= ?', $to_date)
            ->where('d.id = ? OR d.parent = ?', $dealer_id)
            ->where('d.is_ka = ?', 0);

        $sellout = $db->fetchOne($select);

        return $sellout;

    }


    public function getDataStore($params)
    {
        $db = Zend_Registry::get("db");

        $select_sellout_dealer = $db->select()
            ->from(['km' => 'kpi_by_model'], [
                'dealer_id' => "IF(dp.id IS NOT NULL, dp.id, d.id)",
                'sellout' => "SUM(km.qty)"
            ])
            ->join(['s' => 'store'], 'km.store_id = s.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = s.d_id', [])
            ->joinLeft(['dp' => WAREHOUSE_DB . '.distributor'], 'd.parent = dp.id', [])
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date'])
            ->where('d.is_ka = ?', 0)
            ->group('dealer_id');

        $select_sellout_store = $db->select()
            ->from(['km' => 'kpi_by_model'], [
                'km.store_id',
                'sellout' => "SUM(km.qty)"
            ])
            ->join(['s' => 'store'], 'km.store_id = s.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = s.d_id', [])
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date'])
            ->where('d.is_ka = ?', 0)
            ->group('km.store_id');


        
        $select_result_qc = "
            SELECT
                r.store_id,
                GROUP_CONCAT(IF(r.`type` = 1, r.`month`, NULL)) as month_local_reviewed,
                GROUP_CONCAT(IF(r.`type` = 2, r.`month`, NULL)) as month_leader_reviewed,
                SUM(IF(r.`type` = 1, 1, 0)) as count_month_local_reviewed,
                SUM(IF(r.`type` = 2, 1, 0)) as count_month_leader_reviewed
            FROM
                trade_marketing.check_shop_qc_result r 
            WHERE
                r.`quarter` = " . $params['quarter']
            . " AND r.`year` = " . $params['year']
            . " GROUP BY r.store_id
        ";



        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
                'store_name' => 's.name',
                'area_name' => 'a.name',
                'sellout_dealer' => 'sel.sellout',
                'sellout_store' => 'sos.sellout',
                'month_qc_local_reviewed' => 'qc.month_local_reviewed',
                'month_qc_leader_reviewed' => 'qc.month_leader_reviewed',
                'count_month_qc_local_reviewed' => 'qc.count_month_local_reviewed',
                'count_month_qc_leader_reviewed' => 'qc.count_month_leader_reviewed',
                'dealer_id_parent' => "IF(dp.id IS NOT NULL, dp.id, d.id)",
                'dealer_name_parent' => "IF(dp.id IS NOT NULL, dp.title, d.title)",
                'dealer_id' => 's.d_id',
                'dealer_name' => 'd.title',
                'store_address' => 's.shipping_address',
                'level' => 'lp.name'
            ])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->joinLeft(['dp' => WAREHOUSE_DB . '.distributor'], 'd.parent = dp.id', [])
            ->joinLeft(['lo' => 'dealer_loyalty'], 'lo.dealer_id = IF(dp.id IS NOT NULL, dp.id, d.id) AND lo.is_last = 1', [])
            ->joinLeft(['lp' => 'loyalty_plan'], 'lp.id = lo.loyalty_plan_id', [])
            ->joinLeft(['sel' => $select_sellout_dealer], 'sel.dealer_id = IF(dp.id IS NOT NULL, dp.id, d.id)', [])
            ->joinLeft(['sos' => $select_sellout_store], 'sos.store_id = s.id', [])
            ->joinLeft(array('qc' => new Zend_Db_Expr('(' . $select_result_qc . ')')), 'qc.store_id = s.id', array())

            ->where('s.del IS NULL OR s.del = 0')
            ->where('a.id IN (?)', $params['list_area'])
            ->where('d.is_ka = ?', 0)
            ->order('sel.dealer_id');

        if ($params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if ($params['store_name']) {
            $select->where('s.name LIKE ?', "%". TRIM($params['store_name']) . "%");
        }

        if ($params['big_area_id']) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($params['regional_market']) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if($params['district']) {
            $select->where('di.id = ?', $params['district']);
        }

        if ($params['eligigle_to_review']) {
            $select->where('sel.sellout >= ?', $params['rule_sellout_dealer']); // sellout để dealer cha phải >= 75
        }

        if ($params['status_review'] == 1){ //tmk local chưa đánh giá đủ
            $select->where('qc.count_month_local_reviewed < 3 OR qc.count_month_local_reviewed IS NULL'); // 3 là 3 tháng trong quí

        }

        if ($params['status_review'] == 2){ // tmk leader chưa đánh giá
            $select->where('qc.count_month_leader_reviewed < 3 OR qc.count_month_leader_reviewed IS NULL');
        }

        if ($params['status_review'] == 3){ // hoàn thành đánh giá
            $select->where('qc.count_month_local_reviewed = ?', 3);
            $select->where('qc.count_month_leader_reviewed = ?', 3);
        }

        if ($_GET['dev'] == 1) {
            echo $select; die;
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataQc($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['q' => DATABASE_TRADE . '.check_shop_qc_result'], [
                'q.*',
                'qc_title' => 'de.title'
            ])
            ->joinLeft(['de' => DATABASE_TRADE . '.app_checkshop_qc'], 'q.qc_id = de.id', [])
            ->joinLeft(['s' => 'store'], 'q.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])
            ->where('a.id IN (?)', $params['list_area'])
            ->where('q.quarter = ?', $params['quarter'])
            ->where('q.year = ?', $params['year']);

            
         if ($params['store_id']) {
             $select->where('s.id = ?', $params['store_id']);
         }

        if ($params['store_name']) {
            $select->where('s.name LIKE ?', "%". TRIM($params['store_name']) . "%");
        }

        if ($params['big_area_id']) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($params['regional_market']) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if($params['district']) {
            $select->where('di.id = ?', $params['district']);
        }
        
        $result = $db->fetchAll($select);

        $list = [];
        foreach ($result as $item) {
            $list [$item['store_id']] [$item['month']] [$item['type']] = $item;  // store_id => month => loại local/leader 1:local - 2:leader => kq đánh giá
        }

        return $list;
    }

    public function export($params)
    {
        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();
        $QArea      = new Application_Model_Area();
        $QAreaRankByMonth      = new Application_Model_AreaRankByMonth();
        $QQuarter = new Application_Model_Quarter();

        $data_store = $this->getDataStore($params);
        $data_qc = $this->getDataQc($params);
        $list_month = $QQuarter->fetchAll(['quarter = ?' => $params['quarter']])->toArray();


        $list_area = $QArea->getAreaKpi();

        $heads = array(

            'Store ID',
            'Dealer Name',
            'Dealer Parent Name (ID gộp)',
            'Dealer ID',
            'Store Name',
            'Store Address',
            'Area',
            'Dealer Parent ID (ID gộp)',

//            'Sellout Store',
            'Level'
        );

        foreach ($list_month as $month) {
          $heads [] = 'Local đánh giá T' . $month['month'];
          $heads [] = 'Local note T' . $month['month'];
          $heads [] = 'Leader đánh giá T' . $month['month'];
          $heads [] = 'Leader note T' . $month['month'];
        }


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data_store as $item_store){
            $alpha    = 'A';
            $item_qc = $data_qc[$item_store['store_id']];

            $sheet->setCellValue($alpha++.$index, $item_store['store_id']);
            $sheet->setCellValue($alpha++.$index, $item_store['dealer_name']);
            $sheet->setCellValue($alpha++.$index, $item_store['dealer_name_parent']);
            $sheet->setCellValue($alpha++.$index, $item_store['dealer_id']);

            $sheet->setCellValue($alpha++.$index, $item_store['store_name']);
            $sheet->setCellValue($alpha++.$index, $item_store['store_address']);
            $sheet->setCellValue($alpha++.$index, $item_store['area_name']);
            $sheet->setCellValue($alpha++.$index, $item_store['dealer_id_parent']);
//            $sheet->setCellValue($alpha++.$index, $item_store['sellout_store']);
            $sheet->setCellValue($alpha++.$index, $item_store['level']);

            foreach ($list_month as $month) {
                $sheet->setCellValue($alpha++.$index, $item_qc [$month['month']] [1] ['qc_title']); // local đánh giá
                $sheet->setCellValue($alpha++.$index, $item_qc [$month['month']] [1] ['note']); // local note
                $sheet->setCellValue($alpha++.$index, $item_qc [$month['month']] [2] ['qc_title']); // leader đánh giá
                $sheet->setCellValue($alpha++.$index, $item_qc [$month['month']] [2] ['note']); // leader note
            }

            $index++;
        }

        $filename = 'Đánh giá QC 1% Quý ' . $params['quarter'] . ' năm ' . $params['year'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;


    }


}