<?php
class Application_Model_PurchasingRatingFile extends Zend_Db_Table_Abstract
{
	protected $_name = 'purchasing_rating_file';

    public function remove($id){

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $where = $this->getAdapter()->quoteInto('id = ?',$id);
            $this->delete($where);
            $arr = array('code' => 1, 'message' => 'Done');
            $db->commit();
            return $arr;
        } catch (Exception $e) {
            $db->rollBack();
            $arr = array('code' => -1, 'message' => $e->getMessage());
            return $arr;
        }

    }
}