<?php
class Application_Model_PurchasingRatingStaffDetail extends Zend_Db_Table_Abstract
{
    protected $_name = "purchasing_rating_staff_detail"; 
       
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');
        
        if ($result === false) {
            
            $db = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('p' => 'staff'), array('p.id', 'staff_name' => "CONCAT(p.firstname,  ' ',p.lastname)")); 
            $select->where('p.off_date IS NULL');
            $select->where('p.department = ?', 400);

            $data = $db->fetchAll($select);
            $result = array(); 
           
            if ($data){
                foreach ($data as $k => $item){
                    $result[$item['id']] = $item['name'];                    
                }
            }
            
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
}
