<?php
/**
 * Created by PhpStorm.
 * User: hac
 * Date: 9/7/2015
 * Time: 4:01 PM
 */
class InsuranceController extends My_Controller_Action
{
    /**
     * DANH SÁCH NHÂN VIÊN THAM GIA BẢO HIỂM
     */

    public function init(){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if($userStorage->group_id == ADMINISTRATOR_ID || in_array($userStorage->id,array(21,2584,7)) ){

        }else{
            //throw new Exception('');
            $flashMessenger = $this->_helper->flashMessenger;
            $flashMessenger->setNamespace('error')->addMessage('No permission');
            $backUrl = HOST;
            $this->_redirect($backUrl);

        }
    }

    public function indexAction()
    {
        require_once 'insurance'.DIRECTORY_SEPARATOR.'index.php';
    }

    public function _export($data){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $alpha    = 'A';
        $index    = 1;
        $heads = array(
                'stt',
                'Công ty',
                'code',
                'Tên',
                'Ngày sinh',
                'chức vụ',
                'Số sổ',
                'Tăng',
                'Giảm',
                'Lương',
                'Có sổ',
                'Chốt sổ',
                'Trả sổ',
                'Lấy thẻ từ bảo hiểm',
                'Chia thẻ về khu vực',
                // 'thu hồi thẻ từ nhân viên',
                // 'trả thẻ cho BH',
                'Note'

            );
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha++ . $index, $key);
        }
        $index = 2;
        $stt = 1;
        foreach($data as $row):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit($stt++, PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['company_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            // $sheet->getCell($alpha++.$index)->setValueExplicit($row['unit_code_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['staff_code'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['staff_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['dob'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['title_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['insurance_number'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit(date('m/Y',strtotime($row['increase'])), PHPExcel_Cell_DataType::TYPE_STRING);
            $decrease = ($row['decrease']) ? date('m/Y',strtotime($row['decrease'])) : NULL;
            $sheet->getCell($alpha++.$index)->setValueExplicit($decrease, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['salary'], PHPExcel_Cell_DataType::TYPE_STRING);

            $have_book            = (isset($row['have_book']) AND $row['have_book']) ? 'x' : '';
            $close_book           = (isset($row['close_book']) AND $row['close_book']) ? 'x' : '';
            $return_book          = (isset($row['return_ins_book_time']) AND $row['return_ins_book_time']) ? 'x' : '';
            $take_card_from_ins   = (isset($row['take_card_from_ins']) AND $row['take_card_from_ins']) ? 'x' : '';
            $delivery_card        = (isset($row['delivery_card_ins']) AND $row['delivery_card_ins']) ? 'x' : '';
            $take_card_from_staff = (isset($row['take_card_from_staff']) AND $row['take_card_from_staff']) ? 'x' : '';
            $return_card_ins_time = (isset($row['return_card_ins_time']) AND $row['return_card_ins_time']) ? 'x' : '';

            $sheet->getCell($alpha++.$index)->setValueExplicit($have_book, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($close_book, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($return_book, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($take_card_from_ins, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($delivery_card, PHPExcel_Cell_DataType::TYPE_STRING);
            // $sheet->getCell($alpha++.$index)->setValueExplicit($take_card_from_staff, PHPExcel_Cell_DataType::TYPE_STRING);
            // $sheet->getCell($alpha++.$index)->setValueExplicit($return_card_ins_time, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCell($alpha++.$index)->setValueExplicit($row['note'], PHPExcel_Cell_DataType::TYPE_STRING);

            $index++;
        endforeach;

        $filename = 'c45';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function _exportc45($data){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'export-c45.php';
    }

    public function salaryAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $staff_id = $this->getRequest()->getParam('staff_id');
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'salary'),array('p.*'))
            ->where('staff_id = ?',$staff_id)
            ->order('time_effective DESC')
        ;
        $list   = $db->fetchAll($select);
        $QStaff = new Application_Model_Staff();
        $staff  = $QStaff->find($staff_id)->current();
        $this->view->staff = $staff;
        $this->view->list = $list;

        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $this->view->messages_success = $messages_success;
        $this->view->messages_error = $messages_error;
    }

    public function getAction(){
        $id = $this->getRequest()->getParam('id');
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'salary'),array('p.*'))
            ->where('p.id = ?',$id);
        $row = $db->fetchRow($select);
        if($row){
            $tmp = array(
                'salary' => $row['salary'],
                'time_effective' => date('d/m/Y',strtotime($row['time_effective'])),
                're_time_effective' => ($row['re_time_effective']) ? date('d/m/Y',strtotime($row['re_time_effective'])) : NULL
            );
            $result = array('result'=>1,'text'=>$tmp);
        }else{
            $result = array('result'=>-1);
        }
        $this->_helper->json->sendJson($result);
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        exit;
    }

    public function saveSalaryAction(){

        $id                  = $this->getRequest()->getParam('id');
        $staff_id            = $this->getRequest()->getParam('staff_id');
        $salary              = $this->getRequest()->getParam('salary');
        $time_effective      = $this->getRequest()->getParam('time_effective');
        $re_time_effective   = $this->getRequest()->getParam('re_time_effective',NULL);
        $create_sub_contract = $this->getRequest()->getParam('create_sub_contract',0);
        $flashMessenger      = $this->_helper->flashMessenger;
        $backUrl             = '/insurance/salary?staff_id='.$staff_id;

        if(!$staff_id){
            $flashMessenger->setNamespace('error')->addMessage('Please choose staff');
            $backUrl = '/staff-status';
            $this->_redirect($backUrl);
        }

        if(!$salary){
            $flashMessenger->setNamespace('error')->addMessage('Please choose salary');
            $this->_redirect($backUrl);
        }

        if(!$time_effective){
            $flashMessenger->setNamespace('error')->addMessage('Please choose time');
            $this->_redirect($backUrl);
        }

        $data = array(
            'staff_id'            => intval($staff_id),
            'salary'              => intval($salary),
            'time_effective'      => My_Date::normal_to_mysql($time_effective),
            'create_sub_contract' => $create_sub_contract
        );

        if(!$re_time_effective){
            $data['re_time_effective'] = NULL;
        }else{
            $data['re_time_effective'] = My_Date::normal_to_mysql($re_time_effective);
        }

        $QSalary     = new Application_Model_Salary();
        $db          = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $db->beginTransaction();
        try {
            if($id){
                $where = $QSalary->getAdapter()->quoteInto('id = ?',$id);
                $QSalary->update($data,$where);
            }else{

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_by'] = $userStorage->id;

                $selectDate = $db->select()
                    ->from(array('p'=>'salary'),array('p.*'))
                    ->where('staff_id = ?',$staff_id)
                    ->where('time_effective >= ?',$data['time_effective'])
                ;
                $resultDate =  $db->fetchRow($selectDate);
                if($resultDate){
                    $flashMessenger->setNamespace('error')->addMessage('Please choose correct time ');
                    $this->_redirect($backUrl);
                }else{

                    $id = $QSalary->insert($data);

                    if($create_sub_contract){

                        $QAsmContract = new Application_Model_AsmContract();
                        $select2 = $db->select()
                            ->from(array('p'=>'asm_contract'),array('p.*'))
                            ->where('p.staff_id = ?',$staff_id)
                            ->where('print_type = ?',1)
                            ->where('locked = 0 OR locked IS NULL')
                            ->order('id DESC')
                            ->limit(1)
                        ;
                        $rowAsmContract = $db->fetchRow($select2);

                        $dataAsm = array(
                            'staff_id'        => $staff_id,
                            'new_contract'    => $rowAsmContract['new_contract'],
                            'from_date'       => $data['time_effective'],
                            'to_date'         => $rowAsmContract['to_date'],
                            'asm_proposal'    => 0,
                            'note'            => 'phụ lục từ BH',
                            'hospital_id'     => $rowAsmContract['hospital_id'],
                            'status'          => 2,
                            'title'           => $rowAsmContract['title'],
                            'regional_market' => $rowAsmContract['regional_market'],
                            'print_type'      => 2,
                            'salary'          => $data['salary'],
                            'new_salary'      => 0,
                            'created_at'      => date('Y-m-d H:i:s'),
                            'created_by'      => $userStorage->id,
                            'total_salary'    => $rowAsmContract['total_salary'],
                            'allowance_1'     => $rowAsmContract['allowance_1'],
                            'allowance_2'     => $rowAsmContract['allowance_2'],
                            'allowance_3'     => $rowAsmContract['allowance_3'],
                            'salary_id'       => $id
                        );
                        $QAsmContract->insert($dataAsm);
                    }
                    
                }

            }
            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
            $this->_redirect($backUrl);
        } catch (Exception $e) {
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $this->_redirect($backUrl);
        }
    }

    public function delAction(){

        $id             = $this->getRequest()->getParam('id');
        $staff_id       = $this->getRequest()->getParam('staff_id');
        $backUrl        = '/insurance/salary?staff_id='.$staff_id;
        $flashMessenger = $this->_helper->flashMessenger;
        $QSalary        = new Application_Model_Salary();
        $db             = Zend_Registry::get('db');
        $db->beginTransaction();
        try{
            $select = $db->select()
                ->from(array('p'=>'salary'),array('p.*','ps.locked'))
                ->joinLeft(array('ps'=>'pre_status'),'p.staff_id = ps.staff_id AND p.time_effective = ps.time_effective',array())
                ->where('p.id = ?',$id)
                ->where('ps.locked = 0 OR ps.locked IS NULL')
                ;
            $result = $db->fetchRow($select);
            if($result){
                $where = $QSalary->getAdapter()->quoteInto('id = ?',$id);
                $QSalary->delete($where);

                // THÊM code chặn xóa HD khi in

                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Done');
            }else{
                $flashMessenger->setNamespace('error')->addMessage('Cannot Delete!');
            }
            
        }catch (Exception $e) {
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($backUrl);
    }

    public function reportAction(){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'report.php';
    }

    public function loadDataAction(){
        $back_url       = $this->getRequest()->getParam('back_url','/insurance/report');
        $flashMessenger = $this->_helper->flashMessenger;
        $date_bh        = $this->getRequest()->getParam('date_bh');
        $date           = My_Date::normal_to_mysql($date_bh);
         $db            = Zend_Registry::get('db');
        //$db->beginTransaction();
        try {
            $stmt = $db->query('CALL RunSocial(?)',array($date));
            $stmt->execute();
            //$db->commit();
            $flashMessenger->setNamespace("success")->addMessage('Done');
        } catch (Exception $e) {
            //$db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function processAction(){

        $flashMessenger = $this->_helper->flashMessenger;
        $QInsurance     = new Application_Model_Insurance();
        $delete         = $this->getRequest()->getParam('delete');
        $lock           = $this->getRequest()->getParam('lock');
        $ids            = $this->getRequest()->getParam('ids');
        $qdnv           = $this->getRequest()->getParam('qdnv');
        $back_url       = $this->getRequest()->getParam('back_url','/insurance/report');
        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $db             = Zend_Registry::get('db');
        $QInsuranceInfo = new Application_Model_InsuranceInfo();
        if(is_array($ids) AND count($ids) > 0){
            if($delete){
                $db->beginTransaction();
                try{
                    $where = array();
                    $where[] = $QInsurance->getAdapter()->quoteInto('locked != ?',1);
                    $where[] = $QInsurance->getAdapter()->quoteInto('del = ?',0);
                    $where[] = $QInsurance->getAdapter()->quoteInto('id IN (?)',$ids);
                    $where[] = $QInsurance->getAdapter()->quoteInto('del = ?',0);
                    $QInsurance->delete($where);
                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                    $this->_redirect($back_url);
                }catch (Exception $e){
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $db->rollBack();
                    $this->_redirect($back_url);
                }
            }

            if($lock){
                $db->beginTransaction();
                try{

                    $where = $QInsurance->getAdapter()->quoteInto('id IN (?)',$ids);
                    $data = array(
                        'locked'    => 1,
                        'locked_at' => date('Y-m-d H:i:s'),
                        'locked_by' => $userStorage->id,
                    );
                    $QInsurance->update($data,$where);   

                    $selectStaff = $db->select()
                        ->from(array('a'=>'pre_status'),array('staff_id'))
                        ->where('id IN (?)',$ids)
                        ->group('staff_id');
                    $listStaff = $db->fetchAll($selectStaff);

                    foreach($listStaff as $item){
                        $QInsuranceInfo->add($item['staff_id']);
                    }
                    

                    $db->commit();
                    $flashMessenger->setNamespace('success')->addMessage('Done');
                    $this->_redirect($back_url);
                }catch (Exception $e){
                    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
                    $db->rollBack();
                    $this->_redirect($back_url);
                }
            }

        }else{
            $flashMessenger->setNamespace('error')->addMessage('Please select item to process');
            $this->_redirect($back_url);
        }
        $flashMessenger->setNamespace('error')->addMessage('Error');
        $this->_redirect($back_url);
    }

    public function deleteAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $id = $this->getRequest()->getParam('id');
        $db = Zend_Registry::get('db');
        $QInsurance = new Application_Model_Insurance();
        $result = array();
        $db->beginTransaction();
        try{
            $where = $QInsurance->getAdapter()->quoteInto('id = ?',$id);
            $QInsurance->delete($where);
            $db->commit();
            $flashMessenger = $flashMessenger->setNamespace('success')->addMessage('Done');
            $this->_redirect('/insurance/report');
        }catch (Exception $e){
            $flashMessenger = $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
            $db->rollBack();
            $this->_redirect('/insurance/report');
        }
    }

    public function editAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $id = $this->getRequest()->getParam('id');
        $db = Zend_Registry::get('db');
        $cols  = array(
            'p.*',
            'staff_code' => 's.code',
            's.date_insurance_off',
            's.date_insurance_increase'
        );
        $select = $db->select()
            ->from(array('p'=>'pre_status'),$cols)
            ->join(array('s'=>'staff'),'p.staff_id = s.id',array())
            ->where('p.id = ?',$id);
        $result = $db->fetchRow($select);
        if(!$result){
            $this->_redirect('/insurance');
            $flashMessenger->setNamespace('error')->addMessage('Please select item');
        }

        $QStaff = new Application_Model_Staff();
        $staff = $QStaff->find($result['staff_id'])->current();
        $this->view->staff= $staff;

        $QCompany = new Application_Model_Company();
        $this->view->companies = $QCompany->get_cache();

        $QUnitCode                = new Application_Model_UnitCode();
        $this->view->unit_codes   = $QUnitCode->get_all();

        $QStatusOption = new Application_Model_StatusOption();

        $QTeam                   = new Application_Model_Team();
        $this->view->departments = $QTeam->get_department();

        $teams                   = $QTeam->get_team($result['department']);
        $this->view->teams       = $teams;

        $titles                  = $QTeam->get_team($result['team'],'vn');
        $this->view->titles      = $titles;

        $options                 = $QStatusOption->get_all();
        $this->view->options     = $options;
        $this->view->row = $result;

        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $this->view->uri = $uri;

        $messages_error = $flashMessenger->setNamespace('error')->getMessages();
        $messages = $flashMessenger->setNamespace('success')->getMessages();

        $this->view->messages_error = $messages_error;
        $this->view->messages_success = $messages;
    }

    public function saveAction(){
        $QInsurance              = new Application_Model_Insurance();
        $QInsuranceInfo = new Application_Model_InsuranceInfo();
        $flashMessenger          = $this->_helper->flashMessenger;
        $id                      = $this->getRequest()->getParam('id');
        $back_url                = $this->getRequest()->getParam('back_url','/insurance/report');
        $time_alter              = $this->getRequest()->getParam('time_alter');
        $real_time               = $this->getRequest()->getParam('real_time');
        $salary                  = $this->getRequest()->getParam('salary');
        $note                    = $this->getRequest()->getParam('note');
        $locked                  = $this->getRequest()->getParam('locked',0);
        $qdnv                    = $this->getRequest()->getParam('qdnv');


        if(!$id){
            $flashMessenger->setNamespace('error')->addMessage('Please select item');
            $this->_redirect($back_url);
        }

        $time_alter = (isset($time_alter) AND $time_alter) ? My_Date::normal_to_mysql($time_alter) : NULL;
        $real_time = (isset($real_time) AND $real_time) ? My_Date::normal_to_mysql($real_time) : NULL;

        $data = array(
            'time_alter' => $time_alter,
            'note'       => trim($note),
            'salary'     => intval($salary),
            'real_time'  => $real_time,
            'qdnv'       => $qdnv,
        );

        if($locked){
            $data['locked'] = $locked;
        }

        $row = $QInsurance->find($id)->current();
        if(isset($qdnv) AND $qdnv){
            if($this->checkQdnv($id,$data['qdnv'],$row['unit_code_id'],$row['option'])){
                $flashMessenger->setNamespace('error')->addMessage('Quyết định nghỉ việc `'.$qdnv.'` đã tồn tại!');
                $this->_redirect($back_url);

            }
        }


        $row        = $QInsurance->find($id)->current()->toArray();

        // update staff
        $dataStaff = array();
        if(in_array($row['option'],array(1,2))){
            $dataStaff = array(
                'date_insurance_increase' => $real_time
            );
        }else{
            $dataStaff = array(
                'date_insurance_off' => $real_time,
                'date_insurance_off_declare' => $time_alter
            );
        }


        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try{
            if($id){

                //update bảo hiểm
                $where      = $QInsurance->getAdapter()->quoteInto('id = ?',$id);
                $QInsurance->update($data,$where);

                //update insurance info
                if(isset($data['locked']) AND $data['locked']){
                    $QInsuranceInfo->add($row['staff_id']);    
                }

                //update date bên staff
                $QStaff     = new Application_Model_Staff();
                $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?',$row['staff_id']);
                $QStaff->update($dataStaff,$whereStaff);
            }

            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch (Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect($back_url);
    }

    public function printAction(){
        $ids = (array)$this->getRequest()->getParam('ids');
        $flashMessenger = $this->_helper->flashMessenger;
        if(count($ids) <= 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn đối tượng để in');
            $this->_redirect('insurance');
        }

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'pre_status'),array('p.*'))
            ->join(array('s'=>'staff'),'p.staff_id = s.id',array('s.firstname','s.lastname'))
            ->join(array('c'=>'company'),'c.id = p.company_id',array('company_name'=>'c.name'))
            ->join(array('t'=>'team'),'t.id = p.title',array('title_name'=>'name_vn'))
            ->where('p.id IN (?)',$ids)
            ->where('p.type = 2 AND p.option = 3')
            ->order(array('p.company_id ASC'))
        ;
        $list = $db->fetchAll($select);

        if(count($list) <= 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn đối tượng Giảm hẳn để in');
            $this->_redirect('/insurance');
        }

        $this->view->list = $list;
        $this->_helper->layout()->disableLayout(true);
    }

    public function printDcAction(){
        $ids = (array)$this->getRequest()->getParam('ids');

        $flashMessenger = $this->_helper->flashMessenger;

        if(count($ids) <= 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn đối tượng để in');
            $this->_redirect('insurance');
        }

        $QInsurance = new Application_Model_Insurance();
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'pre_status'),array('p.*',
                'reportDate'=> 'f_hr_sothutu_dieuchinhluong(p.time_alter)'
            ))
            ->join(array('s'=>'staff'),'p.staff_id = s.id',array('s.firstname','s.lastname','insurance_number'))
            ->join(array('c'=>'company'),'c.id = p.company_id',array('company_name'=>'c.name'))
            ->join(array('t'=>'team'),'t.id = p.title',array('title_name'=>'name_vn'))
            ->join(array('t2'=>'team'),'t2.id = p.old_title',array('old_title_name'=>'name_vn'))
            ->where('p.id IN (?)',$ids)
            ->where('p.option = 7')
            ->order(array('p.company_id ASC'))
        ;

        $list = $db->fetchAll($select);

        if(count($list) <= 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn đối tượng Giảm hẳn để in');
            $this->_redirect('insurance');
        }

        $company_id = $list[0]['company_id'];
        $so         = $list[0]['reportDate'];

        $this->view->so = $so;
        $this->view->company_id = $company_id;

        $date = explode('-',$list[0]['time_alter']);
        $this->view->date = $date;
        // var_dump($date);
        // exit;

        $this->view->list = $list;
        $this->_helper->layout()->disableLayout(true);
    }

    public function reportTangAction($params){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        $back_url       = (isset($params['back_url']) AND $params['back_url']) ? $params['back_url'] : '/insurance/report';
        $flashMessenger = $this->_helper->flashMessenger;
        $db             = Zend_Registry::get('db');

        if( isset($params['month']) AND $params['month'] ){
            $date = My_Date::normal_to_mysql($params['month']);
        }else{
            $flashMessenger->setNamespace('error')->addMessage("Please enter month to export!");
            $this->_redirect($back_url);
        }

        $date_ins  = (trim($params['date_ins'])) ? My_Date::normal_to_mysql($params['date_ins']) : NULL;
        $paramsStore = array(
            $date,
            $params['unit_code_id'],
            $params['type'],
            $params['locked'],
            $date_ins,
            isset($params['name']) ? $params['name']: NULL,
            isset($params['code']) ? $params['code'] : NULL,
            $params['before']
        );
     
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $alpha    = 'A';
        $index    = 1;
        $stmt     = $db->query('CALL TangLaoDongV2(?,?,?,?,?,?,?,?)',$paramsStore);
        $stmt->setFetchMode(Zend_Db::FETCH_NUM);
        $result   = $stmt->fetchAll();
        foreach($result as $row):
            $alpha = 'A';
            for($i=0;$i < count($row);$i++){
                if(in_array($i, array(2, 6)))
                {
                    $row[$i] = mb_convert_case($row[$i], MB_CASE_TITLE, 'UTF-8');
                }
                $sheet->getCell($alpha++.$index)->setValueExplicit( $row[$i], PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
            }
            $index++;
        endforeach;

        $filename = 'Tang_' . $params['date_bh'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function reportGiamAction($params){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        $db = Zend_Registry::get('db');

        if( isset($params['month']) AND $params['month'] ){
            $date = My_Date::normal_to_mysql($params['month']);
        }else{
            exit('Please enter month to export!');
        }

        $date_ins    = (trim($params['date_ins'])) ? My_Date::normal_to_mysql($params['date_ins']) : NULL;
         $paramsStore = array(
            $date,
            $params['unit_code_id'],
            $params['type'],
            $params['locked'],
            $date_ins,
            isset($params['name']) ? $params['name']: NULL,
            isset($params['code']) ? $params['code'] : NULL,
            $params['before']
        );

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $alpha    = 'A';
        $index    = 1;

        $stmt     = $db->query('CALL GiamLaoDong(?,?,?,?,?,?,?,?)',$paramsStore);
        $stmt->setFetchMode(Zend_Db::FETCH_NUM);
        $result   = $stmt->fetchAll();

        foreach($result as $row):
            $alpha = 'A';
            for($i=0;$i < count($row);$i++){
                if(in_array($i, array(2, 6)))
                {
                    $row[$i] = mb_convert_case($row[$i], MB_CASE_TITLE, 'UTF-8');
                }
                $sheet->getCell($alpha++.$index)->setValueExplicit( $row[$i], PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
            }
            $index++;
        endforeach;

        $filename = 'Giam_' . $params['date_bh'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function reportChotSoAction($params){
        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);
        $db             = Zend_Registry::get('db');
        if( isset($params['month']) AND $params['month'] ){
            $date = My_Date::normal_to_mysql($params['month']);
        }else{
            exit('Please enter month to export!');
        }

        $paramsStore = array(
            $date,
            $params['unit_code_id'],
        );

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();
        $alpha    = 'A';
        $index    = 1;
        $stmt     = $db->query('CALL ChotXo(?,?)',$paramsStore);
        $stmt->setFetchMode(Zend_Db::FETCH_NUM);
        $result   = $stmt->fetchAll();
        $stt = 1;
        foreach($result as $row):
            $alpha = 'A';
            $sheet->getCell($alpha++.$index)->setValueExplicit( $stt++, PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
            for($i=0;$i < count($row);$i++){
                $sheet->getCell($alpha++.$index)->setValueExplicit( $row[$i], PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
            }
            $index++;
        endforeach;

        $filename = 'ChotSo_' . $params['date_bh'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function reportTongAction($params){
        $date_bh  = $this->getRequest()->getParam('date_bh');

        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        if($this->getRequest()->isPost()){
            $db             = Zend_Registry::get('db');

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $PHPExcel->setActiveSheetIndex(0);
            $sheet    = $PHPExcel->getActiveSheet();
            $alpha    = 'A';
            $index    = 1;
            $stmt     = $db->query('CALL C45_report(?)',array(date('Y-m-d')));
            $stmt->setFetchMode(Zend_Db::FETCH_NUM);
            $result   = $stmt->fetchAll();


            $stt = 1;
            foreach($result as $row):
                unset($row[19]);
                $alpha = 'A';
                $sheet->getCell($alpha++.$index)->setValueExplicit($stt++, PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
                for($i=0;$i < count($row);$i++){
                    $sheet->getCell($alpha++.$index)->setValueExplicit( $row[$i], PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
                }
                $index++;
            endforeach;
            $filename = 'Tong';
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }
        echo 'Vui lòng quay lại!';
        exit;
    }

    public function reportGiaiTrinhLaoDongAction(){
        $date_bh  = $this->getRequest()->getParam('date_bh');

        set_time_limit(0);
        error_reporting(0);
        ini_set('memory_limit', -1);

        if($this->getRequest()->isPost()){
            $db             = Zend_Registry::get('db');

            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $PHPExcel->setActiveSheetIndex(0);
            $sheet    = $PHPExcel->getActiveSheet();
            $alpha    = 'A';
            $index    = 1;
            $date_bh = My_Date::normal_to_mysql($date_bh);
            $stmt     = $db->query('CALL GiaiTrinhLaoDong(?)',array($date_bh));
            $stmt->setFetchMode(Zend_Db::FETCH_NUM);
            $result   = $stmt->fetchAll();
            $stt = 1;
            foreach($result as $row):
                unset($row[19]);
                $alpha = 'A';
                $sheet->getCell($alpha++.$index)->setValueExplicit($stt++, PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
                for($i=0;$i < count($row);$i++){
                    $sheet->getCell($alpha++.$index)->setValueExplicit( $row[$i], PHPExcel_Cell_DataType::TYPE_STRING);//ma so bao hiem
                }
                $index++;
            endforeach;
            $filename = 'GiaiTrinhLaoDong';
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            $objWriter->save('php://output');
            exit;
        }
        echo 'Vui lòng quay lại!';
        exit;
    }

    public function showHistoryAction(){
        $staff_id         = $this->getRequest()->getParam('staff_id');
        $QInsurance       = new Application_Model_Insurance();
        $QStaff           = new Application_Model_Staff();
        $staff            = $QStaff->find($staff_id)->current();
        $this->view->staff = $staff;
        $list             = $QInsurance->showHistory($staff_id);
        $this->view->list = $list;
    }

    public function infoAction(){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'info.php';
    }

    public function saveInfoAction(){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'save-info.php';
    }

    public function logAction(){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'log.php';
    }

    public function changeAction(){
        $page = $this->getRequest()->getParam('page',1);
        $desc = $this->getRequest()->getParam('desc',1);
        $sort = $this->getRequest()->getParam('sort','created_at');
        $total = 0;
        $limit = LIMITATION;
        $params = array(
            'sort' => $sort,
            'desc' => $desc
        );

        $QInsuranceStaffChange = new Application_Model_InsuranceStaffChange();
        $list = $QInsuranceStaffChange->fetchPagination($page,$limit,$total,$params);
        $this->view->list = $list;
        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST.'insurance/change'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);

        $labels = array(
            'dob'       => 'Ngày sinh',
            'gender'    => 'Giới tính',
            'name'      => 'Họ tên',
            'ID_number' => 'Chứng minh',
        );

        $this->view->labels = $labels;

        $flashMessenger = $this->_helper->flashMessenger;
        $messages = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages = $messages;
        $this->view->messages_success = $messages_success;
    }

    public function readAction(){

        $flashMessenger = $this->_helper->flashMessenger;
        $ids = $this->getRequest()->getParam('id');
        $QInsuranceStaffChange = new Application_Model_InsuranceStaffChange();
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try{
            $data = array(
                'read' => date('Y-m-d H:i:s')
            );

            if(count($ids)){
                foreach($ids as $id){
                    $where = array();
                    $where[] = $QInsuranceStaffChange->getAdapter()->quoteInto('id = ?',$id);
                    $where[] = $QInsuranceStaffChange->getAdapter()->quoteInto('`read` IS NULL OR `read` <> 0',0);
                    $QInsuranceStaffChange->update($data,$where);
                    $QInsuranceStaffChange->approve($id,$data['read']);
                }
                $db->commit();
            }
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch (Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect('/insurance/change');
    }

    public function disableAction(){
        $ids = $this->getRequest()->getParam('ids');
        $db = Zend_Registry::get('db');
        $flashMessenger = $this->_helper->flashMessenger;
        $QInsurance = new Application_Model_Insurance();

        if(count($ids) == 0){
            $flashMessenger->setNamespace('error')->addMessage('Chon dong de xoa!');
            $this->_redirect('/insurance/recycle-bin');
        }

        $db->beginTransaction();
        try{
            $where = array();
            $where[] = $QInsurance->getAdapter()->quoteInto('locked = ?',0);
            $where[] = $QInsurance->getAdapter()->quoteInto('id IN (?)',$ids);
            $data = array(
                'del' => 1,
                'del_at' => date('Y-m-d H:i:s')
            );
            if(count($ids) > 0){
                $QInsurance->update($data,$where);
                $db->commit();
            }
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch(Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect('/insurance/report');
    }

    public function restoreAction(){
        $ids = $this->getRequest()->getParam('ids');
        $db = Zend_Registry::get('db');
        $flashMessenger = $this->_helper->flashMessenger;
        $QInsurance = new Application_Model_Insurance();

        if(count($ids) == 0){
            $flashMessenger->setNamespace('error')->addMessage('Chon dong restore!');
            $this->_redirect('/insurance/recycle-bin');
        }

        $db->beginTransaction();
        try{
            $where = array();
            $where[] = $QInsurance->getAdapter()->quoteInto('id = (?)',$ids);
            $data = array(
                'del' => 0
            );
            if(count($ids) > 0){
                $QInsurance->update($data,$where);
                $db->commit();
            }
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch(Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect('/insurance/recycle-bin');

    }

    public function recycleBinAction(){
        $page = $this->getRequest()->getParam('page',1);
        $sort = $this->getRequest()->getParam('sort');
        $desc = $this->getRequest()->getParam('desc');
        $code = $this->getRequest()->getParam('code');
        $total = 0;
        $limit = 20;

        $params = array(
            'sort' => $sort,
            'desc' => $desc,
            'code' => $code
        );
        $QInsurance = new Application_Model_Insurance();
        $list = $QInsurance->recycleBin($page,$limit,$total,$params);
        $this->view->list = $list;
        $this->view->desc   = $desc;
        $this->view->sort   = $sort;
        $this->view->params = $params;
        $this->view->limit  = $limit;
        $this->view->total  = $total;
        $this->view->url    = HOST.'insurance/recycle-bin/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset = $limit*($page-1);
    }

    public function getApprovedDateByMonthAction(){
        if($this->getRequest()->isXmlHttpRequest()){
            $this->_helper->layout()->disableLayout(true);
        }
        $month = $this->getRequest()->getParam('month');
        $QInsurance = new Application_Model_Insurance();
        $dates = $QInsurance->getDateIns($month);
        $dates_approved = $QInsurance->getDateInsApproved($month);
        $arr = array(
            'dates_ins' => $dates,
            'dates_approved' => $dates_approved
        );
        $this->_helper->json->sendJson($arr);
        exit;
    }

    public function approveActiveBeforeTermAction(){
        $flashMessenger = $this->_helper->flashMessenger;
        $ids = $this->getRequest()->getParam('id');
        $QInsuranceStaffActive = new Application_Model_InsuranceStaffActive();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try{
            $data = array(
                'status'      => 1,
                'approved_at' => date('Y-m-d H:i:s'),
                'approved_by' => $userStorage->id,
            );

            if(count($ids)){
                $where = array();
                $where[] = $QInsuranceStaffActive->getAdapter()->quoteInto('id IN (?)',$ids);
                $where[] = $QInsuranceStaffActive->getAdapter()->quoteInto('`status` IS NULL OR `status` = 0',0);
                $QInsuranceStaffActive->update($data,$where);
                $db->commit();
                $flashMessenger->setNamespace('success')->addMessage('Done');
            }else{
                $flashMessenger->setNamespace('error')->addMessage('Please select a item to approve');
            }
        }catch (Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect('/insurance/active-before-term');
    }

    public function activeBeforeTermAction(){
        $page   = $this->getRequest()->getParam('page',1);
        $total  = 0;
        $limit  = LIMITATION;
        $params = array();
        $QInsurance = new Application_Model_Insurance();
        $list = $QInsurance->fetchActiveBeforeTerm($page,$limit,$total,$params);
        $this->view->list = $list;

        $this->view->params           = $params;
        $this->view->limit            = $limit;
        $this->view->total            = $total;
        $this->view->url              = HOST.'insurance/active-before-term/'.( $params ? '?'.http_build_query($params).'&' : '?' );
        $this->view->offset           = $limit*($page-1);

        $flashMessenger               = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;

        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $this->view->uri = $uri;

        $off_types = unserialize(OFF_TYPE);
        $this->view->off_types = $off_types;
    }

    public function createRecordAction(){
        $db = Zend_Registry::get('db');
        $cols = array('id','staff_name' => 'CONCAT(s.firstname," ",s.lastname," - ",s.code," - ",t.name)');
        $select = $db->select()
            ->from(array('s' => 'staff'), $cols)
            ->join(array('t' =>'team'), 't.id = s.team', array())
            ->order('s.firstname');
        $staffs = $db->fetchAll($select);
        $this->view->staffs = $staffs;

        $QRegionalMarket = new Application_Model_RegionalMarket();
        $regionals = $QRegionalMarket->get_cache();
        $this->view->regionals = $regionals;

        $QUnitCode = new Application_Model_UnitCode();
        $unit_codes = $QUnitCode->get_cache();
        $this->view->unit_codes = $unit_codes;

        $QTeam = new Application_Model_Team();
        $this->view->departments = $QTeam->get_department();

        $teams = $QTeam->get_cache_team();
        $this->view->teams = $teams;

        $titles = $QTeam->get_cache_title();
        $this->view->titles = $titles;

        $QStatusOption = new Application_Model_StatusOption();
        $options = $QStatusOption->get_all();
        $this->view->options     = $options;

        $flashMessenger = $this->_helper->flashMessenger;
        $messages                     = $flashMessenger->setNamespace('error')->getMessages();
        $messages_success             = $flashMessenger->setNamespace('success')->getMessages();
        $this->view->messages         = $messages;
        $this->view->messages_success = $messages_success;
    }

    public function saveRecordAction(){
        $staff_id        = $this->getRequest()->getParam('staff_id');
        $type            = $this->getRequest()->getParam('type');
        $option          = $this->getRequest()->getParam('option');
        $time_effective  = $this->getRequest()->getParam('time_effective');
        $time_alter      = $this->getRequest()->getParam('time_alter');
        $note            = $this->getRequest()->getParam('note');
        $unit_code_id    = $this->getRequest()->getParam('unit_code_id');
        $department      = $this->getRequest()->getParam('department');
        $team            = $this->getRequest()->getParam('team');
        $title           = $this->getRequest()->getParam('title');
        $salary          = $this->getRequest()->getParam('salary');
        $del             = 0;
        $locked = 1;
        $rate = 32.5;
        $company_id = ($unit_code_id == 2 ) ? 2 : 1;
        $data = array(
            'staff_id'        => intval($staff_id),
            'type'            => intval($type),
            'option'          => intval($option),
            'time_effective'  => My_Date::normal_to_mysql($time_effective),
            'time_alter'      => My_Date::normal_to_mysql($time_alter),
            'note'            => trim($note),
            'department'      => intval($department),
            'team'            => intval($team),
            'title'           => intval($title),
            'company_id'      => $company_id,
            'unit_code_id'    => intval($unit_code_id),
            'salary'          => intval($salary),
            'del'             => $del,
            'time_created_at' => My_Date::normal_to_mysql($time_alter),
            'rate'            => $rate,
            'locked'          => $locked
        );
        $QInsurance = new Application_Model_Insurance();
        $db = Zend_Registry::get('db');
        $flashMessenger = $this->_helper->flashMessenger;
        $db->beginTransaction();
        try{
            //locked data khi thêm mới
            $QInsurance->insert($data);
            $db->commit();
            $flashMessenger->setNamespace('success')->addMessage('Done');
        }catch(Exception $e){
            $db->rollBack();
            $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
        }
        $this->_redirect('/insurance/report');
    }

    public function getStaffAction(){
        $staff_id = $this->getRequest()->getParam('staff_id');
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('a'=>'staff'),array('id','department','team','title','unit_code_id','b.current_salary','a.regional_market'))
                ->joinLeft(array('b'=>'v_salaryinfo'),'a.id = b.staff_id',array())
                ->where('a.id = ?',$staff_id)
        ;
        $staff = $db->fetchRow($select);
        $data = array();
        if($staff){
            $data = $staff;
            $status = 1;
        }else{
            $status = 0;
        }
        $this->_helper->json->sendJson(array('status'=>$status,'staff'=>$data));
    }

    public function checkQdnv($current_id,$qdnv,$unit_code_id,$option = 3){
        if(!trim($qdnv)){
            return false;
        }

        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('a'=>'pre_status'),array('a.*'))
                ->where('a.qdnv = ?',$qdnv)
                ->where('a.del = ?',0)
                ->where('a.id != ?',$current_id)
                ->where('a.unit_code_id = ?',$unit_code_id)
                ->where('a.option = ?',$option)
        ;
        $result = $db->fetchRow($select);
        if($result){
            return true;
        }
        return false;
    }

    public function listSalaryAction(){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'list-salary.php';
    }

    public function massUploadInsuranceBookAction(){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'mass-upload-insurance-book.php';
    }

    public function saveMassUploadInsuranceBookAction(){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'save-mass-upload-insurance-book.php';
    }

    public function getQdnv($month){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'qdnv.php';
    }

    /**
    * Gợi ý các quyết định
    */
    public function hintQdnvAction(){
        $time       = $this->getRequest()->getParam('time_alter');
        $company_id = $this->getRequest()->getParam('company_id');
        $option     = $this->getRequest()->getParam('option');

        $time_mysql = My_Date::normal_to_mysql($time);
        $date       = new DateTime($time_mysql);
        $day        = intval($date->format('d'));
        $month = null;
        if($day <= 15){
            $month = $date->format('m/Y');
        }else{
            $date->sub(new DateInterval('P1M'));
            $month = $date->format('m/Y');
        }
        $QInsurance = new Application_Model_Insurance();
        $result = $QInsurance->hintQdnv($month,$company_id,$option);
        if($result){
            $arr_result = array('code'=>1,'result'=>$result);
        }else{
            $arr_result = array('code'=>-1,'result'=>'failure');
        }
        $this->_helper->json->sendJson($arr_result);
        exit;
    }

    public function getQdNghiKhongLuong($month){
        require_once 'insurance'.DIRECTORY_SEPARATOR.'qd-nghi-khong-luong.php';
    }

    public function printKhongLuongAction(){
        $ids = (array)$this->getRequest()->getParam('ids');
        $flashMessenger = $this->_helper->flashMessenger;
        if(count($ids) <= 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn đối tượng để in');
            $this->_redirect('insurance');
        }

        $db = Zend_Registry::get('db');
        $cols = array('p.*',
                'off_from' => 'o.from_date',
                'off_to' => 'o.to_date'
            );
        $select = $db->select()
            ->from(array('p'=>'pre_status'),$cols)
            ->join(array('s'=>'staff'),'p.staff_id = s.id',array('s.firstname','s.lastname'))
            ->join(array('c'=>'company'),'c.id = p.company_id',array('company_name'=>'c.name'))
            ->join(array('t'=>'team'),'t.id = p.title',array('title_name'=>'name_vn'))
            ->join(array('o'=>'off_childbearing'),'o.staff_id = p.staff_id AND o.from_date = p.time_effective')
            ->where('p.id IN (?)',$ids)
            ->where('p.type = 2 AND (p.option = 5 OR p.option = 6 )')
            ->order(array('p.company_id ASC'))
        ;
        $list = $db->fetchAll($select);

        if(count($list) <= 0){
            $flashMessenger->setNamespace('error')->addMessage('Vui lòng chọn đối tượng Giảm hẳn để in');
            $this->_redirect('/insurance');
        }

        $this->view->list = $list;
        $this->_helper->layout()->disableLayout(true);
    }

}