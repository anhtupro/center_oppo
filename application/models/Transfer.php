<?php

class Application_Model_Transfer extends Zend_Db_Table_Abstract
{
    protected $_name = 'transfer';
    protected $_schema = DATABASE_TRADE;

    function getStatus($id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => DATABASE_TRADE . '.transfer'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.status'))
            ->joinleft(array('c' => DATABASE_TRADE . '.app_status'), 'c.status = a.status', array('status_id' => 'c.id', 'status_name' => 'c.name'));
        $select->where('a.id = ?', $id);
        $select->where('c.type = ?', 6);
        $status = $db->fetchRow($select);
        return $status;
    }

    public function GetAll()
    {
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT D.id id,C.name categoryname,S.name storename,sl,lydo,date,D.status status FROM ' . DATABASE_TRADE . '.' . $this->_name . ' D left join ' . DATABASE_TRADE . '.category C on C.id = D.category_id'
            . ' left join ' . DATABASE_TRADE . '.store S on S.id =D.store_id '
        );
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }

    public function GetList($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('T' => DATABASE_TRADE . '.' . $this->_name), array('id' => 'T.id', 'status' => 'T.status', 'staff_id' => 'T.staff_id', 'ngaytao' => 'T.created_at', 'reason' => 'T.reason_id'))
            ->joinLeft(array('C' => DATABASE_TRADE . '.category'), 'C.id = T.category_id', array('category_id' => 'C.id', 'categoryname' => 'C.name'))
            ->joinLeft(array('S' => DATABASE_CENTER . '.store'), 'S.id =T.store_from', array('storefrom_id' => 'S.id', 'storename_from' => 'S.name'))
            ->joinLeft(array('S1' => DATABASE_CENTER . '.store'), 'S1.id =T.store_to', array('storeto_id' => 'S1.id', 'storename_to' => 'S1.name'))
            ->joinLeft(array('AP' => DATABASE_TRADE . '.app_status'), 'AP.id =T.status', array('status_id' => 'AP.id', 'name_status' => 'AP.name', 'type_status' => 'AP.type'))
            ->joinLeft(array('APT' => DATABASE_TRADE . '.app_status_title'), 'APT.status_id =AP.id', array('title_status' => 'APT.title'))
            ->where('AP.type = 3 and T.id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function fetchPagination($page, $limit, &$total, $params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'store_from' => "s.name",
            'store_to' => "s2.name",
            'created_at' => "p.created_at",
            'created_by' => "p.staff_id",
            'status' => "p.status",
            'reject' => "p.reject",
            'reject_note' => "p.reject_note",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%
            'p.review_cost',
            'p.contract_number',
            'p.contractor_id',
            'p.remove',
            'p.remove_note',
            'date_finish' => 'rc.created_at',
            'area_from' => 'area_from.name',
            'area_to' => 'area_to.name'
        );

        $select->from(array('p' => DATABASE_TRADE . '.transfer'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_from', array());
        $select->joinLeft(array('s2' => 'store'), 's2.id = p.store_to', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('area_from' => 'area'), 'p.area_from = area_from.id', array());
        $select->joinLeft(array('area_to' => 'area'), 'p.area_to = area_to.id', array());

        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_status'), 'a.status = p.status AND a.type = 6', array());
        if (!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 6', array());
        }

        $select->joinLeft(array('q' => DATABASE_TRADE . '.transfer_price'), 'p.id = q.transfer_id AND q.status = (SELECT MAX(status) FROM trade_marketing.transfer_price WHERE transfer_id = p.id AND status <> 1 AND status <> 3 AND status <> 5)', array());
//        $select->where('p.remove IS NULL OR p.remove = 0');
        $select->joinLeft(['rc' => DATABASE_TRADE.'.transfer_confirm'], 'rc.transfer_id = p.id AND rc.transfer_status = 5', []);

        $select->where('p.remove = 0 AND p.reject = 0');


        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if ($params['contractor_id']) {
            $select->where('p.contractor_id = ?', $params['contractor_id']);
        }

        if ($params['contract_number']) {
            $select->where('p.contract_number = ?', $params['contract_number']);
        }

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?) OR p.area_from IN (?)', [$params['area_id'], $params['area_id']]);
            //$select->orwhere('p.area_from IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        //SEARCH
        if (!empty($params['area_id_search'])) {
            $select->where('r.area_id IN (?)', $params['area_id_search']);
            $select->orWhere('p.area_from IN (?)', $params['area_id_search']);
        }
        if (!empty($params['sale_id'])) {
            $select->where('l.staff_id = ?', $params['sale_id']);
        }
        if (!empty($params['status_id'])) {
            $select->where('p.status IN (?)', $params['status_id']);
        }
        if (!empty($params['name_from'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_from'] . '%');
        }
        if (!empty($params['name_to'])) {
            $select->where('s2.name LIKE ?', '%' . $params['name_to'] . '%');
        }

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.created_at) >= ?', $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.created_at) <= ?', $to_date);
        }


        //END SEARCH

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        if ($params['month']) {
            $select->where('MONTH(p.created_at) = ?', $params['month']);
        }

//        if ($params['year']) {
//            $select->where('YEAR(p.created_at) = ?', $params['year']);
//        }


        if ($params['review_cost']) {
            $select->where('p.review_cost = ?', $params['review_cost']);
        }

        $select->group('p.id');

        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }



        if ($_GET['dev'] == 2) {
            echo $select;
            exit;
        }

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    public function fetchPaginationReject($page, $limit, &$total, $params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'store_from' => "s.name",
            'store_to' => "s2.name",
            'created_at' => "p.created_at",
            'created_by' => "p.staff_id",
            'status' => "p.status",
            'reject' => "p.reject",
            'reject_note' => "p.reject_note",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%
            'p.review_cost',
            'p.contract_number',
            'p.contractor_id',
            'p.remove',
            'p.remove_note',
            'date_finish' => 'rc.created_at',
             'area_from' => 'area_from.name',
            'area_to' => 'area_to.name'
        );

        $select->from(array('p' => DATABASE_TRADE . '.transfer'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_from', array());
        $select->joinLeft(array('s2' => 'store'), 's2.id = p.store_to', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('area_from' => 'area'), 'p.area_from = area_from.id', array());
        $select->joinLeft(array('area_to' => 'area'), 'p.area_to = area_to.id', array());

        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_status'), 'a.status = p.status AND a.type = 6', array());
        if (!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 6', array());
        }

        $select->joinLeft(array('q' => DATABASE_TRADE . '.transfer_price'), 'p.id = q.transfer_id AND q.status = (SELECT MAX(status) FROM trade_marketing.transfer_price WHERE transfer_id = p.id AND status <> 1 AND status <> 3 AND status <> 5)', array());
//        $select->where('p.remove IS NULL OR p.remove = 0');
        $select->joinLeft(['rc' => DATABASE_TRADE.'.transfer_confirm'], 'rc.transfer_id = p.id AND rc.transfer_status = 5', []);

        $select->where('p.reject = 1 AND p.remove = 0');

        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if ($params['contractor_id']) {
            $select->where('p.contractor_id = ?', $params['contractor_id']);
        }

        if ($params['contract_number']) {
            $select->where('p.contract_number = ?', $params['contract_number']);
        }

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?) OR p.area_from IN (?)', [$params['area_id'], $params['area_id']]);
            //$select->orwhere('p.area_from IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        //SEARCH
        if (!empty($params['area_id_search'])) {
            $select->where('r.area_id IN (?)', $params['area_id_search']);
        }
        if (!empty($params['sale_id'])) {
            $select->where('l.staff_id = ?', $params['sale_id']);
        }
        if (!empty($params['status_id'])) {
            $select->where('p.status IN (?)', $params['status_id']);
        }
        if (!empty($params['name_from'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_from'] . '%');
        }
        if (!empty($params['name_to'])) {
            $select->where('s2.name LIKE ?', '%' . $params['name_to'] . '%');
        }

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.created_at) >= ?', $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.created_at) <= ?', $to_date);
        }


        //END SEARCH

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        if ($params['month']) {
            $select->where('MONTH(p.created_at) = ?', $params['month']);
        }

//        if ($params['year']) {
//            $select->where('YEAR(p.created_at) = ?', $params['year']);
//        }


        if ($params['review_cost']) {
            $select->where('p.review_cost = ?', $params['review_cost']);
        }

        $select->group('p.id');

        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }



        if ($_GET['dev'] == 2) {
            echo $select;
            exit;
        }

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    public function fetchPaginationRemove($page, $limit, &$total, $params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'store_from' => "s.name",
            'store_to' => "s2.name",
            'created_at' => "p.created_at",
            'created_by' => "p.staff_id",
            'status' => "p.status",
            'reject' => "p.reject",
            'reject_note' => "p.reject_note",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%
            'p.review_cost',
            'p.contract_number',
            'p.contractor_id',
            'p.remove',
            'p.remove_note',
            'date_finish' => 'rc.created_at',
            'area_from' => 'area_from.name',
            'area_to' => 'area_to.name'
        );

        $select->from(array('p' => DATABASE_TRADE . '.transfer'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_from', array());
        $select->joinLeft(array('s2' => 'store'), 's2.id = p.store_to', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('area_from' => 'area'), 'p.area_from = area_from.id', array());
        $select->joinLeft(array('area_to' => 'area'), 'p.area_to = area_to.id', array());

        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_status'), 'a.status = p.status AND a.type = 6', array());
        if (!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 6', array());
        }

        $select->joinLeft(array('q' => DATABASE_TRADE . '.transfer_price'), 'p.id = q.transfer_id AND q.status = (SELECT MAX(status) FROM trade_marketing.transfer_price WHERE transfer_id = p.id AND status <> 1 AND status <> 3 AND status <> 5)', array());
//        $select->where('p.remove IS NULL OR p.remove = 0');
        $select->joinLeft(['rc' => DATABASE_TRADE.'.transfer_confirm'], 'rc.transfer_id = p.id AND rc.transfer_status = 5', []);

        $select->where('p.remove = 1');

        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if ($params['contractor_id']) {
            $select->where('p.contractor_id = ?', $params['contractor_id']);
        }

        if ($params['contract_number']) {
            $select->where('p.contract_number = ?', $params['contract_number']);
        }

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?) OR p.area_from IN (?)', [$params['area_id'], $params['area_id']]);
            //$select->orwhere('p.area_from IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        //SEARCH
        if (!empty($params['area_id_search'])) {
            $select->where('r.area_id IN (?)', $params['area_id_search']);
        }
        if (!empty($params['sale_id'])) {
            $select->where('l.staff_id = ?', $params['sale_id']);
        }
        if (!empty($params['status_id'])) {
            $select->where('p.status IN (?)', $params['status_id']);
        }
        if (!empty($params['name_from'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_from'] . '%');
        }
        if (!empty($params['name_to'])) {
            $select->where('s2.name LIKE ?', '%' . $params['name_to'] . '%');
        }

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.created_at) >= ?', $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.created_at) <= ?', $to_date);
        }


        //END SEARCH

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        if ($params['month']) {
            $select->where('MONTH(p.created_at) = ?', $params['month']);
        }

//        if ($params['year']) {
//            $select->where('YEAR(p.created_at) = ?', $params['year']);
//        }


        if ($params['review_cost']) {
            $select->where('p.review_cost = ?', $params['review_cost']);
        }

        $select->group('p.id');

        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }



        if ($_GET['dev'] == 2) {
            echo $select;
            exit;
        }

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function GetArea($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select('A.area_id as area_id')
            ->from(array('A' => DATABASE_CENTER . '.asm'));
        if (isset($params['staff_id_area']) and $params['staff_id_area'])
            $select->where('A.staff_id = ?', $params['staff_id_area']);

        $result = $db->fetchOne($select);
        return $result;
    }

    public function GetStaffEmail($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select('S.email,ST.id')
            ->from(array('S' => DATABASE_CENTER . '.staff'));
        if (isset($params['id_staff']) and $params['id_staff'])
            $select->where('S.id = ?', $params['id_staff']);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function GetArea_Staff($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select('A.area_id')
            ->from(array('S' => DATABASE_TRADE . '.staff'))
            ->joinLeft(array('A' => DATABASE_TRADE . '.area_staff'), 'A.staff_id = S.id');
        if (isset($params['email']) and $params['email']) {
            $select->where('S.email = ?', $params['email']);
            $result = $db->fetchRow($select);
        }
        if (isset($params['email']) and $params['email'] and isset($params['Trade_Leader']) and $params['Trade_Leader']) {
            $select->where('S.email = ?', $params['email']);
            $result = $db->fetchAll($select);
        }
        if (isset($result) and $result)
            return $result;
    }

    public function getTransferDetails($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'transfer_id' => 'p.transfer_id',
            'category_id' => 'p.category_id',
            'quantity' => 'p.quantity',
            'imei' => 'p.imei',
            'category_name' => 'c.name',
        );

        $select->from(array('p' => DATABASE_TRADE . '.transfer_details'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = p.category_id', array());

        $select->where('p.transfer_id = ?', $params['id']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTransfer($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.created_at",
            "p.staff_id",
            "p.status",
            "store_from" => "s.name",
            "store_to" => "s2.name",
            "area_from" => "p.area_from",
            "area_to" => "p.area_to",
            "store_from_id" => "p.store_from",
            "store_to_id" => "p.store_to",
            "p.warehouse_to",
            "fullname" => "CONCAT(staff.firstname, ' ', staff.lastname)",
            "p.reject",
            "p.reject_note",
            "p.manager_approve",
            "p.remove",
            "p.remove_note",
            'contractor_name' => 'con.name',
            'p.note',
            'reject_by' => "CONCAT(staff1.firstname, ' ', staff1.lastname)",
            'remove_by' => "CONCAT(staff2.firstname, ' ', staff2.lastname)",
            'p.review_cost',
            'p.review_note',
            'p.contract_number',
            'p.month_debt'
        );

        $select->from(array('p' => DATABASE_TRADE . '.transfer'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_from', array());
        $select->joinLeft(array('s2' => 'store'), 's2.id = p.store_to', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->joinLeft(array('staff1' => 'staff'), 'staff1.id = p.reject_by', array());
        $select->joinLeft(array('staff2' => 'staff'), 'staff2.id = p.remove_by', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = s2.regional_market', array());
        $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = p.contractor_id', array());
        $select->where('p.id = ?', $params['id']);
        $result = $db->fetchRow($select);

        return $result;
    }

    public function tranferFromTo($store_from, $store_to, $category_id, $quantity, $imei_sn)
    {

        $result = [
            'code' => 1,
            'message' => 'Done'
        ];

        $QAppCheckshop = new Application_Model_AppCheckshop();
        $QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();

        $where = [];
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('store_id = ?', $store_from);
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
        $lock_from = $QAppCheckshop->fetchRow($where);

        $where = [];
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('store_id = ?', $store_to);
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
        $lock_to = $QAppCheckshop->fetchRow($where);

        if (empty($lock_from) OR empty($lock_to)) {
            $result = [
                'code' => 2,
                'message' => 'Shop is not lock.'
            ];
            return $result;
        }

        $where_from = [];
        $where_from[] = $QAppCheckshopDetail->getAdapter()->quoteInto('checkshop_id = ?', $lock_from['id']);
        $where_from[] = $QAppCheckshopDetail->getAdapter()->quoteInto('category_id = ?', $category_id);
        $from_details = $QAppCheckshopDetail->fetchRow($where_from);

        if (empty($from_details)) {
            $result = [
                'code' => 2,
                'message' => 'Shop details is null.'
            ];
            return $result;
        }

        $data_update = [];
        $data_update['quantity'] = $from_details['quantity'] - $quantity;

        if (!empty($imei_sn)) {
            $imei_transfer = $from_details['imei_sn'];

            $imei_exp = explode(',', $imei_sn);
            foreach ($imei_exp as $k => $v) {
                $imei = str_replace($v . ", ", "", $imei_transfer);
                $imei = rtrim(str_replace($v, "", $imei), ", ");
                $imei_transfer = $imei;
            }
            $data_update['imei_sn'] = $imei_transfer;
        }
        $QAppCheckshopDetail->update($data_update, $where_from);


        $where_to = [];
        $where_to[] = $QAppCheckshopDetail->getAdapter()->quoteInto('checkshop_id = ?', $lock_to['id']);
        $where_to[] = $QAppCheckshopDetail->getAdapter()->quoteInto('category_id = ?', $category_id);
        $to_details = $QAppCheckshopDetail->fetchRow($where_to);

        if (!empty($to_details)) {
            $data_update_to = [];
            $data_update_to['quantity'] = $quantity + $to_details['quantity'];

            if (!empty($imei_sn)) {
                $data_update_to['imei_sn'] = !empty($to_details['imei_sn']) ? $to_details['imei_sn'] . ', ' . $imei_sn : $imei_sn;
            }

            $QAppCheckshopDetail->update($data_update_to, $where_to);

        } else {
            $data_insert_to = [
                'category_id' => $category_id,
                'quantity' => $quantity,
                'checkshop_id' => $lock_to['id'],
                'imei_sn' => $imei_sn
            ];
            $QAppCheckshopDetail->insert($data_insert_to);

        }

        return $result;

    }

    public function transferFromShop($store_from, $category_id, $quantity, $imei_sn, $area_to, $app_checkshop_detail_child_id, $store_to, $transfer_id)
    {

        $result = [
            'code' => 1,
            'message' => 'Done'
        ];

        $QAppCheckshop = new Application_Model_AppCheckshop();
        $QAirInventoryAreaImei = new Application_Model_AirInventoryAreaImei();
        $QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();
        $QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
        $QAirInventoryArea = new Application_Model_AirInventoryArea();
        $QInventoryOrderIn = new Application_Model_InventoryOrderIn();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $where = [];
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('store_id = ?', $store_from);
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
        $checkshop_store_from_is_lock = $QAppCheckshop->fetchRow($where);

        if (!$checkshop_store_from_is_lock) {
            $result = [
                'code' => 2,
                'message' => 'Shop điều chuyển chưa được xác nhận số lượng!'
            ];
            return $result;
        }

        if (!$app_checkshop_detail_child_id) {
            $result = [
                'code' => 2,
                'message' => 'Không đủ số lượng hạng mục để điều chuyển'
            ];
            return $result;
        }


//        $store_from_detail_child = $QAppCheckshopDetailChild->fetchRow(['id = ?' => $app_checkshop_detail_child_id])->toArray();

        $checkshop_detail_child = $QAppCheckshopDetailChild->fetchRow(['id = ?' => $app_checkshop_detail_child_id]);

        if ($checkshop_detail_child['quantity'] <= 0) {
            $result = [
                'code' => 2,
                'message' => 'Không có hạng mục tại shop để điều chuyển!'
            ];
            return $result;
        }

        // chuyển tới kho khu vực
        if ($area_to) {
            // update checkshop_detail_child
            $QAppCheckshopDetailChild->update([
                'area_id' => $area_to,
                'is_warehouse' => 1,
                'checkshop_id' => Null
            ], ['id = ?' => $app_checkshop_detail_child_id]);
        }

        // chuyển tới shop khác
        if ($store_to) {
            $checkshop_store_to_is_lock = $QAppCheckshop->fetchRow([
                'store_id = ?' => $store_to,
                'is_lock = ?' => 1
            ]);
            if (!$checkshop_store_to_is_lock) {
                $result = [
                    'code' => 2,
                    'message' => 'Shop điều chuyển đến chưa được xác nhận số lượng!'
                ];
                return $result;
            }

            // update checkshop_detail_child
            $QAppCheckshopDetailChild->update([
                'checkshop_id' => $checkshop_store_to_is_lock['id']
            ], ['id = ?' => $app_checkshop_detail_child_id]);

            // update quantity tổng trong app_checkshop_detail
            $existed_category = $QAppCheckshopDetail->fetchRow([
                'checkshop_id = ?' => $checkshop_store_to_is_lock['id'],
                'category_id = ?' => $category_id
            ]);

            if ($existed_category) {
                $QAppCheckshopDetail->update([
                    'quantity' => $existed_category['quantity'] + $quantity,
                ], ['id = ?' => $existed_category['id']]);
            } else {
                $QAppCheckshopDetail->insert([
                    'checkshop_id' => $checkshop_store_to_is_lock['id'],
                    'category_id' => $category_id,
                    'quantity' => $quantity
                ]);
            }
        }

        // update checkshop_detail trong shop điều chuyển
        $store_from_detail = $QAppCheckshopDetail->fetchRow([
            'checkshop_id = ?' => $checkshop_store_from_is_lock['id'],
            'category_id = ?' => $category_id
        ]);

        $QAppCheckshopDetail->update([
            'quantity' => $store_from_detail['quantity'] - $quantity
        ], ['id = ?' => $store_from_detail['id']]);

        // cập nhật trạng thái của hạng mục
        if ($app_checkshop_detail_child_id) {
            $QAppCheckshopDetailChild->update([
                'in_processing' => Null
            ], ['id = ?' => $app_checkshop_detail_child_id]);
        }

        // Lưu lịch sử
        $QInventoryOrderIn->insert([
            'sn' => $imei_sn ? $imei_sn : Null,
            'category_id' => $category_id,
            'warehouse_id' => $area_to ? $area_to : Null,
            'quantity' => $quantity,
            'type' => 1,
            'from_source_id' => $transfer_id,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id
        ]);

        return $result;
    }

    public function getStatistic($params = null)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'channel' => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                               WHEN (d.is_ka = 1) THEN 'KA'
                               WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THUỜNG'
                          END",
            'p.name',
            'dealer_id' => 'IF(d.parent IS NULL OR d.parent = 0, d.id, d.parent)',
            'd.partner_id',
            'store_from' => 'tf.store_from',
            'area' => 'a.name',
            'reason' => 'tr.title',
            'category' => 'c.name',
            'td.quantity',
            'status' => 'status.name',
            'sp.first_price',
            'ep.last_price',
            'transfer_type' => "IF (td.transfer_type = 1, 'Nhà thầu điều chuyển', 'Khu vực điều chuyển')",
            'tf.created_at',
            'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
            'store_to' => "IF (tf.warehouse_to = 1, 'Kho', tf.store_to)"

        );

        // first price subquery
        $nestedSelectFirstPrice = $db->select();
        $arrCols_nest = [
            'transfer_details_id',
            'first_price' => 'SUM(total_price) + SUM(total_price) * 0.1'
        ];
        $nestedSelectFirstPrice->from(array('quo' => DATABASE_TRADE . '.transfer_quotation'), $arrCols_nest)
            ->where('status = 0')
            ->group('transfer_details_id');

        // last price subquery
        $nestedSelectLastPrice = $db->select();
        $arrCols_nest = [
            'transfer_details_id',
            'last_price' => 'SUM(total_price) + SUM(total_price) * 0.1'
        ];
        $nestedSelectLastPrice->from(array('quo' => DATABASE_TRADE . '.transfer_quotation'), $arrCols_nest)
            ->where('status = 2')
            ->group('transfer_details_id');


        $select->from(['tf' => DATABASE_TRADE . '.transfer'], $arrCols);
        $select->join(['p' => 'store'], 'tf.store_from = p.id', []);
        $select->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('l' => 'store_staff_log'), 'l.store_id = p.id AND l.released_at IS NULL AND l.is_leader = 1', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array());

        $select->joinLeft(array('td' => DATABASE_TRADE . '.transfer_details'), 'tf.id = td.transfer_id', array());
        $select->joinLeft(array('tr' => DATABASE_TRADE . '.transfer_reason'), 'td.transfer_reason_id = tr.id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'td.category_id = c.id', array());
        $select->joinLeft(array('status' => DATABASE_TRADE . '.app_status'), 'tf.status = status.status AND status.type = 6', array());
        $select->joinLeft(array('st' => 'staff'), 'tf.staff_id = st.id', array());

        $select->joinLeft(array('sp' => new Zend_Db_Expr('(' . $nestedSelectFirstPrice . ')')), 'td.id = sp.transfer_details_id', array());
        $select->joinLeft(array('ep' => new Zend_Db_Expr('(' . $nestedSelectLastPrice . ')')), 'td.id = ep.transfer_details_id', array());

        $select->where('p.del IS NULL OR p.del = 0');
        $select->where('tf.remove IS NULL OR tf.remove = 0');
        $select->group(['p.id', 'td.id']);

        if (!empty($params['staff_id'])) {
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function export($statistics)
    {
        include 'PHPExcel.php';
        $QTransfer = new Application_Model_Transfer();

        $objExcel = new PHPExcel();
        $objExcel->setActiveSheetIndex(0);

        $sheet = $objExcel->getActiveSheet()->setTitle('Transfer');

        $rowCount = 2;
        $index = 1;
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'KÊNH');
        $sheet->setCellValue('C1', 'ID SHOP CHUYỂN');
        $sheet->setCellValue('D1', 'ID SHOP NHẬN');
        $sheet->setCellValue('E1', 'DEALER ID');
        $sheet->setCellValue('F1', 'PARTNER ID');
        $sheet->setCellValue('G1', 'TÊN SHOP CHUYỂN');
        $sheet->setCellValue('H1', 'KHU VỰC');
        $sheet->setCellValue('I1', 'TÌNH TRẠNG');
        $sheet->setCellValue('J1', 'HẠNG MỤC ĐIỀU CHUYỂN');
        $sheet->setCellValue('K1', 'LOẠI ĐIỀU CHUYỂN');
        $sheet->setCellValue('L1', 'SỐ LƯỢNG');
        $sheet->setCellValue('M1', 'CHI PHÍ ĐẦU');
        $sheet->setCellValue('N1', 'CHI PHÍ CUỐI');
        $sheet->setCellValue('O1', 'NGÀY TẠO');
        $sheet->setCellValue('P1', 'NGƯỜI TẠO');


        foreach ($statistics as $statistic) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $statistic['channel']);
            $sheet->setCellValue('C' . $rowCount, $statistic['store_from']);
            $sheet->setCellValue('D' . $rowCount, $statistic['store_to']);
            $sheet->setCellValue('E' . $rowCount, $statistic['dealer_id']);
            $sheet->setCellValue('F' . $rowCount, $statistic['partner_id']);
            $sheet->setCellValue('G' . $rowCount, $statistic['name']);
            $sheet->setCellValue('H' . $rowCount, $statistic['area']);
            $sheet->setCellValue('I' . $rowCount, $statistic['status']);
            $sheet->setCellValue('J' . $rowCount, $statistic['category']);
            $sheet->setCellValue('K' . $rowCount, $statistic['transfer_type']);
            $sheet->setCellValue('L' . $rowCount, $statistic['quantity']);
            $sheet->setCellValue('M' . $rowCount, $statistic['first_price']);
            $sheet->setCellValue('N' . $rowCount, $statistic['last_price']);
            $sheet->setCellValue('O' . $rowCount, date('d-m-Y H:i:s', strtotime($statistic['created_at'])));
            $sheet->setCellValue('P' . $rowCount, $statistic['staff_name']);

            ++$index;
            ++$rowCount;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:Q' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'Q'; ++$i) {
            $sheet->getStyle($i . 1)->getFont()->setBold(true);
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'transfer';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
    }

    public function getCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE . '.transfer'], [
                'r.area_id',
                'total_cost' => 'SUM(q.total_price) + SUM(q.total_price) * 0.1'
            ])
            ->join(['s' => 'store'], 'd.store_from = s.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['q' => DATABASE_TRADE . '.transfer_price'], 'd.id = q.transfer_id AND q.status = (
                        SELECT MAX(status) FROM trade_marketing.transfer_price where transfer_id = d.id AND status <> 3 AND status <> 1
                     )', [])
            ->where('d.remove =  0 OR d.remove is null')
            ->where('q.id IS NOT NULL');


        if ($params['status_finish']) {
            $select->where('d.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if (!empty($params['staff_id'])) {
            $select->where('d.staff_id = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(d.created_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(d.created_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(d.created_at,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(d.created_at,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        $select->group('r.area_id');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $transferCost [$element['area_id']] = $element['total_cost'];
        }

        return $transferCost;
    }

    public function transferFromWarehouse($store_to, $category_id, $quantity, $imei_sn, $app_checkshop_detail_child_id, $area_from, $transfer_id)
    {
        $QAirInventoryArea = new Application_Model_AirInventoryArea();
        $QAirInventoryAreaImei = new Application_Model_AirInventoryAreaImei();
        $QInventoryOrderOut = new Application_Model_InventoryOrderOut();
        $QAppCheckshop = new Application_Model_AppCheckshop();
        $QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();
        $QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $checkshop_is_lock = $QAppCheckshop->fetchRow([
            'store_id = ?' => $store_to,
            'is_lock = ?' => 1
        ]);

        if (! $checkshop_is_lock) {
            $result = [
                'code' => 2,
                'message' => 'Shop điều chuyển đến chưa được xác nhận số lượng!'
            ];
            return $result;
        }


        if (! $app_checkshop_detail_child_id) {
            $result = [
                'code' => 2,
                'message' => 'Không đủ số lượng hạng mục để điều chuyển'
            ];
            return $result;
        }

        $QAppCheckshopDetailChild->update([
           'checkshop_id' => $checkshop_is_lock['id'],
            'area_id' => Null,
            'is_warehouse' => Null
        ], ['id = ?' => $app_checkshop_detail_child_id]);

        // update số lượng tổng checkshop category detail
        $existed_category = $QAppCheckshopDetail->fetchRow([
            'checkshop_id = ?' => $checkshop_is_lock['id'],
            'category_id = ?' => $category_id
        ]);
        if ($existed_category) {
            $imei_update = $existed_category['imei_sn'] ? $existed_category['imei_sn'] . ', ' . $imei_sn : $imei_sn;
            $QAppCheckshopDetail->update([
                'quantity' => $existed_category['quantity'] + $quantity,
                'imei_sn' => $imei_update ? $imei_update : Null
            ], ['id = ?' => $existed_category['id']]);
        } else {
            $QAppCheckshopDetail->insert([
                'checkshop_id' => $checkshop_is_lock['id'],
                'category_id' => $category_id,
                'quantity' => $quantity,
                'imei_sn' => $imei_sn ? TRIM($imei_sn) : Null
            ]);
        }

        //cập nhật trạng thái hạng mục
        if ($app_checkshop_detail_child_id) {
            $QAppCheckshopDetailChild->update([
                'in_processing' => Null
            ], ['id = ?' => $app_checkshop_detail_child_id]);
        }

        // Lưu lịch sử xuất kho
        $QInventoryOrderOut->insert([
            'sn' => $imei_sn ? $imei_sn : Null,
            'category_id' => $category_id,
            'warehouse_id' => $area_from ? $area_from : Null,
            'quantity' => $quantity,
            'type' => 1, // 1: điều chuyển, 2: tiêu hủy
            'from_source_id' => $transfer_id,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $userStorage->id
        ]);


        $result = [
            'code' => 1,
            'message' => 'Done'
        ];

        return $result;

    }

    public function get($transferId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['t' => DATABASE_TRADE.'.transfer'], [
                         't.*',
                         'contractor_name' => 'con.name'
                     ])
                    ->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = t.contractor_id', array())
                    ->where('t.id = ?', $transferId);

          $result = $db->fetchAll($select);

          return $result;
    }


    public function getStatisticReviewCost($params = null)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'channel' => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                               WHEN (d.is_ka = 1) THEN 'KA'
                               WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THUỜNG'
                          END",
            'name' => "IF (tf.warehouse_from = 1, 'Kho', p.name)",
            'dealer_id' => 'IF(d.parent IS NULL OR d.parent = 0, d.id, d.parent)',
            'd.partner_id',
            'store_from' =>  "IF (tf.warehouse_from = 1, 'Kho', tf.store_from)",
            'area' => 'a.name',
            'status' => 'status.name',
            'tf.created_at',
            'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
            'store_to' => "IF (tf.warehouse_to = 1, 'Kho', tf.store_to)",
            'review_cost' => "IF(tf.review_cost = 1, 'Đúng', IF (tf.review_cost = 2, 'Sai', ''))"
        );


        $select->from(['tf' => DATABASE_TRADE . '.transfer'], $arrCols);
        $select->joinLeft(['p' => 'store'], 'tf.store_from = p.id', []);
        $select->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array());

        $select->joinLeft(array('status' => DATABASE_TRADE . '.app_status'), 'tf.status = status.status AND status.type = 6', array());
        $select->joinLeft(array('st' => 'staff'), 'tf.staff_id = st.id', array());

        $select->where('p.del IS NULL OR p.del = 0');
        $select->where('tf.remove IS NULL OR tf.remove = 0');
        $select->order('tf.created_at DESC');

        if (!empty($params['staff_id'])) {
            $select->where('l.staff_id = ?', $params['staff_id']);
        }


        if (!empty($params['area_id'])) {
            $str_area_id = implode(',',$params['area_id']);
            $select->where('r.area_id IN ('.  $str_area_id . ') OR tf.area_from IN (' . $str_area_id . ')');
        }

        if (!empty($params['big_area_id'])) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if (!empty($params['review_cost'])) {
            $select->where('tf.review_cost = ?', $params['review_cost']);
        }

        if (!empty($params['from_date'])) {
            $select->where('tf.created_at >= ?', $params['from_date']);
        }

        if (!empty($params['to_date'])) {
            $select->where('tf.created_at <= ?', $params['to_date']);
        }

        $result = $db->fetchAll($select);


        return $result;

    }

    public function exportReviewCost($data)
    {
        ini_set("memory_limit", -1);
        ini_set("display_error", 1);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'KÊNH',
            'ID SHOP CHUYỂN',
            'ID SHOP NHẬN',
            'DEALER ID',
            'PARTNER ID',
            'TÊN SHOP CHUYỂN',
            'KHU VỰC',
            'TÌNH TRẠNG',
            'NGÀY TẠO',
            'NGƯỜI TẠO',
            'TRẠNG THÁI CHI PHÍ'

        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){


            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_from']);
            $sheet->setCellValue($alpha++.$index, $item['store_to']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['name']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['status']);
            $sheet->setCellValue($alpha++.$index, date('d-m-Y H:i:s', strtotime($item['created_at'])));
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['review_cost']);

            $index++;

        }



        $filename = 'Điều chuyển tổng quát' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }


}































