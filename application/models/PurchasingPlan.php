<?php
class Application_Model_PurchasingPlan extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_plan';

    public function getPurchasingPlan($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "r.id",
            "group_title" => "g.title",
            "group_type_title" => "type.title",
            "team_name" => "t.name",
            "supplier_name" => "s.title",
            "r.request_type_group", 
            "r.month", 
            "r.year", 
            "r.campaign_id",
            "r.request_type",
            "r.team_id", 
            "r.supplier_id", 
            "r.details",
            "r.note",
            "info_id" => "i.id",
            "i.project_id",
            "i.price",
            "i.month",
            "i.year",
            "i.currency",
            "i.from_date", 
            "i.to_date",
            "project_name" => "project.title",
            "purchasing.sn",
        );

        $select->from(array('p' => 'purchasing_plan'), $arrCols);
        $select->joinLeft(array('purchasing' => 'purchasing_request'), 'purchasing.id = p.purchasing_id', array());
        $select->joinLeft(array('i' => DATABASE_SALARY.'.request_office_plan_info'), 'i.id = p.plan_id', array());
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office_plan'), 'r.id = i.rq_plan_id', array());
        $select->joinLeft(array('g' => 'request_type_group'), 'g.id = r.request_type_group', array());
        $select->joinLeft(array('type' => 'request_type'), 'type.id = r.request_type', array());
        $select->joinLeft(array('t' => 'team'), 't.id = r.team_id', array());
        $select->joinLeft(array('s' => 'supplier'), 's.id = r.supplier_id', array());
        $select->joinLeft(array('project' => 'project'), 'project.id = i.project_id', array());

        $select->where('p.del = 0');
        $select->where('i.del = 0');
        $select->where('purchasing.sn = ?', $params['sn']);
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getRequestOfficePlan($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_office_id",
            "p.plan_info_id",
            "info_id" => "p.plan_info_id",
            "p.use",
            "group_title" => "g.title",
            "i.month",
            "i.year",
            "project_name" => "project.title",
            "group_type_title" => "type.title",
            "team_name" => "t.name",
            "supplier_name" => "s.title",
            "r.details",
            "i.from_date",
            "i.to_date",
            "r.note",
            "i.price",
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office_plan_map'), $arrCols);
        $select->joinLeft(array('i' => DATABASE_SALARY.'.request_office_plan_info'), 'i.id = p.plan_info_id', array());
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office_plan'), 'r.id = i.rq_plan_id', array());
        $select->joinLeft(array('g' => 'request_type_group'), 'g.id = r.request_type_group', array());
        $select->joinLeft(array('type' => 'request_type'), 'type.id = r.request_type', array());
        $select->joinLeft(array('t' => 'team'), 't.id = r.team_id', array());
        $select->joinLeft(array('s' => 'supplier'), 's.id = r.supplier_id', array());
        $select->joinLeft(array('project' => 'project'), 'project.id = i.project_id', array());

        $select->where('p.is_del = 0');
        $select->where('p.request_office_id = ?', $params['request_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
}