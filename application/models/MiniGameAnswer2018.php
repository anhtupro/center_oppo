<?php
class Application_Model_MiniGameAnswer2018 extends Zend_Db_Table_Abstract
{
    protected $_name = 'mini_game_answer_2018';

    public function getAnswer($params){
		$db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.answer'
        );
        $select->from(array('p' => 'mini_game_answer_2018'), $arrCols);        
        $select->where('p.question_id = ?', $params['question_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
        return $result;
	}

    public function getListAnswer($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.question_id',
            'p.answer'
        );
        $select->from(array('p' => 'mini_game_answer_2018'), $arrCols);
        $select->where('p.round = ?', $params['round']);
//        echo $select->__toString();die;
        $result = $db->fetchAll($select);

        return $result;
    }
}