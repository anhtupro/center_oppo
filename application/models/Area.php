<?php
class Application_Model_Area extends Zend_Db_Table_Abstract
{
	protected $_name = 'area';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        if (isset($params['leader_ids']) and $params['leader_ids'])
            $select->where('FIND_IN_SET(?, p.leader_ids)', $params['leader_ids']);

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {
			$where = $this->getAdapter()->quoteInto('del = ?', 0);
            $data = $this->fetchAll($where, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
     function get_cache_all_with_del(){
  
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_all_del');

        if ($result === false) {	
       
            $data = $this->fetchAll();
          
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache_all_del', array(), null);
        }
        return $result;
    }

    function get_cache_bi(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_bi');

        if ($result === false) {

            $where = $this->getAdapter()->quoteInto('status = ?', 1 );
            $data  = $this->fetchAll($where, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache_bi', array(), null);
        }
        return $result;
    }

    function get_index_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_index_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data)
                foreach ($data as $item)
                    $result[] = array(
                        'id' => $item->id,
                        'name' => $item->name
                    );

            $cache->save($result, $this->_name.'_index_cache', array(), null);
        }
        return $result;
    }

    function get_cache2(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache2');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item;
                }
            }
            $cache->save($result, $this->_name.'_cache2', array(), null);
        }
        return $result;
    }

    function get_ASM_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_ASM_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();

            $QStaff = new Application_Model_Staff();

            if ($data){
                foreach ($data as $item){

                    $sStaffs = '';
                    //get staffs
                    if ($item['leader_ids']){
                        $ASM_ids = explode(',',$item['leader_ids']);
                        $where = $QStaff->getAdapter()->quoteInto('id IN (?)', $ASM_ids);

                        $ASMs = $QStaff->fetchAll($where);

                        if ($ASMs->count())
                            foreach ($ASMs as $k=>$ASM){
                                if ($k>0)
                                    $sStaffs .= "\n".$ASM->firstname.' '.$ASM->lastname;
                                else
                                    $sStaffs .= $ASM->firstname.' '.$ASM->lastname;
                            }
                    }

                    $result[$item->id] = $sStaffs;
                }
            }
            $cache->save($result, $this->_name.'_ASM_cache', array(), null);
        }
        return $result;
    }

    public function GetAll(){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT id, name FROM area
        ');
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    
    
    function get_cache_showroom()
    {
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_get_showroom');

        if ($result === false)
        {
            $db = Zend_Registry::get('db');
            $select = $db->select()
            ->from(array('p' => CS_DB.'.showroom'),
                array(new Zend_Db_Expr('p.id'), 'p.*'));
        
            $data = $db->fetchAll($select);

            $result = array();
            if ($data)
            {
                foreach ($data as $item)
                {
                    $result[$item['id']] = $item['name'];
                }
            }
                  
            $cache->save($result, $this->_name.'_cache_get_showroom', array(), null);
        }
        return $result;
    }
    
    function getAreaList($params)
    {
        $where = [];
        $where[] = $this->getAdapter()->quoteInto('status = ?', 1 );
        
        if(!empty($params['list_area'])){
            $where[] = $this->getAdapter()->quoteInto('id IN (?)', $params['list_area'] );
        }
        
        $data  = $this->fetchAll($where, 'name');

        $result = array();
        if ($data){
            foreach ($data as $item){
                $result[$item->id] = $item->name;
            }
        }
        
        return $result;
    }

    public function getListArea($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => 'area'], [
                         'a.id',
                         'a.name'
                     ])
                    ->where('a.status = 1')
//                    ->where('a.bigarea_id IS NOT NULL')
                    ->order('a.name');
        if ($params['list_assign_area_id']) {
            $select->where('a.id IN (?)', $params['list_assign_area_id']);
        }
        if ($params['area_list']) {
            $select->where('a.id IN (?)', $params['area_list']);
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    /**
     * @param $staffId
     * @return mixed
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getAreaByAsm($staffId)
    {
        $query = "select rm.area_id
from staff s
join regional_market rm on s.id = :staff_id and s.regional_market = rm.id";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result['area_id'];
    }

    public function getAreaByStore($storeId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'r.area_id'
            ])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->where('s.id = ?', $storeId);
        $result = $db->fetchOne($select);

        return $result;
    }

    public function getBigArea()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['b' => 'bigarea'], [
                'b.*'
            ]);
        $result = $db->fetchAll($select);

        return $result;
    }
    public function getAreaByBigArea($bigAreaId,$listArea=null){
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => $this->_name], ['id','name']);
       
        if(!empty($bigAreaId)){
            $select->where('a.bigarea_id IN(?)',$bigAreaId);
        }
        if(!empty($listArea)){
            $select->where('a.id IN(?)',$listArea);
        }
        $result = $db->fetchAll($select);
        $arrTmp=array();
        foreach ($result as $key => $value) {
            # code...
            $arrTmp[$value['id']]=$value['name'];
        }
        return $arrTmp;

    }

    public function getAreaKpi()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => 'area'], [
                'a.*'
            ])
            ->where('a.is_kpi = ?', 1)
        ->order('name');
        $result = $db->fetchAll($select);

        foreach ($result as $item) {
            $list [$item['id']] = $item['name'];
    }

        return $list;
    }

    public function getAreaByTitle()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => 'area'], [
                'a.id',
                'a.name'
            ])
            ->where('a.is_kpi = ?', 1)
        ->order('a.name');

        if (in_array($userStorage->title, [RM_TITLE, RM_STANDBY_TITLE, SALES_ADMIN_TITLE, PARTNER_TITLE])) {
            $select->join(['asm' => 'asm'], 'a.id = asm.area_id AND asm.staff_id = ' . $userStorage->id, []);
        } elseif (in_array($userStorage->title,[SALES_MANAGER_TITLE, KEY_ACCOUNT_MANAGER_TITLE, TECHNOLOGY_MANAGER_TITLE, KEY_ACCOUNT_SUPERVISOR_TITLE, SALES_ASSISTANT_TITLE, GTM_PO_ASSISTANT_TITLE, CEO_ASSISTANT_TITLE])) {
            // see all
        } else {
            $select->where('1=0');
        }
        
        $result = $db->fetchAll($select);

        return $result;
            
    }

    public function getAreaByStaff($staff_id)
    {
        $db = Zend_Registry::get("db");
        //  priviledge area
        $select = $db->select()
            ->from(['a' => 'asm'], [
                'a.area_id'
            ])
            ->where('a.staff_id = ?', $staff_id);
        $list_area = $db->fetchCol($select);

        // staff see all area
        $select_all_area = $db->select()
            ->from(['p' => 'privilege_all_area'], [
                'a.id'
            ])
            ->joinLeft(['a' => 'area'], 'a.is_kpi = 1', [])
            ->where('p.staff_id = ?', $staff_id);
        $list_area_all = $db->fetchCol($select_all_area);

        return $list_area ? $list_area : $list_area_all;


    }
}