<?php
class Application_Model_CategoryCosting extends Zend_Db_Table_Abstract
{
    protected $_name = 'category_costing';
   // protected $_schema = DATABASE_COST;

     public function getCategory()
	    {
	    	$db     = Zend_Registry::get("db");
	        $select = $db->select();

	        $arrCols = array(
	            'p.id', 
	            'name'        => 'p.name' 
	        );
	        $select->from(array('p'=> 'category_costing'), $arrCols);
	        $result = $db->fetchAll($select);
	        return $result;
	    }
}
