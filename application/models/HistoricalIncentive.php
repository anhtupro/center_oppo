<?php
/**
 *
 */

class Application_Model_HistoricalIncentive extends Zend_Db_Table_Abstract
{
    protected $_name = 'historical_incentive';

    /**
     * @param  [type] $page   [description]
     * @param  [type] $limit  [description]
     * @param  [type] $total [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        if (isset($params['export']) && $params['export']) {
            $get = array(
                'p.level',
                'd.product_id',
                'd.quantity',
                'd.value_unit',
                'd.total',
            );
        } else {
            $get = array('p.level', 'p.total');
        }

        $default_get = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.distributor_id');

        $select = $db->select();

        if (isset($params['export']) && $params['export']) {
            $select
                ->from(array('p' => $this->_name), $default_get)
                ->join(array('d' => 'historical_incentive_detail'), 'p.id=d.historical_incentive_id', $get);
        } else {
            $select->from(array('p' => $this->_name), array_merge($default_get, $get));
        }

        $select->join(
            array('dt' => WAREHOUSE_DB.'.distributor'),
            'dt.id=p.distributor_id',
            array('dt.store_code', 'dt.district', 'dt.title')
        );

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select->where('p.distributor_id = ?', $params['distributor_id']);
        }

        if (isset($params['month']) and date_create_from_format("m/Y", $params['month'])) {
            $select->where(
                'DATE(FROM_UNIXTIME(p.month)) = ?',
                date_create_from_format("m/Y", $params['month'])->format("Y-m-01")
            );
        }

        if (isset($params['name']) and $params['name']) {
            $select->where('dt.title LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['product_id']) and $params['product_id']) {
            $select->where('d.product_id = ?', $params['product_id']);
        }

        if (isset($params['loyalty_plan_id']) && $params['loyalty_plan_id']) {
            if (is_array($params['loyalty_plan_id']) && count($params['loyalty_plan_id'])) {
                $select->where('p.level IN (?)', $params['loyalty_plan_id']);
            } elseif (is_numeric($params['loyalty_plan_id']) && intval($params['loyalty_plan_id'])) {
                $select->where('p.level = ?', intval($params['loyalty_plan_id']));
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['regional_market']) && $params['regional_market']) {
            $district_arr = array();
            if (is_array($params['regional_market']) && count($params['regional_market'])) {
                foreach ($params['regional_market'] as $_key => $_id) {
                    My_Region::getDistrictsByProvince($_id, $district_arr);
                }
            } elseif (is_numeric($params['regional_market']) && $params['regional_market']) {
                My_Region::getDistrictsByProvince($params['regional_market'], $district_arr);
            }

            if (count($district_arr)) {
                $select->where('dt.district IN (?)', $district_arr);
            } else {
                $select->where('1=0', 1);
            }

        } elseif (isset($params['area']) && $params['area']) {
            $district_arr = array();
            if (is_array($params['area']) && count($params['area'])) {
                foreach ($params['area'] as $_key => $_area_id) {
                    My_Region::getDistrictsByArea($_area_id, $district_arr);
                }

            } elseif (is_numeric($params['area']) && $params['area']) {
                My_Region::getDistrictsByArea($params['area'], $district_arr);
            }

            if (count($district_arr)) {
                $select->where('dt.district IN (?)', $district_arr);
            } else {
                $select->where('1=0', 1);
            }
        }

        $select->order('p.distributor_id');

        if (isset($params['export']) && $params['export']) {
            return $select->__toString();
        }

        if ($limit) {
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }

        return $result;
    } // end
}
