<?php

class Application_Model_AreaRankMoney extends Zend_Db_Table_Abstract
{
    protected $_name = 'area_rank_money';

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => 'area_rank_money'], [
                'a.*'
            ]);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['rank']] = $element;
        }
        return $list;
    }
}