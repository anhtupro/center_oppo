<?php
class Application_Model_CampaignAreaContractor extends Zend_Db_Table_Abstract
{
	protected $_name = 'campaign_area_contractor';

	protected $_schema = DATABASE_TRADE;

	function getCampaignAreaContractor($params)
    {
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.campaign_id', 
            'p.contractor_id', 
            'p.area_id', 
            'p.category_id', 
            'p.price', 
            'c.quantity',
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.contractor_category'), $arrCols);
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.campaign_area'), 'c.campaign_id = p.campaign_id AND c.area_id = p.area_id AND c.category_id = p.category_id',array());
        
        $select->where('p.campaign_id = ?', $params['campaign_id']);

        if(!empty($params['contractor_id'])){
            $select->where('p.contractor_id = ?', $params['contractor_id']);            
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    function getContractorArea($params)
    {
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.id',
            'p.contractor_id',
            'p.category_id',
            'p.area_id',
            'quantity'          => 'SUM(p.quantity)',
            'quantity_area'     => 'SUM(p.quantity_area)',
            'p.status',
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.contractor_area'), $arrCols);
        
        $select->where('p.campaign_id = ?', $params['campaign_id']);

        if(!empty($params['contractor_id'])){
            $select->where('p.contractor_id = ?', $params['contractor_id']);            
        }

        $select->group(array('p.area_id','p.category_id'));
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $result = $db->fetchAll($select);

        return $result;
    }

    function getAreaIn($params)
    {
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.id',
            'p.contractor_id',
            'p.category_id',
            'p.area_id',
            'quantity' => 'SUM(p.quantity_area)',
            'p.status',
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.contractor_area'), $arrCols);
        
        $select->where('p.campaign_id = ?', $params['campaign_id']);

        if(!empty($params['area_id'])){
            $select->where('p.area_id IN (?)', $params['area_id']);            
        }

        $select->group(array('p.area_id','p.category_id'));

        $result = $db->fetchAll($select);

        return $result;
    }

    function getContractorCategory($params)
    {
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.id',
            'p.contractor_id',
            'p.category_id',
            'p.area_id',
            'p.price',
            'c.name',
            'c.short_name',
            'c.code'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.contractor_category'), $arrCols);

        $select->joinLeft(array('c'=> DATABASE_TRADE.'.contructors'), 'c.id = p.contractor_id',array());

        $select->where('p.area_id = ?', $params['area_id']);

        $select->where('p.category_id = ?', $params['category_id']);                        

        $select->where('c.code = ?', $params['code']);

        $select->where('p.campaign_id = ?', $params['campaign_id']);            

        $result = $db->fetchRow($select);

        return $result;
    }

}