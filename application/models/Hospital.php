<?php
class Application_Model_Hospital extends Zend_Db_Table_Abstract
{
	protected $_name = 'hospital';
        
        function fetchPagination($page, $limit, &$total, $params){
		$db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'hospital'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'hospital_name'         => 'p.name',
		'province_name'         => 'pr.name'
            ))
            ->joinLeft(array('pr'=>'province'),'p.province_id = pr.id',array())
            ->where('p.del = ?',0)
        ;

        if(isset($params['hospital_name']) AND $params['hospital_name']){
        	$select->where('p.name LIKE ?','%'.$params['hospital_name'].'%');
        }
        
	if(isset($params['province']) AND $params['province']){
          $select->where('p.province_id = ?',$params['province']);
        }
        
        if(isset($params['code']) AND $params['code']){
          $select->where('p.code = ?',$params['code']);
        }
        
        if(isset($params['province_code']) AND $params['province_code']){
          $select->where('p.province_code = ?',$params['province_code']);
        }
        
       
        if ($limit)
            $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
	}

        
	public function get_all($params = array()){
		$db = Zend_Registry::get('db');
		$select = $db->select()
			->from(array('p'=>$this->_name),array('p.*'))
			->where('del IS NULL OR del = ?',0)
                        ->order('province_name')
			->order('name ASC');

		if(isset($params['id']) AND $params['id']){
			$select->where('id = ?',$params['id']);
		}

		if(isset($params['province_id']) AND $params['province_id']){
			$select->where('province_id = ?',$params['province_id']);
		}
			

		$result = $db->fetchAll($select);
		$arr = array();
		foreach($result as $item){
			$arr[$item['id']]  = str_pad($item['code'],3,'0',STR_PAD_LEFT) .' - '.$item['name'].' - '.$item['province_name'];
		}
		return $arr;
	}
        // ẩn 1 số hospital bên contract tại vì full slot đăng kí bvien
        public function get_all_hide_contract($params = array()){
		$db = Zend_Registry::get('db');
		$select = $db->select()
			->from(array('p'=>$this->_name),array('p.*'))
                        ->joinLeft(array('hhc'=>'hospital-hide-contract'),'hhc.id_hospital = p.id',array())
			->where('del IS NULL OR del = ?',0)
                        ->where('hhc.id_hospital IS NULL')
                        ->order('province_name')
			->order('name ASC');

		if(isset($params['id']) AND $params['id']){
			$select->where('id = ?',$params['id']);
		}

		if(isset($params['province_id']) AND $params['province_id']){
			$select->where('province_id = ?',$params['province_id']);
		}
			

		$result = $db->fetchAll($select);
		$arr = array();
		foreach($result as $item){
			$arr[$item['id']]  = str_pad($item['code'],3,'0',STR_PAD_LEFT) .' - '.$item['name'].' - '.$item['province_name'];
		}
		return $arr;
	}
        
        public function get_full($params = array()){
		$db = Zend_Registry::get('db');
		$select = $db->select()
			->from(array('p'=>$this->_name),array('p.*'))
//			->where('del IS NULL OR del = ?',0)
                        ->order('province_name')
			->order('name ASC');

		if(isset($params['id']) AND $params['id']){
			$select->where('id = ?',$params['id']);
		}

		if(isset($params['province_id']) AND $params['province_id']){
			$select->where('province_id = ?',$params['province_id']);
		}
			

		$result = $db->fetchAll($select);
		$arr = array();
		foreach($result as $item){
			$arr[$item['id']]  = str_pad($item['code'],3,'0',STR_PAD_LEFT) .' - '.$item['name'].' - '.$item['province_name'];
		}
		return $arr;
	}

	public function getIdByCode($code){
		$db = Zend_Registry::get('db');
		$select = $db->select()
			->from(array('p'=>$this->_name),array('id'))
			->where('p.code = ?',$code)
			;
		$result = $db->fetchOne($select);
		return $result;
	}
        
        public function get_cache_by_code(){
            
            $cache = Zend_Registry::get('cache');
            $result = $cache->load($this->_name . '_hospital_by_code');

            if ($result === false) {
                $result = array();
                $db = Zend_Registry::get('db');
                $select = $db->select()
                            ->from(array('p'=>$this->_name),array('p.*'))
                            ->where('p.del IS NULL OR p.del = ?',0)
                        ;
                $dataAll =$db->fetchAll($select);
                    if ($dataAll) {
                            foreach ($dataAll as $item) {
                                $result[$item['province_code']][$item['code']] = array(
                                                                                    'id' =>$item['id'], 
                                                                                    'code' =>$item['code'], 
                                                                                    'name' =>$item['name'] 
                                                                                    );
                            }
                    }
                $cache->save($result, $this->_name . '_hospital_by_code', array(), null);
                
            }
            return $result;  
        }
        
        public function getListHospitalHide(){
            $db = Zend_Registry::get('db');
            $select = $db->select()->from(array('p'=>$this->_name),array('p.*'));
            $select->join(['h'=> 'hospital-hide-contract'],'h.id_hospital = p.id','*');
            $select->where('p.del IS NULL OR p.del = ?',0);
            $dataAll =$db->fetchAll($select);
            return $dataAll;
        }
        
        public function checkHospital($list_hospital){
            $db = Zend_Registry::get('db');
            $select = $db->select()->from(array('p'=>$this->_name),array('p.id'));
            $select->where('p.id IN(?)',$list_hospital);
            $dataAll = $db->fetchAll($select);
            $data = [];
            foreach($dataAll as $item){
                $data[] = $item['id'];
            }
            return $data;
        }
        
        public function checkHospitalHide($list_hospital){
            $db = Zend_Registry::get('db');
            $select = $db->select()->from(array('p'=>'hospital-hide-contract'),array('p.id_hospital'));
            $select->where('p.id_hospital IN(?)',$list_hospital);
            $dataAll = $db->fetchAll($select);
            $data = [];
            foreach($dataAll as $item){
                $data[] = $item['id_hospital'];
            }
            return $data;
        }
        
}