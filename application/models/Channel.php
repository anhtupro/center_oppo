<?php
/**
 * Model này để quản lý các config
 * Lưu cache theo [key]=value;
 */
class Application_Model_Channel extends Zend_Db_Table_Abstract
{
	protected $_name = 'channel';

	function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }

            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    public function getAll($params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_CENTER . '.channel'], [
                'c.*'
            ])
            ->where('c.status = ?', 1);

        if ($params['list_channel_id']) {
            $select->where('c.id IN (?)', $params['list_channel_id']);
        }


        $result = $db->fetchAll($select);

        return $result;

    }
}
