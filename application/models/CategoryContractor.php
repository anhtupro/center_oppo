<?php
class Application_Model_CategoryContractor extends Zend_Db_Table_Abstract
{
	protected $_name = 'category_contractor';

	protected $_schema = DATABASE_TRADE;

	public function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select();
            
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 
            "p.name", 
            "p.short_name", 
            "p.address",
            "p.code",
            "p.status",
            "p.user"
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);

        $select->order('id DESC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result  = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;

    }


}