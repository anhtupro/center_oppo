<?php
class Application_Model_TrainerCourseType extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_course_type';

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if (!$result) {

            $where = $this->getAdapter()->quoteInto('del = ?', 0);

            $data = $this->fetchAll($where);

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
}