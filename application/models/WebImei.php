<?php
class Application_Model_WebImei extends Zend_Db_Table_Abstract
{
	protected $_name = 'imei';
    protected $_schema = WAREHOUSE_DB;

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => WAREHOUSE_DB.'.'.$this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->join(array('d'=>WAREHOUSE_DB.'.'.$this->_name), 'p.distributor_id = d.id', array('d.title'))
            ->join(array('g' => WAREHOUSE_DB.'.'.'good'), 'p.good_id = g.id', array('model'=>'g.name'))
            ->join(array('c' => WAREHOUSE_DB.'.'.'good_color'), 'p.good_color = c.id', array('color'=>'c.name'));

        if (isset($params['dealer_id']) and $params['dealer_id'])
            $select->where('d.id = ?', $params['dealer_id']);



        if (isset($params['from']) && $params['from'] && isset($params['to']) && $params['to']) {
            $from = explode('/', $params['from']);
            $from = $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00';
            $to = explode('/', $params['to']);
            $to = $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59';       
        } else {
            return false;
        }

        if (isset($from) and $from)
            $select->where('p.out_date >= ?',$from);

        if (isset($to) and $to)
            $select->where('p.out_date <= ?', $to);

        $select->order('p.out_date', 'ASC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function checkImei($imei, $timing_sales_id, &$info, $tool = null){
        /**
         * Note: by phamquocbuu
         * Check IMEI theo bảng imei_exception
         *      co trong bang exception thi uu tien lay trong do de check co fai imei demo hay khong
         * Check IMEI theo bảng imei
         *      Không tồn tại -> thông báo
         *      Tồn tại -> bước sau
         * Check trong bảng acti
         *      Ngày acti trước 30 ngày -> từ chối chấm công
         *      Ngược lại, check trong phần timing
         *          Tồn tại
         *              So sánh thời gian chấm công
         *                  Chấm hơn 30 ngày -> từ chối
         *                  Ngược lại -> thông báo
         *          Không tồn tại -> chấm công
         */

        // Check IMEI theo bảng imei
        $where = array();
        // $where[] = $QWebImei->getAdapter()->quoteInto('out_date > ?', 0);
        $where[] = $this->getAdapter()->quoteInto('into_date > ?', 0);
        $where[] = $this->getAdapter()->quoteInto('imei_sn = ?', $imei);

        $result = $this->fetchRow($where);
        if (!$result || !preg_match('/^[0-9]{15}$/', $imei)){
            //not existed in list sales out
            return 2;
        }

        $info['activated_at'] = $result['activated_date'];
        $info['out_date'] = $result['out_date'];
        //

        //Check trong bảng acti
        if (!$tool && $this->checkImei30DaysActivated($imei)) {
            return 4;
        }

        //check trong bảng lock_imei
        $QLockedImei = new Application_Model_LockedImei();
        $where = $QLockedImei->getAdapter()->quoteInto('imei_sn = ?', $imei);
        $locked = $QLockedImei->fetchRow($where);

        if ($locked) {
            return 6; // hàng cấm, éo tính
        }

        //check trong bảng imei_demo_fpt
        $QDemo = new Application_Model_ImeiDemoFpt();
        $where = $QDemo->getAdapter()->quoteInto('imei_sn = ?', $imei);
        $locked = $QDemo->fetchRow($where);

        if ($locked) {
            return 7; // hàng demo của fpt, éo tính
        }

        //check hàng demo, for staff, for lending
        // check trong bang imei_exception
        $QImeiException = new Application_Model_ImeiException();
        $where = $QImeiException->getAdapter()->quoteInto('imei_sn = ?', $imei);
        $imei_exception = $QImeiException->fetchRow($where);
        //check hàng demo
        if ($imei_exception){ // neu co trong exception thi uu tien lay trong do

            // kiem tra type cua exception: 1: xuat cho retailer; 2: xuat demo ...
            if ($imei_exception['type'] == 2) {
                return 7; // hàng demo, éo tính
            }

            if ($imei_exception['type'] == 3) {
                return 8; // hàng staff, éo tính
            }

            if ($imei_exception['type'] == 4) {
                return 9; // hàng lending, éo tính
            }

        } else {
            //check trong bảng imei_demo_fpt
            $QDemo = new Application_Model_ImeiDemoFpt();
            $where = $QDemo->getAdapter()->quoteInto('imei_sn = ?', $imei);
            $locked = $QDemo->fetchRow($where);

            if ($locked) {
                return 7; // hàng demo, éo tính
            }

            //check hàng demo
            $QMarket = new Application_Model_Market();
            $where = array();
            $where[] = $QMarket->getAdapter()->quoteInto('sn = ?', $result['sales_sn']);
            $where[] = $QMarket->getAdapter()->quoteInto('outmysql_time >= ?', '2014-06-01 00:00:00');
            $market = $QMarket->fetchRow($where);

            if ($market) {
                if ($market['type'] == 2) {
                    return 7; // hàng demo, éo tính
                }

                if ($market['type'] == 3) {
                    return 8; // hàng staff, éo tính
                }

                if ($market['type'] == 4) {
                    return 9; // hàng lending, éo tính
                }
            }
        }

        //check trong phần timing
        $QTimingSale = new Application_Model_TimingSale();
        $where = array();
        $where[] = $QTimingSale->getAdapter()->quoteInto('id <> ?', $timing_sales_id);
        $where[] = $QTimingSale->getAdapter()->quoteInto('imei = ?', $imei);

        $ts = $QTimingSale->fetchRow($where);

        // IMEI đã chấm công
        if ($ts){
            $QTiming = new Application_Model_Timing();
            $where = $QTiming->getAdapter()->quoteInto('id = ?', $ts['timing_id']);
            $result = $QTiming->fetchRow($where);

            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $user_id = $userStorage->id;

            if ($result && $result['staff_id'] == $user_id) {
                $info['date'] = $result['from'];

                $QStore = new Application_Model_Store();
                $store_rs = $QStore->find($result['store']);
                $store = $store_rs->current();

                $info['store'] = $store['name'];

                return 1; // nó chấm rồi
            } else {
                $QStaff = new Application_Model_Staff();
                $staff_rs = $QStaff->find($result['staff_id']);
                $staff = $staff_rs->current();

                $QStore = new Application_Model_Store();
                $store_rs = $QStore->find($result['store']);
                $store = $store_rs->current();

                if ($staff) {
                    $info['staff'] = $staff['firstname'] . ' ' . $staff['lastname'] . ' | ' . preg_replace('/oppomobile.vn/', '', $staff['email']) ;
                    $info['staff_id'] = $staff['id'];
                }

                if ($store)
                   $info['store'] = $store['name'];
                
                $info['date'] = $result['from'];
                $info['timing_sales_id'] = $ts['id'];
                
                // kiểm tra phòng trường hợp bảng imei acti ko có imei này
                // bán hơn 1 tháng
                if ( strtotime(date('Y-m-d')) - strtotime($result['from']) > 30*24*3600 ) return 5;

                return 3; // thằng khác chấm
            }
        }

        // IMEI chưa chấm công
        return 0;
    }

    function checkImei30DaysActivated($imei)
    {
        //Check trong bảng acti
        $QImeiActivation = new Application_Model_ImeiActivation();
        $where = array();
        $where[] = $QImeiActivation->getAdapter()->quoteInto('imei_sn = ?', intval($imei));
        $tmp_date = date_sub(date_create(), new DateInterval('P30D'))->format('Y-m-d 00:00:00');
        $where[] = $QImeiActivation->getAdapter()->quoteInto('activated_at < ?', $tmp_date);
        $imei_activation = $QImeiActivation->fetchRow($where);

        if ($imei_activation) { // IMEI đã acti hơn 30 ngày
            return 1;
        }

        return 0;
        //
    }
}