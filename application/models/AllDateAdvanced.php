<?php
class Application_Model_AllDateAdvanced extends Zend_Db_Table_Abstract
{
	protected $_name = 'all_date_advanced';

    public function getListDateSetting($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT 
                    ada.`date` as `date`,
                    GROUP_CONCAT(
                        CONCAT(cg.name, ' (', dc.name, ')') SEPARATOR ', '
                    ) as `list_group`,
                    MAX(ada.public) as `public`
                FROM `all_date_advanced` ada
                JOIN `company_group` cg
                    ON cg.id = ada.type_id
                JOIN `date_category` dc
                    ON dc.id = ada.category_date
                WHERE MONTH(ada.`date`) = :month
                    AND YEAR(ada.`date`) = :year
                    AND ada.is_off = 1
                GROUP BY ada.`date`";

        $stmt = $db->prepare($sql);
        
        $params['month'] = (empty($params['month']))?null:$params['month'];
        $params['year'] = (empty($params['year']))?null:$params['year'];
        
        $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);

        $stmt->execute();

        $data = array();
        $data = $stmt->fetchAll();
        
        return $data;
    }

    public function checkEdit($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT *
                FROM `all_date_advanced`
                WHERE type = 4
                AND type_id IN (
                    " . implode(",", $params['list_type_id']) ."
                )
                AND (`date` BETWEEN '" . $params['from_date'] . "' 
                    AND '" . $params['to_date'] . "')";
        
        $stmt = $db->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        
        $stmt = $fb = null;
        
        if(count($data) > 0)
        {
            return array('status' => false);
        }
        else
        {
            return array('status' => true);
        }
    }

    public function insertSuturday($params = array())
    {
        $db = Zend_Registry::get('db');
        if(!empty($params['company_group']))
        {
            $values = "(" . $params['month'] . "," . $params['year'] . "," . $params['number'] . ")";
            $sql_insert = "INSERT INTO 
                        saturday (`month`, `year`, `number`)
                        VALUES " . $values ."
                        ON duplicate key UPDATE `number` = VALUES(`number`)";
            $db->query($sql_insert);
        }
        $db = null;
    }

    public function getSaturday($params = array())
    {
        $db = Zend_Registry::get('db');
        $month = $params['month'] ;
        $year = $params['year'] ;
        $sql = "
            SELECT
                std.id as `id`,
                std.number as `number`,
                cg.name as `group`
            FROM `saturday` std
            JOIN `company_group` cg ON std.type_id = cg.id
            WHERE std.month = " . $params['month'] . " 
                AND std.year = " . $params['year'] . "
        ";

        if(!empty($params['month']) && !empty($params['year']))
        {
            $sql = "SELECT
                std.*
            FROM `saturday` std
            WHERE month = $month AND year = $year";
        
            $data = $db->fetchRow($sql);
        }
        else
        {
            $data = $db->fetchAll($sql);
        }

        $db = null;
        return $data;
    }

    public function insertDate($params = array())
    {
        $db = Zend_Registry::get('db');

        $array_date_setting = array();
        if(!empty($params['input']))
        {
            $values_from_date = strtotime($params['from_date']);
            while ($values_from_date <= strtotime($params['to_date'])) 
            {
                $date_setting = date('Y-m-d', $values_from_date);
                
                foreach($params['input'] as $key => $value)
                {
                    $value['multiple'] = empty($value['multiple'])?1:$value['multiple'];
                    $array_date_setting[] = "(4," . $value['type_id'] . ",'" . $date_setting . "', 1,"
                                                . $value['category_date'] . ", " . $value['public'] . ","
                                                . $value['multiple'] . ")";
                }
                $values_from_date += 86400;
            }
            $sql_insert = "INSERT INTO `all_date_advanced` (`type`, `type_id`, `date`, `is_off`, `category_date`, `public`, `multiple`)
                        VALUES " . implode(",", $array_date_setting) . "
                        ON duplicate key UPDATE 
                            `is_off` = VALUES(`is_off`), 
                            `category_date` = VALUES(`category_date`) , 
                            `updated_at` = NOW(),
                            `public` = VALUES(`public`),
                            `multiple` = VALUES(`multiple`)
                        ";
            $db->query($sql_insert);
            $sql_delete = "DELETE FROM `all_date_advanced` 
                            WHERE (`date` BETWEEN '" . $params['from_date'] ."'"
                                . " AND '" . $params['to_date'] ."') "
                                . " AND type = 4 "
                                . " AND type_id NOT IN (" . implode(",", $params['list_type_id']) . ")";
            $db->query($sql_delete);
            $db = null;
        }
        else
        {
            $sql_delete = "DELETE FROM `all_date_advanced` 
                            WHERE (`date` BETWEEN '" . $params['from_date'] ."'"
                                . " AND '" . $params['to_date'] ."') "
                                . " AND type = 4 ";
            $db->query($sql_delete);
            $db = null;
        }
    }

    public function _insert($values = '')
    {
        $db = Zend_Registry::get('db');

        if(!empty($values))
        {
            $sql = "INSERT INTO 
                    all_date_advanced (`type`, `type_id`, `date`, `is_off`, `category_date`) 
                    VALUES " . $values ."
                    ON duplicate key UPDATE `is_off` = VALUES(`is_off`), `category_date` = VALUES(`category_date`) , `updated_at` = NOW()";
            // echo $sql; die;
            $stmt = $db->prepare($sql);

            $stmt->execute();
            $stmt->closeCursor();
            $db = $stmt = null;
        }
    }

    public function getListByDate($date)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('SELECT *
                                FROM all_date_advanced
                                WHERE `date` = :date
                                    AND is_off = 1');
        $stmt->bindParam('date', $date, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function _insertDateAdvanced($params = array())
    {
        $db = Zend_Registry::get('db');

        if(!empty($params['company_group']))
        {
            $values = "(4, '" . $params['date'] . "', 1, " . $params['category'] . "," . implode("), (4, '" . $params['date'] . "', 1, " . $params['category'] . ", ", $params['company_group']) . ")";
            $sql_insert = "INSERT INTO 
                        all_date_advanced (`type`, `date`, `is_off`, `category_date`, `type_id`) 
                        VALUES " . $values ."
                        ON duplicate key UPDATE `is_off` = VALUES(`is_off`), `category_date` = VALUES(`category_date`) , `updated_at` = NOW()";
            // echo $sql_insert;
            $db->query($sql_insert);
            $sql_update ="UPDATE all_date_advanced
                            SET `is_off` = 0
                            WHERE `date` = '" . $params['date'] 
                                . "' AND type = 4 
                                    AND category_date = " . $params['category']
                                . " AND type_id NOT IN (" . implode(",", $params['company_group']) . ")";
            $db->query($sql_update);
        }
        else
        {
            $sql_delete_all = "UPDATE all_date_advanced
                            SET `is_off` = 0
                            WHERE `date` = '" . $params['date'] 
                                . "' AND type = 4 
                                    AND category_date = " . $params['category'];
            $db->query($sql_delete_all);
        }
        $db = null;
    }

    public function _insertMultiDateAdvanced($params = array())
    {
        $db = Zend_Registry::get('db');

        if(!empty($params['company_group']) && strtotime($params['from_date']) <= strtotime($params['to_date']))
        {
            $array_date_setting = array();
            $values_from_date = strtotime($params['from_date']);
            while ($values_from_date <= strtotime($params['to_date'])) 
            {
                $date_setting = date('Y-m-d', $values_from_date);
                $array_date_setting[] = "(" . $params['multiple'] . "," . $params['public'] . ",4, '" 
                                . $date_setting . "', 1, " . $params['category'] . "," 
                        . implode("), (" . $params['multiple'] . "," . $params['public'] . "4, '" 
                                . $date_setting . "', 1, " . $params['category'] . ", ", $params['company_group']) 
                        . ")";
                $values_from_date += 86400;
            }
            
            $sql_insert = "INSERT INTO 
                        all_date_advanced (`multiple`, `public`, `type`, `date`, `is_off`, `category_date`, `type_id`) 
                        VALUES " . implode(",", $array_date_setting) ."
                        ON duplicate key UPDATE 
                            `is_off` = VALUES(`is_off`), 
                            `category_date` = VALUES(`category_date`) , 
                            `updated_at` = NOW(),
                            `public` = VALUES(`public`),
                            `multiple` = VALUES(`multiple`)
                            ";

            $db->query($sql_insert);
            $sql_update ="UPDATE all_date_advanced
                            SET `is_off` = 0, `updated_at` = NOW(),
                                multiple = 1, public = 0
                            WHERE (`date` BETWEEN '" . $params['from_date'] 
                                . "' AND '" . $params['to_date'] .
                                "') AND type = 4 
                                    AND category_date = " . $params['category']
                                . " AND type_id NOT IN (" . implode(",", $params['company_group']) . ")";
            $db->query($sql_update);
        }
        else
        {
            $sql_delete_all = "UPDATE all_date_advanced
                            SET `is_off` = 0, `updated_at` = NOW()
                            WHERE (`date` BETWEEN '" . $params['from_date'] 
                                . "' AND '" . $params['to_date'] .
                                "') AND type = 4 
                                    AND category_date = " . $params['category'];
            $db->query($sql_delete_all);
        }
        $db = null;
    }

    public function getTitle($params = array())
    {
        $db = Zend_Registry::get('db');
        
        if(!empty($params['date']) && !empty($params['category']))
        {
            
            $stmt = $db->prepare("CALL get_title_date_advanced(:date, :category)");
            $stmt->bindParam("date", $params['date'], PDO::PARAM_STR);
            $stmt->bindParam("category", $params['category'], PDO::PARAM_INT);

            $stmt->execute();
            $data = $stmt->fetch();

            $stmt->closeCursor();
            $stmt = $db = null;
            $array_title = (json_decode(empty($data['array_title'])?"[]":$data['array_title']));
        }
        else
        {
            $array_title = array();
        }
        return $array_title;
    }

    public function getGroup($params = array())
    {
        $db = Zend_Registry::get('db');
        
        if(!empty($params['date']) && !empty($params['category']))
        {
            
            $stmt = $db->prepare("CALL get_group_date_advanced(:date, :category)");
            $stmt->bindParam("date", $params['date'], PDO::PARAM_STR);
            $stmt->bindParam("category", $params['category'], PDO::PARAM_INT);

            $stmt->execute();
            $data = $stmt->fetch();

            $stmt->closeCursor();
            $stmt = $db = null;
            $array_group = (json_decode(empty($data['array_title'])?"[]":$data['array_title']));
        }
        else
        {
            $array_group = array();
        }
        return $array_group;
    }
}