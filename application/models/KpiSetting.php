<?php
class Application_Model_KpiSetting extends Zend_Db_Table_Abstract
{
    protected $_name = 'kpi_setting';
    public function kpi_setting($page, $limit, &$total, $params)
    {
        // echo "<pre>";print_r($params);die;

        $from = date_create_from_format('d/m/Y', $params['from'])->format("Y-m-d");
        $to = date_create_from_format('d/m/Y', $params['to'])->format("Y-m-d");

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('g' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS g.id'), 'g.good_id', 'g.kpi', 'g.type','g.dealer_type','g.is_tgdd',
                'from' => 'g.from_date',
                'to' => 'g.to_date'
            ))
            ->joinLeft(array('b' => WAREHOUSE_DB.'.good'), 'g.good_id = b.id')
            ->joinLeft(array('ar' => 'area'), 'g.area_id = ar.id', array('area_name' => 'ar.name'))
            ->joinLeft(array('pl' => 'policy'), 'g.policy_id = pl.id', array('policy_title' => 'pl.title'));
  

        if (isset($params['good_id']) && $params['good_id'])
            $select->where('g.good_id = ?', intval($params['good_id']));
            
        if (isset($params['product_name']) && $params['product_name'])
            $select->where('b.desc LIKE ?', '%'.$params['product_name'].'%');
        
        if (isset($params['type']) && $params['type'])
            $select->where('g.type = ?', intval($params['type']));

        if (isset($params['area_id']) && $params['area_id'])
            $select->where('g.area_id = ?', intval($params['area_id']));

        if (isset($params['policy_id']) && $params['policy_id'])
            $select->where('g.policy_id = ?', intval($params['policy_id']));

        if (isset($params['dealer_type']) && $params['dealer_type'])
            $select->where('g.dealer_type = ?', intval($params['dealer_type']));

        if (isset($params['is_tgdd']) && $params['is_tgdd'])
            $select->where('g.is_tgdd = ?', intval($params['is_tgdd']));

        if (isset($params['to']) && date_create_from_format('d/m/Y', $params['to']))
            $select->where('g.from_date <= ?', date_create_from_format('d/m/Y', $params['to'])->format("Y-m-d"));

        if (isset($params['from']) && date_create_from_format('d/m/Y', $params['from']))
            $select->where('g.to_date IS NULL OR g.to_date >= ?', date_create_from_format('d/m/Y', $params['from'])->format("Y-m-d"));

        $select->order(array('g.good_id', 'g.from_date','g.area_id'));


        if ($limit)
            $select->limitPage($page, $limit);

        if(!empty($_GET['dev'])){
    echo $select->__toString();
    exit;
}

        $result = $db->fetchAll($select);
        
        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
}