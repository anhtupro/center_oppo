<?php

class Application_Model_RequestConfirm extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_confirm';
    
    public function listConfirm($option){
        
        $QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id = $userStorage->id;
        $department_id = $option['department_id'];
        $team_id = $option['team_id'];
        
        $list_confirm = [];
        
        //Purchasing - Lấy total Purchasing
        $pr_sn = $option["pr_sn"];

        $details = $QPurchasingRequestDetails->getDetailsToVnd($pr_sn);


        $total_price_sn = 0;
        if(!empty($details)){
            foreach($details as $k=>$v){

                $money_ck = $price_after_ck = $total_price = 0;
                $money_ck = $v['price']*$v['ck']/100;
                $price_after_ck = $v['price'] - $money_ck;
                $total_price = $v['quantity']*$price_after_ck+$v['fee'];
                $total_price_sn = $total_price_sn + $total_price;
            }
        }
        
        $cost_temp_to_vnd = $this->to_vnd($option['cost_temp'], $option['currency']);
        $total_price_sn = !empty($total_price_sn) ? $total_price_sn : $cost_temp_to_vnd;
        //END Purchasing
        
        if($department_id == 154){//HR
            $leader_id = $this->getLeaderByTitle($staff_id);
            $manager_id = $this->getManager($staff_id);
            
            if($leader_id){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => $leader_id,
                    'title_confirm' => "Chờ Leader duyệt"
                ];
                
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => $manager_id,
                    'title_confirm' => "Chờ Manager duyệt"
                ];
                
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 3,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ Director duyệt"
                ];
            }
            else{
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => $manager_id,
                    'title_confirm' => "Chờ Manager duyệt"
                ];
                
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ Director duyệt"
                ];
                
            }
            
        }
        elseif($department_id == 156){//MARKETING
            
            $leader_id = $this->getLeaderNew($staff_id);
            $manager_id = $this->getManager($staff_id);
            
            if($leader_id){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => $leader_id,
                    'title_confirm' => "Chờ Leader duyệt"
                ];
                
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 26859,
                    'title_confirm' => "Chờ MARKETING DIRECTOR duyệt"
                ];
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 3,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ A.Đống duyệt"
                    ];
                }
            }
            else{
                
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 26859,
                    'title_confirm' => "Chờ MARKETING DIRECTOR duyệt"
                ];
               
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ A.Đống duyệt"
                    ];
                }
            }
        }
        elseif($department_id == 149){//FINANCE
            //OPPO a.Vu duyệt, DĐTM chị Bình duyệt
            if($option['company_id'] == 1){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 17,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
            else{
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 1672,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
            
            //Nếu chọn "nộp thuế" thì qua Thảo Nguyên: thaonguyen.pham
            if($option['category_id'] == 8){
                $list_confirm = [];
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 19101,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
            
            //Nếu chọn "chuyển trả đại lý" thì qua Phạm Thị Ngọc Hương: ngochuong.pham@oppo-aed.vn
            if($option['category_id'] == 9){
                $list_confirm = [];
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 19372,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
            
            
        }
        elseif($department_id == 557){//GTM 26859: arron duyệt
            
            if(in_array($option['request_type_group'], [90])) {
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 326,
                    'title_confirm' => "Chờ Sale Manager duyệt"
                ];
            }
            else{

                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,
                    'staff_id'      => 3026,
                    'title_confirm' => "Chờ GTM Manager duyệt"
                ];

                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,
                    'staff_id'      => 765,
                    'title_confirm' => "Chờ CEO duyệt"
                ];
            }
            
        }
        elseif($department_id == 159){//SERVICE
            
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 2906,
                'title_confirm' => "Chờ Manager duyệt"
            ];
            
            if($total_price_sn >= 500000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ BOD duyệt"
                ];
            }
        }
        elseif($department_id == 150){//LOGISTICS
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 963,
                'title_confirm' => "Chờ Manager duyệt"
            ];
            if($total_price_sn >= 500000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ BOD duyệt"
                ];
            }
            
        }
        elseif($department_id == 160){//CALL CENTER
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 10557,
                'title_confirm' => "Chờ Manager duyệt"
            ];
            if($total_price_sn >= 500000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ BOD duyệt"
                ];
            }
            
        }
        elseif($department_id == 153){//TECHNOLOGY
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 5899,
                'title_confirm' => "Chờ Manager duyệt"
            ];
            if($option['is_coo'] == 1){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ BOD duyệt"
                ];
            }
            
        }
        elseif($department_id == 723){//BRANDSHOP
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 240,
                'title_confirm' => "Chờ Training Manager duyệt"
            ];

            if($option['is_coo'] == 1){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 156,
                    'title_confirm' => "Chờ TMK Manager duyệt"
                ];
            }
        }
        elseif($department_id == 610){//RETAIL

            if($team_id == 619){//TRAINING
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 13670,
                    'title_confirm' => "Chờ Training Planner duyệt"
                ];

                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 240,
                    'title_confirm' => "Chờ Retail Manager duyệt"
                ];

                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 3,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ A.Đống duyệt"
                    ];
                }
            }
            elseif($team_id == 630){//BRANDSHOP
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,
                    'staff_id'      => 4266,
                    'title_confirm' => "Chờ Brand Shop Supervisor duyệt"
                ];

                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,
                    'staff_id'      => 240,
                    'title_confirm' => "Chờ Retail Manager duyệt"
                ];

                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 3,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ A.Đống duyệt"
                    ];
                }
            }
            else{
                if(in_array($option['request_type_group'], [43, 46, 60])) {

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 1,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 43,
                        'title_confirm' => "Chờ TMK Leader duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 240,
                        'title_confirm' => "Chờ Retail Manager duyệt"
                    ];

                    if($total_price_sn >= 3000000000){
                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 3,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 765,
                            'title_confirm' => "Chờ A.Đống duyệt"
                        ];
                    }
                }
                elseif(in_array($option['request_type_group'], [44])) {

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 1,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 9837,
                        'title_confirm' => "Chờ TMK Leader duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 4199,
                        'title_confirm' => "Chờ TMK Leader duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 3,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 240,
                        'title_confirm' => "Chờ Retail Manager duyệt"
                    ];

                    if($total_price_sn >= 3000000000){
                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 4,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 765,
                            'title_confirm' => "Chờ A.Đống duyệt"
                        ];
                    }
                }
                elseif(in_array($option['request_type_group'], [52, 53, 63])) {

                    if(in_array($option['request_type'], [157, 158, 159, 161]) || in_array($option['request_type_group'], [52, 53, 63])){
                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 1,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 15038,
                            'title_confirm' => "Chờ TMK Leader duyệt"
                        ];

                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 2,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 240,
                            'title_confirm' => "Chờ Retail Manager duyệt"
                        ];

                        if($total_price_sn >= 3000000000){
                            $list_confirm[] = [
                                'request_id'    => $option['request_id'],
                                'step'          => 3,
                                'type'          => 1,//Confirm nội bộ phòng ban
                                'staff_id'      => 765,
                                'title_confirm' => "Chờ A.Đống duyệt"
                            ];
                        }
                    }
                    else {

                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 1,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 240,
                            'title_confirm' => "Chờ Retail Manager duyệt"
                        ];

                        if($total_price_sn >= 3000000000){
                            $list_confirm[] = [
                                'request_id'    => $option['request_id'],
                                'step'          => 2,
                                'type'          => 1,//Confirm nội bộ phòng ban
                                'staff_id'      => 765,
                                'title_confirm' => "Chờ A.Đống duyệt"
                            ];
                        }
                    }


                }
                elseif(in_array($option['request_type_group'], [47])) {

                    if(in_array($option['request_type'], [157, 158, 159, 201])){
                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 1,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 15038,
                            'title_confirm' => "Chờ TMK Leader duyệt"
                        ];

                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 2,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 240,
                            'title_confirm' => "Chờ Retail Manager duyệt"
                        ];

                        if($total_price_sn >= 3000000000){
                            $list_confirm[] = [
                                'request_id'    => $option['request_id'],
                                'step'          => 3,
                                'type'          => 1,//Confirm nội bộ phòng ban
                                'staff_id'      => 765,
                                'title_confirm' => "Chờ A.Đống duyệt"
                            ];
                        }
                    }
                    else if(in_array($option['request_type'], [160])){

                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 1,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 240,
                            'title_confirm' => "Chờ Retail Manager duyệt"
                        ];

                        if($total_price_sn >= 3000000000){
                            $list_confirm[] = [
                                'request_id'    => $option['request_id'],
                                'step'          => 2,
                                'type'          => 1,//Confirm nội bộ phòng ban
                                'staff_id'      => 765,
                                'title_confirm' => "Chờ A.Đống duyệt"
                            ];
                        }
                    }
                    else if(in_array($option['request_type'], [161])){

                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 1,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 43,
                            'title_confirm' => "Chờ TMK Leader duyệt"
                        ];

                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 2,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 240,
                            'title_confirm' => "Chờ Retail Manager duyệt"
                        ];

                        if($total_price_sn >= 3000000000){
                            $list_confirm[] = [
                                'request_id'    => $option['request_id'],
                                'step'          => 3,
                                'type'          => 1,//Confirm nội bộ phòng ban
                                'staff_id'      => 765,
                                'title_confirm' => "Chờ A.Đống duyệt"
                            ];
                        }
                    }


                }
                elseif(in_array($option['request_type_group'], [59])) {
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 1,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 15038,
                        'title_confirm' => "Chờ TMK Leader duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 43,
                        'title_confirm' => "Chờ TMK Leader duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 3,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 240,
                        'title_confirm' => "Chờ Retail Manager duyệt"
                    ];

                    if($total_price_sn >= 3000000000){
                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 4,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 765,
                            'title_confirm' => "Chờ A.Đống duyệt"
                        ];
                    }
                }
                elseif(in_array($option['request_type_group'], [65])) {
                    

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 1,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 240,
                        'title_confirm' => "Chờ Retail Manager duyệt"
                    ];

                    if($total_price_sn >= 3000000000){
                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 2,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 765,
                            'title_confirm' => "Chờ A.Đống duyệt"
                        ];
                    }
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 1,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 240,
                        'title_confirm' => "Chờ Retail Manager duyệt"
                    ];

                    if($total_price_sn >= 3000000000){
                        $list_confirm[] = [
                            'request_id'    => $option['request_id'],
                            'step'          => 2,
                            'type'          => 1,//Confirm nội bộ phòng ban
                            'staff_id'      => 765,
                            'title_confirm' => "Chờ A.Đống duyệt"
                        ];
                    }
                }
            }

            
            
            
        }
        elseif($department_id == 783){//Sale Online

            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 241,
                'title_confirm' => "Chờ Online Sale Leader duyệt"
            ];
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 2,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 20477,
                'title_confirm' => "Chờ Online Sale Manager duyệt"
            ];

            if($total_price_sn >= 3000000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 3,
                    'type'          => 1,
                    'staff_id'      => 765,
                    'title_confirm' => "Chờ CEO duyệt"
                ];
            }
            
        }
        elseif($department_id == 152){//SALE
            
            if(in_array($option['request_type_group'], [28,37,38,39])) {
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 14125,
                    'title_confirm' => "Chờ KA Leader duyệt"
                ];
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 326,
                        'title_confirm' => "Chờ Sale Manager duyệt"
                    ];
                }
                
            }
            elseif(in_array($option['request_type_group'], [30, 35, 36, 58])) {
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 3697,
                    'title_confirm' => "Chờ KA Leader duyệt"
                ];
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 326,
                        'title_confirm' => "Chờ Sale Manager duyệt"
                    ];
                }
                
            }
            elseif(in_array($option['request_type_group'], [32, 33])) {
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 23078,
                    'title_confirm' => "Chờ KA Leader duyệt"
                ];
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 326,
                        'title_confirm' => "Chờ Sale Manager duyệt"
                    ];
                }
                
            }
            elseif(in_array($option['request_type_group'], [29])) {
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 14125,
                    'title_confirm' => "Chờ KA Leader duyệt"
                ];
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 326,
                        'title_confirm' => "Chờ Sale Manager duyệt"
                    ];
                }
                
            }
            elseif(in_array($option['request_type_group'], [27, 45])) {
//                    $list_confirm[] = [
//                        'request_id'    => $option['request_id'],
//                        'step'          => 1,
//                        'type'          => 1,//Confirm nội bộ phòng ban
//                        'staff_id'      => 4912,
//                        'title_confirm' => "Chờ KA Leader duyệt"
//                    ];
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 1,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 1,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 326,
                        'title_confirm' => "Chờ Sale Manager duyệt"
                    ];
                }
            }
            elseif(in_array($option['request_type_group'], [34])) {//PICO
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 3697,
                    'title_confirm' => "Chờ KA Leader duyệt"
                ];
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 326,
                        'title_confirm' => "Chờ Sale Manager duyệt"
                    ];
                }
            }
            elseif(in_array($option['request_type_group'], [57])) {//Thiên Hòa:Đoàn Thanh Tâm
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 23078,
                    'title_confirm' => "Chờ KA Leader duyệt"
                ];
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,//Confirm nội bộ phòng ban
                        'staff_id'      => 326,
                        'title_confirm' => "Chờ Sale Manager duyệt"
                    ];
                }
            }
            else{

                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 326,
                    'title_confirm' => "Chờ Sale Manager duyệt"
                ];
                
                
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 2,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
            }
            
            
        }
        elseif($department_id == 151){//TEST
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 2,
                'title_confirm' => "Chờ Test Leader duyệt"
            ];
            
            if($total_price_sn >= 3000000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,
                    'staff_id'      => 765,
                    'title_confirm' => "Chờ CEO duyệt"
                ];
            }
        }
        elseif($department_id == 400){
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 4,
                'title_confirm' => "Chờ Purchasing Manager duyệt"
            ];
            
            if($total_price_sn >= 3000000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,
                    'staff_id'      => 765,
                    'title_confirm' => "Chờ CEO duyệt"
                ];
            }
            
        }
        elseif($department_id == 793){//IOT
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,
                'staff_id'      => 32093,
                'title_confirm' => "Chờ IOT Sales Manager duyệt"
            ];

            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 2,
                'type'          => 1,
                'staff_id'      => 156,
                'title_confirm' => "Chờ IOT Manager duyệt"
            ];
            
            if($total_price_sn >= 3000000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 3,
                    'type'          => 1,
                    'staff_id'      => 765,
                    'title_confirm' => "Chờ CEO duyệt"
                ];
            }
            
        }
        else{
            $leader_id = $this->getLeader($staff_id);
            $manager_id = $this->getManager($staff_id);
            $leader_manager_id = $this->getLeaderManager($staff_id);
            
            if($manager_id){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => $manager_id,
                    'title_confirm' => "Chờ Manager duyệt"
                ];
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ BOD duyệt"
                ];
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 3,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                
            }
            else{
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => $leader_manager_id,
                    'title_confirm' => "Chờ Manager duyệt"
                ];
                
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 7,
                    'title_confirm' => "Chờ BOD duyệt"
                ];
                
                if($total_price_sn >= 3000000000){
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => 3,
                        'type'          => 1,
                        'staff_id'      => 765,
                        'title_confirm' => "Chờ CEO duyệt"
                    ];
                }
                
            }
        }
        
        if($staff_id == 23154){
            $list_confirm = [];
            
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 326,
                'title_confirm' => "Chờ Sale Manager duyệt"
            ];
            
        }
        
        if($staff_id == 23003 AND in_array($option['company_id'], [1,2])){//maitrang.vu@namvietcapital.vn và cty OPPO
            $list_confirm = [];
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 7,
                'title_confirm' => "Chờ BOD duyệt"
            ];
            
            if($total_price_sn >= 3000000000){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 2,
                    'type'          => 1,
                    'staff_id'      => 765,
                    'title_confirm' => "Chờ CEO duyệt"
                ];
            }
        }
        
        if($staff_id == 22813){//Khả Hoàng
            $list_confirm = [];
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 6705,
                'title_confirm' => "Chờ Leader duyệt"
            ];
            
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 2,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 7,
                'title_confirm' => "Chờ Manager duyệt"
            ];
        }
        
        if($option['company_id'] == 4 || $option['company_id'] == 7){
            
            $list_confirm = [];
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 103,
                'title_confirm' => "Chờ Anh Kha duyệt"
            ];
        }
        
        $max_step = 0;
        foreach($list_confirm as $key=>$value){
            $max_step = $value['step'];
            $this->insert($value);
        }
        
        //Nếu là đề nghị mua máy thì qua Nhân sự Duyệt
        if($option['category_id'] == 7){
            $this->insert(
                [
                    'request_id'    => $option['request_id'],
                    'step'          => $max_step+1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 8807,
                    'title_confirm' => "Chờ HR duyệt"
                ]
            );
        }
        
        
        
        return $list_confirm;
    }
    
    public function getLeaderByTitle($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "s.staff_code", 
            "leader_id" => "staff.id",
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('s' => 'staff_permission'), 's.title_id = p.title', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.code = s.staff_code', array());

        $select->where('p.id = ?', $staff_id);
        $select->where('s.is_leader = 1', NULL);
        $result = $db->fetchRow($select);
        
        if($result['leader_id']){
            return $result['leader_id'];
        }
        else{
            return false;
        }
    }
    
    public function getLeader($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "s.staff_code", 
            "leader_id" => "staff.id",
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('s' => 'staff_permission'), 's.team_id = p.team', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.code = s.staff_code', array());

        $select->where('p.id = ?', $staff_id);
        $select->where('s.is_leader = 1', NULL);
        $result = $db->fetchRow($select);
        
        if($result['leader_id']){
            return $result['leader_id'];
        }
        else{
            return false;
        }
    }
    
    public function getLeaderNew($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.code", 
            "sp.code", 
            "sp2.staff_code", 
            "p.department", 
            "p.team", 
            "IFNULL(sp.code,sp2.staff_code)", 
            "leader_id" => "staff.id", 
            "staff.firstname", 
            "staff.lastname",
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('sp' => 'staff_permission'), 'sp.staff_code = p.code AND sp.is_leader = 1 AND sp.staff_code <> p.code', array());
        $select->joinLeft(array('sp2' => 'staff_permission'), 'sp2.department_id = p.department AND sp2.team_id = p.team AND sp2.is_leader = 1 AND sp2.staff_code <> p.code', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.code = IFNULL(sp.code,sp2.staff_code)', array());
        
        $select->where('p.id = ?', $staff_id);
        $select->where('staff.id <> ?', 14061);
        
        $result = $db->fetchRow($select);
        
        if($result['leader_id']){
            return $result['leader_id'];
        }
        else{
            return false;
        }
    }
    
    public function getManager($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.firstname", 
            "p.lastname", 
            "p.team", 
            "s.staff_code", 
            "manager_id" => "staff.id"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('s' => 'staff_permission'), 's.department_id = p.department', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.code = s.staff_code', array());

        $select->where('s.is_manager = 1', NULL);
        $select->where('p.id = ?', $staff_id);
        
        $result = $db->fetchRow($select);
        
        if($result['manager_id']){
            return $result['manager_id'];
        }
        else{
            return false;
        }
    }
    
    public function getLeaderManager($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.firstname", 
            "p.lastname", 
            "p.team", 
            "s.staff_code", 
            "manager_id" => "staff.id"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('s' => 'staff_permission'), 's.department_id = p.department', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.code = s.staff_code', array());

        $select->where('s.is_leader = 1', NULL);
        $select->where('p.id = ?', $staff_id);
        
        $result = $db->fetchRow($select);
        
        if($result['manager_id']){
            return $result['manager_id'];
        }
        else{
            return false;
        }
    }
    
    public function listFinanceConfirm($option){
        
        //linhdan.le 24694, thaonguyen.pham 19101, vu.nguyen 17
        
        $finance_staff = !empty($option['finance_staff']) ? $option['finance_staff'] : 2084;
        
        $QRequestOffice = new Application_Model_RequestOffice();
        $where = $QRequestOffice->getAdapter()->quoteInto('id = ?', $option['request_id']);
        $request_office = $QRequestOffice->fetchRow($where);

        $list_confirm = [];
        
        
        if($option['company_id'] == COMPANY_OPPO || $option['company_id'] == COMPANY_ONEPLUS){
            if($option['payment_type'] == 1){//Tiền mặt
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => $option['step'],
                    'type'          => 2,//Kế toán confirm
                    'staff_id'      => 24694,
                    'title_confirm' => "Chờ Finance duyệt",
                ];

                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => $option['step']+1,
                    'type'          => 2,//Kế toán confirm
                    'staff_id'      => 2466,
                    'title_confirm' => "Chờ Finance duyệt",
                    'finish_request' => 1,
                    'update_payment' => 1
                ];


            }
            elseif($option['payment_type'] == 2){//Chuyển khoản

                if($finance_staff == 17){

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step'],
                        'type'          => 2,//Confirm nội bộ phòng ban
                        'staff_id'      => 26995,
                        'title_confirm' => "Chờ Finance duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step']+1,
                        'type'          => 2,//Confirm nội bộ phòng ban
                        'staff_id'      => 17,
                        'finish_request' => 1,
                        'title_confirm' => "Chờ Finance chuyển khoản"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step'],
                        'type'          => 2,//Kế toán confirm
                        'staff_id'      => $finance_staff,
                        'title_confirm' => "Chờ Finance duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step']+1,
                        'type'          => 2,//Confirm nội bộ phòng ban
                        'staff_id'      => 26995,
                        'title_confirm' => "Chờ Finance duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step']+2,
                        'type'          => 2,//Confirm nội bộ phòng ban
                        'staff_id'      => 17,
                        'finish_request' => 1,
                        'title_confirm' => "Chờ Finance chuyển khoản"
                    ];
                }
            }
        }
        elseif($option['company_id'] == COMPANY_DIDONGTHONGMINH){

            if($option['payment_type'] == 1){

                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => $option['step'],
                    'type'          => 2,//Kế toán confirm
                    'staff_id'      => 19101,
                    'title_confirm' => "Chờ Finance duyệt",
                    'finish_request' => 1,
                    'update_payment' => 1
                ];
            }
            elseif($option['payment_type'] == 2){

                if($finance_staff == 17){

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step'],
                        'type'          => 2,//Confirm nội bộ phòng ban
                        'staff_id'      => 26995,
                        'title_confirm' => "Chờ Finance duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step']+1,
                        'type'          => 2,//Confirm nội bộ phòng ban
                        'staff_id'      => 17,
                        'finish_request' => 1,
                        'title_confirm' => "Chờ Finance chuyển khoản"
                    ];
                }
                else{
                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step'],
                        'type'          => 2,//Kế toán confirm
                        'staff_id'      => 19101,
                        'title_confirm' => "Chờ Finance duyệt"
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step']+1,
                        'type'          => 2,//Kế toán confirm
                        'staff_id'      => 26995,
                        'title_confirm' => "Chờ Finance duyệt",
                    ];

                    $list_confirm[] = [
                        'request_id'    => $option['request_id'],
                        'step'          => $option['step']+2,
                        'type'          => 2,//Kế toán confirm
                        'staff_id'      => 17,
                        'title_confirm' => "Chờ Finance chuyển khoản",
                        'finish_request' => 1
                    ];
                }
            }
        }
        elseif($option['company_id'] == 4){//NAM VIỆT
            //Nếu chọn kế toán thì qua kế toán OPPO thanh toán, không thì qua linhdan.le
            if(!empty($option['finance_staff'])){
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => $option['step'],
                    'type'          => 2,//Confirm nội bộ phòng ban
                    'staff_id'      => $option['finance_staff'],
                    'finish_request' => 1,
                    'title_confirm' => "Chờ Finance thanh toán"
                ];
            }
            else{
                $list_confirm[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => $option['step'],
                    'type'          => 2,//Confirm nội bộ phòng ban
                    'staff_id'      => 24966,
                    'finish_request' => 1,
                    'title_confirm' => "Chờ Finance thanh toán"
                ];
            }
        }
            
            
        
        //Nộp thuế thì đi qua Huyền vs a Vủ
        if($request_office['category_id'] == 8){
            $list_confirm = [];
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => $option['step'],
                'type'          => 2,//Kế toán confirm
                'staff_id'      => 26995,
                'title_confirm' => "Chờ Finance duyệt",
            ];

            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => $option['step']+1,
                'type'          => 2,//Kế toán confirm
                'staff_id'      => 17,
                'title_confirm' => "Chờ Finance chuyển khoản",
                'finish_request' => 1
            ];
        }
        
        // "Chuyển trả đại lý" thì đi qua Huyền, Vũ
        if($request_office['category_id'] == 9){
            $list_confirm = [];
            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => $option['step'],
                'type'          => 2,//Kế toán confirm
                'staff_id'      => 26995,
                'title_confirm' => "Chờ Finance duyệt",
            ];

            $list_confirm[] = [
                'request_id'    => $option['request_id'],
                'step'          => $option['step']+1,
                'type'          => 2,//Kế toán confirm
                'staff_id'      => 17,
                'title_confirm' => "Chờ Finance chuyển khoản",
                'finish_request' => 1
            ];
            
        }
        
        foreach($list_confirm as $key=>$value){
            $this->insert($value);
        }
        
        return $list_confirm;
        
    }
    
    public function checkRequestConfirm($staff_id, $list_request_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id",
            'p.staff_id',
            'p.step',
            'p.finish_request'
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office'), 'r.id = p.request_id', array());

        $select->where('p.staff_id = ?', $staff_id);
        $select->where('p.is_confirm = 0', NULL);
        
        if(!empty($list_request_id)){
            $select->where('p.request_id IN (?)', $list_request_id);
        }
        
        
        if(in_array($staff_id, [19101])){
            $select->where('r.payment_type <> 1');
        }
        
        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $key=>$value){
            $data[$value['request_id']] = [
                'step' => $value['step'],
                'finish_request' => $value['finish_request'],
            ];
        }

        return $data;
    }
    
    public function listStaffConfirm(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "staff_id",
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);
        $select->group("staff_id");
        
        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $key=>$value){
            $data[] = $value["staff_id"];
        }

        return $data;
    }
    
    public function getListHasConfirm($request_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id", 
            "p.staff_id", 
            "p.confirm_date",
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);

        $select->where('p.request_id = ?', $request_id);
        $select->where('p.type = 1 AND is_confirm = 1');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getFinanceHasConfirm($request_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id", 
            "p.staff_id", 
            "p.confirm_date",
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);

        $select->where('p.request_id = ?', $request_id);
        $select->where('p.type = 2 AND is_confirm = 1');
        
        $result = $db->fetchRow($select);

        return $result;
    }
    
    
    public function getFinanceHasNotConfirm($request_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id", 
            "p.staff_id", 
            "p.confirm_date",
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);

        $select->where('p.request_id = ?', $request_id);
        $select->where('p.type = 2 AND is_confirm = 0');
        
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function checkRequestView($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id",
            "p.all",
            "p.team_id",
            "p.department_id",
            "p.view_staff_id"
        );

        $select->from(array('p' => 'request_view'), $arrCols);

        $select->where('p.staff_id = ?', $staff_id);
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getRequestView($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id",
            "p.all",
            "p.team_id",
            "p.department_id",
            "p.view_staff_id"
        );

        $select->from(array('p' => 'request_view'), $arrCols);

        $select->where('p.staff_id = ?', $staff_id);
        
        $result = $db->fetchAll($select);
        
        $view_all = 0;
        $view_team = [];
        $view_department = [];
        $view_staff = [];
        foreach($result as $key=>$value){
            if($value['all'] == 1){
                $view_all = 1;
            }

            if($value['team_id']){
                $view_team[] =  $value['team_id'];
            }

            if($value['department_id']){
                $view_department[] =  $value['department_id'];
            }

            if($value['view_staff_id']){
                $view_staff[] =  $value['view_staff_id'];
            }
        }
        
        $data = [
            'all' => $view_all,
            'team_id'  => $view_team,
            'department_id'  => $view_department,
            'view_staff_id'  => $view_staff,
        ];
        
        return $data;
    }

    public function listConfirmFlow($option){
        
        $QPurchasingRequestDetails = new Application_Model_PurchasingRequestDetails();
        $QStaffPermissionFinal = new Application_Model_StaffPermissionFinal();
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $staff_id = $userStorage->id;
        $department_id = $option['department_id'];
        $team_id = $option['team_id'];
        
        $list_confirm = [];
        
        //Purchasing - Lấy total Purchasing
        $pr_sn = $option["pr_sn"];
        

        $details = $QPurchasingRequestDetails->getDetailsToVnd($pr_sn);
        
        $total_price_sn = 0;
        if(!empty($details)){
            foreach($details as $k=>$v){

                $money_ck = $price_after_ck = $total_price = 0;
                $money_ck = $v['price']*$v['ck']/100;
                $price_after_ck = $v['price'] - $money_ck;
                $total_price = $v['quantity']*$price_after_ck+$v['fee'];
                $total_price_sn = $total_price_sn + $total_price;
            }
        }
        
        $cost_temp_to_vnd = $this->to_vnd($option['cost_temp'], $option['currency']);
        $total_price_sn = !empty($total_price_sn) ? $total_price_sn : $cost_temp_to_vnd;
        //END - Purchasing
        
        $type = 1;
        $params = [];
        $params = [
            'request_type_group' => $option['request_type_group'] ? $option['request_type_group'] : 0,
            'request_type' => $option['request_type'] ? $option['request_type'] : 0,
            'team_id' => $team_id,
            'department_id' => $department_id,
            'type' => $type,
            'total_price_sn' => ($total_price_sn && ($total_price_sn > $option['cost_temp'])) ? $total_price_sn : $option['cost_temp']
        ];
        
        //Không có flow theo request_type, request_type_group thì mới lấy flow theo team, department

        if(!empty($params['request_type']) && !$list_confirm){
            $list_confirm = $this->getFlowRequestType($params);
        }

        if(!empty($params['request_type_group']) && !$list_confirm){
            $list_confirm = $this->getFlowTypeGroup($params);
        }
        
        
        if(!$list_confirm){
            $list_confirm = $this->getFlowTeam($params);
        }

        if(!$list_confirm){
            $list_confirm = $this->getFlowDepartment($params);
        }
        
        
        $list_return = [];
        $step = 0;

        if($list_confirm[0]['has_leader'] == 1){
            
            $check = [];
            $check['staff_id'] = $staff_id;
            $check['group_permission'] = 5;
            $leader = $QStaffPermissionFinal->getLeaderByStaff($check);
            
            if($leader){
                
                $step = 1;
                
                $staff_confirm = [
                    'request_id'    => $option['request_id'],
                    'step'          => $step,
                    'type'          => 1,
                    'staff_id'      => $leader['staff_id'],
                    'title_confirm' => "Chờ ".$leader['title_name']." duyệt"
                ];
                
                $list_return[] = $staff_confirm;
            }
            
        }

        //Đối với Namviet
        if($option['company_id'] == 4 || $option['company_id'] == 7){
            
            $list_return = [];
            $list_return[] = [
                'request_id'    => $option['request_id'],
                'step'          => 1,
                'type'          => 1,//Confirm nội bộ phòng ban
                'staff_id'      => 103,
                'title_confirm' => "Chờ Anh Kha duyệt"
            ];
        }
        //END - Đối với Namviet


        foreach($list_confirm as $key=>$value){
            $staff_confirm = [
                'request_id'    => $option['request_id'],
                'step'          => $step + $value['step'],
                'type'          => 1,
                'staff_id'      => $value['staff_id'],
                'title_confirm' => "Chờ ".$value['title_name']." duyệt"
            ];
            
            $list_return[] = $staff_confirm;
        }
        
        //Nếu là đề nghị mua máy thì qua Nhân sự Duyệt
        if($option['category_id'] == 7){

            $max_step = 0;
            $list_return_id = [];
            foreach($list_return as $key=>$value){
                $max_step = $value['step'];
                $list_return_id[] = $value['staff_id'];
            }

            if(!in_array(8807, $list_return_id)){
                $list_return[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => $max_step+1,
                    'type'          => 1,
                    'staff_id'      => 8807,
                    'title_confirm'    => "Chờ HR duyệt"
                ];
            }
            
        }
        //END - Nếu là đề nghị mua máy thì qua Nhân sự Duyệt


        //FINANCE
        if($department_id == 149){//FINANCE
            //OPPO a.Vu duyệt, DĐTM chị Bình duyệt
            if($option['company_id'] == 1){
                $list_return[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 17,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
            else{
                $list_return[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 1672,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
            
            //Nếu chọn "nộp thuế" thì qua Thảo Nguyên: thaonguyen.pham
            if($option['category_id'] == 8){
                $list_return = [];
                $list_return[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 19101,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
            
            //Nếu chọn "chuyển trả đại lý" thì qua Phạm Thị Ngọc Hương: ngochuong.pham@oppo-aed.vn
            if($option['category_id'] == 9){
                $list_return = [];
                $list_return[] = [
                    'request_id'    => $option['request_id'],
                    'step'          => 1,
                    'type'          => 1,//Confirm nội bộ phòng ban
                    'staff_id'      => 19372,
                    'title_confirm' => "Chờ Finance duyệt"
                ];
            }
        }
        //END - FINANCE
        

        foreach($list_return as $key=>$value){
            $this->insert($value);
        }
        
        return $list_return;
        
    }

    public function getFlowRequestType($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.flow_id", 
            "d.staff_id", 
            "d.step",
            "title_name" => "t.name",
            "p.has_leader"
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('d' => 'request_flow_details'), 'd.flow_id = p.id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = d.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        
        $select->where('p.from_price < ?', $params['total_price_sn']);
        $select->where('p.to_price >= ? OR p.to_price IS NULL OR p.to_price = 0', $params['total_price_sn']);
        $select->where('p.request_type = ?', $params['request_type']);
        $select->where('p.is_del = 0');
        $select->where('d.is_del = 0');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getFlowTypeGroup($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.flow_id", 
            "d.staff_id", 
            "d.step",
            "title_name" => "t.name",
            "p.has_leader"
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('d' => 'request_flow_details'), 'd.flow_id = p.id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = d.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        
        $select->where('p.from_price < ?', $params['total_price_sn']);
        $select->where('p.to_price >= ? OR p.to_price IS NULL OR p.to_price = 0', $params['total_price_sn']);
        $select->where('p.request_type_group = ?', $params['request_type_group']);
        $select->where('p.is_del = 0');
        $select->where('d.is_del = 0');
        $select->where('p.request_type IS NULL');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getFlowTeam($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.flow_id", 
            "d.staff_id", 
            "d.step",
            "title_name" => "t.name",
            "p.has_leader"
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('d' => 'request_flow_details'), 'd.flow_id = p.id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = d.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        
        $select->where('p.from_price < ?', $params['total_price_sn']);
        $select->where('p.to_price >= ? OR p.to_price IS NULL OR p.to_price = 0', $params['total_price_sn']);
        $select->where('p.team_id = ?', $params['team_id']);
        $select->where('p.is_del = 0');
        $select->where('d.is_del = 0');
        $select->where('p.request_type_group IS NULL');
        $select->where('p.request_type IS NULL');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getFlowDepartment($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.flow_id", 
            "d.staff_id", 
            "d.step",
            "title_name" => "t.name",
            "p.has_leader"
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('d' => 'request_flow_details'), 'd.flow_id = p.id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = d.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        
        $select->where('p.from_price < ?', $params['total_price_sn']);
        $select->where('p.to_price >= ? OR p.to_price IS NULL OR p.to_price = 0', $params['total_price_sn']);
        $select->where('p.department_id = ?', $params['department_id']);
        $select->where('p.is_del = 0');
        $select->where('d.is_del = 0');
        $select->where('p.request_type_group IS NULL');
        $select->where('p.request_type IS NULL');
        $select->where('p.team_id IS NULL');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function to_vnd($price, $currency) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id',
            'to_vnd'
        );

        $select->from(array('p' => 'currency'), $arrCols);
        
        $select->where('p.id = ?', $currency);
        $result = $db->fetchRow($select);
        
        $to_vnd = $price*$result['to_vnd'];
        
        return $to_vnd;
    }

}