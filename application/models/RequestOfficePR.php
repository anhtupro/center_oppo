<?php
class Application_Model_RequestOfficePR extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_office_pr';
    function getData($params =null){

        $db = Zend_Registry::get('db');

        $col = array(
            'p.id',
            'p.request_id',
            'p.name',
            'p.type',
            'p.status',
            'p.created_at',
            'p.urgent_date',
            'p.shipping_address',
            'p.project_id',
            "created_by" => "CONCAT(s.firstname, ' ',s.lastname)" ,
            "department_name" => 't.name' ,
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        //$select->joinleft(array('d'=>'purchasing_request_details'), 'd.pr_id = p.id', array());
        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());
        $select->joinleft(array('t'=>'team'), 's.department = t.id', array());

        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }
        $select->group('p.id');

        $select->order('p.created_at DESC');
        $result = $db->fetchRow($select);

        return $result;
    }


}