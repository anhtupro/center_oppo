<?php
class Application_Model_HistoricalSellinDeduction extends Zend_Db_Table_Abstract
{
    protected $_name = 'historical_sellin_deduction';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        if (isset($params['export']) && $params['export'])
            $get = array(
                'p.level',
                'd.sales_sn',
                'd.invoice_number',
                'd.invoice_time',
                'd.invoice_sign',
                'd.product_id',
                'd.quantity',
                'd.value_vat',
                'd.value',
            );
        else
            $get = array(
                'p.level',
                'p.value_vat',
                'p.value',
                'p.rate',
                'p.total',
            );

        $default_get = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.distributor_id');

        $select = $db->select();

        if (isset($params['export']) && $params['export']) {
            $select
                ->from(array('p' => $this->_name), $default_get)
                ->join(array('d' => 'historical_sellin_deduction_detail'), 'p.id=d.historical_sellin_deduction_id', $get);
        } else {
            $select->from(array('p' => $this->_name), array_merge($default_get, $get));
        }

        $select->join(array('dt' => WAREHOUSE_DB.'.distributor'), 'dt.id=p.distributor_id', array('dt.store_code', 'dt.district', 'dt.title'));

        if (isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('p.distributor_id = ?', $params['distributor_id']);

        if (isset($params['invoice_number']) and $params['invoice_number'])
            $select->where('d.invoice_number = ?', $params['invoice_number']);

        if (isset($params['invoice_time']) and $params['invoice_time'])
            $select->where('d.invoice_time = ?', $params['invoice_time']);

        if (isset($params['from']) and date_create_from_format("m/Y", $params['from']))
            $select->where('DATE(FROM_UNIXTIME(p.month)) >= ?', date_create_from_format("m/Y", $params['from'])->format("Y-m-01"));

        if (isset($params['to']) and date_create_from_format("m/Y", $params['to']))
            $select->where('DATE(FROM_UNIXTIME(p.month)) <= ?', date_create_from_format("m/Y", $params['to'])->format("Y-m-01"));

        if (isset($params['invoice_sign']) and $params['invoice_sign'])
            $select->where('d.invoice_sign LIKE ?', $params['invoice_sign']);

        if (isset($params['name']) and $params['name'])
            $select->where('dt.title LIKE ?', '%'.$params['name'].'%');

        if (isset($params['product_id']) and $params['product_id'])
            $select->where('d.product_id = ?', $params['product_id']);

        if (isset($params['loyalty_plan_id'])) {
            if (is_array($params['loyalty_plan_id']) && count($params['loyalty_plan_id'])) {
                $select->where('p.level IN (?)', $params['loyalty_plan_id']);
            } elseif (is_numeric($params['loyalty_plan_id']) && intval($params['loyalty_plan_id'])) {
                $select->where('p.level = ?', intval($params['loyalty_plan_id']));
            }
        }

        if (isset($params['regional_market']) && $params['regional_market']) {
            $district_arr = array();
            if (is_array($params['regional_market']) && count($params['regional_market'])) {
                foreach ($params['regional_market'] as $_key => $_id)
                    $this->get_districts_by_province($_id, $district_arr);
            } elseif (is_numeric($params['regional_market']) && $params['regional_market']) {
                $this->get_districts_by_province($params['regional_market'], $district_arr);
            }

            if (count($district_arr))
                $select->where('dt.district IN (?)', $district_arr);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['area']) && $params['area']) {
            $district_arr = array();
            if (is_array($params['area']) && count($params['area'])) {
                foreach ($params['area'] as $_key => $_area_id)
                    $this->get_districts_by_area($_area_id, $district_arr);

            } elseif (is_numeric($params['area']) && $params['area']) {
                $this->get_districts_by_area($params['area'], $district_arr);
            }

            if (count($district_arr))
                $select->where('dt.district IN (?)', $district_arr);
            else
                $select->where('1=0', 1);
        }

        $select->order('p.distributor_id');

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    private function get_districts_by_province($province_id, &$district_arr)
    {
        $QRegion = new Application_Model_RegionalMarket();
        $district_cache = $QRegion->get_district_by_province_cache($province_id);

        if ($district_cache)
            foreach ($district_cache as $key => $value)
                $district_arr[] = intval($key);
    }

    private function get_districts_by_area($area_id, &$district_arr)
    {
        $QRegion = new Application_Model_RegionalMarket();
        $district_cache = $QRegion->get_district_by_area_cache($area_id);

        if ($district_cache)
            foreach ($district_cache as $key => $value)
                $district_arr[] = intval($value);
    }
}

