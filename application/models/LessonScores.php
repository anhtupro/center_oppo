<?php
class Application_Model_LessonScores extends Zend_Db_Table_Abstract
{
    protected $_name = 'lesson_scores';

    function getAVGScores($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => $this->_name),
                    array('avg(a.scores) as avg','count(*) as count')
                )
                ->where('a.staff_cmnd = ?', $id)
                ->group('a.lesson_id');
        $result = $db->fetchAll($select);
        return $result;
    }

    
    function getScoresDetail($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => $this->_name),
                    array('b.id','c.cmnd','title_lesson'=>'b.title','content_lesson'=>'b.content','dtb'=>'avg(a.scores)','GROUP_CONCAT(scores) as scores')
                )
                ->joinLeft(array('b' => 'lesson'), 'a.lesson_id = b.id', array())
                ->joinLeft(array('c' => 'staff_training'), 'a.staff_cmnd = c.cmnd', array())
                ->where('a.staff_cmnd = ?', $id)
                ->group('b.id')
                ->order(array('a.created_at DESC'));;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    
    function getScoresDetail2($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => $this->_name),
                    array('a.*','dtb'=>'AVG(a.scores)','title_lesson'=>'b.title','content_lesson'=>'b.content','GROUP_CONCAT(scores) as scores')
                )
                ->joinLeft(array('b' => 'lesson'), 'a.lesson_id = b.id', array())
                ->joinLeft(array('c' => 'staff'), 'a.staff_cmnd = c.ID_number', array())
                ->where('a.staff_cmnd = ?', $id)
                ->group('b.id')
                ->order(array('a.created_at DESC'));
        $result = $db->fetchAll($select);
        
        
        return $result;
    }
    
    function getScoresDetail3($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => $this->_name),
                    array('a.*','dtb'=>'AVG(a.scores)','title_lesson'=>'b.title','content_lesson'=>'b.content','GROUP_CONCAT(scores) as scores')
                )
                ->joinLeft(array('b' => 'lesson'), 'a.lesson_id = b.id', array())
                ->where('a.staff_cmnd = ?', $id)
                ->group('a.lesson_id')
                ->order('a.lesson_id DESC');
        $result = $db->fetchAll($select);
        if($_GET['dev'] == 2){
            echo $select;exit;
        }
        
        
        return $result;
    }

    function getLessonScroreStaff($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id",
            "p.lesson_id",
            "p.total_pass",
            "p.total_fail",
            "p.title",
            "p.dtb",
            "p.ketqua",
            "p.date",
        );

        $select->from(array('p' => 'log_pgs_info_lesson'), $arrCols);

        $select->where('p.staff_id = ?', $params['pg_id']);
        $select->where('p.date = ?', date('Y-m-d'));
        
        $result = $db->fetchAll($select);

        return $result;
    }

    function getLessonScroreByStaff($staff_id){
        $db = Zend_Registry::get('db');

        $sql = "SELECT
            p.id,
            (CASE WHEN MAX(s.scores) = 10 THEN 10 
                        WHEN ((MAX(s.scores) < 10 OR MAX(s.scores) IS NULL) 
                        AND DATE( NOW() ) > DATE( l.to_test ) 
                        AND DATE( l.to_test ) > '2018-03-01') 
                        AND ll.id IS NULL
                        AND (c.channel_id IS NULL OR log.channel_id IN (c.channel_id))
                        THEN - 30 
        ELSE 0 END 
                ) score,
                l.id lesson_id,
                l.title lesson_name
            FROM
                (
                    SELECT p.id,p.ID_number,
                                IF( l.from_date IS NULL, p.joined_at, FROM_UNIXTIME( l.from_date ) ) from_title,
                                IF( l.to_date IS NULL, IFNULL( p.off_date, NOW( ) ), FROM_UNIXTIME( l.to_date ) ) to_title 
                    FROM
                    staff p
                    LEFT JOIN staff_transfer s ON p.id = s.staff_id
                    LEFT JOIN staff_log_detail l ON l.transfer_id = s.id AND l.info_type = 8 
                    WHERE p.title IN ( 182, 183, 274, 312, 293, 419, 420 ) AND p.off_date IS NULL AND p.id = ".$staff_id."
                ) p
                INNER JOIN lesson l ON l.to_test IS NOT NULL AND l.to_test BETWEEN p.from_title AND p.to_title
                LEFT JOIN ( SELECT * FROM `leave_detail` WHERE `status` IN ( 0, 1 ) AND `leave_type` <> 2 ) ll ON p.id = ll.staff_id 
                AND DATE( l.to_test ) BETWEEN ll.from_date AND ll.to_date
                LEFT JOIN lesson_scores s ON l.id = s.`lesson_id` AND s.staff_cmnd = p.ID_number AND l.published = 1 AND DATE( s.created_at ) <= DATE( l.to_test ) 
                LEFT JOIN (
                        SELECT p.staff_id, p.store_id, FROM_UNIXTIME(p.joined_at) joined_at, FROM_UNIXTIME(p.released_at) released_at,v.channel_id
                        FROM store_staff_log p
                        LEFT JOIN store s ON s.id = p.store_id
                        LEFT JOIN v_channel_dealer v ON v.d_id = s.d_id
                    ) log ON log.staff_id = p.id AND log.joined_at <= l.from_test AND (log.released_at >= l.to_test OR log.released_at IS NULL)
            LEFT JOIN lesson_by_channel c ON c.lesson_id = l.id
            GROUP BY p.id,l.id,ll.id
        ";

        $stmt     = $db->query($sql);
        $data = $stmt->fetchAll();

        return $data;

    }

    function getScoreByArea($params){ //function new 

        $db = Zend_Registry::get('db');
        $select=$db->select();
        $select1=$db->select();

        $caseWhenNestedSelect=new Zend_Db_Expr(
        '(CASE WHEN (max(lc.scores) = 10 OR lc.scores is null) THEN 0 ELSE 1 END) AS `fail_staff`,
        (CASE WHEN (max(lc.scores) = 10 ) THEN 1 ELSE 0 END ) AS `pass_staff`'
        );

        $select1->from(['lc'=>'lesson_scores'],['lc.*',$caseWhenNestedSelect]);
        $select1->joinLeft(['l' => 'lesson'],'lc.lesson_id=l.id AND lc.created_at >= l.from_test AND lc.created_at <= l.from_test',[]);
        $select1->joinLeft(['s'=>'staff'],'s.ID_number=lc.staff_cmnd',[
            // 's.lastname','s.firstname','s.department','s.team','s.title',
            'staff_id' =>'s.id'
        ]);

        if(!empty($params['title'])){
            $select1->where('s.title IN (?)',$params['title']);
        }
        if(!empty($params['lesson_id'])){
            $select1->where('lc.lesson_id = ? ',$params['lesson_id']);
        }
        $select1->group('lc.staff_cmnd');
        // if(!empty($params['area_id'])){
        //     $select1->where('rmk.area_id = ? ',$params['area_id']);
        // }
        $select1->where('s.off_date is null AND s.status=1',null);


        $caseWhenSelect=new Zend_Db_Expr(
        'SUM(nestedSelect.pass_staff) AS pass,
        SUM(CASE WHEN (nestedSelect.fail_staff =1 OR nestedSelect.scores is null) THEN 1 ELSE 0 END )AS fail,
        COUNT(ls.id) AS total
        '
        );
        $select->from(['ls'=>'lesson_staff'],['ls.*',$caseWhenSelect]);
        $select->joinLeft(['s'=>'staff'],'s.id=ls.staff_id',['s.lastname','s.firstname','s.department','s.team','s.title','staff_id' =>'s.id']);
        $select->joinLeft(['rmk'=>'regional_market'],'rmk.id=s.regional_market',['rmk.area_id']);
        $select->joinLeft(['a'=>'area'],'a.id=rmk.area_id',['a.bigarea_id','area_name' =>'a.name','area_id'=>'a.id']);
        $select->joinLeft(['bigarea'],'bigarea.id=a.bigarea_id',['region_name'=>'bigarea.name','region_id'=>'bigarea.id']);
        $select->joinLeft(['nestedSelect' => new Zend_Db_Expr('('.$select1.')')],'ls.lesson_id=nestedSelect.lesson_id AND ls.staff_id=nestedSelect.staff_id',['nestedSelect.*']);

        if(!empty($params['lesson_id'])){
            $select->where('ls.lesson_id = ? ',$params['lesson_id']);
        }
        if(!empty($params['big_area_group'])){
            $select->group('bigarea.id');
        }elseif(!empty($params['area_group'])){
            $select->group('a.id');
        }
        if($_GET['dev']){
            echo $select->__toString();
        }

        $result=$db->fetchAll($select);
        return $result;


    }
    


}