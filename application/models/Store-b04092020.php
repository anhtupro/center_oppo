<?php
class Application_Model_Store extends Zend_Db_Table_Abstract
{
	protected $_name = 'store';

    const status_on = 0;
    const status_disable = 1;
    const status_all = 2;

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        // echo $params['asm'];die;
        // echo "<pre>";print_r($params);die;
        $select = $db->select()
            ->distinct()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'))
            ->joinLeft(array('vsa'=>'v_store_area'),'vsa.store_id = p.id',array())
        ;

        $select->joinLeft(array('distributor' => WAREHOUSE_DB.'.distributor'),
            'distributor.id = p.d_id',
            array('d_parent_id'=>'distributor.parent'));

        $select->joinLeft(array('stl' => 'store_staff_log'),
            'p.id = stl.store_id and stl.is_leader = 3 and stl.released_at is null',
            array());

        $select->joinLeft(array('stl_pg' => 'store_staff_log'),
            'p.id = stl_pg.store_id and stl_pg.is_leader = 0 and stl_pg.released_at is null',
            array());

        $select->joinLeft(array('stl_sale_tructiep' => 'store_staff_log'),
            'p.id = stl_sale_tructiep.store_id and stl_sale_tructiep.is_leader = 1 and stl_sale_tructiep.released_at is null',
            array());

        $select->joinLeft(array('stl_sale_giantiep' => 'store_leader_log'),
            'p.id = stl_sale_giantiep.store_id and stl_sale_giantiep.released_at is null',
            array());

        // $select->joinLeft(array('sss' => 'store_staff_log'),
        //     'p.id = sss.store_id and sss.released_at is null',
        //     array());
            

        $select->joinLeft(array('r' => 'regional_market'),
            'vsa.regional_market = r.id',
            array('regional_market_name'=>'r.name'));

        $select->joinLeft(array('d' => 'regional_market'),
            'd.id = p.district',
            array('district_name'=>'d.name'));

        $select->joinLeft(array('a' => 'area'),
            'r.area_id = a.id',
            array('area_name'=>'a.name'));

        if  (
                (isset($params['staff_email']) and $params['staff_email']) 
                    || (isset($params['export']) and $params['export'])
                    || (isset($params['staff_id']) AND $params['staff_id'])
            ){

            

            $select->joinLeft(array('s1' => 'staff'),
                'stl.staff_id = s1.id',
                array('firstname_store_leader' => 's1.firstname','lastname_store_leader' => 's1.lastname', 'code_store_leader' => 's1.code'));

            $select->joinLeft(array('s2' => 'staff'),
                'stl_pg.staff_id = s2.id',
                array('code_pg' => 'GROUP_CONCAT(s2.code separator " / " )','name_pg' => 'GROUP_CONCAT(CONCAT(s2.firstname," ",s2.lastname)  separator " / " )' ));

            $select->joinLeft(array('s3' => 'staff'),
                'stl_sale_tructiep.staff_id = s3.id',
                array('firstname_sale_tructiep' => 's3.firstname','lastname_sale_tructiep' => 's3.lastname', 'code_sale_tructiep' => 's3.code'));

            $select->joinLeft(array('s4' => 'staff'),
                'stl_sale_giantiep.staff_id = s4.id',
                array('firstname_sale_giantiep' => 's4.firstname','lastname_sale_giantiep' => 's4.lastname' , 'code_sale_giantiep' => 's4.code'));


            if (isset($params['staff_email']) and $params['staff_email'])
                $select->where('s.email LIKE ?', '%'.$params['staff_email'].'%');


            if (isset($params['gps']) and $params['gps'] and $params['gps'] == 1)
            $select->where('(LENGTH(p.longitude) > 0 and LENGTH(p.latitude) > 0)' );

            if (isset($params['gps']) and $params['gps'] and $params['gps'] == 2)
                $select->where('(p.del <> 1 or p.del is null) and (p.longitude is null or p.latitude is null) or LENGTH(p.latitude) = 0 or LENGTH(p.longitude) = 0' );
            if(isset($params['staff_id']) AND $params['staff_id']){
                $select->joinLeft(array('ss' => 'store_staff_log'),
                'p.id = ss.store_id',
                array('joined_at'=>'ss.joined_at'));
            
            $select->joinLeft(array('s' => 'staff'),
                'ss.staff_id = s.id and ss.released_at is null',
                array('s.firstname', 's.lastname'));
                $select->where('s.id = ?',$params['staff_id']);
            }
        }

        if (isset($params['asm']) and $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            // echo "<pre>";print_r($list_regions);die;
            $list_district = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();
            $list_regional_market = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();
            

            
            // if($params['area_id'] == 51){
            //     $select_hn_2  = $db->select()
            //     ->from(array('a'=>'regional_market'),'a.id')
            //     ->where('a.parent = ?',3433);
            //     $hn2 = $db->fetchAll($select_hn_2);

            //     $arr_hn2 = array();
            //     if(count($hn2)){
            //         foreach($hn2 as $value){
            //             $arr_hn2[] = $value;
            //         }
            //         $list_regions = array_merge($list_regions,$arr_hn2);
            //     }

            // }

            if (count($list_regions)){
                $select->where('vsa.district IN (?)', $list_district); // lọc staff thuộc regional_market trên
                $select->where('vsa.regional_market IN (?)',$list_regional_market);
            }else
                $select->where('1=0', 1);
        }

        if (isset($params['sale-gps']) and $params['sale-gps'])
            $select->where('stl_sale_tructiep.staff_id = ? ', $params['sale-gps']);

        if (isset($params['gps']) and $params['gps'] and $params['gps'] == 1)
            $select->where('(LENGTH(p.longitude) > 0 and LENGTH(p.latitude) > 0)' );

        if (isset($params['gps']) and $params['gps'] and $params['gps'] == 2)
            $select->where('(p.longitude is null or p.latitude is null) or LENGTH(p.latitude) = 0 or LENGTH(p.longitude) = 0' );

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('p.id = ? ', $params['store_id']);

        if (isset($params['partner_id']) and $params['partner_id'] != '')
            $select->where('distributor.partner_id = ? ', $params['partner_id']);

        if (isset($params['d_id']) and $params['d_id'])
            $select->where('p.d_id = ? ', $params['d_id']);

        if (isset($params['address']) and $params['address'])
            $select->where('p.company_address LIKE ? OR p.shipping_address LIKE ? ', '%'.$params['address'].'%');

        if (isset($params['name']) and $params['name']){
           // $select->where('(p.name LIKE ?', '%'.$params['name'].'%');
			$select->where("(REPLACE( p.name, '  ', ' ' ) LIKE ?", '%'.$params['name'].'%');
            $select->orwhere('p.short_name LIKE ?)', '%'.$params['name'].'%');
        }

        // if (isset($params['area_id']) and $params['area_id']) {
        //     if (is_array($params['area_id']) && $params['area_id'])

        //     elseif (is_numeric($params['area_id']) && intval($params['area_id']))
        //         $select->where('a.id = ?', $params['area_id']);
        // }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                $select->where('p.district IN (?)', $params['district']);
            elseif (is_numeric($params['district']) && $params['district'])
                $select->where('p.district = ?', intval($params['district']));
            else
                $select->where('1=0', 1);

        } elseif (isset($params['regional_market']) && $params['regional_market']) {
            $district_arr = array();

            if (is_array($params['regional_market']) && count($params['regional_market'])) {
                foreach ($params['regional_market'] as $_key => $_id)
                    $this->get_districts_by_province($_id, $district_arr);

            } elseif (is_numeric($params['regional_market']) && $params['regional_market']) {
                $this->get_districts_by_province($params['regional_market'], $district_arr);
            }

            if (count($district_arr))
                $select->where('p.district IN (?)', $district_arr);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['area_id']) && $params['area_id']) {
            $district_arr = array();

            if (is_array($params['area_id']) && count($params['area_id'])) {
                foreach ($params['area_id'] as $_key => $_area_id)
                    $this->get_districts_by_area($_area_id, $district_arr);

            } elseif (is_numeric($params['area_id']) && $params['area_id']) {
                $this->get_districts_by_area($params['area_id'], $district_arr);
            }

            if (count($district_arr))
                $select->where('p.district IN (?)', $district_arr);
            else
                $select->where('1=0', 1);
        }

        // if (isset($params['regional_market']) and $params['regional_market'])
        //     $select->where('p.regional_market = ?', $params['regional_market']);

        // if (isset($params['district']) and $params['district'])
        //     $select->where('p.district = ?', $params['district']);

        if ( isset( $params['have_pg'] ) and $params['have_pg'] == 1 and  isset( $params['have_sales'] ) and $params['have_sales'] == 1 )
            $select->where('stl_pg.staff_id IS NOT NULL and stl_sale_tructiep.staff_id IS NOT NULL');

        elseif ( isset( $params['have_pg'] ) and $params['have_pg'] == 1 and ! ( isset( $params['have_sales'] ) and $params['have_sales'] == 1 ) )
            $select->where('stl_pg.staff_id IS NOT NULL');
        elseif ( isset( $params['have_store_leader'] ) and $params['have_store_leader'] == 1 and ! ( isset( $params['have_sales'] ) and $params['have_sales'] == 1 ) )
            $select->where('stl.staff_id IS NOT NULL');

        elseif ( isset( $params['have_sales'] ) and $params['have_sales'] == 1 and !( isset( $params['have_pg'] ) and $params['have_pg'] == 1 ) )
            $select->where('stl_sale_tructiep.staff_id IS NOT NULL');

        elseif ( isset( $params['no_staff'] ) and $params['no_staff'] == 1 )
            $select->where('stl_pg.staff_id IS NULL and stl.staff_id IS NULL and stl_sale_tructiep.staff_id IS NULL and stl_sale_giantiep.staff_id is null');

        elseif(isset($params['sales_direct']) and $params['sales_direct'] == 1){
            //những shop mà sale/sale leader quản lý trự tiếp
            $select->where('ss.id IS NOT NULL');
            $select->having('SUM(ss.is_leader) = 1');
            $select->having('COUNT(ss.id) = 1');
            
        }

        

        if (isset($params['diamond']) && $params['diamond']) {
           
            $select->join(array('dm' => 'dealer_loyalty'), 'p.d_id=dm.dealer_id', array());

            $select->joinLeft(array('dm2' => 'loyalty_plan'),  'dm.loyalty_plan_id = dm2.id', array('loyalty_plan' => 'dm2.name'));


            if (isset($params['diamond_level']) && intval($params['diamond_level']))
                $select->where('dm.loyalty_plan_id = ?', intval($params['diamond_level']));

             if (isset($params['diamond_store_id']) && intval($params['diamond_store_id']))
                $select->where('p.id = ?', intval($params['diamond_store_id']));



            if (isset($params['diamond_month']) && date_create_from_format('d/m/Y', $params['diamond_month']))
                $select
                    ->where('dm.from_date <= ?', date_create_from_format('d/m/Y', $params['diamond_month'])->format('Y-m'))
                    ->where('dm.to_date >= ?', date_create_from_format('d/m/Y', $params['diamond_month'])->format('Y-m'));
        }

        if (isset($params['sort']) and $params['sort']) {
            $order_str = $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' p.name '.$collate . $desc;
            } elseif ( $params['sort'] == 'area' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= 'a.name' . $collate . $desc;
            } elseif ( $params['sort'] == 'district' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= 'd.name' . $collate . $desc;
            } elseif ( $params['sort'] == 'regional_market' ) {
                $order_str =  ' r.name ' . $collate . $desc;
            } else
                $order_str =  ' p.'.$params['sort'].' ' . $collate . $desc;

            if ($order_str)
                $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('p.id');

        if(!empty($_GET['dev'])){
            echo $select->__toString();
        exit;
        }
        
        if ( $limit && ! ( isset($params['export']) && $params['export'] ) )
            $select->limitPage($page, $limit);
        else{
           // $select->where('p.del IS NULL OR p.del <> ?', 1);
        }


        // PC::debug($select->__toString());
        // echo "<pre>";print_r($select->__toString());die;
        $result = $db->fetchAll($select);
        // echo "<pre>";print_r($result);die;

        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function fetchPaginationLeader($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->joinLeft(array('g' => 'store_leader'),
            'p.id = g.store_id',
            array('status' => 'g.status', 'staff_id' => 'g.staff_id'));

        $select->joinLeft(array('lg' => 'store_leader_log'),
            'g.staff_id = lg.staff_id AND g.store_id = lg.store_id AND released_at IS NULL',
            array('joined_at' => 'lg.joined_at', 'log_id' => 'lg.id'));

        $select->join(array('r' => 'regional_market'),
            'p.regional_market = r.id',
            array('regional_market_name'=>'r.name'));

        $select->join(array('a' => 'area'),
            'r.area_id = a.id',
            array('area_name'=>'a.name'));

        $select->joinLeft(array('s' => 'staff'),
                'g.staff_id = s.id',
                array( 's.firstname', 's.lastname', 's.email'));

        if (isset($params['staff_email']) and $params['staff_email'])
            $select->where('s.email LIKE ?', '%'.$params['staff_email'].'%');

        if (isset($params['address']) and $params['address'])
            $select->where('p.company_address LIKE ? OR p.shipping_address LIKE ? ', '%'.$params['address'].'%');

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        if (isset($params['area_id']) and $params['area_id'])
            $select->where('a.id = ?', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.regional_market = ?', $params['regional_market']);

        if ( isset( $params['have_leader'] ) and $params['have_leader'] == 1 ) {
            $select
                ->where('g.status = 1')
                ->where('g.staff_id IS NOT NULL');
            
        }

        elseif ( isset( $params['no_leader'] ) and $params['no_leader'] == 1 )
            $select->where('g.staff_id IS NULL');

        elseif ( isset( $params['pending'] ) and $params['pending'] == 1 ) {
            $select
                ->where('g.status = 0')
                ->where('g.staff_id IS NOT NULL');
        }

        $QRegion = new Application_Model_RegionalMarket();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if (isset($params['for_asm']) && $params['for_asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache();
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();

            if (count($list_regions))
                $select->where('p.district IN (?)', $list_regions);
            else
                $select->where("1=0", 1);
        }

        if (isset($params['for_leader']) && $params['for_leader']) {
            $region = $QRegion->find($userStorage->regional_market);
            $region = $region->current();

            if ($region) {
                $where = $QRegion->getAdapter()->quoteInto('area_id = ?', $region['area_id']);
                $regions = $QRegion->fetchAll($where);
                
                $region_list = array();

                foreach ($regions as $reg)
                    $region_list[] = $reg['id'];
                
                if (is_array($region_list) && count($region_list) > 0)
                    $select->where('p.regional_market IN (?)', $region_list);
                else
                    $select->where("1=0", 1);
            } else {
                $select->where("1=0", 1);
            }
        }

        $select->where('p.del = 0 OR p.del IS NULL');

        if (isset($params['sort']) and $params['sort']) {
            $order_str = $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' p.name '.$collate . $desc;

            } elseif ( $params['sort'] == 'area' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= 'a.name' . $collate . $desc;

            } elseif ( $params['sort'] == 'leader' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(s.firstname, " ",s.lastname) ' . $collate . $desc;

            } elseif ( $params['sort'] == 'company_address' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' p.company_address ' . $collate . $desc;

            } elseif ( $params['sort'] == 'regional_market' ) {
                $order_str =  ' r.name ' . $collate . $desc;
                
            } else
                $order_str =  ' p.'.$params['sort'].' ' . $collate . $desc;

            if ($order_str)
                $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('p.id');

        if ( $limit && ! ( isset($params['export']) && $params['export'] ) )
            $select->limitPage($page, $limit);
        else
            $select->where('p.del IS NULL OR p.del <> ?', 1);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_assigned_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_assigned_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();

            $db = Zend_Registry::get('db');


            if ($data){
                foreach ($data as $item){
                    $select = $db->select()
                        ->from(array('p' => 'store_staff'),
                            array('p.*'))
                        ->join(array('s' => 'staff'),
                            's.id = p.staff_id',
                            array('s.firstname', 's.lastname', 's.group_id'));

                    $select->where('p.store_id = ?', $item->id);

                    $staffs = $db->fetchAll($select);

                    $sales = $PGs = '';

                    //get staffs
                    if ($staffs){
                        foreach ($staffs as $staff){
                            if ($staff['group_id']==SALES_ID){
                                $break_line = $sales !== '' ? "\n" : '';
                                $sales .= $break_line . $staff['firstname'].' '.$staff['lastname'];

                            }elseif ($staff['group_id']==PGPB_ID){

                                $break_line = $PGs !== '' ? "\n" : '';
                                $PGs .= $break_line . $staff['firstname'].' '.$staff['lastname'];
                            }

                        }
                    }

                    $result[$item->id] = array(
                        'sales' => $sales,
                        'PGs' => $PGs,
                    );
                }
            }
            $cache->save($result, $this->_name.'_assigned_cache', array(), null);
        }
        return $result;
    }

    function compare($full_store_name)
    {
        $full_store_name = My_String::trim($full_store_name);
        $check_list = array();

        $db = Zend_Registry::get('db');

        $store_name_split = explode('-', $full_store_name);

        if (is_array($store_name_split) && !empty($store_name_split[0])) {
            $store_name = $store_name_split[0];
        } else {
            $store_name = $full_store_name;
        }
        
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('del = 0 OR del IS NULL', 1);
        $where[] = $this->getAdapter()->quoteInto('name LIKE ?', '%'.$store_name.'%');
        $check_store_name = $this->fetchAll($where, 'name', 5);

        foreach ($check_store_name as $key => $store) {
            $check_list[ $store['id'] ] = $store['name'];
        }

        //
        
        $full_store_name_like = str_replace(array('đường', 'số', 'ngõ', 'hẻm', 'huyện', 'quận', 'xã', 'phường', 'thôn', 'ấp', 'xóm', 'thành phố', 'tp', 'tỉnh', ',', '-', '(', ')'), 
                                                '', mb_strtolower($full_store_name, 'UTF-8'));
        
        $full_store_name_like = preg_replace('/[\s]+/', '%', $full_store_name_like);
        $full_store_name_like = preg_replace('/[\%]+/', '%', $full_store_name_like);

        $where = array();
        $where[] = $this->getAdapter()->quoteInto('del = 0 OR del IS NULL', 1);
        $where[] = $this->getAdapter()->quoteInto('name LIKE ?', '%'.$full_store_name_like.'%');

        $check_store_name = $this->fetchAll($where);

        $count = 0;
        foreach ($check_store_name as $key => $store) {
            if ($count++ > 10) break;

            if (!isset($check_list[ $store['id'] ]))
                $check_list[ $store['id'] ] = $store['name'];
        }

        //

        $store_list = $this->get_cache();

        $count = 0;
        foreach ($store_list as $_store_id => $_store_name) {
            if ($count++ > 10) break;

            if ( similar_text($_store_name, $full_store_name) > 75 && !isset($check_list[ $_store_id ]))
                $check_list[ $_store_id ] = $_store_name;
        }

        
        return $check_list;
    }

    public function get_by_district_id_list($district_id, $status = self::status_all)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => $this->_name), array('s.id', 's.name'));

        if (is_array($district_id) && count($district_id)){
            $select->where('s.district IN (?)', $district_id);
        }elseif (is_numeric($district_id) && $district_id){
            $select->where('s.district = ?', intval($district_id));
        }

        if($status == 0){
            $select->where('s.del = ? OR s.del IS NULL',0);
        }elseif($status == 1){
            $select->where('s.del = ?',1);
        }else{
            //tất cả
        }


        $select->order('s.name');
        $list = $db->fetchAll($select);
        $result = array();
        if($list and count($list)){
            foreach($list as $item){
                $result[$item['id']] = $item;
            }
        }

        return $result;
    }

    private function get_districts_by_province($province_id, &$district_arr)
    {
        $QRegion = new Application_Model_RegionalMarket();
        $district_cache = $QRegion->get_district_by_province_cache($province_id);

        if ($district_cache)
            foreach ($district_cache as $key => $value)
                $district_arr[] = intval($key);
    }

    private function get_districts_by_area($area_id, &$district_arr)
    {
        $QRegion = new Application_Model_RegionalMarket();
        $district_cache = $QRegion->get_district_by_area_cache($area_id);

        if ($district_cache)
            foreach ($district_cache as $key => $value)
                $district_arr[] = intval($value);
    }

    public function get_store_random($params)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->query("CALL sp_get_saleout_data(?,?,?,?,?,?)", array($params['total_shop'], $params['saleout_from'], $params['saleout_to'], $params['saleout_date'], date('Y-m-d'), $params['shop_group']));
        //$stmt = $db->query("CALL sp_get_saleout_data(?,?,?,?,?,?,?)", array(40, 1, 40, '2015-10-01', '2016-02-29', '1'));
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }
	
	public function get_store_by_id($staff_id){
		if(!$staff_id){
            return false;
        }
		$db = Zend_Registry::get('db');
		
		$select = $db->select()
            ->from(array('a'=>'store_staff'),array('a.store_id'))
            ->joinLeft(array('s'=>'store'), 's.id = a.store_id',array())
            ->joinLeft(array('r'=>'regional_market'), 'r.id = s.regional_market',array('r.area_id'))
            ->where('a.staff_id = ?',$staff_id);
        $result = $db->fetchAll($select);
        return $result;
	}
	
    public function get_pg_store_sale($staff_id)
    {
        if(!$staff_id){
            return false;
        }

        $db = Zend_Registry::get('db');

        $select_title = $db->select()
            ->from(array('a'=>'staff'),array('a.title'))
            ->where('a.id = ?',$staff_id);
        $title = $db->fetchOne($select_title);

        if(!$title){
            return false;
        }

        $select = $db->select()
            ->from(array('a'=>$this->_name),array('c.id'))
            ->join(array('b'=> 'store_staff'),'a.id = b.store_id',array())
            ->join(array('d'=>'store_staff'),'a.id = d.store_id',array())
            ->join(array('c'=>'staff'),'c.id = d.staff_id',array())
            ->join(array('e'=>'team'),'e.id = c.title',array())
            ->where('b.staff_id = ?',$staff_id)
            ->where('a.del IS NULL OR a.del = ?',0)
            ->group('c.id')
        ;

            //Store leader brand shop quan ly PG_BRANDSHOP,  SENIOR_PROMOTER_BRANDSHOP
        if(in_array($title, array(403))){
            $array_permission = array(PG_BRANDSHOP, SENIOR_PROMOTER_BRANDSHOP);
            $select->where('c.title IN (?)',$array_permission);
        }elseif(in_array($title, array(417))){
            $select->where('c.title IN (182,293)');
        }
        elseif(in_array($title, My_Staff_Title::getLeader())){
            $array_permission_merge  = array_merge(My_Staff_Title::getSale() , My_Staff_Title::getPg());
            $select->where('c.title IN (?)',$array_permission_merge);
        }elseif(in_array($title,My_Staff_Title::getSale()) || in_array($title,My_Staff_Title::getPbSale())){
            $select->where('c.title IN (?)',My_Staff_Title::getPg());
            $select->where('d.is_leader = ?',0);
        }
        $result = $db->fetchAll($select);

        $data   = array();

        if(isset($result) and $result)
        {
            foreach($result as $k => $item)
                $data[] = $item['id'];
        }

        return $result;
    }

    public function get_sale_pg_leader($staff_id){
        if(!$staff_id){
            return false;
        }

        $db = Zend_Registry::get('db');

        $select_title = $db->select()
            ->from(array('a'=>'staff'),array('a.title'))
            ->where('a.id = ?',$staff_id);
        $title = $db->fetchOne($select_title);


        if(!$title){
            return false;
        }

        $cols = array(
                'staff_name' => 'CONCAT(c.firstname," ",c.lastname)',
                'email' => 'c.email',
                'title_name'=>'e.name'
            );
        if(in_array($title,My_Staff_Title::getSale())){
            $tbl_b = 'store_staff';
        }else{
            $tbl_b = 'store_leader';
        }

        $select = $db->select()
            ->from(array('a'=>$this->_name),$cols)
            ->join(array('b'=>$tbl_b),'a.id = b.store_id',array())
            ->join(array('d'=>'store_staff'),'a.id = d.store_id',array())
            ->join(array('c'=>'staff'),'c.id = d.staff_id',array())
            ->join(array('e'=>'team'),'e.id = c.title',array())
            ->where('b.staff_id = ?',$staff_id)
            ->where('a.del IS NULL OR a.del = ?',0)
            ->group('c.id')
            ;

        if(in_array($title, My_Staff_Title::getLeader())){
            //Nếu truyền id leader
            $select->where('c.title IN (?)',My_Staff_Title::getSale());
            $select->where('d.is_leader = ?',1);
        }elseif(in_array($title,My_Staff_Title::getSale())){
            //nếu truyền id sale
            $select->where('c.title IN (?)',My_Staff_Title::getPg());
            $select->where('d.is_leader = ?',0);
        }else{

        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getImageByStore($store_id){
        $db  = Zend_Registry::get('db');
        error_reporting(1);
        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
        $this->wssURI = 'http://trade-marketing.opposhop.vn/wss';
        $this->namespace = 'OPPOVN';

        $client = new nusoap_client($this->wssURI);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = false;
        $wsParams = array(
            'store_id' => $store_id
        );

        try {
            $result = $client->call("getImage", $wsParams);
            echo "<pre>";
            var_dump($result);
            echo '</pre>';
            exit;

        } catch (Exception $e) {

        }
        return array('code'=>1,'message'=>'Done');
    }

    public function getStoreByDistrict($params){
        
    }

    public function getSellOutByStore($from,$to,$store_id){
        $db = Zend_Registry::get('db');

        $sql = '
             SELECT count(*) n
             FROM imei_kpi
             WHERE timing_date BETWEEN :from AND :to  
             AND store_id = :store_id
             
        ';

        $stmt = $db->prepare($sql);
        $stmt->bindParam("from",  $from, PDO::PARAM_STR);
        $stmt->bindParam("to",  $to, PDO::PARAM_STR);
        $stmt->bindParam("store_id",  $store_id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }
	
	public function fetchPaginationStoreStaff($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        
        $select = $db->select()
            ->from(array('p'=> 'store_staff_log'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.store_id', 's.name', 's.store_code', 'staff.email'))
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('staff'=> 'staff'), 'staff.id = p.staff_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('channel'=> 'store_area_chanel'), 'channel.store_id = s.id', array())
            ->where('p.`released_at` IS NULL OR p.`released_at` >= UNIX_TIMESTAMP()', NULL);

        $select->where('p.is_leader = 1', NULL);

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id IN (?)', $params['staff_id']);
        }
        elseif(!empty($params['area_list'])){
            $select->where('IFNULL(channel.area_id,r.area_id) IN (?)', $params['area_list']);
        }

        if(!empty($params['store_name'])){
            $select->where('s.name LIKE ?', '%'.$params['store_name'].'%');
        }


        if(!empty($params['store_code'])){
            $select->where('s.store_code LIKE ?', '%'.$params['store_code'].'%');
        }

        if(!empty($params['area_id'])){
            $select->where('IFNULL(channel.area_id,r.area_id) = ?', $params['area_id']);
        }

        if(!empty($params['list_scan'])){
            $select->where('s.id IN (?)', $params['list_scan']);
        }

        if(!empty($params['list_not_scan'])){
            $select->where('s.id NOT IN (?)', $params['list_not_scan']);
        }

        if(!empty($params['email'])){
            $select->where('staff.email LIKE ?', '%'.$params['email'].'%');
        }

         if(!empty($_GET['dev'])){
            echo "<pre>";
             print_r($select->__toString());
             exit;
         }

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function fetchPaginationStoreStaffExport($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        
        $sub_select = 
        "SELECT p.imei_sn, p.good_id, p.object_id, p.object_active, p.del, 1 AS number, p.contract_id, p.contract_active, p.area_id, p.area_active
        FROM `".DATABASE_TRADE."`.imei p
        WHERE p.del <> 1 OR p.del IS NULL
        UNION 
        SELECT null, p.good_id, p.object_id, p.created_at, null, p.number, null, null, null, null
        FROM `".DATABASE_TRADE."`.store_good p
        WHERE p.status IN (1,2)";

        $select = $db->select()
            ->from(array('p'=> 'store_staff_log'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.store_id', 's.name', 's.store_code', 'staff.email', 'category_name' => 'c.name', 'i.number', 'i.imei_sn', 'i.object_active', 'area_name' => 'a.name', 'imei_delete' => 'i.del', 'contract.contract_number', 'contract_active_at' => 'i.contract_active', 'area_active_at' => 'i.area_active' ))
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('i'=> new Zend_Db_Expr('(' . $sub_select . ')')), 'i.object_id = s.id', array())
            ->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = i.good_id', array())
            ->joinLeft(array('contract'=> DATABASE_TRADE.'.contract'), 'contract.id = i.contract_id', array())
            ->joinLeft(array('staff'=> 'staff'), 'staff.id = p.staff_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('channel'=> 'store_area_chanel'), 'channel.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(channel.area_id,r.area_id)', array())
            ->where('p.`released_at` IS NULL OR p.`released_at` >= UNIX_TIMESTAMP()', NULL);

        $select->where('p.is_leader = 1', NULL);

        //Xóa hết dữ liệu scan trừ HCM1
        $select->where("(i.object_active >= '2012-08-08 00:00' AND a.id <> 1) OR (i.object_active >= '2012-08-08 00:00' AND a.id = 1) OR (i.object_id IS NULL)", NULL);

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        elseif(!empty($params['area_list'])){
            $select->where('IFNULL(channel.area_id,r.area_id) IN (?)', $params['area_list']);
        }

        if(!empty($params['store_name'])){
            $select->where('s.name LIKE ?', '%'.$params['store_name'].'%');
        }

        if(!empty($params['store_code'])){
            $select->where('s.store_code LIKE ?', '%'.$params['store_code'].'%');
        }

        if(!empty($params['area_id'])){
            $select->where('IFNULL(channel.area_id,r.area_id) = ?', $params['area_id']);
        }

        if(!empty($params['list_scan'])){
            $select->where('s.id IN (?)', $params['list_scan']);
        }

        if(!empty($params['list_not_scan'])){
            $select->where('s.id NOT IN (?)', $params['list_not_scan']);
        }

        if(!empty($params['email'])){
            $select->where('staff.email LIKE ?', '%'.$params['email'].'%');
        }

        $result = $db->fetchAll($select);

       
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getStoreBrandshop($params){
        $db = Zend_Registry::get('db');
        
        $select = $db->select()
            ->from(array('p'=> 'store'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.store_id', 'p.name', 'p.store_code'))
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = p.regional_market', array());

        if(isset($params['area_id']) and $params['area_id']){
            $select->where('r.area_id = ?', $params['area_id']);
        }

        $select->where('p.is_brand_shop = ?', 1);
        $select->where('p.del IS NULL OR p.del <> ?', 1);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListStaffBrandShop($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            'p.store_id', 
            's.name', 
            's.store_code', 
            'staff.email', 
            'staff.id'
        );

        $select = $db->select()
            ->from(array('p'=> 'store_staff_log'), $col)
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('staff'=> 'staff'), 'staff.id = p.staff_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->where('p.`released_at` IS NULL OR p.`released_at` >= UNIX_TIMESTAMP()', NULL);

        $select->where('s.is_brand_shop = 1', NULL);
        $select->where('staff.title = ?', PGPB_TITLE);

        if(!empty($params['get_title'])){
            $select->where('p.staff_id = ?', $params['get_title']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id = ?', $params['area_id']);
        }

        $select->group('p.staff_id');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function fetchPaginationStaff($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.store_id', 
            's.name', 
            's.store_code', 
            'staff.email', 
            'staff.id',
            'staff.code',
            'staff.firstname',
            'staff.lastname',
            'staff.department',
            'staff.team',
            'staff.title',
            'staff.joined_at',
            'staff.phone_number',
            'staff.status'
        );

        $select = $db->select()
            ->from(array('p'=> 'store_staff_log'), $col)
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('staff'=> 'staff'), 'staff.id = p.staff_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->where('p.`released_at` IS NULL OR p.`released_at` >= UNIX_TIMESTAMP()', NULL);

        $select->where('s.is_brand_shop = 1', NULL);
        

        if(!empty($params['name'])){
            $select->where('CONCAT(staff.firstname, " ", staff.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        if(!empty($params['code'])){
            $select->where('staff.code LIKE ?', '%'.$params['code'].'%');
        }

        if(!empty($params['email'])){
            $select->where('staff.email LIKE ?', '%'.$params['email'].'%');
        }

        if(!empty($params['title'])){
            $select->where('staff.title IN (?)', $params['title']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        $select->group('p.staff_id');

        if ($limit){
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
	
	 public function get_pg_store_brand_shop($staff_id)
    {
        if(!$staff_id){
            return false;
        }
    
        $db = Zend_Registry::get('db');
    $select_shop = "SELECT *
                    FROM `store_sales_brandshop`
                    WHERE staff_id = $staff_id";
        $stmt = $db->prepare($select_shop);
        $stmt->execute();
        $list_shop = $stmt->fetchAll();
        $stmt->closeCursor();
        
    
        if(!$list_shop){
            return false;
        }
        
        foreach($list_shop as $k => $item){
            if(!empty($item['store_id'])){
                $data[] = $item['store_id'];
            }
            
        }
        
        $string_array_staff_id = implode(",", $data);
    
        $select = $db->select()
        ->from(array('a'=>$this->_name),array('c.id'))
        ->join(array('b'=> 'store_staff'),'a.id = b.store_id',array())
        ->join(array('d'=>'store_staff'),'a.id = d.store_id',array())
        ->join(array('c'=>'staff'),'c.id = d.staff_id',array())
        ->join(array('e'=>'team'),'e.id = c.title',array())
        ->where("b.store_id IN ($string_array_staff_id)")
        ->where('c.title IN (403)')
        ->where('a.del IS NULL OR a.del = ?',0)
        ->group('c.id')
        ;

         $result = $db->fetchAll($select);
        
        $data   = array();
    
        if(isset($result) and $result)
        {
            foreach($result as $k => $item)
                $data[] = $item['id'];
        }
        
        return $data;
    
    }
    
    public function get_pg_brand_shop($staff_id){
        if(!$staff_id){
            return false;
        }
    
        $db = Zend_Registry::get('db');
    
        $select_title = $db->select()
        ->from(array('a'=>'staff'),array('a.title'))
        ->where('a.id = ?',$staff_id);
        $title = $db->fetchOne($select_title);
    
        if(!$title){
            return false;
        }
    
        if(in_array($title, array(190)) ){
    
            $select = $db->select()
            ->from(array('p' => 'store_staff_log'),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.store_id')))
                ->where('p.released_at IS NULL OR p.released_at = 0')
                ->where('p.staff_id = ? ',$staff_id);
    
        }else{
    
            $select = $db->select()
            ->from(array('p' => 'store_staff_log'),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.store_id')))
                ->where('p.released_at IS NULL OR p.released_at = 0');
                if(in_array($title, array(183,190))){
                    $select->where('p.is_leader = 1');
                }elseif(in_array($title, array(403))){
                    $select->where('p.is_leader = 3');
                }elseif(in_array($title, array(417))){
                    $select->where('p.is_leader = 5');
                }
                $select->where('p.staff_id = ? ',$staff_id);
        }
    
        $list_shop = $db->fetchAll($select);
        $result = array();
        if(!empty($list_shop)){
            foreach($list_shop as $k => $item){
                if(!empty($item['store_id'])){
                    $result[] = $item['store_id'];
                }
            }
        }
        $string_store = implode(",", $result);
        $select_staff =  $db->select()
        ->from(array('p' => 'store_staff_log'),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.staff_id, p.store_id')))
            ->join(array('st'=>'staff'),'p.staff_id = st.id',array())
            ->where('p.released_at IS NULL OR p.released_at = 0')
            ->where("p.store_id IN ($string_store) ");
    
            if(in_array($title, array(424)) ){
    
                $select_staff->where('st.title IN (403)');
    
            }elseif(in_array($title, array(403))){
    
                $select_staff->where('st.title IN (420,419)');
    
            }elseif (in_array($title, array(183,417))){
    
                $select_staff->where('st.title IN (182,293)');
    
            }elseif (in_array($title, array(190))){
    
                $select_staff->where('st.title IN (183)');
    
            }
            $result_staff = $db->fetchAll($select_staff);
    
            $data_staff_return   = array();
    
            if(isset($result_staff) and $result_staff)
            {
                foreach($result_staff as $k => $item)
                {
                    if(!empty($item['staff_id'])){
    
                        $data_staff_return[] = $item['staff_id'];
    
                    }
                }
            }
    
            return $data_staff_return;
    }
    
	public function getAll($params){
		$db = Zend_Registry::get('db');
		$select = $db->select('R.area_id,
			S.name,
			S.id')
		->from(array('S' => DATABASE_CENTER.'.'.$this->_name))
		->joinLeft(array('R'=>DATABASE_CENTER.'.regional_market'),' R.id = S.regional_market',array('area_id'=>'R.area_id'));
		   if (isset($params['area_id']) and $params['area_id'])
			$select->where('R.area_id IN (?)',$params['area_id']);

		$data = $db->fetchAll($select);
		return $data;
	}

    public function fetchPaginationStoreBrandshop($page, $limit, &$total, $params){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.name',
            "name_area"         => "a.name" , 
            "name_district"     => 'd.district_center',
            "name_province"     => "d.province_name", 
            "p.shipping_address", 
            "fullname"          => "GROUP_CONCAT( DISTINCT CONCAT(t.name, ':',s.firstname, ' ',s.lastname) )", 
            "total_sellout"     => "COUNT(DISTINCT i.imei_sn)" 
        );
        
        $select->from(array('p'=> 'store'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->joinLeft(array('d' => 'province_district'), 'd.district_id = p.district', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('l' => 'store_staff_log'), 'l.store_id = p.id AND l.released_at IS NULL', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id AND s.title IN (419, 403)', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());

        $from = NULL;

        if(!empty($params['from']))
        {
             $from = "AND i.timing_date >= '".$params['from']."'";
        }

        if(!empty($params['to']))
        {
             $from .= " AND i.timing_date <= '".$params['to']."'";
        }

        $select->joinLeft(array('i' => 'imei_kpi'), 'i.store_id = p.id '.$from, array());
        
        $select->where('p.is_brand_shop = ?', 1);
        $select->where('p.del = 0 OR p.del IS NULL', NULL);

        if(!empty($params['name']))
        {
             $select->where('p.name LIKE ?','%'.$params['name'].'%');
        }
         if(!empty($params['area']))
        {
             $select->where('r.area_id = ?',$params['area']);
        }
          if(!empty($params['province']))
        {
             $select->where('d.province_code = ?',$params['province']);
        }
         if(!empty($params['district']))
        {
             $select->where('d.district_center LIKE ?','%'.$params['district'].'%');
        }

        
        $select->group('p.id');
        
        $select->limitPage($page, $limit);
        $result  = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }

    public function getSelloutBranshopMonth($params){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'p.id', 
            'month'     =>   'MONTH(i.timing_date)', 
            'year'      =>   'YEAR(i.timing_date)', 
            'sellout'   =>   'COUNT(DISTINCT i.imei_sn)' 
        );
        
        $select->from(array('p'=> 'store'), $arrCols);
        $select->joinLeft(array('i' => 'imei_kpi'), 'i.store_id = p.id', array());
        $select->where('p.is_brand_shop = ?', 1);
        $select->where('(p.del = 0 OR p.del IS NULL)', NULL);
    
        
        $select->group(['p.id', 'MONTH(i.timing_date)', 'YEAR(i.timing_date)']);
        
        $result  = $db->fetchAll($select);
        
        return $result;
    }
    
    //Lấy shop đang hoạt động theo staff
    public function getListShopByStaff(){

        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $group_id = $userStorage->group_id;
        $title_id = $userStorage->title;
        $staff_id = $userStorage->id;
          
        $result = array();
        if((!empty($staff_id))&&(!empty($title_id))){
            
            $db = Zend_Registry::get('db');
            $select = $db->select(); 
            
            if($title_id==SALES_TITLE){//sale                
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN `store_staff_log` AS `st` ON st.store_id = p.id
                            WHERE  (p.del is NULL or p.del= 0) 
                            AND ( st.released_at IS NULL AND st.is_leader = 1 AND st.staff_id = ".$staff_id." )
                            ORDER BY `p`.`id` DESC ";
                $result = $db->fetchAll($select); 
                
            }elseif($title_id==SALES_LEADER_TITLE){//leader                   
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN store_leader_log AS sll ON sll.store_id = p.id
                            WHERE  (p.del is NULL or p.del= 0) 
                            AND ( sll.released_at IS NULL AND sll.staff_id = ".$staff_id." ) 
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC ";
                $result = $db->fetchAll($select); 
                
            }elseif(in_array($title_id, [179, 181, 308, 391, 392, 462, 463, 464, 465, 466, 467, 471])){//ASM   
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN regional_market AS rm ON rm.id = p.regional_market
                            LEFT JOIN asm ON asm.area_id = rm.area_id
                            LEFT JOIN staff AS s ON s.id = asm.staff_id
                            WHERE  (p.del is NULL or p.del= 0)
                            AND ( asm.area_id is NOT NULL AND asm.staff_id = ".$staff_id.")
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC  ";
                $result = $db->fetchAll($select);
                
            }elseif ($staff_id == SUPERADMIN_ID || in_array($group_id, array(ADMINISTRATOR_ID )) ){
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            WHERE  (p.del is NULL or p.del= 0) ";
                $result = $db->fetchAll($select); 
                
            }
            
        }
        return $result;
    }
    
    
    
    //Lấy shop ĐÃ ĐÓNG theo staff
    public function getShopDeleteByStaff(){

        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $group_id = $userStorage->group_id;
        $title_id = $userStorage->title;
        $staff_id = $userStorage->id;
          
        $result = array();
        if((!empty($staff_id))&&(!empty($title_id))){
            
            $db = Zend_Registry::get('db');
            $select = $db->select(); 
            
            if($title_id==SALES_ADMIN_TITLE){//sale admin             
                
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN regional_market AS rm ON rm.id = p.regional_market
                            LEFT JOIN asm ON asm.area_id = rm.area_id
                            LEFT JOIN staff AS s ON s.id = asm.staff_id
                            WHERE (p.del= 1)
                            AND ( asm.area_id is NOT NULL AND asm.staff_id = ".$staff_id.")
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC  ";
                $result = $db->fetchAll($select);
                
            }elseif ($staff_id == SUPERADMIN_ID || in_array($group_id, array(ADMINISTRATOR_ID )) ){
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            WHERE  (p.del= 1) ";
                $result = $db->fetchAll($select); 
                
            }
            
        }
        return $result;
    }
    
    
    //Lấy shop list open
    public function getOpenShopListAccessByStaff(){

        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $group_id = $userStorage->group_id;
        $title_id = $userStorage->title;
        $staff_id = $userStorage->id;
          
        $result = array();
        if((!empty($staff_id))&&(!empty($title_id))){
            
            $db = Zend_Registry::get('db');
            $select = $db->select(); 
            
            if($title_id==SALES_ADMIN_TITLE){//sale admin             
                
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN regional_market AS rm ON rm.id = p.regional_market
                            LEFT JOIN asm ON asm.area_id = rm.area_id
                            LEFT JOIN staff AS s ON s.id = asm.staff_id
                            WHERE ( asm.area_id is NOT NULL AND asm.staff_id = ".$staff_id.")
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC  ";
                $result = $db->fetchAll($select);
                
            }elseif ($staff_id == SUPERADMIN_ID || in_array($group_id, array(ADMINISTRATOR_ID )) ){
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p` ";
                $result = $db->fetchAll($select); 
                
            }
            
        }
        return $result;
    }
    
    //phân quyền
    public function getShopAccessByStaff(){

        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $group_id = $userStorage->group_id;
        $title_id = $userStorage->title;
        $staff_id = $userStorage->id;
          
        $result = array();
        if((!empty($staff_id))&&(!empty($title_id))){
            
            $db = Zend_Registry::get('db');
            $select = $db->select(); 
            
            if($title_id==SALES_TITLE){//sale                
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN `store_staff_log` AS `st` ON st.store_id = p.id
                            WHERE  
                            ( st.released_at IS NULL AND st.is_leader = 1 AND st.staff_id = ".$staff_id." )
                            ORDER BY `p`.`id` DESC ";
                $result = $db->fetchAll($select); 
                
            }elseif($title_id==SALES_LEADER_TITLE){//leader                   
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN store_leader_log AS sll ON sll.store_id = p.id
                            WHERE 
                            ( sll.released_at IS NULL AND sll.staff_id = ".$staff_id." ) 
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC ";
                $result = $db->fetchAll($select); 
                
            }elseif($title_id==SALE_SALE_ASM){//ASM   
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN regional_market AS rm ON rm.id = p.regional_market
                            LEFT JOIN asm ON asm.area_id = rm.area_id
                            LEFT JOIN staff AS s ON s.id = asm.staff_id
                            WHERE  
                            ( asm.area_id is NOT NULL AND s.title = 179 AND asm.staff_id = ".$staff_id.")
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC  ";
                $result = $db->fetchAll($select); 
                
            }elseif ($staff_id == SUPERADMIN_ID || in_array($group_id, array(ADMINISTRATOR_ID )) ){
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p` ";
                $result = $db->fetchAll($select); 
                
            }
            
        }
        return $result;
    }

    public function getListStore($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => 'store'], [
                        's.id',
                         's.name'
                     ])
                     ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
                     ->where('s.del = 0 OR s.del IS NULL');

        if ($params['list_area_id']) {
            $select->where('r.area_id IN (?)', $params['list_area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }


    public function getListStoreMap($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_id' => 's.id',
                'latitude' => 'a.latitude',
                'longitude' => 'a.longitude',
                'checkshop_id' => 'a.id',
                'store_type' => 's.store_type'
            ])
//            ->joinLeft(['a' => DATABASE_TRADE.'.app_checkshop'], "s.id = a.store_id AND a.updated_at = (SELECT MAX(updated_at) FROM trade_marketing.app_checkshop WHERE store_id = a.store_id)", [])
            ->joinLeft(['a' => DATABASE_TRADE.'.app_checkshop'], 'a.store_id = s.id AND a.is_last = 1', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['area' => 'area'], 'r.area_id = area.id', [])
            ->joinLeft(['di' => WAREHOUSE_DB.'.distributor'], 's.d_id = di.id', [])
            ->joinLeft(['l' => 'dealer_loyalty'], 'l.dealer_id = IF((di.parent IS NULL OR di.parent = 0), di.id, di.parent) AND l.is_last = 1', [])
            ->where('a.latitude IS NOT NULL')
            ->where('a.longitude IS NOT NULL')
            ->where("a.latitude <> '' ")
            ->where("a.longitude <> '' ")
            ->where('s.del IS NULL OR s.del = 0');

//        if ($params['category_id']) {
//            $select->join(['d' => DATABASE_TRADE.'.app_checkshop_detail'], 'a.id = d.checkshop_id AND d.category_id = '.$params['category_id'], []);
//            $select->where('d.category_id = ?', $params['category_id']);
//            $select->where('d.quantity > 0 ');
//        }
        if ($params['category_id']) {
            $select->joinLeft(['detail' => DATABASE_TRADE.'.app_checkshop_detail'], 'a.id = detail.checkshop_id AND detail.category_id = ' . $params['category_id'] , ['oppo_brand' => 'IF(detail.quantity > 0, 1, NULL)'  ]); // kiểm tra shop có biển hiệu alu không

            if ($params['brand_id']) {
                $stringBrand = implode(',', $params['brand_id']);

                $select->joinLeft(['db' => DATABASE_TRADE.'.app_checkshop_detail_brand'], 'a.id = db.checkshop_id AND db.category_id = ' . $params['category_id'] . ' AND db.quantity > 0  AND db.brand_id IN (' . $stringBrand . ')' , ['other_brand' => "GROUP_CONCAT(db.brand_id)"]);

//                $select->joinLeft(['c' => DATABASE_TRADE.'.category'], 'c.parent_id = ' . $params['category_id'], []);
//                $select->joinLeft(['db' => DATABASE_TRADE.'.app_checkshop_detail_brand'], 'a.id = db.checkshop_id AND db.quantity > 0 AND db.category_id = c.id AND db.brand_id IN (' . $stringBrand . ')' , ['other_brand' => "GROUP_CONCAT(db.brand_id)"]);
                $select->group('s.id');
            }
        }



        if ($params['area_list']) {
            if ($params['area_id']) {
                $select->where('area.id = ?', $params['area_id']);
            } else {
                $select->where('area.id IN (?)', $params['area_list']);
            }
        }

        if(($params['regional_market'])){
            $select->where('r.id = ?', $params['regional_market']);
        }

        if(($params['district'])){
            $select->where('s.district = ?', $params['district']);
        }

        if ($params['staff_id']) {
            $select->joinLeft(['v' => 'v_store_staff_leader_log'], 's.id = v.store_id AND v.released_at IS NULL v.is_leader = 1', []);
            $select->where('v.staff_id = ?', $params['staff_id']);
        }

        if(($params['store_name'])){
            $select->where('s.name LIKE ?', '%'.$params['store_name'].'%');
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])){


            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('di.is_ka = ?', 0);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('di.is_ka = ?', 1);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "2"){
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('di.is_ka = ?', 0);
            }
        }

        if(!empty($params['is_ka_details'])){

            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('IF(di.parent = 0, di.id, di.parent) = ?', $params['is_ka_details']);
            }

        }
   
//        $select->limit(500);

        $result = $db->fetchAll($select);

        for ($i = 0; $i < count($result); ++$i) {
            $stringBrand = $result[$i]['oppo_brand'] . ','. $result[$i]['other_brand'];
            $stringBrand =  trim($stringBrand, ',');
            $result[$i] ['brands'] = explode(',', $stringBrand);
        }

        return $result;
    }




}
