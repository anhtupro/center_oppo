<?php

class Application_Model_CheckShopEntire extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT s.id'),
                'store_name' => 's.name',
                'recheck_last' => 're.created_at',
                'e.error_image'
            ])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['re' => DATABASE_TRADE.'.check_shop_entire'], 's.id = re.store_id AND re.is_last = 1', [])
            ->joinLeft(['e' => DATABASE_TRADE.'.error_image_shop_current'], 'e.store_id = s.id', [])
            ->where('s.del = 0 OR s.del IS NULL');


        if ($params['list_area']) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = (?)', $params['area_id']);
        }

        if ($params['store_id']) {
            $select->where('s.id = (?)', $params['store_id']);
        }

        if ($params['store_name']) {
            $select->where('s.name LIKE (?)', '%' . $params['store_name'] . '%');
        }

        if (!empty($params['regional_market'])){
            $select->where('r.id = ?', $params['regional_market']);
        }

        if (!empty($params['district'])){
            $select->where('s.district = ?', $params['district']);
        }

        if ($params['staff_id']) {
            $select->where('re.created_by = ?', $params['staff_id']);
        }

        if ($params['from_date']) {
            $select->where('re.created_at >= ?', date('y-m-d 00:00:00', strtotime($params['from_date'])));
        }

        if ($params['to_date']) {
            $select->where('re.created_at <= ?', date('y-m-d 23:59:59', strtotime($params['to_date'])));
        }

        if ($params['list_store']) {
            $select->where('s.id IN (?)', explode(',', $params['list_store']));
        }

        $select->order("re.created_at DESC");

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function get($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.check_shop_entire'], [
                         'c.*',
                         'repair_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                         'check_shop_by' => "CONCAT(stc.firstname, ' ', stc.lastname)",
                     ])
            ->where('c.id = ?', $params['check_shop_id'])
        ->joinLeft(['st' => 'staff'], 'c.repair_by = st.id', [])
        ->joinLeft(['stc' => 'staff'], 'c.created_by = stc.id', []);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getStoreErrorInfo($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['e' => DATABASE_TRADE.'.error_image_shop_current'], [
                         'e.error_image',
                         'e.store_id',
                         'check_shop_entire_id' => 'c.id'
                     ])
                    ->joinLeft(['c' => DATABASE_TRADE.'.check_shop_entire'], 'e.store_id = c.store_id AND c.is_last = 1', [])
                    ->where('e.store_id = ?', $store_id);
        
        $result = $db->fetchRow($select);

        return $result;

    }

    public function getDataExportTime($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.check_shop_entire'], [
                          'c.created_at',
                          'created_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                          'store_name' => 's.name',
                         'store_id' => 's.id',
                         'channel'     => "(CASE WHEN (l.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                            WHEN (d.is_ka = 1) THEN 'KA'
                                            WHEN (l.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                            END)",
                         "s.partner_id",
                         "dealer_id"         => "d.id",
                         "area_name"         => "a.name",
                         'province_name' => 'r.name',
                         'district_name' => 'i.name'
                     ]);

        $select->joinLeft(array('s' => 'store'), 's.id = c.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('i' => 'regional_market'), 's.district = i.id', array());
        $select->joinLeft(array('a' => 'area'), 'r.area_id = a.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = l.loyalty_plan_id', array());

        $select->joinLeft(array('st' => 'staff'), 'c.created_by = st.id', array());

        if ($params['list_area']) {
            $select->where('a.id IN (?)', $params['list_area']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($params['staff_id']) {
            $select->where('c.created_by = ?', $params['staff_id']);
        }

        if ($params['from_date']) {
            $select->where('c.created_at >= ?', date('y-m-d 00:00:00', strtotime($params['from_date'])));
        }

        if ($params['to_date']) {
            $select->where('c.created_at <= ?', date('y-m-d 23:59:59', strtotime($params['to_date'])));
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function exportTime($params)
    {
        ini_set("memory_limit", -1);
        ini_set("display_error", 1);
        error_reporting(~E_ALL);

        $data = $this->getDataExportTime($params);

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();
        $heads = array(
            'Stt',
            'Kênh',
            'Store ID',
            'Dealer ID',
            'Partner ID',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Huyện',
            'Nhân viên check shop',
            'Ngày check shop',
            'Giờ check shop'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province_name']);
            $sheet->setCellValue($alpha++.$index, $item['district_name']);
            $sheet->setCellValue($alpha++.$index, $item['created_by']);
            $sheet->setCellValue($alpha++.$index, date('d/m/Y', strtotime($item['created_at'])));
            $sheet->setCellValue($alpha++.$index, date('H:i:s', strtotime($item['created_at'])));

            $index++;

        }



        $filename = 'Report_Checkshop_TMK_Local' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }


    public function getDataExportError($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE.'.check_shop_entire'], [
                'c.created_at',
                'created_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                'store_name' => 's.name',
                'store_id' => 's.id',
                'channel'     => "(CASE WHEN (l.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                            WHEN (d.is_ka = 1) THEN 'KA'
                                            WHEN (l.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                            END)",
                "s.partner_id",
                "dealer_id"         => "d.id",
                "area_name"         => "a.name",
                'province_name' => 'r.name',
                'district_name' => 'i.name',
                'error_name' => 'm.name',
                'error_note' => 'e.note',
                'repair_by' => "CONCAT(z.firstname, ' ', z.lastname)",
                'e.is_repair'
            ]);

        $select->joinLeft(array('s' => 'store'), 's.id = c.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('i' => 'regional_market'), 's.district = i.id', array());
        $select->joinLeft(array('a' => 'area'), 'r.area_id = a.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = l.loyalty_plan_id', array());
        $select->joinLeft(array('e' => DATABASE_TRADE.'.check_shop_entire_error_image'), 'c.id = e.check_shop_id', array());
        $select->joinLeft(array('m' => DATABASE_TRADE.'.error_image'), 'e.error_image_id = m.id', array());
        $select->joinLeft(array('z' => 'staff'), 'e.repair_by = z.id', array());

        $select->joinLeft(array('st' => 'staff'), 'c.created_by = st.id', array());
        $select->order('c.store_id');

        if ($params['list_area']) {
            $select->where('a.id IN (?)', $params['list_area']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($params['staff_id']) {
            $select->where('c.created_by = ?', $params['staff_id']);
        }

        if ($params['from_date']) {
            $select->where('c.created_at >= ?', date('y-m-d 00:00:00', strtotime($params['from_date'])));
        }

        if ($params['to_date']) {
            $select->where('c.created_at <= ?', date('y-m-d 23:59:59', strtotime($params['to_date'])));
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function exportError($params)
    {
        ini_set("memory_limit", -1);
        ini_set("display_error", 1);
        error_reporting(~E_ALL);

        $data = $this->getDataExportError($params);

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();
        $heads = array(
            'Stt',
            'Kênh',
            'Store ID',
            'Dealer ID',
            'Partner ID',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Huyện',
            'Nhân viên check shop',
            'Ngày check shop',
            'Giờ check shop',
            'Lỗi hình ảnh',
            'Ghi chú',
            'Trạng thái',
            'Người sửa'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province_name']);
            $sheet->setCellValue($alpha++.$index, $item['district_name']);
            $sheet->setCellValue($alpha++.$index, $item['created_by']);
            $sheet->setCellValue($alpha++.$index, date('d/m/Y', strtotime($item['created_at'])));
            $sheet->setCellValue($alpha++.$index, date('H:i:s', strtotime($item['created_at'])));
            $sheet->setCellValue($alpha++.$index, $item['error_name']);
            $sheet->setCellValue($alpha++.$index, $item['error_note']);
            $sheet->setCellValue($alpha++.$index, $item['is_repair'] ? 'Đã sửa' : '');
            $sheet->setCellValue($alpha++.$index, $item['repair_by']);

            $index++;

        }



        $filename = 'Report_Checkshop_TMK_Local' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }



    public function getStatisticCheck($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.check_shop_entire'], [
                         'total_store_ka' => "SUM(IF(d.is_ka = 1, 1, 0))",
                         'total_store_indi' => "SUM(IF(d.is_ka = 0 OR d.is_ka IS NULL, 1, 0))",
                         'staff_id' => 'c.created_by',
                         'staff_name' => "CONCAT(f.firstname, ' ', f.lastname)",
                         'f.title'
                     ])
            ->joinLeft(['f' => 'staff'], 'c.created_by = f.id', [])
            ->joinLeft(['s' => 'store'], 'c.store_id = s.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->where('r.area_id IN (?)', $params['area_list'])
            ->where('c.created_at >= ? ', $params['from_date'])
            ->where('c.created_at <= ? ', $params['to_date'])
//            ->where("c.created_at BETWEEN " . $params['from_date'] . " AND " . $params['to_date'])
            ->group("c.created_by");

        if ($params['staff_id']) {
            $select->where('c.created_by = ?', $params['staff_id']);
        }
  
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['staff_id']] = $element;
        }
        return $list;
    }

    public function getStatisticStore($params)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.check_shop_entire'], [
                         'store_id' => new Zend_Db_Expr('DISTINCT c.store_id'),
                         'd.is_ka',
                         'staff_id' => 'c.created_by',
                         'staff_name' => "CONCAT(f.firstname, ' ', f.lastname)",
                         'f.title'
                     ])
            ->joinLeft(['f' => 'staff'], 'c.created_by = f.id', [])
            ->joinLeft(['s' => 'store'], 'c.store_id = s.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->where('r.area_id IN (?)', $params['area_list'])
            ->where('c.created_at >= ? ', $params['from_date'])
            ->where('c.created_at <= ? ', $params['to_date']);

        if ($params['staff_id']) {
            $sub_select->where('c.created_by = ?', $params['staff_id']);
        }

        $select = $db->select()
            ->from(['g' => $sub_select], [
                "total_store_ka" => "SUM( IF ( g.is_ka = 1, 1, 0 ) )",
                'total_store_indi' => "SUM( IF ( g.is_ka = 0 OR g.is_ka IS NULL, 1, 0 ) )",
                'g.staff_name',
                'g.staff_id',
                'g.title',
                'list_store_ka' => 'GROUP_CONCAT(IF ( g.is_ka = 1, g.store_id, NULL ))',
                'list_store_indi' => 'GROUP_CONCAT(IF ( g.is_ka = 0 OR g.is_ka IS NULL, g.store_id, NULL ))'
            ])
        ->group('g.staff_id');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['staff_id']] = $element;
        }
        return $list;
    }

    public function getStatisticError($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.check_shop_entire'], [
                         'total_error' => "COUNT(e.id)",
                         'total_repair' => "SUM(IF(e.is_repair = 1 AND e.repair_type = 1, 1, 0))",
                         'staff_id' => 'c.created_by',
                         'staff_name' => "CONCAT(f.firstname, ' ', f.lastname)",
                         'f.title',
                         'list_store_error' => "GROUP_CONCAT(c.store_id)",
                         'list_store_repair' => "GROUP_CONCAT(IF(e.is_repair = 1 AND e.repair_type = 1, c.store_id, NULL))"
                     ])
            ->joinLeft(['f' => 'staff'], 'c.created_by = f.id', [])
            ->joinLeft(['s' => 'store'], 'c.store_id = s.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->join(['e' => DATABASE_TRADE . '.check_shop_entire_error_image'], 'c.id = e.check_shop_id', [])

            ->where('r.area_id IN (?)', $params['area_list'])
            ->where('c.created_at >= ? ', $params['from_date'])
            ->where('c.created_at <= ? ', $params['to_date'])
            ->group("c.created_by");

        if ($params['staff_id']) {
            $select->where('c.created_by = ?', $params['staff_id']);
        }

        $result = $db->fetchAll($select);
        foreach ($result as $element) {
            $list [$element['staff_id']] = $element;
        }

        return $list;
    }
}