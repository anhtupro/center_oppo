<?php
class Application_Model_AppShipmentDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_shipment_details';
    protected $_schema = DATABASE_TRADE;
    
    function fetchShipmentDetails($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            'shipment_details_id' => 'p.id',
            'imei_out' => 'COUNT(DISTINCT o.imei_sn)',
            'p.shipment_id', 
            's.transporter_id',
            'p.area_id',
            'p.category_id', 
            'p.quantity', 
            'p.contractor_id', 
            'p.price', 
            's.campaign_id',
            'category_name'     => 'c.name', 
            'contractor_name'   => 'con.name'
        );


        $select->from(array('p' => DATABASE_TRADE.'.app_shipment_details'), $col);
        $select->joinLeft(array('s' => DATABASE_TRADE.'.app_shipment'), 's.id = p.shipment_id', array() );
        $select->joinLeft(array('o' => DATABASE_TRADE.'.app_contractor_out'), 'o.app_shipment_details = p.id', array() );
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array() );
        $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = p.contractor_id', array() );

        $select->where('p.shipment_id = ?', $params['shipment_id']);
        $select->group('p.id');

        $result = $db->fetchAll($select);

        return $result;
    }

    function fetchShipmentDetailsArea($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.no', 
            'campaign_name'     => 'c.name',
            'transporter_name'  => 't.name',
            'price'             => 'd.price',
            'quantity'          => 'SUM(d.quantity)',
            'category_name'     => 'cat.name',
            'category_id'       => 'cat.id',
            'area_id'           => 'a.id',
            'area_name'         => 'a.name',
            'contractor_name'   => 'con.short_name',
        );


        $select->from(array('p' => DATABASE_TRADE.'.app_shipment'), $col);
        $select->joinLeft(array('d' => DATABASE_TRADE.'.app_shipment_details'), 'd.shipment_id = p.id', array() );
        $select->joinLeft(array('c' => DATABASE_TRADE.'.campaign_demo'), 'c.id = p.campaign_id', array() );
        $select->joinLeft(array('t' => DATABASE_TRADE.'.app_transporter'), 't.id = p.transporter_id', array() );
        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = d.category_id', array() );
        $select->joinLeft(array('a' => 'area'), 'a.id = d.area_id', array() );
        $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = d.contractor_id', array() );

        if(!empty($params['shipment_id'])){
            $select->where('p.id = ?', $params['shipment_id']);
        }

        if(!empty($params['contractor_id'])){
            $select->where('d.contractor_id = ?', $params['contractor_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('d.area_id IN (?)', $params['area_id']);
        }

        $select->group('d.id');

        $select->order('p.id DESC');


        $result = $db->fetchAll($select);

        return $result;
    }

    function getTranferOut($params)
    {
    	if( empty($params['contractor_out_id']) AND empty($params['contractor_out_id']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'contractor_quantity_id'  => 'p.contractor_quantity_id',
            'area_id'      		  	  => "p.area_id",
            'quantity'      		  => "p.quantity",
            'category_id'   		  => "c.category_id",
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_out'), $arrCols);
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.app_contractor_quantity'), 'c.id = p.contractor_quantity_id',array());
        
        $select->where('p.id IN (?)', $params['contractor_out_id']);

        $result = $db->fetchAll($select);

        return $result;
    }

    function getOrderDetailsTranfer($params)
    {
    	if( empty($params['area']) AND empty($params['area']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'id'  			  => 'p.id',
            'area_id'      	  => "p.area_id",
            'category_id'     => "p.category_id",
            'store_id'   	  => "p.store_id",
            'status'   		  => "p.status",
            'status'   		  => "p.status",
            'area_id'		  => 'IFNULL(c.area_id, r.area_id)'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_order_details'), $arrCols);
        $select->joinLeft(array('s'=> 'store'), 's.id = p.store_id',array());
        $select->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market',array());
        $select->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.store_id',array());	
        
        $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area']);

        $select->where('p.category_id IN (?)', $params['cat']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getContractorOut($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'id'                => 'p.id',
            'campaign_id'       => 'c.campaign_id', 
            'category_id'       => 'c.category_id', 
            'contractor_id'     => 'c.contractor_id',
            'short_name'        => 't.short_name',
            'area_name'         => 'a.name',
            'category_name'     => 'cat.name',
            'quantity'          => 'p.quantity',
            'quantity_out'      => 'COUNT(i.out_date)',
            'quantity_in'       => 'COUNT(i.in_area_id)',
            'transporter_name'  => 'tran.name',
            'transporter_price'  => 'p.price'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_contractor_out'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.app_contractor_quantity'), 'c.id = p.contractor_quantity_id', array());
        $select->joinLeft(array('i' => DATABASE_TRADE.'.app_contractor_in'), 'i.contractor_out_id = p.id', array());
        $select->joinLeft(array('t' => DATABASE_TRADE.'.contructors'), 't.id = c.contractor_id', array());
        $select->joinLeft(array('tran' => DATABASE_TRADE.'.app_transporter'), 'tran.id = p.transporter_id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = c.category_id ', array());

        if(!empty($params['id'])){
            $select->where('p.id = ?', $params['id']);
        }

        $select->group('p.id');

        $result  = $db->fetchAll($select);

        return $result;
    }

    public function getListImeiScan($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'category_id'    => 'p.category_id',
            'quantity'       => 'COUNT(p.imei_sn)',
            'list_imei'      => 'GROUP_CONCAT(" ",p.imei_sn)'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_contractor_in'), $arrCols);
        
        $select->where('p.shipment_id = ?', $params['shipment_id']);

        $select->group('p.category_id');

        $result  = $db->fetchAll($select);

        return $result;
    }

    public function getListImeiScanArea($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'category_id'    => 'p.category_id',
            'quantity'       => 'COUNT(p.imei_sn)',
            'list_imei'      => 'GROUP_CONCAT(p.imei_sn)'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_contractor_in'), $arrCols);
        
        $select->where('p.shipment_id = ?', $params['shipment_id']);

        $select->where('p.in_area_id IN (?)', $params['area_id']);

        $select->group('p.category_id');

        $result  = $db->fetchAll($select);

        return $result;
    }

    public function getListImeiScanShipment($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'category_id'    => 'p.category_id',
            'quantity'       => 'COUNT(p.imei_sn)',
            'list_imei'      => 'GROUP_CONCAT(" ",p.imei_sn)',
            'shipment_id'    => 'p.shipment_id'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_contractor_in'), $arrCols);

        if(!empty($params['contractor_id'])){
            $select->where('p.contractor_id = ?', $params['contractor_id']);
        }

        $select->group('p.shipment_id');

        $result  = $db->fetchAll($select);

        return $result;
    }

}