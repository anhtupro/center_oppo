<?php
class Application_Model_SurveyImageByChannel extends Zend_Db_Table_Abstract
{
	protected $_name = 'survey_image_by_channel';
	function getSurveyImageByChannel($params){
		 $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(''));
            $select->joinLeft(array('survey_image'=> 'survey_image'),'survey_image.id=p.survey_image',array('survey_image.name','survey_image.url'));
        if(!empty($params['channel'])){
            $select->where('p.channel IN (?)',$params['channel']);
        }
        if(!empty($params['survey'])){
            $select->where('p.survey_id  = ?',$params['survey']);
        }
        $select->group('survey_image.id');
        $result = $db->fetchAll($select);
        if($_GET['dev']){
            echo $select->__toString();
        }
         return $result;

	}

}                                                      
