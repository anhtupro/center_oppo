<?php

class Application_Model_KpiMonth extends Zend_Db_Table_Abstract {

    protected $_name   = 'kpi_month';
    protected $_schema = DATABASE_SALARY;

    public function getKpiMonth() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        if (!$result) {

            $month   = date('m', strtotime(date('Y-m') . " -3 month"));
            $year    = date('Y', strtotime(date('Y-m') . " -3 month"));
            $where   = array();
            $where[] = $this->getAdapter()->quoteInto('month >= ?', intval($month));
            $where[] = $this->getAdapter()->quoteInto('year >= ?', intval($year));

            //
            $db = Zend_Registry::get('db');

            $to_date   = date("Y-m-t", strtotime("-1 month"));
            $from_date = date("Y-m-1", strtotime("-1 month"));

            $sql  = 'CALL sp_bi(?,?)';
            $stmt = $db->query($sql, array($from_date, $to_date));
            $stmt->closeCursor();
            //


            $data = $this->fetchAll($where);

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->month][$item->year][$item->type] = array(
                                'sell_out'       => $item->sell_out,
                                'sell_out_value' => $item->sell_out_value
                    );
                }
            }
            $cache->save($result, $this->_name . '_cache', array(), null);
        }
        return $result;
    }

    public function getCampaignMonth($params) {
        $db = Zend_Registry::get('db');

        $area = null;
        if (isset($params['area_list']) && $params['area_list']) {
            $area = "WHERE c.object_id IN (" . $params['area_list'] . ")";
        }

        $sql = "
            SELECT sum(total_ia) as num,`month` FROM(
                SELECT 
                    `p`.`month`, 
                    SUM(
                        CASE
                        WHEN c.object_type = 1 THEN
                            (ct1.quantity * ct2.price)
                        ELSE
                            0
                        END
                    ) AS `total_ia`
                FROM
                    " . DATABASE_TRADE . ".`campaign` AS `p`
                INNER JOIN " . DATABASE_TRADE . ".`campaign_order` AS `c` ON p.id = c.campaign_id
                AND c.object_type = 1
                LEFT JOIN " . DATABASE_TRADE . ".`campaign_order_category` AS `ct1` ON c.id = ct1.campaign_order_id
                INNER JOIN " . DATABASE_TRADE . ".`campaign_category` AS `ct2` ON ct1.category_id = ct2.id
                " . $area . "
                GROUP BY
                    `p`.`id`,
                    `c`.`campaign_id`
                ORDER BY
                    `p`.`id` DESC
            ) AS p
            GROUP BY p.`month`
        ";

        $stmt     = $db->query($sql);
        $campaign = $stmt->fetchAll();
        //

        $result = array();
        if ($campaign) {
            foreach ($campaign as $item) {
                $m_y                                   = explode('/', $item['month']);
                $month                                 = (isset($m_y[0]) && $m_y[0]) ? $m_y[0] : 0;
                $year                                  = (isset($m_y[1]) && $m_y[1]) ? $m_y[1] : 0;
                $result[intval($month)][intval($year)] = $item['num'];
            }
        }

        return $result;
    }

    public function getAirMonth() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_air_cache');

        if (!$result) {

            //
            $db       = Zend_Registry::get('db');
            $sql      = "
                SELECT SUM(total_final_detail) as num, MONTH(shop_from_at) as month, YEAR(shop_from_at) as year
                FROM " . DATABASE_TRADE . ".air
                GROUP BY MONTH(shop_from_at), YEAR(shop_from_at)
            ";
            $stmt     = $db->query($sql);
            $campaign = $stmt->fetchAll();
            //

            $result = array();
            if ($campaign) {
                foreach ($campaign as $item) {
                    $result[$item['month']][$item['year']] = $item['num'];
                }
            }
            $cache->save($result, $this->_name . '_air_cache', array(), null);
        }
        return $result;
    }

    function get_total_invest($params) {
        $db = Zend_Registry::get('db');

        //count investment
        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.investment'
                ), new Zend_Db_Expr('count(p.id) as COUNT'));
        $select->where('p.money >?', 0);
        $select->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        if (isset($params['area_list']) && $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $total_invest = $db->fetchOne($select);

        //count store
        $select2 = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.investment'), new Zend_Db_Expr('count(distinct(p.store_id))'));
        $select2->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array());
        $select2->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());

        if (isset($params['area_list']) && $params['area_list']) {
            $select2->where('r.area_id IN (?)', $params['area_list']);
        }

        $total_store = $db->fetchOne($select2);

        //count money
        $select3 = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.investment'), new Zend_Db_Expr('SUM(p.money) as money'));
        $select3->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array());
        $select3->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());

        if (isset($params['area_list']) && $params['area_list']) {
            $select3->where('r.area_id IN (?)', $params['area_list']);
        }

        $money = $db->fetchOne($select3);

        //count marketing fee
        $select4 = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.marketing'), new Zend_Db_Expr('SUM(p.total_money)'));
        $select4->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array());
        $select4->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());

        if (isset($params['area_list']) && $params['area_list']) {
            $select4->where('r.area_id IN (?)', $params['area_list']);
        }

        $marketing = $db->fetchOne($select4);

        $result = array(
            'total_invest' => $total_invest,
            'total_store'  => $total_store,
            'money'        => $money,
            'marketing'    => $marketing
        );

        return $result;
    }

    public function get_checkshop($store_id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.posm_result'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.picture, p.posm_shop_id, p.posm_date_id, p.posm_category_id')));

        $select->joinLeft(array('s' => DATABASE_TRADE . '.posm_shop'), 's.id = p.posm_shop_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.posm_category'), 'c.id = p.posm_category_id', array('category_checkshop' => 'c.category_id'));

        $select->where('s.shop_id = ?', $store_id);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function get_category_posm() {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.category_posm'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, p.*')));

        $result = $db->fetchAll($select);

        $list = array();
        foreach ($result as $key => $value) {
            $list[$value['id']] = $value['name'];
        }

        return $list;
    }

    public function get_investments_by_store($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.investment'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, p.*')));

        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select->where('s.d_id = ?', $params['distributor_id']);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function get_air_gallery_by_store($store_id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.air'), array(new Zend_Db_Expr('a.*')));
        $select->joinLeft(array('a' => DATABASE_TRADE . '.air_gallery'), 'a.sn = p.sn', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = p.cat_id', array('cat_name' => 'c.name'));

        $select->where('p.store_id = ?', $store_id);
        $select->where('a.type = ?', 3);
        $select->group('a.id');

        $result = $db->fetchAll($select);
        return $result;
    }

    //copy Modal Imei_kpi (v� ko c� quy?n ch?nh s?a file imei_kpi cho v�o d�y)
    public function fetchArea($params) {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $countActivateConditionBefore = $countActivateConditionAfter  = ' (i.`activation_date` IS NOT NULL AND i.`activation_date` <>0)
                                    OR ((i.`activation_date` IS NULL OR i.`activation_date` =0) AND i.`status` > 0)';

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <>0
                    THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status` IS NOT NULL AND i.`status` > 0
                    THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'g.good_id',
                'color_id'    => new Zend_Db_Expr('IFNULL(g.color_id, 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN g.from_date < '%s' THEN '%s' ELSE g.from_date END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN g.to_date IS NULL OR g.to_date > '%s' THEN '%s' ELSE g.to_date END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(g.price) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN g.price ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN g.price ELSE 0 END) * 0.8");
            }
        } elseif (isset($params['group_year_month_good']) and $params['group_year_month_good']) {

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['good_id']     = 'i.good_id';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }
        } else {

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        $select->from(array('i' => 'imei_kpi'), $get);

        if (isset($params['kpi']) && $params['kpi']) {
            $select->join(
                    array('g' => WAREHOUSE_DB . '.good_price_log'), 'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT ' . WAREHOUSE_DB . '.`good_price_log`.color_id
                            FROM ' . WAREHOUSE_DB . '.`good_price_log`
                            WHERE ' . WAREHOUSE_DB . '.`good_price_log`.color_id <> 0
                            AND ' . WAREHOUSE_DB . '.`good_price_log`.good_id = i.good_id
                            AND ' . WAREHOUSE_DB . '.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND (' . WAREHOUSE_DB . '.`good_price_log`.to_date IS NULL OR ' . WAREHOUSE_DB . '.`good_price_log`.to_date = 0 OR ' . WAREHOUSE_DB . '.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )', $join_get
            );
        }

        $select->join(array('a' => 'area'), 'a.id=i.area_id', array('area_id' => 'a.id', 'area_name' => 'a.name', 'region_share' => 'a.region_share'));

        if (isset($params['from']) && $params['from'])
            $select->where("i.timing_date >= '" . $from . " 00:00:00'", null);

        if (isset($params['to']) && $params['to'])
            $select->where("i.timing_date <= '" . $to . " 23:59:59'", null);

        if (isset($params['area_id'])) {
            if (is_array($params['area_id']) and count($params['area_id']) > 0) {
                $select->where('a.id IN (?)', $params['area_id']);
            } elseif ($params['area_id']) {
                $select->where('a.id = ?', $params['area_id']);
            }
        }

        if (!isset($params['get_total_sales']) || !$params['get_total_sales']) {

            if (isset($params['kpi']) && $params['kpi']) {
                $select->group(array('a.id', 'g.good_id', 'g.color_id', 'g.from_date'));
            } elseif (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
                $select->group(array('a.id', 'YEAR(i.timing_date)', 'MONTH(i.timing_date)', 'i.good_id'));
            } else {
                //$select->group( array('a.id','YEAR(i.timing_date)','MONTH(i.timing_date)') );
                $select->group(array('a.id'));
            }

            if (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
                $select->order(array('year DESC', 'month DESC', 'a.id', 'total_quantity DESC'));
            }

            //PC::db($select->__toString());
            return $db->fetchAll($select);
        } else {
            return $db->fetchRow($select);
        }
    }

    public function fetchStore($page, $limit, &$total, $params) {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $main_get = array();

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status`>0 THEN 1 ELSE 0 END)"),
            );
        }

        $get['store_id'] = 'i.store_id';

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'g.good_id',
                'color_id'    => new Zend_Db_Expr('IFNULL(g.color_id, 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN g.from_date < '%s' THEN '%s' ELSE g.from_date END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN g.to_date IS NULL OR g.to_date > '%s' THEN '%s' ELSE g.to_date END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(g.price) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN g.price ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN g.price ELSE 0 END) * 0.8");
            }
        } elseif (isset($params['group_year_month_good']) and $params['group_year_month_good']) {

            $get['year']         = 'YEAR(i.timing_date)';
            $get['month']        = 'MONTH(i.timing_date)';
            $get['good_id']      = 'i.good_id';
            $main_get['year']    = 'year';
            $main_get['month']   = 'month';
            $main_get['good_id'] = 'B.good_id';
            $get['total_value']  = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }
        } else {
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        $select->from(array('i' => 'imei_kpi'), $get);

        if (isset($params['kpi']) && $params['kpi']) {
            $select->join(
                    array('g' => WAREHOUSE_DB . '.good_price_log'), 'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT ' . WAREHOUSE_DB . '.`good_price_log`.color_id
                            FROM ' . WAREHOUSE_DB . '.`good_price_log`
                            WHERE ' . WAREHOUSE_DB . '.`good_price_log`.color_id <> 0
                            AND ' . WAREHOUSE_DB . '.`good_price_log`.good_id = i.good_id
                            AND ' . WAREHOUSE_DB . '.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND (' . WAREHOUSE_DB . '.`good_price_log`.to_date IS NULL OR ' . WAREHOUSE_DB . '.`good_price_log`.to_date = 0 OR ' . WAREHOUSE_DB . '.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )', $join_get
            );
        }

        $select->join(array('s' => 'store'), 's.id=i.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array())
                ->joinLeft(array('dl' => 'dealer_loyalty'), "dl.dealer_id = d.id AND '$from' BETWEEN dl.from_date AND dl.to_date AND '$to' BETWEEN dl.from_date AND dl.to_date", array())
                ->joinLeft(array('l' => 'loyalty_plan'), 'l.id = dl.loyalty_plan_id', array());


        if (isset($params['sale_id']) && $params['sale_id']) {

            if (isset($params['title']) and $params['title'] == PB_SALES_TITLE) {
                $select->where('i.pb_sale_id = ?', intval($params['sale_id']));
            } else {
                $select->where('i.sale_id = ?', intval($params['sale_id']));
            }
        }

        if (isset($params['leader_id']) && $params['leader_id']) {
            $select->where('i.leader_id = ?', intval($params['leader_id']));
        }

        if (isset($params['distributor_id']) && $params['distributor_id']) {
            $select->where('s.d_id = ?', intval($params['distributor_id']));
        }


        /*
          if (isset($params['area_list']) && $params['area_list']) {
          if (is_array($params['area_list']) && count($params['area_list']))
          $select->where('i.area_id IN (?)', $params['area_list']);
          elseif (is_numeric($params['area_list']))
          $select->where('i.area_id = ?', intval($params['area_list']));
          else
          $select->where('1=0', 1);
          }

          if (isset($params['district']) && $params['district']) {
          if (is_array($params['district']) && count($params['district']))
          $select->where('i.district_id IN (?)', $params['district']);
          elseif (is_numeric($params['district']))
          $select->where('i.district_id = ?', intval($params['district']));
          else
          $select->where('1=0', 1);
          }elseif (isset($params['province']) && $params['province']) {
          if (is_array($params['province']) && count($params['province']))
          $select->where('i.province_id IN (?)', $params['province']);
          elseif (is_numeric($params['province']))
          $select->where('i.province_id = ?', intval($params['province']));
          else
          $select->where('1=0', 1);
          }elseif (isset($params['area']) && $params['area']) {
          if (is_array($params['area']) && count($params['area']))
          $select->where('i.area_id IN (?)', $params['area']);
          elseif (is_numeric($params['area']))
          $select->where('i.area_id = ?', intval($params['area']));
          else
          $select->where('1=0', 1);
          }
         */

        if (isset($params['store']) && $params['store']) {
            if (is_array($params['store']) && count($params['store']))
                $select->where('i.store_id IN (?)', $params['store']);
            elseif (is_numeric($params['store']))
                $select->where('i.store_id = ?', intval($params['store']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                $select->where('i.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                $select->where('i.store_id = ?', intval($params['store_list']));
            else
                $select->where('1=0', 1);
        }


        if (isset($params['from']) && $params['from'])
            $select->where("i.timing_date >= '" . $from . " 00:00:00'", null);

        if (isset($params['to']) && $params['to'])
            $select->where("i.timing_date <= ?", $to);

        if (isset($params['name']) && $params['name']) {
            $select->joinRight(array('ss' => 'store'), 'ss.id=i.store_id', array());
            $select->where('ss.name LIKE ?', '%' . $params['name'] . '%');
        }

        if (isset($params['store_level']) and $params['store_level']) {
            if ($params['store_level'] == 4) {
                $select->where('l.id IS NULL OR l.id NOT IN (?)', array(1, 2, 3));
                $select->where('d.is_ka IS NULL OR d.is_ka = 0');
            } else {
                $select->where('l.id = ?', $params['store_level']);
            }
        }

        if (isset($params['channel']) and $params['channel'] >= 0) {
            $select->where('d.is_ka = ?', $params['channel']);
        }

        /*
          if (isset($params['get_total_sales']) && $params['get_total_sales'])
          return $db->fetchRow($select);
         */

        if (isset($params['goods']) && $params['goods']) {
            $select->where('i.good_id IN (?)', $params['goods']);
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $select->group(array('i.store_id', 'g.good_id', 'g.color_id', 'g.from_date'));
        } elseif (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
            $select->group(array('YEAR(timing_date)', 'MONTH(timing_date)', 'i.store_id', 'i.good_id'));
        } else {
            $select->group('i.store_id');
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $main_get = array('B.good_id', 'B.color_id', 'B.from_date', 'B.to_date');
        }

        $main_get = array_merge($main_get, array('B.total_quantity', 'B.total_activated', 'B.total_value_activated', 'B.total_value'));

        $date_level = (isset($to) and $to) ? ("'" . $to . "'") : 'CURDATE()';

        $cols = array('store_id'         => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
            'store_name'       => 's.name',
            'store_district'   => 'v_store.district',
            's.company_address',
            's.d_id',
            'store_short_name' => 'IFNULL(s.short_name,s.name)',
            'store_chanel'     => '(CASE WHEN IFNULL(d.is_ka,0) = 0 THEN "IND" ELSE "KA" END)',
            'store_level'      => 'l.name'
        );

        if (isset($params['get_total_sales']) && $params['get_total_sales']) {
            $cols = array('store_id' => 's.id');
        }

        $main_select = $db->select()
                ->from(array('s' => 'store'), $cols)
                ->join(array('v_store' => 'v_store_area'), 'v_store.store_id = s.id', array())
                ->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array('name_distributor' => 'd.name'))
                ->joinLeft(array('dl' => 'dealer_loyalty'), 'dl.dealer_id = d.id AND ' . $date_level . ' BETWEEN dl.from_date AND dl.to_date', array())
                ->joinLeft(array('l' => 'loyalty_plan'), 'l.id = dl.loyalty_plan_id', array())
                ->joinLeft(array('B' => $select), 'B.store_id=s.id', $main_get);

        if (isset($params['store']) && $params['store']) {
            if (is_array($params['store']) && count($params['store']))
                $main_select->where('s.id IN (?)', $params['store']);
            elseif (is_numeric($params['store']))
                $main_select->where('s.id = ?', intval($params['store']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                $main_select->where('s.id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                $main_select->where('s.id = ?', intval($params['store_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                $main_select
                        ->join(array('dt' => 'v_regional_market'), 'dt.id=v_store.district AND dt.parent = v_store.regional_market', array())
                        ->join(array('pr' => 'v_regional_market'), 'pr.id=dt.parent', array())
                        ->where('pr.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                $main_select
                        ->join(array('dt' => 'v_regional_market'), 'dt.id=v_store.district AND dt.parent = v_store.regional_market', array())
                        ->join(array('pr' => 'v_regional_market'), 'pr.id=dt.parent', array())
                        ->where('pr.area_id = ?', intval($params['area_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                $main_select->where('s.district IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                $main_select->where('s.district = ?', intval($params['district']));
            else
                $main_select->where('1=0', 1);
        } elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                $main_select
                        ->join(array('dt2' => 'v_regional_market'), 'dt2.id=s.district', array())
                        ->where('dt2.parent IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                $main_select
                        ->join(array('dt2' => 'v_regional_market'), 'dt2.id=s.district', array())
                        ->where('dt2.parent = ?', intval($params['province']));
            else
                $main_select->where('1=0', 1);
        } elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                $main_select
                        ->join(array('dt2' => 'v_regional_market'), 'dt2.id=v_store.district AND dt2.parent = v_store.regional_market', array())
                        ->join(array('pr2' => 'v_regional_market'), 'pr2.id=dt2.parent', array())
                        ->where('pr2.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                $main_select
                        ->join(array('dt2' => 'v_regional_market'), 'dt2.id=v_store.district AND dt2.parent = v_store.regional_market', array())
                        ->join(array('pr2' => 'v_regional_market'), 'pr2.id=dt2.parent', array())
                        ->where('pr2.area_id = ?', intval($params['area']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['name']) && $params['name'])
            $main_select->where('s.name LIKE ?', '%' . $params['name'] . '%');

        if (isset($params['distributor_id']) && $params['distributor_id'])
            $main_select->where('s.d_id = ?', intval($params['distributor_id']));

        if (isset($params['sales_from']) and $params['sales_from']) {
            $main_select->where('total_quantity >= ?', $params['sales_from']);
        }

        if (isset($params['sales_to']) and $params['sales_to']) {
            $main_select->where('total_quantity <= ?', $params['sales_to']);
        }

        if (isset($params['store_level']) and $params['store_level']) {
            if ($params['store_level'] == 4) {
                $main_select->where('l.id IS NULL OR l.id NOT IN (?)', array(1, 2, 3));
                $main_select->where('d.is_ka IS NULL OR d.is_ka = 0');
            } else {
                $main_select->where('l.id = ?', $params['store_level']);
            }
        }

        if (isset($params['channel']) and $params['channel'] >= 0) {
            $main_select->where('d.is_ka = ?', $params['channel']);
        }

        if (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
            $main_select->order(array('year DESC', 'month DESC', 's.id', 'total_quantity DESC'));
        } else {
            $main_select->order('total_quantity DESC');
        }

        if (isset($params['export']) && $params['export'])
            return $main_select->__toString();

        if (isset($params['get_total_sales']) && $params['get_total_sales']) {
            $cols_total   = array(
                'total_quantity'        => 'SUM(a.total_quantity)',
                'total_activated'       => 'SUM(a.total_activated)',
                'total_value_activated' => 'SUM(a.total_value_activated)',
                'total_value'           => 'SUM(a.total_value)'
            );
            $select_total = $db->select()
                    ->from(array('a' => $main_select), $cols_total);
            return $db->fetchRow($select_total);
        }

        if ($limit) {
            $main_select->limitPage($page, $limit);
        }

        //PC::db($main_select->__toString());
        $result = $db->fetchAll($main_select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;
    }

    public function fetchDistributorBi($page, $limit, &$total, $params) {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db       = Zend_Registry::get('db');
        $select   = $db->select();
        $main_get = array();

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status`>0 THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'g.good_id',
                'color_id'    => new Zend_Db_Expr('IFNULL(g.color_id, 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN g.from_date < '%s' THEN '%s' ELSE g.from_date END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN g.to_date IS NULL OR g.to_date > '%s' THEN '%s' ELSE g.to_date END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(g.price) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN g.price ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN g.price ELSE 0 END) * 0.8");
            }
        } else {
            $get['total_value']    = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            $get['district_id']    = 'i.district_id';
            $get['province_id']    = 'i.province_id';
            $get['area_id']        = 'i.area_id';
            $get['sell_out_store'] = new Zend_Db_Expr("COUNT(DISTINCT i.store_id)");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        if (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
            $get['year']    = 'YEAR(i.timing_date)';
            $get['month']   = 'MONTH(i.timing_date)';
            $get['good_id'] = 'i.good_id';
            $main_get[]     = 'year';
            $main_get[]     = 'month';
            $main_get[]     = 'good_id';
        }


        $select->from(array('i' => 'imei_kpi'), $get)
                ->joinRight(array('s' => 'store'), 's.id=i.store_id', array('store_id' => 's.id'))
                ->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id=s.d_id', array('dealer_id' => 'd.id'));


        if (isset($params['kpi']) && $params['kpi']) {
            $select->join(
                    array('g' => WAREHOUSE_DB . '.good_price_log'), 'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT ' . WAREHOUSE_DB . '.`good_price_log`.color_id
                            FROM ' . WAREHOUSE_DB . '.`good_price_log`
                            WHERE ' . WAREHOUSE_DB . '.`good_price_log`.color_id <> 0
                            AND ' . WAREHOUSE_DB . '.`good_price_log`.good_id = i.good_id
                            AND ' . WAREHOUSE_DB . '.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND (' . WAREHOUSE_DB . '.`good_price_log`.to_date IS NULL OR ' . WAREHOUSE_DB . '.`good_price_log`.to_date = 0 OR ' . WAREHOUSE_DB . '.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )', $join_get
            );
        }

        if (isset($params['sale_id']) && $params['sale_id'])
            $select->where('i.sale_id = ?', intval($params['sale_id']));

        if (isset($params['leader_id']) && $params['leader_id'])
            $select->where('i.leader_id = ?', intval($params['leader_id']));

        if (isset($params['distributor']) && $params['distributor']) {
            if (is_array($params['distributor']) && count($params['distributor']))
                $select->where('d.id IN (?)', $params['distributor']);
            elseif (is_numeric($params['distributor']))
                $select->where('d.id = ?', intval($params['distributor']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                $select->where('i.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                $select->where('i.store_id = ?', intval($params['store_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['from']) && $params['from'])
            $select->where("i.timing_date >= '" . $from . " 00:00:00'", null);

        if (isset($params['to']) && $params['to'])
            $select->where("i.timing_date <= '" . $to . " 23:59:59'", null);

        if (isset($params['name']) && $params['name']) {
            $select->where('d.title LIKE ?', '%' . $params['name'] . '%');
        }

        /*
          if (isset($params['get_total_sales']) && $params['get_total_sales']){
          return $db->fetchRow($select);
          }
         */

        if (isset($params['kpi']) && $params['kpi']) {
            $select->group(array('d.id', 'g.good_id', 'g.color_id', 'g.from_date'));
        } elseif (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
            $select->group(array('d.id', 'YEAR(i.timing_date)', 'MONTH(i.timing_date)', 'i.good_id'));
        } else {
            $select->group('d.id');
        }


        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        if (isset($params['kpi']) && $params['kpi'])
            $main_get = array('B.good_id', 'B.color_id', 'B.from_date', 'B.to_date');


        $main_get = array_merge($main_get, array(
            'total_quantity'        => 'B.total_quantity',
            'total_activated'       => 'B.total_activated',
            'total_value_activated' => 'B.total_value_activated',
            'total_value'           => 'B.total_value',
            'sell_out_store'        => 'B.sell_out_store',
            'store_level'           => new Zend_Db_Expr('(CASE WHEN e.name IS NOT NULL THEN e.name ELSE "NORMAL" END)')
        ));

        $date_level = (isset($to) and $to) ? ("'" . $to . "'") : 'CURDATE()';
        $cols       = array(
            'store_id'        => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS d.id'),
            'store_name'      => 'd.title',
            'store_district'  => 'd.district',
            'company_address' => 'd.add',
            'd_id'            => 'd.parent'
        );

        if (isset($params['get_total_sales']) && $params['get_total_sales']) {
            $cols = array('store_id' => 'd.id');
        }

        $main_select = $db->select()
                ->distinct()
                ->from(
                        array('d' => WAREHOUSE_DB . '.distributor'), $cols)
                ->joinLeft(array('B' => $select), 'B.dealer_id=d.id', $main_get)
                ->joinLeft(array('c' => 'dealer_loyalty'), 'c.dealer_id = d.id AND ' . $date_level . ' BETWEEN c.from_date AND c.to_date', array())
                ->joinLeft(array('e' => 'loyalty_plan'), 'e.id = c.loyalty_plan_id', array())
                ->join(array('f' => 'v_regional_market'), 'f.id = d.district', array())
                ->join(array('g' => 'v_regional_market'), 'g.id = f.parent', array())
        ;

        if (isset($params['distributor']) && $params['distributor']) {
            if (is_array($params['distributor']) && count($params['distributor']))
                $main_select->where('d.id IN (?)', $params['distributor']);
            elseif (is_numeric($params['distributor']))
                $main_select->where('d.id = ?', intval($params['distributor']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                $main_select->where('B.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                $main_select->where('B.store_id = ?', intval($params['store_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                $main_select->where('g.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                $main_select->where('g.area_id = ?', intval($params['area_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                $main_select->where('f.id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                $main_select->where('f.id = ?', intval($params['district']));
            else
                $main_select->where('1=0', 1);
        } elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                $main_select->where('g.id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                $main_select->where('g.id = ?', intval($params['province']));
            else
                $main_select->where('1=0', 1);
        } elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                $main_select->where('g.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                $main_select->where('g.area_id = ?', intval($params['area']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['name']) && $params['name'])
            $main_select->where('d.title LIKE ?', '%' . $params['name'] . '%');

        if (isset($params['store_level']) && $params['store_level']) {
            $main_select->where('e.id = ?', intval($params['store_level']));
        }

        if (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
            $main_select->group(array('d.id', 'year', 'month', 'good_id'));
        } else {
            $main_select->group('d.id');
        }

        if (isset($params['group_year_month_good']) and $params['group_year_month_good']) {
            $main_select->order(array('year', 'month', 'd.id', 'total_quantity DESC'));
        } else {
            $main_select->order('total_quantity DESC');
        }

        if (isset($params['get_total_sales']) && $params['get_total_sales']) {
            $cols_total   = array(
                'total_quantity'        => 'SUM(a.total_quantity)',
                'total_activated'       => 'SUM(a.total_activated)',
                'total_value_activated' => 'SUM(a.total_value_activated)',
                'total_value'           => 'SUM(a.total_value)'
            );
            $select_total = $db->select()
                    ->from(array('a' => $main_select), $cols_total);
            return $db->fetchRow($select_total);
        }

        if ($limit) {
            $main_select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($main_select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;
    }

    /**
     * * Function l?y sell out 3 th�ng g?n nh?t 
     */
    public function get3month($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => 'imei_kpi'), array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum, MONTH(timing_date) AS `month`,YEAR(timing_date) AS `year`')));
        $select->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)', null);

        if (isset($params['area']) and $params['area'])
            $select->where('area_id  IN (?)', $params['area']);

        if (isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);

        $select->group(array('YEAR(timing_date)', 'MONTH(timing_date)'));
        $select->order(array('YEAR(timing_date) ASC', 'MONTH(timing_date) ASC'));
        $result = $db->fetchAll($select);

        $data = array();
        foreach ($result as $key => $value) {
            $data[$value['month']][$value['year']] = array(
                'sell_out'       => $value['num'],
                'sell_out_value' => $value['sum'],
            );
        }

        return $data;
    }

    public function get3month_ka($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => 'imei_kpi'), array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum, MONTH(timing_date) AS `month`,YEAR(timing_date) AS `year`')));
        $select->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)', null);

        $select->joinInner(array('s' => 'store_bi'), 's.store_id = p.store_id', array('s.d_id'));

        if (isset($params['type_ka']) and $params['type_ka'])
            $select->where('s.d_id IN (?)', $params['type_ka']);

        if (isset($params['area']) and $params['area'])
            $select->where('p.area_id  IN (?)', $params['area']);

        if (isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);

        $select->group(array('s.d_id', 'YEAR(timing_date)', 'MONTH(timing_date)'));
        $select->order(array('YEAR(timing_date) ASC', 'MONTH(timing_date) ASC'));
        $result = $db->fetchAll($select);

        $data = array();
        foreach ($result as $key => $value) {
            $data[$value['d_id']][$value['month']][$value['year']] = array(
                'sell_out'       => $value['num'],
                'sell_out_value' => $value['sum']
            );
        }

        return $data;
    }

    public function get3month_ia($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => 'imei_kpi'), array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum, MONTH(timing_date) AS `month`,YEAR(timing_date) AS `year`')));
        $select->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)', null);

        $nestedSelect = "SELECT store_id, area_id, is_ka, d_id, IF(loyalty_plan_id IS NULL, 4, loyalty_plan_id) AS loyalty_plan_id from store_bi";

        $select->joinInner(array('s' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 's.store_id = p.store_id', array('s.loyalty_plan_id'));

        $select->where('s.is_ka = ?', 0);

        if (isset($params['type_ia']) and $params['type_ia'])
            $select->where('s.loyalty_plan_id IN (?) OR s.loyalty_plan_id IS NULL', $params['type_ia']);

        if (isset($params['area']) and $params['area'])
            $select->where('p.area_id  IN (?)', $params['area']);

        if (isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);

        $select->group(array('s.loyalty_plan_id', 'YEAR(timing_date)', 'MONTH(timing_date)'));
        $select->order(array('YEAR(timing_date) ASC', 'MONTH(timing_date) ASC'));
        $result = $db->fetchAll($select);

        $data = array();
        foreach ($result as $key => $value) {
            $data[intval($value['loyalty_plan_id'])][$value['month']][$value['year']] = array(
                'sell_out'       => $value['num'],
                'sell_out_value' => $value['sum']
            );
        }

        return $data;
    }

    public function get3month_good($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => 'imei_kpi'), array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum, MONTH(timing_date) AS month,YEAR(timing_date) AS year,good_id')));
        $select->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)', null);

        if (isset($params['area']) and $params['area'])
            $select->where('area_id  = ?', $params['area']);

        if (isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);

        if (isset($params['good_id']) and $params['good_id'])
            $select->where('good_id  IN (?)', $params['good_id']);

        $select->group(array('YEAR(timing_date)', 'MONTH(timing_date)', 'good_id'));
        $select->order(array('YEAR(timing_date) ASC', 'MONTH(timing_date) ASC'));
        $result = $db->fetchAll($select);

        $list = array();
        foreach ($result as $row => $value) {
            $list[$value['good_id']][$value['month']][$value['year']] = $value['num'];
        }

        //list good_id 3month
        $select_good = $db->select();
        $select_good->from(array('p' => 'imei_kpi'), array(new Zend_Db_Expr('good_id')));
        $select_good->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)', null);

        if (isset($params['area']) and $params['area'])
            $select_good->where('area_id  = ?', $params['area']);

        if (isset($params['distributor_id']) and $params['distributor_id'])
            $select_good->where('distributor_id  = ?', $params['distributor_id']);

        if (isset($params['store_id']) and $params['store_id'])
            $select_good->where('store_id  = ?', $params['store_id']);

        if (isset($params['good_id']) and $params['good_id'])
            $select_good->where('good_id  IN (?)', $params['good_id']);

        $select_good->group('good_id');
        $result_good = $db->fetchAll($select_good);
        //end 
        $data        = array(
            'data' => $list,
            'good' => $result_good
        );

        return $data;
    }

    /**
      / L?y sell out 3 th�ng g?n nh?t to�n qu?c theo good
     */
    public function get3month_good_all($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => 'kpi_month_good'), array(new Zend_Db_Expr('p.*')));

        $select->joinLeft(array('g' => WAREHOUSE_DB . '.good'), 'p.good_id = g.id', 'g.name');

        if (isset($params['good_id']) and $params['good_id'])
            $select->where('good_id  IN (?)', $params['good_id']);

        if (isset($params['area_id']) and $params['area_id'])
            $select->where('area_id  = ?', $params['area_id']);

        $result = $db->fetchAll($select);

        $data_ = array();
        foreach ($result as $k => $v) {
            $data_[$v['good_id']][$v['month']][$v['year']] = array('sell_out' => $v['sell_out'], 'sell_out_value' => $v['sell_out_value']);
        }

        //
        $select_good = $db->select();
        $select_good->from(array('p' => WAREHOUSE_DB . '.good'), array(new Zend_Db_Expr('p.*')));

        if (isset($params['good_id']) and $params['good_id'])
            $select_good->where('id  IN (?)', $params['good_id']);

        $result_good = $db->fetchAll($select_good);

        $data = array(
            'data' => $data_,
            'good' => $result_good
        );

        return $data;
    }

    public function get_total_good_by_store($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => 'imei_kpi'), array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum')));

        if (isset($params['area']) and $params['area'])
            $select->where('area_id  = ?', $params['area']);

        if (isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);

        $select->group('store_id');
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getStaffOffByTitle($title, $date, $params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => 'staff'), array(new Zend_Db_Expr('p.*')));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $select->where('team = ?', $title);
        $select->where('off_date IS NULL', NULL);
        $select->where('joined_at < ?', $date);
        $result = $db->fetchAll($select);
        return count($result);
    }

    public function getSellInDealer($page, $limit, &$total, $params, $range_date) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        if (isset($params['good_id']) && $params['good_id']) {
            $good_id = "AND i.good_id IN(" . implode(",", $params['good_id']) . ")";
        }

        if (isset($params['parent']) && $params['parent'] && $params['parent'] != 0) {
            $distributor = "LEFT JOIN " . WAREHOUSE_DB . ".distributor AS d ON d.id = i.distributor_id";
            $parent      = "AND d.parent IN (" . $params['parent'] . ")";
        } elseif (isset($params['parent']) && $params['parent'] && $params['parent'] == 0) {
            $distributor = "LEFT JOIN " . WAREHOUSE_DB . ".distributor AS d ON d.id = i.distributor_id";
            $parent      = "AND d.parent NOT IN (2316,2317,2363,7483,9187,2318)";
        }

        $select->from(array('p' => new Zend_Db_Expr("
            (SELECT i.distributor_id AS distributor, DATE_FORMAT(outmysql_time,'%Y-%m-%d') AS date, CONCAT(COUNT(i.imei_sn), '-' ,SUM(CASE WHEN (k.`activation_date` IS NOT NULL AND k.`activation_date` <> 0) 
                                    OR (ifnull(k.`status`,0) = 1)
                                    OR (if(i.activated_date IS NOT NULL, 1, 0) = 1)
                                    THEN 1 ELSE 0 END)) AS num
            FROM " . WAREHOUSE_DB . ".imei AS i
            LEFT JOIN " . WAREHOUSE_DB . ".market AS m ON m.id = i.sales_id
            LEFT JOIN imei_kpi AS k ON k.imei_sn = i.imei_sn 
            " . $distributor . "
            WHERE m.outmysql_time >= '" . $params['from'] . "' AND m.outmysql_time <= '" . $params['to'] . "' 
            " . $good_id . "
            " . $parent . "
            GROUP BY i.distributor_id, DATE_FORMAT(outmysql_time, '%Y-%m-%d')) 
        ")), array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS p.distributor" . $range_date . "")));

        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.distributor', array('d.title', 'store_district' => 'd.district'));

        if (isset($params['name']) && $params['name']) {
            $select->where("d.title like '%" . $params['name'] . "%'", null);
        }

        $select->group('p.distributor');

        if ($limit) {
            $select->limitPage($page, $limit);
        }

        if (isset($params['export']) && $params['export'] == 1) {
            return $select;
        }

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getSellInDealerParent($page, $limit, &$total, $params, $range_date) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        if (isset($params['good_id']) && $params['good_id']) {
            $good_id = "AND i.good_id IN(" . implode(",", $params['good_id']) . ")";
        }

        $select->from(array('p' => new Zend_Db_Expr("
            (SELECT d.parent AS distributor, DATE_FORMAT(outmysql_time,'%Y-%m-%d') AS date, CONCAT(COUNT(i.imei_sn), '-' ,SUM(CASE WHEN (k.`activation_date` IS NOT NULL AND k.`activation_date` <> 0) 
                                    OR (ifnull(k.`status`,0) = 1)
                                    OR (if(i.activated_date IS NOT NULL, 1, 0) = 1)
                                    THEN 1 ELSE 0 END)) AS num
            FROM " . WAREHOUSE_DB . ".imei AS i
            LEFT JOIN " . WAREHOUSE_DB . ".market AS m ON m.id = i.sales_id
            LEFT JOIN imei_kpi AS k ON k.imei_sn = i.imei_sn 
            LEFT JOIN " . WAREHOUSE_DB . ".distributor AS d ON d.id = i.distributor_id
            WHERE m.outmysql_time >= '" . $params['from'] . "' AND m.outmysql_time <= '" . $params['to'] . "' 
            " . $good_id . "
            AND d.parent IN (2316,2317,2363,7483,9187,2318)
            GROUP BY d.parent, DATE_FORMAT(outmysql_time, '%Y-%m-%d')) 
        ")), array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS p.distributor" . $range_date . "")));

        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.distributor', array('d.title', 'store_district' => 'd.district'));

        $select->group('p.distributor');

        $result  = $db->fetchAll($select);
        /**
         * Order
         * */
        $select2 = $db->select();
        $select2->from(array('p' => new Zend_Db_Expr("
            (SELECT d.parent AS distributor, DATE_FORMAT(outmysql_time,'%Y-%m-%d') AS date, CONCAT(COUNT(i.imei_sn), '-' ,SUM(CASE WHEN (k.`activation_date` IS NOT NULL AND k.`activation_date` <> 0) 
                                    OR (ifnull(k.`status`,0) = 1)
                                    OR (if(i.activated_date IS NOT NULL, 1, 0) = 1)
                                    THEN 1 ELSE 0 END)) AS num
            FROM " . WAREHOUSE_DB . ".imei AS i
            LEFT JOIN " . WAREHOUSE_DB . ".market AS m ON m.id = i.sales_id
            LEFT JOIN imei_kpi AS k ON k.imei_sn = i.imei_sn 
            LEFT JOIN " . WAREHOUSE_DB . ".distributor AS d ON d.id = i.distributor_id
            WHERE m.outmysql_time >= '" . $params['from'] . "' AND m.outmysql_time <= '" . $params['to'] . "' 
            " . $good_id . "
            AND d.parent NOT IN (2316,2317,2363,7483,9187,2318)
            GROUP BY DATE_FORMAT(outmysql_time, '%Y-%m-%d')) 
        ")), array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS p.distributor" . $range_date . "")));

        $result_order = $db->fetchRow($select2);

        /**
         * END Order
         * */
        $data = array(
            'ka'    => $result,
            'order' => $result_order,
        );
        return $data;
    }

    public function getSaleLeader($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $date   = date('Y-m-d H:i:s');

        $select->from(array('p' => 'sales_region'), array());
        $select->joinLeft(array('s' => 'sales_region_staff_log'), 'p.id = s.sales_region_id', array());
        $select->joinLeft(array('a' => 'area'), 'p.parent = a.id', array());
        $select->joinLeft(array('st' => 'staff'), 'st.id = s.staff_id', array('fullname' => "CONCAT(st.firstname, ' ', st.lastname)", 'id' => "st.id", "phone" => "st.phone_number", "email" => "st.email"));

        $select->where('p.to_date IS NULL OR p.to_date >= ?', $date);
        $select->where('p.from_date <= ?', $date);
        $select->where('st.title = ?', SALES_LEADER_TITLE);

        if (isset($params['area']) and $params['area']) {
            $select->where('a.id IN (?)', $params['area']);
        }

        return $db->fetchAll($select);
    }

    public function getLeaderDetailsSellout($from, $to) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'province_name'         => "a.name",
            'area_name'             => "area.name",
            'total_quantity'        => "COUNT(DISTINCT i.imei_sn)",
            'total_value_activated' => "SUM(
                                            CASE 
                                            WHEN (i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1 THEN 1
                                            ELSE 0 END) AS `total_activated`, 
                                            SUM(i.`value`) * 0.8 AS `total_value`, 
                                            SUM(CASE WHEN i.sale_id = i.leader_id THEN i.`value` ELSE 0 END) * 0.8 AS `total_value_bonus`, 
                                            SUM(CASE WHEN (i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1 THEN i.`value` 
                                                             ELSE 0 
                                                    END) * 0.8",
            'timing_from_date'      => "MIN(DATE(timing_date))",
            'timing_to_date'        => "MAX(DATE(timing_date))"
        );

        $good_id = null;
        if (isset($params['good_id']) and $params['good_id']) {
            $good_id = "WHERE i.good_id = '" . $params['good_id'] . "'";
        }

        $nestedSelect = "SELECT IFNULL(i.distributor_id,0) distributor_id, activated_at, count(p.imei_sn) active 
                         FROM
                         (
                            SELECT DISTINCT imei_sn, DATE(activated_date) activated_at 
                            FROM `warehouse`.`imei`
                            WHERE (activated_date >= '" . $params['from'] . " 00:00:00') AND (activated_date <= '" . $params['to'] . " 23:59:59')
                         ) AS `p` 
                         INNER JOIN `warehouse`.`imei` AS `i` ON p.imei_sn = i.imei_sn 
                         $good_id
                         GROUP BY IFNULL(i.distributor_id,0), activated_at";

        $select->from(array('i' => 'imei_kpi'), $arrCols);
        $select->joinInner(array('s' => 'sales_region_staff_log'), 's.staff_id=i.leader_id AND (s.from_date <= DATE(i.timing_date)) AND (s.to_date IS NULL OR DATE(i.timing_date) <= s.to_date)', array());
        $select->joinInner(array('a' => 'sales_region'), 'a.id = s.sales_region_id ', array());
        $select->joinLeft(array('area' => 'area'), 'area.id = a.parent', array());

        $select->where("s.staff_id = ?", 491);
        if (isset($from) and $from) {
            $select->where("DATE(i.timing_date) >= ?", $from);
        }

        if (isset($to) and $to) {
            $select->where("DATE(i.timing_date) <= ?", $to);
        }

        $select->group(array("s.id"));
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getListSale($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'fullname'     => "CONCAT(s.firstname,' ',s.lastname)",
            'staff_id'     => "s.id",
            'phone_number' => "s.phone_number",
            'email'        => "s.email",
            'title'        => "s.title",
            'title_name'   => "t.name"
        );


        $select->from(array('p' => 'store_leader_log'), $arrCols);
        $select->joinInner(array('l' => 'store_staff_log'), 'p.store_id = l.store_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id ', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title ', array());

        $select->where("(p.released_at >= UNIX_TIMESTAMP() OR p.released_at IS NULL) AND 
                        (l.released_at >= UNIX_TIMESTAMP() OR l.released_at IS NULL) ", null);

        if (isset($params['leader_id']) and $params['leader_id']) {
            $select->where("p.staff_id = ?", $params['leader_id']);
            $select->where("s.id NOT IN (?)", $params['leader_id']);
        }

        $select->group(array("s.id"));

        $select->order(array("field(s.title, 183, 312, 274, 293, 182) ASC"));

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListPg($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'fullname'     => "CONCAT(s.firstname,' ',s.lastname)",
            'staff_id'     => "s.id",
            'phone_number' => "s.phone_number",
            'email'        => "s.email",
        );


        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('l' => 'store_staff_log'), 'p.store_id = l.store_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id ', array());

        $select->where("s.off_date IS NULL", null);

        $select->where("(p.released_at >= UNIX_TIMESTAMP() OR p.released_at IS NULL) AND 
                        (l.released_at >= UNIX_TIMESTAMP() OR l.released_at IS NULL) ", null);

        if (isset($params['sale_id']) and $params['sale_id']) {
            $select->where("p.staff_id IN (?)", $params['sale_id']);
            $select->where("s.title IN (" . PGPB_TITLE . "," . CHUYEN_VIEN_BAN_HANG_TITLE . "," . PG_BRANDSHOP . "," . SENIOR_PROMOTER_BRANDSHOP . ", " . PRODUCT_CONSULTANT_BRANDSHOP . ")", null);
        }

        if (isset($params['store_leader_id']) and $params['store_leader_id']) {
            $select->where("p.staff_id IN (?)", $params['store_leader_id']);
            $select->where("s.title IN (" . PGPB_TITLE . "," . CHUYEN_VIEN_BAN_HANG_TITLE . "," . PG_BRANDSHOP . "," . SENIOR_PROMOTER_BRANDSHOP . ", " . PRODUCT_CONSULTANT_BRANDSHOP . ")", null);
        }

        if (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
            $select->where("p.staff_id = ?", $params['pb_sale_id']);
            $select->where("s.title IN (" . PGPB_TITLE . "," . CHUYEN_VIEN_BAN_HANG_TITLE . ")", null);
        }

        $select->group(array("l.staff_id"));
        // if(!empty($_GET['dev'])){
        //     echo "<pre>";
        //     print_r($select->__toString());
        //     exit;
        // }
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getPgPgsLeader($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'fullname'      => "CONCAT(s.firstname,' ',s.lastname)",
            'staff_id'      => "s.id",
            'phone_number'  => "s.phone_number",
            'email'         => "s.email",
            'ID_number'     => "s.ID_number",
            'month_in_shop' => "TIMESTAMPDIFF(MONTH, (from_unixtime(l.joined_at)), CURRENT_TIMESTAMP())",
            'date_in_shop'  => "DATEDIFF( CURRENT_TIMESTAMP(), (from_unixtime(l.joined_at)) )",
            'total'         => "COUNT(DISTINCT i.imei_sn)",
            'total_score'   => "a.total"
        );


        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('l' => 'store_staff_log'), 'p.store_id = l.store_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id ', array());
        $select->joinLeft(array('a' => 'sp_assessment_temp'), 'a.staff_id = s.id', array());
        $select->joinLeft(array('i' => 'imei_kpi'), ' i.pg_id = l.staff_id ', array());

        $select->where("s.off_date IS NULL", null);

        $select->where("(p.released_at >= UNIX_TIMESTAMP() OR p.released_at IS NULL) AND 
                        (l.released_at >= UNIX_TIMESTAMP() OR l.released_at IS NULL) ", null);

        if (isset($params['sale_id']) and $params['sale_id']) {
            $select->where("p.staff_id IN (?)", $params['sale_id']);
            $select->where("s.title IN (" . PGPB_TITLE . "," . CHUYEN_VIEN_BAN_HANG_TITLE . "," . PG_BRANDSHOP . "," . SENIOR_PROMOTER_BRANDSHOP . ", " . PRODUCT_CONSULTANT_BRANDSHOP . ")", null);
        } elseif (isset($params['pgs_leader']) and $params['pgs_leader']) {
            $select->where("p.staff_id IN (?)", $params['pgs_leader']);
            $select->where("s.title IN (" . PGPB_TITLE . "," . CHUYEN_VIEN_BAN_HANG_TITLE . "," . PG_BRANDSHOP . "," . SENIOR_PROMOTER_BRANDSHOP . ", " . PRODUCT_CONSULTANT_BRANDSHOP . ")", null);
        } elseif (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
            $select->where("p.staff_id = ?", $params['pb_sale_id']);
            $select->where("s.title IN (" . PGPB_TITLE . "," . CHUYEN_VIEN_BAN_HANG_TITLE . ")", null);
        } else {
            return false;
        }

        if (isset($params['from_date']) and $params['from_date'] AND isset($params['to_date']) and $params['to_date']) {
            $select->where("i.timing_date >= '".$params['from_date']."' AND i.timing_date <= '".$params['to_date']."' OR i.timing_date IS NULL" );
        }


        $select->group(array("l.staff_id"));
        
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getScoreStaff($params) {

        if (empty($params['ID_number'])) {
            return false;
        }

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.lesson_id",
            "p.staff_cmnd",
            "p.scores"
        );


        $select->from(array('p' => 'lesson_scores'), $arrCols);
        $select->joinLeft(array('s' => 'lesson_scores'), '(s.staff_cmnd = p.staff_cmnd AND p.created_at < s.created_at)', array());

        $select->where("s.id IS NULL", null);

        if (!empty($params['ID_number'])) {
            $select->where("p.staff_cmnd IN (?)", $params['ID_number']);
        }

        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['staff_cmnd']] = $value['scores'];
        }

        return $data;
    }

    public function getSaleDetailsSellout($params, $from, $to) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'total_quantity'  => "COUNT(imei_sn)",
            'total_activated' => "SUM(
                                            CASE 
                                            WHEN (p.`activation_date` IS NOT NULL AND p.`activation_date` <> 0) or ifnull(p.status,0) = 1 THEN 1
                                            ELSE 0 END)"
        );


        $select->from(array('p' => 'imei_kpi'), $arrCols);

        if (isset($from) and $from) {
            $select->where("p.timing_date >= ?", $from);
        }

        if (isset($to) and $to) {
            $select->where("p.timing_date <= ?", $to);
        }

        if (isset($params['sale_id']) and $params['sale_id']) {
            $select->where("p.sale_id = ?", $params['sale_id']);
        }

        if (isset($params['pg_id']) and $params['pg_id']) {
            $select->where("p.pg_id = ?", $params['pg_id']);
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSelloutDealer($params) {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load('getSelloutDealer_cache');

        if ($result === false) {

            if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {

                $db     = Zend_Registry::get("db");
                $select = $db->select();

                $arrCols = array(
                    'month_date'  => "p.month_date",
                    'channel'     => '(CASE
                                            WHEN (d.is_ka = 1 AND (d.id = 2318 OR d.parent = 2318)) THEN 10007 /*L?y VIETTEL B? V�O 10007*/
                                            WHEN (d.is_ka = 1 AND d.id IN(2316,2363,2317,9187,10007)) THEN d.id /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                            WHEN (d.is_ka = 1 AND d.parent IN(2316,2363,2317,9187,10007)) THEN d.parent /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                            
                                            WHEN (d.is_ka = 1 AND d.id IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                            WHEN (d.is_ka = 1 AND d.parent IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                            
                                            WHEN (d.is_ka = 1 AND d.id NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                            WHEN (d.is_ka = 1 AND d.parent NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                            
                                            WHEN (d.id IS NULL OR d.id = 0) THEN 888 /*L?y RETURN c�n l?i*/
                                            
                                            WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NULL) THEN 4
                                            WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NOT NULL) THEN l.loyalty_plan_id
                                            END)',
                    'num'         => 'SUM(p.num)',
                    'total_value' => 'SUM(p.total_value*8/10)',
                    'total_value_100' => 'SUM(p.total_value)'
                );


                $nestedSelect = "SELECT p.imei_sn, IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                                    FROM imei_kpi p
                                    -- FORCE INDEX (timing_date)
                                    LEFT JOIN store s ON s.id = p.store_id
                                    WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                                    GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";

                $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);
                $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
                $select->joinLeft(array('l' => DATABASE_CENTER . '.dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM ' . DATABASE_CENTER . '.dealer_loyalty p)', array());

                $select->group(array("channel", "p.month_date"));

                $result = $db->fetchAll($select);
            }

            if (isset($result) and $result) {
                //add catche
                /* get sellout, total value theo t?ng k�nh */
                $data_sellout_dealer = array();
                $data_sellout_total  = array();
                foreach ($result as $key => $value) {
                    $data_sellout_dealer[$value['channel']][$value['month_date']] = array(
                        'num'         => $value['num'],
                        'total_value' => $value['total_value'],
                        'total_value_100' => $value['total_value_100']
                    );


                    if (isset($data_sellout_total[$value['month_date']]) and $data_sellout_total[$value['month_date']]) {
                        $data_sellout_total[$value['month_date']]['num']         += $value['num'];
                        $data_sellout_total[$value['month_date']]['total_value'] += $value['total_value'];
                    } else {
                        $data_sellout_total[$value['month_date']]['num']         = $value['num'];
                        $data_sellout_total[$value['month_date']]['total_value'] = $value['total_value'];
                    }
                }
                /* END sellout, total value theo t?ng k�nh */
                //end

                $data = array(
                    'data_sellout_dealer' => $data_sellout_dealer,
                    'data_sellout_total'  => $data_sellout_total
                );

                $cache->save($data, 'getSelloutDealer_cache', array(), null);
                return $data;
            }
        }
        return $result;
    }
    
    public function getSelloutDealerNew($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'month_date'  => "p.month",
            'year_date'   => "p.year",
            'channel'     => 'channel',
            'num'         => 'sell_out',
            'total_value' => 'sell_out_value'
        );

        $select->from(array('p' => 'v_kpi_month_dealer_now_new'), $arrCols);
        $select->where('p.area_id = ?', 0);
        $select->where('p.year >= ?', date_format(date_create($params['from']), "Y"));
        
        $result = $db->fetchAll($select);

        /* get sellout, total value theo t?ng k�nh */
        $data_sellout_dealer = array();
        $data_sellout_total  = array();
        foreach ($result as $key => $value) {
            $data_sellout_dealer[$value['channel']][$value['month_date']][$value['year_date']] = array(
                'num'         => $value['num'],
                'total_value' => $value['total_value']
            );


            if (isset($data_sellout_total[$value['month_date']][$value['year_date']]) and $data_sellout_total[$value['month_date']][$value['year_date']]) {
                $data_sellout_total[$value['month_date']][$value['year_date']]['num']         += $value['num'];
                $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] += $value['total_value'];
            } else {
                $data_sellout_total[$value['month_date']][$value['year_date']]['num']         = $value['num'];
                $data_sellout_total[$value['month_date']][$value['year_date']]['total_value'] = $value['total_value'];
            }
        }
        /* END sellout, total value theo t?ng k�nh */
        //end
        
        $data = array(
            'data_sellout_dealer' => $data_sellout_dealer,
            'data_sellout_total'  => $data_sellout_total
        );

        return $data;
    }

    public function getSelloutGoodCache($params) {

        $cache  = Zend_Registry::get('cache');
        $result = $cache->load('getSelloutGood_catche');

        if ($result === false) {
            $db     = Zend_Registry::get("db");
            $select = $db->select();

            if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {


                $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                                 FROM imei_kpi p
                                 -- FORCE INDEX (timing_date)
                                 WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 00:00:00'
                                 GROUP BY p.good_id, MONTH(timing_date)";

                if (isset($params['area_list']) and $params['area_list']) {
                    $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                                 FROM imei_kpi p
                                 -- FORCE INDEX (timing_date)
                                 WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 00:00:00' AND p.area_id IN (" . $params['area_list'] . ")
                                 GROUP BY p.good_id, MONTH(timing_date)";
                }


                $result = $db->fetchAll($nestedSelect);

                /* get sellout, total value theo t?ng s?n ph?m */
                $data_sellout_good = array();
                foreach ($result as $key => $value) {
                    $data_sellout_good[$value['good_id']][$value['month_date']] = array(
                        'num'         => $value['num'],
                        'total_value' => $value['total_value']
                    );
                }
                /* END sellout, total value theo t?ng s?n ph?m */

                $cache->save($data_sellout_good, 'getSelloutGood_catche', array(), null);
                return $data_sellout_good;
            }
        }
        return $result;
    }
    
    public function getSelloutGoodCacheNew($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'good_id'     => "good_id",
            'month_date'  => "month",
            'year_date'   => 'year',
            'num'         => 'sell_out',
            'total_value' => 'sell_out_value'
        );

        $select->from(array('p' => 'v_kpi_month_good_now'), $arrCols);
        $select->where('p.type = ?', 0);
        $select->where('p.year >= ?', DATE_FORMAT($params['from'], "%Y"));
        
        $result = $db->fetchAll($select);

        /* get sellout, total value theo t?ng s?n ph?m */
        $data_sellout_good = array();
        foreach ($result as $key => $value) {
            $data_sellout_good[$value['good_id']][$value['month_date']][$value['year_date']] = array(
                'num'         => $value['num'],
                'total_value' => $value['total_value']
            );
        }
        /* END sellout, total value theo t?ng s?n ph?m */

        return $data_sellout_good;
            
        
    }
    
    function get_fee_cost($params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.cost'), array(new Zend_Db_Expr('MONTH(p.created_at) month_date, SUM(p.amount) AS sum'))
        );

        if (isset($params['from']) and $params['from']) {
            $select->where('p.created_at >= ?', "" . $params['from'] . " 00:00:00");
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.created_at <= ?', "" . $params['to'] . " 23:59:59");
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('p.object_id IN (?)', $params['area']);
        }

        $select->group('MONTH(p.created_at)');
        $result = $db->fetchAll($select);
        return $result;
    }

    function get_fee_air($params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.air'), array(new Zend_Db_Expr('MONTH(p.shop_from_at) month_date, p.total_detail, a.id AS area_id, SUM(IFNULL(p.total_final_detail,p.total_detail)) AS sum'))
        );
        $select->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array());
        $select->joinLeft(array('r' => DATABASE_TRADE . '.regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.area'), 'r.area_id = a.id', array());

        if (isset($params['from']) and $params['from']) {
            $select->where('p.shop_from_at >= ?', "" . $params['from'] . " 00:00:00");
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.shop_from_at <= ?', "" . $params['to'] . " 23:59:59");
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('a.id IN (?)', $params['area']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('r.id = ?', $params['province']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select->where('s.d_id = ?', $params['distributor_id']);
        }

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if (isset($params['is_brand_shop']) and $params['is_brand_shop']) {
            $select->where('s.is_brand_shop = ?', $params['is_brand_shop']);
        }

        $select->group('MONTH(p.shop_from_at)');
        $result = $db->fetchAll($select);
        return $result;
    }

    function get_fee_campaign($params) {

        $db = Zend_Registry::get('db');

        $arrCols = array(
            'month_date' => "REPLACE(cam.`month`, CONCAT('/',YEAR(CURDATE())), '')",
            'sum'        => "SUM(o.quantity * c.price)",
            'area_id'    => "p.object_id"
        );

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.campaign_order'), $arrCols);
        $select->joinLeft(array('o' => DATABASE_TRADE . '.campaign_order_category'), 'p.id = o.campaign_order_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.campaign_category'), 'c.id = o.category_id', array());
        $select->joinLeft(array('cam' => DATABASE_TRADE . '.campaign'), 'cam.id = p.campaign_id', array());
        if (isset($params['month']) and $params['month']) {
            $select->where('cam.month IN (?)', $params['month']);
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('p.object_id IN (?)', $params['area']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select->where('s.d_id = ?', $params['distributor_id']);
        }

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        $select->group('cam.month');

        $result = $db->fetchAll($select);
        return $result;
    }

    function get_fee_campaign_distributor($params) {

        $db = Zend_Registry::get('db');

        $arrCols = array(
            'month_date' => "REPLACE(cam.`month`, CONCAT('/',YEAR(CURDATE())), '')",
            'sum'        => "SUM(o.quantity * c.price)",
            'area_id'    => "p.object_id"
        );

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.campaign_order'), $arrCols);
        $select->joinLeft(array('o' => DATABASE_TRADE . '.campaign_order_category'), 'p.id = o.campaign_order_id', array());
        $select->joinLeft(array('os' => DATABASE_TRADE . '.campaign_order_category_store'), 'os.campaign_order_category_id = o.id', array());
        $select->joinLeft(array('s' => 'store'), 's.id = os.store_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.campaign_category'), 'c.id = o.category_id', array());
        $select->joinLeft(array('cam' => DATABASE_TRADE . '.campaign'), 'cam.id = p.campaign_id', array());
        if (isset($params['month']) and $params['month']) {
            $select->where('cam.month IN (?)', $params['month']);
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('p.object_id IN (?)', $params['area_list']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select->where('s.d_id = ?', $params['distributor_id']);
        }

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        $select->group('cam.month');

        $result = $db->fetchAll($select);
        return $result;
    }

    function getSelloutByDealer($params) {
		
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {

            $arrCols = array(
                'month_date'  => "p.month_date",
                'channel'     => 'c.id',
                'channel_group'     => 'c.id',
                'num'         => 'SUM(p.num)',
                'total_value' => 'SUM(p.total_value*8/10)',
                'total_value_100' => 'SUM(p.total_value)'
            );


            $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            FORCE INDEX (timing_date)
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";

            if (isset($params['area_list']) and $params['area_list']) {
                $params['area_list'] = implode(",", $params['area_list']);
                $nestedSelect        = "SELECT  IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.area_id IN (" . $params['area_list'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";
                
            }
            if (isset($params['province']) and $params['province']) {
                $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.province_id IN (" . $params['province'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";
            }
            if (isset($params['district']) and $params['district']) {
                $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.district_id IN (" . $params['district'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";
            } elseif (isset($params['leader_id']) and $params['leader_id']) {
                $nestedSelect = "SELECT 
                                 IFNULL(st.d_id,0) d_id, 
                                 MONTH(timing_date) month_date, 
                                 SUM(p.qty) num, 
                                 SUM(p.`value`*p.qty) total_value 
                                 FROM `kpi_by_model` AS p 
                                 LEFT JOIN store st ON st.id = p.store_id
                                 INNER JOIN `sales_region_staff_log` AS `s` ON (s.staff_id=p.leader_id OR s.staff_id=p.sale_id) AND (s.from_date <= DATE(p.timing_date)) AND (s.to_date IS NULL OR DATE(p.timing_date) <= s.to_date) 
                                 WHERE (s.staff_id = '" . $params['leader_id'] . "') AND (DATE(p.timing_date) >= '" . $params['from'] . " 00:00:00') AND (DATE(p.timing_date) <= '" . $params['to'] . " 23:59:59') 
                                 GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)
                                 ";
            } elseif (isset($params['sale_id']) and $params['sale_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.sale_id = " . $params['sale_id'] . ")
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
                
                
                
                
            } elseif (isset($params['store_leader_id']) and $params['store_leader_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE p.timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.store_leader_id = " . $params['store_leader_id'] . " )
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date)";
            } elseif (isset($params['pg_id']) and $params['pg_id']) {
                $nestedSelect_b = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(p.timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM  
                            (	SELECT p.*
                                    FROM imei_kpi p WHERE p.timing_date >= '" . $params['from'] . " 00:00:00'
                                    AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                                    AND (p.sale_id = " . $params['pg_id'] . " OR p.pb_sale_id = " . $params['pg_id'] . " OR p.pg_id = " . $params['pg_id'] .  " )     
                            )p

                             LEFT JOIN store st ON st.id = p.store_id
						
                         
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date)";
                
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(m.timing_date) month_date, SUM(p.qty) num, SUM(m.`value`*p.qty) total_value
                                FROM kpi_by_staff p
                                LEFT JOIN kpi_by_model m ON m.id = p.kpi_by_model_id
                                LEFT JOIN store st ON st.id = m.store_id 
                                WHERE m.timing_date >= '" . $params['from'] . " 00:00:00' AND m.timing_date <= '" . $params['to'] . " 23:59:59'
                                AND p.pg_id = " . $params['pg_id'] . "
                                GROUP BY IFNULL(st.d_id,0), MONTH(m.timing_date)
                        ";
                
                
                
                
            } elseif (isset($params['pgs_supervisor']) and $params['pgs_supervisor']) {
                
                $list_pgs = implode($params['list_staff'], ',');
                
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(p.timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM  
							(	SELECT p.*
								FROM imei_kpi p WHERE p.timing_date >= '" . $params['from'] . " 00:00:00'
								AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                                                                AND p.pg_id IN (".$list_pgs.")     
							)p
							 
							 LEFT JOIN store st ON st.id = p.store_id
						
                         
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date)";
            } elseif (isset($params['pgs_leader']) and $params['pgs_leader']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                FROM kpi_by_model p 
                LEFT JOIN store st ON st.id = p.store_id
                LEFT JOIN store_staff_log s ON s.store_id = p.store_id
                WHERE s.`staff_id` = " . $params['pgs_leader'] . " AND s.is_leader = 5
                AND p.timing_date >= FROM_UNIXTIME(s.joined_at) AND p.timing_date <= IFNULL(FROM_UNIXTIME(s.released_at) , NOW() )
                AND p.timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)
                ";
            } elseif (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.sale_id = " . $params['pb_sale_id'] . " OR p.pb_sale_id = " . $params['pb_sale_id'] . " OR p.pg_id = " . $params['pb_sale_id'] . ")
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            } elseif (isset($params['distributor_id']) and $params['distributor_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            LEFT JOIN " . WAREHOUSE_DB . ".distributor d ON d.id = st.d_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (d.id = " . $params['distributor_id'] . " OR d.parent = " . $params['distributor_id'] . ")
                            GROUP BY IFNULL(d.id,0), MONTH(timing_date)";
            } elseif (isset($params['store_id']) and $params['store_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.store_id = " . $params['store_id'] . "
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            } elseif (isset($params['is_brand_shop']) and $params['is_brand_shop']) {
                exit;
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`*p.qty) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND st.is_brand_shop = " . $params['is_brand_shop'] . "
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            }
            
            $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);
            $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = p.d_id',array());
            $select->joinLeft(array('k' => 'channel_dealer_ka'), 'k.d_id = IF(d.parent = 0, d.id, d.parent)',array());
            $select->joinLeft(array('c' => 'channel_ka'),'c.id = k.channel_id',array());
            
            if (isset($params['channel']) and $params['channel']) {
                $select->where("c.id IN (?)", $params['channel']);
            }

            $select->group(array("c.id", "p.month_date"));

            $result = $db->fetchAll($select);

            return $result;
        }
    }
    
    function getSelloutByDealerRealme($params) {
		
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {

            $arrCols = array(
                'month_date'  => "p.month_date",
                'channel'     => '(CASE
                                    WHEN (d.is_ka = 1 AND (d.id = 2318 OR d.parent = 2318)) THEN 10007 /*L?y VIETTEL B? V�O 10007*/
                                    WHEN (d.is_ka = 1 AND d.id IN(2316,2363,2317,9187,10007,22670,23389,7483,2320)) THEN d.id /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                    WHEN (d.is_ka = 1 AND d.parent IN(2316,2363,2317,9187,10007,23389, 22670,7483,2320)) THEN d.parent /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                    
                                    WHEN (d.is_ka = 1 AND d.id IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                    WHEN (d.is_ka = 1 AND d.parent IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                    
                                    WHEN (d.is_ka = 1 AND d.id NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                    WHEN (d.is_ka = 1 AND d.parent NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                    
                                    WHEN (d.id IS NULL OR d.id = 0) THEN 888 /*L?y RETURN c�n l?i*/
                                    
                                    WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NULL) THEN 4
                                    WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NOT NULL) THEN l.loyalty_plan_id
                                    END)',
                'channel_group'     => '(CASE
                                    WHEN (d.is_ka = 1 AND (d.id = 2318 OR d.parent = 2318)) THEN 10007 /*L?y VIETTEL B? V�O 10007*/
                                    WHEN (d.is_ka = 1 AND d.id IN(2316,2363,2317,9187,10007,22670,23389,7483,2320)) THEN d.id /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                    WHEN (d.is_ka = 1 AND d.parent IN(2316,2363,2317,9187,10007,23389, 22670,7483,2320)) THEN d.parent /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                    
                                    WHEN (d.is_ka = 1 AND d.id IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                    WHEN (d.is_ka = 1 AND d.parent IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                    
                                    WHEN (d.is_ka = 1 AND d.id NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                    WHEN (d.is_ka = 1 AND d.parent NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                    
                                    WHEN (d.id IS NULL OR d.id = 0) THEN 888 /*L?y RETURN c�n l?i*/
                                    
                                    WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NULL) THEN 4
                                    WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NOT NULL) THEN l.loyalty_plan_id
                                    END)',
                'num'         => 'SUM(p.num)',
                'total_value' => 'SUM(p.total_value*8/10)',
                'total_value_100' => 'SUM(p.total_value)'
            );


            $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            FORCE INDEX (timing_date)
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";

            if (isset($params['area_list']) and $params['area_list']) {
                $params['area_list'] = implode(",", $params['area_list']);
                $nestedSelect        = "SELECT  IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.area_id IN (" . $params['area_list'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";
                
            }
            if (isset($params['province']) and $params['province']) {
                $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.province_id IN (" . $params['province'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";
            }
            if (isset($params['district']) and $params['district']) {
                $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.district_id IN (" . $params['district'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date)";
            } elseif (isset($params['leader_id']) and $params['leader_id']) {
                $nestedSelect = "SELECT 
                                 IFNULL(st.d_id,0) d_id, 
                                 MONTH(timing_date) month_date, 
                                 SUM(p.qty) num, 
                                 SUM(p.`value`) total_value 
                                 FROM `kpi_by_model` AS p 
                                 LEFT JOIN store st ON st.id = p.store_id
                                 INNER JOIN `sales_region_staff_log` AS `s` ON (s.staff_id=p.leader_id OR s.staff_id=p.sale_id) AND (s.from_date <= DATE(p.timing_date)) AND (s.to_date IS NULL OR DATE(p.timing_date) <= s.to_date) 
                                 WHERE (s.staff_id = '" . $params['leader_id'] . "') AND (DATE(p.timing_date) >= '" . $params['from'] . " 00:00:00') AND (DATE(p.timing_date) <= '" . $params['to'] . " 23:59:59') 
                                 GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)
                                 ";
            } elseif (isset($params['sale_id']) and $params['sale_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, COUNT(DISTINCT p.imei_sn) num, SUM(p.`value`) total_value
                            FROM ".HR_DB_RM.".imei_kpi_sale_oppo p
                            LEFT JOIN store st ON st.id = p.oppo_store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.oppo_sale_id = " . $params['sale_id'] . ")
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
                
                
                
                
            } elseif (isset($params['store_leader_id']) and $params['store_leader_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE p.timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.store_leader_id = " . $params['store_leader_id'] . " )
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date)";
            } elseif (isset($params['pg_id']) and $params['pg_id']) {
                $nestedSelect_b = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(p.timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM  
                            (	SELECT p.*
                                    FROM imei_kpi p WHERE p.timing_date >= '" . $params['from'] . " 00:00:00'
                                    AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                                    AND (p.sale_id = " . $params['pg_id'] . " OR p.pb_sale_id = " . $params['pg_id'] . " OR p.pg_id = " . $params['pg_id'] .  " )     
                            )p

                             LEFT JOIN store st ON st.id = p.store_id
						
                         
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date)";
                
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(m.timing_date) month_date, SUM(p.qty) num, SUM(m.`value`) total_value
                                FROM kpi_by_staff p
                                LEFT JOIN kpi_by_model m ON m.id = p.kpi_by_model_id
                                LEFT JOIN store st ON st.id = m.store_id 
                                WHERE m.timing_date >= '" . $params['from'] . " 00:00:00' AND m.timing_date <= '" . $params['to'] . " 23:59:59'
                                AND p.pg_id = " . $params['pg_id'] . "
                                GROUP BY IFNULL(st.d_id,0), MONTH(m.timing_date)
                        ";
                
                
                
                
            } elseif (isset($params['pgs_supervisor']) and $params['pgs_supervisor']) {
                
                $list_pgs = implode($params['list_staff'], ',');
                
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(p.timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM  
							(	SELECT p.*
								FROM imei_kpi p WHERE p.timing_date >= '" . $params['from'] . " 00:00:00'
								AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                                                                AND p.pg_id IN (".$list_pgs.")     
							)p
							 
							 LEFT JOIN store st ON st.id = p.store_id
						
                         
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date)";
            } elseif (isset($params['pgs_leader']) and $params['pgs_leader']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                FROM kpi_by_model p 
                LEFT JOIN store st ON st.id = p.store_id
                LEFT JOIN store_staff_log s ON s.store_id = p.store_id
                WHERE s.`staff_id` = " . $params['pgs_leader'] . " AND s.is_leader = 5
                AND p.timing_date >= FROM_UNIXTIME(s.joined_at) AND p.timing_date <= IFNULL(FROM_UNIXTIME(s.released_at) , NOW() )
                AND p.timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)
                ";
            } elseif (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.sale_id = " . $params['pb_sale_id'] . " OR p.pb_sale_id = " . $params['pb_sale_id'] . " OR p.pg_id = " . $params['pb_sale_id'] . ")
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            } elseif (isset($params['distributor_id']) and $params['distributor_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            LEFT JOIN " . WAREHOUSE_DB . ".distributor d ON d.id = st.d_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (d.id = " . $params['distributor_id'] . " OR d.parent = " . $params['distributor_id'] . ")
                            GROUP BY IFNULL(d.id,0), MONTH(timing_date)";
            } elseif (isset($params['store_id']) and $params['store_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.store_id = " . $params['store_id'] . "
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            } elseif (isset($params['is_brand_shop']) and $params['is_brand_shop']) {
                exit;
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND st.is_brand_shop = " . $params['is_brand_shop'] . "
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            }

            
            $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);
            $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
            $select->joinLeft(array('l' => DATABASE_CENTER . '.dealer_loyalty'), 'l.dealer_id = d.id AND l.from_date = (SELECT MAX(p.from_date) FROM ' . DATABASE_CENTER . '.dealer_loyalty p)', array());

            if (isset($params['channel']) and $params['channel']) {
                $select->where("(CASE
                                WHEN (d.is_ka = 1 AND (d.id = 2318 OR d.parent = 2318)) THEN 10007 /*L?y VIETTEL B? V�O 10007*/
                                WHEN (d.is_ka = 1 AND d.id IN(2316,2363,2317,9187,10007,23389, 22670,7483,2320)) THEN d.id /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                WHEN (d.is_ka = 1 AND d.parent IN(2316,2363,2317,9187,10007,23389,22670,7483,2320)) THEN d.parent /*L?y TGD?,FPT,VTA,Vinpro,VIETTEL*/
                                
                                WHEN (d.is_ka = 1 AND d.id IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                WHEN (d.is_ka = 1 AND d.parent IN(2325,7483,2324,2322,2323)) THEN 2325 /* L?y trong nh�m CES */
                                
                                WHEN (d.is_ka = 1 AND d.id NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                WHEN (d.is_ka = 1 AND d.parent NOT IN(2316,2363,2317,9187,10007)) THEN 999 /*L?y Nh�m KA c�n l?i*/
                                
                                WHEN (d.id IS NULL OR d.id = 0) THEN 888 /*L?y RETURN c�n l?i*/
                                
                                WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NULL) THEN 4
                                WHEN (d.is_ka = 0 AND l.loyalty_plan_id IS NOT NULL) THEN l.loyalty_plan_id
                                END) IN (?)", $params['channel']);
            }

            $select->group(array("channel_group", "p.month_date"));
            

            $result = $db->fetchAll($select);

            return $result;
        }
    }
    
    function getSelloutByDealerNewArea($params) {
		
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {

            $arrCols = array(
                'month_date'  => "p.month_date",
                'year_date'   => "p.year_date",
                'channel'     => 'v.channel_id',
                'channel_group'     => 'v.channel_id',
                'num'         => 'SUM(p.num)',
                'total_value' => 'SUM(p.total_value*8/10)'
            );


            $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            FORCE INDEX (timing_date)
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date), YEAR(timing_date)";

            if (isset($params['area_list']) and $params['area_list']) {
                $params['area_list'] = implode(",", $params['area_list']);
                $nestedSelect        = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.area_id IN (" . $params['area_list'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date), YEAR(timing_date)";
                
            }
            if (isset($params['province']) and $params['province']) {
                $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.province_id IN (" . $params['province'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date), YEAR(timing_date)";
                
            }
            if (isset($params['district']) and $params['district']) {
                $nestedSelect = "SELECT IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.district_id IN (" . $params['district'] . ")
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['leader_id']) and $params['leader_id']) {
                $nestedSelect = "SELECT 
                                 IFNULL(st.d_id,0) d_id, 
                                 MONTH(timing_date) month_date, 
                                 YEAR(timing_date) year_date, 
                                 SUM(p.qty) num, 
                                 SUM(p.`value`) total_value 
                                 FROM `kpi_by_model` AS p 
                                 LEFT JOIN store st ON st.id = p.store_id
                                 INNER JOIN `sales_region_staff_log` AS `s` ON (s.staff_id=p.leader_id OR s.staff_id=p.sale_id) AND (s.from_date <= DATE(p.timing_date)) AND (s.to_date IS NULL OR DATE(p.timing_date) <= s.to_date) 
                                 WHERE (s.staff_id = '" . $params['leader_id'] . "') AND (DATE(p.timing_date) >= '" . $params['from'] . " 00:00:00') AND (DATE(p.timing_date) <= '" . $params['to'] . " 23:59:59') 
                                 GROUP BY IFNULL(st.d_id,0), MONTH(timing_date), YEAR(timing_date)
                                 ";
            } elseif (isset($params['sale_id']) and $params['sale_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.sale_id = " . $params['sale_id'] . " OR p.pb_sale_id = " . $params['sale_id'] . " OR p.pg_id = " . $params['sale_id'] . ")
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['store_leader_id']) and $params['store_leader_id']) {
                $nestedSelect = "SELECT IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, SUM(p.qty) num, SUM(p.`value`) total_value
                            FROM kpi_by_model p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE p.timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.store_leader_id = " . $params['store_leader_id'] . " OR p.store_leader_id = " . $params['store_leader_id'] . " OR p.pg_id = " . $params['store_leader_id'] . ")
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date), YEAR(p.timing_date)";
            } elseif (isset($params['pg_id']) and $params['pg_id']) {
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(p.timing_date) month_date, YEAR(p.timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM  
							(	SELECT p.*
								FROM imei_kpi p WHERE p.timing_date >= '" . $params['from'] . " 00:00:00'
								AND p.timing_date <= '" . $params['to'] . " 23:59:59'
							)p
							 
							 LEFT JOIN store st ON st.id = p.store_id
						
                            WHERE p.timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.sale_id = " . $params['pg_id'] . " OR p.pb_sale_id = " . $params['pg_id'] . " OR p.pg_id = " . $params['pg_id'] .  " )
                            GROUP BY IFNULL(st.d_id,0), MONTH(p.timing_date), YEAR(p.timing_date)";
            } elseif (isset($params['pgs_leader']) and $params['pgs_leader']) {
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                FROM imei_kpi p 
                LEFT JOIN store st ON st.id = p.store_id
                LEFT JOIN store_staff_log s ON s.store_id = p.store_id
                WHERE s.`staff_id` = " . $params['pgs_leader'] . " AND s.is_leader = 5
                AND p.timing_date >= FROM_UNIXTIME(s.joined_at) AND p.timing_date <= IFNULL(FROM_UNIXTIME(s.released_at) , NOW() )
                AND p.timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                GROUP BY IFNULL(st.d_id,0), MONTH(timing_date), YEAR(timing_date)
                ";
            } elseif (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.sale_id = " . $params['pb_sale_id'] . " OR p.pb_sale_id = " . $params['pb_sale_id'] . " OR p.pg_id = " . $params['pb_sale_id'] . ")
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['distributor_id']) and $params['distributor_id']) {
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            LEFT JOIN " . WAREHOUSE_DB . ".distributor d ON d.id = st.d_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (d.id = " . $params['distributor_id'] . " OR d.parent = " . $params['distributor_id'] . ")
                            GROUP BY IFNULL(d.id,0), MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['store_id']) and $params['store_id']) {
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.store_id = " . $params['store_id'] . "
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            } elseif (isset($params['is_brand_shop']) and $params['is_brand_shop']) {
                exit;
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND st.is_brand_shop = " . $params['is_brand_shop'] . "
                            GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)";
            }


            $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);
            $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
            $select->joinLeft(array('v' => DATABASE_CENTER . '.v_channel_dealer'), 'v.d_id = d.id', array());

            if (isset($params['channel']) and $params['channel']) {
                $select->where("v.channel_id IN (?)", $params['channel']);
            }

            $select->group(array("v.channel_id", "p.month_date", "p.year_date"));

            $result = $db->fetchAll($select);

            return $result;
        }
    }

    public function getSelloutGood($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {


            $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             FORCE INDEX (timing_date)
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                             GROUP BY p.good_id, MONTH(timing_date)";

            if (isset($params['area_list']) and $params['area_list']) {
                $params['area_list'] = implode(",", $params['area_list']);
                $nestedSelect        = "SELECT p.good_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59' AND p.area_id IN (" . $params['area_list'] . ")
                             GROUP BY p.good_id, MONTH(timing_date)";
            }
            if (isset($params['province']) and $params['province']) {
                $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59' AND p.province_id IN (" . $params['province'] . ")
                             GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['district']) and $params['district']) {
                $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59' AND p.district_id IN (" . $params['district'] . ")
                             GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['leader_id']) and $params['leader_id']) {
                $nestedSelect = "SELECT 
                                 p.good_id, 
                                 MONTH(timing_date) month_date, 
                                 COUNT(p.imei_sn) num, 
                                 SUM(p.`value`*8/10) total_value 
                                 FROM `imei_kpi` AS p 
                                 INNER JOIN `sales_region_staff_log` AS `s` ON (s.staff_id=p.leader_id OR s.staff_id=p.sale_id) AND (s.from_date <= DATE(p.timing_date)) AND (s.to_date IS NULL OR DATE(p.timing_date) <= s.to_date) 
                                 WHERE (s.staff_id = '" . $params['leader_id'] . "') AND (DATE(p.timing_date) >= '" . $params['from'] . " 00:00:00') AND (DATE(p.timing_date) <= '" . $params['to'] . " 23:59:59') 
                                 GROUP BY p.good_id, MONTH(timing_date)
                                 ";
            } elseif (isset($params['sale_id']) and $params['sale_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.pb_sale_id = " . $params['sale_id'] . " OR p.pg_id = " . $params['sale_id'] . " OR p.sale_id = " . $params['sale_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['store_leader_id']) and $params['store_leader_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.store_leader_id = " . $params['store_leader_id'] . " OR p.pg_id = " . $params['store_leader_id'] . " OR p.store_leader_id = " . $params['store_leader_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['pg_id']) and $params['pg_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.pb_sale_id = " . $params['pg_id'] . " OR p.pg_id = " . $params['pg_id'] . " OR p.sale_id = " . $params['pg_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['pgs_supervisor']) and $params['pgs_supervisor']) {
                
                $list_pgs = implode($params['list_staff'], ',');
                
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.pg_id IN (".$list_pgs.")
                            GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['pgs_leader']) and $params['pgs_leader']) {
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                FROM imei_kpi p 
                LEFT JOIN store st ON st.id = p.store_id
                LEFT JOIN store_staff_log s ON s.store_id = p.store_id
                WHERE s.`staff_id` = " . $params['pgs_leader'] . " AND s.is_leader = 5
                AND p.timing_date >= FROM_UNIXTIME(s.joined_at) AND p.timing_date <= IFNULL(FROM_UNIXTIME(s.released_at) , NOW() )
                AND timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                GROUP BY IFNULL(st.d_id,0), MONTH(timing_date)
                ";
            } elseif (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.pb_sale_id = " . $params['pb_sale_id'] . " OR p.pg_id = " . $params['pb_sale_id'] . " OR p.sale_id = " . $params['pb_sale_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['distributor_id']) and $params['distributor_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND st.d_id = " . $params['distributor_id'] . "
                            GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['store_id']) and $params['store_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.store_id = " . $params['store_id'] . "
                            GROUP BY p.good_id, MONTH(timing_date)";
            } elseif (isset($params['is_brand_shop']) and $params['is_brand_shop']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND st.is_brand_shop = " . $params['is_brand_shop'] . "
                            GROUP BY p.good_id, MONTH(timing_date)";
            }



            $result = $db->fetchAll($nestedSelect);

            /* get sellout, total value theo t?ng s?n ph?m */
            $data_sellout_good = array();
            foreach ($result as $key => $value) {
                $data_sellout_good[$value['good_id']][$value['month_date']] = array(
                    'num'         => $value['num'],
                    'total_value' => $value['total_value']
                );
            }
            /* END sellout, total value theo t?ng s?n ph?m */
            return $data_sellout_good;
        }
        return $result;
    }
    
    public function getSelloutGoodAreaNew($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {


            $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             FORCE INDEX (timing_date)
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                             GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";

            if (isset($params['area_list']) and $params['area_list']) {
                $params['area_list'] = implode(",", $params['area_list']);
                $nestedSelect        = "SELECT p.good_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59' AND p.area_id IN (" . $params['area_list'] . ")
                             GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            }
            if (isset($params['province']) and $params['province']) {
                $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59' AND p.province_id IN (" . $params['province'] . ")
                             GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['district']) and $params['district']) {
                $nestedSelect = "SELECT p.good_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                             FROM imei_kpi p
                             WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59' AND p.district_id IN (" . $params['district'] . ")
                             GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['leader_id']) and $params['leader_id']) {
                $nestedSelect = "SELECT 
                                 p.good_id, 
                                 MONTH(timing_date) month_date, 
                                 YEAR(timing_date) year_date, 
                                 COUNT(p.imei_sn) num, 
                                 SUM(p.`value`*8/10) total_value 
                                 FROM `imei_kpi` AS p 
                                 INNER JOIN `sales_region_staff_log` AS `s` ON (s.staff_id=p.leader_id OR s.staff_id=p.sale_id) AND (s.from_date <= DATE(p.timing_date)) AND (s.to_date IS NULL OR DATE(p.timing_date) <= s.to_date) 
                                 WHERE (s.staff_id = '" . $params['leader_id'] . "') AND (DATE(p.timing_date) >= '" . $params['from'] . " 00:00:00') AND (DATE(p.timing_date) <= '" . $params['to'] . " 23:59:59') 
                                 GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)
                                 ";
            } elseif (isset($params['sale_id']) and $params['sale_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.pb_sale_id = " . $params['sale_id'] . " OR p.pg_id = " . $params['sale_id'] . " OR p.sale_id = " . $params['sale_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['store_leader_id']) and $params['store_leader_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.store_leader_id = " . $params['store_leader_id'] . " OR p.pg_id = " . $params['store_leader_id'] . " OR p.store_leader_id = " . $params['store_leader_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['pg_id']) and $params['pg_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.pb_sale_id = " . $params['pg_id'] . " OR p.pg_id = " . $params['pg_id'] . " OR p.sale_id = " . $params['pg_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['pgs_leader']) and $params['pgs_leader']) {
                $nestedSelect = "SELECT p.imei_sn, IFNULL(st.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value
                FROM imei_kpi p 
                LEFT JOIN store st ON st.id = p.store_id
                LEFT JOIN store_staff_log s ON s.store_id = p.store_id
                WHERE s.`staff_id` = " . $params['pgs_leader'] . " AND s.is_leader = 5
                AND p.timing_date >= FROM_UNIXTIME(s.joined_at) AND p.timing_date <= IFNULL(FROM_UNIXTIME(s.released_at) , NOW() )
                AND timing_date >= '" . $params['from'] . " 00:00:00' AND p.timing_date <= '" . $params['to'] . " 23:59:59'
                GROUP BY IFNULL(st.d_id,0), MONTH(timing_date), YEAR(timing_date)
                ";
            } elseif (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND (p.pb_sale_id = " . $params['pb_sale_id'] . " OR p.pg_id = " . $params['pb_sale_id'] . " OR p.sale_id = " . $params['pb_sale_id'] . ")
                            GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['distributor_id']) and $params['distributor_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND st.d_id = " . $params['distributor_id'] . "
                            GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['store_id']) and $params['store_id']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND p.store_id = " . $params['store_id'] . "
                            GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            } elseif (isset($params['is_brand_shop']) and $params['is_brand_shop']) {
                $nestedSelect = "SELECT p.good_id, IFNULL(p.distributor_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`*8/10) total_value
                            FROM imei_kpi p
                            LEFT JOIN store st ON st.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            AND st.is_brand_shop = " . $params['is_brand_shop'] . "
                            GROUP BY p.good_id, MONTH(timing_date), YEAR(timing_date)";
            }



            $result = $db->fetchAll($nestedSelect);

            /* get sellout, total value theo t?ng s?n ph?m */
            $data_sellout_good = array();
            foreach ($result as $key => $value) {
                $data_sellout_good[$value['good_id']][$value['month_date']][$value['year_date']] = array(
                    'num'         => $value['num'],
                    'total_value' => $value['total_value']
                );
            }
            /* END sellout, total value theo t?ng s?n ph?m */
            return $data_sellout_good;
        }
        return $result;
    }

    public function reportDataLesson($params) {
        
        $list_sale_psg = implode(',',unserialize(LIST_PGS_BI));
        
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        
        //Sub select
        $select_lesson = 'SELECT l.id
        FROM lesson l
        LEFT JOIN lesson_cat lc ON lc.id = l.cat_id 
        WHERE l.del = 0 
        ORDER BY l.id DESC
        LIMIT 0,10';
        $get_list_lesson = $db->fetchAll($select_lesson);
        $list_lesson = [];
        foreach($get_list_lesson as $key=>$value){
            $list_lesson[] = $value['id'];
        }

        //END sub select
        
        
        $arrCols = array(
            'title'     => 'l.title',
            'lesson_id' => 'lesson_id',
            'num'       => 'COUNT(DISTINCT p.staff_cmnd)',
            'pass'      => 'SUM(p.pass)',
            'fail'      => 'SUM(p.fail)',
        );

        $area = null;
        if (isset($params['area_list']) and $params['area_list'] AND count($params['area_list']) < 30) {
            $params['area_list'] = implode(",", $params['area_list']);
            $area                = 'AND r.area_id IN (' . $params['area_list'] . ')';
        }
        
        $list_staff = null;
        if (isset($params['list_staff']) and $params['list_staff'] ) {
            $params['list_staff'] = implode(",", $params['list_staff']);
            $list_staff = 'AND s.id IN (' . $params['list_staff'] . ')';
        }
        
        $list_lesson_limit = null;
        if (isset($list_lesson) and $list_lesson ) {
            $list_lesson = implode(",", $list_lesson);
            $list_lesson_limit = 'AND p.lesson_id IN (' . $list_lesson . ')';
        }

        if (isset($params['province']) and $params['province']) {
            $area = 'AND r.id IN (' . $params['province'] . ')';
        }
        
        $nestedSelect = "SELECT p.staff_cmnd, p.lesson_id, 
        (CASE WHEN (max(p.scores) = 10) THEN 1
        ELSE 0
        END) pass,
        (CASE WHEN (max(p.scores) = 10) THEN 0
        ELSE 1
        END) fail,
        s.title
        FROM lesson_scores p
        LEFT JOIN staff s ON p.staff_cmnd = s.ID_number
        LEFT JOIN regional_market r ON r.id = s.regional_market
        WHERE s.title IN (".$list_sale_psg.") " . $area . " ".$list_staff." ".$list_lesson_limit."
        AND s.status = 1 AND s.off_date IS NULL
        GROUP BY staff_cmnd, lesson_id
        ORDER BY staff_cmnd";

        $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);
        $select->joinLeft(array('l' => 'lesson'), 'l.id = p.lesson_id', array('total_staff' => 'l.total_staff'));
        $select->joinLeft(array('lc' => 'lesson_cat'), 'lc.id = l.cat_id', array());
        
        if(isset($params['list_title_staff']) and $params['list_title_staff']){
            //$select->where('p.title IN (?)', $params['list_title_staff']);
        }
        if(isset($params['cat_id']) and $params['cat_id']){
            //$select->where('lc.parent_id = ?', $params['cat_id']);
        }
        $select->where('l.del = 0');
        
        $select->group(array("lesson_id"));
        $select->order("l.created_at DESC");
       
        if (!empty($params['limit'])){
            $select->limitPage(0, $params['limit']);
        }
        else{
            $select->limitPage(0, 10);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }

    public function reportDataLessoArea($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'area_id' => 'p.area_id',
            'title'   => 'l.title',
            'total'   => 'COUNT(staff_cmnd)',
            'pass'    => 'SUM(pass)',
            'fail'    => 'SUM(fail)',
        );

        /*
          $nestedSelect = "SELECT r.area_id, p.staff_cmnd, p.lesson_id,
          (CASE WHEN (max(p.scores) = 10) THEN 1
          ELSE 0
          END) pass,
          (CASE WHEN (max(p.scores) = 10) THEN 0
          ELSE 1
          END) fail
          FROM lesson_scores p
          RIGHT JOIN staff s ON p.staff_cmnd = s.ID_number AND s.title IN (183,312,182,293,274) AND (s.off_date IS NULL) AND s.status = 1
          LEFT JOIN regional_market r ON r.id = s.regional_market
          WHERE lesson_id = ".$params['lesson_id']."
          GROUP BY staff_cmnd, lesson_id";
         */


        $nestedSelect = "SELECT 
                        r.area_id, s.ID_number staff_cmnd, p.lesson_id, 
                        (CASE WHEN (max(p.scores) = 10) THEN 1
                        ELSE 0
                        END) pass,
                        (CASE WHEN (max(p.scores) = 10) THEN 0
                        ELSE 1
                        END) fail
                        FROM staff s
                        LEFT JOIN lesson_scores p ON p.staff_cmnd = s.ID_number AND lesson_id = " . $params['lesson_id'] . "
                        LEFT JOIN regional_market r ON r.id = s.regional_market
                        WHERE s.title IN (182,183,274,293,312,419,417,403) AND (s.off_date IS NULL) AND s.status = 1
                        GROUP BY s.id";


        $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);
        $select->joinLeft(array('l' => 'lesson'), 'l.id = p.lesson_id', array());
        // $select->where('s.off_date is null');
        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('area_id IN (?)', $params['area_list']);
        }

        $select->group('area_id');
        $select->order('area_id DESC');
        $result = $db->fetchAll($select);
        // echo $select; exit;
        return $result;
    }

    public function totalPgsSales($params) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'staff'), array('num' => 'count(p.id)'));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array('area_id' => 'r.area_id'));
        $select->where('p.title IN (?)', array(183, 312, 182, 293, 274, 419, 417, 403));
        $select->where('p.off_date IS NULL', null);
        $select->where('p.status = 1', null);
        $select->group('r.area_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    /* TRADE  MARKETING */

    function get_fee_cost_month($params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.cost'), array(new Zend_Db_Expr('p.object_id AS area_id, SUM(amount) AS sum'))
        );

        if (isset($params['from_date']) and $params['from_date']) {
            $select->where('created_at >= ?', "" . $params['from_date'] . " 00:00:00");
        }

        if (isset($params['to_date']) and $params['to_date']) {
            $select->where('created_at <= ?', "" . $params['to_date'] . " 23:59:59");
        }

        $select->group('object_id');
        $result = $db->fetchAll($select);

        return $result;
    }

    function get_fee_air_month($params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.air'), array(new Zend_Db_Expr('a.id, p.total_detail, a.id AS area_id, SUM(IFNULL(p.total_final_detail,p.total_detail)) AS sum'))
        );
        $select->joinLeft(array('s' => DATABASE_TRADE . '.store'), 'p.store_id = s.id', array());
        $select->joinLeft(array('r' => DATABASE_TRADE . '.regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.area'), 'r.area_id = a.id', array());

        if (isset($params['from_date']) and $params['from_date']) {
            $select->where('p.shop_from_at >= ?', "" . $params['from_date'] . " 00:00:00");
        }

        if (isset($params['to_date']) and $params['to_date']) {
            $select->where('p.shop_from_at <= ?', "" . $params['to_date'] . " 23:59:59");
        }

        $select->group('a.id');

        $result = $db->fetchAll($select);

        return $result;
    }

    function get_fee_campaign_month($params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.campaign_order'), array(new Zend_Db_Expr('p.campaign_id, p.object_id AS area_id, o.category_id, SUM(o.quantity*c.price) AS sum'))
        );
        $select->joinLeft(array('o' => DATABASE_TRADE . '.campaign_order_category'), 'p.id = o.campaign_order_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.campaign_category'), 'c.id = o.category_id', array());
        $select->joinLeft(array('cam' => DATABASE_TRADE . '.campaign'), 'cam.id = p.campaign_id', array());
        if (isset($params['month']) and $params['month']) {
            $select->where('cam.month = ?', $params['month']);
        }
        $select->group('p.object_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    /* END TRADE MARKETING */

    function get_fee_cost_details($page, $limit, &$total, &$total_price, $params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.cost'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, p.contract_name, p.date_contract, c.name AS contructor, t.name AS type, p.invoice_number, p.`desc`, SUM(p.amount) amount'))
        );

        $select->joinLeft(array('c' => DATABASE_TRADE . '.contructors'), 'c.id = p.contructor', array());
        $select->joinLeft(array('t' => DATABASE_TRADE . '.cost_type'), 't.id = p.type', array());

        if (isset($params['from']) and $params['from']) {
            $select->where('p.created_at >= ?', "" . $params['from'] . " 00:00:00");
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.created_at <= ?', "" . $params['to'] . " 23:59:59");
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('p.object_id IN (?)', $params['area']);
        }

        $data_total_price = $db->fetchRow($select);
        $total_price      = $data_total_price['amount'];

        $select->group('p.id');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_fee_decor_details($page, $limit, &$total, &$total_price, $params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.air'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.sn, s.`name`, p.shop_from_at, a.`name` area_name, SUM(IFNULL(p.total_final_detail, p.total_detail)) num'))
        );

        $select->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array());
        $select->joinLeft(array('r' => DATABASE_TRADE . '.regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.area'), 'a.id = r.area_id', array());

        if (isset($params['from']) and $params['from']) {
            $select->where('p.shop_from_at >= ?', "" . $params['from'] . " 00:00:00");
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.shop_from_at <= ?', "" . $params['to'] . " 23:59:59");
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('a.id IN (?)', $params['area']);
        }

        $data_total_price = $db->fetchRow($select);
        $total_price      = $data_total_price['num'];

        $select->group('p.sn');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function getListAir($params) {

        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.air'), array(new Zend_Db_Expr(' p.*')));
        $select->joinLeft(array('s' => DATABASE_TRADE . '.store'), 'p.store_id = s.id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = p.cat_id', array('category_name' => 'c.name'));
        $select->joinLeft(array('d' => DATABASE_TRADE . '.contructors'), 'd.id = p.contructors_id', array('contructors_name' => 'd.name'));

        if (isset($params['sn']) and $params['sn']) {
            $select->where('p.sn = ?', $params['sn']);
        }

        $select->group('p.id');

        $result = $db->fetchAll($select);

        return $result;
    }

    function get_fee_campaign_area($page, $limit, &$total, &$total_price, $params) {

        $db = Zend_Registry::get('db');

        $arrCols = array(
            'id'         => "cam.id",
            'name'       => "cam.name",
            'from'       => "cam.from",
            'to'         => "cam.to",
            'month_date' => "cam.month",
            'sum'        => "SUM(o.quantity * c.price)",
            'area_id'    => "p.object_id"
        );

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.campaign_order'), $arrCols);
        $select->joinLeft(array('o' => DATABASE_TRADE . '.campaign_order_category'), 'p.id = o.campaign_order_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.campaign_category'), 'c.id = o.category_id', array());
        $select->joinLeft(array('cam' => DATABASE_TRADE . '.campaign'), 'cam.id = p.campaign_id', array());

        if (isset($params['month']) and $params['month']) {
            $select->where('cam.month IN (?)', $params['month']);
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('p.object_id IN (?)', $params['area']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select->where('s.d_id = ?', $params['distributor_id']);
        }

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        /* Total Price */
        $data        = $db->fetchRow($select);
        $total_price = $data['sum'];
        /* END Total Price */

        $select->group('cam.id');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function get_fee_campaign_area2($params) {

        $db = Zend_Registry::get('db');

        $arrCols = array(
            'name'     => "cat.name",
            'quantity' => "o.quantity",
            'price'    => "c.price",
            'sum'      => "(o.quantity * c.price)"
        );

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.campaign_order'), $arrCols);
        $select->joinLeft(array('o' => DATABASE_TRADE . '.campaign_order_category'), 'p.id = o.campaign_order_id', array());
        $select->joinLeft(array('os' => DATABASE_TRADE . '.campaign_order_category_store'), 'os.campaign_order_category_id = o.id', array());
        $select->joinLeft(array('s' => 'store'), 's.id = os.store_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.campaign_category'), 'c.id = o.category_id', array());
        $select->joinLeft(array('cam' => DATABASE_TRADE . '.campaign'), 'cam.id = p.campaign_id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE . '.category'), 'cat.id = c.category_id', array());

        if (isset($params['month']) and $params['month']) {
            $select->where('cam.month IN (?)', $params['month']);
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('p.object_id = ?', $params['area']);
        }

        if (isset($params['campaign']) and $params['campaign']) {
            $select->where('p.campaign_id = ?', $params['campaign']);
        }

        $select->group('cat.id');
        $result = $db->fetchAll($select);

        return $result;
    }

    function get_fee_campaign_area_noarea($params) {

        $db = Zend_Registry::get('db');

        $arrCols = array(
            'id'       => "cat.id",
            'name'     => "cat.name",
            'quantity' => "o.quantity",
            'price'    => "c.price",
            'sum'      => "(o.quantity * c.price)"
        );

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.campaign_order'), $arrCols);
        $select->joinLeft(array('o' => DATABASE_TRADE . '.campaign_order_category'), 'p.id = o.campaign_order_id', array());
        $select->joinLeft(array('os' => DATABASE_TRADE . '.campaign_order_category_store'), 'os.campaign_order_category_id = o.id', array());
        $select->joinLeft(array('s' => 'store'), 's.id = os.store_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.campaign_category'), 'c.id = o.category_id', array());
        $select->joinLeft(array('cam' => DATABASE_TRADE . '.campaign'), 'cam.id = p.campaign_id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE . '.category'), 'cat.id = c.category_id', array());

        if (isset($params['month']) and $params['month']) {
            $select->where('cam.month IN (?)', $params['month']);
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('p.object_id = ?', $params['area']);
        }

        if (isset($params['campaign']) and $params['campaign']) {
            $select->where('p.campaign_id = ?', $params['campaign']);
        }

        $select->group(array('cat.id', 'p.object_id'));

        /* select main */
        $select_main  = $db->select();
        $arrCols_main = array(
            'name'     => "name",
            'quantity' => 'sum(quantity)',
            'price'    => "price",
            'sum'      => "(sum(quantity) * price)",
        );
        $select_main->from(array('p' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->group('id');

        $result = $db->fetchAll($select_main);

        return $result;
    }

    function get_total_trade_store($params) {

        $db = Zend_Registry::get('db');

        /* select investment */
        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.investment'), array(new Zend_Db_Expr("st.id, st.`name` cat_name1, p.`name` cat_name2, c.`name`, p.money, p.quantity, p.invested_at, p.created_at, s.email email_create, p.updated_at, s2.email email_update,
                    (CASE WHEN p.`status` = 1 THEN 'Enabled'
                    WHEN p.`status` = 0 THEN 'Disabled' END
                    ) `status`, MONTH(p.invested_at) month, YEAR(p.invested_at) year, 1")));
        $select->joinLeft(array('st' => 'store'), 'p.store_id = st.id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'p.cate_id = c.id', array());
        $select->joinLeft(array('s' => DATABASE_TRADE . '.staff'), 's.id = p.created_by', array());
        $select->joinLeft(array('s2' => DATABASE_TRADE . '.staff'), 's2.id = p.updated_by', array());

        $select->where('p.`status` = ?', 1);

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select->where('st.d_id = ?', $params['distributor_id']);
        }

        if (isset($params['list_store']) and $params['list_store']) {
            $select->where('p.store_id IN (?)', $params['list_store']);
        }

        /* select air */
        $select2 = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.air'), array(new Zend_Db_Expr("st.id, st.`name`, c.`name` cat_name1, c.`name` cat_name2, IFNULL(p.total_final_detail,p.total_detail) total, p.num, p.shop_from_at, p.created_at, s.email, null, null, null, MONTH(p.shop_from_at) month, YEAR(p.shop_from_at) year, 2")));
        $select2->joinLeft(array('st' => 'store'), 'p.store_id = st.id', array());
        $select2->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'p.cat_id = c.id', array());
        $select2->joinLeft(array('s' => DATABASE_TRADE . '.staff'), 's.id = p.created_by', array());

        $select2->where('p.status = ?', 28);

        if (isset($params['store_id']) and $params['store_id']) {
            $select2->where('p.store_id = ?', $params['store_id']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select2->where('st.d_id = ?', $params['distributor_id']);
        }

        if (isset($params['list_store']) and $params['list_store']) {
            $select2->where('p.store_id IN (?)', $params['list_store']);
        }

        /* select campaign */
        $select3 = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.campaign_order'), array(new Zend_Db_Expr("cam.id, s.`name`, cat.`name` cat_name1, cat.`name` cat_name2, (os.`value` * c.price) sum, os.`value`,cam.`to`, null, null, null, null, null, MONTH(cam.`to`) month, YEAR(cam.`to`) year, 3")));
        $select3->joinLeft(array('o' => DATABASE_TRADE . '.campaign_order_category'), 'p.id = o.campaign_order_id', array());
        $select3->joinLeft(array('os' => DATABASE_TRADE . '.campaign_order_category_store'), 'os.campaign_order_category_id = o.id', array());
        $select3->joinLeft(array('s' => 'store'), 's.id = os.store_id', array());
        $select3->joinLeft(array('c' => DATABASE_TRADE . '.campaign_category'), 'c.id = o.category_id', array());
        $select3->joinLeft(array('cam' => DATABASE_TRADE . '.campaign'), 'cam.id = p.campaign_id', array());
        $select3->joinLeft(array('cat' => DATABASE_TRADE . '.category'), 'cat.id = c.category_id', array());

        if (isset($params['store_id']) and $params['store_id']) {
            $select3->where('os.store_id = ?', $params['store_id']);
        }

        if (isset($params['distributor_id']) and $params['distributor_id']) {
            $select3->where('s.d_id = ?', $params['distributor_id']);
        }

        if (isset($params['list_store']) and $params['list_store']) {
            $select3->where('os.store_id IN (?)', $params['list_store']);
        }

        $select3->where('cat.id IS NOT NULL', null);

        /* end */

        $nestedSelect = $db->select()
                ->union(array($select, $select2, $select3));

        $nestedSelect->order('invested_at DESC');

        $result = $db->fetchAll($nestedSelect);
        return $result;
    }

    //REPORT NH�N S?
    public function staff_evolution($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'year'  => "a.YearNo",
            'month' => 'a.MonthNo',
            'team'  => "b.team",
            'name'  => "c.name",
            'name'  => "c.name",
            'total' => "Count(b.id)"
        );

        $sub_select = "(
                        select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                          select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) a, /*10 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) b, /*100 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) c, /*1000 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) d, /*10000 day range*/
                          (select @YearNo := " . $params['year'] . ",  @minDate :=  CONCAT(@YearNo, '-01-01'), @maxDate := CASE WHEN CONCAT(@YearNo, '-12-31') <= DATE(NOW()) THEN CONCAT(@YearNo, '-12-31') ELSE DATE(NOW()) END) e
                        ) f
                        where aDate between @minDate and @maxDate
                        GROUP BY YEAR(aDate), MONTH(aDate)
                            )";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);
        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate', array());
        $select->joinInner(array('c' => 'team'), 'b.team = c.id', array());
        $select->group(array('a.YearNo', 'a.MonthNo'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function headcount($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'team'       => "team",
            'name'       => 'name',
            'QuarterNo1' => "MAX(CASE WHEN QuarterNo = 1 THEN totalEmp END)",
            'QuarterNo2' => "MAX(CASE WHEN QuarterNo = 2 THEN totalEmp END)",
            'QuarterNo3' => "MAX(CASE WHEN QuarterNo = 3 THEN totalEmp END)",
            'QuarterNo4' => "MAX(CASE WHEN QuarterNo = 4 THEN totalEmp END)"
        );

        $sub_select = "(
                        SELECT 
                            a.YearNo, a.QuarterNo, b.team, c.name, Count(b.id) totalEmp 
                        FROM
                        (
                        select YEAR(aDate) YearNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                          select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) a, /*10 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) b, /*100 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) c, /*1000 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) d, /*10000 day range*/
                          (select @YearNo := " . $params['year'] . ",  @minDate :=  CONCAT(@YearNo, '-01-01'), @maxDate := CASE WHEN CONCAT(@YearNo, '-12-31') <= DATE(NOW()) THEN CONCAT(@YearNo, '-12-31') ELSE DATE(NOW()) END) e
                        ) f
                        where aDate between @minDate and @maxDate
                        GROUP BY YEAR(aDate), CEIL(MONTH(aDate) / 3)
                            ) a
                        INNER JOIN staff b
                         on b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate
                        INNER JOIN team c on b.team = c.id
                        group by a.YearNo, a.QuarterNo, b.team, c.name
                        )";

        $select->from(array('p' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        if (isset($params['group']) and $params['group']) {
            $select->where('team IN (?)', $params['group']);
        }

        $select->group(array('name'));

        $select->order(array('name'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function turnover_rate($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'YearNo'    => "a.YearNo",
            'MonthNo'   => 'a.MonthNo',
            'totalEmp'  => "SUM(CASE WHEN b.joined_at <= DATE_ADD(a.FromDate, INTERVAL -1 DAY) and ifnull(b.off_date, DATE(NOW())) >= DATE_ADD(a.FromDate, INTERVAL -1 DAY) THEN 1 ELSE 0 END)",
            'totalOff'  => "SUM(CASE WHEN  b.off_date IS NOT NULL and b.off_date BETWEEN a.FromDate AND a.ToDate THEN 1 ELSE 0 END)",
            'TotalRate' => "(SUM(CASE WHEN  b.off_date IS NOT NULL and b.off_date BETWEEN a.FromDate AND a.ToDate THEN 1 ELSE 0 END) / SUM(CASE WHEN b.joined_at <= DATE_ADD(a.FromDate, INTERVAL -1 DAY) and ifnull(b.off_date, DATE(NOW())) >= DATE_ADD(a.FromDate, INTERVAL -1 DAY) THEN 1 ELSE 0 END))*100",
        );

        $sub_select = "(
                        select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select @YearNo := " . $params['year'] . ",  @minDate :=  CONCAT(@YearNo, '-01-01'), @maxDate := CASE WHEN CONCAT(@YearNo, '-12-31') <= 

                    DATE(NOW()) THEN CONCAT(@YearNo, '-12-31') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                            )";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);
        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.FromDate
and b.title in (182, 293)', array());
        $select->group(array('a.YearNo', 'a.MonthNo'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function turnover_pass_probation($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'YearNo'             => "YearNo",
            'QuarterNo'          => "QuarterNo",
            'MonthNo'            => 'QuarterNo',
            'team'               => 'team',
            'name'               => 'name',
            'totalRate'          => "CASE WHEN SUM(totalOff) = 0 then 0 else ROUND(SUM(totalOff) / ROUND(AVG(totalEmp)), 4) end",
            'totalPassProbation' => "case when SUM(totalContractOff) = 0 then 0 else ROUND(SUM(totalContractOff) / ROUND(AVG(totalContractEmp)), 4) end",
        );

        $sub_select = "(
                        Select a.YearNo, a.MonthNo, CEIL(a.MonthNo /3) QuarterNo, b.team, c.name,
                        SUM(CASE WHEN b.joined_at <= DATE_ADD(a.FromDate, INTERVAL -1 DAY) and ifnull(b.off_date, DATE(NOW())) >= DATE_ADD(a.FromDate, INTERVAL -1 DAY) THEN 1 ELSE 0 END) totalEmp, 
                        SUM(CASE WHEN  b.off_date IS NOT NULL and b.off_date BETWEEN a.FromDate AND a.ToDate THEN 1 ELSE 0 END) totalOff,
                        SUM(CASE WHEN ifnull(d.level,0) >= 2 AND b.joined_at <= DATE_ADD(a.FromDate, INTERVAL -1 DAY) and ifnull(b.off_date, DATE(NOW())) >= DATE_ADD(a.FromDate, INTERVAL -1 DAY) THEN 1 ELSE 0 END) totalContractEmp,
                        SUM(CASE WHEN ifnull(d.level,0) >= 2 AND b.off_date IS NOT NULL and b.off_date BETWEEN a.FromDate AND a.ToDate THEN 1 ELSE 0 END) totalContractOff
                        FROM
                        (
                        select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                          select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) a, /*10 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) b, /*100 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) c, /*1000 day range*/
                          (select 0 as a union all select 1 union all select 2 union all select 3
                           union all select 4 union all select 5 union all select 6 union all
                           select 7 union all select 8 union all select 9) d, /*10000 day range*/
                          (select @YearNo := " . $params['year'] . ",  @minDate :=  CONCAT(@YearNo, '-01-01'), @maxDate := CASE WHEN CONCAT(@YearNo, '-12-31') <= 

                        DATE(NOW()) THEN CONCAT(@YearNo, '-12-31') ELSE DATE(NOW()) END) e
                        ) f
                        where aDate between @minDate and @maxDate
                        GROUP BY YEAR(aDate), MONTH(aDate)
                            ) a
                        INNER JOIN staff b
                         on b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.FromDate
                        inner join team c on b.team = c.id
                        left join contract_term d on b.contract_term = d.id
                        group by a.YearNo, a.MonthNo, b.team, c.name
                            )";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        if (isset($params['group_turnover_rate']) and $params['group_turnover_rate']) {
            $select->where('team IN (?)', $params['group_turnover_rate']);
        }

        if (isset($params['cur_quarter']) and $params['cur_quarter']) {
            $select->where('QuarterNo = ?', $params['cur_quarter']);
        }

        $select->group(array('YearNo', 'QuarterNo', 'team', 'name'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function headcount_probation($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'name'           => "b.name",
            'Probation'      => "SUM(case when ifnull(c.level,0) <= 1 then 1 else 0 end)",
            'Pass_Probation' => 'SUM(case when ifnull(c.level,0) > 1 then 1 else 0 end)',
            'Total'          => 'count(a.id)'
        );

        $select->from(array('a' => 'staff'), $arrCols);
        $select->joinLeft(array('b' => 'team'), 'a.team = b.id', array());
        $select->joinLeft(array('c' => 'contract_term'), 'a.contract_term = c.id', array());

        if (isset($params['group_headcount_probation']) and $params['group_headcount_probation']) {
            $select->where('b.id IN (?)', $params['group_headcount_probation']);
        }

        if (isset($params['date_probation']) and $params['date_probation']) {
            $select->where('joined_at <= ?', $params['date_probation']);
            $select->where('off_date is null or off_date >= ?', $params['date_probation']);
        }

        $select->group(array('b.name'));

        $result = $db->fetchAll($select);

        return $result;
    }

    public function headcount_pgpb_sale($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'YearNo'      => "a.YearNo",
            'MonthNo'     => "a.MonthNo",
            'title'       => "case when b.title in (182, 293, 419) then 'PG' ELSE 'SALE' END",
            'totalEmp'    => 'SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Working'     => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end)',
            'off'         => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end)',
            'Per_Working' => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Per_Off'     => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)'
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinLeft(array('b' => 'staff'), 'b.joined_at <= a.ToDate and title in (182,183,293,312,419)', array());
        
        if(isset($params['list_title_staff']) AND $params['list_title_staff']){
            $select->where('b.title IN (?)', $params['list_title_staff']);
        }
        
        $select->group(array("a.YearNo", "a.MonthNo", "case when b.title in (182, 293, 419) then 'PG' ELSE 'SALE' END"));


        $result = $db->fetchAll($select);
        return $result;
    }

    public function headcount_pgpb_sale_area($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'YearNo'      => "a.YearNo",
            'MonthNo'     => "a.MonthNo",
            'name'        => "d.name",
            'area_id'     => "d.id",
            'title'       => "case when b.title in (182, 293) then 'PG' ELSE 'SALE' END",
            'totalEmp'    => 'SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Working'     => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end)',
            'off'         => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end)',
            'Per_Working' => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Per_Off'     => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)'
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and title in (182,183,293,312)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());

        if (isset($params['area']) and $params['area']) {
            $select->where('d.id IN (?)', $params['area']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('c.id IN (?)', $params['province']);
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('d.id IN (?)', $params['area_list']);
        }

        $select->group(array("a.YearNo", "a.MonthNo", "d.name", "case when b.title in (182, 293) then 'PG' ELSE 'SALE' END"));

        $result = $db->fetchAll($select);

        return $result;
    }

    public function headcount_pgpb_sale_area_index($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'YearNo'      => "a.YearNo",
            'MonthNo'     => "a.MonthNo",
            'name'        => "d.name",
            'area_id'     => "d.id",
            'title'       => "case when b.title in (182,293,419) then 'PG' ELSE 'SALE' END",
            'totalEmp'    => 'SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Working'     => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end)',
            'off'         => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end)',
            'Per_Working' => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Per_Off'     => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)'
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and title in (182,293,419)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());

        if (isset($params['list_title_staff']) and $params['list_title_staff']) {
            $select->where('b.title IN (?)', $params['list_title_staff']);
        }

        if (isset($params['area']) and $params['area']) {
            $select->where('d.id IN (?)', $params['area']);
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('d.id IN (?)', $params['area_list']);
        }

        $select->group(array("a.YearNo", "a.MonthNo", "case when b.title in (182,293,419) then 'PG' ELSE 'SALE' END"));

        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }

    public function getStaffBiArea($params) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'staff'), array('num' => 'count(p.id)', 'p.title', 'staff_id' => 'p.id', 'area_id' => 'r.area_id')
        );
        $select->joinLeft(array('t' => 'team'), 'p.title = t.id', array('name' => 't.name'));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.title IN (?)', array(PB_SALES_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, SALES_TITLE, PGPB_TITLE));
        $select->where('off_date IS NULL', null);
        $select->group('r.area_id');

        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }

    public function getDistributorById($id) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => WAREHOUSE_DB . '.distributor'), array('p.*')
        );
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.region', array('area_id' => 'r.area_id'));
        $select->where('p.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStoreById($id) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'store'), array('p.*')
        );
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array('area_id' => 'r.area_id'));
        $select->where('p.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStaffById($id) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'staff'), array('p.*')
        );
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array('area_id' => 'r.area_id'));
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array('area_name' => 'a.name'));
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array('title_name' => 't.name'));
        $select->where('p.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getGoodByDate($params) {

        //$list_good = unserialize(LIST_PRODUCT_HERO_BI);

        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'kpi_by_model'), array('good_id' => 'p.good_id', 'date' => 'DATE(p.timing_date)', 'num' => 'SUM(p.qty)')
        );

        if (isset($params['brand_shop_id']) and $params['brand_shop_id']) {
            $select->where('p.store_id IN (?)', $params['brand_shop_id']);
        }

        if (isset($params['from']) and $params['from']) {
            $select->where('p.timing_date >= ?', "" . $params['from'] . " 00:00:00");
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.timing_date <= ?', "" . $params['to'] . " 23:59:59");
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('p.area_id = ?', $params['area_id']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('p.province_id IN (?)', $params['province']);
        }

        if (isset($params['district']) and $params['district']) {
            $select->where('p.district_id IN (?)', $params['district']);
        }

        if (isset($params['leader_id']) and $params['leader_id']) {
            $select->where('p.leader_id = ?', $params['leader_id']);
        }

        if (isset($params['list_store']) and $params['list_store']) {
            $select->where('p.store_id IN (?)', $params['list_store']);
        }

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
            $select->where('p.pb_sale_id = ?', $params['pb_sale_id']);
        }

        if (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
            $select->where('p.pb_sale_id = ?', $params['pb_sale_id']);
        }

        if (isset($params['sale_id']) and $params['sale_id']) {
            $select->where('p.sale_id = ?', $params['sale_id']);
        }

        if (isset($params['pg_id']) and $params['pg_id']) {
            $select->where('p.pg_id = ?', $params['pg_id']);
        }

        if (isset($params['list_pg_id']) and $params['list_pg_id']) {
            $select->where('p.pg_id IN (' . $params['list_pg_id'] . ')', NULL);
        }

        //$select->where("p.good_id IN (?)", $list_good);
        $select->group(array('DATE(p.timing_date)', 'p.good_id'));

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGoodByDatePgs($params) {

        //$list_good = unserialize(LIST_PRODUCT_HERO_BI);

        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'kpi_by_model'), array('good_id' => 'p.good_id', 'date' => 'DATE(p.timing_date)', 'num' => 'SUM(s.qty)')
        );
        $select->joinLeft(array('s' => 'kpi_by_staff'), 's.kpi_by_model_id = p.id', array(''));
        

        if (isset($params['brand_shop_id']) and $params['brand_shop_id']) {
            $select->where('p.store_id IN (?)', $params['brand_shop_id']);
        }

        if (isset($params['from']) and $params['from']) {
            $select->where('p.timing_date >= ?', "" . $params['from'] . " 00:00:00");
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.timing_date <= ?', "" . $params['to'] . " 23:59:59");
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('p.area_id = ?', $params['area_id']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('p.province_id IN (?)', $params['province']);
        }

        if (isset($params['district']) and $params['district']) {
            $select->where('p.district_id IN (?)', $params['district']);
        }

        if (isset($params['leader_id']) and $params['leader_id']) {
            $select->where('p.leader_id = ?', $params['leader_id']);
        }

        if (isset($params['list_store']) and $params['list_store']) {
            $select->where('p.store_id IN (?)', $params['list_store']);
        }

        if (isset($params['store_id']) and $params['store_id']) {
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
            $select->where('p.pb_sale_id = ?', $params['pb_sale_id']);
        }

        if (isset($params['pb_sale_id']) and $params['pb_sale_id']) {
            $select->where('p.pb_sale_id = ?', $params['pb_sale_id']);
        }

        if (isset($params['sale_id']) and $params['sale_id']) {
            $select->where('p.sale_id = ?', $params['sale_id']);
        }

        if (isset($params['pg_id']) and $params['pg_id']) {
            $select->where('s.pg_id = ?', $params['pg_id']);
        }

        if (isset($params['list_pg_id']) and $params['list_pg_id']) {
            $select->where('s.pg_id IN (' . $params['list_pg_id'] . ')', NULL);
        }

        //$select->where("p.good_id IN (?)", $list_good);
        $select->group(array('DATE(p.timing_date)', 'p.good_id'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function kpi_avg_bi_cache($params) {

        $cache  = Zend_Registry::get('cache');
        $result = $cache->load('kpi_avg_bi_cache');

        if ($result === false) {
            $db     = Zend_Registry::get('db');
            $select = $db->select();

            $arrCols = array(
                'month_date'  => "MONTH(p.timing_date)",
                'year_date'   => "YEAR(p.timing_date)",
                'sum_pg'      => 'SUM(IF((p.pg_id != 0), 1 ,0))',
                'sum_sale'    => 'SUM(IF((p.sale_id != 0), 1 ,0))',
                'sum_pb_sale' => 'SUM(IF((p.pb_sale_id != 0), 1 ,0))'
            );

            $select->from(array('p' => 'imei_kpi'), $arrCols);

            if (isset($params['from']) and $params['from']) {
                $select->where('p.timing_date >= ?', $params['from']);
            }

            if (isset($params['to']) and $params['to']) {
                $select->where('p.timing_date <= ?', $params['to']);
            }

            $select->group(array('MONTH(p.timing_date)', 'YEAR(p.timing_date)'));

            $resu = $db->fetchAll($select);
            $data = array();
            /* .. */
            foreach ($resu as $key => $value) {
                $data[$value['month_date']][$value['year_date']] = array(
                    'sum_pg'      => $value['sum_pg'],
                    'sum_sale'    => $value['sum_sale'],
                    'sum_pb_sale' => $value['sum_pb_sale']
                );
            }
            $cache->save($data, 'kpi_avg_bi_cache', array(), null);
            /* end.. */
            return $data;
        }

        return $result;
    }

    public function kpi_avg_bi_area_cache($params) {

        $cache  = Zend_Registry::get('cache');
        $result = $cache->load('kpi_avg_bi_area_cache');

        if ($result === false) {
            $db     = Zend_Registry::get('db');
            $select = $db->select();

            $arrCols = array(
                'area_id'     => 'p.area_id',
                'month_date'  => "MONTH(p.timing_date)",
                'year_date'   => "YEAR(p.timing_date)",
                'sum_pg'      => 'SUM(IF((p.pg_id != 0), 1 ,0))',
                'sum_sale'    => 'SUM(IF((p.sale_id != 0), 1 ,0))',
                'sum_pb_sale' => 'SUM(IF((p.pb_sale_id != 0), 1 ,0))'
            );

            $select->from(array('p' => 'imei_kpi'), $arrCols);

            if (isset($params['from']) and $params['from']) {
                $select->where('p.timing_date >= ?', $params['from']);
            }

            if (isset($params['to']) and $params['to']) {
                $select->where('p.timing_date <= ?', $params['to']);
            }

            $select->group(array('MONTH(p.timing_date)', 'YEAR(p.timing_date)', 'p.area_id'));

            $resu = $db->fetchAll($select);
            $data = array();
            /* .. */
            foreach ($resu as $key => $value) {
                $data[$value['month_date']][$value['year_date']][$value['area_id']] = array(
                    'sum_pg'      => $value['sum_pg'],
                    'sum_sale'    => $value['sum_sale'],
                    'sum_pb_sale' => $value['sum_pb_sale']
                );
            }
            $cache->save($data, 'kpi_avg_bi_area_cache', array(), null);
            /* end.. */
            return $data;
        }

        return $result;
    }

    public function kpi_avg_bi($params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'month_date'  => "MONTH(p.timing_date)",
            'year_date'   => "YEAR(p.timing_date)",
            'sum_pg'      => 'SUM(IF((p.pg_id != 0), 1 ,0))',
            'sum_sale'    => 'SUM(IF((p.sale_id != 0), 1 ,0))',
            'sum_pb_sale' => 'SUM(IF((p.pb_sale_id != 0), 1 ,0))'
        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);

        if (isset($params['from']) and $params['from']) {
            $select->where('p.timing_date >= ?', $params['from']);
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.timing_date <= ?', $params['to']);
        }

        if (isset($params['area_list']) and $params['area_list'] AND count($params['area_list']) < 30) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        $select->group(array('MONTH(p.timing_date)', 'YEAR(p.timing_date)'));

        $resu = $db->fetchAll($select);

        $data = array();
        /* .. */
        foreach ($resu as $key => $value) {
            $data[$value['month_date']][$value['year_date']] = array(
                'sum_pg'      => $value['sum_pg'],
                'sum_sale'    => $value['sum_sale'],
                'sum_pb_sale' => $value['sum_pb_sale']
            );
        }
        /* end.. */

        return $data;
    }

    public function kpi_avg_bi_area($params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'area_id'     => 'p.area_id',
            'month_date'  => "MONTH(p.timing_date)",
            'year_date'   => "YEAR(p.timing_date)",
            'sum_pg'      => 'SUM(IF((p.pg_id != 0), 1 ,0))',
            'sum_sale'    => 'SUM(IF((p.sale_id != 0), 1 ,0))',
            'sum_pb_sale' => 'SUM(IF((p.pb_sale_id != 0), 1 ,0))'
        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);

        if (isset($params['from']) and $params['from']) {
            $select->where('p.timing_date >= ?', $params['from']);
        }

        if (isset($params['to']) and $params['to']) {
            $select->where('p.timing_date <= ?', $params['to']);
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        $select->group(array('MONTH(p.timing_date)', 'YEAR(p.timing_date)', 'p.area_id'));

        $resu = $db->fetchAll($select);

        $data = array();
        /* .. */
        foreach ($resu as $key => $value) {
            $data[$value['month_date']][$value['year_date']][$value['area_id']] = array(
                'sum_pg'      => $value['sum_pg'],
                'sum_sale'    => $value['sum_sale'],
                'sum_pb_sale' => $value['sum_pb_sale']
            );
        }
        /* end.. */
        return $data;
    }

    public function getTotalSalesChannel($params) {
        $db          = Zend_Registry::get('db');
        $select      = $db->select();

        $arrCols = array(
            "p.staff_id", 
            "p.store_id", 
            "v.d_id", 
            "v.channel_id", 
            "total_staff" => "COUNT(DISTINCT s.id)",
            "s.title",
            "channel_name" => "c.name",
            "channel_id" => "c.id",
            "total_pgs" => "(COUNT(DISTINCT IF(s.title IN (".PGPB_TITLE.", ".CHUYEN_VIEN_BAN_HANG_TITLE."), s.id, NULL)))",
            "total_sale" => "(COUNT(DISTINCT IF(s.title IN (".SALES_TITLE."), s.id, NULL)))"
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);

        $select->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'st.regional_market = r.id', array());
        $select->joinLeft(array('v' => 'v_channel_dealer'), 'v.d_id = st.d_id', array());
        $select->joinLeft(array('c' => 'channel'), 'c.id = v.channel_id', array());
        

        $select->where('p.released_at IS NULL');
        $select->where('s.title IN (?)', array(PGPB_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, SALES_TITLE));

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        } elseif (isset($params['area']) and $params['area']) {
            $select->where('r.area_id = ?', $params['area']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('st.regional_market IN (?)', $params['province']);
        }

        if (isset($params['district']) and $params['district']) {
            $select->where('st.district IN (?)', $params['district']);
        }
        
        if (isset($params['list_staff']) and $params['list_staff']) {
            $select->where('p.staff_id IN (?)', $params['list_staff']);
        }

        $select->group('v.channel_id');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getChannel($params) {
        $db          = Zend_Registry::get('db');
        $select      = $db->select();

        $arrCols = array(
            "p.id", 
            "p.name", 
            "p.parent", 
            "p.is_ka",
            "p.color" 
        );

        $select->from(array('p' => 'channel'), $arrCols);
        $select->where('p.status = 1');

        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getTotalSalesChannelKa($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id", 
            "p.store_id",
            "title_sum" => "(CASE
            WHEN (staff.title IN (183)) THEN 'sale'
            WHEN (staff.title IN (182,293,417)) THEN 'pgpb'
            END)",
            "title_id" => "staff.title",
            "channel" => "c.channel_id",
            "sum" => "COUNT(DISTINCT staff.id)",
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('c' => 'channel_dealer_ka'), 'c.d_id = IF(d.parent = 0, d.id, d.parent)', array());
        $select->joinLeft(array('ks' => 'channel_ka_staff'), 'ks.channel_ka_id = c.channel_id', array());

        $select->where('p.released_at IS NULL', NULL);
        $select->where('ks.staff_id = ?', $params['staff_id']);
        $select->group(['c.channel_id', 'title_sum']);
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTotalStoreChannel($params) {
        $db          = Zend_Registry::get('db');
        $select      = $db->select();

        $arrCols = array(
            "p.id", 
            "p.d_id", 
            "channel_id" => "c.id", 
            "channel_name" => "c.name", 
            "total_store" => "COUNT(DISTINCT p.id)" 
        );

        $select->from(array('p' => 'store'), $arrCols);

        $select->joinLeft(array('v' => 'v_channel_dealer'), 'v.d_id = p.d_id', array());
        $select->joinLeft(array('c' => 'channel'), 'c.id = v.channel_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        $select->where('p.del IS NULL OR p.del <> 1');

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        } elseif (isset($params['area']) and $params['area']) {
            $select->where('r.area_id = ?', $params['area']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('r.id = ?', $params['province']);
        }

        if (isset($params['district']) and $params['district']) {
            $select->where('p.district = ?', $params['district']);
        }

        $select->group('c.id');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getTotalStoreChannelKa($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.d_id", 
            "channel" => "s.channel_ka_id", 
            "sum" => "COUNT(DISTINCT p.id)",
        );

        $select->from(array('p' => 'store'), $arrCols);
        $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('k' => 'channel_dealer_ka'), 'k.d_id = IF(d.parent = 0, d.id, d.parent)', array());
        $select->joinLeft(array('s' => 'channel_ka_staff'), 's.channel_ka_id = k.channel_id', array());

        $select->where('(p.del <> 1 OR p.del IS NULL)', NULL);
        $select->where('s.staff_id = ?', $params['staff_id']);
        $select->group('s.channel_ka_id');
        $result = $db->fetchAll($select);
        
        return $result;
    }

    public function getListTrainer($params) {
        $db          = Zend_Registry::get('db');
        $select      = $db->select();
        $select_main = $db->select();

        $arrCols = array(
            'p.id',
            'p.title',
            'fullname' => "CONCAT(p.firstname, ' ', p.lastname)",
            'email'    => "p.email",
            'area'     => "GROUP_CONCAT(ar.name)"
        );

        $select->from(array('p' => 'staff'), $arrCols);

        $select->joinLeft(array('a' => 'asm'), 'a.staff_id = p.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->joinLeft(array('ar' => 'area'), 'ar.id = a.area_id', array());

        $select->where('p.team IN (?)', array(TRAINING_TEAM));
        $select->where('p.off_date IS NULL', null);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['view_trainer']) and $params['view_trainer']) {
            $select->where('p.title IN (?)', $params['view_trainer']);
        }

        $select->group(array('p.id'));

        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }

    public function getStaffBiTeamTrainer($params) {
        $db          = Zend_Registry::get('db');
        $select      = $db->select();
        $select_main = $db->select();

        $arrCols = array(
            'p.team',
            'sum' => 'COUNT(p.id)'
        );

        $select->from(array('p' => 'staff'), $arrCols);

        $select->joinLeft(array('a' => 'asm'), 'a.staff_id = p.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->joinLeft(array('ar' => 'area'), 'ar.id = a.area_id', array());

        $select->where('p.team IN (?)', array(TRAINING_TEAM));
        $select->where('p.off_date IS NULL', null);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('ar.id IN (?)', $params['area_list']);
        }

        if (isset($params['view_trainer']) and $params['view_trainer']) {
            $select->where('p.title IN (?)', $params['view_trainer']);
        }

        $select->group(array('p.team'));
        $data = $db->fetchAll($select);

        $result = array();
        foreach ($data as $key => $value) {
            $result[$value['team']] = $value['sum'];
        }
        return $result;
    }

    public function getStaffBiTeam($params) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'staff'), array('team' => 'p.team', 'sum' => 'COUNT(p.id)')
        );
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['view_trainer']) and $params['view_trainer']) {
            $select->where('p.title IN (?)', $params['view_trainer']);
        }

        $select->where('p.off_date IS NULL', NULL);

        $select->group('p.team');
        $data = $db->fetchAll($select);

        $result = array();
        foreach ($data as $key => $value) {
            $result[$value['team']] = $value['sum'];
        }

        return $result;
    }

    public function getStaffBiTitle($params) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'staff'), array('title' => 'p.title', 'sum' => 'COUNT(p.id)')
        );
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        $select->where('p.off_date IS NULL', NULL);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $select->group('p.title');
        $data = $db->fetchAll($select);

        $result = array();
        foreach ($data as $key => $value) {
            $result[$value['title']] = $value['sum'];
        }

        return $result;
    }

    public function getTotalSalesChannelQuarter($params) {
        $db = Zend_Registry::get('db');

        $select = $db->select();
        $select->from(array('p' => 'channel_quarter'), array('id' => 'p.id', 'channel' => 'p.channel', 'sellout' => 'SUM(p.sellout)', 'quarter' => 'p.quarter', 'year' => 'p.year')
        );

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        } else {
            $select->where('p.area_id IS NULL', NULL);
        }

        $select->order(array('p.quarter ASC', 'p.year ASC'));
        $select->group(array('p.channel', 'p.quarter'));
        $data = $db->fetchAll($select);

        $result = array();
        foreach ($data as $key => $value) {
            $result[$value['channel']][$value['quarter']] = array(
                'sellout' => $value['sellout'],
                'quarter' => $value['quarter'],
                'year'    => $value['year']
            );
        }

        return $result;
    }

    public function getTotalSalesChannelQuarterThis($params) {
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $col = array(
            'channel' => 'v.channel_id',
            'sellout' => 'COUNT(DISTINCT p.imei_sn)',
            'year'    => 'YEAR(p.timing_date)');

        $select->from(array('p' => 'imei_kpi'), $col
        );

        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('v' => 'v_channel_dealer'), 'v.d_id = d.id', array());

        $select->where('p.timing_date >= (MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL    1 QUARTER)', null);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        $select->group(array('channel'));
        $data = $db->fetchAll($select);

        $curMonth   = date("m", time());
        $curQuarter = ceil($curMonth / 3);

        $result = array();
        foreach ($data as $key => $value) {
            $result[$value['channel']] = array(
                'sellout' => $value['sellout'],
                'quarter' => $curQuarter,
                'year'    => $value['year']
            );
        }

        return $result;
    }

    function getListFail($page, $limit, &$total, $params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.*'
        );


        //SUB QUERY
        $nestedSelect = $db->select();
        $nestedSelect->from(array('p' => 'staff'), array(
            'p.id',
            'fullname'     => "CONCAT(p.firstname, ' ', p.lastname)",
            'code'         => 'p.code',
            'email'        => 'p.email',
            'joined_at'    => 'p.joined_at',
            'area_id'      => 'a.id',
            'area_name'    => 'a.name',
            'max_scores'   => 'MAX(l.scores)',
            'group_scores' => 'GROUP_CONCAT(l.scores)',
            'avg_scores'   => 'AVG(l.scores)',
            'title_name'   => 't.name',
            'team_name'   => 't2.name',
            'id_number'    => 'p.ID_number'
                )
        );
        $nestedSelect->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $nestedSelect->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $nestedSelect->joinLeft(array('t' => 'team'), 'p.title = t.id', array());
        $nestedSelect->joinLeft(array('t2' => 'team'), 'p.team = t2.id', array());

        if (isset($params['lesson_id']) and $params['lesson_id']) {
            $nestedSelect->joinLeft(array('l' => 'lesson_scores'), 'l.staff_cmnd = p.ID_number AND l.lesson_id = ' . $params['lesson_id'] . '', array());
        } else {
            $nestedSelect->joinLeft(array('l' => 'lesson_scores'), 'l.staff_cmnd = p.ID_number', array());
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $nestedSelect->where('a.id IN (?)', $params['area_list']);
        }
        
        if (isset($params['list_title_staff']) and $params['list_title_staff']) {
            $nestedSelect->where('p.title IN (?)', $params['list_title_staff']);
        }
        
        if (isset($params['list_staff']) and $params['list_staff']) {
            $nestedSelect->where('p.id IN (?)', $params['list_staff']);
        }

        if(!empty($params['title'])){
            $nestedSelect->where('p.title IN (?)', $params['title']);
        }
        $nestedSelect->where('p.status = 1', null);
        $nestedSelect->where('p.off_date IS NULL', null);
        $nestedSelect->group('p.id');
        //END SUB QUERY


        $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);

        $select->where('p.max_scores IS NULL OR p.max_scores < ?', 10);
        
        if(!empty($params['area_id'])){
            $select->where('p.area_id = ?', $params['area_id']);
        }

        if ($limit AND empty($params['export'])) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function getListPass($page, $limit, &$total, $params) {

        $list_sale_pgs = unserialize(LIST_SALE_PGS);

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.*'
        );


        //SUB QUERY
        $nestedSelect = $db->select();
        $nestedSelect->from(array('p' => 'staff'), array(
            'p.id',
            'fullname'     => "CONCAT(p.firstname, ' ', p.lastname)",
            'code'         => 'p.code',
            'email'        => 'p.email',
            'joined_at'    => 'p.joined_at',
            'area_id'      => 'a.id',
            'area_name'    => 'a.name',
            'max_scores'   => 'MAX(l.scores)',
            'group_scores' => 'GROUP_CONCAT(l.scores)',
            'avg_scores'   => 'AVG(l.scores)',
            'title_name'   => 't.name',
            'team_name'   => 't2.name',
            'id_number'    => 'p.ID_number'
                )
        );
        $nestedSelect->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $nestedSelect->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $nestedSelect->joinLeft(array('t' => 'team'), 'p.title = t.id', array());
        $nestedSelect->joinLeft(array('t2' => 'team'), 'p.team = t2.id', array());

        if (isset($params['lesson_id']) and $params['lesson_id']) {
            $nestedSelect->joinLeft(array('l' => 'lesson_scores'), 'l.staff_cmnd = p.ID_number AND l.lesson_id = ' . $params['lesson_id'] . '', array());
        } else {
            $nestedSelect->joinLeft(array('l' => 'lesson_scores'), 'l.staff_cmnd = p.ID_number', array());
        }

        if (isset($params['area_list']) and $params['area_list']) {
            $nestedSelect->where('a.id IN (?)', $params['area_list']);
        }
        
        if (isset($params['list_title_staff']) and $params['list_title_staff']) {
            $nestedSelect->where('p.title IN (?)', $params['list_title_staff']);
        }

        $nestedSelect->where('p.title IN (?)', $params['title']);
        $nestedSelect->where('p.status = 1', null);
        $nestedSelect->where('p.off_date IS NULL', null);
        $nestedSelect->group('p.id');
        //END SUB QUERY


        $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);

        $select->where('p.max_scores IS NOT NULL OR p.max_scores > 10');
        $select->where('p.area_id = ?', $params['area_id']);

        if ($limit AND empty($params['export'])) {
            $select->limitPage($page, $limit);
        }


        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function loadGoodByMonthAll($params) {
        if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {

            $db     = Zend_Registry::get("db");
            $select = $db->select();

            $arrCols = array(
                'channel'     => 'i.channel',
                'month_date'  => "p.month_date",
                'year_date'   => "p.year_date",
                'num'         => 'SUM(p.num)',
                'total_value' => 'SUM(p.total_value*8/10)'
            );


            $nestedSelect = "SELECT p.imei_sn, IFNULL(s.d_id,0) d_id, MONTH(timing_date) month_date, YEAR(timing_date) year_date, COUNT(p.imei_sn) num, SUM(p.`value`) total_value, p.area_id, p.store_id
                            FROM imei_kpi p
                            FORCE INDEX (timing_date)
                            LEFT JOIN store s ON s.id = p.store_id
                            WHERE timing_date >= '" . $params['from'] . " 00:00:00' AND timing_date <= '" . $params['to'] . " 23:59:59'
                            GROUP BY IFNULL(s.d_id,0), MONTH(timing_date), p.area_id";

            $select->from(array('p' => new Zend_Db_Expr('(' . $nestedSelect . ')')), $arrCols);
            $select->joinLeft(array('i' => 'v_store_channel'), 'i.store_id = p.store_id', array());


            if (isset($params['area_list']) and $params['area_list']) {
                $select->where('p.area_id IN (?)', $params['area_list']);
            }

            if (isset($params['channel_id']) AND $params['channel_id']) {
                $select->where('i.parent IN (?)', $params['channel_id']);
            }

            $select->group(array("channel", "p.month_date"));


            $select2 = $db->select();
            $select2->from(array('p' => 'kpi_month'), array('p.channel', 'p.month', 'p.year', 'SUM(p.sell_out)', 'SUM(p.sell_out_value)'));
            if (isset($params['area_list']) and $params['area_list']) {
                $select2->where('p.area_id IN (?)', $params['area_list']);
            } else {
                $select2->where('p.area_id IS NULL', null);
            }


            $select2->group(array('p.channel', 'p.month', 'p.year'));

            $nestedSelect = $db->select()
                    ->union(array($select, $select2));

            $result = $db->fetchAll($nestedSelect);

            return $result;
        }
    }

    public function loadGoodByMonthProvince($params) {
        $db = Zend_Registry::get('db');

        $col = array(
            'channel'     => 'i.channel',
            'month_date'  => 'MONTH(p.timing_date)',
            'year_date'   => 'YEAR(p.timing_date)',
            'total_value' => "SUM(p.`value`)*80/100",
            'num'         => "COUNT(p.imei_sn)"
        );

        $select = $db->select();
        $select->from(array('p' => 'imei_kpi'), $col);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('i' => 'v_store_channel'), 'i.store_id = s.id', array());


        if (isset($params['from_date']) AND $params['from_date'])
            $select->where('p.timing_date >= ?', $params['from_date']);
        else
            return false;

        if (isset($params['province_list']) AND $params['province_list'])
            $select->where('p.province_id IN (?)', $params['province_list']);
        else
            return false;

        if (isset($params['district_list']) AND $params['district_list']) {
            $select->where('p.district_id IN (?)', $params['district_list']);
        }

        if (isset($params['channel_id']) AND $params['channel_id']) {
            $select->where('i.parent IN (?)', $params['channel_id']);
        }

        $select->group(array('i.channel', "DATE_FORMAT(p.timing_date, '%Y-%m')"));

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getRankStar($params) {
        $db = Zend_Registry::get('db');
        
        $list_sale_pgs = unserialize(LIST_PGS_BI);
        
        $select = $db->select();
        $select->from(array('p' => 'sp_assessment_temp_date'), array('star' => 'p.result', 'sum' => 'COUNT(DISTINCT p.staff_id)')
        );
        
        
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.staff_id AND ((st.released_at IS NULL OR st.released_at > UNIX_TIMESTAMP()))', array());
        $select->joinLeft(array('store' => 'store'), 'store.id = st.store_id', array());
        
        $select->where('p.date = (SELECT max(date) FROM sp_assessment_temp_date)', NULL);
        $select->where('s.title IN (?)', $list_sale_pgs);
        
        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('a.id IN (?)', $params['area_list']);
        }

        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('s.regional_market = ?', $params['province']);
        }
        
        if (isset($params['list_title_staff']) and $params['list_title_staff']) {
            $select->where('s.title IN (?)', $params['list_title_staff']);
        }

        if (isset($params['district']) and $params['district']) {
            $select->where('store.district = ?', $params['district']);
        }

        if (isset($params['is_brand_shop']) and $params['is_brand_shop']) {
            $select->where('store.is_brand_shop = ?', $params['is_brand_shop']);
        }
        
        if (isset($params['list_staff']) and $params['list_staff']) {
            $select->where('p.staff_id IN (?)', $params['list_staff']);
        }

        $select->group(array('IFNULL(result,0)'));
        $select->order(array('result DESC'));
        
        
        
        $data = $db->fetchAll($select);

        return $data;
    }
    
    public function getRankStarSales($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.code", 
            "r.star", 
            "sum" => "COUNT(DISTINCT p.id)"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'rank_star_sales'), 'r.staff_id = p.id', array());

        $select->where('p.title IN (183,190) AND p.off_date IS NULL AND r.star IS NOT NULL');
        $select->group('r.star');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    function getListRankStar($page, $limit, &$total, $params) {

        $list_sale_pgs = unserialize(LIST_PGS_BI);

        $db = Zend_Registry::get("db");

        $select = $db->select();
        $select->from(array('p' => 'sp_assessment_temp_date'), array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.staff_id'),
            'p.staff_code',
            'p.firstname',
            'p.lastname',
            'p.photo',
            'p.total_attendance',
            'p.total_ot',
            'p.total_kpi',
            'p.total_warning',
            'p.total_pass',
            'p.total_fail',
            'p.total',
            'p.result',
            'area' => 'a.name',
            'p.title',
            'p.thamnien',
            'area_id' => 'a.id',
			'approve.confirmed_at'
                )
        );

        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
		$select->joinLeft(array('approve' => 'sp_assessment_approve'), 'approve.staff_id = p.staff_id AND approve.result = p.result AND approve.confirmed_at IS NOT NULL', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.staff_id AND ((st.released_at IS NULL OR st.released_at > UNIX_TIMESTAMP()))', array());
        $select->joinLeft(array('store' => 'store'), 'store.id = st.store_id', array());

        if (($params['rank'])) {
            $select->where('p.result IN (?)', $params['rank']);
        }
        
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        
        $select->where('p.date = (SELECT max(date) FROM sp_assessment_temp_date)', NULL);
        
        $select->where('s.off_date IS NULL', NULL);
        $select->where('s.title IN (?)', $list_sale_pgs);
        
        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%'.$params['name'].'%');
        }
        
        if (isset($params['code']) and $params['code']) {
            $select->where('s.code = ?', $params['code']);
        }
        
        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('a.id IN (?)', $params['area_list']);
        }

        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if (isset($params['district']) and $params['district']) {
            $select->where('store.district = ?', $params['district']);
        }

        if (isset($params['title']) and $params['title']) {
            $select->where('s.title IN (?)', $params['title']);
        }
        
        if (isset($params['list_staff']) and $params['list_staff']) {
            $select->where('p.staff_id IN (?)', $params['list_staff']);
        }
        
        
        if ($limit && !$params['export']) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        $arrayStaffId=null;
        foreach($result as $key => $value){
            $arrayStaffId[]=$value['staff_id'];
        }

        $select2 = $db->select();
        $select3 = $db->select();

        $arrColsView = array('staff_id',
                            'total_attendance',
                            'total_ot',
                            'total_kpi',
                            'total_warning',
                            'total_reward', 'total_pass',
                            'total_fail',
                            'total',
                            'result',
                            'thamnien');
        if(!empty($arrayStaffId)){
            $select2->from(['v' => 'v_first_day_pg_up_star'], $arrColsView);
            $select2->where('v.staff_id IN (?)',$arrayStaffId);
            $result2 = $db->fetchAll($select2);
    
            $select3->from(['v' => 'v_first_day_approved_up_star'], $arrColsView);
            $select3->where('v.staff_id IN (?)', $arrayStaffId);
            $result3 = $db->fetchAll($select3);
        }
        $arrPointSub=null;
        foreach($result2 as $key => $value){
            $result2[$value['staff_id']]=$value;
            unset($result2[$key]);
        }
        foreach($result3 as $key => $value){
            $result3[$value['staff_id']]=$value;
            unset($result3[$key]);
            if(!empty($result2[$value['staff_id']]) AND !empty($result3[$value['staff_id']])){
                foreach($result2 as $k => $v){
                    $arrPointSub[$value['staff_id']]['total_attendance']    = $result2[$value['staff_id']]['total_attendance'] - $result3[$value['staff_id']]['total_attendance'];
                    $arrPointSub[$value['staff_id']]['total_ot']            = $result2[$value['staff_id']]['total_ot'] - $result3[$value['staff_id']]['total_ot'];
                    $arrPointSub[$value['staff_id']]['total_kpi']           = $result2[$value['staff_id']]['total_kpi'] - $result3[$value['staff_id']]['total_kpi'];
                    $arrPointSub[$value['staff_id']]['total_pass']          = $result2[$value['staff_id']]['total_pass'] - $result3[$value['staff_id']]['total_pass'];
                    $arrPointSub[$value['staff_id']]['total_fail']          = $result2[$value['staff_id']]['total_fail'] - $result3[$value['staff_id']]['tottotal_failal_attendance'];
                    $arrPointSub[$value['staff_id']]['total']               = $result2[$value['staff_id']]['total'] - $result3[$value['staff_id']]['total'];
                    $arrPointSub[$value['staff_id']]['thamnien']            = $result2[$value['staff_id']]['thamnien'] - $result3[$value['staff_id']]['thamnien'];
                    $arrPointSub[$value['staff_id']]['result']              = $result2[$value['staff_id']]['result'] - $result3[$value['staff_id']]['result'];
                }
            }
        }
        foreach($result as $key => $value){
            if(array_key_exists($value['staff_id'],$arrPointSub)){ 
                foreach($arrPointSub[$value['staff_id']] as $k => $v){
                        $result[$key][$k]=$value[$k] + $v;
                }
            }
        }
        return $result;


    }

    public function getListStaff($params) {
        $db          = Zend_Registry::get('db');
        $select      = $db->select();
        $select_main = $db->select();

        $arrCols = array(
            'id'         => new Zend_Db_Expr("DISTINCT s.id"),
            's.title',
            'title_name' => 't.name'
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);

        $select->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = p.store_id', array());

        $select->where('p.released_at IS NULL OR p.released_at > UNIX_TIMESTAMP()', NULL);
        $select->where('s.title IN (?)', array(182, 274, 312, 183, 293));

        if (isset($params['district']) and $params['district']) {
            $select->where('st.district IN (?)', $params['district']);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSelloutGoodBrandShop($params) {

        if (empty($params['from']) OR empty($params['to'])) {
            return false;
        }

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.good_id',
            'month_date'  => 'MONTH(p.timing_date)',
            'num'         => 'SUM(p.qty)',
            'total_value' => 'SUM(p.value*8/10)'
        );

        $select->from(array('p' => 'kpi_by_model'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('v' => 'v_channel_dealer'), 'v.d_id = s.d_id', array());

        $select->where('DATE(p.timing_date) >= ?', $params['from']);
        $select->where('DATE(p.timing_date) <= ?', $params['to']);
        $select->where('v.channel_id = 17');
        $select->group(array('p.good_id', 'MONTH(p.timing_date)'));

        $result            = $db->fetchAll($select);
        /* get sellout, total value theo t?ng s?n ph?m */
        $data_sellout_good = [];
        $data_sellout      = [];
        foreach ($result as $key => $value) {
            $data_sellout_good[$value['good_id']][$value['month_date']] = array(
                'num'         => $value['num'],
                'total_value' => $value['total_value']
            );

            if (isset($data_sellout[$value['month_date']]) and $data_sellout[$value['month_date']]) {
                $data_sellout[$value['month_date']]['num']         += $value['num'];
                $data_sellout[$value['month_date']]['total_value'] += $value['total_value'];
            } else {
                $data_sellout[$value['month_date']]['num']         = $value['num'];
                $data_sellout[$value['month_date']]['total_value'] = $value['total_value'];
            }
        }
        /* END sellout, total value theo t?ng s?n ph?m */
        return ['data_sellout_good' => $data_sellout_good, 'data_sellout' => $data_sellout];
    }

    public function getSelloutGoodBrandShopTop($params) {

        if (empty($params['from']) OR empty($params['to'])) {
            return false;
        }

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.good_id',
            'month_date'  => 'MONTH(p.timing_date)',
            'num'         => 'SUM(p.qty)',
            'total_value' => 'SUM(p.value*8/10)',
            'good_name'   => 'g.name',
            'good_desc'   => 'g.desc',
        );

        $select->from(array('p' => 'kpi_by_model'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('v' => 'v_channel_dealer'), 'v.d_id = s.d_id', array());
        $select->joinLeft(array('g' => WAREHOUSE_DB.'.good'), 'g.id = p.good_id', array());

        $select->where('DATE(p.timing_date) >= ?', $params['from']);
        $select->where('DATE(p.timing_date) <= ?', $params['to']);
        $select->where('v.channel_id = 17');
        $select->group('p.good_id');
        $select->order("SUM(p.qty) DESC");

        $select->limitPage(0,5);

        $result            = $db->fetchAll($select);

        return $result;
        

    }

    public function getSelloutGoodBrandShopArea($params) {

        if (empty($params['from']) OR empty($params['to']) OR empty($params['brand_shop_id'])) {
            return false;
        }
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'area_id'     => 'p.area_id',
            'num'         => 'COUNT(p.imei_sn)',
            'total_value' => 'SUM(p.`value`*8/10)'
        );
        $select->from(array('p' => 'imei_kpi'), $arrCols);
        $select->where('p.store_id IN (?)', $params['brand_shop_id']);
        $select->where('DATE(p.timing_date) >= ?', $params['from']);
        $select->where('DATE(p.timing_date) <= ?', $params['to']);
        $select->group('p.area_id');
        $result  = $db->fetchAll($select);
        return $result;
    }

    public function fetchStaffTitlePagination($page, $limit, &$total, $params) {
        $list_title = unserialize(LIST_STAFF_TITLE);

        $db          = Zend_Registry::get('db');
        $select      = $db->select();
        $select_main = $db->select();
        $select_hero = $db->select();
        $select_imei_kpi = $db->select();
        
        $date_parent = date('Y-m-01');
        
        $arrHero     = array(
            'id' => 'GROUP_CONCAT(id)'
        );
        $select_hero->from(array('p' => WAREHOUSE_DB.'.good'), $arrHero);
        $select_hero->where('p.hero_product = 1', NULL);
        $select_hero->where('p.hero_product_expired IS NULL', NULL);
        $hero = $db->fetchRow($select_hero);
        
        $arr_imei_kpi     = array(
            'p.imei_sn', 
            'p.pg_id', 
            'p.good_id', 
            'p.timing_date'
        );
        $select_imei_kpi->from(array('p' => 'imei_kpi'), $arr_imei_kpi);
        $select_imei_kpi->where('p.timing_date >= ?', $date_parent);
        
        $arrCols = array(
            'id'              => 'p.id',
            "p.code",
            "p.email",
            "fullname"        => "CONCAT(p.firstname, ' ', p.lastname)",
            "p.department",
            'shop'            => 'sto.name',
            'total'           => 'sp.total',
            'star'            => 'sp.result',
            "department_name" => "t.name",
            "team"            => "t2.name",
            "title_id"        => 'p.title',
            "title"           => "t3.name",
            'joined'          => "p.joined_at",
            'phone'           => "p.phone_number",
            "p.status",
            'sellout'         => "COUNT(i.imei_sn)",
            'hero'            => 'COUNT(DISTINCT IF(i.good_id IN ('.$hero['id'].'), i.imei_sn, NULL))',
            'area_name'       => 'a.name',
            'shop_id'         => 'sto.id',
            'channel'         => 'ch.channel_name'
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        // $select->joinLeft(array('bigarea' => 'bigarea'), 'bigarea.id = a.bigarea_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.department', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.team', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = p.title', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.id and st.released_at IS NULL', array());
        $select->joinLeft(array('sto' => 'store'), 'sto.id = st.store_id', array());
        $select->joinLeft(array('c'   => 'v_channel_store'), 'c.store_id = sto.id', array());
        $select->joinLeft(array('ch' => 'v_channel'), 'ch.channel_id = c.channel_id', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = p.id', array());
        $select->joinLeft(array('i' => $select_imei_kpi), 'i.pg_id = p.id', array());
        $select->where("p.off_date IS NULL", NULL);
        $select->where('p.title IN (?)', $params['title']);

        if (!empty($params['area_list'])) {
            $select->where("r.area_id IN (?)", $params['area_list']);
        }
        if (!empty($params['area_id'])) {
            $select->where("r.area_id = ?", $params['area_id']);
        }
        if (!empty($params['big_area_id'])) {
            $select->where("a.bigarea_id IN (?)", $params['big_area_id']);
        }
        if (!empty($params['province_id'])) {
            $select->where("r.id = ?", $params['province_id']);
        }
        
        if (!empty($params['list_staff'])) {
            $select->where("p.id IN (?)", $params['list_staff']);
        }
        
        $select->group('p.id');



        $arrCols_main = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS f.id'),
            "f.code",
            "f.email",
            "f.fullname",
            "f.department",
            'f.shop',
            'f.total',
            'f.star',
            "f.department_name",
            "f.team",
            "f.title_id",
            "f.title",
            "f.joined",
            "f.phone",
            "f.status",
            'f.sellout',
            'hero' => "IFNULL(f.hero,0)",
            'f.area_name',
            'f.shop_id',
            'f.channel'
        );
        $select_main->from(array('f' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = f.id', array());
        //search
        if (!empty($params['seniority'])) {
            $select_main->where('v.type_date = ?', $params['seniority']);
        }
        if (!empty($params['name'])) {
            $select_main->where('f.fullname LIKE ?', '%' . $params['name'] . '%');
        }
        if (!empty($params['email'])) {
            $select_main->where('f.email LIKE ?', '%' . $params['email'] . '%');
        }
        if (!empty($params['code'])) {
            $select_main->where('f.code = ?', $params['code']);
        }
        //  if(!empty($params['title_search']))
        // {
        //     $select_main->where('f.title = ?', $params['title_search']);
        // }


        if (!empty($params['shop'])) {
            $select_main->where('f.shop LIKE ?', '%' . $params['shop'] . '%');
        }
        if (!empty($params['star'])) {
            $select_main->where('f.star = ?', $params['star']);
        }
        if (!empty($params['from_date']) && !empty($params['to_date'])) {
            $select_main->where('f.joined >= ?', $params['from_date']);

            $select_main->where('f.joined <= ?', $params['to_date']);
        }
        if (!empty($params['from_date']) && empty($params['to_date'])) {
            $select_main->where('f.joined =  ?', $params['from_date']);
        }
        if (!empty($params['to_date'] && empty($params['from_date']))) {
            $select_main->where('f.joined =  ?', $params['to_date']);
        }
        if (!empty($params['sellout_from']) && !empty($params['sellout_to'])) {
            $select_main->where('f.sellout >= ?', $params['sellout_from']);

            $select_main->where('f.sellout <= ?', $params['sellout_to']);
        }
        if (!empty($params['sellout_from']) && empty($params['sellout_to'])) {
            $select_main->where('f.sellout=  ?', $params['sellout_from']);
        }
        if (!empty($params['sellout_to'] && empty($params['sellout_from']))) {
            $select_main->where('f.sellout =  ?', $params['sellout_to']);
        }
        

        if ($limit)
            $select_main->limitPage($page, $limit);

        $result = $db->fetchAll($select_main);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }

    public function getListStaffByTitle($params) {

        $list_staff = unserialize(LIST_STAFF_TITLE);

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.title',
            't.parent_id',
            'title_name' => 't.name',
            'number'     => "COUNT(DISTINCT p.id)"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array());
        $select->where('p.title IN (?)', $list_staff);
        $select->where('p.off_date IS NULL', NULL);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['list_title']) and $params['list_title']) {
            //$select->where('p.title IN (?)', $params['list_title']);
        }
        
        if(isset($params['list_title_staff']) and $params['list_title_staff']){
            //$select->where('p.title IN (?)', $params['list_title_staff']);
        }

        $select->group('p.title');
        $select->order(array("field(p.title,308,179,190,183,293,182,419,417,403,545) ASC"));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function countListStaff($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.title',
            't.parent_id',
            'title_name' => 't.name',
            'number'     => "COUNT(DISTINCT p.id)"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array());
        $select->where('p.off_date IS NULL', NULL);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['list_title']) and $params['list_title']) {
            $select->where('p.title IN (?)', $params['list_title']);
        }
        
        if (isset($params['list_team']) and $params['list_team']) {
            $select->where('p.team IN (?)', $params['list_team']);
        }

        $select->group('p.title');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getThamnien_Month($params, &$total_thamnien) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'type_date_name' => '(CASE
                            WHEN p.joined_at >= DATE_SUB(NOW(),INTERVAL 6 MONTH) THEN "Dưới 6 tháng"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 12 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 6 MONTH) ) THEN "6 đến 12 tháng"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 24 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 12 MONTH) ) THEN "12 đến 24 tháng"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 36 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 24 MONTH) ) THEN "24 đến 36 tháng"
                            ELSE "Trên 36 tháng"
                            END
                            )',
            'type_date'      => '(CASE
                            WHEN p.joined_at >= DATE_SUB(NOW(),INTERVAL 6 MONTH) THEN 1
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 12 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 6 MONTH) ) THEN 2
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 24 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 12 MONTH) ) THEN 3
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 36 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 24 MONTH) ) THEN 4
                            ELSE 5
                            END
                            )',
            'quantity'       => "COUNT(DISTINCT p.id)"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.off_date IS NULL', NULL);
        $select->where('p.title IN (?)', array(182, 293, 419));
        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        
        if (isset($params['list_title_staff']) and $params['list_title_staff']) {
            $select->where('p.title IN (?)', $params['list_title_staff']);
        }
        
        if (isset($params['list_staff']) and $params['list_staff']) {
            $select->where('p.id IN (?)', $params['list_staff']);
        }

        $select->group('type_date_name');
        $select->order('type_date');

        $result         = $db->fetchAll($select);
        $total_thamnien = 0;
        foreach ($result as $key => $value) {
            $total_thamnien += $value['quantity'];
        }
        return $result;
    }

    public function getSellOut_($params) {

        $list_date = array(
            intval(date('m', strtotime(date('Y-m') . " -3 month"))),
            intval(date('m', strtotime(date('Y-m') . " -2 month"))),
            intval(date('m', strtotime(date('Y-m') . " -1 month"))),
            intval(date('m')),
        );

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'sellout_name' => '(CASE
                             WHEN (p.sellout = 0) THEN "0"
                            WHEN (p.sellout >= 1 AND p.sellout < 10) THEN "1 - 10"
                            WHEN (p.sellout >= 10 AND p.sellout <= 19) THEN "10 - 19"
                             WHEN (p.sellout >= 20 AND p.sellout <= 29) THEN "20 - 29"
                              WHEN (p.sellout >= 30 AND p.sellout <= 39) THEN "30 - 39"
                               WHEN (p.sellout >= 40 AND p.sellout <= 49) THEN "40 - 49"
                            ELSE ">= 50"
                            END
                            )',
            'sellout_type' => '(CASE
                            WHEN (p.sellout = 0) THEN 0
                            WHEN (p.sellout >= 1 AND p.sellout < 10) THEN 1
                            WHEN (p.sellout >= 10 AND p.sellout <= 19) THEN 2
                             WHEN (p.sellout >= 20 AND p.sellout <= 29) THEN 3
                              WHEN (p.sellout >= 30 AND p.sellout <= 39) THEN 4
                               WHEN (p.sellout >= 40 AND p.sellout <= 49) THEN 5
                            ELSE 6
                            END
                            )',
            'total'        => "COUNT(p.code)",
            'month'        => 'p.month',
            'year'         => 'p.year'
        );

        $select->from(array('p' => DATABASE_SALARY . '.v_list_sellout_pg'), $arrCols);
        if (!empty($params['area_list'])) {
            $select->where('p.khuvuc IN (?)', $params['area_list']);
        }
        if (!empty($params['list_title_staff'])) {
            $select->where('p.title IN (?)', ['SENIOR PROMOTER', 'PGPB', ]);
            $select->where('p.team = "BRAND SHOP"', NULL);
        }
        
        if(!empty($params['list_staff'])){
            $select->where('p.staff_id IN (?)', $params['list_staff']);
        }
        
        $select->where('p.month IN (?)', $list_date);
        $select->group('sellout_name');
        $select->group(['month', 'year']);
        $result = $db->fetchAll($select);
        

        return $result;
    }

    public function getListSellout($page, $limit, &$total, $params) {

        $list_date   = array(
            intval(date('m', strtotime(date('Y-m') . " -3 month"))),
            intval(date('m', strtotime(date('Y-m') . " -2 month"))),
            intval(date('m', strtotime(date('Y-m') . " -1 month"))),
            intval(date('m')),
        );
        $date_parent = date('Y-m-01');
        $db          = Zend_Registry::get("db");
        $select      = $db->select();
        $select_main = $db->select();
        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
            'sellout_name' => '(CASE
                             WHEN (p.sellout = 0) THEN "0"
                            WHEN (p.sellout >= 1 AND p.sellout < 10) THEN "1 - 10"
                            WHEN (p.sellout >= 10 AND p.sellout <= 19) THEN "10 - 19"
                             WHEN (p.sellout >= 20 AND p.sellout <= 29) THEN "20 - 29"
                              WHEN (p.sellout >= 30 AND p.sellout <= 39) THEN "30 - 39"
                               WHEN (p.sellout >= 40 AND p.sellout <= 49) THEN "40 - 49"
                            ELSE ">= 50"
                            END
                            )',
            'sellout_type' => '(CASE
                            WHEN (p.sellout = 0) THEN 0
                            WHEN (p.sellout >= 1 AND p.sellout < 10) THEN 1
                            WHEN (p.sellout >= 10 AND p.sellout <= 19) THEN 2
                             WHEN (p.sellout >= 20 AND p.sellout <= 29) THEN 3
                              WHEN (p.sellout >= 30 AND p.sellout <= 39) THEN 4
                               WHEN (p.sellout >= 40 AND p.sellout <= 49) THEN 5
                            ELSE 6
                            END
                            )',
            'staff_id'     => "p.staff_id",
            'month'        => 'p.month',
            'year'         => 'p.year',
            'shop'         => 'p.shop',
            'sellout'      => "COUNT(i.imei_sn)",
            'hero'         => 'sub.hero'
        );

        $select->from(array('p' => DATABASE_SALARY . '.v_list_sellout_pg'), $arrCols);
        $select->joinLeft(array('i' => 'imei_kpi'), 'i.pg_id = p.staff_id AND i.timing_date >= "' . $date_parent . '"', array());
        $select->joinLeft(array('sub' => $select_hero), 'sub.pg_id = p.staff_id', array());
        if (!empty($params['area_list'])) {
            $select->where('p.khuvuc IN (?)', $params['area_list']);
        }
        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('p.khuvuc = ?', $params['area_id']);
        }
        if (isset($params['province_id']) and $params['province_id']) {
            $select->where('p.province_id = ?', $params['province_id']);
        }
        
        if (!empty($params['list_staff'])) {
            $select->where('p.staff_id IN (?)', $params['list_staff']);
        }
        
        $select->where('p.month IN (?)', $list_date);
        $select->group('sellout_name');
        $select->group(['month', 'year', 'p.staff_id']);



        $arrCols_main = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT s.id'),
            'staff_id' => 's.id',
            'code'     => 's.code',
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            'email'    => 's.email',
            'joined'   => 's.joined_at',
            'phone'    => 's.phone_number',
            'status'   => 's.status',
            'team'     => 't.name',
            'title'    => 'tt.name',
            'total'    => 'sp.total',
            'star'     => 'sp.result',
            'shop'     => 'f.shop',
            'sellout'  => 'f.sellout',
            'hero'     => "IFNULL(f.hero,0)"
        );
        $select_main->from(array('f' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->joinLeft(array('s' => 'staff'), 's.id = f.staff_id', array());
        $select_main->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = s.id', array());
        $select_main->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = s.id', array());
        $select_main->joinLeft(array('t' => 'team'), 't.id = s.team', array());
        $select_main->joinLeft(array('tt' => 'team'), 'tt.id = s.title', array());
        $select_main->where('f.month = ?', $params['m']);
        $select_main->where('f.year = ?', $params['y']);

        $select_main->where('f.sellout_type = ?', $params['type_sell']);

        //search
        if (!empty($params['seniority'])) {
            $select_main->where('v.type_date = ?', $params['seniority']);
        }
        if (!empty($params['name'])) {
            $select_main->where('CONCAT(s.firstname," ",s.lastname) LIKE ?', '%' . $params['name'] . '%');
        }
        if (!empty($params['email'])) {
            $select_main->where('s.email LIKE ?', '%' . $params['email'] . '%');
        }
        if (!empty($params['code'])) {
            $select_main->where('s.code = ?', $params['code']);
        }
        if (!empty($params['title'])) {
            $select_main->where('tt.id LIKE ?', $params['title']);
        }


        if (!empty($params['shop'])) {
            $select_main->where('f.shop LIKE ?', '%' . $params['shop'] . '%');
        }
        if (!empty($params['star'])) {
            $select_main->where('sp.result = ?', $params['star']);
        }
        if (!empty($params['from_date']) && !empty($params['to_date'])) {
            $select_main->where('s.joined_at >= ?', $params['from_date']);

            $select_main->where('s.joined_at <= ?', $params['to_date']);
        }
        if (!empty($params['from_date']) && empty($params['to_date'])) {
            $select_main->where('s.joined_at =  ?', $params['from_date']);
        }
        if (!empty($params['to_date'] && empty($params['from_date']))) {
            $select_main->where('s.joined_at =  ?', $params['to_date']);
        }
        if (!empty($params['sellout_from']) && !empty($params['sellout_to'])) {
            $select_main->where('f.sellout >= ?', $params['sellout_from']);

            $select_main->where('f.sellout <= ?', $params['sellout_to']);
        }
        if (!empty($params['sellout_from']) && empty($params['sellout_to'])) {
            $select_main->where('f.sellout=  ?', $params['sellout_from']);
        }
        if (!empty($params['sellout_to'] && empty($params['sellout_from']))) {
            $select_main->where('f.sellout =  ?', $params['sellout_to']);
        }
        
        
        $select_main->group('s.id');

        $select_main->limitPage($page, $limit);
        $result = $db->fetchAll($select_main);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getListSeniority($page, $limit, &$total, $params) {

        $db          = Zend_Registry::get("db");
        $select      = $db->select();
        $select_main = $db->select();
        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $date_parent = date('Y-m-01');
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
            'type_date_name' => '(CASE
                            WHEN p.joined_at >= DATE_SUB(NOW(),INTERVAL 3 MONTH) THEN "D??i 3 th�ng"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 6 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 3 MONTH) ) THEN "3 ??n 6 th�ng"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 12 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 6 MONTH) ) THEN "6 ??n 12 th�ng"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 24 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 12 MONTH) ) THEN "12 ??n 24 th�ng"
                            ELSE "Tr�n 24 th�ng"
                            END
                            )',
            'type_date'      => '(CASE
                            WHEN p.joined_at >= DATE_SUB(NOW(),INTERVAL 3 MONTH) THEN 1
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 6 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 3 MONTH) ) THEN 2
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 12 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 6 MONTH) ) THEN 3
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 24 MONTH) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 12 MONTH) ) THEN 4
                            ELSE 5
                            END
                            )',
            'staff_id'       => 'p.id',
            'fullname'       => "CONCAT(p.firstname, ' ', p.lastname)",
            'code'           => 'p.code',
            'team'           => "t.id",
            'email'          => 'p.email',
            'phone'          => 'p.phone_number',
            'joined'         => 'p.joined_at',
            'status'         => 'p.status',
            'title'          => 'p.title',
            'sellout'        => "COUNT(i.imei_sn)",
            'hero'           => 'sub.hero'
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->joinLeft(array('i' => 'imei_kpi'), 'i.pg_id = p.id AND i.timing_date >= "' . $date_parent . '"', array());
        $select->joinLeft(array('sub' => $select_hero), 'sub.pg_id = p.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.team', array());

        $select->where('p.off_date IS NULL', NULL);
        $select->where('p.title IN (?)', array(182, 293, 419));
        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }
        if (isset($params['province_id']) and $params['province_id']) {
            $select->where('r.id = ?', $params['province_id']);
        }
        $select->group(['type_date_name', 'p.id']);

        $arrCols_main = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT q.staff_id'),
            'code'     => 'q.code',
            'fullname' => 'q.fullname',
            'email'    => 'q.email',
            'joined'   => 'q.joined',
            'phone'    => 'q.phone',
            'status'   => 'q.status',
            'team'     => 't.name',
            'title'    => 'tt.name',
            'total'    => 'sp.total',
            'star'     => 'sp.result',
            'shop'     => 'GROUP_CONCAT(s.name)',
            'sellout'  => 'q.sellout',
            'hero'     => "IFNULL(q.hero,0)"
        );

        $select_main->from(array('q' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->joinLeft(array('st' => 'staff'), 'st.id = q.staff_id', array());
        $select_main->joinLeft(array('sp' => 'sp_assessment_temp'), 'q.staff_id = sp.staff_id', array());
        $select_main->joinLeft(array('l' => 'store_staff_log'), 'l.staff_id = st.id AND l.released_at IS NULL', array());
        $select_main->joinLeft(array('s' => 'store'), 's.id = l.store_id', array());
        $select_main->joinLeft(array('t' => 'team'), 't.id = q.team', array());
        $select_main->joinLeft(array('tt' => 'team'), 'tt.id = st.title', array());

        $select_main->where('q.type_date = ?', $params['type']);



        if (!empty($params['name'])) {
            $select_main->where('q.fullname LIKE ?', '%' . $params['name'] . '%');
        }
        if (!empty($params['email'])) {
            $select_main->where('q.email LIKE ?', '%' . $params['email'] . '%');
        }
        if (!empty($params['code'])) {
            $select_main->where('q.code = ?', $params['code']);
        }

        if (!empty($params['title'])) {
            $select_main->where('tt.id = ?', $params['title']);
        }
        
        if (!empty($params['list_title_staff'])) {
            $select_main->where('tt.id IN (?)', $params['list_title_staff']);
        }

        if (!empty($params['shop'])) {
            $select_main->where('s.name LIKE ?', '%' . $params['shop'] . '%');
        }
        if (!empty($params['star'])) {
            $select_main->where('sp.result = ?', $params['star']);
        }
        if (!empty($params['from_date']) && !empty($params['to_date'])) {
            $select_main->where('q.joined >= ?', $params['from_date']);

            $select_main->where('q.joined <= ?', $params['to_date']);
        }
        if (!empty($params['from_date']) && empty($params['to_date'])) {
            $select_main->where('q.joined=  ?', $params['from_date']);
        }
        if (!empty($params['to_date'] && empty($params['from_date']))) {
            $select_main->where('q.joined =  ?', $params['to_date']);
        }
        if (!empty($params['sellout_from']) && !empty($params['sellout_to'])) {
            $select_main->where('q.sellout >= ?', $params['sellout_from']);

            $select_main->where('q.sellout <= ?', $params['sellout_to']);
        }
        if (!empty($params['sellout_from']) && empty($params['sellout_to'])) {
            $select_main->where('q.sellout=  ?', $params['sellout_from']);
        }
        if (!empty($params['sellout_to'] && empty($params['sellout_from']))) {
            $select_main->where('q.sellout =  ?', $params['sellout_to']);
        }
        $select_main->group('q.staff_id');
        $select_main->limitPage($page, $limit);
        $result = $db->fetchAll($select_main);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getListTitleEmp($page, $limit, &$total, $params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select_sellout = $db->select();
        $arrSel         = array(
            'pg_id'      => 'i.pg_id',
            'total_imei' => "COUNT(DISTINCT i.imei_sn)"
        );
        $date_parent    = date('Y-m-01');

        $select_sellout->from(array('i' => 'imei_kpi'), $arrSel);
        $select_sellout->where('i.timing_date >= ?', $date_parent);
        $select_sellout->group('i.pg_id');

        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS b.id'),
            'code'        => 'b.code',
            'sellout'     => 'sub.total_imei',
            'shop'        => 'st.name',
            'total'       => 'sp.total',
            'star'        => 'sp.result',
            'team'        => 't.name',
            'staff_id'    => 'b.id',
            'fullname'    => "CONCAT(b.firstname, ' ', b.lastname)",
            'department'  => 'b.department',
            'phone'       => 'b.phone_number',
            'email'       => 'b.email',
            'joined'      => 'b.joined_at',
            'status'      => 'b.status',
            'YearNo'      => "a.YearNo",
            'MonthNo'     => "a.MonthNo",
            'name'        => "d.name",
            'area_id'     => "d.id",
            'title'       => "tt.name",
            'hero'        => "ifnull(su.hero,0)",
            'totalEmp'    => 'SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Working'     => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end)',
            'off'         => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end)',
            'Per_Working' => 'SUM(case when b.joined_at between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)',
            'Per_Off'     => 'SUM(case when b.off_date IS NOT NULL AND b.off_date between a.FromDate and a.ToDate then 1 else 0 end) / SUM(case when b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate then 1 else 0 end)'
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate and b.title in (182,293,419)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = b.team', array());
        $select->joinLeft(array('tt' => 'team'), 'tt.id = b.title', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = b.id', array());
        $select->joinLeft(array('stl' => 'store_staff_log'), 'stl.staff_id = b.id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = stl.store_id', array());
        $select->joinLeft(array('sub' => $select_sellout), 'sub.pg_id = b.id', array('total_imei' => 'sub.total_imei'));
        $select->joinLeft(array('su' => $select_hero), 'su.pg_id = b.id', array());
        $select->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = b.id', array());

        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('c.area_id = ?', $params['area_id']);
        }
        if (isset($params['province_id']) and $params['province_id']) {
            $select->where('c.id = ?', $params['province_id']);
        }
        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('d.id IN (?)', $params['area_list']);
        }
        $select->where('a.YearNo = ?', $params['y']);
        $select->where('a.MonthNo = ?', $params['m']);


        if (!empty($params['seniority'])) {
            $select->where('v.type_date = ?', $params['seniority']);
        }
        if (!empty($params['name'])) {
            $select->where('CONCAT(b.firstname," ",b.lastname) LIKE ?', '%' . $params['name'] . '%');
        }
        if (!empty($params['email'])) {
            $select->where('b.email LIKE ?', '%' . $params['email'] . '%');
        }
        if (!empty($params['code'])) {
            $select->where('b.code = ?', $params['code']);
        }



        if (!empty($params['title'])) {
            $select->where('tt.id = ?', $params['title']);
        }


        if (!empty($params['shop'])) {
            $select->where('st.name LIKE ?', '%' . $params['shop'] . '%');
        }
        if (!empty($params['star'])) {
            $select->where('sp.result = ?', $params['star']);
        }
        if (!empty($params['from_date']) && !empty($params['to_date'])) {
            $select->where('b.joined_at >= ?', $params['from_date']);

            $select->where('b.joined_at <= ?', $params['to_date']);
        }
        if (!empty($params['from_date']) && empty($params['to_date'])) {
            $select->where('b.joined_at =  ?', $params['from_date']);
        }
        if (!empty($params['to_date'] && empty($params['from_date']))) {
            $select->where('b.joined_at =  ?', $params['to_date']);
        }




        if (!empty($params['sellout_from']) && !empty($params['sellout_to'])) {
            $select->where('sub.total_imei >= ?', $params['sellout_from']);

            $select->where('sub.total_imei <= ?', $params['sellout_to']);
        }
        if (!empty($params['sellout_from']) && empty($params['sellout_to'])) {
            $select->where('sub.total_imei=  ?', $params['sellout_from']);
        }
        if (!empty($params['sellout_to'] && empty($params['sellout_from']))) {
            $select->where('sub.total_imei =  ?', $params['sellout_to']);
        }
        $select->group(array("b.id", "a.YearNo", "a.MonthNo"));
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getListChannel($page, $limit, &$total, $params) {
        $db           = Zend_Registry::get('db');
        $select       = $db->select();
        
        $arrHero      = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            "p.code", 
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)", 
            "store_name" => "s.name", 
            "team_name" => "t.name", 
            "title_name" => "t2.name",
            "p.joined_at", 
            "p.phone_number", 
            "total_score" => "sp.total", 
            "total_star" => "sp.result", 
            "total_so" => "SUM(kps.qty)", 
            "total_hero" => "SUM(IF(kpm.good_id = 906, kps.qty, 0))"
        );

        $select->from(array('p' => 'staff'), $arrHero);
        $select->joinInner(array('l' => 'store_staff_log'), 'l.staff_id = p.id', []);
        $select->joinLeft(array('t' => 'team'), 't.id = p.team', []);
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.title', []);
        $select->joinLeft(array('s' => 'store'), 's.id = l.store_id', []);
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = p.id', []);
        $select->joinLeft(array('kps' => 'kpi_by_staff'), 'kps.pg_id = p.id', []);
        $select->joinLeft(array('kpm' => 'kpi_by_model'), 'kpm.id = kps.kpi_by_model_id', []);
        $select->joinLeft(array('d' => 'v_channel_dealer'), 'd.d_id = s.d_id', []);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', []);

        $select->where('l.released_at IS NULL');
        $select->where("kpm.timing_date >=  DATE_FORMAT(NOW() ,'%Y-05-01')");
        $select->where('p.title IN (?)', array(PGPB_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE));
        $select->where('d.channel_id = ?', $params['channel']);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        if (isset($params['area_id']) and $params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if (isset($params['province_id']) and $params['province_id']) {
            $select->where('r.id = ?', $params['province_id']);
        }

        if (isset($params['district']) and $params['district']) {
            $select->where('s.district IN (?)', $params['district']);
        }

        /*
        if (!empty($params['seniority'])) {
            $select_final->where('v.type_date = ?', $params['seniority']);
        }
        */

        if (!empty($params['name'])) {
            $select->where('CONCAT(p.firstname," ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
        }

        if (!empty($params['email'])) {
            $select->where('p.email LIKE ?', '%' . $params['email'] . '%');
        }

        if (!empty($params['code'])) {
            $select->where('p.code = ?', $params['code']);
        }

        if (!empty($params['star'])) {
            $select->where('sp.result = ?', $params['star']);
        }

        if (!empty($params['shop'])) {
            $select->where('s.name LIKE ?', '%' . $params['shop'] . '%');
        }


        if (!empty($params['title'])) {
            $select->where('t2.id = ?', $params['title']);
        }

        if (!empty($params['from_date']) && !empty($params['to_date'])) {
            $select->where('s.joined_at >=  ?', $params['from_date']);
            $select->where('s.joined_at <=  ?', $params['to_date']);
        }


        if (!empty($params['to_date'] && empty($params['from_date']))) {
            $select->where('s.joined_at =  ?', $params['to_date']);
        }

        if (!empty($params['sellout_from']) && !empty($params['sellout_to'])) {
            $select->having('SUM(kps.qty) > ?', $params['sellout_from']);

            $select->where('SUM(kps.qty) <= ?', $params['sellout_to']);
        }

        $select->group('p.id');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getThamnien($params, &$total_thamnien) {

        $db      = Zend_Registry::get("db");
        $select  = $db->select();
        $arrCols = array(
            'type_date_name' => '(CASE
                            WHEN p.joined_at >= DATE_SUB(NOW(),INTERVAL 1 YEAR) THEN "Dưới 1 năm"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 2 YEAR) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 1 YEAR) ) THEN "1 đến 2 năm"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 3 YEAR) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 2 YEAR) ) THEN "2 đến 3 năm"
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 4 YEAR) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 3 YEAR) ) THEN "3 đến 4 năm"
                            ELSE "Trên 4 năm"
                            END
                            )',
            'type_date'      => '(CASE
                            WHEN p.joined_at >= DATE_SUB(NOW(),INTERVAL 1 YEAR) THEN 1
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 2 YEAR) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 1 YEAR) ) THEN 2
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 3 YEAR) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 2 YEAR) ) THEN 3
                            WHEN ( p.joined_at >= DATE_SUB(NOW(),INTERVAL 4 YEAR) AND p.joined_at < DATE_SUB(NOW(),INTERVAL 3 YEAR) ) THEN 4
                            ELSE 5
                            END
                            )',
            'quantity'       => "COUNT(DISTINCT p.id)"
        );
        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.off_date IS NULL', NULL);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        $select->group('type_date_name');
        $select->order('type_date');
        $result         = $db->fetchAll($select);
        $total_thamnien = 0;
        foreach ($result as $key => $value) {
            $total_thamnien += $value['quantity'];
        }
        return $result;
    }

    public function getDataBrandshop($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'store_id'  => 'p.store_id',
            'staff_id'  => 'p.staff_id',
            'is_leader' => 'p.is_leader',
            'area_id'   => 'r.area_id',
        );
        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->where('p.released_at IS NULL', NULL);
        $select->where('s.is_brand_shop = ?', 1);
        $select->where('p.is_leader = ?', 0);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListBrandshop($page, $limit, &$total, $params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'store_id'  => 'p.store_id',
            'staff_id'  => 'p.staff_id',
            'is_leader' => 'p.is_leader',
            'area_id'   => 'r.area_id',
            'code'      => 'sf.code',
            'email'     => 'sf.email',
            'team'      => 't.name',
            'title'     => 'tt.name',
            'shop'      => 'st.name',
            'total'     => 'sp.total',
            'star'      => 'sp.result',
            'joined'    => 'sf.joined_at',
            'phone'     => 'sf.phone_number',
            'status'    => 'sf.status',
            'fullname'  => "CONCAT(sf.firstname, ' ', sf.lastname)",
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = p.staff_id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = p.store_id', array());
        $select->joinLeft(array('sf' => 'staff'), 'sf.id = p.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = sf.team', array());
        $select->joinLeft(array('tt' => 'team'), 'tt.id = sf.title', array());
        $select->where('p.released_at IS NULL', NULL);
        $select->where('s.is_brand_shop = ?', 1);
        $select->where('p.is_leader = ?', 0);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        //search
        if (!empty($params['name'])) {
            $select->where('sf.lastname LIKE ?', '%' . $params['name'] . '%');
        }
        if (!empty($params['email'])) {
            $select->where('sf.email LIKE ?', '%' . $params['email'] . '%');
        }
        if (!empty($params['code'])) {
            $select->where('sf.code = ?', $params['code']);
        }
        if (!empty($params['star'])) {
            $select->where('sp.result = ?', $params['star']);
        }
        if (!empty($params['shop'])) {
            $select->where('st.name LIKE ?', '%' . $params['name'] . '%');
        }

        if (!empty($params['title'])) {
            $select->where('tt.id = ?', $params['title']);
        }

        if (!empty($params['from_date']) && !empty($params['to_date'])) {
            $select->where('f.joined_at >=  ?', $params['from_date']);
            $select->where('f.joined_at <=  ?', $params['to_date']);
        }
        if (!empty($params['from_date']) && empty($params['to_date'])) {
            $select->where('f.joined_at =  ?', $params['from_date']);
        }
        if (!empty($params['to_date'] && empty($params['from_date']))) {
            $select->where('f.joined_at =  ?', $params['to_date']);
        }

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getLessonListArea($params) {
        
        $list_sale_psg = unserialize(LIST_PGS_BI);

        $db      = Zend_Registry::get("db");
        $select  = $db->select();
        $arrCols = array(
            'area_id'    => 'r.area_id',
            'staff_Cmnd' => 's.ID_number',
            'lesson_id'  => 'p.lesson_id',
            'fail'       => '(CASE
                            WHEN (max(p.scores) = 10) THEN
                                0
                            ELSE
                                1
                            END)',
            'pass'       => '(CASE
                            WHEN (max(p.scores) = 10) THEN
                                1
                            ELSE
                                0
                            END)'
        );
        $select->from(array('s' => 'staff'), $arrCols);
        $select->joinLeft(array('p' => 'lesson_scores'), 'p.staff_cmnd = s.ID_number AND p.lesson_id =' . $params['lesson_id'] . '', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->where('s.title IN (?)', $params['title']);
        
        if(isset($params['list_title_staff']) and $params['list_title_staff']){
            $select->where('s.title IN (?)', $params['list_title_staff']);
        }
        
        $select->where('s.off_date IS NULL');
        $select->where('s.status = ?', 1);
        $select->group('s.id');


        $select_main  = $db->select();
        $arrCols_main = array(
            'lesson_id' => 'l.id',
            'area_id'   => 'p.area_id',
            'area'      => 'a.name',
            'title'     => 'l.title',
            'total'     => "COUNT(staff_cmnd)",
            'pass'      => "SUM(pass)",
            'fail'      => "SUM(fail)"
        );
        $select_main->from(array('p' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->joinLeft(array('l' => 'lesson'), 'l.id = ' . $params['lesson_id'] . '', array());
        $select_main->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());
        
        if(isset($params['area_list']) and $params['area_list']){
            $select_main->where('p.area_id IN (?)', $params['area_list']);
        }
        
        if(!empty($params['region_id'])){
            $select_main->where('a.bigarea_id = ?', $params['region_id']);
        }
        $select_main->group('p.area_id');
        $result       = $db->fetchAll($select_main);

        if($_GET['zzz'] == 9){
            echo $select_main;exit;
        }

        return $result;
    }

    public function getLessonListRegion($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'area_id'    => 'r.area_id',
            'staff_Cmnd' => 's.ID_number',
            'lesson_id'  => 'p.lesson_id',
            'fail'       => '(CASE
                            WHEN (max(p.scores) = 10) THEN
                                0
                            ELSE
                                1
                            END)',
            'pass'       => '(CASE
                            WHEN (max(p.scores) = 10) THEN
                                1
                            ELSE
                                0
                            END)'
        );
        $select->from(array('s' => 'staff'), $arrCols);
        $select->joinLeft(array('p' => 'lesson_scores'), 'p.staff_cmnd = s.ID_number and p.lesson_id = ' . $params['lesson_id'] . '', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        
        $select->where('s.title IN (?)', $params['title']);
        $select->where('s.off_date IS NULL');
        $select->where('s.status = ?', 1);
        
        if(isset($params['list_title_staff']) and $params['list_title_staff']){
            $select->where('s.title IN (?)', $params['list_title_staff']);
        }
        
        $select->group('s.id');

        $select_main  = $db->select();
        $arrCols_main = array(
            'lesson_id' => 'p.lesson_id',
            'area_id'   => 'p.area_id',
            'area'      => 'a.name',
            'title'     => 'l.title',
            'total'     => "COUNT(staff_cmnd)",
            'pass'      => "SUM(pass)",
            'fail'      => "SUM(fail)"
        );
        $select_main->from(array('p' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->joinLeft(array('l' => 'lesson'), 'l.id = p.lesson_id', array());

        $select_main->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());
        
        if(isset($params['area_list']) and $params['area_list']){
            $select_main->where('p.area_id IN (?)', $params['area_list']);
        }

        
        $select_main->group('p.area_id');

        $select_final  = $db->select();
        $arrCols_final = array(
            'lesson_id'   => 'f.lesson_id',
            'region_id'   => 'b.id',
            'region_name' => 'b.name',
            'fail'        => "SUM(f.fail)",
            'pass'        => "SUM(f.pass)",
            'total'       => "SUM(f.total)",
            'title'       => 'f.title'
        );
        $select_final->from(array('f' => new Zend_Db_Expr('(' . $select_main . ')')), $arrCols_final);
        $select_final->joinLeft(array('a' => 'area'), 'a.id = f.area_id', array());
        $select_final->joinLeft(array('b' => 'bigarea'), 'a.bigarea_id = b.id', array());
        $select_final->group('b.id');
        $select_final->order('b.sort');

        $result        = $db->fetchAll($select_final);

        if($_GET['zzz'] == 9){
            echo $select_final;exit;
        }

        return $result;
    }

    public function getAreaTrainerReport($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id'              => "p.id",
            'name'            => "p.name",
            'quantity_pg'     => "SUM(t.quantity_trainees)",
            'quantity_course' => "SUM(t.quantity_session)",
        );

        $select->from(array('p' => 'area'), $arrCols);
        $select->joinLeft(array('a' => 'bigarea'), 'a.id = p.bigarea_id', array());
        $select->joinLeft(array('t' => 'staff_training_report'), 't.area = p.id AND (t.del IS NULL OR t.del = 0) AND MONTH(t.date) =' . $params['month'] . ' AND YEAR(t.date) =' . $params['year'] . ' AND t.type =' . $params['type'] . '', array());
        if (!empty($params['area_list'])) {
            $select->where('p.id IN (?)', $params['area_list']);
        }
        if (isset($params['id']) and $params['id']) {
            $select->where('a.id = ?', $params['id']);
        }
        $select->group(['p.id']);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListAreaCourse($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id'              => "p.id",
            'name'            => "p.name",
            'quantity_course' => "COUNT(DISTINCT t.id)",
            'quantity_pg'     => "SUM(IF(e.id IS NULL,0,1))",
            'quantity_pass'   => "SUM(IF(e.id IS NOT NULL AND e.result = 1,1,0))",
        );

        $select->from(array('p' => 'area'), $arrCols);
        $select->joinLeft(array('a' => 'bigarea'), 'a.id = p.bigarea_id', array());
        $select->joinLeft(array('t' => 'trainer_course'), 't.area_id = p.id AND (t.del IS NULL OR t.del = 0) AND MONTH(t.from_date) = ' . $params['month'] . ' AND YEAR(t.from_date) =' . $params['year'] . '', array());
        $select->joinLeft(array('e' => 'trainer_course_detail'), 'e.course_id = t.id AND (e.del = 0 OR e.del IS NULL)', array());

        if (!empty($params['area_list'])) {
            $select->where('p.id IN (?)', $params['area_list']);
        }
        if (!empty($params['id'])) {
            $select->where('a.id = ?', $params['id']);
        }

        $select->group('p.id');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getBigareaCourse($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id'              => "p.id",
            'name'            => "p.name",
            'quantity_course' => "COUNT(DISTINCT t.id)",
            'quantity_pg'     => "SUM(IF(e.id IS NULL,0,1))",
            'quantity_pass'   => "SUM(IF(e.id IS NOT NULL AND e.result = 1,1,0))",
        );

        $select->from(array('p' => 'bigarea'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.bigarea_id = p.id', array());
        $select->joinLeft(array('t' => 'trainer_course'), 't.area_id = a.id', array());
        $select->joinLeft(array('e' => 'trainer_course_detail'), 'e.course_id = t.id AND (e.del = 0 OR e.del IS NULL)', array());

        $select->where('t.del IS NULL OR t.del = ?', 0);

        if (!empty($params['area_list'])) {
            $select->where('a.id IN (?)', $params['area_list']);
        }

        if (isset($params['month']) and $params['month']) {
            $select->where('MONTH(t.from_date) = ?', $params['month']);
        }

        if (isset($params['year']) and $params['year']) {
            $select->where('YEAR(t.from_date) = ?', $params['year']);
        }


        $select->group('p.id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getBigareaTrainingReport($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id'              => "p.id",
            'name'            => "p.name",
            'quantity_pg'     => "SUM(t.quantity_trainees)",
            'quantity_course' => "SUM(t.quantity_session)",
        );

        $select->from(array('p' => 'bigarea'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.bigarea_id = p.id', array());
        $select->joinLeft(array('t' => 'staff_training_report'), 't.area = a.id', array());

        $select->where('t.del IS NULL OR t.del = ?', 0);

        if (!empty($params['area_list'])) {
            $select->where('a.id IN (?)', $params['area_list']);
        }

        if (isset($params['month']) and $params['month']) {
            $select->where('MONTH(t.date) = ?', $params['month']);
        }

        if (isset($params['year']) and $params['year']) {
            $select->where('YEAR(t.date) = ?', $params['year']);
        }
        if (isset($params['type']) and $params['type']) {
            $select->where('t.type = ?', $params['type']);
        }
        $select->group(['p.id', 't.type']);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getRegionDealerTrainingReport($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'name_dealer'     => "IFNULL(l.name,'Others')",
            'id'              => "p.id",
            'name'            => "p.name",
            'quantity_pg'     => "SUM(t.quantity_trainees)",
            'quantity_course' => "SUM(t.quantity_session)",
        );

        $select->from(array('p' => 'bigarea'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.bigarea_id = p.id', array());
        $select->joinLeft(array('t' => 'staff_training_report'), 't.area = a.id', array());
        $select->joinLeft(array('l' => 'trainer_dealer'), 'l.id = t.trainer_dealer', array());
        $select->where('t.del IS NULL OR t.del = ?', 0);

        if (!empty($params['area_list'])) {
            $select->where('a.id IN (?)', $params['area_list']);
        }
        if (!empty($params['region'])) {
            $select->where('p.id = ?', $params['region']);
        }
        if (isset($params['month']) and $params['month']) {
            $select->where('MONTH(t.date) = ?', $params['month']);
        }

        if (isset($params['year']) and $params['year']) {
            $select->where('YEAR(t.date) = ?', $params['year']);
        }
        if (isset($params['type']) and $params['type']) {
            $select->where('t.type = ?', $params['type']);
        }
        $select->group(['p.id', 't.type', 'l.id']);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAreaCourse($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id'              => "p.id",
            'name'            => "a.name",
            'quantity_course' => "COUNT(DISTINCT t.id)",
            'quantity_pg'     => "SUM(IF(e.id IS NULL,0,1))",
            'quantity_pass'   => "SUM(IF(e.id IS NOT NULL AND e.result = 1,1,0))",
        );

        $select->from(array('p' => 'bigarea'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.bigarea_id = p.id', array());
        $select->joinLeft(array('t' => 'trainer_course'), 't.area_id = a.id', array());
        $select->joinLeft(array('e' => 'trainer_course_detail'), 'e.course_id = t.id AND (e.del = 0 OR e.del IS NULL)', array());

        $select->where('t.del IS NULL OR t.del = ?', 0);

        if (!empty($params['area_list'])) {
            $select->where('a.id IN (?)', $params['area_list']);
        }

        if (isset($params['month']) and $params['month']) {
            $select->where('MONTH(t.from_date) = ?', $params['month']);
        }

        if (isset($params['year']) and $params['year']) {
            $select->where('YEAR(t.from_date) = ?', $params['year']);
        }

        if (isset($params['bigarea_id']) and $params['bigarea_id']) {
            $select->where('p.id = ?', $params['bigarea_id']);
        }


        $select->group('a.id');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getAreaDealerTrainingReport($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'name_dealer'     => "IFNULL(l.name,'Others')",
            'id'              => "p.id",
            'name'            => "p.name",
            'quantity_pg'     => "SUM(t.quantity_trainees)",
            'quantity_course' => "SUM(t.quantity_session)",
        );

        $select->from(array('p' => 'area'), $arrCols);
        $select->joinLeft(array('a' => 'bigarea'), 'a.id = p.bigarea_id', array());
        $select->joinLeft(array('t' => 'staff_training_report'), 't.area = p.id AND (t.del IS NULL OR t.del = 0) AND MONTH(t.date) =' . $params['month'] . ' AND YEAR(t.date) =' . $params['year'] . ' AND t.type =' . $params['type'] . '', array());
        $select->joinLeft(array('l' => 'trainer_dealer'), 'l.id = t.trainer_dealer', array());
        if (!empty($params['area'])) {
            $select->where('p.id = ?', $params['area']);
        }
        // if(!empty($params['area_list'])){
        //     $select->where('p.id IN (?)', $params['area_list']);
        // }
        if (isset($params['id']) and $params['id']) {
            $select->where('a.id = ?', $params['id']);
        }
        $select->group(['p.id', 'l.id']);
        $result = $db->fetchAll($select);
        // echo $select; exit;
        return $result;
    }

    public function goodByKa($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'good_id' => "p.good_id",
            'sellout' => "SUM(p.qty)",
            'date'    => "DATE(p.timing_date)"
        );

        $select->from(array('p' => 'kpi_by_model'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('dk' => 'channel_dealer_ka'), 'dk.d_id = IF(d.parent = 0, d.id, d.parent)', array());
        $select->where('timing_date >= ?', $params['from']. ' 00:00:00');
        $select->where('timing_date <= ?', $params['to']. ' 23:59:59');
        $select->where('dk.channel_id = ?', $params['channel']);

        $select->group(['p.good_id', 'DATE(p.timing_date)']);

        $result = $db->fetchAll($select);
        
        return $result;
    }

    public function goodByKaTop($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'good_id' => "p.good_id",
            'sellout' => "SUM(p.qty)",
        );

        $select->from(array('p' => 'kpi_by_model'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('dk' => 'channel_dealer_ka'), 'dk.d_id = IF(d.parent = 0, d.id, d.parent)', array());
        $select->where('timing_date >= ?', $params['from']. ' 00:00:00');
        $select->where('timing_date <= ?', $params['to']. ' 23:59:59');
        $select->where('dk.channel_id IN (?)', $params['channel']);

        $select->group('p.good_id');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function goodByKaMonth($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $from = date('Y-m-01', strtotime($params['to'] . ' -6 month'));

        $arrCols = array(
            'good_id'           => "p.good_id",
            'sellout'           => "SUM(p.qty)",
            'month'             => "MONTH(timing_date)",
            'year'              => "YEAR(timing_date)",
            'sellout_value'     => "SUM(p.qty*p.value*8/10)",
            'sellout_value_100' => "SUM(p.qty*p.value)",
            'store_so'          => "COUNT( DISTINCT p.store_id)"
        );

        $select->from(array('p' => 'kpi_by_model'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('dk' => 'channel_dealer_ka'), 'dk.d_id = IF(d.parent = 0, d.id, d.parent)', array());
        $select->where('timing_date >= ?', $from. ' 00:00:00');
        $select->where('timing_date <= ?', $params['to']. ' 23:59:59');
        $select->where('dk.channel_id = ?', $params['channel']);

        $select->group(['MONTH(timing_date)']);
        
        $select->order(['YEAR(timing_date) ASC', 'MONTH(timing_date) ASC']);
        
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function goodChannelByMonth($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $from = date('Y-m-01', strtotime($params['to'] . ' -6 month'));

        $arrCols = array(
            'good_id'       => "p.good_id",
            'sellout'       => "COUNT( DISTINCT p.imei_sn)",
            'month'         => "MONTH(timing_date)",
            'sellout_value' => "SUM(p.value)*8/10",
            'store_so'      => "COUNT( DISTINCT p.store_id)",
            'channel'       => "IF(d.parent = 0, d.id, d.parent)"
        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.distributor_id', array());
        $select->where('timing_date >= ?', $from);
        $select->where('timing_date <= ?', $params['to']);
        $select->where('IF(d.parent = 0, d.id, d.parent) IN (?)', $params['channel']);

        $select->group(['p.good_id', 'MONTH(timing_date)', "IF(d.parent = 0, d.id, d.parent)"]);

        $result = $db->fetchAll($select);

        $data = [];

        foreach ($result as $key => $value) {
            $data[$value['good_id']][$value['channel']][$value['month']]['sellout']       = $value['sellout'];
            $data[$value['good_id']][$value['channel']][$value['month']]['sellout_value'] = $value['sellout_value'];
        }

        return $data;
    }

    public function getSelloutByDealerKa($params) {

        if (empty($params['from'])) {
            return false;
        }

        if (empty($params['to'])) {
            return false;
        }

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'month_date'  => 'MONTH(p.timing_date)',
            'channel'     => 'IF(d.parent = 0, d.id, d.parent)',
            'num'         => 'COUNT(DISTINCT p.imei_sn)',
            'total_value' => 'SUM(p.value*8/10)'
        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->where('DATE(p.timing_date) >= ?', $params['from']);
        $select->where('DATE(p.timing_date) <= ?', $params['to']);

        if (!empty($params['channel'])) {
            $select->where('IF(d.parent = 0, d.id, d.parent) IN (?)', $params['channel']);
        }

        $select->group(['IF(d.parent = 0, d.id, d.parent)', 'MONTH(p.timing_date)']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function countPgsKaByMonth($params) {
        if (empty($params['to'])) {
            return false;
        }

        $from = date('Y-m-01', strtotime($params['to'] . ' -6 month'));

        $sub_select = "
        select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
          select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
          (select 0 as a union all select 1 union all select 2 union all select 3
           union all select 4 union all select 5 union all select 6 union all
           select 7 union all select 8 union all select 9) a, /*10 day range*/
          (select 0 as a union all select 1 union all select 2 union all select 3
           union all select 4 union all select 5 union all select 6 union all
           select 7 union all select 8 union all select 9) b, /*100 day range*/
          (select 0 as a union all select 1 union all select 2 union all select 3
           union all select 4 union all select 5 union all select 6 union all
           select 7 union all select 8 union all select 9) c, /*1000 day range*/
          (select 0 as a union all select 1 union all select 2 union all select 3
           union all select 4 union all select 5 union all select 6 union all
           select 7 union all select 8 union all select 9) d, /*10000 day range*/
          (select  @minDate :=  CONCAT('" . $from . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
        ) f
        where aDate between @minDate and @maxDate
        GROUP BY YEAR(aDate), MONTH(aDate)
        ";

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'year'      => 'a.YearNo',
            'month'     => 'a.MonthNo',
            'count_pgs' => 'SUM(CASE WHEN 
                            ( FROM_UNIXTIME(p.joined_at) <= a.ToDate AND (p.released_at IS NULL OR FROM_UNIXTIME(p.released_at) >= a.FromDate) )  
                            THEN (DATEDIFF((LEAST(IFNULL(FROM_UNIXTIME(p.released_at), a.ToDate), a.ToDate)), (GREATEST(FROM_UNIXTIME(p.joined_at), a.FromDate))) + 1)
                            ELSE 0 END/(DATEDIFF(a.ToDate, a.FromDate)+1))',
            'channel'   => 'dk.channel_id',
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('dk' => 'channel_dealer_ka'), 'dk.d_id = IF(d.parent = 0, d.id, d.parent)', array());
        $select->joinCross(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), NULL, NULl);
        $select->where('p.is_leader = 0', NULL);

        if (!empty($params['channel'])) {
            $select->where('dk.channel_id IN (?)', $params['channel']);
        }

        $select->group(['dk.channel_id', 'a.MonthNo', 'a.YearNo']);

        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['year']][$value['month']][$value['channel']] = $value['count_pgs'];
        }
        return $data;
    }

    public function getSelloutMonthStore($list_store) {
      
        exit();
        if (count($list_store) > 10) {
            return false;
        }
       
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'store_id'   => "p.store_id",
            'month'      => "MONTH(p.timing_date)",
            'total_imei' => "COUNT( DISTINCT p.imei_sn)",
        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);
        if (!empty($list_store)) {
            $select->where('p.store_id IN (?)', $list_store);
        }
        $select->where("DATE(p.timing_date) >= DATE_FORMAT( CURRENT_DATE - INTERVAL 6 MONTH, '%Y-%m-01' )", NULL);

        $select->group(['p.store_id', 'MONTH(p.timing_date)']);

        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['month']][$value['store_id']] = $value['total_imei'];
        }

        return $data;
    }
    
    public function getKpiStoreLeader($staff_id){
        
        $from = date('Y-m-01 00:00:00');
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.imei_sn", 
            "p.timing_date", 
            "p.store_leader_id", 
            "p.good_id", 
            "number" => "COUNT(DISTINCT p.imei_sn)", 
            "max_date" => "MAX(p.timing_date)", 
            "min_date" => "MIN(p.timing_date)", 
            "good_name" => "g.desc",
            "kpi" => "p.kpi_store_leader",
            "total" => "SUM(kpi_store_leader)",
        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);
        $select->joinLeft(array('g' => WAREHOUSE_DB.'.good'), 'g.id = p.good_id', array());
        $select->where('p.store_leader_id = ?', $staff_id);
        $select->where('p.timing_date >= ?', $from);
        
        $select->group('p.good_id');

        $result = $db->fetchAll($select);

        return $result;
    }

     public function getListRegionSellout($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'regional_id'   => 'a.id',
            'regional' => 'a.name',
            'quantity'     => "COUNT(p.CODE)",
            'month'         => 'p.month',
            'year'          => 'p.year'
        );

        $select->from(array('p' => DATABASE_SALARY.'.v_list_sellout_pg'), $arrCols);
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = p.khuvuc', array());
         $select->joinLeft(array('a' => 'bigarea'), 'a.id = b.bigarea_id', array());
       
       
        $select->where('p.month = ?', $params['m']);
        $select->where('p.year = ?', $params['y']);

        // if($params['type_sell']== 0){
        //     $select->where('p.sellout < 1');
        // }
        // if(!empty($params['type_sell']) && $params['type_sell']==1){
        //     $select->where('p.sellout >= 1 AND p.sellout <= 9');
        // }
        // if(!empty($params['type_sell']) && $params['type_sell']==2){
        //     $select->where('p.sellout >= 10 AND p.sellout <= 19');
        // }
        // if(!empty($params['type_sell']) && $params['type_sell']==3){
        //     $select->where('p.sellout >= 20 AND p.sellout <= 29');
        // }
        // if(!empty($params['type_sell']) && $params['type_sell']==4){
        //     $select->where('p.sellout >= 30 AND p.sellout <= 39');
        // }
        // if(!empty($params['type_sell']) && $params['type_sell']==5){
        //     $select->where('p.sellout >= 40 AND p.sellout <= 49');
        // }
        // if(!empty($params['type_sell']) && $params['type_sell']==6){
        //     $select->where('p.sellout >= 50 ');
        // }


        if($params['type_sell']== 0){
            $select->where('p.sellout >= 0 AND p.sellout < 30');

        }
        if(!empty($params['type_sell']) && $params['type_sell']==1){
            $select->where('p.sellout >= 30 AND p.sellout < 50');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==2){
            $select->where('p.sellout >= 50 AND p.sellout < 100');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==3){
            $select->where('p.sellout >= 100');
        }
        
        
       if(!empty($params['area_list']))
        {
            $select->where('b.area_id IN (?)', $params['area_list']);
        }
        $select->group('a.id');
        // echo $select->__toString();exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListAreaSellout($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'area_id'   => 'p.khuvuc',
            'area_name' => 'r.name',
            'quantity'     => "COUNT(p.CODE)",
            'month'         => 'p.month',
            'year'          => 'p.year'
        );

        $select->from(array('p' => DATABASE_SALARY.'.v_list_sellout_pg'), $arrCols);
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = p.khuvuc', array());
         $select->joinLeft(array('a' => 'bigarea'), 'a.id = b.bigarea_id', array());
         $select->joinLeft(array('r' => 'area'), 'r.id = p.khuvuc', array());
       
       
        $select->where('p.month = ?', $params['m']);
        $select->where('p.year = ?', $params['y']);
         $select->where('a.id = ?', $params['regional']);

        if($params['type_sell']== 0){
            $select->where('p.sellout < 1');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==1){
            $select->where('p.sellout >= 1 AND p.sellout <= 9');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==2){
            $select->where('p.sellout >= 10 AND p.sellout <= 19');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==3){
            $select->where('p.sellout >= 20 AND p.sellout <= 29');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==4){
            $select->where('p.sellout >= 30 AND p.sellout <= 39');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==5){
            $select->where('p.sellout >= 40 AND p.sellout <= 49');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==6){
            $select->where('p.sellout >= 50 ');
        }
        
        if(!empty($params['area_list'])){
            $select->where('p.khuvuc IN (?)',$params['area_list']);
        }
        $select->group('p.khuvuc');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListStaffSellout($page, $limit, &$total, $params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
             new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.staff_id'),
            'area_id'   => 'p.khuvuc',
            'name' => 'p.fullname',
            'code'  => 'p.code',
            'department'    => 'p.department',
            'team'          => 'p.team',
            'title'          => 'p.title',
            'phone'         => 'p.phone',
            'email'         => 'p.email',
            'month'         => 'p.month',
            'year'          => 'p.year'
        );

        $select->from(array('p' => DATABASE_SALARY.'.v_list_sellout_pg'), $arrCols);
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = p.khuvuc', array());
         $select->joinLeft(array('a' => 'bigarea'), 'a.id = b.bigarea_id', array());
         $select->joinLeft(array('r' => 'area'), 'r.id = p.khuvuc', array());
       
       
        $select->where('p.month = ?', $params['m']);
        $select->where('p.year = ?', $params['y']);
         $select->where('a.id = ?', $params['regional']);
         $select->where('p.khuvuc = ?', $params['area_staff']);

        if($params['type_sell']== 0){
            $select->where('p.sellout < 1');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==1){
            $select->where('p.sellout >= 1 AND p.sellout <= 9');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==2){
            $select->where('p.sellout >= 10 AND p.sellout <= 19');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==3){
            $select->where('p.sellout >= 20 AND p.sellout <= 29');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==4){
            $select->where('p.sellout >= 30 AND p.sellout <= 39');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==5){
            $select->where('p.sellout >= 40 AND p.sellout <= 49');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==6){
            $select->where('p.sellout >= 50 ');
        } 
        $select->order('a.sort');
        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

     public function getRankStarRegion($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'regional'   => 'l.name',
            'region_id' => 'l.id',
            'quantity'     => "COUNT(DISTINCT p.staff_id)"
        );

        $select->from(array('p' => 'sp_assessment_temp'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.name = p.area', array());
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = a.id', array());
        $select->joinLeft(array('l' => 'bigarea'), 'l.id = b.bigarea_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.staff_id AND ((st.released_at IS NULL OR st.released_at > UNIX_TIMESTAMP()))', array());
        $select->joinLeft(array('t' => 'store'), 't.id = st.store_id', array());
        if(!empty($params['area_list']))
        {
            $select->where('a.id IN (?)', $params['area_list']);
        }
        $select->where('p.result = ?', $params['rank']);
        $select->group('b.bigarea_id');
        $select->order('l.sort');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getRankStarArea($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'area'   => 'a.name',
            'area_id' => 'a.id',
            'quantity'     => "COUNT(DISTINCT p.staff_id)"
        );

        $select->from(array('p' => 'sp_assessment_temp'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.name = p.area', array());
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = a.id', array());
        $select->joinLeft(array('l' => 'bigarea'), 'l.id = b.bigarea_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.staff_id AND ((st.released_at IS NULL OR st.released_at > UNIX_TIMESTAMP()))', array());
        $select->joinLeft(array('t' => 'store'), 't.id = st.store_id', array());

        $select->where('p.result = ?', $params['rank']);
        $select->where('l.id = ?', $params['regional']);
        
        if(!empty($params['area_list']))
         {
            $select->where('a.id IN (?)', $params['area_list']);
         }
        $select->group('a.id');
        $select->order('l.sort');
        $result = $db->fetchAll($select);

        return $result;
    }

     public function getRankStarStaff($page, $limit, &$total,$params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.staff_id'),
            'name'          => "CONCAT(p.firstname,' ',p.lastname)",
            'email'         => 'n.email',
            'phone'         => 'n.phone_number',
            'title'         => 'p.title',
            'team'          => 'ttt.name',
            'department'    => 'tt.name'
        );

        $select->from(array('p' => 'sp_assessment_temp'), $arrCols);
        $select->joinLeft(array('n' => 'staff'), 'n.id = p.staff_id', array());

        $select->joinLeft(array('tt' => 'team'), 'tt.id = n.department', array());
        $select->joinLeft(array('ttt' => 'team'), 'ttt.id = n.team', array());
        $select->joinLeft(array('a' => 'area'), 'a.name = p.area', array());
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = a.id', array());
        $select->joinLeft(array('l' => 'bigarea'), 'l.id = b.bigarea_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.staff_id AND ((st.released_at IS NULL OR st.released_at > UNIX_TIMESTAMP()))', array());
        $select->joinLeft(array('t' => 'store'), 't.id = st.store_id', array());

        $select->where('p.result = ?', $params['rank']);
        $select->where('l.id = ?', $params['regional']);
        $select->where('a.id = ?', $params['area']);
        $select->order('l.sort');
        
        $select->limitPage($page, $limit);
         $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }



public function getListPgRegion($params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select_sellout = $db->select();
        $arrSel         = array(
            'pg_id'      => 'i.pg_id',
            'total_imei' => "COUNT(DISTINCT i.imei_sn)"
        );
        $date_parent    = date('Y-m-01');

        $select_sellout->from(array('i' => 'imei_kpi'), $arrSel);
        $select_sellout->where('i.timing_date >= ?', $date_parent);
        $select_sellout->group('i.pg_id');

        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
           'regional'   => 'br.name',
           'region_id'  => 'br.id',
           'quantity'      => "COUNT(DISTINCT b.id)"
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate and b.title in (182,293,419)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = b.team', array());
        $select->joinLeft(array('tt' => 'team'), 'tt.id = b.title', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = b.id', array());
        $select->joinLeft(array('stl' => 'store_staff_log'), 'stl.staff_id = b.id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = stl.store_id', array());
        $select->joinLeft(array('sub' => $select_sellout), 'sub.pg_id = b.id', array());
        $select->joinLeft(array('su' => $select_hero), 'su.pg_id = b.id', array());
        $select->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = b.id', array());
        $select->joinLeft(array('ba' => 'bigarea_area'), 'ba.area_id = d.id', array());
        $select->joinLeft(array('br' => 'bigarea'), 'br.id = ba.bigarea_id', array());
        $select->where('a.MonthNo = ?', $params['m']);
         $select->where('a.YearNo = ?', $params['y']);
         if(!empty($params['area_list']))
         {
            $select->where('d.id IN (?)', $params['area_list']);
         }
        $select->group(array("br.id"));
        $select->order("br.sort");

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListPgArea($params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select_sellout = $db->select();
        $arrSel         = array(
            'pg_id'      => 'i.pg_id',
            'total_imei' => "COUNT(DISTINCT i.imei_sn)"
        );
        $date_parent    = date('Y-m-01');

        $select_sellout->from(array('i' => 'imei_kpi'), $arrSel);
        $select_sellout->where('i.timing_date >= ?', $date_parent);
        $select_sellout->group('i.pg_id');

        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
           'area_name'   => 'd.name',
           'area_id'  => 'd.id',
           'quantity'      => "COUNT(DISTINCT b.id)"
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate and b.title in (182,293,419)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = b.team', array());
        $select->joinLeft(array('tt' => 'team'), 'tt.id = b.title', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = b.id', array());
        $select->joinLeft(array('stl' => 'store_staff_log'), 'stl.staff_id = b.id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = stl.store_id', array());
        $select->joinLeft(array('sub' => $select_sellout), 'sub.pg_id = b.id', array());
        $select->joinLeft(array('su' => $select_hero), 'su.pg_id = b.id', array());
        $select->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = b.id', array());
        $select->joinLeft(array('ba' => 'bigarea_area'), 'ba.area_id = d.id', array());
        $select->joinLeft(array('br' => 'bigarea'), 'br.id = ba.bigarea_id', array());
        $select->where('br.id = ?', $params['regional']);
         $select->where('a.MonthNo = ?', $params['m']);
         $select->where('a.YearNo = ?', $params['y']);

         if(!empty($params['area_list']))
         {
            $select->where('d.id IN (?)', $params['area_list']);
         }
        $select->group(array("d.id"));
        $select->order("br.sort");

        $result = $db->fetchAll($select);

        return $result;
    }


     public function getListPgStaff($page, $limit, &$total,$params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select_sellout = $db->select();
        $arrSel         = array(
            'pg_id'      => 'i.pg_id',
            'total_imei' => "COUNT(DISTINCT i.imei_sn)"
        );
        $date_parent    = date('Y-m-01');

        $select_sellout->from(array('i' => 'imei_kpi'), $arrSel);
        $select_sellout->where('i.timing_date >= ?', $date_parent);
        $select_sellout->group('i.pg_id');

        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT b.id'),
             
             'name'             => "CONCAT(b.firstname,' ',b.lastname)",
             'email'            => 'b.email',
             'phone'            => 'b.phone_number',
             'team'             => 't.name',
             'title'            => 'tt.name',
             'department'       => 'ttt.name'   
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate and b.title in (182,293,419)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = b.team', array());
        $select->joinLeft(array('tt' => 'team'), 'tt.id = b.title', array());
        $select->joinLeft(array('ttt' => 'team'), 'ttt.id = b.department', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = b.id', array());
        $select->joinLeft(array('stl' => 'store_staff_log'), 'stl.staff_id = b.id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = stl.store_id', array());
        $select->joinLeft(array('sub' => $select_sellout), 'sub.pg_id = b.id', array());
        $select->joinLeft(array('su' => $select_hero), 'su.pg_id = b.id', array());
        $select->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = b.id', array());
        $select->joinLeft(array('ba' => 'bigarea_area'), 'ba.area_id = d.id', array());
        $select->joinLeft(array('br' => 'bigarea'), 'br.id = ba.bigarea_id', array());
         $select->where('a.MonthNo = ?', $params['m']);
         $select->where('a.YearNo = ?', $params['y']);
        $select->where('br.id = ?', $params['regional']);
        $select->where('d.id = ?', $params['area_staff']);
        
         $select->limitPage($page, $limit);
         $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getExportListRegionSellout($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'regional_id'   => 'a.id',
            'regional' => 'a.name',
            'fullname'  => 'p.fullname',
            'code'      => 'p.code',
            'department'    => 'p.department',
            'team'          => 'p.team',
            'title'         => 'p.title',
            'month'         => 'p.month',
            'year'          => 'p.year'
        );

        $select->from(array('p' => DATABASE_SALARY.'.v_list_sellout_pg'), $arrCols);
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = p.khuvuc', array());
         $select->joinLeft(array('a' => 'bigarea'), 'a.id = b.bigarea_id', array());
       
       
        $select->where('p.month = ?', $params['m']);
        $select->where('p.year = ?', $params['y']);

        if($params['type_sell']== 0){
            $select->where('p.sellout < 1');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==1){
            $select->where('p.sellout >= 1 AND p.sellout <= 9');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==2){
            $select->where('p.sellout >= 10 AND p.sellout <= 19');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==3){
            $select->where('p.sellout >= 20 AND p.sellout <= 29');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==4){
            $select->where('p.sellout >= 30 AND p.sellout <= 39');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==5){
            $select->where('p.sellout >= 40 AND p.sellout <= 49');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==6){
            $select->where('p.sellout >= 50 ');
        }
        
       if(!empty($params['area_list']))
        {
            $select->where('b.area_id IN (?)', $params['area_list']);
        }
       
        $result = $db->fetchAll($select);
        return $result;
    }


    public function getExportListAreaSellout($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'area_id'   => 'p.khuvuc',
            'area_name' => 'r.name',
            'fullname'     => "p.fullname", 
            'code'      => 'p.code',
            'department'    => 'p.department',
            'team'          => 'p.team',
            'title'         => 'p.title',
            'month'         => 'p.month',
            'year'          => 'p.year'
        );

        $select->from(array('p' => DATABASE_SALARY.'.v_list_sellout_pg'), $arrCols);
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = p.khuvuc', array());
         $select->joinLeft(array('a' => 'bigarea'), 'a.id = b.bigarea_id', array());
         $select->joinLeft(array('r' => 'area'), 'r.id = p.khuvuc', array());
       
       
        $select->where('p.month = ?', $params['m']);
        $select->where('p.year = ?', $params['y']);
         $select->where('a.id = ?', $params['regional']);

        if($params['type_sell']== 0){
            $select->where('p.sellout < 1');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==1){
            $select->where('p.sellout >= 1 AND p.sellout <= 9');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==2){
            $select->where('p.sellout >= 10 AND p.sellout <= 19');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==3){
            $select->where('p.sellout >= 20 AND p.sellout <= 29');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==4){
            $select->where('p.sellout >= 30 AND p.sellout <= 39');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==5){
            $select->where('p.sellout >= 40 AND p.sellout <= 49');
        }
        if(!empty($params['type_sell']) && $params['type_sell']==6){
            $select->where('p.sellout >= 50 ');
        }
        
        if(!empty($params['area_list'])){
            $select->where('p.khuvuc IN (?)',$params['area_list']);
        }
    
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getExportListPgRegion($params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select_sellout = $db->select();
        $arrSel         = array(
            'pg_id'      => 'i.pg_id',
            'total_imei' => "COUNT(DISTINCT i.imei_sn)"
        );
        $date_parent    = date('Y-m-01');

        $select_sellout->from(array('i' => 'imei_kpi'), $arrSel);
        $select_sellout->where('i.timing_date >= ?', $date_parent);
        $select_sellout->group('i.pg_id');

        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
            'staff_id'      => "DISTINCT (b.id)",
            'code'      => 'b.code',
           'regional'   => 'br.name',
           'region_id'  => 'br.id',
           'fullname'       => "CONCAT(b.firstname,' ',b.lastname)",
           'phone'      => 'b.phone_number',
           'email'      => 'b.email',
           'title'      => 'tt.name',
           'team'       => 't.name',
           'department' => 'ttt.name'
           
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate and b.title in (182,293,419)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = b.team', array());
        $select->joinLeft(array('tt' => 'team'), 'tt.id = b.title', array());
         $select->joinLeft(array('ttt' => 'team'), 'ttt.id = b.department', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = b.id', array());
        $select->joinLeft(array('stl' => 'store_staff_log'), 'stl.staff_id = b.id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = stl.store_id', array());
        $select->joinLeft(array('sub' => $select_sellout), 'sub.pg_id = b.id', array());
        $select->joinLeft(array('su' => $select_hero), 'su.pg_id = b.id', array());
        $select->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = b.id', array());
        $select->joinLeft(array('ba' => 'bigarea_area'), 'ba.area_id = d.id', array());
        $select->joinLeft(array('br' => 'bigarea'), 'br.id = ba.bigarea_id', array());
        $select->where('a.MonthNo = ?', $params['m']);
         $select->where('a.YearNo = ?', $params['y']);
         if(!empty($params['area_list']))
         {
            $select->where('d.id IN (?)', $params['area_list']);
         }
        $result = $db->fetchAll($select);

        return $result;
    }


    public function getExportListPgArea($params) {

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select_sellout = $db->select();
        $arrSel         = array(
            'pg_id'      => 'i.pg_id',
            'total_imei' => "COUNT(DISTINCT i.imei_sn)"
        );
        $date_parent    = date('Y-m-01');

        $select_sellout->from(array('i' => 'imei_kpi'), $arrSel);
        $select_sellout->where('i.timing_date >= ?', $date_parent);
        $select_sellout->group('i.pg_id');

        $select_hero = $db->select();
        $arrHero     = array(
            'pg_id' => 'i.pg_id',
            'hero'  => "COUNT(DISTINCT i.imei_sn)"
        );
        $select_hero->from(array('i' => 'imei_kpi'), $arrHero);
        $select_hero->where('i.timing_date >= ?', $date_parent);
        $select_hero->where('i.good_id = ?', HERO_ID);
        $select_hero->group('i.pg_id');
        $arrCols     = array(
            'staff_id'      => "DISTINCT (b.id)",
            'code'      => 'b.code',
           'area'   => 'd.name',
           'region_id'  => 'br.id',
           'fullname'       => "CONCAT(b.firstname,' ',b.lastname)",
           'phone'      => 'b.phone_number',
           'email'      => 'b.email',
           'title'      => 'tt.name',
           'team'       => 't.name',
           'department' => 'ttt.name'
        );

        $sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('" . $params['from'] . "'), @maxDate := CASE WHEN CONCAT('" . $params['to'] . "') <= DATE(NOW()) THEN CONCAT('" . $params['to'] . "') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);

        $select->joinInner(array('b' => 'staff'), 'b.joined_at <= a.ToDate and ifnull(b.off_date, DATE(NOW())) >= a.ToDate and b.title in (182,293,419)', array());

        $select->joinInner(array('c' => 'regional_market'), 'b.regional_market = c.id', array());

        $select->joinInner(array('d' => 'area'), 'c.area_id = d.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = b.team', array());
        $select->joinLeft(array('tt' => 'team'), 'tt.id = b.title', array());
         $select->joinLeft(array('ttt' => 'team'), 'ttt.id = b.department', array());
        $select->joinLeft(array('sp' => 'sp_assessment_temp'), 'sp.staff_id = b.id', array());
        $select->joinLeft(array('stl' => 'store_staff_log'), 'stl.staff_id = b.id', array());
        $select->joinLeft(array('st' => 'store'), 'st.id = stl.store_id', array());
        $select->joinLeft(array('sub' => $select_sellout), 'sub.pg_id = b.id', array());
        $select->joinLeft(array('su' => $select_hero), 'su.pg_id = b.id', array());
        $select->joinLeft(array('v' => 'v_seniority_pg'), 'v.id = b.id', array());
        $select->joinLeft(array('ba' => 'bigarea_area'), 'ba.area_id = d.id', array());
        $select->joinLeft(array('br' => 'bigarea'), 'br.id = ba.bigarea_id', array());
        $select->where('br.id = ?', $params['regional']);
         $select->where('a.MonthNo = ?', $params['m']);
         $select->where('a.YearNo = ?', $params['y']);

         if(!empty($params['area_list']))
         {
            $select->where('d.id IN (?)', $params['area_list']);
         }
       

        $result = $db->fetchAll($select);
       
        return $result;
    }

     public function getExportRankStarRegion($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'staff_id'  => "DISTINCT(p.staff_id)",
            'regional'   => 'l.name',
            'region_id' => 'l.id',
            'fullname'  => "CONCAT(p.firstname,' ',p.lastname)",
            'code'      => 'p.staff_code',
            'title'     => 'p.title',
            'team'         => 'tt.name',
            'department'    => 'ttt.name',
            'email'         => 's.email',
            'phone'         => 's.phone_number'

        );

        $select->from(array('p' => 'sp_assessment_temp'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.name = p.area', array());
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = a.id', array());
        $select->joinLeft(array('l' => 'bigarea'), 'l.id = b.bigarea_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.staff_id AND ((st.released_at IS NULL OR st.released_at > UNIX_TIMESTAMP()))', array());
         $select->joinLeft(array('tt' => 'team'), 'tt.id = s.team', array());
         $select->joinLeft(array('ttt' => 'team'), 'ttt.id = s.department', array());
        $select->joinLeft(array('t' => 'store'), 't.id = st.store_id', array());
        if(!empty($params['area_list']))
        {
            $select->where('a.id IN (?)', $params['area_list']);
        }
        $select->where('p.result = ?', $params['rank']);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getExportRankStarArea($params) {
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'staff_id'  => "DISTINCT(p.staff_id)",
            'area'   => 'a.name',
            'area_id' => 'a.id',
             'fullname'  => "CONCAT(p.firstname,' ',p.lastname)",
            'code'      => 'p.staff_code',
            'title'     => 'p.title',
            'team'         => 'tt.name',
            'department'    => 'ttt.name',
            'email'         => 's.email',
            'phone'         => 's.phone_number'

        );

        $select->from(array('p' => 'sp_assessment_temp'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.name = p.area', array());
        $select->joinLeft(array('b' => 'bigarea_area'), 'b.area_id = a.id', array());
        $select->joinLeft(array('l' => 'bigarea'), 'l.id = b.bigarea_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('st' => 'store_staff_log'), 'st.staff_id = p.staff_id AND ((st.released_at IS NULL OR st.released_at > UNIX_TIMESTAMP()))', array());
         $select->joinLeft(array('tt' => 'team'), 'tt.id = s.team', array());
         $select->joinLeft(array('ttt' => 'team'), 'ttt.id = s.department', array());
        $select->joinLeft(array('t' => 'store'), 't.id = st.store_id', array());

        $select->where('p.result = ?', $params['rank']);
        $select->where('l.id = ?', $params['regional']);
        
        if(!empty($params['area_list']))
         {
            $select->where('a.id IN (?)', $params['area_list']);
         }
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getStaffTransfer($id) {
        $db = Zend_Registry::get('db');
        $arrCols = array(
            'note'=>'s.note',
            'sl.transfer_id',
            'staff_id'=> 's.staff_id',
            'sl.object',
            's.from_date',
            'info_types'     => new Zend_Db_Expr('GROUP_CONCAT(sl.info_type)'),
            'current_values' => new Zend_Db_Expr('GROUP_CONCAT(sl.current_value)'),
        );
        $select = $db->select()
            ->from(array('s'=>'staff_transfer'),$arrCols)
            ->joinLeft(array('sl'=>'staff_log_detail'),'s.id = sl.transfer_id',array())
            ->where("s.staff_id = ?",$id)
            ->group('sl.transfer_id')
            ->order('s.from_date DESC')
        ;

        $transfer = $db->fetchAll($select);
        return $result;
    }
    
    public function getTotalSellout($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'total_imei' => "COUNT(imei_sn)",
        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);

        $select->where('p.timing_date >= ?', $params['start_month']);
        
        $result = $db->fetchRow($select);

        return $result;
    }
    
    
    public function getListStaffDashboard($params){
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if ($userStorage->email == 'minhtam.nguyen@oppomobile.vn') {
            $list_staff = [183, 293, 419, 417, 403, 182, 545, 190, 577, 627, 628, 626, 629];
        }
        else{
            $list_staff = [183, 293, 419, 417, 403, 182, 545, 190, 577];
        }
        
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'total' => 'COUNT(DISTINCT p.id)' , 
            'title_name' => '(CASE WHEN t2.id = 397 THEN CONCAT(t.name, " - ", t2.name) ELSE t.name END)', 
            'title_id' => 't.id' , 
            'p.team', 
            'team_name' => 't2.name'
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('t'=>'team'),'t.id = p.title',array());
        $select->joinLeft(array('t2'=>'team'),'t2.id = p.team',array());
        $select->joinLeft(array('r'=>'regional_market'),'r.id = p.regional_market',array());
        $select->where('p.off_date IS NULL AND p.title IN (?)', $list_staff);
        
        if(!empty($params['area_id'])){
            $select->where('r.area_id = ?', $params['area_id']);
        }
        
        $select->group('p.title');
        $select->order('p.team');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getStaffByPgsSupervisor($id ){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id", 
            "p.store_id",
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('l' => 'store_staff_log'), 'l.store_id = p.store_id AND l.is_leader = 8 AND l.released_at IS NULL', array());
        
        if(!empty($id)){
            $select->where('l.staff_id = ?', $id);
        }
        $select->where('p.released_at IS NULL', NULL);
        $select->where('p.is_leader = 0', NULL);
        // echo $select->__toString();exit;
        
        $result = $db->fetchAll($select);
        
        if($result){
            return $result;
        }
        else{
            //Trường hợp Pgs suppervisor chưa gán shop
            $data = [];
            $data[] = ['staff_id' => 0, 'store_id' => 0];
            return $data;
        }
        
        
    }

    public function exportPgScore($data)
    {
        ini_set("memory_limit", -1);
        ini_set("display_error", 1);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();
        $heads = array(
            'Stt',
            'Khu vực',
            'Mã nv',
            'Họ và tên',
            'Chức vụ',
            'Công thường',
            'Công OT',
            'KPI',
            'Vi phạm',
            'Khóa học đậu',
            'Khóa học rớt',
            'Thâm niên',
            'Tổng',
            'Kết quả',
            'Date confirmed'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['staff_code']);
            $sheet->setCellValue($alpha++.$index, $item['firstname'] . ' ' . $item['lastname']);
            $sheet->setCellValue($alpha++.$index, $item['title']);
            $sheet->setCellValue($alpha++.$index, $item['total_attendance']==null ? '0' : $item['total_attendance'] );
            $sheet->setCellValue($alpha++.$index, $item['total_ot']==null ? '0' : $item['total_ot']);
            $sheet->setCellValue($alpha++.$index, $item['total_kpi']==null ? '0' : $item['total_kpi']);
            $sheet->setCellValue($alpha++.$index, $item['total_warning']==null ? '0' : $item['total_warning']);
            $sheet->setCellValue($alpha++.$index, $item['total_pass']==null ? '0' : $item['total_pass'] );
            $sheet->setCellValue($alpha++.$index, $item['total_fail']==null ? '0' : $item['total_fail']);
            $sheet->setCellValue($alpha++.$index, $item['thamnien']==null ? '0' : $item['thamnien']);
            $sheet->setCellValue($alpha++.$index, $item['total']==null ? '0' : $item['total']);
            $sheet->setCellValue($alpha++.$index, $item['result']);
            $sheet->setCellValue($alpha++.$index, $item['confirmed_at']==null ? NULL : $item['confirmed_at'] );

            $index++;

        }



        $filename = 'PGs Star' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
	public function getSelloutMonthStoreNew($list_store) {
        if (count($list_store) > 10) {
            return false;
        }
       
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'store_id'   => "p.store_id",
            'month'      => "MONTH(p.timing_date)",
            'total_imei' => "SUM(p.qty)",
        );

        $select->from(array('p' => 'kpi_by_model'), $arrCols);
        if (!empty($list_store)) {
            $select->where('p.store_id IN (?)', $list_store);
        }
        $select->where("DATE(p.timing_date) >= DATE_FORMAT( CURRENT_DATE - INTERVAL 6 MONTH, '%Y-%m-01' )", NULL);

        $select->group(['p.store_id', 'MONTH(p.timing_date)']);
		/*if($_GET['dev112'] == 1){
            echo '<pre>'; print_r($select->__toString()); die;
        }*/
        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['month']][$value['store_id']] = $value['total_imei'];
        }

        return $data;
    }

    public function getUpstarDate($id){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id",
            "p.result",
            "p.date"
        );

        $select->from(array('p' => 'staff_upstar_date'), $arrCols);
        
        $select->where("staff_id = ?", $id);

       
        $result = $db->fetchAll($select);

        return $result;
    }
    
}
