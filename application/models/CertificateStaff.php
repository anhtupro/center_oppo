<?php

class Application_Model_CertificateStaff extends Zend_Db_Table_Abstract
{
    protected $_name = 'certificate_staff';
    
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>$this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.*',                           
                        ))              
            ->order('p.id DESC');   
        if(!empty($params['id'])) {
            $select->where('p.certificate_id = ?', $params['id']);
        }
        if(!empty($params['code'])){
            $select->where('p.code LIKE ?', '%'.$params['code'].'%');
        }
        
        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        return $result;
    }
    
    function getStaffInfo(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');
        if (!$result) {
            $db = Zend_Registry::get('db');
            $select = $db->select()
                ->from(
                        array('p'=>'staff'),
                        array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                                'p.id',
                                'p.code',
                                'fname' => "CONCAT(p.firstname,  ' ',p.lastname)",
                            )); 
            $data = $db->fetchAll($select);
            $result = array();
            foreach($data as $value){
                $result[ $value['code'] ] = array(
                                                    'id' => $value['id'],
                                                    'fname' => $value['fname']
                                                );
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }        
        return $result;
    }
    
    function getAll($id){
        $db = Zend_Registry::get('db');
            $select = $db->select()
                ->from(
                        array('p'=>$this->_name),
                        array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                                'p.code'
                            ))
                ->where('p.certificate_id = ?', (int)$id);
        $data = $db->fetchAll($select);        
        if ($data){
                foreach ($data as $item){
                    $result[$item['id']] = $item['code'];
                }
            }
        return $result;
    }
}    