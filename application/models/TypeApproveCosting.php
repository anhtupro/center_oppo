<?php
class Application_Model_TypeApproveCosting extends Zend_Db_Table_Abstract
{
    protected $_name = 'type_approve_costing';
    //protected $_schema = DATABASE_COST;


     public function getTypeApprove()
	    {
	    	$db     = Zend_Registry::get("db");
	        $select = $db->select();

	        $arrCols = array(
	            'p.id', 
	            'name'        => 'p.name' 
	        );
	        $select->from(array('p'=> 'type_approve_costing'), $arrCols);
	        $result = $db->fetchAll($select);
	        return $result;
	    }

	    public function getDataList(){
	    	$db     = Zend_Registry::get("db");
	        $select = $db->select();

	        $arrCols = array(
	            'p.id', 
	            'name'        => 'p.name',
	            'type_sign'	=>"GROUP_CONCAT(g.type)"
	        );
	        $select->from(array('p'=> 'type_approve_costing'), $arrCols);
	        $select->joinLeft(array('g' => 'type_approve_details_costing'), 'g.type_approve_id = p.id', array());
	        $select->GROUP('p.id');
	        $result = $db->fetchAll($select);
	        return $result;
	    }

	    public function getData($params)
	    {
	    	$db     = Zend_Registry::get("db");
	        $select = $db->select();

	        $arrCols = array(
	            'p.id', 
	            'name'        => 'p.name' 
	        );
	        $select->from(array('p'=> 'type_approve_costing'), $arrCols);
	        if(!empty($params['id'])){
	        	$select->where('p.id = ?',$params['id']);
	        }
	        $result = $db->fetchAll($select);
	        //echo $select; exit;
	        return $result;
	    }
}