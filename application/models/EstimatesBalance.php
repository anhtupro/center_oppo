<?php
class Application_Model_EstimatesBalance extends Zend_Db_Table_Abstract
{
    protected $_name = 'estimates_balance';
    //protected $_wh = WAREHOUSE_DB;

    
    public function get_list_table($params)
    {
    	$db = Zend_Registry::get('db');
    	$select = $db->select();
        
    	if(!empty($params['year'])&&!empty($params['month'])){
          $select = "SELECT CONCAT(m.`year`,'_',m.`month`,'_',m.d_id,'_',m.pmodel_code_id) str_balance_id, m.*
                    FROM (	SELECT '".$params['year']."' as `year`, '".$params['month']."' as `month`  ,t.`d_id`, t.`pmodel_code_id` , 
                                    IFNULL(b.`amount_estimates`,0) as `amount_estimates`, IFNULL(b.`amount_reality`,0) as `amount_reality`, 
                                    t.title channel_name, t.`name` content_name
                                FROM (
                                        SELECT d.title, d.id as `d_id` , p.`name`, p.id AS pmodel_code_id
                                        FROM ".WAREHOUSE_DB.".distributor d
                                        CROSS JOIN pmodel_code p
                                        WHERE d.channel_cost =1 AND (d.del is NULL OR d.del = 0)
                                        AND p.category_id =31
                                        AND p.`status` = 0  
                                ) AS t
                                LEFT JOIN estimates_balance b ON b.`d_id` = t.`d_id`
                                AND b.`pmodel_code_id` = t.`pmodel_code_id` AND  b.`month` =".$params['month']." AND b.`year` =".$params['year']."

                                UNION

                                SELECT a.`year`, a.`month`, a.d_id, a.pmodel_code_id, a.amount_estimates, a.amount_reality, d.title channel_name, p.`name` content_name
                                FROM `estimates_balance` a
                                LEFT JOIN ".WAREHOUSE_DB.".distributor d ON a.d_id = d.id
                                LEFT JOIN pmodel_code p ON a.pmodel_code_id = p.id
                                WHERE  a.`month` =".$params['month']." AND a.`year` =".$params['year']."
                    ) m
                    ORDER BY m.d_id, m.pmodel_code_id
                ";        
        	$result = $db->fetchAll($select);
        	return $result;
        }else{
            return null;
        }
    }// End get_list_table
    
}