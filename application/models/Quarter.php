<?php

class Application_Model_Quarter extends Zend_Db_Table_Abstract
{
    protected $_name = 'quarter';
    protected $_schema = DATABASE_TRADE;

    public function getTimeFromTo($quarter, $year)
    {
        if ($quarter == 1) {
            $time['from_date'] = date('Y-m-d', strtotime($year . '-01-01'));
            $time['to_date'] = date('Y-m-d', strtotime($year . '-03-31'));

            $time['from_date_time'] = date('Y-m-d 00:00:00', strtotime($year . '-01-01'));
            $time['to_date_time'] = date('Y-m-d 23:59:59', strtotime($year . '-03-31'));
        }
        if ($quarter == 2) {
            $time['from_date'] = date('Y-m-d', strtotime($year . '-04-01'));
            $time['to_date'] = date('Y-m-d', strtotime($year . '-06-30'));

            $time['from_date_time'] = date('Y-m-d 00:00:00', strtotime($year . '-04-01'));
            $time['to_date_time'] = date('Y-m-d 23:59:59', strtotime($year . '-06-30'));
        }
        if ($quarter == 3) {
            $time['from_date'] = date('Y-m-d', strtotime($year . '-07-01'));
            $time['to_date'] = date('Y-m-d', strtotime($year . '-09-30'));

            $time['from_date_time'] = date('Y-m-d 00:00:00', strtotime($year . '-07-01'));
            $time['to_date_time'] = date('Y-m-d 23:59:59', strtotime($year . '-09-30'));
        }
        if ($quarter == 4) {
            $time['from_date'] = date('Y-m-d', strtotime($year . '-10-01'));
            $time['to_date'] = date('Y-m-d', strtotime($year . '-12-31'));

            $time['from_date_time'] = date('Y-m-d 00:00:00', strtotime($year . '-10-01'));
            $time['to_date_time'] = date('Y-m-d 23:59:59', strtotime($year . '-12-31'));
        }

        return $time;
    }

    public function getQuarter($month)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['q' => DATABASE_TRADE . '.quarter'], [
                'q.quarter'
            ])
            ->where('q.month = ?', $month);

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getPreQuarter($quarter, $year)
    {
        if ($quarter == 1) {
            $data['pre_quarter'] = 4;
            $data['pre_year'] = $year - 1;
        } else {
            $data['pre_quarter'] = $quarter - 1;
            $data['pre_year'] =$year;
        }

        return $data;
    }
}