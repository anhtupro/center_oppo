<?php
class Application_Model_JobCategory extends Zend_Db_Table_Abstract
{
	protected $_name = 'job_category';

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if (!$result) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_cache_wss(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_wss_cache');

        if (!$result) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[] = array(
                        'id'      => $item['id'],
                        'name'    => $item['name'],
                        'parent'  => $item['parent'],
                    );
                }
            }
            $cache->save($result, $this->_name.'_wss_cache', array(), null);
        }
        return $result;
    }
    
    function get_all(){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'job_category'),
                    array('a.*')
                );
        $result = $db->fetchAll($select);
        return $result;
    }

  
}                                                      
