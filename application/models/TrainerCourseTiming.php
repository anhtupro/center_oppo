<?php
class Application_Model_TrainerCourseTiming extends Zend_Db_Table_Abstract
{
	protected $_name = 'trainer_course_timing';

	function getStaffCheckin($date,$course_id)
	{
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name),array('*'));

        $select->where('p.del = ? OR p.del IS NULL', 0);

        $select->where('p.date = ? ', $date);

        $select->joinLeft(array('e'=>'trainer_course_detail'),'p.course_detail_id = e.id AND (e.del IS NULL OR e.del = 0)',array('e.course_id'));

        $select->having('e.course_id = ?',$course_id);

        $result = $db->fetchAll($select);

        $arrayCourseDetail = array();

        if(count($result))
        {
            foreach ($result as $key => $value) {

            $arrayCourseDetail[] = $value['course_detail_id'];
            }
        }
        return $arrayCourseDetail;

	}
}