<?php

class Application_Model_RequestType extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_type';
    
    public function getRequetsType($department_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "r.id", 
            "title_name" => "r.title", 
            "p.department_id",
            "r.group_id"
        );

        $select->from(array('p' => 'request_type_group'), $arrCols);
        $select->joinLeft(array('r' => 'request_type'), 'r.group_id = p.id', array());

        $select->where('p.is_del = 0 AND r.is_del = 0', NULL);
        $select->where('p.department_id = ?', $department_id);
        
        $result = $db->fetchAll($select);
        

        return $result;
    }
    
    function get_rtype_cache()
    {
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_rtype_cache');

        if (!$result) {

            $db = Zend_Registry::get('db');
            $select = $db->select()
                ->distinct()
                ->from(array('p' => 'request_type'), array('type_id' => 'p.id', 'type_title' => 'p.title', 'type_desc' => 'p.desc'))
                ->joinLeft( array('g'=> 'request_type_group'), 'g.id = p.group_id', array('group_id' => 'g.id' , 'group_title' => 'g.title'));                
                

            $data = $db->fetchAll($select);

            $result = array();

            if ($data) {
                foreach ($data as $value) {

                    if ( ! isset( $result[ $value['group_id'] ] ) )
                        $result[ $value['group_id'] ] = array(
                                                                'name'     => $value['group_title'],                                                                                                                               
                                                                'children' => array(),
                                                                );

                    if ( ! isset( $result[ $value['group_id'] ]
                                                ['children']
                                                    [ $value['type_id'] ] ) )
                        $result[ $value['group_id'] ]
                                    ['children']
                                        [ $value['type_id'] ] = array(
                                                                    'name' => $value['type_title'],
                                                                    );
                }
            }

            $cache->save($result, $this->_name.'_rtype_cache', array(), null);
        }
        return $result;
    }
    
    function get_request_type($params){
        
        if(empty($params['department_id'])){
            return false;
        }
        
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->distinct()
            ->from(array('p' => 'request_type'), array('type_id' => 'p.id', 'type_title' => 'p.title', 'type_desc' => 'p.desc'))
            ->joinLeft( array('g'=> 'request_type_group'), 'g.id = p.group_id', array('group_id' => 'g.id' , 'group_title' => 'g.title'));                
        
        $select->where('p.is_del = 0');
        $select->where('g.is_del = 0');
        $select->where('g.department_id = ?', $params['department_id']);

        $result = $db->fetchAll($select);
        
        return $result;
        
    }

    function getInfoById($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "title" => "p.title", 
            "p.group_id", 
            "p.desc", 
            "p.is_del", 
            "g.department_id", 
            "department_name" => "t.name",
            "group_title" => "g.title"
        );

        $select->from(array('p' => 'request_type'), $arrCols);
        $select->joinLeft(array('g' => 'request_type_group'), 'g.id = p.group_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = g.department_id', array());

        $select->where('p.id = ?', $id);
        
        $result = $db->fetchRow($select);

        return $result;
        
    }
    
}