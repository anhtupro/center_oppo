<?php

class Application_Model_NewImeiKpi extends Zend_Db_Table_Abstract{

	protected $name = 'imei_kpi';

	public function dbconnect(){
		return Zend_Registry::get('db');
	}
        
		
		public function checkTitle($staff_id){
		$db = Zend_Registry::get('db');
		
		$sql = 'SELECT s.title
				FROM staff s
				WHERE s.id = :staff_id
		';

		$stmt = $db->prepare($sql);
		$stmt->bindParam('staff_id', $staff_id, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$db   = $stmt = NULL;
		return $data;
	}
        public function Get_All_New($params = array()){
            $db = Zend_Registry::get('db');
		$params = array_merge(
			array(
				'staff_id' => null,
				'from' => null,
				'to' => null
			),
			$params
		);

		$from_date = $params['from'].' 00:00:00';
		$to_date   = $params['to'].' 23:59:59';

		$sql = 
				'SELECT 	
					g.`desc` as `name`,
					store.name store,
					COUNT(ik.imei_sn) sellout,
					CASE s.id
						WHEN ik.pg_id  THEN "PGPB" 
						WHEN ik.iks_staff_id THEN "PRODUCT CONSULTANT" 
						WHEN ik.sale_id THEN "Sale"
						WHEN ik.store_leader_id THEN "Store Leader"
						WHEN ik.sales_brs_id THEN "Sale Leader Brandshop"
					END AS staff_title,
					CASE s.id
						WHEN ik.pg_id THEN ik.kpi_pg
						WHEN ik.iks_staff_id THEN  ik.kpi
						WHEN ik.sale_id THEN ik.kpi_sale
						WHEN ik.store_leader_id THEN ik.kpi_store_leader
						WHEN ik.sales_brs_id THEN ik.kpi_sales_brs
					END AS kpi,
					MIN(ik.timing_date) from_date,
					MAX(ik.timing_date) to_date
				FROM 			
				(
					SELECT a.good_id, a.kpi_pg, a.kpi_sale, a.kpi_store_leader, a.kpi_sales_brs, a.store_id, a.pg_id, a.sale_id, a.store_leader_id, a.imei_sn, a.sales_brs_id, a.timing_date, b.staff_id `iks_staff_id`, b.kpi 
					FROM
					(SELECT * FROM imei_kpi WHERE timing_date BETWEEN :from_date  AND :to_date) a	
							LEFT JOIN imei_kpi_staff b ON a.imei_sn  = b.imei_sn 
				) ik
				JOIN staff s ON (ik.iks_staff_id = s.id OR ik.pg_id  = s.id OR ik.sale_id = s.id OR ik.store_leader_id = s.id OR ik.sales_brs_id = s.id )
							JOIN warehouse.good g ON ik.good_id = g.id
							JOIN store ON store.id = ik.store_id
							WHERE s.id = :staff_id
							GROUP BY g.id,
                                                        ik.kpi_pg,
							ik.kpi_sale,
							ik.kpi_store_leader,
							ik.iks_staff_id,
							ik.sales_brs_id,
kpi,staff_title,ik.store_id
							ORDER BY g.name,from_date,sellout';

		$stmt = $db->prepare($sql);
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
		$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$db   = $stmt = NULL;
		// echo "<pre>";print_r($data);die;
		return $data;
        }
		
		public function Get_All_New_Full($params){
		$db = Zend_Registry::get('db');
             
		$from_date = $params['from'].' 00:00:00';
		$to_date   = $params['to'].' 23:59:59';
                if($params['from'] < '2020-03-01'){
                    $sql = 'SELECT 	
			g.`desc` AS "name",
			gc.name color,
			store.name store,
			SUM(ik.qty) sellout,
			IF(ik.type = 7, "PRODUCT CONSULTANT"  , "PGPB") AS staff_title,
			ik.`kpi_pg` AS kpi,
			DATE(ik.timing_date) AS `ngay`,	
			MIN(ik.timing_date) from_date,
			MAX(ik.timing_date) to_date
		FROM (
			SELECT kb.*, km.timing_date, km.good_id, km.store_id, km.color_id
			FROM kpi_by_staff kb
			INNER JOIN kpi_by_model km ON km.id = kb.kpi_by_model_id
			WHERE (km.timing_date BETWEEN :from_date AND :to_date ) AND kb.pg_id = :staff_id
		) ik
		JOIN ' . WAREHOUSE_DB . '.good g ON ik.good_id = g.id
		JOIN ' . WAREHOUSE_DB . '.good_color gc ON ik.color_id = gc.id
		JOIN store ON store.id = ik.store_id
		GROUP BY g.id,kpi,staff_title,ik.store_id
		
		UNION ALL

			SELECT g.`desc` AS "name",
						gc.name color,
						store.name store,
						`m`.`qty` AS `sellout`,
						"SALE" AS staff_title,
						`m`.`kpi_sale`        AS `kpi`,
						`m`.`ngay`     AS `ngay`,
						MIN(m.ngay) from_date,
						MAX(m.ngay) to_date
					FROM `v_kpi_t2` `m`
					JOIN ' . WAREHOUSE_DB . '.good g ON m.good_id = g.id
					JOIN ' . WAREHOUSE_DB . '.good_color gc ON m.color_id = gc.id
					JOIN store ON store.id = m.store_id
					WHERE ((`m`.`ngay` BETWEEN  :from_date AND :to_date)
						AND (`m`.`kpi_sale` > 0)
						AND (m.sale_id = :staff_id))
					GROUP BY `m`.`store_id`,`m`.`ngay`,`m`.`good_id`,`m`.`color_id`  
				
			UNION ALL
			
			SELECT g.`desc` AS "name",
						gc.name color,
						store.name store,
						`m`.`qty` AS `sellout`,
						"SALE" AS staff_title,
						`m`.`kpi_store_leader`  AS `kpi`,
						`m`.`timing_date`     AS `ngay`,
						MIN(m.timing_date) from_date,
						MAX(m.timing_date) to_date
					FROM `kpi_by_model` `m`
					JOIN ' . WAREHOUSE_DB . '.good g ON m.good_id = g.id
					JOIN ' . WAREHOUSE_DB . '.good_color gc ON m.color_id = gc.id
					JOIN store ON store.id = m.store_id
					WHERE (`m`.`timing_date` BETWEEN  :from_date AND :to_date)
						AND (`m`.`kpi_store_leader` > 0)
						AND (m.store_leader_id = :staff_id)
					GROUP BY `m`.`store_id`,`m`.`timing_date`,`m`.`good_id`,`m`.`color_id`   	
					';
                }else{
                    $sql = 'SELECT 	
			g.`desc` AS "name",
			GROUP_CONCAT(DISTINCT gc.name) color,
			store.name store,
			SUM(ik.qty) sellout,
			IF(ik.type = 7, "PRODUCT CONSULTANT"  , "PGPB") AS staff_title,
			ik.`kpi_pg` AS kpi,
			DATE(ik.timing_date) AS `ngay`,	
			MIN(ik.timing_date) from_date,
			MAX(ik.timing_date) to_date
		FROM (
			SELECT kb.*, km.timing_date, km.good_id, km.store_id, km.color_id
			FROM kpi_by_staff kb
			INNER JOIN kpi_by_model km ON km.id = kb.kpi_by_model_id
			WHERE (km.timing_date BETWEEN :from_date AND :to_date ) AND kb.pg_id = :staff_id
		) ik
		JOIN ' . WAREHOUSE_DB . '.good g ON ik.good_id = g.id
		JOIN ' . WAREHOUSE_DB . '.good_color gc ON ik.color_id = gc.id
		JOIN store ON store.id = ik.store_id
		GROUP BY g.id,kpi,staff_title,ik.store_id
		
		UNION ALL

			SELECT
			g.`desc` AS "name",
				GROUP_CONCAT(DISTINCT gc.name) color,
				store.name store,
				COUNT(`i`.`imei_sn`) AS `sellout`,
				"SALE" AS staff_title,
				`i`.`kpi_sale`       AS `kpi`,
				CAST(`i`.`timing_date` AS DATE) AS `ngay`,	
				MIN(i.timing_date) from_date,
				MAX(i.timing_date) to_date
			FROM `imei_kpi` `i`
			JOIN ' . WAREHOUSE_DB . '.good g ON i.good_id = g.id
			JOIN ' . WAREHOUSE_DB . '.good_color gc ON i.color_id = gc.id
			JOIN store ON store.id = i.store_id
			WHERE (`i`.`timing_date` BETWEEN :from_date AND :to_date)
				AND  i.sale_id = :staff_id
			GROUP BY `i`.`store_id`,`ngay`,`i`.`good_id`,`i`.`color_id` 
			
			UNION ALL
			
			SELECT g.`desc` AS "name",
						GROUP_CONCAT(DISTINCT gc.name) color,
						store.name store,
						`m`.`qty` AS `sellout`,
						"SALE" AS staff_title,
						`m`.`kpi_sale`        AS `kpi`,
						`m`.`timing_date`     AS `ngay`,
						MIN(m.timing_date) from_date,
						MAX(m.timing_date) to_date
					FROM `kpi_by_model` `m`
					JOIN ' . WAREHOUSE_DB . '.good g ON m.good_id = g.id
					JOIN ' . WAREHOUSE_DB . '.good_color gc ON m.color_id = gc.id
					JOIN store ON store.id = m.store_id
					WHERE ((`m`.`timing_date` BETWEEN  :from_date AND :to_date)
						AND (`m`.`status` = 1)
						AND (m.sale_id = :staff_id))
					GROUP BY `m`.`store_id`,`m`.`timing_date`,`m`.`good_id`,`m`.`color_id`  
				
			UNION ALL
			
			SELECT g.`desc` AS "name",
						GROUP_CONCAT(DISTINCT gc.name) color,
						store.name store,
						`m`.`qty` AS `sellout`,
						"SALE" AS staff_title,
						`m`.`kpi_store_leader`  AS `kpi`,
						`m`.`timing_date`     AS `ngay`,
						MIN(m.timing_date) from_date,
						MAX(m.timing_date) to_date
					FROM `kpi_by_model` `m`
					JOIN ' . WAREHOUSE_DB . '.good g ON m.good_id = g.id
					JOIN ' . WAREHOUSE_DB . '.good_color gc ON m.color_id = gc.id
					JOIN store ON store.id = m.store_id
					WHERE (`m`.`timing_date` BETWEEN  :from_date AND :to_date)
						AND (m.store_leader_id = :staff_id)
					GROUP BY `m`.`store_id`,`m`.`timing_date`,`m`.`good_id`,`m`.`color_id`   	
					';
                }

		$stmt = $db->prepare($sql);

		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
		$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$db   = $stmt = NULL;
		return $data;
	}
        
	public function Get_All($params = array()){

		$db = Zend_Registry::get('db');
		$params = array_merge(
			array(
				'staff_id' => null,
				'from' => null,
				'to' => null
			),
			$params
		);

		$from_date = $params['from'].' 00:00:00';
		$to_date   = $params['to'].' 23:59:59';

		$sql = 
				'SELECT 	
					g.`desc` name,
					store.name store,
					COUNT(ik.imei_sn) sellout,
					CASE s.id
						WHEN ik.pg_id THEN "PGPB" 
						WHEN ik.sale_id THEN "Sale"
						WHEN ik.store_leader_id THEN "Store Leader"
						WHEN ik.sales_brs_id THEN "Sale Leader Brandshop"
					END AS staff_title,
					CASE s.id
						WHEN ik.pg_id THEN ik.kpi_pg 
						WHEN ik.sale_id THEN ik.kpi_sale
						WHEN ik.store_leader_id THEN ik.kpi_store_leader
						WHEN ik.sales_brs_id THEN ik.kpi_sales_brs
					END AS kpi,
					MIN(ik.timing_date) from_date,
					MAX(ik.timing_date) to_date
				FROM imei_kpi ik
				JOIN staff s ON (ik.pg_id = s.id OR ik.sale_id = s.id OR ik.store_leader_id = s.id OR ik.sales_brs_id = s.id)
				JOIN warehouse.good g ON ik.good_id = g.id
				JOIN store on store.id = ik.store_id
				WHERE ik.timing_date BETWEEN :from_date AND :to_date AND s.id = :staff_id
				GROUP BY g.id,kpi,staff_title,ik.store_id
				ORDER BY g.name,from_date,sellout';

		$stmt = $db->prepare($sql);
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
		$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$db   = $stmt = NULL;
		// echo "<pre>";print_r($data);die;
		return $data;

	}

	public function GetAll($params = array()){
		$params = array_merge(
			array(
				'staff_id' => null,
				'from' => null,
				'to' => null
			),
			$params
		);
		
		$db  = $this->dbconnect();
 		$stmt = $db->prepare('CALL SP_imei_kpi_GetAll(:staff_id, :from, :to, @total_pg, @total_sale, @total_pb_sale)');
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from', $params['from'], PDO::PARAM_STR);
		$stmt->bindParam('to', $params['to'], PDO::PARAM_STR);
		$stmt->execute();
		$res['data'] = $stmt->fetchAll();
		$stmt->closeCursor();

		$pg = $db->query('SELECT @total_pg AS total')->fetch();
		$sale = $db->query('SELECT @total_sale AS total')->fetch();
		$pb_sale = $db->query('SELECT @total_pb_sale AS total')->fetch();
		$res['total_pg_level'] = $pg['total'];
		$res['total_sale_level'] = $sale['total'];
		$res['total_pb_sale_level'] = $pb_sale['total'];
		$stmt = $db->prepare('DROP TEMPORARY TABLE IF EXISTS _temp');
		$stmt->execute();
		$stmt->closeCursor();

		$db = $stmt = null;
		return $res;
	}

	public function checkTimeOnShop($dataCheck = array()){
		
		$dataCheck = array_merge(
			array(
				'store_id'	=> null,
				'date'		=> null,
				'is_leader'	=> null,
			),
			$dataCheck
		);
		$db  = $this->dbconnect();
		// var_dump($dataCheck);die;
		$stmt = $db->prepare('CALL sp_check_time_on_shop(:p_date, :p_store, :p_is_leader)');
		$stmt->bindParam('p_date', $dataCheck['date'], PDO::PARAM_INT);
		$stmt->bindParam('p_store', $dataCheck['store_id'], PDO::PARAM_INT);
		$stmt->bindParam('p_is_leader', $dataCheck['is_leader'], PDO::PARAM_INT);
		$stmt->execute();
		$res = $stmt->fetch();

		$stmt->closeCursor();

		$db = $stmt = null;
		return $res;

	}
        
        public function Get_All_RM($params = array()){

		$db = Zend_Registry::get('db');
		$params = array_merge(
			array(
				'staff_id' => null,
				'from' => null,
				'to' => null
			),
			$params
		);

		$from_date = $params['from'].' 00:00:00';
		$to_date   = $params['to'].' 23:59:59';

		$sql = "SELECT 	
					ik.`good_name` `name`,
					ik.store,ik.store_id,
					COUNT(ik.imei) sellout,
					CASE s.id
						WHEN ik.pg THEN 'PGPB' 
						WHEN ik.sale THEN 'Sale'
					END AS staff_title,
					CASE s.id
						WHEN ik.pg THEN ik.kpi_pg 
						WHEN ik.sale THEN ik.kpi_sale
					END AS kpi,
					MIN(ik.timing_date) from_date,
					MAX(ik.timing_date) to_date
	FROM  				
(SELECT ts.`imei`,DATE(t.`from`) AS timing_date,i.`good_id`
,g.name AS good_name ,ss.`name` AS store,ss.id as store_id
,IFNULL(MIN(TRAN.staff_id), 0) pg, 	
IF(k8.kpi, k8.kpi, 0) kpi_pg,
IFNULL(MIN(TRAN1.staff_id), 0) sale,
IF(ks.kpi, ks.kpi, 0) kpi_sale,
IFNULL(MIN(LEAD.staff_id), 0) leader,
IF(kla.kpi_leader, kla.kpi_leader, 0) kpi_leader,
IF(kla.kpi_area, kla.kpi_area, 0) kpi_area
FROM timing_rm t
JOIN timing_sale_rm ts ON t.`id`= ts.`timing_id`
JOIN staff s ON s.id= t.`staff_id`
JOIN store ss ON ss.id= t.`store`
JOIN warehouse.`imei_rm` i ON ts.`imei`= i.`imei_sn`
JOIN warehouse.good g ON g.id= i.good_id
LEFT JOIN store_staff_log TRAN ON t.staff_id = TRAN.staff_id
							 AND t.store = TRAN.store_id
							 AND DATE( t.`from` ) >= FROM_UNIXTIME(TRAN.joined_at, '%Y-%m-%d') 
							 AND (TRAN.released_at IS NULL OR DATE( t.`from` ) < FROM_UNIXTIME(TRAN.released_at, '%Y-%m-%d') )
							 AND TRAN.is_leader = 0
LEFT JOIN store_staff_log TRAN1 ON t.store = TRAN1.store_id
									AND DATE( t.`from` ) >= FROM_UNIXTIME(TRAN1.joined_at, '%Y-%m-%d') 
									AND ( TRAN1.released_at IS NULL OR DATE( t.`from` ) < FROM_UNIXTIME(TRAN1.released_at, '%Y-%m-%d') )
									AND TRAN1.is_leader = 1		
LEFT JOIN `store_leader_log` LEAD ON t.store = LEAD.store_id
								AND DATE( t.`from` ) >= FROM_UNIXTIME(LEAD.joined_at, '%Y-%m-%d') 
								AND ( LEAD.released_at IS NULL OR DATE( t.`from` ) < FROM_UNIXTIME(LEAD.released_at, '%Y-%m-%d') )   	
LEFT JOIN 
( 
 SELECT *,  
 DATE_FORMAT(CONCAT(DATE(from_date), ' 00:00:00'),'%Y-%m-%d %H:%i:%s') from_date_value,
 DATE_FORMAT(CONCAT(DATE(to_date), ' 23:59:59'),'%Y-%m-%d %H:%i:%s') to_date_value
 FROM kpi_setting
) k8
	ON i.`good_id` = k8.good_id
	AND k8.type = 1 
	AND k8.policy_id = 4
	AND DATE(t.from) >= k8.from_date_value
	AND (
				DATE(t.from) <= k8.to_date_value
				OR k8.to_date IS NULL
			)
LEFT JOIN 
		( 
		 SELECT *,  
		 DATE_FORMAT(CONCAT(DATE(from_date), ' 00:00:00'),'%Y-%m-%d %H:%i:%s') from_date_value,
		 DATE_FORMAT(CONCAT(DATE(to_date), ' 23:59:59'),'%Y-%m-%d %H:%i:%s') to_date_value
		 FROM kpi_setting
		) ks
			ON i.`good_id` = ks.good_id
			AND DATE(t.from) >= ks.from_date_value
			AND (
				DATE(t.from) <= ks.to_date_value
				OR ks.to_date IS NULL
			)
			AND ks.dealer_type=2
			AND ks.type = 2	
LEFT JOIN 
( 
 SELECT *,  
 DATE_FORMAT(CONCAT(DATE(from_date), ' 00:00:00'),'%Y-%m-%d %H:%i:%s') from_date_value,
 DATE_FORMAT(CONCAT(DATE(to_date), ' 23:59:59'),'%Y-%m-%d %H:%i:%s') to_date_value
 FROM kpi_setting_rm
) kla
	ON i.`good_id` = kla.good_id
	AND DATE(t.from) >= kla.from_date_value
	AND (
				DATE(t.from) <= kla.to_date_value
				OR kla.to_date IS NULL
			)			 
WHERE t.`status`=1 and t.from BETWEEN :from_date AND :to_date
GROUP BY t.`from`,
                         t.staff_id,
                         t.store,
                         ts.imei
) ik
JOIN staff s ON (ik.pg = s.id OR ik.sale = s.id)
where  s.id = :staff_id
GROUP BY ik.good_id,kpi,staff_title,ik.store_id
				ORDER BY ik.good_name,from_date,sellout;
 ;"
				;

		$stmt = $db->prepare($sql);
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
		$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$db   = $stmt = NULL;
		// echo "<pre>";print_r($data);die;
		return $data;

	}
        
        public function Get_All_CONSULTANT($params = array()){

		$db = Zend_Registry::get('db');
		$params = array_merge(
			array(
				'staff_id' => null,
				'from' => null,
				'to' => null
			),
			$params
		);

		$from_date = $params['from'].' 00:00:00';
		$to_date   = $params['to'].' 23:59:59';

		$sql = 
				'SELECT
			g.`desc` name,
					store.name store,
					COUNT(ik.imei_sn) sellout,
					CASE s.id
						WHEN iks.staff_id THEN "PRODUCT CONSULTANT" 
						
					END AS staff_title,
					iks.kpi kpi,
					MIN(ik.timing_date) from_date,
					MAX(ik.timing_date) to_date
	FROM
					imei_kpi_staff iks
INNER JOIN imei_kpi ik ON iks.imei_sn = ik.imei_sn
	JOIN staff s ON iks.staff_id = s.id
				JOIN warehouse.good g ON ik.good_id = g.id
				JOIN store on store.id = ik.store_id
				WHERE ik.timing_date BETWEEN :from_date AND :to_date AND s.id = :staff_id
				GROUP BY g.id,kpi,staff_title,ik.store_id
				ORDER BY g.name,from_date,sellout';

		$stmt = $db->prepare($sql);
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
		$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$db   = $stmt = NULL;
		// echo "<pre>";print_r($data);die;
		return $data;

	}


}