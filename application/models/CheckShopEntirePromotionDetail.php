<?php

class Application_Model_CheckShopEntirePromotionDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_promotion_detail';
    protected $_schema = DATABASE_TRADE;

    public function get($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['p' => DATABASE_TRADE.'.check_shop_entire_promotion'], [
                         'd.*',
                         'promotion_name' => 'o.name'
                     ])
                     ->joinLeft(['d' => DATABASE_TRADE.'.check_shop_entire_promotion_detail'], 'p.id = d.check_shop_promotion_id', [])
            ->joinLeft(['o' => DATABASE_TRADE.'.promotion'], 'd.promotion_id = o.id', [])
        ->where('p.check_shop_id = ?', $params['check_shop_id']);
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['check_shop_promotion_id']] [] = $element;
        }

        return $list;

    }
}