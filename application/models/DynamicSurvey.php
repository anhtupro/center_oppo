<?php

class Application_Model_DynamicSurvey extends Zend_Db_Table_Abstract
{
    protected $_name = 'dynamic_survey';

    function fetchPagination($page, $limit, &$total, $params)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $select = $db
            ->select()
            ->from('dynamic_survey')
            ->where('ds_is_deleted = ?', 0);
        if ($params['staff_id']) {
            $select->where('ds_created_by = ?', $params['staff_id']);
        }
        if ($params['ref']) {
            $select->where('ds_ref = ?', $params['ref']);
        }
        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $select->order('ds_id desc');
        $result = $db->fetchAll($select);
        if ($limit) {
            $select = $db->select()
                ->from('dynamic_survey', ['count(ds_id) count'])
                ->where('ds_is_deleted = ?', 0);
            $total = $db->fetchRow($select)['count'];
        }
        return $result;
    }

    /**
     * @param $ref
     * @return array
     */
    public function getActiveSurveyByRef($ref)
    {
        return $this->fetchAll(
            $this->select()
                ->from($this, ['ds_id', 'ds_name', 'ds_status'])
                ->where('ds_is_deleted = ?', 0)
                ->where('ds_status = ?', 1)
                ->where('ds_ref = ?', $ref)
                ->order('ds_id desc')
        )->toArray();
    }
}