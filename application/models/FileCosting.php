<?php
class Application_Model_FileCosting extends Zend_Db_Table_Abstract
{
    protected $_name = 'file_costing';
    //protected $_schema = DATABASE_COST;


    public function getFile($params)
    {
    	$db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'file_name'        	=> 'p.name', 
            'url'       	=> 'p.url',
            'url_signature'     => 'p.url_signature',
            'type'   => 'p.type'
        );
         
        $select->from(array('p'=> 'file_costing'), $arrCols);
        $select->where('p.cost_id = ?',$params['id']);
        $result = $db->fetchAll($select);
        return $result;
    }
}