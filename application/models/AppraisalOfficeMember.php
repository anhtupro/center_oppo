<?php

class Application_Model_AppraisalOfficeMember extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_member';
    protected $_primary = 'aom_id';
    
    function fetchPaginationPrdApprove($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT s.id'),
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            'joined_at' => "s.joined_at",
            'department_name' => 't3.name',
            'team_name' => 't2.name',
            'title_name' => "t.name",
            'aom_staff_approved' => 'p.aom_staff_approved',
            'aom_head_approved' => 'p.aom_head_approved',
            'aom_staff_id' => 'p.aom_staff_id',
            'aop_id' => 'p.aop_id',
            'plan.aop_status'
        );

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.aom_staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = s.team', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = s.department', array());
        $select->joinLeft(array('plan' => 'appraisal_office_plan'), 'plan.aop_id = p.aop_id', array());
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = p.aom_id', array());
        $select->joinLeft(array('task' => 'appraisal_office_task'), 'task.aof_id = f.aof_id', array());
        
        $select->where('task.aot_id IS NOT NULL', NULL);
        $select->where('plan.aop_type = ?', 1);
        
        if(!empty($params['name'])){
            $select->where("CONCAT(s.firstname, ' ', s.lastname) LIKE ?", "%".$params['name']."%");
        }
        
        if(!empty($params['code'])){
            $select->where("s.code LIKE ?", "%".$params['s.code']."%");
        }
        
        if(!empty($params['title'])){
            $select->where("s.title IN (?)", $params['title']);
        }
        
        if(!empty($params['team'])){
            $select->where("s.team IN (?)", $params['team']);
        }
        
        if(!empty($params['department'])){
            $select->where("s.department IN (?)", $params['department']);
        }
        
        if($params['level'] == 2){
            $select->where("p.aom_head_department_id = ?", $params['staff_id']);
        }
        elseif($params['level'] == 3){
            $select->where("p.aom_staff_id = ?", $params['staff_id']);
        }
        
        if($params['staff_id'] == 24979 OR $params['staff_id'] == 24980){
            $select->where("s.department = ?", 156);
        }
        
        $select->where('p.aop_id = ?', $params['plan_id']);
        $select->where('s.off_date IS NULL', NULL);
        $select->where('p.aom_staff_id IS NOT NULL', NULL);
        $select->where('p.aom_is_deleted = 0', NULL);
        $select->group('p.aom_id');
        $select->order(['s.title ASC','s.id ASC']);
        
        
        if($_GET['dev'] == 1){
            echo $select;exit;
        }
        
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    function fetchPaginationPrdApprove2($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.to_staff'),
            'fullname' => "CONCAT(s.firstname,' ',s.lastname)" , 
            "title_name" => "t.name", 
            "team_name" => "t2.name", 
            "department_name" => "t3.name",
            "m.aom_id", 
            "m.aom_staff_id",
            "m.aop_id",
            "m.aom_staff_approved", 
            "m.aom_head_approved", 
            "f.aof_id", 
            "task.aot_id",
            "status" => "(CASE WHEN task.aot_id IS NULL THEN 'Chưa thiết lập KPI' "
            . "WHEN task.aot_id IS NOT NULL AND m.aom_head_approved = 0 THEN 'Chưa được TBP duyệt' "
            . "WHEN task.aot_id IS NOT NULL AND m.aom_staff_approved = 0 THEN 'Chưa tự đánh giá' "
            . "WHEN task.aot_id IS NOT NULL AND m.aom_head_approved = 1 THEN 'Chưa được TBP đánh giá' "
            . "WHEN task.aot_id IS NOT NULL AND m.aom_head_approved = 2 THEN 'Hoàn thành' "
            . "END)"
        );

        $select->from(array('p' => 'appraisal_office_to_do_root'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = s.team', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = s.department', array());
        $select->joinLeft(array('m' => 'appraisal_office_member'), 'm.aom_staff_id = p.to_staff AND m.aop_id = '.$params['plan_id'].' AND m.aom_is_deleted = 0', array());
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = m.aom_id AND f.aof_is_deleted = 0', array());
        $select->joinLeft(array('task' => 'appraisal_office_task'), 'task.aof_id = f.aof_id', array());
        
        $select->where("s.off_date IS NULL", NULL);
        $select->where("(p.is_del = 0 OR p.is_del IS NULL)", NULL);
        $select->where("p.from_staff <> p.to_staff", NULL);
        $select->where("p.is_prd = 1", NULL);
        $select->where("p.to_staff IS NOT NULL", NULL);
        
        if(!empty($params['name'])){
            $select->where("CONCAT(s.firstname, ' ', s.lastname) LIKE ?", "%".$params['name']."%");
        }
        
        if(!empty($params['code'])){
            $select->where("s.code LIKE ?", "%".$params['s.code']."%");
        }
        
        if(!empty($params['title'])){
            $select->where("s.title IN (?)", $params['title']);
        }
        
        if(!empty($params['team'])){
            $select->where("s.team IN (?)", $params['team']);
        }
        
        if(!empty($params['department'])){
            $select->where("s.department IN (?)", $params['department']);
        }
        
        if(!empty($params['status'])){
            $select->where("(CASE
                                WHEN task.aot_id IS NULL THEN 'Chưa thiết lập KPI' 
                                WHEN task.aot_id IS NOT NULL AND m.aom_head_approved = 0 THEN 'Chưa được TBP duyệt'
                                WHEN task.aot_id IS NOT NULL AND m.aom_staff_approved = 0 THEN 'Chưa tự đánh giá'
                                WHEN task.aot_id IS NOT NULL AND m.aom_head_approved = 1 THEN 'Chưa được TBP đánh giá'
                                WHEN task.aot_id IS NOT NULL AND m.aom_head_approved = 2 THEN 'Hoàn thành' 
                        END) = ?", $params['status']);
        }
        
        if($params['level'] == 2){
            $select->where("p.from_staff = ?", $params['staff_id']);
        }
        elseif($params['level'] == 3){
            $select->where("p.to_staff = ?", $params['staff_id']);
        }
        
        if($params['staff_id'] == 24979 OR $params['staff_id'] == 24980){
            $select->where("s.department = ?", 156);
        }
        $select->group('p.to_staff');
        
        
        if($_GET['dev'] == 1){
            echo $select;exit;
        }
        
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    public function updateInitRatioByField($fieldId)
    {
        $QField = new Application_Model_AppraisalOfficeField();
        $field = $QField->fetchRow($QField->getAdapter()->quoteInto('aof_id = ? and aof_is_deleted = 0', $fieldId));
        if(!$field) {
            return;
        }
        $memberId = $field['aom_id'];
        $fields = $QField->fetchAll($QField->getAdapter()->quoteInto('aom_id = ? and aof_is_deleted = 0', $memberId))->toArray();
        $fieldIds = [];
        foreach ($fields as $field) {
            $fieldIds[] = $field['aof_id'];
        }
        $QTask = new Application_Model_AppraisalOfficeTask();
        $tasks = $QTask->fetchAll($QTask->getAdapter()->quoteInto('aof_id IN (?) and aot_is_deleted = 0', $fieldIds))->toArray();
        $ratio = 0;
        foreach ($tasks as $task) {
            $ratio += intval($task['aot_ratio']);
        }
        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $this->update([
            'aom_updated_at' => date('Y-m-d H:i:s'),
            'aom_updated_by' => $userId,
            'aom_init_ratio' => $ratio
        ], $this->getAdapter()->quoteInto('aom_id = ? and aom_is_deleted = 0', $memberId));
    }
    
    
    public function getByPlanAndStaff($planId, $staffId)
    {
        $QCapacity = new Application_Model_DynamicSurveyCapacity();
            $capacity = $QCapacity->fetchAll([
                $QCapacity->getAdapter()->quoteInto('fk_staff = ? and aoc_is_deleted = 0', $staffId),
                $QCapacity->getAdapter()->quoteInto('fk_aop = ?', $planId),
            ])->toArray();
        
        return $capacity;
    }
    
    public function checkPrdComplete($planId, $staffId){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.aop_id', 
            'p.aom_staff_id', 
            'p.aom_staff_approved', 
            'p.aom_is_deleted'
        );

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);

        $select->where('p.aop_id = ?', $planId);
        $select->where('p.aom_staff_id = ?', $staffId);
        $select->where('p.aom_staff_approved = 0', NULL);
        $select->where('p.aom_is_deleted = 0', NULL);
        $result = $db->fetchAll($select);
        
        if($result){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function checkPrdHeadComplete($planId, $staffId){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.aop_id', 
            'p.aom_staff_id', 
            'p.aom_head_approved', 
            'p.aom_head_department_id'
        );

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);

        $select->where('p.aop_id = ?', $planId);
        $select->where('p.aom_head_department_id = ?', $staffId);
        $select->where('p.aom_head_approved = 0', NULL);
        $select->where('p.aom_is_deleted = 0', NULL);
        $result = $db->fetchAll($select);
        
        if($result){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function fetchPagination($planId, $userId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.staff_id'),
            'name' => "CONCAT(p.firstname,' ',p.lastname)",
        );

        $select->from(array('p' => 'sp_assessment_temp'), $arrCols);
        $select->joinLeft(array('n' => 'staff'), 'n.id = p.staff_id', array());

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getAllMemberApproved($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.aop_id', 
            'p.aom_staff_id', 
            'p.aom_head_department_id', 
            'p.aom_head_approved',
            'f.aof_is_deleted', 
            'f.aof_name', 
            'f.aom_id',
            't.aof_id', 
            't.aof_id', 
            't.aot_ratio', 
            't.aot_self_appraisal', 
            't.aot_head_department_appraisal', 
            't.aot_is_deleted', 
            's.star', 
            's.ratio', 
            'total_point' => 'SUM((t.aot_ratio*s.ratio)/100)',
            'star' => 'CASE WHEN SUM((t.aot_ratio*s.ratio)/100) >= 110 THEN 5
                             WHEN SUM((t.aot_ratio*s.ratio)/100) >= 100 THEN 4
                             WHEN SUM((t.aot_ratio*s.ratio)/100) >= 90 THEN 3
                             WHEN SUM((t.aot_ratio*s.ratio)/100) >= 70 THEN 2
                             WHEN SUM((t.aot_ratio*s.ratio)/100) < 70 THEN 1
                        END',
            'plan.aop_name',
            'fullname' => "CONCAT(staff.firstname, ' ', staff.lastname)",
            'title_name' => "title.name",
            "fullname_to" => "CONCAT(staff2.firstname, ' ', staff2.lastname)",
            "title_to_name" => "title2.name",
            "team_name" => "title_team.name",
            "department_name" => "title_department.name",
            "staff.joined_at",
            "joined_at_to" => "staff2.joined_at"
        );

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = p.aom_id', array());
        $select->joinLeft(array('t' => 'appraisal_office_task'), 't.aof_id = f.aof_id', array());
        $select->joinLeft(array('s' => 'appraisal_office_star'), 's.star = t.aot_head_department_appraisal', array());
        $select->joinLeft(array('plan' => 'appraisal_office_plan'), 'plan.aop_id = p.aop_id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.aom_staff_id', array());
        $select->joinLeft(array('title' => 'team'), 'title.id = staff.title', array());
        $select->joinLeft(array('title_team' => 'team'), 'title_team.id = staff.team', array());
        $select->joinLeft(array('title_department' => 'team'), 'title_department.id = staff.department', array());
        
        $select->joinLeft(array('staff2' => 'staff'), 'staff2.id = p.aom_head_department_id', array());
        $select->joinLeft(array('title2' => 'team'), 'title2.id = staff2.title', array());
        
        
        $select->where('p.aom_head_approved >= 0 AND s.ratio IS NOT NULL', NULL);
        $select->where('p.aom_is_deleted = 0', NULL);
        
        if(!empty($params['staff_id'])){
            $select->where('p.aom_staff_id = ?', $params['staff_id']);
        }
        
        if(!empty($params['staff_head_id'])){
            $select->where('p.aom_head_department_id = ?', $params['staff_head_id']);
        }
        
        if(!empty($params['department'])){
            $select->where("staff.department IN (?)", $params['department']);
        }
        
        if(!empty($params['team'])){
            $select->where("staff.team IN (?)", $params['team']);
        }
        
        if(!empty($params['title'])){
            $select->where("staff.title IN (?)", $params['title']);
        }
        
        if($params['level'] == 2){
            $select->where("p.aom_staff_id = ?", $params['staff_id']);
        }
        elseif($params['level'] == 3){
            $select->where("p.aom_head_department_id = ?", $params['staff_id']);
        }
        
        if($params['staff_id'] == 24979 OR $params['staff_id'] == 24980){
            $select->where("staff.department = ?", 156);
        }
        
        $select->group(['p.aop_id', 'p.aom_staff_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getAllMemberApprovedResult($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.aop_id', 
            'p.aom_staff_id', 
            'p.aom_head_department_id', 
            'p.aom_head_approved',
            'f.aof_is_deleted', 
            'f.aof_name', 
            'f.aom_id',
            't.aof_id', 
            't.aof_id', 
            't.aot_ratio', 
            't.aot_self_appraisal', 
            't.aot_head_department_appraisal', 
            't.aot_is_deleted', 
            's.star', 
            's.ratio', 
            'total_point' => 'SUM((t.aot_ratio*s.ratio)/100)',
            'star' => 'CASE WHEN SUM((t.aot_ratio*s.ratio)/100) >= 110 THEN 5
                             WHEN SUM((t.aot_ratio*s.ratio)/100) >= 100 THEN 4
                             WHEN SUM((t.aot_ratio*s.ratio)/100) >= 90 THEN 3
                             WHEN SUM((t.aot_ratio*s.ratio)/100) >= 70 THEN 2
                             WHEN SUM((t.aot_ratio*s.ratio)/100) < 70 THEN 1
                        END',
            'plan.aop_name',
            'plan.aop_name_eng',
            'fullname' => "CONCAT(staff.firstname, ' ', staff.lastname)",
            'title_name' => "title.name",
            "fullname_to" => "CONCAT(staff2.firstname, ' ', staff2.lastname)",
            "title_to_name" => "title2.name",
            "team_name" => "title_team.name",
            "department_name" => "title_department.name",
            "staff.joined_at",
            "joined_at_to" => "staff2.joined_at"
        );

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = p.aom_id', array());
        $select->joinLeft(array('t' => 'appraisal_office_task'), 't.aof_id = f.aof_id', array());
        $select->joinLeft(array('s' => 'appraisal_office_star'), 's.star = t.aot_head_department_appraisal', array());
        $select->joinLeft(array('plan' => 'appraisal_office_plan'), 'plan.aop_id = p.aop_id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.aom_staff_id', array());
        $select->joinLeft(array('title' => 'team'), 'title.id = staff.title', array());
        $select->joinLeft(array('title_team' => 'team'), 'title_team.id = staff.team', array());
        $select->joinLeft(array('title_department' => 'team'), 'title_department.id = staff.department', array());
        
        $select->joinLeft(array('staff2' => 'staff'), 'staff2.id = p.aom_head_department_id', array());
        $select->joinLeft(array('title2' => 'team'), 'title2.id = staff2.title', array());
        
        
        $select->where('p.aom_head_approved >= 0 AND s.ratio IS NOT NULL', NULL);
        $select->where('p.aom_is_deleted = 0', NULL);
        
        if($params['level'] != 1 AND $params['staff_id'] != 1451){
            $select->where('p.aom_head_department_id = ?', $params['staff_head_id']);
        }
        
        if(!empty($params['department'])){
            $select->where("staff.department IN (?)", $params['department']);
        }
        
        if(!empty($params['team'])){
            $select->where("staff.team IN (?)", $params['team']);
        }
        
        if(!empty($params['title'])){
            $select->where("staff.title IN (?)", $params['title']);
        }
        
        if(!empty($params['view_result_head']) AND $params['staff_id'] != 1451 AND $params['staff_id'] != 7){
            $select->where("p.aom_head_department_id = ?", $params['staff_head_id']);
        }
        
        if($params['staff_id'] == 24979 OR $params['staff_id'] == 24980){
            $select->where("staff.department = ?", 156);
        }
        
        if($params['staff_id'] == 1451){
            $select->where("staff.department = ?", 149);
        }
        
        if($params['staff_id'] == 7){
            $select->where("staff.department = ?", 154);
        }
        
        $select->group(['p.aop_id', 'p.aom_staff_id']);
        
        if($_GET['dev'] == 1){
            echo $select;exit;
        }
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    
}