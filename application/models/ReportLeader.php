<?php
class Application_Model_ReportLeader extends Zend_Db_Table_Abstract
{
	protected $_name = 'report_kpi_leader';

	public function fetch($params,$area){
		$db = Zend_Registry::get('db');

		$date = $params['month'].'-'.$params['year'];

		// var_dump($area);die;

        $select = $db->select()
			->from(array('p'=>$this->_name),array('p.*'))
            ->where('p.kpi_date = ?', $date);

        if (isset($params['area']) and $params['area'])
            $select->where('p.area_id = ?', $params['area']);

        $result = $db->fetchAll($select);

        // echo "<pre>";print_r($result);die;
        
        return $result;
	}

	public function getTotalSellout($params){
		$db = Zend_Registry::get('db');

		$date = $params['month'].'-'.$params['year'];

		// var_dump($params);die;

        $select = $db->select()
			->from(array('p'=>$this->_name),array('total_sellout' => new Zend_Db_Expr("SUM(p.total_sellout)"),'total_sellout_active' => new Zend_Db_Expr("SUM(p.total_sellout - p.total_sellout_not_active)")))
			// ->from ($this->_name, array("sellout", "total_sellout" => "sum(total_sellout)"));
            ->where('p.kpi_date = ?', $date);

        if (isset($params['area']) and $params['area'])
            $select->where('p.area_id = ?', $params['area']);
            
        // $select->group('p.area_id')

        $result = $db->fetchRow($select);

        // echo "<pre>";print_r($result);die;
        
        return $result;	
	}

}