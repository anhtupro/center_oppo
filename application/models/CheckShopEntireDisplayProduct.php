<?php
class Application_Model_CheckShopEntireDisplayProduct extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_display_product';
    protected $_schema = DATABASE_TRADE;

    public function get($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_TRADE.'.check_shop_entire_display_product'], [
                'p.*'
            ])
            ->where('p.check_shop_id = ?', $params['check_shop_id']);

        $result = $db->fetchALl($select);

        foreach ($result as $element) {
            $list [$element['brand_id']] [] = $element;
        }

        return $list;
    }
}    