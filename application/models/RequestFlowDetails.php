<?php
class Application_Model_RequestFlowDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_flow_details';
    
    public function getInfo($flow_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.type", 
            "p.department_id", 
            "p.company_id",
            "p.team_id", 
            "p.from_price", 
            "p.to_price", 
            "department_name" => "t.name", 
            "team_name" => "t2.name",
            'group_name' => 'r.title',
            'p.has_leader',
            'request_type_name' => "type.title",
            "p.request_type",
            "p.request_type_group"
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.department_id', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.team_id', array());
        $select->joinLeft(array('r' => 'request_type_group'), 'r.id = p.request_type_group', array());
        $select->joinLeft(array('type' => 'request_type'), 'type.id = p.request_type', array());

        $select->where('p.id = ?', $flow_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    
    public function getListConfirm($flow_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.type", 
            "p.department_id", 
            "p.team_id", 
            "p.from_price", 
            "p.to_price",
            "flow_details_id" => "f.id",
            "f.step",
            "f.staff_id",
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)",
            "department_name" => "t.name",
            "team_name" => "t2.name",
            "title_name" => "t3.name"
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('f' => 'request_flow_details'), 'f.flow_id = p.id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = f.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 's.department = t.id', array());
        $select->joinLeft(array('t2' => 'team'), 's.team = t2.id', array());
        $select->joinLeft(array('t3' => 'team'), 's.title = t3.id', array());
        
        $select->where('f.is_del = 0');
        
        $select->where('p.id = ?', $flow_id);
        $result = $db->fetchAll($select);

        return $result;
    }

}