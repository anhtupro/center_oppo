<?php
class Application_Model_AppraisalSaleLevel extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_sale_level';
    protected $_primary = 'asl_id';

    /**
     * @param $skillId
     * @return bool
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function reindexPoint($skillId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql  = 'SET @counter = 0; update appraisal_sale_level set asl_point = @counter := @counter + 1 where fk_ass = :fk_ass and asl_status = 0 and asl_is_deleted = 0 order by asl_created_at asc;';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("fk_ass", $skillId, PDO::PARAM_INT);
        $result = $stmt->execute();
        $stmt->closeCursor();
        return $result;
    }
}