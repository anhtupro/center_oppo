<?php
class Application_Model_AppContractorQuantity extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_contractor_quantity';
    protected $_schema = DATABASE_TRADE;

    function getDataByCampaign($params){
        if( empty($params['campaign_id']) AND empty($params['campaign_id']) ){
            return false;
        }


        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'contractor_quantity_id'     => 'p.id',
            'p.category_id', 
            'p.quantity', 
            'quantity_in'                => 'COUNT(DISTINCT i.imei_sn)', 
            'quantity_in'                => 'COUNT(DISTINCT o.imei_sn)', 
            'category_name'              => 'c.name',
            'contructors_name'           => 'con.name',
            'photo'                      => 'c.photo',
            'contractor_id'              => 'p.contractor_id',
            'contractor_name'            => "con.name",
            'campaign_id'                => "p.campaign_id",
            'quantity'                   => "p.quantity",
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_quantity'), $arrCols);
        $select->joinLeft(array('i'=> DATABASE_TRADE.'.app_contractor_in'), 'i.contractor_id = p.contractor_id AND i.campaign_id = p.campaign_id AND i.category_id = p.category_id',array());
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_contractor_out'), 'o.contractor_id = p.contractor_id AND o.campaign_id = p.campaign_id AND o.category_id = p.category_id',array());
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id',array());
        $select->joinLeft(array('con'=> DATABASE_TRADE.'.contructors'), 'con.id = p.contractor_id',array());

        $select->where('p.campaign_id = ?', $params['campaign_id']);

        $select->group('p.id');

        $result = $db->fetchAll($select);
        
        return $result;
    }

    function getCampaignCategory($params){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'contractor_quantity_id'     => 'p.id',
            'p.category_id', 
            'p.quantity', 
            'imei_in'       => 'COUNT(DISTINCT i.imei_sn)', 
            'imei_out'      => 'COUNT(DISTINCT o.imei_sn)', 
            'category_name' => 'c.name', 
            'contructors_name'  => 'con.name',
            'photo'             => 'c.photo',
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_quantity'), $arrCols);
        $select->joinLeft(array('i'=> DATABASE_TRADE.'.app_contractor_in'), 'i.contractor_id = p.contractor_id AND i.campaign_id = p.campaign_id AND i.category_id = p.category_id',array());
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_contractor_out'), 'o.contractor_id = p.contractor_id AND o.campaign_id = p.campaign_id AND o.category_id = p.category_id',array());
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id',array());
        $select->joinLeft(array('con'=> DATABASE_TRADE.'.contructors'), 'con.id = p.contractor_id',array());

        $select->where('p.contractor_id = ?', $params['contractor_id']);
        $select->where('p.campaign_id = ?', $params['campaign_id']);

        $select->group('p.id');

        $result = $db->fetchAll($select);

        return $result;
    }

    function getContractor($params){

        if($params['title'] != CONTRACTOR){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.*'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);

        if(isset($params['staff_id']) and $params['staff_id'] AND ($params['title'] == CONTRACTOR) ){
            $select->where('p.user = ?', $params['staff_id']);
        }

        $result = $db->fetchRow($select);

        return $result;
    }

    function getContractorIn($params){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.id', 
            'p.category_id',
            'p.imei_sn',
            'category_name' => 'c.name',
            
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_in'), $arrCols);
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id',array());
    

        $result = $db->fetchAll($select);

        return $result;
    }

    function getContractorOut($params){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.id',
            'p.contractor_quantity_id',  
            'p.category_id',
            'p.imei_sn',
            'category_name' => 'c.name',
            
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_in'), $arrCols);
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id',array());
        

        if( isset($params['contractor_quantity_id']) and $params['contractor_quantity_id'] ){
            $select->where('p.contractor_quantity_id = ?', $params['contractor_quantity_id']);
        }

        $select->where('p.out_date IS NOT NULL');

        $result = $db->fetchAll($select);

        return $result;
    }

    function getContractorAll(){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.*',
            
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);

        $result = $db->fetchAll($select);

        $data = [];

        foreach ($result as $key => $value) {
            $data[$value['id']] = $value['short_name'];
        }

        return $data;
    }

    function getContractorQuantityOut($params){
        if( empty($params['campaign_id']) AND empty($params['campaign_id']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'area_id'            => 'o.area_id',
            'id'                 => 'p.id',
            'quantity'           => "SUM(o.quantity)",
            'contractor_out_id'  => "o.id",
            'status'             => "o.status"
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_quantity'), $arrCols);
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_contractor_out'), 'o.contractor_quantity_id = p.id',array());
        
        if(isset($params['campaign_id']) and $params['campaign_id']){
            $select->where('p.campaign_id = ?', $params['campaign_id']);
        }

        $select->group(['p.id', 'o.area_id']);

        $result = $db->fetchAll($select);

        return $result;
    }

    function getInOutContractor($params){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'area_id'           => 'o.area_id',
            'quantity'          => 'o.quantity',
            'quantity_out'      => 'o.quantity',
            'quantity_out_area' => "COUNT(i.out_area_id)",
            "quantity_in"       => "COUNT(i.imei_sn)",
            "list_imei"         => "GROUP_CONCAT(i.imei_sn)"
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_quantity'), $arrCols);
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_contractor_out'), 'o.contractor_quantity_id = p.id',array());
        $select->joinLeft(array('i'=> DATABASE_TRADE.'.app_contractor_in'), 'i.contractor_quantity_id = p.id AND i.out_area_id = o.area_id',array());
        
        if(isset($params['contractor_quantity_id']) and $params['contractor_quantity_id']){
            $select->where('p.id = ?', $params['contractor_quantity_id']);
        }

        $select->group('o.area_id');

        $result = $db->fetchAll($select);

        return $result;
    }

    function getContractorQuantity($params){
        $db = Zend_Registry::get('db');

        $arrCols = array(
            'p.contractor_id', 
            'p.category_id', 
            'p.quantity', 
            'imei_in'           => 'COUNT(DISTINCT i.imei_sn)', 
            'imei_out'          => 'COUNT(DISTINCT o.imei_sn)', 
            'category_name'     => 'c.name', 
            'contractor_name'   => 'con.name',
            'stock_quantity'    => 'COUNT(DISTINCT i.imei_sn) - COUNT(DISTINCT o.imei_sn)',
            'contractor_quantity_id'    => 'p.id'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_quantity'), $arrCols);
        $select->joinLeft(array('i'=> DATABASE_TRADE.'.app_contractor_in'), 'i.contractor_id = p.contractor_id AND i.campaign_id = p.campaign_id AND i.category_id = p.category_id',array());
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_contractor_out'), 'o.contractor_id = p.contractor_id AND o.campaign_id = p.campaign_id AND o.category_id = p.category_id',array());
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id',array());
        $select->joinLeft(array('con'=> DATABASE_TRADE.'.contructors'), 'con.id = p.contractor_id',array());

        $select->where('p.campaign_id = ?', $params['campaign_id']);

        $select->where('p.category_id = ?', $params['category_id']);

        $select->group('p.contractor_id');

        $result = $db->fetchAll($select);

        return $result;
    }

    function getAreaContractorQuantity($params){
        $db = Zend_Registry::get('db');

        $arrCols_sub = array(
            'o.campaign_id',
            'p.category_id',
            'p.store_id',
            'r.area_id',
            'quantity' => 'SUM(p.quantity)'
        );

        $select_sub = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_order_details'), $arrCols_sub);
        $select_sub->joinLeft(array('o'=> DATABASE_TRADE.'.app_order'), 'o.id = p.order_id',array());
        $select_sub->joinLeft(array('s'=> 'store'), 's.id = p.store_id',array());
        $select_sub->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market',array());
        $select_sub->joinLeft(array('i'=> DATABASE_TRADE.'.app_area_in'), 'i.campaign_id = o.campaign_id AND i.category_id = p.category_id',array());
        $select_sub->where('p.category_id = ?', $params['category_id']);
        $select_sub->where('o.campaign_id = ?', $params['campaign_id']);
        $select_sub->group('r.area_id');


        $arrCols = array(
            'd.id',
            'd.contractor_id',
            'p.campaign_id',
            'd.category_id',
            'p.created_at',
            'p.created_by',
            'd.area_id',
            'd.quantity'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_shipment'), $arrCols);
        $select->joinLeft(array('d'=> DATABASE_TRADE.'.app_shipment_details'), 'd.shipment_id = p.id', array());
        
        //SELECT MAIN
        $arr_main = array(
            'p.campaign_id', 
            'p.category_id', 
            'p.area_id', 
            'quantity'       => 'p.quantity', 
            'quantity_out'   => 'SUM(d.quantity)'
        );
        $select_main = $db->select();
        $select_main->from(array('p'=> new Zend_Db_Expr('(' . $select_sub . ')')), $arr_main);
        $select_main->joinLeft(array('d'=> new Zend_Db_Expr('(' . $select . ')')), 'd.campaign_id = p.campaign_id AND d.category_id = p.category_id', array());
        //END SELECT MAIN

        $select_main->group('p.area_id');

        $result = $db->fetchAll($select_main);

        return $result;
    }

    function checkImeiScan($params){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.shipment_id', 
            'p.area_id', 
            'p.category_id', 
            'p.quantity', 
            'p.price', 
            'p.contractor_id', 
            's.campaign_id', 
            'imei_in'       => 'COUNT(i.imei_sn)', 
            'imei_out'      => 'COUNT(o.imei_sn)'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_shipment_details'), $arrCols);
        $select->joinLeft(array('s'=> DATABASE_TRADE.'.app_shipment'), 's.id = p.shipment_id',array());
        $select->joinLeft(array('i'=> DATABASE_TRADE.'.app_contractor_in'), 'i.campaign_id = s.campaign_id AND i.category_id = p.category_id AND i.contractor_id = p.contractor_id',array());
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_contractor_out'), 'o.campaign_id = s.campaign_id AND o.category_id = p.category_id AND o.contractor_id = p.contractor_id AND o.imei_sn = i.imei_sn',array());
        
        $select->where('p.id = ?', $params['shipment_details_id']);

        $select->where('i.imei_sn = ?', $params['imei_sn']);

        $result = $db->fetchRow($select);

        return $result;
    }

}