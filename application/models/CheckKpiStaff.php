<?php

use http\Params;

class Application_Model_CheckKpiStaff extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_kpi_staff';

    public function getLechTiming($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => 'check_kpi_staff'], [
                'c.*',
                'staff_code' => 'st.code',
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'title_name' => 't.name',
                'area_name' => 'a.name'
            ])
            ->joinLeft(['st' => 'staff'], 'st.id = c.staff_id', [])
            ->joinLeft(['t' => 'team'], 'c.title = t.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = c.area_id', [])
            ->where('c.type = ?', 1)
            ->where('c.month = ?', $params['month'])
            ->where('c.year = ?', $params['year']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getKhongCong($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => 'check_kpi_staff'], [
                'c.*',
                'staff_code' => 'st.code',
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'title_name' => 't.name',
                'area_name' => 'a.name'
            ])
            ->joinLeft(['st' => 'staff'], 'st.id = c.staff_id', [])
            ->joinLeft(['t' => 'team'], 'c.title = t.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = c.area_id', [])
            ->where('c.type = ?', 2)
            ->where('c.month = ?', $params['month'])
            ->where('c.year = ?', $params['year']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getKhacChucDanh($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => 'check_kpi_staff'], [
                'c.*',
                'staff_code' => 'st.code',
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'title_name' => 't.name',
                'area_name' => 'a.name'
            ])
            ->joinLeft(['st' => 'staff'], 'st.id = c.staff_id', [])
            ->joinLeft(['t' => 'team'], 'c.title = t.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = c.area_id', [])
            ->where('c.type = ?', 3)
            ->where('c.month = ?', $params['month'])
            ->where('c.year = ?', $params['year']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function updateKpiPg($params)
    {
        $db = Zend_Registry::get('db');

        // update imei kpi và kpi log
        $sql = "
			UPDATE imei_kpi i 
			LEFT JOIN kpi_log l ON i.imei_sn = l.imei_sn
			SET i.pg_id = 0, i.kpi_pg = 0, l.pg_id = 0, l.kpi_pg = 0
			WHERE (i.timing_date BETWEEN :from_date_time AND :to_date_time)
			AND i.pg_id = :staff_id
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date_time', $params['from_date_time']);
        $stmt->bindParam('to_date_time', $params['to_date_time']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;


        // update kpi_by_staff
        $sql = "
			UPDATE kpi_by_model km 
			JOIN kpi_by_staff ks ON km.id = ks.kpi_by_model_id
			SET ks.pg_id = 0, ks.kpi_pg = 0
			WHERE (km.timing_date BETWEEN :from_date AND :to_date)
			AND ks.pg_id = :staff_id
			AND ks.type = 0
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;

    }

    public function getSelloutPg($params)
    {
        $db = Zend_Registry::get('db');
        $sql = "
			SELECT SUM(ks.qty) as sellout
			FROM kpi_by_model km 
			JOIN kpi_by_staff ks ON km.id = ks.kpi_by_model_id
			WHERE (km.timing_date BETWEEN :from_date AND :to_date)
			AND ks.pg_id = :staff_id
			AND ks.type = 0
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = null;

        return $data['sellout'];
    }


    public function updateKpiConsultant($params, $type_pg)
    {
        $db = Zend_Registry::get('db');

        // nếu là consultant thì update bảng imei_kpi_staff
            $sql = "
			UPDATE imei_kpi_staff i 
			SET i.staff_id = 0, i.kpi = 0
			WHERE (i.timing_date BETWEEN :from_date_time AND :to_date_time)
			AND i.staff_id = :staff_id
			AND i.type = 7
			";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('from_date_time', $params['from_date_time']);
            $stmt->bindParam('to_date_time', $params['to_date_time']);
            $stmt->bindParam('staff_id', $params['staff_id']);
//            $stmt->bindParam('type_pg', $type_pg);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = null;

        // update kpi_by_staff
        $sql = "
			UPDATE kpi_by_model km 
			JOIN kpi_by_staff ks ON km.id = ks.kpi_by_model_id
			SET ks.pg_id = 0, ks.kpi_pg = 0
			WHERE (km.timing_date BETWEEN :from_date AND :to_date)
			AND ks.pg_id = :staff_id
			AND ks.type = 7
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
    }

    public function getSelloutConsultant($params)
    {
        $db = Zend_Registry::get('db');

        $sql = "
			SELECT SUM(ks.qty) as sellout
			FROM kpi_by_model km 
			JOIN kpi_by_staff ks ON km.id = ks.kpi_by_model_id
			WHERE (km.timing_date BETWEEN :from_date AND :to_date)
			AND ks.pg_id = :staff_id
			AND ks.type = 7
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = null;

        return $data['sellout'];
    }

    public function updateKpiSale($params)
    {
        $db = Zend_Registry::get('db');

        // update imei kpi và kpi log
        $sql = "
			UPDATE imei_kpi i 
			LEFT JOIN kpi_log l ON i.imei_sn = l.imei_sn
			SET i.sale_id = 0, i.kpi_sale = 0, l.sale_id = 0, l.kpi_sale = 0
			WHERE (i.timing_date BETWEEN :from_date_time AND :to_date_time)
			AND i.sale_id = :staff_id
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date_time', $params['from_date_time']);
        $stmt->bindParam('to_date_time', $params['to_date_time']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;

        // update kpi_by_model
        $sql = "
		   UPDATE kpi_by_model km 
		   SET km.sale_id = 0, km.kpi_sale = 0
		    WHERE (km.timing_date BETWEEN :from_date AND :to_date)
		    AND km.sale_id = :staff_id
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
    }

    public function getSelloutSale($params)
    {
        $db = Zend_Registry::get('db');

        // sellout khác tgdđ
        $sql = "
			SELECT COUNT(i.imei_sn) as sellout
			FROM imei_kpi i 
			WHERE (i.timing_date BETWEEN :from_date_time AND :to_date_time)
			AND i.sale_id = :staff_id
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date_time', $params['from_date_time']);
        $stmt->bindParam('to_date_time', $params['to_date_time']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $data_diff_tgdd = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = null;


        // sellout tgdđ
        $sql = "
		   SELECT SUM(km.qty)  as sellout
		   FROM kpi_by_model km 
		    WHERE (km.timing_date BETWEEN :from_date AND :to_date)
		    AND km.sale_id = :staff_id
		    AND km.status = 1
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $data_tgdd = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = null;

        $sellout = $data_diff_tgdd['sellout'] + $data_tgdd['sellout'];

        return $sellout;

    }

    public function updateKpiStoreLeader($params)
    {
        $db = Zend_Registry::get('db');

        // update imei kpi và kpi log
        $sql = "
			UPDATE imei_kpi i 
			LEFT JOIN kpi_log l ON i.imei_sn = l.imei_sn
			SET i.store_leader_id = 0, i.kpi_store_leader = 0, l.store_leader_id = 0, l.kpi_store_leader = 0
			WHERE (i.timing_date BETWEEN :from_date_time AND :to_date_time)
			AND i.store_leader_id = :staff_id
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date_time', $params['from_date_time']);
        $stmt->bindParam('to_date_time', $params['to_date_time']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;


        // update kpi_by_model
        $sql = "
		   UPDATE kpi_by_model km 
		   SET km.store_leader_id = 0, km.kpi_store_leader = 0
		    WHERE (km.timing_date BETWEEN :from_date AND :to_date)
		    AND km.store_leader_id = :staff_id
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
    }

    public function getSelloutStoreLeader($params)
    {
        $db = Zend_Registry::get('db');

        $sql = "
		   SELECT SUM(km.qty) as sellout 
		   FROM kpi_by_model km 
		    WHERE (km.timing_date BETWEEN :from_date AND :to_date)
		    AND km.store_leader_id = :staff_id
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->bindParam('staff_id', $params['staff_id']);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = null;

        return $data['sellout'];
    }

    public function createTableLog($params)
    {

        $db = Zend_Registry::get("db");
        $table_kpi_by_model = 'backup.kpi_by_model_cks_' . $params['month'] . '_' . $params['year'];
        $table_kpi_by_staff = 'backup.kpi_by_staff_cks_' . $params['month'] . '_' . $params['year'];
        $table_imei_kpi = 'backup.imei_kpi_cks_' . $params['month'] . '_' . $params['year'];

        // xóa trước khi tạo
        $sql_drop_1 = "DROP TABLE IF EXISTS " . $table_kpi_by_model ;
        $sql_drop_2 = "DROP TABLE IF EXISTS " . $table_kpi_by_staff ;
        $sql_drop_3 = "DROP TABLE IF EXISTS " . $table_imei_kpi ;

        $stmt_drop_1 = $db->prepare($sql_drop_1);
        $stmt_drop_2 = $db->prepare($sql_drop_2);
        $stmt_drop_3 = $db->prepare($sql_drop_3);

        $stmt_drop_1->execute();
        $stmt_drop_2->execute();
        $stmt_drop_3->execute();

        $stmt_drop_1->closeCursor();
        $stmt_drop_2->closeCursor();
        $stmt_drop_3->closeCursor();


        // kpi by model
        $sql = "
			CREATE TABLE " . $table_kpi_by_model .
            " SELECT * FROM kpi_by_model 
			WHERE timing_date BETWEEN :p_from_date AND :p_to_date
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('p_from_date', $params['from_date']);
        $stmt->bindParam('p_to_date', $params['to_date']);

        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;


        // kpi by staff

        $sql = "
			CREATE TABLE " . $table_kpi_by_staff .
            " SELECT ks.* FROM kpi_by_model km
			  JOIN kpi_by_staff ks ON km.id = ks.kpi_by_model_id
			WHERE km.timing_date BETWEEN :p_from_date AND :p_to_date
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('p_from_date', $params['from_date']);
        $stmt->bindParam('p_to_date', $params['to_date']);

        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;


        // imei kpi

        $sql = "
			CREATE TABLE " . $table_imei_kpi .
            " SELECT km.* FROM imei_kpi km
			WHERE km.timing_date BETWEEN :p_from_date AND :p_to_date
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('p_from_date', $params['from_date']);
        $stmt->bindParam('p_to_date', $params['to_date']);

        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;


    }

    public function exportLechTiming($data)
    {

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();

        $heads = array(
            'Staff ID',
            'Staff Code',
            'Staff Name',
            'Title',
            'Area',
            'Off date',
            'Sellout báo số',
            'Sellout được tính KPI'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        $i = 1;

        foreach ($data as $item) {
            $alpha = 'A';

            $sheet->setCellValue($alpha++ . $index, $item['staff_id']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_code']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_name']);
            $sheet->setCellValue($alpha++ . $index, $item['title_name']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);
            $sheet->setCellValue($alpha++ . $index, $item['off_date'] ? date('Y-m-d', strtotime($item['off_date'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['sellout_timing']);
            $sheet->setCellValue($alpha++ . $index, $item['sellout_pg']);

            $index++;
        }

        $filename = 'Lệch timing';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function exportKhongCong($data)
    {

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();

        $heads = array(
            'Staff ID',
            'Staff Code',
            'Staff Name',
            'Title',
            'Area',
            'Off date',
            'Sellout PG',
            'Sellout Sale',
            'Sellout Consultant',
            'Sellout Store Leader'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        $i = 1;

        foreach ($data as $item) {
            $alpha = 'A';

            $sheet->setCellValue($alpha++ . $index, $item['staff_id']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_code']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_name']);
            $sheet->setCellValue($alpha++ . $index, $item['title_name']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);
            $sheet->setCellValue($alpha++ . $index, $item['off_date'] ? date('Y-m-d', strtotime($item['off_date'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['sellout_pg']);
            $sheet->setCellValue($alpha++ . $index, $item['sellout_sale']);
            $sheet->setCellValue($alpha++ . $index, $item['sellout_consultant']);
            $sheet->setCellValue($alpha++ . $index, $item['sellout_store_leader']);


            $index++;
        }

        $filename = 'Không công';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function exportKhacChucDanh($data)
    {

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();

        $heads = array(
            'Staff ID',
            'Staff Code',
            'Staff Name',
            'Title',
            'Area',
            'Off date',
            'Transfer date',
            'Sellout PG',
            'Sellout Sale',
            'Sellout Consultant',
            'Sellout Store Leader'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        $i = 1;

        foreach ($data as $item) {
            $alpha = 'A';

            $sheet->setCellValue($alpha++ . $index, $item['staff_id']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_code']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_name']);
            $sheet->setCellValue($alpha++ . $index, $item['title_name']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);
            $sheet->setCellValue($alpha++ . $index, $item['off_date'] ? date('Y-m-d', strtotime($item['off_date'])) : '');
            $sheet->setCellValue($alpha++ . $index, str_replace(";", " ; ", $item['transfer_note']));
            $sheet->setCellValue($alpha++ . $index, $item['sellout_pg']);
            $sheet->setCellValue($alpha++ . $index, $item['sellout_sale']);
            $sheet->setCellValue($alpha++ . $index, $item['sellout_consultant']);
            $sheet->setCellValue($alpha++ . $index, $item['sellout_store_leader']);


            $index++;
        }

        $filename = 'Khác chức danh';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

}