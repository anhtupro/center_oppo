<?php
class Application_Model_TrainerInventory extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_inventory';

    public function updateInventory($id){



        $result = array(
            'status'  => 1,
            'message' => 'Done' 
        );

        $QTrainerOrder          = new Application_Model_TrainerOrder();
        $QTrainerInventory      = new Application_Model_TrainerInventory();

        $where_order = array();
        $where_order = $QTrainerOrder->getAdapter()->quoteInto('id = ?', $id);
        $order       = $QTrainerOrder->fetchRow($where_order);

        $asset_id    = $order['asset_id'];
        $object      = $order['object'];

        //Stock người gửi
        $order_from = $order['order_from'];
        if(isset($order_from) and $order_from){
            $where = array();
            $where[] = $QTrainerInventory->getAdapter()->quoteInto('trainer_id = ?', $order_from);
            $where[] = $QTrainerInventory->getAdapter()->quoteInto('asset_id = ?', $asset_id);
            $where[] = $QTrainerInventory->getAdapter()->quoteInto('object = ?', $object);

            $inventory_from = $QTrainerInventory->fetchRow($where);

            if(isset($inventory_from) and $inventory_from){
                $data = array(
                    'status' => 3
                );
                $QTrainerInventory->update($data, $where);
            }
        }

        //Stock người nhận
        $order_to = $order['order_to'];
        if(isset($order_to) and $order_to){
            $data_to = array(
                'trainer_id' => $order_to,
                'asset_id'   => $asset_id,
                'object'     => $object,
                'date_in'   => date('Y-m-d H:i:s'),
                'status'    => 1,
                'order_id'  => $id
            );
            $id = $QTrainerInventory->insert($data_to);
            if($id){
                return $result;
            }
        }
        else{
            $result['status'] = 2;
            $result['message'] = 'NOT trainer TO!!!';
            return $result;
        }
    }

    public function fetchInventoryByUser($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'asset_name' => 'a.name',
            'p.*'
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('a'=>'trainer_type_asset'), 'a.id = p.asset_id',array())
            ->joinLeft(array('o'=>'trainer_order'), 'o.id = p.order_id',array('note_model' => 'o.note'));

        $select->where('p.status IN (1)', null);

        if(isset($params['trainer_id']) and $params['trainer_id']){
            $select->where('p.trainer_id = ?', $params['trainer_id']);
        }

        if(isset($staff_id) and $staff_id){
            $select->where('p.order_to = ?', $staff_id);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInventoryByStaff($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'trainer_id' => 'p.trainer_id',
            'sum'        => 'SUM(IF(p.`status` = 1, 1,0))',
            'current'    => "SUM(IF(p.`status` = 2, 1,0))"
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('s'=>'staff'), 's.id = p.trainer_id',array('name' => "CONCAT(s.firstname, ' ', s.lastname)"));

        if(isset($params['name']) and $params['name']){
            $select->where('CONCAT(s.firstname, " ", s.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        $select->group('p.trainer_id');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAllInventory($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'model'        => 't.name',
            'fullname'     => "CONCAT(s.firstname, ' ',s.lastname)",
            'object'       => "p.object",
            'date_in'      => 'p.date_in',
            'note'         => 'o.note'
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('t'=>'trainer_type_asset'), 't.id = p.asset_id',array())
            ->joinLeft(array('s'=>'staff'), 's.id = p.trainer_id',array())
            ->joinLeft(array('o'=>'trainer_order'), 'o.id = p.order_id',array());

        $select->where('p.status = ?', 1);

        if(isset($params['name']) and $params['name']){
            $select->where('CONCAT(s.firstname, " ", s.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        $select->order('p.trainer_id');
        $result = $db->fetchAll($select);
        return $result;
    }


    public function getInventoryByAsset($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'asset_id'   => 'p.asset_id',
            'name'       => "a.name",
            'current'    => "SUM(IF(p.`status` = 2, 1,0))",
            "sum"        => "SUM(IF(p.`status` = 1, 1, 0))"
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('a' => 'trainer_type_asset'), 'p.asset_id = a.id',array());

        if(isset($params['name']) and $params['name']){
            $select->where("a.`name` LIKE ?", '%'.$params['name'].'%');
        }

        $select->group('p.asset_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function fetchInventoryDetailsByUser($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'p.trainer_id',
            'a.name',
            'p.object',
            'p.date_in',
            'o.note'
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('a'=>'trainer_type_asset'), 'a.id = p.asset_id',array())
            ->joinLeft(array('o'=>'trainer_order'), 'o.id = p.order_id',array());

        $select->where('p.`status` IN (1)', null);
        $select->where('p.trainer_id = ?', $params['staff_id']);

        $result = $db->fetchAll($select);
    
        return $result;
    }

    public function fetchInventoryDetailsByAsset($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'p.trainer_id',
            'name' => "CONCAT(s.firstname, ' ', s.lastname)",
            'p.object',
            'p.date_in',
            'o.note'
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('s'=>'staff'), 's.id = p.trainer_id',array())
            ->joinLeft(array('o'=>'trainer_order'), 'o.id = p.order_id',array());

        $select->where('p.`status` IN (1,2)', null);
        $select->where('p.asset_id = ?', $params['asset_id']);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function fetchCurrentDetailsByUser($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'p.trainer_id',
            'a.name',
            'p.object',
            'o.created_at',
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            'p.date_out',
            'o.confirm_by',
            'p.note'
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('a'=>'trainer_type_asset'), 'a.id = p.asset_id',array())
            ->joinLeft(array('o'=>'trainer_order'), 'o.order_from = p.trainer_id',array())
            ->joinLeft(array('s'=>'staff'), 's.id = o.order_to',array());

        $select->where('p.`status` = ?', 2);
        $select->where('p.date_out IS NOT NULL ', null);
        $select->where('p.trainer_id = ?', $params['staff_id']);
        $select->where('o.order_to_confirm_at IS NULL', null);
        $select->where('o.del IS NULL OR o.del <> ?', 1);

        $select->group('o.id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function fetchCurrentDetailsByAsset($params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'p.trainer_id',
            'a.name',
            'p.object',
            'o.created_at',
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            'fullname_from' => "CONCAT(s2.firstname, ' ', s2.lastname)",
            'p.date_out',
            'o.confirm_by',
            'p.note'
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_inventory'), $cols)
            ->joinLeft(array('a'=>'trainer_type_asset'), 'a.id = p.asset_id',array())
            ->joinLeft(array('o'=>'trainer_order'), 'o.order_from = p.trainer_id',array())
            ->joinLeft(array('s'=>'staff'), 's.id = o.order_to',array())
            ->joinLeft(array('s2'=>'staff'), 's2.id = o.order_from',array());

        $select->where('p.`status` = ?', 2);
        $select->where('p.date_out IS NOT NULL ', null);
        $select->where('p.asset_id = ?', $params['asset_id']);
        $select->where('o.order_to_confirm_at IS NULL', null);
        $select->where('o.del IS NULL OR o.del <> ?', 1);

        $select->group('o.id');

        $result = $db->fetchAll($select);
        return $result;
    }
}




























