<?php
class Application_Model_Title extends Zend_Db_Table_Abstract
{
	protected $_name = 'title';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    public function getStaffByTtitle($title)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['st' => 'staff'], [
                'st.id'
            ])
            ->where('st.title = ?', $title)
            ->where('st.off_date IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

}                                                      
