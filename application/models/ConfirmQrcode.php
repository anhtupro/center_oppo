<?php
class Application_Model_ConfirmQrcode extends Zend_Db_Table_Abstract
{
    protected $_name = 'confirm_qrcode';

    function get_confirm_asm(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array('p.sale_id', 'p.asm_id', "full_name" => "CONCAT(s.firstname, ' ', s.lastname)"));

        $select->joinLeft(array('s' => 'staff'), 's.id = p.asm_id', array());
        $select->where('p.asm_id IS NOT NULL', NULL);

        $result = $db->fetchAll($select);

        return $result;
    }

    function get_confirm_leader(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array('p.sale_id', 'p.leader_id', "full_name" => "CONCAT(s.firstname, ' ', s.lastname)"));

        $select->joinLeft(array('s' => 'staff'), 's.id = p.leader_id', array());
        $select->where('p.leader_id IS NOT NULL', NULL);

        $result = $db->fetchAll($select);

        return $result;
    }

    function get_confirm_trade(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array('p.sale_id', 'p.trade_id', "full_name" => "CONCAT(s.firstname, ' ', s.lastname)"));

        $select->joinLeft(array('s' => 'staff'), 's.id = p.trade_id', array());
        $select->where('p.trade_id IS NOT NULL', NULL);

        $result = $db->fetchAll($select);

        return $result;
    }
}