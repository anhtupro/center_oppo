<?php
class Application_Model_AppShipment extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_shipment';
    protected $_schema = DATABASE_TRADE;

    function getLastNumber()
    {
        $db = Zend_Registry::get('db');

        $result = NULL;

        try {
            $select = $db->select();
            $select->from([ 'p' => DATABASE_TRADE.'.app_shipment' ], [ 'p.id' ]);

            $select->order('p.id DESC');

            $result = $db->fetchRow($select);
        }
        catch(Exception $e) {
        }

        return $result;
    }

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.no', 
            'campaign_name'     => 'c.name',
            'transporter_name'  => 't.name',
            'total_price'       => 'SUM(d.price)',
            'quantity'          => 'SUM(d.quantity)'
        );


        $select->from(array('p' => DATABASE_TRADE.'.app_shipment'), $col);
        $select->joinLeft(array('d' => DATABASE_TRADE.'.app_shipment_details'), 'd.shipment_id = p.id', array() );
        $select->joinLeft(array('c' => DATABASE_TRADE.'.campaign_demo'), 'c.id = p.campaign_id', array() );
        $select->joinLeft(array('t' => DATABASE_TRADE.'.app_transporter'), 't.id = p.transporter_id', array() );

        if(!empty($params['campaign_id'])){
            $select->where('p.campaign_id = ?', $params['campaign_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('d.area_id IN (?)', $params['area_id']);
        }

        if(!empty($params['contractor_id'])){
            $select->where('d.contractor_id = ?', $params['contractor_id']);
        }

        $select->group('p.id');
        $select->order('p.id DESC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function getContractorById($user_id){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            'p.*'
        );

        $select->from(array('p' => DATABASE_TRADE.'.contructors'), $col);

        $select->where('p.user = ?', $user_id);

        $result = $db->fetchRow($select);

        return $result;

    }
}