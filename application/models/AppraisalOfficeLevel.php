<?php
class Application_Model_AppraisalOfficeLevel extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_level';
    protected $_primary = 'aol_id';

    /**
     * @param $skillId
     * @return bool
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function reindexPoint($skillId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql  = 'SET @counter = 0; update appraisal_office_level set aol_point = @counter := @counter + 1 where fk_aos = :fk_aos and aol_status = 0 and aol_is_deleted = 0 order by aol_created_at asc;';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("fk_aos", $skillId, PDO::PARAM_INT);
        $result = $stmt->execute();
        $stmt->closeCursor();
        return $result;
    }
}