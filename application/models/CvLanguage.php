<?php
class Application_Model_CvLanguage extends Zend_Db_Table_Abstract
{
    protected $_name = 'cv_language';
    function getLanguageByID($cv_Id){
        $where=$this->getAdapter()->quoteInto('cv_id = ? ',$cv_Id);
        $CvLanguage=$this->fetchAll($where);
        $result=array();
        foreach($CvLanguage as $k => $v){
        		$result[$v["language"]]=$v["level"];
        }
        return $result;
    }

}