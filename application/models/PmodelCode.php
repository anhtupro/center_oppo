<?php
class Application_Model_PmodelCode extends Zend_Db_Table_Abstract
{
    protected $_name = 'pmodel_code';

    public function getPmodelPrice($params){
		$db = Zend_Registry::get('db');

        $col = array(
            "p.id", 
            "p.code", 
            "p.name",
            "code_supplier_id" => "c.id", 
            "c.price",
            "p.unit",
            "p.sub_name"
        );

        $select = $db->select()
            ->from(array('p' => $this->_name),
                $col);
        ;   

        $select->joinleft(array('c'=>'code_supplier'), 'c.code_id = p.id AND c.`default` = 1', array());

        //$select->where('c.`default` = ?', 1);
        
        $select->where('p.`status` = ?', 0);

        $select->group('p.id');

        $select->order('p.id');
        $result = $db->fetchAll($select);

        return $result;
	}

    public function getCodeSupplier($pmodel_code_id){
        $db = Zend_Registry::get('db');

        $col = array(
            "p.id", 
            "p.code", 
            "p.name",
            "code_supplier_id" => "c.id", 
            "c.price",
            "supplier_name" => "s.title"
        );

        $select = $db->select()
            ->from(array('p' => $this->_name),
                $col);

        $select->joinleft(array('c'=>'code_supplier'), 'c.code_id = p.id', array());
        $select->joinleft(array('s'=>'supplier'), 's.id = c.supplier_id', array());

        $select->where('p.id = ?', $pmodel_code_id);

        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    
    public function cache_pmodel_code()
    {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache_pmodel_code');

        if ($result === false) {

            $db = Zend_Registry::get('db');
            $select = $db->select()
                ->from(array('p' => $this->_name), array('p.id', 'p.*'));
            $data = $db->fetchAll($select);

            $result = array();
            if ($data){
                foreach ($data as $item)
                    $result[ $item['id'] ] = $item;                
            }

            $cache->save($result, $this->_name.'_cache_pmodel_code', array(), null);
        }

        return $result;
    }
    
}