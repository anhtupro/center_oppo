<?php

class Application_Model_InventoryCampaign extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_campaign';

    public function getActiveCampaign()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => 'inventory_campaign'], [
                'c.*'
            ])
        ->where('c.from_date <= ?', date('Y-m-d'))
        ->where('c.to_date >= ?', date('Y-m-d'))
        ->where('c.status = ? ', 1);

        $result = $db->fetchAll($select);
        
        return $result;
                             
    }
    public function needToCheckInventory($staff_id)
    {
        $db = Zend_Registry::get('db');
        $list_active_campaign = $this->getActiveCampaign();

        if (! $list_active_campaign) {
            return false;
        }

        foreach ($list_active_campaign as $campaign) {
            $stmt = $db->prepare("CALL `sp_need_to_to_check_inventory`(:p_staff_id, :p_campaign_id, :p_type)");
            $stmt->bindParam('p_staff_id', $staff_id, PDO::PARAM_STR);
            $stmt->bindParam('p_campaign_id', $campaign['id'], PDO::PARAM_STR);
            $stmt->bindParam('p_type', $campaign['type'], PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetch();
            $stmt->closeCursor();

            if ($data['campaign_id']) {
                return $data['campaign_id'];
            }
        }

        return false;
    }

    public function getStoreNotFinish($campaign_id, $staff_id, $type)
    {
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare("CALL `sp_get_process_inventory_campaign`(:p_staff_id, :p_campaign_id, :p_type)");
        $stmt->bindParam('p_staff_id', $staff_id, PDO::PARAM_STR);
        $stmt->bindParam('p_campaign_id', $campaign_id, PDO::PARAM_STR);
        $stmt->bindParam('p_type', $type, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();

        foreach ($data as $item) {
             $list [$item['store_id']] = $item['store_id'];
        }

        return $list;

    }

    public function fetchPagination($page, $limit, &$total, $params )
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => 'inventory_campaign'], [
                'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS c.id'),
                'c.*'
            ])
            ->where('c.status = ?', 1);

        $select->limitPage($page, $limit);
        $select->order('c.id DESC');
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
}