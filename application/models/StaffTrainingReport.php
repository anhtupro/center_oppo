<?php
class Application_Model_StaffTrainingReport extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_training_report';

    function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p' => $this->_name),array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
             'p.*'))
        ->joinLeft(array('d'=>WAREHOUSE_DB.'.distributor'),'d.id = p.dealer_id',array('dealer_name'=>'d.title'))
        ;

        if(isset($params['staff_id']) and $params['staff_id'])
        {
            $select
                ->where('p.staff_id IN (?)', $params['staff_id']);
        }

        if(isset($params['type']) and $params['type'])
        {
            $select
                ->where('p.type = ?', $params['type']);
        }

        if(isset($params['area']) and $params['area'])
        {
            $select
                ->where('p.area = ?', $params['area']);
        }

        if(isset($params['province']) and $params['province'])
        {
            $select
                ->where('p.province = ?', $params['province']);
        }

        if(isset($params['date']) and $params['date'])
        {
            $select
                ->where('p.date = ?', $params['date']);
        }

        if(isset($params['trainer_dealer']) and $params['trainer_dealer'])
        {
            $select
                ->where('p.trainer_dealer = ?', $params['trainer_dealer']);
        }

        if(isset($params['store']) and $params['store'])
        {
            $select
                ->where('p.store_id = ?', $params['store']);
        }

        if(isset($params['from_date']) and $params['from_date'])
        {
            $select
                ->where('p.date >= ?', $params['from_date']);
        }

        if(isset($params['to_date']) and $params['to_date'])
        {
            $select
                ->where('p.date <= ?', $params['to_date']);
        }

        if (isset($params['sort']) and $params['sort']) {

            $order_str = ' ';

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if (
                $params['sort'] == 'dealer_id'
                || $params['sort'] == 'store_id'
                || $params['sort'] == 'date'
                || $params['sort'] == 'type'
                || $params['sort'] == 'area'
                || $params['sort'] == 'province'
                || $params['sort'] == 'quantity_participant'
                || $params['sort'] == 'quantity_disqualified'
                || $params['sort'] == 'quantity_remain'
                || $params['sort'] == 'from_date'
                || $params['sort'] == 'to_date'
            ) {

                $order_str = 'p.`'.$params['sort'] . '` ' .$desc;
            }else{
                $order_str = 'p.date DESC';
            }

            $select->order(new Zend_Db_Expr($order_str));
        }else{
            $select->order('p.date DESC');    
        }
        

        $select->where('p.del = ? OR p.del IS NULL', 0);

        if ($limit)
            $select->limitPage($page, $limit);
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        foreach($result as $key => $value){
            if($value['type'] == 2 AND $value['store_id']){
                $result[$key]['sellout'] = $this->getSelloutByStore($value['store_id'],$value['date']);
            }
        }

        return $result;
    }

    /*function get dealer */
    public function getDealerArea($content)
    {
        $db              = Zend_Registry::get('db');

        $arrayDealer     = array();

        if(count(array_keys($content))>0)
        {
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
            $config = $config->toArray();

            $select_dealer  = $db->select()->from(array('w' => $config['resources']['db2']['params']['dbname'] . '.distributor'), array('w.id','w.title','w.parent'))
                ->where('district IN (?) ',array_keys($content))
                ->where('del = ? OR del IS NULL',0)
                ->orwhere('is_ka = ?',1);
            $result  = $db->query($select_dealer)->fetchAll();

            foreach($result as $key => $value)
            {
                $arrayDealer[$value['id']] = $value['title'];
            }

        }
        return $arrayDealer;

    }

    public function getStaffIDFromAreaAsm($area = array(),$userStorage = null)
    {
        $result          = $rows =  $valuesStaffID = array();

        if(isset($area) and $area)
        {
            $db              = Zend_Registry::get('db');
            $select          = $db->select();
            $select->from(array('a'=>'asm'));

            $select->where('a.area_id IN (?)',$area);
            $rows = $db->fetchAll($select);

            if(count($rows))
            {
                foreach($rows as $key => $value)
                {
                    $valuesStaffID[] = $value['staff_id'];
                }
            }

            $valuesStaffID[] = $userStorage->id;

            $result = array_unique($valuesStaffID);
        }

        return $result;
    }

    public function getSelloutByStore($store_id,$date){
        if(!$store_id) return false;
        if(!$date) return false;
        $db = Zend_Registry::get('db');
        $sql = 'SELECT
                    SUM(CASE WHEN DATE(a.timing_date) BETWEEN DATE_SUB("'.$date.'",INTERVAL 7 DAY) AND DATE_SUB("'.$date.'",INTERVAL 1 DAY) THEN 1 ELSE 0 END) as "before",
                    SUM(CASE WHEN DATE(a.timing_date) BETWEEN "'.$date.'" AND DATE_ADD("'.$date.'",INTERVAL 6 DAY) THEN 1 ELSE 0 END) as "after"
                FROM imei_kpi a
                WHERE store_id = '.$store_id.'
                GROUP BY a.store_id
                ';
//        echo $sql;
//        exit;
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll();
        if(count($result)){
            $result = $result[0];
        }
        return $result;
    }

    public function reportDataBi($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        
        $arrCols = array(
            'type'              => 'p.type',
            'month_date'        => "MONTH(p.date)",
            'year_date'        => "YEAR(p.date)",
            'sum'               => "SUM(p.quantity_trainees)",
            'quantity_session'  => "SUM(p.quantity_session)"
        );
        
        $select->from(array('p'=> $this->_name), $arrCols);
        $select->where('p.del IS NULL OR p.del = ?', 0);
        
        if(isset($params['from']) and $params['from']){
            $select->where('p.date >= ?', $params['from']);
        }
        
        if(isset($params['to']) and $params['to']){
            $select->where('p.date <= ?', $params['to']);
        }
        
        if(isset($params['area_list']) and $params['area_list']){
            $select->where('p.area IN (?)', $params['area_list']);
        }

        if(isset($params['province']) and $params['province']){
            $select->where('p.province = ?', $params['province']);
        }
        
        $select->group(array("MONTH(p.date)", "p.type", "YEAR(p.date)"));
        
        $result  = $db->fetchAll($select);

        return $result;
    }

    public function reportDataBiArea($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        
        $arrCols = array(
            'area_id'           => 'p.area',
            'type'              => 'p.type',
            'month_date'        => "MONTH(p.date)",
            'year_date'         => "YEAR(p.date)",
            'sum'               => "SUM(p.quantity_trainees)",
            'quantity_session'  => "SUM(p.quantity_session)"
        );
        
        $select->from(array('p'=> $this->_name), $arrCols);
        $select->where('p.del IS NULL OR p.del = ?', 0);
        
        if(isset($params['from']) and $params['from']){
            $select->where('p.date >= ?', $params['from']);
        }
        
        if(isset($params['to']) and $params['to']){
            $select->where('p.date <= ?', $params['to']);
        }
        
        if(isset($params['area_list']) and $params['area_list']){
            $select->where('p.area IN (?)', $params['area_list']);
        }
        
        $select->group(array("MONTH(p.date)", "p.type", "YEAR(p.date)", "p.area"));
        
        $result  = $db->fetchAll($select);

        return $result;
    }

    public function reportDataBiCourse($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        
        $arrCols = array(
            'type'              => 'p.type',
            'month_date'        => "MONTH(p.from_date)",
            'year_date'        => "YEAR(p.from_date)",
            'quantity_course'   => "COUNT(DISTINCT p.id)",
            'quantity_pg'       => "SUM(IF(e.id IS NULL,0,1))",
            'quantity_pass'     => "SUM(IF(e.id IS NOT NULL AND e.result = 1,1,0))"
        );
        
        $select->from(array('p'=> 'trainer_course'), $arrCols);
        $select->joinLeft(array('t'=>'trainer_course_type'),'p.type = t.id',array(''));
        $select->joinLeft(array('e'=>'trainer_course_detail'),'e.course_id = p.id AND (e.del = 0 OR e.del IS NULL)',array(''));
        $select->where('p.del IS NULL OR p.del = ?', 0);
        
        if(isset($params['from']) and $params['from']){
            $select->where('p.from_date >= ?', $params['from']);
        }
        
        if(isset($params['to']) and $params['to']){
            $select->where('p.from_date <= ?', $params['to']);
        }
        
        if(isset($params['area_list']) and $params['area_list']){
            $select->where('p.area_id IN (?)', $params['area_list']);
        }
        
        $select->group(array("MONTH(p.from_date)","p.type","YEAR(p.from_date)"));

        if($_GET['abc'] == 1){
            echo $select;exit;
        }
        
        $result  = $db->fetchAll($select);
        return $result;
    }

    public function reportDataBiCourseArea($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        
        $arrCols = array(
            'area_id'           => 'p.area_id',
            'type'              => 'p.type',
            'month_date'        => "MONTH(p.from_date)",
            'year_date'         => "YEAR(p.from_date)",
            'quantity_course'   => "COUNT(DISTINCT p.id)",
            'quantity_pg'       => "SUM(IF(e.id IS NULL,0,1))",
            'quantity_pass'     => "SUM(IF(e.id IS NOT NULL AND e.result = 1,1,0))"
        );
        
        $select->from(array('p'=> 'trainer_course'), $arrCols);
        $select->joinLeft(array('t'=>'trainer_course_type'),'p.type = t.id',array(''));
        $select->joinLeft(array('e'=>'trainer_course_detail'),'e.course_id = p.id AND (e.del = 0 OR e.del IS NULL)',array(''));
        $select->where('p.del IS NULL OR p.del = ?', 0);
        
        if(isset($params['from']) and $params['from']){
            $select->where('p.from_date >= ?', $params['from']);
        }
        
        if(isset($params['to']) and $params['to']){
            $select->where('p.from_date <= ?', $params['to']);
        }
        
        if(isset($params['area_list']) and $params['area_list']){
            $select->where('p.area_id IN (?)', $params['area_list']);
        }
        
        $select->group(array("MONTH(p.from_date)","p.type","YEAR(p.from_date)", "p.area_id"));
        
        $result  = $db->fetchAll($select);
        return $result;
    }

}