<?php 

class Application_Model_SurveyQuestions extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_questions';

    public function getAllQuestions($survey_id)
    {
        $db = Zend_Registry::get('db');

    	$selectQuestion = $db->select()->from(array('sq' => 'survey_questions'), array('question' => 'sq.question'))
								    	->where('sq.survey_id = ?', $survey_id)
                                        ->where('sq.type <> ?', 'sort')
                                        ->group('sq.group_name')
								    	->order('sq.id');

    	 $questions =  $db->fetchAll($selectQuestion);

    	 foreach ($questions as $value) {
    	 	$result [] = $value['question'];
    	 }

    	 return  $result;
    }
    public function getSortQuestion($survey_id)
    {
        $db = Zend_Registry::get('db');

        $sql = " SELECT option  
                 FROM `survey_questions`
                 WHERE survey_id = $survey_id AND type ='sort' ";

        $stmt = $db->prepare($sql);    
        $stmt->execute();
        $options = $stmt->fetchAll();  
        $stmt->closeCursor();

        foreach ($options as $option) {
            $result [] = $option['option'];
        }

        return $result;
    }

    public function getQuestions($surveyId)
    {
        return $this->fetchAll(
            $this->select()
                ->from($this, ['question', 'group_name'])
                ->where('survey_id = ?', $surveyId)
                ->group(['question', 'group_name'])
                ->order(['id'])
        )->toArray();
    }
    
}