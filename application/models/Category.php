<?php
class Application_Model_Category extends Zend_Db_Table_Abstract
{
	protected $_name = 'category';

	protected $_schema = DATABASE_TRADE;

    

	public function GetAll(){
        $db = Zend_Registry::get('db');
        $select = $db->select('C.id, C.name')
        ->from(array('C' => DATABASE_TRADE.'.'.$this->_name));
        //->where('group_bi is not null and group_bi >0');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCategoryByCampaign($params = null){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'c.id', 
            'c.name', 
            'p.price', 
            'p.campaign_id', 
        );

        $select->from(array('p'=> DATABASE_TRADE.'.campaign_category_demo'), $arrCols);
        $select->joinLeft(array('cam' => DATABASE_TRADE.'.campaign_demo'), 'cam.id = p.campaign_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
        
        if(!empty($params['campaign_id'])){
            $select->where('p.campaign_id = ?', $params['campaign_id']);
        }

        if(!empty($params['type'])){
            $select->where('cam.type = ?', $params['type']);
        }

        $select->where('c.id IS NOT NULL', NULL);
        
        $result  = $db->fetchAll($select);

        return $result;
    }

    public function GetCategoryPosm($params = null){
        $db = Zend_Registry::get('db');
        $select = $db->select('C.id, C.name')
        ->from(array('C' => DATABASE_TRADE.'.'.$this->_name))
        ->where('type = 1', NULL);
        if ($params['statistic_posm']) {
            $select->where('C.statistic_posm = ?', $params['statistic_posm']);
        }
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCode(){
        $db = Zend_Registry::get('db');
        $select = $db->select('C.id, C.name, C.code')
        ->from(array('C' => DATABASE_TRADE.'.'.$this->_name))
        ->where('type = 1', NULL);
        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['code']] = $value['id'];
        }

        return $data;
    }

    public function getCategoryCheckshop($params = null){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            "p.id", 
            "p.name", 
            "p.desc", 
            "p.group_bi",
            'p.has_type',
            'p.has_width'
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.category'), $arrCols);
        $select->where('group_bi IN (?)', [1,2,3,4,6]);

        if(!empty($params['investments_cat'])){
            //$select->where('p.id NOT IN (?)', $params['investments_cat']);
        }
        
        $select->order('p.name');
        
        $result  = $db->fetchAll($select);
        
        return $result;
    }
    public function getCategory(){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            "p.id", 
            "p.name", 
            "p.desc", 
            "p.group_bi"
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.category'), $arrCols);
        $select->where('group_bi IN (?)', [1,2,3,4]);
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    public function getCategoryAir(){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            "p.id", 
            "p.name", 
            "p.desc", 
            "p.group_bi"
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.category'), $arrCols);
        $select->where('group_bi IN (?)', [1,4]);
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

     public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
    //DISTINCT
        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'name' => "p.name",
            'price' => "p.price",
            'status'=> "p.status",
            'type'  => "p.type",
            'desc'  => "p.desc",
            'contructor_code'   => 'GROUP_CONCAT(con.code)'
        );

        $select->from(array('p' => DATABASE_TRADE.'.category'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.contractor_price'), 'c.category_id = p.id', array());
        $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = c.contractor_id',array());
        $select->where('p.type = 1');
        $select->where('c.to_date IS NULL', NULL);
        if(!empty($params['has_contructor']) AND $params['has_contructor'] == 1){
            $select->where("con.id IS NOT NULL", NULL);
        }
        
        if(!empty($params['has_contructor']) AND $params['has_contructor'] == 2){
            $select->where("con.id IS NULL", NULL);
        }
       if(!empty($params['name_search'])){
            $select->where('p.name LIKE ?', '%'.$params['name_search'].'%');
        }
        if(!empty($params['price'])){
            $select->where('p.price = ?',$params['price']);
        }
         if(!empty($params['status'])){
            if($params['status']==1)
            {
                $select->where('p.status = 1');
            }
            if($params['status']==2)
            {
                $select->where('p.status = 0');
            }
            
        }
         if(!empty($params['type'])){
            $select->where('p.type = ?',$params['type']);
        }
         if(!empty($params['description'])){
            $select->where('p.desc LIKE ?', '%'.$params['description'].'%');
        }
        //---T END SEARCH
        if (!empty($params['export'])){
            
        }else{
             $select->limitPage($page, $limit);
        }
        $select->group('p.id');
        $select->order('p.id DESC');
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
       
        return $result;
    }

    public function getAllChildCategory($parentId = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.category'], [
                         'c.id',
                         'c.name',
                         'c.parent_id_additional',
                         'category_parent_name' => 'p.name'
                     ])
            ->joinLeft(['p' => DATABASE_TRADE.'.category_additional'], 'c.parent_id_additional = p.id', [])
        ->where('c.parent_id_additional IS NOT NULL');

        if ($parentId) {
            $select->where('c.parent_id_additional = ?', $parentId);
            return $db->fetchAll($select);
        }
        
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listCategoryChild [$element['parent_id_additional']] [] = $element;
        }

        return $listCategoryChild;

    }

    public function getCategoryAirStatistic($params = null)
    {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name",
            "p.desc",
            "p.group_bi",
            'p.has_type',
            'p.has_width',
            'p.has_material',
            'p.has_painting'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.category'), $arrCols);
        $select->where('p.statistic_air = 1');
        $select->order('p.name');

        $result  = $db->fetchAll($select);

        return $result;
    }

    public function getCategoryBrand()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.category'], [
                         'c.id',
                         'c.name'
                     ])
        ->where('c.has_brand = 1');

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getIdCategoryCheckshop()
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'category_id' => "GROUP_CONCAT(p.id)"
        );

        $select->from(array('p'=> DATABASE_TRADE.'.category'), $arrCols);
        $select->where('group_bi IN (?)', [1,2,3,4,6]);

        $select->order('p.name');

        $stringResult  = $db->fetchRow($select);

        $listCategoryId = explode(',', $stringResult['category_id']);

        return $listCategoryId;
    }


    public function getCategoryOrderAir()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.category'], [
                        'c.id',
                        'c.name'
                     ])
                   ->where('c.is_order_air = ?', 1);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function isCategoryAirInventory($categoryId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.category'], [
                        'c.id'
                     ])
                    ->where('c.id = ?', $categoryId)
                    ->where('c.statistic_air = ?', 1);

        $result = $db->fetchRow($select);

        return $result ? true : false;
    }
}