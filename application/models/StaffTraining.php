<?php
class Application_Model_StaffTraining extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_training';

    function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p' => $this->_name),array(new Zend_Db_Expr(
            'SQL_CALC_FOUND_ROWS
             p.*'
        )));

        if(isset($params['name']) and $params['name'])
            $select->where('CONCAT(p.firstname," ",p.lastname) LIKE ?','%'.$params['name'].'%');

        if(isset($params['cmnd']) and $params['cmnd'])
            $select->where('p.cmnd = ?',$params['cmnd']);

        if(isset($params['area_id']) and $params['area_id'])
            $select->where('p.area_id = ?',$params['area_id']);

        if(isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.regional_market = ?',$params['regional_market']);

        if(isset($params['regional_market_right']) and $params['regional_market_right'])
            $select->where('p.regional_market IN (?)',$params['regional_market_right']);

        $select->where('p.del = ? OR p.del IS NULL',0);
        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);


        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->firstname . ' ' . $item->lastname;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_all_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_all_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item;
                }
            }
            $cache->save($result, $this->_name.'_all_cache', array(), null);
        }
        
        return $result;
    }
    
    function fetchPagination_training($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select = $db->select()
            ->from(array('a' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.*', 'title_market' => 'b.name', 
                'dtb' => 'AVG(c.scores)',
                'sothanglamviec' => 'TIMESTAMPDIFF(MONTH,a.created_at,NOW())',
                'songaylamviec' => 'TIMESTAMPDIFF(DAY,a.created_at,NOW())',
                'sokhoahoc' => 'COUNT(Distinct c.lesson_id)'))
            ->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market', array())
            ->joinLeft(array('c' => 'lesson_scores'), 'a.cmnd = c.staff_cmnd', array());
             $select->group('a.id');
        
        if(isset($params['title']) and $params['title'])
            $select->where('CONCAT(a.firstname," ",a.lastname) LIKE ?','%'.$params['title'].'%');

        if(isset($params['cmnd']) and $params['cmnd'])
            $select->where('a.cmnd = ?',$params['cmnd']);

        if(isset($params['area_id']) and $params['area_id'])
            $select->where('a.area_id = ?',$params['area_id']);

        if(isset($params['regional_market']) and $params['regional_market'])
            $select->where('a.regional_market = ?',$params['regional_market']);

        if(isset($params['region']) and $params['region'])
            $select->where('a.regional_market IN (?)',$params['region']);
        
        
        if(isset($params['dtb']) and $params['dtb'])
            $select->having('round(AVG(c.scores)) = ?',$params['dtb']);
        
        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(a.firstname, " ",a.lastname) '.$collate . $desc;
            } elseif ( /*$params['sort'] == 'title' || */$params['sort'] == 'department' || $params['sort'] == 'team' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } else {
                $order_str = '`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }
        else{
            $select->order('id DESC');
        }
        
        $select->where('a.del = ? OR a.del IS NULL',0);
        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);


        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    function fetchPagination_training2($page, $limit, &$total, $params) {
        
        $db = Zend_Registry::get('db');
        
        //nestedSelect    
        $select1 = "SELECT
                    	`a`.`id`,
                    	`a`.`firstname`,
                    	`a`.`lastname`,
                    	`a`.`cmnd`,
                    	`a`.`team`,
                    	`a`.`title`,
                    	`a`.`regional_market`,
                    	`a`.`created_at`,
                    	`a`.`training_online`,
                    	NULL
                    FROM
                    	`staff_training` AS `a`
                    WHERE
                    	(a.training_online = 1) 
                    AND (a.or_training IS NULL OR a.or_training = 0)
                    AND (a.del = 0 OR a.del IS NULL) 
                    AND cmnd NOT IN (
                    	SELECT ID_number from staff WHERE ID_number IS NOT NULL AND ID_number != ''
                    )";
        
        //query 2
		
        $select2 = $db->select()
        ->from(array('a' => 'staff'),
                array( 'id','firstname','lastname','ID_number','team','title','regional_market','joined_at',new Zend_Db_Expr('null'),'id' ))
		->where('a.status = ?',1)
        ->where('a.off_date is null');

		if(!empty($params['title_staff'])){
			$select2->where('a.title IN (?)',$params['title_staff']);
		}

        $nestedSelect = $db->select()
             ->union(array($select1, $select2));
        //
             
        $select = $db->select()
            ->from(array('a' => new Zend_Db_Expr('(' . $nestedSelect . ')')),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.*', 'title_market' => 'b.name', 
                'dtb' => 'AVG(c.scores)',
                'sothanglamviec' => 'TIMESTAMPDIFF(MONTH,a.created_at,NOW())',
                'songaylamviec' => 'TIMESTAMPDIFF(DAY,a.created_at,NOW())',
                'sokhoahoc' => 'COUNT(Distinct c.lesson_id)'))
            ->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market', array())
            ->joinLeft(array('c' => 'lesson_scores'), 'a.cmnd = c.staff_cmnd', array());
             $select->group('a.cmnd');
        
        if(isset($params['name']) and $params['name'])
            $select->where('CONCAT(TRIM(a.firstname)," ",TRIM(a.lastname)) LIKE ?','%'.$params['name'].'%');
        
        if(isset($params['cmnd']) and $params['cmnd'])
            $select->where('a.cmnd = ?',$params['cmnd']);
        
        if(isset($params['title']) and $params['title'])
            $select->where('a.title = ?',$params['title']);

        if(isset($params['area_id']) and $params['area_id'])
            $select->where('a.area_id = ?',$params['area_id']);


        if(isset($params['regional_market']) and $params['regional_market'])
            $select->where('a.regional_market = ?',$params['regional_market']);
        
        if(isset($params['region']) and $params['region'])
            $select->where('a.regional_market IN (?)',$params['region']);
        
        if(isset($params['region_access']) and $params['region_access']){
            $regi = explode(',',$params['region_access']);
            if(count($regi))
                $select->where('a.regional_market IN (?)',$regi);
        }
        
        if(isset($params['sale_leader']) and $params['sale_leader']){
            $regi = explode(',',$params['sale_leader']);
            if(count($regi))
                $select->where('a.NULL IN (?)',$regi);
        }

        if(isset($params['team']) and $params['team'])
            $select->where('a.team IN (?)',$params['team']);
        
        if(isset($params['dtb']) and $params['dtb'])
            $select->having('round(AVG(c.scores)) = ?',$params['dtb']);

    	        if(isset($params['area_list']) and $params['area_list'])
            $select->where('b.area_id IN (?)',$params['area_list']);

        $order_str = '';
        
        $select->order(array('training_online DESC','a.id DESC'));

        
        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    
    
    function getStaffByCmnd($cmnd){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'staff_training'),
                    array('a.*','b.name')
                )
                ->joinLeft(array('b' => 'regional_market'), 'a.regional_market = b.id', array())
                ->where("cmnd = ?",$cmnd);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function staffMainNew($params){
        $db     = Zend_Registry::get("db");

        $select      = $db->select();
        $select_sub1 = $db->select();

        $col = array(
            'p.id', 
            'p.firstname', 
            'p.lastname', 
            'p.cmnd', 
            'p.team', 
            'p.title', 
            'p.regional_market', 
            'joined_at' => 'p.created_at', 
            'p.del', 
            'time_pass' => 's.created_at'
        );
        $select_sub1->from(array('p'=> 'staff_staff_training'), $col);
        $select_sub1->joinLeft(array('s' => 'staff_main_new'), 's.ID_number = p.cmnd', array());
        $select_sub1->where('p.created_at >= ?', $params['from']);

        $select_sub2 = "(
                        select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                        select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) a, /*10 day range*/
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) b, /*100 day range*/
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) c, /*1000 day range*/
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) d, /*10000 day range*/
                            (select  @minDate :=  CONCAT('".$params['from']."'), @maxDate := CASE WHEN CONCAT('".$params['to']."') <= DATE(NOW()) THEN CONCAT('".$params['to']."') ELSE DATE(NOW()) END) e
                        ) f
                        where aDate between @minDate and @maxDate
                        GROUP BY YEAR(aDate), MONTH(aDate)
                        )";

        $arrCols = array(
            'p.id',
            'p.firstname', 
            'p.lastname', 
            'p.cmnd', 
            'p.team', 
            'p.title', 
            'p.regional_market', 
            'p.joined_at', 
            'p.del', 
            'p.time_pass',
            'staff_joined'      => 'SUM((CASE WHEN (DATE(p.joined_at) >= a.FromDate AND DATE(p.joined_at) <= a.ToDate) THEN 1 ELSE 0 END))',
            'staff_joined_pass' => 'SUM((CASE WHEN ((DATE(p.joined_at) >= a.FromDate AND DATE(p.joined_at) <= a.ToDate) AND (DATE(p.time_pass) >= a.FromDate AND DATE(p.time_pass) <= a.ToDate)) THEN 1 ELSE 0 END))',
            'staff_pass'        => 'SUM((CASE WHEN ((DATE(p.joined_at) <= a.FromDate) AND (DATE(p.time_pass) >= a.FromDate AND DATE(p.time_pass) <= a.ToDate)) THEN 1 ELSE 0 END))',
            'staff_fails'        => 'SUM((CASE WHEN ((DATE(p.joined_at) <= a.ToDate) AND (DATE(p.joined_at) >= a.FromDate) AND (DATE(p.time_pass) > a.ToDate OR p.time_pass IS NULL)) THEN 1 ELSE 0 END))',
            'a.MonthNo', 
            'a.YearNo'
        );


        $select->from(array('p'=> new Zend_Db_Expr('(' . $select_sub1 . ')')), $arrCols);
        $select->joinCross(array('a' => new Zend_Db_Expr('(' . $select_sub2 . ')')), NULL, NULl);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if(!empty($params['area_list'])){
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        
        if(!empty($params['list_title_staff'])){
            $select->where('p.title IN (?)', $params['list_title_staff']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        $select->group(['a.MonthNo', 'a.YearNo']);
        
        if($_GET['eev'] == 2){
            echo "<pre>";
            echo $select;exit;
        }

        $result  = $db->fetchAll($select);
        return $result;
    }

    public function fetchStaffNewMain($page, $limit, &$total, $params){
        $db     = Zend_Registry::get("db");

        $select      = $db->select();
        $select_sub1 = $db->select();

        $col = array(
            'p.id', 
            'p.firstname', 
            'p.lastname', 
            'p.cmnd', 
            'p.team', 
            'p.title', 
            'p.regional_market', 
            'joined_at' => 'p.created_at', 
            'p.del', 
            'time_pass' => 's.created_at'
        );
        $select_sub1->from(array('p'=> 'staff_staff_training'), $col);
        $select_sub1->joinLeft(array('s' => 'staff_main_new'), 's.ID_number = p.cmnd', array());
        $select_sub1->where('p.created_at >= ?', $params['from']);

        $select_sub2 = "(
                        select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                        select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) a, /*10 day range*/
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) b, /*100 day range*/
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) c, /*1000 day range*/
                            (select 0 as a union all select 1 union all select 2 union all select 3
                             union all select 4 union all select 5 union all select 6 union all
                             select 7 union all select 8 union all select 9) d, /*10000 day range*/
                            (select  @minDate :=  CONCAT('".$params['from']."'), @maxDate := CASE WHEN CONCAT('".$params['to']."') <= DATE(NOW()) THEN CONCAT('".$params['to']."') ELSE DATE(NOW()) END) e
                        ) f
                        where aDate between @minDate and @maxDate
                        GROUP BY YEAR(aDate), MONTH(aDate)
                        )";

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.firstname', 
            'p.lastname', 
            'p.cmnd', 
            'p.team', 
            'p.title', 
            'p.regional_market', 
            'p.joined_at', 
            'p.del', 
            'p.time_pass',
            'staff_joined'      => '(CASE WHEN (DATE(p.joined_at) >= a.FromDate AND DATE(p.joined_at) <= a.ToDate) THEN 1 ELSE 0 END)',
            'staff_joined_pass' => '(CASE WHEN ((DATE(p.joined_at) >= a.FromDate AND DATE(p.joined_at) <= a.ToDate) AND (DATE(p.time_pass) >= a.FromDate AND DATE(p.time_pass) <= a.ToDate)) THEN 1 ELSE 0 END)',
            'staff_pass'        => '(CASE WHEN ((DATE(p.joined_at) <= a.FromDate) AND (DATE(p.time_pass) >= a.FromDate AND DATE(p.time_pass) <= a.ToDate)) THEN 1 ELSE 0 END)',
            'staff_fails'        => '(CASE WHEN ((DATE(p.joined_at) <= a.ToDate)AND (DATE(p.joined_at) >= a.FromDate) AND (DATE(p.time_pass) > a.ToDate OR p.time_pass IS NULL)) THEN 1 ELSE 0 END)',
            'a.MonthNo', 
            'a.YearNo',
            'r.area_id',
            'regional_market' => 'r.id'
        );


        $select->from(array('p'=> new Zend_Db_Expr('(' . $select_sub1 . ')')), $arrCols);
        $select->joinCross(array('a' => new Zend_Db_Expr('(' . $select_sub2 . ')')), NULL, NULl);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if(!empty($params['area_list'])){
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if(!empty($params['month'])){
            $select->where('a.MonthNo = ?', $params['month']);
        }

        if(!empty($params['name'])){
            $select->where('CONCAT(p.firstname, "",p.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        if(!empty($params['province_id'])){
            $select->where('r.id = ?', $params['province_id']);
        }

        if(!empty($params['title'])){
            $select->where('p.title = ?', $params['title']);
        }

        if(!empty($params['year'])){
            $select->where('a.YearNo = ?', $params['year']);
        }

        if(!empty($params['staff_fails'])){
            $select->where('(CASE WHEN ((DATE(p.joined_at) <= a.ToDate) AND (DATE(p.joined_at) >= a.FromDate) AND (DATE(p.time_pass) > a.ToDate OR p.time_pass IS NULL)) THEN 1 ELSE 0 END) = ?', $params['staff_fails']);
        }
       

        $select->limitPage($page, $limit);

        if($_GET['dev']==100){
            echo $select->__toString();
        }
        $result  = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function fetchPagination_training_or($page, $limit, &$total, $params) {

        $db = Zend_Registry::get('db');

        //nestedSelect
        $select1 = "SELECT
                    	`a`.`id`,
                    	`a`.`firstname`,
                    	`a`.`lastname`,
                    	`a`.`cmnd`,
                    	`a`.`team`,
                    	`a`.`title`,
                    	`a`.`regional_market`,
                    	`a`.`created_at`,
                    	`a`.`training_online`,
                    	NULL
                    FROM
                    	`staff_training` AS `a`
                    WHERE
                    	(a.training_online = 1) AND (a.or_training = 1)
                    AND (a.del = 0 OR a.del IS NULL) 
                    ";



        $select = $db->select()
            ->from(array('a' => new Zend_Db_Expr('(' . $select1 . ')')),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.*', 'title_market' => 'b.name',
                    'dtb' => 'AVG(c.scores)',
                    'sothanglamviec' => 'TIMESTAMPDIFF(MONTH,a.created_at,NOW())',
                    'songaylamviec' => 'TIMESTAMPDIFF(DAY,a.created_at,NOW())',
                    'sokhoahoc' => 'COUNT(Distinct c.lesson_id)'))
            ->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market', array())
            ->joinLeft(array('c' => 'lesson_scores'), 'a.cmnd = c.staff_cmnd', array());
        $select->group('a.cmnd');

        if(isset($params['name']) and $params['name'])
            $select->where('CONCAT(TRIM(a.firstname)," ",TRIM(a.lastname)) LIKE ?','%'.$params['name'].'%');

        if(isset($params['cmnd']) and $params['cmnd'])
            $select->where('a.cmnd = ?',$params['cmnd']);

        if(isset($params['title']) and $params['title'])
            $select->where('a.title = ?',$params['title']);

        if(isset($params['area_id']) and $params['area_id'])
            $select->where('a.area_id = ?',$params['area_id']);

        if(isset($params['regional_market']) and $params['regional_market'])
            $select->where('a.regional_market = ?',$params['regional_market']);

//        if(isset($params['region']) and $params['region'])
//            $select->where('a.regional_market IN (?)',$params['region']);

//        if(isset($params['region_access']) and $params['region_access']){
//            $regi = explode(',',$params['region_access']);
//            if(count($regi))
//                $select->where('a.regional_market IN (?)',$regi);
//        }

        if(isset($params['sale_leader']) and $params['sale_leader']){
            $regi = explode(',',$params['sale_leader']);
            if(count($regi))
                $select->where('a.NULL IN (?)',$regi);
        }

        if(isset($params['team']) and $params['team'])
            $select->where('a.team IN (?)',$params['team']);

        if(isset($params['dtb']) and $params['dtb'])
            $select->having('round(AVG(c.scores)) = ?',$params['dtb']);

        $order_str = '';

        $select->order(array('training_online DESC','a.id DESC'));

        if($_GET['dev'] == 1){
            echo $select;
        }

        if ($limit && ! $params['export']) {
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }


    public function exportTrainingOr($data)
    {
        require_once 'PHPExcel.php';

            $objExcel = new PHPExcel();
            $objExcel->setActiveSheetIndex(0);

            $sheet = $objExcel->getActiveSheet()->setTitle('Training');

            $rowCount = 2;
            $index = 1;
            $sheet->setCellValue('A1', 'STT');
            $sheet->setCellValue('B1', 'Tên nhân viên');
            $sheet->setCellValue('C1', 'Số tháng làm việc');
            $sheet->setCellValue('D1', 'Số khóa');
            $sheet->setCellValue('E1', 'Điểm TB');
            $sheet->setCellValue('F1', 'Kết quả');


            foreach ($data as $element) {
                if($element['dtb']){
                    if($element['dtb'] >=TRAINING_SCORES_PASS && ($element['dtb'])<=TRAINING_SCORES_PASS_GOOD){
                        $type = 'Khá';
                    }
                    if($element['dtb'] > TRAINING_SCORES_PASS_GOOD){
                        $type = 'Giỏi';
                    }
                    if($element['dtb'] < TRAINING_SCORES_PASS){
                        $type = 'Fail';
                    }
                }


                $sheet->setCellValue('A' . $rowCount, $index);
                $sheet->setCellValue('B' . $rowCount, $element['firstname'] . ' ' . $element['lastname']);
                $sheet->setCellValue('C' . $rowCount, $element['sothanglamviec']);
                $sheet->setCellValue('D' . $rowCount, $element['sokhoahoc']);
                $sheet->setCellValue('E' . $rowCount, $element['dtb']);
                $sheet->setCellValue('F' . $rowCount, $type);

                ++$index;
                ++$rowCount;
            }
 
        //style sheet
            $style_border = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $sheet->getStyle('A0:F' . $rowCount)->applyFromArray($style_border);

            for ($i = 'A'; $i < 'F' ; ++$i) {
                $sheet->getStyle($i. 1)->getFont()->setBold(true);
            }


        $filename = 'Training';
        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }



}