<?php

class Application_Model_StoreMapping extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_mapping';
    protected $_schema = REALME_DATABASE_CENTER;

    public function getStoreOppo($store_id_realme)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['m' => REALME_DATABASE_CENTER . '.store_mapping'], [
                'm.id_oppo'
            ])
            ->joinLeft(['s' => 'store'], 'm.id_oppo = s.id', [])
            ->where('m.id_rm = ?', $store_id_realme)
            ->where('s.id IS NOT NULL');

        $store_id_oppo = $db->fetchOne($select);

        return $store_id_oppo;

    }

    public function getStoreRealme($store_id_oppo)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'm.id_rm'
            ])
        ->join(['m' => REALME_DATABASE_CENTER .'.store_mapping'], 's.id = m.id_oppo', [])
        ->join(['sr' => REALME_DATABASE_CENTER .'.store'], 'm.id_rm = sr.id', [])
        ->where('s.id = ?', $store_id_oppo)
        ->where('s.del IS NULL OR s.del = 0')
        ->where('sr.del IS NULL OR sr.del = 0');

        $store_id_realme = $db->fetchOne($select);

        return $store_id_realme;
    }

    public function checkStoreIsValid($store_id_realme)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['sr' => REALME_DATABASE_CENTER . '.store'], [
                'm.id_oppo'
            ])
            ->join(['m' => REALME_DATABASE_CENTER .'.store_mapping'], 'sr.id = m.id_rm', [])
            ->join(['s' => 'store'], 's.id = m.id_oppo', [])

            ->where('sr.id = ?', $store_id_realme)
            ->where('sr.del IS NULL OR sr.del = 0')
            ->where('s.del IS NULL OR s.del = 0');

        $result = $db->fetchOne($select);

        return $result ? true : false;
    }
}