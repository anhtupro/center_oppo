<?php
class Application_Model_AppStatus extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_status';
    protected $_schema = DATABASE_TRADE;

    function get_cache($type){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.$type.'_cache');

        if ($result === false) {

            $where = $this->getAdapter()->quoteInto('type = ?', $type);
            $data = $this->fetchAll($where);

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->status] = $item->name;
                }
            }
            $cache->save($result, $this->_name.$type.'_cache', array(), null);
        }

        return $result;
    }
}