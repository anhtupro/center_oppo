<?php

class Application_Model_InstallmentPrepayRate extends Zend_Db_Table_Abstract
{
    protected $_name = 'installment_prepay_rate';

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_CENTER . '.installment_prepay_rate'], [
                'p.*'
            ])
            ->where('p.del = ?', 0);

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getRateByTerm($channel_id, $good_price_log_id, $pay_term_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_CENTER .'.installment_package'], [
                'pr.*',
            ])
            ->joinLeft(['pr' => DATABASE_CENTER . '.installment_prepay_rate'], 'p.prepay_rate_id = pr.id', [])
            ->where('p.channel_id = ?', $channel_id)
            ->where('p.good_price_log_id = ?', $good_price_log_id)
            ->where('p.pay_term_id = ?', $pay_term_id)
            ->where('p.from_date <= CURDATE()')
            ->where('p.to_date IS NULL OR p.to_date >= CURDATE()')
            ->where('pr.del = ?', 0)
            ->group('pr.id')
            ->order('pr.rate ASC');

        $result = $db->fetchAll($select);

        return $result;
    }
}