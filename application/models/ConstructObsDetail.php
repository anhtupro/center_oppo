<?php

class Application_Model_ConstructObsDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'construct_obs_detail';
    protected $_schema = DATABASE_TRADE;

    public function getDetail($construct_obs_id, $type_category = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE . '.construct_obs'], [
                'category_name' => 'c.name',
                'd.quantity'
            ])
            ->join(['d' => DATABASE_TRADE . '.construct_obs_detail'], 'o.id = d.construct_obs_id', [])
            ->joinLeft(['c' => DATABASE_TRADE .'.category'], 'd.category_id = c.id', [])
            ->where('o.id = ?', $construct_obs_id)
            ->order('c.name');

        if ($type_category) {
            $select->where('c.type = ?', $type_category);
        }

        $result = $db->fetchAll($select);

        return $result;

    }
}