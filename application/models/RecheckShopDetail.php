<?php

class Application_Model_RecheckShopDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'recheck_shop_detail';
    protected $_schema = DATABASE_TRADE;

    public function getRecheckShopDetail($recheckShopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['r' => DATABASE_TRADE.'.recheck_shop'], [
                         'r.*',
                         'category_name' => 'c.name',
                         'check_shop_quantity' => 'ad.quantity',
                         'recheck_shop_quantity' => 'rd.quantity'
                     ])
                    ->joinLeft(['rd' => DATABASE_TRADE.'.recheck_shop_detail'], 'r.id = rd.recheck_shop_id', [])
                    ->joinLeft(['a' => DATABASE_TRADE.'.app_checkshop'], 'r.app_check_shop_id = a.id', [])
                    ->joinLeft(['ad' => DATABASE_TRADE.'.app_checkshop_detail'], 'a.id = ad.checkshop_id AND rd.category_id = ad.category_id', [])
                    ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'rd.category_id = c.id', [])
                    ->where('r.id = ?',$recheckShopId);

        $result = $db->fetchAll($select);

        return $result;
    }
}    