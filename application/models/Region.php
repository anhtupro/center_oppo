<?php
class Application_Model_Region extends Zend_Db_Table_Abstract
{
    protected $_name = 'region';

    function fetchPagnation($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        if (isset($params['leader_ids']) and $params['leader_ids'])
            $select->where('FIND_IN_SET(?, p.leader_ids)', $params['leader_ids']);

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {
            $where = $this->getAdapter()->quoteInto('del = ?', 0);
            $data = $this->fetchAll($where, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    /**
     * @param $staffId
     * @return mixed
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getRegionByRsm($staffId)
    {
        $query = "select a.region_id
from staff s
join regional_market rm on s.id = :staff_id and s.regional_market = rm.id
join area a on rm.area_id = a.id";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result['region_id'];
    }
}