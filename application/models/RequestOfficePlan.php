<?php
class Application_Model_RequestOfficePlan extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_office_plan';
    
    protected $_schema = DATABASE_SALARY;
    
    function fetchPagination($page, $limit, &$total, $params) {

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "group_title"       => "g.title",
            "group_type_title"  => "r.title",
            "team_name"         => "t.name",
            "supplier_name"     => "s.title",
            "p.request_type_group", 
            "p.month", 
            "p.year", 
            "p.campaign_id",
            "p.request_type",
            "p.team_id", 
            "p.supplier_id", 
            "p.details",
            "p.note",
            "i.project_id",
            "total_price" => "SUM(i.price)",
            "i.month",
            "i.year",
            "i.currency",
            "i.from_date", 
            "i.to_date",
            "project_name" => "project.name",
            "m.plan_info_id",
            "m.total_payment"
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office_plan'), $arrCols);
        $select->joinLeft(array('i'=> DATABASE_SALARY.'.request_office_plan_info'), 'i.rq_plan_id = p.id', array());
        $select->joinLeft(array('g'=>'request_type_group'), 'g.id = p.request_type_group', array());
        $select->joinLeft(array('r'=>'request_type'), 'r.id = p.request_type', array());
        $select->joinLeft(array('t'=>'team'), 't.id = p.team_id', array());
        $select->joinLeft(array('s'=>'supplier'), 's.id = p.supplier_id', array());
        $select->joinLeft(array('project'=>'purchasing_project'), 'project.id = i.project_id', array());
        
        
        $nestedSelect = "SELECT
                         p.plan_info_id,
                         p.request_office_id,
                         SUM( p.`use` ) total_payment 
                         FROM ".DATABASE_SALARY.".request_office_plan_map p
                         LEFT JOIN ".DATABASE_SALARY.".request_office r ON r.id = p.request_office_id 
                         WHERE p.is_del = 0 AND r.del = 0
                         GROUP BY p.plan_info_id";
        
        $select->joinLeft(array('m' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 'm.plan_info_id = i.id', array());
        

        $budget_admin = unserialize(BUDGET_ADMIN);
        if(!in_array($userStorage->id, $budget_admin)){
            $select->where('p.team_id = ?', $params['team_id_list']);
            $select->where('p.department_id = ?', $params['department_id_list']);
        }

        if(isset($params['team_id']) AND $params['team_id']){
            $select->where('p.team_id = ?', $params['team_id']);
        }

        if(isset($params['department_id']) AND $params['department_id']){
            $select->where('p.department_id = ?', $params['department_id']);
        }
        
        $select->where('p.del = 0');
        $select->where('i.del = 0');
        
        if(isset($params['request_type']) AND $params['request_type']){
            $select->where('p.request_type = ?', $params['request_type']);
        }

        if(isset($params['details']) AND $params['details']){
            $select->where("p.details LIKE ?", '%'.$params['details'].'%');
        }

        if(isset($params['note']) AND $params['note']){
            $select->where("p.note LIKE ?", '%'.$params['note'].'%');
        }

        $select->group('p.id');
        
        $select->order('p.id DESC');
        
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    function getListPlan($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "group_title"       => "g.title",
            "group_type_title"  => "r.title",
            "team_name"         => "t.name",
            "supplier_name"     => "s.title",
            "p.request_type_group", 
            "p.month", 
            "p.year", 
            "p.campaign_id",
            "p.request_type",
            "p.team_id", 
            "p.supplier_id", 
            "p.details", 
            "p.from_date", 
            "p.to_date", 
            "p.note", 
            "p.price", 
            "p.invoice_date",
            "p.invoice_number"
        );

        $select->from(array('p' => $this->_name), $arrCols);
        $select->joinLeft(array('g'=>'request_type_group'), 'g.id = p.request_type_group', array());
        $select->joinLeft(array('r'=>'request_type'), 'r.id = p.request_type', array());
        $select->joinLeft(array('t'=>'team'), 't.id = p.team_id', array());
        $select->joinLeft(array('s'=>'supplier'), 's.id = p.supplier_id', array());
        
        if(isset($params['request_type']) AND $params['request_type']){
            $select->where('p.request_type = ?', $params['request_type']);
        }
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function loadBudgetList($params) {

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "group_title"       => "g.title",
            "group_type_title"  => "r.title",
            "team_name"         => "t.name",
            "supplier_name"     => "s.title",
            "p.request_type_group", 
            "p.month", 
            "p.year", 
            "p.campaign_id",
            "p.request_type",
            "p.team_id", 
            "p.supplier_id", 
            "p.details",
            "p.note",
            "i.project_id",
            "i.price",
            "i.month",
            "i.year",
            "i.currency",
            "i.from_date", 
            "i.to_date",
            "project_name" => "project.name",
            "m.plan_info_id",
            "m.total_payment",
            "info_id" => "i.id",
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office_plan'), $arrCols);
        $select->joinLeft(array('i'=> DATABASE_SALARY.'.request_office_plan_info'), 'i.rq_plan_id = p.id', array());
        $select->joinLeft(array('g'=>'request_type_group'), 'g.id = p.request_type_group', array());
        $select->joinLeft(array('r'=>'request_type'), 'r.id = p.request_type', array());
        $select->joinLeft(array('t'=>'team'), 't.id = p.team_id', array());
        $select->joinLeft(array('s'=>'supplier'), 's.id = p.supplier_id', array());
        $select->joinLeft(array('project'=>'purchasing_project'), 'project.id = i.project_id', array());
        
        
        $nestedSelect = "SELECT
                         p.plan_info_id,
                         p.request_office_id,
                         SUM( p.`use` ) total_payment 
                         FROM ".DATABASE_SALARY.".request_office_plan_map p
                         LEFT JOIN ".DATABASE_SALARY.".request_office r ON r.id = p.request_office_id 
                         WHERE p.is_del = 0 AND r.del = 0
                         GROUP BY p.plan_info_id";
        
        $select->joinLeft(array('m' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 'm.plan_info_id = i.id', array());
        
        $select->where('p.team_id = ?', $params['team_id']);
        $select->where('p.department_id = ?', $params['department_id']);
        $select->where('p.del = 0');
        $select->where('i.del = 0');
        $select->where('p.request_type = ?', $params['request_type']);
        
        $select->order('p.id DESC');
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function listDepartmentPlan($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.company_id", 
            "p.request_type_group", 
            "p.request_type", 
            "p.department_id", 
            "p.area_id", 
            "p.team_id", 
            "p.supplier_id", 
            "p.details", 
            "p.note",
            "i.from_date", 
            "i.to_date", 
            "total_plan" => "SUM(i.price)", 
            "i.month", 
            "i.year", 
            "i.project_id", 
            "i.currency",
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office_plan'), $arrCols);
        $select->joinLeft(array('i' => DATABASE_SALARY.'.request_office_plan_info'), 'i.rq_plan_id = p.id', array());

        $select->where('p.del = 0 AND i.del = 0');
        $select->where('i.year = ?', $params['year']);
        
        $select->group("p.department_id");
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function listRequestOfficeUse($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.company_id", 
            "p.request_type_group", 
            "p.request_type", 
            "p.department_id", 
            "p.area_id", 
            "p.team_id", 
            "p.supplier_id", 
            "p.details", 
            "p.note",
            "i.from_date", 
            "i.to_date", 
            "i.month", 
            "i.year", 
            "i.project_id", 
            "i.currency",
            "total_use" => "SUM(m.use)"
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office_plan'), $arrCols);
        $select->joinLeft(array('i' => DATABASE_SALARY.'.request_office_plan_info'), 'i.rq_plan_id = p.id', array());
        $select->joinLeft(array('m' => DATABASE_SALARY.'.request_office_plan_map'), 'm.plan_info_id = i.id', array());
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office'), 'r.id = m.request_office_id', array());

        $select->where('p.del = 0 AND i.del = 0');
        $select->where('i.year = ?', $params['year']);
        $select->where('r.del = 0');
        
        $select->group("p.department_id");
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function listRequestOfficeUseQuater($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.company_id", 
            "p.request_type_group", 
            "p.request_type", 
            "p.department_id", 
            "p.area_id", 
            "p.team_id", 
            "p.supplier_id", 
            "p.details", 
            "p.note",
            "i.from_date", 
            "i.to_date", 
            "i.month", 
            "i.year", 
            "i.project_id", 
            "i.currency",
            "q.quarter",
            "total_use" => "SUM(m.use)"
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office_plan'), $arrCols);
        $select->joinLeft(array('i' => DATABASE_SALARY.'.request_office_plan_info'), 'i.rq_plan_id = p.id', array());
        $select->joinLeft(array('m' => DATABASE_SALARY.'.request_office_plan_map'), 'm.plan_info_id = i.id', array());
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office'), 'r.id = m.request_office_id', array());
        $select->joinLeft(array('q' => 'month_quarter'), 'q.month = i.month', array());

        $select->where('p.del = 0 AND i.del = 0');
        $select->where('i.year = ?', $params['year']);
        $select->where('r.del = 0');
        
        $select->group(["p.department_id", "q.quarter"]);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    

}