<?php
class Application_Model_BigArea extends Zend_Db_Table_Abstract
{
    protected $_name = 'bigarea';
    function get_big_area(){

            $data = $this->fetchAll();
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
        return $result;
    }
    function get_big_area_by_area($listArea){
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('ba' => 'region' ));
        $select->joinLeft(array('a' => 'area'),'a.region_id=ba.id',array());
        $select->where('a.id IN (?)',$listArea);
        $select->group('ba.id');
        $select->order('ba.name');
        $res=$db->fetchAll($select);
        $arrTmp=array();
        foreach ($res as $key => $value) {
            $arrTmp[$value['id']]=$value['name'];
        }
        return $arrTmp;
    }
}