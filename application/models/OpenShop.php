<?php
class Application_Model_OpenShop extends Zend_Db_Table_Abstract
{
    protected $_name = 'open_shop';
 
    public function getOpenShop($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => "p.id",
            'store_id' => 'p.store_id',
            'reason'   => 'p.reason',
            'created_at'    => 'p.created_at',
            'created_by'    => 'p.created_by',
            'store_name'    => 's.name',
            'reason_name'   => 'r.name',
            'note'   => 'p.note',
            "created_name"    => "CONCAT(staff.firstname, ' ',staff.lastname)",
            "status" => "p.status",
            "status_name" => "status.name",
            "p.reject",
//            "p.latitude",
//            "p.longitude"
        );

        $select->from(array('p' => 'open_shop'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'open_shop_reason'), 'r.id = p.reason', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.created_by', array());
        $select->joinLeft(array('status' => DATABASE_TRADE.'.app_status'), 'status.status = p.status AND status.type = 10', array());
        $select->where('p.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }


    public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
 
        $select->from(array('p' => 'open_shop'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.status', 'p.reject', 'p.created_at'));
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array('store_name' => "s.name",'store_code' => "s.store_code",'store_id' => "s.id"));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array('province_name' => 'r.name'));
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = s.district', array('district_name'=>'r2.name'));
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array('area_name'=>'a.name'));
        
        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id'] );
        }
        
        if(!empty($params['area'])){
            $select->where('r.area_id IN (?)', $params['area'] );
        }
        
        if(!empty($params['access_shop_id'])){
            $select->where('s.id IN (?)', $params['access_shop_id']);
        }
        
        if(!empty($params['store_code'])){
            $select->where('s.store_code = ?', $params['store_code']);
        }
        if(!empty($params['store_name'])){
             $select->where('s.name LIKE ?','%'.$params['store_name'].'%');
        }
        
        if(!empty($params['reason'])){
            $select->where('p.reason = ?', $params['reason']);
        }        
        if(!empty($params['status'])){
            $select->where('p.status = ?', $params['status']);
        }
        
        if(!empty($params['created_from'])){
            $select->where('DATE(p.created_at) >= ?', $params['created_from']);
        }
        if(!empty($params['created_to'])){
            $select->where('DATE(p.created_at) <= ?', $params['created_to']);
        }
        
        
        $select->limitPage($page, $limit);
        $select->order('p.id DESC');
        
        $result = $db->fetchAll($select);
         
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    
}