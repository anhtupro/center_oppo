<?php
class Application_Model_CompanyCosting extends Zend_Db_Table_Abstract
{
    protected $_name = 'company_costing';
    //protected $_schema = DATABASE_COST;



    public function getCompany()
    {
    	$db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'name'        => 'p.name' 
        );
        $select->from(array('p'=>'company_costing'), $arrCols);
        $result = $db->fetchAll($select);
        return $result;
    }
    
}
