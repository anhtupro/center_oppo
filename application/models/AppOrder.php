<?php
class Application_Model_AppOrder extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_order';
    protected $_schema = DATABASE_TRADE;

    function get_cache(){
        
    }

    function getStoreInfo($store_id){
    	if( empty($store_id) AND empty($store_id) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'store_id'     => 'p.id',
            'store_name'   => "p.name",
            'area_id'      => "IFNULL(c.area_id, r.area_id)",
        );

        $select = $db->select()
                ->from(array('p'=> 'store'), $arrCols);
        $select->joinLeft(array('r'=> 'regional_market'), 'r.id = p.regional_market',array());
        $select->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = p.id',array());
        
        $select->where('p.id = ?', $store_id);
       
        $result = $db->fetchRow($select);

        return $result;
    }
}