<?php

class Application_Model_SpecialUser extends Zend_Db_Table_Abstract
{
    protected $_name = 'special_user';
    protected $_schema = DATABASE_TRADE;

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => DATABASE_TRADE.'.special_user'], [
                       's.staff_id'
                     ]);
   
        $result = $db->fetchCol($select);

        return $result;
    }
}