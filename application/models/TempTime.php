<?php
class Application_Model_TempTime extends Zend_Db_Table_Abstract
{
	protected $_name = 'temp_time';
    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.staff_id,p.note,p.approved_by,GROUP_CONCAT(p.date,",") as arrDate,p.office_time,GROUP_CONCAT(p.office_time,",") as office_time'
            ));
        $select->from(array('p' => "temp_time"), $arrCols);
        $select->joinLeft(array('s' => "staff"),"s.id=p.staff_id",array("concat(s.firstname,' ',s.lastname) as name","s.department","s.team","s.title","s.code","s.department","s.team","s.title"));

        $select->where('p.add_reason = ?', 7); //Work From Home Reason
        if (!empty($params["from_date"])) {
            $select->where('p.date >= ?', $params["from_date"]);
        }
        if (!empty($params["to_date"])) {
            $select->where('p.date <= ?', $params["to_date"]);
        }
        if (!empty($params["department"])) {
            $select->where('s.department = ?', $params["department"]);
        }
        if (!empty($params["team"])) {
            $select->where('s.team = ?', $params["team"]);
        }
        if (!empty($params["title"])) {
            $select->where('s.title = ?', $params["title"]);
        }

        $select->group('p.staff_id');
        $select->order('s.department DESC');
//        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

}