<?php

class Application_Model_AppCheckshopDetailChildPainting extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_checkshop_detail_child_painting';
    protected $_schema = DATABASE_TRADE;

    public function getPainting($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'p.*',
                'material_name' => 'm.name'
            ])
            ->joinLeft(['p' => DATABASE_TRADE.'.app_checkshop_detail_child_painting'], 'd.id = p.app_checkshop_detail_child_id', [])
            ->joinLeft(['m' => DATABASE_TRADE.'.category_material'], 'p.material_id = m.id', [])
            ->where('p.id IS NOT NULL')
            ->where('d.quantity > 0');

        if ($params['checkshop_id']) {
            $select->where('d.checkshop_id = ?', $params['checkshop_id']);
        }

        if ($params['area_id']) {
            $select->where('d.area_id = ?', $params['area_id']);
        }

        if ($params['list_app_checkshop_detail_child_id']) {
            $select->where('p.app_checkshop_detail_child_id IN (?)', $params['list_app_checkshop_detail_child_id']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list[$element['app_checkshop_detail_child_id']] [] = $element;
        }
        return $list;
    }
}