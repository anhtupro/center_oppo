<?php
class Application_Model_ApproveCosting extends Zend_Db_Table_Abstract
{
	//protected $_schema = DATABASE_COST;
    protected $_name = 'approve_costing';
    public function getDataApproved($params)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'cost_id'        => 'p.cost_id', 
            'staff_id'        => 'p.staff_id' , 
            'created_at'    => 'p.created_at'
        );
         
        $select->from(array('p'=> 'approve_costing'), $arrCols);
        $select->where('p.cost_id = ?',$params['id']);
        $select->where('p.staff_id = ?',$params['staff_id']);
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getApprove($cost_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            't.img_signature', 
            't.key_signature', 
            't.staff_id', 
            't.swh_signature'
        );

        $select->from(array('p' => 'approve_costing'), $arrCols);
        $select->joinLeft(array('t' => 'type_approve_details_costing'), 't.id = p.type_approve_details_id', array());
        $select->where('p.cost_id = ?', $cost_id);

        $result = $db->fetchAll($select);

        return $result;
    }


 }