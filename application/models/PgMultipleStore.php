<?php
class Application_Model_PgMultipleStore extends Zend_Db_Table_Abstract
{
	protected $_name = 'pg_multiple_store';

    function fetchPagination(&$page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('s' => 'staff'),
                array(new Zend_Db_Expr(($limit ? 'SQL_CALC_FOUND_ROWS ' : ''). 's.id'), 'staff_id' => 's.id', 's.code', 's.email', 's.firstname', 's.lastname', 's.joined_at'))
            ->join(array('p' => $this->_name), 's.id=p.staff_id', array('p_id' => 'p.id', 'p.approved_from', 'p.removed_at', 'p.number_of_store'))
            ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array('province_name' => 'r.name'))
            ->join(array('a' => 'area'), 'a.id=r.area_id', array('area_name' => 'a.name'))
        ;

        if (isset($params['name']) and $params['name'])
            $select->where("CONCAT(TRIM(s.firstname), ' ', TRIM(s.lastname)) LIKE ?", '%'.$params['name'].'%');

        if (isset($params['email']) and $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']). EMAIL_SUFFIX);

        if (isset($params['area']) and $params['area'])
            $select->where('a.id LIKE', $params['area']);

        if (isset($params['province']) and $params['province'])
            $select->where('r.id LIKE', $params['province']);

        $select->order(array('p.approved_from DESC', 's.lastname'));

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
}
