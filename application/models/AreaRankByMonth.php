<?php
class Application_Model_AreaRankByMonth extends Zend_Db_Table_Abstract
{
	protected $_name = 'area_rank_by_month';

    function getDataChot($getMonth, $getYear) {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => 'area_rank_by_month'], [
                'c.*'
            ])
            ->where('c.month = ?', $getMonth)
            ->where('c.year = ?', $getYear)
            ->order('c.rank');

        $result = $db->fetchAll($select);


        $data_area = [];
        $total_quantity = 0;
        $total_quantity_activated = 0;
        $total_value = 0;
        $total_value_kpi = 0;
        foreach ($result as $item) {
            $data_area [$item['area_id']] = $item;

            $total_quantity += $item['quantity'];
            $total_quantity_activated += $item['quantity_activated'];
            $total_value += $item['value'];
            $total_value_kpi += $item['value_kpi'];
        }

        $list = [];
        $list ['data_area'] = $data_area;
        $list ['total_quantity'] = $total_quantity;
        $list ['total_quantity_activated'] = $total_quantity_activated;
        $list ['total_value'] = $total_value;
        $list ['total_value_kpi'] = $total_value_kpi;

        return $list;
    }






    public function getDataOppoByArea($params)
    {
        $db = Zend_Registry::get('db');

        $data_oppo = array();
        $total_value_oppo = $total_quantity_oppo = $total_quantity_activated_oppo = $total_value_oppo_phone = 0;

        $stmt_out = $db->prepare("CALL `PR_fetch_Area_Point`(:p_from_date, :p_to_date, :p_area_list)");
        $stmt_out->bindParam('p_from_date', $params['from_f'], PDO::PARAM_STR);
        $stmt_out->bindParam('p_to_date', $params['to_f'], PDO::PARAM_STR);
        $stmt_out->bindParam('p_area_list', $params['area_list'], PDO::PARAM_STR);
        $stmt_out->execute();
        $data_oppo = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;

        foreach ($data_oppo as $item) {
            $total_quantity_oppo += $item['total_quantity'];
            $total_quantity_activated_oppo += $item['total_activated'];
            $total_value_oppo += $item['total_value'];
            $total_value_oppo_phone += $item['value_phone'];
        }

        $sales_oppo = array();
        $sales_final_oppo = array();

        // tính active vượt và value tính kpi từng khu vực
        foreach ($data_oppo as $item) {
            $percent_not_activated_area = $percent_not_activated_total = $active_vuot = 0;
            $val = [];

            $percent_not_activated_area = round((($item['total_quantity'] - $item['total_activated']) / $item['total_quantity']) * 100, 1); // tỉ lệ chưa active khu vực
            $percent_not_activated_total = round((($total_quantity_oppo - $total_quantity_activated_oppo) / $total_quantity_oppo) * 100, 1); // tỉ lệ chưa active cả nước
            $active_vuot = $percent_not_activated_area - $percent_not_activated_total;

            $val = $item;
            $val ['active_vuot'] = $active_vuot;
            $val ['percent_not_activated_area'] = $percent_not_activated_area;

            if ($active_vuot <= 0) {
                $val['value_kpi'] = $item ['total_value'];
                $val['value_kpi_phone'] = $item ['value_phone'];
                $val['value_kpi_watch'] = $item ['value_watch'];
            } else {
                $val['value_kpi'] = $item ['total_value'] * ((100 - $active_vuot) / 100);
                $val['value_kpi_phone'] = $item ['value_phone'] * ((100 - $active_vuot) / 100);
                $val['value_kpi_watch'] = $item ['value_watch'] * ((100 - $active_vuot) / 100);
            }

            $sales_oppo [] = $val;
        }

        // tổng value tính kpi cả nước
        $total_value_kpi_oppo = 0;
        $total_value_kpi_oppo_phone = 0;
        foreach ($sales_oppo as $key => $value) {
            $total_value_kpi_oppo += $value['value_kpi'];
            $total_value_kpi_oppo_phone += $value['value_kpi_phone'];
        }

        // point mới theo value tính kpi
        foreach ($sales_oppo as $item) {
            $point_moi = 0;
            $val = [];

//            $point_moi = round(($item['value_kpi'] / $total_value_kpi_oppo) * 60 / ($item['region_share'] / 100), 2);
            $point_moi = round(($item['value_kpi_phone'] / $total_value_kpi_oppo_phone) * 60 / ($item['region_share'] / 100), 2);
            $val = $item;
            $val['point'] = $point_moi ? $point_moi : 0;
            $sales_final_oppo[] = $val;
        }

        $sales_final_oppo_new = [];
        foreach ($sales_final_oppo as $element) {
            $sales_final_oppo_new [$element['area_id']] = $element;
        }

//        usort($sales_final_oppo, array($this, 'cmp'));

        return $sales_final_oppo_new;
    }

    public function getDataAllOrderByRank($sales_final_oppo, $sales_final_realme)
    {
        $QAreaRankMoney = new Application_Model_AreaRankMoney();
        $QArea = new Application_Model_Area();

        $list_area_kpi = $QArea->getAreaKpi();
        $list_money = $QAreaRankMoney->getAll();// list thưởng phạt

        $sales_final_all = [];
        $total_value_kpi_all = 0;
        $total_value_kpi_phone_all = 0;

// set point mới cho cả nước
        foreach ($list_area_kpi as $area_id => $area_name) {
            $realme = $sales_final_realme [$area_id];
            $oppo = $sales_final_oppo [$area_id];

            $total_value_kpi_all += $oppo['value_kpi'] + $realme['value_kpi'];
            $total_value_kpi_phone_all += $oppo['value_kpi_phone'] + $realme['value_kpi'];
        }
        foreach ($list_area_kpi as $area_id => $area_name) {

//foreach ($sales_final_oppo as $oppo) {
            $realme = $sales_final_realme [$area_id];
            $oppo = $sales_final_oppo [$area_id];

            $point =  round( (($oppo['value_kpi_phone'] + $realme['value_kpi']) / $total_value_kpi_phone_all) * 60 / ($oppo['region_share'] / 100),2 );

            $sales_final_all [] = [
                'area_id' => $area_id,
                'area_name' =>   $area_name,
                'region_share' => $oppo['region_share'],
                'quantity_oppo' => $oppo['total_quantity'],
                'quantity_activated_oppo' => $oppo['total_activated'],
                'percent_not_activated_oppo' => $oppo['percent_not_activated_area'],
                'value_oppo' => $oppo['total_value'],
                'value_kpi_oppo' => $oppo['value_kpi'],
                'point_oppo' => $oppo['point'],
                'quantity_watch_oppo' =>$oppo['quantity_watch'],
                'quantity_phone_oppo' => $oppo['quantity_phone'],
                'quantity_activated_watch_oppo' =>$oppo['quantity_activated_watch'],
                'quantity_activated_phone_oppo' => $oppo['quantity_activated_phone'],

                'value_phone_oppo' => $oppo['value_phone'],
                'value_watch_oppo' => $oppo['value_watch'],
                'value_kpi_phone_oppo' => $oppo['value_kpi_phone'],
                'value_kpi_watch_oppo' => $oppo['value_kpi_watch'],

                'quantity_realme' => $realme['total_quantity'],
                'quantity_activated_realme' => $realme['total_activated'],
                'percent_not_activated_realme' => $realme['percent_not_activated_area'],
                'value_realme' => $realme['total_value'],
                'value_kpi_realme' => $realme['value_kpi'],

                'quantity_all' => $oppo['total_quantity'] + $realme['total_quantity'],
                'quantity_phone_all' => $oppo['quantity_phone'] + $realme['total_quantity'],
                'quantity_activated_all' => $oppo['total_activated'] + $realme['total_activated'],
                'quantity_activated_phone_all' => $oppo['quantity_activated_phone'] + $realme['total_activated'],
                'percent_not_activated_all' => $oppo['percent_not_activated_area'] + $realme['percent_not_activated_area'],
                'value_all' => $oppo['total_value'] + $realme['total_value'],
                'value_phone_all' => $oppo['value_phone'] + $realme['total_value'],
                'value_kpi_all' => $oppo['value_kpi'] + $realme['value_kpi'],
                'value_kpi_phone_all' => $oppo['value_kpi_phone'] + $realme['value_kpi'],
                'point' => ! is_nan($point) ? $point : 0,

                'quantity_highend_all' => $oppo['total_quantity_highend'] ,
                'quantity_lowend_all' => $oppo['total_quantity_lowend'],
                'quantity_hero_product' => $oppo['total_quantity_hero_product'],
                'quantity_hero_product_target' => $oppo['total_quantity_hero_product_target'],
                'not_enough_quantity_hero_product' => $oppo['total_quantity_hero_product_target'] - $oppo['total_quantity_hero_product']
            ];
        }



        // sort by point
        array_multisort(array_column($sales_final_all, 'point'), SORT_DESC, $sales_final_all);

        $sales_final_all_new = [];
        $total_quantity_all = 0;
        $total_quantity_phone_all = 0;
        $total_quantity_activated_all = 0;
        $total_quantity_activated_phone_all = 0;
        $total_value_all = 0;
        $total_value_phone_all = 0;

// tính tiền thưởng phạt

        foreach ($sales_final_all as $key => $element) {
            $val = [];
            $money = 0;
            $rank = $key + 1;
            $val = $element;
            $rank_money = $list_money [$rank];
            
            if ($rank_money['type'] == 1) { // thưởng
                $money = $rank_money['money'] * $element['quantity_highend_all'];
            } elseif ($rank_money['type'] == 2) { // phạt
//                $money = $rank_money['money'] * $element['quantity_lowend_all'];
                if ($element['not_enough_quantity_hero_product'] > 0) {
                    $money = $rank_money['money'] * $element['not_enough_quantity_hero_product'];
                } else {
                    $money = 0;
                }
            }

            $val ['money'] = $money;
            $val['rank_type'] = $rank_money['type'];
            $val['rank'] = $rank_money['rank'] ? $rank_money['rank'] : $rank;

            $sales_final_all_new [] = $val;

            // lấy ra total unit  và unit activated oppo + realme  , total value oppo + realme
            $total_quantity_all += $element['quantity_all'];
            $total_quantity_phone_all += $element['quantity_phone_all'];
            $total_quantity_activated_all += $element['quantity_activated_all'];
            $total_quantity_activated_phone_all += $element['quantity_activated_phone_all'];
            $total_value_all += $element['value_all'];
            $total_value_phone_all += $element['value_phone_all'];
        }

        // format lại array
        $list = [];
        foreach ($sales_final_all_new as $item) {
            $list [$item['area_id']] = $item;
        }

        $result = [] ;
        $result ['data_area'] = $list;
        $result ['total_quantity_all'] = $total_quantity_all;
        $result ['quantity_phone_all'] = $total_quantity_phone_all;
        $result ['total_quantity_activated_all'] = $total_quantity_activated_all;
        $result ['quantity_activated_phone_all'] = $total_quantity_activated_phone_all;
        $result ['total_value_all'] = $total_value_all;
        $result ['value_phone_all'] = $total_value_phone_all;
        $result ['total_value_kpi_all'] = $total_value_kpi_all;
        $result ['total_value_kpi_phone_all'] = $total_value_kpi_phone_all;

        return $result;
    }

    public function getData($params)
    {
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_report_area`(:p_from_date, :p_to_date)");
        $stmt_out->bindParam('p_from_date', $params['from_f'], PDO::PARAM_STR);
        $stmt_out->bindParam('p_to_date', $params['to_f'], PDO::PARAM_STR);
        $stmt_out->execute();
        $stmt_out->nextRowset();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;


        foreach ($data as $item) {
            $list [$item['area_id']]  = $item;
        }

        return $list;
    }

    public function getDataSpecialModel($params)
    {
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_report_special_model`(:p_from_date, :p_to_date)");
        $stmt_out->bindParam('p_from_date', $params['from_date'], PDO::PARAM_STR);
        $stmt_out->bindParam('p_to_date', $params['to_date'], PDO::PARAM_STR);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();

        return $data;
    }
}