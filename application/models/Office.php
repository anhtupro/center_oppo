<?php
class Application_Model_Office extends Zend_Db_Table_Abstract
{
	protected $_name = 'office';

	function fetchPagination($page, $limit, &$total, $params){
		$db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'office'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'type_name' 		=> 'ot.name',
                'area_name' 		=> 'a.name',
                'province_name'     => 'rm.name'
            ))
            ->joinLeft(array('a'=>'area'),'p.area_id = a.id',array())
						->joinLeft(array('rm'=>'regional_market'),'p.province = rm.id',array())
            ->joinLeft(array('ot'=>'office_type'),'p.office_type = ot.id',array())
            ->where('p.del = ?',0)
        ;

        if(isset($params['office_name']) AND $params['office_name']){
        	$select->where('p.office_name LIKE ?','%'.$params['office_name'].'%');
        }
        if(isset($params['office_type']) AND $params['office_type']){
        	$select->where('p.office_type = ?', $params['office_type'] );
        }
        if(isset($params['area_id']) AND $params['area_id']){
        	$select->where('p.area_id = ?',$params['area_id']);
        }
		  	if(isset($params['province']) AND $params['province']){
          $select->where('p.province = ?',$params['province']);
        }
        if(isset($params['province']) AND $params['province']){
            $select->where('p.province = ?',$params['province']);
        }
        if(isset($params['district']) AND $params['district']){
            $select->where('p.district = ?',$params['district']);
        }
        $order_str = '';
        if(isset($params['sort']) AND $params['sort']){
        	$collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'area_name' || $params['sort'] == 'province_name' || $params['sort'] == 'district_name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $params['sort'].$collate . $desc;
            } else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }
            $select->order(new Zend_Db_Expr($order_str));
        }else{
        	$select->order('p.created_at ASC');	
        }
        
        if ($limit)
            $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
		
        return $result;
	}

	public function save($data,$id){
		$db = Zend_Registry::get('db');
		$userStorage    = Zend_Auth::getInstance()->getStorage()->read();
		$arr = array();
		$db->beginTransaction();
		try {	
			if($id){
				$where = $this->getAdapter()->quoteInto('id = ?',$id);
				$this->update($data,$where);
			}else{

				//check duplicate
				$select = $db->select()
					->from(array('p'=>'office'),'p.*')
					->where('p.office_name = ?',$data['office_name'])
					->where('area_id = ?',$data['area_id'])
					;
				$row = $db->fetchRow($select);
				if($row){
					$arr = array('code'=>-2,'message'=>'office name is existed');
					return $arr;
				}else{
					$data['created_at'] = date('Y-m-d H:i:s');
					$data['created_by'] = $userStorage->id;
					$this->insert($data);	
				}
			}
			$arr = array('code'=>1,'message'=>'Done');
			$db->commit();
			
		} catch (Exception $e) {
			$db->rollBack();
			$arr = array('code'=>-1,'message'=>$e->getMessage());
		}
		return $arr;
	}

	public function fetchAllOffice()
	{
		$db = Zend_Registry::get('db');

		$sql = "SELECT o.id as `id`,
					CONCAT(a.name, ' - ', o.office_name) as `office_name`
				FROM office o
				JOIN area a ON a.id = o.area_id
				WHERE o.del = 0";
		$stmt = $db->prepare($sql);
		$stmt->execute();

		$data = $stmt->fetchAll();
		$stmt->closeCursor();
		$stmt = $db = null;

		return $data;
	}

	public function get_all($params = array()){
		$db = Zend_Registry::get('db');
		$cols = array(
				'id' => 'p.id',
				'name' => 'CONCAT(a.name," - ",p.office_name)'
			);
		$select = $db->select()
			->from(array('p'=>'office'),$cols)
			->join(array('a'=>'area'),'a.id = p.area_id',array())
			->where('p.del = ?',0)
			->order('p.area_id')
			;
		$result = $db->fetchPairs($select);
		return $result;
	}
	
	function get_cache(){
	    $cache      = Zend_Registry::get('cache');
	    $result     = $cache->load($this->_name.'_cache');
	
	    if ($result === false) {
	
	        $data = $this->fetchAll(null, 'office_name');
	
	        $result = array();
	        if ($data){
	            foreach ($data as $item){
	                $result[$item->id] = $item->office_name;
	            }
	        }
	        $cache->save($result, $this->_name.'_cache', array(), null);
	    }
	    return $result;
	}
	
	public function getLocationOffice($office_id){
	    $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'office'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*'
                ))
            ->where('p.id = ?',$office_id);
          $row = $db->fetchRow($select);
          return $row;
	}
	public function getAreaId($str_list_officeIds)
	{
	    $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $select->from(array('o'=> 'office'), array('area_id' => 'o.area_id'))->where("o.id IN ($str_list_officeIds)");
        $result = $db->fetchAll($select);

        foreach ($result as $value) {
        	$areaIds[] = $value['area_id'];
        }
 
        return $areaIds;
            
	}
}