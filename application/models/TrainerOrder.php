<?php
class Application_Model_TrainerOrder extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_order';

    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*', 'count(p.id) sum','(count(p.id)-count(p.confirm_by)) no_confirm', '((count(p.id)-count(p.order_to_confirm_at)) - SUM(IF(p.order_to IS NULL, 1,0))) pending'));

        $select->joinleft(array('a'=>'trainer_type_asset'),'a.id = p.asset_id', array( 'asset_name' => 'a.name'));

        if (isset($params['sn']) and $params['sn'])
            $select->where('p.sn = ?', $params['sn']);

        if (isset($params['asset_id']) and $params['asset_id'])
        {
            $select->where('a.id = ?', $params['asset_id']);
        }

        if (isset($params['confirmed_at_from']) and $params['confirmed_at_from'])
            $select->where('p.confirmed_at >= ?', $params['confirmed_at_from']);

        if (isset($params['confirmed_at_to']) and $params['confirmed_at_to'])
            $select->where('p.confirmed_at <= ?', $params['confirmed_at_to']);

        if (isset($params['created_at_from']) and $params['created_at_from'])
            $select->where('p.created_at >= ?', $params['created_at_from']);

        if (isset($params['created_at_to']) and $params['created_at_to'])
            $select->where('p.created_at <= ?', $params['created_at_to']);

        if (isset($params['status']) and $params['status'])
            $select->where('p.status = ?', $params['status']);

        $select->where('p.del = ? OR p.del IS NULL',0);

        $select->group(array('p.sn'));
        $select->order('p.created_at DESC');
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function save($params)
    {
        $order_id                   = isset($params['order_id']) ? $params['order_id'] : null;
        $ip                         = isset($params['ip']) ? $params['ip'] : null;
        $userStorage                = isset($params['userStorage']) ? $params['userStorage'] : null;
        $date                       = isset($params['date']) ? $params['date'] : null;
        $list                       = isset($params['list']) ? $params['list'] : null;
        $source                     = isset($params['source']) ? $params['source']: null;
        $QTrainerOrderDetail        = new Application_Model_TrainerOrderDetail();
        $QTrainerOrderInDetail      = new Application_Model_TrainerOrderImei();

        $source = intval($source);
        if($source == 0){
            return array('code'=>-1,'message'=>'Vui lòng chọn nguồn nhập!');
        }

        if(!$date){
            return array('code'=>-1,'message'=>'Vui lòng nhập ngày tháng!');
        }

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            if($order_id){
                $order = $this->find($order_id)->current();
                if(!$order){
                    return array('code'=>-1,'message'=>'order not exist');
                }

                /* Kiểm tra imei trước khi update */
                $checkData = $this->checkData($list);
                if($checkData['code'] == -1 ){
                    return $checkData['message'];
                }

                /* lấy dữ liệu cũ */
                $selectOldDetail = $db->select()
                    ->from(array('p'=>'trainer_order_detail'),array('p.*'))
                    ->where('p.del IS NULL OR p.del = 0')
                    ->where('p.order_id = ?',$order_id)
                ;
                $listOldDetail = $db->fetchAll($selectOldDetail);
                $arrListOldDetail = array();
                foreach($listOldDetail as $item){
                    $arrListOldDetail[$item['id']] = $item;
                }

                foreach($list as $item){
                    if(isset($item['order_detail_id'])){
                        $arrOrderDetailId[] = $item['order_detail_id'];
                    }
                }
                $arrKeyOrderDetail      = array_keys($arrListOldDetail);
                $arrRemoveOrderDetail   = array_diff($arrKeyOrderDetail,$arrOrderDetailId);
                if(count($arrRemoveOrderDetail) > 0){
                    $whereRemoveDetail      = $QTrainerOrderDetail->getAdapter()->quoteInto('id IN (?)',$arrRemoveOrderDetail);
                    $QTrainerOrderDetail->delete($whereRemoveDetail);
                }

                foreach($list as $item){
                    if(isset($item['order_detail_id']) AND $item['order_detail_id']) {
                        $dataUpdate = array(
                            'asset_id'  => intval($item['asset_id']),
                            'type'      => intval($item['type']),
                            'quantity'  => intval($item['quantity']),
                            'note'      => ($item['type'] == CATEGORY_ASSET_TRAINER_STATIONERY ) ? $item['note_stationery'] : '',
                            'trainer_id' => intval($item['trainer_id']),
                            'source_text' => trim($item['source_text'])
                        );

                        //update detail
                        $where = $QTrainerOrderDetail->getAdapter()->quoteInto('id = ?', $item['order_detail_id']);
                        $QTrainerOrderDetail->update($dataUpdate, $where);

                        //xử lý imei
                        if($item['type'] == CATEGORY_ASSET_TRAINER_PHONE){

                            //nếu type không thay đổi
                            if($item['type'] == $arrListOldDetail[$item['order_detail_id']]['type']){

                                //danh sách imei cũ
                                $selectImei = $db->select()
                                        ->from(array('p'=>'trainer_order_imei'),array('p.id','p.imei'))
                                        ->where('order_detail_id = ?',$item['order_detail_id'])
                                        ->where('type = ?',1 )
                                        ->where('del IS NULL OR del = 0');
                                $imeis = $db->fetchPairs($selectImei);

                                //xóa imei
                                $arrKeyImei = array_keys($imeis);
                                $removeImei =  array_diff($arrKeyImei,$item['imei_id']);

                                if(count($removeImei) > 0){
                                    $whereRemoveImei = $QTrainerOrderInDetail->getAdapter()->quoteInto('id IN (?)',$removeImei);
                                    $QTrainerOrderInDetail->delete($whereRemoveImei);
                                }

                                foreach($item['imei'] as $k => $imei){
                                    if(isset($item['imei_id'][$k]) AND $item['imei_id'][$k]){
                                        $whereUpdateImei  = $QTrainerOrderInDetail->getAdapter()->quoteInto('id = ?',$item['imei_id'][$k]);
                                        $QTrainerOrderInDetail->update(array('imei'=>$imei,'note'=>$item['note'][$k],'output_trainer_id'=>$item['output_trainer_id'][$k]),$whereUpdateImei);
                                    }else{
                                        $dataIn = array(
                                            'imei'            => trim($imei),
                                            'note'            => $item['note'][$k],
                                            'asset_id'        => intval($item['asset_id']),
                                            'order_detail_id' => intval($item['order_detail_id']),
                                            'order_in'        => $order_id,
                                            'type'            => 1,
                                            'output_trainer_id' => intval($item['output_trainer_id'][$k])
                                        );
                                        $QTrainerOrderInDetail->insert($dataIn);
                                    }
                                }
                            }
                        }else{
                            // neu la Van phong pham thi xoa het imei
                            if($item['type'] != $arrListOldDetail[$item['order_detail_id']]['type']){
                                $whereImei = $QTrainerOrderInDetail->getAdapter()->quoteInto('order_detail_id = ?', $item['order_detail_id']);
                                $QTrainerOrderInDetail->delete($whereImei);
                            }
                        }
                    }else{
                        //insert new row
                        $data_detail = array(
                            'order_id' => $order_id,
                            'asset_id' => $item['asset_id'],
                            'quantity' => $item['quantity'],
                            'type'     => $item['type'],
                            'note'     => $item['note_stationery']
                        );
                        $order_detail_id = $QTrainerOrderDetail->insert($data_detail);

                        //Nếu phone thì nhập thêm imei
                        if($item['type'] == 1){
                            foreach($item['imei'] as $k => $imei):
                                $dataIn = array(
                                    'imei'            => $imei,
                                    'note'            => $item['note'][$k],
                                    'asset_id'        => $item['asset_id'],
                                    'order_detail_id' => $order_detail_id,
                                    'order_in'        => $order_id,
                                    'type'            => 1,
                                    'output_trainer_id' => intval($item['output_trainer_id'][$k])
                                );
                                $QTrainerOrderInDetail->insert($dataIn);
                            endforeach;
                        }
                    }//End insert data
                }//End foreach
            }else{
                /* Kiểm tra imei trước khi update */
                $checkData = $this->checkData($list,$source);
                if($checkData['code'] == -1 ){
                    return $checkData['message'];
                }

                $sn = date ('YmdHis') . substr ( microtime (), 2, 4 );
                $dataOrder = array(
                    'sn'         => $sn,
                    'date'       => $date,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id,
                    'status'     => ORDER_STATUS_PENDING_TRAINING,
                    'source'     => $source
                );
                $order_id = $this->insert($dataOrder);
                //detail
                foreach($list as $key => $value):
                    $note = '';
                    $data_detail = array(
                        'order_id'   => $order_id,
                        'asset_id'   => intval($value['asset_id']),
                        'quantity'   => intval($value['quantity']),
                        'type'       => intval($value['type']),
                        'note'       => $note,
                        'trainer_id' => intval($value['trainer_id']),
                        'source_text' => trim($value['source_text'])
                    );
                    $order_detail_id = $QTrainerOrderDetail->insert($data_detail);

                    //Nếu phone thì nhập thêm imei
                    if($value['type'] == 1){
                        foreach($value['imei'] as $k => $imei):
                            $dataIn = array(
                                'imei'            => $imei,
                                'note'            => $value['note'][$k],
                                'asset_id'        => $value['asset_id'],
                                'order_detail_id' => $order_detail_id,
                                'order_in'        => $order_id,
                                'type'            => 1,
                                'output_trainer_id' => intval($value['output_trainer_id'][$k])
                            );
                            $QTrainerOrderInDetail->insert($dataIn);
                        endforeach;
                    }
                endforeach;

            }
            $db->commit();
            return array('code'=>1,'message'=>'Done');
        } catch (Exception $e) {
            $db->rollBack();
            return array('code'=> -1,'message'=>$e->getMessage());
        }

    }

    function checkImei($imei,$asset){
        $db = Zend_Registry::get('db');

        /* tạm thời không check vì đã số là chưa có imei trên hệ thống
        //Neu san pham moi ve chua co tren he thong thì tạm thời bỏ qua
        $selectCheckNew = $db->select()
                ->from(array('p'=>'trainer_type_asset'),array('p.*'))
                ->where('p.status = ?',1)
                ->where('p.id = ?',$asset);

        $resultCheckNew = $db->fetchRow($selectCheckNew);
        if($resultCheckNew){
        */
            if(strlen(trim($imei)) != 15){
                return false;
            }
            return true;
        /*
        }
        */
        $select = $db->select()
            ->from(array('a'=>WAREHOUSE_DB.'.imei'),array('a.*'))
            ->join(array('c'=>'trainer_type_asset'),'a.good_id = c.good_id AND a.good_color = c.color_id',array())
            ->join(array('b'=>WAREHOUSE_DB.'.market'),'b.sn = a.sales_sn AND b.type = 4',array())
            ->where('c.id = ?',$asset)
            ->where('a.imei_sn = ?',$imei)
            ;
        $row = $db->fetchRow($select);
        if($row){
            return true;
        }else{
            return false;
        }
    }

    function checkData($list,$source = 0){
        $arrImei = array();
        if(count($list) == 0){
            return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'không được để trống'));
        }

        foreach($list as $item){
            if($source == TRAINER_ASSET_SOURCE_RETURN){
                if(!intval($item['trainer_id'])){
                    return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'Vui lòng chọn trainer'));
                }
            }

            if(!intval($item['asset_id'])){
                return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'Vui lòng nhập sản phẩm'));
            }

            if(!intval($item['quantity'])){
                return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'Số lượng không được để trông'));
            }

            if($item['type'] == CATEGORY_ASSET_TRAINER_PHONE){
                $arrImei += $item['imei'];
                if($item['quantity'] != count($item['imei'])){
                    return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'số lượng và số imei nhập vào không khớp'));
                }

                foreach($item['imei'] as $imei){
                    $result = $this->checkImei($imei,$item['asset_id']);
                    if(!$result){
                        return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'imei '.$imei.' không hợp lệ'));
                    }
                }
            }
        }

        //kiểm tra imei bị trung
        $arr_unique = array_unique($arrImei);
        $diff = array_diff_assoc($arrImei,$arr_unique);
        if(count($diff)){
            $imei = array_shift($diff);
            return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'imei '.$imei.' đã được nhập hơn 1 lần'));
        }
    }

    function  getOrderConfirmByTrainer($staff_id){
        $db = Zend_Registry::get('db');
        $cols = array(
            'p.id',
            'p.sn',
            'asset_name' => 'a.name',
            'object'     => 'p.object',
            'note'       => 'p.note',
            'order_from' => 'p.order_from',
            'confirm_at' => 'p.confirm_at'
        );
        $select = $db->select()
            ->from(array('p'=>'trainer_order'), $cols)
            ->joinLeft(array('a'=>'trainer_type_asset'), 'a.id = p.asset_id',array());

        $select->where('(p.del IS NULL OR p.del <> ?)', 1);
        $select->where('p.confirm_by IS NOT NULL', null);
        $select->where('p.order_to_confirm_at IS NULL', null);

        if(isset($staff_id) and $staff_id){
            $select->where('p.order_to = ?', $staff_id);
        }

        $select->order(array('p.confirm_at'));

        $result = $db->fetchAll($select);
        return $result;
    }


}