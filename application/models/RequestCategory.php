<?php
class Application_Model_RequestCategory extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_category';
    public function getCategory()
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'name'        => 'p.name'
        );
        $select->from(array('p'=> 'request_category'), $arrCols);
        $result = $db->fetchAll($select);
        return $result;
    }
}