<?php
class Application_Model_Repair extends Zend_Db_Table_Abstract
{
	protected $_name = 'repair';
    protected $_schema = DATABASE_TRADE;

    public function GetAll(){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT id, name FROM '. $this->_name
        );
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }

    public function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'store_name' => "s.name",
            'created_at' => "p.create_at",
            'created_by' => "p.staff_id",
            'status'    => "p.status",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%,
            'review_cost' => "GROUP_CONCAT(DISTINCT  d.review_cost)",
            'date_finish' => 'rc.created_at'
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_status'), 'a.status = p.status AND a.type = 4', array());
        if(!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 4', array());
        }
        $select->joinLeft(array('d' => DATABASE_TRADE.'.repair_details'), 'p.id = d.repair_id', array());
        $select->joinLeft(array('q' => DATABASE_TRADE.'.repair_quotation'), 'd.id = q.repair_details_id AND q.status = (SELECT MAX(status) FROM trade_marketing.repair_quotation WHERE repair_details_id = d.id AND status <> 1 AND status <> 3 AND status <> 5)', array());
        $select->joinLeft(['dis' => WAREHOUSE_DB.'.distributor'], 's.d_id = dis.id', []);

        $select->joinLeft(['rc' => DATABASE_TRADE.'.repair_confirm'], 'rc.repair_id = p.id AND rc.repair_status = 5', []);

        $select->where('p.remove = 0 AND p.reject = 0');


        if ($params['review_cost']) {
            $select->where('d.review_cost = ?', $params['review_cost']);
        }

        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if ($params['contractor_id']) {
            $select->where('d.contructors_id = ?', $params['contractor_id']);
        }

        if ($params['contract_number']) {
            $select->where('d.contract_number = ?', $params['contract_number']);
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        
        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }
        
        if(!empty($params['area_id_search'])){
            $select->where('r.area_id IN (?)', $params['area_id_search']);
        }
        
        if(!empty($params['status_id'])){
            $select->where('p.status IN (?)', $params['status_id']);
        }
        
        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        
        if(!empty($params['name_search'])){
            $select->where('s.name LIKE ?', "%".$params['name_search']."%");
        }
        
        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.create_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.create_at) <= ?', $to_date);
        }
        
        if(!empty($params['title'])){
    		$select->where('t.title = ?', $params['title']);
    	}

        if ($params['is_ka'] == 1) {
            $select->where('dis.is_ka = ?', 1);
        }

        if (isset($params['is_ka']) AND $params['is_ka'] == 0) {
            $select->where('dis.is_ka = ?', 0);
        }

        if ($params['is_brandshop']) {
            $select->where('dis.is_company_brandshop = ?', 1);
        }

        if ($params['month']) {
            $select->where('MONTH(p.create_at) = ?', $params['month']);
        }

//        if ($params['year']) {
//            $select->where('YEAR(p.create_at) = ?', $params['year']);
//        }

        $select->group('p.id');

        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);



        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    public function fetchPaginationReject($page, $limit, &$total, $params){

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'store_name' => "s.name",
            'created_at' => "p.create_at",
            'created_by' => "p.staff_id",
            'status'    => "p.status",
            'reject'    => "p.reject",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%,
            'review_cost' => "GROUP_CONCAT(DISTINCT  d.review_cost)",
            'p.reject_note',
            'date_finish' => 'rc.created_at'
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_status'), 'a.status = p.status AND a.type = 4', array());
        if(!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 4', array());
        }
        $select->joinLeft(array('d' => DATABASE_TRADE.'.repair_details'), 'p.id = d.repair_id', array());
        $select->joinLeft(array('q' => DATABASE_TRADE.'.repair_quotation'), 'd.id = q.repair_details_id AND q.status = (SELECT MAX(status) FROM trade_marketing.repair_quotation WHERE repair_details_id = d.id AND status <> 1 AND status <> 3 AND status <> 5)', array());
        $select->joinLeft(['dis' => WAREHOUSE_DB.'.distributor'], 's.d_id = dis.id', []);

        $select->joinLeft(['rc' => DATABASE_TRADE.'.repair_confirm'], 'rc.repair_id = p.id AND rc.repair_status = 5', []);

        $select->where('p.reject = 1 AND p.remove = 0');


        if ($params['review_cost']) {
            $select->where('d.review_cost = ?', $params['review_cost']);
        }

        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if ($params['contractor_id']) {
            $select->where('d.contructors_id = ?', $params['contractor_id']);
        }

        if ($params['contract_number']) {
            $select->where('d.contract_number = ?', $params['contract_number']);
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        if(!empty($params['area_id_search'])){
            $select->where('r.area_id IN (?)', $params['area_id_search']);
        }

        if(!empty($params['status_id'])){
            $select->where('p.status IN (?)', $params['status_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['name_search'])){
            $select->where('s.name LIKE ?', "%".$params['name_search']."%");
        }

        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.create_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.create_at) <= ?', $to_date);
        }

        if(!empty($params['title'])){
    		$select->where('t.title = ?', $params['title']);
    	}

        if ($params['is_ka'] == 1) {
            $select->where('dis.is_ka = ?', 1);
        }

        if (isset($params['is_ka']) AND $params['is_ka'] == 0) {
            $select->where('dis.is_ka = ?', 0);
        }

        if ($params['is_brandshop']) {
            $select->where('dis.is_company_brandshop = ?', 1);
        }

        if ($params['month']) {
            $select->where('MONTH(p.create_at) = ?', $params['month']);
        }

//        if ($params['year']) {
//            $select->where('YEAR(p.create_at) = ?', $params['year']);
//        }

        $select->group('p.id');

        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }
 
        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);


        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    public function fetchPaginationRemove($page, $limit, &$total, $params){

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'store_name' => "s.name",
            'created_at' => "p.create_at",
            'created_by' => "p.staff_id",
            'status'    => "p.status",
            'reject'    => "p.reject",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%,
            'review_cost' => "GROUP_CONCAT(DISTINCT  d.review_cost)",
            'p.reject_note',
            'date_finish' => 'rc.created_at',
            'p.remove'
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_status'), 'a.status = p.status AND a.type = 4', array());
        if(!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 4', array());
        }
        $select->joinLeft(array('d' => DATABASE_TRADE.'.repair_details'), 'p.id = d.repair_id', array());
        $select->joinLeft(array('q' => DATABASE_TRADE.'.repair_quotation'), 'd.id = q.repair_details_id AND q.status = (SELECT MAX(status) FROM trade_marketing.repair_quotation WHERE repair_details_id = d.id AND status <> 1 AND status <> 3 AND status <> 5)', array());
        $select->joinLeft(['dis' => WAREHOUSE_DB.'.distributor'], 's.d_id = dis.id', []);

        $select->joinLeft(['rc' => DATABASE_TRADE.'.repair_confirm'], 'rc.repair_id = p.id AND rc.repair_status = 5', []);

        $select->where('p.remove = 1');


        if ($params['review_cost']) {
            $select->where('d.review_cost = ?', $params['review_cost']);
        }

        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if ($params['contractor_id']) {
            $select->where('d.contructors_id = ?', $params['contractor_id']);
        }

        if ($params['contract_number']) {
            $select->where('d.contract_number = ?', $params['contract_number']);
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        if(!empty($params['area_id_search'])){
            $select->where('r.area_id IN (?)', $params['area_id_search']);
        }

        if(!empty($params['status_id'])){
            $select->where('p.status IN (?)', $params['status_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['name_search'])){
            $select->where('s.name LIKE ?', "%".$params['name_search']."%");
        }

        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.create_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.create_at) <= ?', $to_date);
        }

        if(!empty($params['title'])){
    		$select->where('t.title = ?', $params['title']);
    	}

        if ($params['is_ka'] == 1) {
            $select->where('dis.is_ka = ?', 1);
        }

        if (isset($params['is_ka']) AND $params['is_ka'] == 0) {
            $select->where('dis.is_ka = ?', 0);
        }

        if ($params['is_brandshop']) {
            $select->where('dis.is_company_brandshop = ?', 1);
        }

        if ($params['month']) {
            $select->where('MONTH(p.create_at) = ?', $params['month']);
        }

//        if ($params['year']) {
//            $select->where('YEAR(p.create_at) = ?', $params['year']);
//        }

        $select->group('p.id');

        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");


        return $result;
    }

    public function GetList($id){
    
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT R.id id,C.id category_id,C.name categoryname,R.quotation_id,S.id store_id,S.name storename,R.quantity quantity,R.imei imei,R.status status, R.note note,R.staff_id staff_id ,R.type type,R.contructors_id contructors, Q.price price , R.create_at ngaytao, R.update_at ngaycapnhat, AP.name status_name  , AST.title  staff_title  FROM '.DATABASE_TRADE.'.'.$this->_name 
            .' R left join '.DATABASE_TRADE.'.category C on C.id = R.category_id'
            . ' left join '.DATABASE_CENTER.'.store S on S.id =R.store_id '
            . ' left join '.DATABASE_TRADE.'.quotation Q on Q.id =R.quotation_id '
            .' left join '.DATABASE_TRADE.'.app_status AP on R.status = AP.id'
            .' left join '.DATABASE_TRADE.'.app_status_title AST on AST.status_id=AP.id'
            .' where R.id = '.$id
        );

        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    
    public function getRepair($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.store_id", 
            "p.create_at", 
            "p.staff_id", 
            "p.status", 
            "store_name" => "s.name", 
            "fullname" => "CONCAT(staff.firstname, ' ', staff.lastname)",
            "p.reject",
            "p.reject_note",
            "p.remove",
            "p.remove_at",
            "p.reject_at",
            "remove_by" => "CONCAT(st1.firstname, ' ', st1.lastname)",
            "reject_by" => "CONCAT(st2.firstname, ' ', st2.lastname)",
            "p.remove_note",
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->joinLeft(array('st1' => 'staff'), 'st1.id = p.remove_by', array());
        $select->joinLeft(array('st2' => 'staff'), 'st2.id = p.reject_by', array());
        $select->where('p.id = ?', $params['id']);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getStatistic($params = null)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'channel' => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                               WHEN (d.is_ka = 1) THEN 'KA'
                               WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THUỜNG'
                          END",
            'p.name',
            'dealer_id' => 'IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)',
            'd.partner_id',
            'store_id' => 'p.id',
            'repair_id' => 'rp.id',
            'status' => 'ast.name',
            'repair_details_id' => 'rd.id',
            'category' => 'c.name',
            'type' => 'GROUP_CONCAT(DISTINCT type.title)',
            'tp' => 'GROUP_CONCAT(DISTINCT tp.title)',
            'part' => 'GROUP_CONCAT(DISTINCT part.title)',
            'qp.quantity',
            'total_price' => 'IFNULL(quotation.total_price, IFNULL(rd.price_final, rd.price))',
            'area' => 'a.name',
            'created_at' => 'rp.create_at',
            'fullname'  => 'CONCAT(cr.firstname, " ", cr.lastname)',
            'province_name' => 'r.name'
        );

        // total price subquery
        $nestedSelectTotalPrice = $db->select();
        
        $arrCols_nest = [
            'repair_details_id',
            'total_price' => '(SUM(total_price) + SUM(total_price) * 0.1)'
        ];
        $nestedSelectTotalPrice->from(array('quo'=> DATABASE_TRADE.'.repair_quotation'), $arrCols_nest);
        $nestedSelectTotalPrice->where('status = (SELECT MAX(status)
                                                  FROM trade_marketing.repair_quotation  
                                                  WHERE status <> 3 AND status <> 1 AND repair_details_id = quo.repair_details_id)'
        );
        $nestedSelectTotalPrice->group('repair_details_id');

        // quantity part subquery
        $nestedSelectQuantityPart = $db->select();
        $arrCols_nest = [
            'repair_details_id',
            'quantity' => 'GROUP_CONCAT(quantity)'
        ];
        $nestedSelectQuantityPart->from(array('dp'=> DATABASE_TRADE.'.repair_details_part'), $arrCols_nest);
        $nestedSelectQuantityPart->where('status = 1');
        $nestedSelectQuantityPart->group('repair_details_id');

        $select->from(array('rp' => DATABASE_TRADE . '.repair'), $arrCols);
        $select->joinLeft(array('cr' => 'staff'), 'cr.id = rp.staff_id', array());
        $select->join(array('ast' => DATABASE_TRADE . '.app_status'), 'rp.status = ast.status AND ast.type = 12', array());
        $select->join(array('rd' => DATABASE_TRADE . '.repair_details'), 'rp.id = rd.repair_id', array());
        $select->join(array('c' => DATABASE_TRADE . '.category'), 'rd.category_id = c.id', array());

        $select->joinLeft(array('rdt' => DATABASE_TRADE . '.repair_details_type'), 'rd.id = rdt.repair_details_id AND rdt.del = 0', array());
        $select->joinLeft(array('type' => DATABASE_TRADE . '.repair_type'), 'rdt.repair_type_id = type.id AND type.status = 1', array());
        $select->joinLeft(array('tp' => DATABASE_TRADE . '.repair_tp'), 'rdt.repair_tp_id = tp.id', array());
        $select->joinLeft(array('rdp' => DATABASE_TRADE . '.repair_details_part'), 'rd.id = rdp.repair_details_id AND rdp.status = 1', array());
        $select->joinLeft(array('part' => DATABASE_TRADE . '.repair_part'), 'rdp.repair_part_id = part.id AND part.status = 1', array());

        $select->join(array('p' => 'store'), 'p.id = rp.store_id', array());
        $select->join(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->join(array('a' => 'area'), 'a.id = r.area_id', array());

        $select->joinLeft(array('l' => 'store_staff_log'), 'l.released_at IS NULL AND l.store_id = p.id  AND l.is_leader = 1', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id', array());

        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('loyalty' =>  'dealer_loyalty'), 'loyalty.dealer_id = ( IF ( ( d.parent = 0 OR d.parent IS NULL ), d.id, d.parent ) ) AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' =>  'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array());


        $select->joinLeft(array('quotation' => new Zend_Db_Expr('(' . $nestedSelectTotalPrice . ')')), 'rd.id = quotation.repair_details_id',array());
        $select->joinLeft(array('qp' => new Zend_Db_Expr('(' . $nestedSelectQuantityPart . ')')), 'rd.id = qp.repair_details_id',array());

        $select->where('rp.remove = 0');
//        $select->where('p.del IS NULL OR p.del = 0');
        $select->group(['rp.store_id', 'rd.id']);

        if(!empty($params['staff_id'])){
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if(!empty($params['area_id_search'])){
            $select->where('r.area_id IN (?)', $params['area_id_search']);
        }

        if(!empty($params['from_date'])){
            $select->where('rp.create_at >= (?)', date('Y-m-d', strtotime($params['from_date'])));
        }

        if(!empty($params['to_date'])){
            $select->where('rp.create_at <= (?)', date('Y-m-d', strtotime($params['to_date'])));
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function export($statistics)
    {
        include 'PHPExcel.php';
        $QRepair = new Application_Model_Repair();
//        $statistics = $QRepair->getStatistic();

        $objExcel = new PHPExcel();
        $objExcel->setActiveSheetIndex(0);

        $sheet = $objExcel->getActiveSheet()->setTitle('Repair');

        $rowCount = 2;
        $index = 1;
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'KÊNH');
        $sheet->setCellValue('C1', 'ID SHOP');
        $sheet->setCellValue('D1', 'DEALER ID');
        $sheet->setCellValue('E1', 'PARTNER ID');
        $sheet->setCellValue('F1', 'TÊN SHOP');
        $sheet->setCellValue('G1', 'KHU VỰC');
        $sheet->setCellValue('H1', 'TỈNH');
        $sheet->setCellValue('I1', 'TÌNH TRẠNG');
        $sheet->setCellValue('J1', 'HẠNG MỤC SỬA CHỮA');
        $sheet->setCellValue('K1', 'LOẠI HƯ HỎNG');
        $sheet->setCellValue('L1', 'LOẠI SỬA CHỮA');
        $sheet->setCellValue('M1', 'LINH KIỆN');
        $sheet->setCellValue('N1', 'SỐ LƯỢNG');
        $sheet->setCellValue('O1', 'CHI PHÍ');
        $sheet->setCellValue('P1', 'NGÀY TẠO');
        $sheet->setCellValue('Q1', 'NGƯỜI TẠO');

        foreach ($statistics as $statistic) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $statistic['channel']);
            $sheet->setCellValue('C' . $rowCount, $statistic['store_id']);
            $sheet->setCellValue('D' . $rowCount, $statistic['dealer_id']);
            $sheet->setCellValue('E' . $rowCount, $statistic['partner_id']);
            $sheet->setCellValue('F' . $rowCount, $statistic['name']);
            $sheet->setCellValue('G' . $rowCount, $statistic['area'] );
            $sheet->setCellValue('H' . $rowCount, $statistic['province_name'] );
            $sheet->setCellValue('I' . $rowCount, $statistic['status'] );
            $sheet->setCellValue('J' . $rowCount, $statistic['category'] );
            $sheet->setCellValue('K' . $rowCount, $statistic['type'] );
            $sheet->setCellValue('L' . $rowCount, $statistic['tp'] );
            $sheet->setCellValue('M' . $rowCount, $statistic['part'] );
            $sheet->setCellValue('N' . $rowCount, $statistic['quantity'] );
            $sheet->setCellValue('O' . $rowCount, $statistic['total_price'] );
            $sheet->setCellValue('P' . $rowCount, $statistic['created_at'] );
            $sheet->setCellValue('Q' . $rowCount, $statistic['fullname'] );
            

            ++$index;
            ++$rowCount;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:Q' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'Q' ; ++$i) {
            $sheet->getStyle($i. 1)->getFont()->setBold(true);
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'repair';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
    }

    public function getCost($params)
    {
        $date = "2019-04-01"; // không tính chi phí quí 1 năm 2019

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['r' => DATABASE_TRADE . '.repair'], [
                're.area_id',
                'cost_ia_not_brandshop' => "IF (d.is_ka = 0 AND d.is_company_brandshop = 0, SUM(total_price) + SUM(total_price) * 0.1, 0)",
                'cost_ia_brandshop' => "IF (d.is_ka = 0 AND d.is_company_brandshop = 1, SUM(total_price) + SUM(total_price) * 0.1, 0)",
                'cost_ka_not_brandshop' => "IF (d.is_ka = 1 AND d.is_company_brandshop = 0, SUM(total_price) + SUM(total_price) * 0.1, 0)",
                'cost_ka_brandshop' => "IF (d.is_ka = 1 AND d.is_company_brandshop = 1, SUM(total_price) + SUM(total_price) * 0.1, 0)"
            ])
            ->join(['s' => 'store'], 'r.store_id = s.id', [])
            ->joinLeft(['re' => 'regional_market'], 's.regional_market = re.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB.'.distributor'], 's.d_id = d.id', [])
            ->joinLeft(['detail' => DATABASE_TRADE.'.repair_details'], 'r.id = detail.repair_id', [])
            ->joinLeft(['q' => DATABASE_TRADE.'.repair_quotation'], 'detail.id = q.repair_details_id AND q.status = (
                                select MAX(status) from trade_marketing.repair_quotation where repair_details_id = detail.id AND status <> 3 AND status <> 1
                                )', [])
            ->where('r.remove = 0 OR r.remove IS NULL')
            ->where('r.create_at >= ? ', $date)
//            ->where('q.id IS NOT NULL')
            ->group(['re.area_id', 'd.is_ka', 'd.is_company_brandshop']);

        if ($params['status_finish']) {
            $select->where('r.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('re.area_id IN (?)', $params['area_list']);
        }

        if ($params['month']) {
            $select->where('MONTH(r.create_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(r.create_at) = ?', $params['year']);
        }

        if ($params['month_in_season']) {
            $select->where('MONTH(r.create_at) IN (?)', $params['month_in_season']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') <= ?", $params['to_date']);
        }
        
        $result = $db->fetchAll($select);

//        echo $select->__toString();die;

        // 3: sữa chữa IA, 4: sữa chữa KA, 5:sữa chữa branshop
        foreach ($result as $element) {
            $repairCost [$element['area_id']] ['4'] += $element ['cost_ka_not_brandshop'] + $element ['cost_ka_brandshop'];
            $repairCost [$element['area_id']] ['5']  += $element ['cost_ka_brandshop'] + $element ['cost_ia_brandshop'];
            $repairCost [$element['area_id']] ['3']  += $element ['cost_ia_not_brandshop'] + $element ['cost_ia_brandshop'];
        }
        return $repairCost;

    }

    public function getContractNumber($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['r' => DATABASE_TRADE.'.repair'], [
                         'repair_id' => 'r.id',
                         'd.contract_number'
                     ])
                    ->joinLeft(['d' => DATABASE_TRADE.'.repair_details'], 'r.id = d.repair_id', [])
                    ->where('r.remove IS NULL OR r.remove = 0')
                    ->where('d.contract_number IS NOT NULL');
                    
        $result = $db->fetchALl($select);

        foreach ($result as $element) {
            $list [$element['repair_id']] [] = $element['contract_number']; 
        }

        return $list;
    }


    public function getStatisticReviewCost($params = null)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'channel' => "CASE 
                            WHEN (loyalty.loyalty_plan_id IS NOT NULL AND distributor.is_ka = 0) THEN plan.name
                            WHEN (loyalty.loyalty_plan_id IS NULL AND distributor.is_ka = 0) THEN 'Shop Thường'
                            WHEN (distributor.is_ka = 1) THEN 'KA'
                          END",
            'repair.store_id',
            'store_name' => 's.name',
            'dealer_id' => 'IF(distributor.parent IS NULL OR distributor.parent = 0, distributor.id, distributor.parent)',
            'distributor.partner_id',
            'area' => 'a.name',
            'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
            'created_at' =>'repair.create_at',
            'status' => 'status.name',
            'review_cost' => "IF(detail.review_cost = 1, 'Đúng', IF (detail.review_cost = 2, 'Sai', ''))"

        );

        $select->from(['repair' => DATABASE_TRADE.'.repair'], $arrCols);
        $select->joinLeft(array('detail' => DATABASE_TRADE.'.repair_details'), 'repair.id = detail.repair_id', array());
        $select->joinLeft(['s' => 'store'], 'repair.store_id = s.id', []);
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'r.area_id = a.id', array());
        $select->joinLeft(array('distributor' => WAREHOUSE_DB.'.distributor'), 's.d_id = distributor.id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = IF (distributor.parent IS NULL OR distributor.parent = 0, distributor.id, distributor.parent)  AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'loyalty.loyalty_plan_id = plan.id', array());

        $select->joinLeft(array('st' => 'staff'), 'repair.staff_id = st.id', array());
        $select->joinLeft(array('status' => DATABASE_TRADE.'.app_status'), 'repair.status = status.status AND status.type = 4', array());

        $select->where('s.del IS NULL OR s.del = 0');
        $select->where('repair.remove IS NULL OR repair.remove = 0');
        $select->order('repair.create_at DESC');
        $select->group('repair.id');
        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if (!empty($params['review_cost'])) {
            $select->where('detail.review_cost = ?', $params['review_cost']);
        }

        if (!empty($params['from_date'])) {
            $select->where('repair.create_at >= ?', date('Y-m-d 00:00:00', strtotime($params['from_date'])));
        }

        if (!empty($params['to_date'])) {
            $select->where('repair.create_at <= ?', date('Y-m-d 23:59:59', strtotime($params['to_date'])));
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function exportReviewCost($data)
    {
        ini_set("memory_limit", -1);
        ini_set("display_error", 1);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'KÊNH',
            'ID SHOP',
            'DEALER ID',
            'PARTNER ID',
            'TÊN SHOP',
            'KHU VỰC',
            'TÌNH TRẠNG',
            'NGÀY TẠO',
            'NGƯỜI TẠO',
            'TRẠNG THÁI CHI PHÍ'

        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){


            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['status']);
            $sheet->setCellValue($alpha++.$index, date('d-m-Y H:i:s', strtotime($item['created_at'])));
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['review_cost']);

            $index++;

        }



        $filename = 'Sửa chữa tổng quát' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
}