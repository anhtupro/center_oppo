<?php
class Application_Model_StaffExperience extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_experience';
    
    public function getListExperience($staff_id){
        $db   = Zend_Registry::get('db');
        $select       = $db->select()
            ->from(array('ste'=>$this->_name),array('ste.company_name','ste.job_position','ste.from_date','ste.to_date','ste.reason_for_leaving'))
            ->where('staff_id = ?',$staff_id);
        $list_experience = $db->fetchAll($select);
        return $list_experience;
    }

}