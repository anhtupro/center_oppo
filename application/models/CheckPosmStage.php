<?php

class Application_Model_CheckPosmStage extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_posm_stage';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get("db");
        // sub query
        $select = $db->select();
        //DISTINCT
        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.*'
        );

        $select->from(array('p' => DATABASE_TRADE.'.check_posm_stage'), $arrCols);

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getStageValid($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => DATABASE_TRADE.'.check_posm_stage'], [
                       's.*'  
                     ])
            ->join(['a' => DATABASE_TRADE.'.check_posm_assign_store'], 'a.store_id = ' . $store_id . ' AND s.id = a.stage_id AND a.del = 0' , [])
            ->where("CURDATE() BETWEEN s.from_date AND s.to_date")
            ->group('s.id');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategory($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE.'.check_posm_stage'], [
                'd.*',
                'category_name' => 'c.name'
            ])
            ->join(['d' => DATABASE_TRADE.'.check_posm_assign_category'], 'd.store_id = ' . $store_id . ' AND d.quantity > 0 AND s.id = d.stage_id' , [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
            ->where("CURDATE() BETWEEN s.from_date AND s.to_date");

        $result = $db->fetchAll($select);
        foreach ($result as $element) {
            $list [$element['stage_id']] [] = $element;
        }

        return $list;
    }

    public function getStatisticCategory($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                         'area_id' => 'e.id',
                         'area_name' => 'e.name',
                         'category_id' => 'c.id',
                         'category_name' => 'c.name',
                         'total_quantity' => "SUM(a.quantity)",
                         'total_real_quantity' => "SUM(a.real_quantity)"
                     ])
        ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
        ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
        ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
        ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
        ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.category_id = c.id', [])

        ->where('a.stage_id = ?', $params['stage_id'])
         ->where('e.id IN (?)', $params['area_list'])
        ->group(['e.id','a.category_id']);
        
        if ($params['area_id']) {
            $select->where('e.id = ?', $params['area_id']);
        }
  
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] [$element['category_id']] = $element;
        }

        return $list;
    }

    public function getStatisticCategoryAll($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                         'category_id' => 'c.id',
                         'category_name' => 'c.name',
                         'total_quantity' => "SUM(a.quantity)",
                         'total_real_quantity' => "SUM(a.real_quantity)"
                     ])
        ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
        ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
        ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
        ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
        ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.category_id = c.id', [])

        ->where('a.stage_id = ?', $params['stage_id'])
         ->where('e.id IN (?)', $params['area_list'])
        ->group(['a.category_id']);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['category_id']] = $element;
        }

        return $list;
    }

    public function getAllStoreEachArea($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.check_posm_assign_store'], [
                         'r.area_id',
                         'area_name' => 'e.name',
                         'count_store' => "count(a.store_id)",
                         'list_store' => "GROUP_CONCAT(a.store_id)"
                     ])
                    ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
                    ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
                    ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
                    ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])

                    ->where('a.stage_id = ?', $params['stage_id'])
                    ->where('r.area_id IN (?)', $params['area_list'])
                    ->where('a.del = ? ', 0)
                    ->group('r.area_id');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] = $element;
        }

        return $list;
    }


    public function getNotFinishStoreEachArea($params)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                'store_id' => new Zend_Db_Expr('DISTINCT a.store_id'),
                'area_id' => 'r.area_id',
                'area_name' => 'e.name',
                'count_store' => "count(a.store_id)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.real_quantity < a.quantity')
            ->where('r.area_id IN (?)', $params['area_list'])
            ->group('a.store_id');

        $select = $db->select()
                     ->from(['g' => $sub_select], [
                         'g.area_id',
                         'g.area_name',
                         'count_store' => 'count(g.area_id)',
                         'list_store' => "GROUP_CONCAT(g.store_id)"
                     ])
                    ->group('g.area_id');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] = $element;
        }

        return $list;
    }


    public function getAllStore($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_store'], [
                'count_store' => "count(a.store_id)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('r.area_id IN (?)', $params['area_list'])
            ->where('a.del = ? ', 0);


        $result = $db->fetchOne($select);

        return $result;
    }

    public function getAllFinishStore($params)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                'area_id' => 'r.area_id',
                'count_store' => "count(a.store_id)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.real_quantity < a.quantity')
            ->where('r.area_id IN (?)', $params['area_list'])
            ->group('a.store_id');

        $select = $db->select()
            ->from(['g' => $sub_select], [
                'count_store' => 'count(g.area_id)'
            ]);

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getDataExportDetail($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.check_posm_assign_category'], [
                'area_id' => 'e.id',
                'area_name' => 'e.name',
                'province_name' => 'r.name',
                'district_name' => 'd.name',
                'store_id' => 's.id',
                's.shipping_address',
                'store_name' => 's.name',
                'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)",
                'category_id' => 'c.id',
                'category_name' => 'c.name',
                'a.quantity',
                'a.real_quantity',
                'a.note',
                'channel' => "IF(di.is_ka = 1, IF(dp.id IS NOT NULL, dp.title, di.title),
                                 IFNULL(lp.name, 'Không thuộc DIAMOND CLUB')
                                )"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])

            ->joinLeft(['di' => WAREHOUSE_DB . '.distributor'], 's.d_id = di.id', [])
            ->joinLeft(['dp' => WAREHOUSE_DB . '.distributor'], 'di.parent = dp.id', [])
            ->joinLeft(['lo' => 'dealer_loyalty'], 'lo.dealer_id = IF(dp.id IS NOT NULL, dp.id, di.id) AND lo.is_last = 1', [])
            ->joinLeft(['lp' => 'loyalty_plan'], 'lp.id = lo.loyalty_plan_id', [])

            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['r' => 'regional_market'], 'd.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['c' => DATABASE_TRADE . '.category'], 'a.category_id = c.id', [])
            ->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', [])
//            ->joinLeft(['l' => 'v_store_staff_leader_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['f' => 'staff'], 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = f.id', [])
            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('e.id IN (?)', $params['area_list']);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['store_id']] ['store_info'] = $element;
            $list [$element['store_id']] ['detail'] [$element['category_id']] ['quantity'] = $element['quantity'];
            $list [$element['store_id']] ['detail'] [$element['category_id']] ['real_quantity'] = $element['real_quantity'];
            $list [$element['store_id']] ['detail'] [$element['category_id']] ['note'] = $element['note'];
        }

        return $list;
    }
    public function exportDetail($params)
    {
        require_once 'PHPExcel.php';

        $QCategory = new Application_Model_Category();
        $PHPExcel = new PHPExcel();
        $QCheckPosmDetail = new Application_Model_CheckPosmDetail();

        $list_category = $QCheckPosmDetail->getListCategory($params['stage_id']);

        $data = $this->getDataExportDetail($params);

        $heads = array(
            'Store ID',
            'Tên shop',
            'Địa chị',
            'Kênh',
            'Khu vực',
            'Tỉnh',
            'Quận huyện',
            'Sale quản lý'
        );

        foreach ($list_category as $category) {
            $heads [] = $category['name'] . '(Đăng ký)';
            $heads [] = $category['name'] . '(Đã triển khai)';
            $heads [] = 'Ghi chú';
        }

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $store_info = $item['store_info'];
            $detail = $item['detail'];
            $alpha    = 'A';
            
            $sheet->setCellValue($alpha++.$index, $store_info['store_id']);
            $sheet->setCellValue($alpha++.$index, $store_info['store_name']);
            $sheet->setCellValue($alpha++.$index, $store_info['shipping_address']);
            $sheet->setCellValue($alpha++.$index, $store_info['channel']);
            $sheet->setCellValue($alpha++.$index, $store_info['area_name']);
            $sheet->setCellValue($alpha++.$index, $store_info['province_name']);
            $sheet->setCellValue($alpha++.$index, $store_info['district_name']);
            $sheet->setCellValue($alpha++.$index, $store_info['sale_name']);

            foreach ($list_category as $category) {
                $quantity = $detail[$category['id']] ['quantity'];
                $real_quantity = $detail[$category['id']] ['real_quantity'];
                $note = $detail[$category['id']] ['note'];
                $sheet->setCellValue($alpha++.$index, $quantity);

                // set color
                if ($quantity > 0) {
                    if ($real_quantity >= $quantity) {
                        $color = 'c4f5b0';
                    } else {
                        $color = 'f7aa9e';
                    }
                    $sheet->getStyle($alpha . $index)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => $color)
                            )
                        )
                    );
                }

                // end set color
                $sheet->setCellValue($alpha++.$index,$real_quantity);
                $sheet->setCellValue($alpha++.$index,$note);


            }

            $index++;
        }

        $filename = 'Kiem tra POSM' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function getListStoreInvalid($stage_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.check_posm_assign_store'], [
                         'a.store_id'
                     ])
                    ->joinLeft(['s' => 'store'], 'a.store_id = s.id AND (s.del IS NULL OR s.del = 0)', [])
            
        ->where('a.stage_id = ?', $stage_id)
        ->where('s.id IS NULL');
        
        $result = $db->fetchCol($select);

        return $result;
    }

    public function getStatisticCategorySale($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                'area_id' => 'e.id',
                'area_name' => 'e.name',
                'category_id' => 'c.id',
                'category_name' => 'c.name',
                'total_quantity' => "SUM(a.quantity)",
                'total_real_quantity' => "SUM(a.real_quantity)",
                'percent_finish' => "SUM(a.real_quantity) * 100 / SUM(a.quantity)",
                'sale_id' => 'f.id',
                'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.category_id = c.id', [])
            ->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', [])
//            ->joinLeft(['l' => 'v_store_staff_leader_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['f' => 'staff'], 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = f.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('e.id = ?', $params['area_id'])
//            ->where('f.id is not null')
            ->group(['f.id','a.category_id']);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['sale_id']] ['sale_name']  = $element['sale_name'];
            $list [$element['sale_id']] ['detail'] [$element['category_id']]  = $element;
        }

        return $list;
    }

    public function getAllStoreSale($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_store'], [
                'r.area_id',
                'area_name' => 'e.name',
                'count_store' => "count(a.store_id)",
                'sale_id' => 'f.id',
                'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', [])
//            ->joinLeft(['l' => 'v_store_staff_leader_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['f' => 'staff'], 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = f.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('e.id = ?', $params['area_id'])
            ->where('a.del = ? ', 0)

            ->group('f.id');

        $result = $db->fetchAll($select);


        foreach ($result as $element) {
            $list [$element['sale_id']] = $element;
        }

        return $list;
    }
    
    public function getNotFinishStoreSale($params)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                'area_id' => 'r.area_id',
                'area_name' => 'e.name',
                'count_store' => "count(a.store_id)",
                'sale_id' => 'f.id',
                'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', [])
//            ->joinLeft(['l' => 'v_store_staff_leader_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['f' => 'staff'], 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = f.id', [])
            
            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.real_quantity < a.quantity')
            ->where('e.id = ?', $params['area_id'])
            ->group('a.store_id');

        $select = $db->select()
            ->from(['g' => $sub_select], [
                'g.area_id',
                'g.area_name',
                'count_store' => 'count(g.area_id)',
                'sale_id' => 'g.sale_id',
                'sale_name' => 'g.sale_name'
            ])
            ->group('g.sale_id');

        $result = $db->fetchAll($select);
 
        foreach ($result as $element) {
            $list [$element['sale_id']] = $element;
        }

        return $list;
    }


    public function getHasDistributionStoreEachArea($params)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                'area_id' => 'r.area_id',
                'area_name' => 'e.name',
                'count_store' => "count(a.store_id)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.real_quantity > 0')
            ->where('r.area_id IN (?)', $params['area_list'])
            ->group('a.store_id');

        $select = $db->select()
            ->from(['g' => $sub_select], [
                'g.area_id',
                'g.area_name',
                'count_store' => 'count(g.area_id)'
            ])
            ->group('g.area_id');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] = $element;
        }


        return $list;
    }

    public function getDataStoreFinish($params)
    {
        $db = Zend_Registry::get("db");

        $select_store_not_finish = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
                'a.store_id'
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])


            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('e.id IN (?)', $params['area_list'])
            ->where('a.real_quantity < a.quantity')
            ->group(['a.store_id']);


        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_posm_assign_category'], [
              'a.store_id',
                'store_name' => 's.name',
                'area' => 'e.name'
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'r.id = di.parent', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['n' => $select_store_not_finish], 'a.store_id = n.store_id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('e.id IN (?)', $params['area_list'])
            ->where('n.store_id IS NULL')
            ->group(['a.store_id']);

        if ($params['area_id']) {
            $select->where('e.id = ?', $params['area_id']);
            $select_store_not_finish->where('e.id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportStoreFinish($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getDataStoreFinish($params);

        $heads = array(
            'Store ID',
            'Store name',
            'Area'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }


        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $index++;

        }

        $filename = 'Store đã hoàn thành' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }


}