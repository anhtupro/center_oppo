<?php

class Application_Model_CheckIn extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_in';

    public function get_check_in_list($params)
    {
        $db = Zend_Registry::get('db');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $userCode = null;

        $stmt = $db->prepare("CALL `get_check_in_list`(:name, :code, :from_date, :to_date, :limit, :offset, :user_code)");

        $stmt->bindParam("name", $params['name'], PDO::PARAM_STR);
        $stmt->bindParam("code", $params['code'], PDO::PARAM_STR);
        $stmt->bindParam("from_date", $params['from_date'], PDO::PARAM_STR);
        $stmt->bindParam("to_date", $params[ 'to_date'], PDO::PARAM_STR);
        
        if($userStorage->group_id == HR_ID)
        {
            $stmt->bindParam("user_code", $userCode, PDO::PARAM_STR);
        }
        else
        {
            $stmt->bindParam("user_code", $userStorage->code, PDO::PARAM_STR);
        }

        $stmt->bindParam("limit", $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset", $params['offset'], PDO::PARAM_INT);

        $stmt->execute();

        $data = array();

        $data['data'] = $stmt->fetchAll();

        $stmt->closeCursor();

        $data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
        $db = $stmt = null;
        
        return $data; 
        
    }

    public function getListPermission($staff_code)
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT *
                FROM staff_permission
                WHERE staff_code = :code
                AND (is_leader > 0 OR is_manager > 0)";
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam('code', $staff_code, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();

        return $data;
    }

    public function getListCodePermission($code)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare(' select *
                                from staff_permission
                                where (is_leader > 0 or is_manager > 0) 
                                        and staff_code = :code');

        $stmt->bindParam('code', $code, PDO::PARAM_STR);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function xuat_luoi($params = array())
    {
        $db = Zend_Registry::get('db');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if(in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID)))
        {
            $where_sql = "";
        }
        else
        {
            $data_permission = $this->getListPermission($userStorage->code);
            if(!empty($data_permission))
            {
                $array_where = array();
                foreach ($data_permission as $key => $value) 
                {
                    $array_where[] = "( st.department = " . $value['department_id'] . " AND st.office_id = " . $value['office_id'] . " )";
                }
                $where_sql = " AND (" . implode(" OR ", $array_where) . ") ";
            }
            else
            {
                $where_sql = " AND st.id = " . $userStorage->id;
            }
        }

        $sql = "SELECT
                    st.code as `code`,
                    CONCAT(st.firstname, ' ', st.lastname) AS `staff_name`,
                    t.name AS `department`,
                    ad.date as `check_in_day`,
                    cid.check_in_at as `check_in_at`,
                    cid.check_out_at as `check_out_at`,
                    cid.check_in_late_time as `late`,
                    cid.check_out_soon_time as `soon`,
                    IF(cgm.company_group = 6 AND ad.is_off = 0,
                        1,
                        IF( 
                            (TIME(cid.check_in_at) BETWEEN cis.begin_break_time AND cis.end_break_time) OR (TIME(cid.check_out_at) BETWEEN cis.begin_break_time AND cis.end_break_time), 0.5,
                            TIME_TO_SEC(TIMEDIFF( IF(TIME(cid.check_out_at)>cis.end OR DATE(cid.check_out_at) > cid.check_in_day ,cis.end, TIME(cid.check_out_at)), IF(TIME(cid.check_in_at)<cis.begin,cis.begin, TIME(cid.check_in_at)) )) / TIME_TO_SEC(TIMEDIFF(cis.end, cis.begin))
                        )
                    ) as `cong`,
                    TIME_TO_SEC(cid.realtime)/3600 as `total`,
                    cid.overtime as `overtime`,
                    (TIME_TO_SEC(TIMEDIFF(cis.end, cis.begin)) - TIME_TO_SEC(TIMEDIFF(cis.end_break_time, cis.begin_break_time)))/3600 as `time_per_day`,
                    ar.name as `area_name`
                FROM `check_in_machine_map` cimm
                JOIN `staff` AS st ON st.code = cimm.staff_code
                LEFT JOIN `company_group_map` AS cgm ON cgm.title = st.title 
                LEFT JOIN `regional_market` rm ON st.regional_market = rm.id
                LEFT JOIN `area` ar ON rm.area_id = ar.id
                CROSS JOIN (SELECT * FROM  `all_date` WHERE date BETWEEN :from_date AND :to_date) as ad
                LEFT JOIN `check_in_detail` cid ON cid.staff_code = st.code AND cid.check_in_day = ad.date
                LEFT JOIN `check_in_shift` cis 
                    ON (cis.staff_code = st.code AND cis.date IS NULL) 
                        OR (cis.staff_code = st.code AND cis.date = ad.date)
                LEFT JOIN `check_in_shift` cis2 ON ad.date = cis2.date AND cis2.staff_code = st.code
                JOIN `team` t ON st.department = t.id
                WHERE st.status = 1 AND (cis2.id IS NULL OR cis2.id = cis.id)
                " . $where_sql . "
                GROUP BY st.id, ad.date
                ORDER BY st.department, st.lastname, ad.date";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to_date'], PDO::PARAM_STR);

        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function insertMachineStaff($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "INSERT INTO `check_in_machine_map` (`enroll_number`, `id_machine`, `ip_machine`, `staff_code`, `status`)
                    VALUES (:enroll_number, 0, :ip_machine, :staff_code, 1)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('enroll_number', $params['enroll_number'], PDO::PARAM_INT);
        $stmt->bindParam('ip_machine', $params['ip'], PDO::PARAM_STR);
        $stmt->bindParam('staff_code', $params['staff_code'], PDO::PARAM_STR);

        $stmt->execute();
        $stmt->closeCursor();
        
        $stmt = $db = null;
    }

    public function getStaffEnrollNumber($params = array())
    {
        $db = Zend_Registry::get('db');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if(in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID)))
        {
            $where_sql = "";
        }
        else
        {
            $data_permission = $this->getListPermission($userStorage->code);
            if(!empty($data_permission))
            {
                $array_where = array();
                foreach ($data_permission as $key => $value) 
                {
                    $array_where[] = "( st.department = " . $value['department_id'] . " AND st.office_id = " . $value['office_id'] . " )";
                }
                $where_sql = " AND (" . implode(" OR ", $array_where) . ") ";
            }
            else
            {
                $where_sql = " AND st.id = " . $userStorage->id;
            }
        }

        $sql = "
                SELECT 
                    SQL_CALC_FOUND_ROWS st.id,
                    CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                    st.code as `code`,
                    of.office_name as `office_name`,
                    of.ip_machine as `ip_machine`,
                    cimm.enroll_number as `enroll_number`
                FROM `staff` st
                JOIN `office` of ON st.office_id = of.id
                LEFT JOIN `check_in_machine_map` cimm ON cimm.staff_code = st.code AND of.ip_machine AND  cimm.status = 1
                WHERE st.status = 1 " . $where_sql;

        $sql .= " LIMIT " . $params['limit'] ." OFFSET " . $params['offset'];

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data['data'] = $stmt->fetchAll();
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function offAllMachineConfig($code)
    {
        $db = Zend_Registry::get('db');
        $sql = "UPDATE `check_in_machine_map`
                SET `status` = 0
                WHERE `staff_code` = :code";
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam('code', $code, PDO::PARAM_STR);
        $stmt->execute();

        $stmt->closeCursor();
        $stmt = $db = null;
    }

    public function getDateInfo($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT *
                FROM `all_date`
                WHERE `date` BETWEEN :from_date AND :to_date";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to'], PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function fetchLate($params = array())
    {
         $db = Zend_Registry::get('db');

         $sql = "CALL `PR_staff_late_check_in` (:from, :to, NULL, NULL)";
         $stmt = $db->prepare($sql);
         $stmt->bindParam('from', $params['from'], PDO::PARAM_STR);
         $stmt->bindParam('to', $params['to'], PDO::PARAM_STR);

         $stmt->execute();
         $data = $stmt->fetchAll();

         $stmt->closeCursor();
         $stmt = $db = null;

         return $data;
    }

    public function listLate($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "CALL `get_late_list_final`(:from_date, :to_date, :pDepartment, :pTeam, :pTitle, :pName, :pCode, :pArea)";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to_date'], PDO::PARAM_STR);
        $stmt->bindParam('pDepartment', $params['department'], PDO::PARAM_STR);
        $stmt->bindParam('pTeam', $params['team'], PDO::PARAM_STR);
        $stmt->bindParam('pTitle', $params['title'], PDO::PARAM_STR);
        $stmt->bindParam('pName', $params['name'], PDO::PARAM_STR);
        $stmt->bindParam('pCode', $params['code'], PDO::PARAM_STR);
        $stmt->bindParam('pArea', $params['area'], PDO::PARAM_STR);
        
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function getByDepartment($params = array())
    {
        $db = Zend_Registry::get('db');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if(in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID)))
        {
            $where_sql = "";
        }
        else
        {
            $data_permission = $this->getListPermission($userStorage->code);
            if(!empty($data_permission))
            {
                $array_where = array();
                foreach ($data_permission as $key => $value) 
                {
                    $array_where[] = "( st.department = " . $value['department_id'] . " AND st.office_id = " . $value['office_id'] . " )";
                }
                $where_sql = " AND (" . implode(" OR ", $array_where) . ") ";
            }
            else
            {
                $where_sql = " AND st.id = " . $userStorage->id;
            }
        }

        $sql = "SELECT
                    st.code as `code`,
                    CONCAT(st.firstname, ' ', st.lastname) AS `staff_name`,
                    t.name AS `title`,
                    st.department AS `department_id`,
                    t2.name AS `department_name`,
                    ad.date as `check_in_day`,
                    IF( (cgm.company_group = 6 AND ad.is_off = 0) OR (ti.status = 1 AND ti.shift = 0 AND ti.off <> 1),
                        1,
                        IF( 
                            (TIME(cid.check_in_at) BETWEEN cis.begin_break_time AND cis.end_break_time) 
                                OR (TIME(cid.check_out_at) BETWEEN cis.begin_break_time AND cis.end_break_time), 0.5,
                            TIME_TO_SEC(TIMEDIFF( IF(TIME(cid.check_out_at)>cis.end OR DATE(cid.check_out_at) > cid.check_in_day  ,cis.end,
                                IF(TIME(cid.check_out_at)<cis.begin ,cis.begin , TIME(cid.check_out_at) )), 
                                IF(TIME(cid.check_in_at)<cis.begin,cis.begin, IF( TIME(cid.check_in_at)>cis.end, cis.end, TIME(cid.check_in_at)) ) )) / TIME_TO_SEC(TIMEDIFF(cis.end, cis.begin))
                        )
                    ) as `cong`,
                    ti.approve_time as `approve_time`,
                    ar.name as `area_name`
                FROM `check_in_machine_map` cimm
                JOIN `staff` AS st ON st.code = cimm.staff_code
                LEFT JOIN `regional_market` rm ON st.regional_market = rm.id
                LEFT JOIN `area` ar ON rm.area_id = ar.id
                LEFT JOIN `company_group_map` AS cgm ON cgm.title = st.title 
                LEFT JOIN `all_date` ad ON ad.date BETWEEN :from_date AND :to_date
                LEFT JOIN `check_in_detail` cid ON cid.staff_code = st.code AND cid.check_in_day = ad.date
                LEFT JOIN `check_in_shift` cis ON cis.staff_code = cid.staff_code
                LEFT JOIN `time` AS ti ON ad.date = DATE(ti.created_at) AND ti.staff_id = st.id
                JOIN `team` t ON st.title = t.id
                JOIN `team` t2 ON st.department = t2.id
                WHERE st.status = 1" . $where_sql;

        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to'], PDO::PARAM_STR);

        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        $data_map = array();

        foreach ($data as $key => $value) 
        {
            if(empty($data_map[$value['department_id']]['name']))
            {
                $data_map[$value['department_id']]['name'] = $value['department_name'];
                $data_map[$value['department_id']]['list_staff'] = array();
            }

            if(empty($data_map[$value['department_id']]['list_staff'][$value['code']]['name']))
            {
                $data_map[$value['department_id']]['list_staff'][$value['code']]['name'] = $value['staff_name'];
                $data_map[$value['department_id']]['list_staff'][$value['code']]['title'] = $value['title'];
                $data_map[$value['department_id']]['list_staff'][$value['code']]['area_name'] = $value['area_name'];
                $data_map[$value['department_id']]['list_staff'][$value['code']]['code'] = $value['code'];
                $data_map[$value['department_id']]['list_staff'][$value['code']]['list_time'] = array();
            }

            $data_map[$value['department_id']]['list_staff'][$value['code']]['list_time'][intval(date('d', strtotime($value['check_in_day'])))] = ($value['cong']>$value['approve_time'] && $value['approve_time'] != '')?$value['approve_time']:$value['cong'];
        }
        return $data_map;
    }

    public function fetchStaffMap($params = array())
    {
        $db = Zend_Registry::get('db');
        $sql = "
            SELECT
                cimm.*,
                CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                st.email as `email`,
                st.code as `code`
            FROM `check_in_machine_map` cimm
            JOIN `staff` st ON cimm.staff_code = st.code
            WHERE TRUE
                AND st.status = 1
            ORDER BY cimm.ip_machine,  cimm.enroll_number
        ";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

    public function getIpMachine($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT *
                FROM `device`
                WHERE status = 1";

        $stmt = $db->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();

        $stmt = $db = null;
        return $data;
    }

    public function getStaffIpMachine($staff_code)
    {
        if(!empty($staff_code))
        {
            $db = Zend_Registry::get('db');

            $sql = "SELECT *
                    FROM check_in_machine_map 
                    WHERE staff_code = :code
                    ORDER BY status DESC";

            $stmt = $db->prepare($sql);
            $stmt->bindParam('code', $staff_code, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll();

            $stmt->closeCursor();
            $stmt = $db = null;
            return $data;
        }
    }

    public function insertData($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql_check = "SELECT *
                        FROM check_in_machine_map
                        WHERE staff_code <> '" . $params['code'] . "' ";
        $sql_where_check = array();
        foreach ($params['data'] as $key => $value) 
        {
            $sql_where_check[] = " (enroll_number = " . $value['enroll_number'] . " AND ip_machine = '" . $value['ip_machine'] . "')";
        }
        $sql_check .= " AND (" . implode(" OR ", $sql_where_check) . ")";
        $stmt = $db->prepare($sql_check);
        $stmt->execute();
        $data_check = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = null;
        $data_return = array();
        if(!empty($data_check))
        {
            $data_return['status'] = false;
            $data_return['data'] = $data_check;
        }
        else
        {
            $sql_update = "INSERT INTO check_in_machine_map (`enroll_number`, `ip_machine`, `staff_code`, `status`) ";
            $array_value = array();
            foreach ($params['data'] as $key => $value) 
            {
                $array_value[] = "(" . $value['enroll_number'] . ",'" . $value['ip_machine'] . "','" . $params['code'] . "'," . $value['status'] . ")";
            }

            $sql_update .= " VALUES" . implode(",", $array_value); 
            $sql_update .= " ON DUPLICATE KEY UPDATE `status` = VALUES(`status`)";

            $stmt = $db->prepare($sql_update);
            $stmt->execute();
            $stmt->closeCursor();
            $stmt = $db = null;
        }
    }
    
//     public function listLate($params = array())
//     {
//         $db = Zend_Registry::get('db');
    
//         $sql = "CALL `get_late_list`(:from_date, :to_date)";
    
//         $stmt = $db->prepare($sql);
//         $stmt->bindParam('from_date', $params['from_date'], PDO::PARAM_STR);
//         $stmt->bindParam('to_date', $params['to_date'], PDO::PARAM_STR);
//         $stmt->execute();
    
//         $data = $stmt->fetchAll();
//         $stmt->closeCursor();
//         $stmt = $db = null;
    
//         return $data;
//     }

}