<?php
class Application_Model_CultureGame extends Zend_Db_Table_Abstract
{
	protected $_name = 'culture_game';


    public function getPlayedFinishRoundFirst($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.staff_id'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->joinleft(array('d'=>'culture_game_detail_round_first'),'d.master_id = p.id', array ('d.*'));       
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $select->where('d.is_right = ?', 1);
        $select->where('p.is_done = ?', 1);
        $select->group('d.question_id');
        $result = $db->fetchAll($select);
        return $result;
    }


    public function getPlayedFinishRoundSecond($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.*'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchRow($select);
        return $result;
    }
    public function getPlayedFinishRoundThird($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.*'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchRow($select);
        return $result;
    }
    public function getPlayedFinishRoundFour($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.*'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchRow($select);
        return $result;
    }
    public function getSecondFinishRoundFirst($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.staff_id'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->joinleft(array('d'=>'culture_game_detail_round_first'),'d.master_id = p.id', array ('d.*'));       
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $select->where('d.is_right = ?', 1);
        $select->where('d.question_id = ?', 2);
        $result = $db->fetchRow($select);
        return $result;
    }
     public function getSecondFinishRoundSecond($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.*'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchRow($select);
        return $result;
    }
     public function getSecondFinishRoundThird($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.*'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchRow($select);
        return $result;
    }
    public function getSecondFinishRoundFour($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.*'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchRow($select);
        return $result;
    }
     public function getPlayedRoundFirst($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.staff_id'
        );
        $select->from(array('p' => 'culture_game'), $arrCols); 
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCurrentResultOne(){
        $db = Zend_Registry::get('db');
        $select = " SELECT * FROM (
                        SELECT p.staff_id,
                                CONCAT(s.firstname,' ',s.lastname) as staff_name,
                                CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                                    WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND1."- (p.share_fb*10))
                                    ELSE (".CULTURE_GAME_TIME_SECOND_ROUND1."+".CULTURE_GAME_TIME_SECOND_ROUND1."+ 300 -(p.share_fb*10)) END as `total_seconds`,
                                p.seconds as seconds,
                                p.`miliseconds` as `miliseconds`,
                                p.share_fb as `share_fb`, 
                                p.num_play as times,
                                a.name as area 
                        FROM `culture_game` p
                        LEFT JOIN staff s ON s.id = p.staff_id
                        LEFT JOIN regional_market r ON r.id = s.regional_market
                        LEFT JOIN area a on a.id = r.area_id
                        WHERE p.round = ".CULTURE_GAME_CURRENT_ROUND."
                    ) f where 1 
                    ORDER BY f.total_seconds,f.miliseconds ASC";
                      

        $result          = $db->fetchAll($select);
        return $result;
    }
    public function getCurrentResultTwo(){
        $db = Zend_Registry::get('db');
        $select = "SELECT * FROM (
                        SELECT p.staff_id,
                                CONCAT(s.firstname,' ',s.lastname) as staff_name,
                                CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                                    WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND2."- (p.share_fb*10))
                                    ELSE (".CULTURE_GAME_TIME_SECOND_ROUND2."+ ".CULTURE_GAME_TIME_SECOND_ROUND2."+ 300 -(p.share_fb*10)) END as `total_seconds`,
                                p.seconds as seconds,
                                p.`miliseconds` as `miliseconds`,
                                p.share_fb as `share_fb`, 
                                p.num_play as times,
                                a.name as area 
                        FROM `culture_game` p
                        LEFT JOIN staff s ON s.id = p.staff_id
                        LEFT JOIN regional_market r ON r.id = s.regional_market
                        LEFT JOIN area a on a.id = r.area_id
                        WHERE p.round = ".CULTURE_GAME_CURRENT_ROUND."
                    ) f where 1 
                    ORDER BY f.total_seconds,f.miliseconds ASC";
                      

        $result          = $db->fetchAll($select);
        return $result;
    }

    public function getCurrentResultThird(){
        $db = Zend_Registry::get('db');
        $select = "SELECT * FROM (
                        SELECT p.staff_id,
                                CONCAT(s.firstname,' ',s.lastname) as staff_name,
                                CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                                    WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND3."- (p.share_fb*10))
                                    ELSE (".CULTURE_GAME_TIME_SECOND_ROUND3."+ ".CULTURE_GAME_TIME_SECOND_ROUND3."+ 300 -(p.share_fb*10)) END as `total_seconds`,
                                p.seconds as seconds,
                                p.`miliseconds` as `miliseconds`,
                                p.share_fb as `share_fb`, 
                                p.num_play as times,
                                a.name as area 
                        FROM `culture_game` p
                        LEFT JOIN staff s ON s.id = p.staff_id
                        LEFT JOIN regional_market r ON r.id = s.regional_market
                        LEFT JOIN area a on a.id = r.area_id
                        WHERE p.round = ".CULTURE_GAME_CURRENT_ROUND."
                    ) f where 1 
                    ORDER BY f.total_seconds,f.miliseconds ASC";
                      

        $result          = $db->fetchAll($select);
        return $result;
    }

    public function getCurrentResultFour(){
        $db = Zend_Registry::get('db');
        $select = "SELECT * FROM (
                        SELECT p.staff_id,
                                CONCAT(s.firstname,' ',s.lastname) as staff_name,
                                CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                                    WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND4."- (p.share_fb*10))
                                    ELSE (".CULTURE_GAME_TIME_SECOND_ROUND4."+ ".CULTURE_GAME_TIME_SECOND_ROUND4."+ 300 -(p.share_fb*10)) END as `total_seconds`,
                                p.seconds as seconds,
                                p.`miliseconds` as `miliseconds`,
                                p.share_fb as `share_fb`, 
                                p.num_play as times,
                                a.name as area 
                        FROM `culture_game` p
                        LEFT JOIN staff s ON s.id = p.staff_id
                        LEFT JOIN regional_market r ON r.id = s.regional_market
                        LEFT JOIN area a on a.id = r.area_id
                        WHERE p.round = ".CULTURE_GAME_CURRENT_ROUND."
                    ) f where 1 
                    ORDER BY f.total_seconds,f.miliseconds ASC";
                      

        $result          = $db->fetchAll($select);
        return $result;
    }

    public function getCurrentResultAll(){
        $db = Zend_Registry::get('db');
        $select = "SELECT * FROM (
                        SELECT  p.staff_id, 
                                CONCAT(s.firstname,' ',s.lastname) as staff_name,
                                a.name as area, 
                                IFNULL(r1.total_seconds,1500) as round1,
                                (r1.`miliseconds`) as mili_round1,
                                IFNULL(r2.total_seconds,900) as round2,
                                (r2.`miliseconds`) as mili_round2,
                                IFNULL(r3.total_seconds,1140) as round3,
                                (r3.`miliseconds`) as mili_round3,
                                IFNULL(r4.total_seconds,1140) as round4,
                                (r4.`miliseconds`) as mili_round4,
                                ( IFNULL(r1.total_seconds,1500) + IFNULL(r2.total_seconds,900) + IFNULL(r3.total_seconds,1140)+ IFNULL(r4.total_seconds,1140)) as total_all,
                                ( IFNULL(r1.`miliseconds`,0) + IFNULL(r2.`miliseconds`,0) + IFNULL(r3.`miliseconds`,0)+ IFNULL(r4.`miliseconds`,0)) as total_milisecond
                        FROM culture_game p
                        LEFT JOIN (
                            SELECT p.staff_id,
                            CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                            WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND1."- (p.share_fb*10))
                            ELSE (".CULTURE_GAME_TIME_SECOND_ROUND1."+".CULTURE_GAME_TIME_SECOND_ROUND1."+ 300 -(p.share_fb*10)) END as `total_seconds`, p.miliseconds
                            FROM `culture_game` p
                            WHERE p.round = ".CULTURE_GAME_ROUND1."
                        ) as r1 ON r1.staff_id = p.staff_id
                        LEFT JOIN (
                            SELECT p.staff_id,
                            CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                            WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND2."- (p.share_fb*10))
                            ELSE (".CULTURE_GAME_TIME_SECOND_ROUND2." + ".CULTURE_GAME_TIME_SECOND_ROUND2." + 300 -(p.share_fb*10)) END as `total_seconds`, p.miliseconds
                            FROM `culture_game` p
                            WHERE p.round = ".CULTURE_GAME_ROUND2."
                        ) as r2 ON r2.staff_id = p.staff_id
                        LEFT JOIN (
                            SELECT p.staff_id,
                            CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                            WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND3."- (p.share_fb*10))
                            ELSE (".CULTURE_GAME_TIME_SECOND_ROUND3." + ".CULTURE_GAME_TIME_SECOND_ROUND3." + 300 -(p.share_fb*10)) END as `total_seconds`, p.miliseconds
                            FROM `culture_game` p
                            WHERE p.round = ".CULTURE_GAME_ROUND3."
                        ) as r3 ON r3.staff_id = p.staff_id
                        LEFT JOIN (
                            SELECT p.staff_id,
                            CASE WHEN p.is_done = 1 AND p.num_play = 1 THEN (p.seconds - (p.share_fb*10))
                            WHEN p.is_done = 1 AND p.num_play = 2 THEN (p.seconds + 300 + ".CULTURE_GAME_TIME_SECOND_ROUND4."- (p.share_fb*10))
                            ELSE (".CULTURE_GAME_TIME_SECOND_ROUND4." + ".CULTURE_GAME_TIME_SECOND_ROUND4." + 300 -(p.share_fb*10)) END as `total_seconds`, p.miliseconds
                            FROM `culture_game` p
                            WHERE p.round = ".CULTURE_GAME_ROUND4."
                        ) as r4 ON r4.staff_id = p.staff_id
                        LEFT JOIN staff s ON s.id = p.staff_id
                        LEFT JOIN regional_market r ON r.id = s.regional_market
                        LEFT JOIN area a on a.id = r.area_id
                        WHERE 1
                        GROUP BY p.staff_id
                    ) f WHERE 1
                    ORDER BY f.total_all,f.total_milisecond ASC";
        $result          = $db->fetchAll($select);
        return $result;
    }


    public function getStationReward(){
        $db = Zend_Registry::get('db');
        $select = "SELECT p.staff_id, CONCAT(s.firstname,' ',s.lastname,'_',a.name) as              staff_name, r.reward as reward, r.image_name
                    FROM `mini_game_reward_log` p 
                    LEFT JOIN staff s on s.id = p.staff_id
                    LEFT JOIN mini_game_reward r ON r.id = p.reward
                    LEFT JOIN regional_market g ON g.id = s.regional_market
                    LEFT JOIN area a ON a.id = g.area_id
                    WHERE p.reward NOT IN(21)";
                      

        $result          = $db->fetchAll($select);
        return $result;
    }


}
