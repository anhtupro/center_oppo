<?php
class Application_Model_Timing extends Zend_Db_Table_Abstract
{
    protected $_name = 'timing';

    function getSellOut($date, $to_date, $good_id){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('ts' => 'timing_sale'), array('product_count' => 'COUNT(ts.id)'))
            ->joinLeft(array('t' => 'timing'), 't.id=ts.timing_id', array())
            ->join(array('g' => WAREHOUSE_DB.'.'.'good'), 'ts.product_id=g.id', array())
            ->group('ts.product_id');

        $select->where('t.from >= ?', $date.' 00:00:00');
        $select->where('t.from <= ?', $to_date.' 23:59:59');
        $select->where('ts.product_id = ?', $good_id);

        $result = $db->fetchOne($select);
        return $result;
    }

    function getSellOut2($date, $to_date, $good_id){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('ts' => 'timing_sale'), array('product_count' => 'COUNT(ts.id)'))
            ->joinLeft(array('t' => 'timing'), 't.id=ts.timing_id', array())
            ->join(array('g' => WAREHOUSE_DB.'.'.'good'), 'ts.product_id=g.id', array())
            ->group('ts.product_id');

        $select->where('t.from >= ?', $date.' 00:00:00');
        $select->where('t.from <= ?', $to_date.' 23:59:59');
        $select->where('ts.product_id = ?', $good_id);

        $result = $db->fetchOne($select);
        return $result;
    }

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
        } else {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array('p.*'));
        }
        $select->distinct();
        //sửa điều kiện inner join -> left join on staff
        $select
            ->join(array('t1' => 'timing_sale'),'p.id = t1.timing_id',array())
            ->joinLeft(array('s' => 'staff'),
                'p.staff_id = s.id',
                array('s.email', 's.firstname', 's.lastname', 's.department', 's.team', 's.regional_market', 's.phone_number'))
            ->joinLeft(array('st' => 'store'),'p.store = st.id',array())
            ->joinLeft(array('v'=>'v_store_area'),'v.store_id = st.id',array())
            ;
                
                



        if (isset($params['imei']) and $params['imei']){

            //chỉ khi search theo imei mới cần join timing_sale
     
            $select->group('p.id');

            $select->where('t1.imei = ?', $params['imei']);
        }
        
        if (isset($params['type_approve']) and $params['type_approve'] and (!isset($params['imei']))){

            //chỉ khi search theo type_approve mới cần join timing_sale
            if(intval($params['type_approve']) == PHONE_CAT_ID){
                $select
                    ->joinLeft(array('ts' => 'timing_sale'),
                        'p.id = ts.timing_id',
                        array());
                
            }
            else if(intval($params['type_approve']) == ACCESS_CAT_ID){
                $select
                    ->joinLeft(array('ts' => 'timing_accessories'),
                        'p.id = ts.timing_id',
                        array());
            }
            $select->group('p.id');
            $select->where('ts.id IS NOT NULL',null);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('s.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if (isset($params['name']) and $params['name'])
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%'.$params['name'].'%');

        if (isset($params['staff_id']) and $params['staff_id']) {
            $select->where('s.id = ?', $params['staff_id']);

        } elseif (isset($params['sale']) and $params['sale']) {
            // follow STORE-STAFF concept
            $is_leader  = 1;
            if(isset($params['title']) and $params['title'] == PB_SALES_TITLE){
                $is_leader = 2;
            }

            $select ->join(array('ssl' => 'store_staff_log'), 'p.store = ssl.store_id', array());
            $log_where = $this->getAdapter()->quoteInto('ssl.staff_id = ?', $params['sale']).
                        " AND " . $this->getAdapter()->quoteInto('ssl.is_leader = ?', $is_leader).
                        " AND " . $this->getAdapter()->quoteInto('p.`from` >= FROM_UNIXTIME(ssl.joined_at, \'%Y-%m-%d\')', 1).
                        " AND (".
                            $this->getAdapter()->quoteInto('p.`from` < FROM_UNIXTIME(ssl.released_at, \'%Y-%m-%d 23:59:59\')', 1).
                            " OR " . $this->getAdapter()->quoteInto('ssl.released_at IS NULL', 1).
                            " OR " . $this->getAdapter()->quoteInto('ssl.released_at = 0', 1).
                        " ) ";

            $select->where($log_where);

        } elseif (isset($params['leader']) and $params['leader']) {
            // follow STORE-STAFF concept
            // lấy store thuộc region mà nó quản lý
            $select
                ->joinLeft(array('lg' => 'store_leader_log'), 'st.id = lg.store_id', array())
                ->joinLeft(array('ssl' => 'store_staff_log'), 'st.id = ssl.store_id', array());

            // quyền leader
            $log_where = " ( " . $this->getAdapter()->quoteInto('lg.staff_id = ?', $params['leader']).
                        " AND " . $this->getAdapter()->quoteInto('p.`from` >= FROM_UNIXTIME(lg.joined_at, \'%Y-%m-%d\')', 1).
                        " AND (".
                            $this->getAdapter()->quoteInto('p.`from` < FROM_UNIXTIME(lg.released_at, \'%Y-%m-%d 23:59:59\')', 1).
                            " OR " . $this->getAdapter()->quoteInto('lg.released_at IS NULL', 1).
                            " OR " . $this->getAdapter()->quoteInto('lg.released_at = 0', 1).

                        " )
                        ) ";
            // quyền sales
            $log_where .= " OR " .

                        " ( " . $this->getAdapter()->quoteInto('ssl.staff_id = ?', $params['leader']).
                        " AND " . $this->getAdapter()->quoteInto('ssl.is_leader = ?', 1).
                        " AND " . $this->getAdapter()->quoteInto('p.`from` >= FROM_UNIXTIME(ssl.joined_at, \'%Y-%m-%d\')', 1).
                        " AND (".
                            $this->getAdapter()->quoteInto('p.`from` < FROM_UNIXTIME(ssl.released_at, \'%Y-%m-%d 23:59:59\')', 1).
                            " OR " . $this->getAdapter()->quoteInto('ssl.released_at IS NULL', 1).
                            " OR " . $this->getAdapter()->quoteInto('ssl.released_at = 0', 1).
                        " )
                        )";

            $select->where($log_where);

        } elseif (isset($params['asm']) and $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            
            if (count($list_regions))
                $select->where('v.regional_market IN (?)', $list_regions); // lọc staff thuộc regional_market trên
            else
                $select->where('1=0', 1);

        }

        if (isset($params['from_date']) and $params['from_date']){
            $date = explode('/', $params['from_date']);
            $select->where('p.`from` >= ?', $date[2].'-'.$date[1].'-'.$date[0]);
        }

        if (isset($params['to_date']) and $params['to_date']){
            $date = explode('/', $params['to_date']);
            $select->where('p.`from` <= ?', $date[2].'-'.$date[1].'-'.$date[0] . ' 23:59:59');
        }

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('v.regional_market = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('v.district = ?', $params['district']);

        if (isset($params['store']) and $params['store'])
            $select->where('st.id = ?', $params['store']);

        if (isset($params['area_id']) and $params['area_id']){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();
            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            $select->where('v.regional_market IN (?)', $tem);
        }

        if (isset($params['sort']) and $params['sort']) {
            $order_str = '';

            switch ( $params['sort'] ) {
                case 'st.id':
                    $order_str = 'st.`name`';
                    break;
                case 'st.regional_market':
                    $select->join(array('rm' => 'regional_market'),
                        'v.regional_market = rm.id',
                        array());
                    $order_str = 'rm.`name`';
                    break;
                default:
                    break;
            }

            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(s.firstname, " ",s.lastname) '.$collate . $desc;
            } elseif ( in_array($params['sort'], array('st.regional_market', 'st.id'  ))   ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } else {
                $order_str = $params['sort'] . ' ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }



        if ($limit)
            $select->limitPage($page, $limit);

        if(!empty($_GET['dev'])){
    echo $select->__toString();
    exit;
}

        PC::debug($select->__toString());
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function report($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        if ( isset($params['get_total_sales']) and $params['get_total_sales'] )
            $get = array();
        elseif (isset($params['export2']) and $params['export2'])
            $get = array(
                's.id', 's.title', 's.firstname', 's.lastname', 's.regional_market'
            );
        else
            $get = array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'), 'sid' => 's.id', 's.title', 's.firstname', 's.lastname', 's.department', 's.phone_number'
            );

        $from = explode('/', $params['from']);
        $to = explode('/', $params['to']);

        if (isset($params['export2']) and $params['export2']){
            $select = $db->select()
                ->from(array('s' => 'staff'),
                    $get
                )
                ->joinLeft(array('t' => 'timing'),
                    's.id = t.staff_id AND t.approved_at IS NOT NULL AND t.approved_at <> 0 AND t.approved_at <> \'\'  AND t.`from` >= \''. $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00\' AND t.`from` <= \''. $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59\'',
                    array('t.staff_id', 't.from', 't.store')
                )
                ->joinLeft(array('sto' => 'store'),
                    't.store = sto.id',
                    array('store_name' => 'sto.name', 'sto.company_address', 'sto.shipping_address')
                )
                ->joinLeft(array('r' => 'regional_market'),
                    'sto.regional_market = r.id',
                    array('regional_market' => 'r.name'))
                ->joinLeft(array('d' => 'regional_market'),
                    'sto.district = d.id',
                    array('district' => 'd.name'))
                ->joinLeft(array('a' => 'area'),
                    'r.area_id = a.id',
                    array('area' => 'a.name'))
                ->joinLeft(array('ts' => 'timing_sale'),
                    't.id = ts.timing_id',
                    array('ts.product_id', 'ts.model_id', 'ts.customer_id', 'ts.imei'))

                ->joinLeft(array('cus' => 'customer'),
                    'ts.customer_id = cus.id',
                    array('customer_name' => 'cus.name', 'cus.email', 'cus.phone_number', 'cus.address'));

        } else {

            $select = $db->select()
                ->from(array('s' => 'staff'),
                    $get
                )
                ->joinLeft(array('t' => 'timing'),
                    's.id = t.staff_id AND t.`from` >= \''. $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00\' AND t.`from` <= \''. $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59\'',
                    array('t.from', 't.approved_at', 't.store')
                )
                ->joinLeft(array('sto' => 'store'),
                    't.store = sto.id',
                    array('store_name' => 'sto.name', 'sto.company_address', 'sto.shipping_address')
                )
                ->joinLeft(array('r' => 'regional_market'),
                    'sto.regional_market = r.id',
                    array('regional_market' => 'r.name'))
                ->joinLeft(array('a' => 'area'),
                    'a.id = r.area_id',
                    array('area' => 'a.name'))
                ->joinLeft(array('ts' => 'timing_sale'),
                    't.id = ts.timing_id  AND t.approved_at IS NOT NULL AND t.approved_at <> 0 AND t.approved_at <> \'\'',
                    array('product_count' => 'COUNT(ts.id)'))
                ->group('s.id');
        }

        $sub_select = $db->select()
            ->from(array('t1'=>'timing'), array('t1.staff_id'));
        if ( isset($params['store']) && $params['store'] ) {
            $sub_select->where( 't1.store = ?', $params['store']);
        }


        if (isset($params['from']) && $params['from'] && !isset($params['to'])) {

            $sub_select->where( 't1.`from` > ?', $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00' );

        } elseif (isset($params['to']) && $params['to'] && !isset($params['from'])) {

            $sub_select->where( 't1.`from` < ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59' );

        } elseif (isset($params['to']) && $params['to'] && isset($params['from']) && $params['from'] ) {

            $sub_select->where( 't1.`from` >= ?', $from[2].'-'.$from[1].'-'.$from[0] .' 00:00:00');

            $sub_select->where( 't1.`from` <= ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59');
        }

        $cond = $select
            ->orWhere('s.group_id IN (?)', array(PGPB_ID, SALES_ID))
            ->orWhere('s.id IN (?)', $sub_select)
            ->getPart(Zend_Db_Select::WHERE);

        $select->reset(Zend_Db_Select::WHERE);

        $select->where ( implode(' ', $cond ) );

        if ( isset($params['name']) && $params['name'] ) {
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        if ( isset($params['phone_number']) && $params['phone_number'] ) {
            $select->where( 's.phone_number LIKE ?', '%'.$params['phone_number'].'%');
        }

        if ( isset($params['area']) && $params['area'] ) {
            $select->where( 'r.area_id = ?', $params['area']);
        }

        if ( isset($params['asm']) && $params['asm'] ) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();

            if (count($list_regions) > 0)
                $select->where( 'sto.district IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        }

        if ( isset($params['regional_market']) && $params['regional_market'] ) {
            $select->where( 'sto.regional_market = ?', intval($params['regional_market']));
        }

        if ( isset($params['store']) && $params['store'] ) {
            $select->where( 't.store = ?', intval($params['store']));
        }

        if ( isset($params['distributor_id']) && $params['distributor_id'] ) {
            $select->where( 'sto.d_id = ?', intval($params['distributor_id']));
        }

        if (isset( $params['sort'] ) && $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) && $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            $order_str = '';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(s.firstname, " ",s.lastname) '.$collate . $desc;
            } elseif ( $params['sort'] == 'area' || $params['sort'] == 'province' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $params['sort'] . ' ' . $collate . $desc;
            } else {
                $order_str = $params['sort'] . ' ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        if ($limit) {
            $select->limitPage($page, $limit);
        }

        if ( isset($params['get_total_sales']) and $params['get_total_sales'] ){
            $select_p = $db->select()
                ->from(array('pa' => $select),
                    array(new Zend_Db_Expr('SUM( pa.product_count )')));
            return $db->fetchOne($select_p);
        }


        if (isset($params['export2']) and $params['export2']){
            return $select->__toString();
        }

        $analytics = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        $sales = array();
        foreach ($analytics as $key => $value) {
            $temp = array();
            $temp['total'] = $value['product_count'];
            $temp['title'] = $value['title'];
            $temp['firstname'] = $value['firstname'];
            $temp['lastname'] = $value['lastname'];
            $temp['phone_number'] = $value['phone_number'];
            $temp['regional_market'] = $value['regional_market'];
            $temp['store'] = $value['store'];
            $temp['area'] = $value['area'];

            $sales[$value['sid']] = $temp;
        }

        return $sales;
    }

    function report2a()
    {
        $sql = "SELECT
			COUNT(ts.product_id)
		FROM
			timing_sale ts
		WHERE
			ts.timing_id IN(
				SELECT
					t.id
				FROM
					timing t
				WHERE
					t.store IN(?)
				AND DATE(t.`from`)>= ?
				AND DATE(t.`from`)<= ?
			)
		AND ts.product_id = ?";


    }

    // function get_leader_by_area_cache(){
        //     $cache      = Zend_Registry::get('cache');
        //     $result     = $cache->load('leader_by_area_cache');

        //     if ($result === false) {

        //         $result = array();

        //         $db = Zend_Registry::get('db');

        //         $QArea = new Application_Model_Area();
        // 		$areas = $QArea->get_cache();

        //  	foreach ($areas as $area => $v) {
        // $select	= $db->select()
        // 	->from(array('st'=>'sales_team'), array('st.id'));
        // $st_ids = $db->fetchAll($select);

        // $select = $db->select()
        // 	->from(array('sts'=>'sales_team_staff'), array('sts.staff_id'))
        // 	->where('sts.is_leader = 1 AND sts.sales_team_id IN (?)', $st_ids);

        // $result[$area] = $db->fetchAll($select);
        //  	}

        //         $cache->save($result, 'leader_by_area_cache', array(), null);
        //     }
        //     return $result;
    // }

    function report_by_staff($params, &$analytics, &$count)
    {

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('s' => 'staff'),
                array());

        $select
            ->join(array('r' => 'regional_market'),
                'r.id = s.regional_market',
                array())
            ->join(array('a' => 'area'),
                'r.area_id = a.id',
                array())
            ->join(array('t' => 'timing'),
                's.id = t.staff_id AND t.approved_at IS NOT NULL AND t.approved_at <> 0',
                array('time' => 't.from',  't.store'))
            ->join(array('ts' => 'timing_sale'),
                'ts.timing_id = t.id',
                array('imei' => 'ts.imei'))
            ->joinLeft(array('c' => 'customer'),
                'c.id = ts.customer_id',
                array('customer_name' => 'c.name', 'customer_phone' => 'phone_number', 'customer_address' => 'c.address'))
            ->join(array('p' => WAREHOUSE_DB.'.'.'good'),
                'p.id = ts.product_id',
                array('product_name' => 'p.desc', 'product_id' => 'p.id'));


        if ( isset($params['staff_id']) && $params['staff_id'] ) {
            $select->where('t.staff_id = ?', $params['staff_id']);
        }

        if ( isset($params['name']) && $params['name'] ) {
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        if ( isset($params['phone_number']) && $params['phone_number'] ) {
            $select->where( 's.phone_number LIKE ?', '%'.$params['phone_number'].'%');
        }

        if ( isset($params['area']) && $params['area'] ) {
            $select->where( 'r.area_id = ?', $params['area']);
        }

        if ( isset($params['regional_market']) && $params['regional_market'] ) {
            $select->where( 's.regional_market = ?', $params['regional_market']);
        }

        $from = explode('/', $params['from']);
        $to = explode('/', $params['to']);

        if (isset($params['from']) && $params['from'] && !isset($params['to'])) {
            $select->where( 't.from > ?', $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00' );

        } elseif (isset($params['to']) && $params['to'] && !isset($params['from'])) {
            $select->where( 't.from < ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59' );

        } elseif (isset($params['to']) && $params['to'] && isset($params['from']) && $params['from'] ) {
            $select->where( 't.from >= ?', $from[2].'-'.$from[1].'-'.$from[0] .' 00:00:00');
            $select->where( 't.from <= ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59');
        }

        $stmt  = $select->query();
        $total = $stmt->fetchAll();

        $sales = array();
        $analytics = array();
        $count = 0;

        if ( isset($params['staff_id']) && $params['staff_id'] ) {
            foreach ($total as $key => $value) {
            	// thống kê thêm cho PG
            	if ( ! isset( $analytics[ $value['product_id'] ] )  ) {
            		$analytics[ $value['product_id'] ] = 0;
            	}

            	$analytics[ $value['product_id'] ] += 1;
            	$count += 1;

                $temp                     = array();
                $temp['product']          = $value['product_name'];
                $temp['time']             = $value['time'];
                $temp['imei']             = $value['imei'];
                $temp['store']            = $value['store'];
                $temp['customer_name']    = $value['customer_name'];
                $temp['customer_phone']   = $value['customer_phone'];
                $temp['customer_address'] = $value['customer_address'];

                $sales[] = $temp;
            }
        }

        return $sales;
    }

    function report_by_product($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        if ( isset($params['get_total_sales']) and $params['get_total_sales'] )
            $count_expr = array();
        else
            $count_expr = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS t.id'));

        $select = $db->select()
            ->from(array('t' => 'timing'), $count_expr)
            ->join(array('ts' => 'timing_sale'), 't.id=ts.timing_id AND t.approved_at <> 0 AND t.approved_at IS NOT NULL', array('total' => 'COUNT(DISTINCT ts.imei)'))
            ->join(array('g' => WAREHOUSE_DB.'.'.'good'), 'g.id=ts.product_id', array('product_id' => 'g.id', 'product_name' => 'g.name', 'product_desc' => 'desc'))
            ->join(array('s' => 'store'), 's.id=t.store', array())
            ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array())
            ->join(array('a' => 'area'), 'a.id=r.area_id', array());

        $from = explode('/', $params['from']);
        $to = explode('/', $params['to']);

        if (isset($params['from']) && $params['from'])
            $select->where('t.from >= ?', $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00');

        if (isset($params['to']) && $params['to'])
            $select->where('t.from <= ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59');

        if (isset($params['sales_from']) && $params['sales_from'])
            $select->having('total >= ?', $params['sales_from']);

        if (isset($params['sales_to']) && $params['sales_to'])
            $select->having('total <= ?', $params['sales_to']);

        if (isset($params['name']) && $params['name'])
            $select->where('g.name LIKE ?', '%'.$params['name'].'%');

        if (isset($params['area_id'])) {
            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $select->where('a.id IN (?)', $params['area_id']);
            }
        }

        if (isset($params['asm']) && $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();

            if (count($list_regions) > 0)
                $select->where('s.district IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['sales_store']) && intval($params['sales_store']) > 0) {
            $select
                ->joinRight(array('ssl' => 'store_staff_log'), 'ssl.store_id=t.store', array());

            $log_where = $this->getAdapter()->quoteInto('ssl.staff_id = ?', $params['sales_store']).
                        " AND " . $this->getAdapter()->quoteInto('ssl.is_leader = ?', 1).
                        " AND " . $this->getAdapter()->quoteInto('DATE(t.from) >= FROM_UNIXTIME(ssl.joined_at, \'%Y-%m-%d\')', 1).
                        " AND (".
                            $this->getAdapter()->quoteInto('DATE(t.from) < FROM_UNIXTIME(ssl.released_at, \'%Y-%m-%d\')', 1).
                            " OR " . $this->getAdapter()->quoteInto('ssl.released_at IS NULL', 1).
                            " OR " . $this->getAdapter()->quoteInto('ssl.released_at = 0', 1).
                        " ) ";

            $select->where($log_where);
        }

        if (isset($params['leader_province']) && intval($params['leader_province']) > 0) {
            $select
                ->joinRight(array('lg' => 'leader_log'), 's.regional_market = lg.regional_market', array());

            $log_where = $this->getAdapter()->quoteInto('lg.staff_id = ?', $params['leader_province']).
                        " AND " . $this->getAdapter()->quoteInto('DATE(t.from) >= FROM_UNIXTIME(lg.from_date, \'%Y-%m-%d\')', 1).
                        " AND (".
                            $this->getAdapter()->quoteInto('DATE(t.from) < FROM_UNIXTIME(lg.to_date, \'%Y-%m-%d\')', 1).
                            " OR " . $this->getAdapter()->quoteInto('lg.to_date IS NULL', 1).
                            " OR " . $this->getAdapter()->quoteInto('lg.to_date = 0', 1).
                        " ) ";

            $select->where($log_where);
        }

        if (isset($params['regional_market'])) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('r.id IN (?)', $params['regional_market']);
            }
        }

        if (isset($params['district'])) {
            if (is_array($params['district']) && count($params['district']) > 0) {
                $select->where('s.district IN (?)', $params['district']);
            }
        }

        if (isset($params['store'])) {
            if (is_array($params['store']) && count($params['store']) > 0) {
                $select->where('s.id IN (?)', $params['store']);
            }
        }

        if (isset($params['sort']) && $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) && $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            $order_str = '';

            switch ($params['sort']) {
                case 'area_name':
                    $params['sort'] = 'a.name';
                    break;
                case 'province_name':
                    $params['sort'] = 'r.name';
                    break;
                case 'product_name':
                    $params['sort'] = 'g.name';
                    break;
                default:
                    break;
            }

            if ( in_array( $params['sort'], array( 'area_name', 'product_name', 'province_name' ) ) )
                $collate = ' COLLATE utf8_unicode_ci ';

            $order_str = $params['sort'] . ' ' . $collate . $desc;

            $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('g.id');
        if ($limit && !$params['export'])
            $select->limitPage($page, $limit);

        if ( isset($params['get_total_sales']) and $params['get_total_sales'] ){
            $select_p = $db->select()
                ->from(array('pa' => $select),
                    array(new Zend_Db_Expr('SUM( pa.total )')));

            return $db->fetchOne($select_p);
        } else {
            $result = $db->fetchAll($select);
        }

        if (!$params['export'])
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function short_report_by_store($params)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('s' => 'store'),
                array());

        $select
            ->join(array('r' => 'regional_market'),
                'r.id = s.regional_market',
                array())
            ->join(array('a' => 'area'),
                'r.area_id = a.id',
                array())
            ->join(array('t' => 'timing'),
                's.id = t.store AND t.approved_at IS NOT NULL AND t.approved_at <> 0 AND t.approved_at <> \'\'',
                array('time' => 't.from'))
            ->join(array('sta' => 'staff'),
                'sta.id = t.staff_id',
                array('sta.firstname', 'sta.lastname'))
            ->join(array('ts' => 'timing_sale'),
                'ts.timing_id = t.id',
                array('imei' => 'ts.imei'))
            ->joinLeft(array('c' => 'customer'),
                'c.id = ts.customer_id',
                array('customer_name' => 'c.name', 'customer_phone' => 'phone_number', 'customer_address' => 'c.address'))
            ->join(array('p' => WAREHOUSE_DB.'.'.'good'),
                'p.id = ts.product_id',
                array('product_name' => 'p.desc'));


        if ( isset($params['store_id']) && $params['store_id'] ) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if ( isset($params['name']) && $params['name'] ) {
            $select->where('s.name LIKE ?', '%'.$params['name'].'%');
        }

        if ( isset($params['area']) && $params['area'] ) {
            $select->where( 'r.area_id = ?', $params['area']);
        }

        if ( isset($params['regional_market']) && $params['regional_market'] ) {
            $select->where( 's.regional_market = ?', $params['regional_market']);
        }

        $from = explode('/', $params['from']);
        $to = explode('/', $params['to']);

        if (isset($params['from']) && $params['from'] && !isset($params['to'])) {
            $select->where( 't.from > ?', $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00' );

        } elseif (isset($params['to']) && $params['to'] && !isset($params['from'])) {
            $select->where( 't.from < ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59' );

        } elseif (isset($params['to']) && $params['to'] && isset($params['from']) && $params['from'] ) {
            $select->where( 't.from >= ?', $from[2].'-'.$from[1].'-'.$from[0] .' 00:00:00');
            $select->where( 't.from <= ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59');
        }

        $stmt  = $select->query();
        $total = $stmt->fetchAll();

        $sales = array();
        if ( isset($params['store_id']) && $params['store_id'] ) {
            foreach ($total as $key => $value) {
                $temp                     = array();
                $temp['product']          = $value['product_name'];
                $temp['time']             = $value['time'];
                $temp['imei']             = $value['imei'];
                $temp['customer_name']    = $value['customer_name'];
                $temp['customer_phone']   = $value['customer_phone'];
                $temp['customer_address'] = $value['customer_address'];
                $temp['pg']               = $value['firstname'] . ' ' . $value['lastname'];

                $sales[] = $temp;
            }
        }

        return $sales;
    }

    function report_by_area($params)
    {
        // SELECT
        //     a.id,
        //     a.`name`,
        //     COUNT(DISTINCT ts.imei) AS total_quantity,
        //     SUM(ts.price)* 0.8 AS total_value
        // FROM
        //     timing t
        // INNER JOIN timing_sale ts ON t.id = ts.timing_id
        // AND t.approved_at IS NOT NULL
        // AND t.approved_at <> 0
        // AND t.approved_at <> ''
        // AND t.`from` >= '2015-02-01 00:00:00'
        // AND t.`from` <= '2015-02-07 23:59:59'
        // INNER JOIN store s ON t.store = s.id
        // INNER JOIN regional_market r ON s.regional_market = r.id
        // INNER JOIN area a ON r.area_id = a.id
        // GROUP BY
        //     a.id
        $from = explode('/', $params['from']);
        $to = explode('/', $params['to']);


        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('t' => 'timing'), array())
            ->join(array('ts' => 'timing_sale'), 't.id=ts.timing_id
                                                    AND t.approved_at IS NOT NULL
                                                    AND t.approved_at <> 0
                                                    AND t.approved_at <> \'\'',
                                                array(
                                                    'total_quantity' => 'COUNT(DISTINCT ts.imei)',
                                                    'total_activated' => 'COUNT( DISTINCT CASE WHEN (i.activated_date IS NOT NULL AND i.activated_date <> 0 AND i.activated_date <> "") THEN i.imei_sn ELSE NULL END )',
                                                    'total_value' => 'SUM( ts.price )*0.8',
                                                    'total_value_activated' => 'SUM( CASE WHEN i.activated_date IS NOT NULL AND i.activated_date <> 0 AND i.activated_date <> "" THEN ts.price ELSE 0 END )*0.8',
                                                    ))
            ->join(array('s' => 'store'), 't.store=s.id', array())
            ->join(array('d' => 'regional_market'), 's.district=d.id', array())
            ->join(array('r' => 'regional_market'), 'd.parent=r.id', array())
            ->join(array('a' => 'area'), 'r.area_id=a.id', array('a.id', 'a.name', 'a.region_share'))
            ->join(array('i' => WAREHOUSE_DB.'.imei'),'i.imei_sn = ts.imei',array())
            ->group('a.id');

        if (isset($params['from']) && $params['from'])
            $select->where('t.from >= ?', $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00');

        if (isset($params['to']) && $params['to'])
            $select->where('t.from <= ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59');

        if (isset($params['export']) and $params['export'])
            return $select->__toString();

        if ( isset($params['get_total_sales']) and $params['get_total_sales'] ){
            $select_total = $db->select()
                ->from(array('A' => $select), array(
                        'total_quantity'        => 'SUM(A.total_quantity)',
                        'total_activated'       => 'SUM(A.total_activated)',
                        'total_value'           => 'SUM(A.total_value)',
                        'total_value_activated' => 'SUM(A.total_value_activated)',
                    )
                );
            return $db->fetchRow($select_total);
        }

        $analytics = $db->fetchAll($select);

        return $analytics;
    }

    function getDay($params)
    {
        $db = Zend_Registry::get('db');
        $staff_id = $params['user_id'];
        $month = $params['month'];
        // $condition  = $params['condition'];


        if (isset($staff_id) and isset($month)) {
            $select = $db->select()->from(array('p' => $this->_name), array('p.*'));
            $select->where('staff_id = ? ', $staff_id);
            $select->where('MONTH(created_at) = ?', $month);

            $result = $db->fetchAll($select);
            $day_approve = $day_not_approve = 0;

            foreach ($result as $k => $v)
            {
                if($v['status'] == 0)
                {
                    $day_not_approve++;
                }
                else
                {
                    $day_approve++;
                }
            }

            $result = array(
            'day_approve' => $day_approve,
            'day_not_approve' => $day_not_approve,
            'total' => count($result),
            );

            return $result;
        } else
            return - 1;
    }

    function count_product_by_area($area_id, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('t' => 'timing'),array()
            );

        $select->join(array('s' => 'store'),
            't.store = s.id',
            array());

        $select->join(array('r' => 'regional_market'),
            's.regional_market = r.id',
            array());

        $select->joinLeft(array('ts' => 'timing_sale'),
            'ts.timing_id = t.id',
            array('product_count' => 'COUNT(ts.product_id)', 'ts.product_id'));

        $select->where('t.approved_at IS NOT NULL AND t.approved_at <> 0 AND t.approved_at <> \'\'');

        $select->group('ts.product_id');

        $from = explode('/', $params['from']);
        $to = explode('/', $params['to']);

        if (isset($params['from']) && $params['from'] && !isset($params['to'])) {
            $select->where( 't.from > ?', $from[2].'-'.$from[1].'-'.$from[0] . ' 00:00:00' );

        } elseif (isset($params['to']) && $params['to'] && !isset($params['from'])) {
            $select->where( 't.from < ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59' );

        } elseif (isset($params['to']) && $params['to'] && isset($params['from']) && $params['from'] ) {
            $select->where( 't.from >= ?', $from[2].'-'.$from[1].'-'.$from[0] .' 00:00:00');
            $select->where( 't.from <= ?', $to[2].'-'.$to[1].'-'.$to[0] . ' 23:59:59');
        }

        if ( isset($params['key_account']) and $params['key_account']==1 )
            $select->where( '   s.`name` like "TGDĐ%"
                                or s.`name` like "VTA%"
                                or s.`name` like "Viettel%"
            ', null );
        elseif ( isset($params['key_account']) and $params['key_account']==-1 )
            $select->where( '   not (s.`name` like "TGDĐ%"
                                or s.`name` like "VTA%"
                                or s.`name` like "Viettel%")
            ', null );

        $select->where( 'r.area_id = ?', $area_id);

        return $db->fetchAll($select);
    }

    public function checkImeiDealer($imei,$store){
        // kiểm tra store
        $QStore = new Application_Model_Store();
        $where = $QStore->getAdapter()->quoteInto('id = ?', intval($store));
        $store_check = $QStore->fetchRow($where);

        if (!$store_check || !isset($store_check['d_id'])) return false;

        // dealer của store
        $QDistributor = new Application_Model_Distributor();
        $where = $QDistributor->getAdapter()->quoteInto('id = ?', intval($store_check['d_id']));
        $distributor_check = $QDistributor->fetchRow($where);

        $ka_list = array(2316, 2317, 2319, 2320, 2321, 2322, 2323, 2324, 2325,
            7535, 8349, 8350, 8351, 8422, 8571, 698, 707, 708, 709, 710, 711, 794, 2071,
            2896, 5109, 6334, 7418, 7419, 1277, 823, 824, 1351, 693, 95
        );

        //7534 Deal SM

        $Viettel_list = array(2318, 10007);
        $DealSM_list = array(2090);
        $AEON = array(7534);

        if (!$distributor_check) return false;

        // check theo imei
        $QImei = new Application_Model_WebImei();
        $where = $QImei->getAdapter()->quoteInto('imei_sn = ?', $imei);
        $imei_check = $QImei->fetchRow($where);

        if (!$imei_check || !isset($imei_check['distributor_id']))  return false;

        // dealer mà imei xuất tới
        $where = $QDistributor->getAdapter()->quoteInto('id = ?', $imei_check['distributor_id']);
        $distributor_imei = $QDistributor->fetchRow($where);

        if (!$distributor_imei) return false;

        ///////////////////////////////////////////////////////
        // không cho báo cáo imei xuất nội bộ (dealer internal)
        if ( (isset($distributor_imei['is_internal']) && $distributor_imei['is_internal']) 
        && !in_array($distributor_check['id'], unserialize(IMEI_INTERNAL)) )return false;
 
        ///////////////////////////////////////////////////////
        // kiểm tra dealer của store, hoặc dealer cha của nó thuộc list KA (load hàng từ Viettel)
        // nếu imei này có dealer con và dealer cha đều không phải Viettel
        // và imei cũng éo xuất trực tiếp cho dealer của cửa hàng đó thì false
        if (in_array($store_check['d_id'], $ka_list) || in_array($distributor_check['parent'], $ka_list)) {
            if (!in_array($distributor_imei['id'], $Viettel_list)
                && !in_array($distributor_imei['parent'], $Viettel_list)
                && !$this->imeiBelongToDealer($distributor_imei, $distributor_check)
            ) return false;

            return true;
        }

        ///////////////////////////////////////////////////////
        // check cho dealer con của viettel
        // nếu imei này có dealer con và dealer cha đều không phải Viettel
        // và imei đó cũng éo xuất cho dealer của cửa hàng thì false
        if (in_array($store_check['d_id'], $Viettel_list) || in_array($distributor_check['parent'], $Viettel_list)) {
            if (!in_array($distributor_imei['id'], $Viettel_list)
                && !in_array($distributor_imei['parent'], $Viettel_list)
                && !$this->imeiBelongToDealer($distributor_imei, $distributor_check)
            ) return false;

            return true;
        }

        ///////////////////////////////////////////////////////
        // EAON Mall_KA
        // nếu imei này có dealer con và dealer cha đều không phải Deal Smartphone (Q5 - HCM)
        // và imei đó cũng éo xuất cho dealer của cửa hàng thì false
        if (in_array($store_check['d_id'], $AEON) || in_array($distributor_check['parent'], $AEON)) {
            if (!in_array($distributor_imei['id'], $DealSM_list)
                && !in_array($distributor_imei['parent'], $DealSM_list)
                && !$this->imeiBelongToDealer($distributor_imei, $distributor_check)
            ) return false;

            return true;
        }

        ///////////////////////////////////////////////////////
        //  dealer thường
        if (!$this->imeiBelongToDealer($distributor_imei, $distributor_check)) return false;

        return true;
    }

    private function imeiBelongToDealer($imei_dealer, $store_dealer) {
        if (!isset($imei_dealer['id'])) return false;
        if (!isset($store_dealer['id'])) return false;
        if (!isset($imei_dealer['parent'])) return false;
        if (!isset($store_dealer['parent'])) return false;
        $child_dealer = $parent_dealer = false;

        // nếu dealer của IMEI = dealer của của hàng hoặc dealer cha cửa hàng
        $child_dealer = in_array(
            $imei_dealer['id'],
            array($store_dealer['id'], $store_dealer['parent'])
        );

        // nếu dealer CHA của IMEI = dealer của của hàng hoặc dealer cha cửa hàng
        $parent_dealer = in_array($imei_dealer['parent'], array($store_dealer['id'], $store_dealer['parent'] ))
            && $imei_dealer['parent'] <> 0;

        // một trong hai điều trên xảy ra là imei này đúng của cửa hàng đó
        return $child_dealer || $parent_dealer;
    }

    /**
     * @param  [type] $page   [description]
     * @param  [type] $limit  [description]
     * @param  [type] &$total [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    function report_inventory_by_dealer($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();

        $con=mysqli_connect($config['resources']['db_tunnel']['params']['host'],$config['resources']['db_tunnel']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db_tunnel']['params']['password']),$config['resources']['db_tunnel']['params']['dbname'], $config['resources']['db_tunnel']['params']['port']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        /* change character set to utf8 */
        if (!$con->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $con->error);
        }

        $centerDbName = $config['resources']['db']['params']['dbname'];
        $biDbName = $config['resources']['db_tunnel']['params']['dbname'];
        $whDbName = $config['resources']['db_tunnel']['params']['dbnamewh'];

        $countFoundRows = true;
        if (isset($params['export']) and $params['export'])
            $countFoundRows = false;

        $where = ' WHERE 1=1
            AND tmp1.parent = 0
            AND tmp1.del = 0
            ';
        $SQL_CALC_FOUND_ROWS = $sLimit = '';

        if ($countFoundRows){
            $SQL_CALC_FOUND_ROWS = ' SQL_CALC_FOUND_ROWS ';
            if ($limit){
                $offset = $limit*($page-1);
                $sLimit .= ' LIMIT '.$offset.', '.$limit.' ';
            }
        }

        if (isset($params['asm']) && intval($params['asm']) > 0) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();

            if (count($list_regions))
                $where .= ' AND '.$db->quoteInto('tmp1.district IN (?)', $list_regions);
            else
                $where .= ' AND 1=0 ';
        }

        if (isset($params['name']) and $params['name']){
            $where .= ' AND '.$db->quoteInto('tmp1.title LIKE ?', '%'.$params['name'].'%');

        }

        if (isset($params['area']) and $params['area']){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $districtsByArea = $QRegionalMarket->get_district_by_area_cache($params['area']);
            if ($districtsByArea){
                $arrDistricts = array();
                foreach ($districtsByArea as $id=>$val)
                    $arrDistricts[] = $val;

                $where .= ' AND '.$db->quoteInto('tmp1.district IN (?)', $arrDistricts);

            } else
                $where .= ' AND 1=0 ';

        }

        if (isset($params['regional_market']) and $params['regional_market']){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $districtsByProvince = $QRegionalMarket->get_district_by_province_cache($params['regional_market']);
            if ($districtsByProvince){
                $arrDistricts = array();
                foreach ($districtsByProvince as $id=>$val)
                    $arrDistricts[] = $id;

                $where .= ' AND '.$db->quoteInto('tmp1.district IN (?)', $arrDistricts);

            } else
                $where .= ' AND 1=0 ';
        }

        if (isset($params['district']) and $params['district']){
            $where .= ' AND '.$db->quoteInto('tmp1.district = ?', $params['district']);
        }

        $sql = '
            select '.$SQL_CALC_FOUND_ROWS.' tmp1.id,
                tmp1.title, tmp1.parent, tmp1.product_id, tmp1.color,
                SUM((IFNULL(tmp1.sum_total_activated,0) + IFNULL(tmp2.sum_total_activated,0))) as sum_sum1 ,
                SUM((IFNULL(tmp1.sum_total_sell_in,0) + IFNULL(tmp2.sum_total_sell_in,0))) as sum_sum2

            from
            (
                SELECT
                    d.id, d.district, d.title, d.parent, d.del, dsi.product_id, dsi.color, SUM(dsi.total_activated) as sum_total_activated, SUM(dsi.total_sell_in) as sum_total_sell_in
               FROM
                    '.$whDbName.'.distributor d
               LEFT JOIN
                    '.$biDbName.'.dealer_sell_in dsi
               ON
                    dsi.dealer_id = d.id
               WHERE
                    d.del = 0
               GROUP BY
                    d.id

            ) as tmp1

            left join (
                SELECT
                    d.id, d.title, d.parent, d.del, dsi.product_id, dsi.color, SUM(dsi.total_activated) as sum_total_activated, SUM(dsi.total_sell_in) as sum_total_sell_in
               FROM
                    '.$whDbName.'.distributor d
               LEFT JOIN
                    '.$biDbName.'.dealer_sell_in dsi
               ON
                    dsi.dealer_id = d.id
               WHERE
                    d.del = 0
               GROUP BY
                    d.id
            ) tmp2
            on tmp2.parent = tmp1.id


            '.$where.'
            group by tmp1.id
            '.$sLimit.'
        ';

        if (isset($params['export']) and $params['export'])
            return $sql;

        $resultSql = mysqli_query($con,$sql);

        $result = array();
        while($row = mysqli_fetch_array($resultSql))
            $result[] = $row;

        if ($countFoundRows){
            $totalSql = mysqli_query($con,'select FOUND_ROWS()');
            $totalResult = mysqli_fetch_row($totalSql);
            $total = isset($totalResult[0]) ? $totalResult[0] : 0;
        }

        return $result;
    }

    function report_inventory_by_dealer_detail($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();

        $con=mysqli_connect($config['resources']['db_tunnel']['params']['host'],$config['resources']['db_tunnel']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db_tunnel']['params']['password']),$config['resources']['db_tunnel']['params']['dbname'], $config['resources']['db_tunnel']['params']['port']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        /* change character set to utf8 */
        if (!$con->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $con->error);
        }

        $biDbName = $config['resources']['db_tunnel']['params']['dbname'];
        $whDbName = $config['resources']['db_tunnel']['params']['dbnamewh'];

        $countFoundRows = true;
        if (isset($params['export']) and $params['export'])
            $countFoundRows = false;

        $where = ' WHERE 1=1
            AND d.del = 0
            ';
        $SQL_CALC_FOUND_ROWS = $sLimit = '';

        if ($countFoundRows){
            $SQL_CALC_FOUND_ROWS = ' SQL_CALC_FOUND_ROWS ';
            if ($limit){
                $offset = $limit*($page-1);
                $sLimit .= ' LIMIT '.$offset.', '.$limit.' ';
            }
        }

        if (isset($params['asm']) && intval($params['asm']) > 0) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();

            if (count($list_regions))
                $where .= ' AND '.$db->quoteInto('d.district IN (?)', $list_regions);
            else
                $where .= ' AND 1=0 ';
        }

        if (isset($params['name']) and $params['name']){
            $where .= ' AND '.$db->quoteInto('d.title LIKE ?', '%'.$params['name'].'%');

        }

        if (isset($params['area']) and $params['area']){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $districtsByArea = $QRegionalMarket->get_district_by_area_cache($params['area']);
            if ($districtsByArea){
                $arrDistricts = array();
                foreach ($districtsByArea as $id=>$val)
                    $arrDistricts[] = $val;

                $where .= ' AND '.$db->quoteInto('d.district IN (?)', $arrDistricts);

            } else
                $where .= ' AND 1=0 ';

        }

        if (isset($params['regional_market']) and $params['regional_market']){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $districtsByProvince = $QRegionalMarket->get_district_by_province_cache($params['regional_market']);
            if ($districtsByProvince){
                $arrDistricts = array();
                foreach ($districtsByProvince as $id=>$val)
                    $arrDistricts[] = $id;

                $where .= ' AND '.$db->quoteInto('d.district IN (?)', $arrDistricts);

            } else
                $where .= ' AND 1=0 ';
        }

        if (isset($params['district']) and $params['district']){
            $where .= ' AND '.$db->quoteInto('d.district = ?', $params['district']);
        }

        if (isset($params['product_id']) and $params['product_id']){
            $where .= ' AND '.$db->quoteInto('dsi.product_id = ?', $params['product_id']);
        }

        if (isset($params['color']) and $params['color']){
            $where .= ' AND '.$db->quoteInto('dsi.color = ?', $params['color']);
        }

        if (isset($params['dealer_id']) and $params['dealer_id']){
            $where .= ' AND ( '.$db->quoteInto('d.id = ?', $params['dealer_id']) . ' OR ' . $db->quoteInto('d.parent = ?', $params['dealer_id']) . ' ) ';
        }

        $sql = '
                SELECT
                    '.$SQL_CALC_FOUND_ROWS.' d.id, d.district, d.title, d.parent, d.del, dsi.product_id, dsi.color, SUM(dsi.total_activated) as sum_total_activated, SUM(dsi.total_sell_in) as sum_total_sell_in
               FROM
                    '.$whDbName.'.distributor d
               LEFT JOIN
                    '.$biDbName.'.dealer_sell_in dsi
               ON
                    dsi.dealer_id = d.id
               '.$where.'
               GROUP BY
                    d.id, product_id, color
               '.$sLimit.'

        ';

        if (isset($params['export']) and $params['export'])
            return $sql;

        $resultSql = mysqli_query($con,$sql);

        $result = array();
        while($row = mysqli_fetch_array($resultSql))
            $result[] = $row;

        if ($countFoundRows){
            $totalSql = mysqli_query($con,'select FOUND_ROWS()');
            $totalResult = mysqli_fetch_row($totalSql);
            $total = isset($totalResult[0]) ? $totalResult[0] : 0;
        }

        return $result;
    }

    /**
     * @param  [type] $page   [description]
     * @param  [type] $limit  [description]
     * @param  [type] &$total [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    function report_loyalty_plan($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();

        $dbTunnel = $config['resources']['db_tunnel']['params'];

        $con=mysqli_connect($dbTunnel['host'],$dbTunnel['username'],My_HiddenDB::decryptDb($dbTunnel['password']),$dbTunnel['dbname'], $dbTunnel['port']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        /* change character set to utf8 */
        if (!$con->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $con->error);
        }

        $biDbName = $dbTunnel['dbname'];
        $centerDbName = $dbTunnel['dbnamecenter'];
        $whDbName = $dbTunnel['dbnamewh'];

        $countFoundRows = true;
        if (isset($params['export']) and $params['export'])
            $countFoundRows = false;

        $where = ' WHERE 1=1
            ';
        $SQL_CALC_FOUND_ROWS = $sLimit = '';

        if ($countFoundRows) {
            $SQL_CALC_FOUND_ROWS = ' SQL_CALC_FOUND_ROWS ';
            if ($limit) {
                $offset = $limit*($page-1);
                $sLimit .= ' LIMIT '.$offset.', '.$limit.' ';
            }
        }

        if (isset($params['asm']) && intval($params['asm']) > 0) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();

            if (count($list_regions))
                $where .= ' AND '.$db->quoteInto('d.district IN (?)', $list_regions);
            else
                $where .= ' AND 1=0 ';
        }

        if (isset($params['name']) and $params['name'])
            $where .= ' AND '.$db->quoteInto('d.`title` LIKE ?', '%'.$params['name'].'%');

        if (isset($params['loyalty_plan_id']) and $params['loyalty_plan_id'])
            $where .= ' AND '.$db->quoteInto('dl.`loyalty_plan_id` IN (?)', $params['loyalty_plan_id']);

        if (isset($params['from_date']) and $params['from_date']) {
            list($day, $month, $year) = explode('/', $params['from_date']);
            $from_date = $year.'-'.$month.'-'.$day;
            $where .= ' AND '.$db->quoteInto('dlpr.`date` >= ?', $from_date);
        }

        if (isset($params['to_date']) and $params['to_date']) {
            list($day, $month, $year) = explode('/', $params['to_date']);
            $to_date = $year.'-'.$month.'-'.$day;
            $where .= ' AND '.$db->quoteInto('dlpr.`date` <= ?', $to_date);
        }

        if (isset($params['area']) and $params['area']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $districtsByArea = $QRegionalMarket->get_district_by_area_cache($params['area']);
            if ($districtsByArea) {
                $arrDistricts = array();
                foreach ($districtsByArea as $id => $val)
                    $arrDistricts[] = $val;

                $where .= ' AND '.$db->quoteInto('d.`district` IN (?)', $arrDistricts);

            } else
                $where .= ' AND 1=0 ';

        }

        if (isset($params['regional_market']) and $params['regional_market']){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $districtsByProvince = $QRegionalMarket->get_district_by_province_cache($params['regional_market']);
            if ($districtsByProvince) {
                $arrDistricts = array();
                foreach ($districtsByProvince as $id => $val)
                    $arrDistricts[] = $id;

                $where .= ' AND '.$db->quoteInto('d.`district` IN (?)', $arrDistricts);

            } else
                $where .= ' AND 1=0 ';
        }

        if (isset($params['district']) and $params['district']) {
            $where .= ' AND '.$db->quoteInto('d.`district` = ?', $params['district']);
        }

        $sql = '

            SELECT '.$SQL_CALC_FOUND_ROWS.'
                B.id, B.district, B.title, B.loyalty_plan_id,
                SUM(B.total_result_sell_in) as total_result_sell_in,
                SUM(B.total_result_sell_out) as total_result_sell_out,
                SUM(B.total_sell_out) as total_sell_out
            FROM (
            SELECT
                 d.`id`, d.`district`, d.`title`,
                 dl.`loyalty_plan_id`
                , SUM(dlpr.`result_sell_in`) as total_result_sell_in
                , SUM(dlpr.`result_sell_out`) as total_result_sell_out
                , SUM(dlpr.`sell_out`) as total_sell_out
            FROM
                `'.$whDbName.'`.`distributor` d
                LEFT JOIN
                    `'.$centerDbName.'`.`dealer_loyalty` dl
                ON
                    dl.`dealer_id` = d.`id`
                LEFT JOIN
                    `'.$biDbName.'`.`dealer_loyalty_plan_result` dlpr
                ON
                    dl.`dealer_id` = dlpr.`dealer_id`
            '.$where.'
                AND (d.`del` = 0 OR d.`del` IS NULL)
                AND (d.is_ka IS NULL OR d.is_ka = 0)
                AND (d.is_internal IS NULL OR d.is_internal = 0)
                AND (d.parent = 0 OR d.parent IS NULL)
            GROUP BY
                d.`id`
            UNION
            SELECT
                d.`parent` as id, d.`district`, d.`title`,
                 dl.`loyalty_plan_id`
                , SUM(dlpr.`result_sell_in`) as total_result_sell_in
                , SUM(dlpr.`result_sell_out`) as total_result_sell_out
                , SUM(dlpr.`sell_out`) as total_sell_out
            FROM
                `'.$whDbName.'`.`distributor` d
                LEFT JOIN
                    `'.$centerDbName.'`.`dealer_loyalty` dl
                ON
                    dl.`dealer_id` = d.`id`
                LEFT JOIN
                    `'.$biDbName.'`.`dealer_loyalty_plan_result` dlpr
                ON
                    dl.`dealer_id` = dlpr.`dealer_id`
            '.$where.'
                AND (d.`del` = 0 OR d.`del` IS NULL)
                AND (d.is_ka IS NULL OR d.is_ka = 0)
                AND (d.is_internal IS NULL OR d.is_internal = 0)
                AND (d.parent <> 0 AND d.parent IS NOT NULL)
            GROUP BY
                d.`parent`
            ) AS B
            GROUP BY B.id

            '.$sLimit.'
        ';

        if (isset($params['export']) and $params['export'])
            return $sql;
        
        if($_GET['dev'] == 1){
            echo $sql;exit;
        }
        
        $resultSql = mysqli_query($con,$sql);

        $result = array();
        while ($row = mysqli_fetch_array($resultSql))
            $result[] = $row;

        if ($countFoundRows) {
            $totalSql = mysqli_query($con,'select FOUND_ROWS()');
            $totalResult = mysqli_fetch_row($totalSql);
            $total = isset($totalResult[0]) ? $totalResult[0] : 0;
        }

        return $result;
    }

    function getListRule($params){
        $db = Zend_Registry::get('db');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();

        $dbTunnel = $config['resources']['db_tunnel']['params'];

        $con=mysqli_connect($dbTunnel['host'],$dbTunnel['username'],My_HiddenDB::decryptDb($dbTunnel['password']),$dbTunnel['dbname'], $dbTunnel['port']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        /* change character set to utf8 */
        if (!$con->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $con->error);
        }

        $biDbName = $dbTunnel['dbname'];
        $centerDbName = $dbTunnel['dbnamecenter'];
        $whDbName = $dbTunnel['dbnamewh'];



        $where = ' WHERE 1=1
            ';

        if (isset($params['from_date']) and $params['from_date'])
            $where .= ' AND '.$db->quoteInto('lpr.from_date <= ?', $params['from_date']);

        if (isset($params['to_date']) and $params['to_date'])
            $where .= ' AND '.$db->quoteInto('lpr.to_date >= ? OR lpr.to_date IS NULL', $params['to_date']);

        $sql = '

            SELECT
                lp.*,
                lpr.loyalty_plan_id, lpr.from_date, lpr.to_date, lpr.type,
                lpr.product_id, lpr.color, lpr.value
            FROM
                '.$centerDbName.'.loyalty_plan lp
                LEFT JOIN
                    '.$centerDbName.'.loyalty_plan_rule lpr
                ON
                    lp.id = lpr.loyalty_plan_id
            '. $where .'
            ORDER BY
                lpr.type
        ';


        $resultSql = mysqli_query($con,$sql);

        $result = array();
        while($row = mysqli_fetch_array($resultSql)){
            $result[] = $row;
        }


        return $result;
    }

    function getResultByProductAndPlan($params, $con){
        $db = Zend_Registry::get('db');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();

        $dbTunnel = $config['resources']['db_tunnel']['params'];

        $biDbName = $dbTunnel['dbname'];
        $centerDbName = $dbTunnel['dbnamecenter'];
        $whDbName = $dbTunnel['dbnamewh'];



        $where = ' WHERE 1=1
            ';

        if (isset($params['from_date']) and $params['from_date'])
            $where .= ' AND '.$db->quoteInto('dlpr.date >= ?', $params['from_date']);

        if (isset($params['to_date']) and $params['to_date'])
            $where .= ' AND '.$db->quoteInto('dlpr.date <= ?', $params['to_date']);

        if (isset($params['dealer_id']) and $params['dealer_id'])
            $where .= ' AND '.$db->quoteInto('dlpr.dealer_id = ?', $params['dealer_id']);

        if (isset($params['product_id']) and $params['product_id'])
            $where .= ' AND '.$db->quoteInto('dlpr.product_id = ?', $params['product_id']);

        if (isset($params['color']) and $params['color'])
            $where .= ' AND '.$db->quoteInto('dlpr.color = ?', $params['color']);


        $sql = '

            SELECT
                SUM(dlpr.`sell_in`) AS total_sell_in, SUM(dlpr.`result_sell_in`) AS total_result_sell_in, SUM(dlpr.`sell_out`) AS total_sell_out, SUM(dlpr.`result_sell_out`) AS total_result_sell_out
            FROM
                `'.$biDbName.'`.`dealer_loyalty_plan_result` dlpr

            '. $where .'
        ';

        $resultSql = mysqli_query($con,$sql);

        $result = array();
        while($row = mysqli_fetch_array($resultSql)){
            $result = $row;
        }


        return $result;
    }

    public function getDataImeiKa($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['ts' => 'timing_sale'], [
                'ts.imei',
                'model' => 'g.desc',
                'color' => 'gc.name',
                'store_name' => 's.name',
                'timing_date' => "DATE_FORMAT(t.`from`, '%Y-%m-%d %H:%i:%s')",
                'activated_at' => "DATE_FORMAT(ia.activated_date, '%Y-%m-%d %H:%i:%s')",
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'staff_email' => 'st.email',
                'staff_code' => 'st.code'
            ])
            ->join(['t' => 'timing'], 'ts.timing_id = t.id', [])
            ->joinLeft(['s' => 'store'], 't.store = s.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'g.id = ts.product_id', [])
            ->joinLeft(['gc' => WAREHOUSE_DB . '.good_color'], 'ts.model_id = gc.id', [])
            ->joinLeft(['st' => 'staff'], 't.staff_id = st.id', [])
            ->join(['ia' => WAREHOUSE_DB . '.imei'], 'ts.imei = ia.imei_sn', [])
            ->where('t.from >= ? ', $params['from_date'])
            ->where('t.from <= ?', $params['to_date'])
            ->where('d.id IN (?) OR d.parent IN (?)', $params['distributor_id']);

        $data = $db->fetchAll($select);

        return $data;
                             
    }

    public function isSalesOnlineImei($imei)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => WAREHOUSE_DB . '.imei'], [
                'i.imei_sn'
            ])
            ->join(['d' => WAREHOUSE_DB . '.distributor'], 'i.distributor_id = d.id AND (d.online_sale = 1 OR d.is_internal = 1)', [])
            ->where('i.imei_sn = ?', $imei);

        $result = $db->fetchOne($select);

        return $result ? true : false;

    }
}
