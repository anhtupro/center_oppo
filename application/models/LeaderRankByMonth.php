<?php

class Application_Model_LeaderRankByMonth extends Zend_Db_Table_Abstract
{
    protected $_name = 'leader_rank_by_month';

    public function getList($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['r' => 'leader_rank_by_month'], [
                'r.staff_name',
                'staff_title' => 'r.title',
                'staff_from_date' => 'r.from_date',
                'staff_to_date' => 'r.to_date',
                'region_name' => 'r.province',
                'r.province_share',
                'r.area_name',
                'area_shared' => 'r.area_share',
                'total_quantity' => 'r.unit',
                'total_activated' => 'r.unit_activated',
                'total_value' => 'r.value_80',
                'r.point',
                'sum_total_value' => 'r.total_value'
            ])
            ->where('r.month = ?', $params['month'])
            ->where('r.year = ?', $params['year'])
            ->order('r.point DESC');

        if ($params['area']) {
            $select->where('r.area_id = ?', $params['area']);
        }

        if ($params['list_area']) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getTotalUnit($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['r' => 'leader_rank_by_month'], [
                'total_unit' => "SUM(r.unit)",
                'total_unit_activated' => "SUM(r.unit_activated)"
            ])
            ->where('r.month = ?', $params['month'])
            ->where('r.year = ?', $params['year']);

        if ($params['area']) {
            $select->where('r.area_id = ?', $params['area']);
        }

        if ($params['list_area']) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getFinalData($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['l' => 'leader_rank_by_month'], [
                'l.*'
            ])
            ->where('l.month = ?', $params['month'])
            ->where('l.year = ?', $params['year'])
        ->order('l.area_name');

        if ($params['staff_id'] > 0) {
            $select->where('l.staff_id = ?', $params['staff_id']);
        }
        if ($params['list_area']) {
            $select->where('l.area_id IN (?)', $params['list_area']);
        }

        $result = $db->fetchAll($select);

        return $result;

    }
}