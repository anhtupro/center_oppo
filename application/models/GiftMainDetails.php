<?php
class Application_Model_GiftMainDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'gift_main_details';

    public function getInventory($id){
        $db         = Zend_Registry::get("db");
        $select     = $db->select();
        $arrCols    = array(
            "g.name", "g.desc", "g.file",
            "g.id", "g.inventory"
        );
        $select->from(array('p' => $this->_name), $arrCols);
        $select->joinLeft(array('g' => 'gift'), 'g.id = p.gift_id', array());
        $select->where('g.del = ?', 0);
        $select->where('p.main_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
}