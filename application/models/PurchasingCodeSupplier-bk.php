<?php
class Application_Model_PurchasingCodeSupplier extends Zend_Db_Table_Abstract
{
	protected $_name = 'code_supplier';

	function fetchCodeSupplier($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name), array(new Zend_Db_Expr
                ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->join(array('a' => 'supplier'), 'p.supplier_id = a.id', array('supplier' =>
                'a.title'));
        $select->join(array('b' => 'pmodel_code'), 'p.code_id = b.id', array('code' =>
                'b.code'));

        if (isset($params['supplier']) and $params['supplier'])
            $select->where('a.title LIKE ?', '%' . $params['supplier'] . '%');

        if (isset($params['code']) and $params['code'])
            $select->where('b.code LIKE ?', '%' . $params['code'] . '%');

        if (isset($params['price']) && $params['price'])
            $select->where('p.price = ?', $params['price']);

        // if (isset($params['default']) && $params['default'])
        //     $select->where('p.default = ?', $params['default']);

        $select->where('p.default = ?',1);
        $select->group('p.code_id');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    function fetchRowCodeSupplier($id){
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name), array(new Zend_Db_Expr
                ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->join(array('a' => 'supplier'), 'p.supplier_id = a.id', array('supplier' =>
                'a.title'));
        $select->join(array('b' => 'pmodel_code'), 'p.code_id = b.id', array('code' =>
                'b.code'));

        $select->where('p.id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
    function fetchRowCode($code){
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name), array(new Zend_Db_Expr
                ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->join(array('a' => 'supplier'), 'p.supplier_id = a.id', array('supplier' =>
                'a.title'));
        $select->join(array('b' => 'pmodel_code'), 'p.code_id = b.id', array('code' =>
                'b.code'));

        $select->where('b.code LIKE ?', '%' . $code . '%');

        $result = $db->fetchAll($select);
        return $result;
    }

}