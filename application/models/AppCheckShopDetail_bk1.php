<?php
class Application_Model_AppCheckshopDetail extends Zend_Db_Table_Abstract
{
	protected $_name = 'app_checkshop_detail';

	protected $_schema = DATABASE_TRADE;

	function getDataCheckshop($params){
    	if( empty($params['store_id']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.category_id', 
            'p.quantity', 
            'c.store_id', 
            'category_name' => 'cat.name',
            'group_bi'		=> 'cat.group_bi'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_checkshop_detail'), $arrCols);
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.app_checkshop'), 'c.id = p.checkshop_id',array());
        $select->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.category_id',array());
        
        $select->where('c.store_id = ?', $params['store_id']);
        $select->where('c.id = (SELECT MAX(id) FROM `'.DATABASE_TRADE.'`.`app_checkshop` p WHERE store_id = ?)', $params['store_id']);
        $select->where('p.quantity > 0', NULL);
       
        $result = $db->fetchAll($select);

        return $result;
    }

}