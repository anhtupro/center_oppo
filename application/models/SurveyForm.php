<?php
class Application_Model_SurveyForm extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_form';

    function fetchPagination($page, $limit, &$total, $params){
    $db = Zend_Registry::get('db');
            $select = $db->select()
                ->from(array('p'=>'survey_form'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'CONCAT(st.firstname , \' \' , st.lastname) AS staff_name'
            ))
            ->joinLeft(array('st'=>'staff'),'p.created_by = st.id',array())
            ->where('p.del = ?',0);

            if (isset($params['staff_id']) && $params['staff_id']) {
                $select->where('p.created_by = ?', $params['staff_id']);
            }

            $select->order('id desc');
            if ($limit) {
                $select->limitPage($page, $limit);
            }
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    /**
     * @return array
     */
    public function getActiveSurvey()
    {
        return $this->fetchAll(
            $this->select()
                ->from($this, ['id', 'name', 'status'])
                ->where('del = ?', 0)
                ->where('status != ?', 0)
                ->order('name')
        )->toArray();
    }
}