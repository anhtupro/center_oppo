<?php

class Application_Model_SelloutDistributorMap extends Zend_Db_Table_Abstract
{
    public function getAll()
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT * FROM `sellout_distributor_map` ";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();

        $stmt = $db = null;
        return $data;
    }
}