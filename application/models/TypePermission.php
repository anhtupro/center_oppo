<?php
class Application_Model_TypePermission extends Zend_Db_Table_Abstract
{
	protected $_name = 'type_permission';

    public function getTypeGroup()
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT tp.*
                    FROM `type_permission` tp
                    WHERE tp.id IN (1,2)";

        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }

}