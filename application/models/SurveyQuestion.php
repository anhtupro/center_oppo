<?php
class Application_Model_SurveyQuestion extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_question';

    public function random($params, $quantity, $random = false)
    {
        $db = Zend_Registry::get('db');
        $select_question = $db->select()
            ->from(array('sq' => $this->_name),
                array('question_id' => 'sq.id', 'sq.question', 'sq.response_type', 'sq.required'));

        if (isset($params['survey_id']) && $params['survey_id']) {
            if (is_numeric($params['survey_id']) && intval($params['survey_id']))
                $select_question->where('survey_id = ?', intval($params['survey_id']));

            elseif (is_array($params['survey_id']) && count($params['survey_id']))
                $select_question->where('survey_id IN (?)', $params['survey_id']);

            else
                $select_question->where('1=0');
        }

        if ($random)
            $select_question->order('RAND()');

        if ($quantity)
            $select_question->limitPage(1, $quantity);

        $select_answer = $db->select()
            ->from(array('sqo' => 'survey_question_option'),
                array('sqo.option', 'sqo.order', 'option_id' => 'sqo.id', 'sqo.survey_question_id'));

        if ($random){
            //$select_answer->order('RAND()');
        }else{
            $select_answer->order('sqo.order ASC');
        }


        $select = $db->select()
            ->from(array('sq' => $select_question), array('sq.*'))
            ->joinLeft(array('sqo' => $select_answer), 'sq.question_id=sqo.survey_question_id', array('sqo.*'));

        if ($random)
            $select->order('RAND()');
        else
            $select->order(array('question_id', 'order', 'option_id'));
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getQuestionRandom($survey_id,$params){
        try {
            $survey_id = intval($survey_id);
            if (!$survey_id || !is_numeric($survey_id) || !intval($survey_id)){
                return array('code'=>0,'question'=>"Invalid ID");
            }

            $QSurvey = new Application_Model_Survey();
            $where = $QSurvey->getAdapter()->quoteInto('id = ?', $survey_id);
            $survey = $QSurvey->fetchRow($where);

            if (!$survey){
                return array('code'=>-1,'question'=>"Invalid survey");
            }

            // load questions and answers
            $result = $this->random($params, 2, true);
            $questions = array();

            if (!$result){
                return array('code'=>-1,'question'=>"No question in this survey");
            }

            foreach ($result as $key => $value) {
                if (!isset($questions[ $value['question_id'] ])) {
                    $questions[ $value['question_id'] ] = array(
                        'content' => $value['question'],
                        'options' => array(),
                        'response_type' => $value['response_type'],
                    );
                }

                $questions[ $value['question_id'] ]['options'][ $value['option_id'] ] = $value['option'];
            }

            return array('code'=>0,'question'=>$questions);
        } catch (Exception $e) {
            return array('code'=>-1,'message'=>$e->getMessage());
        }
    }


}
