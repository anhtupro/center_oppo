<?php
class Application_Model_Devices extends Zend_Db_Table_Abstract{
    protected $_name = 'good' ;
    protected $_schema = WAREHOUSE_DB;
    protected $_primary = 'id';
   
    //Get limit data devices
    public function getLimitDevices($curPage,$limitItems){
        $db = Zend_Registry::get('db');
        $select  = $db->select()
                      ->from(WAREHOUSE_DB.'.'.$this->_name, array('id','name','desc','cat_id','price_5 AS price_for_fpt'))
                      ->limitPage($curPage,$limitItems)
                      ->order('id ASC');
        $result = $db->fetchAll($select);
        return $result;
    }
    // Get total devices 
    public function totalDevices(){
        $db = Zend_Registry::Get('db');
        $select = $db->select()
                     ->from(WAREHOUSE_DB.'.good',array('COUNT(id) as totalDevices'));
        $result = $db->fetchOne($select);
        return $result;
    }
    //Get ID - DESC all devices devices bonus_id not null
    public function getAllDevices(){
        $db = Zend_Registry::get('db');
        $select = $db->select()
                     ->from(WAREHOUSE_DB.'.'.$this->_name.' as g',['id','desc'])
                     ->join(DATABASE_CENTER.'.bonus as b','g.id = b.product_id');
        $result = $db->fetchAll($select);
        return $result;
    }
    //Insert devices 
    public function insertDevices($data){
        $row = $this->createRow($data);
        $row->save();
    }
    public function getDeviceById($id){
        $db = Zend_Registry::get('db');
        $select = $db->select()
                     ->from( WAREHOUSE_DB.'.'.$this->_name, array('name','desc','cat_id','price_5 AS price_for_fpt'))
                     ->where('id = ?',$id,INTEGER);
        $result = $db->fetchAll($select);
        return $result;
    }
    public function getDeviceByCode($code){
        $db = Zend_Registry::get('db');
        $select = $db->select()
                     ->from( WAREHOUSE_DB.'.'.$this->_name, array('id','desc','cat_id','price_5 AS price_for_fpt'))
                     ->where('name = ?',$code,STRING); 
        $result = $db->fetchRow($select);
        return $result;
    }
    public function updateDeviceById($data,$id){
        $db = Zend_Registry::get('db');
        $where = 'id ='.$id;
        $db->update(WAREHOUSE_DB.'.'.$this->_name,$data,$where);
    }
    //check bonus each device
    public function selectBonusByIdDevice($id,$select = []){
        $db = Zend_Registry::get('db');
        $select = $db->select()
                     ->from($this->_schema.'.'.$this->_name.' as g',['desc'])
                     ->join(DATABASE_CENTER.'.bonus as b','g.id = b.product_id',$select)
                     ->where('b.product_id = ? ',$id,STRING);
        $result = $db->fetchRow($select);
        return $result;
    }

    
}