<?php
class Application_Model_AppOrderDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_order_details';
    protected $_schema = DATABASE_TRADE;

    function getDataDetails($params)
    {
    	if( empty($params['order_id']) AND empty($params['area_id']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'id'                => 'p.id',
            'order_details_id'  => 'od.id',
            'order_id'          => "od.order_id",
            'store_id'          => "od.store_id",
            'category_id'       => "od.category_id",
            'quantity'          => "od.quantity",
            'quantity_limit'    => "od.quantity_limit",
            'url'	            => "oi.url",
            'status'    	    => "od.status",
            'store_name'        => "CONCAT(SUBSTRING_INDEX(s.name, ' ', 2) ,' - ', r.name, ' - ', r2.name)",
            'total_scan'        => 'COUNT(DISTINCT i.imei_sn)',
            'area_id'           => 'IFNULL(c.area_id, r2.area_id)'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_order'), $arrCols);
        $select->joinLeft(array('od'=> DATABASE_TRADE.'.app_order_details'), 'od.order_id = p.id',array());
        $select->joinLeft(array('s'=> 'store'), 's.id = od.store_id',array());
        $select->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.store_id',array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.district', array());
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = s.regional_market', array());

        $select->joinLeft(array('oi'=> DATABASE_TRADE.'.app_order_image'), 'oi.order_details_id = od.id',array());
        $select->joinLeft(array('i'=> DATABASE_TRADE.'.app_store_in'), 'i.store_id = od.store_id AND i.campaign_id = p.campaign_id',array());
        
        if(isset($params['order_id']) and $params['order_id']){
            $select->where('p.id = ?', $params['order_id']);
        }

        if(isset($params['category_id']) and $params['category_id']){
            $select->where('od.category_id = ?', $params['category_id']);
        }

        $select->where('p.campaign_id = ?', $params['campaign_id']);

        if(isset($params['area_id']) and $params['area_id']){
            $select->where('IFNULL(c.area_id, r2.area_id) IN (?)', $params['area_id']);
        }

        $select->group('od.store_id');

        $result = $db->fetchAll($select);

        return $result;
    }

    function getDetailsArea($params){

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'area_id'       => 'IFNULL(c.area_id, r2.area_id)',
            'category_id'   => "p.category_id",
            'quantity'      => "SUM(p.quantity)",
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_order_details'), $arrCols);
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_order'), 'o.id = p.order_id',array());
        $select->joinLeft(array('s'=> 'store'), 's.id = p.store_id',array());
        $select->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.store_id',array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.district', array());
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = s.regional_market', array());

        $select->group(array('IFNULL(c.area_id, r2.area_id)', 'p.category_id'));

        $result = $db->fetchAll($select);

        return $result;

    }

    function getImeiArea($params){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.contractor_id', 
            'p.campaign_id', 
            'p.category_id', 
            'p.imei_sn', 
            'd.area_id', 
            'd.shipment_id',
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_out'), $arrCols);
        $select->joinLeft(array('d'=> DATABASE_TRADE.'.app_shipment_details'), 'p.app_shipment_details = d.id',array());

        $select->where('p.campaign_id = ?', $params['campaign_id']);
        $select->where('p.category_id = ?', $params['category_id']);
        $select->where('d.area_id IN (?)', $params['area_id']);

        $result = $db->fetchAll($select);

        return $result;
    }

    function getImeiAreaIn($params){
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.area_id', 
            'p.campaign_id', 
            'p.category_id', 
            'p.imei_sn', 
            'p.created_at', 
            'p.created_by'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_area_in'), $arrCols);

        $select->where('p.category_id = ?', $params['category_id']);
        $select->where('p.campaign_id IN (?)', $params['campaign_id']);

        $result = $db->fetchAll($select);

        return $result;
    }

    function getImeiScanIn($params){

        if(empty($params['store_id'])){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'imei_sn'          => 'p.imei_sn',
            'category_id'      => "p.category_id",
            'campaign_id'      => "p.campaign_id",
            'store_id'         => "p.store_id",
            'quantity'         => "COUNT(DISTINCT p.imei_sn)",
            'list_imei'        => "GROUP_CONCAT(p.imei_sn)",
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_store_in'), $arrCols);

        $select->where('p.category_id = ?', $params['category_id']);
        $select->where('p.campaign_id = ?', $params['campaign_id']);
        $select->where('p.store_id IN (?)', $params['store_id']);

        $select->group('p.store_id');

        $result = $db->fetchAll($select);

        return $result;

    }

    function getAreaCategoryQuantity($params)
    {
        if( empty($params['category_id']) AND empty($params['campaign_id']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'store_id'          => 'p.store_id',
            'area_id'           => 'IFNULL(c.area_id, r.area_id)',
            'category_id'       => "p.category_id",
            'quantity'          => "SUM(p.quantity)",
            'campaign_id'       => "o.campaign_id",
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_order_details'), $arrCols);
        $select->joinLeft(array('o'=> DATABASE_TRADE.'.app_order'), 'o.id = p.order_id',array());
        $select->joinLeft(array('s'=> 'store'), 's.id = p.store_id',array());
        $select->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.store_id',array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());

        if($params['category_id'] AND $params['category_id']){
            $select->where('p.category_id = ?', $params['category_id']);
        }

        if($params['campaign_id'] AND $params['campaign_id']){
            $select->where('o.campaign_id = ?', $params['campaign_id']);
        }

        $select->group(array('IFNULL(c.area_id, r.area_id)', 'p.category_id'));

        $result = $db->fetchAll($select);

        return $result;
    }


}