<?php

class Application_Model_OrderAirArea extends Zend_Db_Table_Abstract
{
    protected $_name = 'order_air_area';
    protected $_schema = DATABASE_TRADE;

    public function getDetail($stageId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.order_air_detail'], [
                       'd.category_id',
                       'd.price',
                       'a.*'
                     ])
                    ->joinLeft(['a' => DATABASE_TRADE.'.order_air_area'], 'd.id = a.order_air_detail_id', [])
                    ->where('d.order_air_stage_id = ?', $stageId)
                     ->order('d.id');;

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] [] =  $element;
         }

        return $list;
    }
}