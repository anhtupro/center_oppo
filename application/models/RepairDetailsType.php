<?php
class Application_Model_RepairDetailsType extends Zend_Db_Table_Abstract
{
    protected $_name = 'repair_details_type';
    protected $_schema = DATABASE_TRADE;

    public function getRepairDetailsPart($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.repair_part_id", 
            "p.quantity", 
            "r.title",
            'repair_details_id' => 'd.id',
            'category_name' => 'c.name'
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair_details_part'), $arrCols);
        $select->joinLeft(array('r' => DATABASE_TRADE.'.repair_part'), 'r.id = p.repair_part_id', array());
        $select->joinLeft(array('d' => DATABASE_TRADE.'.repair_details'), 'd.id = p.repair_details_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'r.category_id = c.id', array());
        $select->where('d.repair_id = ?', $params['repair_id']);

        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['repair_details_id']][] = $value;
        }
        
        return $data;
    }
    
    public function getRepairDetailsType($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.repair_details_id", 
            "p.repair_tp_id", 
            "p.repair_type_id", 
            "type_title" => "t.title", 
            "tp_title" => "tp.title" 
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair_details_type'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE.'.repair_details'), 'd.id = p.repair_details_id', array());
        $select->joinLeft(array('t' => DATABASE_TRADE.'.repair_type'), 't.id = p.repair_type_id', array());
        $select->joinLeft(array('tp' => DATABASE_TRADE.'.repair_tp'), 'tp.id = p.repair_tp_id', array());
        $select->where('d.repair_id = ?', $params['repair_id']);
         $select->where('p.del IS NULL OR p.del = 0');
        
        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['repair_details_id']][] = $value;
        }
        
        
        return $data;
    }
    
}