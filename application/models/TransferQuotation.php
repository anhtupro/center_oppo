<?php

class Application_Model_TransferQuotation extends Zend_Db_Table_Abstract
{
    protected $_name = 'transfer_quotation';
    protected $_schema = DATABASE_TRADE;

    public function getTransferDetailsQuotation($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.transfer'), ['td.id', 'tq.*'])
            ->joinLeft(array('td' => DATABASE_TRADE.'.transfer_details'), 'r.id = td.transfer_id', [])
            ->joinLeft(array('tq' => DATABASE_TRADE.'.transfer_quotation'), 'td.id = tq.transfer_details_id', [])
            ->where('r.id = ?', $params['transfer_id']);

        $select->where('tq.status = 0');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $transfer_quotation [$element ['transfer_details_id'] ] [] = $element;
            $sum_total_price_quotation [$element ['transfer_details_id'] ] += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
            'transfer_quotation' => $transfer_quotation,
            'sum_total_price_quotation' =>  $sum_total_price_quotation
        ];

        return $result;
    }

    public function getTransferDetailsQuotationLast($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.transfer'), ['td.id', 'tq.*'])
            ->joinLeft(array('td' => DATABASE_TRADE.'.transfer_details'), 'r.id = td.transfer_id', [])
            ->joinLeft(array('tq' => DATABASE_TRADE.'.transfer_quotation'), 'td.id = tq.transfer_details_id', [])
            ->where('r.id = ?', $params['transfer_id']);

        $select->where('tq.status = 2');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $transfer_quotation [$element ['transfer_details_id'] ] [] = $element;
            $sum_total_price_quotation [$element ['transfer_details_id'] ] += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
            'transfer_quotation' => $transfer_quotation,
            'sum_total_price_quotation' =>  $sum_total_price_quotation
        ];

        return $result;
    }
}