<?php
class Application_Model_GroupBasic extends Zend_Db_Table_Abstract
{
	protected $_name = 'group_basic';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');


        $select->where('p.del is null');
        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function getAllGroupBasic(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from(array('p' => $this->_name),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->where('p.del is null');
        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
    }


    function getGroupBasicByName($name, $id_str){
        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from(array('p' => $this->_name), 'p.*');

        if(isset($name) and $name)
            $select->where('p.name LIKE ?', '%'.$name.'%');

        if(isset($id_str) and $id_str)
            $select->where("find_in_set(p.id, '".$id_str."') = 0");

        $select->where('p.del is null');
        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    //export
    function getAllGroupBasicAndGroup(){
        $db = Zend_Registry::get('db');

        $col = array(
            'group_basic_id' => 'gb.id',
            'group_basic_name' => 'gb.name',
			'group_id' => 'gl2.id',
			'group_name' => 'gl2.name',
		);

        $select = $db->select()
            ->from(array('gb' => $this->_name),$col);
        $select->joinLeft(array('gm' => 'group_mapping'), 'gb.id = gm.group_basic', array());
        $select->joinLeft(array('gl2' => 'group_level2'), 'gm.group_level2 = gl2.id', array());

        $select->where('gl2.del is null');
        $select->where('gb.del is null');

        $select->order('gb.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    //for timing
    function get_cache_2(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_2');

        if ($result === false) {
           
            $where = array();
            $where[] = $this->getAdapter()->quoteInto('id NOT IN (?)', array(ADMINISTRATOR_ID, HR_EXT_ID ,HR_ID , EXPORT_ID));
            
            $data = $this->fetchAll($where);

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache_2', array(), null);
        }
        return $result;
    }
}                                                      
