<?php
class Application_Model_AppAir extends Zend_Db_Table_Abstract
{
	protected $_name = 'app_air';
	protected $_schema = DATABASE_TRADE;
    
    public function getSellOut($params)
    {
        
        $db = Zend_Registry::get('db');
        $select = $db->select();
       $select->from(array('a' => 'imei_kpi'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.imei_sn')));

       $select->where('a.store_id = ?',$params['store_id']);
       $d= date('Y-m-d');
       $date=date_create($d);
       $day=date_add($date,date_interval_create_from_date_string("-90 days"));

       // $date = DATE_ADD($d,INTERVAL -3 MONTH);
       $select->where('a.out_date >= ?',date_format($day,'Y-m-d'));        
       $result = $db->fetchAll($select);
       $sell_out=COUNT($result);
        return $sell_out;
    }
    public function GetAll(){
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('C' => DATABASE_TRADE.'.'.$this->_name));
        $result = $db->fetchAll($select);
        return $result;
    }
      
    
    public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        // sub query
        $select = $db->select();
    //DISTINCT
        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'store_name' => "s.name",
            'created_at' => "p.created_at",
            'created_by' => "p.created_by",
            'status'    => "p.status",
            'reject'    => "p.reject",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%,
            'review_cost' => "GROUP_CONCAT(DISTINCT  d.review_cost)",
            'p.remove',
            'p.remove_note',
            'date_finish' => 'rc.created_at'
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_air'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_status'), 'a.status = p.status AND a.type = 8', array());
        if(!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE.'.app_status_title'), 't.status_id = p.status AND t.type = 8', array());
        }
        $select->joinLeft(array('d' => DATABASE_TRADE.'.air_details'), 'p.id = d.air_id', array());
        $select->joinLeft(array('q' => DATABASE_TRADE.'.air_quotation'), 'd.id = q.air_details_id AND q.status = (SELECT MAX(status) FROM trade_marketing.air_quotation WHERE air_details_id = d.id AND status <> 1 AND status <> 3 AND status <> 5)', array());
        $select->joinLeft(['dis' => WAREHOUSE_DB.'.distributor'], 's.d_id = dis.id', []);
//        $select->where('p.remove IS NULL OR p.remove = 0');

        $select->joinLeft(['rc' => DATABASE_TRADE.'.air_status'], 'rc.air_id = p.id AND rc.status_id = 5', []);

        if ($params['review_cost']) {
            $select->where('d.review_cost = ?', $params['review_cost']);
        }
        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if ($params['contractor_id']) {
            $select->where('d.contractor_id = ?', $params['contractor_id']);
        }

        if ($params['contract_number']) {
            $select->where('d.contract_number = ?', $params['contract_number']);
        }

        if(!empty($params['staff_id'])){
            $select->where('p.created_by = ?', $params['staff_id']);
        }
      
        if(!empty($params['area_id']) && empty($params['area_id_search'])){

            $select->where('r.area_id IN (?)', $params['area_id']);
        }
         //---T SEARCH
        if(!empty($params['area_id_search'])){
            $select->where('r.area_id = ?', $params['area_id_search']);
        }
        
        if(!empty($params['status_id'])){
            $select->where('p.status IN (?)', $params['status_id']);
            $select->where('p.reject IS NULL OR p.reject = 0');
        }
         if(!empty($params['name_search'])){
             $select->where('s.name LIKE ?', '%'.$params['name_search'].'%');
         }

        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.created_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.created_at) <= ?', $to_date);
        }
        
        if(!empty($params['title'])){
    		$select->where('t.title = ? AND (p.reject IS NULL OR p.reject = 0)', $params['title']);
    	}

        if ($params['month']) {
            $select->where('MONTH(p.created_at) = ?', $params['month']);
        }

//        if ($params['year']) {
//            $select->where('YEAR(p.created_at) = ?', $params['year']);
//        }

        if ($params['is_ka'] == 1) {
            $select->where('dis.is_ka = ?', 1);
        }

        if (isset($params['is_ka']) AND $params['is_ka'] == 0) {
            $select->where('dis.is_ka = ?', 0);
        }


        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }

        $select->group('p.id');


        //---T END SEARCH
        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    public function getAir($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.store_id", 
            "p.created_at", 
            "p.created_by", 
            "p.status", 
            "store_name" => "s.name", 
            "fullname" => "CONCAT(staff.firstname, ' ', staff.lastname)",
            "p.reject",
            'p.remove',
            'p.remove_note',
            "remove_by" => "CONCAT(st1.firstname, ' ', st1.lastname)",
            "reject_by" => "CONCAT(st2.firstname, ' ', st2.lastname)",
            'p.reject_note'
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_air'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.created_by', array());
        $select->joinLeft(array('st1' => 'staff'), 'st1.id = p.remove_by', array());
        $select->joinLeft(array('st2' => 'staff'), 'st2.id = p.reject_by', array());
        $select->where('p.id = ?', $params['id']);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function fetchPaginationAll($page, $limit, &$total, $params){
       $db = Zend_Registry::get('db');
        $select = $db->select();
       $select->from(array('a' => DATABASE_TRADE.'.app_air'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.sn','a.created_by','a.store_id','a.created_at','a.status'))
        ->joinLeft(array('b' => 'staff'), 'a.created_by = b.id', array('created_by' => "CONCAT(b.firstname,' ',b.lastname)"))
        ->joinleft(array('d'=>DATABASE_TRADE.'.store'),'d.id = a.store_id', array ('namestore' => 'd.name'));
        $select->order ('a.id DESC');
       $select->where('c.type = ?',7);
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function getStatus($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => DATABASE_TRADE.'.app_air'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.status'))
        ->joinleft(array('c'=>DATABASE_TRADE.'.app_status'),'c.status = a.status', array ('status_id' => 'c.id','status_name'=>'c.name'));
        $select->where('a.id = ?',$id);
        $select->where('c.type = ?',7);  
        $status = $db->fetchRow($select);
        return $status;
    }
	function getLastNumber()
    {
        $db = Zend_Registry::get('db');

        $result = NULL;

        try {
            $select = $db->select();
            $select->from([ 'p' => DATABASE_TRADE.'.app_air' ], [ 'p.id' ]);

            $select->order('p.id DESC');

            $result = $db->fetchRow($select);
        }
        catch(Exception $e) {
        }

        return $result;
    }
  function getDSDeXuatUpdate($params){
       
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $select->from(array('D' => DATABASE_TRADE.'.app_air_detail'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'),'D.air_id','D.width','D.wide','D.deep','D.total','D.cate_id','D.dvt','D.contractor_id',
            'soluong'=>'D.quantity'))
        ->joinleft(array('C'=>DATABASE_TRADE.'.category'),'C.id = D.cate_id', array ('categoryname'=>'C.name'));
        $select->joinleft(array('t'=>DATABASE_TRADE.'.contructors'),'t.id = D.contractor_id', array ('contractor_id'=>'D.contractor_id'));

        $select->where('D.air_id = ?', $params['id']);
        
        $list=$db->fetchAll($select);
        return $list;
    }
    function getDSDeXuat($params){
       
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $select->from(array('D' => DATABASE_TRADE.'.app_air_detail'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'),'D.air_id','D.width','D.wide','D.deep','D.total','D.cate_id','D.dvt','D.contractor_id',
            'soluong'=>'D.quantity'))
        ->joinleft(array('C'=>DATABASE_TRADE.'.category'),'C.id = D.cate_id', array ('categoryname'=>'C.name'));
        $select->joinleft(array('t'=>DATABASE_TRADE.'.contructors'),'t.id = D.contractor_id', array ('contractor_id'=>'D.contractor_id'));

        $select->where('D.air_id = ?', $params['id']);
        $list=$db->fetchAll($select);
       // var_dump($list); exit;
        return $list;
    }
    function getDSDeXuatNhathau($params){
       
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            new Zend_Db_Expr('d.id'),
            'd.air_id',
            'd.width',
            'd.wide',
            'd.deep',
            'd.total',
            'd.cate_id',
            'd.dvt',
            'd.contractor_id',
            'categoryname' =>  'cat.name',
            'soluong' => 'p.quantity');

        $select->from(array('p' => DATABASE_TRADE.'.app_air_contructor'), $col)
        ->joinleft(array('d'=>DATABASE_TRADE.'.app_air_detail'),'d.id = p.air_detail_id', array ())
        ->joinleft(array('cat'=>DATABASE_TRADE.'.category'),'cat.id = d.cate_id', array ('cat_id'=>'cat.id'));
        $select->joinleft(array('c'=>DATABASE_TRADE.'.contructors'),'c.id = p.contructor_id', array ());

        if (isset($params['title']) and $params['title']==199){
            $select->where('c.user = ?', $params['id_user']);
        }

        $select->where('d.air_id = ?', $params['id']);
        $list=$db->fetchAll($select);
       // var_dump($list); exit;
        return $list;
    }
    function getShopDeXuat($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $select->from(array('D' => DATABASE_TRADE.'.app_air'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.store_id'));
         $select->where('D.id = ?',$id);
       $select ->joinleft(array('C'=>DATABASE_TRADE.'.store'),'C.id = D.store_id', array ('shop_name'=>'C.name'));
       
        $shop=$db->fetchRow($select);
        return $shop;
    }
    function getContructor()
    {
        $db= Zend_Registry::get('db');
        $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.contructors'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.name','D.short_name')); 
        $contructor=$db->fetchAll($select);
        //echo $contractor; exit;
        return $contructor;
    }
    //download file đề xuất
    public function getUrlFile($id)
    {
        $db= Zend_Registry::get('db');
        $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_air_file'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.url'));
         $select->where('D.air_id = ?',$id);
         $select->where('D.type = ?',2);

        $file_dx=$db->fetchRow($select);
         //var_dump($file_dx['id']); exit;
        return $file_dx;
    }
     public function getUrlFileQuotation($id)
    {
        $db= Zend_Registry::get('db');
        $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_air_file'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.url'));
         $select->where('D.air_id = ?',$id);
         $select->where('D.type = ?',4);

        $file_quotation=$db->fetchRow($select);
         //var_dump($file_dx['id']); exit;
        return $file_quotation;
    }
    public function getCategory()
    {
         $db= Zend_Registry::get('db');
        $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.category'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.name')); 
         $select->where('D.group_bi IN (?)',array(1,4));
        $cat=$db->fetchAll($select);
        return $cat;
    }
    public function getCountnhathau($id){
        $db= Zend_Registry::get('db');
        $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_quotation'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.contractor_id')); 
         $select->where('D.air_id = ?',$id);
        $select->group('D.contractor_id');
        $con=$db->fetchAll($select);
        return $con;
    }
    public function getContructor_ID($id)
    {
         $db= Zend_Registry::get('db');
        $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.contructors'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.name','D.user')); 
         $select->where('D.user = ?',$id);
        $contructor=$db->fetchRow($select);
        //echo $contractor; exit;
        return $contructor;
    }
    public function getChiaNhaThau($id){
         $db= Zend_Registry::get('db');
         $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_air_contructor'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.contructor_id'));
         $select ->joinleft(array('a'=>DATABASE_TRADE.'.app_air_detail'),'a.id = D.air_detail_id', array ());
         $select->where('a.air_id = ?',$id);
         $select->group('D.contructor_id');
         $contructor=$db->fetchAll($select);
         $result=count($contructor);
       //echo $select; exit;
        return $result;
    }

    public function getNhathauXacNhan($id)
    {
        $db= Zend_Registry::get('db');
         $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_air_contructor'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.contructor_id'));
         $select ->joinleft(array('a'=>DATABASE_TRADE.'.app_air_detail'),'a.id = D.air_detail_id', array ());
         $select->where('a.air_id = ?',$id);
         $select->where('D.step = ?',1);
         $select->group('D.contructor_id');
         $contructor=$db->fetchAll($select);
         $result=count($contructor);
         //var_dump($result); exit;
        return $result;
    }
    public function getNhaThauUpload($id){
          $db= Zend_Registry::get('db');
        $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_quotation'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.contractor_id')); 
         $select->where('D.air_id = ?',$id);
        // $select->where('D.contractor_id IS NOT NULL');
         $select->group('contractor_id');
        $contructor=$db->fetchAll($select);
        $result=count($contructor);
       
        return $result;
    }

   public function getInfor($params){
         $db= Zend_Registry::get('db');
         $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_air_contructor'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'), 'D.contructor_id','D.air_detail_id','D.quantity'))
         ->joinleft(array('F'=>DATABASE_TRADE.'.contructors'),'F.id = D.contructor_id', array ('name_contructor'=>'F.short_name'))
          ->joinleft(array('E'=>DATABASE_TRADE.'.app_air_detail'),'E.id = D.air_detail_id', array ())
          ->joinleft(array('C'=>DATABASE_TRADE.'.app_air'),'C.id = E.air_id', array ());
         

         $select->where('C.id = ?',$params['id']);
       
          $result=$db->fetchAll($select);
          //var_dump($result); exit;
        // $select->where('D.contractor_id IS NOT NULL');
       return $result;
   }
   public function getSoluongdexuat($id){
        $db= Zend_Registry::get('db');
         $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_air_detail'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'),'quanti'=>'SUM(D.quantity)'))
          ->joinleft(array('C'=>DATABASE_TRADE.'.app_air'),'C.id = D.air_id', array ());
         

         $select->where('C.id = ?',$id);
         $select->group('D.air_id');
          $result=$db->fetchAll($select);
         // var_dump($result); exit;
        // $select->where('D.contractor_id IS NOT NULL');
       return $result;
   }
   public function getChangeQuotation($id){
            $db= Zend_Registry::get('db');
         $select=$db->select();
         $select->from(array('D' => DATABASE_TRADE.'.app_quotation'),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS D.id'),'D.edit_id','D.air_id','D.contractor_id'));
         $select->where('D.air_id = ?',$id);
         $select->where('D.edit_id IS NOT NULL');
          $result=$db->fetchAll($select);
         // var_dump($result); exit;
       return $result;
   }
    public function fetchPaginationStore($page, $limit, &$total, $params){
       $db = Zend_Registry::get('db');
        $select = $db->select();
            $select->from(array('a' =>'store'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.name','a.regional_market','a.district','a.longitude','a.latitude','a.shipping_address'))
             ->joinleft(array('c'=>'province_district'),'c.district_id = a.district', array ('name_district' => 'c.district_center','name_province'=>'c.province_name'))
             ->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market', array())
             ->joinleft(array('d'=>'area'),'d.id = b.area_id', array ('name_area' => 'd.name'));
        if(!empty($params['name']))
        {
             $select->where('a.name LIKE ?','%'.$params['name'].'%');
        }
         if(!empty($params['area']))
        {
             $select->where('b.area_id = ?',$params['area']);
        }
          if(!empty($params['province']))
        {
             $select->where('c.province_code = ?',$params['province']);
        }
         if(!empty($params['district']))
        {
             $select->where('c.district_center LIKE ?','%'.$params['district'].'%');
        }

        if(!empty($params['brand_shop'])){
            $select->where('a.is_brand_shop = ?', 1);
        }

        $select->where('a.del = 0 OR a.del IS NULL', NULL);

        $select->order ('a.id DESC');

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
  
    }
    public function getArea(){
       $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' =>'area'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.name'));
        $result = $db->fetchAll($select);
      return $result;
  
    }
     public function getAjax($params){
       $db = Zend_Registry::get('db');
        $select = $db->select();
    $select->from(array('a' =>'store'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT(d.province_name)'),'province_id'=>'d.province_code'))
        ->joinleft(array('b'=>'regional_market'),'b.id = a.regional_market', array ())
        ->joinleft(array('c'=>'area'),'c.id = b.area_id', array ())
        ->joinleft(array('d'=>'province_district'),'d.district_id = a.district', array ());
        $select->where('c.id = ?',$params['area_id']);
        $select->where('d.province_name IS NOT NULL');
        $result = $db->fetchAll($select);
      return $result;
    }

    public function getPovince($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' =>'area'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT(a.id)'),'area_name'=>'a.name','province_id'=>'r.id','province_name'=>'r.name'))
        ->joinleft(array('r'=>'regional_market'),'r.area_id = a.id', array ());        
        $select->where('a.id = ?',$params['area_province_id']);
        $result = $db->fetchAll($select);
      return $result;
    }

    public function getAjaxDistrict($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();
    $select->from(array('a' =>'province_district'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.district_center')));
    $select->where('a.province_code = ?',$params['province_id']);
    $kq = $db->fetchAll($select);
      return $kq;
    }

    public function getCost($params)
    {
        $date = "2019-04-01"; // không tính chi phí quí 1 năm 2019

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.app_air'], [
                'r.area_id',
                'di.is_ka',
                'cost' => '(q.total_price) + (q.total_price * 0.1)'
            ])
            ->join(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => WAREHOUSE_DB.'.distributor'], 's.d_id = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['d' => DATABASE_TRADE.'.air_details'], 'a.id = d.air_id', [])
            ->joinLeft(['q' => DATABASE_TRADE.'.air_quotation'], 'd.id = q.air_details_id AND q.status = (
                                select MAX(status) from trade_marketing.air_quotation where air_details_id = d.id AND status <> 3 AND status <> 1 AND status <> 5
                                )', [])
            ->where('a.remove IS NULL OR a.remove = 0')
            ->where('a.created_at >= ? ', $date);
//            ->where('di.is_ka = 0');


        if ($params['status_finish']) {
            $select->where('a.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if(!empty($params['staff_id'])){
            $select->where('a.created_by = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(a.created_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(a.created_at) = ?', $params['year']);
        }

        if ($params['month_in_season']) {
            $select->where('MONTH(a.created_at) IN (?)', $params['month_in_season']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        $result = $db->fetchAll($select);


        // 1: thi công IA, 2: thi công KA
        foreach ($result as $element) {
            $airCost [$element['area_id']] ['1'] += $element['is_ka'] == 0 ? $element['cost'] : 0  ;
            $airCost [$element['area_id']] ['2'] += $element['is_ka'] == 1 ? $element['cost'] : 0  ;
        }

        return $airCost;
    }

    public function getContractNumber($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['r' => DATABASE_TRADE.'.app_air'], [
                'air_id' => 'r.id',
                'd.contract_number'
            ])
            ->joinLeft(['d' => DATABASE_TRADE.'.air_details'], 'r.id = d.air_id', [])
            ->where('r.remove IS NULL OR r.remove = 0')
            ->where('d.contract_number IS NOT NULL');

        $result = $db->fetchALl($select);

        foreach ($result as $element) {
            $list [$element['air_id']] [] = $element['contract_number'];
        }

        return $list;
    }

    public function getStatisticReviewCost($params = null)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'channel' => "CASE 
                            WHEN (loyalty.loyalty_plan_id IS NOT NULL AND distributor.is_ka = 0) THEN plan.name
                            WHEN (loyalty.loyalty_plan_id IS NULL AND distributor.is_ka = 0) THEN 'Shop Thường'
                            WHEN (distributor.is_ka = 1) THEN 'KA'
                          END",
            'air.store_id',
            'store_name' => 's.name',
            'dealer_id' => 'IF(distributor.parent IS NULL OR distributor.parent = 0, distributor.id, distributor.parent)',
            'distributor.partner_id',
            'area' => 'a.name',
            'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
            'air.created_at',
            'status' => 'status.name',
            'review_cost' => "IF(detail.review_cost = 1, 'Đúng', IF (detail.review_cost = 2, 'Sai', ''))"

        );

        $select->from(['air' => DATABASE_TRADE.'.app_air'], $arrCols);
        $select->joinLeft(array('detail' => DATABASE_TRADE.'.air_details'), 'air.id = detail.air_id', array());
        $select->joinLeft(['s' => 'store'], 'air.store_id = s.id', []);
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'r.area_id = a.id', array());
        $select->joinLeft(array('distributor' => WAREHOUSE_DB.'.distributor'), 's.d_id = distributor.id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = IF (distributor.parent IS NULL OR distributor.parent = 0, distributor.id, distributor.parent)  AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'loyalty.loyalty_plan_id = plan.id', array());

        $select->joinLeft(array('st' => 'staff'), 'air.created_by = st.id', array());
        $select->joinLeft(array('status' => DATABASE_TRADE.'.app_status'), 'air.status = status.status AND status.type = 8', array());

        $select->where('s.del IS NULL OR s.del = 0');
        $select->where('air.remove IS NULL OR air.remove = 0');
        $select->order('air.created_at DESC');
        $select->group('air.id');
        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if (!empty($params['review_cost'])) {
            $select->where('detail.review_cost = ?', $params['review_cost']);
        }

        if (!empty($params['from_date'])) {
            $select->where('air.created_at >= ?', $params['from_date']);
        }

        if (!empty($params['to_date'])) {
            $select->where('air.created_at <= ?', $params['to_date']);
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function exportReviewCost($data)
    {
        ini_set("memory_limit", -1);
        ini_set("display_error", 1);
        error_reporting(~E_ALL);

        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();
        $heads = array(
            'STT',
            'KÊNH',
            'ID SHOP',
            'DEALER ID',
            'PARTNER ID',
            'TÊN SHOP',
            'KHU VỰC',
            'TÌNH TRẠNG',
            'NGÀY TẠO',
            'NGƯỜI TẠO',
            'TRẠNG THÁI CHI PHÍ'

        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){


            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['status']);
            $sheet->setCellValue($alpha++.$index, date('d-m-Y H:i:s', strtotime($item['created_at'])));
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['review_cost']);

            $index++;

        }



        $filename = 'Thi công tổng quát' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
}