<?php
class Application_Model_ContractorArea extends Zend_Db_Table_Abstract
{
	protected $_name = 'contractor_area';

	protected $_schema = DATABASE_TRADE;

	function getDetails($params)
    {
        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'p.id',
            'p.contractor_id',
            'p.category_id',
            'p.area_id',
            'quantity' => 'p.quantity',
            'p.status',
            'p.quantity_area',
            'p.created_at',
            'p.area_date'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.contractor_area'), $arrCols);
        
        $select->where('p.campaign_id = ?', $params['campaign_id']);

        if(!empty($params['area_id'])){
            $select->where('p.area_id IN (?)', $params['area_id']);            
        }

        if(!empty($params['contractor_id'])){
            $select->where('p.contractor_id IN (?)', $params['contractor_id']);            
        }

        if(!empty($params['cat_id'])){
            $select->where('p.category_id IN (?)', $params['cat_id']);            
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getContractorId($staff_id)
    {
        
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'id'     => "p.id" 
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);
        $select->where('p.user = ?', $staff_id);
        
        $result  = $db->fetchRow($select);
        
        return $result['id'];
    }


}