<?php
class Application_Model_WarrantyCard extends Zend_Db_Table_Abstract
{
    protected $_name = 'warranty_card';

    public function getData($params){
    	$db = Zend_Registry::get('db');
        $select = $db->select();
        
        $col = array(
            'p.*'
        );

        $select->from(array('p' => 'warranty_card'), $col);
        
        if(isset($params['purchase_date']) and $params['purchase_date']){
            $select->where('p.purchase_date = ?', $params['purchase_date']);
        }

        return $db->fetchAll($select); 
    }

}