<?php
class Application_Model_StaffTrainer extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_trainer';

    public function getAreaTrainer($staff_id)
    {
        //remove cache
        $cache = Zend_Registry::get('cache');
        $cache->remove('asm_cache');
        $cache->remove('regional_market_cache');
        $cache->remove('regional_market_district_cache');
        $cache->remove('area_cache');

        $userStorage          = Zend_Auth::getInstance()->getStorage()->read();
        $QAsm                 = new Application_Model_Asm();
        $QRegionalMarket      = new Application_Model_RegionalMarket();
        $QArea                = new Application_Model_Area();
        
        $cachedRegionalMarket = $QRegionalMarket->get_cache();
        $allDistrict          = $QRegionalMarket->get_district_cache();
        $cachedArea           = $QArea->get_cache();
        $cachedAsm            = $QAsm->get_cache($staff_id);

        $area                 = $cachedAsm['area'];
        $province             = $cachedAsm['province'];
        $district             = $cachedAsm['district'];
        $result               = array();
        $areaStaff            = array();
        $provinceStaff        = array();
        $districtStaff        = array();

        if($area and count($area))
        {
            foreach ($area as $key => $value)
            {
                $areaStaff[$value] = $cachedArea[$value];
            }
            $result['area']      = $areaStaff;
        }

        if($province and count($province))
        {
            foreach ($province as $key => $value)
            {
                if($value)
                $provinceStaff[$value] = $cachedRegionalMarket[$value];
            }
            $result['province']  = $provinceStaff;
        }

        if($district and count($district))
        {
            foreach ($district as $key => $value)
            {
                if($value)
                    $districtStaff[$value] = $allDistrict[$value]['name'];
            }
            $result['district']  = $districtStaff;
        }

        $full_rights_trainer = unserialize(FULL_RIGHTS_TRAINER);
        $key_project         = unserialize(TRAINER_KEY_PROJECT);

        if(in_array($userStorage->title,$full_rights_trainer)  || in_array($userStorage->id,$key_project) || $userStorage->group_id == ADMINISTRATOR_ID)
        {
            $result['area']      = $cachedArea;
            $result['province']  = $cachedRegionalMarket;
            $arrDistrictAll      = array();
            foreach ($allDistrict as $key => $value)
            {
                $arrDistrictAll[$key] = $value['name'];
            }
            $result['district']  = $arrDistrictAll;
        }

        return $result;
    }

    public function checkPGPBSALE($staff_id)
    {
        $QStaff             = new Application_Model_Staff();
        $whereStaff         = $QStaff->getAdapter()->quoteInto('id = ?',$staff_id);
        $rowStaff           = $QStaff->fetchRow($whereStaff);
        $result             = null;

        if(!$rowStaff)
        {
            $result = 'Can Find Information PGPB';

        }
        else
        {
            if(!in_array($rowStaff['title'],array(PGPB_TITLE, PGPB_2_TITLE, SALE_SALE_SALE, PG_BRANDSHOP)))
            {
                $result = 'The Record Not PGPB OR SALE';
            }

            if($rowStaff['status']!= 1)
            {
                $result = 'PGPB OR SALE is disabled';
            }
        }

        return $result;
    }

    public function checkAreaPGSALEISAreaTrainer($staff_id_Pg,$staff_id_trainer)
    {
        $result             = array();
        $userStorage        = Zend_Auth::getInstance()->getStorage()->read();
        $QStaffTrainer      = new Application_Model_StaffTrainer();
        $area_trainer       = null;

        $QStaff     = new Application_Model_Staff();
        $whereStaff = $QStaff->getAdapter()->quoteInto('id = ?',$staff_id_Pg);
        $rowStaff   = $QStaff->fetchRow($whereStaff);
        if($rowStaff)
        {
            $regionalMarket = $rowStaff['regional_market'];
            $Allvalue       = $QStaffTrainer->getAreaTrainer($staff_id_trainer);
            $province       = array_keys($Allvalue['province']);

            $full_rights_trainer = unserialize(FULL_RIGHTS_TRAINER);
            $key_project = unserialize(TRAINER_KEY_PROJECT);

            if(in_array($regionalMarket,$province) || $userStorage->group_id == ADMINISTRATOR_ID || in_array($userStorage->title,$full_rights_trainer) || in_array($userStorage->id,$key_project)  )
            {

            }
            else
            {
                $result = "Area PG Not True Area Trainer";
            }
        }
        else
        {
            $result = 'Can not found PG OR SALE';
        }

        return $result;
    }

    function fetchPaginationStaff($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $name = 'staff';

        /*
        if ($limit) {
            $select = $db->select()
                ->from(array('p' => $name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db->select()
                ->from(array('p' => $name),
                    array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }
        */
       
        //viet lại đoạn trên
        $cols = array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'fullname' => "CONCAT(p.firstname,' ', p.lastname)",
                'p.joined_at',
                'p.code',
                'p.email',
                'p.phone_number',
                'department_name' => 't.name',
                'team_name' => 't2.name',
                'title_name' => 't3.name',
                'staff_temp_id' => 'st.id', 
                'staff_temp_is_approved' => 'st.is_approved',
                'sp.point',
                '_st.staff_id as staff_trainer',
                '_st.facebook_link',
                'srw.staff_id as staff_reward_warning',
                'srw.content as content_reward_warning'
            );
        $select = $db->select()
            ->from(array('p' => $name),$cols)
            ->joinLeft(array('st' => 'staff_temp'),'p.id = st.staff_id',array())
            ->joinLeft(array('sp' => 'staff_point'),'p.id = sp.staff_id and sp.month = '.date('m').' and sp.year = '.date('Y'),array())
            ->joinLeft(array('_st' => 'staff_trainer'),'p.id = _st.staff_id',array())
            ->joinLeft(array('srw' => 'staff_reward_warning'),'p.id = srw.staff_id and srw.month = '.date('m').' and srw.year = '.date('Y').' and (st.del = 0 OR st.del IS NULL) ',array())
            ->joinLeft(['rmk'=>'regional_market'],'rmk.id=p.regional_market',[])
            ->joinLeft(['a'=>'area'],'a.id=rmk.area_id',['area'=>'a.id'])
            ->joinLeft(['t'=>'team'],'t.id = p.department',[])
            ->joinLeft(['t2'=>'team'],'t2.id = p.team',[])
            ->joinLeft(['t3'=>'team'],'t3.id = p.title',[])
            ;

        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            }
        }

        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            }
        }

        if (isset($params['is_officer']) and intval($params['is_officer']) > 0){
            if ($params['is_officer'] == 1)
                $select->where('p.is_officer = ?', 1);
            elseif ($params['is_officer'] == 2)
                $select->where('p.is_officer = ?', 0);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if (isset($params['date']) and $params['date'])
        {
            $date = explode(',', $params['date']);
            $select->where("DAY(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month']) and $params['month'])
        {
            $date = explode(',', $params['month']);
            $select->where("MONTH(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if(isset($params['month_off']) and $params['month_off'])
        {
            $select
                ->joinLeft(array('mo' => 'off_date_add'), 'p.id = mo.staff_id ' , array('staff_id' => 'mo.staff_id'));
            $select->where('MONTH(mo.date) <> ?' , $params['month_off'] )->orWhere('MONTH(mo.date) is null' , null);
        }

        if (isset($params['year']) and $params['year'])
        {
            $date = explode(',', $params['year']);
            $select->where("YEAR(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }


        if (isset($params['title']) and $params['title']) {
            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }
        if (isset($params['status']) and $params['status']) {
                $select->where('p.status IN (?)', $params['status']);
        }

        if (isset($params['need_approve']) and $params['need_approve']){
            $select->where('st.is_approved = ?', 0);
        }

        if ( ( isset($params['off']) and intval($params['off']) > 0 ) || ( isset($params['sname']) and $params['sname'] == 1 ) )
            $select->where('p.off_date is '
                . ( $params['off'] == 2 ? 'not' : '')
                .' null');


        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])){

            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if(isset($params['area_trainer_right']) and $params['area_trainer_right'] and !(isset($params['regional_market_right']) and $params['regional_market_right']))
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight  = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }
        elseif (isset($params['regional_market_right']) and $params['regional_market_right']) {
            if (is_array($params['regional_market_right']) && count($params['regional_market_right']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market_right']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%'.$params['name'].'%');
        }
        // if(!empty($params['officialStaff'])){
        //     $select->where('st.contract_term NOT IN(?)', $params['officialStaff']);
        // }
        
        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name')
            {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) '.$collate . $desc;
            }
            else if ( $params['sort'] == 'department' || $params['sort'] == 'team' )
            {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            }
            else if($params['sort'] == 'point')
                $order_str = 'sp.`'.$params['sort'] . '` ' . $desc;
            else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        if (isset($params['big_area_id']) and $params['big_area_id']) {
                $select->where('a.bigarea_id IN (?)', $params['big_area_id']);
        }

        if (isset($params['joined_at_from']) and $params['joined_at_from']) {
                $select->where('p.joined_at >= ?', $params['joined_at_from']);
        }

        if (isset($params['joined_at_to']) and $params['joined_at_to']) {
                $select->where('p.joined_at <= ?', $params['joined_at_to']);
        }

        $select->group('p.id');

        if(!empty($params['export']) AND $params['export'] == 1){
            return $select->__toString();
        }


        if ($limit){
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function getInfoStaff($staff_id)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),array('p.staff_id','p.knowledge','p.skill','p.attitude','p.facebook_id'));

        $select->where('p.staff_id = ?',$staff_id);

        $result = $db->fetchAll($select);

        return $result;

    }

    function getStaffForCourse($params)
    {
        $db = Zend_Registry::get('db');

        $name = 'staff';

        $select = $db->select()
                ->from(array('p' => $name),
                    array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));


        if (isset($params['title']) and $params['title']) {
            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])){

            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if(isset($params['area_trainer_right']) and $params['area_trainer_right'] and !(isset($params['regional_market_right']) and $params['regional_market_right']))
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight  = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }
        elseif (isset($params['regional_market_right']) and $params['regional_market_right']) {
            if (is_array($params['regional_market_right']) && count($params['regional_market_right']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market_right']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListExportPgsSale($params){
        $db = Zend_Registry::get('db');
        $select  = $db->select();

        $arrCols = array(
            "p.code", 
            "fullname"     => "CONCAT( p.firstname,  ' ', p.lastname )", 
            "p.email", 
            "department"   => "t.name", 
            "team"         => "t2.name", 
            "title"        => "t3.name", 
            "p.joined_at", 
            "p.phone_number", 
            "p.status", 
            "reward"       => "GROUP_CONCAT( DISTINCT s.id,s.content SEPARATOR  '/' )", 
            "warning"      => "GROUP_CONCAT( DISTINCT s2.id, s2.content SEPARATOR  '/' )",
            "sellout"      => "count(DISTINCT i.imei_sn)"
        );

        $select->from(array('p'=> 'staff'), $arrCols);

        $select->joinLeft(array('i'=>'imei_kpi'), '(i.sale_id = p.id OR i.pg_id = p.id )',array());
        $select->joinLeft(array('s'=>'staff_reward_warning'), 's.staff_id = p.id AND s.type = 1',array());
        $select->joinLeft(array('s2'=>'staff_reward_warning'), 's2.staff_id = p.id AND s2.type = 2',array());
        $select->joinLeft(array('t'=>'team'), 't.id = p.department',array());
        $select->joinLeft(array('t2'=>'team'), 't2.id = p.team',array());
        $select->joinLeft(array('t3'=>'team'), 't3.id = p.title',array());
        
        $select->where('p.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE, SALE_SALE_SALE));
        $select->where('p.off_date IS NULL', null);

        if(isset($params['area_list']) and $params['area_list']){
            $select->where('ar.id IN (?)', $params['area_list']);
        }
        if(isset($params['from_date']) and $params['from_date']){
            $select->where('DATE(i.timing_date) >= ?', $params['from_date']);
        }
        else{
            return false;
        }

        if(isset($params['to_date']) and $params['to_date']){
            $select->where('DATE(i.timing_date) <= ?', $params['to_date']);
        }
        else{
            return false;
        }
        $select->group(array('p.id'));
        // echo $select->__toString();exit;
        $result = $db->fetchAll($select);

        return $result;
    }


}