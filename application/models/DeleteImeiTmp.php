<?php

class Application_Model_DeleteImeiTmp extends Zend_Db_Table_Abstract
{
    protected $_name = 'delete_imei_tmp';

    public function getDuplicateImei()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => 'delete_imei_tmp'], [
                'd.imei'
            ])
            ->group('d.imei')
            ->having("COUNT(d.imei) > 1");

        $result = $db->fetchCol($select);

        return $result;

    }

    public function getInvalidImei($params)
    {
        $from_date = "'" . $params['from_date'] . "'";
        $to_date = "'" . $params['to_date'] . "'";
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => 'delete_imei_tmp'], [
                'd.imei'
            ])
            ->joinLeft(['ts' => 'timing_sale'], 'd.imei = ts.imei', [])
            ->joinLeft(['t' => 'timing'], 'ts.timing_id = t.id AND t.`from` >= ' . $from_date . ' AND t.`from` <= ' . $to_date, [])
            ->where('t.id IS NULL');

        $result = $db->fetchCol($select);

        return $result;

    }
}