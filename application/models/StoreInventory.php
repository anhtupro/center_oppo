<?php

class Application_Model_StoreInventory extends Zend_Db_Table_Abstract
{
    public function fetchPagination($page, $limit, &$total, $params)
    {
        // tg stock lấy theo 6 tháng gần nhất
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $from_date = date('Y-m-d 00:00:00', strtotime('- 3 Months'));
        $to_date = date('Y-m-d 23:59:59');

        $db = Zend_Registry::get("db");

        // sub select
        $arr_col_inventory = [
            'distributor_id' => 'd.id',

//                'sellin_deliveried'  => 'SUM(IF(dr.status_dn IN(5,6) OR dr.type IN(1,3), 1, 0))',
            'sellin_not_deliveried' => 'SUM(IF((dr.status_dn IN(1,2,3,4) AND dr.type NOT IN(1,3) AND dr.carrier_id IN (14,15,16)) OR dr.status_dn IS NULL, 1, 0))',
//                'sellin' => 'COUNT(*)',
//                'inventory' => 'COUNT(*) - COUNT(i.activated_date)',
            'stock' => 'SUM( IF( (dr.status_dn IN ( 5, 6 ) OR dr.type IN ( 1, 3 ) OR dr.carrier_id NOT IN (14,15,16)) AND i.activated_date IS NULL, 1, 0 ))',
            'activated_average' => 'ROUND(SUM( IF( (DATE(i.activated_date) BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND CURDATE()) , 1, 0) ) / 7)'
        ];

        if ($params['export_by_model']) {
            $arr_col_inventory ['good_id'] =  'i.good_id';
            $arr_col_inventory ['model'] =  'g.desc';
        }

        $select_inventory = $db->select()
            ->from(['i' => WAREHOUSE_DB . '.imei'], $arr_col_inventory)
            ->join(['m' => WAREHOUSE_DB . '.market'], 'i.sales_id = m.id AND m.type = 1 AND m.isbacks = 0', [])
            ->join(['d' => WAREHOUSE_DB . '.distributor'], 'i.distributor_id = d.id', [])
            ->joinLeft(['ds' => WAREHOUSE_DB . '.delivery_sales'], 'i.sales_sn = ds.sales_sn', [])
            ->joinLeft(['dr' => WAREHOUSE_DB . '.delivery_order'], 'ds.delivery_order_id = dr.id', [])
            ->join(['g' => WAREHOUSE_DB . '.good'], 'g.id = i.good_id', [])
            ->where('m.invoice_time >= ?', $from_date)
            ->where('m.invoice_time <= ?', $to_date)
            ->where('g.cat_id IN (?)', [11,15])
            ->where('g.brand_id = ?', 12);
//            ->group('d.id');

        // main select
        $select = $db->select()
            ->from(['s' => 'store'], [
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
                'store_id' => 's.id',
                'store_name' => 's.name',
                'district' => 'dis.name',
                'province' => 'p.name',
                'area' => 'a.name',
                'd.*'
            ])
            ->joinLeft(['dis' => 'regional_market'], 's.district = dis.id', [])
            ->joinLeft(['p' => 'regional_market'], 'dis.parent = p.id', [])
            ->joinLeft(['a' => 'area'], 'p.area_id = a.id', [])
            ->joinLeft(['d' => $select_inventory], 's.d_id = d.distributor_id', [])
            ->joinLeft(['d2' => WAREHOUSE_DB . '.distributor'], 's.d_id = d2.id', [])



            ->where('s.del IS NULL OR s.del = 0')
//            ->group('s.id')
            ->order('d.stock DESC');

        if (! $params['export_by_model']) { // group theo shop
            $select_inventory->group('d.id');
            $select->group('s.id');
        } else { // export theo shop , model


            $select_inventory->group(['d.id', 'i.good_id']);
            $select->group(['s.id', 'd.good_id']);
        }




        if (in_array($userStorage->title, [SALES_TITLE])) {
            // join lấy ra danh sách store hiển thị
            $select_sale = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'l.store_id',
                    'l.staff_id'
                ])
                ->where('l.staff_id = ?', $userStorage->id)
                ->where('l.is_leader = ?', 1)
                ->where('l.released_at IS NULL')
                ->group('l.store_id');

            $select->join(['stl' => $select_sale], 'stl.store_id = s.id', []);

            // join lấy ra inventory theo ds store distributor
            $select_sale_dis = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    's.d_id'
                ])
                ->join(['s' => 'store'], 'l.store_id = s.id', [])
                ->where('l.staff_id = ?', $userStorage->id)
                ->where('l.is_leader = ?', 1)
                ->where('l.released_at IS NULL')
                ->group('s.d_id');

            $select_inventory->join(['stld' => $select_sale_dis], 'stld.d_id = d.id', []);

        } elseif (in_array($userStorage->title, [SALES_LEADER_TITLE])) {
            // join lấy ra danh sách store hiển thị
            $select_leader = $db->select()
                ->from(['l' => 'store_leader_log'], [
                    'l.store_id',
                    'l.staff_id'
                ])
                ->where('l.staff_id = ?', $userStorage->id)
                ->where('l.released_at IS NULL')
                ->group('l.store_id');

            $select->join(['stl' => $select_leader], 'stl.store_id = s.id', []);


            // join lấy ra inventory theo ds store distributor
            $select_leader_dis = $db->select()
                ->from(['l' => 'store_leader_log'], [
                    's.d_id'
                ])
                ->join(['s' => 'store'], 'l.store_id = s.id', [])
                ->where('l.staff_id = ?', $userStorage->id)
                ->where('l.released_at IS NULL')
                ->group('s.d_id');

            $select_inventory->join(['stld' => $select_leader_dis], 'stld.d_id = d.id', []);

        } elseif (in_array($userStorage->title, [RM_TITLE, RM_STANDBY_TITLE, SALES_ADMIN_TITLE, PARTNER_TITLE] )) {
            // danh sách store chính
            $select->join(['asm' => 'asm'], 'a.id = asm.area_id AND asm.staff_id = ' . $userStorage->id, []);

            // danh sách inventory
            $select_inventory->joinLeft(['di' => 'regional_market'], 'd.district = di.id', []);
            $select_inventory->joinLeft(['p' => 'regional_market'], 'di.parent = p.id', []);
            $select_inventory->join(['asm' => 'asm'], 'p.area_id = asm.area_id AND asm.staff_id = ' . $userStorage->id, []);

        } elseif (in_array($userStorage->title,[SALES_MANAGER_TITLE, KEY_ACCOUNT_MANAGER_TITLE, KEY_ACCOUNT_SUPERVISOR_TITLE, TECHNOLOGY_MANAGER_TITLE, SALES_ASSISTANT_TITLE, GTM_PO_ASSISTANT_TITLE, CEO_ASSISTANT_TITLE])) {
            // see all
        } else {
            $select->where('1=0');
        }

        if ($params['good_id']) {
            $select_inventory->where('g.id = ?', $params['good_id']);
        }

        if ($params['area_id']) {
            // danh sách store chính
            $select->where('a.id = ?', $params['area_id']);

            // danh sách inventory
            if (! in_array($userStorage->title, [RM_TITLE, RM_STANDBY_TITLE, SALES_ADMIN_TITLE, PARTNER_TITLE] )) { // không join nữa vì ở trên đã join rồi
                $select_inventory->joinLeft(['di' => 'regional_market'], 'd.district = di.id', []);
                $select_inventory->joinLeft(['p' => 'regional_market'], 'di.parent = p.id', []);
            }

            $select_inventory->where('p.area_id = ?', $params['area_id']);
        }

        if ($params['channel_id'] || $params['export'] || $params['export_by_model']) {
            $select->joinLeft(['v' => 'v_channel_dealer'], 'd2.id = v.d_id', []);
            $select->joinLeft(['cha' => 'channel'], 'v.channel_id = cha.id', ['channel_name' => 'cha.name']);

            if ($params['channel_id']) {
                // danh sách store chính
            $select_inventory->joinLeft(['vc' => 'v_channel_dealer'], 'd.id = vc.d_id', []);
            $select_inventory->where('vc.channel_id = ?', $params['channel_id']);

            $select->where('v.channel_id = ?', $params['channel_id']);

            }
        }


        if ($params['store_name']) {
            $select->where('s.name LIKE ?', '%' . TRIM($params['store_name']) . '%');
        }

        if (!$params['export'] && !$params['export_by_model']) {
            $select->limitPage($page, $limit);
        }


        if ($_GET['dev']) {
            echo $select;die;
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getTotalOrder($distributor_id, $from_date, $to_date)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['m' => WAREHOUSE_DB . '.market'], [
                'model' => 'g.desc',
                'color' => 'c.name',
                'quantity' => 'SUM(m.num)'
//                'delivery_status' => "CASE
//                                        WHEN (d.status_dn = 1) THEN  'Ready for Warehouse'
//                                        WHEN (d.status_dn = 2) THEN  'Ready for Outbound'
//                                        WHEN (d.status_dn = 3) THEN  'Ready for Delivery'
//                                        WHEN (d.status_dn = 4) THEN  'Carrier Confirmed'
//                                        WHEN (d.status_dn = 5) THEN  'Delivered'
//                                        WHEN (d.status_dn = 6) THEN  'Virtual Order'
//                                    END"
            ])
            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'm.good_id = g.id', [])
            ->joinLeft(['c' => WAREHOUSE_DB . '.good_color'], 'm.good_color = c.id', [])
            ->joinLeft(['s' => WAREHOUSE_DB . '.delivery_sales'], 'm.sn = s.sales_sn', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.delivery_order'], 's.delivery_order_id = d.id', [])
            ->where('m.d_id = ?', $distributor_id)
            ->where('m.type = ?', 1)
            ->where('m.isbacks = ?', 0)
            ->where('m.invoice_time >= ?', $from_date)
            ->where('m.invoice_time <= ?', $to_date)
            ->where('g.cat_id IN (?)', [11,15])
            ->where('g.brand_id = ?', 12)
            ->group(['m.good_id', 'm.good_color']);

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getDetailSellin($distributor_id, $from_date, $to_date, $type)
    {
        // sub select
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => WAREHOUSE_DB . '.imei'], [
                'distributor_id' => 'd.id',
                'model' => 'g.desc',
                'color' => 'c.name',
                'sellin' => 'COUNT(*)',
                'sellin_not_deliveried' => 'SUM(IF((dr.status_dn IN(1,2,3,4) AND dr.type NOT IN(1,3) AND dr.carrier_id IN (14,15,16) ) OR dr.status_dn IS NULL, 1, 0))',
//                'inventory' => 'COUNT(*) - COUNT(i.activated_date)',
                'stock' => 'SUM( IF( (dr.status_dn IN ( 5, 6 ) OR dr.type IN ( 1, 3 ) OR dr.carrier_id NOT IN (14,15,16) ) AND i.activated_date IS NULL, 1, 0 ))'
            ])
            ->join(['m' => WAREHOUSE_DB . '.market'], 'i.sales_id = m.id AND m.type = 1 AND m.isbacks = 0', [])
            ->join(['d' => WAREHOUSE_DB . '.distributor'], 'i.distributor_id = d.id', [])

            ->joinLeft(['ds' => WAREHOUSE_DB . '.delivery_sales'], 'i.sales_sn = ds.sales_sn', [])
            ->joinLeft(['dr' => WAREHOUSE_DB . '.delivery_order'], 'ds.delivery_order_id = dr.id', [])

            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'i.good_id = g.id', [])
            ->joinLeft(['c' => WAREHOUSE_DB . '.good_color'], 'i.good_color = c.id', [])
            ->where('m.invoice_time >= ?', $from_date)
            ->where('m.invoice_time <= ?', $to_date)
            ->where('i.distributor_id = ?', $distributor_id)
            ->where('g.cat_id IN (?)', [11,15])
            ->where('g.brand_id = ?', 12)
            ->group(['i.good_id', 'i.good_color']);

        // 2:get inventory, 1: get sellin deliveried, 3: get sellin on the way
        if ($type == 1) {
            $select->where('dr.status_dn IN (5,6) OR dr.type IN(1,3)');
        }

        if ($type == 3) {
            $select->where('(dr.status_dn IN(1,2,3,4) AND dr.type NOT IN(1,3)) OR dr.status_dn IS NULL');
        }

        if ($type == 2)  {
            $select->having('stock > 0');
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function export($data)
    {
        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();

        $heads = array(
            'Store',
            'Kênh',
            'Tỉnh',
            'Khu vực',
            'Hàng trên đường',
            'Tồn kho hiện tại',
            'Số máy activated trung bình trong 7 ngày gần nhất',
            'Số ngày bán hàng dự kiến'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['channel_name']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['sellin_not_deliveried']);
            $sheet->setCellValue($alpha++.$index, $item['stock']);
            $sheet->setCellValue($alpha++.$index, ($item['activated_average'] > 0) ? $item['activated_average'] : 0);
            $sheet->setCellValue($alpha++.$index,(ROUND($item['stock'] / $item['activated_average']) > 0 && $item['activated_average'] > 0) ? ROUND($item['stock'] / $item['activated_average']) : '');

            $index++;
        }

        $filename = 'Tồn kho shop' ;
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function exportByModel($data)
    {
        require_once 'PHPExcel.php';

        $PHPExcel = new PHPExcel();

        $heads = array(
            'Store',
            'Kênh',
            'Tỉnh',
            'Khu vực',
            'Model',
            'Hàng trên đường',
            'Tồn kho hiện tại',
            'Số máy activated trung bình trong 7 ngày gần nhất',
            'Số ngày bán hàng dự kiến'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['channel_name']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['model']);
            $sheet->setCellValue($alpha++.$index, $item['sellin_not_deliveried']);
            $sheet->setCellValue($alpha++.$index, $item['stock']);
            $sheet->setCellValue($alpha++.$index, ($item['activated_average'] > 0) ? $item['activated_average'] : 0);
            $sheet->setCellValue($alpha++.$index,(ROUND($item['stock'] / $item['activated_average']) > 0 && $item['activated_average'] > 0) ? ROUND($item['stock'] / $item['activated_average']) : '');

            $index++;
        }

        $filename = 'Tồn kho theo shop và model' ;
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

}