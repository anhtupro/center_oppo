<?php
class Application_Model_InsuranceLogInfo extends Zend_Db_Table_Abstract
{
    protected $_name = 'insurance_log_info';

    public function save($before,$after,$staff_id,$created_by,$created_at = NULL){
        if(serialize($before) != serialize($after)){
                $data = array(
                'before'        => serialize($before),
                'after'         => serialize($after),
                'staff_id'      => $staff_id,
                'created_by'    => $created_by,
                'created_at'    => ($created_at) ? $created_at : date('Y-m-d H:i:s')
            );
            $this->insert($data);    
        }
        
    }

    public function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'a.*',
            'created_by_name' => 'CONCAT(b.firstname," ",b.lastname)'
        );
        $select = $db->select()
            ->from(array('a'=>$this->_name),$cols)
            ->join(array('b'=>'staff'),'a.created_by = b.id',array())
            ->where('a.before != a.after')
            ->order('a.created_at DESC')
        ;

        if(isset($params['staff_id']) AND $params['staff_id']){
            $select->where('a.staff_id = ?',$params['staff_id']);
        }

        if(isset($limit) AND $limit){
            $select->limitPage($page,$limit);
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;

    }
}