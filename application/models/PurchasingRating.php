<?php

class Application_Model_PurchasingRating extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_rating';


    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'purchasing_rating'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'purchasing_type_name' => 'pt.name',
                'firstname' => 's.firstname',
                'lastname' => 's.lastname',
                'supplier_name' => 'sp.title',
                'department_name_type' => 't.name'
            ))
            ->joinLeft(array('pt'=>'purchasing_type'),'pt.id = p.purchasing_type_id',array())
            ->joinLeft(array('s'=>'staff'),'s.id = p.created_by',array())
            ->joinInner(
                array('sp'=>'supplier'),'sp.id = p.supplier_id',array()
            )
            ->joinInner(
                array('t'=>'team'),'t.id = pt.department_id',array()
            )
//            ->joinLeft(array('sp'=>'supplier'),'sp.id = p.supplier_id',array())
            ->where('p.del = ?',0)

        ;
        if(isset($params['supplier_name']) AND $params['supplier_name']){
            $select->where('sp.title LIKE ?','%'.$params['supplier_name'].'%');
        }

        if(isset($params['department_name']) AND $params['department_name']){
            $select->where('p.department_name LIKE ?', '%'.$params['department_name'].'%' );
        }
        if(isset($params['purchasing_type_id']) AND $params['purchasing_type_id']){
            $select->where('p.purchasing_type_id = ?',$params['purchasing_type_id']);
        }
        if(isset($params['date']) AND $params['date']){
            $select->where('p.date = ?',$params['date']);
        }
        if(isset($params['point']) AND $params['point']){
            if($params['point']==1) {
                $select->where('p.point IN (?)', array(0,1,2));
            }
            if($params['point']==2) {
                $select->where('p.point IN (?)', array(3,4));
            }
            if($params['point']==3) {
                $select->where('p.point = ?', 5);
            }
        }
        if(isset($params['supplier_id'])||isset($params['sn'])) {
            if (isset($params['supplier_id']) AND $params['supplier_id']) {
                $select->where('p.supplier_id = ?', $params['supplier_id']);
            }
            if (isset($params['sn']) AND $params['sn']) {
                $select->where('p.sn = ?', $params['sn']);
            }
        }

        else{
            if(!isset($params['export'])||($params['export']==1)) {
                $select->group(array("sn"));
            }
        }
        $order_str = '';
        if(isset($params['sort']) AND $params['sort']){
            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            if ($params['sort'] == 'purchasing_type_name' || $params['sort'] == 'date' || $params['sort'] == 'supplier_name'){
                $order_str .= $params['sort'].$collate . $desc;
            } else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }
            $select->order(new Zend_Db_Expr($order_str));
        }else{
            $select->order('p.created_at ASC');
        }


        if ($limit)
            $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);


        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function save($data,$id){

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            if($id){
                $where = $this->getAdapter()->quoteInto('id = ?',$id);
                $this->update($data,$where);
            }else {
                $id = $this->insert($data);
            }
                $arr = array('code' => 1, 'message' => 'Done','id' => $id);
            $db->commit();
            return $arr;
        } catch (Exception $e) {
            $db->rollBack();
            $arr = array('code' => -1, 'message' => $e->getMessage());
        }

    }
    public function getBySn($sn){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'purchasing_rating'),array(
                'p.*',
                'supplier_name' => 'sp.title'
            ))
            ->joinInner(
                array('sp'=>'supplier'),'sp.id = p.supplier_id',array()
            )
            ->where('p.del = ?',0)
            ->where('p.sn = ?',$sn)
        ;
        $result = $db->fetchAll($select);
        return $result;


    }

}



