<?php

class Application_Model_TradeCheckIn extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_in';
    protected $_schema = DATABASE_TRADE;

    public function getAll($params = Null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['st' => 'staff'], [
                         'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                         'check_in_id' => "MAX(c.id)",
                         'staff_id' => 'st.id',
                         'last_check_in' => 'MAX(c.created_at)' 
                     ])
        ->joinLeft(['c' => DATABASE_TRADE.'.check_in'], 'c.staff_id = st.id', [])
                    ->joinLeft(['r' => 'regional_market'], 'st.regional_market = r.id', [])
            ->where('st.title IN (?)', [TRADE_MARKETING_EXECUTIVE,TRADE_MARKETING_LEADER])
            ->where('st.off_date IS NULL')
        ->group('st.id');

        if (! in_array($params['staff_id'], $params['list_special_user'])) {
            if ($params['title'] == TRADE_MARKETING_LEADER) { // trade leader thấy trade local cùng khu vục
//                $select->joinLeft(['staff_trade' => DATABASE_TRADE.'.staff'], 'st.email = staff_trade.email', []);
                $select->joinLeft(['p' => DATABASE_TRADE.'.area_staff'], 'st.id = p.staff_id', []);
                $select->where('p.area_id IN (?)', $params['area_list']);
            } else {
                $select->where('st.id = ?', $params['staff_id']);  // trade local chỉ thấy của chính mình
            }
        }

        if ($params['staff_name']) {
            $select->where('CONCAT(st.firstname, " ",st.lastname) LIKE ?', '%'.$params['staff_name'].'%');
        }
        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;


    }
}    