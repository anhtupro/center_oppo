<?php
class Application_Model_UploadBankAreaTemp extends Zend_Db_Table_Abstract
{
    protected $_name = 'upload_bank_area_temp';

     public function fetchPagination($page, $limit, &$total, $params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.staff_code', 
            'area_id'        => 'p.area_id' , 
            'full_name'    => 'p.full_name',
            'cmnd'      => 'p.CMND',
            'bank_name'          =>'p.bank_name',
            'bank_number'           => 'p.bank_number',
            'bank_province'			=>'p.bank_province',
            'bank_brand'			=> 'p.bank_brand',
            'brand_province'        => 'p.brand_province',
            'note'					=> 'p.note',
            'area_name'				=> 'a.name',
            'created_at'            => 'p.created_at'
        );
        $select->from(array('p'=> 'upload_bank_area_temp'), $arrCols);
        
        $select->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());
       
        
     

         if(!empty($params['list_area'])){
             $select->where('p.area_id IN (?)', $params['list_area']);
         }
        // //TUONG
         if(!empty($params['area_id'])){
             $select->where('p.area_id = ?', $params['area_id']);
         }
         if(!empty($params['fullname'])){
             $select->where('p.full_name LIKE ?', '%'.$params['fullname'].'%');
         }
         if(!empty($params['code'])){
             $select->where('p.staff_code LIKE ?', $params['code']);
         }
         //TUONG
        $select->order("p.id DESC");
        
        if ($_GET['tev'] == 1) {
            echo $select;
            exit;
        }
        if(empty($params['export'])){
        	$select->limitPage($page, $limit);
        }
 
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;

    }

     public function getInfo($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.staff_code', 
            'area_id'        => 'p.area_id' , 
            'full_name'    => 'p.full_name',
            'cmnd'      => 'p.CMND',
            'bank_name'          =>'p.bank_name',
            'bank_number'           => 'p.bank_number',
            'bank_province'         =>'p.bank_province',
            'bank_brand'            => 'p.bank_brand',
            'brand_province'        => 'p.brand_province',
            'note'                  => 'p.note',
            'area_name'             => 'a.name',
            'created_at'            => 'p.created_at'
        );
        $select->from(array('p'=> 'upload_bank_area_temp'), $arrCols);
        
        $select->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.code = p.staff_code', array());

        if(!empty($params['from'])){
            $select->where('p.created_at >= ?', $params['from']);
        }
        if(!empty($params['to'])){
         $select->where('p.created_at <= ?', $params['to']);
        }
     

         if(!empty($params['list_area'])){
             $select->where('p.area_id IN (?)', $params['list_area']);
         }

          if(!empty($params['team'])){
             $select->where('s.team IN (?)', $params['team']);
         }
        // // //TUONG
        //  if(!empty($params['area_id'])){
        //      $select->where('p.area_id = ?', $params['area_id']);
        //  }
        //  if(!empty($params['fullname'])){
        //      $select->where('p.full_name LIKE ?', '%'.$params['fullname'].'%');
        //  }
        //  if(!empty($params['code'])){
        //      $select->where('p.staff_code LIKE ?', $params['code']);
        //  }
         //TUONG
        $select->order("p.id DESC");

        $result = $db->fetchAll($select);
        return $result;

    }

}

