<?php

class Application_Model_InstallmentTmpUpload extends Zend_Db_Table_Abstract
{
    protected $_name = 'installment_tmp_upload';

    public function checkNotEnoughInfo()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.good_code IS NULL')
            ->orWhere('t.prepay_rate_name IS NULL')
            ->orWhere('t.pay_term_name IS NULL')
            ->orWhere('t.interest_name IS NULL')
            ->orWhere('t.channel_name IS NULL')
            ->orWhere('t.from_date IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkPrepayRate()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.prepay_rate_id IS NULL');
        
        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkPayTerm()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.pay_term_id IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkInterest()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.interest_id IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkModelCode()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.good_id IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkFinancier()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.financier_id IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkChannel()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.channel_id IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkPriceLog()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.good_price_log_id IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkTotal()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'DISTINCT(t.excel_row)'
            ])
            ->where('t.channel_id IS NULL')
            ->orWhere('t.financier_id IS NULL')
            ->orWhere('t.prepay_rate_id IS NULL')
            ->orWhere('t.pay_term_id IS NULL')
            ->orWhere('t.interest_id IS NULL')
            ->orWhere('t.from_date IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function updateInfo()
    {
        $db = Zend_Registry::get("db");
        $db->query('
        UPDATE installment_tmp_upload t 
 
        LEFT JOIN installment_financier f ON t.financier_name = f.name
        LEFT JOIN channel ch ON t.channel_name = ch.name
        LEFT JOIN installment_prepay_rate pr ON t.prepay_rate_name = pr.name
        LEFT JOIN installment_pay_term pt ON t.pay_term_name = pt.name
        LEFT JOIN installment_interest i ON t.interest_name = i.name
        LEFT JOIN warehouse.good g ON t.good_code = g.name
        LEFT JOIN warehouse.good_color c ON t.good_color = c.name
        LEFT JOIN warehouse.good_price_log l ON g.id = l.good_id 
                                     AND IFNULL(c.id, 0) = l.color_id
									 AND t.from_date >= l.from_date
                                     AND (t.from_date <= l.to_date OR l.to_date IS NULL)
        
        SET t.financier_id = f.id,
            t.channel_id = ch.id,
                t.prepay_rate_id = pr.id,
                t.pay_term_id = pt.id,
                t.interest_id = i.id,
                t.good_id = g.id,
                t.color_id = IFNULL(c.id,0),
                t.good_price_log_id = l.id
        ');

    }

    public function insertIntoMainTable()
    {
        $db = Zend_Registry::get("db");
        $db->query('
            INSERT INTO hr.installment_package(channel_id, financier_id, good_price_log_id, prepay_rate_id, pay_term_id, interest_id, from_date, to_date, created_at)

            SELECT channel_id, financier_id, good_price_log_id, prepay_rate_id, pay_term_id, interest_id, from_date, to_date, NOW()
            FROM hr.installment_tmp_upload t 
        ');
    }

    public function checkDuplicatePackage()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                't.excel_row',
                'error_channel' => "GROUP_CONCAT(DISTINCT t.channel_name)"
            ])
            ->join(['i' => DATABASE_CENTER . '.installment_package'], 't.channel_id = i.channel_id 
                                                                        AND t.financier_id = i.financier_id 
                                                                        AND t.prepay_rate_id = i.prepay_rate_id 
                                                                        AND t.pay_term_id = i.pay_term_id 
                                                                        AND t.interest_id = i.interest_id 
                                                                        AND t.from_date >= i.from_date 
                                                                        AND (t.from_date <= i.to_date OR i.to_date IS NULL)'
                                                                        , [])
            ->group('t.excel_row');
         
        $result = $db->fetchAll($select);

        return $result;
    }

    public function checkDuplicatePackageInFile()
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['t' => DATABASE_CENTER . '.installment_tmp_upload'], [
                'excel_row' => "GROUP_CONCAT(DISTINCT i.excel_row SEPARATOR '-')"
            ])
            ->join(['i' => DATABASE_CENTER . '.installment_tmp_upload'], 't.channel_id = i.channel_id 
                                                                        AND t.financier_id = i.financier_id 
                                                                        AND t.prepay_rate_id = i.prepay_rate_id 
                                                                        AND t.pay_term_id = i.pay_term_id 
                                                                        AND t.interest_id = i.interest_id 
                                                                        AND t.from_date >= i.from_date 
                                                                        AND (t.from_date <= i.to_date OR i.to_date IS NULL)'
                , [])
            ->group('t.id')
            ->having('COUNT(*) > 1');

        $select = $db->select()
            ->from(['t' => $sub_select], [
                'excel_row' =>new Zend_Db_Expr('DISTINCT t.excel_row'),
            ]);

        $result = $db->fetchCol($select);

        return $result;
    }
}