<?php
class Application_Model_PurchasingSupplier extends Zend_Db_Table_Abstract
{
	protected $_name = 'supplier';

	function fetchSupplier($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['title']) and $params['title'])
            $select->where('p.title LIKE ?', '%' . $params['title'] . '%');

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%' . $params['name'] . '%');

        if (isset($params['address']) and $params['address'])
            $select->where('p.address LIKE ?', '%' . $params['address'] . '%');

        if (isset($params['surrogater_name']) and $params['surrogater_name'])
            $select->where('p.surrogater_name LIKE ?', '%' . $params['surrogater_name'] . '%');

        if (isset($params['surrogater_phone']) and $params['surrogater_phone'])
            $select->where('p.surrogater_phone LIKE ?', '%' . $params['surrogater_phone'] . '%');

        if (isset($params['email']) and $params['email'])
            $select->where('p.email LIKE ?', '%' . $params['email'] . '%');

        if (isset($params['group_product']) and $params['group_product'])
            $select->where('p.group_product = (?)',  $params['group_product']);

        if (isset($params['product_services']) and $params['product_services'])
            $select->where('p.product_services LIKE ?', '%' . $params['product_services'] . '%');

        $select->where('p.del = ?', 0);
        $select->order('p.title', 'COLLATE utf8_unicode_ci ASC');

        
        if(empty($params['export'])){
            if ($limit)
                $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
    function getContructorsList(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => "p.id",
            'name' => "p.name",
        );

        $select->from(array('p' => DATABASE_TRADE.'.contructors'), $arrCols);

        $result = $db->fetchAll($select);

        return $result;
    }
    

}