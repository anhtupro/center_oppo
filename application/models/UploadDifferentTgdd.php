<?php

class Application_Model_UploadDifferentTgdd extends Zend_Db_Table_Abstract
{
    protected $_name = 'upload_different_tgdd';

    public function getInvalidImei()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'u.imei'
            ])
            ->joinLeft(['i' => WAREHOUSE_DB . '.imei'], 'u.imei = i.imei_sn', [])
            ->where('i.imei_sn IS NULL');

        $result = $db->fetchCol($select);

        return $result;

    }

    public function getInvalidPartner($array_distributor_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'u.partner_id'
            ])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'u.partner_id = d.partner_id AND (d.del = 0 OR d.del IS NULL)', [])
            ->where('d.partner_id IS NULL')
            ->orwhere('d.id NOT IN (?) AND d.parent NOT IN (?)', $array_distributor_id, $array_distributor_id)
            ->group('u.partner_id');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function getInvalidDistributor()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'u.partner_id'
            ])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'u.partner_id = d.partner_id AND (d.del = 0 OR d.del IS NULL)', [])
            ->joinLeft(['s' => 'store'], 'd.id = s.d_id AND (s.del IS NULL OR s.del = 0)', [])
            ->where('u.partner_id IS NOT NULL')
            ->where('s.id IS NULL')
             ->group('u.partner_id');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function updateStoreIdAndType($params)
    {
        $db = Zend_Registry::get('db');
        $stmt_out = $db->prepare("CALL `sp_update_upload_different_tgdd`(:p_from_date, :p_to_date)");
        $stmt_out->bindParam('p_from_date', $params['from_date'], PDO::PARAM_STR);
        $stmt_out->bindParam('p_to_date', $params['to_date'], PDO::PARAM_STR);
        $stmt_out->execute();
        $stmt_out->closeCursor();
    }

    public function getImeiNotTiming()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'u.imei',
                'u.timing_date',
                'store_name' => 's.name',
                'store_id' => 's.id',
                'u.partner_id',

                'activated_at' => 'i.activated_date',
                'model' => 'g.desc',
                'color' => 'c.name',
                'area_name' => 'a.name'
            ])
            ->joinLeft(['s' => 'store'], 'u.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])

            ->joinLeft(['i' => WAREHOUSE_DB. '.imei'], 'u.imei = i.imei_sn', [])
            ->joinLeft(['g' => WAREHOUSE_DB. '.good'], 'i.good_id = g.id', [])
            ->joinLeft(['c' => WAREHOUSE_DB. '.good_color'], 'i.good_color = c.id ', [])

            ->where('u.type = ?', 1);

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getImeiNotInFile($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => 'timing'], [
                'ts.imei',
                'model' => 'g.desc',
                'color' => 'c.name',
                'activated_at' => 'i.activated_date',
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'st.email',
                'timing_date' => 't.from',
                'store_name' => 's.name',
                'partner_id' => 'd.partner_id',
                'area_name' => 'a.name'

            ])
            ->join(['ts' => 'timing_sale'], 't.id = ts.timing_id', [])
            ->joinLeft(['u' => 'upload_different_tgdd'], 'ts.imei = u.imei', [])
            ->joinLeft(['s' => 'store'], 't.store = s.id', [])

            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])

            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = s.d_id', [])
            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'ts.product_id = g.id', [])
            ->joinLeft(['c' => WAREHOUSE_DB . '.good_color'], 'ts.model_id = c.id', [])
            ->joinLeft(['i' => WAREHOUSE_DB . '.imei'], 'ts.imei = i.imei_sn', [])
            ->joinLeft(['st' => 'staff'], 't.staff_id = st.id', [])
            ->where('t.from >= ?', $params['from_date'])
            ->where('t.from <= ?', $params['to_date'])
            ->where('d.id IN (?) OR d.parent IN (?)', $params['array_distributor_id'], $params['array_distributor_id'])
            ->where('u.imei IS NULL');

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getHasTimingAndDuplicate($params, $type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'ts.imei',
                'model' => 'g.desc',
                'color' => 'c.name',
                'activated_at' => 'i.activated_date',

                'staff_name_1' => "CONCAT(st1.firstname, ' ', st1.lastname)",
                'email_1' => 'st1.email',
                'timing_date_1' => 't.from',
                'store_name_1' => 's1.name',
                'partner_id_1' => 'd1.partner_id',
                'area_name_1' => 'a1.name',

                'staff_name_2' => "CONCAT(st2.firstname, ' ', st2.lastname)",
                'email_2' => 'st2.email',
                'timing_date_2' => 'd.date',
                'store_name_2' => 's2.name',
                'partner_id_2' => 'd2.partner_id',
                'area_name_2' => 'a2.name',

                'timing_date' => 'u.timing_date',
                'store_name' => 's.name',
                'partner_id' => 'u.partner_id',
                'area_name' => 'af.name'
            ])
            ->joinLeft(['s' => 'store'], 's.id = u.store_id', [])
            ->join(['ts' => 'timing_sale'], 'u.imei = ts.imei', [])
            ->join(['t' => 'timing'], 'ts.timing_id = t.id', [])
            ->join(['d' => 'duplicated_imei'], 'ts.imei = d.imei AND d.asm_check = 1 AND d.solved = 0', [])
            ->joinLeft(['dif' => 'regional_market'], 's.district = dif.id', [])
            ->joinLeft(['rf' => 'regional_market'], 'dif.parent = rf.id', [])
            ->joinLeft(['af' => 'area'], 'af.id = rf.area_id', [])

            ->joinLeft(['st1' => 'staff'], 'st1.id = t.staff_id', [])
            ->joinLeft(['s1' => 'store'], 't.store = s1.id', [])
            ->joinLeft(['d1' => WAREHOUSE_DB . '.distributor'], 'd1.id = s1.d_id', [])
            ->joinLeft(['di1' => 'regional_market'], 's1.district = di1.id', [])
            ->joinLeft(['r1' => 'regional_market'], 'di1.parent = r1.id', [])
            ->joinLeft(['a1' => 'area'], 'a1.id = r1.area_id', [])


            ->joinLeft(['st2' => 'staff'], 'st2.id = d.staff_id', [])
            ->joinLeft(['s2' => 'store'], 'd.store_id = s2.id', [])
            ->joinLeft(['d2' => WAREHOUSE_DB . '.distributor'], 'd2.id = s2.d_id', [])
            ->joinLeft(['di2' => 'regional_market'], 's2.district = di2.id', [])
            ->joinLeft(['r2' => 'regional_market'], 'di2.parent = r2.id', [])
            ->joinLeft(['a2' => 'area'], 'a2.id = r2.area_id', [])


            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'ts.product_id = g.id', [])
            ->joinLeft(['c' => WAREHOUSE_DB . '.good_color'], 'ts.model_id = c.id', [])
            ->joinLeft(['i' => WAREHOUSE_DB . '.imei'], 'ts.imei = i.imei_sn', [])
            ->where('u.type = ?', $type)
            ->where('d.date >= ?', $params['from_date']);
//            ->where('d.date <= ?', $params['to_date']);

        $result = $db->fetchAll($select);
        return $result;

    }

    public function exportImeiNotTiming()
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data_not_timing = $this->getImeiNotTiming();

        $heads = array(
            'Imei',
            'Model',
            'Store',
            'Store_id',
            'Partner_id',
            'Sellout date',
            'Activated date',
            'Area'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $title) {
            $sheet->setCellValue($alpha . $index, $title);
            $alpha++;
        }


        $index = 2;

        $i = 1;

        foreach ($data_not_timing as $item) {

            $alpha = 'A';
            $sheet->setCellValueExplicit($alpha++ . $index, $item['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $item['model'] . '/' . $item['color']);
            $sheet->setCellValue($alpha++ . $index, $item['store_name']);
            $sheet->setCellValue($alpha++ . $index, $item['store_id']);
            $sheet->setCellValue($alpha++ . $index, $item['partner_id']);
            $sheet->setCellValue($alpha++ . $index, $item['timing_date'] ? date('Y-m-d', strtotime($item['timing_date'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['activated_at'] ? date('Y-m-d', strtotime($item['activated_at'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);


            $index++;

        }

        $filename = 'File Check ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        ob_start();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        $xlsData = ob_get_contents();
        ob_end_clean();

        $url_file = "data:application/vnd.ms-excel;base64," . base64_encode($xlsData);

        return $url_file;
    }

    public function exportImeiNotInFile($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data_not_timing = $this->getImeiNotInFile($params);

        $heads = array(
            'Imei',
            'Model',
            'Activated date',
            'Staff name',
            'Email',
            'Timing date',
            'Store',
            'Partner_id',
            'Area'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $title) {
            $sheet->setCellValue($alpha . $index, $title);
            $alpha++;
        }


        $index = 2;

        $i = 1;

        foreach ($data_not_timing as $item) {

            $alpha = 'A';
            $sheet->setCellValueExplicit($alpha++ . $index, $item['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $item['model'] . '/' . $item['color']);
            $sheet->setCellValue($alpha++ . $index, $item['activated_at'] ? date('Y-m-d', strtotime($item['activated_at'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['staff_name']);
            $sheet->setCellValue($alpha++ . $index, $item['email']);
            $sheet->setCellValue($alpha++ . $index, $item['timing_date'] ? date('Y-m-d', strtotime($item['timing_date'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['store_name']);
            $sheet->setCellValue($alpha++ . $index, $item['partner_id']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);

            $index++;

        }

        $filename = 'File Check không tồn tại trong file FPT ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        ob_start();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        $xlsData = ob_get_contents();
        ob_end_clean();

        $url_file = "data:application/vnd.ms-excel;base64," . base64_encode($xlsData);

        return $url_file;
    }

    public function exportHasTimingAndDuplicate($params, $type)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data_not_timing = $this->getHasTimingAndDuplicate($params, $type);

        $heads = array(
            'Imei',
            'Model',
            'Activated date',

            '1st Staff name',
            '1st Email',
            '1st Timing date',
            '1st Store',
            '1st Partner_id',
            '1st Area',

            '2nd Staff name',
            '2nd Email',
            '2nd Timing date',
            '2nd Store',
            '2nd Partner_id',
            '2nd Area',

            'File Timing date',
            'File Store',
            'File Partner_id',
            'File Area',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $title) {
            $sheet->setCellValue($alpha . $index, $title);
            $alpha++;
        }


        $index = 2;

        $i = 1;

        foreach ($data_not_timing as $item) {

            $alpha = 'A';
            $sheet->setCellValueExplicit($alpha++ . $index, $item['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $item['model'] . '/' . $item['color']);
            $sheet->setCellValue($alpha++ . $index, $item['activated_at'] ? date('Y-m-d', strtotime($item['activated_at'])) : '');

            $sheet->setCellValue($alpha++ . $index, $item['staff_name_1']);
            $sheet->setCellValue($alpha++ . $index, $item['email_1']);
            $sheet->setCellValue($alpha++ . $index, $item['timing_date_1'] ? date('Y-m-d', strtotime($item['timing_date_1'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['store_name_1']);
            $sheet->setCellValue($alpha++ . $index, $item['partner_id_1']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name_1']);

            $sheet->setCellValue($alpha++ . $index, $item['staff_name_2']);
            $sheet->setCellValue($alpha++ . $index, $item['email_2']);
            $sheet->setCellValue($alpha++ . $index, $item['timing_date_2'] ? date('Y-m-d', strtotime($item['timing_date_2'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['store_name_2']);
            $sheet->setCellValue($alpha++ . $index, $item['partner_id_2']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name_2']);

            $sheet->setCellValue($alpha++ . $index, $item['timing_date'] ? date('Y-m-d', strtotime($item['timing_date'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['store_name']);
            $sheet->setCellValue($alpha++ . $index, $item['partner_id']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);

            $index++;

        }

        $filename = 'File Check có báo số + có duplicate + ko giống store ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        ob_start();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        $xlsData = ob_get_contents();
        ob_end_clean();

        $url_file = "data:application/vnd.ms-excel;base64," . base64_encode($xlsData);

        return $url_file;
    }

    public function insertDuplicate($type)
    {
        $db = Zend_Registry::get('db');

        $query = "
        INSERT INTO duplicated_imei(staff_id, imei, date, shift, store_id, note, created_at, timing_sales_first, staff_id_first, asm_check, asm_id, asm_at)
        
        SELECT 3025, u.imei, u.timing_date, 1, u.store_id, 'Tool upload auto insert duplicate', NOW(), ts.id, t.staff_id, 1, 341, CURDATE()
        FROM upload_different_tgdd u
        JOIN timing_sale ts ON u.imei = ts.imei
        JOIN timing t ON ts.timing_id = t.id 
        
        WHERE u.type = " . $type;

        $stmt = $db->prepare($query);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;

        return;
    }

    public function getListImei($type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'u.imei'
            ])
            ->where('u.type = ?', $type);

        $result = $db->fetchCol($select);

        $str_list_imei = implode("\n", $result);

        return $str_list_imei;

    }


    public function exportImeiHasManyDuplicate($list_imei)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data_not_timing = $this->getImeiNotTiming();

        $heads = array(
            'Imei'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $title) {
            $sheet->setCellValue($alpha . $index, $title);
            $alpha++;
        }


        $index = 2;

        $i = 1;

        foreach ($list_imei as $imei) {

            $alpha = 'A';
            $sheet->setCellValueExplicit($alpha++ . $index, $imei, PHPExcel_Cell_DataType::TYPE_STRING);
            $index++;

        }

        $filename = 'Imei phải xủ lý tay ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        ob_start();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        $xlsData = ob_get_contents();
        ob_end_clean();

        $url_file = "data:application/vnd.ms-excel;base64," . base64_encode($xlsData);

        return $url_file;
    }

    public function getImeiHasSolved($list_type, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'u.imei',
                'staff_name_1' => "CONCAT(st1.firstname, ' ', st1.lastname)",
                'email_1' => 'st1.email',
                'staff_name_2' => "CONCAT(st2.firstname, ' ', st2.lastname)",
                'email_2' => 'st2.email',
                'staff_win' => "IF(d.staff_win = d.staff_id_first, 1, IF(d.staff_win = d.staff_id , 2, ''))"
            ])
            ->join(['d' => 'duplicated_imei'], 'u.imei = d.imei AND d.solved = 1 AND d.staff_id <> 3025', [])
            ->joinLeft(['st1' => 'staff'], 'd.staff_id_first = st1.id', [])
            ->joinLeft(['st2' => 'staff'], 'd.staff_id = st2.id', [])


            ->where('u.type IN (?)', $list_type)
            ->where('d.date >= ?', $params['from_date'])
            ->where('d.date <= ?', $params['to_date'])
            ->group('u.imei')
            ->having("COUNT(u.imei) = 1");

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportImeiHasSolved($list_type, $params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getImeiHasSolved($list_type, $params);

        $heads = array(
            'Imei',
            '1st staff name',
            '1st email',
            '2nd staff name',
            '2nd email',
            'Staff win'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $title) {
            $sheet->setCellValue($alpha . $index, $title);
            $alpha++;
        }


        $index = 2;

        $i = 1;

        foreach ($data as $item) {

            $alpha = 'A';
            $sheet->setCellValueExplicit($alpha++ . $index, $item['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $item['staff_name_1']);
            $sheet->setCellValue($alpha++ . $index, $item['email_1']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_name_2']);
            $sheet->setCellValue($alpha++ . $index, $item['email_2']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_win']);

            $index++;

        }

        $filename = 'Imei hệ thống auto chấm duplicate ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        ob_start();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        $xlsData = ob_get_contents();
        ob_end_clean();

        $url_file = "data:application/vnd.ms-excel;base64," . base64_encode($xlsData);

        return $url_file;

    }


    public function getHasTimingWrongStore()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'upload_different_tgdd'], [
                'u.imei',
                'store_name_timing' => 'sb.name' ,
                'store_id_timing' => 'sb.id',
                'partner_id_timing' => 'd.partner_id',
                'staff_timing' => "CONCAT(st.firstname, ' ', st.lastname)",
                'staff_email_timing' => 'st.email',
                'timing_date' => 't.from',

                'store_name_file' => 's.name',
                'store_id_file' => 's.id',
                'partner_id_file' => 'u.partner_id',
                'timing_date_file' => 'u.timing_date',

                'activated_at' => 'i.activated_date',
                'model' => 'g.desc',
                'color' => 'c.name',

                'area_name_file' => 'af.name',
                'area_name_timing' => 'at.name'

            ])
            ->joinLeft(['s' => 'store'], 'u.store_id = s.id', []) // store file
            ->joinLeft(['dif' => 'regional_market'], 's.district = dif.id', [])
            ->joinLeft(['rf' => 'regional_market'], 'dif.parent = rf.id', [])
            ->joinLeft(['af' => 'area'], 'af.id = rf.area_id', [])

            ->joinLeft(['i' => WAREHOUSE_DB. '.imei'], 'u.imei = i.imei_sn', [])
            ->joinLeft(['g' => WAREHOUSE_DB. '.good'], 'i.good_id = g.id', [])
            ->joinLeft(['c' => WAREHOUSE_DB. '.good_color'], 'i.good_color = c.id ', [])
            ->joinLeft(['ts' => 'timing_sale'], 'u.imei = ts.imei', [])
            ->joinLeft(['t' => 'timing'], 'ts.timing_id = t.id', [])
            ->joinLeft(['sb' => 'store'], 't.store = sb.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = sb.d_id', [])
            ->joinLeft(['st' => 'staff'], 't.staff_id = st.id', [])

            ->joinLeft(['dit' => 'regional_market'], 'sb.district = dit.id', [])
            ->joinLeft(['rt' => 'regional_market'], 'dit.parent = rt.id', [])
            ->joinLeft(['at' => 'area'], 'at.id = rt.area_id', [])


            ->where('u.type = ?', 3);

        $result = $db->fetchAll($select);
        return $result;

    }


    public function exportHasTimingWrongStore()
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getHasTimingWrongStore();

        $heads = array(
            'Imei',
            'Model',
            'Ngày activated imei',

            'Ngày báo số',
            'Store báo số',
            'Store_id báo số',
            'Partner_id báo số',
            'Area báo số',
            'Staff báo số',
            'Email staff báo số',

            'Ngày báo số File',
            'Store File',
            'Store_id File',
            'Partner_id File',
            'Area File'


        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $title) {
            $sheet->setCellValue($alpha . $index, $title);
            $alpha++;
        }


        $index = 2;

        $i = 1;

        foreach ($data as $item) {
            $alpha = 'A';
            $sheet->setCellValueExplicit($alpha++ . $index, $item['imei'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++ . $index, $item['model'] . '/' . $item['color']);
            $sheet->setCellValue($alpha++ . $index, $item['activated_at'] ? date('Y-m-d', strtotime($item['activated_at'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['timing_date'] ? date('Y-m-d', strtotime($item['timing_date'])) : '');

            $sheet->setCellValue($alpha++ . $index, $item['store_name_timing']);
            $sheet->setCellValue($alpha++ . $index, $item['store_id_timing']);
            $sheet->setCellValue($alpha++ . $index, $item['partner_id_timing']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name_timing']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_timing']);
            $sheet->setCellValue($alpha++ . $index, $item['staff_email_timing']);

            $sheet->setCellValue($alpha++ . $index, $item['timing_date_file'] ? date('Y-m-d', strtotime($item['timing_date_file'])) : '');
            $sheet->setCellValue($alpha++ . $index, $item['store_name_file']);
            $sheet->setCellValue($alpha++ . $index, $item['store_id_file']);
            $sheet->setCellValue($alpha++ . $index, $item['partner_id_file']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name_file']);



            $index++;

        }

        $filename = 'File Check ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        ob_start();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        $xlsData = ob_get_contents();
        ob_end_clean();

        $url_file = "data:application/vnd.ms-excel;base64," . base64_encode($xlsData);

        return $url_file;
    }




}