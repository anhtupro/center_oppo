<?php
class Application_Model_PurchasingCodeCategory extends Zend_Db_Table_Abstract
{
    protected $_name = 'pmodel_code_category';

    function fetchPage($page, $limit, &$total, $params){

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "category_name" => "p.name", 
            "code_id" => "code.id", 
            "code_name" => "code.name",
            "code.category_id", 
            "category_code_name" => "category.name", 
            "type_name" => "(CASE WHEN map.type = 1 THEN 'Thi công' WHEN map.type = 2 THEN 'Sửa chữa' WHEN map.type = 3 THEN 'Điều chuyển' WHEN map.type = 4 THEN 'Tiêu hủy' END)",
        );

        $select->from(array('p' => DATABASE_TRADE.'.category'), $arrCols);
        $select->joinLeft(array('map' => 'pmodel_code_category'), 'map.category_id = p.id', array());
        $select->joinLeft(array('code' => 'pmodel_code'), 'code.id = map.code_purchasing_id', array());
        $select->joinLeft(array('category' => 'pcategory'), 'category.id = code.category_id', array());
        
        if(!empty($params['category_name'])){
            $select->where('p.name LIKE ?', '%'.$params['category_name'].'%');
        }
        
        if(!empty($params['code_name'])){
            $select->where('code.name LIKE = ?', '%'.$params['code_name'].'%');
        }
        
        if(!empty($params['category_code_name'])){
            $select->where('category.name LIKE = ?', '%'.$params['category_code_name'].'%');
        }
        
        if(!empty($params['type'])){
            $select->where('map.type = ?', $params['type']);
        }
        
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getPurchasingCode($category_id, $type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['p' => 'pmodel_code_category'], [
                        'p.code_purchasing_id'
                     ])
        ->where('p.category_id = ?', $category_id)
        ->where('p.type = ?', $type);

        $result = $db->fetchOne($select);

        return $result;
    }
    
}