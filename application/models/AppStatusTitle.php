<?php
class Application_Model_AppStatusTitle extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_status_title';
    protected $_schema = DATABASE_TRADE;

    function get_cache($type){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.$type.'_cache');

        if ($result === false) {

            $where = $this->getAdapter()->quoteInto('type = ?', $type);
            $data = $this->fetchAll($where);

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->title][] = $item->status_id;
                }
            }
            $cache->save($result, $this->_name.$type.'_cache', array(), null);
        }

        return $result;
    }


    function get_cache_title($type){
        
        $where = $this->getAdapter()->quoteInto('type = ?', $type);
        $data = $this->fetchAll($where);

        $result = array();
        if ($data){
            foreach ($data as $item){
                $result[$item->title][] = $item->status_id;
            }
        }

        return $result;
    }

    public function getStaffToNotify($type, $status, $area_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.app_status_title'], [
                'f.staff_id'
            ])
            ->join(['st' => 'staff'], 'a.title = st.title', [])
            ->join(['f' => DATABASE_TRADE.'.area_staff'], 'st.id = f.staff_id', [])
            ->where('a.type = ?', $type)
            ->where('a.status_id = ?', $status)
            ->where('f.area_id IN (?)', $area_id)
            ->where('st.off_date IS NULL OR st.off_date = 0')
            ->where('f.not_send_notification IS NULL')
            ->group('st.id');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function checkPermission($title, $status, $type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.app_status_title'], [
                'a.id'
            ])
            ->where('a.type = ?', $type)
            ->where('a.status_id = ?', $status)
            ->where('a.title = ?', $title);
        $result = $db->fetchOne($select);

        return $result ? true : false;

    }


}