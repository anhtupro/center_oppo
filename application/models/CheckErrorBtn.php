<?php

class Application_Model_CheckErrorBtn extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_error_btn';
    protected $_schema = DATABASE_TRADE;

    public function isStoreErrorBtn($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE . '.store_error_btn'], [
                's.store_id'
            ])
            ->where('s.store_id = ?', $store_id);

        $result = $db->fetchOne($select);

        return $result ? true : false;
    }

    public function getListErrorBtn()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['l' => DATABASE_TRADE . '.error_btn'], [
                'l.*'
            ])
        ->order('l.ordering');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCurrentError($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE . '.check_error_btn'], [
                'c.error_id'
            ])
            ->where('c.store_id = ?', $store_id);
        
        $result = $db->fetchCol($select);

        return $result;
                             
    }

    public function getCurrentErrorNote($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE . '.check_error_btn'], [
                'c.error_id',
                'c.note'
            ])
            ->where('c.store_id = ?', $store_id);

        $result = $db->fetchAll($select);

        $list = [];
        
        foreach ($result as $item) {
            $list [$item['error_id']] = $item['note'];    
        }

        return $list;

    }


    public function getCurrentErrorName($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE . '.check_error_btn'], [
                'error' => 'e.name',
                'c.note'
            ])
            ->joinLeft(['e' => DATABASE_TRADE . '.error_btn'], 'c.error_id = e.id', [])
            ->where('c.store_id = ?', $store_id);

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getDataExport($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE . '.store_error_btn'], [
                'store_id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
                'store_name' => 's.name',
                'district' => 'di.name',
                'province' => 'r.name',
                'area' => 'a.name',
                'store_address' => 's.shipping_address',
                'error' => 'e.name',
                'd.note'
            ])
            ->joinLeft(['s' => 'store'], 's.id = c.store_id', [])
            ->joinLeft(['d' => DATABASE_TRADE . '.check_error_btn'], 'c.store_id = d.store_id', [])
            ->joinLeft(['e' => DATABASE_TRADE . '.error_btn'], 'd.error_id = e.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])

            ->where('s.del IS NULL OR s.del = 0')
            ->where('a.id IN (?)', $params['list_area'])
            ->order('c.store_id');

        if ($params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if ($params['store_name']) {
            $select->where('s.name LIKE ?', "%". TRIM($params['store_name']) . "%");
        }

        if ($params['big_area_id']) {
            $select->where('a.bigarea_id = ?', $params['big_area_id']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($params['regional_market']) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if($params['district']) {
            $select->where('di.id = ?', $params['district']);
        }

        if ($_GET['dev'] == 1) {
            echo $select; die;
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function export($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getDataExport($params);


        $heads = array(
            'Store ID',
            'Store name',
            'Store address',
            'District',
            'Province',
            'Area',
            'Lỗi',
            'Ghi chú'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }

        $index    = 2;
        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['store_address']);
            $sheet->setCellValue($alpha++.$index, $item['district']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['error']);
            $sheet->setCellValue($alpha++.$index, $item['note']);

            $index++;
        }

        $filename = 'Danh sách Lỗi BTN 3.0';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
}