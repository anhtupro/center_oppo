<?php

class Application_Model_SalesRegionLog extends Zend_Db_Table_Abstract{

	protected $_name = 'sales_region_log';

	public function _insert($params = array()){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('
			INSERT INTO sales_region_log
			(
				SELECT
					null AS id, id AS sales_region_id, parent, region_id,
					shared, type, from_date, to_date, :staff_id AS staff_id,
					:action AS action, :date AS date
				FROM sales_region WHERE id = :id
			);
		');
		$stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('action', $params['action'], PDO::PARAM_STR);
		$stmt->bindParam('date', $params['date'], PDO::PARAM_STR);
		$res = $stmt->execute();
		$stmt->closeCursor();
		$db = $stmt = null;
		return $res;
	}

}