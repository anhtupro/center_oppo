<?php

class Application_Model_RankStarSales extends Zend_Db_Table_Abstract
{
    protected $_name = 'rank_star_sales';

    function fetchPagination($page, $limit, &$total, $params)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => 'staff'), array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',

            ))
            ->joinLeft(array('p' => 'rank_star_sales'), 's.code = p.staff_code', array("staff_id" => "s.id","s.regional_market","s.lastname","s.firstname","s.title","s.email","s.phone_number"));
             $select->joinLeft(array('rmk' => 'regional_market'), 's.regional_market = rmk.id', array("rmk.area_id"));
            $select->where("s.title IN ( ".SALES_TITLE.",".SALES_LEADER_TITLE.")");
            $select->where("s.off_date IS NULL");
            
            
        if(!$params["rank"] ){
            $select->where("p.star = ? ",0);
        }else{
            $select->where("p.star = ? ",$params["rank"]);
        }
        if(!empty($params["area_id"]) AND $params["area_id"] ){
           

            $select->where("rmk.area_id = ? ",$params["area_id"]);
        }
        if(!empty($params["bigarea_id"]) AND $params["bigarea_id"] ){
//            $select->joinLeft(array('area' => 'area'), 'area.bigarea_id = rmk.area_id', array());
//
//            $select->where("area.bigarea_id = ? ",$params["bigarea_id"]);
        }
        if(!empty($params["code"]) AND $params["code"] ){
            $select->where("s.code = ? ",$params["code"]);
        }

        if(!empty($params['area_list']))
        {
            $select->where('rmk.area_id IN (?)', $params['area_list']);
        }

        if ($limit)
            $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    function getRankStarRegion($params=null){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => 'staff'), array(
                'p.*',
                "quantity" => "COUNT(s.id)",
            ))
            ->joinLeft(array('p' => 'rank_star_sales'), 's.code = p.staff_code', array("staff_id" => "s.id","s.regional_market","s.lastname","s.firstname","s.title"))
            ->joinLeft(array('rmk' => 'regional_market'), 'rmk.id = s.regional_market', array("rmk.area_id"))
            ->joinLeft(array('area' => 'area'), 'area.id = rmk.area_id', array("area.id","area.bigarea_id"))
            ->joinLeft(array('ba' => 'bigarea'), 'ba.id = area.bigarea_id', array());
            $select->where("s.title IN ( ".SALES_TITLE.",".SALES_LEADER_TITLE.")");
            if(isset($params["rank"])){
                $select->where("p.star = ? ", $params["rank"]);
                $select->group("area.bigarea_id");
                
            }else{
                $select->group("p.star");
            }
            if(!empty($params['area_list']))
            {
                $select->where('area.id IN (?)', $params['area_list']);
            }
            $select->order('ba.sort');
        $result = $db->fetchAll($select);

        return $result;
    }
    public function getRankStarArea($params) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => 'staff'), array(
                'p.*',
                "quantity" => "COUNT(s.id)",
            ))
            ->joinLeft(array('p' => 'rank_star_sales'), 's.code = p.staff_code', array("staff_id" => "s.id","s.regional_market","s.lastname","s.firstname","s.title"))
            ->joinLeft(array('rmk' => 'regional_market'), 'rmk.id = s.regional_market', array("rmk.area_id"))
            ->joinLeft(array('area' => 'area'), 'area.id = rmk.area_id', array("area.bigarea_id"));
            $select->where("s.title IN ( ".SALES_TITLE.",".SALES_LEADER_TITLE.")");
        $select->where("p.star = ? ",$params["rank"]);
        if(!empty($params['area_list']))
            {
                $select->where('area.id IN (?)', $params['area_list']);
            }

        if(!empty($params["bigarea_id"]) AND $params["bigarea_id"] ){
            $select->where("area.bigarea_id = ? ",$params["bigarea_id"]);
        }
        $select->group("rmk.area_id");
        $result = $db->fetchAll($select);
        return $result;
    }

}