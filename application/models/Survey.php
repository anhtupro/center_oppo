<?php
class Application_Model_Survey extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey';

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['filter']) && $params['filter']) {
            $select_filter = array(
                'c_area'       => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = a.id AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::AREA)),
                'c_department' => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = s.department AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::DEPARTMENT)),
                'c_team'       => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = s.team AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::TEAM)),
                'c_title'      => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = s.title AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::TITLE)),
                'c_staff'      => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = s.id AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::STAFF)),
                'c_all'        => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = 1 AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::ALL_STAFF)),
            );

            $select->joinLeft(array('o' => 'survey_object'), 'o.survey_id=p.id', $select_filter)
                ->joinLeft(array('s' => 'staff'), '1=1', array())
                ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array())
                ->join(array('a' => 'area'), 'a.id=r.area_id', array())
                ->joinLeft(array('sr' => 'survey_response'), 'sr.staff_id = s.id AND sr.survey_id = p.id', array('response' => 'sr.survey_id'));
        }

        if (isset($params['id']) and $params['id'])
            $select->where('p.id = ?', $params['id']);

        if (isset($params['title']) and $params['title'])
            $select->where('p.title LIKE ?', '%'.$params['title'].'%');

        if (isset($params['content']) and $params['content'])
            $select->where('p.content LIKE ?', '%'.$params['content'].'%');

        // filter who can see the notification
        if (isset($params['filter']) && $params['filter'])
            $select->having('(c_area > ? AND c_department > ? AND c_team > ? AND c_title > ?) OR c_staff > ? OR c_all > ?', 0);
        ////////////////////////////

        if (isset($params['response'])) {
            if ($params['response'])
                $select->where('sr.survey_id IS NOT NULL', 1);
            else
                $select->where('sr.survey_id IS NULL', 1);
        }

        if (isset($params['created_from']) && $params['created_from']) {
            $select->where('p.created_at >= ?', $params['created_from']);
        }

        if (isset($params['created_to']) && $params['created_to']) {
            $select->where('p.created_at <= ?', $params['created_to']);
        }

        if (isset($params['filter']) && $params['filter']) {
            $today =  date('Y-m-d H:i:s');
            $select->where('p.show_from IS NULL OR p.show_from <= ?', $today);
            $select->where('p.show_to IS NULL OR p.show_to >= ?', $today);
        }

        if (isset($params['staff_id']) && $params['staff_id'])
            $select->where('s.id = ?', $params['staff_id']);

        if (isset($params['status']) && $params['status'])
            $select->where('p.status = ?', $params['status']);

        $select->group('p.id');

        if(isset($params['sort']) && $params['sort']) {
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            $collate = ' COLLATE utf8_unicode_ci ';
            $order_str = 'p.`'.$params['sort']. '` ' . $collate . $desc;

            $select->order(new Zend_Db_Expr($order_str));
        } else {
            $select->order('created_at DESC');
        }

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function fetchDetail($id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => $this->_name), array('s_id' => 's.id', 's_title' => 's.title', 's_content' => 's.content'))
            ->join(array('sq' => 'survey_question'), 's.id=sq.survey_id', array('sq_id' => 'sq.id', 'sq_question' => 'sq.question'))
            ->join(array('sqo' => 'survey_question_option'), 'sq.id=sqo.survey_question_id', array('sqo_id' => 'sqo.id', 'sqo_option' => 'sqo.option'));

        $select->order(array('s.created_at', 'sq.order', 'sqo.order'));

        $data = array();
        $result = $db->fetchAll($select);

        if ($result) {
            foreach ($result as $key => $value) {
                // $data[ $value['s_id'] ][ $value[] ]
            }
        }
    }
    public function getSelectedTitleChannel($survey_id){
        //SELECT so.object_id,GROUP_CONCAT(channel_id) from survey s left join survey_object  so ON s.id=so.survey_id left join survey_object_channel soc on soc.object_id=so.object_id where s.status=1  group by so.object_id
         $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('s' => $this->_name),
                array());
        $select->joinLeft(array('so' => 'survey_object'),'s.id=so.survey_id',array('title' =>'so.object_id'));
        $select->joinLeft(array('soc' => 'survey_object_channel'),'soc.object_id=so.object_id AND soc.survey_id=so.survey_id',array('channel_id' =>'GROUP_CONCAT(channel_id)'));
        $select->where('s.status = ?',1);
        $select->where('s.id <> ?',$survey_id);
        $select->group('so.object_id');
        $select->having('channel_id is not null');


        $result = $db->fetchAll($select);
        $arrayTmp=array();
        foreach($result as $key => $value){
            if(!empty($value['channel_id'])){
                $arrayTmp[$value['title']]=  explode(',', $value['channel_id']);
            }
        }

        // if($_GET['dev']){
        //     echo "<pre>";
        //     echo $select->__toString();
        //     print_r($result);

        //     print_r($arrayTmp);
        // }
        return $arrayTmp;
    }
}
