<?php

class Application_Model_JobRegional extends Zend_Db_Table_Abstract {

    protected $_name = 'job_regional';

    public function init() {
        error_reporting(-1);
        require_once 'My' . DIRECTORY_SEPARATOR . 'nusoap' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nusoap.php';
        $this->wssURI    = 'https://vieclam.oppomobile.vn/wss/index?wsdl';
        $this->namespace = 'REALME';
    }

    function insertWebservice($data) {
       
        $client                   = new nusoap_client($this->wssURI);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8      = false;
        try {

            $data         = base64_encode(serialize($data));
            $currentTime  = time();
            $token_string = 'cuongdethuong' . $currentTime;
            $token        = md5($token_string);
            $wsParams     = array(
                'data'  => $data,
                'token' => $token,
                'auth'  => $currentTime
            );

            $result = $client->call("wsRegional", $wsParams);
           
            $err    = $client->getError();
            if ($err) {
                echo "wsRegional error: " . $err;
                exit();
            }


            return array('code' => 0, 'message' => $result['message']);
        } catch (exception $e) {
            return array('code' => -1, 'message' => $e->getMessage());
        }

        return $result;
    }

    function get_regional_by_id($id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => $this->_name), array('b.id', 'b.name')
                )
                ->joinLeft(array('b' => 'regional_market'), 'b.id = a.regional_market_id', array())
                ->joinLeft(array('c' => 'province'), 'b.province_id = c.id', array('name_province' => 'c.name'))
                ->where('a.job_id = ?', $id)
                ->group('c.id');
        $result = $db->fetchAll($select);
        return $result;
    }

    function get_regional() {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'job'), array('b.*')
                )
                ->joinLeft(array('b' => 'job_regional'), 'a.id = b.job_id', array())
                ->where('a.status = ?', 1)
                ->where('DATE(a.to) >= DATE(NOW())', '');
        $result = $db->fetchAll($select);
        return $result;
    }

}
