<?php

class Application_Model_StaffWorkFromHome extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_work_from_home';
    public function checkStaffWFH($staff_id){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p' => $this->_name));
        $select->where('p.staff_id  = ?',$staff_id);
        $result = $db->fetchRow($select);
        return $result;
    }
}