<?php

class Application_Model_CheckShopOutsideDetailBrand extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_outside_detail_brand';
    protected $_schema = DATABASE_TRADE;

    public function getDetail($checkshopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.check_shop_outside_detail_brand'], [
                         'category_id' => 'd.category_id',
                         'category_name' => 'c.name',
                         'quantity' => 'd.quantity',
                         'brand_id' => 'b.id',
                         'brand_name' => 'b.name'
                     ])
        ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
        ->joinLeft(['b' => 'brand'], 'd.brand_id = b.id', [])
        ->where('d.checkshop_id = ? ', $checkshopId);

        $result = $db->fetchAll($select);

        return $result;
    }
}