<?php
class Application_Model_AppraisalOfficeTask extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_task';
    protected $_primary = 'aot_id';
}