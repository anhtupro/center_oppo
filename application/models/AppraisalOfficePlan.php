<?php
class Application_Model_AppraisalOfficePlan extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_plan';
    protected $_primary = 'aop_id';

    public function getActiveQuarterPlan()
    {
        return $this->fetchAll([
            $this->getAdapter()->quoteInto('aop_is_deleted = ?', 0), // Still available
            $this->getAdapter()->quoteInto('aop_type = ?', 1), // Running
            $this->getAdapter()->quoteInto('aop_status = ?', 1), // Running
            $this->getAdapter()->quoteInto("aop_from <= '?'", date('Y-m-d')),
            $this->getAdapter()->quoteInto("aop_to >= '?'", date('Y-m-d')),
        ])->toArray();
    }

    public function getPlanNotifyAvailable()
    {
        return $this->fetchAll([
            $this->getAdapter()->quoteInto('aop_is_deleted = ?', 0), // Still available
            $this->getAdapter()->quoteInto('aop_status = ?', 1), // Running
            $this->getAdapter()->quoteInto("aop_to <= '?'", date('Y-m-d', strtotime('+10 days'))),
            $this->getAdapter()->quoteInto("aop_to >= '?'", date('Y-m-d')),
        ])->toArray();
    }

    public function getActiveCapacityPlan()
    {
        return $this->fetchAll([
            $this->getAdapter()->quoteInto('aop_is_deleted = ?', 0), // Still available
            $this->getAdapter()->quoteInto('aop_type = ?', 2), // Running
            $this->getAdapter()->quoteInto('aop_status = ?', 1), // Running
            $this->getAdapter()->quoteInto("aop_from <= ?", date('Y-m-d')),
            $this->getAdapter()->quoteInto("aop_to >= ?", date('Y-m-d')),
        ])->toArray();
    }
    
    public function getMaxPlanId($notPlan)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'aop_id' => "MAX(aop_id)",
        );

        $select->from(array('p' => 'appraisal_office_plan'), $arrCols);

        $select->where('p.aop_id <> ?', $notPlan);
        $select->where('p.aop_is_deleted = 0', NULL);
        
        $result = $db->fetchRow($select);

        return $result['aop_id'];
    }
    
    public function getListPlan($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'aop_id',
            'aop_to',
            'aop_name',
            'aop_name_eng',
            'aop_from',
            'aop_init',
            'aop_status',
            'aop_type',
            'aop_survey_id',
            'aop_is_deleted',
            'aop_created_at'
        );

        $select->from(array('p' => 'appraisal_office_plan'), $arrCols);

        $select->where('p.aop_type = ?', 1);
        $select->where('p.aop_init = ?', 1);
        $select->where('p.aop_is_deleted = ?', 0);
        
        $select->where('p.is_year IS NULL OR p.is_year = 0');
        
        $select->order('p.aop_created_at DESC');
        $select->limitPage(0,4);
        
        $main_select = $db->select();
        $main_select->from(array('p' => $select), '*');
        $main_select->order('p.aop_created_at ASC');
        
        $result = $db->fetchAll($main_select);

        return $result;
        
    }
    
    
    public function checkActivePlan($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.aop_id', 
            'p.aop_name', 
            'm.aom_staff_id', 
            'm.aom_staff_approved', 
            'm.aom_head_approved',
        );

        $select->from(array('p' => 'appraisal_office_plan'), $arrCols);
        $select->joinLeft(array('m' => 'appraisal_office_member'), 'm.aop_id = p.aop_id', array());

        $select->where('p.aop_type = 1 AND p.aop_is_deleted = 0 AND p.aop_status = 1', NULL);
        $select->where('m.aom_staff_approved = 0', NULL);
        $select->where('m.aom_staff_id = ?', $staff_id);
        
        $result = $db->fetchRow($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getListPlanStaff($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.aop_id',
            'p.aop_status',
            'p.aop_name', 
            'p.aop_from', 
            'p.aop_to', 
            'CURDATE()', 
            "set_prd" => "IF(CURDATE() <= p.aop_to, 1, 0)",
            'm.aom_id',
            'm.aom_staff_id',
            'm.aom_staff_approved',
            'm.aom_head_approved',
            'f.aof_id',
            't.aot_id'
        );

        $select->from(array('p' => 'appraisal_office_plan'), $arrCols);
        $select->joinLeft(array('m' => 'appraisal_office_member'), 'm.aop_id = p.aop_id', array());
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = m.aom_id', array());
        $select->joinLeft(array('t' => 'appraisal_office_task'), 't.aof_id = f.aof_id', array());

        $select->where('p.aop_init = 1 AND p.aop_type = 1 AND p.aop_is_deleted = 0', NULL);
        $select->where('m.aom_is_deleted = 0', NULL);
        $select->where('m.aom_staff_id = ?', $staff_id);
        $select->where('f.aof_is_deleted = 0', NULL);
        $select->where('t.aot_id IS NOT NULL', NULL);
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        if($_GET['dev'] == 1){
            echo $select;exit;
        }
        
        return $result;
    }
    
    function getLastPlan($plan_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "aop_id"    => "p.aop_id",
            "aop_name"  => "p.aop_name",
        );

        $select->from(array('p' => 'appraisal_office_plan'), $arrCols);

        $select->where("p.aop_id < ?", $plan_id);
        $select->where("p.aop_to >= ?", date('Y-m-d'));
        $select->where('p.aop_type = ?', 1);
        $select->where("p.aop_is_deleted = ?", 0);
        $select->where('p.is_year IS NULL OR p.is_year <> ?', 1);
        
        $select->order("p.aop_id DESC");
        
        $result = $db->fetchRow($select);

        return $result;
    }
}