<?php

class Application_Model_AdditionCategory extends Zend_Db_Table_Abstract
{
    protected $_name = 'addition_category';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params )
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['l' => DATABASE_TRADE . '.addition_category'], [
                'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS l.id'),
                'l.*',
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'store_name' => 's.name',
                'area_name' => 'a.name'
            ])
            ->joinLeft(['s' => 'store'], 's.id = l.store_id', [])
            ->joinLeft(['di' => 'regional_market'], 'di.id = s.district', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
            ->joinLeft(['st' => 'staff'], 'l.created_by = st.id', []);

        if ($params['list_area']) {
            $select->where('a.id IN (?)', $params['list_area']);
        }
        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($params['store_name']) {
            $select->where('s.name LIKE ?', '%' . $params['store_name'] . '%');
        }

        if ($params['status']) {
            $select->where('l.status = ?',  $params['status'] );
        }

        if ($params['remove']) {
            $select->where('l.remove = ?',  1 );
        }

        $select->limitPage($page, $limit);
        $select->order('l.id DESC');
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getInfo($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.addition_category'], [
                 'a.*',
                'store_name' => 's.name',
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['st' => 'staff'], 'a.created_by = st.id', [])
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getDetail($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.addition_category'], [
                'd.quantity',
                'category_name' => 'c.name',
                'reason_name' => 'r.name',
            ])
            ->joinLeft(['d' => DATABASE_TRADE . '.addition_category_detail'], 'd.addition_id = a.id', [])
            ->joinLeft(['c' => DATABASE_TRADE. '.category'], 'd.category_id = c.id', [])
            ->joinLeft(['r' => DATABASE_TRADE. '.addition_category_reason'], 'd.reason_id = r.id', [])
            ->where('a.id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getImage($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.addition_category'], [
                'd.*'
            ])
            ->joinLeft(['d' => DATABASE_TRADE . '.addition_category_image'], 'd.addition_id = a.id', [])
            ->where('a.id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;

    }

    public function insertToShop($store_id, $array_category)
    {

        $QAppCheckshop = new Application_Model_AppCheckshop();
        $QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();
        $QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $checkshop_lock = $QAppCheckshop->fetchRow([
            'store_id = ?' => $store_id,
            'is_lock = ?' => 1
        ]);
     
        if (! $checkshop_lock) {
            $checkshop_id = $QAppCheckshop->insert([
               'store_id' => $store_id,
               'is_lock' => 1,
               'created_by' => $userStorage->id,
               'created_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            $checkshop_id = $checkshop_lock['id'];
        }

        foreach ($array_category as $category) {
            $category_id = $category['category_id'];
            $quantity = $category['quantity'];

            $exist_category = $QAppCheckshopDetail->fetchRow([
                'checkshop_id = ?' => $checkshop_id,
                'category_id = ?' => $category_id
            ]);

            // nếu có sẵn hạng mục trong bãng app_checkshop_detail rồi thì update số lượng, nếu chưa thì insert
            if ($exist_category) {
                $QAppCheckshopDetail->update([
                    'quantity' => $exist_category['quantity'] + $quantity
                ], ['id = ?' => $exist_category['id']]);
            } else {
                $QAppCheckshopDetail->insert([
                    'checkshop_id' => $checkshop_id,
                    'category_id' => $category_id,
                    'quantity' => $quantity
                ]);
            }

            // insert vào app_checkshop_detail_child
            for ($i=0; $i < $quantity; $i++) {
                $QAppCheckshopDetailChild->insert([
                    'checkshop_id' => $checkshop_id,
                    'category_id' => $category_id,
                    'quantity' => 1
                ]);
            }

        }


    }

}