<?php
 //thong test db
class Application_Model_PurchasingRatingSurveyDetail extends Zend_Db_Table_Abstract{
    protected $_name = "purchasing_rating_survey_detail";
    
    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(
                        array('p'=>'purchasing_rating_survey_detail'),
                        array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                                'p.*',
                                'rating_staff_name'     => "CONCAT(t1.firstname,  ' ',t1.lastname)",
                                '_staff_name'           => "CONCAT(t2.firstname,  ' ',t2.lastname)",
                                'rating_survey_name'    => 'sd.name'
                            ))
                ->joinLeft(array('sd'   =>'purchasing_rating_survey_detail'),'p.survey_id = sd.id',array())
                ->joinLeft(array('t1'   =>'staff')                          ,'t1.id = p.id_rating_staff'     ,array())
                ->joinLeft(array('t2'   =>'staff')                          ,'t2.id = p.staff_id'     ,array())
                ->order('p.id DESC');        
      
        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        return $result;
        
    }
    
    function getData($params){
        $db = Zend_Registry::get('db');

        $col = array(
            'p.id',
            'p.name',
           
        );
        $select = $db-> select()->from(array('p' => $this->_name), $col);

        if(!empty($params['id'])){
            $select->where('p.id = ?', $params['id']);
        }
        $select->group('p.id');
        //$select->order('p.created_at DESC');
        $result = $db->fetchRow($select);
        return $result;
    }
}





