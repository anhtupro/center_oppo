<?php
class Application_Model_Maxim extends Zend_Db_Table_Abstract
{
	protected $_name = 'maxim';

        function get_cache(){
            $cache      = Zend_Registry::get('cache');
            $result     = $cache->load($this->_name.'_cache');

            if ($result === false) {

                $date            = date('d');
                $db = Zend_Registry::get('db');

                $select = $db->select()->from(array('p' => 'maxim'),array('p.*'));
                $select->where('p.`active` = 1',null);
                $data 	=	$db->fetchAll($select); 

                $result = array();
                if ($data){
                    foreach ($data as $item){
                        $result[$item['date']] = $item['text'];
                    }
                }
                $cache->save($result, $this->_name.'_cache', array(), null);
            }

            return $result;
        }
}