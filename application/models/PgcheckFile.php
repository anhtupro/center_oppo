<?php
class Application_Model_PgcheckFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'pgcheck_file';
    
    public function getIdImagePg($store_id, $type){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.store_id", 
            "p.channel_id", 
            "c.id" 
        );

        $select->from(array('p' => 'v_channel_store'), $arrCols);
        $select->joinLeft(array('c' => 'pgcheck_channel_file'), 'c.channel_id = p.channel_id', array());
        
        $select->where("p.store_id = ?", $store_id);
        $select->where("c.is_del = 0 AND c.type = ?", $type);

        $result = $db->fetchRow($select);
        
        return $result;
    }
    
    public function getImagePgcheckdate($pgcheck_date_id, $type){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.pgcheck_date_id", 
            "p.url", 
            "p.type", 
            "p.channel_file_id", 
            "url_channel" => "f.url"  
        );

        $select->from(array('p' => 'pgcheck_file'), $arrCols);
        $select->joinLeft(array('f' => 'pgcheck_channel_file'), 'f.id = p.channel_file_id', array());
        
        $select->where("p.pgcheck_date_id = ?", $pgcheck_date_id);
        $select->where("p.type = ?", $type);

        $result = $db->fetchRow($select);
        
        return $result;
    }
    
    
}