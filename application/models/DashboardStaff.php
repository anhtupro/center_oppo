<?php
class Application_Model_DashboardStaff extends Zend_Db_Table_Abstract
{
    protected $_name = 'dashboard_staff';

    public function fetchStaffById($dashboard_id, array $fields)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('d' => $this->_name), array())
        ->join(array('s' => 'staff'), 's.id=d.staff_id', is_array($fields) ? $fields : array())
        ->order('d.add_time_unix ASC');

        return $db->fetchAll($select);
    }
}
