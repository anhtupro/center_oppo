<?php
class Application_Model_TitleMappingReport extends Zend_Db_Table_Abstract
{
	protected $_name = 'title_mapping_report';

	function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

        	$db = Zend_Registry::get('db');

        	$select = $db->select()
                ->from(array('s' => $this->_name),
                    array('s.*'))
                ->join(array('t' => 'team_report'), 's.team_report_id = t.id', array('team_name' => 't.name'));
           

            $data = $db->fetchAll($select);
           
            // echo "<pre>";print_r($data);die;
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item['title_id']] = $item['team_name'].' - '.$item['name'];
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
}