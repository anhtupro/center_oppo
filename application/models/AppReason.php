<?php
class Application_Model_AppReason extends Zend_Db_Table_Abstract
{
	protected $_name = 'app_reason';
    protected $_schema = DATABASE_TRADE;
    public function GetAll(){

       $db = Zend_Registry::get('db');
       $select = $db->select()
       ->from(array('R' => DATABASE_TRADE.'.'.$this->_name),array('id'=>'R.id','name'=>'R.name'));

       $result = $db->fetchAll($select);
       return $result;
   }
}