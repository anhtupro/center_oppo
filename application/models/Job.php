<?php

class Application_Model_Job extends Zend_Db_Table_Abstract {

    protected $_name = 'job';

    public function init() {
        error_reporting(-1);
        require_once 'My' . DIRECTORY_SEPARATOR . 'nusoap' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nusoap.php';
        $this->wssURI    = 'https://vieclam.oppomobile.vn/wss/index?wsdl';
        $this->namespace = 'REALME';
    }

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['title']) and $params['title'])
            $select->where('p.title LIKE ?', '%' . $params['title'] . '%');

        if (isset($params['status']) and $params['status']) {

            if ($params['status'] == 1) {
                $select->where('p.status = ?', $params['status']);
                $select->where('p.to >= NOW()', NULL);
            } elseif ($params['status'] == 2) {
                $select->where('p.status = ?', $params['status']);
                $select->where('p.to >= NOW()', NULL);
            } elseif ($params['status'] == 3) {
                $select->where('p.status = ?', $params['status']);
                $select->where('p.to >= NOW()', NULL);
            } elseif ($params['status'] == 4) {
                $select->where('p.status = 1', NULL);
                $select->where('p.to <= NOW()', NULL);
            }
        }

        if (isset($params['category']) and $params['category'])
            $select->where('p.category = ?', $params['category']);

        if (isset($params['content']) and $params['content'])
            $select->where('p.content LIKE ?', '%' . $params['content'] . '%');

        if (isset($params['province']) and $params['province'])
            $select->where('p.regional_market_id = ? ', $params['content']);


        $select->joinLeft(array('b' => 'job_regional'), 'p.id = b.job_id', array('job_regional_id' => 'b.id'));
        $select->joinLeft(array('c' => 'regional_market'), 'b.regional_market_id = c.id', array('regional_market_list' => 'GROUP_CONCAT(c.`name`)'));
        $select->joinLeft(array('d' => 'area'), 'c.area_id = d.id', array('area_list' => 'GROUP_CONCAT(DISTINCT  d.`name`)'));
        $select->joinLeft(array('e' => 'team'), 'p.category = e.id', array('name_category' => 'e.name'));
        $select->joinLeft(array('cv' => new Zend_Db_Expr('(SELECT a.id,a.title,count(b.id) AS count_cv, SUM(CASE WHEN ((b.status = 0 OR b.status IS NULL) AND b.id IS NOT NULL) THEN 1 ELSE 0 END) count_cv_view
                	FROM job as a
                	LEFT JOIN cv as b On b.job_id = a.id
                	GROUP BY a.id,a.title)')), 'cv.id = p.id', array('count_cv' => 'cv.count_cv', 'count_cv_view' => 'cv.count_cv_view'));

        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('b.id IS NULL OR c.id IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('b.id IS NULL OR c.id = ?', intval($params['list_province_ids']));
            }
        }
        
        if (isset($params['area']) and $params['area']){
            $select->where('d.id IN (?) ', $params['area']);
        }
        else{
            if($userStorage->group_id != HR_ID){
                $select->where('p.team = ?', $userStorage->team);
            }
        }
            

        if (isset($params['location']) and $params['location'] )
            $select->where('c.id = ? ', $params['location']);

        $select->group('p.id');


        $select->order('p.created_at DESC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function fetchAllJob($params) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'job'), array('a.*')
                )
                ->joinLeft(array('b' => 'job_regional'), 'a.id = b.job_id', array())
                ->joinLeft(array('c' => 'regional_market'), 'b.regional_market_id = c.id', array())
                ->joinLeft(array('d' => 'province'), 'c.province_id = d.id', array('province' => 'GROUP_CONCAT(d.name)'))
                ->where('a.status IN (?)', [1,3])
                ->where('DATE(a.to) >= DATE(NOW())', '');

        if (isset($params['name']) and $params['name'])
            $select->where('a.title like ?', '%' . $params['name'] . '%');

        if (isset($params['location']) and $params['location']) {
            //$select->where('b.regional_market_id = ?', $params['location']);
            $select->where('(d.code = ? OR b.regional_market_id IS NULL)', $params['location']);
        }

        if (isset($params['category']) and $params['category']) {
            $select->where('a.category = ?', $params['category']);
        }
        if (isset($params['job_group']) and $params['job_group']) {
            $select->where('a.job_group = ?', $params['job_group']);
            
        }

        $select->group('a.id');
        $select->order(array('a.created_at DESC'));
        
        if($params['job_group'] == 3){
            //$select->where('a.id NOT IN (?)', [646]);
            //return strval($select);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }

    function get_cache() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'title');

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name . '_cache', array(), null);
        }
        return $result;
    }
    // get_job function have the same return value as get_cache
    function get_job() {
            $data = $this->fetchAll(null, 'title');

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = $item->title;
                }
            }
        return $result;
    }

    function fetchSendMailList() {
        $return_arr  = array();
        $db          = Zend_Registry::get('db');
        $mail_select = $db->select();
        $mail_select->from(array('c' => 'cv'),
                        // array('c.id','COUNT(c.id)')
                        array()
                )
                ->joinLeft(
                        array('j' => 'job'), 'c.job_id = j.id', array(
                    'j.email',
                    'j.title',
                    'j.id',
                    'j.created_by'
                        )
                )
                ->joinLeft(
                        array('s' => 'staff'), 'j.created_by = s.id', array(
                    'CONCAT(s.firstname," ",s.lastname) AS fullname'
                        )
                )
                ->where('j.email IS NOT NULL')
                ->where("c.status = '' OR c.status = ?", 0)
                // ->where('j.status = ?',1)
                ->group('j.id');

        // get email to sent
        $emails = $db->fetchAll($mail_select);
        foreach ($emails as $item) {
            $job_id         = $item['id'];
            $this_approveCv = $this->getApproveCV($job_id);
            if (count($this_approveCv) != 0) {
                $return_arr[] = [
                    'infor' => $item,
                    'cvs'   => $this_approveCv
                ];
            }
        }
        return $return_arr;
    }

    function getApproveCV($job_id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('c' => 'cv'), array("c.*")
                )
                ->joinLeft(
                        array('l' => 'log_job_email'), 'c.id = l.cv_id', array()
                )
                ->where('c.job_id = ?', $job_id)
                ->where('c.status IS NULL OR c.status = ?', 0)
                ->where('l.cv_id IS NULL');
        return $db->fetchAll($select);
        // $QCV = new Application_Model_CV;
        // $where[] = $QCV->getAdapter()->quoteInto('job_id = ?', $job_id);
        // $where[] = $QCV->getAdapter()->quoteInto('status = ?', 0);
        // echo "<pre>";
        // var_dump($QCV->fetchAll($where)->toArray());
        // die();
        // return $QCV->fetchAll($where)->toArray();
    }

    function selectJobWebservice($data) {
//        var_dump($data);die;
        $client                   = new nusoap_client($this->wssURI);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8      = false;
        try {

            $data         = base64_encode(serialize($data));
            $currentTime  = time();
            $token_string = 'cuongdethuong' . $currentTime;
            $token        = md5($token_string);
            $wsParams     = array(
                'data'  => $data,
                'token' => $token,
                'auth'  => $currentTime
            );
            $result = $client->call("wsInsertJob", $wsParams);
            $err    = $client->getError();
            if ($err) {
                echo "Constructor error: " . $err;
                exit();
            }
            
            return array('code' => 0, 'message' => $result['message']);
        } catch (exception $e) {
            return array('code' => -1, 'message' => $e->getMessage());
        }

        return $result;
    }

}
