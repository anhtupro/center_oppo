<?php

class Application_Model_AppCheckshop extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_checkshop';

    protected $_schema = DATABASE_TRADE;

    public function getExcelMerge($params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            'checkshop.id',
            'store_name' => 's.name',
            'store_id' => 's.id',
            'dealer_id' => 's.d_id',
            'partner_id' => 's.partner_id',
            'checkshop_last' => "checkshop.updated_at",
            'checkshop_id' => "checkshop.id",
            'soluong' => "detail.quantity",
            'imei' => "detail.imei_sn",
            'category_name' => "cat.name",
            'fullname' => "CONCAT(staff.firstname, ' ', staff.lastname)",
            'is_lock' => "a.is_lock",
            'channel' => "CASE WHEN (l.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (l.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END",
            'area_name' => "area.name"
        );


        $nestedSelect = $db->select();
        $arrCols_nest = [
            'p.id',
            'p.store_id',
            'p.latitude',
            'p.longitude',
            'p.updated_at'
        ];
        $nestedSelect->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols_nest);
        $nestedSelect->where('p.id IN (SELECT MAX(p.id) FROM trade_marketing.app_checkshop p WHERE p.updated_at IS NOT NULL GROUP BY p.store_id)');


        $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'area.id = r.area_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = l.loyalty_plan_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_checkshop'), 'a.store_id = p.store_id AND a.is_lock = 1', array());
        $select->joinLeft(array('checkshop' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 'checkshop.store_id = p.store_id', array());
        $select->joinLeft(array('detail' => DATABASE_TRADE . '.app_checkshop_detail'), 'detail.checkshop_id = checkshop.id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE . '.category'), 'cat.id = detail.category_id', array());

        $select->where('p.is_leader = 1');
        $select->where('p.released_at IS NULL');
        $select->where('s.del IS NULL OR s.del = 0');

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }


        $select->order('detail.id DESC', 'p.store_id');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getStoreCheckshop($params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            'checkshop.id',
            'store_name' => 'p.name',
            'store_id' => 'p.id',
            'dealer_id' => 'p.d_id',
            'partner_id' => 'p.partner_id',
            'shipping_address' => 'p.shipping_address',
            'dealer_name' => 'd.name',
            'is_ka' => 'd.is_ka',
            'checkshop_last' => "checkshop.updated_at",
            'checkshop_id' => "checkshop.id",
            'soluong' => "detail.quantity",
            'imei' => "detail.imei_sn",
            'category_name' => "cat.name",
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            'is_lock' => "a.is_lock",
            'is_leader_lock' => "a2.is_leader_lock",
            'channel' => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END",
            'area_name' => "area.name",
            "parent_id" => "IF(d.parent = 0, d.id, d.parent)",
            'province' => 'r.name',
            'district' => 'district.name',
            'store_level' => 'sl.title'
        );


        $nestedSelect = $db->select();
        $arrCols_nest = [
            'p.id',
            'p.store_id',
            'p.latitude',
            'p.longitude',
            'p.updated_at'
        ];
        $nestedSelect->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols_nest);
        $nestedSelect->where('p.id IN (SELECT MAX(p.id) FROM trade_marketing.app_checkshop p WHERE p.updated_at IS NOT NULL GROUP BY p.store_id)');


        $select->from(array('p' => 'store'), $arrCols);
        $select->joinLeft(array('district' => 'regional_market'), 'p.district = district.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'district.parent = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'area.id = r.area_id', array());
        $select->joinLeft(array('l' => 'v_store_staff_leader_log'), 'l.store_id = p.id AND l.released_at IS NULL AND l.is_leader = 1', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_checkshop'), 'a.store_id = p.id AND a.is_lock = 1', array());
        $select->joinLeft(array('a2' => DATABASE_TRADE . '.app_checkshop'), 'a2.store_id = p.id AND a2.is_leader_lock = 1', array());
        $select->joinLeft(array('checkshop' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 'checkshop.store_id = p.id', array());
        $select->joinLeft(array('detail' => DATABASE_TRADE . '.app_checkshop_detail'), 'detail.checkshop_id = a.id AND detail.quantity > 0', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE . '.category'), 'cat.id = detail.category_id', array());
        $select->joinLeft(array('si' => 'store_level_info'), 'p.id = si.store_id AND si.default = 1', array());
        $select->joinLeft(array('sl' => 'store_level'), 'sl.id = si.level_id', array());

        $select->where('p.del IS NULL OR p.del = 0');

        if (!empty($params['staff_id'])) {
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('loyalty.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('loyalty.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('loyalty.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }



        $select->order('p.id');
//        $select->limit(30000);
        $result = $db->fetchAll($select);

        return $result;
    }


    public function getExcel($params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.staff_id',
            'store_name' => 's.name',
            'checkshop_last' => "MAX(c.created_at)",
            'checkshop_id' => "MAX(c.id)",
            'store_id' => 'c.store_id',
            'category_name' => 'f.name',
            'k.category_id',
            'k.quantity',
            'k.imei_sn',
            'f.group_bi'
        );


        $nestedSelect = "SELECT p.id, p.store_id, MAX(p.created_at), p.created_at
                        FROM trade_marketing.app_checkshop p
                        GROUP BY p.store_id";

        $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());

        $select->joinLeft(array('c' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 'c.store_id = s.id', array());

        $select->joinLeft(array('k' => DATABASE_TRADE . '.app_checkshop_detail'), 'k.checkshop_id = c.id', array());
        $select->joinLeft(array('f' => DATABASE_TRADE . '.category'), 'f.id = k.category_id', array());
        $select->where('p.released_at IS NULL ', NULL);
        $select->where('s.del is NULL or s.del= 0', NULL);

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));
            $select->where('DATE(c.created_at) >= ?', $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(c.created_at) <= ?', $to_date);
        }

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        $select->group('k.id');
        $select->order('checkshop_id', 'ASC');
        $result = $db->fetchAll($select);

        //echo $select; exit;
        //echo $result; exit;
        return $result;
    }


    public function fetchPagination_bk($page, $limit, &$total, $params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.staff_id',
            'store_name' => 's.name',
            'checkshop_last' => "MAX(c.updated_at)",
            'checkshop_id' => "MAX(c.id)",
            'store_id' => 'p.store_id',
            'is_lock' => 'a.is_lock'
        );

        $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.store_id = s.id AND c.updated_at IS NOT NULL AND c.status = 1', array());
        $select->joinLeft(array('m' => DATABASE_TRADE . '.app_checkshop_map_qc'), 'c.id = m.checkshop_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_checkshop'), 'a.store_id = p.store_id AND a.is_lock = 1', array());
        $select->where('p.released_at IS NULL ', NULL);
        $select->where('s.del is NULL or s.del= 0', NULL);

        if (!empty($params['name_search'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_search'] . '%');
        }

        if (!empty($params['key_search'])) {
            $select->where('s.id = ?', $params['key_search']);
        }
        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(c.updated_at) >= ?', $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(c.updated_at) <= ?', $to_date);
        }


        //TUONG
        if (!empty($params['checkshop_qc_selectbox'])) {
            $select->where('m.qc_id IN (?)', $params['checkshop_qc_selectbox']);
            $select->where('m.type = ?', $params['type_qc']);

            $select->where('MONTH(c.updated_at) = ?', $params['month']);
            $select->where('YEAR(c.updated_at) = ?', $params['year']);
        }
        //TUONG
        $select->group('s.id');
        $select->order("MAX(c.updated_at) DESC");

        if ($_GET['sev'] == 1) {
            echo $select;
            exit;
        }

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $from_date_time = $params['from_date_time'];
        $to_date_time = $params['to_date_time'];
        $team_trade_marketing = TEAM_TRADE_MARKETING;
        $title_trade_local = TRADE_MARKETING_EXECUTIVE;

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $sub_select_count_checkshop_sales = "
       	SELECT 
        h.store_id,
        COUNT(DISTINCT h.legal_date) as count_legal_checkshop
    
        FROM 
        (
            SELECT 
    
            @legal_date :=
            IF (g.store_id <> @store_id  , g.date_checkshop , 
                IF( DATE_SUB(g.date_checkshop ,INTERVAL 1 DAY) >= @legal_date, g.date_checkshop, @legal_date)
            ) as legal_date,
            
            @store_id := g.store_id as store_id
            
            FROM 
            (
                SELECT a.store_id,
                DATE(a.created_at) as date_checkshop
                
                FROM trade_marketing.app_checkshop a 
                LEFT JOIN hr.staff st ON a.created_by = st.id 
                
                WHERE (a.created_at BETWEEN '$from_date_time' AND '$to_date_time')
                AND st.team <> '$team_trade_marketing'
                
                GROUP BY a.store_id, date_checkshop 
                ORDER BY a.store_id, date_checkshop ASC
            ) g
            
            JOIN (SELECT @store_id := 0, @legal_date := '0000-00-00') t
        ) h
    
        GROUP BY h.store_id

        ";



        $sub_select_count_checkshop_trade_local = "
       	SELECT 
        h.store_id,
        COUNT(DISTINCT h.legal_date) as count_legal_checkshop
    
        FROM 
        (
            SELECT 
    
            @legal_date :=
            IF (g.store_id <> @store_id  , g.date_checkshop , 
                IF( DATE_SUB(g.date_checkshop ,INTERVAL 1 DAY) >= @legal_date, g.date_checkshop, @legal_date)
            ) as legal_date,
            
            @store_id := g.store_id as store_id
            
            FROM 
            (
                SELECT a.store_id,
                DATE(a.created_at) as date_checkshop
                
                FROM trade_marketing.app_checkshop a 
                LEFT JOIN hr.staff st ON a.created_by = st.id 
                
                WHERE (a.created_at BETWEEN '$from_date_time' AND '$to_date_time')
                AND st.title = '$title_trade_local'
                
                GROUP BY a.store_id, date_checkshop 
                ORDER BY a.store_id, date_checkshop ASC
            ) g
            
            JOIN (SELECT @store_id := 0, @legal_date := '0000-00-00') t
        ) h
    
        GROUP BY h.store_id

        ";


        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
//            'sale.staff_id',
            'store_name' => 's.name',
            'checkshop_last' => "c.created_at",
            'checkshop_id' => "c.id",
            'store_id' => 's.id',
            'is_lock' => 'a.is_lock',
            'is_leader_lock' => 'a.is_leader_lock',
            'y.error_image',
            'y.is_repair',
            'd.is_ka',
            'store_temp_stop' => 'w.status',
            'store_flash_sale' => 'GROUP_CONCAT(f.flash_sale_id)',
            'l.loyalty_plan_id',

            'store_done_checkshop' => "IF(ch.count_legal_checkshop >= 2, 1, 0)",
            'ch.count_legal_checkshop',
            'count_legal_checkshop_trade_local' => 'cht.count_legal_checkshop'
        );


        $select->from(array('s'=> 'store'), $arrCols);
//        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('district' => 'regional_market'), 's.district = district.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'district.parent = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.store_id = s.id AND c.is_last = 1', array());
//        $select->joinLeft(array('m' => DATABASE_TRADE . '.app_checkshop_map_qc'), 'c.id = m.checkshop_id', array());

//        $select->joinLeft(array('a' => new Zend_Db_Expr('(' . $select_checkshop_lock . ')')), 'a.store_id = s.id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_checkshop'), 'a.store_id = s.id AND a.is_lock = 1', array());

        $select->joinLeft(array('i' => DATABASE_TRADE . '.error_image_shop_current'), 's.id = i.store_id', array());
        $select->joinLeft(array('y' => DATABASE_TRADE . '.check_shop_entire'), 's.id = y.store_id AND y.is_last = 1', array());
        $select->joinLeft(array('w' => DATABASE_TRADE . '.store_temp_stop'), 'w.store_id = s.id', array());
        $select->joinLeft(array('f' => DATABASE_TRADE . '.store_flash_sale'), 'f.store_id = s.id AND f.is_del = 0', array());
        $select->joinLeft(['ch' => new Zend_Db_Expr('(' . $sub_select_count_checkshop_sales . ')')], 's.id = ch.store_id', []);
        $select->joinLeft(['cht' => new Zend_Db_Expr('(' . $sub_select_count_checkshop_trade_local . ')')], 's.id = cht.store_id', []);

        $select->where('s.del is NULL or s.del = 0', NULL);



        if ($params['type_staff'] == 1 || $params['staff_id']) { // nếu là sale , pg sup, search theo sale
            $sub_select_sale = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['staff_id'])
                ->where('l.is_leader IN (?)', [1, 8]) // sale , PGs supervisor
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');

            // select main
            $select->join(['sale' => $sub_select_sale], 's.id = sale.store_id', []);
        }


        if ($params['type_staff'] == 2) { // nếu là sale leader
            $sub_select_leader = $db->select()
                ->from(['l' => 'store_leader_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['leader_id'])
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');


            $sub_select_sale = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['leader_id'])
                ->where('l.is_leader IN (?)', [1]) // sale
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');

            $sub_select_leader_union = $db->select()->union(array($sub_select_leader, $sub_select_sale));


            // select main
            $select->join(['leader' => $sub_select_leader_union], 's.id = leader.store_id', []);
        }

        if ($params['type_staff'] == 3) { // các title còn lại
            $select->where('area.id IN (?)', $params['list_area']);
        }

        if ($params['legal_checkshop'] == 1) { // đã check đủ
            $select->where('ch.count_legal_checkshop >= ?', 2);
        }

        if ($params['legal_checkshop'] == 2) { // chưa check đủ
            $select->where('ch.count_legal_checkshop < ? OR ch.count_legal_checkshop IS NULL', 2);
        }


        if ($params['from_date_sellout'] && $params['to_date_sellout'] && $params['quantity_sellout']) {
            $sub_select_sellout = $db->select()
                ->from(['km' => 'kpi_by_model'], [
                    'km.store_id',
                    'quantity_sellout' => "SUM(km.qty)"
                ])
                ->where('km.timing_date >= ?', $params['from_date_sellout'])
                ->where('km.timing_date <= ?', $params['to_date_sellout'])
                ->group('km.store_id')
                ->having("SUM(km.qty) >= ?", intval($params['quantity_sellout']));

            $select->join(array('sellout' => new Zend_Db_Expr('(' . $sub_select_sellout . ')')), 'sellout.store_id = s.id', array());

        }
        
        if ($params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if ($params['is_lock'] == 1) {
            $select->where('a.id IS NOT NULL');
        }

        if ($params['is_lock'] == 2) {
            $select->where('a.id IS NULL');
        }

        if ($params['error_image']) {
            $select->where('i.error_image = ?', 1);
        }
        if ($params['category_id']) {
            $select->join(array('detail' => DATABASE_TRADE . '.app_checkshop_detail'), 'a.id = detail.checkshop_id AND detail.quantity > 0 AND detail.category_id = ' . $params['category_id'], array());
        }




        if (!empty($params['name_search'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_search'] . '%');
        }

        if (!empty($params['key_search'])) {
            $select->where('s.id = ?', $params['key_search']);
        }

        if (!empty($params['trade_local_id'])) {
            $select->where('c.created_by = ?', $params['trade_local_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        if (!empty($params['regional_market'])) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if (!empty($params['district'])) {
            $select->where('s.district = ?', $params['district']);
        }

        if (!empty($params['shop_check_in_month_arr'])) {
            $select->where('s.id NOT IN (?)', $params['shop_check_in_month_arr']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
//                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
//                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

//        if (!empty($params['from_date'])) {
//            $from_date = str_replace('/', '-', $params['from_date']);
//            $from_date = date('Y-m-d', strtotime($from_date));
//
//            $select->where('c.created_at >= ?', $from_date_time);
//        }
//
//        if (!empty($params['to_date'])) {
//            $to_date = str_replace('/', '-', $params['to_date']);
//            $to_date = date('Y-m-d', strtotime($to_date));
//            $select->where('c.created_at <= ?', $to_date_time);
//        }

        if (!empty($params['is_pending']) && $params['is_pending'] == 1) {

        }

        //TUONG
        if (!empty($params['checkshop_qc_selectbox'])) {
            $select->where('m.qc_id IN (?)', $params['checkshop_qc_selectbox']);
            $select->where('m.type = ?', $params['type_qc']);

            $select->where('MONTH(c.created_at) = ?', $params['month']);
            $select->where('YEAR(c.created_at) = ?', $params['year']);
        }

        //Author : Trang
        if (!empty($params['checkshop_qc_selectbox_realme'])) {

            $select->joinLeft(array('m1' => DATABASE_TRADE . '.app_checkshop_map_qc_realme'), 'c.id = m1.checkshop_id', array());// Author : Trang
            $select->where('m1.qc_id IN (?)', $params['checkshop_qc_selectbox_realme']);
            $select->where('m1.type = ?', $params['type_qc_realme']);

            $select->where('MONTH(c.created_at) = ?', $params['month_realme']);
            $select->where('YEAR(c.created_at) = ?', $params['year_realme']);
        }

        if ($params['check_posm_stage_id']) {
            $select->join(['posm' => DATABASE_TRADE . '.check_posm_assign_store'], 's.id = posm.store_id AND posm.stage_id = ' . $params['check_posm_stage_id'] . ' AND posm.del = 0', []);
        }

        if ($params['list_store']) {
            $select->where("s.id IN (?)", explode(',', $params['list_store']));
        }

        if ($params['change_picture_stage_id']) {
            $select_change_picture = $db->select();
            $select_change_picture->from(array('change_picture' => DATABASE_TRADE . '.change_picture_detail'), [
                'store_id' => new Zend_Db_Expr('DISTINCT change_picture.store_id'),
            ])
                ->where('change_picture.stage_id = ?', $params['change_picture_stage_id'])
                ->where('change_picture.product_id > 0');

            $select->join(array('u' => new Zend_Db_Expr('(' . $select_change_picture . ')')), 'u.store_id = s.id', array());

        }

        // kiểm tra lỗi btn 3.0
        if ($params['status_error_btn']) {
            // sub query
            $select_store_has_check_error_btn = $db->select()
                ->from(['c' => DATABASE_TRADE . '.check_error_btn'], [
                    'c.store_id'
                ])
                ->group('c.store_id');
            // sub query

            $select->join(['seb' => DATABASE_TRADE . '.store_error_btn'], 'seb.store_id = s.id', []);
            $select->joinLeft(['sh' => $select_store_has_check_error_btn], 'seb.store_id = sh.store_id', []);


            if ($params['status_error_btn'] == 1) { // tât cả shop trong danh sách
//                $select->where('seb.store_id IS NOT NULL');
            }

            if ($params['status_error_btn'] == 2) { // shop đã check
                 $select->where('sh.store_id IS NOT NULL');
            }

            if ($params['status_error_btn'] == 3) { // shop chưa check
                $select->where('sh.store_id IS NULL');
            }
        }
        // kiểm tra lỗi btn 3.0




        //TUONG
        $select->group('s.id');
        $select->order("c.created_at DESC");


        $select->limitPage($page, $limit);
        if ($_GET['dev']) {
            echo $select;die;
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }


    public function GetList($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select('')
            ->from(array('ACS' => DATABASE_TRADE . '.' . $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ACS.id'), 'ACS.store_id', 'ACS.create_at', 'ACS.store_id', 'ACS.update_at'))
            ->joinLeft(array('S' => DATABASE_CENTER . '.store'), 'S.id =ACS.store_id', array('storename' => "CONCAT(SUBSTRING_INDEX(S.name, ' ', 2) ,' - ', R.name, ' - ', R2.name)"))
            ->joinLeft(array('R' => DATABASE_CENTER . '.regional_market'), 'R.id =S.regional_market', array('area_id' => 'R.area_id', 'name_city' => 'R.name'))
            ->joinLeft(array('R2' => DATABASE_CENTER . '.regional_market'), 'R2.id =S.district', array('name_district' => 'R2.name'))
            ->joinLeft(array('ACSD' => DATABASE_TRADE . '.app_check_shop_detail'), 'ACSD.parrent_id =ACS.id', array('detail_id' => 'ACSD.id', 'soluong' => 'ACSD.sl', 'category_id' => 'ACSD.category_id'))
            ->joinLeft(array('C' => DATABASE_TRADE . '.category'), 'C.id = ACSD.category_id', array('category_id' => 'C.id', 'categoryname' => 'C.name'))
            ->joinLeft(array('AF' => DATABASE_TRADE . '.app_file'), 'AF.parrent_id = ACSD.id', array('name_img' => 'AF.file_name'))
            ->where('AF.type =4 and ACS.id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getShopInfo($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.store_id',
            'p.created_at',
            'p.updated_at',
            'store_name' => 's.name',
            'p.latitude',
            'p.longitude',
            'latitude_store' => 's.latitude',
            'longitude_store' => 's.longitude',
            'p.is_lock'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->where('p.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getCheckshopDetails($id, $company = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'category_name' => 'c.name',
            'category_id' => 'p.category_id',
            'p.quantity',

            'p.imei_sn',
            'c.group_bi',
            'c.has_type',
            'c.has_width'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_detail'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = p.category_id', array());
        $select->where('p.checkshop_id = ?', $id);
        $select->where('p.quantity > 0');

        if ($company) {
            $select->where('c.company = ?', $company);
        }

        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }


    public function getLastCheckshop($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.created_at',
            'p.store_id',
            'd.category_id',
            'd.quantity',
            'd.imei_sn',
            'p.latitude',
            'p.longitude'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE . '.app_checkshop_detail'), 'd.checkshop_id = p.id', array());
        $select->where('p.store_id = ?', $store_id);
        $select->where("p.created_at = (SELECT MAX(created_at) FROM `" . DATABASE_TRADE . "`.app_checkshop)", NULL);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getLastCs($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.created_at',
            'p.store_id',
            'p.latitude',
            'p.longitude'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols);
        $select->where('p.store_id = ?', $store_id);
        $select->order('p.id DESC');
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getImg($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.url',
            'p.type'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_file'), $arrCols);
        $select->where('p.checkshop_id = ?', $id);
        $select->where('p.type IN (?)', array(0, 1, 2));
        $select->order('p.type ASC');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListImg($date, $store_id, $checkshop_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.url',
            'p.type',
            'p.checkshop_id',
            'c.updated_at'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_file'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->where('MONTH(c.created_at) = MONTH(?)', $date);
        $select->where('YEAR(c.created_at) = YEAR(?)', $date);
        $select->where('c.store_id = ?', $store_id);
        $select->where('p.checkshop_id <> ?', $checkshop_id);
        $select->where('p.type IN (?)', array(0, 1, 2));
        $select->order('c.created_at DESC');

        if ($_GET['dev'] == 1) {
            echo $select->__toString();
            die;
        }
        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['checkshop_id']][] = $value;
        }

        return $data;
    }

    public function getImgDifferentList($date, $store_id, $checkshop_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.url',
            'p.type',
            'p.checkshop_id',
            'c.updated_at'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_file'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->where('MONTH(c.updated_at) = MONTH(?)', $date);
        $select->where('c.store_id = ?', $store_id);
        $select->where('p.checkshop_id <> ?', $checkshop_id);
        $select->where('p.type NOT IN (?)', array(0, 1, 2));
        $select->order('c.updated_at DESC');
        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['checkshop_id']][] = $value;
        }

        return $data;
    }

    public function getImgDifferent($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.url',
            'p.type'
        );
        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_file'), $arrCols);
        $select->where('p.checkshop_id = ?', $id);
        $select->where('p.type NOT IN (?)', array(0, 1, 2));
        $select->order('p.type ASC');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListCheckshop($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.store_id',
            'p.created_at',
            'p.updated_at',
            'store_name' => 's.name',
            'p.is_lock',
            'p.is_leader_lock',
            's.shipping_address',
            'province_name' => 'r.name',
            'district_name' => 'r2.name',
            'area_name' => 'a.name',
            'created_by' => "CONCAT(st.firstname, ' ', st.lastname)"
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('r2' => 'regional_market'), 's.district = r2.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('st' => 'staff'), 'p.created_by = st.id', array());

        $select->where('p.store_id = ?', $params['id_store']);
        $select->where('p.updated_at IS NOT NULL AND p.status = 1');

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));
            $select->where('DATE(p.updated_at) >= ?', $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.updated_at) <= ?', $to_date);
        }

        $select->order('p.updated_at DESC');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListShop($params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            'checkshop.id',
            'store_name' => 'p.name',
            'store_id' => 'p.id',
            'dealer_id' => 'p.d_id',
            'partner_id' => 'p.partner_id',
            'shipping_address' => 'p.shipping_address',
            'dealer_name' => 'd.name',
            'is_ka' => 'd.is_ka',
            'checkshop_last' => "checkshop.updated_at",
            'checkshop_id' => "checkshop.id",
            'soluong' => "detail.quantity",
            'imei' => "detail.imei_sn",
            'category_name' => "cat.name",
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            'is_lock' => "a.is_lock",
            'channel' => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END",
            'area_name' => "area.name",
            "parent_id" => "IF(d.parent = 0, d.id, d.parent)"
        );


        $nestedSelect = $db->select();
        $arrCols_nest = [
            'p.id',
            'p.store_id',
            'p.latitude',
            'p.longitude',
            'p.updated_at'
        ];
        $nestedSelect->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols_nest);
        $nestedSelect->where('p.id IN (SELECT MAX(p.id) FROM trade_marketing.app_checkshop p WHERE p.updated_at IS NOT NULL GROUP BY p.store_id)');


        $select->from(array('p' => 'store'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'area.id = r.area_id', array());
        $select->joinLeft(array('l' => 'v_store_staff_leader_log'), 'l.store_id = p.id AND l.released_at IS NULL AND l.is_leader = 1', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = l.staff_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_checkshop'), 'a.store_id = p.id AND a.is_lock = 1', array());
        $select->joinLeft(array('checkshop' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 'checkshop.store_id = p.id', array());
        $select->joinLeft(array('detail' => DATABASE_TRADE . '.app_checkshop_detail'), 'detail.checkshop_id = checkshop.id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE . '.category'), 'cat.id = detail.category_id', array());


        $select->where('p.del IS NULL OR p.del = 0');

        if (!empty($params['staff_id'])) {
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('loyalty.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('loyalty.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('loyalty.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }




        $select->group('p.id');
        $select->order('p.id');
//echo $select ; die;
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListDealer($params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.store_id',
            "store_name" => "s.name",
            "s.shipping_address",
            "area_name" => "a.name",
            "dealer_id" => "d.id",
            "dealer_name" => "d.title",
            "is_ka" => 'd.is_ka',
            "loyalty_plan_id" => "l.loyalty_plan_id",
            "channel" => "IF(d.parent = 0, d.id, d.parent)",
            "title" => "d.title"
        );

        $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->where('p.released_at IS NULL', NULL);
        $select->where('s.del is NULL or s.del= 0', NULL);

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['key_search'])) {
            $select->where('s.name LIKE ?', '%' . $params['key_search'] . '%');
        }

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

        $select->group('d.id');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function ListShop($params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.store_id',
            "store_name" => "s.name",
            "s.shipping_address",
            "area_name" => "a.name",
            "dealer_id" => "d.id",
            "dealer_name" => "d.title"
        );

        $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->where('p.released_at IS NULL', NULL);
        $select->where('s.del is NULL or s.del= 0', NULL);

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }


        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        $select->group('p.store_id');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getQc($params)
    {

        $first_date = current($params['range_date']);
        $last_date = end($params['range_date']);

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "c.store_id",
            "month" => "MONTH(c.created_at)",
            "q.title",
            "p.type"
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_map_qc'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->joinLeft(array('q' => DATABASE_TRADE . '.app_checkshop_qc'), 'q.id = p.qc_id', array());
        $select->where('(c.created_at) >= ?', $first_date);
        $select->where('(c.created_at) <= ?', $last_date);
        $result = $db->fetchAll($select);


        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['store_id']][$value['month']][$value['type']] = $value['title'];
        }

        return $data;
    }

    public function getQcNew($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "c.store_id",
            "month" => "MONTH(c.created_at)",
            "q.title",
            "p.type"
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_map_qc'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->joinLeft(array('q' => DATABASE_TRADE . '.app_checkshop_qc'), 'q.id = p.qc_id', array());

        $result = $db->fetchAll($select);

        //
        $select2 = $db->select();

        $arrCols2 = array(
            "p.store_id",
            "month" => "MONTH(p.updated_at)",
            "list_date" => 'GROUP_CONCAT(DATE_FORMAT(p.updated_at, "%d"))'
        );

        $select2->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols2);
        $select2->group(array("MONTH(p.updated_at)", "p.store_id"));
        $result2 = $db->fetchAll($select2);

        $data2 = [];
        foreach ($result2 as $key => $value) {
            $data2[$value['store_id']][$value['month']] = $value['list_date'];
        }

        $data = [];
        foreach ($result as $key => $value) {

            $title = $value['title'];

            $list_date = $data2[$value['store_id']][$value['month']];
            $count_list_date = $this->countDateCheckshop(explode(',', $list_date));

            if ($count_list_date < 3) {

                $title = 'KHÔNG ĐẠT - Checkshop chưa đủ số lần quy định (' . $count_list_date . ' lần).';

                $data[$value['store_id']][$value['month']][2] = $title;
                $data[$value['store_id']][$value['month']][3] = $title;
            } else {
                $data[$value['store_id']][$value['month']][$value['type']] = $title;
            }
        }

        return $data;
    }

    public function getSaleArea()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'staff_id' => "p.id",
            "area_id" => "r.area_id",
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.title IN (?)', [183, 190]);
        $select->where('p.off_date IS NULL');

        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['area_id']][] = ['staff_id' => $value['staff_id'], 'fullname' => $value['fullname']];
        }

        return $data;
    }

    public function getCheckshopLock($store_id, $company = null)
    {
        if (empty($store_id)) {
            return false;
        }

        $db = Zend_Registry::get('db');

        $col = array(
            "store_id" => "p.store_id",
            "category_id" => "d.category_id",
            "quantity" => "d.quantity",
            "category_name" => "c.name",
            "group_bi" => "c.group_bi",
            "store_name" => "s.name",
            "category_name" => "c.name",
            "group_bi" => "c.group_bi",
            "imei_sn" => "d.imei_sn"
        );

        $select = $db->select()
            ->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $col)
            ->joinLeft(array('s' => 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('d' => DATABASE_TRADE . '.app_checkshop_detail'), 'd.checkshop_id = p.id AND d.quantity > 0', array())
            ->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = d.category_id', array());

        $select->where('p.store_id = ?', $store_id);

        $select->where('p.is_lock = 1', NULL);

        $select->where('d.id IS NOT NULL', NULL);

        if ($company) {
            $select->where('c.company = ?', $company);
        }


        $result = $db->fetchAll($select);

        return $result;

    }

    public function getChannel()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.title'
        );

        $select->from(array('p' => WAREHOUSE_DB . '.distributor'), $arrCols);

        $select->where('p.is_ka = ?', 1);
        $select->where('p.is_ka_parent = ?', 1);

        $result = $db->fetchAll($select);


        //KA
        $data = [];
        foreach ($result as $key => $value) {
            $data[1][] = ['id' => $value['id'], 'title' => $value['title']];
        }

        //IA
        $data[0][] = ['id' => 1, 'title' => 'SILVER'];
        $data[0][] = ['id' => 2, 'title' => 'GOLD'];
        $data[0][] = ['id' => 3, 'title' => 'DIAMOND'];
        $data[0][] = ['id' => 4, 'title' => 'NORMAL'];


        return $data;
    }

    public function getChannelTotal()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.title'
        );

        $select->from(array('p' => WAREHOUSE_DB . '.distributor'), $arrCols);

        $select->where('p.is_ka = ?', 1);
        $select->where('p.is_ka_parent = ?', 1);

        $result = $db->fetchAll($select);

        //KA
        $data = [];
        foreach ($result as $key => $value) {
            $data[1][$value['id']] = $value['title'];
        }

        //IA
        $data[0][1] = 'SILVER';
        $data[0][2] = 'GOLD';
        $data[0][3] = 'DIAMOND';
        $data[0][4] = 'NORMAL';

        return $data;
    }

    function countDateCheckshop($list_date)
    {

        $total = (count($list_date) > 0) ? 1 : 0;
        $j = 0;
        for ($i = 0; $i < count($list_date); $i++) {

            if ($list_date[$i] >= ($list_date[$j] + 3) AND $i > 0) {
                $total++;
                $j = $i;
            }

        }

        return $total;

    }

    public function getListHaveQc($list_store, $month, $year)
    {

        if (empty($list_store)) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'c.store_id',
            'p.checkshop_id',
            'p.qc_id',
            'p.type',
            'p.note'
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop_map_qc'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->where('c.store_id IN (?)', $list_store);

        $select->where('MONTH(c.updated_at) = ?', $month);
        $select->where('YEAR(c.updated_at) = ?', $year);

        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['store_id']][] = $value['type'];
        }

        return $data;
    }

    public function getSellout2($id_store)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'number_imei' => "COUNT(DISTINCT p.imei_sn)",
            'p.store_id',
            'month' => "MONTH(p.timing_date)",
            "l.loyalty_plan_id",
            "d.is_ka",
            "loyalty_name" => "lp.name",
            "level" => "(CASE WHEN d.is_ka = 1 THEN 'KA' WHEN d.is_ka <> 1 AND lp.name IS NOT NULL THEN lp.name ELSE 'Shop thường' END)"

        );

        $select->from(array('p' => 'imei_kpi'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND is_last = 1', array());
        $select->joinLeft(array('lp' => 'loyalty_plan'), 'lp.id = l.loyalty_plan_id', array());
        $select->where("p.timing_date >= DATE_FORMAT( CURRENT_DATE - INTERVAL 3 MONTH, '%Y-%m-01 00:00' )");
        $select->where("p.store_id = ?", $id_store);

        $select->group('MONTH(p.timing_date)');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getSellout($id_store)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $sub_select = "(
                        SELECT p.YearNo, p.MonthNo, p.QuarterNo, p.FromDate, p.ToDate, d.from_date, d.to_date, d.dealer_id, d.loyalty_plan_id, l.name loyalty_plan_name
	FROM 
	(
		select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
			select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
			(select 0 as a union all select 1 union all select 2 union all select 3
			 union all select 4 union all select 5 union all select 6 union all
			 select 7 union all select 8 union all select 9) a, /*10 day range*/
			(select 0 as a union all select 1 union all select 2 union all select 3
			 union all select 4 union all select 5 union all select 6 union all
			 select 7 union all select 8 union all select 9) b, /*100 day range*/
			(select 0 as a union all select 1 union all select 2 union all select 3
			 union all select 4 union all select 5 union all select 6 union all
			 select 7 union all select 8 union all select 9) c, /*1000 day range*/
			(select 0 as a union all select 1 union all select 2 union all select 3
			 union all select 4 union all select 5 union all select 6 union all
			 select 7 union all select 8 union all select 9) d, /*10000 day range*/
			(select  @minDate :=  CONCAT(DATE_FORMAT( CURRENT_DATE - INTERVAL 3 MONTH, '%Y-%m-01' )), @maxDate := CASE WHEN CONCAT(NOW()) <= DATE(NOW()) THEN CONCAT(NOW()) ELSE DATE(NOW()) END) e
		) f
		where aDate between @minDate and @maxDate
		GROUP BY YEAR(aDate), MONTH(aDate)
	) p
	CROSS JOIN dealer_loyalty d ON d.to_date >= p.FromDate AND d.from_date <= p.ToDate
	LEFT JOIN loyalty_plan l ON l.id = d.loyalty_plan_id
                            )";

        $arrCols = array(
            'number_imei' => "SUM(p.qty)",
            'p.store_id',
            'month' => "MONTH(p.timing_date)",
            "d.is_ka",
            "level" => "(CASE WHEN d.is_ka = 1 THEN 'KA' WHEN d.is_ka <> 1 AND a.loyalty_plan_name IS NOT NULL THEN a.loyalty_plan_name ELSE 'Shop thường' END)"

        );

        $select->from(array('p' => 'kpi_by_model'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), 'a.MonthNo = MONTH(p.timing_date) AND a.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent))', array());
        $select->where("p.timing_date >= DATE_FORMAT( CURRENT_DATE - INTERVAL 3 MONTH, '%Y-%m-01 00:00' )");
        $select->where("p.store_id = ?", $id_store);

        $select->group('MONTH(p.timing_date)');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getLevelStore($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'store_id' => "p.id",
            'level' => "(CASE WHEN d.is_ka = 1 THEN 'KA' WHEN d.is_ka <> 1 AND lp.`name` IS NOT NULL THEN lp.`name` ELSE 'Shop thường' END)"
        );

        $select->from(array('p' => 'store'), $arrCols);
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND is_last = 1', array());
        $select->joinLeft(array('lp' => 'loyalty_plan'), 'lp.id = l.loyalty_plan_id', array());
        $select->where('p.id = ?', $store_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getSaleLog($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'store_id' => "p.store_id",
            'fullname' => "CONCAT(s.firstname, ' ',s.lastname)"
        );

        $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->where('p.released_at IS NULL AND p.is_leader = 1');
        $select->where('p.store_id = ?', $store_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getInfoCheckshop($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "checkshop_id" => "p.id",
            "p.store_id",
            "p.updated_at",
            "p.latitude",
            "d.category_id",
            "category_name" => "c.name",
            "d.quantity"
            // "imei_sn" => "REPLACE(d.imei_sn, 'https://trade-marketing.opposhop.vn/tool/scan-imei?imei_sn=', '')"
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE . '.app_checkshop_detail'), 'd.checkshop_id = p.id AND d.quantity > 0', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = d.category_id', array());
        $select->where('p.store_id = ?', $params['store_id']);
        $select->where('c.company = ?', 1);
        $select->where('p.is_lock = ?', 1);

        // $select->where('p.updated_at IS NOT NULL');
        // $select->where('p.updated_at = (
        //                 SELECT MAX(p.updated_at)
        //                 FROM `' . DATABASE_TRADE . '`.app_checkshop p
        //                 WHERE p.store_id = ' . $params['store_id'] . ' AND p.updated_at IS NOT NULL AND p.is_lock = 1)');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getShopCategorySize($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'store_id' => 'p.store_id',
            'store_name' => 's.name',
            'area_name' => 'a.name',
            'shipping_address' => 's.shipping_address',
            'quantity' => "SUM(d.quantity)"

        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE . '.app_checkshop_detail'), 'd.checkshop_id = p.id', array());
        $select->joinLeft(array('s' => DATABASE_CENTER . '.store'), 's.id = p.store_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = d.category_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->where('p.is_lock = 1 AND d.quantity > 0');

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['name_search'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_search'] . '%');
        }
        if (!empty($params['area_id_search'])) {
            $select->where('r.area_id IN (?)', $params['area_id_search']);
        }
        if (!empty($params['sale_id'])) {
            $select->where('p.created_by = ?', $params['sale_id']);
        }

        $select->group(array('p.store_id'));
        $select->order("p.updated_at DESC");
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }


    public function getDataDetailListStore($list_store)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "id_checkshop" => "d.checkshop_id",
            "category_id" => "d.category_id",
            "category" => "c.name",
            "quantity" => "d.quantity",
            "store_id" => "p.store_id"

        );

        $select->from(array('p' => DATABASE_TRADE . '.app_checkshop'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE . '.app_checkshop_detail'), 'd.checkshop_id = p.id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = d.category_id', array());
        $select->where('p.store_id IN (?)', $list_store);
        $select->where('p.is_lock = 1 AND d.quantity > 0');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getStoreInfo($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'store_name' => 'p.name',
            'p.shipping_address',
            'province_name' => 'r.name',
            'district_name' => 'r2.name',
            'area_name' => 'a.name',
            'store_level' => 'sl.title'
        );

        $select->from(array('p' => 'store'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        $select->joinLeft(array('r2' => 'regional_market'), 'p.district = r2.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('si' => 'store_level_info'), 'p.id = si.store_id AND si.default = 1', array());
        $select->joinLeft(array('sl' => 'store_level'), 'sl.id = si.level_id', array());

        $select->where('p.id = ?', $store_id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function countEligible($month, $year, $store_id)
    {

        $sub_select = "select
			p.store_id,
			p.updated_at,
			@last_updated_at,
			@last_updated_at := IF(@last_store_id <> p.store_id, DATE_FORMAT(p.updated_at, '%Y-%m-%d'), IF( DATE_FORMAT(p.updated_at, '%Y-%m-%d') > DATE_ADD(@last_updated_at, INTERVAL +2 DAY) , DATE_FORMAT(p.updated_at, '%Y-%m-%d'), @last_updated_at )) AS last_updated_at,
			@last_store_id := p.store_id
                        from
			`" . DATABASE_TRADE . "`.`app_checkshop` p,
			( select @last_store_id := 0, @last_updated_at := 0 ) i
                        WHERE MONTH(p.updated_at) = '" . $month . "' AND YEAR(p.updated_at) = '" . $year . "'
                        order by p.store_id,p.updated_at ASC";

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.store_id',
            'p.updated_at',
            'p.last_updated_at',
            'eligible' => 'COUNT(DISTINCT p.last_updated_at)'
        );

        $select->from(array('p' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);
        $select->where('p.store_id = ?', $store_id);

        $result = $db->fetchRow($select);


        return $result;
    }

    public function exportTime($params)
    {

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));
            $from_date = "'" . $from_date . "'";
        } else {
            $from_date = date('Y-m-d');
            $from_date = "'" . $from_date . "'";
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $to_date = "'" . $to_date . "'";
        } else {
            $to_date = date('Y-m-d');
            $to_date = "'" . $to_date . "'";
        }

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.staff_id',
            'store_name' => 's.name',
            'checkshop_at' => "c.updated_at",
            'store_id' => 'p.store_id',
            'is_lock' => 'c.is_lock',
            'channel' => "(CASE WHEN (l.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (l.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END)",
            "s.partner_id",
            "area_name" => "a.name",
            "dealer_id" => "d.id",
            "fullname" => "CONCAT(staff.firstname, ' ',staff.lastname)",
            "fullname_check" => "CONCAT(staff_check.firstname, ' ',staff_check.lastname)",
            'title_check' => "title_check.name",
            'district' => 'dis.name',
            'province' => 'r.name'
        );

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if ($userStorage->title == SALES_LEADER_TITLE) {
            $select->from(array('p' => 'store_leader_log'), $arrCols);
        } else {
            $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
            $select->where('p.is_leader = 1');
        }


        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('dis' => 'regional_market'), 's.district = dis.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'dis.parent = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.store_id = s.id AND c.status = 1 AND (DATE(c.created_at) >=' . $from_date . " AND DATE(c.created_at) <=" . $to_date . ")", array());
        $select->joinLeft(array('staff_check' => 'staff'), 'staff_check.id = c.created_by', array());
        $select->joinLeft(array('title_check' => 'team'), 'staff_check.title = title_check.id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = l.loyalty_plan_id', array());

        $select->where('p.released_at IS NULL ', NULL);
        $select->where('s.del is NULL or s.del= 0', NULL);

        if (!empty($params['name_search'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_search'] . '%');
        }

        if (!empty($params['key_search'])) {
            $select->where('s.id = ?', $params['key_search']);
        }

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['trade_local_id'])) {
            $select->where('c.created_by = ?', $params['trade_local_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

//        if(!empty($params['from_date'])){
//            $from_date = str_replace('/', '-', $params['from_date']);
//            $from_date = date('Y-m-d', strtotime($from_date));
//
//            $select->where('DATE(c.updated_at) >= ?', $from_date);
//        }
//
//        if(!empty($params['to_date'])){
//            $to_date = str_replace('/', '-', $params['to_date']);
//            $to_date = date('Y-m-d', strtotime($to_date));
//            $select->where('DATE(c.updated_at) <= ?', $to_date);
//        }

        $select->order(['s.id', 'c.updated_at DESC']);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListCheckshopInMonth($params)
    {

        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.staff_id',
            'store_name' => 's.name',
            'checkshop_last' => "MAX(c.updated_at)",
            'checkshop_id' => "MAX(c.id)",
            'store_id' => 'p.store_id',
        );

        if (!empty($params['staff_id'])) {

            $QStaff = new Application_Model_Staff();
            $where = $QStaff->getAdapter()->quoteInto('id = ?', $params['staff_id']);
            $staff = $QStaff->fetchRow($where);

            if ($staff['title'] == 190) {
                $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
            } else {
                $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
                $select->where('p.is_leader = 1');
            }

        } else {
            $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
            $select->where('p.is_leader = 1');
        }

        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.store_id = s.id', array());
        $select->where("c.updated_at >= DATE_FORMAT(NOW(), '%Y-%m-01')");


        $select->where('p.released_at IS NULL ', NULL);
        $select->where('s.del is NULL or s.del= 0', NULL);

        if (!empty($params['name_search'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_search'] . '%');
        }

        if (!empty($params['key_search'])) {
            $select->where('s.id = ?', $params['key_search']);
        }
        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['regional_market'])) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if (!empty($params['district'])) {
            $select->where('s.district = ?', $params['district']);
        }

        if (!empty($params['list_area'])) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }

        if (!empty($params['from_date'])) {
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(c.updated_at) >= ?', $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(c.updated_at) <= ?', $to_date);
        }


        $select->group('s.id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getLatestCheckShopTest($storeId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'a.id',
                'a.updated_at',
                'a.created_at',
                'store_name' => 's.name',
                's.shipping_address',
                'district_name' => 'r.name',
                'area_name' => 'e.name',
                'staff_check_shop' => "CONCAT(st.firstname, ' ', st.lastname)",
                'app_check_shop_id' => 'a.id'
            ])
            ->joinLeft(['a' => DATABASE_TRADE . '.app_checkshop'], 's.id = a.store_id  AND a.updated_at = (SELECT MAX(updated_at) FROM trade_marketing.app_checkshop WHERE store_id = s.id AND DATE(updated_at) = "2021-07-05")', [])
            ->joinLeft(['r' => 'regional_market'], 's.district = r.id', [])
            ->joinLeft(['r2' => 'regional_market'], 's.regional_market = r2.id', [])
            ->joinLeft(['e' => 'area'], 'r2.area_id = e.id', [])
            ->joinLeft(['st' => 'staff'], 'a.created_by = st.id', [])
            ->where('s.id = ?', $storeId);

        $result = $db->fetchRow($select);
//echo $select->__toString();die;
        return $result;
    }

    public function getImageCheckshop($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.app_checkshop'], [
                'checkshop_id' => 'a.id',
                'url' => "CONCAT('/', f.url)"
            ])
            ->joinLeft(['f' => DATABASE_TRADE . '.app_checkshop_file'], 'a.id = f.checkshop_id', []);

        if ($params['checkshop_id']) {
            $select->where('a.id = ?', $params['checkshop_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getInforShopMap($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.app_checkshop'], [
                'store_name' => 's.name',
                'area_name' => 'e.name',
                'province_name' => 'r.name',
                'district_name' => 'd.name'
//                         'sales_name' => "CONCAT(st.firstname, ' ', st.lastname)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
//                     ->joinLeft(['v' => 'v_store_staff_leader_log'], 's.id = v.store_id AND v.is_leader = 1', [])
//                     ->joinLeft(['st' => 'staff'], 'v.staff_id = st.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->where('a.id = ?', $params['checkshop_id']);

        $result = $db->fetchAll($select);

        return $result[0];
    }

    public function getCheckShopIdLock($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.app_checkshop'], [
                'a.id'
            ])
            ->where('a.store_id = ?', $store_id)
            ->where('a.is_lock = ?', 1);

        $result = $db->fetchOne($select);
        return $result;
    }

    public function getDataRealme($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'area_name' => 'a.name',
                'province_name' => 'r.name',
                'district_name' => 'd.name',
                'store_name' => 's.name',
                'store_id' => 's.id',
                'p.status',
                'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)"
            ])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['r' => 'regional_market'], 'd.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
            ->joinLeft(['p' => DATABASE_TRADE . '.'], 'p.store_id = s.id', [])
            ->joinLeft(['v' => 'v_store_staff_leader_log'], 'v.store_id = s.id', [])
            ->joinLeft(['f' => 'staff'], 'v.staff_id = f.id', [])
            ->where('s.del IS NULL OR s.del = 0');

        if ($params['list_area']) {
            $select->where('a.id IN (?)', $params['list_area']);
        }

        if ($params['staff_id']) {
            $select->where('f.id = ?', $params['staff_id']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportRealme($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getData($params);


        $heads = array(
            'Store ID',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Quận huyện',
            'Sale quản lý',
            'Trạng thái'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        $i = 1;

        foreach ($data as $item) {
            $alpha = 'A';
            if ($item['status'] == 1) {
                $status_name = 'Shop đã tạm đóng';
            } elseif ($item['status'] == 2) {
                $status_name = 'Shop đã rút demo';
            } elseif ($item['status'] == 3) {
                $status_name = 'Shop đã trở lại hoạt động';
            } else {
                $status_name = '';
            }

            $sheet->setCellValue($alpha++ . $index, $item['store_id']);
            $sheet->setCellValue($alpha++ . $index, $item['store_name']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);
            $sheet->setCellValue($alpha++ . $index, $item['province_name']);
            $sheet->setCellValue($alpha++ . $index, $item['district_name']);
            $sheet->setCellValue($alpha++ . $index, $item['sale_name']);
            $sheet->setCellValue($alpha++ . $index, $status_name);

            $index++;
        }

        $filename = 'Trang thai shop CORONA' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function deleteCategoryRealme($checkshop_id)
    {
        $checkshop_id = intval($checkshop_id);

        $db = Zend_Registry::get('db');
        $sql_detail = "
			DELETE d 
			FROM trade_marketing.app_checkshop_detail d 
            JOIN trade_marketing.category c ON d.category_id = c.id
            where d.checkshop_id =" . $checkshop_id . " AND c.company = 2
		";

        $sql_detail_child = "
			DELETE d 
			FROM trade_marketing.app_checkshop_detail_child d 
            JOIN trade_marketing.category c ON d.category_id = c.id
            where d.checkshop_id =" . $checkshop_id . " AND c.company = 2
		";

        $db->query($sql_detail);
        $db->query($sql_detail_child);
    }

    public function getQcByMonth($params)
    {

        $sub_select = "
            select
                p.store_id,
                p.updated_at,
                @last_updated_at,
                @last_updated_at := IF(@last_store_id <> p.store_id, DATE_FORMAT(p.updated_at, '%Y-%m-%d'), IF( DATE_FORMAT(p.updated_at, '%Y-%m-%d') > DATE_ADD(@last_updated_at, INTERVAL +2 DAY) , DATE_FORMAT(p.updated_at, '%Y-%m-%d'), @last_updated_at )) AS last_updated_at,
                @last_store_id := p.store_id
            from
                `trade_marketing`.app_checkshop p,
                ( select @last_store_id := 0, @last_updated_at := 0 ) i
            WHERE p.updated_at IS NOT NULL
            order by p.store_id,p.updated_at ASC
            ";


        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            's.name',
            'p.store_id',
            'p.updated_at',
            'p.last_updated_at',
            'month' => 'MONTH(p.updated_at)',
            'year' => 'YEAR(p.updated_at)',
            'month_year' => 'CONCAT(MONTH(p.updated_at), "/", YEAR(p.updated_at))',
            'eligible' => 'COUNT(DISTINCT p.last_updated_at)'
        );

        $select->from(array('p' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());

        if (!empty($params['store_id'])) {
            $select->where('p.store_id = ?', $params['store_id']);
        }

        $select->group(array('p.store_id', 'MONTH(p.updated_at)', 'YEAR(p.updated_at)'));
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDataLegalCheckshop($params)
    {
        $from_date_time = $params['from_date_time'];
        $to_date_time = $params['to_date_time'];

        $team_trade_marketing = TEAM_TRADE_MARKETING;

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $sub_select_count_checkshop_sales = "
       	SELECT 
        h.store_id,
        COUNT(DISTINCT h.legal_date) as count_legal_checkshop
    
        FROM 
        (
            SELECT 
    
            @legal_date :=
            IF (g.store_id <> @store_id  , g.date_checkshop , 
                IF( DATE_SUB(g.date_checkshop ,INTERVAL 1 DAY) >= @legal_date, g.date_checkshop, @legal_date)
            ) as legal_date,
            
            @store_id := g.store_id as store_id
            
            FROM 
            (
                SELECT a.store_id,
                DATE(a.created_at) as date_checkshop
                
                FROM trade_marketing.app_checkshop a 
                LEFT JOIN hr.staff st ON a.created_by = st.id 
                
                WHERE (a.created_at BETWEEN '$from_date_time' AND '$to_date_time')
                AND st.team <> '$team_trade_marketing'
                
                GROUP BY a.store_id, date_checkshop 
                ORDER BY a.store_id, date_checkshop ASC
            ) g
            
            JOIN (SELECT @store_id := 0, @legal_date := '0000-00-00') t
        ) h
    
        GROUP BY h.store_id

        ";


        $arrCols = array(
            'store_name' => 's.name',
            'store_id' => 's.id',
            's.shipping_address',
            'district_name' => 'district.name',
            'province_name' => 'r.name',
            'area_name' => 'area.name',
            'dealer_id_parent' => 'IF(dp.id IS NOT NULL, dp.id, d.id)',
            'level' => 'lp.name',
            'channel' => "IF(d.is_ka = 1, 'KA', 'IND' )",
            'ch.count_legal_checkshop',
            'ow.staff_id_own_store',
            'ow.staff_name_own_store',
            'ow.staff_code_own_store',
            'ow.staff_title_own_store'
        );


        $select->from(array('s'=> 'store'), $arrCols);
        $select->joinLeft(array('district' => 'regional_market'), 's.district = district.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'district.parent = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'r.area_id = area.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('dp' => WAREHOUSE_DB . '.distributor'), 'd.parent = dp.id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(['lp' => 'loyalty_plan'], 'lp.id = l.loyalty_plan_id', []);

        $select->joinLeft(['ow' => 'v_staff_own_store'], 's.id = ow.store_id', []);
        $select->joinLeft(['ch' => new Zend_Db_Expr('(' . $sub_select_count_checkshop_sales . ')')], 's.id = ch.store_id', []);

        $select->where('s.del is NULL or s.del = 0', NULL);



        if ($params['type_staff'] == 1 || $params['staff_id']) { // nếu là sale , pg sup, search theo sale
            $sub_select_sale = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['staff_id'])
                ->where('l.is_leader IN (?)', [1, 8]) // sale , PGs supervisor
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');

            // select main
            $select->join(['sale' => $sub_select_sale], 's.id = sale.store_id', []);
        }


        if ($params['type_staff'] == 2) { // nếu là sale leader
            $sub_select_leader = $db->select()
                ->from(['l' => 'store_leader_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['leader_id'])
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');

            // select main
            $select->join(['leader' => $sub_select_leader], 's.id = leader.store_id', []);
        }

        if ($params['type_staff'] == 3) { // các title còn lại
            $select->where('area.id IN (?)', $params['list_area']);
        }

        if ($params['legal_checkshop'] == 1) { // đã check đủ
            $select->where('ch.count_legal_checkshop >= ?', 2);
        }

        if ($params['legal_checkshop'] == 2) { // chưa check đủ
            $select->where('ch.count_legal_checkshop < ? OR ch.count_legal_checkshop IS NULL', 2);
        }


        if ($params['from_date_sellout'] && $params['to_date_sellout'] && $params['quantity_sellout']) {
            $sub_select_sellout = $db->select()
                ->from(['km' => 'kpi_by_model'], [
                    'km.store_id',
                    'quantity_sellout' => "SUM(km.qty)"
                ])
                ->where('km.timing_date >= ?', $params['from_date_sellout'])
                ->where('km.timing_date <= ?', $params['to_date_sellout'])
                ->group('km.store_id')
                ->having("SUM(km.qty) >= ?", intval($params['quantity_sellout']));

            $select->join(array('sellout' => new Zend_Db_Expr('(' . $sub_select_sellout . ')')), 'sellout.store_id = s.id', array());

        }

        if ($params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if ($params['is_lock'] == 1) {
            $select->where('a.id IS NOT NULL');
        }

        if ($params['is_lock'] == 2) {
            $select->where('a.id IS NULL');
        }

        if ($params['error_image']) {
            $select->where('i.error_image = ?', 1);
        }
        if ($params['category_id']) {
            $select->join(array('detail' => DATABASE_TRADE . '.app_checkshop_detail'), 'a.id = detail.checkshop_id AND detail.quantity > 0 AND detail.category_id = ' . $params['category_id'], array());
        }




        if (!empty($params['name_search'])) {
            $select->where('s.name LIKE ?', '%' . $params['name_search'] . '%');
        }

        if (!empty($params['key_search'])) {
            $select->where('s.id = ?', $params['key_search']);
        }

        if (!empty($params['trade_local_id'])) {
            $select->where('c.created_by = ?', $params['trade_local_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['big_area_id'])) {
            $select->where('area.bigarea_id = ?', $params['big_area_id']);
        }

        if (!empty($params['regional_market'])) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if (!empty($params['district'])) {
            $select->where('s.district = ?', $params['district']);
        }

        if (!empty($params['shop_check_in_month_arr'])) {
            $select->where('s.id NOT IN (?)', $params['shop_check_in_month_arr']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])) {


            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('d.is_ka = ?', 1);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "2") {
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if (!empty($params['is_ka_details'])) {

            if (isset($params['is_ka']) && $params['is_ka'] === "0") {
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if (isset($params['is_ka']) && $params['is_ka'] === "1") {
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }


        if ($params['check_posm_stage_id']) {
            $select->join(['posm' => DATABASE_TRADE . '.check_posm_assign_store'], 's.id = posm.store_id AND posm.stage_id = ' . $params['check_posm_stage_id'] . ' AND posm.del = 0', []);
        }

        if ($params['list_store']) {
            $select->where("s.id IN (?)", explode(',', $params['list_store']));
        }


        //TUONG
        $select->group('s.id');
        $select->order("area.id ASC");

        if ($_GET['dev']) {
            echo $select;die;
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportLegalCheckshop($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getDataLegalCheckshop($params);

        $heads = array(
            'Store ID',
            'Store Name',
            'Address',
            'Dealer Parent ID (ID gộp)',
            'District',
            'Province',
            'Area',
            'Channel',
            'Level',
            'Staff quản lý store',
            'Staff code quản lý store',
            'Staff title quản lý store',
            'Số lần check shop hợp lệ'
        );




        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['shipping_address']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id_parent']);
            $sheet->setCellValue($alpha++.$index, $item['district_name']);
            $sheet->setCellValue($alpha++.$index, $item['province_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['channel'] );
            $sheet->setCellValue($alpha++.$index, $item['level']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name_own_store']);
            $sheet->setCellValue($alpha++.$index, $item['staff_code_own_store']);
            $sheet->setCellValue($alpha++.$index, $item['staff_title_own_store']);
            $sheet->setCellValue($alpha++.$index, $item['count_legal_checkshop']);

            $index++;
        }

        $filename = 'Đếm số lần check shop hợp lệ từ '  . $params['from_date'] . ' đến '. $params['to_date'] ;
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }




}