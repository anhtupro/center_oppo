<?php
class Application_Model_PayCosting extends Zend_Db_Table_Abstract
{
    protected $_name = 'pay_costing';
    //protected $_schema = DATABASE_COST;

    public function getHistory($params){
    	$db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'date_pay'        	=> 'p.date_pay', 
            'number_pay'       	=> 'p.number_pay' , 
            'cantru_xuathang'   => 'p.cantru_xuathang',
            'number_ct'      	=> 'p.number_ct',
            'date_ct'     		=> 'p.date_ct'
        );
         
        $select->from(array('p'=> 'pay_costing'), $arrCols);
        $select->where('p.cost_id = ?',$params['id']);
        $result = $db->fetchAll($select);
        return $result;
    }
}