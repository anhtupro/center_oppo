<?php
class Application_Model_NotificationAccess extends Zend_Db_Table_Abstract
{
    protected $_name = 'notification_access';
	
    function fetchPaginationAccess($page, $limit, &$total, $params) {
    	 
    	$db = Zend_Registry::get('db');
    	$select = $db->select()
    	->from(array('p' => $this->_name),
    			array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
    	
        $select->joinLeft(array('s' => 'staff'), 's.id = p.user_id', array());
        $select->where('s.department <> ?', 157);

    	if (isset($params['status']) && $params['status'])
    		$select->where('p.notification_status = 0');
    
    	if (isset($params['filter']) && $params['filter']) {
    
    		$select	->join(array('n' => 'notification_new'), 'n.id=p.notification_id');
    		
	    		if (isset($params['pop_up']) && $params['pop_up']){
						$select->where('n.pop_up = ?', $params['pop_up']);
				}
			$select->where('n.status <> 0');	
			$today =  date('Y-m-d H:i:s');
            $select->where('n.category_id =10 OR n.title like "%phiếu lương %" OR n.show_from IS NULL OR n.show_from <= ?', $today);
            $select->where('n.category_id =10 OR n.title like "%phiếu lương %" OR n.show_to IS NULL OR n.show_to >= ?', $today);
	    		
    	}
    
    
    	if (isset($params['read'])) {
    		if ($params['read'])
    			$select->where('nr.notification_id IS NOT NULL', 1);
    		else
    			$select->where('nr.notification_id IS NULL', 1);
    	}
        
    
    	
    	if (isset($params['staff_id']) && $params['staff_id'])
    		$select->where('p.user_id = ?', $params['staff_id']);
    
    	$select->group('p.id');
        
		$select->order('p.notification_status');
		 $select->order('p.id DESC');
    	if($limit)
    		$select->limitPage($page, $limit);
    
    	$result = $db->fetchAll($select);
    
    	$total = $db->fetchOne("select FOUND_ROWS()");
    	return $result;
    }

    function fetchPaginationAccessPopUp($page, $limit, &$total, $params) {
    	 
    	$db = Zend_Registry::get('db');
    	$select = $db->select()
    	->from(array('p' => $this->_name),
    			array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
    	
        $select->joinLeft(array('s' => 'staff'), 's.id = p.user_id', array());
        $select->where('s.department <> ?', 157);

    	if (isset($params['status']) && $params['status'])
    		$select->where('p.notification_status = 0');
    
    	if (isset($params['filter']) && $params['filter']) {
    
    		$select	->join(array('n' => 'notification_new'), 'n.id=p.notification_id');
    		
	    		if (isset($params['pop_up']) && $params['pop_up']){
						$select->where('n.pop_up = ?', $params['pop_up']);
				}
            $select->where('p.passby_popup = 0');	
            $select->where('n.status <> 0');	
            
			$today =  date('Y-m-d H:i:s');
            $select->where('n.category_id =10 OR n.title like "%phiếu lương %" OR n.show_from IS NULL OR n.show_from <= ?', $today);
            $select->where('n.category_id =10 OR n.title like "%phiếu lương %" OR n.show_to IS NULL OR n.show_to >= ?', $today);
	    		
    	}
    
    
    	if (isset($params['read'])) {
    		if ($params['read'])
    			$select->where('nr.notification_id IS NOT NULL', 1);
    		else
    			$select->where('nr.notification_id IS NULL', 1);
    	}
        
    
    	
    	if (isset($params['staff_id']) && $params['staff_id'])
    		$select->where('p.user_id = ?', $params['staff_id']);
    
    	$select->group('p.id');
        
		$select->order('p.notification_status');
		 $select->order('p.id DESC');
    	if($limit)
    		$select->limitPage($page, $limit);
    
    	$result = $db->fetchAll($select);
    
    	$total = $db->fetchOne("select FOUND_ROWS()");
    	return $result;
    }

    const TITLE_RSM_ID = 308;
    const TITLE_ASM_ID = 179;
    const TITLE_ASM_STANDBY_ID = 181;
    const TITLE_SALE_LEADER_ID = 190;
    const TITLE_SALE_ID = 183;
    const TITLE_PG_ID = 182;

    /**
     * @param $userId
     * @param $userTitle
     * @throws Zend_Exception
     */
    public function addNotifyForSaleAppraisal($userId, $userTitle)
    {
        if(!in_array($userTitle, [
            // All Title link with first AppraisalSale 21/02/2019
            179, 181, 182, 183, 190, 293, 308, 391, 392, 419, 463, 464, 465, 467, 47,533,534,535,536,462
        ])) {
            return;
        }

        $QAppraisalSalePlan = new Application_Model_AppraisalSalePlan();
        $plan = $QAppraisalSalePlan->getLastPlan();
        
        

        if ($plan) { // check if there is an active appraisal sale plan
            $QAppraisalView = new Application_Model_AppraisalSaleView();
            
            
            
            if ($QAppraisalView->hasSurveyToDo($userId)){
                
                

                $QNotificationAccess = new Application_Model_NotificationAccess();
                $whereCurrentNotify = [
                    $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $userId),
                    $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', 316586),
                    $QNotificationAccess->getAdapter()->quoteInto('created_date = ?', date("Y-m-d")),
                ];
                $currentNotify = $QNotificationAccess->fetchRow($whereCurrentNotify);
                if (!$currentNotify) {
                    $QNotificationAccess->delete([
                        $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $userId),
                        $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', 316586),
                    ]);
                    $QNotificationAccess->insert([
                        'user_id'  =>  $userId,
                        'notification_id' => 316586,
                        'notification_from' =>  $plan['asp_from'],
                        'notification_to' => $plan['asp_to'],
                        'created_date' => date('Y-m-d')
                    ]);
                }
            }
            
            
        }
    }

    /**
     * @param $userId
     * @param $userTitle
     * @throws Zend_Exception
     */
    public function addNotifyForOfficeCapacityAppraisal($userId, $userTitle)
    {
        
        if(!in_array($userId, [6705, 8807, 7])) {
            return;
        }
        
        
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $plans = $QPlan->getActiveCapacityPlan();
        
        
        foreach ($plans as $plan) {
            $QAppraisal = new Application_Model_AppraisalOffice();
            if ($QAppraisal->checkCapacityPlanComplete($plan['aop_id'], $userId) && $plan['aop_notification_id']){
                $QNotificationAccess = new Application_Model_NotificationAccess();
                $whereCurrentNotify = [
                    $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $userId),
                    $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', $plan['aop_notification_id']),
                    $QNotificationAccess->getAdapter()->quoteInto('created_date = ?', date("Y-m-d")),
                ];
                $currentNotify = $QNotificationAccess->fetchRow($whereCurrentNotify);
                
                if (!$currentNotify) {
                    $QNotificationAccess->insert([
                        'user_id'  =>  $userId,
                        'notification_id' => $plan['aop_notification_id'],
                        'notification_from' =>  date("Y-m-d"),
                        'notification_to' => date("Y-m-d"),
                        'created_date' => date('Y-m-d')
                    ]);
                }
            }
        }
    }
    
    public function addNotifyForOfficePrdAppraisal($userId, $userTitle)
    {
//        if(!in_array($userId, [6705, 8807, 7, 7564])) {
//            return;
//        }
        
        if(!in_array($userId, [5899,7564,20915])) {
            return;
        }
        
        $QPlan = new Application_Model_AppraisalOfficePlan();
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $QMember = new Application_Model_AppraisalOfficeMember();
        $QAppraisal = new Application_Model_AppraisalOffice();
        
        $where = [];
        $where[] = $QPlan->getAdapter()->quoteInto('aop_status = ?', 1);
        $where[] = $QPlan->getAdapter()->quoteInto('aop_type = ?', 1);
        $where[] = $QPlan->getAdapter()->quoteInto('aop_is_deleted = ?', 0);
        $where[] = $this->getAdapter()->quoteInto("aop_from <= ?", date('Y-m-d'));
        $where[] = $this->getAdapter()->quoteInto("aop_to >= ?", date('Y-m-d'));
        $plans = $QPlan->fetchAll($where);
        
        $is_head = $QToDo->checkIsHead($userId);
        $is_staff = $QToDo->checkIsStaff($userId);
        
        foreach ($plans as $plan) {
            
            
            if($is_head == 1){
                $complete = $QMember->checkPrdHeadComplete($plan['aop_id'], $userId);
            }
            elseif($is_staff == 1){
                $complete = $QMember->checkPrdComplete($plan['aop_id'], $userId);
            }
            
            if ($complete && $plan['aop_notification_id']){
                $QNotificationAccess = new Application_Model_NotificationAccess();
                $whereCurrentNotify = [
                    $QNotificationAccess->getAdapter()->quoteInto('user_id = ?', $userId),
                    $QNotificationAccess->getAdapter()->quoteInto('notification_id = ?', $plan['aop_notification_id']),
                    $QNotificationAccess->getAdapter()->quoteInto('created_date = ?', date("Y-m-d")),
                ];
                $currentNotify = $QNotificationAccess->fetchRow($whereCurrentNotify);
                 
                if (!$currentNotify) {
                    $QNotificationAccess->insert([
                        'user_id'  =>  $userId,
                        'notification_id' => $plan['aop_notification_id'],
                        'notification_from' =>  date("Y-m-d"),
                        'notification_to' => date("Y-m-d"),
                        'created_date' => date('Y-m-d')
                    ]);
                }
            }
        }
    }
    
    
    
}