<?php
class Application_Model_PgcheckCategory extends Zend_Db_Table_Abstract
{
    protected $_name = 'pgcheck_category';
    
    public function getList(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name", 
            "p.type", 
            "p.is_del", 
        );

        $select->from(array('p' => 'pgcheck_category_group'), $arrCols);

        $select->where('p.is_del = 0');
        
        $select->order(['p.type', 'p.stt']);
        
        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $key=>$value){
            $data[$value['id']] = $value['name'];
        }
        
        return $data;
    }
    
    public function getCategory($channel_id_list){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.name',
            'p.type',
            'p.stt',
            'p.channel_id'
        );

        $select->from(array('p' => 'pgcheck_category'), $arrCols);

        $select->where('p.channel_id IN (?)', $channel_id_list);
        
        $select->order('p.stt ASC');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
}