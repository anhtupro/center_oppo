<?php

class Application_Model_DuplicatedImeiRm extends Zend_Db_Table_Abstract {

    protected $_name = 'duplicated_imei';
    protected $_schema = 'realme_hr';

    public function getStoreRmFromOppo($id_store_op) {
        $db = Zend_Registry::get('db');
        $sql = "SELECT s.id_rm FROM realme_hr.store_mapping s WHERE s.id_oppo = :id_oppo ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id_oppo', $id_store_op, PDO::PARAM_INT);
        $stmt->execute();
        $getData = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $getData['id_rm'];
    }

	public function checkSecondImei($data, $store_id_oppo) {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => HR_DB_RM.'.duplicated_imei'], [
                'd.*'
            ])
            ->join(['sm' => HR_DB_RM.'.store_mapping'], 'sm.id_rm = d.store_id', [])
            ->where('MONTH(d.date) = ?', $data['month_check'])
            ->where('d.imei = ?', $data['imei_check'])
            ->where('sm.id_oppo = ?', $store_id_oppo);

        $getData = $db->fetchAll($select)      ;


//        $db = Zend_Registry::get('db');
//		$sql = "
//			SELECT d.*
//			FROM realme_hr.duplicated_imei d
//			INNER JOIN realme_hr.`store_mapping` sm ON sm.`id_rm` = d.`store_id`
//			WHERE MONTH(d.`date`) = " . $data['month_check'] . " AND d.`imei` = " . $data['imei_check'] . " AND sm.`id_oppo` = " . $store_id_oppo . "";
//
//        $stmt = $db->prepare($sql);
//        $stmt->execute();
//        $getData = $stmt->fetchAll();
//        $stmt->closeCursor();
//        $db = $stmt = null;
        return $getData;
    }
	
	public function getIdPgTimingSaleRm($imei) {
        $db = Zend_Registry::get('db');
        $sql = "SELECT ts.`id` timing_sales_first, t.`staff_id` staff_id_first
				FROM realme_hr.timing_sale ts
				INNER JOIN realme_hr.timing t ON t.`id` = ts.`timing_id`
				WHERE ts.`imei` = :imei";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
        $stmt->execute();
        $getData = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $getData;
    }
}
