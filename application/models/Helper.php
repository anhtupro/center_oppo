<?php

class Application_Model_Helper extends Zend_Db_Table_Abstract
{
    public function normalizedArrayFile($array)
    {
        foreach($array as $index => $file) {

            if (!is_array($file['name'])) {
                $normalized_array[$index][] = $file;
                continue;
            }

            foreach($file['name'] as $idx => $name) {
                if($name) {
                    $normalized_array[$index][$idx] = [
                        'name' => $name,
                        'type' => $file['type'][$idx],
                        'tmp_name' => $file['tmp_name'][$idx],
                        'error' => $file['error'][$idx],
                        'size' => $file['size'][$idx]
                    ];
                }
            }
        }

        return $normalized_array;

    }
}