<?php
class Application_Model_BudgetContract extends Zend_Db_Table_Abstract
{
    protected $_name = 'budget_contract';
    
    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>'budget_contract'),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.*',
                            'department_name' => 't.name',
                        ))
            ->joinLeft(array('t'=>'team'),'p.department_id = t.id',array())  
            ->order('p.id DESC');
         

        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        return $result;
    }
    
    function getTitle(){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>'budget_contract'),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.id',
                            'p.name',
                        ))            
            ->order('p.id DESC');
        $result = $db->fetchAll($select);
        return $result;
    }
}
